<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
#example td:last-child {
	text-align:left  !important;
}
</style>
	<script type="text/javascript">
		var aJaxURL	          = "server-side/view/task_type.action.php";		//server side folder url
		var tName	          = "example";													//table name
		var fName	          = "add-edit-form";
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";												//form name
		    	
		$(document).ready(function () {        	
			LoadTable();	
			SetEvents("", "", "", tName, fName, aJaxURL);
		});
        
		function LoadTable(){
			GetDataTable(tName, aJaxURL, "get_list", 2, "", 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}
		
		function LoadDialog(){
			var id		= $("#task_id").val();
			
			/* Dialog Form Selector Name, Buttons Array */
			GetDialog(fName, 360, "auto", "", "center top");
		}

		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_task";
	    	param.id		= $("#task_id").val();
	    	param.name		= $("#name").val();
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	   
    </script>
</head>

<body>
    <div id="tabs">
	<div class="l_responsible_fix">
    	<div class="callapp_head">დავალების ტიპები<hr class="callapp_head_hr"></div>
    	<div id="button_area">
			<button style="display:none" id="add_button">დამატება</button>
			<button style="display:none" id="delete_button">წაშლა</button>
		</div>
		<table id="table_right_menu" style="top: 37px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">
                </td>
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
                	<img alt="link" src="media/images/icons/select.png" height="14" width="14">
                </td>
            </tr>
        </table>
        <table class="display" id="example" style="width: 100%">
            <thead >
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 100%;">დასახელება</th>
                </tr>
            </thead>
            <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                    	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                    </th>
                  	
                </tr>
            </thead>
        </table>
		</div>
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ტიპი">
    	<!-- aJax -->
	</div>
</body>
</html>





