<?php

// MySQL Connect Link
require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$action        = $_REQUEST['act'];
//$user_id       = $_SESSION['USERID'];
$count=0;
$data=array();
$row_count_manual=0;
$row_count_auto_dialer=0;
$sub_select='';
$sub_type1='';
$error='';
switch($action){

    case 'change_sub_tee':
        $sub_tree_id=$_REQUEST['sub_tree_id'];
        $sub_select= get_sub_tree($sub_tree_id);
        break;


    case 'get_tabs' :

        $c=$_REQUEST['c'];
        $t_c=$_REQUEST['t_c'];

        $page		= get_tabs(get_parent_status(),$c,$t_c);

        $data		= array('page'	=> $page);

        break;

    case 'get_list_manual':
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $tree_id=$_REQUEST['tree_id'];
        $id=$_REQUEST['id'];

        $db->setQuery("UPDATE autocall_request ar 
                       JOIN   autocall_request_details ard on ard.request_id=ar.id
                       JOIN   autocall_request_base arb on arb.request_id=ard.id
                          SET arb.actived_status=0
                       WHERE  arb.actived=1 and ard.actived=1 and ar.actived=1 and arb.number_8 + INTERVAL ar.duration DAY < NOW()");
        $db->execQuery();
        if($tree_id==-1){
            $db->setQuery("SELECT arb.id,
                                  ard.set_time,
                                  ar.`name`,
                                  ar.`code`,
                                  arb.phone_number,
                                  ard.comment
                            FROM  autocall_request ar
                            JOIN  autocall_request_details ard on ar.id=ard.request_id and ar.call_type_id=3
                            JOIN  autocall_request_base arb on arb.request_id=ard.id
                            WHERE ar.actived=1 and arb.actived=1 and ard.actived=1 and arb.actived_status=0 ");
            
        }else{
            $db->setQuery(" SELECT arb.id,
                                   ard.set_time,
                                   ar.`name`,
                                   ar.`code`,
                                   arb.phone_number,
                                   ard.comment
                            FROM   autocall_request ar
                            JOIN   autocall_request_details ard on ar.id=ard.request_id and ar.call_type_id=3
                            JOIN   autocall_request_base arb on arb.request_id=ard.id
                            WHERE  ar.actived=1 and arb.actived=1 and ard.actived=1 and arb.actived_status=1 and arb.tree_id='$tree_id'");
        }



        $data = $db->getList($count, $hidden, 1);
        break;
    case 'get_list_auto_dialer':
        $count   = $_REQUEST['count'];
        $hidden  = $_REQUEST['hidden'];
        $tree_id = $_REQUEST['tree_id'];
        $id      = $_REQUEST['id'];

        $db->setQuery(" UPDATE autocall_request ar 
                        JOIN   autocall_request_details ard on ard.request_id=ar.id
                        JOIN   autocall_request_base arb on arb.request_id=ard.id
                           SET arb.actived_status=0
                        WHERE  arb.actived=1 and ard.actived=1 and ar.actived=1 and arb.number_8 + INTERVAL ar.duration DAY < NOW()");

        if($tree_id==-1){
            $db->setQuery(" SELECT arb.id,
                                   ard.set_time,
                                   ar.`name`,
                                   ar.`code`,
                                   arb.phone_number,
                                   ard.comment
                            
                            FROM   autocall_request ar
                            JOIN   autocall_request_details ard on ar.id=ard.request_id and ar.call_type_id=2
                            JOIN   autocall_request_base arb on arb.request_id=ard.id
                            WHERE  ar.actived=1 and arb.actived=1 and arb.actived_status=0 and ard.actived='1'");

        }else{
            $db->setQuery("SELECT arb.id,
                                  ard.set_time,
                                  ar.`name`,
                                  ar.`code`,
                                  arb.phone_number,
                                  ard.comment
                            
                            FROM  autocall_request ar
                            JOIN  autocall_request_details ard on ar.id=ard.request_id and ar.call_type_id=2
                            JOIN  autocall_request_base arb on arb.request_id=ard.id
                            WHERE ar.actived=1 and arb.actived=1 and arb.actived_status=1 and ard.actived='1' and arb.tree_id='$tree_id'");
        }
        $data = $db->getList($count, $hidden,1);
        
        break;
    case 'sava-manual':
        $request_base_id = $_REQUEST['request_base_id'];
        $first_type      = $_REQUEST['first_type'];
        $sub_type        = $_REQUEST['sub_type'];
        $comment         = $_REQUEST['comment'];
        
        $db->setQuery("SELECT tree_id FROM `autocall_request_base` WHERE id = '$request_base_id'");
        
        $sub_type_querry = $db->getResultArray();
        $sub_type1       = $sub_type_querry[result][0]['tree_id'];

        $db->setQuery("UPDATE autocall_request_base 
                          SET tree_id = '$sub_type',
                              comment = '$comment'
                       WHERE  id      = '$request_base_id'");
        $db->execQuery();
        
        $data		= array('page'	=> $page);
        break;

    default : echo ("error");


}

$data['sub_type']=$sub_type1;
$data['sub_tree']=$sub_select;
$data['error']=$error;
$data['error_crm'] = $error;
echo json_encode($data);


function get_sub_tree($sub_tree_id){
    $data_select='<select  id="sub_type" class="idle" style="width: 95%"  readonly>' . get_nick($sub_tree_id) . '</select>';

    return $data_select;
}

function get_nick($nick_id){
    global $db;
    $data = '';

    $db->setQuery("SELECT * FROM `add_page_tree` WHERE  actived=1 AND parent_id='$nick_id'");
    $req = $db->getResultArray();
    $count=0;
    foreach($req[result] AS $res) {
        if ($count==0) {
            $data .= '<option value="'.$res['id'].'" selected="selected">' . $res['name'] . '</option>';
            $count++;
        } else  {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    return $data;
}


function get_tabs($res,$c1,$t_c1){
    global $db;
    $column_number=0;
    if($c1==1){
        $length=$db->getNumRow();
        $datafull="";
        $data2='';
        $css_li1='<style>
    #ul_tab'.$t_c1.' li{
                display: inline;
                margin: 13px;
                
                color: #0b5c9a;
                border-bottom: 1px solid gray;
                padding-bottom: 7px;
                cursor: pointer;
            }
            #ul_tab'.$t_c1.'{
                margin-left: -10px;
            }
            #ul_tab'.$t_c1.' input{
                width: 50px;
                border: 1px solid gray;
                border-width: 1px;
                border-radius: 22px;
                text-align: center;
                background-color: white;
                margin-left: 5px;
                color: gray;
                margin-top: -5px;
                height: 15px;
            }
            #ul_tab'.$t_c1.' a{
                color: gray;
            }
';
        $css_li2='</style>';
        $js_li1='<script>';
        $js_li5='</script>';
        $data='<ul id="ul_tab'.$t_c1.'">';
        global $count;
        $c=$c1;
        $t_c=$t_c1;
        $js_child_f='';
        $chil_li=0;
        $check_li=1;
        $js_li3='';
        $js_li2='function change_li_color'.$t_c1.'(x,id) {';
        $js_li4=' x.style.color="black";
            x.style.borderBottomWidth="2px";
            x.style.borderBottomColor="#0b5c9a";
            x.children[0].style.color="black";
            
            var i='.$length.';
            
            while(i>=1){
            var ii='.$t_c1.'+i;
            document.getElementById(`ul_tab${ii}`).style.display="none";
            i--;
            }
            
            document.getElementById(`ul_tab${id}`).style.display="block";
            
            if(id==21){

            document.getElementById("23").click();
            }else  if(id==31){

            document.getElementById("36").click();
            
            }
            
            }';
        $cc1=1;
        foreach ($res AS $arr){

            if($arr['parent_id']==0){
                $count++;
                $c++;
                $css_li1.='#ul_tab'.$c.' li{
                display: inline;
                margin: 13px;
                
                color: #0b5c9a;
                border-bottom: 1px solid gray;
                padding-bottom: 7px;
                cursor: pointer;
            }
            #ul_tab'.$c.'{
                // margin-left: -10px;
            }
            #ul_tab'.$c.' input{
                width: 50px;
                border: 1px solid gray;
                border-width: 1px;
                border-radius: 22px;
                text-align: center;
                background-color: white;
                margin-left: 5px;
                color: gray;
                margin-top: -5px;
                height: 15px;
            }
            #ul_tab'.$c.' a{
                color: gray;
            }';
                $ss=$chil_li;
                $js_li3.='document.getElementById("ul_tab'.$t_c.'").children['.$chil_li.'].style.borderBottomWidth="1px";
            document.getElementById("ul_tab'.$t_c.'").children['.$chil_li.'].style.borderBottomColor="gray";
            document.getElementById("ul_tab'.$t_c.'").children['.$chil_li.'].children[0].style.color="gray";
           
            ';



                $js_child_f_check=0;
                $js_child_f.='function change_li_color'.$c.'(x,id) {';
                $chil_li++;
                $check_li=1;

                if($cc1==1){
                    $data   .='<li class="main_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'.$c.'" onclick="change_li_color'.$t_c1.'(this,this.id); call_first_tab(this.id);" style="color: black; border-bottom-width: 2px; border-bottom-color: #0b5c9a; " ><a style="color: black;text-decoration: none" href="#tab-'.$c.'">'.$arr['name'].'</a></li>';

                }else{
                    $data   .='<li class="main_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'.$c.'" onclick="change_li_color'.$t_c1.'(this,this.id); call_first_tab(this.id);"  ><a href="#tab-'.$c.'" style="text-decoration: none">'.$arr['name'].'</a></li>';

                }
                $cc1++;
                if($c==$t_c1+2){
                    $data2  .='<ul id="ul_tab'.$c.'" style="margin-top: 15px; display:none">  ';
                }else{
                    $e=$c+t_c1;
                    $data2  .='<ul id="ul_tab'.$e.'" style="margin-top: 15px;">  ';
                }



                $child_req = get_child_status($arr['id']);

                foreach($child_req AS $child_res){
                    $js_child_f.='document.getElementById("ul_tab'.$c.'").children['.$js_child_f_check.'].style.borderBottomWidth="1px";
                                  document.getElementById("ul_tab'.$c.'").children['.$js_child_f_check.'].style.borderBottomColor="gray";
                                  document.getElementById("ul_tab'.$c.'").children['.$js_child_f_check.'].children[0].style.color="gray";';

                    $data2.=get_child_tabs($child_res,$child_res['parent_id'],$c,$check_li,$column_number,$c1);
                    $check_li++;

                    $js_child_f_check++;
                }
                $js_child_f.='x.style.color="black";
                              x.style.borderBottomWidth="2px";
                              x.style.borderBottomColor="#0b5c9a";
                              x.children[0].style.color="black";}';

                $data2.='</ul>';
            }

        }
        $data.='</ul>';
        $css_li1.=$css_li2;
        $js_li1.=$js_li2.$js_li3.$js_li4.$js_child_f.$js_li5;

        $datafull.=$js_li1;
        $datafull.=$css_li1;
        $datafull .=  $data;
        $datafull .=  $data2;
    }else if($c1==30){
        $length=$db->getNumRow();
        $datafull="";
        $data2='';
        $css_li1='<style>
                        #ul_tab'.$t_c1.' li{
                            display: inline;
                            margin: 13px;
                            
                            color: #0b5c9a;
                            border-bottom: 1px solid gray;
                            padding-bottom: 7px;
                            cursor: pointer;
                        }
                        #ul_tab'.$t_c1.'{
                            margin-left: -10px;
                        }
                        #ul_tab'.$t_c1.' input{
                            width: 50px;
                            border: 1px solid gray;
                            border-width: 1px;
                            border-radius: 22px;
                            text-align: center;
                            background-color: white;
                            margin-left: 5px;
                            color: gray;
                            margin-top: -5px;
                            height: 15px;
                        }
                        #ul_tab'.$t_c1.' a{
                            color: gray;
                        }';
        $css_li2='</style>';
        $js_li1='<script>';
        $js_li5='</script>';
        $data='<ul id="ul_tab'.$t_c1.'">';
        global $count;
        $c=$c1;
        $t_c=$t_c1;
        $js_child_f='';
        $chil_li=0;
        $check_li=1;
        $js_li3='';
        $js_li2='function change_li_color'.$t_c1.'(x,id) {';
        $js_li4=' x.style.color="black";
                  x.style.borderBottomWidth="2px";
                  x.style.borderBottomColor="#0b5c9a";
                  x.children[0].style.color="black";
            
                  var i='.$length.';
            
            while(i>=1){
                var ii='.$t_c1.'+i;
                document.getElementById(`ul_tab${ii}`).style.display="none";
                i--;
                }
                
                document.getElementById(`ul_tab${id}`).style.display="block";
                
                if(id==21){
                    document.getElementById("23").click();
                }else  if(id==31){
                    document.getElementById("36").click();
                
                }
            
            }';
        $cc1=1;
        foreach($res AS $arr){

            if($arr['parent_id']==0){
                $count++;
                $c++;
                $css_li1.='#ul_tab'.$c.' li{
                display: inline;
                margin: 13px;
                
                color: #0b5c9a;
                border-bottom: 1px solid gray;
                padding-bottom: 7px;
                cursor: pointer;
            }
            #ul_tab'.$c.'{
                margin-left: -10px;
            }
            #ul_tab'.$c.' input{
                width: 50px;
                border: 1px solid gray;
                border-width: 1px;
                border-radius: 22px;
                text-align: center;
                background-color: white;
                margin-left: 5px;
                color: gray;
                margin-top: -5px;
                height: 15px;
            }
            #ul_tab'.$c.' a{
                color: gray;
            }';
                $ss=$chil_li;
                $js_li3.='
                          document.getElementById("ul_tab'.$t_c.'").children['.$chil_li.'].style.borderBottomWidth="1px";
                          document.getElementById("ul_tab'.$t_c.'").children['.$chil_li.'].style.borderBottomColor="gray";
                          document.getElementById("ul_tab'.$t_c.'").children['.$chil_li.'].children[0].style.color="gray";';



                $js_child_f_check=0;
                $js_child_f.='function change_li_color'.$c.'(x,id) {';
                $chil_li++;
                $check_li=1;

                if($cc1==1){
                    $data   .='<li class="main_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'.$c.'" onclick="change_li_color'.$t_c1.'(this,this.id); call_first_tab(this.id);" style="color: black; border-bottom-width: 2px; border-bottom-color: #0b5c9a; " ><a style="color: black;text-decoration: none" href="#tab-'.$c.'">'.$arr['name'].'</a></li>';

                }else{
                    $data   .='<li class="main_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'.$c.'" onclick="change_li_color'.$t_c1.'(this,this.id); call_first_tab(this.id);"  ><a href="#tab-'.$c.'" style="text-decoration: none">'.$arr['name'].'</a></li>';

                }
                $cc1++;
                if($c==$t_c1+2){
                    $data2  .='<ul id="ul_tab'.$c.'" style="margin-top: 30px; display:none">  ';
                }else{
                    $e=$c+t_c1;
                    $data2  .='<ul id="ul_tab'.$e.'" style="margin-top: 30px;">  ';
                }



                $child_req = get_child_status($arr['id']);

                foreach($child_req AS $child_res){
                    $js_child_f.='document.getElementById("ul_tab'.$c.'").children['.$js_child_f_check.'].style.borderBottomWidth="1px";
                                  document.getElementById("ul_tab'.$c.'").children['.$js_child_f_check.'].style.borderBottomColor="gray";
                                  document.getElementById("ul_tab'.$c.'").children['.$js_child_f_check.'].children[0].style.color="gray";';

                    $data2.=get_child_tabs($child_res,$child_res['parent_id'],$c,$check_li,$column_number,$c1);
                    $check_li++;

                    $js_child_f_check++;
                }
                $js_child_f.='x.style.color="black";
                              x.style.borderBottomWidth="2px";
                              x.style.borderBottomColor="#0b5c9a";
                              x.children[0].style.color="black";}';

                $data2.='</ul>';
            }

        }
        $data.='</ul>';
        $css_li1.=$css_li2;
        $js_li1.=$js_li2.$js_li3.$js_li4.$js_child_f.$js_li5;

        $datafull.=$js_li1;
        $datafull.=$css_li1;
        $datafull .=  $data;
        $datafull .=  $data2;
    }



    return $datafull;
}


function get_child_tabs($arr,$id,$check_li,$ck,$c_n,$c1){

    if($c1==1){
        $data='';
        $tab_li=$check_li+$ck+$arr['id'];
        $tab_li++;
        if($arr['parent_id']==$id && $arr['parent_id']!=0 )
        {
            if($ck==1){
                $data.='<li class="child_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'. $tab_li.'"  onclick="change_li_color'.$check_li.'(this,this.id); call_table'.$c1.'(this.id);" style="color: black; border-bottom-width: 2px; border-bottom-color: #0b5c9a; "><a style="color: black; text-decoration: none"  href="#dt_example">'.$arr['name'].'</a></li>';

            }else{
                $data.='<li  class="child_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'. $tab_li.'" onclick="change_li_color'.$check_li.'(this,this.id); call_table'.$c1.'(this.id);"><a href="#tab-'.$tab_li.'" style="text-decoration: none">'.$arr['name'].'</a></li>';

            }
        }

    }else if($c1==30){
        $data='';
        $tab_li=$check_li+$ck+$arr['id'];
        $tab_li++;
        if($arr['parent_id']==$id && $arr['parent_id']!=0 )
        {
            if($ck==1){
                $data.='<li  class="child_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'. $tab_li.'"  onclick="change_li_color'.$check_li.'(this,this.id); call_table'.$c1.'(this.id);" style="color: black; border-bottom-width: 2px; border-bottom-color: #0b5c9a; "><a style="color: black; text-decoration: none"  href="#dt_example">'.$arr['name'].'</a></li>';

            }else{
                $data.='<li  class="child_li_'.$arr['id'].'" tree_id="'.$arr['id'].'" id="'. $tab_li.'" onclick="change_li_color'.$check_li.'(this,this.id); call_table'.$c1.'(this.id);"><a href="#tab-'.$tab_li.'" style="text-decoration: none">'.$arr['name'].'</a></li>';

            }
        }

    }
    return $data;
}


function get_parent_status(){
    global $db;
    $db->setQuery("SELECT *
                   From `add_page_tree`
                   where actived=1 and parent_id = 0");
    $res = $db->getResultArray();
    return $res[result];
}
function get_child_status($id){
    global $db;
    $db->setQuery("SELECT *
                   From `add_page_tree`
                   where actived=1 and parent_id = '$id'");
    $res = $db->getResultArray();
    return $res[result];
}




?>