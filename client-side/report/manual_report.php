<style type='text/css'>
        .table_button_panel{
            transform:translateY(-300px);
        }
        .fg-toolbar {
            transform:translateY(-300px);
        }
        #example{
            transform:translateY(-300px);
        }
        .chosen-container .chosen-results{
            height:auto;
        }
        #example_wrapper{
            margin-left:10%;
            width:80%;
            margin-top:50px;
        }
        #operators{
            margin-left:13%;
            margin-top:20px;
        }
        #content_container {
            margin-left: 170px;
        }
</style>
<script type='text/javascript'>
var aJaxURL = 'server-side/report/manual_report.action.php';
var tName = 'example';
var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
var dialog                  = "add-edit-form";
    $(document).ready(function () {
        
        $('#show_report').button();
      
        GetDate('start_date');
        GetDate('end_date');
        LoadTable();
        GetButtons("add_button_task", "delete_button_task");
        SetEvents("add_button_task", "delete_button", "check-all", tName, dialog, aJaxURL);
    });
    function LoadTable(){
        GetDataTable(tName, aJaxURL, 'show_report', 5, '&start_date='+$('#start_date').val()+'&end_date='+$('#end_date').val(), 0, '', 1, 'asc', '', change_colum_main);
        setTimeout(function(){  $('.ColVis, .dataTable_buttons').css('display','none');}, 90);
    }
    $(document).on('click', '#show_report', ()=>{
        var obj = new Object();
        obj.act ='show_report';
        obj.start_date  = $('#start_date').val();
        obj.end_date    = $('#end_date').val();
        $.ajax({
            url:aJaxURL,
            data: obj,
            success:(data)=>{
                LoadTable();
            }
        })
    });
  
    

        function LoadDialog() {
            
                    var id = $("#department_id").val();

                    var buttons = {
                        "appeal": {
                            text: "საჩივარი",
                            id: "appeal_button"
                        },
                        "save": {
                            text: "შენახვა",
                            id: "save-dialog"

                        },
                        "cancel": {
                            text: "გაუქმება",
                            id: "cancel-dialog",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }



                    };

                    GetDialog('add-edit-form', 792, "auto", buttons);
                

                   
              

            }
        


        
        
</script>
</head>
<body>
<div style='margin-top: 12px; margin-left: 5px;' class='callapp_head'>ავტორიზაციის ფილტრი<hr class='callapp_head_hr'></div>
<div id='content_container'>
    <div id='operators'>
        <span style='margin-left:20px;' >დრო</span>
        <input type = 'text' id = 'start_date' class='datetimes'/> -დან
        <input type = 'text' id = 'end_date' class='datetimes'/> -მდე
        <button id='show_report' style='margin-left:20px;'>რეპორტის ჩვენება</button>
    </div>
    <div id="button_area">
        <button style="margin-bottom:5px;" id="add_button_task">ახალი დიალოგი</button>
        <button id="delete_button_task">წაშლა</button>
    </div>
    <table class='display' id='example' style='width:80%; margin-left:10%;'>
        <thead>
            <tr>
                <th>ID</th>
                <th style='width: 10%; text-align: center;' >ID</th>
                <th style='width: 30%; text-align: center;' >ფართი კვ/მ</th>
                <th style='width: 30%; text-align: center;'>ფასი</th>
                <th style='width: 30%; text-align: center;'>ფასი განვადებით</th>
                <th style='width: 30%; text-align: center;'>#</th>
            </tr>
        </thead>
        <thead>
            <tr class='search_header'>
                <th class='colum_hidden'>
                    <input type='text' name='search_category' value='ფილტრი' class='search_init' />
                </th>
                <th>
                    <input type='text' name='search_category' value='ფილტრი' class='search_init' />
                </th>
                <th>
                    <input type='text' name='search_category' value='ფილტრი' class='search_init' />
                </th>
                <th>
                    <input type='text' name='search_category' value='ფილტრი' class='search_init' />
                </th>
                <th>
                    <input type='text' name='search_category' value='ფილტრი' class='search_init' />
                </th>
                <th>
                    <input type='text' name='search_category' value='ფილტრი' class='search_init' />
                </th>
            </tr>
        </thead>
    </table>
</div>
<div  id="add-edit-form" class="form-dialog" title="დავალება">
</div>
</body>


