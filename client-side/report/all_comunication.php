<head>
<style type="text/css">

.hidden{
	display: none;
}
.high {
    height: 15px;
}
.callapp_head{
	font-family: pvn;
	font-weight: bold;
	font-size: 20px;
	color: #2681DC;
}
.callapp_head_hr{
	border: 1px solid #2681DC;
}
.callapp_refresh{
    padding: 5px;
    border-radius:3px;
    color:#FFF;
    background: #9AAF24;
    float: right;
    font-size: 13px;
    cursor: pointer;
}
.ui-widget-content {
    border: 0px solid #2681dc;
}

#tabs {
    top: 0;
}
#table_right_menu {
	top: 56px;
}

</style>
<script>
$(document).on('change','#report_type_selector', function(){
    var report = $("#report_type_selector").val();
    if(report == "ზარები"){
        window.location.replace("index.php?pg=140&report=1");
    }
    if(report == "ჩატი"){
        window.location.replace("index.php?pg=140&report=2");
    }
    if(report == "Messenger"){
        window.location.replace("index.php?pg=140&report=3");
    }
    if(report == "Mail"){
        window.location.replace("index.php?pg=140&report=4");
    }
    if(report == "Viber"){
        window.location.replace("index.php?pg=140&report=5");
    }
});
</script>
</head>

<body>
<?php
if(isset($_GET['report']) and !empty($_GET['report']))
{
    $report = $_GET['report'];
    if($report == 1)
        loadReport("calls_report");
    if($report == 2)
        loadReport("chat_report");
    if($report == 3)
        loadReport("fb_report");
    if($report == 4)
        loadReport("mail_report");
    if($report == 5)
        loadReport("viber_report");
}
else{
    loadReport("calls_report");
}


function loadReport($opt){
    require_once "client-side/report/".$opt.".php";
}
?>

</body>