<html>
<head>
<style>
    .ui-tabs .ui-tabs-panel {
        display: block!important;
        border-width: 0;
        padding: 0.2em 0em;
        background: none;
    }

    .ui.tabs li[aria-selected="true"] input {
        background-color: green !important
    }
    #tabs1 div {
        border:none;
    }
    #tabs1{
        height:76px!important;
        margin-bottom:50px;
    }
    a{
        cursor:pointer!important;
    }

    .download_shablon {
        background-color:#4997ab;
        border-radius:2px;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:arial;
        font-size:14px;
        border:0px;
        text-decoration:none;
        text-overflow: ellipsis;
    }

    .download_shablon:hover {
        background-color:#ffffff;
        color:#4997ab;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
    }

    .ui-dialog-titlebar-close{
        display: none;
    }
    /*-INFO TOP MENU-*/
    .ifo-top-menu {
        width: 100%; height: 26px;
        margin-top: -50px;
        margin-left: -10px;
        position: relative;
        border-bottom: 1px solid #A3D0E4;
    }

    /*menu buttons*/
    .itm-unit {
        height: 100%;
        padding: 0 16px;
        float: left;
        background: none;
        border-width: 1px;
        border-style: solid;
        border-color: transparent;
        outline: none;
        cursor: pointer;
        font-family: pvn !important;
    }

    .itm-unit-active {
        border-color: #A3D0E4;
        border-bottom: none;
        background: #E6F2F8;
    }

    .itm-unit:not(.itm-unit-active):hover {
        color: #158da4;
    }


    /*hide side menu*/
    .hide_said_menu {
        height: 17px;
        top: 6px; right: 12px;
    }

    .ifo-top-menu .hide_said_menu {
        height: 15px;
        top: 4px; right: -21px;
        border: 1px solid #A3D0E4;
        background: #E6F2F8;
    }

    /*info section*/
    .inc-info-section {
        display: none;
    }

    .is-active {
        display: block;
    }

    /*info multiple selects section side container unit*/
    .iiss-sc-unit {
        width: 100%; height: auto;
        margin-right: 10px;
        margin-top: 7px;
        box-sizing: content-box;
    }

    

    /*info page category filter display*/
    .cat-listener {
        display: none;
    }

    .incomming-cat-visible {
        display: block;
    }

    /*input edit button styles*/
    .input-edit-button {
        width: 37px !important; height: 21px !important;
    }
    .section-separator {
        width: 102.8%; height: 10px;
        background: white;
        margin-left: -11px;
        border-top: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
    }

    .hidden{
        display: none;
    }
    #table_index td:nth-child(12) {
        padding:0px;
    }

    

    #question 
    {
        overflow-y : scroll;
        height: 584px;
    }
    
    #get_answer
    {
        cursor:pointer;
    }

    #project_info_table {
        border-collapse: collapse !important;
    }

    #right_side2 {
        width: 620px;
        float: left;
        margin-left: 4px;
        height: 571px;
        overflow-y: auto;
    }

    .block-area {
        background: #0000000d;
        top: 0;
        display: block;
        width: 55.9%;
        position: absolute;
        height: 100%;
        z-index: 1;
        border-radius: 13px;
        
    }

    /*empty input style*/
    .empty-input {
        animation: empty-input .3s ease forwards;
        -webkit-animation: empty-input .3s ease forwards;
        -moz-animation: empty-input .3s ease forwards;
    }

    /*inputs*/
    .main-container input {
        background: none;
        outline: none;
        padding-left: 6px;
    }


    /*main container*/
    .main-container {
        max-width: 860px; height: 543px !important;
        margin-left: 0;
        font-size: 11px;
        letter-spacing: .7px;
        line-height: 21px;
        user-select: none;
        overflow: hidden;
        overflow-y: scroll;
        padding: 21px !important;
    }

    /*print wrapper*/
    .print-wrapper {
        width: 100%; height: auto;
    }

    /*header*/
    header {
        width: 100%; height: 52px;
        display: -webkit-flex;
        display: flex;
    }


    .header-lefter {
        -webkit-flex-grow: 1;
        flex-grow: 1;
    }

    #appCompleteDate {
        border: none;
        outline: none;
        border-bottom: 1px solid black;
        position: relative;
        top: 3px;
    }

    .header-img {
        -webkit-flex-basis: 141px;
        flex-basis: 141px;
        height: 100%;
    }

    .header-img img {
        height: 100%;
    }

    /*title holder*/
    .title-holder {
        width: 100%;
        padding: 8px;
        margin: 15px 0;
        text-align: center;
    }

    .title-holder h1 {
        font-size: 13px;
        font-family: pvn;
    }
    
    #add_comment
    {
        margin-left: 5px;
        top: -6px;
    }

    #edit_coment
    {
        height:13px;
    }

    #delete_coment
    {
        height:13px;
    }

    .tb_head td{
        border-right: 1px solid #E6E6E6;
    }

    .td_center{
        text-align: center !important;
    }
    .callapp_tabs{
        margin-top: 5px;
        margin-bottom: 5px;
        float: right;
        width: 100%;

    }
    .callapp_tabs span{
        color: #FFF;
        border-radius: 5px;
        padding: 5px;
        float: left;
        margin: 0 3px 0 3px;
        background: #2681DC;
        font-weight: bold;
        font-size: 11px;
        margin-bottom: 2px;
    }

    .ColVis, .dataTable_buttons{
        z-index: 50;
    }
   
    #table_right_menu{
        top: 39px;
    }
   
    .ColVis, .dataTable_buttons{
        z-index: 50;
    }
    
    #table_right_menu{
        top: 23px;
    }
    
    #table_index thead th:last-child .DataTables_sort_wrapper{
        display: none;
    }


    .dialog-right-side {
        width: 67%;
        float: left;
        margin-left: 10px;
    }
    
    
        [data-selector="info"] td{
            vertical-align: middle;
            height:16px !important;
            width:150px;
        }
        [data-selector="info"] input{
            height:16px !important;
            width:120px;
        }
        [data-selector="info"]{
            height:550px;
            overflow-y: auto;
        }
        [data-selector="info"] legend{
            font-size:11px;
        }
   
        #loan_plan_button .ui-button-text{
            padding: 1px 8px 0px 8px;
        }
        .comment_table input, .comment_table textarea{
            width: 99.1%;
            resize: vertical;
        }
        .comment_table textarea{
            resize: vertical;
        }
        #right_side{
            width: 620px;
            float: left;
            margin-left: 4px;
            height: 571px;
            overflow-y: auto;
        }
        #right_side fieldset {
            margin-bottom: 10px;
        }
        
        [data-select="chosen"] {
            width: 100%;
        }
        
        #info table{
            border-collapse:separate; 
            border-spacing:0 0.2em;
        }

     
        /* ifo top menu */
        .ifo-top-menu {
            width: 100%; 
            height: 36px;
            margin-top: -13px;
            margin-left: -10px; 
            position: relative;
            border-bottom: 1px solid #A3D0E4;
        }
        .itm-unit {
            height: 100%;
            padding: 0 5px;
            float: left;
            background: none;
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            outline: none;
            cursor: pointer;
            font-family: pvn !important;
            font-weight:700;
        }
        .itm-unit-active {
            border-color: #A3D0E4;
            border-bottom: none;
            background: #E6F2F8;
        }
        .itm-unit:not(.itm-unit-active):hover {
            color: #158da4;
        }
        .inc-info-section {
            display: none;
        }

        .is-active {
            display: block;
        }
        .iiss-sc-unit {
            width: 100%; height: auto;
            margin-right: 10px;
            margin-top: 7px;
            box-sizing: content-box;
        }
        /* ifo top menu end*/
        /* task submenu */
        #task_info_table tr{
            width:100%;
        }
        #task_info_table td{
            width:230px;
        }
        #task_info_table input{
            width:150px;
        }
        #task_info_table select{
            width:155px;
        }
        /* task submenu end */

        
        #add_comment{
            margin-left: 5px;
            top: -6px;
        }
        #get_answer{
            cursor:pointer;
        }
        /* left side fieldset css */
        #left_side_fieldset input{
            width: 140px;
        }
        #left_side_fieldset select{
            width: 145px!important;
        }
        #left_side_fieldset textarea{
            height:69px;
            margin:0;
            resize:vertical;
            width:93%
        }
        #left_side_fieldset td {
            font-family: BPG !important;
            padding: 3px 4px;
        }
      
        /* main table status */
        .read-action {
            width: 100%;
			height: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
			font-family: pvn;
			font-weight: bold;
			color: white;
			border-radius: 2px;
			background: rgba(39,174,96,0.8);;
		}
		.unread-action {
            width: 100%;
			height: 100%;
			display: flex;
			align-items: center;
			justify-content: center;
			font-family: pvn;
			font-weight: bold;
			color: white;
			border-radius: 2px;
			background: rgba(231,76,60,0.8);;
		}

        
        #table_index tbody td:last-child {
            padding: 0;
            min-width: 53px !important;
            line-height: 4px;
        }
    </style>
<script type="text/javascript">
    var aJaxURL_tabs            = "server-side/call/task/task_tabs.action.php";
    var aJaxURL_task            = "server-side/call/task/task.action.php";
    var aJaxURL                 = "server-side/call/task/task.action.php";
    var aJaxURL_answer          = "server-side/call/chat_answer.action.php";
    var aJaxURL_inc         = "server-side/call/incomming.action.php";
    var aJaxURL_getmail         = "includes/phpmailer/smtp.php";
    var tName                   = "table_";
    var change_colum_main       = "<'dataTable_buttons'T><'F'Cfipl>";
    var dialog                  = "add-edit-form";
    var colum_number            = 11;
    var main_act                = "get_list";
	var addEditFormWidth        = 630;
    var coment_id               = "";
    var coment_user_id          = "";
    var edit_coment_id          = "";
    var quantity                = "";


    function LoadKendoTable__Info_Projects(hidden){

$.ajax({
url: 'server-side/call/incomming.action.php',
data: {
    act: "get_status_items"
},
type: "GET",
success: function(data){

    var options = data.options;

    //KendoUI CLASS CONFIGS BEGIN
    var aJaxURL	        =   "server-side/call/waiters.action.php";
    var gridName        = 	'kendo_project_info_table';
    var actions         = 	'<button id="add_project_info" style="margin-right: 10px"> დამატება</button><select id="project_info_item_status">' + options.map(o => { return '<option value="'+o.id+'">' + o.name + '</option>'}) + '</select><button data-select="info" id="change_project_status">სტატუსის შეცვლა</button><button id="remove_project_info_item">წაშლა</button>';
    var editType        =   "popup"; // Two types "popup" and "inline"
    var itemPerPage     = 	10;
    var columnsCount    =	4;
    var columnsSQL      = 	[
                                "id:string",
                                "status:date",
                                "project:string",
                                "info:string"
                            ];
    var columnGeoNames  = 	[  
                                "ID", 
                                "სტატუსი",
                                "პროექტი",
                                "გაცემული ინფორმაცია"
                            ];

    var showOperatorsByColumns  =   [0,0,0,0]; //IF NEED USE 1 ELSE USE 0
    var selectors               =   [0,0,0,0]; //IF NEED NOT USE 0

    var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
    //KendoUI CLASS CONFIGS END

        
    const kendo = new kendoUI();
    kendo.loadKendoUI(aJaxURL,'get_list_info_project',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);

    }
})
}

    $(document).ready(function (){
    	
        loadtabs();
        GetButtons("add_button_task","delete_button_task")
        setTimeout(() => {
            SetEvents("add_button_task", "delete_button_task", "", tName+'index', dialog, aJaxURL_task);
        }, 500);
        $("#filter_button").button();
        $("#excel_export").button();
        GetDate("start_date");
        GetDate("end_date");
        var start_date = new Date();
        start_date.setHours(0,0,0,0)
        $("#start_date").datepicker().datepicker("setDate", start_date);
        $("#end_date").datepicker().datepicker("setDate", new Date());

    })
    function active(id){
        
        $(id).attr( "aria-selected", "true");
        
        $(id).css("cursor", "grabbing");
    }
    function deactive(id){
        $(id).attr( "aria-selected", "false");
        
        $(id).css("cursor", "pointer");
    }
    $(document).on("click", "#view", function() {
        param 	  = new Object();
    	param.act = "view";
    	param.id  = $("#hidden_id").val();
    	$.ajax({
    		url: aJaxURL_task,
    		data: param,
    		success: function(data) {
    			if(typeof(data.error) != "undefined"){
    				if(data.error != ""){
    					alert(data.error);
    				}else{
        				alert('ოპერაცია წარმატებით შესრულდა')
    					var obj = new Object;
    		            obj.start_date  = $('#start_date').val();
    		            obj.end_date    = $('#end_date').val();
    		            obj.parent=$("#activeparent").val();
    		            obj.child=$("#activechild").val();
    		            loadtabs("#t_"+obj.parent,"#t_"+obj.child);
    		            LoadTable('index',colum_number,main_act,change_colum_main,obj);
    				}
    			}
    		}
    	});
    });
    $(document).on("click", "#tabs1 li", function() {
        if(!$(this).hasClass("child")){ // parent
            deactive("li[id^='t_']");
            $('div[id^="tab_"]').css("display","inline-block");
            $('div[id^="tab_"]').css("position","relative");
            GetTabs("tab_"+$(this).attr("name"));
            active("#t_"+$(this).attr("name") );
            $('div[id^="tab_"]').not(".main_tab").not('.tab_'+$(this).attr("name")).css("display","none");
            active("#tab_"+$(this).attr("name")+" ul li:first-child");
            var obj = new Object;
            obj.parent=$(this).attr("name");
            obj.child=$("#tab_"+$(this).attr("name")+" ul li:first-child").attr("name");
            obj.start_date  = $('#start_date').val();
            obj.end_date    = $('#end_date').val();
            quantity_ajax();
            $("#activeparent").val(""+obj.parent);
            $("#activechild").val(""+obj.child);
            LoadTable('index',colum_number,main_act,change_colum_main,obj);
            SetEvents("add_button_task", "delete_button_task", "", 'table_index', 'add-edit-form', aJaxURL_task);
        } else { // child   
            deactive("li[id^='t_']");
            active("#"+$(this).attr("id") );
            active("#t_"+$(this).attr("id_select") );
            var obj = new Object;
            obj.parent  =$(this).attr("id_select");
            obj.child   =$(this).attr("name");
            obj.start_date  = $('#start_date').val();
            obj.end_date    = $('#end_date').val();
            quantity_ajax();
            $("#activeparent").val(""+obj.parent);
            $("#activechild").val(""+obj.child);
            LoadTable('index',colum_number,main_act,change_colum_main,obj);
            SetEvents("add_button_task", "delete_button_task", "", 'table_index', 'add-edit-form', aJaxURL_task);
        }
    });
   
    function LoadDialog(fName){
            if(fName == 'add-edit-form'){
                var buttons = {
                    "view": {
				            text: "გავეცანი",
				            id: "view"
				    },
                    "save": {
                        text: "შენახვა",
                        id: "save-dialog"
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $('#add-edit-form').dialog("close");
                        }
                    }
                };

                $("#side_menu").find(".task").trigger("click");
                ///// checkbox
                GetDialog(fName, "630", "auto", buttons, 'center center');
                $(".add-edit-form-class").css('left','86.5px');
                
                GetDateTimes("task_start_date");
                GetDateTimes("task_end_date");
                GetDateTimes("info_last_pay_date");
                GetDateTimes("monitoring");
                GetDateTimes("info_loan_date");
                GetDateTimes("history_start_date");
                GetDateTimes("history_end_date");
                GetDateTimes("ambition_registration_date");
                GetDateTimes("ambition_date");
                GetDateTimes1("date_time_input");
                $("#info_born_date, #info_ensuring_author_birthday, #info_entrusted_birthday").datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "1900:2100"
                });
                
                if($("#wiev_status").val()==1){
                	param 	  = new Object();
        			param.act = "view";
        			param.id  = $("#hidden_id").val();
        			$.ajax({
        				url: aJaxURL_task,
        				data: param,
        				success: function(data) {
        					var obj = new Object;
				            obj.start_date  = $('#start_date').val();
				            obj.end_date    = $('#end_date').val();
				            obj.parent=$("#activeparent").val();
				            obj.child=$("#activechild").val();
				            loadtabs("#t_"+obj.parent,"#t_"+obj.child);
				            LoadTable('index',colum_number,main_act,change_colum_main,obj);
        				}
        			});
        		}
                
                $(".jquery_button").button();

                $("[data-select='chosen']").chosen({ search_contains: true });
                $("[data-button='jquery-ui-button']").button();
                $("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0").chosen();
                
                $(".fieldset_row select").chosen();
                $(".fieldset_row .chosen-container").css('width','182px');
                $("#dep_0_chosen,#dep_project_0_chosen,#dep_sub_project_0_chosen,#dep_sub_type_0_chosen").css('width','110px')
                var hidden = '&hidden='+$('#hidden_incomming_call_id').val();
                $( "#info input").prop( "disabled", true );
                $( "#info textarea").prop( "disabled", true );
                $( "#info select").prop( "disabled", true ).trigger("chosen:updated");
                LoadKendoTable__Info_Projects(hidden);

                setTimeout(function(){
                    var grid = $("#kendo_project_info_table").data("kendoGrid");
                    //grid.hideColumn(0); 

                    $("#kendo_project_info_table").kendoTooltip({  
                        show: function(e){  
                            if(this.content.text().length > 30){  
                            this.content.parent().css("visibility", "visible");  
                            }  
                        },  
                        hide:function(e){  
                            this.content.parent().css("visibility", "hidden");  
                        },  
                        filter: "td", 
                        position: "right",  
                        content: function(e){  
                            // var dataItem = $("#kendo_project_info_table").data("kendoGrid").dataItem(e.target.closest("tr"));  
                            // console.log("bottom ", dataItem.info)
                            var content = e.target[0].innerText;  
                            return content;  
                        }  
                        }).data("kendoTooltip");  
                }, 3000)

                
            } else if(fName == 'add-edit-form-answer'){
                var buttons = {
                    "save": {
                        text: "შენახვა",
                        id: "save-dialog-answer",

                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog-answer", 
                        click: function () {
                                        $('#'+fName).dialog("close");
                        }  
                    }
                };
                GetDialog(fName, 358, "auto", buttons);
            }
            $('.ui-widget-overlay').css('z-index',99);
        }

        $(document).on("click", ".client_history", function () {
            $("#client_history_user_id").val($("#phone").val());
            GetDate("date_from");
            GetDate("date_to");
            $("#search_client_history").button();
            
            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("table_history", aJaxURL_inc, "get_list_history", 6, "&start_check="+$('#date_from').val()+"&end_check="+$('#date_to').val()+"&phone="+$("#phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
           
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", "#search_client_history", function () {
        	var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("table_history", aJaxURL_inc, "get_list_history", 6, "&start_check="+$('#date_to').val()+"&end_check="+$('#date_to').val()+"&phone="+$("#phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
           
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", ".client_conversation", function (event) {
            param 	  = new Object();
            param.act = "get_client_conversation_count";

            $.ajax({
                url: aJaxURL_inc,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                        }else{
                            $("#client_conversation_count_hint").html(0);
                            $("#client_conversation_count_hint").css('display','none');
                            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
                            GetDataTable("table_question", aJaxURL_inc, "get_list_quest", 5, "source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), 0, dLength, 0, "desc",'',"<'F'lip>");
                            $("#table_question_length").css('top','0px');
                        }
                    }
                }
            });

        });

        $(document).on("click", ".sms", function () {
          var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
          GetDataTable("table_sms", aJaxURL_inc, "get_list_sms", 5, "&incomming_id=" + $("#hidden_incomming_call_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
      	  $("#table_sms_length").css("top", "0px");
      	  $("#table_sms_length select").css("height", "16px");
      	  $("#table_sms_length select").css("width", "56px");
      	  setTimeout(function () {$(".ColVis, .dataTable_buttons").css("display", "none");}, 90);
      	});
      	
        function LoadTable(tbl,col_num,act,change_colum,data){
        	start_date = $('#start_date').val();
            end_date   = $('#end_date').val();
            parent1     = $("#activeparent").val();
            child      = $("#activechild").val();
            
            GetDataTable(tName+tbl,aJaxURL_tabs,act,col_num,"start_date="+start_date+"&end_date="+end_date+"&parent="+parent1+"&child="+child, 0, "",1,"desc",'',change_colum);
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 120);
        }

        function quantity_ajax(){
            var param = new Object;
            param.act         = "get_quantity";
            param.start_date  = $('#start_date').val();
            param.end_date    = $('#end_date').val();
            $.ajax({
                url:aJaxURL_tabs,
                data: param,
                success: function(data){
                    $( "li[id^='t_']" ).each(function(index) {
                        if(""+data.quantity[$(this).attr("name")]!="null")
                            $("#input_"+$(this).attr("name")).val(""+data.quantity[$(this).attr("name")]);
                    });
                }
            });
        }
          $(document).on("click", "#save-dialog", function () {
        	values         = $("#my_site").chosen().val();
        	task_recipient = $("#add-edit-form #task_recipient").val();
            param 				      = new Object();
            param.act			      = "save_incomming";
            param.task_id			  = $("#task_id").val();
            param.id			      = $("#hidden_id").val();
            param.incomming_call_id   = $("#hidden_incomming_call_id").val();
            param.task_phone          = $("#task_phone").val();

            param.task_status         = $("#task_status").val();
            param.task_status_2       = $("#task_status_2").val();
            param.task_branch         = $("#task_branch").val();
            param.task_date           = $("#task_date").val();
            param.task_start_date     = $("#task_start_date").val();
            param.task_end_date       = $("#task_end_date").val();
            param.task_description    = $("#task_description").val();
            param.task_position       = $("#task_position").val();
            param.task_recipient      = $("#add-edit-form #task_recipient").val();
            param.task_note           = $("#task_note").val();
           
            param.incomming_cat_1     = $("#incomming_cat_1").val();
            param.incomming_cat_1_1   = $("#incomming_cat_1_1").val();
            param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();
            param.site                = values;
            
            
            if($('#task_start_date').val() == ''){
            	alert('შეავსე პერიოდის დასაწყისი!!!');
            }else if ($('#task_end_date').val() == ''){
            	alert('შეავსე პერიოდის დასასრული!!!');
            }else if($('#task_status').val() == 0){
                alert('აირჩიე სტატუსი!!!');
            }else if ($("#task_status").val() == "77" && $("#task_status_2").val() == "0"){
				alert("შეავსეთ ქვე-სტატუსი !");
			}else if ($('#task_branch').val() == 0){
            	alert('აირჩიე განყოფილება!!!');
            }else if ($('#task_recipient').val() == 0){
            	alert('აირჩიე დავალების მიმღები!!!');
            }else if (param.task_description == ''){
            	alert('შეავსეთ კომენტარი!!!');
            }else {
            	var link = GetAjaxData(param);
                $.ajax({
                    url: aJaxURL_task,
                    data: link, 
                    success: function(data) {
                        if(typeof(data.error) != "undefined"){
                            if(data.error != ""){
                                alert(data.error);
                            }else{
                                LoadTable('index', colum_number, main_act, change_colum_main);
                                
                                var obj = new Object;
                                obj.start_date  = $('#start_date').val();
                                obj.end_date    = $('#end_date').val();
                                obj.parent      = $("#activeparent").val();
                                obj.child       = $("#activechild").val();
                                loadtabs("#t_"+obj.parent,"#t_"+obj.child);
                                LoadTable('index',colum_number,main_act,change_colum_main,obj);
                                CloseDialog("add-edit-form");
                                console.log(data);
                                param1                   = new Object();
                                param1.task_id           = data.task_id;
                                param1.time              = data.time;
                                param1.task_type         = data.task_type;
                                param1.status_name       = data.status_name;
                                param1.site              = values;
                                param1.send_type         = 1;
                                
                                
                                var link1                = GetAjaxData(param1);
                                
                                $.ajax({
                                    url: aJaxURL_getmail,
                                    data: link1,
                            		success: function (data) {
                                    }
                                });
                                
                            }
                        }
                    }
                });
            }
        });
        
        $(document).on("click","#delete_button_task",function(){
            setTimeout(() => {
                location.reload();
            }, 200);
        });
        
        $(document).on("click","#filter_button",function(){
            var obj = new Object;
            obj.start_date  = $('#start_date').val();
            obj.end_date    = $('#end_date').val();
            obj.parent=$("#activeparent").val();
            obj.child=$("#activechild").val();
            loadtabs("#t_"+obj.parent,"#t_"+obj.child);
            LoadTable('index',colum_number,main_act,change_colum_main,obj);
            // setTimeout(() => {
            //     SetEvents("add_button_task", "delete_button_task", "", 'table_index', 'add-edit-form', aJaxURL_task);
            // }, 200);
        });
        
        function loadtabs(activep="", activech=""){
            
            obj = new Object;
            obj.act="get_tabs";
            $.ajax({
                url: aJaxURL_tabs,
                data:obj,
                success: function (data)
                {
                    $("#tabs1").html(data.page);
                    GetTabs("tab_0");
                    if(activep==""){
                        $("[id^=t_]").first().click();
                    } else{
                        $(activep).click();
                        if(activech!=""){
                            $(activech).click();
                        }
                    }
                    quantity_ajax();
                }
            })
        }
        function show_right_side(id) {
      	  $("#right_side ff").hide();
      	  $("#" + id).show();
      	  //$(".add-edit-form-class").css("width", "1260");
      	  //$('#add-edit-form').dialog({ position: 'left top' });
      	  hide_right_side();

      	  var str = $("." + id).children("img").attr("src");
      	  str = str.substring(0, str.length - 4);
      	  $("#side_menu span").children("img").css("border-bottom", "2px solid transparent");
      	  $("." + id).children("img").css("filter", "brightness(0.1)");
      	  $("." + id).children("img").css("border-bottom", "2px solid #333");
      	}

      	function hide_right_side() {
      		
      	  $(".info").children("img").css("filter", "brightness(1.1)");
      	  $(".client_history").children("img").css("filter", "brightness(1.1)");
      	  $(".task").children("img").css("filter", "brightness(1.1)");
      	  $(".record").children("img").css("filter", "brightness(1.1)");
      	  $(".incomming_file").children("img").css("filter", "brightness(1.1)");
      	  $(".sms").children("img").css("filter", "brightness(1.1)");
      	  $(".incomming_mail").children("img").css("filter", "brightness(1.1)");
      	  $(".client_conversation").children("img").css("filter", "brightness(1.1)");
      	  $(".crm").children("img").css("filter", "brightness(1.1)");
      	  $(".chat_question").children("img").css("filter", "brightness(1.1)");
      	  $(".user_logs").children("img").css("filter", "brightness(1.1)");
      	  $(".call_resume").children("img").css("filter", "brightness(1.1)");
      	  $(".newsnews").children("img").css("filter", "brightness(1.1)");
      	  
      	  $("#record fieldset").show();
      	}
        $(document).on("click", ".itm-unit:not(.itm-unit-active)", function() {
            //define variables
            let selector = $(this).data("selector");

            //deactivate all menu items and activate relevant one
            $(".itm-unit").removeClass("itm-unit-active");
            $(this).addClass("itm-unit-active");

            //deactivate info sections and activate menu relevant one
            $(".inc-info-section").removeClass("is-active");
            $(`.inc-info-section[data-selector='${selector}']`).addClass("is-active");

            //stylize chosen objects
            setTimeout(function() {
                $(".iiss-sc-unit").find(".chosen-container").css({width:"100%"});
            }, 10);
        });
        $(document).on("change", "#incomming_cat_1", function () {
            param = new Object();
            param.act = "cat_2";
            param.cat_id = $('#incomming_cat_1').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#incomming_cat_1_1").html(data.page);
                    $('#incomming_cat_1_1').trigger("chosen:updated");
                    if ($('#incomming_cat_1_1 option:selected').val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $('#incomming_cat_1_1').val();
                        $.ajax({
                            url: aJaxURL_task,
                            data: param,
                            success: function (data) {
                                $("#incomming_cat_1_1_1").html(data.page);
                                $('#incomming_cat_1_1_1').trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });
        $(document).on("change", "#incomming_cat_1_1", function () {
            param = new Object();
            param.act = "cat_3";
            param.cat_id = $('#incomming_cat_1_1').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#incomming_cat_1_1_1").html(data.page);
                    $('#incomming_cat_1_1_1').trigger("chosen:updated");
                }
            });
        });
         $(document).on("change", "#task_status, #task_status_2", function () {

            //define variables
            let actionObj = $(this).attr("id");
            let filterCat = $("#task_status").val();

            //reset sub category values
            if(actionObj === "task_status") {
                $.post(aJaxURL_task, {
                    act: "task_status_cat",
                    cat_id: filterCat
                }, function(result) {
                    $("#task_status_2").html(result.page);
                    $("#task_status_2").trigger("chosen:updated");
                });
            }

            //define variables
            let subCat = $("#task_status_2").val();
            let subFiltCat = `${filterCat}:${subCat}`;

            //remove class that gives elements visibility to hide them all
            $(".cat-listener").removeClass("incomming-cat-visible");

            //give visibility class to objects that belongs to chosen categories
            $(`.cat-listener[data-category="cat-level-${filterCat}"]`).addClass("incomming-cat-visible");
            $(`.cat-listener[data-subCategory="cat-level-${subFiltCat}"]`).addClass("incomming-cat-visible");

            //stylize chosen containers
            setTimeout(function() {
                $(".cat-listener").find(".chosen-container").css({width:"100%"});
            }, 10);
        });
        $(document).on("change", "#task_branch, #task_position", function () {
            var task_type       = $('#task_type').val();
            param               = new Object();
            param.act           = "task_branch_changed";
            param.cat_id        = $('#task_branch').val();
            param.position_id   = $('#task_position').val();
                $.ajax({
                    url: aJaxURL_task,
                    data: param,
                    success: function (data) {
                        $("#task_recipient").html(data.page);
                        $('#task_recipient').trigger("chosen:updated");
                    }
                });
        });
        
        $(document).on("change", "#task_recipient", function () {
            var task_type       = $('#task_type').val();
            param               = new Object();
            param.act           = "task_recipient_changed";
            param.recipient_id  = $(this).val();
                $.ajax({
                    url: aJaxURL_task,
                    data: param,
                    success: function (data) {
                        $("#task_branch").html(data.page);
                        $('#task_branch').trigger("chosen:updated");
                    }
                });
        });
        
        //log-tab functions log functions
        $(document).on("click", ".log-tab", function(){
            GetDataTable("user_log_table",aJaxURL,"get_user_log",7,"incomming_call_id="+$("#incomming_id").val(),0,"",1,"desc",'',"<'dataTable_buttons'T><'F'Cfipl>");
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
                }, 160);
            $("#log-tab").css("width", "100%");
        });
        //chat question functions
        $(document).on("click", "#add_comment", function () { // save answer 
            param                   = new Object();
            param.act               = 'save_comment';
            param.comment           = $("#add_ask").val();
            param.incomming_call_id = $("#incomming_id").val();
            param.task_id           = $("#task_id").val();
            if(param.comment != ""){
                $.ajax({
                    url: aJaxURL_task,
                    data: param,
                    success: function(data) {
                        param2                      = new Object();
                        param2.act                  = 'get_add_question';
                        param2.incomming_call_id    = $("#incomming_id").val();
                        param2.task_id    = $("#task_id").val();
                        $.ajax({
                            url: aJaxURL_task,
                            data: param2,
                            success: function(data) {
                                $("#chat_question").html(data.page);
                                $("#add_comment").button();
                            }
                        });
                    }
                });
            }else{
                console.log("empty")
            }
        });
        // $(document).on("click", "#get_answer", function () { // get answer page
        //     LoadDialog("add-edit-form-answer");
        //     param           = new Object();
        //     param.act       = 'get_add_page';
        //     comment_id      = $(this).attr('class');
        //     $.ajax({
        //         url: aJaxURL_answer,
        //         data: param,
        //         success: function(data) {
        //             $("#add-edit-form-answer").html(data.page);
        //             $("#add_comment").button();
        //         }
        //     });
        // });
        // $(document).on("click", "#save-dialog-answer", function () { 
        //     // save answer 
        //     if($("#comment_info_sorce_id").val()==""){
        //         param                   = new Object();
        //         param.act               = 'save_answer';
        //         param.comment           = $("#in_answer").val();
        //         param.comment_id        = $('#hidden_comment_id_'+comment_id+'').val();
        //         param.incomming_call_id = $("#incomming_id").val();
        //         param.task_id           = $("#task_id").val();
        //         if(param.comment!=""){
        //             $.ajax({
        //                 url: aJaxURL_answer,
        //                 data: param,
        //                 success: function(data) {
        //                     param2                   = new Object();
        //                     param2.act               = 'get_add_question';
        //                     param2.incomming_call_id = $("#incomming_id").val();
        //                     param2.task_id           = $("#task_id").val();
        //                     $.ajax({
        //                         url: aJaxURL_task,
        //                         data: param2,
        //                         success: function(data) {
        //                             $("#add-edit-form-answer").dialog("close");
        //                             $("#chat_question").html(data.page);
        //                             $("#add_comment").button();
        //                         }
        //                     });
        //                 }
        //             });
        //         } else {
        //             alert("შეავსეთ ველი")
        //         }
        //     } else {
        //         param2          = new Object();
        //         param2.act      = 'update_comment';
        //         param2.id       = edit_comment_id;
        //         param2.comment  = $("#in_answer").val();
        //         $.ajax({
        //             url: aJaxURL_task,
        //             data: param2,
        //             success: function(data) {
        //                 param2 = new Object();
        //                 param2.act='get_add_question';
        //                 param2.incomming_call_id = $("#incomming_id").val();
        //                 param2.task_id           = $("#task_id").val();
        //                 $.ajax({
        //                     url: aJaxURL_task,
        //                     data: param2,
        //                     success: function(data) {
        //                         $("#add-edit-form-answer").dialog("close");
        //                         $("#chat_question").html(data.page);
        //                         $("#add_comment").button();
        //                     }
        //                 });
        //             }
        //         });
        //     }
        // });
        // $(document).on("click", "#delete_comment", function () { // save answer 
        //     param = new Object();
        //     param.act='delete_comment';
        //     param.comment_id=$(this).attr('my_id');
        //     $.ajax({
        //             url: aJaxURL_task,
        //             data: param,
        //             success: function(data) {
        //                 param2 = new Object();
        //                 param2.act='get_add_question';
        //                 param2.incomming_call_id = $("#incomming_id").val();
        //                 param2.task_id           = $("#task_id").val();
        //                 $.ajax({
        //                     url: aJaxURL_task,
        //                     data: param2,
        //                     success: function(data) {
        //                         $("#chat_question").html(data.page);
        //                         $("#add_comment").button();
        //                     }
        //                 });
        //             }
        //     });
        // });
        // $(document).on("click", "#edit_comment", function () { // save answer 
        //     param = new Object();
        //     param.act='get_edit_page';
        //     edit_comment_id=$(this).attr('my_id');
        //     param.id= $(this).attr('my_id');
        //     $.ajax({
        //         url: aJaxURL_answer,
        //         data: param,
        //         success: function(data) {
        //             $("#add-edit-form-answer").html(data.page);
        //             LoadDialog("add-edit-form-answer");
        //             }
        //         });
        //     });
        //chat question functions end
        $(document).on("click", "#upload_file", function () {
            $('#file_name').click();
        });

        $(document).on('click','.dialog_input_tabs li',function(){
            ///SWITCHING TABS HERE///
            var field_id = $(this).parent().attr('id');
            var tab_id   = $(this).attr('data-id');
            $('#'+field_id+' li').removeAttr('aria-selected');
            $(this).attr('aria-selected','true');
            /////////////////////////
            $("."+field_id).css('display','none');

            $("."+field_id+"[data-tab-id='"+tab_id+"']").css('display','block');

        });
        $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
	    
        $(document).on("change", "#file_name", function () {
            var file_url  = $(this).val();
            var file_name = this.files[0].name;
            var file_size = this.files[0].size;
            var file_type = file_url.split('.').pop().toLowerCase();
            var path	  = "../../media/uploads/file/";

            if($("#incomming_id").val()!='' && $("#incomming_id").val()!=0){
                table_id   = $("#incomming_id").val();
                table_name = 'incomming_call';
            }else{
            	table_id = $("#task_id").val();
            	table_name = 'task';
            }
            
            if($.inArray(file_type, ['pdf', 'pptx', 'png','xls','xlsx','jpg','docx','doc','csv']) == -1){
                alert("დაშვებულია მხოლოდ 'pdf', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv' გაფართოება");
            }else if(file_size > '15728639'){
                alert("ფაილის ზომა 15MB-ზე მეტია");
            }else{
                $.ajaxFileUpload({
                    url: "server-side/upload/file.action.php",
                    secureuri: false,
                    fileElementId: "file_name",
                    dataType: 'json',
                    data: {
                        act: "file_upload",
                        button_id: "file_name",
                        table_name: table_name,
                        file_name: Math.ceil(Math.random()*99999999999),
                        file_name_original: file_name,
                        file_type: file_type,
                        file_size: file_size,
                        path: path,
                        table_id: table_id,
					},
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                var tbody = '';
                                for(i = 0;i <= data.page.length;i++){
                                    tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                                    tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                                    tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                                    tbody += "<div id=\"for_div\" onclick=\"delete_file('" + data.page[i].id + "', 'outgoing')\">-</div>";
                                    $("#add-edit-form #paste_files").html(tbody);
                                }							
                            }						
                        }					
                    }
                });
            }
        });
        function delete_file(id) {
        	if($("#incomming_id").val()!='' && $("#incomming_id").val()!=0){
                table_name = 'incomming_call';
            }else{
            	table_name = 'task';
            }
            $.ajax({
                url: "server-side/upload/file.action.php",
                data: "act=delete_file1&file_id=" + id + "&table_name="+table_name,
                success: function (data) {

                    var tbody = '';
                    if (data.page.length == 0) {
                        $("#paste_files").html('');
                    }
                    ;
                    for (i = 0; i <= data.page.length; i++) {
                        tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                        tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                        tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                        tbody += "<div id=\"for_div\" onclick=\"delete_file('" + data.page[i].id + "')\">-</div>";
                        $("#paste_files").html(tbody);
                    }
                }
            });
        }
        function listen(file) {
            var url = 'http://172.16.0.80:8000/' + file;
            $("#record audio source").attr('src', url);
            $("#record audio").load();
        }
        //ინფოსა და პრეტენზიის ტაბების ზოგიერთი ველის დაკავშირება
        $(document).on("change paste keyup","#info_loan_number, #info_pid, #info_mobile, #info_email, #info_address", function() {
            var loan_number = $("#info_loan_number").val();
            var pid         = $("#info_pid").val();
            var mobile      = $("#info_mobile").val();
            var email       = $("#info_email").val();
            var address     = $("#info_address").val();
            var name        = $("#info_name").val();
            $("#ambition_loan_number").val(loan_number);
            $("#ambition_pid").val(pid);
            $("#ambition_mobile").val(mobile);
            $("#ambition_client_email").val(email);
            $("#ambition_client_address").val(address);
            $("#ambition_name").val(name);
        });

        $(document).on("click", "#excel_export", function () {
        	var parent = $("#activeparent").val();
            var child  = $("#activechild").val();
			var start  = $("#start_date").val();
			var end    = $("#end_date").val(); 
	        url="includes/excel/writexlsx.php?act=task&start="+start+"&end="+end+"&parent="+parent+"&child="+child;
	        window.open(url);
	    });
	    
        </script>


    </head>
    <body>
    <div id="tabs" style="width: 80%; overflow-x: auto;">
    <div class="l_responsible_fix">
        	<table id="table_right_menu" style="top: 183px; left: -1px;">
            	<tr>
            		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            		</td>
            		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
            			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
            		</td>
            		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
            			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
            		</td>
            	</tr>
            </table>
        	<div id="tabs1" class="l_inline-tabs" style="display: inline-block; width: 98%"></div>
            <div id="section" class="section"></div>
            <div id="filter_div">
                <input type="text" id="start_date" value="">
                <input type="text" id="end_date" value="">
                <button id="filter_button">ფილტრი</button>
            </div>
            <div id="button_area">
            <button style="margin-bottom: 5px;" id="add_button_task">ახალი დავალება</button>
            <button id="delete_button_task">წაშლა</button>
    </div>

            <table class="display" id="table_index" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width:6%;">N:</th>
                        <th style="width:11%;">ფორმირების თარიღი</th>
                        <th style="width:11%;">დასაწყისი</th>
                        <th style="width:11%;">დასასრული</th>
                        <th style="width:10%;">შემქმნელი</th>
                        <th style="width:10%;">დავალების მიმღები</th>
                        <th style="width:10%;">სტატუსი</th>
                        <th style="width:10%;">სტატუსი 2</th>
                        <th style="width:11%;">განყოფილება</th>
                        <th style="width:10%;">კომენტარი</th>
                        <th style="width:35px;">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <div class="callapp_checkbox" >
                                <input type="checkbox" id="check-all"  name="check-all" />
                                <label for="check-all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
    </div>
<!-- statment styles -->
<input id="check_state" type="hidden" value="1" />
<input id="whoami" type="hidden" value="1" />
<input id="activeparent" type="hidden" value=""/>
<input id="activechild" type="hidden" value=""/>
<!-- jQuery Dialog -->
<div  id="add-edit-form" class="form-dialog" title="დავალება">
</div>
<!-- <div  id="add-edit-form-answer" class="form-dialog" title="პასუხი"> -->
</div>
<input id="confirm_update" type="hidden" value="">
<input id="confirm_click" type="hidden" value="0">
    </body>
</html>