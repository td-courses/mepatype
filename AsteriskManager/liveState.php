<?php
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ERROR);
$script_start = time();
require_once '../includes/classes/class.Mysqli.php';
global $db;
$db = new dbClass();
//error_reporting(E_ALL);
$user           = $_SESSION['USERID'];
$group          = $_SESSION['GROUPID'];
$checkState     = $_REQUEST['checkState'];



$db->setQuery(" SELECT     user_info.`name`,
                           user_info.user_id
                FROM      `users`
                JOIN       user_info ON user_info.user_id = users.id
                LEFT JOIN  asterisk_extension ON users.extension_id = asterisk_extension.id
                WHERE      logged = 1 AND users.actived = 1");

$res = $db->getResultArray();

$new_data = array();
$counter     = 0;

foreach ($res[result] AS $aRow_new){
    $new_data[users][$counter] = array('userid'   => $aRow_new[user_id],
                                       'username' => $aRow_new[name]);
        
    $db->setQuery(" SELECT     user_info.`name`,
                    		   user_info.user_id,
                    		  `queues`.member AS `last_extension`,
                    		  `queues`.queue,
                    	       CASE
                                    WHEN `channels`.`context` = 'macro-dial-one' OR `channels`.`context` = 'macro-dial' THEN (`channels`.`callerid`)

                                    WHEN `channels`.`context` = 'macro-dialout-trunk' THEN 
                                    (SELECT MAX(`chan`.`callerid`)
                                     FROM   `asteriskcdrdb`.`channels` AS chan
                                     WHERE   chan.bridgedto = `channels`.bridgedto
                                     AND     chan.application = 'AppDial'
                                     LIMIT 1)
									ELSE (SELECT callerid FROM asteriskcdrdb.channels WHERE bridgedto = `ch2`.`bridgedto` AND application='Dial' LIMIT 1)
    					       END  AS `phone`,
                               CASE
                                    WHEN `channels`.`context` = 'macro-dial-one' OR `channels`.`context` = 'macro-dial' THEN 'in'
                                    WHEN `channels`.`context` = 'macro-dialout-trunk' THEN 'out'
                                    WHEN (SELECT context FROM asteriskcdrdb.channels WHERE bridgedto = `ch2`.`bridgedto` AND application='Dial' LIMIT 1) = 'autodialer' THEN 'in-autodialer'
                                END  AS `type`,
                                CASE
                                    WHEN `channels`.`context` = 'macro-dial-one' OR `channels`.`context` = 'macro-dial' THEN 'arr_busy_in'
                                    WHEN `channels`.`context` = 'macro-dialout-trunk' THEN 'arr_busy_out'
                                    WHEN (SELECT context FROM asteriskcdrdb.channels WHERE bridgedto = `ch2`.`bridgedto` AND application='Dial' LIMIT 1) = 'autodialer' THEN 'arr_busy_in'
                                END  AS `arrow_call`,
                                TIME_FORMAT(SEC_TO_TIME(IFNULL(`channels`.`duration`,ch2.duration)), '%i:%s') AS `time`,
                               IF(ISNULL(`channels`.`context`),
                               CASE
                                    WHEN users.work_activities_id > 1 THEN CONCAT('paused_',users.work_activities_id)
                                   
                                    ELSE `queues`.`status`
                               END,
                               `queues`.`status`) AS status,
                    		   '' As pin,
                               (SELECT department.name
                                FROM asterisk_queue
                                JOIN department_queues ON department_queues.queue_id = asterisk_queue.id
                                JOIN department ON department.id = department_queues.dep_id
                                WHERE department.actived = 1 AND asterisk_queue.number = `queues`.`queue`) AS `dep`
                    FROM      `users`
                    JOIN       user_info ON user_info.user_id = users.id
                    JOIN       asterisk_extension ON users.extension_id = asterisk_extension.id
                    JOIN       asteriskcdrdb.queues ON asterisk_extension.number = queues.member
                    LEFT JOIN `asteriskcdrdb`.`channels` ON REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(`channels`.`channel`, '@', 1),'-',1),'SIP','Local') = `queues`.`extention`
                          AND `channels`.`context` IN('macro-dial-one','macro-dialout-trunk','autodialer','macro-dial') AND `channels`.`state` IN ('Up', 'Ring')
													
										LEFT JOIN `asteriskcdrdb`.`channels` AS `ch2` ON REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(`ch2`.`channel`, '@', 1),'-',1),'SIP','Local') = `queues`.`extention`
                           AND `ch2`.`state` IN ('Up', 'Ring')
                    WHERE      logged = 1 AND users.actived = 1 AND users.id = $aRow_new[user_id] AND `queues`.status != 'unavailable' AND queues.queue IN (20)
                    GROUP BY `queues`.member,`queues`.queue
                    ORDER BY  `queues`.queue DESC,  `queues`.member ASC");
    
    $res_asterisk = $db->getResultArray();
    $counter_ast  = 0;
    $phone_array  = array();
    
    foreach ($res_asterisk[result] AS $aRow_ast){

        // $db->setQuery(" SELECT  `name` 
        //                 FROM    `saved_users_by_phone` 
        //                 WHERE   `phone` = '$aRow_ast[phone]'");
        // $saved_name_arr = $db->getResultArray();
        // $saved_name = $saved_name_arr['result'][0]['name'];

        // if($saved_name != ''){
        //     $saved_name = $aRow_ast['phone'].'/'.$saved_name;
        // }else{
            $saved_name = $aRow_ast['phone'];
        // }

        $db->setQuery(" SELECT  1 AS `cc`
                        FROM    asterisk_call_log
                        WHERE   call_type_id = 2 AND user_id = '$aRow_new[user_id]' AND destination = '$aRow_ast[phone]' AND UNIX_TIMESTAMP(NOW()) - call_datetime < 1800
                        ORDER BY id DESC
                        LIMIT   1");
        $exist_call = $db->getResultArray();
        $exist_call = $exist_call['result'][0]['cc'];

        $phone_array[$counter_ast] =  array( 'queue'  => $aRow_ast['queue'],
                                             'ext'    => $aRow_ast['last_extension'],
                                             'number' => $aRow_ast['phone'],
                                             'time'   => $aRow_ast['time'],
                                             'arrow_call'   => $aRow_ast['arrow_call'],
                                             'pin'    => $aRow_ast['pin'],
                                             'type'   => $aRow_ast['type'],
                                             'dep'    => $aRow_ast['dep'],
                                             'exist_call'    => $exist_call,
                                             'status' => $aRow_ast['status'],
                                             'info'   => $saved_name);
        $counter_ast ++;
    }
     
    $new_data[users][$counter]['phone'] = $phone_array;
    
    $db->setQuery(" SELECT    `callerid` AS `number`, `exten`,
                    		  TIME_FORMAT(SEC_TO_TIME(`duration`), '%i:%s') AS `time` 
                    FROM  	 `asteriskcdrdb`.`channels` 
                    WHERE  	  `context` = 'ext-queues' AND `application` = 'Queue' AND callerid NOT IN(SELECT callerid FROM asteriskcdrdb.channels AS `chan` WHERE (chan.context = 'macro-dial-one' OR chan.context = 'macro-dial') AND chan.state = 'Up')
                    ORDER BY  `time` DESC");
    
    $phone_queue = $db->getResultArray();
    $phone_queue_array = array();
    $q = 1;
    foreach ($phone_queue[result] AS $phone_queue_live){

        $db->setQuery(" SELECT  `name` 
                        FROM    `saved_users_by_phone` 
                        WHERE   `phone` = '$phone_queue_live[number]'");
        $saved_name_arr = $db->getResultArray();
        $saved_name = $saved_name_arr['result'][0]['name'];
        
        $phone_queue_array[] = array('id'       => $phone_queue_live['id'],
                                     'queue'    => $phone_queue_live['exten'],
                                     'number'   => $phone_queue_live['number'],
                                     'position' => $q,
                                     'time'     => $phone_queue_live['time'],
                                     'info'     => $saved_name);
        $q++;
    }
    
    $db->setQuery("SELECT   `chat`.id,
                            'georgia.png' AS `country`,
                            `chat`.`browser`,
        				    `chat`.`name` AS `name`,
        				     TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat.join_date,'%H:%i:%s')) AS `time`
                   FROM     `chat`
                   WHERE    `chat`.`status` = 1
                   GROUP BY `chat`.`id`");
    
    $chat_queue = $db->getResultArray();
    $chat_queue_array = array();
    $i = 1;
    foreach ($chat_queue[result] AS $chat_queue_live){
        $chat_queue_array[] = array('id'       => $chat_queue_live[id],
                                      'number'   => $chat_queue_live[name],
                                      'position' => $i,
                                      'country'  => $chat_queue_live[country],
                                      'browser'  => $chat_queue_live[browser],
                                      'time'     => $chat_queue_live[time]);
        $i ++;
    }
    
    $db->setQuery("SELECT  c.id,
                           sender_name AS `name`,
                           TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.send_datetime,'%H:%i:%s')) AS `time` 
                   FROM   `mail_live` as c
                   WHERE   c.`mail_status_id` = 1 AND c.mail_type_id = 2");
    
    $mail_queue = $db->getResultArray();
    $mail_queue_array = array();
    $j = 1;
    foreach ($mail_queue[result] AS $mail_queue_live){
        $mail_queue_array[] = array('id'       => $mail_queue_live[id],
                                      'number'   => $mail_queue_live[name],
                                      'position' => $j,
                                      'country'  => '',
                                      'browser'  => '',
                                      'time'     => $mail_queue_live[time]);
        $j ++;
    }
    
    $db->setQuery("SELECT  c.id, 
                           sender_avatar,
                           sender_name AS `name`,
                           c.`status`,
                           TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`
                   FROM   `fb_chat` as c
                   WHERE   c.`status` = 1");
     
    $fb_queue = $db->getResultArray();
    $fb_queue_array = array();
    $k = 1;
    
    foreach ($fb_queue[result] AS $fb_queue_live){
        $fb_queue_array[] = array('id'       => $fb_queue_live[id],
                                    'number'   => $fb_queue_live[name],
                                    'picture'  => $fb_queue_live[sender_avatar],
                                    'position' => $k,
                                    'country'  => '',
                                    'browser'  => '',
                                    'time'     => $fb_queue_live[time]);
        $k ++;
    }

    // $db->setQuery("SELECT   c.id, 
    //                         fb_user_name AS `name`,
    //                         c.`status`,
    //                         TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.time,'%H:%i:%s')) AS `time`
    //                         FROM   `fb_comments` as c
    //                         WHERE   c.`status` = 1");

    // $fb_comments_queue = $db->getResultArray();
    $fb_comments_queue_array = array();
    $k = 1;

    // foreach ($fb_comments_queue[result] AS $fb_comments_queue_live){
    // $fb_comments_queue_array[] = array('id'       => $fb_comments_queue_live[id],
    //             'number'   => $fb_comments_queue_live[name],
    //             'picture'  => '',
    //             'position' => $k,
    //             'country'  => '',
    //             'browser'  => '',
    //             'time'     => $fb_comments_queue_live[time]);
    // $k ++;
    // }
    
    // $db->setQuery("SELECT  c.id,
    //                        sender_avatar,
    //                        sender_name AS `name`,
    //                        c.`status`,
    //                        TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`
    //                FROM   `viber_chat` as c
    //                WHERE   c.`status` = 1");
    
    // $viber_queue = $db->getResultArray();
    $viber_queue_array = array();
    $v = 1;
    
    // foreach ($viber_queue[result] AS $viber_queue_live){
    //     $viber_queue_array[] = array('id'       => $viber_queue_live[id],
    //                                  'number'   => $viber_queue_live[name],
    //                                  'picture'  => $viber_queue_live[sender_avatar],
    //                                  'position' => $v,
    //                                  'country'  => '',
    //                                  'browser'  => '',
    //                                  'time'     => $viber_queue_live[time]);
    //     $v ++;
    // }

    // $db->setQuery("SELECT  c.id,
    //                        sender_avatar,
    //                        sender_name AS `name`,
    //                        c.`status`,
    //                        TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`
    //                FROM   `whatsapp_chat` as c
    //                WHERE   c.`status` = 1");
    
    // $whatsapp_queue = $db->getResultArray();
    $whatsapp_queue_array = array();
    $v = 1;
    
    // foreach ($whatsapp_queue[result] AS $whatsapp_queue_live){
    //     $whatsapp_queue_array[] = array('id'       => $whatsapp_queue_live[id],
    //                                  'number'   => $whatsapp_queue_live[name],
    //                                  'picture'  => $whatsapp_queue_live[sender_avatar],
    //                                  'position' => $v,
    //                                  'country'  => '',
    //                                  'browser'  => '',
    //                                  'time'     => $whatsapp_queue_live[time]);
    //     $v ++;
    // }

    // $db->setQuery("SELECT  c.id,
    //                        sender_avatar,
    //                        sender_name AS `name`,
    //                        c.`status`,
    //                        TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.first_datetime,'%H:%i:%s')) AS `time`
    //                FROM   `skype_chat` as c
    //                WHERE   c.`status` = 1");
    
    // $skype_queue = $db->getResultArray();
    $skype_queue_array = array();
    $v = 1;
    
    // foreach ($skype_queue[result] AS $skype_queue_live){
    //     $skype_queue_array[] = array('id'       => $skype_queue_live[id],
    //                                  'number'   => $skype_queue_live[name],
    //                                  'picture'  => $skype_queue_live[sender_avatar],
    //                                  'position' => $v,
    //                                  'country'  => '',
    //                                  'browser'  => '',
    //                                  'time'     => $skype_queue_live[time]);
    //     $v ++;
    // }

    // $db->setQuery("SELECT   c.id,
    //                         '',
    //                         c.peer_id AS `name`,
    //                         c.`status_id` AS `status`,
    //                         TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(c.start_datetime,'%H:%i:%s')) AS `time`
    //                     FROM   `video_calls` as c
    //                     WHERE   c.`status_id` = 1");

    // $video_queue = $db->getResultArray();
    $video_queue_array = array();
    $v = 1;

    // foreach ($video_queue[result] AS $video_queue_live){
    // $video_queue_array[] = array('id'       => $video_queue_live[id],
    //             'number'   => $video_queue_live[name],
    //             'picture'  => '',
    //             'position' => $v,
    //             'country'  => '',
    //             'browser'  => '',
    //             'time'     => $video_queue_live[time]);
    // $v ++;
    // }

    
    $db->setQuery("SELECT    chat.id,
    chat.last_request_datetime,
                                users_cumunication.user_id,
                                user_info.`name` AS `nname`,
                                1 AS `count`,
                               'georgia.png' AS `country`,
                               `chat`.`browser`,
                               `chat`.`device`,
                                chat.`name`,
                                (SELECT IF(chat_details.operator_user_id = 0,0,1)
                                 FROM chat_details
                                 WHERE chat_details.chat_id = chat.id
                                 ORDER BY chat_details.id DESC
                                 LIMIT 1) AS `duration_arrow`,
                                (SELECT IF(chat_details.operator_user_id = 0,TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat_details.message_datetime,'%H:%i:%s')),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat_details.message_datetime,'%H:%i:%s')))
                                 FROM chat_details
                                 WHERE chat_details.chat_id = chat.id
                                 ORDER BY chat_details.id DESC
                                 LIMIT 1) AS `time`,
                                 TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(chat.join_date,'%H:%i:%s')) AS `time1`
                                                        
                                                     
                      FROM      chat
                      JOIN      users_cumunication ON users_cumunication.chat = 1 
                      JOIN      users ON users.id = chat.last_user_id 
                      JOIN      user_info ON users.id = user_info.user_id
                      WHERE     users.logged = 1 AND chat.`status` = 2 AND users.id = '$aRow_new[user_id]'
                      GROUP BY  chat.last_user_id");
    
    $user_chat    = $db->getResultArray();
    $chat_array   = array();
    $chat_counter = 0;
    
    foreach ($user_chat[result] AS $user_chat_live){
            $chat_array[$chat_counter] =  array(   'id'             => $user_chat_live[id],
                                                   'country'        => $user_chat_live[country],
                                                   'browser'        => $user_chat_live[browser],
                                                   'os'             => $user_chat_live[device],
                                                   'author'         => $user_chat_live[name],
                                                   'duration'       => $user_chat_live[time],
                                                   'duration_arrow' => $user_chat_live[duration_arrow],
                                                   'all_duration'   => $user_chat_live[time1]);
                                                   $chat_counter ++;
    }
    
    $new_data[users][$counter]['chat']=$chat_array;
    
    $db->setQuery("SELECT    mail_live.id,
                        sender_name AS `name`,
                    (SELECT IF(ISNULL(mail_live_detail.user_id),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(mail_live_detail.datetime,'%H:%i:%s')),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(mail_live_detail.datetime,'%H:%i:%s')))
                    FROM mail_live_detail
                    WHERE mail_live_detail.mail_id = mail_live.id
                    ORDER BY mail_live_detail.id DESC
                    LIMIT 1) AS `time`,
                    (SELECT IF(ISNULL(mail_live_detail.user_id),0,1)
                    FROM mail_live_detail
                    WHERE mail_live_detail.mail_id = mail_live.id
                    ORDER BY mail_live_detail.id DESC
                    LIMIT 1) AS `duration_arrow`,
                        TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(mail_live.send_datetime,'%H:%i:%s')) AS `time1`
                    FROM     users_cumunication
                    JOIN     users ON users.id = users_cumunication.user_id
                    JOIN     user_info ON users.id = user_info.user_id
                    JOIN     mail_live ON mail_live.user_id = users_cumunication.user_id AND mail_live.`mail_status_id` = 2
                    WHERE    users_cumunication.chat = 1 AND users.logged = 1 AND users.id = '$aRow_new[user_id]'
                    GROUP BY mail_live.id");
    
    
    $user_mail_chat  = $db->getResultArray();
    $mail_chat_array = array();
    $mail_counter    = 0;
    
    foreach ($user_mail_chat[result] AS $mail_chat_live){
        $mail_chat_array[$mail_counter] =  array(       'id'             => $mail_chat_live[id],
                                                        'country'        => 'georgia.png',
                                                        'picture'        => $mail_chat_live[sender_avatar],
                                                        'browser'        => '',
                                                        'author'         => $mail_chat_live[name],
                                                        'duration'       => $mail_chat_live[time],
                                                        'duration_arrow' => $mail_chat_live[duration_arrow],
                                                        'all_duration'   => $mail_chat_live[time1]);
                                                        $mail_counter++;
    }

    
    $new_data[users][$counter]['mail']=$mail_chat_array;
    
    
    $db->setQuery(" SELECT     fb_chat.id,
                               sender_avatar,
                               sender_name AS `name`,
                              (SELECT IF(ISNULL(fb_messages.user_id),0,1)
							   FROM fb_messages
							   WHERE fb_messages.fb_chat_id = fb_chat.id
							   ORDER BY fb_messages.id DESC
							   LIMIT 1) AS `duration_arrow`,
                              (SELECT IF(ISNULL(fb_messages.user_id),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_messages.datetime,'%H:%i:%s')),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_messages.datetime,'%H:%i:%s')))
							   FROM fb_messages
							   WHERE fb_messages.fb_chat_id = fb_chat.id
							   ORDER BY fb_messages.id DESC
							   LIMIT 1) AS `time`,
                               TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_chat.first_datetime,'%H:%i:%s')) AS `time1`
                     FROM      users_cumunication
                     JOIN      users ON users.id = users_cumunication.user_id
                     JOIN      user_info ON users.id = user_info.user_id
                     JOIN      fb_chat ON fb_chat.last_user_id = users_cumunication.user_id AND fb_chat.`status` = 2
                     WHERE     users_cumunication.chat = 1 AND users.logged = 1 AND users.id = '$aRow_new[user_id]'
                     GROUP BY  fb_chat.id");
    
    $user_fb_chat = $db->getResultArray();
    
    $fb_chat_array = array();
    $fb_counter    = 0;
    foreach ($user_fb_chat[result] AS $fb_chat_live){
        
        $fb_chat_array[$fb_counter] =  array(   'id'             => $fb_chat_live[id],
                                                'country'        => '',
                                                'picture'        => $fb_chat_live[sender_avatar],
                                                'browser'        => '',
                                                'author'         => $fb_chat_live[name],
                                                'duration'       => $fb_chat_live[time],
                                                'duration_arrow' => $fb_chat_live[duration_arrow],
                                                'all_duration'   => $fb_chat_live[time1]);
                                                $fb_counter ++;
    }
    
    $new_data[users][$counter]['fb'] = $fb_chat_array;


    $db->setQuery(" SELECT    fb_comments.id, fb_user_name AS `name`,

                             (SELECT IF(ISNULL(fb_comments.user_id	),0,1)
                              FROM fb_comments
                              WHERE fb_comments.feeds_id = fb_feeds.id
                              ORDER BY fb_comments.id DESC
                              LIMIT 1) AS `duration_arrow`,

                             (SELECT IF(ISNULL(fb_comments.user_id),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_comments.time,'%H:%i:%s')),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_comments.id,'%H:%i:%s')))
                              FROM fb_comments
                              ORDER BY fb_comments.id DESC
                              LIMIT 1) AS `time`,

                              TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(fb_comments.time,'%H:%i:%s')) AS `time1`
                    FROM      users_cumunication
                    JOIN      users ON users.id = users_cumunication.user_id
                
                    JOIN      user_info ON users.id = user_info.user_id
                    JOIN      fb_comments ON fb_comments.last_user_id = users_cumunication.user_id AND fb_comments.`status` = 2
                    JOIN      fb_feeds ON fb_feeds.id = fb_comments.feeds_id
                    WHERE     users_cumunication.chat = 1 AND users.logged = 1 AND users.id = '$aRow_new[user_id]'
                    GROUP BY  fb_comments.id");

            $user_fb_comments = $db->getResultArray();

            $fb_comments_array = array();
            $fb_comments_counter    = 0;

            foreach ($user_fb_comments[result] AS $fb_comments_live) {

                $fb_comments_array[$fb_comments_counter] =  array(  'id'             => $fb_comments_live[id],
                                                                    'country'        => '',
                                                                    'picture'        => $fb_comments_live[sender_avatar],
                                                                    'browser'        => '',
                                                                    'author'         => $fb_comments_live[name],
                                                                    'duration'       => $fb_comments_live[time],
                                                                    'duration_arrow' => $fb_comments_live[duration_arrow],
                                                                    'all_duration'   => $fb_comments_live[time1]);
                
                $fb_comments_counter ++;
            }

            $new_data[users][$counter]['fbc'] = $fb_comments_array;
    
    $db->setQuery(" SELECT    viber_chat.id,
                              sender_avatar,
                              sender_name AS `name`,
                             (SELECT   IF(ISNULL(viber_messages.user_id),0,1)
                              FROM     viber_messages
                              WHERE    viber_messages.viber_chat_id = viber_chat.id
                              ORDER BY viber_messages.id DESC
                              LIMIT 1) AS `duration_arrow`,
                             (SELECT IF(ISNULL(viber_messages.user_id),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(viber_messages.datetime,'%H:%i:%s')),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(viber_messages.datetime,'%H:%i:%s')))
                              FROM     viber_messages
                              WHERE    viber_messages.viber_chat_id = viber_chat.id
                              ORDER BY viber_messages.id DESC
                              LIMIT 1) AS `time`,
                              TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(viber_chat.first_datetime,'%H:%i:%s')) AS `time1`
                    FROM      users_cumunication
                    JOIN      users ON users.id = users_cumunication.user_id
                    JOIN      user_info ON users.id = user_info.user_id
                    JOIN      viber_chat ON viber_chat.last_user_id = users_cumunication.user_id AND viber_chat.`status` = 2
                    WHERE     users_cumunication.chat = 1 AND users.logged = 1 AND users.id = '$aRow_new[user_id]'
                    GROUP BY  viber_chat.id");
    
    $user_viber_chat = $db->getResultArray();
    
    $viber_chat_array = array();
    $viber_counter    = 0;
    foreach ($user_viber_chat[result] AS $viber_chat_live){
        
        $viber_chat_array[$viber_counter] =  array( 'id'             => $viber_chat_live[id],
                                                    'country'        => '',
                                                    'picture'        => $viber_chat_live[sender_avatar],
                                                    'browser'        => '',
                                                    'author'         => $viber_chat_live[name],
                                                    'duration'       => $viber_chat_live[time],
                                                    'duration_arrow' => $viber_chat_live[duration_arrow],
                                                    'all_duration'   => $viber_chat_live[time1]);
        $viber_counter ++;
    }
    
    


    $new_data[users][$counter]['viber'] = $viber_chat_array;




    $db->setQuery(" SELECT      video_calls.id,
						'' AS sender_avatar,
						video_calls.peer_id AS `name`,
                        video_calls.status_id AS `status_id`,
						IF( ISNULL ( video_calls.user_id ), 0, 1 ) AS `duration_arrow`,
						IF(ISNULL(video_calls.user_id), TIMEDIFF(TIME_FORMAT(NOW(), '%H:%i:%s'), TIME_FORMAT(video_calls.start_datetime,'%H:%i:%s')), TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'), TIME_FORMAT(video_calls.start_datetime,'%H:%i:%s') )) AS `time`,
					TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'), TIME_FORMAT(video_calls.start_datetime,'%H:%i:%s') ) AS `time1`
					FROM      users_cumunication
					JOIN      users ON users.id = users_cumunication.user_id
					JOIN      user_info ON users.id = user_info.user_id
					JOIN      video_calls ON video_calls.user_id = users_cumunication.user_id AND video_calls.`status_id` = 2
                    WHERE     users_cumunication.chat = 1 AND users.logged = 1 AND users.id = '$aRow_new[user_id]'
                    GROUP BY  video_calls.id");

$user_video_chat = $db->getResultArray();


    
    $video_chat_array = array();
    $video_counter    = 0;
    foreach ($user_video_chat[result] AS $video_chat_live){
        
        $video_chat_array[$video_counter] =  array( 'id'             => $video_chat_live[id],
                                                    'country'        => '',
                                                    'picture'        => $video_chat_live[sender_avatar],
                                                    'browser'        => '',
                                                    'author'         => $video_chat_live[name],
                                                    'duration'       => $video_chat_live[time],
                                                    'status_id'      => $video_chat_live[status_id],
                                                    'duration_arrow' => $video_chat_live[duration_arrow],
                                                    'all_duration'   => $video_chat_live[time1]);
        $video_counter ++;
    }
    
    


    $new_data[users][$counter]['video'] = $video_chat_array;



    ///////////////////////

    $db->setQuery(" SELECT    whatsapp_chat.id,
                              sender_avatar,
                              sender_name AS `name`,
                             (SELECT   IF(ISNULL(whatsapp_messages.user_id),0,1)
                              FROM     whatsapp_messages
                              WHERE    whatsapp_messages.whatsapp_chat_id = whatsapp_chat.id
                              ORDER BY whatsapp_messages.id DESC
                              LIMIT 1) AS `duration_arrow`,
                             (SELECT IF(ISNULL(whatsapp_messages.user_id),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(whatsapp_messages.datetime,'%H:%i:%s')),TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(whatsapp_messages.datetime,'%H:%i:%s')))
                              FROM     whatsapp_messages
                              WHERE    whatsapp_messages.whatsapp_chat_id = whatsapp_chat.id
                              ORDER BY whatsapp_messages.id DESC
                              LIMIT 1) AS `time`,
                              TIMEDIFF(TIME_FORMAT(NOW(),'%H:%i:%s'),TIME_FORMAT(whatsapp_chat.first_datetime,'%H:%i:%s')) AS `time1`
                    FROM      users_cumunication
                    JOIN      users ON users.id = users_cumunication.user_id
                    JOIN      user_info ON users.id = user_info.user_id
                    JOIN      whatsapp_chat ON whatsapp_chat.last_user_id = users_cumunication.user_id AND whatsapp_chat.`status` = 2
                    WHERE     users_cumunication.chat = 1 AND users.logged = 1 AND users.id = '$aRow_new[user_id]'
                    GROUP BY  whatsapp_chat.id");
    
    $user_whatsapp_chat = $db->getResultArray();
    
    $whatsapp_chat_array = array();
    $whatsapp_counter    = 0;
    foreach ($user_whatsapp_chat[result] AS $whatsapp_chat_live){
        
        $whatsapp_chat_array[$whatsapp_counter] =  array( 'id'             => $whatsapp_chat_live[id],
                                                    'country'        => '',
                                                    'picture'        => $whatsapp_chat_live[sender_avatar],
                                                    'browser'        => '',
                                                    'author'         => $whatsapp_chat_live[name],
                                                    'duration'       => $whatsapp_chat_live[time],
                                                    'duration_arrow' => $whatsapp_chat_live[duration_arrow],
                                                    'all_duration'   => $whatsapp_chat_live[time1]);
        $whatsapp_counter ++;
    }
    
    


    $new_data[users][$counter]['whatsapp'] = $whatsapp_chat_array;

    ///////////////////////




    
    $new_data[queue]['phone'] = $phone_queue_array;
    $new_data[queue]['chat']  = $chat_queue_array;
    $new_data[queue]['mail']  = $mail_queue_array;
    $new_data[queue]['fb']    = $fb_queue_array;
    $new_data[queue]['fbc']    = $fb_comments_queue_array;
    $new_data[queue]['viber'] = $viber_queue_array;
    $new_data[queue]['whatsapp'] = $whatsapp_queue_array;
    $new_data[queue]['skype'] = $skype_queue_array;
    $new_data[queue]['video'] = $video_queue_array;

    $counter ++;
}


$ext_queues = $db->setQuery("SELECT     user_info.`name`,
                					    user_info.user_id,
                					   `queues`.member AS `last_extension`,
                					   `queues`.queue,
                					    CASE
                    						WHEN `channels`.`context` = 'macro-dial-one' THEN `channels`.`callerid`
                    						WHEN `channels`.`context` = 'macro-dialout-trunk' THEN 
                    						(SELECT `chan`.`callerid` 
                    					 	 FROM   `asteriskcdrdb`.`channels` AS chan
                    						 WHERE   chan.bridgedto = `channels`.bridgedto
                    						 AND     chan.application = 'AppDial'
                    						 LIMIT 1)
                                             ELSE (SELECT callerid FROM asteriskcdrdb.channels WHERE bridgedto = `ch2`.`bridgedto` AND application='Dial' LIMIT 1)
                					    END  AS `phone`,
                                        CASE
                    						WHEN `channels`.`context` = 'macro-dial-one' THEN 'in'
                    						WHEN `channels`.`context` = 'macro-dialout-trunk' THEN 'out'
                                            WHEN `channels`.`context` = 'autodialer' THEN 'out'
                					    END  AS `type`,
                                        CASE
                                            WHEN `channels`.`context` = 'macro-dial-one' OR `channels`.`context` = 'macro-dial' THEN 'arr_busy_in'
                                            WHEN `channels`.`context` = 'macro-dialout-trunk' THEN 'arr_busy_out'
                                            WHEN (SELECT context FROM asteriskcdrdb.channels WHERE bridgedto = `ch2`.`bridgedto` AND application='Dial' LIMIT 1) = 'autodialer' THEN 'arr_busy_in'
                                        END  AS `arrow_call`,
                					    TIME_FORMAT(SEC_TO_TIME(IFNULL(`channels`.`duration`,ch2.duration)), '%i:%s') AS `time`,
                					   `queues`.`status`,
                					   '' As pin,
                                       (SELECT department.name
                                FROM asterisk_queue
                                JOIN department_queues ON department_queues.queue_id = asterisk_queue.id
                                JOIN department ON department.id = department_queues.dep_id
                                WHERE department.actived = 1 AND asterisk_queue.number = `queues`.`queue`) AS `dep`
                             FROM       asteriskcdrdb.queues
                             LEFT JOIN `asteriskcdrdb`.`channels` ON REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(`channels`.`channel`, '@', 1),'-',1),'SIP','Local') = `queues`.`extention`
                            	   AND `channels`.`context` IN('macro-dial-one','macro-dialout-trunk','autodialer') AND `channels`.`state` IN ('Up', 'Ring')
                                   LEFT JOIN `asteriskcdrdb`.`channels` AS `ch2` ON REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(`ch2`.`channel`, '@', 1),'-',1),'SIP','Local') = `queues`.`extention`
                           AND `ch2`.`state` IN ('Up', 'Ring')
                             LEFT JOIN  asterisk_extension ON queues.member = asterisk_extension.number
                             LEFT JOIN  users ON users.extension_id = asterisk_extension.id AND logged = 1 AND users.actived = 1
                             LEFT JOIN  user_info ON user_info.user_id = users.id
                             WHERE      ISNULL(users.id) AND `queues`.status != 'unavailable' AND queues.queue IN (20)
                             GROUP BY `queues`.queue, `queues`.member");

$res_ext_queues = $db->getResultArray();
$queue_ext_unselect = array();
$arry = array();
$script_end = time();
foreach ($res_ext_queues[result] AS $extentions){
    $new_data[users][$counter] = array( 'userid'   => $extentions['user_id'],
                                        'username' => $extentions['name']);
    

        // $db->setQuery(" SELECT  `name` 
        //                 FROM    `saved_users_by_phone` 
        //                 WHERE   `phone` = '$aRow_ast[phone]'");
        // $saved_name_arr = $db->getResultArray();
        // $saved_name = $saved_name_arr['result'][0]['name'];

        // if($saved_name != ''){
        //     $saved_name = $aRow_ast['phone'].'/'.$saved_name;
        // }else{
            $saved_name = $aRow_ast['phone'];
        // }


    $queue_ext_unselect  = array('queue'    => $extentions['queue'],
                                 'ext'      => $extentions['last_extension'],
                                 'number'   => $extentions['phone'],
                                 'time'     => $extentions['time'],
                                 'arrow_call'   => $aRow_ast['arrow_call'],
                                 'pin'      => $extentions['pin'],
                                 'type'     => $extentions['type'],
                                 'dep'      => $extentions['dep'],
                                 'status'   => $extentions['status'],
                                 'info'     => $extentions['phone']);
    
    $new_data[users][$counter][phone][0] = $queue_ext_unselect;
    
    $new_data[users][$counter]['chat'] = array();
    $new_data[users][$counter]['mail'] = array();
    $new_data[users][$counter]['fb']   = array();
    $new_data[users][$counter]['viber']   = array();
    $new_data[users][$counter]['fbc']   = array();
    $new_data[users][$counter]['whatsapp']   = array();
    $new_data[users][$counter]['skype']   = array();
    $new_data[users][$counter]['video']   = array();
    $counter ++;
    
}

$db->setQuery("SELECT `key`,
                       background_image_name,
                       actived
               FROM   `source`
               WHERE  `key` != '0'");

$comunicatin_result = $db->getResultArray();

foreach ($comunicatin_result[result] AS $source){
    
    $display = '';
    if ($source[actived] == 0) {
        $display='style="display:none"';
    }
    $key = $source[key].'_permission';
    $new_data[permission][$key] = $display;
    
    $counter ++;
}


$db->setQuery(" SELECT   `status_id`, COUNT(*) AS `count` 
                FROM     `crm` 
                LEFT JOIN crm_views ON crm_views.crm_id = crm.id AND crm_views.user_id = 1
                WHERE    `actived` = 1 AND ISNULL(crm_views.id)
                GROUP BY `crm`.`status_id`");
$crm_count = $db->getResultArray();

$crm_count_arr = Array();
$crm_all_count = 0;
foreach($crm_count['result'] as $count_obj){

    array_push($crm_count_arr, $count_obj);
    $crm_all_count = $crm_all_count + $count_obj['count'];
}

$new_data[crm_counts]       = $crm_count_arr;
$new_data[crm_counts_sum]   = $crm_all_count;

$new_data['script_diff'] = $script_end - $script_start;
// echo '<pre>';
// var_dump($new_data);
// echo '<pre>';
//echo json_encode($new_data);

$json = json_encode($new_data);
//var_dump($json);
file_put_contents('liveState.json', $json);

//echo $json;
?>

