<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
require_once('../../includes/classes/dialog_automator.class.php');
include('../../includes/classes/wsdl.class.php');

$action = $_REQUEST['act'];
$error	= '';
$filterID = '';
$data	= array();
$user_id	             = $_SESSION['USERID'];
//incomming
$incom_id			 = $_REQUEST['id'];
$call_date			 = $_REQUEST['call_date'];
$hidden_user		 = $_REQUEST['hidden_user'];
$hidden_inc			 = $_REQUEST['hidden_inc'];
$imchat			     = $_REQUEST['imchat'];
$chat_id             = $_REQUEST['chat_id'];
$ipaddres            = $_REQUEST['ipaddres'];
$ii			         = $_REQUEST['ii'];
$phone				 = $_REQUEST['phone'];
// file
$rand_file			= $_REQUEST['rand_file'];
$file				= $_REQUEST['file_name'];
$optData = array();

$permissionLVL = getPermissionLvl($user_id);
function getPermissionLvl($id)
{
	global $db;
	$db->setQuery("SELECT group_id FROM users WHERE id = $id");
	$res = $db->getResultArray();
	return $res["result"][0]["group_id"];
}
switch ($action) {
	case 'get_add_page':
		$number		= $_REQUEST['number'];
		$ipaddres   = $_REQUEST['ipaddres'];
		$ab_pin     = $_REQUEST['ab_pin'];
		$callType   = $_REQUEST['calltype'];
		if ($callType == 'autodialer') {
			$page		= GetPage('', $number, '', $ipaddres, $ab_pin, getOptionals($_REQUEST['source'], $chat_id), 'autodialer');
		} else if ($callType == 'outgoing') {
			$page		= GetPage('', $number, '', $ipaddres, $ab_pin, getOptionals($_REQUEST['source'], $chat_id), 'outgoing');
		} else {
			$page		= GetPage(getlastphone($number), $number, '', $ipaddres, $ab_pin, getOptionals($_REQUEST['source'], $chat_id), '');
		}
		$data		= array('page'	=> $page);

		break;
	case 'check_mail':
		$mail = $_REQUEST['value'];

		if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			$db->setQuery("INSERT INTO mail_contact SET `datetime` = NOW(),
														`address` = '$mail'");
			$db->execQuery();

			$data['status'] = 'ok';
		}
		break;
	case 'get_new_addr_dialog':
		$type 		= $_REQUEST['type'];

		$page 		= get_new_addr_page($type);
		$data		= array('page'	=> $page);
		break;
	case 'get_docs_project':
		$project_id = $_REQUEST['project_id'];
		$board      = $_REQUEST['info_board'];
		$inc_id     = $_REQUEST['inc_id'];
		$page       = get_pined_docs2($project_id, $board, $inc_id);
		$data		= array('page'	=> $page);

		break;
	case 'check_beneficiary':
		$pn = $_REQUEST['pn'];

		$login = 'agrocredit';
		$password = '!4groCred1t_p';
		$url = 'http://maps.mepa.gov.ge/dbgis-proxy/api/rest_tl_data/agro_credit/check_interview_status?frn=' . urlencode($pn);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		//curl_setopt($ch, CURLOPT_POSTFIELDS, "frn=".urlencode($pn));
		//curl_setopt($ch, CURLOPT_POSTREDIR, 3);


		$resp = curl_exec($ch);
		$beneficiary = json_decode($resp, true);

		curl_close($ch);
		$fermer_data = $beneficiary['services'][0];
		if ($beneficiary == NULL) {
			$data['response'] = 999;
		} else {
			if ($fermer_data['entities'][0]['is_valid']) {
				$data['response'] = 1;
			} else {
				$data['response'] = 0;
			}
		}
		break;
	case 'get_user':
		$phone = $_REQUEST['phone'];
		$inc_call_id = $_REQUEST['inc_id'];
		$db->setQuery("	SELECT did
						FROM asterisk_call_log
						WHERE id = (SELECT 	asterisk_incomming_id
									FROM 	incomming_call
									WHERE 	id = '$inc_call_id' LIMIT 1)");
		$did = $db->getResultArray();
		$db->setQuery("	SELECT value

						FROM incomming_request_processing
						WHERE processing_setting_detail_id = 127 
						AND incomming_request_id = (SELECT id FROM incomming_call WHERE phone = '$phone' AND user_id > 0 ORDER BY id DESC LIMIT 1)");
		$firstname = $db->getResultArray();


		$db->setQuery("	SELECT value

						FROM incomming_request_processing
						WHERE processing_setting_detail_id = 128 
						AND incomming_request_id = (SELECT id FROM incomming_call WHERE phone = '$phone' AND user_id > 0 ORDER BY id DESC LIMIT 1)");
		$lastname = $db->getResultArray();

		$data = array("firstname" => $firstname['result'][0]['value'], "lastname" => $lastname['result'][0]['value'], "call_log_number" => $did['result'][0]['did']);

		break;
	case 'chat_docs_list':
		$columnCount    = $_REQUEST['count'];
		$cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 	id,
								name,
								`datetime`,
								CONCAT('<a target=\"blank\" style=\"color:blue;\" href=',link,'>',link,'</a>') AS 'link',
								CONCAT('<a class=\"select_chat_doc\" data-link=\"',link,'\" style=\"color:blue;\" href=\"#\">არჩევა</a>') AS 'act'
						FROM 	mepa_project_docs
						WHERE 	actived='1'");
		$data = $db->getKendoList($columnCount, $cols);
		break;
	case 'chat_docs':
		$page = get_chat_docs_page();
		$data = array('page' => $page);
		break;
	case 'sms4mepa':
		$type = $_REQUEST['type'];
		$docs_ids = $_REQUEST['ids'];
		$data		= array('page' => get_new_mepa_sms($type, $docs_ids));

		break;
	case 'mail4mepa':
		$type = $_REQUEST['type'];
		$docs_ids = $_REQUEST['ids'];
		$data		= array('page' => get_new_mepa_mail($type, $docs_ids));

		break;
	case 'task_branch_changed':
		$page		= get_task_recipient($_REQUEST['cat_id']);
		$data		= array('page'	=> $page);
		break;
	case 'task_recipient_changed':
		$page		= get_task_branch($_REQUEST['recipient_id']);
		$data		= array('page'	=> $page);
		break;
	case 'get_list_contracts1':
		$phone 	= $_REQUEST['phone'];
		$pn 	= $_REQUEST['pn'];
		$ab_num = $_REQUEST['ab_num'];

		if ($phone != '') {
			$post_data = "mobile=" . $phone;
		} else if ($pn != '') {
			$post_data = "pn=" . $pn;
		} else {
			$post_data = "code=" . $ab_num;
		}

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://test.billing.com.ge/api/get_abonent");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Accept : application/json',
			'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOWZhYzJiMmM2ODAzYTVkZDU5OWI3YzI4NGQzZDM2YWQ3YjUwOTAyODYyYTU1NDQxN2NhMzg0ZWFhZmU0NTVhOTlhMmVmZDlkYWVlNDllZmMiLCJpYXQiOjE1OTQ4MTI1MDEsIm5iZiI6MTU5NDgxMjUwMSwiZXhwIjoxNjI2MzQ4NTAxLCJzdWIiOiI5MSIsInNjb3BlcyI6W119.B9R4TDPrNNNclOcvHh8oEvXPica_yqs26qr0FZ-ZLBBFOKvPS28lwG_qB6YEppeMF3ReZbJ5k3QDD6Ahh9Da-OtQFP4IK2VK_VHQ6SYypK_aIedYDdqUfR2aYNyvcCBNP9mOQ94Gr35mO1rLPa3B6uOWZCWpmESB_MXxLIRJ4aeJolhYbg5sW0-xlXS_mGS6xnTBjj63obVzdmR4t4TQtlkKW_eR2JGLPNW5FA0ryBhYqU7r9wVHVXdQLHcIz90jPFz0FS5wih3ppOGLi0kMZAycYEkkDvJMZU4zSZOElgrHOgoI4w8m6q8PMreeCNQ9QMLdANEGV6PJFAFv_zKusZ4ovRLccElciTuq-2rONzh7VtYMnD6UQT71GxDVLs0ZqoQxXMYdwp8AMnANOCJnAl2d1Hlk4F9iOxTPCr7VeYCgc4Y6nc9weZ4gfipaZeU4z7qXF9laiN9LY8ONgdJWL-lH1li52HOuvI7EZAA_e5R8GE8W6zlXZYxoy1-EZKYdqm_3JYNUEDzt5f7tY2ftYGII8VJXDRe__BJXuipfFKUNl2_t3QyDmQj2Z-iHpOCieAPjeQEM-D0TcZsd-ZxJIyJ7sPU3dSB8uipMNuq__ev60Fp2Du1NZKohp63zN5cyUYXj1E05iveZD1dINJi5yqJ0t-F7Ue7zGxhUfLhmbvo'
		));

		$server_output = curl_exec($ch);

		curl_close($ch);

		$data = json_decode($server_output, true);


		break;
	case 'get_list_abonent':
		$pn = $_REQUEST["pn"];
		$code = $_REQUEST["code"];
		$mobile = $_REQUEST["mobile"];
		$identify = $_REQUEST["identify"];

		$post = array();
		switch ($identify) {
			case 1:
				$post = array('code'=>$code);
				break;
			case 2:
				$post = array('pn'=>$pn);
				break;
			case 3:
				$post = array('mobile'=>$mobile);
				break;
			
			default:
				# code...
				break;
		}
		
		// if($code != ""){
		// 	$post = array('code'=>$code);
		// }
		// elseif($mobile != ""){
		// 	$post = array('mobile'=>$mobile);
		// }
		// elseif($pn != ""){
		// 	$post = array('pn'=>$pn);
		// }
		// P202118572
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://billing.com.ge/api/get_abonent',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $post,
		CURLOPT_HTTPHEADER => array(
			'Accept: application/json',
			'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMzU0OTAxNGU2ZmRiYjIwNGU2MmMzYjNjMTRiMzMyZWUwMzQ4MTNmN2UyYTI4M2FiZTMyOTMyMjJjMTQ1ZTc5NDI3NjIyZDhlZjg5NmEyN2EiLCJpYXQiOjE2MzEyNjMwODMsIm5iZiI6MTYzMTI2MzA4MywiZXhwIjoxNjYyNzk5MDgzLCJzdWIiOiIxNjEiLCJzY29wZXMiOltdfQ.JYdQ2KVew1mcAZBtGk2Y9Nyl_fVM-muQ0oJGvU3vSaEONq_NDOx5h-Djx6orUWu0ky9Z8NjqGf6foGiz1FLOy_eFmq7_GjQbiKj83LzxiRRyPpow_PReycU6ASZVO-UnXpr1kUitq1cOwNVAHn3NI9TWC59IzKvPzVvXTLmhARoQTDjOmjXh1e3qtF_Qwq9N6OQ67qyTKpMQryOBp7hsJCJUO4_9cUEoUc7-2she5NiqX4K2Bv-FG95lBMH_I8dQn1M2sn4OlVwTyR7oDQ1YtP7RyONw23Lk-vPyuz5uChXLjE5QT0fIDjBfhtivo2euKF7R8CJ0IJwu_VcaTN-aJxesMOhSfXJPzKKICAVqb8Rw5M9mcZU9UOhPwKewEI03USUIC3shC_zWa_60olNumtqVwVyfY4g-FQLdUUnrSa-4cDfMkPoWgGwGiq5VrH58-K2kcIJNy-VyPddBlFT-UKFZTsxSPUS3UWz7fQHzaX0gVD9DdVxSqTLl0fvAK22hcO4YPoKZCGAW2gjAYpBZnh2WoQ7Mg7LkDwcdymUmNHs1D5mBSpT6K2l_smHVEksQnCUGOwUM2FoCVnN5M5j5yyA2fRrJ5xu0cfZLj_GB0cYbDLqnhQxhWjE7sHNEfGBYL4ZPKwvSFVJufoldy_bjMhMC44HikOig9Gxf-ojCWfs'
		),
		));

		$response = curl_exec($curl);
		$dataResult = json_decode($response, true);
		curl_close($curl);
	

		if($dataResult["code"] == 0)
			$error = " BY melioration Service: გამოგზავნილი პარამეტრი არასწორია ";
		else if($dataResult["code"] == 1)
			$error = " აბონენტი არ მოიძებნა ";
		else if($dataResult["code"] == 2) {
			$data = array('data'=>$dataResult["abonent"]);
		}

		break;
	case 'get_list_contracts':
		$columnCount    = $_REQUEST['count'];
		$cols[]         = $_REQUEST['cols'];
		$code 	= $_REQUEST['code'];

		if ($code != '') {
			$post_data = "code=" . $code;
		}
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://billing.com.ge/api/get_contract");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Accept : application/json',
			'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMzU0OTAxNGU2ZmRiYjIwNGU2MmMzYjNjMTRiMzMyZWUwMzQ4MTNmN2UyYTI4M2FiZTMyOTMyMjJjMTQ1ZTc5NDI3NjIyZDhlZjg5NmEyN2EiLCJpYXQiOjE2MzEyNjMwODMsIm5iZiI6MTYzMTI2MzA4MywiZXhwIjoxNjYyNzk5MDgzLCJzdWIiOiIxNjEiLCJzY29wZXMiOltdfQ.JYdQ2KVew1mcAZBtGk2Y9Nyl_fVM-muQ0oJGvU3vSaEONq_NDOx5h-Djx6orUWu0ky9Z8NjqGf6foGiz1FLOy_eFmq7_GjQbiKj83LzxiRRyPpow_PReycU6ASZVO-UnXpr1kUitq1cOwNVAHn3NI9TWC59IzKvPzVvXTLmhARoQTDjOmjXh1e3qtF_Qwq9N6OQ67qyTKpMQryOBp7hsJCJUO4_9cUEoUc7-2she5NiqX4K2Bv-FG95lBMH_I8dQn1M2sn4OlVwTyR7oDQ1YtP7RyONw23Lk-vPyuz5uChXLjE5QT0fIDjBfhtivo2euKF7R8CJ0IJwu_VcaTN-aJxesMOhSfXJPzKKICAVqb8Rw5M9mcZU9UOhPwKewEI03USUIC3shC_zWa_60olNumtqVwVyfY4g-FQLdUUnrSa-4cDfMkPoWgGwGiq5VrH58-K2kcIJNy-VyPddBlFT-UKFZTsxSPUS3UWz7fQHzaX0gVD9DdVxSqTLl0fvAK22hcO4YPoKZCGAW2gjAYpBZnh2WoQ7Mg7LkDwcdymUmNHs1D5mBSpT6K2l_smHVEksQnCUGOwUM2FoCVnN5M5j5yyA2fRrJ5xu0cfZLj_GB0cYbDLqnhQxhWjE7sHNEfGBYL4ZPKwvSFVJufoldy_bjMhMC44HikOig9Gxf-ojCWfs'
		));

		$server_output = curl_exec($ch);
		//print_r($server_output);
		curl_close($ch);

		$dataResult = json_decode($server_output, true);
		//print_r($dataResult);
		
		if($dataResult["code"] == 0)
			$error = " BY melioration Service: გამოგზავნილი პარამეტრი არასწორია ";
		else if($dataResult["code"] == 1)
			$error = " აბონენტი არ მოიძებნა ";
		else if($dataResult["code"] == 3) {
			$dataArr = $dataResult["contracts"];
			$i = 0;
			foreach ($dataArr as $key => $value) {
				$id = $value["service"];
				$db->setQuery(" SELECT `name` FROM service_center WHERE id = $id ");
				$service = $db->getResultArray()["result"][0]["name"];
				$dataArr[$i]["service"] = $service;
				$dataArr[$i]["service_id"] = $id;
				$i++;
			}
	
			
			// echo "<br><br><br>2<br><br><br>";
			$data = array('data'=>$dataArr,'total'=>COUNT($dataArr));
		}

		break;
	case 'calculate_fields':
		$pr_id = $_REQUEST['project_id'];

		$db->setQuery("	SELECT COUNT(*) AS 'cc'
						FROM mepa_project_inputs
						WHERE cat_id = '$pr_id' AND actived = 1");
		$field_count = $db->getResultArray();

		$db->setQuery("	SELECT 	region_selector
						FROM 	info_category
						WHERE 	id = '$pr_id'");
		$regions_on = $db->getResultArray();
		$regions_on = $regions_on['result'][0]['region_selector'];

		if ($regions_on == 1) {
			$regions_on = 4;
		}
		$fields_cc = $field_count['result'][0]['cc'] + $regions_on;
		$db->setQuery("	SELECT
							incomming_request_processing.id,
							incomming_request_processing.processing_setting_detail_id AS 'input_id',
							incomming_request_processing.`value`,
							incomming_request_processing.int_value,
							processing_setting_detail.processing_field_type_id AS 'type' 
						FROM
							incomming_request_processing
							LEFT JOIN processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id 
						WHERE
							incomming_request_processing.incomming_request_id = '$res[id]' 
							AND incomming_request_processing.additional_dialog_id = '0'
							AND incomming_request_processing.processing_setting_detail_id != 0
							AND incomming_request_processing.processing_setting_detail_id != 999999999");
		$ready_fields = $db->getResultArray();

		$db->setQuery("	SELECT
							incomming_request_processing.id,
							incomming_request_processing.processing_setting_detail_id AS 'input_id',
							incomming_request_processing.`value`,
							incomming_request_processing.int_value,
							processing_setting_detail.processing_field_type_id AS 'type' 
						FROM
							incomming_request_processing
							LEFT JOIN processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id 
						WHERE
							incomming_request_processing.incomming_request_id = '$res[id]' 
							AND incomming_request_processing.additional_dialog_id = '0'
							AND incomming_request_processing.processing_setting_detail_id != 0
							AND incomming_request_processing.processing_setting_detail_id = 999999999
							AND incomming_request_processing.int_value != 0");
		$regions_cc = $db->getResultArray();


		$r_fields = $ready_fields['count'] + $regions_cc['count'];
		$data = array("fields_cc" => $fields_cc, "r_fields" => $r_fields);
		break;
	case 'get_contact_project':
		$project_id = $_REQUEST['project_id'];
		$board      = $_REQUEST['info_board'];
		$inc_id     = $_REQUEST['inc_id'];
		$page       = get_pined_contact2($project_id, $board, $inc_id);
		$data		= array('page'	=> $page);

		break;
	case 'add_dep':
		$rand_id = rand(100, 999);
		$returnable = '<div class="fieldset_row" id="dep_row_' . $rand_id . '">
						<div class="col-3-half">
							<label>უწყება</label>
							<select id="dep_' . $rand_id . '" class="dep" data-id="' . $rand_id . '">
							' . get_dep_1(0) . '
							</select>
						</div>
						<div class="col-3-half">
							<label>პროექტი</label>
							<select id="dep_project_' . $rand_id . '" class="dep_project" data-id="' . $rand_id . '">
							' . get_dep_1_1(0, 0) . '
							</select>
						</div>
						<div class="col-3-half">
							<label>ქვე პროექტი</label>
							<select id="dep_sub_project_' . $rand_id . '" class="dep_sub_project" data-id="' . $rand_id . '">
							' . get_dep_1_1_1(0, 0) . '
							</select>
						</div>
						<div class="col-3-half">
							<label>მომართვის ტიპი</label>
							<select id="dep_sub_type_' . $rand_id . '" class="dep_sub_type" data-id="' . $rand_id . '">
							' . get_project_type('') . '
							</select>
						</div>
						<div class="col-3-half" style="display: flex;">
							<div class="delete_dep" data-id="' . $rand_id . '"><i style="font-size:28px; margin-top:20px; margin-left:0px;cursor:pointer;" class="fa fa-minus-circle" aria-hidden="true"></i></div>
							<div id="get_dep_additional_addit" style="position: relative;" data-id="' . $rand_id . '"><img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/additional_inputs.png"></div>
							<img id="get_docs_project_addit" data-id="' . $rand_id . '" style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/documents.png">
							<img id="get_contact_project_addit" data-id="' . $rand_id . '" style="margin-left:1px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/contacts.png">
						</div>
					</div>';
		$data		= array('data'	=> $returnable, 'chosen_data' => $rand_id);
		break;
	case 'disable':
		$incom_id = $_REQUEST['id'];

		$db->setQuery("UPDATE `incomming_call`
						SET `noted`= 2
					   WHERE id=$incom_id");
		$db->execQuery();
		break;
	case 'get_signature':
		$site_id = $_REQUEST['site_id'];

		$db->setQuery(" SELECT signature 
                        FROM  `signature`
                        WHERE  account_id = '$site_id' AND actived = 1
                        LIMIT 1");

		$res = $db->getResultArray();

		$data = array('signature' => $res[result][0][signature]);

		break;
	case 'get_docs_list':
		$id = $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
		$cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 	id,
								name,
								`datetime`,
								CONCAT('<a target=\"blank\" style=\"color:blue;\" href=',link,'>',link,'</a>') AS 'link',
								CONCAT('<a target=\"blank\" style=\"color:blue;\" href=\"\media/uploads/documents/',file,'\">ნახვა</a>') AS 'act'
						FROM 	mepa_project_docs
						WHERE 	cat_id='$id' AND actived='1'");
		$result = $db->getKendoList($columnCount, $cols);

		$data = $result;
		break;
	case 'get_contacts_list':
		$id = $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
		$cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 	id,
								CONCAT(firstname,' ',lastname),
								position,
								phone,
								email
						FROM mepa_project_contacts
						WHERE cat_id='$id' AND actived='1'");
		$result = $db->getKendoList($columnCount, $cols);

		$data = $result;
		break;
	case 'get_mail_addresats':
		$db->setQuery("	SELECT 	`address` AS 'id',
								`address` AS 'name'
						FROM   	`mail_contact`
						WHERE  	`actived` = 1");
		$data = $db->getResultArray();
		break;
	case 'get_fieldsets':
		$setting_id = $_REQUEST['setting_id'];
		$chosen_keys = '';
		$datetime_keys = array();

		$multilevel_keys = array();
		$date_keys = array();

		$db->setQuery("	SELECT id,name
						FROM processing_fieldset
						WHERE actived = '1' AND id IN (SELECT fieldset_id FROM processing_fieldset_by_setting WHERE processing_setting_id='$setting_id')");
		$fieldsets = $db->getResultArray();
		foreach ($fieldsets['result'] as $field) {
			$db->setQuery("	SELECT 	id,`key`,depth,processing_field_type_id AS 'type'

							FROM 	processing_setting_detail
							WHERE 	processing_fieldset_id = '$field[id]' AND actived = '1' AND processing_field_type_id IN (8,10)
							ORDER BY position");
			$selectors = $db->getResultArray();
			foreach ($selectors['result'] as $item) {
				if ($item['type'] == 10) {
					$depth = $item['depth'];
					for ($i = 1; $i <= $depth; $i++) {
						$chosen_keys .= '#' . $item['key'] . '_' . $i . '--' . $item['id'] . '--' . $item['type'] . ',';

						$multilevel_keys[] = $item['key'] . '_' . $i . '--' . $item['id'] . '--' . $item['type'];
					}
				} else {
					$chosen_keys .= '#' . $item['key'] . '--' . $item['id'] . '--' . $item['type'] . ',';
				}
			}

			$db->setQuery("	SELECT 	id,`key`,processing_field_type_id AS 'type'

							FROM 	processing_setting_detail
							WHERE 	processing_fieldset_id = '$field[id]' AND actived = '1' AND processing_field_type_id='5'
							ORDER BY position");
			$datetimes = $db->getResultArray();
			foreach ($datetimes['result'] as $item) {
				$datetime_keys[] .= $item['key'] . '--' . $item['id'] . '--' . $item['type'];
			}

			$db->setQuery("	SELECT 	id,`key`,processing_field_type_id AS 'type'

							FROM 	processing_setting_detail
							WHERE 	processing_fieldset_id = '$field[id]' AND actived = '1' AND processing_field_type_id='4'
							ORDER BY position");
			$date = $db->getResultArray();
			foreach ($date['result'] as $item) {
				$date_keys[] .= $item['key'] . '--' . $item['id'] . '--' . $item['type'];
			}
		}

		$chosen_keys = substr($chosen_keys, 0, -1);

		$data['chosen_keys'] = $chosen_keys;
		$data['datetime_keys'] = $datetime_keys;
		$data['date_keys'] = $date_keys;
		$data['multilevel_keys'] = $multilevel_keys;


		break;
	case 'get_name_lastname':

		$phone =  preg_replace('#-#','',$_REQUEST['phone']);
		$db->setQuery(" SELECT name,lastname FROM saved_users_by_phone WHERE REPLACE(phone,'-','') = '$phone' ");
		$res = $db->getResultArray();
		$data = $res["result"][0];
		
		break;
	case 'get_inputs_ids':
		$setting_id = $_REQUEST['setting_id'];
		$base_id 	= $_REQUEST['base_id'];
		$keys 				= array();
		$campaignKeys 		= array();
		$campaignCheckboxes = array();
		$checkboxes 		= array();
		$db->setQuery("	SELECT id,name
						FROM processing_fieldset
						WHERE actived = '1' AND id IN (SELECT fieldset_id FROM processing_fieldset_by_setting WHERE processing_setting_id='$setting_id')");
		$fieldsets = $db->getResultArray();
		foreach ($fieldsets['result'] as $field) {
			$db->setQuery("	SELECT 		id,`key`,depth,processing_field_type_id AS 'type'

							FROM 		processing_setting_detail
							WHERE 		processing_fieldset_id = '$field[id]' AND actived = '1' AND processing_field_type_id IN (1,2,3,4,5,6,7,8,9,10)
							ORDER BY 	position");
			$selectors = $db->getResultArray();
			foreach ($selectors['result'] as $item) {
				if ($item['type'] == 10) {
					$depth = $item['depth'];
					for ($i = 1; $i <= $depth; $i++) {
						$keys[] .= $item['key'] . '_' . $i . '--' . $item['id'] . '--' . $item['type'];
					}
				}
				if ($item['type'] == 7 or $item['type'] == 6) {
					$checkboxes[] .= $item['key'];
				} else {
					$keys[] .= $item['key'] . '--' . $item['id'] . '--' . $item['type'];
				}
			}
		}

		if ($base_id != '') {
			$db->setQuery("	SELECT outgoing_campaign_request_id AS 'req_id'
							FROM outgoing_campaign_request_base
							WHERE id = '$base_id'");
			$req_id = $db->getResultArray();
			$req_id = $req_id['result'][0]['req_id'];
			$db->setQuery("	SELECT 		id,`key`,depth,field_type_id AS 'type'

							FROM 		outgoing_campaign_inputs
							WHERE 		request_id = '$req_id' AND actived = '1' AND field_type_id IN (1,2,6,7)
							ORDER BY 	position");
			$selectors = $db->getResultArray();
			foreach ($selectors['result'] as $item) {
				if ($item['type'] == 10) {
					$depth = $item['depth'];
					for ($i = 1; $i <= $depth; $i++) {
						$campaignKeys[] .= $item['key'] . '_' . $i . '--' . $item['id'] . '--' . $item['type'];
					}
				}
				if ($item['type'] == 7 or $item['type'] == 6) {
					$campaignCheckboxes[] .= $item['key'];
				} else {
					$campaignKeys[] .= $item['key'] . '--' . $item['id'] . '--' . $item['type'];
				}
			}
		}

		//$keys = substr($keys, 0, -1); 
		$data['campaign_ids'] 			= $campaignKeys;
		$data['input_ids'] 				= $keys;
		$data['checkboxes'] 			= $checkboxes;
		$data['campaignCheckboxes'] 	= $campaignCheckboxes;

		break;
	case 'get_inputs_ids_addit':
		$cat_id = $_REQUEST['cat_id'];
		$keys = array();
		$db->setQuery("	SELECT 		id,`key`,field_type_id AS 'type'

						FROM 		mepa_project_inputs
						WHERE 		cat_id = '$cat_id' AND actived = '1' AND field_type_id IN (1,2,3,4,5,6,7,8,9,10)
						ORDER BY 	position");
		$selectors = $db->getResultArray();
		foreach ($selectors['result'] as $item) {
			if ($item['type'] == 10) {
				$depth = $item['depth'];
				for ($i = 1; $i <= $depth; $i++) {
					$keys[] .= $item['key'] . '_' . $i . '--' . $item['id'] . '--' . $item['type'];
				}
			}
			if ($item['type'] == 7 or $item['type'] == 6) {
				$checkboxes[] .= $item['key'];
			} else {
				$keys[] .= $item['key'] . '--' . $item['id'] . '--' . $item['type'];
			}
		}


		//$keys = substr($keys, 0, -1); 

		$data['input_ids'] = $keys;
		$data['checkboxes'] = $checkboxes;


		break;
	case 'save_user_comunication':
		$user	             = $_SESSION['USERID'];
		$web_chat_checkbox   = $_REQUEST['web_chat_checkbox'];
		$site_chat_checkbox  = $_REQUEST['site_chat_checkbox'];
		$messanger_checkbox  = $_REQUEST['messanger_checkbox'];
		$mail_chat_checkbox  = $_REQUEST['mail_chat_checkbox'];
		$video_call_checkbox = $_REQUEST['video_call_checkbox'];

		$db->setQuery("SELECT id, user_id, chat, site, messenger, mail, cf, video 
                       FROM   users_cumunication
                       WHERE  user_id = '$user'");

		$check = $db->getNumRow();
		$req   = $db->getResultArray();
		$row   = $req[result][0];
		if ($check == 0) {
			if ($web_chat_checkbox != 1) {
				$web_chat_checkbox = 0;
			}

			if ($site_chat_checkbox != 1) {
				$site_chat_checkbox = 0;
			}

			if ($messanger_checkbox != 1) {
				$messanger_checkbox = 0;
			}

			if ($mail_chat_checkbox != 1) {
				$mail_chat_checkbox = 0;
			}

			if ($video_call_checkbox != 1) {
				$video_call_checkbox = 0;
			}

			$db->setQuery("INSERT INTO `users_cumunication` 
                                    (`user_id`, `chat`, `site`, `messenger`, `mail`, `video`) 
                              VALUES 
                                    ('$user', '$web_chat_checkbox', '$site_chat_checkbox', '$messanger_checkbox', '$mail_chat_checkbox', '$video_call_checkbox');");

			$db->execQuery();
		} else {
			if ($web_chat_checkbox != 1) {
				$web_chat_checkbox = 0;
			}

			if ($site_chat_checkbox != 1) {
				$site_chat_checkbox = 0;
			}

			if ($messanger_checkbox != 1) {
				$messanger_checkbox = 0;
			}

			if ($mail_chat_checkbox != 1) {
				$mail_chat_checkbox = 0;
			}

			if ($video_call_checkbox != 1) {
				$video_call_checkbox = 0;
			}

			$db->setQuery("UPDATE `users_cumunication`
                              SET `chat`      = '$web_chat_checkbox',
                    			  `site`     = '$site_chat_checkbox',
                    			  `messenger` = '$messanger_checkbox',
                    			  `mail`      = '$mail_chat_checkbox',
                    			  `video`     = '$video_call_checkbox'
                           WHERE  `id`        = '$row[id]'");
			$db->execQuery();
		}
		break;

	case 'check_user_comunication':
		$user = $_SESSION['USERID'];

		$db->setQuery("SELECT chat, site, messenger, mail, cf, video
	                   FROM   users_cumunication
	                   WHERE  user_id = '$user'");

		$check = $db->getNumRow();
		$req   = $db->getResultArray();
		$row   = $req[result][0];
		if ($check == 0) {
			$data = array(
				'web_chat_checkbox'   => 0,
				'site_chat_checkbox' => 0,
				'messanger_checkbox'  => 0,
				'mail_chat_checkbox'  => 0,
				'video_call_checkbox' => 0
			);
		} else {
			$data = array(
				'web_chat_checkbox'   => $row[chat],
				'site_chat_checkbox' => $row[site],
				'messanger_checkbox'  => $row[messenger],
				'mail_chat_checkbox'  => $row[mail],
				'video_call_checkbox' => $row[video]
			);
		}
		break;
	case 'load_dnd_data':

		$user_id = $_SESSION["USERID"];
		$get_options = $_REQUEST["getOptions"];

		$options = $get_options == "true" ? get_activitie_options() : "";
		$data = array('options' => $options);

		break;
	case 'focusin':
		$id = $_REQUEST["id"];
		$db->setQuery("UPDATE chat SET writes_op = 1 WHERE id = '$id'");
		$db->execQuery();
		break;
	case 'get_client_conversation_count':
		$user = $_SESSION['USERID'];
		// 	    $db->setQuery("SELECT   queries.id, 
		// 					            queries_history.id AS `queries_id`
		//                       FROM      queries
		//                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user'
		//                       WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
		// 	    $res = $db->getResultArray();
		// 	    foreach ($res[result] AS $req){
		// 	        $db->setQuery("INSERT INTO `queries_history`
		//                     	              (`user_id`, `datetime`, `queries_id`, `status`, `actived`)
		//                     	        VALUES
		//                     	              ('$user', NOW(), '$req[id]', 1, 1)");
		// 	        $db->execQuery();
		// 	    }

		$data = array('count' => 0);

		break;
	case 'focusout':
		$id = $_REQUEST["id"];
		$db->setQuery("UPDATE chat SET writes_op = 0 WHERE id = '$id'");
		$db->execQuery();
		break;
	case 'seen':
		$id = $_REQUEST["id"];
		$db->setQuery("UPDATE chat SET seen_op = 1 WHERE id = '$id'");
		$db->execQuery();
		break;
	case 'get_resp_user':

		$extention = $_REQUEST['extention'];

		$user_name = mysql_fetch_array(mysql_query("SELECT `id`, `name`
                                                     FROM   crystal_users
                                                     WHERE  last_extension = '$extention'
	                                                 AND    logged = 1"));
		$data = array('id' => $user_name[id], 'name' => $user_name[name]);
		break;

	case 'get_edit_page':

		$check_id = $_REQUEST["id"];
		$user_id = $_SESSION["USERID"];
		if ($check_id == '') {
			$check_id = 0;
		}

		$db->setQuery(" SELECT IF(incomming_call.source_id = 1 AND users.group_id != 31,asterisk_call_log.user_id,  $user_id) AS `user_id` 
					    FROM incomming_call 
						LEFT JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id 
						LEFT JOIN users ON users.id = $user_id
						WHERE incomming_call.id = $check_id ");
		$resulted = $db->getResultArray();
		$permission_result = $resulted["result"];
		//  && $user_id  != 105 && $user_id  != 104  && $user_id  != 132 && $user_id  != 109
		//  
		// 
		//  
		if (COUNT($permission_result) != 1 && $check_id != 0) {
			$error .= " no result has been found of user_id on this call ";
			break;
		} elseif ($permission_result[0]["user_id"] != $user_id && $check_id != 0 && $user_id  != 145 && $user_id  != 130 && $user_id  != 139 && $user_id  != 137 && $user_id  != 140 && $user_id  != 105 && $user_id  != 104  && $user_id  != 132 && $user_id  != 109) {
			$error .= " თქვენ იუზერს არ აქვს ამ ზარზე წვდომა ";
			//$error .= " თქვენ იუზერს არ აქვს ამ ზარზე წვდომა " . $user_id . " != " . $permission_result[0]["user_id"] ;
			break;
		} else {

			$imby 	= $_REQUEST['imby'];
			$source2 = $_REQUEST['source'];
			$db->setQuery("	SELECT call_type_id
							FROM incomming_call
							LEFT JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
							WHERE incomming_call.id = '$incom_id'");
			$call_type = $db->getResultArray();
			$call_type = $call_type['result'][0]['call_type_id'];
			$call = '';
			if ($call_type == 5) {
				$call = "autodialer";
			}
			//die("tt:" . $call);

			$db->setQuery("	SELECT 	chat_id,
									video_call_id
							FROM 	incomming_call
							WHERE 	id = '$incom_id'");
			$chat_is = $db->getResultArray();

			if ($chat_is['result'][0]['chat_id'] != '' and !isset($_REQUEST['chat_id'])) {
				$source = 'chat';
				$chat_id = $chat_is['result'][0]['chat_id'];
			} else {
				$source = 'chat';
			}

			if ($source2 == 'video') {
				$source = 'video';
				$chat_id = '';
				$chat_id2 = $_REQUEST['chat_id'];

				$db->setQuery("UPDATE video_calls SET `user_id` = '$user_id' WHERE id = '$chat_id2'");
				$db->execQuery();

				$db->setQuery("SELECT id FROM incomming_call WHERE video_call_id = '$chat_id2'");
				$incom_id = $db->getResultArray();
				$incom_id = $incom_id['result'][0]['id'];
			}

			if ($source == 'chat') {
				if($_REQUEST['source'] == 'mail'){
					$db->setQuery("UPDATE mail_live SET mail_status_id = 2
								WHERE mail_status_id = 1 AND id = '$chat_id'");
					$db->execQuery();
				}
				$db->setQuery("	SELECT 	last_user_id
								FROM 	chat
								WHERE 	id = '$chat_id'");
				$respUser = $db->getResultArray();
				$respUser = $respUser['result'][0]['last_user_id'];

				$db->setQuery("	SELECT 	allowed_groups_to_view_chat.id
								FROM 	users
								JOIN	`allowed_groups_to_view_chat` ON `allowed_groups_to_view_chat`.group_id = users.group_id
								WHERE 	users.id = '$user_id'");
				$check_group = $db->getResultArray();
				
				if ($respUser == '' or $respUser == $user_id or $user_id == 1 or $respUser == 0 or $user_id == 101 or $user_id == 145 or $user_id == 102 or $user_id  == 130 or $user_id  == 139 or $user_id  ==137 or $user_id  == 140 or $check_group['count'] > 0) {
					$db->setQuery("SELECT video_calls.peer_id FROM incomming_call LEFT JOIN video_calls ON video_calls.id = incomming_call.video_call_id WHERE incomming_call.id = '$incom_id'");
					$peer_id = $db->getResultArray();
					$peer_id = $peer_id['result'][0]['peer_id'];
					if ($peer_id == '') {
						$peer_id = 0;
					}
					if($_REQUEST['source'] == 'chat'){
						$db->setQuery("	UPDATE 	chat
										SET		last_user_id = '$user_id'
										WHERE 	id = '$chat_id' AND (ISNULL(last_user_id) OR last_user_id='' OR last_user_id = 0)");
						$db->execQuery();
					}
					$page		= GetPage(Getincomming($incom_id, $chat_id), '', $imby, '', '', getOptionals($_REQUEST['source'], $chat_id), $call);

					$data		= array('page'	=> $page, 'peer_id' => $peer_id);
				} else {
					if($user_id != 105 &&  $user_id != 104 && $user_id != 132 && $user_id != 109 )
						$data['access'] = 'NO';
				}
			} 
			else {
				
				$page		= GetPage(Getincomming($incom_id, $chat_id), '', $imby, '', '', getOptionals($_REQUEST['source'], $chat_id), $call);

				$data		= array('page'	=> $page);
			}
		}
		break;
	case 'chat_on_off':
		$_SESSION['chat_off_on'] = $_REQUEST['value'];
		$user	= $_SESSION['USERID'];
		if ($_REQUEST['value'] == 2) {
			mysql_query("UPDATE `crystal_users` SET `online`='1' WHERE (`id`='$user');");
		} else {
			mysql_query("UPDATE `crystal_users` SET `online`='0' WHERE (`id`='$user');");
		}

		break;
	case 'cancel_dialog':
			$inc_id = $_REQUEST['inc_id'];
			$db->setQuery("SELECT id,manualy_added FROM incomming_call WHERE id = $inc_id ");
			$res = $db->getResultArray();
			if(COUNT($res["result"]) < 1){
				var_dump(" id does not exists ");
			}else{
				if($res["result"][0]["manualy_added"] != 1){
					var_dump(" this case is not manually created ");
				}
				else{
					$db->setQuery("DELETE FROM incomming_call WHERE id = $inc_id ");
					$db->execQuery();
				}
			}
		break;
	case 'save_micr':
		$user	= $_SESSION['USERID'];
		$status = $_REQUEST['activeStatus'];
		$db->setQuery("SELECT * FROM `user_mute_check` WHERE user_id = $user");
		$check = $db->getNumRow();
		if ($check == 0) {
			$db->setQuery("INSERT INTO user_mute_check SET user_id='$user', `muted`='$status' ");
			$db->execQuery();
		} else {
			$db->setQuery("UPDATE `user_mute_check` SET `muted`='$status' WHERE `user_id`='$user';");
			$db->execQuery();
		}

		break;
	case 'logout':

		$c_date	= date('Y-m-d H:i:s');
		$user	= $_SESSION['USERID'];

		$rResult = mysql_fetch_array(mysql_query("SELECT  MIN(ABS(UNIX_TIMESTAMP(person_work_graphic.`end`)-UNIX_TIMESTAMP(NOW()))) AS end_date
												FROM person_work_graphic
												WHERE person_work_graphic.person_id='$user'"));

		if ($rResult['end_date'] < 15 && $rResult['end_date'] != null) {
			$data['logout']	= 1;
			$data['time']	= $rResult['end_date'];
			mysql_query("UPDATE `crystal_users` SET `online`='0' WHERE (`id`='$user');");
		} else {
			$data['logout']	= 0;
			$data['time']	= $rResult['end_date'];
		}

		break;
	case 'get_list':
		$id          =      $_REQUEST['hidden'];

		$columnCount = 		$_REQUEST['count'];
		$json_data   = 		json_decode($_REQUEST['add'], true);
		$itemPerPage = 		$json_data['pageSize'];
		$skip        = 		$json_data['skip'];
		$cols[]      =      $_REQUEST['cols'];

		$all_calls      = $_REQUEST['filter_all_calls'];
		$operator_id	= $_REQUEST['filter_operator'];
		$start        	= $_REQUEST['start'];
		$end         	= $_REQUEST['end'];
		$user_info_id 	= $_REQUEST['user_info_id'];
		$user	      	= $_SESSION['USERID'];
		$user_filt    	= '';

		if ($user_info_id == 1) {
			$user_filt = "AND incomming_call.user_id = $user";
		}

		if ($all_calls > 0) {
			$filt = " AND incomming_request_processing.int_value = '$all_calls'";
		} else {
			$filt = '';
		}

		if ($operator_id > 0) {
			$filt1 = "AND IF(ISNULL(incomming_call.asterisk_incomming_id),incomming_call.user_id,asterisk_call_log.user_id) = '$operator_id'";
		} else {
			$filt1 = '';
		}

		$filter_call_status 	= $_REQUEST['filter_call_status'];

		$filt_status = '';

		if ($filter_call_status != 'undefined' && $filter_call_status != '') {
			$filt_status = "AND (CASE
            							WHEN ISNULL(incomming_call.`user_id`) OR incomming_call.`user_id` = 0 AND IFNULL(`asterisk_call_log`.`call_status_id`,100) NOT IN(2,9,3) THEN 2
            							WHEN (NOT ISNULL(incomming_call.`user_id`) OR incomming_call.`user_id` > 0) AND IFNULL(`asterisk_call_log`.`call_status_id`,100) NOT IN(2,9,3) THEN 1
            							WHEN asterisk_call_log.call_status_id = 9 THEN 3
            							WHEN asterisk_call_log.call_status_id = 3 THEN 4
            							WHEN asterisk_call_log.call_status_id = 2 THEN 5
            					  END) IN ($filter_call_status)";
		}
		if ($permissionLVL == 50) {
			$azirui_farosana = " AND  ( (project.int_value = 71  OR project2.int_value = 71) OR (project.int_value = 3  OR project2.int_value = 3) )";
		}
		// echo $filt_status;

		$filter_call_sources = $_REQUEST['filter_call_sources'];

		if ($filter_call_sources != 'undefined' && $filter_call_sources != '') {
			$filter_source = "AND IFNULL(incomming_call.source_id, 11) IN($filter_call_sources)";
		}
		$filter_call_type = $_REQUEST['filter_call_type'];
		if ($filter_call_type != 'undefined' && $filter_call_type != '') {
			$filter_source = "AND asterisk_call_log.call_type_id IN($filter_call_type)";
		}

		$manualy_check = " AND IF(incomming_call.manualy_added = 1, IF((SELECT COUNT(*) FROM incomming_request_processing WHERE incomming_request_id = incomming_call.id) > 0, 1 = 1, 0 = 1), 1 = 1)";

		// echo $filter_source;
		// IF(asterisk_call_log.call_status_id = 9, '', incomming_call.id),

		/* $phone = $_REQUEST['phone'];

		$db->setQuery("	SELECT value

						FROM incomming_request_processing
						WHERE processing_setting_detail_id = 127 
						AND incomming_request_id = (SELECT id FROM incomming_call WHERE phone = '$phone' ORDER BY id ASC LIMIT 1)");
		$firstname = $db->getResultArray();
			

		$db->setQuery("	SELECT value

						FROM incomming_request_processing
						WHERE processing_setting_detail_id = 128 
						AND incomming_request_id = (SELECT id FROM incomming_call WHERE phone = '$phone' ORDER BY id ASC LIMIT 1)");
		$lastname = $db->getResultArray(); */
		$db->setQuery("	SELECT      IF(asterisk_call_log.call_status_id IN( 9, 12 ), '', incomming_call.id),
                                    incomming_call.date,
                                    CASE
										WHEN NOT ISNULL(incomming_call.phone) THEN incomming_call.phone
										WHEN NOT ISNULL(proc_phone.value) THEN proc_phone.value
										WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.phone
										WHEN NOT ISNULL(incomming_call.fb_chat_id)  THEN fb_chat.sender_name
										WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN proc_phone.value
										WHEN NOT ISNULL(incomming_call.viber_chat_id)  THEN viber_chat.sender_name
										WHEN NOT ISNULL(incomming_call.video_call_id) THEN incomming_call.phone
									END AS `phone`,
									CASE
										WHEN NOT ISNULL(proc_name.value)  THEN proc_name.value
										WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
										WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.name
										WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN mail.sender_name
										WHEN NOT ISNULL(incomming_call.video_call_id)  THEN IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value))
									END AS `mom_auth`,
										
									-- KOJO
									momartvaa.`name` AS `mom_type`,
									IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),GROUP_CONCAT(DISTINCT uwyeba_dat.name SEPARATOR ', '),GROUP_CONCAT(DISTINCT uwyeba_dat2.name SEPARATOR ', ')) as 'uwyeba',
									IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),project_dat.name,project_dat2.name)as 'project',
									CASE
										WHEN NOT ISNULL(incomming_call.chat_id)  THEN user_info1.name
										WHEN NOT ISNULL(incomming_call.user_id) THEN user_info1.name
										WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(IFNULL(IFNULL(user_info1.`name`,user_info.`name`), ''), '(',asterisk_extension.number, ')'),user_info1.`name`),user_info.name)
										WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN user_info1.name
										WHEN NOT ISNULL(incomming_call.video_call_id) THEN user_info1.name
									END AS `user_name`,
									CASE
										WHEN incomming_request_processing.int_value = 1 OR compaign_request_processing.int_value = 1 THEN 'გადაცემულია გასარკვევად'
										WHEN incomming_request_processing.int_value = 2 OR compaign_request_processing.int_value = 2 THEN 'გარკვევის პროცესშია'
										WHEN incomming_request_processing.int_value = 3 OR compaign_request_processing.int_value = 3 THEN 'დასრულებულია'
									END AS `status`,
                                    

									IF(`asterisk_call_log`.`talk_time`,
                                    CONCAT(TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.talk_time),'%i:%s'), ' (', TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.wait_time),'%i:%s'), ')',
									CASE
										WHEN `asterisk_call_log`.`call_status_id` IN ( 6, 7, 8, 13 ) THEN
										IF
											(
												isnull(incomming_call.`user_id`) AND asterisk_call_log.call_type_id = 5,

										IF(asterisk_call_log.call_type_id = 5,
										    CONCAT(
											'<span tt=\'autodialer\' class=\'play_audio_complate\' str=http://172.16.0.80:8000/autodialer/', CONCAT( asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span>',
											'<a target=_blank href=http://172.16.0.80:8000/autodialer/',CONCAT(asterisk_call_record.NAME, '.', asterisk_call_record.format ),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT(asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span></a>' ),
											CONCAT(
											'<span tt=\'autodialer\' class=\'play_audio_complate\' str=http://172.16.0.80:8000/autodialer/', CONCAT( asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span>',
											'<a target=_blank href=http://172.16.0.80:8000/autodialer/',CONCAT( asterisk_call_record.NAME, '.', asterisk_call_record.format ),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT( asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span></a>' )
											),
											IF(incomming_call.transfer = 1,

										CONCAT('<button class=\'download5\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '><span style=\"vertical-align: -1px; font-size: 11px\">გადართული  </span></button>' ),

										CONCAT('<span class=\'play_audio_complate\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span>',
											'<a target=_blank href=http://172.16.0.80:8000/',CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span></a>' )

										))
                                        ELSE CONCAT(
											'<span style=\'filter: grayscale(100%);\' class=play_audio_complate_disabled>
											</span>',
											'<span style=\'filter: grayscale(100%);\' class=download_audio_complate_disabled>
											</span>' )
                                    END
									),
                                   CASE
                                      WHEN NOT ISNULL(incomming_call.video_call_id) THEN
                                        IF(video_calls.status_id IN (4, 5),
                                            CONCAT(
												TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, video_calls.answer_datetime, video_calls.end_datetime)), '%i:%s'),' (', TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, video_calls.start_datetime, video_calls.answer_datetime)), '%i:%s'), ') ',
												'<span tt=\'autodialer\' class=\'play_audio_complate\' str=https://video.mepa.gov.ge/server/uploads/', CONCAT( video_calls.peer_id, '.mp3'), '>
                                            </span>',
                                            '<a target=_blank href=https://video.mepa.gov.ge/server/uploads/',CONCAT( video_calls.peer_id, '.mp3'),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT( video_calls.peer_id, '.mp3'), '>
                                            </span></a>' ),

                                            CONCAT(
												TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, video_calls.answer_datetime, video_calls.end_datetime)), '%i:%s'),' (', TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, video_calls.start_datetime, video_calls.answer_datetime)), '%i:%s'), ') ',
                                            '<span tt=\'autodialer\' class=\'play_audio_complate\' str=https://video.mepa.gov.ge/server/uploads/', CONCAT( video_calls.peer_id, '.mp3'), '>
                                            </span>',
                                            '<a target=_blank href=https://video.mepa.gov.ge/server/uploads/',CONCAT( video_calls.peer_id, '.mp3'),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT( video_calls.peer_id, '.mp3'), '>
                                            </span></a>' )
                                        )
                                   END ) AS `file`,

									-- CASE
									-- 	WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                    --         THEN concat('<span title=\'მესენჯერი\' class=\'inc_fb_status incomplete\'>დაუმუშავებელი</span>')
                                    --     WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                    --         THEN concat('<span title=\'მესენჯერი\' class=\'inc_fb_status complete\'>დამუშავებული</span>')
									-- END AS `status`


                                    CASE
									-- CHAT
                                        WHEN (NOT ISNULL(incomming_call.`chat_id`) AND NOT ISNULL(incomming_call.`user_id`) AND chat.status != 5)
                                            THEN concat(_utf8 '<button class=\'download7\' ><img src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დამუშავებელი</span></button>')
                                        -- manually created case
										WHEN (NOT ISNULL(incomming_call.`manualy_added`) )
											THEN concat(_utf8 '<button class=\'download7\' style=\"background:#fe7500;\" ><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px;background:#fe7500;\">მანუალური</span></button>')
                                        
										WHEN (NOT ISNULL(incomming_call.`chat_id`) AND ISNULL(incomming_call.`user_id`) AND chat.status != 5)
                                            THEN concat(_utf8 '<button class=\'download8\' ><img src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>')
										WHEN (NOT ISNULL(incomming_call.`chat_id`) AND chat.status = 5)
                                            THEN concat(_utf8 '<button style=\"background-color:blue;\" class=\'download7\' ><img src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">არასამუშ.ჩატი</span></button>')
                                        -- MAIL
                                        WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND ISNULL(incomming_call.`user_id`))
                                            THEN concat(_utf8 '<button class=\'download15\' ><img src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND NOT ISNULL(incomming_call.`user_id`))
                                            THEN concat(_utf8 '<button class=\'download15\' ><img src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დამუშავებელი</span></button>')

                                        -- FB CHAT 
                                        WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download11\' ><img src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download11\' ><img src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დამუშავებელი</span></button>')

										-- FB COMMENT 
										WHEN (NOT ISNULL(incomming_call.`fb_comment_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download12\' ><img src=\"media/images/icons/comunication/fb_comments.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`fb_comment_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download12\' ><img src=\"media/images/icons/comunication/fb_comments.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 11px\">დამუშავებელი</span></button>')
                                        -- VIBER
                                        WHEN (NOT ISNULL(incomming_call.`viber_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                            THEN concat(_utf8 '<button class=\'download10\' ><img src=\"media/images/icons/comunication/Viber.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG;vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>')
                                        WHEN (NOT ISNULL(incomming_call.`viber_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                            THEN concat(_utf8 '<button class=\'download10\' ><img src=\"media/images/icons/comunication/Viber.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG;vertical-align: -1px; font-size: 11px\">დამუშავებელი</span></button>')
                                        
										-- VIDEO CALL
										WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND ISNULL(incomming_call.`user_id`))
                                            THEN concat(_utf8 '<button class=\'download_videocall\' ><img src=\"media/images/icons/comunication/VIDEO_OFF_OFF.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG;vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>')
										WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND NOT ISNULL(incomming_call.`user_id`))
												THEN concat(_utf8 '<button class=\'download_videocall\' ><img src=\"media/images/icons/comunication/VIDEO_OFF_OFF.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG;vertical-align: -1px; font-size: 11px\">დამუშავებელი</span></button>')
										
										WHEN `asterisk_call_log`.`call_status_id` = 9
												THEN concat(_utf8 '<button class=\'download2\' ><img src=\"media/images/icons/comunication/busy_out.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">უპასუხო</span></button>')
										WHEN `asterisk_call_log`.`call_status_id` = 2
										
											THEN concat(_utf8 '<button style=\'background-color: #ff9800;\' class=\'download2\' ><img src=\"media/images/icons/comunication/ringing.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">რიგშია</span></button>')
                                        WHEN `asterisk_call_log`.`call_status_id` = 3
                                            THEN concat(_utf8 '<button style=\'background-color: #ff9800;\' class=\'download2\' ><img src=\"media/images/icons/comunication/ringing.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">საუბრობს</span></button>')
                                        WHEN `asterisk_call_log`.`call_status_id` = 11
                                            THEN concat('<button style=\'background-color: #ff9800;\' class=\'download2\'><span style=\"vertical-align: -1px; font-size: 11px\">გადამისამართება</span></button>')
										WHEN `asterisk_call_log`.`call_status_id` = 12
                                            THEN concat('<button style=\'background-color: #f00000;font-family: BPG;\' class=\'download2\'><span style=\"vertical-align: -1px; font-size: 11px\">უპ.Autodialer</span></button>')
										WHEN `asterisk_call_log`.`call_status_id` = 13
                                            THEN concat('<button style=\'background-color: #1df2d6;font-family: BPG;\' class=\'download2\'><span style=\"vertical-align: -1px; font-size: 11px\">ნაპ.Autodialer</span></button>')
                                        WHEN `asterisk_call_log`.`call_status_id` IN(6,7,8)
                                            THEN IF (isnull(`incomming_call`.`user_id`),
											CONCAT('<button class=\'download4\'><img src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დაუმუშავებელი</span></button>'),
											IF(`incomming_call`.transfer=1, 
											CONCAT('<button class=\'download5\'><span style=\"vertical-align: -1px; font-size: 11px\">გადართული</span></button>'),
											CONCAT('<button class=\'download\'><img src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">დამუშავებული</span></button>')))
                                        ELSE '<!-- <button class=\"download15\"\><span style=\"vertical-align: 1px; font-size: 13px;\"></span></button> -->'
                                    END AS `status`,
									CASE 
										WHEN genderID.`int_value` = 1 THEN 'მდედრობითი'
										WHEN genderID.`int_value` = 2 THEN 'მამრობითი'
									END AS `gender`

                        FROM        incomming_call					
                        LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
						LEFT JOIN	asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
						LEFT JOIN	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
						LEFT JOIN   asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
                        LEFT JOIN   user_info ON user_info.user_id = asterisk_call_log.user_id
                        LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
						
                        LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                        LEFT JOIN   info_category ON info_category.id = incomming_call.cat_1
						LEFT JOIN   `chat` ON `chat`.id = incomming_call.chat_id
						LEFT JOIN   fb_chat ON fb_chat.id = incomming_call.fb_chat_id
						LEFT JOIN   mail ON mail.id = incomming_call.mail_chat_id
						LEFT JOIN   viber_chat ON viber_chat.id = incomming_call.viber_chat_id
						LEFT JOIN   video_calls ON video_calls.id = incomming_call.video_call_id
						LEFT JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 136
						LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = incomming_call.id AND proc_name.processing_setting_detail_id = 127
						LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = incomming_call.id AND proc_lastname.processing_setting_detail_id = 128
						LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = incomming_call.id AND proc_phone.processing_setting_detail_id = 124
						LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = incomming_call.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
						LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = incomming_call.id AND project.processing_setting_detail_id = 0 AND project.value = 2

						LEFT JOIN 	incomming_request_processing AS `momartvebi` ON momartvebi.incomming_request_id = incomming_call.id AND momartvebi.processing_setting_detail_id = 0 AND momartvebi.value = 4

						LEFT JOIN   compaign_request_processing ON compaign_request_processing.base_id = outgoing_campaign_request_base.id AND compaign_request_processing.processing_setting_detail_id = 136
						LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
						LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
						LEFT JOIN   compaign_request_processing AS `proc_phone2` ON proc_phone2.base_id = outgoing_campaign_request_base.id AND proc_phone2.processing_setting_detail_id = 124
						LEFT JOIN 	compaign_request_processing AS `uwyeba2` ON uwyeba2.base_id = outgoing_campaign_request_base.id AND uwyeba2.processing_setting_detail_id = 0 AND uwyeba2.value = 1
						LEFT JOIN 	compaign_request_processing AS `project2` ON project2.base_id = outgoing_campaign_request_base.id AND project2.processing_setting_detail_id = 0 AND project2.value = 2
						LEFT JOIN 	info_category AS `uwyeba_dat` ON uwyeba_dat.id = uwyeba.int_value
						LEFT JOIN 	info_category AS `project_dat` ON project_dat.id = project.int_value

						LEFT JOIN 	info_category AS `uwyeba_dat2` ON uwyeba_dat2.id = uwyeba2.int_value
						LEFT JOIN 	info_category AS `project_dat2` ON project_dat2.id = project2.int_value

						LEFT JOIN incomming_request_processing AS genderID ON genderID.processing_setting_detail_id = 130 AND incomming_call.id = genderID.incomming_request_id
                
						LEFT JOIN mepa_project_types AS momartvaa ON momartvaa.id = momartvebi.int_value 

                        WHERE       (asterisk_call_log.call_type_id = 1 OR asterisk_call_log.call_type_id = 5 OR ISNULL(asterisk_call_log.call_type_id)) AND NOT ISNULL(incomming_call.source_id) AND (incomming_call.noted!=2 OR incomming_call.noted IS NULL ) AND DATE(incomming_call.date) BETWEEN '$start' AND '$end' $filt $filt1 $filt_status $filter_source $user_filt $azirui_farosana $manualy_check
						GROUP BY  	`incomming_call`.`id`
						ORDER BY 	`incomming_call`.`id` DESC");



		$result = $db->getKendoList($columnCount, $cols);
		$data = $result;


		break;
	case 'get_list_history':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$start_check  = $_REQUEST['start_check'];
		$end_check    = $_REQUEST['end_check'];
		$phone        = $_REQUEST['phone'];
		$s_u_user_id  = $_REQUEST['s_u_user_id'];

		$filt = '';
		if ($phone == '' && $s_u_user_id == '') {
			$filt = "AND inc.client_user_id = 'fddhgdhgd'";
		}

		if ($phone != '' && $s_u_user_id != '') {
			$filt = "AND inc.client_user_id = '$s_u_user_id' OR inc.phone = '$phone'";
		}

		if ($phone != '' && $s_u_user_id == '') {
			$filt = "AND inc.phone = '$phone'";
		}

		if ($phone == '' && $s_u_user_id != '') {
			$filt = "AND inc.client_user_id = '$s_u_user_id'";
		}

		$db->setQuery(" SELECT    `inc`.`id` AS `task_id`,
                        		  `inc`.`date` AS `date`,
                        		   IF(`inc`.`phone` = '2004', `inc`.`ipaddres`, `inc`.`phone`) AS `phone`,
                        	       user_info.`name`,
                        		  `inc`.`call_content`,
                                   IF(inc.inc_status_id=0,'დაუმუშავებელი',inc_status.`name`) AS `status`
                        FROM      `incomming_call` AS `inc`
                        LEFT JOIN  user_info ON `inc`.`user_id` = user_info.user_id
                        LEFT JOIN  inc_status  ON inc.inc_status_id = inc_status.id
                        WHERE      DATE(inc.date) BETWEEN '$start_check' AND '$end_check' $filt");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_filter_details':
		$filter_name	= $_REQUEST['filterName'];

		$db->setQuery("	SELECT 	column_name, id
						FROM 	directory_column_titles 
						WHERE	title_geo = '$filter_name'
						ORDER BY id 
						LIMIT 1 ");

		$column_name = $db->getResultArray();
		$column = $column_name[result][0]['column_name'];
		$filterID = $column;

		$db->setQuery("	SELECT 	COUNT(id) AS `count`,  $column AS `name`
						FROM	directory__projects
						GROUP BY $column ");

		$data = $db->getResultArray();


		break;
	case 'get_list_info_project':

		$id          =      $_REQUEST['hidden'];
		$columnCount = 		$_REQUEST['count'];
		$json_data   = 		json_decode($_REQUEST['add'], true);
		$itemPerPage = 		$json_data['pageSize'];
		$skip        = 		$json_data['skip'];
		$cols[]      =      $_REQUEST['cols'];

		$db->setQuery("SELECT   incomming_call_info.id,
                			    'პროქტები' AS `name`,
								directory__projects.`status`,
                				CONCAT('პროექტი: ', directory__projects.project, ' კატეგორია: ', directory__projects.category, ' ხედი: ', directory__projects.`view`, ' ნომერი: ', directory__projects.number, ' სართული: ', directory__projects.floor, ' სექტორი: ', directory__projects.sector, ' საკადასტრო კოდი: ', directory__projects.cadr_code, ' საზაფხულო ფართი: ', directory__projects.saz_far, ' საცხოვრებელი ფართი: ', directory__projects.sacx_far, ' ფასი: ', directory__projects.price, ' აივანი: ', directory__projects.balcony, ' საერთო ფართი: ', directory__projects.saerto_far, ' სრული ფასი: ', directory__projects.full_price, ' განვადების ფასი: ', directory__projects.ganv_price, ' სტანდარტული წინასწარ: ', directory__projects.stand_winaswar, ' სტატუსი: ', directory__projects.`status`) AS `info`
                        FROM   `incomming_call_info`
                        JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
                        JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
                        WHERE  `incomming_call_info`.`actived` = 1 AND incomming_call.id = $id");

		$result = $db->getKendoList($columnCount, $cols);
		$data = $result;

		break;
	case 'get_list_project_filter':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		$filterArr	  = $_REQUEST['FilterArr'];
		$filterArrD	  = json_decode($filterArr);
		$id           = $_REQUEST['id'];

		$query = "	SELECT  `directory__projects`.`id`,
							`directory__projects`.`id`,
							directory__projects.status, 
							directory__projects.project, 
							directory__projects.category,
							directory__projects.`view`,  
							directory__projects.sector,
							directory__projects.floor, 
							directory__projects.number, 
							directory__projects.sleeping_room, 
							directory__projects.sacx_far, 
							directory__projects.saz_far, 
							directory__projects.balcony,
							directory__projects.saerto_far,
							directory__projects.full_price,
							directory__projects.ganv_price, 
							directory__projects.price, 
							directory__projects.stand_winaswar, 
							directory__projects.cadr_code 
							FROM 	`directory__projects`
							WHERE ";

		$count = count($filterArrD, COUNT_NORMAL);

		foreach ($filterArrD as $key => $d) {
			// $query = $d->columnName." = '".$d->filterValue."' ";
			$cart[] = array('Column' => $d->columnName, 'Value' => $d->filterValue);

			$QueryArray = [];
			foreach ($cart as $innerArray) {
				$key = array_search($innerArray['Column'], array_column($QueryArray, 'Column'));

				if ($key === false) {
					array_push($QueryArray, $innerArray);
				} else {
					$QueryArray[$key]['Value'] .= "', '" . $innerArray['Value'];
				}
			}
		}

		foreach ($QueryArray as $q => $d) {
			$columns = $d["Column"];
			$values = $d['Value'];
			if ($q == 0) {
				$query .= " `directory__projects`.`" . $columns . "` IN ('" . $values . "')";
			} else {
				$query .= " AND `directory__projects`.`" . $columns . "` IN ('" . $values . "')";
			}
		}




		$db->setQuery($query);

		$data = $db->getList(18, $hidden, 1);

		break;
	case 'get_list_project':
		$id          =      $_REQUEST['id'];
		$columnCount = 		$_REQUEST['count'];
		$json_data   = 		json_decode($_REQUEST['add'], true);
		$itemPerPage = 		$json_data['pageSize'];
		$skip        = 		$json_data['skip'];
		$cols[]      =      $_REQUEST['cols'];

		// $filterArr	  = $_REQUEST['FilterArr'];
		// $filterArrD	  = json_decode($filterArr);

		$incomming_id = $_REQUEST['incomming_id'];


		$query = " SELECT  `directory__projects`.`id`,
							directory__projects.status, 
							directory__projects.project, 
							directory__projects.category,
							directory__projects.`view`,  
							directory__projects.sector,
							directory__projects.floor, 
							directory__projects.number, 
							directory__projects.sleeping_room, 
							directory__projects.sacx_far, 
							directory__projects.saz_far, 
							directory__projects.balcony,
							directory__projects.saerto_far,
							directory__projects.price,
							directory__projects.ganv_price, 
							directory__projects.full_price,
							directory__projects.stand_winaswar, 
							directory__projects.cadr_code,
							directory__projects.phone,
							directory__projects.address
					FROM   `incomming_call_info`
					JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
					JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
					WHERE  `incomming_call_info`.`actived` = 1 AND incomming_call.id = '$incomming_id'
					UNION ALL
					SELECT 			
							`directory__projects`.`id`,
							directory__projects.status, 
							directory__projects.project, 
							directory__projects.category,
							directory__projects.`view`,  
							directory__projects.sector,
							directory__projects.floor, 
							directory__projects.number, 
							directory__projects.sleeping_room, 
							directory__projects.sacx_far, 
							directory__projects.saz_far, 
							directory__projects.balcony,
							directory__projects.saerto_far,
							directory__projects.price,
							directory__projects.ganv_price, 
							directory__projects.full_price,
							directory__projects.stand_winaswar, 
							directory__projects.cadr_code,
							directory__projects.phone,
							directory__projects.address
					FROM 	`directory__projects`
					WHERE `directory__projects`.`id` NOT IN (
					SELECT  `directory__projects`.`id` 
					FROM   `incomming_call_info`
					JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
					JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
					WHERE  incomming_call.id = '$incomming_id') ";

		// 	$count = count($filterArrD, COUNT_NORMAL);

		// 	if($count > 0){
		// 		foreach($filterArrD as $key => $d){
		// 			// $query = $d->columnName." = '".$d->filterValue."' ";
		// 			$cart[] = array('Column' => $d->columnName, 'Value' => $d->filterValue);

		// 			$QueryArray = [];
		// 			foreach ($cart as $innerArray) {
		// 				$key = array_search($innerArray['Column'], array_column($QueryArray, 'Column'));

		// 				if ($key === false) {
		// 					array_push($QueryArray, $innerArray);
		// 				} else { 
		// 					$QueryArray[$key]['Value'] .= "', '". $innerArray['Value'];
		// 				}
		// 			}
		// 		}

		// 		foreach($QueryArray as $q => $d){
		// 			$columns = $d["Column"];
		// 			$values = $d['Value'];
		// 			if($q == 0){
		// 				$query .= " `directory__projects`.`".$columns."` IN ('".$values."')";
		// 			}else{
		// 				$query .= " AND `directory__projects`.`".$columns."` IN ('".$values."')";
		// 			}
		// 		}
		// }else{
		// 	$query .= "`directory__projects`.`actived` = 1";
		// }

		$db->setQuery($query);

		$db->execQuery();


		$result = $db->getKendoList($columnCount, $cols);
		$data = $result;

		break;
	case 'get_project_address':
		$id      = $_REQUEST['id'];
		$db->setQuery("SELECT 	`directory__projects`.`address`
						FROM 	`directory__projects`
						WHERE	`directory__projects`.`id` = $id");
		$result = $db->getResultArray();
		$data = array('address' => $result['result'][0]['address']);
		break;
	case 'get_project_addresses':

		$inc_id		= $_REQUEST['inc_id'];
		$db->setQuery("	SELECT  `directory__projects`.`address`
					FROM   `incomming_call_info`
					JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
					JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
					WHERE	`incomming_call`.`id` = $inc_id
					GROUP BY  `directory__projects`.`address`");
		$res = $db->getResultArray();
		$list = '';

		if (count($res['result']) == 0) {
			$list = "<span class='empty-list'>სია ცარიელია</span>";
		}

		foreach ($res['result'] as $a) {
			if (!empty($a['address'])) {
				$list .= "<li>" . $a['address'] . "</li>";
			}
		}

		$data = array('addresses' => $list);
		break;
	case 'add_project':
		$id      = $_REQUEST['id'];
		//$letters = json_decode('[' . $_REQUEST['lt'] . ']');
		$letters = explode(",", $_REQUEST['lt']);

		//$db->setQuery("DELETE FROM incomming_call_info WHERE incomming_call_id = $id");
		//$db->execQuery();

		$user_id = $_SESSION['USERID'];
		foreach ($letters as $pr_id) {
			$db->setQuery("INSERT INTO `incomming_call_info`
                                      (`user_id`, `datetime`, `incomming_call_id`, `directory_tables_id`, `directory_row_id`, `actived`) 
                                VALUES 
                                      ($user_id, NOW(), $id, 3, '$pr_id', 1);");
			$db->execQuery();
		}

		break;
	case 'get_list_client_order':
		$count                         = $_REQUEST['count'];
		$hidden                        = $_REQUEST['hidden'];
		$history_user_id               = $_REQUEST['history_user_id'];
		$incomming_call_id             = $_REQUEST['incomming_call_id'];
		$client_history_filter_date    = $_REQUEST['client_history_filter_date'];
		$client_history_filter_activie = $_REQUEST['client_history_filter_activie'];

		$db->setQuery("SELECT  id,
                			   production_id,
                			   title,
                			   create_date,
                			   prom_color,
                			   '' AS `moqmedeba`,
                			   block_date,
                			   '' AS `ar_vici`,
                			   '' AS `ar_vici1`,
                			   '' AS `ar_vici1`
                        FROM  `my_order`
                        WHERE  my_user_id = $history_user_id");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_list_client_transaction':
		$count                                  = $_REQUEST['count'];
		$hidden                                 = $_REQUEST['hidden'];

		$history_user_id                        = $_REQUEST['history_user_id'];
		$incomming_call_id                      = $_REQUEST['incomming_call_id'];
		$client_history_filter_site             = $_REQUEST['client_history_filter_site'];
		$client_history_filter_group            = $_REQUEST['client_history_filter_group'];
		$client_history_filter_transaction_type = $_REQUEST['client_history_filter_transaction_type'];

		$db->setQuery("SELECT  id,
                			   site_name,
                              `comment`,
                			  `name`,
                			  `product_id`,
                			  `money_before`,
                			  `money_after`,
                			  `quantity`,
                			  `expense`,
                			  `insert_date`
                        FROM  `my_transaction`
                        WHERE  my_user_id = $history_user_id");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_user_log':
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		$incomming_call_id = $_REQUEST['incomming_call_id'];
		$db->setQuery("SELECT `logs`.id,
                			  `logs`.date,
                			   IF(`logs`.`event`=1,'რედაქტირება','დამატება'),
                			   user_info.`name`,
                			    CASE
                					WHEN collumn = 'phone' THEN	'ტელეფონი'
                					WHEN collumn = 'cat_1' THEN 'კატეგორია'
                					WHEN collumn = 'cat_1_1' THEN 'ქვე კატეგორია1'
                					WHEN collumn = 'cat_1_1_1' THEN 'ქვე კატეგორია2'
                					WHEN collumn = 'call_content' THEN 'საუბრის შინაარსი'
                					WHEN collumn = 'inc_status' THEN 'რეაგირება'
                					WHEN collumn = 'client_user_id' THEN 'User ID'
                					WHEN collumn = 'client_pid' THEN 'საიდ. კოდი / პ.ნ'
                					WHEN collumn = 'client_name' THEN 'სახელი,გვარი / დასახელება'
                					WHEN collumn = 'client_mail' THEN 'ელ. ფოსტა'
                					WHEN collumn = 'client_satus' THEN 'სტატუსი'
                					WHEN collumn = 'client_sex' THEN 'სქესი'
									WHEN collumn = 'source' THEN 'ინფორმაციის წყარო'
                				END AS `collumt`,
                			   `logs`.new_value,
                			   `logs`.old_value
                        FROM   `logs`
                        JOIN   user_info ON user_info.user_id = `logs`.user_id
                        WHERE  `table` = 'incomming_call' AND row_id = '$incomming_call_id' AND new_value != '' ");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_list_news':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$db->setQuery(" SELECT action.id,
                    	       action.start_date,
                    	       action.end_date,
                    	       action.`name`,
                              (SELECT GROUP_CONCAT(my_web_site.`name`) 
                               FROM  `action_site`
                               JOIN   my_web_site ON action_site.site_id = my_web_site.id
                               WHERE  action_site.action_id = action.id) AS `site`,
                    	       action.content
            	        FROM   action
            	        WHERE  action.actived=1 AND action.end_date >= DATE(CURDATE())");

		$data = $db->getList($count, $hidden, 1);

		break;
	case 'get_list_crm':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$start_crm  = $_REQUEST['start_crm'];
		$end_crm    = $_REQUEST['end_crm'];
		$phone      = $_REQUEST['phone'];
		$pin        = $_REQUEST['pin'];

		$db->setQuery("SELECT 	   ar.id,
                                   ard.set_time,
                                   arb.number_8,
                                   arb.number_2,
            					   arb.number_3,
            					   arb.phone_number,
            					   arb.number_5,
                                   ar.comment_main,
                                   CONCAT(IFNULL(tr.`name`,''),IF(ISNULL(tr.`name`),'','/'),IFNULL(add_page_tree.`name`,'')) AS `status`
                        FROM 	   autocall_request ar 
                        left JOIN  autocall_request_details ard ON ar.id = ard.request_id
                        LEFT JOIN  autocall_request_base arb ON arb.request_id = ard.id
                        LEFT JOIN  add_page_tree ON arb.tree_id = add_page_tree.id
                        LEFT JOIN  add_page_tree AS `tr` ON add_page_tree.parent_id = tr.id
                        WHERE      ar.call_type_id = 4 AND ar.actived=1 AND ard.actived = 1 
						AND       ((arb.number_2 = '$pin' AND arb.number_2 != '')  OR (arb.phone_number = '$phone' AND arb.phone_number != ''))
                        AND        DATE(ard.set_time) BETWEEN '$start_crm' AND '$end_crm'");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_list_quest':
		$count = 		$_REQUEST['count'];
		$hidden = 		$_REQUEST['hidden'];



		$db->setQuery(" SELECT 	queries.id,
                                queries.datetime,
                                directory__projects.`project`,
								queries.`quest`,
                                queries.`answer`
					    FROM 	queries
                        LEFT JOIN user_info ON user_info.user_id = queries.user_id
						LEFT JOIN directory__projects ON directory__projects.id = queries.project_id
					    WHERE 	queries.actived=1 ");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_list_mail':
		$count             = $_REQUEST['count'];
		$hidden            = $_REQUEST['hidden'];
		$incomming_call_id = $_REQUEST['incomming_id'];

		$db->setQuery(" SELECT   id,`date`,
							CONCAT( 'to: ', `address`, 
							(IF(bcc_address !='', CONCAT(',<br>  bcc: ', bcc_address),'')),
							(IF(cc_address !='',CONCAT(',<br>  cc: ', cc_address),'')) ) AS address,
							`body`, 
							CASE 
									WHEN status = 3 THEN 'გასაგზავნია' 
									WHEN status = 2 THEN 'გაგზავნილია' 
									WHEN status = 4 THEN 'დაფორვარდებულია' 
									END AS `status` 
							FROM   `sent_mail`
							WHERE   incomming_call_id = '$incomming_call_id' AND `status` != 1");

		$data = $db->getList($count, $hidden);
		break;
	case 'save_incomming':
		$incom_id     = $_REQUEST['id'];
		$id           = $_REQUEST['hidde_incomming_call_id'];
		$user		  = $_SESSION['USERID'];
		$hidden_user  = $_REQUEST['hidden_user'];

		if ($id == '') {
			Addincomming($incom_id);
		} else {
			Saveincomming($incom_id);
		}

		break;
	case "set_user":
		$chat_ID = $_REQUEST['id'];
		$db->setQuery("	SELECT	last_user_id AS 'user'
							FROM 	chat
							WHERE 	id ='$chat_ID'");
		$user_id = $db->getResultArray();
		$user_id = $user_id['result'][0]['user'];

		if ($user_id == '' OR $user_id == $_SESSION['USERID'] OR $user_id == 0) {
			$db->setQuery("		SELECT 	id, status
								FROM 	chat
								WHERE 	last_user_id = '$_SESSION[USERID]' AND status < 3");
			$chats = $db->getResultArray();
			if ($chats['count'] < 6) {
				$db->setQuery("UPDATE `chat` SET
									`last_user_id` = '$user_id',
									`status`='2'
									WHERE  `id`='$_REQUEST[id]';");
				$db->execQuery();

				$data['status'] = 'OK';
				$data['chat_status'] = $chats['result'][0]['status'];
			} else {
				$data['status'] = 'max_reached';
			}
		} else {
			$data['status'] = 'FAIL';
		}




		break;
	case 'move_incomming':
		$note = $_REQUEST['note'];

		note_incomming();

		break;
	case 'close_chat':
		$last_request_res = mysql_fetch_assoc(mysql_query(
			"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"
		));
		$timedifference = $last_request_res['timedifference'];
		$who_rejected = $last_request_res['who_rejected'];
		$query_addition = '';
		if ($timedifference >= 10 && $who_rejected == '') {
			$query_addition .= ", `who_rejected` = 0";
		} else if ($timedifference < 10 && $who_rejected == '') {
			$query_addition .= ", `who_rejected` = 1";
		}
		mysql_query("UPDATE `chat` SET
			        `status`='4'$query_addition
			        WHERE  `id`='$_REQUEST[chat_id]';");

		break;
	case 'black_list_add':

		num_block_incomming();

		break;
	case 'num_check':

		$phone = $_REQUEST['phone'];

		$check_num = mysql_num_rows(mysql_query("SELECT phone
											   FROM black_list
											   WHERE phone='$phone' AND actived=1
											   LIMIT 1"));
		if ($check_num == 0 && $phone != '2004') {
			$check = 1;
		} else {
			$check = 0;
		}
		$data = array('check' => $check);

		break;

	case 'black_list_add_chat':
		$user              = $_SESSION['USERID'];
		$source_id         = $_REQUEST['source_id'];
		$chat_block_reason = $_REQUEST['chat_block_reason'];
		$id                = $_REQUEST['id'];

		if ($source_id == 6) {
			$db->setQuery("SELECT fb_chat_id FROM incomming_call WHERE id = '$id'");
			$fb = $db->getResultArray();
			$db->setQuery("SELECT sender_id, sender_name FROM fb_chat WHERE id = '$fb[result][0][fb_chat_id]'");
			$sender = $db->getResultArray();
			$db->setQuery("SELECT id FROM `fb_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
			$check_block = $db->getResultArray();
			if ($check_block == 0) {
				mysql_query("INSERT INTO `fb_chat_black_list` 
                                        (`user_id`, `datetime`, `sender_id`, `name`, `reason`) 
                                  VALUES 
	                                    ('$user', NOW(), '$sender[result][0][sender_id]', '$sender[result][0][sender_name]', '$chat_block_reason');");

				mysql_query("UPDATE fb_chat SET status = 3 WHERE id = $fb[result][0][fb_chat_id]");
			} else {
				global $error;
				$error = 'მომხმარებელი უკვე დაბლოკილია';
			}
		} else if ($source_id == 7) {
			$db->setQuery("SELECT mail_chat_id FROM incomming_call WHERE id = '$id'");
			$mail = $db->getResultArray();
			$db->setQuery("SELECT sender_id FROM mail_chat WHERE id = '$mail[result][0][mail_chat_id]'");
			$sender = $db->getResultArray();
			$db->setQuery("SELECT id FROM `mail_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
			$check_block = $db->getNumRow();
			if ($check_block == 0) {
				mysql_query("INSERT INTO `mail_chat_black_list`
                    	                (`user_id`, `datetime`, `sender_id`, `reason`)
                    	          VALUES
	                                    ('$user', NOW(), '$sender[0][sender_id][sender_id]', '$chat_block_reason');");

				mysql_query("UPDATE mail_chat SET status = 3 WHERE id = $mail[0][sender_id][mail_chat_id]");
			} else {
				global $error;
				$error = 'მომხმარებელი უკვე დაბლოკილია';
			}
		} else if ($source_id == 9) {
			$db->setQuery("SELECT site_chat_id FROM incomming_call WHERE id = '$id'");
			$site = $db->getResultArray();
			$db->setQuery("SELECT sender_id, sender_name FROM site_chat WHERE id = '$site[result][0][site_chat_id]'");
			$sender = $db->getResultArray();
			$db->setQuery("SELECT id FROM `site_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
			$check_block = $db->getNumRow();
			if ($check_block == 0) {
				$db->setQuery("INSERT INTO `site_chat_black_list`
                    	                (`user_id`, `datetime`, `sender_id`, `name`, `reason`)
                    	          VALUES
	                                    ('$user', NOW(), '$sender[result][0][sender_id]', '$sender[result][0][sender_name]', '$chat_block_reason');");
				$db->execQuery();
				$db->setQuery("UPDATE site_chat SET status = 3 WHERE id = $site[result][0][site_chat_id]");
				$db->execQuery();
			} else {
				global $error;
				$error = 'მომხმარებელი უკვე დაბლოკილია';
			}
		}

		$data = array('check' => $check);

		break;
	case 'ipaddr_check':

		// 	    $ipaddres		= $_REQUEST['ipaddres'];
		// 	    $ipaddr_block_comment = $_REQUEST['ipaddr_block_comment'];
		// 	    mysql_close();
		// 	    mysql_connect('212.72.155.176','root','Gl-1114');
		// 	    mysql_select_db('webcall');
		// 	    $check_num=mysql_num_rows(mysql_query(" SELECT phone
		//                                     	        FROM blocked_ip
		//                                     	        WHERE ip='$ipaddres'
		//                                     	        LIMIT 1"));
		// 	    if ($check_num==0 && $ipaddres != '0') {
		// 	        mysql_query("INSERT INTO `blocked_ip`
		// 	                     (`clients_id`, `ip`, `comment`, `status`)
		//                          VALUES
		//                          ('7', '$ipaddres', '$ipaddr_block_comment', '1');");
		// 	        $check=1;
		// 	    }else {
		// 	        $check=0;
		// 	    }
		// 	    $data = array('check' => $check);

		break;

	case 'delete_file':
		$delete_id = $_REQUEST['delete_id'];
		$edit_id   = $_REQUEST['edit_id'];

		$db->setQuery("DELETE FROM file WHERE id = $delete_id");
		$db->execQuery();
		$db->setQuery("SELECT  `name`,
							   `rand_name`,
							   `id`,
                               `file_date`
					   FROM    `file`
					   WHERE   `incomming_call_id` = $edit_id");

		$increm = $db->getResultArray();
		$data1 = '';

		foreach ($increm[result] as $increm_row) {
			$data1 .= '<tr style="border-bottom: 1px solid #CCC;">
                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[file_date] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[name] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
        	          	<td style="height: 20px;vertical-align: middle;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none;  height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
        			  </tr>';
		}

		$data = array('page' => $data1);

		break;

	case 'up_now':
		$user    = $_SESSION['USERID'];
		$edit_id = $_REQUEST['edit_id'];

		if ($rand_file != '') {
			$db->setQuery("INSERT INTO 	`file`
                    				   (`user_id`, `file_date`, `incomming_call_id`, `name`, `rand_name`)
                    			VALUES
                    				   ('$user',NOW(), '$edit_id', '$file', '$rand_file');");
			$db->execQuery();
		}

		$db->setQuery("	SELECT  `name`,
								`rand_name`,
								`id`,
                                `file_date`
						FROM 	`file`
						WHERE   `incomming_call_id` = $edit_id");

		$increm = $db->getResultArray();
		$data1 = '';

		foreach ($increm[result] as $increm_row) {
			$data1 .= '<tr style="border-bottom: 1px solid #CCC;">
                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[file_date] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[name] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
        	          	<td style="height: 20px;vertical-align: middle;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none;  height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
        			  </tr>';
		}

		$data = array('page' => $data1);

		break;
	case 'dep_1':
		$page		= get_dep_11($_REQUEST['cat_id'], '');
		$data		= array('page'	=> $page);

		break;
	case 'dep_2':
		$page		= get_dep_1_1($_REQUEST['cat_id'], '');
		$data		= array('page'	=> $page);

		break;
	case 'dep_3':
		$page		= get_dep_1_1_1($_REQUEST['cat_id'], '');
		$data		= array('page'	=> $page);

		break;
	case 'dep_4':
		$page		= get_dep_1_1_1_1($_REQUEST['cat_id'], '');
		$data		= array('page'	=> $page);

		break;
	case 'cat_2':
		$selector_id = $_REQUEST['selector_id'];
		$input_id = explode('--', $selector_id);
		$db->setQuery("	SELECT 	multilevel_table
						FROM 	processing_setting_detail
						WHERE 	id = '$input_id[1]'");
		$table_name = $db->getResultArray();
		$page		= get_cat_1_1($_REQUEST['cat_id'], '', $table_name['result'][0]['multilevel_table']);
		$data		= array('page'	=> $page);

		break;
	case 'cat_3':
		$selector_id = $_REQUEST['selector_id'];
		$input_id = explode('--', $selector_id);
		$db->setQuery("	SELECT 	multilevel_table
						FROM 	processing_setting_detail
						WHERE 	id = '$input_id[1]'");
		$table_name = $db->getResultArray();
		$page		= get_cat_1_1($_REQUEST['cat_id'], '', $table_name['result'][0]['multilevel_table']);
		$data		= array('page'	=> $page);

		break;
	case 'status_2':
		$page		= get_status_1_1($_REQUEST['status_id'], '');
		$data		= array('page'	=> $page);

		break;

	case 'block_ip':
		if ($_REQUEST[ip] != '') {
			mysql_query("INSERT INTO `blocked`
                    (`user_id`, `datetime`,`ip`,`comment`)
                    VALUES
					('$_SESSION[USERID]',NOW(),'$_REQUEST[ip]','$_REQUEST[chat_comment]');");
			$last_request_res = mysql_fetch_assoc(mysql_query(
				"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"
			));
			$timedifference = $last_request_res['timedifference'];
			$who_rejected = $last_request_res['who_rejected'];
			$query_addition = '';
			if ($timedifference >= 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 0";
			} else if ($timedifference < 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 1";
			}
			mysql_query("UPDATE `chat` SET
        	                `status`='3'$query_addition
        	         WHERE  `id`='$_REQUEST[chat_id]';");
		} else {
			$error = 'IP მისამართი არასწორია!';
		}
		break;
	case 'block_pin':
		if ($_REQUEST[pin] != '') {
			mysql_query("INSERT INTO `blocked`
                    (`user_id`, `datetime`,`pin`,`comment`)
                    VALUES
					('$_SESSION[USERID]',NOW(),'$_REQUEST[pin]','$_REQUEST[chat_comment]');");
			$last_request_res = mysql_fetch_assoc(mysql_query(
				"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"
			));
			$timedifference = $last_request_res['timedifference'];
			$who_rejected = $last_request_res['who_rejected'];
			$query_addition = '';
			if ($timedifference >= 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 0";
			} else if ($timedifference < 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 1";
			}
			mysql_query("UPDATE `chat` SET
        	                `status`='3'$query_addition
        	         WHERE  `id`='$_REQUEST[chat_id]';");
		} else {
			$error = 'IP მისამართი არასწორია!';
		}
		break;
	case 'chat_end':
		if ($_REQUEST[chat_id] != '' && $_REQUEST[chat_id] != '0') {
			if ($_REQUEST[chat_source] == "fbc") {
				$db->setQuery("UPDATE fb_comments SET `status` = '12' WHERE id = $_REQUEST[chat_id] ");
				$db->execQuery();
			} else if ($_REQUEST[chat_source] == "mail ") {
				$db->setQuery("UPDATE mail_live SET `mail_status_id` = '3' WHERE id = $_REQUEST[chat_id] ");
				$db->execQuery();
			} else {
				$db->setQuery("UPDATE chat SET `status` = '12' WHERE id = $_REQUEST[chat_id] ");
				$db->execQuery();
			}
		} else {
			$error = 'შეცდომა: ჩატი ვერ დაიხურა!';
		}
		break;

	case 'save_comment':
		$comment            = $_REQUEST['comment'];
		$incomming_call_id  = $_REQUEST['incomming_call_id'];
		$user_id	        = $_SESSION['USERID'];
		$db->setQuery(" INSERT INTO `chat_comment`
                                   (`user_id`, `incomming_call_id`, `parent_id`, `comment`, `datetime`, `active`)
                             VALUES
                                   ('$user_id', '$incomming_call_id', '0', '$comment', NOW(),'1');");
		$db->execQuery();

		break;

	case 'update_comment':
		$comment = $_REQUEST['comment'];
		$id      = $_REQUEST['id'];
		$db->setQuery("	SELECT 		incomming_call.user_id 
						FROM 		chat_comment 
						LEFT JOIN 	incomming_call ON incomming_call.id = chat_comment.incomming_call_id
						WHERE 		chat_comment.id = '$id'");
		$resp_user = $db->getResultArray();
		$resp_user = $resp_user['result'][0]['user_id'];
		$user_group = $_SESSION['USERGR'];
		if ($user_id != $resp_user and $user_group != 1 and $user_group != 31) {
			$error = 'თქვენ არ გაქვთ ცვლილების უფლება :' . $user_group;
		} else {
			$db->setQuery("	UPDATE 	`chat_comment`
							SET 	`comment` = '$comment'
							WHERE  	`id`      = $id");
			$db->execQuery();
		}


		break;

	case 'delete_comment':

		$comment_id = $_REQUEST['comment_id'];
		$db->setQuery("	SELECT 		incomming_call.user_id 
						FROM 		chat_comment 
						LEFT JOIN 	incomming_call ON incomming_call.id = chat_comment.incomming_call_id
						WHERE 		chat_comment.id = '$comment_id'");
		$resp_user = $db->getResultArray();
		$resp_user = $resp_user['result'][0]['user_id'];
		if ($user_id != $resp_user) {
			$error = 'თქვენ არ გაქვთ წაშლის უფლება';
		} else {
			$db->setQuery("	UPDATE 	`chat_comment`
							SET 	`active` = 0
							WHERE  	`id`     = $comment_id");
			$db->execQuery();
		}

		break;

	case 'get_add_question':
		$id   = $_REQUEST['incomming_call_id'];
		$page = Getquestion($id);
		$data = array('page'	=> $page);
		break;

	case 'get_list_sms':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		$incomming_id = $_REQUEST['incomming_id'];

		$db->setQuery(" SELECT id,
                               date,
                               phone,
                              `content`,
                               if(`status`=1,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
                        FROM  `sent_sms`
                        WHERE  incomming_call_id = '$incomming_id' AND incomming_call_id != ''");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_all_chat':

		getOptionals($_REQUEST['source'], $chat_id);

		if ($_REQUEST['source'] == "mail") {
			$db->setQuery("SELECT  	IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,IF(c.sender_name='', c.sender_address, c.sender_name)))) as name,
                					'',
                					'',
                					'',
                					m.body AS `message_client`,
                					m.body AS `message_operator`,
                					m.datetime AS message_datetime,
									c.to,
									c.cc,
									c.bcc,
                					'',
                					m.id,
									c.sender_address,
                					'',
                                    c.subject AS `subject`,
                                    IFNULL(m.user_id,0) AS `check`
                		FROM mail_detail as m
                		JOIN mail as c on m.mail_id=c.id
                		LEFT JOIN user_info as u on m.user_id=u.user_id
                		WHERE m.mail_id =  '$_REQUEST[chat_id]' AND m.mail_id != ''
                		ORDER BY m.id ASC");
		} elseif ($_REQUEST['source'] == "fbm") {
			$db->setQuery(" SELECT    IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.sender_name))) as name,
    								  '',
    								  '',
    								  '',
    								  m.text AS `message_client`,
    								  m.text AS `message_operator`,
    								  m.datetime AS message_datetime,
    								  '',
    								  m.id,
    								  '',
    								 (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) as media,
                                      IFNULL(m.user_id,0) AS `check`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    					    FROM      fb_messages as m
    						JOIN      fb_chat as c on m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     m.fb_chat_id =  '$_REQUEST[chat_id]' AND m.fb_chat_id != ''
    						ORDER BY  m.id ASC");
		} elseif ($_REQUEST['source'] == "fbc") {

			$db->setQuery(" SELECT    IF(c.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.fb_user_name))) as name,
										'',
										'',
										'',
										c.message AS `message_client`,
										c.message AS `message_operator`,
										c.time AS message_datetime,
										'',
										c.id,
										'',
									(SELECT url from fb_attachments WHERE messages_id=c.id LIMIT 1) as media,
										IFNULL(c.user_id,0) AS `check`,
										(SELECT type from fb_attachments WHERE messages_id=c.id LIMIT 1) as media_type
							FROM      fb_comments as c
							JOIN      fb_feeds as f on c.feeds_id = f.id
							LEFT JOIN user_info as u on c.user_id = u.user_id
							WHERE     c.comment_id = (SELECT comment_id FROM fb_comments WHERE id = '$_REQUEST[chat_id]' AND id != '') 
								OR 	  c.parent_id = (SELECT comment_id FROM fb_comments WHERE id = '$_REQUEST[chat_id]' AND id != '')
							ORDER BY  c.id ASC");
		} elseif ($_REQUEST['source'] == "viber") {
			$db->setQuery(" SELECT    IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.sender_name))) as name,
                        	          '',
                        	          '',
                        	          '',
                        	          m.text AS `message_client`,
                        	          m.text AS `message_operator`,
                        	          m.datetime AS message_datetime,
                        	          '',
                        	          m.id,
                        	          '',
                        	          m.media,
                        	          IFNULL(m.user_id,0) AS `check`,
                        	          '' as media_type
                	        FROM      viber_messages as m
                	        JOIN      viber_chat as c on m.viber_chat_id=c.id
                	        LEFT JOIN user_info as u on m.user_id=u.user_id
                	        WHERE     m.viber_chat_id =  '$_REQUEST[chat_id]' AND m.viber_chat_id != ''
                	        ORDER BY  m.id ASC");
		} else {
			$db->setQuery("UPDATE `chat` 
                              SET `answer_date`    = NOW(),
                                  `answer_user_id` = '$_SESSION[USERID]'
                           WHERE  `id`             = '$_REQUEST[chat_id]'
                           AND     status          = 1");

			//$db->execQuery();
			$name           = '';
			$os             = '';
			$bro            = '';
			$ip             = '';
			$sms            = '';
			$chat_detail_id = 0;

			$db->setQuery(" SELECT     	chat.`name`,
										chat.`device`,
										chat.`browser`,
										chat.`ip`,
										chat_details.message_client,
										chat_details.message_operator,
										chat_details.message_datetime,
										IF(ISNULL(user_info.`chat_nick`),'საიტის ოპერატორი',user_info.chat_nick) AS `op_name`,
										chat_details.id,
										chat.pin,
										chat_details.edited,
										chat_details.edit_date
							FROM      `chat`
							LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
														LEFT JOIN user_info ON user_info.user_id = chat_details.operator_user_id
							WHERE     `chat`.id = '$_REQUEST[chat_id]' AND `chat_details`.`actived` = 1 AND `chat`.id != ''
							ORDER BY   chat_details.message_datetime ASC");
		}

		$res = $db->getResultArray();

		foreach ($res[result] as $req) {
			$subject = '';
			if ($_REQUEST['source'] == "mail") {
				$subject_text = str_replace('Re:', '', $req[subject]);
				$subject .= '<div id="mail_data">';
				$subject .= '<p><b>თემა:</b> ' . $subject_text . '</p>';
				if ($req['to'] != '') {
					$subject .= '<p><b>TO:</b> ' . str_replace(array('<', '>'), '', $req['to']) . '</p>';
				}
				if ($req['cc'] != '') {
					$subject .= '<p><b>CC:</b> ' . str_replace(array('<', '>'), '', $req['cc']) . '</p>';
				}
				if ($req['bcc'] != '') {
					$subject .= '<p><b>BCC:</b> ' . str_replace(array('<', '>'), '', $req['bcc']) . '</p>';
				}
				$subject .= '</div>';

				$req['message_client'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req[message_client]);
				$db->setQuery("SELECT `name` FROM `mail_attachment` WHERE mail_detail_id=$req[id]");
				$fq = $db->getResultArray();
				foreach ($fq[result] as $file) {
					if ($file[check_mesage] == 1) {
						$req['message_operator'] = $req[message_operator] . "<a href='sada$file[name]' target='_blank'>$file[name]</a><br>";
					} else {
						//$file[name] = str_replace('/var/www/callappProduction/','',$file[patch]);	
						/* $req['message_client'] = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[patch].'"',  $req['message_client']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
        				$req[message_client] = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $req['message_client']);	 */
						$req[message_client] = $req[message_client] . "<a href='downloads/$file[name]' target='_blank'>$file[name]</a><br>";
					}
				}
			}

			$gif_url = preg_grep('/(?<=http).*?(?=.gif)/', array($req[message_client], '123'));
			if ($gif_url[0])
				$req[message_client] = "<a href='$gif_url[0]' target='_blank'><img style='border-radius: 10%;max-height: 200px;' src='$gif_url[0]'></a>";

			if ($req[media_type] == 'fallback') {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'file') {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'video') {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} else {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
			}


			$name = $req[name];
			$email = $req[sender_id];

			if ($name == '') $name = 'უცნობი';
			$os  = $req[device];
			$bro = $req[browser];
			$ip  = $req[ip];
			$pin = $req[pin];


			if ($req[edited] == 1) {
				$edited = '<p class="edit_date" title="გაგზავნის თარიღი: ' . $req[message_datetime] . '" id="' . $req[id] . '" style="color: #878787; width: 100%;"><img src="media/images/icons/l_edit.png" style=" height: 14px;margin-bottom: 1px;margin-right: 4px;">' . $req[edit_date] . '</p>';
			}
			if ($req[edited] == 0) {
				$edited = '<p class="edit_date" id="' . $req[id] . '" style="color: #878787; width: 100%;">' . $req[message_datetime] . '</p>';
			}

			$color = 'color:#2681DC;';
			if ($req[check] > 0 && $_REQUEST['source'] != 'chat') {
				$color = '';
			}
			if ($req[message_client] == '' && $_REQUEST['source'] == 'chat') {
				if ($req[message_operator] != '') {
					$sms .= '<tr class="chat_message_id" data-text="' . $req[message_operator] . '" data-id="' . $req[id] . '" style="height: 17px;">
							<td colspan="2" style=" word-break: break-word;     text-align: right;">
							<span class="chat_message_edit_button" ><span class="open_edit" data-id="' . $req[id] . '"><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: -5px;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: 5px;"></div></span><div class="chat_message_edit_block" id="' . $req[id] . '" data-status="0" style="display: none"><div class="sms_edit" data-id="' . $req[id] . '"><img src="media/images/icons/l_edit.png" style="
							margin-right: 3px;
						"> რედაქტირება</div><div class="sms_delete" data-id="' . $req[id] . '"><img src="media/images/icons/l_delete.png" style="
						margin-right: 7px;
					">წაშლა</div></div></span>
								<div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE; ">
									<p style="text-align: left; font-weight: bold; font-size: 12px;">' . $req[op_name] . '</p>
									<p class="operator_message"  id="' . $req[id] . '" style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; text-align: left; line-height: 20x; font-size: 12px;">' . $req[message_operator] . '</p>
									<div style="text-align: right; padding-top: 7px;">
										' . $edited . '
									
									</div>
								</div>
							</td>
						</tr>
						<tr style="height:10px;"></tr>';
				}
			} else {
				if ($_REQUEST['source'] == 'chat') {
					if ($req[message_client] != '') {
						$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 12px;">' . $name . '</p>
                                            <p style="font- family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[message_client] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req[message_datetime] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
    							<tr style="height:10px;"></tr>';
					}
				} else {
					if ($req[check] > 0) {
						if ($req[message_operator] != '') {
							$sms .= '<tr class="chat_message_id" data-text="" data-id="' . $req[id] . '" style="height: 17px;">
									<td colspan="2" style=" word-break: break-word;     text-align: right;">
									<span class="chat_message_edit_button" ><span class="open_edit" data-id="' . $req[id] . '"><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: -5px;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: 5px;"></div></span><div class="chat_message_edit_block" id="' . $req[id] . '" data-status="0" style="display: none"><div class="mail_forward" data-id="' . $req[id] . '"><img src="media/images/icons/l_edit.png" style="
									margin-right: 3px;
								"> Forward</div></div></span>
										<div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE; ">
											<p style="text-align: left; font-weight: bold; font-size: 12px;">' . $req[op_name] . '</p>
											<p class="operator_message"  id="' . $req[id] . '" style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; text-align: left; line-height: 20x; font-size: 12px;">' . $req[message_operator] . '</p>
											<div style="text-align: right; padding-top: 7px;">
												' . $edited . '
											
											</div>
										</div>
									</td>
								</tr>
								<tr style="height:10px;"></tr>';
						}
					} else {



						if ($req[message_client] != '') {

							$text = $req[message_client];

							$sms .= '<tr class="chat_message_id" data-text="" data-id="' . $req[id] . '" style="height: 17px;">
										<td colspan="2" style=" word-break: break-word;">
										<span class="chat_message_edit_button" ><span class="open_edit" data-id="' . $req[id] . '"><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: -5px;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: 5px;"></div></span><div class="chat_message_edit_block" id="' . $req[id] . '" data-status="0" style="display: none"><div class="mail_forward" data-id="' . $req[id] . '"><img src="media/images/icons/l_edit.png" style="
											margin-right: 3px;
										"> Forward</div></div></span>
                                            <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                                	            <p style="font-weight: bold;font-size: 12px;">' . $name . '</p>
    											<p style="overflow-x: auto; font-weight: bold; font-size: 12px;">' . $subject . '</p>
    											<p style="overflow-x: auto; font-weight: 100; font-size: 12px;">' . $email . '</p>
                                                <p style="font-family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . strip_tags($text, '<br><p><a><img>') . '</p>
                                                <div>
                                                    <p style="width: 100%; color: #878787;">' . $req[message_datetime] . '</p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
    								<tr style="height:10px;"></tr>';
						}
					}
				}
			}
		}

		$sms =  str_replace('https://e.mail.ru/', '',  $sms);
		$sms =  str_replace('https://touch.mail.ru/cgi-bin/', '',  $sms);

		if ($_REQUEST['source'] == "fbm") {
			$db->setQuery("SELECT `id` FROM `fb_messages` WHERE  `fb_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} elseif ($_REQUEST['source'] == "fbc") {
			$db->setQuery("SELECT `id` FROM `fb_comments` WHERE  `id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} elseif ($_REQUEST['source'] == "viber") {
			$db->setQuery("SELECT `id` FROM `viber_messages` WHERE  `viber_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} elseif ($_REQUEST['source'] == "mail") {
			$db->setQuery("SELECT `id` FROM `mail_detail` WHERE  `mail_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} else {
			$db->setQuery("SELECT `chat_details`.`id`
                           FROM   `chat_details`
                           WHERE  `chat_details`.`chat_id` = '$_REQUEST[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]' ORDER BY chat_details.id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
			$db->setQuery("	SELECT 	CASE
										WHEN status = 1 THEN ''
										ELSE chat.last_user_id
									END AS last_user_id
							FROM 	`chat` WHERE  `id` = '$_REQUEST[chat_id]' 
							ORDER BY id DESC 
							LIMIT 1");

			$us = $db->getResultArray();
			$last_user_id = $us['result'][0]['last_user_id'];
		}




		$data = array('name' => $name, 'chat_user_id' => $last_user_id, 'os' => $os, 'bro' => $bro, 'pin' => $pin, 'ip' => $ip, 'sms' => $sms, 'chat_detail_id' => $chat_detail_id);

		break;
	case 'chat_action':

		$smsID = $_REQUEST['id'];
		$smsAction = $_REQUEST['chatAction'];
		$smsText = $_REQUEST['text'];

		if ($smsAction == "delete") {
			$db->setQuery("	UPDATE `chat_details` 
								SET 	`actived`   = 0,
										`edited` 	= 0
								WHERE  `id` 		= $smsID ");
		} else {

			$db->setQuery("	UPDATE `chat_details` 
								SET 	`message_operator` = '$smsText',
										`edited`	= 1,
										`edit_date` = NOW()
								WHERE  `id` 		= $smsID ");
		}
		$db->execQuery();
		break;
	case 'get_history':
		if ($_REQUEST['source'] == 'site') {
			$res = mysql_query("SELECT   `text`,
										  IFNULL(u.`name`,`sender_name`) as name,
										  m.datetime as 'datetime',
										  m.media,
                                          m.user_id,
                                          c.first_datetime,
                                          c.id AS `cat_id`
								FROM     `site_chat` as c
								JOIN     `site_messages` as m ON m.site_chat_id=c.id
								LEFT JOIN crystal_users as u on u.id=m.user_id 
								WHERE     c.sender_id in (SELECT sender_id FROM site_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)");
			$imID = 0;
			while ($req = mysql_fetch_array($res)) {
				if (!empty($ge['media'])) $ge[text] = $ge[text] . "<a href='$ge[media]' target='_blank'><img style='border-radius: 10%;max-height: 200px;' src='$ge[media]'></a>";
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}
				if ($req['user_id'] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
         font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 14px;">' . $req[name] . '</p>
                                            <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">' . $req[name] . '</p>
                                            <p style="padding-top: 7px; line-height: 20x; font-size: 14px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				}
				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == 'fbm') {
			$db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    						FROM     `fb_chat` as c
    						JOIN     `fb_messages` as m ON m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM fb_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				if ($req[media_type] == 'fallback') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'file') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'video') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} else {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
				}
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}

				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
				}

				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == 'fbc') {
			$db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    						FROM     `fb_chat` as c
    						JOIN     `fb_messages` as m ON m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM fb_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				if ($req[media_type] == 'fallback') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'file') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'video') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} else {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
				}
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}

				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
				}

				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == 'viber') {
			$db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  m.`media` AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      '' as media_type
    						FROM     `viber_chat` as c
    						JOIN     `viber_messages` as m ON m.viber_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM viber_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY  m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";

				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}

				$db->setQuery("SELECT `emojis`.`code`, `emojis`.viber_key FROM `emojis` WHERE NOT ISNULL(viber_key) AND viber_key != ''");

				$emoji_arr = $db->getResultArray();

				$text = $req[text];
				foreach ($emoji_arr['result'] as $r) {
					$code      = '&#' . $r[code];
					$emoji     = '<span code="' . $code . '">' . $code . '</span>';
					$received  = $text;
					$viber_key = $r[viber_key];
					$text      = str_replace($viber_key, $emoji, $received);
				}

				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">' . $text . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
				}

				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == "mail") {
			$db->setQuery("SELECT  	IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name, IF(c.sender_name='', c.sender_id, c.sender_name)))) as name,
                        		        m.text,
                        		        m.text,
                        		        m.datetime,
                        		        m.id,
                        		        m.subject AS `subject`,
                                        m.user_id,
                                        c.first_datetime,
										c.sender_id AS `sender_id`,
                                        c.id AS `cat_id`
            		        FROM        mail_messages as m
            		        JOIN        mail_chat as c on m.mail_chat_id=c.id
            		        LEFT JOIN   user_info as u on m.user_id=u.user_id
            		        WHERE       c.sender_id = (SELECT mail_chat.sender_id FROM mail_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				$name = $req[0];
				$subject_text = str_replace('Re:', '', $req[subject]);
				if ($subject_text != '') {
					$subject = 'თემა: ' . $subject_text;
				}

				$db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch`, `check_mesage` FROM `mail_attachments` WHERE messages_id=$req[id]");
				$fq = $db->getResultArray();
				foreach ($fq[result] as $file) {
					if ($file[check_mesage] == 1) {
						$req['text']      = $req[text] . "<a href='$file[patch]' target='_blank'>$file[name]</a><br>";
					} else {
						$file[patch] = str_replace('/var/www/html/', '', $file[patch]);
						$req['text']    = preg_replace('/(?<=cid:' . $file[name] . ').*?(?=>)/', $file[patch] . '"',  $req['text']); //preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
						$req['text']      = str_replace('cid:' . $file[name], 'https://crm.my.ge/', $req['text']);
						$req['text']      = $req[text] . "<a href='https://crm.my.ge/$file[patch]' target='_blank'>$file[name]</a><br>";
					}
				}
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}
				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; font-weight: bold;font-size: 14px;">' . $subject . '</p>
                                            
											
											<p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				}
				$imID = $req[cat_id];
				//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$name.'</td></tr>'.$subject.'<tr style="height: 17px;"><td colspan=2 style=" word-break: break-word;">'.$req[1].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[3].'</p><time class="timeago" datetime="'.$req[3].'"></time></td></tr>';
			}

			$sms =  str_replace('https://e.mail.ru/', '',  $sms);
			$sms =  str_replace('https://touch.mail.ru/cgi-bin/', '',  $sms);

			$data = array('name' => $name, 'sms' => $sms);
		} else {
			$name = '';
			$os = '';
			$bro = '';
			$ip = '';
			$sms = '';
			$chat_id = $_REQUEST['chat_id'];
			$db->setQuery("SELECT phone FROM chat WHERE id = '$chat_id'");
			$phone = $db->getResultArray();
			$phone = $phone['result'][0]['phone'];
			$db->setQuery("SELECT  chat.`name`,
									chat.`device`,
									chat.`browser`,
									chat.`ip`,
									chat_details.message_client,
									chat_details.message_operator,
									chat_details.message_datetime,
									IF(ISNULL(chat_nikname.`name`),'ოპერატორი',chat_nikname.name) AS `name`,
									chat.join_date,
									chat.id
							FROM `chat`
							LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
							LEFT JOIN `chat_nikname` ON chat_nikname.crystal_users_id = chat_details.operator_user_id AND chat_nikname.actived = 1
							WHERE chat.`id` IN (SELECT id FROM chat WHERE phone = '$phone')
                            ORDER BY chat_details.message_datetime ASC");
			$imID = 0;
			$res = $db->getResultArray(MYSQLI_NUM);
			foreach ($res[result] as $req) {
				$name = $req[0];
				$os = $req[1];
				$bro = $req[2];
				$ip = $req[3];
				if ($imID != $req[9]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[8] . '-------</td></tr>';
				}
				if ($req[4] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[0] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[5] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req[6] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold; word-break: break-word;">'.$req[7].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[5].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[6].'</p><time class="timeago" datetime="'.$req[6].'"></time></td></tr>';
				} else {
					//$sms .= '<tr style="height: 17px; background: #e9e9e9;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$name.'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[4].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[6].'</p><time class="timeago" datetime="'.$req[6].'"></time></td></tr>';

					$sms .= '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                        	            <p style="overflow-x: auto; font-weight: bold;font-size: 12px;">' . $name . '</p>
                                        <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[4] . '</p>
                                        <div>
                                            <p style="width: 100%; color: #878787;">' . $req[6] . '</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
				}
				$imID = $req[9];
			}
			$data = array('name' => $name, 'os' => $os, 'bro' => $bro, 'ip' => $ip, 'sms' => $sms);
		}
		break;

	case 'check_user_dnd_on_local':
		$user_id = $_SESSION["USERID"];
		$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
		if ($db->getNumRow() == 0) {
			$db->setQuery("SELECT muted FROM `user_mute_check` where user_id = '$user_id'");
			if ($db->getNumRow() == 0) {
				$muted = 0;
			} else {
				$row1  = $db->getResultArray();
				$muted = $row1[result][0][muted];
			}

			$db->setQuery("SELECT    COUNT(*) AS `unrid`
        	               FROM      queries
	                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user_id'
        	               WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
			$res_queries = $db->getResultArray();

			$data = array("status" => 0, 'muted' => $muted, 'queries_unrid_count' => $res_queries[result][0][unrid]);
		} else {
			$db->setQuery("SELECT muted FROM `user_mute_check` where user_id = '$user_id'");
			if ($db->getNumRow() == 0) {
				$muted = 0;
			} else {
				$row1  = $db->getResultArray();
				$muted = $row1[result][0][muted];
			}
			$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
			$row = $db->getResultArray();

			$db->setQuery("SELECT    COUNT(*) AS `unrid`
        	               FROM      queries
	                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user_id'
        	               WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
			$res_queries = $db->getResultArray();

			$data = array("status" => $row[result][0][dndlocalon_status], 'muted' => $muted, 'queries_unrid_count' => $res_queries[result][0][unrid]);
		}





		break;
	case 'change_incall_dnd_status':

		$user_id 	  	= $_SESSION["USERID"];
		$active_status 	= $_REQUEST["activeStatus"];


		$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
		if ($db->getNumRow() == 0) {
			$db->setQuery("INSERT INTO `users_call_dndlocalon` 
                                        (`user_id`, `dndlocalon_status`) 
                                   VALUES 
                                        ('$user_id', '$active_status')");
			$db->execQuery();
		} else {
			$db->setQuery("UPDATE users_call_dndlocalon
    							 SET dndlocalon_status = '$active_status'
    						  WHERE  user_id           = '$user_id'");
			$db->execQuery();
		}
		$data = array("active" => true);
		break;
	case 'change_incall_activitie_status':

		$user_id 	  	= $_SESSION["USERID"];
		$seesion_id		= session_id();
		$activitie_id 	= $_REQUEST["activitieId"];
		$type			= $_REQUEST["type"];
		$data 			= array();

		// define activitie statuses
		$crystal_users_activitie_id = $type == "on" ? $activitie_id : 0;

		$db->setQuery("UPDATE users
						      SET work_activities_id = '$crystal_users_activitie_id'
						   WHERE  id                 = '$user_id'");
		$db->execQuery();

		if ($type == "on") {

			// insert new activitie data
			$db->setQuery("INSERT INTO work_activities_log 
                                         (`user_id`, `session_id`, `work_activities_id`, `start_datetime`)
							        VALUES 
                                         ('$user_id', '$seesion_id', '$activitie_id', NOW())");
			$db->execQuery();

			// define dnd status
			$db->setQuery("SELECT dnd FROM work_activities WHERE id = '$activitie_id' AND actived = 1");
			$dnd_status_assoc = $db->getResultArray();

			$dnd_status = $dnd_status_assoc[result][0]["dnd"];

			$db->setQuery("UPDATE users
							      SET saved_dnd = dnd,
								      dnd       = '$dnd_status'
							    WHERE id        = '$user_id'");
			$db->execQuery();
		} else if ($type == "off") {

			$db->setQuery("UPDATE work_activities_log
							      SET end_datetime = NOW()

							   WHERE  session_id         = '$seesion_id'
							   AND    work_activities_id = '$activitie_id'
							   AND    ISNULL(end_datetime)");
			$db->execQuery();

			$db->setQuery("UPDATE users
							      SET dnd = saved_dnd
							   WHERE  id = '$user_id'");
			$db->execQuery();
		}

		// define online and dnd status
		$db->setQuery("SELECT online, dnd from users WHERE id = '$user_id'");

		$online_status_assoc = $db->getResultArray();
		$online_status       = $online_status_assoc[result][0]["online"];
		$dnd_status          = $online_status_assoc[result][0]["dnd"];

		// define responce data
		$data["onlineStatus"] = (int)$online_status;
		$data["dndStatus"] = (int)$dnd_status;

		break;
	case 'check_activitie_status':

		$user_id          = $_SESSION["USERID"];

		$activitie_data   = get_activitie_status_data($user_id);
		$activitie_id     = $activitie_data["id"];
		$activitie_status = $activitie_data["status"];
		$seconds_passed   = get_activitie_seconds_passed($user_id, $activitie_id);

		$data = array(
			"activitieId" => $activitie_id,
			"activitieStatus" => $activitie_status,
			"secondsPassed" => $seconds_passed
		);

		break;
	case 'check_activitie_status_off':

		$user_id = $_SESSION["USERID"];
		$activitie_data = get_activitie_status_data($user_id);
		$activitie_id = $activitie_data["id"];
		$activitie_status = $activitie_data["status"];
		$seconds_passed = get_activitie_seconds_passed_off($user_id, $activitie_id);

		$data = array(

			"secondsPassed" => $seconds_passed
		);

		break;

	case 'open_new_sms_dialog':

		$type = $_REQUEST["type"];


		if ($_REQUEST['crm'] == 1) {
			$crm_base_id = $_REQUEST['crm_base_id'];
			$crm_phone = mysql_fetch_assoc(mysql_query("SELECT  * FROM autocall_request_base WHERE id='$crm_base_id'"));
			$page		= get_new_sms_dialog_crm($type, $crm_phone);
			$data		= array('page'	=> $page, 'type' => $type);
		} else {
			$page		= get_new_sms_dialog($type);
			$data		= array('page'	=> $page, 'type' => $type);
		}


		break;
	case 'get_shablon':

		$data		 = array('shablon' => getShablon());
		break;

	case 'send_new_sms':

		$send_type = $_REQUEST["sendType"];
		$addressee = $_REQUEST["addressee"];
		$sms_text = $_REQUEST["smsText"];

		$json = array(
			'Number' => $addressee,
			'Message' => $sms_text
		);

		$json = json_encode($json);

		$opts = array(
			'http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Authorization: Basic EE9F474A-B34C-4502-A352-8DC8DA53E647',
				'header'  => 'Content-type:application/json; charset=utf-8',
				'content' => $json
			)
		);

		$context  = stream_context_create($opts);
		$result = file_get_contents('http://192.168.40.19:8089/api/Smses/Send', false, $context);

		$data = array('response' => $result);

		break;
	case 'get_sms_template_dialog':

		$data = array(
			"html" => sms_template_dialog()
		);

		break;
	case 'phone_directory':

		$count = 		$_REQUEST['count'];

		$db->setQuery("SELECT id,
							  id, 
							  name, 
							  CONCAT('<button class=\"download_shablon\" sms_id=\"', id ,'\" data-message=\"', message ,'\" >არჩევა</button>')
						FROM  sms
						WHERE actived = 1");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_phone_dir_list':

		$count  = $_REQUEST['count'];
		$hidden = $_REQUEST['hidden'];
		$type   = $_REQUEST['type'];


		$db->setQuery("SELECT 	 persons_phone.id,
                    	         persons_phone.name,
                    	         person_position AS position,
                    	         IF($type=1, persons_phone.phone_number, persons_phone.mail) AS number
            	       FROM      persons_phone
            	       LEFT JOIN position ON position.id=persons_phone.position_id
            	       WHERE     persons_phone.actived = 1");

		$res = $db->getResultArray(MYSQLI_NUM);

		$data = array("aaData"	=> array());

		foreach ($res[result] as $aRow) {
			$row = array();
			for ($i = 0; $i < $count; $i++) {
				/* General output */
				$row[] = $aRow[$i];
				if ($i == ($count - 1)) {
					$row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="pdl_checkbox_' . $aRow[$hidden] . '" name="check_' . $aRow[$hidden] . '" value="' . $aRow[3] . '" class="pdl-check" />
                                  <label for="pdl_checkbox_' . $aRow[$hidden] . '"></label>
                              </div>';
				}
			}
			$data['aaData'][] = $row;
		}

		break;

	case 'phone_dir_list':

		//data variables
		$type = $_REQUEST['type'];
		$html = phoneDirList($type);
		$data = array('html'	=> $html);

		break;

		/**
		 *  ****************************************************************
		 *  ADD KENDO TABLE
		 * 	****************************************************************
		 */
	case 'get_columns':

		$columnCount = 		$_REQUEST['count'];
		$cols[] =           $_REQUEST['cols'];
		$columnNames[] = 	$_REQUEST['names'];
		$operators[] = 		$_REQUEST['operators'];
		$selectors[] = 		$_REQUEST['selectors'];
		//$query = "SHOW COLUMNS FROM $tableName";
		//$db->setQuery($query,$tableName);
		//$res = $db->getResultArray();
		$f = 0;
		foreach ($cols[0] as $col) {
			$column = explode(':', $col);

			$res[$f]['Field'] = $column[0];
			$res[$f]['type'] = $column[1];
			$f++;
		}
		$i = 0;
		$columns = array();
		foreach ($res as $item) {
			$columns[$i] = $item['Field'];
			$i++;
		}

		$dat = array();
		$a = 0;
		for ($j = 0; $j < $columnCount; $j++) {
			if (1 == 2) {
				continue;
			} else {

				if ($operators[0][$a] == 1) $op = true;
				else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
				//$op = false;

				if ($res['data_type'][$j] == 'date') {
					$g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
				} else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
				{
					$g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
				} else {
					if ($columns[$j] == "inc_status") {
						$g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 150);
					} elseif ($columns[$j] == "audio_file") {
						$g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 100);
					} elseif ($columns[$j] == "id") {
						$g = array('field' => $columns[$j], 'hidden' => false, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 50);
					} elseif ($columns[$j] == "inc_date") {
						$g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 90);
					} else {
						$g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
					}
				}
				$a++;
			}
			array_push($dat, $g);
		}

		// array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

		$new_data = array();
		//{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
		for ($j = 0; $j < $columnCount; $j++) {
			if ($res['data_type'][$j] == 'date') {
				$new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
			} else {
				$new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
			}
		}

		$filtArr = array('fields' => $new_data);
		$kendoData = array('columnss' => $dat, 'modelss' => $filtArr);

		//$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

		$data = $kendoData;
		//$data = '[{"gg":"sd","ads":"213123"}]';

		break;
	case 'get_status_items':
		$db->setQuery("	SELECT		`directory__projects`.`status` as `name`,
											`directory__projects`.`status` as `id`
								FROM
											`directory__projects` 
								WHERE  		`status` <> ''
								GROUP BY 	`status`");
		$res = $db->getResultArray();
		$data = array('options' => $res['result']);
		break;
	case 'edit_project_status':

		$value 		= $_REQUEST['value'];
		$IDs 		= $_REQUEST['ids'];

		$ids = '';

		if (count($IDs) > 0) {
			foreach ($IDs as $id) {
				$ids .= $id . ', ';
			}

			$trimmed = rtrim($ids, ', ');

			$db->setQuery("	UPDATE	`directory__projects`
						SET		`status` = '$value'
						WHERE	`id` IN ($trimmed)");

			$db->execQuery();

			$data = array('errors' => 0, "message" => "edited", "ids" => $trimmed);
		} else {
			$data = array('errors' => 1, "message" => "მონიშნეთ პროექტი", "ids" => $trimmed);
		}
		break;
	case 'edit_project_info_status':

		$value 		= $_REQUEST['value'];
		$IDs 		= $_REQUEST['ids'];

		$ids = '';

		if (count($IDs) > 0) {
			foreach ($IDs as $id) {
				$ids .= $id . ', ';
			}

			$trimmed_info = rtrim($ids, ', ');

			$db->setQuery("	SELECT 	`directory_row_id` 
							FROM	`incomming_call_info`
							WHERE	`id` IN($trimmed_info)");

			$info = $db->getResultArray();

			$i = '';
			foreach ($info['result'] as $r) {
				$i .= $r['directory_row_id'] . ', ';
			}
			$trimmed_project = rtrim($i, ', ');

			$db->setQuery("	UPDATE	`directory__projects`
							SET		`status` = '$value'
							WHERE	`id` IN ($trimmed_project)");
			$db->execQuery();

			$data = array('errors' => 0, "message" => "edited", "ids" => $trimmed_project);
		} else {
			$data = array('errors' => 1, "message" => "მონიშნეთ პროექტი");
		}
		break;
	case 'remove_project_info_item':

		$IDs 			= $_REQUEST['ids'];
		$incomming_id 	= $_REQUEST['incomming_id'];

		$ids 		= '';
		if (count($IDs) > 0) {
			foreach ($IDs as $id) {
				$ids .= $id . ', ';
			}

			$trimmed_info = rtrim($ids, ', ');

			$db->setQuery("DELETE FROM `incomming_call_info` 
							WHERE `incomming_call_id` = '$incomming_id' AND `id` IN($trimmed_info)");
			$db->execQuery();

			$data = array('errors' => 0, "message" => "მოთხოვნა წარმატებით განხორციელდა");
		} else {
			$data = array('errors' => 1, "message" => "მონიშნეთ პროექტი");
		}

		break;
	case 'get_operators':

		$db->setQuery("	SELECT 	users.`id`,
								user_info.`name`
						FROM   	users
						JOIN   	user_info ON users.id = user_info.user_id
						WHERE   `group_id` IN (34,46) AND users.`actived` = 1");
		$data = $db->getResultArray();

		break;
	case 'get_all_calls':

		$db->setQuery("	SELECT id,`name`
						FROM   dir_momartvis_info_newinputID_136
						WHERE  actived = 1");
		$data = $db->getResultArray();

		break;
	case 'get_call_statuses':

		$db->setQuery("	SELECT 	`id`,
								`name`
						FROM   	`call_status`
						WHERE  	`actived` = 1");
		$data = $db->getResultArray();

		break;
	case 'get_call_types':
		$db->setQuery("	SELECT 	`id`,
								`name`
						FROM   	`call_type2`
						WHERE  	`actived` = 1");
		$data = $db->getResultArray();

		break;
	case 'get_sources':

		$db->setQuery("	SELECT 	`id`,
								`name`
						FROM   	`source`
						WHERE  	`actived` = 1");
		$data = $db->getResultArray();

		break;
	case 'get_crm_form':

		$data = array('page' => crmForm(get_crm()));

		break;
	case 'get_crm_sub':

		$id = $_REQUEST['parent_id'];
		$data = array("result" => get_crm_sub($id));

		break;
	case 'insert_in_crm':

		$user_id	= $_SESSION['USERID'];
		$status_id 	= $_REQUEST['status'];
		$comment 	= $_REQUEST['comment'];
		$inc_id 	= $_REQUEST['inc_id'];
		$crm_phone 	= $_REQUEST['phone'];
		$dialogType = $_REQUEST['dialogType'];
		$phone		= $_REQUEST['phone'];
		$call_type 	= 0;
		$crm_phone = substr($crm_phone, -9);
		if ($dialogType == '') {
			$db->setQuery("SELECT `id` FROM `crm` WHERE `incomming_request_id` = '$inc_id'");

			$crm_count = $db->getNumRow();
		} else if ($dialogType == 'outgoing') {
			$phone = substr(preg_replace('/[^0-9.]+/', '', $phone), -9);

			// $db->setQuery("	SELECT		incomming_call.id AS 'id'
			// 				FROM 		asterisk_call_log
			// 				JOIN 		incomming_call ON incomming_call.asterisk_incomming_id = asterisk_call_log.id
			// 				WHERE 		asterisk_call_log.call_type_id IN (2) AND asterisk_call_log.destination = '$phone' AND asterisk_call_log.call_status_id = 13
			// 				ORDER BY 	asterisk_call_log.id DESC
			// 				LIMIT 		1");

			// $incomming_id = $db->getResultArray();
			// $incomming_id = $incomming_id['result'][0]['id'];

			$db->setQuery("SELECT `id` FROM `crm` WHERE `incomming_request_id` = '$inc_id'");

			$crm_count = $db->getNumRow();

			//$inc_id = $incomming_id;
			// es igebda bolos gamaval zars da arasworia
			$crm_phone = $phone;
		} else if ($dialogType == 'autodialer') {
			$phone = substr(preg_replace('/[^0-9.]+/', '', $phone), -9);

				$db->setQuery("	SELECT  id, call_log_id
								FROM    outgoing_campaign_request_base 
								WHERE   phone_number = '995$phone'
								ORDER BY id DESC
								LIMIT   1");

				$base_data = $db->getResultArray();

				if ($base_data['result'][0]['call_log_id'] != '') {
					$call_log_id = $base_data['result'][0]['call_log_id'];
					$db->setQuery("	SELECT 	id
									FROM 	incomming_call
									WHERE 	asterisk_incomming_id = '$call_log_id'");
					$incomming_id = $db->getResultArray();
					$incomming_id = $incomming_id['result'][0]['id'];
				} else {
					$incomming_id = $base_data['result'][0]['id'];
				}
			

			$db->setQuery("SELECT `id` FROM `crm` WHERE `incomming_request_id` = '$incomming_id'");

			$crm_count = $db->getNumRow();


			$inc_id = $incomming_id;
			$crm_phone = $phone;
			$call_type = 5;
		}
		if ($crm_count == 0) {
		    $db->setQuery(" SELECT user_id
                            FROM   incomming_call 
                            WHERE  id = $inc_id");
		    
		    $check_original_user = $db->getResultArray();
		    
		    if ($check_original_user[result][0][user_id] == '' || empty($check_original_user[result][0][user_id])) {
		        $crm_user_id  = $user_id;
		    }else{
		        $crm_user_id  = $check_original_user[result][0][user_id];
		    }
		    
			$crm_phone = substr(preg_replace('/[^0-9.]+/', '', $phone), -9);
			$db->setQuery("INSERT INTO crm
									SET `datetime`				= NOW(),
										`status_id` 			= '$status_id',
										`comment`				= '$comment',
										`incomming_request_id` 	= '$inc_id',
										`crm_phone` 			= '$crm_phone',
			                            `user_id` 				= '$crm_user_id',
										`call_type`				= '$call_type'
							");
			$db->execQuery();

			$data = array('status' => 1, 'message' => 'ინფორმაცია წარმატებით დაემატა');
		} else {
			$data = array('status' => 0, 'message' => 'აღნიშნული მომართვა უკვე არსებობს CRM-ში');
		}

		break;
	case 'get_peer_info':

		$peer_id = $_REQUEST['peer_id'];

		$db->setQuery("SELECT offerer_from_ref FROM video_calls WHERE peer_id = '$peer_id' ORDER BY id ASC LIMIT 1");
		$res = $db->getResultArray();

		$data = $res['result'][0];

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;
$data['permission'] = $permissionLVL;
$data["filterID"] = $filterID;
$data["request_code"] = $_REQUEST["code"];
echo json_encode($data);


/**
 * *********************************
 * KENDO FUNCTIONS
 * *********************************
 */




function get_crm()
{
	global $db;

	$db->setQuery("SELECT * FROM `info_status` WHERE `parent_id` = 0 AND `actived` = 1");
	$res = $db->getResultArray();

	$data = '<option value=0>----</option>';


	foreach ($res['result'] as $option) {
		$data .= '<option value="' . $option['id'] . '">' . $option['name'] . '</option>';
	}

	return $data;
}


function get_crm_sub($id)
{
	global $db;

	$db->setQuery("SELECT * FROM `info_status` WHERE `parent_id` = '$id' AND `actived` = 1");
	$res = $db->getResultArray();

	$data = '';
	$data .= '<option value="0">----</option>';

	foreach ($res['result'] as $option) {
		$data .= '<option value="' . $option['id'] . '">' . $option['name'] . '</option>';
	}

	return $data;
}



function getSelectors($table)
{
	global $db;
	$db->setQuery("	SELECT		`directory__projects`.`status` as `name`,
									`directory__projects`.`status` as `id`
						FROM
									`directory__projects` 
						WHERE  		`status` <> ''
						GROUP BY 	`status`");
	$result = $db->getResultArray();

	$selectorData = array();

	foreach ($result['result'] as $option) {
		array_push($selectorData, array('text' => $option['name'], 'value' => $option['id']));
	}

	return $selectorData;
}

/* ******************************
 *	Request Functions
* ******************************
*/
function phoneDirList($type)
{
	if ($type == 1) {
		$address = 'ტელ. ნომერი';
	} else {
		$address = 'მეილი';
	}
	return '<table class="display" id="phoneDirectoryList" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 27%;">სახელი</th>
                        <th style="width: 28%;">თანამდებობა</th>
                        <th style="width: 42%;">' . $address . '</th>
                        <th style="width: 3%;">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <div class="callapp_checkbox">
                              <input type="checkbox" id="pdl_check_all" name="pdl_check_all" />
                              <label for="pdl_check_all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
            <input type="hidden" id="view_type" value="' . $type . '" />';
}
function checkgroup($user)
{
	$res = mysql_fetch_assoc(mysql_query("  SELECT crystal_users.group_id
											FROM    crystal_users
											WHERE  crystal_users.id = $user"));
	return $res['group_id'];
}

function Addincomming($incom_id)
{
	global $db;


	$ext                 = $_SESSION['EXTENSION'];
	$incom_id	         = $_REQUEST['id'];
	$user		         = $_SESSION['USERID'];
	$c_date		         = date('Y-m-d H:i:s');

	$call_date			 = $_REQUEST['call_date'];
	$hidden_user		 = $_REQUEST['hidden_user'];
	$hidden_inc			 = $_REQUEST['hidden_inc'];
	$imchat			     = $_REQUEST['imchat'];
	$chat_id             = $_REQUEST['chat_id'];
	$ipaddres            = $_REQUEST['ipaddres'];
	$ii			         = $_REQUEST['ii'];
	$phone				 = $_REQUEST['phone'];
	$incomming_cat_1	 = $_REQUEST['incomming_cat_1'];
	$incomming_cat_1_1	 = $_REQUEST['incomming_cat_1_1'];
	$incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
	$incomming_status_1	 = $_REQUEST['incomming_status_1'];
	$incomming_status_1_1 = $_REQUEST['incomming_status_1_1'];
	$source			     = $_REQUEST['source'];
	$my_site			 = $_REQUEST['site_id'];
	$s_u_mail			 = $_REQUEST['s_u_mail'];

	$chat_language		 = $_REQUEST['chat_language'];
	$chat_site			 = $_REQUEST['chat_site'];
	$inc_status_a		 = $_REQUEST['inc_status_a'];
	$company			 = $_REQUEST['company'];
	$space_type			 = $_REQUEST['space_type'];

	$s_u_mail			= $_REQUEST['s_u_mail'];
	$ab_person_name     = $_REQUEST['s_u_name'];
	$lid        		= $_REQUEST['lid'];
	$phone1        		= $_REQUEST['phone1'];
	$fb_link        	= $_REQUEST['fb_link'];
	$viber_address      = $_REQUEST['viber_address'];
	$vizit_datetime     = $_REQUEST['vizit_datetime'];
	$vizit_location     = $_REQUEST['vizit_location'];
	$client_comment     = $_REQUEST['client_comment'];
	$out_comment     = $_REQUEST['out_comment'];

	$client_sex			 = $_REQUEST['client_sex'];
	$client_birth_year	 = $_REQUEST['client_birth_year'];
	$s_u_status			 = $_REQUEST['s_u_status'];
	$inc_status			 = $_REQUEST['inc_status'];
	$call_content		 = htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
	$source_id			 = $_REQUEST['source_id'];
	$personal_pin		 = $_REQUEST['s_u_user_id'];
	$personal_id		 = $_REQUEST['s_u_pid'];
	$lid_comment         = $_REQUEST['lid_comment'];
	$processing_start_date  = $_REQUEST['processing_start_date'];

	$db->setQuery("INSERT INTO `incomming_call`
                           SET `id`             = '$incom_id',
                               `date`           = NOW(),
                               `processing_start_date` = '$processing_start_date',
                               `processing_end_date`   = NOW(),
                               `user_id`        = '$user',
                               `extension`      = '$ext',
                               `phone`          = '$phone',

                               `phone1`         = '$phone1',
                               `fb_link`        = '$fb_link',
                               `viber_address`  = '$viber_address',
                               `vizit_date`     = '$vizit_datetime',
                               `vizit_location` = '$vizit_location',
                               `client_comment` = '$client_comment',
							   `out_comment`	= '$out_comment',
                               `client_name`    = '$ab_person_name',
                               `client_mail`    = '$s_u_mail',
                               `lid`            = '$lid',
                               `client_user_id` = '$personal_pin',
                               `client_pid`     = '$personal_id',
                               `client_satus`   = '$s_u_status',
                               `cat_1`          = '$incomming_cat_1',
                               `cat_1_1`        = '$incomming_cat_1_1',
                               `cat_1_1_1`      = '$incomming_cat_1_1_1',
							   `status_1`          = '$incomming_status_1',
                               `status_1_1`        = '$incomming_status_1_1',
							   `chat_language` 	= '$chat_language',
							   `chat_site`		= '$chat_site',
							   `inc_status`		= '$inc_status_a',
							   `company`		= '$company',
							   `space_type`		= '$space_type',
                               `lid_comment`    = '$lid_comment',
                               `call_content`   = '$call_content'");
	$db->execQuery();

	$site_array = explode(",", $my_site);
	$db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incom_id'");
	$db->execQuery();

	//    foreach($site_array AS $site_id){
	//        $db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incom_id', $site_id)");
	//        $db->execQuery();
	//    }

	if ($lid == 1) {
		$db->setQuery("SELECT *
                       FROM   outgoing_call
                       WHERE  incomming_call_id = '$incom_id' AND outgoing_type_id = 4");

		$check = $db->getNumRow();
		if ($check == 0) {
			$db->setQuery("INSERT INTO `outgoing_call`
                                   SET `user_id`           = '$user',
                                       `date`              =  NOW(),
                                       `start_date`        =  NOW(),
                                       `incomming_call_id` = '$incom_id',
                                       `phone`             = '$phone',
                                       `outgoing_type_id`  = '4',
                                       `status_id`         = '1',
                                       `lid_comment`       = '$lid_comment'
                                       `actived`           = '1'");
			$db->execQuery();
		}
	}
}

function Saveincomming($incom_id)
{
	global $error;
	global $db;

	$ext                 = $_SESSION['EXTENSION'];
	$incom_id	         = $_REQUEST['id'];
	$user		         = $_SESSION['USERID'];
	$c_date		         = date('Y-m-d H:i:s');

	$call_date			 = $_REQUEST['call_date'];
	$hidden_user		 = $_REQUEST['hidden_user'];
	$hidden_inc			 = $_REQUEST['hidden_inc'];
	$imchat			     = $_REQUEST['imchat'];
	$chat_id             = $_REQUEST['chat_id'];
	$ipaddres            = $_REQUEST['ipaddres'];
	$ii			         = $_REQUEST['ii'];
	$phone				 = $_REQUEST['phone'];
	$incomming_cat_1	 = $_REQUEST['incomming_cat_1'];
	$incomming_cat_1_1	 = $_REQUEST['incomming_cat_1_1'];
	$incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
	$incomming_status_1	 = $_REQUEST['incomming_status_1'];
	$incomming_status_1_1	 = $_REQUEST['incomming_status_1_1'];
	$source			    = $_REQUEST['source'];
	$my_site			= $_REQUEST['site_id'];

	$chat_language		 = $_REQUEST['chat_language'];
	$chat_site			 = $_REQUEST['chat_site'];
	$inc_status_a		 = $_REQUEST['inc_status_a'];
	$company		 	= $_REQUEST['company'];
	$space_type			= $_REQUEST['space_type'];

	$s_u_mail			= $_REQUEST['s_u_mail'];
	$ab_person_name     = $_REQUEST['s_u_name'];
	$lid        		= $_REQUEST['lid'];
	$phone1        		= $_REQUEST['phone1'];
	$fb_link        	= $_REQUEST['fb_link'];
	$viber_address      = $_REQUEST['viber_address'];
	$vizit_datetime     = $_REQUEST['vizit_datetime'];
	$vizit_location     = $_REQUEST['vizit_location'];
	$client_comment     = $_REQUEST['client_comment'];
	$out_comment		= $_REQUEST['out_comment'];
	$client_sex			= $_REQUEST['client_sex'];
	$client_birth_year	= $_REQUEST['client_birth_year'];
	$s_u_status			= $_REQUEST['s_u_status'];
	$inc_status			= $_REQUEST['inc_status'];
	$call_content		= htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
	$source_id			= $_REQUEST['source_id'];
	$personal_pin		= $_REQUEST['s_u_user_id'];
	$personal_id		= $_REQUEST['s_u_pid'];

	$lid_comment        = $_REQUEST['lid_comment'];

	//if ($ext=0 || $_REQUEST[imchat] == '1'){
	//if ($hidden_user == $user || $_REQUEST['imchat'] == '1' || $hidden_user == '' || $user == 1){

	if ($phone != '' && $phone != '2004' && ($personal_pin != '' || $ab_person_name != '')) {
		$db->setQuery("SELECT phone,
                	                  id,
                	                  name
                	           FROM   abonent_pin
                	           WHERE  abonent_pin.phone = '$phone'
                	           LIMIT 1");

		$check_ab_pin = $db->getResultArray();
		$ab_inf_id = $check_ab_pin[result][0][id];
		if ($check_ab_pin[result][0][phone] == '' && $check_ab_pin[result][0][name] == '') {

			$db->setQuery("INSERT INTO `abonent_pin`
	                                          (`user_id`, `datetime`, `phone`, `pin`, `name`)
	                                    VALUES
	                                          ('$user', NOW(), '$phone', '$personal_pin', '$ab_person_name')");
			$db->execQuery();
		} else {
			$db->setQuery("UPDATE `abonent_pin`
                                      SET `user_id`  = $user,
                                          `datetime` = NOW(),
                                          `pin`      = '$personal_pin',
                                          `name`     = '$ab_person_name'
                                   WHERE  `id`       = '$ab_inf_id'");

			$db->execQuery();
		}
	}

	if ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'fbm') {
		$db->setQuery("SELECT last_user_id FROM fb_chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();

		if ($user == $check_user[result][0][last_user_id]) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($check_user[result][0][last_user_id] == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'fbc') {
		$db->setQuery("SELECT last_user_id FROM fb_comments WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		$last_user_id = $check_user[result][0][last_user_id];

		if ($user == $last_user_id) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($last_user_id == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'mail') {
		$db->setQuery("SELECT user_id FROM mail WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		$last_user_id = $check_user[result][0][user_id];

		if ($user == $last_user_id) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($last_user_id == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'chat') {
		$db->setQuery("SELECT last_user_id FROM chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		if ($user == $check_user[result][0][last_user_id]) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($check_user[result][0][last_user_id] == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'viber') {
		$db->setQuery("SELECT last_user_id FROM viber_chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		if ($user == $check_user[result][0][last_user_id]) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($check_user[result][0][last_user_id] == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} else {
		if ($_REQUEST['ii'] == 1 && $phone != '') {
			$user_update = "`user_id` = '$user',";
		}
	}


	if ($_REQUEST['source'] == 'fbm') {
		if ($user != 1) {
			$db->setQuery("UPDATE fb_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'fbc') {
		if ($user != 1) {
			$db->setQuery("UPDATE fb_comments SET last_user_id = '$user', `status` = 3 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'mail') {
		if ($user != 1) {
			$db->setQuery("UPDATE mail SET user_id = '$user', `mail_status_id` = 2 WHERE id = '$chat_id' AND `mail_status_id` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'chat') {
		if ($user != 1) {
			$db->setQuery("UPDATE chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'viber') {
		if ($user != 1) {
			$db->setQuery("UPDATE viber_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	}

	$db->setQuery("UPDATE `incomming_call`
                              SET `user_id`           = '$user',
                                  `extension`         = '$ext',
                                  `phone`             = '$phone',
                                  `client_user_id`    = '$personal_pin',
                                  `client_pid`        = '$personal_id',
                                  `client_name`       = '$ab_person_name',
                                  `client_mail`       = '$s_u_mail',

                                  `phone1`            = '$phone1',
                                  `fb_link`           = '$fb_link',
                                  `viber_address`     = '$viber_address',
                                  `vizit_date`        = '$vizit_datetime',
                                  `vizit_location`    = '$vizit_location',
                                  `client_comment`    = '$client_comment',
								  `out_comment`			= '$out_comment',

                                  `client_satus`      = '$s_u_status',
                                  `client_sex`        = '$client_sex',
                                  `client_birth_year` = '$client_birth_year',
                                  `cat_1`             = '$incomming_cat_1',
                                  `cat_1_1`           = '$incomming_cat_1_1',
                                  `cat_1_1_1`         = '$incomming_cat_1_1_1',

								  `status_1`             = '$incomming_status_1',
                                  `status_1_1`           = '$incomming_status_1_1',
								  
								  `chat_language` 		= '$chat_language',
							  	  `chat_site`			= '$chat_site',
							   	  `inc_status`		= '$inc_status_a',
							   	  `company`			= '$company',
                                  `inc_status_id`     = '$inc_status',
                                  `source_id`         = '$source_id',
								  `space_type`		= '$space_type',
                                  `lid`               = '$lid',
                                  `lid_comment`       = '$lid_comment',
                                  `call_content`      = '$call_content'
                           WHERE  `id`                = '$incom_id'");

	$db->execQuery();

	$db->setQuery("UPDATE `incomming_call`
			                  SET  processing_end_date = NOW()
			               WHERE  `id` = '$incom_id' AND NOT ISNULL(asterisk_incomming_id) AND ISNULL(processing_end_date)");
	$db->execQuery();

	$site_array = explode(",", $my_site);
	$db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incom_id'");
	$db->execQuery();

	//			foreach ($site_array AS $site_id){
	//			    $db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incom_id', $site_id)");
	//			    $db->execQuery();
	//			}

	if ($lid == 1) {
		$db->setQuery("SELECT * 
                               FROM   outgoing_call
                               WHERE  incomming_call_id = '$incom_id' AND outgoing_type_id = 4");

		$check = $db->getNumRow();
		if ($check == 0) {
			$db->setQuery("INSERT INTO `outgoing_call`
                    			           SET `user_id`           = '$user',
                        			           `date`              =  NOW(),
                        			           `start_date`        =  NOW(),
                                               `incomming_call_id` = '$incom_id',
                        			           `phone`             = '$phone',
                        			           `outgoing_type_id`  = '4',
                        			           `status_id`         = '1',
                                               `lid_comment`       = '$lid_comment',
                        			           `actived`           = '1'");
			$db->execQuery();
		}
	}

	// 		}else{
	// 			$error='თქვენ არ გაქვთ შეცვლის უფლება';
	// 		}
	if (($chat_id != '' || $chat_id != 0)) {

		$source              	= $_REQUEST['source'];

		if ($_REQUEST['ii'] == 1 && $source == 'site') {
			$db->setQuery("UPDATE `site_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
			                     SET  processing_end_date = NOW()
			                  WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'fbm') {
			$db->setQuery("UPDATE `fb_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
            			          SET  processing_end_date = NOW()
            			       WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'fbc') {
			$db->setQuery("UPDATE `fb_comments` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
            			          SET  processing_end_date = NOW()
            			       WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'mail') {
			$db->setQuery("UPDATE `mail` SET user_id = '$user', `mail_status_id`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
			                     SET   processing_end_date = NOW()
			                   WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'viber') {
			$db->setQuery("UPDATE `viber_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
			                      SET  processing_end_date = NOW()
			                   WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} else {
			if ($_REQUEST['ii'] == 1 && $phone != '2004') {
				$db->setQuery("SELECT `last_request_datetime`,
            							  `status`,
            							   TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
            							   who_rejected
            						from   chat 
            						where `id` = '$chat_id';");
				$last_request_res = $db->getResultArray();
				$timedifference   = $last_request_res[result][0]['timedifference'];
				$who_rejected     = $last_request_res[result][0]['who_rejected'];
				$query_addition   = '';

				if ($timedifference >= 10 && $who_rejected == '') {
					$query_addition .= ", `who_rejected` = 0";
				} else if ($timedifference < 10 && $who_rejected == '') {
					$query_addition .= ", `who_rejected` = 1";
				}

				$db->setQuery("UPDATE `chat` SET
    									  `status` = '4' 
    				                       $query_addition
    							   WHERE  `id`     = '$chat_id';");
				$db->execQuery();

				$db->setQuery("UPDATE `incomming_call`
    				                  SET  processing_end_date = NOW()
    				               WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
				$db->execQuery();
			}
		}
	}
	// 	}else{
	// 		$error='თქვენ არ გაქვთ არჩეული ექსთენშენი, გთხოვთ გახვიდეთ სისტემიდან და აირიოთ ';
	// 	}

}

function note_incomming()
{
	global $error;
	$incom_id	= $_REQUEST['id'];
	$user		= $_SESSION['USERID'];
	$group		= checkgroup($user);

	//      $inc_user = mysql_fetch_array(mysql_query("SELECT user_id 
	//                                                 FROM   incomming_call
	//                                                 WHERE  id = $incom_id"));

	//      if ($group==2 || $group==3) {

	mysql_query("UPDATE `incomming_call`
					    SET `noted`              = '1',
                            `monitoring_user_id` =  $user
							 WHERE `id`          = '$incom_id'");

	// 	}elseif ($group==1 && $inc_user[user_id] == $user){

	// 	    mysql_query("UPDATE `incomming_call`
	//             	        SET `noted`              ='1',
	//             	            `monitoring_user_id` = $user
	//             	     WHERE  `id`                 = '$incom_id'");
	// 	}else{
	// 	     $error='ეს ფუნქცია თქვენთვის შეზღუდულია';
	//  	}

}

function num_block_incomming()
{
	global $error;
	$phone			= $_REQUEST['phone'];
	$block_comment	= $_REQUEST['block_comment'];
	$user			= $_SESSION['USERID'];
	$group			= checkgroup($user);
	$c_date			= date('Y-m-d H:i:s');

	if ($group == 2 || $group == 1 || $group == 3) {
		mysql_query("INSERT INTO `black_list`
						(`user_id`,`date`,`phone`,`comment`)
					VALUES
						('$user','$c_date', '$phone','$block_comment');");
	} else {
		$error = 'ეს ფუნქცია თქვენთვის შეზღუდულია';
	}
}

function get_comment($hidden_id)
{
	global $db;
	$data = "";
	$db->setQuery("SELECT   `id`,
                            `user_id`,
                            `incomming_call_id`,
                            `comment`,
                            `datetime`
                   FROM     `chat_comment`
                   WHERE     parent_id = 0 AND incomming_call_id = '$hidden_id' AND active = 1
                   ORDER BY `datetime` DESC");

	$req = $db->getResultArray();
	foreach ($req[result] as $res) {
		$data .= '<div style="margin-top: 15px; padding:10px;">
                        <input type=hidden id="hidden_comment_id_' . $res['id'] . '" value="' . $res['id'] . '"/>
						<span style="color: #369; font-weight: bold;">' . get_user($res['user_id']) . '</span> ' . $res['datetime'] . '
						<img id="edit_comment" my_id="' . $res['id'] . '" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="' . $res['id'] . '" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"> <br><br>
                        <div style="border: 1px #D7DBDD solid; padding: 14px; border-radius:20px; background-color:#D7DBDD; "> ' . $res['comment'] . '</div>
                        <br/><span id="get_answer" class="' . $res['id'] . '"  style="color: #369; font-size:15px;     font-size: 11px;
                        margin-left: 10px;  ">პასუხის გაცემა</span>
                 </div>
                 <div style="margin-left: 50px; margin-top: 10px;">
                    ' . get_answers($res['id'], $hidden_id) . '
                 </div>';
	}


	return $data;
}


function getOptionals($chat_source, $chat_id)
{

	global $db;

	if ($chat_source == '') {
		$chat_source = $_REQUEST["source"];
	}
	$chat_id = $_REQUEST['chat_id'];

	if ($chat_source) {
		$db->setQuery("	SELECT 	id, name
						FROM	source
						WHERE 	`key` = '$chat_source'	");
		$source_res = $db->getResultArray();
		$chat_source_a_id = $source_res[result][0][id];
		$chat_source_a = $source_res[result][0][name];
	} else {
		$chat_source_a = $chat_source;
	}

	if ($chat_source == "fbm" && $chat_id != '') {
		$db->setQuery("	SELECT 	fb_account.`name`, fb_account.`id`
						FROM 	fb_account 
						JOIN 	fb_chat ON `fb_chat`.`id` = $chat_id AND `fb_chat`.`account_id` = `fb_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0][name];
		$chat_site_id = $result_source[result][0][id];
	} elseif ($chat_source == "viber" && $chat_id != '') {
		$db->setQuery("	SELECT 	viber_account.`name`, viber_account.`id`
						FROM 	viber_account 
						JOIN 	viber_chat ON `viber_chat`.`id` = $chat_id AND `viber_chat`.`account_id` = `viber_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0][name];
		$chat_site_id = $result_source[result][0][id];
	} elseif ($chat_source == "mail" && $chat_id != '') {
		$db->setQuery("	SELECT 	mail_account.`name`, mail_account.`id`
						FROM 	mail_account 
						JOIN 	mail ON `mail`.`id` = $chat_id AND `mail`.`account_id` = `mail_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0][name];
		$chat_site_id = $result_source[result][0][id];
	} else {
		$chat_site = "ID not found";
	}

	$optData = array('chat_source_a_id' => $chat_source_a_id, 'chat_source_a' => $chat_source_a, 'chat_site' => $chat_site, 'chat_site_id' => $chat_site_id, 'chat_source' => $chat_source);

	return $optData;
}

function web_site($id)
{
	global $db;
	$data = '';

	$db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");

	$res = $db->getResultArray();
	foreach ($res[result] as $value) {
		$db->setQuery("SELECT id
	                   FROM  `incoming_call_site`
	                   WHERE incoming_call_site.incomming_call_id = '$id' AND site_id = '$value[id]'");
		$check = $db->getNumRow();

		if ($check > 0) {
			$data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
		} else {
			$data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
		}
	}

	return $data;
}

function get_search_site($id)
{
	global $db;
	$data = '';

	$db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");

	$data = $db->getSelect($id);

	return $data;
}

function get_search_transaction_type($id)
{
	global $db;
	$data = '';

	$db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `transaction_type`
				   WHERE   actived = 1");

	$data = $db->getSelect($id);

	return $data;
}


function Getclientstatus($id)
{

	if ($id == 1) {
		$data .= '<option value="0">----</option>
                  <option value="1" selected="selected">აქტიური</option>
                  <option value="2">პასიური</option>';
	} elseif ($id == 2) {
		$data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2" selected="selected">პასიური</option>';
	} else {
		$data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2">პასიური</option>';
	}

	return $data;
}

function get_s_u_sex($id)
{

	if ($id == 1) {
		$data .= '<option value="0">----</option>
                  <option value="1" selected="selected">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
	} elseif ($id == 2) {
		$data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2" selected="selected">მამრობითი</option>';
	} else {
		$data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
	}

	return $data;
}

function Getincstatus($object_id)
{
	global $db;
	$data = '';
	$db->setQuery("SELECT `id`,
                          `name`
				   FROM   `inc_status`
				   WHERE  `actived` = 1");

	$data = $db->getSelect($object_id);

	return $data;
}

function Getsource($object_id)
{
	global $db;
	$data = '';
	$db->setQuery("SELECT 	`id`,
                            `name`
				    FROM 	`source`
				    WHERE 	 actived=1");


	$data = $db->getSelect($object_id);

	return $data;
}

function get_transfer_ext($ext, $check)
{
	$data = '';
	if ($check == 1) {
		$req = mysql_query("SELECT `id`,
                                   `name`
                            FROM   `crystal_users`
                            WHERE  `group_id` IN(1,2,3) AND `actived` = 1");

		$data .= '<option value="0" selected="selected">----</option>';
		while ($res = mysql_fetch_assoc($req)) {
			if ($res['id'] == $ext) {
				$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
			} else {
				$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
			}
		}
	} else {
		$req = mysql_query("SELECT   extention_login.extention 
                            FROM    `extention_login` 
                            JOIN     crystal_users ON crystal_users.last_extension = extention_login.extention
                            WHERE    extention_login.id < 11 AND crystal_users.logged = 1
                            ORDER BY extention_login.extention ASC");


		$data .= '<option value="0" selected="selected">----</option>';
		while ($res = mysql_fetch_assoc($req)) {
			if ($res['extention'] == $ext) {
				$data .= '<option value="' . $res['extention'] . '" selected="selected">' . $res['extention'] . '</option>';
			} else {
				$data .= '<option value="' . $res['extention'] . '">' . $res['extention'] . '</option>';
			}
		}
	}
	return $data;
}

function Getcategory($category_id)
{

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `category`
						WHERE actived=1 && parent_id=0 ");


	$data .= '<option value="0" selected="selected">----</option>';
	while ($res = mysql_fetch_assoc($req)) {
		if ($res['id'] == $category_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_lang($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `user_language`
                   WHERE   actived = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == 1) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_inc_status($id)
{
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `inc_status`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_company($id)
{
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `company`
					WHERE   `actived` = 1 AND `disabled` = 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_space_type($id)
{
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `space_type`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_cat_1($id, $table)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   $table
                   WHERE   actived = 1 AND `parent_id` = 0");

	$req = $db->getResultArray();
	$data .= '<option value="0" selected="selected">----</option>';
	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_cat_1_1($id, $child_id, $table)
{
	global $db;

	$db->setQuery("SELECT  `id`,
                           `name`
                   FROM    $table
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}

function get_cat_1_1_1($id, $child_id)
{
	global $db;
	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;

	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}

	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}


function get_dep_1($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");

	$req = $db->getResultArray();
	$data .= '<option value="0" selected="selected">----</option>';
	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
function get_dep_11($id, $child_id)
{
	global $db;
	$hidden = ' IN(121,126,129,132,135,138,141,145,151,154,157) ';

	if ($id != 0) {
		$db->setQuery("SELECT  `id`,
								`name`
						FROM    `info_category`
						WHERE   `info_category`.id NOT $hidden AND actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
	} else {
		$db->setQuery("SELECT id,name 
		FROM info_category 
	   WHERE parent_id IN(SELECT id 
						  FROM info_category 
						WHERE `info_category`.id NOT $hidden AND parent_id = 0 AND actived = 1) AND actived = 1");
	}


	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}

function get_dep_1_1($id, $child_id)
{
	global $db;
	$hidden = ' IN(121,126,129,132,135,138,141,145,151,154,157) ';
	
	$db->setQuery("SELECT  		`cat1`.id,
								`cat1`.`name`
					FROM 		`info_category`
					LEFT JOIN 	`info_category` AS `cat1` ON `cat1`.`parent_id` = `info_category`.`id`
					
					WHERE 	 	`cat1`.id NOT $hidden AND `info_category`.id NOT $hidden AND `info_category`.`parent_id` = 0 AND `info_category`.actived=1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}


function get_dep_1_1_1_1($id, $child_id)
{
	global $db;

	$db->setQuery("SELECT id,name,parent_id
					FROM info_category
					WHERE actived = 1 AND parent_id != 0 AND id = (SELECT parent_id FROM info_category WHERE id='$id') LIMIT 1");

	$req = $db->getResultArray();
	$res  = $req[result][0];
	$data .= '<option value="0">----</option>';
	$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
	$db->setQuery("SELECT id,name FROM info_category WHERE actived = 1 AND id = '$res[parent_id]'");
	$req2 = $db->getResultArray();
	$res2 = $req2['result'][0];
	$data1 .= '<option value="0">----</option>';
	$data1 .= '<option value="' . $res2['id'] . '" selected="selected">' . $res2['name'] . '</option>';

	$arr = array('data' => $data, 'data1' => $data1);
	return $arr;
}



function get_dep_1_1D($id, $child_id)
{
	global $db;

	$db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `info_category`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}
function get_project_type($id)
{
	global $db;
	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `mepa_project_types`
                   WHERE   actived = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}

	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}
function get_dep_1_1_1($id, $child_id)
{
	global $db;
	if ($id != 0) {
		$db->setQuery("SELECT `id`,
								`name`
						FROM   `info_category`
						WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
	} else {
		$db->setQuery("SELECT id, `name`
		FROM info_category
	   
	   WHERE parent_id IN(SELECT id 
											 FROM info_category 
											WHERE parent_id IN(SELECT id 
															   FROM info_category 
																				   WHERE parent_id = 0 AND actived = 1) AND actived = 1) AND actived = 1");
	}


	$req = $db->getResultArray();

	$data2 .= '<option value="0" selected="selected">----</option>';
	$i = 0;

	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data2 .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data2 .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}

	if ($i == 0 && $id > 0) {
		$data2 .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	$db->setQuery("	SELECT id,name

					FROM info_category
					WHERE id = (SELECT parent_id
											FROM info_category
											WHERE info_category.id = '$id')");
	$main = $db->getResultArray();

	$data3 .= '<option value="' . $main['result'][0]['id'] . '" selected="selected">' . $main['result'][0]['name'] . '</option>';

	if ($child_id == '') {
		if ($id == 0) {

			$data = array('data2' => $data2, 'data3' => get_dep_1(0));
		} else {
			$data = array('data2' => $data2, 'data3' => $data3);
		}
	} else {
		$data = $data2;
	}

	return $data;
}

// ####################

function get_status_1($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_status`
                   WHERE   actived = 1 AND `parent_id` = 0");

	$req = $db->getResultArray();
	$data .= '<option value="0" selected="selected">----</option>';
	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_status_1_1($id, $child_id)
{
	global $db;

	$db->setQuery("SELECT  `id`,
                           `name`,
						   `comment`
                   FROM    `info_status`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option title="' . $res['comment'] . '" value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option title="' . $res['comment'] . '" value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}


// ####################


function Getcomunication_tab()
{

	global $db;

	$user_id	             = $_SESSION['USERID'];

	$db->setQuery("SELECT      `source`.`key`,
								`source`.`background_image_name`,
								`users_source_status`.`status`
					FROM        `source`
					LEFT JOIN   `users_source_status` ON `users_source_status`.`source_id` = `source`.`id` AND `users_source_status`.`user_id` = '$user_id'
					WHERE       `source`.`actived` = 1 AND `source`.`key` != '0'
					ORDER BY `source`.`position`");

	$req = $db->getResultArray();

	$data = '';

	foreach ($req['result'] as $res) {
		if ($res['status'] == 1 or $res['key'] == 'phone') {

			$data .= '<li class="incoming_chat_tab_' . $res[key] . '"><a href="#incoming_chat_tab_' . $res[key] . '"><img src="media/images/icons/comunication/' . $res[background_image_name] . '" height="21" width="21"><span class="' . $res[key] . '_chat fabtext" >0</span></a></li>';
		} else {

			$data .= '<li disabled="true" class="incoming_chat_tab_' . $res[key] . '"><a href="#incoming_chat_tab_' . $res[key] . '"><img src="media/images/icons/comunication/' . $res[background_image_name] . '" height="21" width="21"><span class="' . $res[key] . '_chat fabtext" >0</span></a></li>';
		}
	}

	return $data;
}



function getlastphone($phone)
{
	global $db;
	if ($phone == '') {
		$res = '';
	} else {
		$db->setQuery("SELECT    	incomming_call.id AS id,
                                    incomming_call.`date` AS call_date,
            						incomming_call.`date` AS record_date,
            						incomming_call.phone AS `phone`,
                                    incomming_call.ipaddres,
            						incomming_call.asterisk_incomming_id,
            						user_info.`name` AS operator,
            						IF(ISNULL(asterisk_call_log.user_id),chat_user_name.`name`,user_info.`name`) AS operator,
            						IF(ISNULL(asterisk_call_log.user_id),chat.answer_user_id,asterisk_call_log.user_id ) as `user_id`,
            						/* incomming_call.inc_status_id AS `inc_status`, */
            						incomming_call.cat_1,
            						incomming_call.cat_1_1,
                                    incomming_call.cat_1_1_1,
									incomming_call.status_1,
            						incomming_call.status_1_1,
            						incomming_call.space_type,
            						incomming_call.chat_language,
            						incomming_call.inc_status,
            						incomming_call.company,
            						incomming_call.call_content AS call_content,
            						incomming_call.source_id,
            						incomming_call.client_user_id AS personal_pin,
            						incomming_call.client_pid AS personal_id,
            						incomming_call.web_site_id,
            						incomming_call.client_name,

                                    incomming_call.client_comment,
									incomming_call.out_comment,
                                    incomming_call.vizit_location,
                                    incomming_call.vizit_date,
                                    incomming_call.fb_link,
                                    incomming_call.viber_address,
                                    incomming_call.phone1,

            						incomming_call.client_satus,
            						incomming_call.client_sex,
                                    incomming_call.client_mail,
            						incomming_call.chat_id,
                                    incomming_call.lid,
                                    incomming_call.lid_comment,
                                    asterisk_call_log.source AS `ast_source`,
                                    chat.ip,
            						site_chat_id,
            						fb_chat_id,
            						mail_chat_id,
									viber_chat_id,
									fb_comment_id,
                                    messenger_chat_id,
                                    video_call_id
                        FROM 	    incomming_call
                        LEFT JOIN   asterisk_call_log on asterisk_call_log.id = incomming_call.asterisk_incomming_id
                        LEFT JOIN   users ON users.id=asterisk_call_log.user_id
                        LEFT JOIN   user_info ON users.id = user_info.user_id
                        LEFT JOIN   chat on incomming_call.chat_id = chat.id
                        LEFT JOIN   users AS chat_user ON chat_user.id=chat.answer_user_id
                        LEFT JOIN   user_info AS chat_user_name ON chat_user.id=chat_user_name.user_id
                        WHERE      	incomming_call.phone = '$phone'
                        ORDER BY	incomming_call.id DESC
                        LIMIT 1");
		$res = $db->getResultArray();
	}
	return $res[result][0];
}

function get_signatures()
{
	global $db;

	$db->setQuery("	SELECT 	id,
							name
					FROM 	signature
					WHERE 	actived=1");
	$data = $db->getSelect(0);

	return $data;
}

function get_task_branch($id)
{
	global $db;
	$data .= '';

	$db->setQuery("SELECT dep_id FROM user_info WHERE user_id = $id");
	$dep_id = $db->getResultArray();
	$db->setQuery(" SELECT `id`,
                           `name`
                    FROM    department
                    WHERE   actived = 1");

	$req = $db->getResultArray();
	$data .= '<option value="0">-------</option>';
	foreach ($req[result] as $res) {
		if ($res[id] == $dep_id[result][0][dep_id]) {
			$data .= '<option selected="selected" value="' . $res['id'] . '">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_task_recipient($id)
{
	global $db;
	$where = '';
	if ($id != 0) {
		$where = "AND user_info.dep_id = '$id'";
	}

	$data .= '';

	$db->setQuery(" SELECT users.`id`,
                           user_info.`name`
                    FROM   user_info
                    JOIN   users ON users.id = user_info.user_id
                    WHERE  users.actived = 1
        $where");

	$req = $db->getResultArray();

	$data .= '<option value="0">-------</option>';

	foreach ($req[result] as $res) {
		$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
	}

	return $data;
}

function Getincomming($incom_id, $chat_id)
{
	global $db;
	if ($chat_id == '') {
		$wh = "incomming_call.id ='$incom_id'";
	} else {
		if ($_REQUEST['source'] == 'fbm') {
			$wh = "incomming_call.fb_chat_id = $chat_id";
		} elseif ($_REQUEST['source'] == 'mail') {
			$wh = "incomming_call.mail_chat_id = $chat_id";
		} elseif ($_REQUEST['source'] == 'viber') {
			$wh = "incomming_call.viber_chat_id = $chat_id";
		} elseif ($_REQUEST['source'] == 'fbc') {
			$wh = "incomming_call.fb_comment_id = $chat_id";
		} elseif ($_REQUEST['source'] == 'phone') {
			$wh = "incomming_call.asterisk_incomming_id = $chat_id";
		} else {
			$wh = "incomming_call.chat_id = $chat_id";
		}
	}

	$db->setQuery("SELECT    	incomming_call.id AS id,
                                incomming_call.`date` AS call_date,
        						incomming_call.`date` AS record_date,
        						IFNULL(asterisk_call_log.source,incomming_call.phone) AS `phone`,
                                incomming_call.ipaddres,
        						incomming_call.asterisk_incomming_id,
                                IF(NOT ISNULL(incomming_call.asterisk_incomming_id),
                                    CASE 
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) <= incomming_call.processing_start_date 
                    						THEN SEC_TO_TIME(UNIX_TIMESTAMP(processing_end_date)-UNIX_TIMESTAMP(processing_start_date))
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) > incomming_call.processing_start_date
                    						AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) >= incomming_call.processing_end_date
                    						THEN '00:00:00'
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) > incomming_call.processing_start_date
                    						AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) < incomming_call.processing_end_date 
                    						THEN SEC_TO_TIME(UNIX_TIMESTAMP(incomming_call.processing_end_date) - (UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time))
                    			    END,
                                '00:00:00') AS `processed_time`,
        						user_info.`name` AS operator,
        						IF(ISNULL(asterisk_call_log.user_id),chat_user_name.`name`,user_info.`name`) AS operator,
        						IF(ISNULL(asterisk_call_log.user_id),chat.answer_user_id,asterisk_call_log.user_id ) as `user_id`,
        						/* incomming_call.inc_status_id AS `inc_status`, */
        						incomming_call.cat_1,
        						incomming_call.cat_1_1,
                                incomming_call.cat_1_1_1,
								incomming_call.status_1,
								incomming_call.status_1_1,
								incomming_call.space_type,
								incomming_call.chat_language,
								incomming_call.inc_status,
								incomming_call.company,
        						incomming_call.call_content AS call_content,
        						incomming_call.source_id,
        						incomming_call.client_user_id AS personal_pin,
        						incomming_call.client_pid AS personal_id,
        						incomming_call.web_site_id,
        						incomming_call.client_name,

                                incomming_call.client_comment,
								incomming_call.out_comment,
                                incomming_call.vizit_location,
                                incomming_call.vizit_date,
                                incomming_call.fb_link,
                                incomming_call.viber_address,
                                incomming_call.phone1,

        						incomming_call.client_satus,
        						incomming_call.client_sex,
                                incomming_call.client_mail,
                                incomming_call.client_birth_year,
        						incomming_call.chat_id,
                                incomming_call.lid,
								incomming_call.note_comm,
                                incomming_call.lid_comment,
                                asterisk_call_log.source AS `ast_source`,
								asterisk_call_log.call_type_id AS `call_type`,
								outgoing_campaign_request_base.id AS 'base_id',
                                SEC_TO_TIME(asterisk_call_log.talk_time) AS `duration`,
			                    SEC_TO_TIME(asterisk_call_log.wait_time) AS `wait_time`,
                                chat.ip,
        						site_chat_id,
        						fb_chat_id,
								fb_comment_id,
        						mail_chat_id,
                                viber_chat_id,
                                messenger_chat_id,
                                video_call_id,
								asterisk_queue.client_account_id,
								asterisk_call_log.source AS `crm_phone`
                    FROM 	    incomming_call
                    LEFT JOIN   asterisk_call_log on asterisk_call_log.id = incomming_call.asterisk_incomming_id
					LEFT JOIN 	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
                    LEFT JOIN   users ON users.id=asterisk_call_log.user_id
					LEFT JOIN	asterisk_queue ON	asterisk_call_log.queue_id = asterisk_queue.id
                    LEFT JOIN   user_info ON users.id = user_info.user_id
                    LEFT JOIN   chat on incomming_call.chat_id = chat.id
                    LEFT JOIN   users AS chat_user ON chat_user.id=chat.answer_user_id
                    LEFT JOIN   user_info AS chat_user_name ON chat_user.id=chat_user_name.user_id
					WHERE      	$wh 
					ORDER BY incomming_call.id DESC
					LIMIT 1");

	$req = $db->getResultArray();
	$res = $req[result][0];
	return $res;
}

function get_address($id)
{

	global $db;

	$db->setQuery("	SELECT  `directory__projects`.`address`
					FROM   `incomming_call_info`
					JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
					JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
					WHERE	`incomming_call`.`id` = $id
					GROUP BY  `directory__projects`.`address`");
	$res = $db->getResultArray();
	$list = '';

	if (count($res['result']) == 0) {
		$list = "<span class='empty-list'>სია ცარიელია</span>";
	}

	foreach ($res['result'] as $a) {
		if (!empty($a['address'])) {
			$list .= "<li>" . $a['address'] . "</li>";
		}
	}

	return $list;
}

function GetPage($res = '', $number, $imby, $ipaddres, $ab_pin, $optData = '', $dialogType)
{
	global $db;

	if ($res[id] == '') {
		if($number[0] == 0){
			$number = substr($number, 1);
		}
		
		if ($dialogType == 'outgoing') {
			$db->setQuery("	SELECT	id
						FROM 	incomming_call
						WHERE 	phone = '$number' 
						ORDER BY id DESC
						LIMIT 	1");
			$outgoing_inc_id = $db->getResultArray();
			$outgoing_inc_id = $outgoing_inc_id['result'][0]['id'];
			$hidde_inc_id = $outgoing_inc_id;

			$processing_start_date = date('Y-m-d H:i:s');
		} else {
			$user_id = $_SESSION["USERID"];
			#IN(101,102,137,130,140,139)

			if ($dialogType != 'autodialer'){
    			$db->setQuery("INSERT INTO incomming_call
    								   SET user_id = '$_SESSION[USERID]',
    								   processing_start_date = NOW(),
    								   date = NOW(),
    								   source_id = 1,
    								   manualy_added = 1");
    			
    			$db->execQuery();
    			$hidde_inc_id = $db->getLastId();
    			$hidde_incomming_call_id_new = $hidde_inc_id;
    			$res["id"] = $hidde_inc_id;
    			$processing_start_date = date('Y-m-d H:i:s');
    			//$db->setQuery("DELETE FROM incomming_call WHERE id = '$hidde_inc_id'");
    			//$db->execQuery();
				
    			$processing_start_date = date('Y-m-d H:i:s');
				if($user_id != 1 && $user_id != 101 && $user_id != 102 && $user_id != 137 && $user_id != 130 && $user_id != 140 && $user_id != 139){
					return null;
				}
			}
		}
	} else {
		$hidde_inc_id = $res[id];
		// $db->setQuery("UPDATE incomming_call
	    //                   SET processing_start_date = NOW()
        //                WHERE (ISNULL(processing_end_date) OR processing_end_date = '') 
        //                AND    id = '$hidde_inc_id'");
		// $db->execQuery();
	}

	$db->setQuery("SELECT    COUNT(*) AS `unrid`
                   FROM      queries
                   LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$_SESSION[USERID]'
                   WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
	$res_queries = $db->getResultArray();

	if ($res_queries[result][0][unrid] > 0) {
		$queries_display = "";
		$queries_count   = $res_queries[result][0][unrid];
	} else {
		$queries_display = "display:none";
		$queries_count   = '';
	}

	$phone = $_REQUEST['phone'];
	$num   = 0;
	$lid_check = '';
	if ($res[phone] == "") {
		if ($number == '') {
			$num = $phone;
		} else {
			$num = $number;
		}
	} else {
		$num = $res[phone];
	}
	$lid_comment_hidde = "display:none;";
	if ($res[id] != '') {
		$dis = 'disabled="disabled"';
	} else {
		$dis = '';
	}
	if ($res[lid] == 1) {
		$lid_check = 'checked';
		$lid_comment_hidde = '';
	}
	if ($ab_pin == '' || $res[mail_chat_id] > 0) {
		$ab_pin = $res[personal_pin];
	}

	$person_pin = $ab_pin;

	$crm_count = 0;
	if ($person_pin != '' || $num != '') {

		$db->setQuery("SELECT 	  COUNT(*) AS `count`  
                       FROM 	  outgoing_compaign_request ar 
                       LEFT JOIN  outgoing_compaign_request_details ard ON ar.id = ard.request_id
                       LEFT JOIN  aoutgoing_compaign_base arb ON arb.request_id = ard.id
                       WHERE      ar.call_type_id = 4 AND ar.actived=1 AND ard.actived = 1 
                       AND       ((arb.number_2 = '$person_pin' AND arb.number_2 != '')  OR (arb.phone_number = '$num' AND arb.phone_number != ''))
                       AND        DATE(ard.set_time) >=CURDATE() - INTERVAL 7 DAY AND DATE(ard.set_time) <= CURDATE()");

		//$check_crm_count = $db->getResultArray();
		$crm_count       = $check_crm_count[result][0][count];
	}

	if ($crm_count == 0) {
		$crm_display = 'display:none;';
	} else {
		$crm_display = '';
	}
	$person_name = $res['client_name'];
	$person_user = $res['personal_pin'];

	if ($res['person_name'] == '') {

		$db->setQuery(" SELECT   `name` AS `name`,
                                 `pin`  AS `user_id`
                        FROM     `abonent_pin`
                        WHERE   (`pin` != '' OR `name` != '') AND phone = '$num' AND phone != 'Anonymous'
                        ORDER BY  id DESC
                        LIMIT 1");

		$pers_info   = $db->getResultArray();
		$person_name = $pers_info[result][0][name];
		$person_user = $pers_info[result][0][user_id];
	}

	$db->setQuery("SELECT  `comment`
				   FROM    `black_list`
				   WHERE    phone = '$num' AND phone != 'Anonymous' AND `comment` != ''
				   ORDER BY id DESC
				   LIMIT 1");

	$req                 = $db->getResultArray();
	$last_blocked_reason = $req[result][0];

	$db->setQuery("SELECT  `id`,
                           `name`,
						   `rand_name`,
                           `file_date`
				   FROM    `file`
				   WHERE   `incomming_call_id` = '$res[id]'");

	$increm = $db->getResultArray();

	$db->setQuery(" SELECT     IF(ISNULL(user_info.`name`),'ოპერატორი',user_info.`name`) AS `name`,
                               IFNULL(extention.extention,0) AS last_extension
                    FROM      `users`
                    LEFT JOIN  extention ON users.extension_id = extention.extention
                    LEFT JOIN `user_info` ON `user_info`.`user_id` = users.id
                    WHERE      users.`id` = '$_SESSION[USERID]'");

	$req     = $db->getResultArray();
	$getuser = $req[result][0];

	if ($number == '') {
		if ($res['chat_id'] != 0 && $res['chat_id'] != '' &&  $res['chat_id'] != null) {
			$imchat = 1;
		} else {
			$imchat = 0;
		}
	} else {
		$imchat = 0;
	}

	if ($res['asterisk_incomming_id'] != 'null' && $res['asterisk_incomming_id'] != null) {
		$chatcom = 'display:block;';
		$callcom = 'display:none;';
	} else {
		$chatcom = 'display:none;';
		$callcom = 'display:block;';
	}

	if ($_REQUEST[chat_id] == '' || $_REQUEST[site_chat_id] != '') {
		$mychat_id = 0;
	} else {
		$mychat_id = $_REQUEST[chat_id];
	}

	$transfer_resps = 0;
	if ($res['chat_id'] != '' || $res['chat_id'] != 0) {
		$mychat_id = $res['chat_id'];
		$transfer_resps = 0;
	}
	$transfer_resps = 0;
	$chat_source = $_REQUEST["source"];
	if ($res['fb_chat_id']) {
		$chat_source = 'fbm';
		$mychat_id = $res['fb_chat_id'];
		$transfer_resps = 0;
	}
	if ($res['fb_comment_id']) {
		$chat_source = 'fbc';
		$mychat_id = $res['fb_comment_id'];
		$transfer_resps = 0;
	}
	if ($res['mail_chat_id']) {
		$chat_source = 'mail';
		$mychat_id = $res['mail_chat_id'];
		$transfer_resps = 0;
	}
	if ($res['viber_chat_id']) {
		$chat_source = 'viber';
		$mychat_id = $res['viber_chat_id'];
		$transfer_resps = 0;
	}

	$sms = '';

	$chat_disable = '';
	$chat_select = '';
	$border = 'border-bottom: 2px solid #333 !important;';

	if ($chat_source == "fbc") {
		$db->setQuery("	SELECT	message, time
						FROM	fb_feeds 
						WHERE	id = (SELECT feeds_id FROM fb_comments WHERE id = '" . $mychat_id . "')");

		$post_arr = $db->getResultArray();

		$post = '<div class="chat_fb_comment_post"><div class="chat_fb_coment_post_title"><span style="font-size: 11px;color: #878787;text-align: right;">' . $post_arr[result][0][time] . '</span></div><div class="chat_fb_comment_post_inner"> ' . $post_arr[result][0][message] . '</div></div>';
	} else {
		$post = '';
	}



	if ($res[chat_id] > 0) {
		$db->setQuery(" SELECT  chat.`name`,
                                chat.`device`,
                                chat.`browser`,
                                chat.`ip`,
                                chat_details.message_client,
                                chat_details.message_operator,
                                chat_details.message_datetime,
                                IF(ISNULL(chat_nikname.`name`),'ოპერატორი',chat_nikname.name) AS `op_name`,
                                chat_details.id
                        FROM `chat`
                        LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
                        LEFT JOIN `chat_nikname` ON chat_nikname.crystal_users_id = chat_details.operator_user_id AND chat_nikname.actived = 1
                        WHERE `chat`.id = $res[chat_id]
	                    ORDER BY chat_details.message_datetime");

		$res1           = $db->getResultArray();
		$chat_detail_id = 0;

		$db->setQuery("SELECT  `chat_details`.`id`
            	       FROM    `chat_details`
            	       WHERE   `chat_details`.`chat_id` = '$res[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]' 
					   ORDER BY chat_details.id DESC LIMIT 1");

		$req            = $db->getResultArray();
		$ge             = $req[result][0];
		$chat_detail_id = $ge[id];

		foreach ($res1[result] as $req) {
			if (strpos($req[message_client], 'შემოუერთდა ჩატს.') === FALSE) {
				$name = $req[op_name];
			} else {
				$name = '';
			}
			if ($req[message_client] == '') {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[op_name] . '</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">adsa' . $req[message_operator] . '</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">' . $req[message_datetime] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			} else {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                            <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">' . $name . '</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[message_client] . '</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">' . $req[message_datetime] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			}
		}

		if ($imby != '1') {
			$chat_disable = 'disabled';
			$chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
			$border = 'display:none;';
		}

		$db->setQuery(" SELECT	IF(who_rejected = 1, 'ოპერატორმა გათიშა', 'აბონენტმა გათიშა') AS rejecter
						FROM   `chat`
						WHERE  `answer_user_id` != 0 AND `status` > 2 
						AND     id               = '$res[chat_id]'");

		$rejecter_res = $db->getResultArray();
		$rejecter     = $rejecter_res[result][0]['rejecter'];

		$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">' . $rejecter . '</td></tr>';
	}

	if ($res[fb_chat_id] > 0) {
		$db->setQuery("SELECT     fb_chat.last_user_id,
								  fb_messages.datetime,
								  fb_chat.sender_name AS `name`,
								  fb_messages.text,
								 (SELECT url from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media,
								  fb_messages.id,
								  IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_messages.user_id),'') as operator,
                                  (SELECT type from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media_type
					   FROM      `fb_chat`
					   LEFT JOIN `fb_messages` ON fb_chat.id = fb_messages.fb_chat_id# AND chat_details.message_operator != 'daixura'
					   WHERE     `fb_chat`.id = $res[fb_chat_id]
	                   ORDER BY   fb_messages.datetime");

		$res1           = $db->getResultArray();
		$chat_detail_id = 0;

		foreach ($res1[result] as $req) {
			if ($req[media_type] == 'fallback') {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'file') {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'video') {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} else {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
			}

			if ($req[operator] == '') {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">' . $req[name] . '</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			} else {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[operator] . '</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">' . $req[text] . '</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			}
			$chat_detail_id = $req['id'];
		}
		if ($imby != '1') {
			$chat_disable = 'disabled';
			$chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
			$border       = 'display:none;';
		}
	}


	if ($res[fb_comment_id] > 0) {
		$db->setQuery("SELECT     fb_comments.last_user_id,
								  fb_comments.time,
								  fb_comments.fb_user_name AS `name`,
								  fb_comments.message,
								 (SELECT url from fb_attachments WHERE messages_id=fb_comments.id LIMIT 1) as media,
								  fb_comments.id,
								  IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_comments.user_id),'') as operator,
                                  (SELECT type from fb_attachments WHERE messages_id=fb_comments.id LIMIT 1) as media_type
					   FROM      `fb_comments`
					   WHERE     `fb_comments`.id = $res[fb_comment_id]
	                   ORDER BY   fb_comments.time");

		$res1           = $db->getResultArray();
		$chat_detail_id = 0;

		foreach ($res1[result] as $req) {
			if ($req[media_type] == 'fallback') {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'file') {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'video') {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} else {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
			}

			if ($req[operator] == '') {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">' . $req[name] . '</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			} else {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[operator] . '</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">' . $req[text] . '</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			}
			$chat_detail_id = $req['id'];
		}
		if ($imby != '1') {
			$chat_disable = 'disabled';
			$chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
			$border       = 'display:none;';
		}
	}


	if ($res[mail_chat_id] > 0) {

		$db->setQuery("SELECT 		mail.user_id,
									mail_detail.datetime,
									IF(mail.sender_name = '', mail.sender_address, mail.sender_name) AS `name`,
									mail_detail.body,
									mail_detail.id,
									IFNULL((SELECT `name` from user_info WHERE user_info.user_id = mail_detail.user_id),'') as operator,
									mail.subject

						FROM      `mail`
						LEFT JOIN `mail_detail` ON mail.id = mail_detail.mail_id
						WHERE     `mail`.id = '$res[mail_chat_id]'
						ORDER BY   mail_detail.datetime");

		$res1           = $db->getResultArray();
		$chat_detail_id = 0;
		foreach ($res1[result] as $req) {

			$db->setQuery("SELECT `name` FROM `mail_attachment` WHERE mail_detail_id='$req[id]'");
			$fq          = $db->getResultArray();
			$req['text'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req['text']);

			foreach ($fq[result] as $file) {
				if ($file[check_mesage] == 1) {
					$req['text']      = $req[text] . "<a href='$file[name]' target='_blank'>$file[name]</a><br>";
				} else {
					$req['text'] = preg_replace('/(?<=cid:' . $file[name] . ').*?(?=>)/', $file[name] . '"',  $req['text']); //preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
					$req['text'] = str_replace('cid:' . $file[name], 'https://crm.my.ge/', $req['text']);
					$file[patch]    = str_replace('/var/www/html/', '', $file[name]);
					$req['text']     = $req['text'] . "<a href='https://crm.my.ge/$file[name]'>$file[name]</a><br>";
				}
			}

			$subject = '';
			if ($req[subject] != '') {
				$subject = 'თემა: ' . $req[subject];
			}

			if ($req[operator] == '') {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold; font-size: 12px;">' . $req[name] . '</p>
                                    <p style="font-weight: bold; font-size: 12px;">' . $subject . '</p>
                                    <p style="padding-bottom: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                    <div style="padding-top: 7px;">
                                        <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			} else {
				$sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[operator] . '</p>
                                    <p style="padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[text] . '</p>
                                    <div style="text-align: right; padding-top: 7px;">
                                        <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
			}
			$chat_detail_id = $req['id'];
		}


		$db->setQuery("	SELECT     mail.sender_address AS `name`
						FROM      `mail`
						LEFT JOIN `mail_detail` ON mail.id = mail_detail.mail_id
						WHERE     `mail`.id = '$res[mail_chat_id]'");

		$mail_info = $db->getResultArray();

		if ($res[client_mail] == '') {
			$mail_name = $mail_info[result][0][name];
		} else {
			$mail_name = $res[client_mail];
		}

		if ($imby != '1') {
			$chat_disable = 'disabled';
			$chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
			$border = 'display:none;';
		}
	}


	$display_standart_amount_action = "block";
	$display_in_out_amount_action   = "none";

	$chat_source_id = $res['source_id'];

	$db->setQuery("	SELECT `key`
					FROM	`source`
					WHERE	`id` = '$chat_source_id'");
	$sourceData = $db->getResultArray();
	$chat_source = $sourceData['result'][0]['key'];

	if ($chat_source == '') {
		$chat_source = $_REQUEST["source"];
	}
	$client_id = $res['client_account_id'];

	if ($client_id) {
		$db->setQuery("	SELECT 	`id`, `name` 
						FROM	`client_accounts`
						WHERE	`id` = $client_id");
		$client_name = $db->getResultArray();
	}

	if ($res['chat_id'] != '') {
		$chat_id = $res['chat_id'];
	} elseif ($res['fb_chat_id'] != '') {
		$chat_id = $res['fb_chat_id'];
	} elseif ($res['mail_chat_id'] != '') {
		$chat_id = $res['mail_chat_id'];
	} elseif ($res['messenger_chat_id'] != '') {
		$chat_id = $res['messenger_chat_id'];
	} elseif ($res['viber_chat_id'] != '') {
		$chat_id = $res['viber_chat_id'];
	} elseif ($res['video_call_id'] != '') {
		$chat_id = $res['video_call_id'];
	} elseif ($res['fb_comment_id'] != '') {
		$chat_id = $res['fb_comment_id'];
	}

	if ($chat_id == 0) {
		$chat_id = $_REQUEST['chat_id'];
	}


	if ($chat_source) {
		$db->setQuery("	SELECT 	id, name
						FROM	source
						WHERE 	`key` = '$chat_source'	");
		$source_res = $db->getResultArray();
		$chat_source_a_id = $source_res[result][0][id];
		$chat_source_a = $source_res[result][0][name];
	} else {
		$chat_source_a = "Source not found";
	}


	if ($chat_source == "fbm" && $chat_id != '') {
		$db->setQuery("	SELECT 	fb_account.`name`, fb_account.`site_id`
						FROM 	fb_account 
						JOIN 	fb_chat ON `fb_chat`.`id` = $chat_id AND `fb_chat`.`account_id` = `fb_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0]['name'];
		$chat_site_id = $result_source[result][0]['site_id'];
	} elseif ($chat_source == "viber" && $chat_id != '') {
		$db->setQuery("	SELECT 	viber_account.`name`, viber_account.`site_id`
						FROM 	viber_account 
						JOIN 	viber_chat ON `viber_chat`.`id` = $chat_id AND `viber_chat`.`account_id` = `viber_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0]['name'];
		$chat_site_id = $result_source[result][0]['site_id'];
	} elseif ($chat_source == "mail" && $chat_id != '') {
		$db->setQuery("	SELECT 	mail_account.`name`
						FROM 	mail_account 
						JOIN 	mail ON `mail`.`id` = $chat_id AND `mail`.`account_id` = `mail_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0]['name'];
		$chat_site_id = $result_source[result][0]['site_id'];
	} elseif ($chat_source == 'chat') {
		$chat_site	= "SOLUM";
	} elseif ($client_name != '') {
		$chat_site = $client_name['result'][0]['name'];
		$chat_site_id = $client_name['result'][0]['id'];
	} else {
		$chat_site = "ID not found";
	}

	$optData = array('chat_source_a_id' => $chat_source_a_id, 'chat_source_a' => $chat_source_a, 'chat_site' => $chat_site, 'chat_site_id' => $chat_site_id, 'chat_source' => $chat_source);



	$data  .= '
	<div id="dialog-form">

    	<div class="dialog-style">
    		<input type="hidden" value="' . $imchat . '" id="imchat">
    		<input type="hidden" value="' . $res[fb_chat_id] . '" id="fb_chat">
            <input type="hidden" value="' . $res['mail_chat_id'] . '" id="mail_chat">
            <input type="hidden" value="' . $res['viber_chat_id'] . '" id="viber_chat">
            <input type="hidden" value="' . $res['source_id'] . '" id="source_id">
    	    <input type="hidden" value="' . $_SESSION[USERID] . '" id="chat_user_id" chat_user_name="' . $getuser[name] . '" ext="' . $getuser[last_extension] . '">
			<fieldset class="communication_chatsys" id="chatistema">
			
    			<div style="float:left;width: 100%;">
        	    	<table class="dialog-form-table" style="margin-bottom:20px;" width="100%" id="main_call_chat">
                		<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
        	            	<td colspan="2" ><span style="display:block;font-size: 12px;margin-bottom: 7px;">ზარი</span></td>
        	        	</tr>
        	        	<tr style="height: 27px;text-align: left;border-bottom: 1px solid #666;">
        	            	<td>' . $getuser[last_extension] . '</td>
                    		<td style="text-align: right;"><img id="chat_call_live" src="media/images/icons/flesh_free.png" alt="ლაივი" title="მიმდინარე ჩატი" width="15px" style="border:0 !important;padding: 0;"></td>
        	        	</tr>
        	        	<tr id="chat_call_queue" style="height: 27px;text-align: left;border-bottom: 1px solid #666;display:none;">
        	            	<td style="cursor:pointer;" id="chat_call_number" class="open_dialog" check_save_chat="1" extention="" number="" ipaddres=""></td>
                        	<td id="chat_call_duration" style="text-align: right;"></td>
        	        	</tr>
        	    	</table>';

	$db->setQuery("SELECT id FROM `chat` WHERE `status` = 1");
	$req = $db->getNumRow();

	$data  .= '
    
    				<div style="max-height: 620px;" class="communication_chatsys_aloo" id="aloo">
    				
    					<div id="incoming_chat_tabs">
    					<table class="chatsys_table">
    					<tr>
    					<td class="chatsys_tabs">
    					<ul class="chatsys_style">						
    							' . Getcomunication_tab() . '
    							</ul>
    					</td>
						<td class="chatsys_list">
						
						
						<div id="incoming_chat_tab_phone" style="max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ზარი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_phone1" class="chat_tab_style" style="height: 427px; overflow-y: auto;"></div>
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_phone_queue" style="padding: 9px 6px; background: #F8F8F8; border: 1px solid #DEDEDE; border-radius: 6px;height: 186px; overflow-y: auto;"></div>
    				 </div>


    					<div id="incoming_chat_tab_chat" style="max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    					
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ჩატი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_chat1" class="chat_tab_style" style="height: 427px;overflow-y: auto;" aria-actived="false"></div>
    
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_chat_queue" style="padding: 9px 6px; background: #F8F8F8; border: 1px solid #DEDEDE; border-radius: 6px;height: 186px; overflow-y: auto;"></div>
    					</div>
     
    
    					<div id="incoming_chat_tab_mail" style="max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ჩატი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_mail1"  class="chat_tab_style" style="height: 427px; overflow-y: auto;"></div>
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_mail_queue" style="padding: 9px 6px; background: #F8F8F8; border: 1px solid #DEDEDE; border-radius: 6px;height: 186px; overflow-y: auto;"></div>
					 </div>
					 
    				 <div id="incoming_chat_tab_fbm" style="max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ჩატი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_fbm1" class="chat_tab_style" style="height: 427px; overflow-y: auto;"></div>
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_fbm_queue" style="padding: 9px 6px; background: #F8F8F8; border: 1px solid #DEDEDE; border-radius: 6px;height: 186px; overflow-y: auto;"></div>
    				 </div>
    
    				<div id="incoming_chat_tab_fbc" style="margin-left: 3px; max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ჩატი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_fbc1" class="chat_tab_style" style="height: 427px; overflow-y: auto;"></div>
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    				<div id="incoming_chat_tab_fbc_queue" style="padding: 9px 6px; background: #F8F8F8; border: 1px solid #DEDEDE; border-radius: 6px;height: 186px; overflow-y: auto;"></div>
    				</div>
    				
    				 <div id="incoming_chat_tab_viber" style="max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ჩატი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_viber1" class="chat_tab_style" style="height: 427px; overflow-y: auto;"></div>
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_tab_viber_queue" style="padding: 9px 6px; background: #F8F8F8; border: 1px solid #DEDEDE; border-radius: 6px;height: 186px; overflow-y: auto;"></div>
    				 </div>
    				 <div id="incoming_chat_tab_video" style="max-height: 693px; width: 250px" class="chat_layouts">
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin-bottom: 7px;">აქტიური ჩატი</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_video1" class="chat_tab_style" style="height: 427px; overflow-y: auto;"></div>
    					<table class="dialog-form-table" width="100%">
    						
    						<tr style="height: 10px;text-align: left;font-weight: bold;border-bottom: 1px solid #666;">
    							<td><span style="display:block;font-size: 12px;margin: 11px;">რიგშია</span></td>
    						</tr>
    					</table>
    					<div id="incoming_chat_video_queue" style="height: 186px; overflow-y: auto;"></div>
    				</div>
    					</td>
    					</tr>
    					</table>
    						
        				</div>				
        			</div>
        	    </div>
    		</fieldset>
    	    <div class="communication_chat_style" id="gare_div">
    	        <fieldset class="chat_top">
    	           <table id="blocklogo" >
                        <tr>
                            <td>
								<img id="chat_live" src="media/images/icons/lemons_chat.png" alt="ლაივი" title="მიმდინარე ჩატი" width="20px" style="filter: brightness(0.1);  border-bottom: 2px solid #333 !important;">
								
                                <img id="chat_hist" src="media/images/icons/history.png" alt="ისტორია" title="ისტორია" width="20px" style="filter: brightness(0.1);">
                    
								<img id="chat_flag" src="media/images/icons/georgia.png" alt="ქვეყანა" title="ქვეყანა" width="20px">
								
								<img id="chat_browser" src="media/images/icons/chrome.png" alt="ბრაუზერი" title="ბრაუზერი" width="20px">
								
                    	        <img id="chat_device" src="media/images/icons/windows.png" alt="სისტემა" title="სისტემა" width="20px">
                    
                    	        <img id="user_block" ip="' . $res[ip] . '" src="media/images/icons/block.png" alt="block" title="დაბლოკვა" width="20px" class="chat_icon_right">
                                <img id="chat_expand" expanded="0" src="media/images/icons/chat_expander.png" alt="chat_expand" title="ჩატის გახსნა" width="20px" style="cursor:pointer;" class="chat_icon_right">
								<img id="chat_end" ip="' . $res[ip] . '" src="media/images/icons/chat_end.png" alt="chat_end" title="ჩატის დახურვა" width="20px" class="chat_icon_right">
								
                                
                                <input id="choose_file" name="choose_file" type="file" class="file" style="display: none;">
                            </td>
                        </tr>
                    </table>
    	        </fieldset>
                <fieldset class="communication_chat">
                   <!-- <span check_zoom="0" id="zomm_div" style="float: right; cursor: pointer;">>></span> -->
                    <div style="float:left;" id="chatcontent" class="l_chatcontent">
                        <div class="chat_scroll" id="chat_scroll">
						   <table style="width: 100%;" id="log">
						   ' . $post . '
                            ' . $sms . '
						   </table>
						   <div style="display:flex;flex-direction: column;">
						   		<span id="span_seen" style="display:none;text-align: end; float:right; margin-right:5px;">&#10004; seen</span>
						   		<span id="chat_close_text" style="display:none; text-align: end; float:right; margin-right:5px;">მომხმარებელმა დატოვა ჩატი</span>
							</div>
						</div>
						<span id="chat_typing"> ... ბეჭდავს</span>
						<div id="addr_to_add">
							<div class="row_2">
								<label style="vertical-align: baseline;">ადრესატები:</label>
								<select id="mail_addresats" style="width: 180px !important; font-size: 12px;"></select>
							</div>
							<div class="row_2">
								<label style="vertical-align: baseline;">CC:</label>
								<select id="mail_cc" style="width: 180px !important; font-size: 12px;left: 56px;"></select>
							</div>
							<div class="row_2">
								<label style="vertical-align: baseline;">BCC:</label>
								<select id="mail_bcc" style="width: 180px !important; font-size: 12px;left: 49px;"></select>
							</div>
						</div>
						<div style="padding-left:1px; padding-right:1px; margin-top:18px;">
							<a id="add_new_addr" style="padding: 10px;border: 1px solid #18dc18; color: #18dc18;border-radius: 6px;" href="#"><i class="fa fa-plus"></i> ადრესატები</a>
						</div>
                        <div id="email_attachment_files1">
                        </div>
                	    <div style="border-radius: 12px;margin-top:14px;overflow: hidden;border: 2px solid #DBDBDB;position:relative; height: 207px; margin-bottom: 8px">
							<div id="emoji_wrapper"><span id="emoji_close" code="0" >დახურვა</span>' . get_emoji() . '</div>
                            <button id="send_chat_program" data-edit="0" data-id></button>
							<img id="dialog_emojis" src="media/images/icons/smiling-emoticon-square-face.png" width="20px" height="20px" alt="">
							<button id="choose_button5"><i class="fas fa-paperclip"></i></button>
							<button id="choose_docs"><img style="height:20px;" src="media/images/icons/documents.png"></button>
							<select id="signatures" style="display:none;">
								' . get_signatures() . '

							</select>
							<div id="close_edit" style="display: none; position: absolute;bottom: 1px;left: 9px;font-size: 12px;border: none;background: transparent;color: #999999;cursor: pointer;">X დახურვა</div>
							<div id="email_attachment_files"></div> 
							
							<div ' . $chat_disable . '  contenteditable="true" placeholder="შეიყვანეთ ტექსტი" id="send_message" style="padding: 0 8px; border:0px;width:85%;height:120px;resize:none;overflow-y: auto;"></div>
                    		<input type="hidden" value="' . $mychat_id . '" id="chat_original_id">
                    		<input type="hidden" value="' . $chat_source . '" id="chat_source">
                            <input type="hidden" value="' . ($chat_detail_id == '' ? '0' : $chat_detail_id) . '" id="chat_detail_id">
							<div id="start_chat_wrap"><button id ="start_chat" >ჩატის დაწყება</button></div>
						</div>
                    </div>
                </fieldset>
			</div>
			<div class="communication_fieldset_style">
                 <div id="side_menu" class="communication_side_menu">
        			<span class="info communication_side_menu_button" onclick="show_right_side(\'info\')"><img class="communication_side_menu_img"  style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/lemons_info.png" alt="24 ICON" title="ინფო"></span>
              
                    <span class="client_history communication_side_menu_button"  onclick="show_right_side(\'client_history\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_client_history.png" alt="24 ICON"  title="ისტორია"></span>
                    				
                    <span class="task communication_side_menu_button"  onclick="show_right_side(\'task\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_task.png" alt="24 ICON"  title="დავალება"></span>

                    <span class="client_conversation communication_side_menu_button"  onclick="show_right_side(\'client_conversation\')"><img class="communication_side_menu_img" src="media/images/icons/client_conversation.png" alt="24 ICON"  title="დავალება"></span>

    				<span class="sms communication_side_menu_button" onclick="show_right_side(\'sms\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_sms.png" alt="24 ICON" title="ესემეს"></span>
    				
    				<span class="incomming_mail communication_side_menu_button" onclick="show_right_side(\'incomming_mail\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_mail.png" alt="24 ICON" title="ელ. ფოსტა"></span>
    
                    <span class="chat_question communication_side_menu_button" onclick="show_right_side(\'chat_question\')"><img class="communication_side_menu_img" src="media/images/icons/chat_question.png" alt="24 ICON" title="კომენტარები"></span>
    			    
                    <span style="display:none" class="newsnews communication_side_menu_button" onclick="show_right_side(\'newsnews\')"><img class="communication_side_menu_img" src="media/images/icons/newsnews.png" alt="24 ICON" title="სიახლეები"></span>
                    
                    <span class="record communication_side_menu_button" onclick="show_right_side(\'record\')"><img class="communication_side_menu_img" src="media/images/icons/inc_record.png" alt="24 ICON"  title="ჩანაწერები"></span>
                        				
                    <span class="incomming_file communication_side_menu_button" onclick="show_right_side(\'incomming_file\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_files.png" alt="24 ICON"  title="ფაილები"></span>
    				
                    <span class="user_logs communication_side_menu_button" onclick="show_right_side(\'user_logs\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_logs.png" alt="24 ICON"  title="ლოგები"></span>

                    <span style="float: right; margin-right: 10px;" class="call_resume communication_side_menu_button" onclick="show_right_side(\'call_resume\')"><img class="communication_side_menu_img" src="media/images/icons/inc_resume.png" alt="24 ICON"  title="ლოგები"></span>
                   
    			 </div>
			 </div>
			 <div class="communication_right_side" id="right_side">
				<ff class="communication_right_side_info" id="info">';


	$tt = new automator();
	if ($dialogType == 'autodialer') {
		if ($number == '') {
			$number = $res['phone'];
		}
		$baseID = explode('-', $number);

		$db->setQuery("	SELECT		outgoing_campaign_request.outgoing_campaign_category_id AS 'cat_id'
										FROM 		outgoing_campaign_request_base
										LEFT JOIN 	outgoing_campaign_request ON outgoing_campaign_request.id = outgoing_campaign_request_base.outgoing_campaign_request_id
										WHERE 		outgoing_campaign_request_base.id = '$baseID[1]'");
		$category_id = $db->getResultArray();
		$category_id = $category_id['result'][0]['cat_id'];

		if ($category_id == 5) {
			$data .= '
							<fieldset class="communication_right_side_info">
								<legend>კვლევა</legend>
								<div class="fieldset_row">';
			$data .= $tt->getCampaignInputs($baseID[1]);
			$data .= '</div>
							</fieldset>';
		}

		$phone2 = substr($res['phone'], -12);
		if ($phone2 == '') {
			$phone2 = substr($number, -12);
		}
		$db->setQuery(" SELECT  id,
												number_1,
												number_2,
												number_3,
												number_4,
												number_5,
												number_6,
												number_7,
												number_8,
												number_9,
												number_10,
												outgoing_campaign_request_id AS 'request_id'
										FROM    outgoing_campaign_request_base 
										WHERE   phone_number = '$phone2'
										ORDER BY id DESC
										LIMIT   1");
		$processing_id = $db->getResultArray();
		$req_id = $processing_id['result'][0]['request_id'];
		if ($processing_id['result'][0]['number_1'] != '') {
			$data .= '
							<fieldset class="communication_right_side_info">
									<legend>კამპანიის ინფო</legend>
									<div class="fieldset_row">';


			$db->setQuery("	SELECT name
													FROM outgoing_campaign_request_voice
													WHERE request_id = '$req_id' AND actived = 1
													ORDER BY id ASC");
			$fields = $db->getResultArray();
			$i = 1;
			foreach ($fields['result'] as $item) {
				if ($i == 5 or $i == 6) {
					$data .= '	<div class="col-12">
															<label>' . $item[name] . '</label>
															<textarea data-nec="0" style="resize: vertical;width: 97%;height: 60px;" class="idle" name="call_content">' . $processing_id[result][0]['number_' . $i] . '</textarea>
														</div>';
				} else {
					$data .= '	<div class="col-4">
															<label>' . $item[name] . '</label>
															<input value="' . preg_replace('/"/', '', $processing_id[result][0]['number_' . $i]) . '" data-nec="0" style="height: 18px; width: 95%;" type="text" class="idle" autocomplete="off" disabled>
														</div>';
				}

				$i++;
			}
			$data .= '</div>
							</fieldset>';
		}

		$data .= $tt->getDialog($res[base_id], $res['phone'], 'compaign_request_processing', 'base_id', 1);
	} else if ($res['call_type'] == 5) {
		$data .= $tt->getDialog($res[base_id], $res['phone'], 'compaign_request_processing', 'base_id', 1);
	} else if ($dialogType == 'outgoing') {

		$data .= $tt->getDialog($res[id], $res['phone'], 'incomming_request_processing', 'incomming_request_id', 1);
	} else {
		$data .= $tt->getDialog($res[id], $res['phone'], 'incomming_request_processing', 'incomming_request_id', 1);
	}

	/* <fieldset class="communication_right_side_info">
                        <table width="100%" class="dialog-form-table">
                            <legend>მომრთვა</legend>
                            <tr style="height:0px">
    							<td colspan="2" style="width: 196px;"><label for="d_number">მომართვის კატეგორია</label></td>
                            </tr>
    						<tr style="height:0px" class="ff_till">
                                <td colspan="2"><select  id="incomming_cat_1" style="width: 98%; height: 18px; ">'.get_cat_1($res['cat_1']).'</select></td>
                            </tr>
                            <tr style="height:0px">
    							<td style="width: 50%;"><label for="d_number">ქვე-კატეგორია 1</label></td>
    							<td style="width: 50%;"><label for="d_number">ქვე-კატეგორია 2</label></td>
                            </tr>
    						<tr style="height:0px" class="ff_till">
                                <td><select  id="incomming_cat_1_1"   style="width: 96%; height: 18px; ">'.get_cat_1_1($res['cat_1'],$res['cat_1_1']).'</select></td>
                                <td><select  id="incomming_cat_1_1_1" style="width: 96%; height: 18px; ">'.get_cat_1_1_1($res['cat_1_1'],$res['cat_1_1_1']).'</select></td>
							</tr>
							<tr style="height:0px">
							<td style="width: 50%;"><label for="d_number">მომართვის სტატუსი</label></td>
							<td style="width: 50%;"><label for="d_number">მომართვის ქვე-სტატუსი</label></td>
						</tr>
						<tr style="height:0px" class="ff_till">
							<td><select  id="incomming_status_1" style="width: 96%; height: 18px; ">'.get_status_1($res['status_1']).'</select></td>
							<td><select  id="incomming_status_1_1"   style="width: 96%; height: 18px; ">'.get_status_1_1($res['status_1'],$res['status_1_1']).'</select></td>
						</tr>
							<tr style="height:0px">
								<td><label for="d_number">წყარო</label></td>
								<td><label for="d_number">ფართის ტიპი</label></td>
							</tr>
							<tr class="ff_till" style="height:0px">
							<td>
								<input id="chat_source_a" type="text" style="width: 96%; text-align: center" value="'.$optData[chat_source_a].'" disabled />
								<input id="chat_source_a_id" type="hidden" value="'.$optData[chat_source_a_id].'" />
							</td>
								<td><select id="space_type" style="width: 96%; height: 18px; ">'.get_space_type($res['space_type']).'</select></td>
							</tr>
							<tr style="height:0px">
								<td><label for="d_number">საიტი</label></td>
								<td><label for="d_number">რეაგირება</label></td>
							</tr>
							<tr style="height:0px" class="ff_till" style="height:0px">
								<td><input id="chat_site" type="text" style="width: 96%; text-align: center" value="'.$optData[chat_site].'" disabled />
								<input id="chat_site_id" type="hidden" value="'.$optData[chat_site_id].'" />
								</td>
								<td><select id="inc_status_a" style="width: 96%; height: 18px; ">'.get_inc_status($res['inc_status']).'</select></td>
							</tr>
							<tr style="height:0px">
								<td><label for="d_number">აქცია</label></td>
							</tr>
							<tr style="height:0px" class="ff_till" style="height:0px">
								<td><select id="company" style="width: 96%; height: 18px; ">'.get_company($res['company']).'</select></td>
							</tr>

						</table>
    				</fieldset> */
	$data .= '</ff>
                 
                 <ff style="display:none; height: 656px;" id="newsnews">
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                        <table class="display" id="table_news" >
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width:15%;">დასაწყისი</th>
                        			<th style="width:15%;">დასასრული</th>
                        			<th style="width:15%;">დასა-<br>ხელება</th>
                                    <th style="width:15%;">საიტი(ები)</th>
                        			<th style="width:40%;">შინაარსი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                    	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                    	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="client_history">
                    <table style="margin-left: 7px;">
                        <tr style="height:0px;">
                            <td colspan="2" style="text-align: center;">პერიოდი</td>
                            <td><label>ტელეფონი</label></td>
                            <td></td>
                        </tr>
                        <tr style="height:0px;">
                            <td style="width: 120px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_from" style="width: 110px;" value="' . date('Y-m-d', strtotime('-7 day')) . '"></td>
                            <td style="width: 120px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_to" style="width: 110px;" value="' . date('Y-m-d') . '"></td>
                            <td style="width: 140px;"><input class="callapp_filter_body_span_input" type="text" id="client_history_phone" style="width: 130px;" value="' . $person_pin . '"></td>
                            <td><button id="search_client_history" style="margin-top: -6px;">ძებნა</button></td>
                        </tr>
                    </table>
                
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                        <table class="display" id="table_history" style="width: 100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 10%;">თარიღი</th>
                					<th style="width: 10%">მომართვის ტიპი</th>
                					<th style="width: 20%;">ტელეფონი</th>
                                    <th style="width: 20%;">ოპერატორი</th>
									<th style="width: 20%;">კომენტარი</th>
									<th style="width: 20%;">სტატუსი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
									</th>
									<th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
        	                        <th>
                                        <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
              				</thead>
          				</table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="task">
                    <div id="task_button_area" class="margin_top_10" style="margin-left: 7px;"> 
                        <button id="add_task_button">ახალი დავალება</button>
                    </div>
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
        	            <table class="display" id="task_table" style="width: 100%;">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%">თარიღი</th>
                                    <th style="width: 20%;">დასაწყისი</th>
                                    <th style="width: 20%;">დასასრული</th>
                                    <th style="width: 20%;">სტატუსი</th>
                                    <th style="width: 20%;">პასუხისმგებელი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                        <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </ff>
				<ff style="display:none; height: 656px;" id="incomming_comment">
					<div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
						
					</div>
				</ff>
				<ff style="display:none; height: 656px;" id="incomming_mail">
					<div style="margin-left: 7px;" id="task_button_area">
						<button id="add_mail">ახალი E-mail</button>
					</div>
					<div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
						<table class="display" id="table_mail" >
							<thead>
								<tr id="datatable_header">
									<th>ID</th>
									<th style="width: 20%;">თარიღი</th>
									<th style="width: 25%;">ადრესატი</th>
									<th style="width: 35%;">გზავნილი</th>
									<th style="width: 20%;">სტატუსი</th>
								</tr>
							</thead>
							<thead>
								<tr class="search_header">
									<th class="colum_hidden">
										<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
									</th>
									<th>
										<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
									</th>
									<th>
										<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
									</th>
									<th>
										<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
									</th>
									<th>
										<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
									</th>
								</tr>
							</thead>
						</table>
					</div>
				</ff>
                <ff style="display:none; height: 656px;" id="client_conversation">
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
        	           <table class="display" id="table_question" style="width:100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 20%;">პროექტი</th>
                                    <th style="width: 20%;">კითხვა</th>
                                    <th style="width: 40%;">პასუხი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </ff>
                <ff style="display:none; height: 656px; " id="chat_question">
                     <div style="margin-left: 7px;"><p>ახალი კომენატრი</p></div>
    	             <table>
                        <tr>
                            <td><textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; margin-left: 7px; width:510px; height:50px; display:inline-block;" rows="3"></textarea > </td>
                            <td><button style="height: 50px; margin-left: 9px;" id="add_comment">დამატება</button></td>
                        </tr>
                    </table>

                     <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; border: 1px solid #dbdbdb;">' . get_comment($hidde_inc_id) . '</div>
                     <input type=hidden id="comment_incomming_id" value="' . $res['id'] . '"/>
                </ff>
                <ff style="display:none; height: 656px;" id="user_logs">
                     <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                         <table class="display" id="user_log_table">
                            <thead>
                                <tr style="font-size: 11px;" id="datatable_header">
                                    <th>ID</th>
                                    <th style="width:20%">თარიღი</th>
                                    <th style="width:15%">ქმედება</th>
                                    <th style="width:12%">მომხმა<br>რებელი</th>
                                    <th style="width:15%">ველი</th>
                                    <th style="width:19%">ახალი მნიშვნელობა</th>
                                    <th style="width:19%">ძველი მნიშვნელობა</th>
                                </tr>
                            </thead>
                            <thead >
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                        <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width:97%" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="call_resume">
                     <table style="margin-left: 7px;" width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ოპერატორი</label></td>
							<td style="width: 205px;"><label for="d_number">თარიღი</label></td>
                            <td style="width: 205px;"><label for="d_number">მომართვის ნომერი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="my_operator_name" class="idle" value="' . $res[operator] . '" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_datetime" class="idle" value="' . $res[call_date] . '" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="incomming_call_id" class="idle" value="' . $hidde_inc_id . '" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ლოდინის დრო</label></td>
							<td style="width: 205px;"><label for="d_number">საუბრის ხანგრძლივობა</label></td>
                            <td style="width: 205px;"><label for="d_number">დამუშავების ხანგრძლივობა</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="call_wait_time" class="idle" value="' . $res[wait_time] . '" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_duration" class="idle" value="' . $res[duration] . '" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id=call_done_date" class="idle" value="' . $res[processed_time] . '" disabled="disabled"/>
                            </td>
                        </tr>
                        
                    </table>
                 </ff>
				 
				<ff style="display:none; height: 656px;" id="sms">
					
                    <div id="task_button_area" style="margin-left: 7px;">
						<button id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
					</div>
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
    					<table class="display" id="table_sms" >
    						<thead>
    							<tr id="datatable_header">
    								<th>ID</th>
    								<th style="width: 20%;">თარიღი</th>
    								<th style="width: 15%;">ადრესატი</th>
    								<th style="width: 50%;">ტექსტი</th>
    								<th style="width: 15%;">სტატუსი</th>
    							</tr>
    						</thead>
    						<thead>
    							<tr class="search_header">
    								<th class="colum_hidden">
    								<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
    								</th>
    							</tr>
    						</thead>
    					</table>
					</div>
				</ff>
                <ff style="display:none; height: 656px;" id="incomming_file">
                    <table style="float: right;text-align: center; border: 1px solid #ccc; width: 100%;">
    					<tr>
    						<td>
    							<div class="file-uploader">
    								<input id="choose_file1" type="file" name="choose_file1" class="input" style="display: none;">
    								<button id="choose_button" style="width: 100%;" class="center">აირჩიეთ ფაილი</button>
    								<input id="hidden_inc" type="text" value="" style="display: none;">
    							</div>
    						</td>
    					</tr>
    				</table>
    			     <table style="float: right;border: 1px solid #ccc;width: 100%;margin-top: 2px;text-align: center;">
    			          <tr>
    			           <td colspan="4">მიმაგრებული ფაილი</td>
    			          </tr>
    				</table>
    				<table id="file_div" style="float: right; border: 1px solid #ccc; width: 100%;margin-top: 2px; text-align: center;">';

	foreach ($increm[result] as $increm_row) {
		$data .= '
    						        <tr style="border-bottom: 1px solid #CCC;">
                                      <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[file_date] . '</td>
    						          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[name] . '</td>
    						          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
    						          <td style="height: 20px;vertical-align: middle;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
    						        </tr>';
	}

	$data .= '
							</table>


        		</ff>
                <ff style="display:none; height: 656px;" id="crm">
                    <div style="width:100%">
                       
                        <table>
                            <tr style="height: 0px;">
                                <td colspan="2" style="width: 145px; text-align: center;">პერიოდი</td>
                                <td><label>User Id</label></td>
                                <td>ტელეფონი</td>
                                <td></td>
                            </tr>
                            <tr style="height: 0px;">
                                <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="start_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="' . date('Y-m-d', strtotime('-7 day')) . '"></td>
                                <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="end_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="' . date('Y-m-d') . '"></td>
                                <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_pin" style="width: 111px; height: 20px; margin-top: 20px;" value="' . $person_pin . '"></td>
                                <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_phone" style="width: 111px; height: 20px; margin-top: 20px;" value="' . $res[phone] . '"></td>
                                <td><button id="search_ab_crm_pin" style="margin-left: 10px; margin-top: 4px;">ძებნა</button></td>
                            </tr>
                        <table>
                        <table class="display" id="table_crm" style="width: 100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 15%;">Upload<br>date</th>
                                    <th style="width: 14%;">Prom-<br>date</th>
                					<th style="width: 11%;">User Id</th>
                					<th style="width: 12%;">User<br>name</th>
                                    <th style="width: 13%;">Mobile</th>
                                    <th style="width: 13%;">PID</th>
                                    <th style="width: 10%;">Com-<br>ment</th>
                                    <th style="width: 12%;">status</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
        	                        <th>
                                        <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 96%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
              				</thead>
          				</table>
                    </div>
			    </ff>
                <ff style="display:none; height: 656px;" id="record" >
	                <div style="margin-top: 10px;">
                        <audio controls style="margin-left: 7px; width: 97%" id="auau">
                          <source src="" type="audio/wav">
                          Your browser does not support the audio element.
                        </audio>
                    </div>
                    <table style="margin: auto; width: 97%;">
                       <tr>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">თარიღი</td>
                    	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ხანგძლივობა</td>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ოპერატორი</td>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ვინ გათიშა</td>
                    	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">მოსმენა</td>
                        </tr>
                        ' . GetRecordingsSection($res) . '
                    </table>
    	            
					
						';
	$data .= ($res["source_id"] == 16) ? '<strong>Video Call</strong>' : '';
	$data .= '
					<table style="margin: auto; width: 97%;">
						' . GetVideoRecordings($res) . '
					</table>

					' . GetPhotos($res) . '
				
    	      	</ff>
			</div>';

	if ($dialogType == 'autodialer') {
		$ast_source = $number;
	} else {
		$ast_source = $res['ast_source'];
	}
	$data .= '<input id="hidden_user" type="text" value="' . $res['user_id'] . '" style="display: none;">
	 		<input id="note" type="text" value="' . $res['note'] . '" style="display: none;">
			<input id="ast_source" type="text" value="' . $ast_source . '" style="display: none;">
			
			
			<input id="hidde_incomming_call_id" type="text" value="' . $res['id'] . '" style="display: none;">
			<input id="hidden_incomming_call_id" type="hidden" value="' . $hidde_inc_id . '">
			<input id="chat_name" type="hidden" value="" />
			<input id="crm_phone" type="hidden" value="' . $res['crm_phone'] . '" />
			<input id="dialogType" type="hidden" value="' . $dialogType . '" />
			
			<input id="processing_start_date" type="text" value="' . $processing_start_date . '" style="display: none;">
			</div>
	 </div>';

	return $data;
}

function GetRecordingsSection($res)
{
	global $db;

	if ($res[phone] == '') {
		$num_row = 0;
	} else {
		$db->setQuery(" SELECT    SEC_TO_TIME(asterisk_call_log.talk_time) AS `time`,
								  if(asterisk_call_log.call_type_id = 5,'1','0') AS autoDialer_check,
								  if(asterisk_call_log.call_type_id = 5,CONCAT(asterisk_call_record.name,'.',asterisk_call_record.format),
                				  		CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format))  AS `userfield` ,                				  FROM_UNIXTIME(asterisk_call_log.call_datetime),
                				  user_info.`name`,
                				  IF(asterisk_call_log.call_status_id = 6, 'ოპერატორმა', 'მომხმარებელმა') AS `completed`
                        FROM 	  asterisk_call_log
                        LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
						LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
	                    WHERE	  asterisk_call_log.id ='$res[asterisk_incomming_id]'");

		$num_row = $db->getNumRow();
		$req     = $db->getResultArray();
	}
	
	if ($num_row == 0) {
		$data .= '<tr>
                      <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
	} else {
		foreach ($req[result] as $res2) {
			$autoDialer_check = 0;
			if($res2['autoDialer_check'] == '1'){
				$autoDialer_check = 1;
			}
			$src = $res2['userfield'];
			$data .= '
    		      <tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[call_datetime] . '</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[time] . '</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[name] . '</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[completed] . '</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\'' . $src . '\','.$autoDialer_check.')"><span style="color: #2a8ef2;">მოსმენა</span></td>
        	      </tr>';
		}
	}

	return $data;
}


function GetPhotos($res)
{

	global $db;

	$db->setQuery(" SELECT `datetime`, `path`  FROM video_call_uploads WHERE peer_id = '$res[phone]'");

	$num_row_photos = $db->getNumRow();
	$req     = $db->getResultArray();

	if ($num_row_photos != 0) {
		$data .= '<div class="videoCallPhotos">';

		foreach ($req[result] as $item) {

			$photo = "https://video.mepa.gov.ge/server/uploads/" . $item['path'];

			$data .= '<span><a href="' . $photo . '" target="_blank"><img src="' . $photo . '" /></a> ' . $item[datetime] . '</span>';
		}
		$data .= '</div>';
	}

	return $data;
}

function GetVideoRecordings($res)
{

	global $db;

	if ($res[phone] == '') {
		$num_row = 0;
	} else {
		$num_row = 1;
	}

	if ($num_row == 0) {
		$data .= '<tr>
                      <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=2>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
	} else {
		$rec = "https://video.mepa.gov.ge/server/uploads/" . $res[phone] . ".mp3";
		$data .= '
    		      <tr>
            	    <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle; ">' . $res[phone] . '</td>
            	    <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle; cursor: pointer; " onclick="listen2(\'' . $rec . '\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
        	      </tr>';
	}


	// https://video.mepa.gov.ge/server/uploads/


	return $data;
}


function get_activitie_options()
{
	global $db;
	$options = '<li class="is-active" data-id="0" data-color="#bdc3c7">
					<span class="activitie-color-block" style="background: #bdc3c7;"></span>
					<span class="activitie-name">აირჩიეთ აქტივობა</span>
				</li>';
	$db->setQuery("SELECT id, name, color FROM work_activities WHERE actived = 1");
	$req = $db->getResultArray();
	foreach ($req[result] as $res) {

		$options .= '<li data-id="' . $res["id"] . '" data-color="' . $res["color"] . '">
						<span class="activitie-color-block" style="background:' . $res["color"] . ';"></span>
						<span class="activitie-name">' . $res["name"] . '</span>
					</li>';
	}

	return $options;
}

function get_dnd_status($userid)
{

	$query = mysql_query("SELECT dnd FROM crystal_users WHERE id = '$userid'");
	$res = mysql_fetch_assoc($query);

	return $res["dnd"];
}

function get_activitie_status_data($userid)
{
	global $db;
	$db->setQuery("SELECT work_activities_id
	               FROM   users
	               WHERE  id = '$userid'");

	$res = $db->getResultArray();
	$activitie_id = (int) $res[result][0]["work_activities_id"];
	$activitie_status = $activitie_id == 0 ? 'off' : 'on';

	return array(
		"id" => $activitie_id,
		"status" => $activitie_status
	);
}

function get_activitie_seconds_passed($userid, $activitieid)
{
	global $db;

	$db->setQuery("SELECT TIME_TO_SEC(TIMEDIFF(NOW(),start_datetime)) AS time_difference
				   FROM   work_activities_log
				   WHERE  user_id            = '$userid'
				   AND 	  work_activities_id = '$activitieid'
				   AND	  ISNULL(end_datetime) AND start_datetime > date(now())");

	$req = $db->getResultArray();
	$res = $req[result][0];



	$db->setQuery("SELECT    TIME_TO_SEC(DATE_FORMAT(work_shift.timeout,'%H:%i')) AS shift_timeout
					   FROM      work_graphic_rows
					   LEFT JOIN work_shift ON work_shift.id = work_graphic_rows.work_shift_id
					   WHERE     work_graphic_id =(SELECT id
        							               FROM   work_graphic
        						                   WHERE  project_id = '2' AND year = year(now()) 
                                                   AND    month = DATE_FORMAT(now(), '%m') 
                                                   AND    actived = 1 AND `operator_id` = '$userid')
					   AND `date` = CONCAT(DATE_FORMAT(now(), '%d'),'.',DATE_FORMAT(now(), '%m'),'.20',DATE_FORMAT(now(), '%y'))");

	$res_break_time = $db->getResultArray();
	$arr_break_time = $res_break_time[result][0];

	$db->setQuery("SELECT sum(TIME_TO_SEC(TIMEDIFF(end_datetime ,start_datetime)) ) AS time_difference
					   FROM   work_activities_log
					   WHERE  user_id = '$userid'
					   AND 	  work_activities_id IN (SELECT id FROM work_activities WHERE actived = 1)
					   AND    end_datetime > date(now()) AND start_datetime > date(now())");

	$res_used_time = $db->getResultArray();
	$arr_used_time = $res_used_time[result][0];

	// echo ">>>".$arr_break_time['shift_timeout'] - $arr_used_time["time_difference"];

	// if($arr_break_time['shift_timeout'] != 0){

	if ($res['time_difference'] > 0) {
		$result = $arr_break_time['shift_timeout'] - $arr_used_time["time_difference"] - $res["time_difference"];
	} else
		$result = $arr_break_time['shift_timeout'] - $arr_used_time["time_difference"];

	// }
	// else {
	// 	$result = "not_set";
	// }


	return $result;
}

function get_fieldset_inputs($field_id, $saved_values, $inputs_by_tabs, $pphone)
{
	global $db;

	$db->setQuery("	SELECT 	id,
							`key`,
							name,
							multilevel_table,
							table_name,
							processing_field_type_id AS 'type',
							position,
							multilevel_1,
							multilevel_2,
							multilevel_3,
							depth,
							tab_id,
							necessary_input AS 'nec',
							'' AS 'class'

					FROM 	processing_setting_detail
					WHERE 	processing_fieldset_id = '$field_id' AND actived = '1'
					ORDER BY position");
	$inputs = $db->getResultArray();

	$tab_count = count($inputs_by_tabs);
	$tabs_count = 0;
	foreach ($inputs_by_tabs as $tabs) {
		$display = 'none';
		if ($tabs_count == 0) {
			$display = 'block';
			$tabs_count++;
		}
		$output .= '<div class="inp_tabs dialog-tab-' . $field_id . '" data-tab-id="' . $tabs . '"  style="display:' . $display . ';">';
		$divider = 1;
		for ($j = 0; $j < $inputs['count']; $j++) {
			if ($tabs == $inputs['result'][$j]['tab_id']) {
				if ($inputs['result'][$j]['type'] == 11) {
					if ($divider % 3 == 0) {
						$inputs['result'][$j - 1]['class'] = 'col-6';
						$inputs['result'][$j - 2]['class'] = 'col-6';
					} else {
						$inputs['result'][$j - 1]['class'] = 'col-12';
					}
				}
				$divider++;
			}
		}
		for ($i = 0; $i < $inputs['count']; $i++) {
			if ($tabs == $inputs['result'][$i]['tab_id']) {
				$nec = '';
				$nec_is = 0;
				$default = 'col-4';
				if ($inputs['result'][$i]['nec'] == 1) {
					$nec = '<span style="color:red;"> *</span>';
					$nec_is = 1;
				}

				if ($inputs['result'][$i]['class'] != '') {
					$default = $inputs['result'][$i]['class'];
				}

				if ($inputs['result'][$i]['type'] == 1 or $inputs['result'][$i]['type'] == 4 or $inputs['result'][$i]['type'] == 5) {
					$show = 0;
					$input_name = $inputs['result'][$i]['name'];

    

					$service_updater = '';
					if ($input_name == 'ტელეფონი') {
						$service_updater = '<img class="service_icon" id="service_phone" src="media/images/icons/inner_call_2.png">';
					} else if ($input_name == 'პირადი ნომერი') {
						$service_updater = '<img class="service_icon for_service_contract" id="service_pn" src="media/images/icons/inner_call_2.png"><img class="ferm_service" id="service_pn_ferm" src="media/images/icons/inner_call_2.png" style="transform:rotate(45deg);margin-top: -47px;margin-left: 124px;">';
					} else if ($input_name == 'აბონენტის კოდი') {
						$service_updater = '<img class="service_icon" id="service_ab" src="media/images/icons/inner_call_2.png">';
					} else if ($input_name == 'საიდენტიფიკაციო კოდი') {
						$service_updater = '<img class="service_icon for_service_contract" id="service_rs_id" src="media/images/icons/inner_call_2.png">';
					}

					foreach ($saved_values as $saved) {
						$val = $saved[value];
						if ($input_name == 'ტელეფონი') {
							$number = $pphone;
							if ($pphone != '') {
								$last = substr($number, -2);
								$middle = substr($number, -4, -2);
								$first = substr($number, -6, -4);
								$triple = substr($number, -9, -6);

								$formated_phone = $triple . '-' . $first . '-' . $middle . '-' . $last;

								$val = $formated_phone;
							}
						}


						if ($saved['input_id'] == $inputs['result'][$i]['id']) {
							$show++;
							$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label><input value="' . $val . '" data-nec="' . $nec_is . '" style="height: 18px; width: 95%;" type="text" id="' . $inputs['result'][$i]['key'] . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" class="idle" />' . $service_updater . '</div>';
						}
					}
					if ($show == 0) {
						$number = $pphone;
						$val = '';
						if ($pphone != '' and $input_name == 'ტელეფონი') {
							$last = substr($number, -2);
							$middle = substr($number, -4, -2);
							$first = substr($number, -6, -4);
							$triple = substr($number, -9, -6);

							$formated_phone = $triple . '-' . $first . '-' . $middle . '-' . $last;

							$val = $formated_phone;
						}
						$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label><input value="' . $val . '" data-nec="' . $nec_is . '" style="height: 18px; width: 95%;" type="text" id="' . $inputs['result'][$i]['key'] . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" class="idle" />' . $service_updater . '</div>';
					}
				} else if ($inputs['result'][$i]['type'] == 8) {
					$cnobari_table = $inputs['result'][$i]['table_name'];
					$show = 0;
					foreach ($saved_values as $saved) {
						if ($saved['input_id'] == $inputs['result'][$i]['id']) {
							$show++;
							$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label><select data-nec="' . $nec_is . '" id="' . $inputs['result'][$i]['key'] . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cnobari_selector_options($cnobari_table, 'selector', '', '', $saved['int_value']) . '</select></div>';
						}
					}
					if ($show == 0) {
						$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label><select data-nec="' . $nec_is . '" id="' . $inputs['result'][$i]['key'] . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cnobari_selector_options($cnobari_table, 'selector', '', '', 0) . '</select></div>';
					}
				} else if ($inputs['result'][$i]['type'] == 6) {
					$cnobari_table = $inputs['result'][$i]['table_name'];

					$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label>' . get_cnobari_selector_options($cnobari_table, 'radio', $inputs['result'][$i]['key'], $inputs['result'][$i]['id'], '', $saved_values) . '</div>';
				} else if ($inputs['result'][$i]['type'] == 7) {
					$cnobari_table = $inputs['result'][$i]['table_name'];

					$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label>' . get_cnobari_selector_options($cnobari_table, 'checkbox', $inputs['result'][$i]['key'], $inputs['result'][$i]['id'], '', $saved_values) . '</div>';
				} else if ($inputs['result'][$i]['type'] == 2) {
					$show = 0;
					foreach ($saved_values as $saved) {
						if ($saved['input_id'] == $inputs['result'][$i]['id']) {
							$show++;
							$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label><textarea data-nec="' . $nec_is . '" style="resize: vertical;width: 97%;height: 30px;" id="' . $inputs['result'][$i]['key'] . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" class="idle" name="call_content">' . $saved[value] . '</textarea></div>';
						}
					}
					if ($show == 0) {
						$output .= '<div class="' . $default . '"><label>' . $inputs['result'][$i]['name'] . $nec . '</label><textarea data-nec="' . $nec_is . '" style="resize: vertical;width: 97%;height: 30px;" id="' . $inputs['result'][$i]['key'] . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" class="idle" name="call_content"></textarea></div>';
					}
				} else if ($inputs['result'][$i]['type'] == 12) {
					$output .= '<div class="col-4"></div>';
				} else if ($inputs['result'][$i]['type'] == 10) {
					$depth = $inputs['result'][$i]['depth'];
					$level_1_id = 0;
					$level_2_id = 0;
					$level_3_id = 0;
					for ($k = 1; $k <= $depth; $k++) {

						if ($k == 1) {
							$show = 0;
							foreach ($saved_values as $saved) {
								if ($saved['input_id'] == $inputs['result'][$i]['id'] and $saved['value'] == 1) {
									$show++;
									$level_1_id = $saved['int_value'];
									$output .= '<div class="col-4"><label>' . $inputs['result'][$i]['multilevel_' . $k] . '</label><select  id="' . $inputs['result'][$i]['key'] . '_' . $k . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cat_1($saved['int_value'], $inputs['result'][$i]['multilevel_table']) . '</select></div>';
								}
							}
							if ($show == 0) {
								$output .= '<div class="col-4"><label>' . $inputs['result'][$i]['multilevel_' . $k] . '</label><select  id="' . $inputs['result'][$i]['key'] . '_' . $k . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cat_1(0, $inputs['result'][$i]['multilevel_table']) . '</select></div>';
							}
						} else if ($k == 2) {
							$show = 0;
							foreach ($saved_values as $saved) {
								if ($saved['input_id'] == $inputs['result'][$i]['id'] and $saved['value'] == 2) {
									$show++;
									$level_2_id = $saved['int_value'];
									$output .= '<div class="col-4"><label>' . $inputs['result'][$i]['multilevel_' . $k] . '</label><select  id="' . $inputs['result'][$i]['key'] . '_' . $k . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cat_1_1($level_1_id, $saved['int_value'], $inputs['result'][$i]['multilevel_table']) . '</select></div>';
								}
							}
							if ($show == 0) {
								$output .= '<div class="col-4"><label>' . $inputs['result'][$i]['multilevel_' . $k] . '</label><select  id="' . $inputs['result'][$i]['key'] . '_' . $k . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cat_1_1($level_1_id, 0, $inputs['result'][$i]['multilevel_table']) . '</select></div>';
							}
						} else {
							$show = 0;
							foreach ($saved_values as $saved) {
								if ($saved['input_id'] == $inputs['result'][$i]['id'] and $saved['value'] == 3) {
									$show++;
									$level_3_id = $saved['int_value'];
									$output .= '<div class="col-4"><label>' . $inputs['result'][$i]['multilevel_' . $k] . '</label><select  id="' . $inputs['result'][$i]['key'] . '_' . $k . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cat_1_1($level_2_id, $saved['int_value'], $inputs['result'][$i]['multilevel_table']) . '</select></div>';
								}
							}
							if ($show == 0) {
								$output .= '<div class="col-4"><label>' . $inputs['result'][$i]['multilevel_' . $k] . '</label><select  id="' . $inputs['result'][$i]['key'] . '_' . $k . '--' . $inputs['result'][$i]['id'] . '--' . $inputs['result'][$i]['type'] . '" style="width: 98%; height: 18px; ">' . get_cat_1_1($level_2_id, 0, $inputs['result'][$i]['multilevel_table']) . '</select></div>';
							}
						}
					}
				}
				$divider++;
			}
		}
		$output .= '</div>';
	}
	return $output;
}

function get_cnobari_selector_options($table, $selector_type = 'selector', $key = '', $input_id = '', $selected = 0, $saved_values = '')
{
	global $db;
	if ($table != '') {
		$db->setQuery("	SELECT 	id,
								name
						FROM 	$table
						WHERE 	actived = '1'");
		if ($selector_type == 'selector') {

			return $db->getSelect($selected);
		} else if ($selector_type == 'radio') {
			$output = '';
			$radio = $db->getResultArray();
			$output .= '<div class="checkbox_container">';
			foreach ($radio['result'] as $item) {
			}
			foreach ($radio['result'] as $item) {
				$show = 0;
				foreach ($saved_values as $saved) {

					if ($saved['input_id'] == $input_id and $saved['int_value'] == $item[id]) {
						$show++;
						$output .= '<div class="lonely-checkbox"><input checked type="radio" id="' . $key . '-' . $item[id] . '--' . $input_id . '--6" name="' . $key . '" name="' . $key . '" value="' . $item[name] . '">
							<label style="margin-top: 6px;margin-right: 5px;" for="' . $key . '-' . $item[id] . '">' . $item[name] . '</label></div>';
					}
				}
				if ($show == 0) {
					$output .= '<div class="lonely-checkbox"><input type="radio" id="' . $key . '-' . $item[id] . '--' . $input_id . '--6" name="' . $key . '" name="' . $key . '" value="' . $item[name] . '">
							<label style="margin-top: 6px;margin-right: 5px;" for="' . $key . '-' . $item[id] . '">' . $item[name] . '</label></div>';
				}
			}
			$output .= '</div>';
			return $output;
		} else if ($selector_type == 'checkbox') {
			//var_dump($saved_values);
			$output = '';
			$radio = $db->getResultArray();
			$output .= '<div class="checkbox_container">';
			foreach ($radio['result'] as $item) {
				$show = 0;
				foreach ($saved_values as $saved) {

					if ($saved['input_id'] == $input_id and $saved['int_value'] == $item[id]) {
						$show++;
						$output .= '<div class="lonely-checkbox"><input checked style="height:12px!important;" type="checkbox" id="' . $key . '-' . $item[id] . '--' . $input_id . '--7" name="' . $key . '" value="' . $item[name] . '">
							<label style="margin-right: 7px;margin-left: 3px;" for="' . $key . '-' . $item[id] . '">' . $item[name] . '</label></div>';
					}
				}
				if ($show == 0) {
					$output .= '<div class="lonely-checkbox"><input style="height:12px!important;" type="checkbox" id="' . $key . '-' . $item[id] . '--' . $input_id . '--7" name="' . $key . '" value="' . $item[name] . '">
							<label style="margin-right: 7px;margin-left: 3px;" for="' . $key . '-' . $item[id] . '">' . $item[name] . '</label></div>';
				}
			}

			$output .= '</div>';
			return $output;
		}
	}
}
function get_new_addr_page($type)
{
	$data = '
	<div id="dialog-form">
        <span class="communication_right_side_info">
        <table class="dialog-form-table" style="margin-top: 14px;">
              
            <tbody><tr>
                  <td style="width: 90px; "><label for="d_number">ადრესატები:</label></td>
                  <td>
                      <textarea id="receivers" style="width:360px; height:90px"></textarea>
                  </td>
              </tr>
            </tbody>
		</table>
		<input type="hidden" value="' . $type . '" id="addr_type">
        </span>
    </div>
	';

	return $data;
}
function get_new_sms_dialog($type)
{
	//new sms send dialog content
	return '<div id="dialog-form"><fieldset style="display:block;" class="new-sms-send-container">

				<!-- label for fieldset -->
				<legend>SMS</legend>

				<!-- row unit -->
				<div class="new-sms-row">
					<button id="smsTemplate" data-button="jquery-ui-button">შაბლონი</button>
				</div>

				<!-- row unit -->
				' . $adressee_inputs . '

				<!-- row unit -->
				<div class="new-sms-row">
					<label for="smsText">ტელეფონი</label>
					<div class="new-sms-input-holder" style="width:190px;">
						<input style="height:25px;" type="text" id="sms_phone">
					</div>
				</div>
				<div class="new-sms-row">
					<label for="smsText">ტექსტი</label>
					<div class="new-sms-input-holder">
						<textarea id="newSmsText"></textarea>
					</div>
				</div>

				<!-- row unit -->
				<div class="new-sms-row">
					<input type="text" id="smsCharCounter" data-limit="150" value="0/150">
					<button id="sendNewSms" data-button="jquery-ui-button">გაგზავნა</button>
				</div>

			</fieldset>
			</div>
			';
}

function getShablon()
{
	global $db;
	$db->setQuery("SELECT 	sms.id,
        					sms.`name`,
        					sms.`message`
					FROM 	sms
					WHERE 	sms.actived=1 ");

	$data_count = $db->getNumRow();

	if ($data_count > 0) {

		$data = '<table id="box-table-b1">
            		<tr class="odd">
            			<th style="width: 26px;">#</th>
            			<th style="width: 160px;">დასახელება</th>
            			<th>ქმედება</th>
            		</tr> ';

		$req = $db->getResultArray();
		foreach ($req[result] as $res3) {
			$data .= '<tr class="odd">
            				<td>' . $res3[id] . '</td>
            				<td style="width: 30px !important;">' . $res3['name'] . '</td>
            				<td style="font-size: 10px !important;">
            					<button style="width: 45px;" class="download_shablon" sms_id="' . $res3['id'] . '" data-message="' . $res3['message'] . '">არჩევა</button>
            				</td>
        			   </tr>';
		}
		$data .= '</table>';
	} else {
		$data = '<div class="empty-sms-shablon">
					<p>შაბლონთა ჩამონათვალი ცარიელია</p>
				 </div>';
	}
	return $data;
}

function get_regions_1($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   regions
                   WHERE   actived = 1 AND `parent_id` = 0");

	$req = $db->getResultArray();
	$data .= '<option value="0" selected="selected">----</option>';
	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
function get_regions_1_1($id, $child_id)
{
	global $db;

	$db->setQuery("SELECT  `id`,
                           `name`
                   FROM    regions
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}
function sms_template_dialog()
{

	return '
		<div class="sms-template-table-wrapper">
			<table class="display" id="sms_template_table" style="width: 100%;">
				<thead>
					<tr id="datatable_header">
						<th>ID</th>
						<th style="width: 10%;" >№</th>
						<th style="width:  70%;">დასახელება</th>
						<th style="width:  15%">ქმედება</th>
					</tr>
				</thead>
				<thead>
					<tr class="search_header">
						<th class="colum_hidden">
							<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
						</th>
						<th>
							<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
						<th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
			</table>
		<div>';
}

function Getquestion($id)
{
	$data = '
        <div style="margin-left: 7px;"><p>ახალი კომენატრი</p></div>
        <table>
            <tr>
                <td><textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; margin-left: 7px; width:510px; height:50px; display:inline-block;" rows="3"></textarea > </td>
                <td><button style="height: 50px; margin-left: 9px;" id="add_comment">დამატება</button></td>
            </tr>
        </table>
        <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; border: 1px solid #dbdbdb;">' . get_comment($id) . '</div>
        <input type=hidden id="comment_incomming_id" value="' . incomming_call_id . '"/>
    ';
	return $data;
}

function get_user($id)
{
	global $db;
	$data   = '';
	$db->setQuery("SELECT `name`
                   FROM   `user_info`
                   WHERE   user_id = $id");

	$res    = $db->getResultArray();
	$data   = $res[result][0]['name'];
	return $data;
}
function get_chat_docs()
{
	global $db;
	$columnCount    = $_REQUEST['count'];
	$cols[]         = $_REQUEST['cols'];
	$db->setQuery("	SELECT 	id,
							name,
							`datetime`,
							CONCAT('<a target=\"blank\" style=\"color:blue;\" href=',link,'>',link,'</a>') AS 'link',
							CONCAT('<a target=\"blank\" style=\"color:blue;\" href=\"\media/uploads/documents/',file,'\">ნახვა</a>') AS 'act'
					FROM 	mepa_project_docs
					WHERE 	actived='1'");
	$data = $db->getKendoList($columnCount, $cols);
}
function get_answers($id, $hidden_id)
{
	global $db;
	$data = '';
	$db->setQuery("SELECT `id`,
                          `user_id`,
                          `incomming_call_id`,
                          `comment`,
                          `datetime`
                    FROM  `chat_comment`
                    WHERE  parent_id = $id AND incomming_call_id = '$hidden_id' AND active = 1");

	$req = $db->getResultArray();

	foreach ($req[result] as $res) {
		$data .= '<div>
                        <span style="color: #369; font-weight: bold;">' . get_user($res['user_id']) . '</span> ' . $res['datetime'] . ' <br><br>
                        <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; ">' . $res['comment'] . '</span>
                        <img id="edit_comment" my_id="' . $res['id'] . '" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="' . $res['id'] . '" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
	}

	return $data;
}

function get_emoji()
{
	global $db;
	$db->setQuery("SELECT `emojis`.`code` FROM `emojis` WHERE `emojis`.actived = 1");

	$result  = $db->getResultArray();
	$emojis = "";
	foreach ($result[result] as $arr) {
		$emojis .= "<span code= '&#" . $arr[code] . "' >&#" . $arr[code] . "</span>";
	}
	$data = array("emojis" => $emojis);
	return $emojis;
}

function get_pined_docs2($project_id, $board, $inc_id)
{
	$data .= '	<div id="dialog-form">	
					<fieldset class="communication_right_side_info">
						<legend>დოკუმენტაცია</legend>
						<button id="mepa_sms_docs" style="margin-bottom:10px;margin-left:10px;">SMS</button>
						<button id="mepa_email_docs" style="margin-bottom:10px;margin-left:10px;">E-Mail</button>
						<div id="kendo_docs2"></div>
					</fieldset>
				</div>';
	return $data;
}
function get_chat_docs_page()
{
	$data .= '	<div id="dialog-form">	
					<fieldset class="communication_right_side_info">
						<legend>დოკუმენტაცია</legend>
						<div id="kendo_chat_docs"></div>
					</fieldset>
				</div>';
	return $data;
}
function get_pined_contact2($project_id, $board, $inc_id)
{
	$data .= '	<div id="dialog-form">	
					<fieldset class="communication_right_side_info">
						<legend>კონტაქტები</legend>
						<button id="mepa_sms_contact" style="margin-bottom:10px;margin-left:10px;">SMS</button>
						<button id="mepa_email_contact" style="margin-bottom:10px;margin-left:10px;">E-Mail</button>
						<div id="kendo_contact2"></div>
					</fieldset>
				</div>';
	return $data;
}
function get_new_mepa_mail($type, $ids)
{
	global $db;
	if ($type == "docs") {
		$db->setQuery("	SELECT 	`link`, `file`
						FROM 	mepa_project_docs
						WHERE 	actived = 1 AND id IN ($ids)");
		$links = $db->getResultArray();
		$files = '';

		foreach ($links['result'] as $link) {
			$message .= $link['link'] . "\n\n";
			$files .= '<input type="hidden" class="attachment_address" value="' . $link['file'] . '" />';
		}
	} else if ($type == "contact") {
		$db->setQuery("	SELECT 	firstname,
								lastname,
								position,
								phone,
								email
						FROM 	mepa_project_contacts
						WHERE 	actived = 1 AND id IN ($ids)");
		$cont = $db->getResultArray();
		foreach ($cont['result'] as $item) {
			$message .= $item['firstname'] . ' ' . $item['lastname'] . "<br>";
			$message .= $item['position'] . "<br>";
			$message .= $item['phone'] . ' ' . $item['email'] . "<br>";
			$message .= '----------------------------' . "<br>";
		}
	}
	$data = '
	<div id="dialog-form">
	    <fieldset style="height: auto;">
	    	<table class="dialog-form-table">
              
				<tr>
					<td style="width: 90px; "><label for="d_number">ადრესატი:</label></td>
					<td>
						<input type="text" check style="width: 484px !important;"id="mail_address" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['address'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 90px;"><label for="d_number">CC:</label></td>
					<td>
						<input type="text" style="width: 484px !important;" id="mail_address1" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['cc_address'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 90px;"><label for="d_number">Bcc:</label></td>
					<td>
						<input type="text" style="width: 484px !important;" id="mail_address2" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['bcc_address'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 90px;"><label for="d_number">სათაური:</label></td>
					<td>
						<input type="text" style="width: 484px !important;" id="mail_text" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['subject'] . '" />
					</td>
				</tr>
			</table>
			<table class="dialog-form-table">
				<tr>
					<td colspan="2">	
						<textarea id="input" style="width:400px; height:200px">' . $message . '</textarea>
					</td>
				</tr>
				
			</table>
			<div style="margin-top: 15px;">
				<div style="width: 100%; border:1px solid #CCC;float: left;">    	            
						<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 180px;float:left;">თარიღი</div>
						<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 189px;float:left;">დასახელება</div>
						<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 160px;float:left;">ჩამოტვირთვა</div>
						<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 20px;float:left;">-</div>
						<div style="text-align: center;vertical-align: middle;float: left;width: 595px;"><button id="choose_button_mail" style="cursor: pointer;background: none;border: none;width: 100%;height: 25px;padding: 0;margin: 0;">აირჩიეთ ფაილი</button><input style="display:none;" type="file" name="choose_mail_file" id="choose_mail_file" value=""></div>
						<div id="paste_files1">';
	foreach ($file[result] as $file_body) {
		$data .= '<div id="first_div">' . $file_body[file_date] . '</div>
										<div id="two_div">' . $file_body[name] . '</div>
										<div id="tree_div" onclick="download_file(\'' . $file_body[rand_name] . '\',\'' . $file_body[name] . '\')">ჩამოტვირთვა</div>
										<div id="for_div" onclick="delete_file1(\'' . $file_body[id] . '\')">-</div>
										<input type="hidden" class="attachment_address" value="' . $file_body[rand_name] . '">';
	}
	$data .= '</div>
				</div>
					
	        </div>
			
			<fieldset style="display: inline-flex; width: 100%; margin-left: -11px; float: left;">
			<table class="dialog-form-table">
			<tr>
					<td style="width: 69px;">
						<button id="email_shablob" class="center">შაბლონი</button>
					</td>		
					<td>
						<div class="file-uploader">
							
							<input id="hidden_inc" type="text" value="' . $res['name'] . '" style="display: none;">
						</div>
				    </td>
					<td style="width: 69px;">
						
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
					</td>
					<td style="width: 69px;">
						<button id="send_email" class="center">გაგზავნა</button>
					</td>
				</tr>
			</table>
			</fieldset>
			<!-- ID -->
			<input type="hidden" id="source_id" value="' . $res[id] . '" />
			<input type="hidden" id="hidden_increment" value="' . $increment . '" />
			' . $files . '
        </fieldset>
    </div>
    ';
	return $data;
}
function get_new_mepa_sms($type, $ids)
{
	global $db;
	//new sms send dialog content
	if ($type == "docs") {
		$db->setQuery("	SELECT 	link
						FROM 	mepa_project_docs
						WHERE 	actived = 1 AND id IN ($ids)");
		$links = $db->getResultArray();

		foreach ($links['result'] as $link) {
			$message .= $link['link'] . "\n\n";
		}
	} else if ($type == "contact") {
		$db->setQuery("	SELECT 	firstname,
								lastname,
								position,
								phone,
								email
						FROM 	mepa_project_contacts
						WHERE 	actived = 1 AND id IN ($ids)");
		$cont = $db->getResultArray();
		foreach ($cont['result'] as $item) {
			$message .= $item['firstname'] . ' ' . $item['lastname'] . "\n";
			$message .= $item['position'] . "\n";
			$message .= $item['phone'] . ' ' . $item['email'] . "\n";
			$message .= '----------------------------' . "\n";
		}
	}
	return '<div id="dialog-form"><fieldset style="display:block;" class="new-sms-send-container">

				<!-- label for fieldset -->
				<legend>SMS</legend>

				<!-- row unit -->
				<div class="new-sms-row">
					<button id="smsTemplate" data-button="jquery-ui-button">შაბლონი</button>
				</div>

				<!-- row unit -->
				<div class="new-sms-row">
					<label for="smsText">ადრესატი</label>
					<div class="new-sms-input-holder">
						<input style="height: 26px; padding:7px;" type="text" id="phone_to_send">
					</div>
				</div>
				<div class="new-sms-row">
					<label for="smsText">ტექსტი</label>
					<div class="new-sms-input-holder">
						<textarea id="newSmsText" style="padding: 6px;height: 100px;">' . $message . '</textarea>
					</div>
				</div>

				<!-- row unit -->
				<div class="new-sms-row">
					<input type="text" id="smsCharCounter" data-limit="150" value="0/150">
					<button id="sendNewSms" data-button="jquery-ui-button">გაგზავნა</button>
				</div>

			</fieldset>
			</div>
			';
}









function crmForm($crm = '')
{

	$data = '
	
	<div style="width: 100%; display: flex; justify-content: center; flex-wrap: wrap; margin: 10px 0;">
	
	<table style="width: 100%; table-layout: fixed;">
	<tr>
		<td style="padding-left: 6px;">
			<select id="crm-0" >
				' . $crm . '
			</select>
		</td>
	</tr>
	<tr>
		<td style="text-align: center">
			<textarea id="crm-comment" style="width: 97%; margin: 10px auto; padding: 5px 6px;"></textarea>
		</td>
	</tr>
	</table>
		
	
	</div>
	
	';

	return $data;
}
