
let addBtn = document.querySelector('.add_button');

if (addBtn) {
    setTimeout(() => {

        var options = {
            rootMargin: '500px',
            threshold: 1.0 // trigger only when element comes into view completely
        };
        var ob = new IntersectionObserver((entries, observer) => {
            // console.log(entries[0].isIntersecting); 
            // console.log(entries[0]);

            if (entries[0].intersectionRatio < 1) {
                let addBtn = document.querySelector('.add_button');
                addBtn.classList.add('add_button__scrolled')
                addBtn.querySelector(".add_button__button").classList.add('display-none')
            } else {
                addBtn.classList.remove('add_button__scrolled')
                addBtn.querySelector(".add_button__button").classList.remove('display-none')
            }
        }, options);

        ob.observe(document.querySelector('.activation_navigation'));

    }, 4000);   
}




