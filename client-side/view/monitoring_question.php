<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/monitoring_question.action.php";		//server side folder url
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		 	
		$(document).ready(function () {
			
			LoadTable();
				
			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");
			SetEvents("add_button", "", "", tName, fName, aJaxURL);
		});
        
		function LoadTable(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var aJaxURL	= "server-side/view/monitoring_question.action.php";
            var gridName        = 	'example';
            var actions         = 	'';
            var editType        =   "popup"; // Two types "popup" and "inline"
            var itemPerPage     = 	40;
            var columnsCount    =	3;
            var columnsSQL      = 	[
                                        "id:string",
                                        "name:string",
                                        "text:string"
                                    ];
            var columnGeoNames  = 	[
                                        "ID", 
                                        "კრიტერიუმი",
                                        "ქულა"
                                    ];

            var showOperatorsByColumns  =   [0,0,0]; 
            var selectors               =   [0,0,0]; 

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END
                
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL,'get_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden, 1, '', '', '', true, true, true);
			
		}
		
		
		
		function LoadDialog(fr){
			if(fr == 'add-edit-form'){
    			var buttons = {
    					"save": {
    			            text: "შენახვა",
    			            id: "save-dialog"
    			        }, 
    		        	"cancel": {
    			            text: "დახურვა",
    			            id: "cancel-dialog",
    			            click: function () {
    			            	$(this).dialog("close");
    			            }
    			        } 
    			    };
    			
    			GetDialog(fName, 600, "auto", buttons,"top");
			}
		}

		$(document).on("dblclick", "#example tr.k-state-selected", function () {
            var grid = $("#example").data("kendoGrid");
            var dItem = grid.dataItem($(this));
           
            if(dItem.id == ''){
                return false;
            }
            
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_edit_page",
                    id: dItem.id
                },
                success: function(data){
                    $("#add-edit-form").html(data.page);
                    LoadDialog(fName);
                }
            })
            
        });

		$(document).on("click","#delete_button",function(){
            var grid = $("#example").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());
            $.ajax({
				url: aJaxURL,
				data: 'act=disable&id='+dItem.id,
				success: function(data) {
                    $("#example").data("kendoGrid").dataSource.read();
				}
			});
            
        });
	    
		
		// Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act	  ="save_priority";
	    	param.id	  = $("#comment_id").val();
	    	param.comment = $("#comment").val();
	    	param.level   = $("#level").val();
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	</script>
</head>

<body>

    <div id="tabs" style="width: 80%; display:inline-block; position: absolute;">
		<div id="button_area">
			<button id="add_button">დამატება</button>
			<button id="delete_button">წაშლა</button>
		</div>
		
        <div id="example"></div>        
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="კითხვა">
    	<!-- aJax -->
	</div>
	<!-- jQuery Dialog -->
</body>
</html>






