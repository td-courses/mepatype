<?php

include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action = $_REQUEST['act'];
$error  = '';
$data   = array();
$user   = $_SESSION['USERID'];
date_default_timezone_set("Asia/Tbilisi");

switch ($action) {
    case 'get_list':
        $filter = "";

        $id          = $_REQUEST['hidden'];
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $startDate   = $_REQUEST['start_date'];
        $endDate     = $_REQUEST['end_date'];

        

        $mysqli->setQuery("SELECT monit.operator_id,
                                  monit.`name`,
                    			  COUNT(*),
                    			  SUM(IF(monit.monitoring_main_id > 0 AND monit.monitoring_user_id > 0 AND monit.monitoring_status_id > 0,1,0)),
                    			  SUM(IF(monit.`level` = 100,1,0)),
                    			  ROUND(SUM(IF(monit.`level` = 100,1,0))/COUNT(*)*100,2),
                    			  SUM(IF(monit.`level` = 95,1,0)),
                    			  ROUND(SUM(IF(monit.`level` = 95,1,0))/COUNT(*)*100,2),
                    			  SUM(IF(monit.`level` = 90,1,0)),
                    			  ROUND(SUM(IF(monit.`level` = 90,1,0))/COUNT(*)*100,2),
                    			  SUM(IF(monit.`level` = 85,1,0)),
                    			  ROUND(SUM(IF(monit.`level` = 85,1,0))/COUNT(*)*100,2),
                    			  SUM(IF(monit.`level` = 80,1,0)),
                    			  ROUND(SUM(IF(monit.`level` = 80,1,0))/COUNT(*)*100,2),
                    			  SUM(IF(monit.`level` = 75,1,0)),
                    			  ROUND(SUM(IF(monit.`level` = 75,1,0))/COUNT(*)*100,2)
                            FROM( SELECT    user_info.`name`,
            							    SUM(monitoring_rate.question_level) AS `level`,
            							    monitoring.operator_id,
            							    monitoring.monitoring_main_id,
            							    monitoring.monitoring_user_id,
            							    monitoring.monitoring_status_id
                    			  FROM     `monitoring`
                    			  LEFT JOIN monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id AND monitoring_rate.question_level > 0
                    			  JOIN      user_info ON user_info.user_id = monitoring.operator_id
                    			  WHERE     DATE(FROM_UNIXTIME(monitoring.request_date)) BETWEEN '$startDate' AND '$endDate'
                    			  AND       monitoring.operator_id > 0
                    			  GROUP BY  monitoring.id) AS `monit`
                            GROUP BY monit.operator_id");
        
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;
    case 'get_columns':
        
        $columnCount =         $_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] =     $_REQUEST['names'];
        $operators[] =         $_REQUEST['operators'];
        $selectors[] =         $_REQUEST['selectors'];
        $f = 0;
        foreach ($cols[0] as $col) {
            $column = explode(':', $col);
            
            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        foreach ($res as $item) {
            $columns[$i] = $item['Field'];
            $i++;
        }
        
        $dat = array();
        $a = 0;
        for ($j = 0; $j < $columnCount; $j++) {
            if (1 == 2) {
                continue;
            } else {
                
                if ($operators[0][$a] == 1) $op = true;
                else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                //$op = false;
                
                if ($res['data_type'][$j] == 'date') {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
                } else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
                } else {
                    if ($columns[$j] == "inc_status") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 153);
                    } elseif ($columns[$j] == "audio_file") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 150);
                    } elseif ($columns[$j] == "id" || $columns[$j] == 'protocol_hidde_id') {
                        $g = array('field' => $columns[$j], 'hidden' => true, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 100);
                    } elseif ($columns[$j] == "inc_date") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 130);
                    } else {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
                    }
                }
                $a++;
            }
            array_push($dat, $g);
        }
        
        // array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
        
        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for ($j = 0; $j < $columnCount; $j++) {
            if ($res['data_type'][$j] == 'date') {
                $new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
            } else {
                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
            }
        }
        
        $filtArr = array('fields' => $new_data);
        $kendoData = array('columnss' => $dat, 'modelss' => $filtArr);
        
        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
        
        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';
        
        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);

