
import Helper from "../../helpers/helper.class.js";

export default class Input extends Helper {


    constructor(prop){

        super();
        this.state = {
            dom: [],
            element: [],
            prop: prop,
            types: ["text", "textarea", "checkbox", "radio"],
            column: prop.column,
            row: prop.row
        }

        

    }




    build = () => {

        if(typeof this.state.prop.column != 'undefined') delete this.state.prop.column
        if(typeof this.state.prop.row != 'undefined') delete this.state.prop.row

        if(!this.state.types.includes(this.state.prop.type)) {
            console.warn("Incorrect Type")
        }else{

            this.state.prop.type == "text" && (this.state.element = this.buildText(this.state.prop))

            this.state.prop.type == "radio" && (this.state.element = this.buildText(this.state.prop))

            this.state.prop.type == "textarea" && (this.state.element = this.buildTextArea(this.state.prop))

            return this.state.element

        }

    }


    buildText = (arg) => {

        this.state.element.input = this.CreateElement({
            element: "field",
            "grid-column": (typeof this.state.column != 'undefined' ? this.state.column : ''),
            "grid-row": (typeof this.state.row != 'undefined' ? this.state.row : ''),
            title: (arg.placeholder ? arg.placeholder : '')
        }, this.CreateElement({
            element: "input",
            ...arg
        }))

        return this.state.element.input
        
    }

    buildTextArea = (arg) => {

        this.state.element.textarea = this.CreateElement({
            element: "field",
            "grid-column": (typeof this.state.column != 'undefined' ? this.state.column : ''),
            "grid-row": (typeof this.state.row != 'undefined' ? this.state.row : ''),
            title: (arg.placeholder ? arg.placeholder : '')
        }, this.CreateElement({
            element: "textarea",
            ...arg
        }))

        return this.state.element.textarea

    }


}