`use strict`;

import kendoSelector from "../../components/selector/selector.class.js";
import {kendo_SelectedID} from '../../helpers/kendo.helper.js'
import Tdg from "../../tdg.class.js"


export default class Authorization extends Tdg{

    constructor(){
        super();

        this.state = {
            sectionName: $("section[Authorization]"),
            username: '',
            password: '',
            interface: '',
            element: []
        }

        document.title = "Authorization"

        $.ajaxSetup({
            url: "index.php",
            async: true,
            method: "POST",
            dataType: "json"
        })

        this.initializeAuth();

    }

    createInterface = () => {
        let interfaceEl = this.CreateElement({
            element: "interface",
            attributes: ["auth"],
            onkeyup: (event) => {
                if (event.keyCode === 13) this.handleAuth()
            }
        })
        return interfaceEl;
    }

    

    createLogo = () => {
        let logo = this.CreateElement({
            element: "logo",
            attributes: ["callapp"]
        })
        return logo;
    }

    createTitle = () => {
        let title = this.CreateElement({
            element: "div",
            className: ["auth-title"],
            children: "სისტემაში შესვლა"
        })
        return title;
    }

    createInputs = () => {
        this.state.element.username = this.CreateElement({
            element: "input",
            attributes: [
                {
                    type: "text"
                },
                {
                    placeholder: "მომხმარებელი"
                },
                {
                    value: this.state.username
                }
            ],
            onInput: (e) => this.state.username = e.target.value
            
        })

        this.state.element.password = this.CreateElement({
            element: "input",
            attributes: [
                {
                    type: "password"
                },
                {
                    placeholder: "პაროლი"
                },
                {
                    value: this.state.password
                }
            ],
            onInput: (e) => this.state.password = e.target.value
        })

        this.state.element.ext =  this.CreateElement({
            element: "kendo",
            type: "selector",
            title: "ექსტენშენი",
            data: {
                route: "Authorization",
                act: "getExt"
            },
            style: {
                width: "210px"
            }
        })
       
    }

    createButton = () => {
        let button = this.CreateElement({
            element: "button",
            children: "შესვლა",
            onClick: () => this.handleAuth()
        })

        return button;
    }

    /**
     * 
     * @param {String} text 
     * @returns callback Element
     */
    createCallBack = (text) => {
        let callback = this.CreateElement({
            element: "callback",
            children: text
        })
        return callback;
    }

    handleAuth = () => {

        this.state.ext = kendo_SelectedID(this.state.element.ext)

        $.ajax({
            url: "index.php",
            async: true,
            method: "POST",
            dataType: "json",
            data: {
                route: "Authorization",
                act: "login",
                username: this.state.username,
                password: this.state.password,
                ext: this.state.ext
            },
            success: (data) => {
                if(data.status == 1){
                    location.reload()
                }else{
                    this.append(this.state.interface, this.createCallBack(data.message), {
                        setTimeout: 3000
                    });
                }
            }
        })
    }

    


    buildAuth = () => {
        this.state.interface = this.createInterface();
        this.append(this.state.interface, this.createLogo());
        this.append(this.state.interface, this.createTitle());
        this.createInputs();
        this.append(this.state.interface, this.state.element.username)
        this.append(this.state.interface, this.state.element.password)
        this.append(this.state.interface, this.state.element.ext)
        this.append(this.state.interface, this.createButton())
       
        return this.state.interface;
    }


    initializeAuth = () => {

        this.append(this.state.sectionName, this.buildAuth())

    }

}