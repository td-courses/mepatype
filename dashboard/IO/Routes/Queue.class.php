<?php

namespace IO\Routes;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use IO\Request;

use IO\Routes\flashpanel\phoneTaken;

class Queue extends Request implements MessageComponentInterface {
    
    public $clients;
    protected $response;
    protected $request;
    protected $msg;
    protected $class;
    protected $method;
    protected $loop;

    // ROUTES
    public $phoneTaken;


    public function __construct($loop) {

        $this->clients = new \SplObjectStorage;

        $that = $this;
        $this->loop = $loop;

        $this->loop->addPeriodicTimer(5, function() use ($that) {

            $phoneTaken = new phoneTaken();
            $phoneTaken->RUN();
            $this->phoneTaken  = $phoneTaken->GET();

        });

    }

    public function onOpen(ConnectionInterface $conn) {

        $this->clients->attach($conn);

    }

    public function onMessage(ConnectionInterface $from, $msg) {
        
        $msg = json_decode($msg);

        
        foreach ($this->clients as $client) {
            $client->send(json_encode($this->phoneTaken));
        }


    }

    public function onClose(ConnectionInterface $conn) {
    echo "CLOSE";
        $this->clients->detach($conn);

    }

    public function onError(ConnectionInterface $conn, \Exception $e) {

        parent::sLog($e->getMessage(), "Flashpanel");

        $conn->close();
    }


    
}