<?php 
/* ******************************
 *	Workers aJax actions
 * ******************************
 */ 
// include('../../core.php');
include('../../includes/classes/class.Mysqli.php');

$mysqli = new dbClass();
$action 	= $_REQUEST['act'];
$user_id	= $_SESSION['USERID'];
$error 		= '';
$data 		= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
	    $per_id		= $_REQUEST['id'];
		$page		= GetPage(GetWorker($per_id));

        $data		= array('page'	=> $page);

        break;
	case 'get_list':
	    $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        
        $data = '';
        $query = "SELECT 	`id`,`person_position`
                            FROM 	`position`
                            WHERE `actived` = 1";
                            $mysqli -> setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

		break;
		
		
    case 'save_pers':
		$persons_id 		= $_REQUEST['id'];
		$position 			= $_REQUEST['position'];
		
		if(empty($persons_id)){
            AddWorker($position);
		}else{
			SaveWorker($persons_id, $position);
		}

        break;
    case 'disable':
		$per_id = $_REQUEST['id'];
		DisableWorker($per_id);

		break;
   
    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Workers Functions
 * ******************************
 */
 
function AddWorker($position)
{
  
	global $mysqli;
    
    $user_id = $_SESSION["USERID"];

    $query="INSERT	INTO `position`
                (`user_id`,`person_position`,`actived`)
                VALUES
                ('$user_id','$position', 1)";
                $mysqli -> setQuery($query);
                $mysqli -> execQuery(); 


}

function SaveWorker($persons_id, $position){	
    global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	
	$query = "UPDATE `position` 
              SET `person_position` = '$position',
                  `user_id` = '$user_id'
			  WHERE   `id` = $persons_id";
    $mysqli -> setQuery($query);
    $mysqli -> execQuery();
}

function DisableWorker($per_id){	
    global $mysqli;
	
    $query = "UPDATE `position` SET
                    `actived` = 0
                WHERE  `position`.`id` = '$per_id'";
    $mysqli -> setQuery($query);
    $mysqli -> execQuery(); 
		
}

function GetWorker($per_id)
{
	global $mysqli;
    $query = "	SELECT `id`,`person_position`,`actived`
                FROM `position`
                WHERE `id` = $per_id";
											
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}

function GetPage($res = '')
{
  
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table" id="info_table">

				<tr>
					<td class="label_td">
						<label for="person_position">თანამდებობის პოზიცია</label>
					</td>
					<td>
                        <input type="text" id="person_position" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['person_position'] . '" /> 
					</td>
				</tr>
			
			</table>
        </fieldset>
 	
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}




?>