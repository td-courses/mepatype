<head>
	<style type="text/css">
		caption {
			margin: 0;
			padding: 0;
			background: #fff;
			height: 40px;
			line-height: 40px;
			font-weight: bold;
			text-align: left;
			letter-spacing: 1px;
		}
		#loading1 {
            z-index: 999;
            top: 45%;
            left: 45%;
            position: absolute;
            display: none;
            padding-top: 15px;
        }
		#show_report{
			width:15%;
			background:#fef400;
			border:0;
			outline:0;
			color:#337ab7;
		}
		div,
		caption,
		td,
		th,
		h2,
		h3,
		h4 {
			font-size: 12px;
			font-family: verdana, sans-serif;
			voice-family: "\"}\"";
			voice-family: inherit;
			color: #333;
		}

		tbody {
			display: table-row-group;
			vertical-align: middle;
			border-color: inherit;
		}

		tbody tr {
			background: #dfedf3;
			font-size: 110%;
		}

		tr {
			display: table-row;
			vertical-align: inherit;
			border-color: inherit;
		}

		tbody tr th,
		tbody tr td {
			padding: 5px;
			border: solid 1px #326e87;
			text-align: center;
			vertical-align: middle;
		}

		thead tr th {
			height: 32px;
			aline-height: 32px;
			text-align: center;
			vertical-align: middle;
			color: #1c5d79;
			background: #CBDFEE;
			border-left: solid 1px #FF9900;
			border-right: solid 1px #FF9900;
			border-collapse: collapse;
		}

		table.sortable a.sortheader {
			text-decoration: none;
			display: block;
			color: #1c5d79;
			xcolor: #000000;
			font-weight: bold;
		}

		a {
			cursor: pointer;
		}

		.tdstyle {
			text-align: left;
			vertical-align: middle;
		}

		.download {


			background-color: #4997ab;
			border-radius: 0px;
			cursor: pointer;
			color: #ffffff;
			font-size: 14px;
			border: 0px;
			width: 100%;

		}

		#logout1 {
			float: right;
		}

		.download1 {

			background-color: #ce8a14;
			border-radius: 0px;
			cursor: pointer;
			color: #ffffff;
			font-size: 14px;
			border: 0px;
			width: 100%;
		}

		.download4 {


			background-color: #71b251;
			border-radius: 0px;
			cursor: pointer;
			color: #ffffff;
			font-size: 14px;
			border: 0px;
			width: 100%;
		}

		.download3 {

			background-color: #a07ab3;
			border-radius: 5px;
			cursor: pointer;
			color: #ffffff;
			font-size: 14px;
			border: 0px;
			width: 100%;

		}

		.callapp_head {
			font-family: pvn;
			font-weight: bold;
			font-size: 20px;
			color: #2681DC;
		}

		.callapp_head_hr {
			border: 1px solid #2681DC;
		}

		.ui-widget-content {
			border: 0px solid #2681dc;
		}

		#show_report {
			width: 15%;
			float: left;
			margin: 15px 0;
		}

		#button_area {
			float: left;
			display: flex;
			align-items: center;
			justify-content: space-between;
			width: auto;
			height: auto;
		}

		.navigation {
			top: 0 !important;
		}

		#tabs {
			position: absolute;
			right: 8vw;
			width: 80vw;
		}

		.left input,
		.right input {
			width: 500px !important;

		}

		.left,
		.right {
			width: 500px;
			float: left;
			display: flex;
			align-items: center;
			justify-content: space-between;
		}
	</style>

	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/series-label.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>

	<script type="text/javascript">
		var aJaxURL = "server-side/report/operators_report.action.php";
		var aJaxURL1 = "server-side/report/sales_statistics.action.php";
		var tName = "example0";
		var tbName = "tabs";
		var fName = "add-edit-form";
		var file_name = '';
		var rand_file = '';

		$(document).ready(function() {
			GetDate("start_time");
			GetDate("end_time");
			
			$(".excel_answer_call_by_agent_info").button({});

		});
		
		function getData() {

			var i = 0;

			agent = '';
			queuet = '';



			var optionss = $('#myform_List_Agent_to option');
			var values = $.map(optionss, function(option) {
				if (agent != '') {
					agent += ',';
				}
				agent += "'" + option.value + "'";
			});

			start_time = $('#start_time').val();
			end_time = $('#end_time').val();

			$.getJSON("server-side/report/operators_rep.action.php?start=" + start_time + "&end=" + end_time + "&agent=" + agent, function(json) {

				Highcharts.chart("", {
					chart: {
						renderTo: 'chart_container',
						margin: [50, 50, 100, 80]
					},
					title: {
						text: 'ნაპასუხები ზარები ოპერატორების მიხედვით',
						x: -20
					},
					xAxis: {
						categories: json[1]['agent'],
						labels: {
							rotation: -45,
							align: 'right'
						}
					},
					yAxis: {
						title: {
							text: 'ზარები'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						valueSuffix: json[1]['unit']
					},
					legend: {
						layout: 'vertical',
						align: 'left',
						verticalAlign: 'top',
						borderWidth: 0
					},
					series: [{
						name: json[1]['name'],
						data: json[1]['call_count'],
						type: "column"
					}]
				});
			});
		}

		function go_next(val, par) {
			if (val != undefined) {
				$("#myform_List_" + par + "_from option:selected").remove();
				$("#myform_List_" + par + "_to").append(new Option(val, val));
			}
		}

		function go_previous(val, par) {
			if (val != undefined) {
				$("#myform_List_" + par + "_to option:selected").remove();
				$("#myform_List_" + par + "_from").append(new Option(val, val));
			}
		}

		function go_last(par) {
			var options = $('#myform_List_' + par + '_from option');
			$("#myform_List_" + par + "_from option").remove();
			var values = $.map(options, function(option) {
				$("#myform_List_" + par + "_to").append(new Option(option.value, option.value));
			});
		}
		function go_first(par) {
			var options = $('#myform_List_' + par + '_to option');
			$("#myform_List_" + par + "_to option").remove();
			var values = $.map(options, function(option) {
				$("#myform_List_" + par + "_from").append(new Option(option.value, option.value));
			});
		}

		
		$(document).on('click', '#excel', function() {
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("answer_call_by_queue", "ოპერატორების მიხედვით", []);
                $("#loading1").hide();
            }, 200);
        });


        function LoadKendoTable(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'answer_call_by_queue';
            var actions = '<button style="float:right;" id="excel">ექსპორტი EXCEL</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 14;
            var columnsSQL = [
				"operator:string", 
				"calls:string", 
				"proccessed:string", 
				"unproccessed:string", 
				"answered:string", 
				"proccessAVGduration:string", 
				"missedCalls:string", 
				"callTime:string", 
				"AVGduration:string", 
				"callMAXtime:string", 
				"callMINtime:string", 
				"outgoingCall:string", 
				"outgoingProcessed:string", 
				"outgoingUnprocessed:string"
			];
			
            var columnGeoNames = [
				"ოპერატორი", 
				"ზარები", 
				"დამუშ", 
				"დაუმუშ", 
				"ნაპას %", 
				"დამუშავების საშ. ხანგრძლივობა", 
				"გამოტოვებული ზარები", 
				"ზარის დრო", 
				"საშუა.ხანგრძლ.", 
				"ზარის <br>მაქს.დრო", 
				"ზარის<br> მინ.დრო", 
				"გამავალი<br> ზარი", 
				"დამუშ", 
				"დაუმუშ"
			];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI('server-side/report/operators_report.action.php', 'check', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0, '', '', '', true, false, true, []);
        }

		$(document).on("click", "#show_report", function() {
			var i = 0;
			paramq = new Object();
			parama = new Object();
			parame = new Object();
			parame.agent = '';
			parame.queuet = '';
			paramm = "server-side/report/operators_report.action.php";

			getData();


			var options = $('#myform_List_Agent_to option');
			var values = $.map(options, function(option) {
				if (parame.agent != '') {
					parame.agent += ',';
				}
				parame.agent += "'" + option.value + "'";
			});

			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.act = 'check';
			

			if (parame.agent == '') {
				alert('აირჩიე ოპერატორი');
			} else {
				var hidden = '&start_time='+parame.start_time+'&end_time='+parame.end_time+'&agent='+parame.agent;
				LoadKendoTable(hidden);
				

				// $("#loading1").show();
				// $.ajax({
				// 	url: paramm,
				// 	data: parame,
				// 	success: function(data) {
				// 		$("#answer_call_by_queue").html(data.page.answer_call_by_queue);
				// 	}
				// }).done(function(){
				// 	$("#loading1").hide();
				// });
			}
		});

		$(document).on("click", "#answear_dialog", function() {
			var name = $(this).attr('user');

			LoadDialog(name);
		});
		$(document).on("click", "#undone_dialog", function() {
			var name = $(this).attr('user1');

			LoadDialog1(name);
		});
		$(document).on("click", "#answear_dialog1", function() {
			var name = $(this).attr('user2');
			LoadDialog3(name);
		});

		// $(document).on("click", "#undone_dialog1", function () {
		// 	var name = $(this).attr('user3');
		// 	LoadDialog2(name)
		// });

		var record;

		function play(record) {

			link = 'https://91.233.15.136/records/' + record;
			var newWin = window.open(link, 'newWin', 'width=320,height=200');
			newWin.focus();

		}

		function LoadDialog(name) {
			parame = new Object();
			paramm = "server-side/report/operators_report.action.php";
			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.act = 'answear_dialog';
			parame.agent = '';
			parame.queuet = '';
			parame.name = name;


			var options = $('#myform_List_Agent_to option');
			var values = $.map(options, function(option) {
				if (parame.agent != '') {
					parame.agent += ',';
				}
				parame.agent += "'" + option.value + "'";
			});

			$("#loading1").show();
			$.ajax({
				url: paramm,
				data: parame,
				success: function(data) {
					$("#test").html(data.page.answear_dialog);
					GetDialog("add-edit-form", 1000, "auto", "", "top");
					GetDataTable("example", aJaxURL, "answear_dialog_table&start_time=" + parame.start_time + "&end_time=" + parame.end_time + "&queuet=" + parame.queuet + "&agent=" + parame.agent + "&name=" + name, 8, "", 0, "", 1, "desc", '', "<'dataTable_buttons'T><'F'Cfipl>");

				}
			}).done(function(){
					$("#loading1").hide();
			});
		}

		function LoadDialog1(name) {
			parame = new Object();
			paramm = "server-side/report/operators_report.action.php";
			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.act = 'undone_dialog';
			parame.agent = '';
			parame.queuet = '';
			parame.name = name;


			var options = $('#myform_List_Agent_to option');
			var values = $.map(options, function(option) {
				if (parame.agent != '') {
					parame.agent += ',';
				}
				parame.agent += "'" + option.value + "'";
			});
			$("#loading1").show();
			$.ajax({
				url: paramm,
				data: parame,
				success: function(data) {
					$("#add-edit-form-undone").html(data.page.answear_dialog);
					var button = {
						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog",
							click: function() {
								$(this).dialog("close");
							}
						}
					};
					GetDialog("add-edit-form-undone", 1000, "auto", button, "top");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example2", aJaxURL, "undone_dialog_table&start_time=" + parame.start_time + "&end_time=" + parame.end_time + "&queuet=" + parame.queuet + "&agent=" + parame.agent + "&name=" + name, 8, "", 0, "", 1, "desc", '', "<'dataTable_buttons'T><'F'Cfipl>");

					$("div").removeClass("ui-widget-overlay");
				}
			}).done(function(){
					$("#loading1").hide();
			});
		}

		function LoadDialog2(name) {
			parame = new Object();
			paramm = "server-side/report/operators_report.action.php";
			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.act = 'undone_dialog1';
			parame.agent = '';
			parame.queuet = '';
			parame.name = name;



			var options = $('#myform_List_Agent_to option');
			var values = $.map(options, function(option) {
				if (parame.agent != '') {
					parame.agent += ',';

				}
				parame.agent += "'" + option.value + "'";
			});
			$("#loading1").show();
			$.ajax({
				url: paramm,
				data: parame,
				success: function(data) {
					$("#add-edit-form-undone1").html(data.page.answear_dialog);
					var button = {
						"cancel": {
							text: "დახურვა",
							id: "cancel-dialog",
							click: function() {
								$(this).dialog("close");
							}
						}
					};
					GetDialog("add-edit-form-undone1", 1000, "auto", button, "top");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example2", aJaxURL, "undone_dialog_table1&start_time=" + parame.start_time + "&end_time=" + parame.end_time + "&queuet=" + parame.queuet + "&agent=" + parame.agent + "&name=" + name, 8, "", 0, "", 1, "desc", '', "<'dataTable_buttons'T><'F'Cfipl>");

					$("div").removeClass("ui-widget-overlay");
				}
			}).done(function(){
					$("#loading1").hide();
			});
		}

		function LoadDialog3(name) {
			parame = new Object();
			paramm = "server-side/report/operators_report.action.php";
			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.act = 'answear_dialog1';
			parame.agent = '';
			parame.queuet = '';
			parame.name = name;




			var options = $('#myform_List_Agent_to option');
			var values = $.map(options, function(option) {
				if (parame.agent != '') {
					parame.agent += ',';

				}
				parame.agent += "'" + option.value + "'";
			});
			$("#loading1").show();
			$.ajax({
				url: paramm,
				data: parame,
				success: function(data) {
					$("#add-edit-form1").html(data.page.answear_dialog);
					GetDialog("add-edit-form1", 850, "auto", "", "top");
					/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
					GetDataTable("example2_1", aJaxURL, "answear_dialog_table1&start_time=" + parame.start_time + "&end_time=" + parame.end_time + "&queuet=" + parame.queuet + "&agent=" + parame.agent + "&name=" + name, 8, "", 0, "", 1, "desc", '', "<'dataTable_buttons'T><'F'Cfipl>");

				}
			}).done(function(){
					$("#loading1").hide();
			});
		}

		$(document).on("click", ".excel_answer_call_by_agent_info", function() {
			var i = 0;
			parame = new Object();
			parame.start_time = $('#start_time').val();
			parame.end_time = $('#end_time').val();
			parame.agent = '';

			var options = $('#myform_List_Agent_to option');
			var values = $.map(options, function(option) {
				if (parame.agent != '') {
					parame.agent += ',';
				}
				parame.agent += "'" + option.value + "'";
			});
			$("#loading1").show();
			$.ajax({
				url: 'server-side/report/technical/excel_answer_call_by_agent_info.php',
				data: parame,
				success: function(data) {
					if (data == 1) {
						alert('ჩანაწერი არ მოიძებნა');
					} else {
						SaveToDisk('server-side/report/technical/excel.xls', 'excel.xls');
					}
				}
			}).done(function(){
					$("#loading1").hide();
			});
		});

		$(document).on("click", ".download1", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download1").css("background", "#F99B03");
			$(this).css("background", "#33dd33");

		});
		$(document).on("click", ".download3", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);

			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download1").css("background", "#F99B03");
			$(this).css("background", "#33dd33");

		});
		$(document).on("click", ".download", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);
			//	alert('hfgj');
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#408c99");
			$(this).css("background", "#FF5555");

		});
		$(document).on("click", ".download2", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);
			//	alert('hfgj');
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#408c99");
			$(this).css("background", "#FF5555");

		});
		$(document).on("click", ".download4", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://91.233.15.136:8181/' + link;
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);
			//	alert('hfgj');
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#408c99");
			$(this).css("background", "#FF5555");

		});


		var record;

		function play(record) {

			link = 'http://91.233.15.136:8181/' + record;
			GetDialog_audio("audio_dialog", "auto", "auto", "");
			$(".ui-dialog-buttonpane").html(" ");
			$(".ui-dialog-buttonpane").removeClass("ui-widget-content ui-helper-clearfix ui-dialog-buttonpane");
			$("#audio_dialog").html('<audio controls autoplay style="width:500px; min-height: 33px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
		}



		function SaveToDisk(fileURL, fileName) {
			var hyperlink = document.createElement('a');
			hyperlink.href = fileURL;
			hyperlink.target = '_blank';
			hyperlink.download = fileName || fileURL;

			(document.body || document.documentElement).appendChild(hyperlink);
			hyperlink.onclick = function() {
				(document.body || document.documentElement).removeChild(hyperlink);
			};

			var mouseEvent = new MouseEvent('click', {
				view: window,
				bubbles: true,
				cancelable: true
			});

			hyperlink.dispatchEvent(mouseEvent);

			// NEVER use "revoeObjectURL" here
			// you can use it inside "onclick" handler, though.
			// (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);

			// if you're writing cross-browser function:
			if (!navigator.mozGetUserMedia) { // i.e. if it is NOT Firefox
				window.URL.revokeObjectURL(hyperlink.href);
			}
		}
		// $(document).on("click", "#name", function () {
		// 	var start_time 	= $('#start_time').val();
		// 	//var start_time = start_timee.substring(0,10);
		// 	var end_time 	= $('#end_time').val();
		// 	//var end_time = end_timee.substring(0,10);
		// 	var name = $(this).text();
		// 	var pathname = window.location.pathname;
		// 	window.open("person_info.php?name="+name+"&start_time="+start_time+"&end_time="+end_time, '_blank');
		// });
	</script>

</head>

<body>
	<div style="margin-top: 12px; margin-left: 5px;" class="callapp_head">ოპერატორების მიხედვით
		<hr class="callapp_head_hr">
	</div>
	<div id="tabs" style="position: absolute !important;display: inline-block; width: 75%;">

		<div style="width: 100%; float:left;">
			<span>აირჩიე ოპერატორი</span>
			<hr>
			<table border="0" cellspacing="0" cellpadding="8" style="width:31%;float:left;">
				<tbody>
					<tr>
						<td>ხელმისაწვდომია<br><br>
							<select size="10" name="excel_answer_call_by_agent_info" multiple="multiple" id="myform_List_Agent_from" style="height: 100px;width: 173px;">
								<?php
								$db->setQuery("  SELECT users.id,user_info.`name`
                                                    FROM   users
                                                    JOIN   user_info ON user_info.user_id = users.id");
								$rResult = $db->getResultArray();
								foreach ($rResult[result] as $aRow) {
									echo '<option value="' . $aRow[name] . '">' . $aRow[name] . '</option>';
								}
								?>
							</select>
						</td>
						<td align="left">
							<a onclick="go_next($('#myform_List_Agent_from option:selected').val(),'Agent')"><img src="media/images/go-next.png" width="16" height="16" border="0"></a>
							<a onclick="go_previous($('#myform_List_Agent_to option:selected').val(),'Agent')"><img src="media/images/go-previous.png" width="16" height="16" border="0"></a>
							<br>
							<br>
							<a onclick="go_last('Agent')"><img src="media/images/go-last.png" width="16" height="16" border="0"></a>
							<a onclick="go_first('Agent')"><img src="media/images/go-first.png" width="16" height="16" border="0"></a>
						</td>
						<td>
							არჩეული<br><br>
							<select size="10" name="List_Agent[]" multiple="multiple" style="height: 100px;width: 125px;" id="myform_List_Agent_to">
							</select>
						</td>
					</tr>
				</tbody>
			</table>

			<div style="width:40%;float:left;margin-left:10%;display:flex;flex-direction:column;align-items:center;">
				<hr style="width: 97%;">
				<div id="button_area">
					<div class="left" style="width: 180px;">
						<label for="search_start" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასაწყისი</label>
						<input type="text" name="search_start" value="" id="start_time" class="inpt right" style="width: 95px; height: 16px;" />
					</div>
					<div class="right" style="width: 190px;">
						<label for="search_end" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასასრული</label>
						<input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 95px; height: 16px;" />
					</div>
				</div>
				<input style="margin-left: 15px;width:50%;" id="show_report" name="show_report" type="submit" value="რეპორტების ჩვენება">
			</div>
		</div>
		<div id="rest" style="margin-top: 300px; width: 100%; float:none;">
			<div id="answer_call_by_queue"></div>
			<!-- <table width="99%" cellpadding="3" cellspacing="3" border="0" class="sortable" id="table1">
				<caption>ნაპასუხები ზარები ოპერატორების მიხედვით<a style="float: right;"><button style="margin-right: 5px;" class="excel_answer_call_by_agent_info">Excel</button></a></caption>
				<thead>
					<tr>
						<th style="width: 15%"><a class="sortheader">ოპერატორი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">ზარები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">დამუშ.<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">დაუმუშ.<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">ნაპას.%<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">დამუშავების საშ. ხანგრძლივობა<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">გამოტოვებული ზარები<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">ზარის დრო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">საშუა.ხანგრძლ.<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">ზარის <br>მაქს.დრო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 8%"><a class="sortheader">ზარის<br> მინ.დრო<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 7%"><a class="sortheader">გამავალი<br> ზარი<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 7%"><a class="sortheader">დამუშ.<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
						<th style="width: 7%"><a class="sortheader">დაუმუშ.<span class="sortarrow">&nbsp;&nbsp;&nbsp;</span></a></th>
					</tr>
					<tr id="technik_info">
						<td></td>
						<td></td>
						<td id="answear_dialog" style="cursor: pointer; text-decoration: underline;"></td>
						<td></td>
						<td></td>
						<td></td>
						<td id="undone_dialog" style="cursor: pointer; text-decoration: underline;"></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td id="answear_dialog1" style="cursor: pointer; text-decoration: underline;"></td>
						<td></td>
						<td id="undone_dialog1" style="cursor: pointer;"></td>
					</tr>
				</thead>
				<tbody id="answer_call_by_queue">

				</tbody>

			</table> -->
			<br>
			<div id="chart_container" style="float:left; width: 90%; height: 300px; margin-left: 68px;"></div>
			<br>
		</div>
	</div>

	<!-- jQuery Dialog -->
	<div id="add-edit-form" class="form-dialog" title="ნაპასუხები ზარები">
		<div id="test"></div>
	</div>

	<!-- jQuery Dialog -->
	<div id="add-edit-form-undone" class="form-dialog" title="დაუმუშავებელი შემომავავლი ზარები">
		<!-- aJax -->
	</div>

	<!-- jQuery Dialog -->
	<div id="add-edit-form1" class="form-dialog" title="გამავალი ზარები">
		<!-- aJax -->
	</div>
	<!-- jQuery Dialog -->
	<div id="add-edit-form-undone1" class="form-dialog" title="დაუმუშავებელი გამავალი ზარები">
		<div id="test1"></div>
		<!-- jQuery Dialog -->
		<div id="audio_dialog" title="ჩანაწერი">
		</div>
	</div>
	<div id="loading1"><p><img src="media/images/loader.gif" /></p></div>
</body>