
import Tdg from "../tdg.class.js";

export default class Flashpanel extends Tdg {

    constructor(){
        super();

        self.Flashpanel = this;

        this.state = {
            dom: document.querySelector("section[content]"),
            element: [],
            interface: this.CreateElement({
                element: "interface",
                attributes: ["flashpanel"]
                
            }),
            content: this.CreateElement({
                element:"interface",
                attributes: ["flashpanel-content", "hidden"],
                // style: {
                //     right: "-9999px"
                // }
            }),
            fieldset: [],
            table: [],
            currentKey: ''
            
        }

    }

    
    init = async () => {

        await this.build(); 

        this.state.dom.insertBefore(this.state.interface, this.state.dom.lastChild)

        this.state.dom.insertBefore(this.state.content, this.state.dom.lastChild)
        
        
    }

    build = async () => {

        this.buildSource()

    }

  

    /**
     * CREATE ICON FOR FLASHPANEL
     * @param {*} dataObj 
     */
    createSourceItem = (source) => {
        
        this.state.element[source.key] = this.CreateElement({
            element: "icon",
            attributes: ["svg"],
            children: ` <svg viewBox="0 0 32 32" width="32" height="32">
                            <use xlink:href="Assets/images/icons/sources/source.svg#${source.key}-o" />
                        </svg>`,
            onclick: (e) => {
                
                if(e.target.getAttribute("actived")){
                    e.target.removeAttribute("actived")
                    typeof this.state.fieldset.taken != "undefined" && this.state.fieldset.taken.remove()
                    typeof this.state.fieldset.waiter != "undefined" && this.state.fieldset.waiter.remove()
                    this.state.content.style.display = "none"
                    typeof this.state.element.blur != "undefined" && this.state.element.blur.remove()
                    this.state.currentKey = '';
                }else{
                    e.target.setAttribute("actived", true)
                    this.setBlur(this.state.dom);
                    this.state.content.style.display = "grid"
                    this.state.currentKey = source.key;
                    // $(this.state.content).animate({
                    //     right: "toggle"
                    // })
                    this.buildFieldSet()
                }
                
                
            }
        })

        this.append(this.state.interface, this.state.element[source.key])
    }

    setBlur = (dom) => {
        this.state.element.blur = this.CreateElement({
            element: "overflow",
            type: "blur"
        })

        dom.insertBefore(this.state.element.blur, dom.lastChild)
    }
    removeBlur = (dom) => {
        document.querySelector("overflow[type='blur']").remove();
    }

    /**
     * GET SOURCE DATA
     */
    buildSource = async () => {

        this.state.data = await this.getResponse({
                            route: "source",
                            act: "get",
                            ns: "Helpers"
                        })

        this.state.data.length > 0 && this.state.data.forEach(source => this.createSourceItem(source))

    }


    /**
     * Create FieldSet Element
     * @param {String} name 
     * @param {String} title 
     * @returns 
     */
    buildTakenFieldSet = async () => {

        this.state.fieldset.taken = this.CreateElement({
            element: "div",
            attributes: ["fieldset"],
            name: "taken",
            title: "აყვანილი მომართვები"
        })

        this.append(this.state.content, this.state.fieldset.taken)

        let jsonData = [{
            id: 1,
            operator: {
                text: "NAME",
                avatar: "Assets/images/no-image.png",
                status: {
                    key: "active",
                    title: "აქტიური",
                    background: "red"
                },
                extension: {
                    text: 105,
                    background: "#B2E9A9",
                    foreground: ""
                }
            },
            accList: [{
                text: "2 500 055",
                background: "#000",
                foreground: "#FFF"
            }],
            account: {
                text: "2 500 055",
                background: "#DEFFD9",
                foreground: ""
            },
            sourceKey: "phone",
            author: {
                text: "Davit Gogua",
                avatar: "Assets/images/no-image.png",
                number: {
                    text: "577 22 00 44",
                    foreground: "",
                    background: ""
                }
            },
            type: 'incomming',
            duration: "02:20",
            totalDuration: "05:20"
        },
        {
            id: 1,
            operator: {
                text: "NAME",
                avatar: "Assets/images/no-image.png",
                status: {
                    key: "active",
                    title: "აქტიური",
                    background: "red"
                },
                extension: {
                    text: 105,
                    background: "#B2E9A9",
                    foreground: ""
                }
            },
            accList: [{
                text: "2 500 055",
                background: "#000",
                foreground: "#FFF"
            }],
            account: {
                text: "2 500 055",
                background: "#FFF7CF",
                foreground: ""
            },
            sourceKey: "phone",
            author: {
                text: "Davit Gogua",
                avatar: "Assets/images/no-image.png",
                number: {
                    text: "577 22 00 44",
                    foreground: "",
                    background: ""
                }
            },
            type: 'incomming',
            duration: "02:20",
            totalDuration: "05:20"
        }];

        this.state.table.taken = this.CreateElement({
            element: "kendo",
            type: "table",
            ws: true,
            column: [
                {
                    field: "ოპერატორი",
                    filter: false
                },
                {
                    field: "ექაუნთი",
                    size: 100,
                    filter: false
                },
                {
                    field: "კომუნიკაციის არხი",
                    filter: false
                },
                {
                    field: "მომართვის ავტორი",
                    filter: false
                },
                {
                    field: "ხბა",
                    size: 80,
                    filter: false
                },
                {
                    field: "სულ ხბა",
                    size: 80,
                    filter: false
                }
            ],
            data: {
                wsRoute: "phoneTaken"
            },
            option: {
                footer: false
            },
            template: `
                    <tr data-uid="#: id #" >
                        
                        <td class="operator" data-bind="#: operator.text #">
                            # if(!operator.avatar){ # 
                                <img class="avatar" src="Assets/images/no-image.png" />
                            # }else{ #
                                <img class="avatar" src="#= operator.avatar #" />
                            # } #
                            <span class="name">#: operator.text #</span>
                            <span class="extension" style="background-color: #: operator.extension.background #; color: #: operator.extension.foreground #">#: operator.extension.text #</span>
                        </td>
                        <td class="account" data-bind="#: account.text #">
                            <span class="account" style="background-color: #: account.background #; color: #: account.foreground #">#: account.text #</span>
                        </td>
                        <td class="source">

                            ${await this.getSource().then(data => {
                                let icon = '';
                                data.forEach(x => {
                                    icon += $(this.CreateElement({
                                        element: "icon",
                                        children: ` <svg title='${x.key}' viewBox="0 0 17 17" key=${x.key} width="17" height="17">
                                                        <use xlink:href="Assets/images/icons/sources/source.svg\\\\\#${x.key}-i" />
                                                    </svg>`
                                    })).html()

                                })

                                return icon;
                            }).catch(e => console.log(e))}
                        </td>
                        <td class="author">
                            # if(!author.avatar){ # 
                                <img class="avatar" src="Assets/images/no-image.png" />
                            # }else{ #
                                <img class="avatar" src="#= author.avatar #" />
                            # } #
                            <span class="name" title="#: author.number.text #">#: author.text #</span>
                        </td>
                        <td class="duration">
                            <span class="count">#: duration #</span>
                        </td>
                        <td class="total_duration">
                            <span class="count">#: totalDuration #</span>
                        </td>
                    </tr>
                `
        
        })


        this.append(this.state.fieldset.taken, this.state.table.taken) 

    }

    getSource = async () => {

        let sources = await this.getResponse({
            route: "source",
            act: "get",
            ns: "Helpers"})

        return sources

    }

    buildWaiterFieldSet = () => {

        this.state.fieldset.waiter = this.CreateElement({
            element: "div",
            name: "waiter",
            title: "რიგში",
            attributes: ["fieldset"],
            style: {
                width: "300px"
            }
        })

        this.append(this.state.content, this.state.fieldset.waiter)


        this.state.table.waiter = this.CreateElement({
            element: "kendo",
            type: "table",
            column: [
                {
                    field: "id",
                    size: "52px"
                },
                {
                    field: "მომ. ავტორი",
                },
                {
                    field: "დრო",
                    size: "100px"
                }
            ],
            data: {
                route: "Flashpanel",
                act: "getQueue",
                key: this.state.currentKey
            },
            option: {
                footer: false
            }
        })


        this.append(this.state.fieldset.waiter, this.state.table.waiter) 

    }

    buildFieldSet = async () => {

        await this.buildTakenFieldSet()
        await this.buildWaiterFieldSet()

    }

}