import Tdg from "../tdg.class.js";
import {Theme} from "../common/theme.class.js";
export default class Header extends Tdg {

  constructor() {
    super();

    this.state = {
      section: document.querySelector('section[header]'),
      element: []
    };

  }


  init = async () => {

    await this.build();

  }


  build = async () => {

    this.append(this.state.section, this.createLogo());

    this.append(this.state.section, this.buildInterface())

    this.append(this.state.userInterface, this.createChat())
    this.append(this.state.userInterface, this.createNotification())
    this.append(this.state.userInterface, this.createProfile())
  }

  /**
   * Create Header Logo
   * @returns LOGO
   */
  createLogo = () => {

    let logo = this.CreateElement({
      element: "logo",
      attributes: ["callapp"]
    })

    return logo

  }

  buildInterface =  () => {

    this.state.userInterface = this.CreateElement({
      element: "interface",
      attributes: ["user"]
    })

    return this.state.userInterface;
  }


  createDrop = (content, hideContent) => {

    this.state.drop = this.CreateElement({
      element: "drop"
    }, this.CreateElement({
      element: "content",
      children: content,
      onClick: this.handleDrop
    }), this.CreateElement({
      element: "hidecontent",
      attributes: [{ "aria-open": false }],
      children: hideContent
    }))

    return this.state.drop

  }


  createChat = () => {

    this.state.chat = this.createDrop(this.buildSendMessage(), 'CHAT')

    return this.state.chat;

  }

  buildSendMessage = () => {

    this.state.element.chat = this.CreateElement({
      element: "icon",
      name: "webchat",
      children: `<svg viewbox="0 0 24 24" width=24 height=24>
                  <use xlink:href="Assets/images/icons/icons.svg#sendmessage" />
                </svg>`
    })

    return this.state.element.chat

  }

  createNotification = () => {

    this.state.notification = this.createDrop(this.buildNotification(), 'notification')

    return this.state.notification;

  }

  buildNotification = () => {

    this.state.element.notification = this.CreateElement({
      element: "icon",
      name: "notification",
      children: `<svg viewBox="0 0 24 24" width="24" height="24">
                    <use xlink:href="Assets/images/icons/icons.svg#notification" />
                </svg>`
    })

    return this.state.element.notification

  }

  createProfile = () => {

    this.state.profile = this.createDrop(this.buildProfile(), this.buildProfileMenu(this.buildStatusSubMenu(),this.buildChannelSubMenu()))

    return this.state.profile;

  }

  buildProfile = () => {

    this.state.element.profile = this.CreateElement({
      element: "menu",
      name: "profileMenu",
    }, this.CreateElement({
      element: "div",
      class: "menuheader",
      children: `<div>
                    <span name="username">Nino Bagratashvili</span>
                    <span name="status">Admin</span>
                </div>`
    },this.CreateElement({
      element: "div",
      children: `<div name="photo">
                    <img src="Assets/test.png" />
                </div>`
    }),this.CreateElement({
      element: "i",
      class: "fas fa-chevron-down"
    })))

    return this.state.element.profile

  }

  buildProfileMenu = (statussubmenu,channelsubmenu) => {
    this.state.element.profilemenu = this.CreateElement({
      element: "div",
      class: "profilemenuitems",
    },
    this.CreateElement({
      element:"section",
      class:"status_sub_menu",
      children: statussubmenu
    }),
    this.CreateElement({
      element:"section",
      class:"channel_sub_menu",
      children: channelsubmenu
    }),this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/polygon.svg"}],
      style:{
        width: "16px",
        position: "absolute",
        top: "-8px",
        right: "51px"
      }
    }),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"}]
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/user.svg"}],
      style:{
        width: "16px",
        justifySelf: "center"
      }
    }),this.CreateElement({
      element:"span",
      text:"პროფილი"
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}],
      onclick: this.handleMenu
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#25E207"
      }
    }),this.CreateElement({
      element:"span",
      text:"აქტიური",
    }),this.CreateElement({
      element:"span"
    },this.CreateElement({
      element: "i",
      class: "fas fa-angle-right",
      style: {
        float: "right"
      }
    }))),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"channel"}],
      onclick: this.handleMenu
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/monitor.svg"}],
      style:{
        width: "16px",
        justifySelf: "center"
      }
    }),this.CreateElement({
      element:"span",
      text:"კომ. არხი",
    }),this.CreateElement({
      element:"span"
    },this.CreateElement({
      element: "i",
      class: "fas fa-angle-right",
      style: {
        float: "right"
      }
    }))),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"switch"}],
      onclick: this.handleMenu
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/moon.svg"}],
      style:{
        width: "16px",
        justifySelf: "center"
      }
    }),this.CreateElement({
      element:"span",
      text:"Dark Mode",
    }),this.CreateElement({
      element:"span"
    },this.CreateElement({
      element: "span",
      class: `switchspan ${(new Theme().getTheme() == "dark" ? 'switchspanchange' : '')}`,
      onclick: this.handleSwitchDark,
      attributes: [{ "aria-on": false }],
      style:{
        width: "12px"
      }
    },this.CreateElement({
      element: "span",
      class: `onoffspan ${(new Theme().getTheme() == "dark" ? 'onoffspanchange' : '')}`,
      style:{
        width: "0px"
      }
    }),this.CreateElement({
      element: "i",
      class: "fas fa-times onoffexit",
      style:{
        fontSize: '9px'
      }
    }),
    this.CreateElement({
      element: "i",
      class: "fas fa-check onoffaccept",
      style:{
        fontSize: '8px'
      }
    })))),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"switch"}],  
      onclick: this.handleMenu
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/volume-2.svg"}],
      style:{
        width: "16px",
        justifySelf: "center"
      }
    }),this.CreateElement({
      element:"span",
      text:"ხმა",
    }),this.CreateElement({
      element:"span"
    },this.CreateElement({
      element: "span",
      class: `switchspan`,
      onclick: this.handleSwitchSound,
      attributes: [{ "aria-on": false }],
      style:{
        width: "12px"
      }
    },this.CreateElement({
      element: "span",
      class: `onoffspan`,
      style:{
        width: "0px"
      }
    }),this.CreateElement({
      element: "i",
      class: "fas fa-times onoffexit",
      style:{
        fontSize: '9px'
      }
    }),
    this.CreateElement({
      element: "i",
      class: "fas fa-check onoffaccept",
      style:{
        fontSize: '8px'
      }
    })))),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"}]
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/log-out.svg"}],
      style:{
        width: "16px",
        justifySelf: "center"
      }
    }),this.CreateElement({
      element:"span",
      text:"გასვლა"
    })))

    return this.state.element.profilemenu
  }

  buildStatusSubMenu = () => {
    this.state.element.statussubmenu = this.CreateElement({
      element: "section",
      class: "statussubmenu"
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/polygon.svg"}],
      style:{
        width: "16px",
        position: "absolute",
        top: '22px',
        right: '-12px',
        transform: 'rotate(90deg)'
      }
    }),this.CreateElement({
      element: "div",
      class: "profilemenuitems"
    },this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#25E207"
      }
    }),this.CreateElement({
      element:"span",
      text:"აქტიური",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#FBD300"
      }
    }),this.CreateElement({
      element:"span",
      text:"შესვენება",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#A8A8A8"
      }
    }),this.CreateElement({
      element:"span",
      text:"მოწევა",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#FEB019"
      }
    }),this.CreateElement({
      element:"span",
      text:"სხვა",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#0050EB"
      }
    }),this.CreateElement({
      element:"span",
      text:"გამავალი ზარი",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#FD0A50"
      }
    }),this.CreateElement({
      element:"span",
      text:"შვებულება",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#6F56ED"
      }
    }),this.CreateElement({
      element:"span",
      text:"შვებულება",
    }))))

    return this.state.element.statussubmenu
  }

  buildChannelSubMenu = () => {
    this.state.element.channelsubmenu = this.CreateElement({
      element: "section",
      class: "channelsubmenu"
    },this.CreateElement({
      element: "img",
      attributes:[{"src":"Assets/images/icons/polygon.svg"}],
      style:{
        width: "16px",
        position: "absolute",
        top: '22px',
        right: '-12px',
        transform: 'rotate(90deg)'
      }
    }),this.CreateElement({
      element: "div",
      class: "profilemenuitems"
    },this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#25E207"
      }
    }),this.CreateElement({
      element:"span",
      text:"აქტიური",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#FBD300"
      }
    }),this.CreateElement({
      element:"span",
      text:"შესვენება",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#A8A8A8"
      }
    }),this.CreateElement({
      element:"span",
      text:"მოწევა",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#FEB019"
      }
    }),this.CreateElement({
      element:"span",
      text:"სხვა",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#0050EB"
      }
    }),this.CreateElement({
      element:"span",
      text:"გამავალი ზარი",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#FD0A50"
      }
    }),this.CreateElement({
      element:"span",
      text:"შვებულება",
    })),this.CreateElement({
      element:"a",
      attributes:[{"href":"#"},{"btn":"status"}]
    },this.CreateElement({
      element: "i",
      class: "far fa-circle",
      style: {
        color: "#6F56ED"
      }
    }),this.CreateElement({
      element:"span",
      text:"შვებულება",
    }))))

    return this.state.element.channelsubmenu
  }

  handleDrop = function() {
    var rotate = $(this).children('menu').children(".menuheader").children(".fas");
    var check = $(this).nextAll("hidecontent").attr("aria-open");
    var checkname = $(this).children('menu').attr("name");

    if(check == "false"){
      var hidecontent = document.querySelector('hidecontent[aria-open=true]');
      $(hidecontent).attr("aria-open","false");
      $(hidecontent).hide(200,"swing");
    } 

    if(checkname){
      rotate.toggleClass('fasrotate'); 
    }else{
      $(".fasrotate").removeClass('fasrotate');
    }
    
    $(this).nextAll("hidecontent").attr("aria-open","true");
    $(this).nextAll("hidecontent").toggle(200,"swing");
  }

  handleSwitchDark = function(){
    $(this).toggleClass("switchspanchange");
    $(this).children('.onoffspan').toggleClass("onoffspanchange");

    if($(this).hasClass("switchspanchange")){
      new Theme().DarkMode()
    }else{
      new Theme().LightMode()
    }

  }

  handleSwitchSound = function(){
    $(this).toggleClass("switchspanchange");
    $(this).children('.onoffspan').toggleClass("onoffspanchange");

  }

  
  handleMenu = function(e){
    // if($(this).attr("btn") == "switch"){
    //   $(this).children().last().children(".switchspan ").click();
    // }
    
     if($(this).attr("btn") == "status"){
      $(this).children().last().children("i").toggleClass("fasrotate");
      $(".channelsubmenu").hide(200, "linear");
      $(".statussubmenu").toggle(200, "linear");
    }else if($(this).attr("btn") == "channel"){
      $(this).children().last().children("i").toggleClass("fasrotate");
      $(".statussubmenu").hide(200, "linear");
      $(".channelsubmenu").toggle(200, "linear");
    }
  }
  

}