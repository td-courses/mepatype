<html>

<head>
	<style type="text/css">
		#in_form,
		.in_form-class {
			overflow: visible;
		}

		.green {
			background-color: rgba(255, 0, 29, 0.51) !important;
			color: #0D233A !important;
			font-size: 14px !important;

		}

		.ui-dropdownchecklist-selector {
			background: #FFF;
			padding: 9px 0;
		}

		.ui-dropdownchecklist-text {
			display: inline;
		}

		.ui-dropdownchecklist-item {
			background: #FFF !important;
		}

		#report td:nth-child(3) {
			text-align: right !important;
		}

		#report td:nth-child(4) {
			text-align: right !important;
		}

		.download {
			background-color: #2dc100;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download1 {
			background-color: #ce8a14;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 14px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download2 {
			background-color: #d35454;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download4 {
			background-color: #2dc100;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			text-decoration: none;
			border: 0px !important;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download3 {
			background-color: #ce8a14;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download5 {
			background-color: #4980AF;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download6 {
			background-color: #CE5F14;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 98%;
			font-weight: normal !important;
		}

		.download7 {
			background-color: #f80aea;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download8 {
			background-color: #f80aea;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download9 {
			background-color: #D1EC38;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download10 {
			background-color: #7d3daf;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download11 {
			background-color: #2196f3;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download12 {
			background-color: #48a7f3;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download13 {
			background-color: #030202;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download14 {
			background-color: #089b98;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.download15 {
			background-color: #e81d42;
			border-radius: 0px;
			display: inline-block;
			cursor: pointer;
			color: #ffffff !important;
			font-size: 12px;
			border: 0px !important;
			text-decoration: none;
			text-overflow: ellipsis;
			width: 100%;
			font-weight: normal !important;
		}

		.search_button {
			padding: 2px;
			border-radius: 0px;
			background: #009688;
			cursor: pointer;
			float: left;
		}
	</style>
	<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>
	<script type="text/javascript">
		var title = '0';
		var i = 0;
		var done = ['', '', '', '', '', '', ''];
		var aJaxURL = "server-side/report/shetyobineba_report.action.php";
		var aJaxURL_inc = "server-side/report/operator_call_chat_out.action.php"; //server side folder url
		var CustomSaveURL = "server-side/view/automator.action.php";
		var tName = "report";

		var start = $("#search_start").val();
		var end = $("#search_end").val();

		$(document).ready(function() {
			$('#show_filter').button();
			$("#region_id, #raion_id, #call_type,#sofel_id ,#type_selector").chosen();
			$("#type_selector_chosen").css("margin-right", "15px");
			GetDate("search_start");
			GetDate("search_end");
			$("#dropdown2").dropdownchecklist({
				firstItemChecksAll: true,
				explicitClose: 'დახურვა',
				icon: {},
				width: 200,
				zIndex: 111
			});
			drawFirstLevel();
			$("#back").button({
				disabled: true
			});
			$("#back").button({
				icons: {
					primary: "ui-icon-arrowthick-1-w"
				}
			});
			$('#back').click(function() {
				i--;
				drawFirstLevel();
				if (i == 0) {
					$("#back").button({
						disabled: true
					});
				}
			});

			$("#type_selector").on('change', function() {
				var type = $("#type_selector").val();
				if (type == 'საერთო') {
					$('#type_selector option').prop('selected', true);
					$('#type_selector').trigger('chosen:updated');
				}

			});
		});




		$(document).on("click", "#show_filter", function() {
			drawFirstLevel();
		});
		$("#report tbody").on("click", "tr", function() {
			if (i == 2) {
				d_url1 = "&start=" + start + "&end=" + end + "&done=" + i + "&type=" + done[0] + "&category=" + done[1] + "&sub_category=" + done[2] + "&source_info=" + source_info + "&rep_selector=" + report_type;
				var nTds = $("td", this);
				var rID = $(nTds[1]).text();
				GetDataTable("report_1", aJaxURL, "get_in_page", 12, d_url1 + "&rid=" + rID, 0, "", 1, "asc", '', "<'dataTable_buttons'T><'F'Cfipl>");
				GetDialog("in_form", "90%", "auto", "", "left+43 top");
				SetEvents("", "", "", "report_1", "add-edit-form", aJaxURL_inc);
				$('.ui-dialog-buttonset').hide();
			}
			setTimeout(function() {
				$('#in_form  .ColVis,#in_form .dataTable_buttons').css('display', 'none');
				$("#show_copy_prit_exel1").css('background', '#E6F2F8');
				$("#show_copy_prit_exel1").children('img').attr('src', 'media/images/icons/select.png');
				$("#show_copy_prit_exel1").attr('myvar', '0');
			}, 190);
		});

		function drawFirstLevel() {
			var start = $("#search_start").val();
			var end = $("#search_end").val();
			var source_info = $("#dropdown2").val();
			var call_type = $("#call_type").val();
			var region_id = $("#region_id").val();
			var raion_id = $("#raion_id").val();
			var sofel_id = $("#sofel_id").val();
			var report_type = $("#type_selector").val();
			var d_url = "&start=" + start + "&end=" + end + "&done=" + i + "&departament=" + done[0] + "&type=" + done[1] + "&call_type=" + call_type + "&category=" + done[2] + "&sub_category=" + done[3] + "&source_info=" + source_info + "&rep_selector=" + report_type + "&region_id=" + region_id + "&raion_id=" + raion_id + "&sofel_id=" + sofel_id;
			var url = aJaxURL + "?act=get_category" + d_url;
			GetDataTable(tName, aJaxURL, "get_list", 3, d_url, 0, "", '', '', '', "<'dataTable_buttons'T><'F'Cfipl>");

			$.getJSON(url, function(json) {
				console.log(json);
				Highcharts.chart("chart_container", {
					chart: {
						renderTo: 'chart_container',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false,
						type: 'pie'
					},
					title: {
						text: json.text
					},
					tooltip: {
						formatter: function() {
							return '<b>' + this.point.name + '-' + this.point.y + ' მომართვა :  ' + this.percentage.toFixed(2) + ' %</b>';

						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>' + this.point.name + '-' + this.point.y + ' მომართვა :  ' + this.percentage.toFixed(2) + ' %</b>';
								}
							},
							point: {
								events: {
									click: function() {
										$("#back").button({
											disabled: false
										});
										done[i] = this.name;
										if (i == 2) i = 0;
										else i++;
										drawFirstLevel();
									}
								}
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'კატეგორიები',
						data: json.data
					}]
				});
				$("#chart_container").show();

				$("#total_quantity").html("იტვირთება....");
				setTimeout(function() {
					$("#total_quantity").html($("#qnt").html().split(">")[1]);
				}, 500);

				if (i == 2) {
					$("#report td").addClass("green");
				} else {
					$(".green").removeClass("green");
				}
			});

			setTimeout(function() {
				$('.ColVis, .dataTable_buttons').css('display', 'none');
				$("#show_copy_prit_exel").css('background', '#E6F2F8');
				$("#show_copy_prit_exel").children('img').attr('src', 'media/images/icons/select.png');
				$("#show_copy_prit_exel").attr('myvar', '0');
			}, 190);
		}

		$(document).on("click", ".download", function() {
			var str = 1;
			var link = ($(this).attr("str"));
			link = 'http://92.241.76.198:8081/' + link;
			var btn = {
				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			};
			GetDialog_audio("audio_dialog", "auto", "auto", btn);
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
			$(".download").css("background", "#408c99");
			$(this).css("background", "#FF5555");

		});

		function LoadDialog(fname) {
			var button_modal = {

				"cancel": {
					text: "დახურვა",
					id: "cancel-dialog",
					click: function() {
						$(this).dialog("close");
					}
				}
			}
			var dLength = [
				[5, 10, 30, -1],
				[5, 10, 30, "ყველა"]
			];
			GetDialog(fname, 1050, "auto", button_modal, "center top");
			$("#search_ab_pin").button();
			var dLength = [
				[5, 10, 30, -1],
				[5, 10, 30, "ყველა"]
			];
			GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check=" + $('#start_check').val() + "&end_check=" + $('#end_check').val() + "&phone=" + $("#phone").val() + "&s_u_user_id=" + $("#s_u_user_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");

			$("#table_history_length").css('top', '0px');
			$("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({
				search_contains: true
			});


		};

		$(document).on("click", "#search_ab_pin", function() {
			var dLength = [
				[5, 10, 30, -1],
				[5, 10, 30, "ყველა"]
			];
			GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check=" + $('#start_check').val() + "&end_check=" + $('#end_check').val() + "&phone=" + $("#phone").val() + "&s_u_user_id=" + $("#s_u_user_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");

			$("#table_history_length").css('top', '0px');
		});

		function show_right_side(id) {
			$("#right_side fieldset").hide();
			$("#" + id).show();
			//$(".add-edit-form-class").css("width", "1260");
			//$('#add-edit-form').dialog({ position: 'left top' });
			hide_right_side();

			var str = $("." + id).children('img').attr('src');
			str = str.substring(0, str.length - 4);
			$("#side_menu span").children('img').css('border-bottom', '');
			$("." + id).children('img').css('filter', 'brightness(0.1)');
			$("." + id).children('img').css('border-bottom', '2px solid #333');
		}

		function hide_right_side() {
			$(".info").children('img').css('filter', 'brightness(1.1)');
			$(".record").children('img').css('filter', 'brightness(1.1)');
			$(".call_resume").children('img').css('filter', 'brightness(1.1)');
			$("#record fieldset").show();
		}

		function listen(file) {
			$('#auau').each(function() {
				this.pause(); // Stop playing
				this.currentTime = 0; // Reset time
			});
			var url = 'http://92.241.76.198:8081/' + file;
			$("#auau source").attr('src', url);
			$("#auau").load();
		}

		$(document).on("click", "#show_copy_prit_exel", function() {

			if ($(this).attr('myvar') == 0) {
				$('.ColVis,.dataTable_buttons,#table_right_menu_content').css('display', 'block');
				$(this).css('background', '#2681DC');
				$(this).children('img').attr('src', 'media/images/icons/select_w.png');
				$(this).attr('myvar', '1');
			} else {
				$('.ColVis,.dataTable_buttons,#table_right_menu_content').css('display', 'none');
				$(this).css('background', '#E6F2F8');
				$(this).children('img').attr('src', 'media/images/icons/select.png');
				$(this).attr('myvar', '0');
			}
		});

		$(document).on("click", "#show_copy_prit_exel1", function() {

			if ($(this).attr('myvar') == 0) {
				$('#in_form .ColVis,#in_form .dataTable_buttons,#in_form #table_right_menu_content').css('display', 'block');
				$(this).css('background', '#2681DC');
				$(this).children('img').attr('src', 'media/images/icons/select_w.png');
				$(this).attr('myvar', '1');
			} else {
				$('#in_form .ColVis,#in_form .dataTable_buttons,#in_form #table_right_menu_content').css('display', 'none');
				$(this).css('background', '#E6F2F8');
				$(this).children('img').attr('src', 'media/images/icons/select.png');
				$(this).attr('myvar', '0');
			}
		});

		$(document).on("change", "#region_id", function() {
			param = new Object();
			param.act = "region_2";
			param.cat_id = $("#region_id").val();
			$.ajax({
				url: CustomSaveURL,
				data: param,
				success: function(data) {
					$("#raion_id").html(data.page);
					$("#raion_id").trigger("chosen:updated");
				}
			});
		});

		$(document).on("change", "#raion_id", function() {
			param = new Object();
			param.act = "region_2";
			param.cat_id = $("#raion_id").val();
			$.ajax({
				url: CustomSaveURL,
				data: param,
				success: function(data) {
					$("#sofel_id").html(data.page);
					$("#sofel_id").trigger("chosen:updated");
				}
			});
		});
	</script>
</head>

<body>
	<div id="tabs" style="  height: 850px;">
		<div class="l_responsible_fix">
			<div class="callapp_head">კატეგორიების მიხედვით
				<hr class="callapp_head_hr">
			</div>

			<div class="callapp_filter_body" myvar="1">
				<div style="width: 100%;">
					<table>
						<tr>
							<td style="margin-right:15px;">
								<button id="back" style="margin-top:0px; margin-right:15px;">უკან</button>
							</td>
							<td>
								<select multiple id="type_selector" style="margin-right:15px; width:150px;" data-placeholder="წყაროს ტიპი">
									<option value="">საერთო</option>
									<option value="ზარები">ზარები</option>
									<option value="ჩატი">ჩატი</option>
									<option value="Messenger">Messenger</option>
									<option value="Mail">Mail</option>
								</select>
							</td>
							<td style="width: 165px;">
								<select id="call_type" style="margin:0 15px; width: 150px;" data-placeholder="ზარის ტიპი">
									<option value="0">ყველა</option>
									<option value="1">შემომავალი</option>
									<option value="2">გამავალი</option>
								</select>
							</td>
							<td style="width: 200px;">
								<select id="region_id" style="width: 185px;">
									<?php
									$data = '<option value="0">აირჩიე რეგიონი</option>';
									global $db;
									$db->setQuery("  SELECT  id,
                                                        	   `name`
                                                        FROM   `regions` 
                                                        WHERE   parent_id = 0 AND actived = 1");

									$res = $db->getResultArray();

									foreach ($res[result] as $req) {
										$data .= '<option value="' . $req[id] . '">' . $req[name] . '</option>';
									}
									echo $data;
									?>
								</select>
							</td>
							<td style="width: 212px;">
								<select id="raion_id" style="width: 185px;">
									<option value="0">აირჩიე რაიონი</option>
								</select>
							</td>
							<td style="width: 212px;">
								<select id="sofel_id" style="width: 185px;">
									<option value="0">აირჩიე სოფელი</option>
								</select>
							</td>
							<td class="l_filter_date" style="width: 300px;">
								<span style="margin: 5px 10px 0 0">
									<input value="" class="callapp_filter_body_span_input" type="text" name="search_start" id="search_start" style="width: 100px;margin-top: 6px; margin-left:12px;">
									<label for="start_date" style="margin-top: 9px;">-დან</label>
								</span>
								<span style="margin: 5px 10px 0 0">
									<input value="" class="callapp_filter_body_span_input" type="text" name="search_end" id="search_end" style="width: 100px;margin-top: 6px;">
									<label for="end_date" style="margin-top: 9px;">-მდე</label>
								</span>
							</td>
							<td rowspan="4">
								<button id="show_filter">ფილტრი</button>
							</td>
							<td>
								<label class="left" style="margin:5px 0 0 40px">მომართვების ჯამური რაოდენობა: </label> <label id="total_quantity" class="left" style="margin:5px 0 0 2px; font-weight: bold;">5</label>
							</td>
						</tr>

					</table>
				</div>
			</div>
			<br><br><br><br>

			<div id="chart_container" style="width: 100%; height: 480px; margin-top:-30px;"></div>
			<input type="text" id="hidden_name" value="" style="display: none;" />
			<br><br><br><br><br><br>
			<table style="margin-top:10%;" id="table_right_menu">
				<tr>
					<td>
						<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
					</td>
					<td>
						<img alt="log" src="media/images/icons/log.png" height="14" width="14">
					</td>
					<td id="show_copy_prit_exel" myvar="0">
						<img alt="link" src="media/images/icons/select.png" height="14" width="14">
					</td>
				</tr>
			</table>


			<table class="display" id="report" style="width: 100%;">
				<thead>
					<tr id="datatable_header">
						<th>ID</th>
						<th style="width:50%">დასახელება</th>
						<th style="width:50%">რაოდენობა</th>
						<!-- <th style="width:25%">პროცენტი</th> -->
					</tr>
				</thead>
				<thead>
					<tr class="search_header">
						<th class="colum_hidden">
							<input type="text" name="search_id" value="ფილტრი" class="search_init" />
						</th>

						<th>
							<input type="text" name="search_number" value="ფილტრი" class="search_init">
						</th>

						<th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" />
						</th>

						<!-- <th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" />
						</th> -->


					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th id="qnt">&nbsp;</th>
						<!-- <th>&nbsp;</th> -->
					</tr>
				</tfoot>
			</table>

			<div id="audio_dialog" class="form-dialog" title="ჩანაწერი"></div>
			<div id="in_form" class="form-dialog">
				<table style="margin-top: 10px;" id="table_right_menu">
					<tr>
						<td>
							<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
						</td>
						<td>
							<img alt="log" src="media/images/icons/log.png" height="14" width="14">
						</td>
						<td id="show_copy_prit_exel1" myvar="0">
							<img alt="link" src="media/images/icons/select.png" height="14" width="14">
						</td>
					</tr>
				</table>
				<table class="display" id="report_1" style="width: 100%;">
					<thead style="font-size: 10px;">
						<tr id="datatable_header">
							<th>ID</th>
							<th style="width: 6%;">№</th>
							<th style="width:  13%;">თარიღი</th>
							<th style="width:  10%">სახელი/გვარი</th>
							<th style="width:  10%">ტელეფონი</th>
							<th style="width:  11%">ოპერატორი</th>
							<th style="width:  5%">ს.დრო</th>
							<th style="width:  12%">ქმედება</th>
						</tr>
					</thead>
					<thead>
						<tr class="search_header">
							<th class="colum_hidden">
								<input type="text" name="search_id" value="ფილტრი" class="search_init" style="" />
							</th>
							<th>
								<input type="text" name="search_number" value="" class="search_init" style="width:100%;">
							</th>
							<th>
								<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;" />
							</th>
							<th>
								<input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;" />
							</th>
							<th>
								<input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:100%;" />
							</th>

						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div id="add-edit-form" class="form-dialog" title="შემომავალი კომუნიკაცია"> </div>
</body>

</html>