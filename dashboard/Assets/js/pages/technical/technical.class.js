import Tdg from "../../tdg.class.js";
import Tabs from "../../components/tabs/tabs.class.js";
import Button from "../../components/button/button.class.js";
import Input from "../../components/input/input.class.js";
import kendoSelector from "../../components/selector/selector.class.js";
import Table from "../../components/table/table.class.js";
import multikendoSelector from "../../components/multiselector/multiselector.class.js";

export default class Technical extends Tdg {
    constructor(){
        super();
        this.state = {
            sectionName: $("component[techreport]"),
            element: []
        }
        this.initialize();

        /* this.sendRequest({
            route: "TechRe"
        }) */
    }

     

    initialize = () => {
        let tabData =   [
                            {
                                text:"მთავარი",
                                content_id: "main",
                                eventAttached: this.mainTab,
                                refresh: false
                            },
                            {
                                text:"ნაპასუხები",
                                content_id: "answered",
                                eventAttached: this.answeredTab,
                                refresh: false
                            },
                            {
                                text:"უპასუხო",
                                content_id: "unanswered",
                                eventAttached: this.unansweredTab,
                                refresh: false
                            },
                            {
                                text:"გადამისამართებული",
                                content_id: "transfered",
                                eventAttached: this.transferedTab,
                                refresh: false
                            },
                            {
                                text:"დამუშავებელი",
                                content_id: "done",
                                eventAttached: this.doneTab,
                                refresh: false
                            },
                            {
                                text:"დაუმუშავებელი",
                                content_id: "undone",
                                eventAttached: this.undoneTab,
                                refresh: false
                            },
                            {
                                text:"SL",
                                content_id: "sl",
                                eventAttached: this.slTab,
                                refresh: true
                            },
                            {
                                text:"FCR",
                                content_id: "fcr",
                                eventAttached: this.fcrTab,
                                refresh: true
                            },
                            {
                                text:"ASA",
                                content_id: "asa",
                                eventAttached: this.asaTab,
                                refresh: true
                            },
                            {
                                text:"ATT",
                                content_id: "att",
                                eventAttached: this.attTab,
                                refresh: true
                            },
                            {
                                text:"AHT",
                                content_id: "aht",
                                eventAttached: this.ahtTab,
                                refresh: true
                            },
                            {
                                text:"AWT",
                                content_id: "awt",
                                eventAttached: this.awtTab,
                                refresh: true
                            },
                            {
                                text:"CSAT",
                                content_id: "csat",
                                eventAttached: this.csatTab,
                                refresh: true
                            }
                        ];
        this.tabs = new Tabs('maintabs',tabData);
        this.tabs.initializeTabs();
        
        this.append(this.state.sectionName, this.Filterblock());

        this.append(this.state.element.filterBlock, this.queueSelector());
        this.append(this.state.element.filterBlock, this.extSelector());
        this.append(this.state.element.filterBlock, this.dateTimePicker_start());
        this.append(this.state.element.filterBlock, this.dateTimePicker_end());
        this.append(this.state.element.filterBlock, this.slTypeSelector());
        this.append(this.state.element.filterBlock, this.percentInput());
        this.append(this.state.element.filterBlock, this.secondsInput());
        this.append(this.state.element.filterBlock, this.slPercentSecondInput());
        this.append(this.state.element.filterBlock, this.filterButton());

        
        this.removeLoading();
    }

    mainTab = () => {
        alert(123);
    }
    answeredTab = () => {
        alert(987);
    }

    Filterblock = () => {

        this.state.element.filterBlock = this.CreateElement({
            element: "filterblock"
        })

        return this.state.element.filterBlock;

    }

    queueSelector = () => {
        this.state.element.queueSelector = this.CreateElement({
            element: "kendo",
            type: "multiselector",
            title: "რიგი",
            data: {
                route: "Authorization",
                act: "getQueue"
            },
        });
        return this.state.element.queueSelector;
    }

    extSelector = () => {
        this.state.element.extSelector = this.CreateElement({
            element: "kendo",
            type: "multiselector",
            title: "შიდა ნომერი",
            data: {
                route: "Authorization",
                act: "getExt"
            },
        });
        return this.state.element.extSelector;
    }
    slTypeSelector = () => {
        this.state.element.slTypeSelector = this.CreateElement({
            element: "kendo",
            type: "selector",
            title: "SL ტიპი",
            style:{
                width: "400px"
            },
            data: {
                route: "Authorization",
                act: "getExt"
            },
        });
        return this.state.element.slTypeSelector;
    }

    dateTimePicker_start = () => {
        this.state.element.datetime_start = this.CreateElement({
            element: "input",
            type: "date"
        });
        return this.state.element.datetime_start;
    }
    dateTimePicker_end = () => {
        this.state.element.datetime_end = this.CreateElement({
            element: "input",
            type: "date"
        });
        return this.state.element.datetime_end;
    }
    percentInput = () => {
        this.state.element.percentInput = new Input().build({
            type: "text",
            placeholder: "პროცენტი"
        });

        return this.state.element.percentInput;
    }
    secondsInput = () => {
        this.state.element.secondsInput = new Input().build({
            type: "text",
            placeholder: "წმ-ში"
        });

        return this.state.element.secondsInput;
    }
    slPercentSecondInput = () => {
        this.state.element.slPercentSecondInput = this.CreateElement({
            element: "input",
            type: "text",
            placeholder: "SL (%/წმ)"
        });

        return this.state.element.slPercentSecondInput;
    }
    filterButton = () => {

        this.state.element.filterButton = new Button({
            text: "ფილტრი",
            onclick: function () {
                alert(123)
            }

            
        })

        return this.state.element.filterButton
    }
    

}