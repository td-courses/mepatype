<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$action = $_REQUEST['act'];
$error = '';
$data = '';

switch ($action) {
    case 'get_add_page':
        $page = GetPage();
        $data = array('page' => $page);

        break;
    case 'get_edit_page':
        $source_id = $_REQUEST['id'];
        $page = GetPage(Getsource($source_id));
        $data = array('page' => $page);

        break;
    case 'get_list' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];

        $db->setQuery(" SELECT 	id,
                				date,
                				site_user_id,
                                my_user_name,
                			   `content`,
                				if(`status`=0,'გასაგზავნია',IF(`status`=1,'გაგზავნილია',''))
                        FROM   `send_site`");

        $data = $db->getList($count, $hidden);

        break;
    case "send_sms_easy":
        $data = array('data' => easySmsSendPage());
        break;
    case "send_sms_complicated":
        $data = array('data' => complicatedSmsSendPage());
        break;
    case 'save_source':
        $source_id = $_REQUEST['id'];
        $source_name = $_REQUEST['name'];
        $content = $_REQUEST['content'];


        if ($source_name != '') {

            if ($source_id == '') {
                if (!ChecksourceExist($source_name, $source_id)) {
                    Addsource($source_name, $content);
                } else {
                    $error = '"' . $source_name . '" უკვე არის სიაში!';

                }
            } else {
                Savesource($source_id, $source_name, $content);
            }

        }

        break;
    case 'disable':
        $source_id = $_REQUEST['id'];
        Disablesource($source_id);

        break;
    case 'get_shablon':

        $data = array('shablon' => getShablon());

        break;
    case 'get_smsevent':

        $user_id = $_SESSION['USERID'];
        $test = $_REQUEST['test'];
        $sms_id = $_REQUEST['sms_id'];
        $res = mysql_query("SELECT  id
						   FROM  `sms_event`
						   WHERE `status` = 1");

        if (mysql_num_rows($res) > 0) {
            while ($row = mysql_fetch_assoc($res)) {
                mysql_query("UPDATE `sms_event` 
								SET 
									`user_id`='$user_id', 
									`sms_id`='$sms_id', 
									`content`='$test', 
									`date`= NOW(), 
									`status`='1'
							 WHERE (`id`='$row[id]');");

                $sms_event_event = $row[id];
                $data = array('insert' => $sms_event_event);
            }
        } else {
            mysql_query("INSERT INTO `sms_event`
								(`user_id`, `actived`, `sms_id`, `content`, `date`, `status`)
							VALUES
								('$user_id', 1, '$sms_id', '$test', NOW(), 1);");

            $sms_event_id = mysql_insert_id();
            $data = array('insert' => $sms_event_id);

        }


        break;
    case 'phone_directory':

        $html = getShablon();
        $data = array('html' => $html);

        break;
    case 'phone_dir_list':

        //data variables
        $html = phoneDirList();
        $data = array('html' => $html);

        break;
    case 'get_phone_dir_list':

//         $count = $_REQUEST['count'];
//         $hidden = $_REQUEST['hidden'];

//         $rResult = mysql_query("SELECT 	persons_phone.id,
//                                                 persons_phone.name,
//                                                 person_position AS position,
//                                                 persons_phone.phone_number AS number
//                                       FROM persons_phone
//                                       LEFT JOIN position ON position.id=persons_phone.position_id");

//         $data = array(
//             "aaData" => array()
//         );

//         while ($aRow = mysql_fetch_array($rResult)) {
//             $row = array();
//             for ($i = 0; $i < $count; $i++) {
//                 /* General output */
//                 $row[] = $aRow[$i];
//                 if ($i == ($count - 1)) {
//                     $row[] = '<div class="callapp_checkbox">
//                                   <input type="checkbox" id="pdl_checkbox_' . $aRow[$hidden] . '" name="check_' . $aRow[$hidden] . '" value="' . $aRow['number'] . '" class="pdl-check" />
//                                   <label for="pdl_checkbox_' . $aRow[$hidden] . '"></label>
//                               </div>';
//                 }
//             }
//             $data['aaData'][] = $row;
//         }

        break;
    case 'new_number_form':

        $html = newNumberInDirectoryForm();
        $data = array('html' => $html);

        break;
    case 'save_new_phone_in_directory':

        //data variables
        $name = $_REQUEST["name"];
        $position = $_REQUEST["position"];
        $number = $_REQUEST["number"];

        //check if similar data exist in database
        if (phone_num_exists($number)) {
            $data_return = "exists";

            //if not exists
        } else {

            //check if data was saved successfully
            if (save_phone_in_dir($name, $position, $number)) {
                $data_return = "saved";
            } else {
                $data_return = "error";
            }
        }

        //send result to client side
        $data = array('data' => $data_return);

        break;

    case 'get_complicated_sms_list' :
        global $db;
        $sms_send_event_id = $_REQUEST["sms_send_event_id"];
        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];
        $db->setQuery("SELECT CONCAT('d',hard_site_event_detail.id) AS sms_name,
                    		  hard_site_event_detail.id,
                    		  hard_site_event_detail.site_user_id,
                              hard_site_event_detail.text,
                    		  IF(hard_site_event_detail.`status`=2,'გაგზავნილი',IF(hard_site_event_detail.`status`=1,'გასაგზავნია','არ გაიგზავნა')) AS sms_status
                       FROM   hard_site_event_detail
                       WHERE  hard_site_event_detail.sms_event_id = '$sms_send_event_id'");
        
        $data = $db->getList($count, $hidden);

        break;
    case 'get_sms_template_dialog':
        
        $data = array("html" => sms_template_dialog());
        
        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addsource($source_name, $content)
{
    global $db;
    $user = $_SESSION['USERID'];
    $db->setQuery("INSERT INTO 	 	`sms`
									(`name`, `message`, `user_id`, `actived`)
							VALUES 		
									('$source_name', '$content', '$user', '1')");
    $db->execQuery();
    //GLOBAL $log;
    //$log->setInsertLog('template');
}

function Savesource($source_id, $source_name, $content)
{
    global $db;
    $user = $_SESSION['USERID'];
    $db->setQuery("	UPDATE `sms`
					SET    `name` = '$source_name',
						   `message`='$content'
					WHERE  `id` = $source_id");
    $db->execQuery();
    //$log->setInsertLog('template',$template_id);
}

function Disablesource($source_id)
{
    global $db;
    $user = $_SESSION['USERID'];
    $db->setQuery("	UPDATE `sms`
					SET    `actived` = 0
					WHERE  `id` = $source_id");
    $db->execQuery();
    //$log->setInsertLog('template',$template_id);
}

function Getsource($source_id){
    
    global  $db;
    $db->setQuery("	SELECT id,
            			   site_user_id,
            			   site,
            			   my_user_name,
            			   content 
                    FROM  `send_site`
                    WHERE  id = $source_id");

    $res = $db->getResultArray();
    return $res[result][0];
    
}

function GetPage($res = ''){
    $hidde = $res[id];
        
    $data = '<fieldset class="new-sms-send-container">

                <!-- row unit -->
                <div class="new-sms-row">
	                <button class="jquery-ui-button" id="smsTemplate">
	                    <span>შაბლონი</span>
                    </button>
	                <button style="display:none" class="jquery-ui-button" id="smsPhoneDir">
	                    <span>ცნობარი</span>
	                </button>
                </div>
	            
	            <!-- row unit -->
	            <div class="new-sms-row">
	                <label for="smsAddressee">User Id</label>
	                <div style="width: 80%;" class="new-sms-input-holder">
	                    <input type="text" id="smsAddressee" value="'.$res[site_user_id].'">
                        <button style="margin-left: 10px;" class="jquery-ui-button" id="search_user_info">
    	                    <span>შემოწმება</span>
                        </button>
                    </div>
                </div>
                
                <div class="new-sms-row">
	                <label for="smsAddressee">სახელი,გვარი</label>
	                <div style="width: 80%;" class="new-sms-input-holder">
	                    <input type="text" id="user_name_surname" disabled="disabled" value="'.$res[my_user_name].'">
                    </div>
                </div>

                <div class="new-sms-row">
	                <label for="smsAddressee">საიტი</label>
	                <div style="width: 100%;" class="new-sms-input-holder">
	                    <select style="height: 18px; width: 100%;" id="my_site" class="idls object" multiple>'.web_site($res[site]).'</select>
                    </div>
                </div>

                <!-- row unit -->
                <div style="height:170px;" class="new-sms-row">
	                <label for="smsText">ტექსტი</label>
	                <div class="new-sms-input-holder">
	                    <textarea style="resize:none; height:143px;" id="newSmsText" maxlength="150">'.$res[content].'</textarea>
                    </div>
                </div>
                
                <!-- row unit -->
                <div " class="new-sms-row">
	                <button class="jquery-ui-button" id="sendNewSms">
	                    <span>გაგზავნა</span>
                    </button>
                </div>
	            
            </fieldset>
	   <input id="site_sms_id" type="text" value="' . $hidde . '" style="display: none;">
    ';
    return $data;
}

function sms_template_dialog() {
    
    return '
		<div class="sms-template-table-wrapper">
			<table class="display" id="sms_template_table" style="width: 100%;">
				<thead>
					<tr id="datatable_header">
						<th>ID</th>
						<th style="width: 10%;" >№</th>
						<th style="width:  70%;">დასახელება</th>
						<th style="width:  15%">ქმედება</th>
					</tr>
				</thead>
				<thead>
					<tr class="search_header">
						<th class="colum_hidden">
							<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
						</th>
						<th>
							<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
						<th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
						</th>
						<th>
        
						</th>
					</tr>
				</thead>
			</table>
		<div>';
    
}

/*================================================================= NEW FUNCTIONS ============================================================*/

// easy mailing page structure
function easyMailingPage()
{

    return '<table class="display" id="easy_mailing_table" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 100%;">თარიღი</th>
                        <th style="width: 100%;">ადრესატი</th>
                        <th style="width: 100%;">ტექსტი</th>
                        <th style="width: 100%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>';

}

// easy mailing page structure

// complicate mailing page structure
function complicateMailingPage()
{

    return '<table class="display" id="complicated_mailing_table" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 100%;">თარიღი</th>
                        <th style="width: 100%;">ადრესატი</th>
                        <th style="width: 100%;">ტექსტი</th>
                        <th style="width: 100%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>';

}

// complicate mailing page structure
function web_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `site_chat_account`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        if($value[id] == $id){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}
// easy sms sending dialog structure
function easySmsSendPage()
{
    $id = '';
    return '<fieldset class="new-sms-send-container">

                <!-- row unit -->
                <div class="new-sms-row">
	                <button class="jquery-ui-button" id="smsTemplate">
	                    <span>შაბლონი</span>
                    </button>
	                <button style="display:none" class="jquery-ui-button" id="smsPhoneDir">
	                    <span>ცნობარი</span>
	                </button>
                </div>
	            
	            <!-- row unit -->
	            <div class="new-sms-row">
	                <label for="smsAddressee">User Id</label>
	                <div style="width: 80%;" class="new-sms-input-holder">
	                    <input type="text" id="smsAddressee">
                        <button style="margin-left: 10px;" class="jquery-ui-button" id="search_user_info">
    	                    <span>შემოწმება</span>
                        </button>
                    </div>
                </div>
                
                <div class="new-sms-row">
	                <label for="smsAddressee">სახელი,გვარი</label>
	                <div style="width: 80%;" class="new-sms-input-holder">
	                    <input type="text" id="user_name_surname" disabled="disabled">
                    </div>
                </div>

                <div class="new-sms-row">
	                <label for="smsAddressee">საიტი</label>
	                <div style="width: 100%;" class="new-sms-input-holder">
	                    <select style="height: 18px; width: 100%;" id="my_site" class="idls object" multiple>'.web_site($id).'</select>
                    </div>
                </div>

                <!-- row unit -->
                <div style="height:170px;" class="new-sms-row">
	                <label for="smsText">ტექსტი</label>
	                <div class="new-sms-input-holder">
	                    <textarea style="resize:none; height:143px;" id="newSmsText" maxlength="150"></textarea>
                    </div>
                </div>
                
                <!-- row unit -->
                <div " class="new-sms-row">
	                <button class="jquery-ui-button" id="sendNewSms">
	                    <span>გაგზავნა</span>
                    </button>
                </div>
	          <input id="site_sms_id" type="text" value="" style="display: none;">  
            </fieldset>';

}

// easy sms sending dialog structure


// complicate sms sending dialog structure
function complicatedSmsSendPage(){
    global $db;
    $db->setQuery("UPDATE site_message_increment SET increment = increment+1 WHERE id = 1");
    $db->execQuery();
    $db->setQuery("SELECT increment FROM site_message_increment");
    $res = $db->getResultArray();
    return '<fieldset class="new-sms-send-container">

                <!-- row unit -->
                <div id="dt_example" class="inner-table" style="width: 100%;">
			         <div id="container" style="width: 100%; margin-top: 22px;   height: auto;">        	
			            <div id="dynamic">
			            	<h2 align="center"></h2>
			            	<table class="display" id="example_2">
			                    <thead >
			                        <tr style="height: 20px;" id="datatable_header">
                                        <th>ID</th>
			                            <th style="width: 15%;">#</th>
										<th style="width: 15%;">User Id</th>
                                        <th style="width: 50%;">მესიჯი</th>
										<th style="width: 20%;">სტატუსი</th>
			                        </tr>
			                    </thead>
			                    <thead>
			                        <tr class="search_header">
			                            <th class="colum_hidden">
			                            <th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 50px; height: 15px;  padding: 0;   margin:  0 auto;"/>
			                            </th>
			                          	<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
                                        <th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
										<th>
			                                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="height: 15px;  padding: 0;  margin:  0 auto;"/>
			                            </th>
			                        </tr>
			                    </thead>
			                </table>
			            </div>
			       </div>
		           <table class="dialog-form-table" style="width: 100%; table-layout: fixed;">
						<tr>
							
			   				<td>
								<button style="width: 87px; font-family: pvn; font-size: 12px; float: left;" id="send_sms_hard" class="center jquery-ui-button">გაგზავნა</button>
							</td>
			   				<td>
								
							</td>
							<td style="width: 150px;">
								<input id="hard_choose_file" type="file" name="choose_file" class="input" value="" style="display: none; "><button style="  width: 136px; font-family: pvn; font-size: 12px; float: right;" id="hard_choose_button" class="jquery-ui-button" value="">აირჩიეთ ფაილი</button>	
							</td>
							
							<td style="width: 145px;">
								<button style="width: 138px; font-family: pvn; font-size: 12px; float: right;" id="hard_excel_template" class="center jquery-ui-button">ექსელის შაბლონი</button>
							</td>
						</tr>	
					</table>
		    </div>
	        <input id="hidde_message_event_id" type="text" value="'. $res[result][0]['increment'] .'" style="display: none;">  
            </fieldset>';

}

// complicate sms sending dialog structure


// sms sending shablon
// function getShablon()
// {

//     $req = mysql_query("SELECT 	sms.id,
// 								sms.`name`,
// 								sms.`message`
// 							    FROM 	sms
// 							    WHERE 	sms.actived=1 ");

//     $data = '<table id="box-table-b1">
// 				<tr class="odd">
// 					<th style="width: 26px;">#</th>
// 					<th style="width: 160px;">ტექსტი</th>
// 					<th>ქმედება</th>
// 				</tr> ';

//     while ($res3 = mysql_fetch_assoc($req)) {

//         $data .= '<tr class="odd">
// 					<td>' . $res3[id] . '</td>
// 					<td style="width: 30px !important;">' . $res3['name'] . '</td>
// 					<td style="font-size: 10px !important;">
// 					    <button style="width: 45px;" class="download_shablon" sms_id="' . $res3['id'] . '" data-message="' . $res3['message'] . '">არჩევა</button>
//                     </td>
// 				 </tr>';

//     }
//     $data .= '</table>';
//     return $data;
// }

// // sms sending shablon


// //returns structure of phone directory list
// function phoneDirList()
// {

//     return '<table class="display" id="phoneDirectoryList" style="width: 100%;">
//                 <thead>
//                     <tr id="datatable_header">
//                         <th>ID</th>
//                         <th style="width: 38%;">სახელი</th>
//                         <th style="width: 37%;">თანამდებობა</th>
//                         <th style="width: 23%;">ტელ. ნომერი</th>
//                         <th style="width: 3%;">#</th>
//                     </tr>
//                 </thead>
//                 <thead>
//                     <tr class="search_header">
//                         <th class="colum_hidden">
//                            <input type="text" name="search_id" value="ფილტრი" class="search_init" />
//                         </th>             
//                         <th>
//                             <input type="text" name="search_category" value="ფილტრი" class="search_init" />
//                         </th>            
//                         <th>
//                             <input type="text" name="search_category" value="ფილტრი" class="search_init" />
//                         </th>
//                         <th>
//                             <input type="text" name="search_category" value="ფილტრი" class="search_init" />
//                         </th>
//                         <th>
//                             <div class="callapp_checkbox">
//                               <input type="checkbox" id="pdl_check_all" name="pdl_check_all" />
//                               <label for="pdl_check_all"></label>
//                             </div>
//                         </th>   
//                     </tr>
//                 </thead>
//             </table>';

// }

// //returns structure of phone directory list


// //returns the form of adding new number in phone directory
// function newNumberInDirectoryForm()
// {

//     return '
//         <div id="dialog-form">
//             <fieldset>
//                 <legend>ძირითადი ინფორმაცია</legend>
    
//                 <table class="dialog-form-table">
//                     <tr>
//                         <td style="width: 170px;"><label for="CallType">სახელი</label></td>
//                         <td>
//                             <input type="text" id="name" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="" />
//                         </td>
//                     </tr>
//                     <tr>
//                         <td style="width: 170px;"><label for="CallType">თანამდებობა</label></td>
//                         <td>
//                             <select name="position" id="position" data-select="chosen">
//                                 ' . getPositionList() . '
//                             </select>
//                         </td>
//                     </tr>
//                     <tr>
//                         <td style="width: 170px;"><label for="CallType">ტელ. ნომერი</label></td>
//                         <td>
//                             <input type="text" id="telNumber" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="" />
//                         </td>
//                     </tr>
    
//                 </table>
                
//             </fieldset>
//         </div>
//     ';

// }

// //returns the form of adding new number in phone directory


// //get position list
// function getPositionList()
// {

//     $query = mysql_query("	SELECT  `id`,
// 											`person_position`
//     								FROM    `position`");

//     $options = "";

//     while ($row = mysql_fetch_assoc($query)) {

//         $options .= "<option value='" . $row['id'] . "'>" . $row['person_position'] . "</option>";
//     }

//     return $options;

// }

// //get position list

// //check for phone number in phone directory
// function phone_num_exists($phone_number)
// {

//     //define result variable
//     $result = false;

//     //query from database
//     $q = mysql_query("SELECT phone_number
//                             FROM persons_phone
//                             WHERE phone_number = '$phone_number'");

//     //check for existing data length
//     $res_count = mysql_num_rows($q);

//     if ($res_count > 0) {
//         $result = true;
//     }

//     //return result
//     return $result;

// }

// //save new phone number in directory
// function save_phone_in_dir($_name, $_position, $_number)
// {

//     //define variables
//     $user_id = $_SESSION['USERID'];
//     $name = $_name;
//     $position = $_position;
//     $number = $_number;
//     $query = "INSERT INTO persons_phone (user_id, datetime, name, position_id, phone_number, actived)
//               VALUES ('" . $user_id . "', NOW(), '" . $name . "', '" . $position . "', '" . $number . "', '1')";

//     //query to database
//     if (mysql_query($query)) {
//         $result = true;
//     } else {
//         $result = false;
//     }

//     return $result;

// }

?>

