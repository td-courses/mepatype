<?php
include('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$action	= $_REQUEST['act'];
$error	= '';
$data	= '';

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$comment_id	 = $_REQUEST['id'];
		$page		 = GetPage(Getcomment($comment_id));
		$data		 = array('page'	=> $page);

		break;
	case 'get_list' :
	    $columnCount = $_REQUEST['count'];
	    $cols[]      = $_REQUEST['cols'];
			
		$db->setQuery("SELECT 	monitoring_question.id,
								monitoring_question.`question`,
                                level
					    FROM 	monitoring_question
					    WHERE 	monitoring_question.actived=1");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;

		break;
	case 'get_columns':
	    
	    $columnCount   = $_REQUEST['count'];
	    $cols[]        = $_REQUEST['cols'];
	    $columnNames[] = $_REQUEST['names'];
	    $operators[]   = $_REQUEST['operators'];
	    $selectors[]   = $_REQUEST['selectors'];
	    //$query = "SHOW COLUMNS FROM $tableName";
	    //$db->setQuery($query,$tableName);
	    //$res = $db->getResultArray();
	    $f=0;
	    foreach($cols[0] AS $col)
	    {
	        $column = explode(':',$col);
	        
	        $res[$f]['Field'] = $column[0];
	        $res[$f]['type'] = $column[1];
	        $f++;
	    }
	    $i = 0;
	    $columns = array();
	    $types = array();
	    foreach($res AS $item)
	    {
	        $columns[$i] = $item['Field'];
	        $types[$i] = $item['type'];
	        $i++;
	    }
	    
	    
	    $dat = array();
	    $a = 0;
	    for($j = 0;$j<$columnCount;$j++)
	    {
	        if(1==2)
	        {
	            continue;
	        }
	        else{
	            
	            if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
	            $op = false;
	            if($columns[$j] == 'id')
	            {
	                $width = "5%";
	            }
	            else if($columns[$j] == 'position'){
	                $width = "12%";
	            }
	            else{
	                $width = 'auto';
	            }
	            if($columns[$j] == 'inc_id')
	            {
	                $hidden = true;
	            }
	            else if($columns[$j] == 'docs_project_id')
	            {
	                $hidden = true;
	            }
	            else if($columns[$j] == 'id')
	            {
	                $hidden = true;
	            }
	            else{
	                $hidden = false;
	            }
	            if($res['data_type'][$j] == 'date')
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
	            }
	            else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
	            }
	            else
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
	            }
	            $a++;
	        }
	        array_push($dat,$g);
	        
	    }
	    
	    //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
	    
	    $new_data = array();
	    //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
	    for($j=0;$j<$columnCount;$j++)
	    {
	        if($types[$j] == 'date')
	        {
	            $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
	        }
	        else if($types[$j] == 'number'){
	            
	            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
	        }
	        else
	        {
	            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
	        }
	    }
	    
	    $filtArr = array('fields'=>$new_data);
	    
	    
	    
	    $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
	    
	    
	    //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
	    
	    $data = $kendoData;
	    //$data = '[{"gg":"sd","ads":"213123"}]';
	    
	    break;
	case 'save_priority':
		$comment_id = $_REQUEST['id'];
		$comment    = $_REQUEST['comment'];
		$level      = $_REQUEST['level'];
		
		if ($comment_id == '') {
		    Addcomment( $comment_id, $comment, $level);
		}else {
		    Savecomment($comment_id, $comment, $level);
		}				
			
		
		break;
	case 'disable':
		$comment_id	= $_REQUEST['id'];
		Disablecomment($comment_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addcomment($comment_id, $comment, $level){
    global $db;
    
    $db->setQuery("SELECT SUM(level) AS `level` FROM `monitoring_question` WHERE actived = 1");
    
    $res         = $db->getResultArray();
    $checl_level = $res[result][0];
    
    $sum_level   = $level +$checl_level[level];
	$user_id	 = $_SESSION['USERID'];
	
	if ($sum_level > 100) {
	    global $error;
	    $error = 'ქულების ჯამმა გადააჭარბა მაქსიმალურ ჯამს(მაქსიმალური ჯამი 100)';
	}else{
    	$db->setQuery("INSERT INTO  `monitoring_question`
    							  (`user_id`,`question`, `level`)
    					VALUES 	  
    							  ('$user_id','$comment', '$level')");
    	$db->execQuery();
	}

}

function Savecomment($comment_id, $comment, $level){
    
    global $db;
    
    $db->setQuery("SELECT SUM(level) AS `level` FROM `monitoring_question` WHERE actived = 1 AND id != '$comment_id'");
    
    $res         = $db->getResultArray();
    $checl_level = $res[result][0];
    
    $sum_level   = $level +$checl_level[level];
    $user_id	 = $_SESSION['USERID'];
	
	if ($sum_level > 100) {
	    global $error;
	    $error = 'ქულების ჯამმა გადააჭარბა მაქსიმალურ ჯამს(მაქსიმალური ჯამი 100)';
	}else{
    	$db->setQuery("	UPDATE `monitoring_question`
    					   SET `user_id`  = '$user_id',
    						   `question` = '$comment',
                               `level`    = '$level'
    					WHERE  `id`       = '$comment_id'");
    	$db->execQuery();
	}
}


function Disablecomment($comment_id){
    global $db;
    $db->setQuery("	UPDATE `monitoring_question`
                       SET `actived` = 0
                    WHERE  `id`      = $comment_id");
    $db->execQuery();
    
}

function CheckcommentExist($comment){
    global $db;
	$db->setQuery("	SELECT `id`
					FROM   `question`
					WHERE  `monitoring_question` = '$comment' && `actived` = 1");
	
	$req = $db->getResultArray();
	$res = $req[result][0];
	
	if($res['id'] != ''){
		return true;
	}
	return false;
}


function Getcomment($comment_id){
    global $db;
	$db->setQuery("	SELECT  `id`,
                            `level`,
							`question`
					FROM    `monitoring_question`
					WHERE   `id` = $comment_id");

	$req = $db->getResultArray();
	
	$res = $req[result][0];
	return $res;
}

function GetPage($res = ''){
    
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>
            <table class="dialog-form-table">
                <tr>
					<td><label for="CallType">ქულა</label></td>
				</tr>
				<tr>
					<td>
						<input style="width: 100px;" min="0" type="number" id="level" class="idle" value="'.$res[level].'" />
					</td>
				</tr>
				<tr>
					<td><label for="CallType">კითხვა</label></td>
				</tr>
				<tr>
					<td>
						<div id="content">	
						<textarea  style="width: 550px; resize: vertical;" id="comment" class="idle" name="call_content" cols="300" rows="8">' . $res['question'] . '</textarea>
						</div>		
					</td>
				</tr>
            </table>
			<!-- ID -->
			<input type="hidden" id="comment_id" value="' . $res['id'] . '" />
        </fieldset>
	

    </div>
					
    ';
	return $data;
}

?>
