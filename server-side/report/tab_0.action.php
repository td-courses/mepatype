<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$start = $_REQUEST['start'].' 00:00:00';
$end   = $_REQUEST['end'].' 23:59:59';
$agent = $_REQUEST['agent'];
$queuet = $_REQUEST['queuet'];
$source = $_REQUEST['source'];

if ($source == 4) {
    $db->setQuery(" SELECT   COUNT(*) AS count,
                            'ნაპასუხები' AS `cause`
                    FROM     chat
                    JOIN     incomming_call ON incomming_call.chat_id = chat.id
                    WHERE    chat.`status` IN(1,2,12,13)
                    AND      chat.join_date BETWEEN '$start' AND '$end'
                    AND      chat.last_user_id IN($queuet)
                    UNION ALL
                    SELECT 0 AS count,
                          'უპასუხო' AS `cause`");
}elseif ($source == 7){
    $db->setQuery(" SELECT   COUNT(*) AS count,
                            'ნაპასუხები' AS `cause`
                    FROM     mail
                    JOIN     incomming_call ON incomming_call.mail_chat_id = mail.id
                    WHERE    mail.`mail_status_id` IN(1,2,3)
                    AND      mail.send_datetime BETWEEN '$start' AND '$end'
                    AND      mail.user_id IN($queuet)
                    UNION ALL
                    SELECT 0 AS count,
                          'უპასუხო' AS `cause`");
}elseif ($source == 6){
    $db->setQuery(" SELECT   COUNT(*) AS count,
                            'ნაპასუხები' AS `cause`
                    FROM     fb_chat
                    JOIN     incomming_call ON incomming_call.fb_chat_id = fb_chat.id
                    WHERE    fb_chat.`status` IN(1,2,3)
                    AND      fb_chat.first_datetime BETWEEN '$start' AND '$end'
                    AND      fb_chat.last_user_id IN($queuet)
                    UNION ALL
                    SELECT 0 AS count,
                          'უპასუხო' AS `cause`");
}elseif ($source == 14){
    $db->setQuery(" SELECT   COUNT(*) AS count,
                            'ნაპასუხები' AS `cause`
                    FROM     viber_chat
                    JOIN     incomming_call ON incomming_call.viber_chat_id = viber_chat.id
                    WHERE    viber_chat.`status` IN(1,2,3)
                    AND      viber_chat.first_datetime BETWEEN '$start' AND '$end'
                    AND      viber_chat.last_user_id IN($queuet)
                    UNION ALL
                    SELECT 0 AS count,
                    'უპასუხო' AS `cause`");
}else{
    $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS count,
                           'ნაპასუხები' AS `cause`
                    FROM   asterisk_call_log
                    JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                    JOIN   asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                    WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') 
                    AND    asterisk_queue.number IN ($queuet)
                    AND    asterisk_extension.number IN ($agent)
                    AND    asterisk_call_log.call_type_id = 1
                    AND    asterisk_call_log.call_status_id IN(6,7)
                    UNION ALL
                    SELECT COUNT(asterisk_call_log.id) AS count,
                           'უპასუხო' AS `cause`
                    FROM   asterisk_call_log
                    JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                    WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') 
                    AND    asterisk_queue.number IN ($queuet)
                    AND    asterisk_call_log.call_type_id = 1
                    AND    asterisk_call_log.call_status_id IN(9)
                    UNION ALL
                    SELECT 	0 AS `count`,
                    	   'გადართული' AS `cause`");
}

$result = $db->getResultArray();
$row = array();
$rows = array();
foreach($result[result] AS $r) {
    $row[0] = $r[cause].': '.$r[count];
    $row[1] = (float)$r[count];
	array_push($rows,$row);
}


echo json_encode($rows);

?>