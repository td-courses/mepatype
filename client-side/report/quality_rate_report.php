<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        .pages {
            display: flex;
            justify-content: space-between;
            margin-top: 50px;
            margin-bottom: 40px;
        }

        .sections {
            display: flex;
            justify-content: center;
            width: 100%;
            min-height: 15vh;
            height: auto;
        }

        .sections>div {
            height: auto;
            border: 0 !important;
        }

        .Page-buttons {
            background-color: #EEECEC;
            transform: skew(-18deg, 0deg);
            margin-right: 2%;
            letter-spacing: 1px;
        }

        .half {
            width: 48%;
            margin: 0 1%;
        }

        .chosen-container,
        .chosen-container-multi {
            z-index: 1000;
        }

        #container,
        #container2 {
            width: 500px;
            height: 400px;
            margin: 0px !important;
        }


        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

        .activePage {
            background: #fef400 !important;
        }

        #loading1 {
            z-index: 999;
            top: 45%;
            left: 45%;
            position: absolute;
            display: none;
            padding-top: 15px;
        }
    </style>

    <link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>


    <script type="text/javascript">
        var tablesColumns = setColumns();
        var aJaxURL = "server-side/report/quality_rate_report.action.php";
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var SelectedItem = "";
        var currentDate = new Date().getFullYear();

        $(document).ready(function() {
            var kendo = new kendoUI();
            GetDate("start_date");
            GetDate("end_date");
            $("#region_id").chosen();
            $("#page1").show();
            $("#page2").hide();
            $("#page3").hide();
            $("#page4").hide();
            $("#page5").hide();
            $("#page6").hide();

            $("#first").addClass("activePage"); // active 1st button 
            $(".first").show(); // show first page

            $("#region_id_chosen").hide();
            LoadKendoTable_incomming("kendo_incomming_table1", tablesColumns[1]);



        });


        function printcontent(cols, data) {
            var mywindow = window.open('', '', '');
            mywindow.document.write('<html><title>Print</title><style> table{margin:auto;} td{border:1px solid #333;text-align:center;} </style><body>');
            mywindow.document.write('<table>' + cols + data + '</table>');
            mywindow.document.write('</body></html>');
            mywindow.document.close();
            mywindow.print();

            return true;
        }
        $(document).on("click", "#communication_excel_export", function() {

            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_incomming_table1", "ხარისხის შეფასების რეპორტი", [0]);
                $("#loading1").hide();
            }, 200);
        });

        function setColumns() {
            // first element of array is null DONT CANGE IT
            var kendoTableData = [{
                    sql: null,
                    geo: null,
                    action: null
                },
                {
                    sql: [
                        "id:string",
                        "operator:string",
                        "all_cases:string",
                        "checked:string",
                        "hundred:string",
                        "hundredPerc:string",
                        "nintyfive:string",
                        "nintyfivePerc:string",
                        "ninty:string",
                        "nintyPerc:string",
                        "eightyFive:string",
                        "eightyFivePerc:string",
                        "eighty:string",
                        "eightyPerc:string",
                        "severtyFive:string",
                        "severtyFivePerc:string"
                    ],
                    geo: [
                        "id",
                        "ოპერატორი",
                        "სულ მომართვები",
                        "შემოწმებული",
                        "100",
                        "%",
                        "95",
                        "%",
                        "90",
                        "%",
                        "85",
                        "%",
                        "80",
                        "%",
                        "75",
                        "%"
                    ],
                    action: "get_list"
                }
            ];
            return kendoTableData;
        }


        function LoadKendoTable_incomming(gridname, cols) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = gridname;
            var actions = '<button style="float:right;" id="communication_excel_export">EXCEL რეპორტი</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 40;
            var columnsSQL = cols.sql;
            var columnGeoNames = cols.geo;
            // alert error if sql and georgian cols quantrity is not same
            if (columnGeoNames.length != columnsSQL.length) {
                alert("error -> LoadKendoTable_incomming() geocols and sqlcols length not equal");
                return;
            }
            var columnsCount = columnGeoNames.length;
            var showOperatorsByColumns = [];
            var selectors = [];
            for (let i = 0; i < columnsCount; i++) {
                showOperatorsByColumns.push(0);
                selectors.push(0);
            }
            var filtersCustomOperators =
                '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI $(selector).addClass(className); CONFIGS END
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, cols.action, itemPerPage, columnsCount,
                columnsSQL, gridName, actions, editType, columnGeoNames,
                filtersCustomOperators, showOperatorsByColumns, selectors, "&table=" + gridname +
                "&start_date=" + $('#start_date').val() + "&end_date=" + $('#end_date').val() + "&callsType=" + $("#callsType").val() + "&raion_id=" + $("#raion_id").val() + "&region_id=" + $("#region_id").val() + "&sofel_id=" + $("#sofel_id").val(),
                1, '', '', '', false, false, false);
        }


        // filter kendo table one by one
        $(document).on("click", "#loadtablenew", function() {
            LoadKendoTable_incomming("kendo_incomming_table1", tablesColumns[1]);
 		});
    </script>
</head>

<body>
    <div id="tabs">
        <!-- BUTTONS FOR PAGE SWITCH -->
        <div style="display: flex;padding: 20px 5px;">
        </div>

        <!-- FILTER -->
        <div class="callapp_filter_show">
            <div class="callapp_filter_body" myvar="1">
                <div style="width: 100%;">
                    <table>
                        <tr>
                            <td style="padding: 5px 0 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                            </td>
                            <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">-
                            </td>
                            <td style="padding: 5px 10px 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 84px;">
                            </td>
                            
                            <td>
                                <div class="k-content">
                                    <button id="loadtablenew">ფილტრი</button>
                                </div>
                            </td>


                            <!-- FILTERS -->
                            <td style="width: 200px;">
                                <select id="region_id" style="width: 185px;">
                                    <?php
                                    // $data = '<option value="0">აირჩიე რეგიონი</option>';
                                    // global $db;
                                    // $db->setQuery("  SELECT  id,
                                    //                     	   `name`
                                    //                     FROM   `regions` 
                                    //                     WHERE   parent_id = 0 AND actived = 1");

                                    // $res = $db->getResultArray();

                                    // foreach ($res[result] as $req) {
                                    //     $data .= '<option value="' . $req[id] . '">' . $req[name] . '</option>';
                                    // }
                                    //echo $data;
                                    ?>
                                </select>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <!-- KENDO TABLES' OUTPUTS -->
        <div class="pages first">
            <!-- PAGE 1 -->
            <div id="page1" class="sections">
                <div id="kendo_incomming_table1" name="kendo_incomming_table1" ></div>
            </div>

        </div>

    </div>
    <div id="loading1">
        <p><img src="media/images/loader.gif" /></p>
    </div>
</body>

</html>