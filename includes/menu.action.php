<?php

// IMPORT FILES
require_once('classes/class.Mysqli.php');

/**
 * @global db
**/

global $db;

$db = new dbClass();

$action 	= $_REQUEST['act'];
$error		= '';
$data		= array();

switch ($action){
    case 'check_dnd': 

        $userID = $_SESSION['USERID'];
        
        $db->setQuery(" SELECT
                                    `source`.`id`,
                                    `source`.`name`,
                                    `source`.`background_image_name`,
                                    ifNull(`users_source_status`.`status`, 0) AS `status`
                        FROM        `source`
                        LEFT JOIN   `users_source_status` ON `users_source_status`.`source_id` = `source`.`id` AND `users_source_status`.`user_id` = '$userID'
                        WHERE       `source`.`actived` = 1 AND `source`.`key` != '0'
                        ORDER BY `source`.`position`");
                        
        $sources = $db->getResultArray();
        $data = $sources['result'];
        
    break;
    case 'activity_status': 

        $user = $_SESSION['USERID'];
        $statusID = $_REQUEST['status_id'];
        $session_id = $_SESSION['SESID'];
        
        $db->setQuery(" UPDATE users
                        SET `work_activities_id` = $statusID
                        WHERE `id` = '$user'
                     ");

        $db->execQuery();

        $db->setQuery("UPDATE work_activities_log SET end_datetime=NOW() WHERE user_id='$user' AND ISNULL(end_datetime)");
        $db->execQuery();



        $db->setQuery("INSERT INTO  work_activities_log
                                    (`user_id`,`session_id`,`work_activities_id`,`start_datetime`) 
                            VALUES('$user','$session_id','$statusID',NOW())");
        $db->execQuery();
        
    break;
    case 'source_status':
        $user           = $_SESSION['USERID'];
        $sourceID       = $_REQUEST['source_id'];
        $source_status  = $_REQUEST['source_status'];

        $db->setQuery(" SELECT      `users_source_status`.*, `source`.`key`  
                        FROM        `users_source_status`
                        LEFT JOIN   `source` ON `source`.id = `users_source_status`.source_id
                        WHERE       `user_id` = '$user' AND `source_id` = '$sourceID'
        ");

        $rr = $db->getResultArray();
        $res = $db->getNumRow();

        if($res == 0){
            $db->setQuery(" INSERT 
                            INTO `users_source_status` 
                                    (`user_id`,`source_id`, `status`)
                            VALUES  
                                    ($user, $sourceID, $source_status)
            ");
        }else{
            $db->setQuery(" UPDATE  `users_source_status` 
                            SET     `status` = $source_status
                            WHERE   `user_id` = $user AND `source_id` = $sourceID
            ");
        }

        $db->execQuery();
        
        $key = $rr['result'][0]['key'];

        if($key == ''){
            $db->setQuery(" SELECT    `key`  
                            FROM      `source`
                            WHERE     `id` = '$sourceID'
            ");
            $res2 = $db->getResultArray();

            $key = $res2['result'][0]['key'];
        }else{
            $key = $rr['result'][0]['key'];
        }

        $data = array("key" => $key);

    break;
    case 'get_source_status':
        $user = $_SESSION['USERID'];

        $db->setQuery(" SELECT *
                        FROM `users_source_status`
                        WHERE   `user_id` = $user 
                        ");

        $data = $db->getResultArray();


    break;
    case 'save_micr':
	    $user	= $_SESSION['USERID'];
	    $status = $_REQUEST['activeStatus'];
	    $db->setQuery("SELECT * FROM `user_mute_check` WHERE user_id = $user");
	    $check = $db->getNumRow();
	    if($check == 0){
	        $db->setQuery("INSERT INTO user_mute_check SET user_id='$user', `muted`='$status' ");
	        $db->execQuery();
	    }else{
	        $db->setQuery("UPDATE `user_mute_check` SET `muted`='$status' WHERE `user_id`='$user';");
	        $db->execQuery();
	    }
	    
	    break;
    default: 
        $error = 'Action is null';
        $data['error'] = $error;
}



echo json_encode($data);