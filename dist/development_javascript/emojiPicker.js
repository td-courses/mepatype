
const buttons = document.querySelectorAll('.emojiBtn');
if (buttons) {
  window.addEventListener('DOMContentLoaded', () => {
    const picker = new EmojiButton();

    picker.on('emoji', emoji => {
      document.querySelectorAll('.emoji_input').forEach(input =>{
        input.value += emoji;
      })
    });

    buttons.forEach(button => {
      button.addEventListener('click', () => {
        picker.pickerVisible ? picker.hidePicker() : picker.showPicker(button);
      });
    })



  });

}




