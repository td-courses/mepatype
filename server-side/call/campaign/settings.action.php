<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../../../includes/classes/class.Mysqli.php');

global $db;


$db = new dbClass();
$action	= $_REQUEST['act'];

$error	= '';
$data	= '';

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		// $source_id		= $_REQUEST['id'];
		// $page		= GetPage(Getsource($source_id));
        // $data		= array('page'	=> $page);

        $per_id		= $_REQUEST['id'];
		$page		= GetPage();

        $data		= array('page'	=> $page);

		break;
	case 'get_list' :
        $count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		
        $db->setQuery("SELECT id,
                              id,
                              `datetime`,
                              `name`,
                              `company_form_id`,
                              `category_id`,
                              `type`,
                              contact_count,
                              contact_source_id,
                                (CASE
                                    WHEN progress >= 0 AND progress < 80 THEN CONCAT('<p class=progress_grey>',progress,'%</p>')
                                    WHEN progress >= 80 AND progress <= 99 THEN CONCAT('<p class=progress_yellow>',progress,'%</p>')
                                    ELSE CONCAT('<p class=progress_green>',progress,'%</p>')
                                END),
                                (CASE
                                    WHEN `status` = 1 AND progress < 100 THEN '<p class=status_pink><img src=\"media/images/icons/play.png\"></p>'
                                    WHEN `status` = 2 AND progress < 100 THEN '<p class=status_lightgreen><img src=\"media/images/icons/pause.png\"></p>'
                                    WHEN `status` = 3 AND progress < 100 THEN CONCAT('<p class=status_red><img src=\"media/images/icons/stop.png\"></p>')
                                END)
                        FROM companies_test_table");
		
        $data = $db->getList($count, $hidden, 1);


		break;
	case 'save_source':
		$source_id 		= $_REQUEST['id'];
		$source_name    = $_REQUEST['name'];
		$content    	= $_REQUEST['content'];

		if($source_name != ''){
			
			if ($source_id == '') {
					if(!ChecksourceExist($source_name, $source_id)){
						Addsource($source_name,$content);
					} else {
						$error = '"' . $source_name . '" უკვე არის სიაში!';
					
					}
			}else {
				Savesource($source_id, $source_name,$content);
			}

		}

		break;
	case 'disable':
		$source_id	= $_REQUEST['id'];
		Disablesource($source_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


// ----------------------------------------------------------------------------------------
// --- FUNCTIONS
// ----------------------------------------------------------------------------------------


function Addsource($source_name, $content)
{	global $db;
   
}

function Savesource($source_id, $source_name, $content)
{
    global $db;
    
}

function Disablesource($source_id)
{
    global $db;
   
}

function Getsource($source_id){   
    global $db;
	// $db->setQuery("	SELECT    sms_event.id,
	// 						  sms.`name`,
	// 						  sms_event.content
	// 				FROM      sms_event
	// 				WHERE     sms_event.actived=1 AND sms_event.id = $source_id");
	
    $res = $db->getResultArray();
    
	return $res['result'][0];
}

function GetPage($res = '')
{
	$data = '
	<div id="dialog-form">
        <section id="dialog__top_section" class="dialog__top_seciton">
            <table class="dialog__top_section_table">
            <tr>
                <td class="dialog__top_section_stats">
                <div class="dialog__top_section_chart" id="graph" data-percent="75"></div>
                </td>
                <td class="dialog__top_section_fields">
                    <table>
                      
                        <tr>
                            <td>
                                <label>შეიყვანეთ კომპანიის დასახელება ან აირჩიეთ <a class="permalink">შაბლონიდან</a></label>
                                <input type="text" />
                            </td>
                            <td>
                                <label>კომპანიის კატეგორია</label>
                                <select>
                                    <option></option>
                                </select>
                            </td>
                            <td>
                                <label>კომუნიკაციის არხი</label>
                                <select>
                                    <option></option>
                                </select>
                            </td>
                            <td>
                                <label>დაკავშირების ტიპი</label>
                                <select>
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>დასაწყისი</label>
                                <input type="text" />
                            </td>
                            <td>
                                <label>დასასრული</label>
                                <input type="text" />
                            </td>
                            <td colspan="2">
                                <label>დასრულებამდე დარჩა</label>
                                <input type="text" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        </section>
        <section id="dialog__second_section" class="dialog__second_section">
            <table class="dialog__second_section_table">
                <tr>
                    <td class="dialog__second_section_tablebar">
                        <div class="dialog__second_section_item" id="second_operators" title="ოპერატორები" aria-selected="true"></div>
                        <div class="dialog__second_section_item" id="second_settings" title="პარამეტრები"></div>
                        <div class="dialog__second_section_item" id="second_calendar" title="კალენდარი"></div>
                        <div class="dialog__second_section_item" id="second_mic" title=""></div>
                        <div class="dialog__second_section_item" id="second_upload" title="ატვირთვა"></div>
                        <div class="dialog__second_section_item" id="second_edit" title="რედაქტირება"></div>
                    </td>
                    <td class="dialog__second_section_tablecontent">

                        <fieldset class="dialog__second_section_content" id="second_operators" aria-selected="true">1</fieldset>

                        <fieldset class="dialog__second_section_content" id="second_settings">
                        
                            <table class="dialog__second_section_content_table">
                                <tr>
                                    <td label>პირველი ზარი შევიდეს <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <div class="radio_inline">
                                            <div class="custom_radio">
                                                <input type="radio" id="operator" name="setting" value="operator">
                                                <span class="checkmark"></span>
                                                <label for="operator">ოპერატორი</label>
                                            </div>
                                            <div class="custom_radio">
                                                <input type="radio" id="client" name="setting" value="client">   
                                                <span class="checkmark"></span> 
                                                <label for="client">აბონენტთან</label>
                                                </div>
                                            </div>
                                        <div class="input__withlabel">
                                            <label>ატვირთეთ მისასალმებელი ხმოვანი ფაილი <span help-icon title="გამარჯობა მეგობარო" /></label>
                                            <input type="text" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td label>ლოდინის ხანგრძლივობა პასუხამდე <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" style="width: 60px" /> წმ
                                    </td>
                                </tr>
                                <tr>
                                    <td label>მცდელობების რ-ბა <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" style="width: 60px" /> წმ
                                    </td>
                                </tr>
                                <tr>
                                    <td label>მცდელობებს შორის შუალედი <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" style="width: 60px" /> წმ
                                    </td>
                                </tr>
                                <tr>
                                    <td label>ზარების განხორციელების წესი <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                       <select>
                                        <option>1</option>
                                        <option>2</option>
                                       </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td label>სტატუსი - „მოისმინა“ <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" style="width: 60px" /> წმ
                                </td>
                                </tr>
                            </table>

                        </fieldset>
                            
                        <fieldset class="dialog__second_section_content" id="second_calendar" >3</fieldset>

                        <fieldset class="dialog__second_section_content" id="second_mic">4</fieldset>

                        <fieldset class="dialog__second_section_content" id="second_upload">5</fieldset>

                        <fieldset class="dialog__second_section_content" id="second_edit">6</fieldset>
                    </td>
                </tr>
            </table>
        </section>

        <section id="dialog__comment_section" class="dialog__comment_section">
        <label>კომენტარი</label>
            <textarea></textarea>
        </section>
    </div>		

    ';
	return $data;
}

?>

