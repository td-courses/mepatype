<html>
<head>
<script type="text/javascript">
	var aJaxURL	= "server-side/view/chat_setting.action.php";		//server side folder url
	var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
	    	
	$(document).ready(function () {        	
		loadContent();
		$("#saveSetting").button();
		GetButtons("add_button","delete_button");
		GetTimes("start_work");
		GetTimes("end_work");
		LoadTable();
			
			SetEvents("add_button", "delete_button", "check-all", 'table_news_setting', 'edit-news', aJaxURL);
		$.ajax({
			url: aJaxURL,
			data: "act=get_allowed_groups",
			success: function(data) {			        
				if(typeof(data.error) != 'undefined'){
					if(data.error != ''){
						alert(data.error);
					}else{
						var kendo = new kendoUI();
						kendo.kendoMultiSelector('chat_permission_group', 'server-side/view/chat_setting.action.php', 'get_groups', 'ჩატის ნახვის უფლების მქონე ჯგუფები',data.groups);
					}
				}
			}
		});
		
	
	});

	function LoadTable(){
		
		GetDataTable("table_news_setting", aJaxURL, "get_welcome_list", 4, "", 0, "", 1, "desc", "", change_colum_main);
		setTimeout(function(){
		    	$('.ColVis, .dataTable_buttons').css('display','none');
			}, 90);
	}
	
    function loadContent(){
    	$.ajax({
	        url: aJaxURL,
		    data: "act=get_setting",
	        success: function(data) {
	        	if(typeof(data.error) != 'undefined'){
	        		if(data.error != ''){
						alert(data.error);
					}else{
						$('#ip_count').val(data.ip_count);
						if(data.welcome_on_off == 1){
							$( "#welcome_on_off" ).prop( "checked", true );
						}else{
							$( "#welcome_on_off" ).prop( "checked", false );
						}
						$('#welcome_text').val(data.welcome_text);
						if(data.input_on_off == 1){
							$( "#input_on_off" ).prop( "checked", true );
						}else{
							$( "#input_on_off" ).prop( "checked", false );
						}
						if(data.input_name == 1){
							$( "#input_name" ).prop( "checked", true );
						}else{
							$( "#input_name" ).prop( "checked", false );
						}
						if(data.input_mail == 1){
							$( "#input_mail" ).prop( "checked", true );
						}else{
							$( "#input_mail" ).prop( "checked", false );
						}
						if(data.input_phone == 1){
							$( "#input_phone" ).prop( "checked", true );
						}else{
							$( "#input_phone" ).prop( "checked", false );
						}
						if(data.sound_on_off == 1){
							$( "#sound_on_off" ).prop( "checked", true );
						}else{
							$( "#sound_on_off" ).prop( "checked", false );
						}
						if(data.send_file == 1){
							$( "#send_file" ).prop( "checked", true );
						}else{
							$( "#send_file" ).prop( "checked", false );
						}
						if(data.send_mail == 1){
							$( "#send_mail" ).prop( "checked", true );
						}else{
							$( "#send_mail" ).prop( "checked", false );
						}
						if(data.send_sound == 1){
							$( "#send_sound" ).prop( "checked", true );
						}else{
							$( "#send_sound" ).prop( "checked", false );
						}
						if(data.send_close == 1){
							$( "#send_close" ).prop( "checked", true );
						}else{
							$( "#send_close" ).prop( "checked", false );
						}
						
						$('#color_id').val(data.color_id);
						$('#text_color').val(data.text_color);
						$('#taimer').val(data.taimer);
						$('#close_time').val(data.close_time);
						$('#news_time').val(data.news_time);
						$('#start_work').val(data.start_work);
						$('#end_work').val(data.end_work);
					}
		    	}
	        }
	    });
    }

    function saveSetting(){
    	ip_count = $('#ip_count').val();
		if($("#welcome_on_off").is(':checked')){
			welcome_on_off = 1;
		}else{
			welcome_on_off = 0;
		}
		welcome_text = $('#welcome_text').val();
		if($("#input_on_off").is(':checked')){
			input_on_off = 1
		}else{
			input_on_off = 0;
		}
		if($("#input_name").is(':checked')){
			input_name = 1;
		}else{
			input_name = 0;
		}
		if($("#input_mail").is(':checked')){
			input_mail = 1;
		}else{
			input_mail = 0;
		}
		if($("#input_phone").is(':checked')){
			input_phone = 1;
		}else{
			input_phone = 0;
		}
		if($("#sound_on_off").is(':checked')){
			sound_on_off = 1;
		}else{
			sound_on_off = 0;
		}

		if($("#send_file").is(':checked')){
			send_file = 1;
		}else{
			send_file = 0;
		}
		if($("#send_mail").is(':checked')){
			send_mail = 1;
		}else{
			send_mail = 0;
		}
		if($("#send_sound").is(':checked')){
			send_sound = 1;
		}else{
			send_sound = 0;
		}
		if($("#send_close").is(':checked')){
			send_close = 1;
		}else{
			send_close = 0;
		}
		/* +"&send_file="+send_file+"&send_mail="+send_mail+"&send_sound="+send_sound+"&send_close="+send_close+ */



		var groups_perms = [];
		$('#chat_permission_group option:selected').toArray().map(c => groups_perms.push(c.value));
    	$.ajax({
	        url: aJaxURL,
		    data: "act=save_setting&ip_count="+ip_count+"&welcome_on_off="+welcome_on_off+"&welcome_text="+welcome_text+"&input_on_off="+input_on_off+"&input_name="+input_name+"&input_mail="+input_mail+"&input_phone="+input_phone+"&sound_on_off="+sound_on_off+"&color_id="+$('#color_id').val()+"&text_color="+$('#text_color').val()+"&send_file="+send_file+"&send_mail="+send_mail+"&send_sound="+send_sound+"&send_close="+send_close+'&taimer='+$('#taimer').val()+'&close_time='+$('#close_time').val()+'&news_time='+$("#news_time").val()+'&start_work='+$("#start_work").val()+'&end_work='+$("#end_work").val()+'&groups_perms='+groups_perms,
	        success: function(data) {
	        	if(typeof(data.error) != 'undefined'){
	        		if(data.error != ''){
						alert(data.error);
					}else{
						alert("მონაცემები წარმატებით განახლდა ბაზაში");
						loadContent();
					}
		    	}
	        }
	    });
	}

	function LoadDialog(dialog){

		switch(dialog){
			case "edit-translation":
				var buttons = {
                    "save_translation": {
                        text: "შენახვა",
                        id: "save_translation",
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "translation_cancel",
                        click: function () {
                            $(this).dialog('close');
                        }
                    }

                };
				GetDialog(dialog, 450, "auto", buttons);
				break;

			case "edit-news":
				var buttons_news = {
                    "save_news": {
                        text: "შენახვა",
                        id: "save_news",
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "news_cancel",
                        click: function () {
                            $(this).dialog('close');
                        }
                    }

                };
				GetDialog(dialog, 450, "auto", buttons_news);
				break;


			default : alert("Errror load dialog");
		}
						
		}

		
	
	// $(document).on("dblclick","#table_news_setting tbody tr", function(){
	// 	var nTds = $("td", this);
    //     var rID = $(nTds[0]).text();
	// 	$.ajax({
	// 		url:aJaxURL,
	// 		data:{
	// 			act:"get_dialog_news",
	// 			id:rID
	// 		},
	// 		success:function(data){
	// 			$("#edit-news").html(data.page);
	// 			LoadDialog("edit-news");
	// 		}

	// 	})
	// })

	$(document).on("dblclick","#table_translations tbody tr", function(){
		var nTds = $("td", this);
        var rID = $(nTds[0]).text();
		$.ajax({
			url:aJaxURL,
			data:{
				act:"get_dialog_translation",
				id:rID
			},
			success:function(data){
				$("#edit-translation").html(data.page);
				LoadDialog("edit-translation");
			}

		})
	})

	$(document).on("click","#save_news", function(){
		var obj = new Object();
		obj.act = "save_news";
		obj.text =  $("#welcome_text").val();
		obj.id = $("#wlc").val();

		$.ajax({
			url:aJaxURL,
			data:obj,
			success:function(data){
				GetDataTable("table_news_setting", aJaxURL, "get_welcome_list", 4, "", 0, "", 1, "desc", "", change_colum_main);
				CloseDialog("edit-news");
			}
		})
	})
	
	$(document).on("focus",".news_textarea",function(){
		$(".news_textarea").removeClass("focus_news_textarea");
		$(this).addClass("focus_news_textarea");
		var leng = $(this).val().length;
		$("#news_counter").html(`${leng}/500`);
	})

	$(document).on("focus",".translate_textarea",function(){
		$(".translate_textarea").removeClass("focus_translate_textarea");
		$(this).addClass("focus_translate_textarea");
	})

	$(document).on("keyup",".focus_news_textarea",function(){
		var leng = $(this).val().length;
		$("#news_counter").html(`${leng}/500`);
	})

    $(document).on("click", "#input_on_off", function () {
    	if ($(this).is(':checked')) {
    		$( "#input_mail" ).prop( "checked", true );
    		$( "#input_phone" ).prop( "checked", true );
    		$( "#input_name" ).prop( "checked", true );
    	}else{
    		$( "#input_mail" ).prop( "checked", false );
    		$( "#input_phone" ).prop( "checked", false );
    		$( "#input_name" ).prop( "checked", false );
    	}
	});
	
	$(document).on("click",".tab_button",function(){
		var id = $(this).attr('tab');
		$(".tab").removeClass("active");
		$(`#${id}`).addClass("active");
		$(".tab_button").removeClass("active_button");
		$(this).addClass("active_button");
	})

	$(document).on("click","#save_translation", function(){
		var obj = new Object();
		obj.act = "save_translation";
		obj.key = $("#key_hidden").val();
		obj.geo = $("#input_geo").val();
		obj.eng = $("#input_eng").val();
		obj.ru = $("#input_ru").val();
		$.ajax({
			url:aJaxURL,
			data:obj,
			success:function(data){
				GetDataTable("table_translations", aJaxURL, "get_list_translations", 5, "", 0, "", 1, "asc", "", change_colum_main);
				CloseDialog("edit-translation");
			}
		})
	})

	$(document).on("click", ".download15", function () {
			var welcome_id = $(this).attr('data-id');
			$.ajax({
				url: aJaxURL,
				data: "act=update_welcome&wlc_id="+welcome_id,
				success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							LoadTable();
							alert('მისალმების ტექსტი წარმატებით შეიცვალა!!')
						}
					}
				}
			});
			
		});

</script>
<style>
*:focus {outline:0;}
.work_times{
	display: flex;
    justify-content: space-between;
    width: 330px;
}

.ColVis, .dataTable_buttons{
	display:none!important;
}

.tab{
	display:none;
}

.active {
	display:block!important;
}


.focus_news_textarea{
	border-color:#009658;
	width: 421px!important;
    height: 213px!important;

}

.focus_translate_textarea{
	border-color:#009658;
	width: 421px!important;
    height: 213px!important;

}

fieldset
{
    padding: 13px 10px 10px 10px;
    border: solid 1px #CCC;
	background-color: #F1F1F1;
    
}
.tab_button{
	width: max-content;
	color:#009658;
	background-color:#FFF;
	border: 1px solid #009658;
    border-radius: 7px;
    transition:0.3;
    /* padding: 5px 10px; */
    cursor: pointer;
    margin-left: 5px;
}
.tab_button:hover{
	border: 1px solid transparent;
	color: #FFF;
	background-color: #009658;
	transition:0.3;
}

.active_button {
	border: 1px solid transparent;
	color: #FFF;
	background-color: #009658;
}

#tab_buttons{
	margin:20px;
}


</style>
</head>

<body>
    <div id="tabs" >
	<div class="l_responsible_fix">

		<div id="tab_buttons">
			<button class="tab_button active_button" tab="settings">ძირითადი პარამეტრები</button>
			<button class="tab_button" tab="news_setting">მისალმების ტექსტები</button>
		</div>
    <div class="tab active" id="settings">
	    <fieldset>
	    	<legend>ძირითადი პარამეტრები</legend>

	    	<!-- <table class="dialog-form-table">
				<tr>
					<td style="width: 250px;"><label for="ip_count" title="თუ ip რაოდენობა მითითებულია 0 მაშინ ულიმიტოა!">მაქსიმალური რაოდენობა ერთი IP იდან</label></td>
					<td>
						<input style="width: 50px;" type="number" min="0" max="99" id="ip_count" class="idle" value="0" title="თუ ip რაოდენობა მითითებულია 0 მაშინ  ულიმიტოა!" />
					</td>
				</tr>
				<tr>
					<td style="width: 250px;"><label for="welcome_on_off">მისალმების ტექსტის ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="welcome_on_off"  />
					</td>
					<td style="width: 150px;"><label for="welcome_text">მისალმების ტექსტი</label></td>
					<td style="width: 250px;"><textarea style="width: 455px; height: 80px;" id="welcome_text" rows="" cols=""></textarea></td>
				</tr>
			</table> -->
			<table class="dialog-form-table" style="margin:0;">
				<tr>
					<td style="width: 250px;"><label for="input_on_off">შესასვლელად საჭირო აუცილებელი ველების ჩართვა/გამორთვა</label></td>
					<td style="width: 60px;">
						<input type="checkbox" id="input_on_off"  />
					</td>
					<td style="width: 60px;"><label for="input_name">სახელი*</label></td>
					<td style="width: 60px;"><input type="checkbox" id="input_name"  /></td>
					<td style="width: 70px;"><label for="input_mail">ე-ლფოსტა*</label></td>
					<td style="width: 60px;"><input type="checkbox" id="input_mail"  /></td>
					<td style="width: 70px;"><label for="input_phone">ტელეფონი*</label></td>
					<td style="width: 60px;"><input type="checkbox" id="input_phone"  /></td>
				</tr>
			</table>
			<!-- <table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="sound_on_off">მესიჯების ხმის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="sound_on_off"  />
					</td>
				 </tr>
			</table> -->
			<table class="dialog-form-table" style="margin:0;">
			     <tr>
					<td style="width: 250px;"><label for="color_id">ჩატის ფერი</label></td>
					<td>
						<input type="color" id="color_id" value="#f5f5f5" />
					</td>
				 </tr>
			</table>
			<table class="dialog-form-table" style="margin:0;">
			     <tr>
					<td style="width: 250px;"><label for="text_color">ჩატის სათაურის ფერი</label></td>
					<td>
						<input type="color" id="text_color" value="#f5f5f5" />
					</td>
				 </tr>
			</table>
			
			<!-- <table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_file">ფაილის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_file"  />
					</td>
				 </tr>
			</table> -->
			<!-- <table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_mail">მეილის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_mail"  />
					</td>
				 </tr>
			</table> -->
			<!-- <table class="dialog-form-table">
			     <tr>
					<td style="width: 250px;"><label for="send_sound">ხმის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_sound"  />
					</td>
				 </tr>
			</table> -->
			<table class="dialog-form-table" style="margin:0;">
			     <tr>
					<td style="width: 250px;"><label for="send_close">გამორთვის ღილაკის  ჩართვა/გამორთვა</label></td>
					<td>
						<input type="checkbox" id="send_close"  />
					</td>
				 </tr>
			</table>
			
			<table class="dialog-form-table" style="margin:0;">
			     <!-- <tr>
					<td style="width: 250px;"><label for="taimer">შეტყობინება ალტერნატიული კონტაქტის შესახებ(წამი)</label></td>
					<td>
						<input style="width: 50px;" type="number" id="taimer" min="1"  value="1" />
					</td>
				 </tr> -->
				 <tr>
					<td style="width: 250px;"><label for="close_time">დასაშვები უმოქმედობის დრო (წუთი)</label></td>
					<td>
						<input style="width: 50px;" type="number" id="close_time" min="1"  value="1" />
					</td>
				 </tr>
				 <!-- <tr>
					<td style="width: 250px;"><label for="news_time">სიახლის განახლების დრო (წამი)</label></td>
					<td>
						<input style="width: 50px;" type="number" id="news_time" min="1"  value="1" />
					</td>
				 </tr> -->
				 <tr>
					<td style="width: 250px;"><label for="work_times">სამუშაო საათეები</label></td>
					<td>
						<div class="work_times">
							<div style="display: flex;"><input style="width: 130px;" type="text" id="start_work" /> - დან</div> <div style="display: flex;"><input style="width: 130px;" type="text" id="end_work" /> -მდე</div>
						</div>
					</td>
				 </tr>
				 <tr>
					<td>უფლებები</td>
					<td>
						<div class="k-content">
							<select id="chat_permission_group" style="width: 332px !important; font-size: 12px;"></select>
						</div>
					</td>
				</tr>
			</table>
			<button id="saveSetting" onclick="saveSetting()">განახლება</button>
        </fieldset>
    </div>

	<div class="tab" id="news_setting">
	<fieldset>
	<div style="margin-top: 15px; margin-bottom: 5px; display: flex;">
        <button id="add_button">დამატება</button>
        <button id="delete_button" style="margin-left:5px">წაშლა</button>
    </div>
		<table class="display" id="table_news_setting" >
		<thead>
			<tr id="datatable_header">
				<th>ID</th>
				<th style="width:75%">მისალმების ტექსტი</th>
				<th style="width:25%">ქმედება</th>
				<th class="check">#</th>
			</tr>
		</thead>
		<thead>
			<tr class="search_header">
				<th class="colum_hidden">
					<input type="text" name="search_id" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_name" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_position" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<div class="callapp_checkbox">
						<input type="checkbox" id="check-all" name="check-all" />
						<label for="check-all"></label>
					</div>
				</th>
				
			</tr>
		</thead>
	</table>
	
	</fieldset>
	</div>

</div>
	 <!-- jQuery Dialog -->
	 <div id="edit-translation" class="form-dialog" title="თარგმნა">
    	<!-- aJax -->
	</div>

	 <!-- jQuery Dialog -->
	 <div id="edit-news" class="form-dialog" title="სიახლეები">
    	<!-- aJax -->
	</div>

</body>
</html>