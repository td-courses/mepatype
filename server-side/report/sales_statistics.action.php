<?php

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

header('Content-Type: application/json');
$start_date      = $_REQUEST['start'].' 00:00';
$end_date        = $_REQUEST['end'].' 23:59';
$agent           = $_REQUEST['agent'];
$queue           = $_REQUEST['queuet'];
$source           = $_REQUEST['source'];

$quantity        = array();
$cause           = array();
$cause1          = array();
$name            = array();
$agentt          = array();
$call_count      = array();
$answer_count1   = array();
$datetime1       = array();
$unanswer_call2  = array();
$date1           = array();
$answer_count3   = array();
$count1          = array();
$queue1          = array();
$answer_count    = array();
$datetime 	     = array();
$count2          = array();
$queue2          = array();
$unanswer_call   = array();
$times		     = array();
$answer_count2   = array();
$datetime2	     = array();
$unanswer_count1 = array();
$times2  		 = array();
$series          = array();

//-----------------------კავშირის გაწყვეტის მიზეზი
if($_REQUEST['act']=='cause_fix'){
    $db->setQuery("SELECT   COUNT(*) AS `count`,
                            IF(asterisk_call_log.call_status_id = 6, 'ოპერატორმა გათიშა', 'აბონენტმა გათიშა') AS `cause`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_extension.number IN ($agent)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(6,7)
                   GROUP BY asterisk_call_log.call_status_id");
    
    $res = $db->getResultArray();
    
    foreach($res[result] AS $row){	
    	$quantity[] = (float)$row[count];
    	$cause[]	= $row[cause];
    }
}

//-----------------------ნაპასუხები ზარები ოპერატორების მიხედვით
if($_REQUEST['act']=='answer_call_operator'){
    
    if ($source == 4) {
        $db->setQuery(" SELECT    user_info.`name` AS `agent`,
                                  COUNT(*) AS `num`
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                          chat.id
                                FROM      chat
                                LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                                AND 			chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                JOIN      user_info ON chat.last_user_id = user_info.user_id
                                WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                GROUP BY  chat.id) AS `chat_dur`
                        JOIN    chat ON chat.id = chat_dur.id
                        JOIN    user_info ON user_info.user_id = chat.last_user_id
                        WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY chat.last_user_id");
    }elseif ($source == 7){
        $db->setQuery("SELECT   		user_info.`name` AS `agent`,
                                COUNT(*) AS `num`
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                         mail.id
                                FROM      mail
                                LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                                AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                                JOIN      user_info ON mail.user_id = user_info.user_id
                                WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                
                                GROUP BY  mail.id) AS `chat_dur`
                        JOIN      mail ON mail.id = chat_dur.id
                        JOIN      user_info ON user_info.user_id = mail.user_id
                        WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        GROUP BY mail.user_id");
    }elseif ($source == 6){
        $db->setQuery(" SELECT   user_info.`name` AS `agent`,
                                 COUNT(*) AS `num`
                                 FROM   (SELECT   SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                                  fb_chat.id
                                        FROM      fb_chat
                                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                        AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                        JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                        WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                        
                                        GROUP BY  fb_chat.id) AS `chat_dur`
                        JOIN     fb_chat ON fb_chat.id = chat_dur.id
                        JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                        WHERE    fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                        GROUP BY fb_chat.last_user_id");
    }elseif ($source == 14){
        $db->setQuery(" SELECT   user_info.`name` AS `agent`,
                                 COUNT(*) AS `num`
                        FROM   (SELECT   SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                          viber_chat.id
                                FROM      viber_chat
                                LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                                AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                
                                GROUP BY  viber_chat.id) AS `chat_dur`
                        JOIN     viber_chat ON viber_chat.id = chat_dur.id
                        JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                        WHERE    viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                        GROUP BY viber_chat.last_user_id");
    }else{
        $db->setQuery("SELECT   COUNT(*) AS `num`,
                                asterisk_extension.number AS `agent`
                       FROM     asterisk_call_log
                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                       JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                       AND      asterisk_queue.number IN ($queue)
                       AND      asterisk_extension.number IN ($agent)
                       AND      asterisk_call_log.call_type_id = 1
                       AND      asterisk_call_log.call_status_id IN(6,7)
                       GROUP BY asterisk_call_log.extension_id");
    }
    $ress = $db->getResultArray();
    foreach($ress[result] AS $row1){
        $call_count[] = (float)$row1[num];
    	$agentt[]	  = $row1[agent];
    }
}
//------------------------------ ნაპასუხები ზარები კვირის დღეების მიხედვით
if($_REQUEST['act']=='answer_call_week'){
    if ($source == 4) {
        $db->setQuery("SELECT   CASE
                                    WHEN DAYOFWEEK(chat.join_date) = 1 THEN 'კვირა'
                                    WHEN DAYOFWEEK(chat.join_date) = 2 THEN 'ორშაბათი'
                                    WHEN DAYOFWEEK(chat.join_date) = 3 THEN 'სამშაბათი'
                                    WHEN DAYOFWEEK(chat.join_date) = 4 THEN 'ოთხშაბათი'
                                    WHEN DAYOFWEEK(chat.join_date) = 5 THEN 'ხუთშაბათი'
                                    WHEN DAYOFWEEK(chat.join_date) = 6 THEN 'პარასკევი'
                                    WHEN DAYOFWEEK(chat.join_date) = 7 THEN 'შაბათი'
                                END AS `date`,
                                COUNT(*) AS `answer_count1`
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                        chat.id
                        FROM      chat
                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                        AND 			chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                        JOIN      user_info ON chat.last_user_id = user_info.user_id
                        WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY  chat.id) AS `chat_dur`
                        JOIN    chat ON chat.id = chat_dur.id
                        JOIN    user_info ON user_info.user_id = chat.last_user_id
                        WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY DAYOFWEEK(chat.join_date)");
    }elseif ($source == 7){
        $db->setQuery("SELECT   CASE
                                    WHEN DAYOFWEEK(mail.send_datetime) = 1 THEN 'კვირა'
                                    WHEN DAYOFWEEK(mail.send_datetime) = 2 THEN 'ორშაბათი'
                                    WHEN DAYOFWEEK(mail.send_datetime) = 3 THEN 'სამშაბათი'
                                    WHEN DAYOFWEEK(mail.send_datetime) = 4 THEN 'ოთხშაბათი'
                                    WHEN DAYOFWEEK(mail.send_datetime) = 5 THEN 'ხუთშაბათი'
                                    WHEN DAYOFWEEK(mail.send_datetime) = 6 THEN 'პარასკევი'
                                    WHEN DAYOFWEEK(mail.send_datetime) = 7 THEN 'შაბათი'
                                END AS `date`,
                                COUNT(*) AS `answer_count1`
                        
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                        mail.id
                        FROM      mail
                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                        AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                        JOIN      user_info ON mail.user_id = user_info.user_id
                        WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        
                        GROUP BY  mail.id) AS `chat_dur`
                        JOIN      mail ON mail.id = chat_dur.id
                        JOIN      user_info ON user_info.user_id = mail.user_id
                        WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        GROUP BY  DAYOFWEEK(mail.send_datetime)");
    }elseif ($source == 6){
        $db->setQuery("SELECT   CASE
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 1 THEN 'კვირა'
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 2 THEN 'ორშაბათი'
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 3 THEN 'სამშაბათი'
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 4 THEN 'ოთხშაბათი'
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 5 THEN 'ხუთშაბათი'
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 6 THEN 'პარასკევი'
                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 7 THEN 'შაბათი'
                                END AS `date`,
                                COUNT(*) AS `answer_count1`
                        
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                fb_chat.id
                                FROM      fb_chat
                                LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                
                                GROUP BY  fb_chat.id) AS `chat_dur`
                        JOIN      fb_chat ON fb_chat.id = chat_dur.id
                        JOIN      user_info ON user_info.user_id = fb_chat.last_user_id
                        WHERE     fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                        GROUP BY  DAYOFWEEK(fb_chat.first_datetime)");
    }elseif ($source == 14){
        $db->setQuery("SELECT     CASE
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 1 THEN 'კვირა'
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 2 THEN 'ორშაბათი'
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 3 THEN 'სამშაბათი'
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 4 THEN 'ოთხშაბათი'
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 5 THEN 'ხუთშაბათი'
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 6 THEN 'პარასკევი'
                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 7 THEN 'შაბათი'
                                  END AS `date`,
                                  COUNT(*) AS `answer_count1`
                        
                        FROM   (  SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                            viber_chat.id
                                  FROM      viber_chat
                                  LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                                  AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                  JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                  WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                
                                  GROUP BY  viber_chat.id) AS `chat_dur`
                        JOIN      viber_chat ON viber_chat.id = chat_dur.id
                        JOIN      user_info ON user_info.user_id = viber_chat.last_user_id
                        WHERE     viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                        GROUP BY  DAYOFWEEK(viber_chat.first_datetime)");
    }else{
    	$db->setQuery("SELECT   CASE
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 1 THEN 'კვირა'
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 2 THEN 'ორშაბათი'
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 3 THEN 'სამშაბათი'
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 4 THEN 'ოთხშაბათი'
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 5 THEN 'ხუთშაბათი'
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 6 THEN 'პარასკევი'
    								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 7 THEN 'შაბათი'
    							END AS `date`,
    							COUNT(*) AS `answer_count1`
                       FROM     asterisk_call_log
                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                       JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                       AND      asterisk_queue.number IN ($queue)
                       AND      asterisk_extension.number IN ($agent)
                       AND      asterisk_call_log.call_type_id = 1
                       AND      asterisk_call_log.call_status_id IN(6,7)
                       GROUP BY DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
    }
	$res3 = $db->getResultArray();
	
	foreach($res3[result] AS $row3){
        $answer_count1[] = (float)$row3[answer_count1];
    	$datetime1[]	 = $row3[date];
	}
}
	
	//------------------------------ უპასუხო ზარები კვირის დღეების მიხედვით
if($_REQUEST['act']=='unanswer_call_week'){	
	$db->setQuery(" SELECT   CASE
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 1 THEN 'კვირა'
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 2 THEN 'ორშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 3 THEN 'სამშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 4 THEN 'ოთხშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 5 THEN 'ხუთშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 6 THEN 'პარასკევი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 7 THEN 'შაბათი'
							END AS `date`,
							COUNT(*) AS `unanswer_count`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(9)
                   GROUP BY DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
	$res10 = $db->getResultArray();
	foreach($res10[result] AS $row10){
	   $unanswer_call2[] = (float)$row10[unanswer_count];
	   $date1[]		     = $row10[date];
	}
}
	//------------------------------ კავშირის გაწყვეტის მიზეზი
if($_REQUEST['act']=='disconect_couse'){
    
    $db->setQuery("SELECT   COUNT(*) AS `count`,
                           'დრო ამოიწურა' AS `cause`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(9)");
    
    $res5 = $db->getResultArray();
    foreach($res5[result] AS $row5){
        $answer_count3[] = (float)$row5[count];
        $cause1[]		 = $row5[cause];
    }
}
//------------------------------ უპასუხო ზარები რიგის მიხედვით
if($_REQUEST['act']=='unanswer_call_queue'){
	$db->setQuery("SELECT   COUNT(*) AS `count1`,
                            asterisk_queue.number AS `queue1`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(9)
                   GROUP BY asterisk_call_log.queue_id");
	$res6 = $db->getResultArray();
	foreach($res6[result] AS $row6){
	   $count1[] = (float)$row6[count1];
       $queue1[] = $row6[queue1];
	}
}
//------------------------------ ნაპასუხები ზარები რიგის მიხედვით
if($_REQUEST['act']=='answer_call_queue'){
	$db->setQuery("SELECT   asterisk_queue.number AS `dst_queue`,
							COUNT(*) AS `count`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_extension.number IN ($agent)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(6,7)
                   GROUP BY asterisk_call_log.queue_id");
	
	$res7 = $db->getResultArray();
	foreach($res7[result] AS $row7){
        $count2[] = (float)$row7[count];
    	$queue2[] = $row7[dst_queue];
    }
}
//------------------------------ უპასუხო ზარები დღეების მიხედვით
if($_REQUEST['act']=='unanswer_call_day'){
    $db->setQuery("SELECT   COUNT(*) AS `unanswer_call`,
                            DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `datetime`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(9)
                   GROUP BY DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
	
    $res8 = $db->getResultArray();
    foreach($res8[result] AS $row8){
        $unanswer_call[] = (float)$row8[unanswer_call];
    	$times[]		= $row8[datetime];
    }
}
		
//------------------------------ ნაპასუხები ზარები დღეების მიხედვით		
if($_REQUEST['act']=='answer_call_day'){
    if ($source==4) {
        $db->setQuery("SELECT   DATE(chat.join_date) AS `datetime`,
                                COUNT(*) AS `answer_count2`
                        
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                        chat.id
                        FROM      chat
                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                        AND 			chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                        JOIN      user_info ON chat.last_user_id = user_info.user_id
                        WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY  chat.id) AS `chat_dur`
                        JOIN    chat ON chat.id = chat_dur.id
                        JOIN    user_info ON user_info.user_id = chat.last_user_id
                        WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY DATE(chat.join_date)");
    }elseif ($source == 7){
        $db->setQuery("SELECT   DATE(mail.send_datetime) AS `datetime`,
                                COUNT(*) AS `answer_count2`
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                        mail.id
                        FROM      mail
                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                        AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                        JOIN      user_info ON mail.user_id = user_info.user_id
                        WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        
                        GROUP BY  mail.id) AS `chat_dur`
                        JOIN      mail ON mail.id = chat_dur.id
                        JOIN      user_info ON user_info.user_id = mail.user_id
                        WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        GROUP BY DATE(mail.send_datetime)");
    }elseif ($source == 6){
        $db->setQuery(" SELECT   DATE(fb_chat.first_datetime) AS `datetime`,
                                 COUNT(*) AS `answer_count2`
                        FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                          fb_chat.id
                                 FROM      fb_chat
                                 LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                 AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                 JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                 WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                
                                 GROUP BY  fb_chat.id) AS `chat_dur`
                        JOIN     fb_chat ON fb_chat.id = chat_dur.id
                        JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                        WHERE    fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                        GROUP BY DATE(fb_chat.first_datetime)");
    }elseif ($source == 14){
        $db->setQuery(" SELECT   DATE(viber_chat.first_datetime) AS `datetime`,
                                 COUNT(*) AS `answer_count2`
                        FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                           viber_chat.id
                                 FROM      viber_chat
                                 LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                                 AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                 JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                 WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                
                                GROUP BY  viber_chat.id) AS `chat_dur`
                        JOIN     viber_chat ON viber_chat.id = chat_dur.id
                        JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                        WHERE    viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                        GROUP BY DATE(viber_chat.first_datetime)");
    }else{
        $db->setQuery("SELECT   DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `datetime`,
    							COUNT(*) AS `answer_count2`
                       FROM     asterisk_call_log
                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                       JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                       AND      asterisk_queue.number IN ($queue)
                       AND      asterisk_extension.number IN ($agent)
                       AND      asterisk_call_log.call_type_id = 1
                       AND      asterisk_call_log.call_status_id IN(6,7)
                       GROUP BY DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
    }
    $res4 = $db->getResultArray();
    foreach($res4[result] AS $row4){
        $answer_count2[] = (float)$row4[answer_count2];
    	$datetime2[]	 = $row4[datetime];
	}
}
//------------------------------ უპასუხო ზარები საათების მიხედვით
if($_REQUEST['act']=='unanswer_call_hour'){			
	$db->setQuery("SELECT  CASE		
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 0 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 1 THEN '00:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 1 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 2 THEN '01:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 2 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 3 THEN '02:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 3 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 4 THEN '03:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 4 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 5 THEN '04:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 5 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 6 THEN '05:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 6 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 7 THEN '06:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 7 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 8 THEN '07:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 8 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 9 THEN '08:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 9 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 10 THEN '09:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 10 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 11 THEN '10:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 11 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 12 THEN '11:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 12 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 13 THEN '12:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 13 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 14 THEN '13:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 14 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 15 THEN '14:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 15 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 16 THEN '15:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 16 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 17 THEN '16:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 17 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 18 THEN '17:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 18 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 19 THEN '18:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 19 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 20 THEN '19:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 20 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 21 THEN '20:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 21 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 22 THEN '21:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 22 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 23 THEN '22:00'
								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 23 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 24 THEN '23:00'
                            END AS `times`,
							COUNT(*) AS `unanswer_count`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(9)
                   GROUP BY HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
	
	$res9 = $db->getResultArray();
	foreach($res9[result] AS $row9){
	    $unanswer_count1[] = (float)$row9[unanswer_count];
		$times2[]		= $row9[times];
	}
}
	//------------------------------ ნაპასუხები ზარები საათების მიხედვით
if($_REQUEST['act']=='answer_call_hour'){
    
    if ($source == 4) {
        $db->setQuery("SELECT   CASE		
    								WHEN HOUR(chat.join_date) >= 0 AND HOUR(chat.join_date) < 1 THEN '00:00'
    								WHEN HOUR(chat.join_date) >= 1 AND HOUR(chat.join_date) < 2 THEN '01:00'
    								WHEN HOUR(chat.join_date) >= 2 AND HOUR(chat.join_date) < 3 THEN '02:00'
    								WHEN HOUR(chat.join_date) >= 3 AND HOUR(chat.join_date) < 4 THEN '03:00'
    								WHEN HOUR(chat.join_date) >= 4 AND HOUR(chat.join_date) < 5 THEN '04:00'
    								WHEN HOUR(chat.join_date) >= 5 AND HOUR(chat.join_date) < 6 THEN '05:00'
    								WHEN HOUR(chat.join_date) >= 6 AND HOUR(chat.join_date) < 7 THEN '06:00'
    								WHEN HOUR(chat.join_date) >= 7 AND HOUR(chat.join_date) < 8 THEN '07:00'
    								WHEN HOUR(chat.join_date) >= 8 AND HOUR(chat.join_date) < 9 THEN '08:00'
    								WHEN HOUR(chat.join_date) >= 9 AND HOUR(chat.join_date) < 10 THEN '09:00'
    								WHEN HOUR(chat.join_date) >= 10 AND HOUR(chat.join_date) < 11 THEN '10:00'
    								WHEN HOUR(chat.join_date) >= 11 AND HOUR(chat.join_date) < 12 THEN '11:00'
    								WHEN HOUR(chat.join_date) >= 12 AND HOUR(chat.join_date) < 13 THEN '12:00'
    								WHEN HOUR(chat.join_date) >= 13 AND HOUR(chat.join_date) < 14 THEN '13:00'
    								WHEN HOUR(chat.join_date) >= 14 AND HOUR(chat.join_date) < 15 THEN '14:00'
    								WHEN HOUR(chat.join_date) >= 15 AND HOUR(chat.join_date) < 16 THEN '15:00'
    								WHEN HOUR(chat.join_date) >= 16 AND HOUR(chat.join_date) < 17 THEN '16:00'
    								WHEN HOUR(chat.join_date) >= 17 AND HOUR(chat.join_date) < 18 THEN '17:00'
    								WHEN HOUR(chat.join_date) >= 18 AND HOUR(chat.join_date) < 19 THEN '18:00'
    								WHEN HOUR(chat.join_date) >= 19 AND HOUR(chat.join_date) < 20 THEN '19:00'
    								WHEN HOUR(chat.join_date) >= 20 AND HOUR(chat.join_date) < 21 THEN '20:00'
    								WHEN HOUR(chat.join_date) >= 21 AND HOUR(chat.join_date) < 22 THEN '21:00'
    								WHEN HOUR(chat.join_date) >= 22 AND HOUR(chat.join_date) < 23 THEN '22:00'
    								WHEN HOUR(chat.join_date) >= 23 AND HOUR(chat.join_date) < 24 THEN '23:00'
                                END AS `times`,
    							COUNT(*) AS `answer_count`
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                        chat.id
                        FROM      chat
                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                        AND 			chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                        JOIN      user_info ON chat.last_user_id = user_info.user_id
                        WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY  chat.id) AS `chat_dur`
                        JOIN    chat ON chat.id = chat_dur.id
                        JOIN    user_info ON user_info.user_id = chat.last_user_id
                        WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                        GROUP BY HOUR(chat.join_date)");
    }elseif ($source == 7){
        $db->setQuery("SELECT   CASE		
    								WHEN HOUR(mail.send_datetime) >= 0 AND HOUR(mail.send_datetime) < 1 THEN '00:00'
    								WHEN HOUR(mail.send_datetime) >= 1 AND HOUR(mail.send_datetime) < 2 THEN '01:00'
    								WHEN HOUR(mail.send_datetime) >= 2 AND HOUR(mail.send_datetime) < 3 THEN '02:00'
    								WHEN HOUR(mail.send_datetime) >= 3 AND HOUR(mail.send_datetime) < 4 THEN '03:00'
    								WHEN HOUR(mail.send_datetime) >= 4 AND HOUR(mail.send_datetime) < 5 THEN '04:00'
    								WHEN HOUR(mail.send_datetime) >= 5 AND HOUR(mail.send_datetime) < 6 THEN '05:00'
    								WHEN HOUR(mail.send_datetime) >= 6 AND HOUR(mail.send_datetime) < 7 THEN '06:00'
    								WHEN HOUR(mail.send_datetime) >= 7 AND HOUR(mail.send_datetime) < 8 THEN '07:00'
    								WHEN HOUR(mail.send_datetime) >= 8 AND HOUR(mail.send_datetime) < 9 THEN '08:00'
    								WHEN HOUR(mail.send_datetime) >= 9 AND HOUR(mail.send_datetime) < 10 THEN '09:00'
    								WHEN HOUR(mail.send_datetime) >= 10 AND HOUR(mail.send_datetime) < 11 THEN '10:00'
    								WHEN HOUR(mail.send_datetime) >= 11 AND HOUR(mail.send_datetime) < 12 THEN '11:00'
    								WHEN HOUR(mail.send_datetime) >= 12 AND HOUR(mail.send_datetime) < 13 THEN '12:00'
    								WHEN HOUR(mail.send_datetime) >= 13 AND HOUR(mail.send_datetime) < 14 THEN '13:00'
    								WHEN HOUR(mail.send_datetime) >= 14 AND HOUR(mail.send_datetime) < 15 THEN '14:00'
    								WHEN HOUR(mail.send_datetime) >= 15 AND HOUR(mail.send_datetime) < 16 THEN '15:00'
    								WHEN HOUR(mail.send_datetime) >= 16 AND HOUR(mail.send_datetime) < 17 THEN '16:00'
    								WHEN HOUR(mail.send_datetime) >= 17 AND HOUR(mail.send_datetime) < 18 THEN '17:00'
    								WHEN HOUR(mail.send_datetime) >= 18 AND HOUR(mail.send_datetime) < 19 THEN '18:00'
    								WHEN HOUR(mail.send_datetime) >= 19 AND HOUR(mail.send_datetime) < 20 THEN '19:00'
    								WHEN HOUR(mail.send_datetime) >= 20 AND HOUR(mail.send_datetime) < 21 THEN '20:00'
    								WHEN HOUR(mail.send_datetime) >= 21 AND HOUR(mail.send_datetime) < 22 THEN '21:00'
    								WHEN HOUR(mail.send_datetime) >= 22 AND HOUR(mail.send_datetime) < 23 THEN '22:00'
    								WHEN HOUR(mail.send_datetime) >= 23 AND HOUR(mail.send_datetime) < 24 THEN '23:00'
                                END AS `times`,
    							COUNT(*) AS `answer_count`
                        FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                        mail.id
                        FROM      mail
                        LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                        AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                        JOIN      user_info ON mail.user_id = user_info.user_id
                        WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        
                        GROUP BY  mail.id) AS `chat_dur`
                        JOIN      mail ON mail.id = chat_dur.id
                        JOIN      user_info ON user_info.user_id = mail.user_id
                        WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                        GROUP BY HOUR(mail.send_datetime)");
    }elseif ($source == 6){
        $db->setQuery(" SELECT   CASE
                                    WHEN HOUR(fb_chat.first_datetime) >= 0 AND HOUR(fb_chat.first_datetime) < 1 THEN '00:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 1 AND HOUR(fb_chat.first_datetime) < 2 THEN '01:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 2 AND HOUR(fb_chat.first_datetime) < 3 THEN '02:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 3 AND HOUR(fb_chat.first_datetime) < 4 THEN '03:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 4 AND HOUR(fb_chat.first_datetime) < 5 THEN '04:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 5 AND HOUR(fb_chat.first_datetime) < 6 THEN '05:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 6 AND HOUR(fb_chat.first_datetime) < 7 THEN '06:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 7 AND HOUR(fb_chat.first_datetime) < 8 THEN '07:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 8 AND HOUR(fb_chat.first_datetime) < 9 THEN '08:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 9 AND HOUR(fb_chat.first_datetime) < 10 THEN '09:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 10 AND HOUR(fb_chat.first_datetime) < 11 THEN '10:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 11 AND HOUR(fb_chat.first_datetime) < 12 THEN '11:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 12 AND HOUR(fb_chat.first_datetime) < 13 THEN '12:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 13 AND HOUR(fb_chat.first_datetime) < 14 THEN '13:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 14 AND HOUR(fb_chat.first_datetime) < 15 THEN '14:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 15 AND HOUR(fb_chat.first_datetime) < 16 THEN '15:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 16 AND HOUR(fb_chat.first_datetime) < 17 THEN '16:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 17 AND HOUR(fb_chat.first_datetime) < 18 THEN '17:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 18 AND HOUR(fb_chat.first_datetime) < 19 THEN '18:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 19 AND HOUR(fb_chat.first_datetime) < 20 THEN '19:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 20 AND HOUR(fb_chat.first_datetime) < 21 THEN '20:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 21 AND HOUR(fb_chat.first_datetime) < 22 THEN '21:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 22 AND HOUR(fb_chat.first_datetime) < 23 THEN '22:00'
                                    WHEN HOUR(fb_chat.first_datetime) >= 23 AND HOUR(fb_chat.first_datetime) < 24 THEN '23:00'
                                 END AS `times`,
                                 COUNT(*) AS `answer_count`
                        FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                           fb_chat.id
                                 FROM      fb_chat
                                 LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                 AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                 JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                 WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                
                                 GROUP BY  fb_chat.id) AS `chat_dur`
                        JOIN     fb_chat ON fb_chat.id = chat_dur.id
                        JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                        WHERE    fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                        GROUP BY HOUR(fb_chat.first_datetime)");
    }elseif ($source == 14){
        $db->setQuery(" SELECT   CASE
                                    WHEN HOUR(viber_chat.first_datetime) >= 0 AND HOUR(viber_chat.first_datetime) < 1 THEN '00:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 1 AND HOUR(viber_chat.first_datetime) < 2 THEN '01:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 2 AND HOUR(viber_chat.first_datetime) < 3 THEN '02:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 3 AND HOUR(viber_chat.first_datetime) < 4 THEN '03:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 4 AND HOUR(viber_chat.first_datetime) < 5 THEN '04:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 5 AND HOUR(viber_chat.first_datetime) < 6 THEN '05:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 6 AND HOUR(viber_chat.first_datetime) < 7 THEN '06:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 7 AND HOUR(viber_chat.first_datetime) < 8 THEN '07:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 8 AND HOUR(viber_chat.first_datetime) < 9 THEN '08:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 9 AND HOUR(viber_chat.first_datetime) < 10 THEN '09:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 10 AND HOUR(viber_chat.first_datetime) < 11 THEN '10:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 11 AND HOUR(viber_chat.first_datetime) < 12 THEN '11:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 12 AND HOUR(viber_chat.first_datetime) < 13 THEN '12:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 13 AND HOUR(viber_chat.first_datetime) < 14 THEN '13:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 14 AND HOUR(viber_chat.first_datetime) < 15 THEN '14:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 15 AND HOUR(viber_chat.first_datetime) < 16 THEN '15:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 16 AND HOUR(viber_chat.first_datetime) < 17 THEN '16:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 17 AND HOUR(viber_chat.first_datetime) < 18 THEN '17:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 18 AND HOUR(viber_chat.first_datetime) < 19 THEN '18:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 19 AND HOUR(viber_chat.first_datetime) < 20 THEN '19:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 20 AND HOUR(viber_chat.first_datetime) < 21 THEN '20:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 21 AND HOUR(viber_chat.first_datetime) < 22 THEN '21:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 22 AND HOUR(viber_chat.first_datetime) < 23 THEN '22:00'
                                    WHEN HOUR(viber_chat.first_datetime) >= 23 AND HOUR(viber_chat.first_datetime) < 24 THEN '23:00'
                                 END AS `times`,
                                 COUNT(*) AS `answer_count`
                        FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                           viber_chat.id
                                 FROM      viber_chat
                                 LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                                 AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                 JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                 WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                
                                 GROUP BY  viber_chat.id) AS `chat_dur`
                        JOIN     viber_chat ON viber_chat.id = chat_dur.id
                        JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                        WHERE    viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                        GROUP BY HOUR(viber_chat.first_datetime)");
    }else{
    	$db->setQuery("SELECT  CASE		
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 0 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 1 THEN '00:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 1 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 2 THEN '01:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 2 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 3 THEN '02:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 3 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 4 THEN '03:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 4 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 5 THEN '04:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 5 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 6 THEN '05:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 6 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 7 THEN '06:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 7 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 8 THEN '07:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 8 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 9 THEN '08:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 9 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 10 THEN '09:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 10 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 11 THEN '10:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 11 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 12 THEN '11:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 12 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 13 THEN '12:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 13 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 14 THEN '13:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 14 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 15 THEN '14:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 15 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 16 THEN '15:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 16 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 17 THEN '16:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 17 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 18 THEN '17:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 18 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 19 THEN '18:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 19 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 20 THEN '19:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 20 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 21 THEN '20:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 21 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 22 THEN '21:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 22 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 23 THEN '22:00'
    								WHEN HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 23 AND HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) < 24 THEN '23:00'
                                END AS `times`,
    							COUNT(*) AS `answer_count`
                       FROM     asterisk_call_log
                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                       JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                       AND      asterisk_queue.number IN ($queue)
                       AND      asterisk_extension.number IN ($agent)
                       AND      asterisk_call_log.call_type_id = 1
                       AND      asterisk_call_log.call_status_id IN(6,7)
                       GROUP BY HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
    }
	$res2 = $db->getResultArray();
	foreach($res2[result] AS $row2){
        $answer_count[] = (float)$row2[answer_count];
    	$datetime[] 	= $row2[times];
	}
}
			//------------------------------- მომსახურების დონე(Service Level)
			
			
if($_REQUEST['act']=='answer_call_sec'){			
	$db->setQuery("	SELECT  asterisk_call_log.wait_time AS `wait_time`
                   FROM     asterisk_call_log
                   JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                   JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                   WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                   AND      asterisk_queue.number IN ($queue)
                   AND      asterisk_extension.number IN ($agent)
                   AND      asterisk_call_log.call_type_id = 1
                   AND      asterisk_call_log.call_status_id IN(6,7)");
	
	$res_service_level = $db->getResultArray();
	
	$w15 = 0;
	$w30 = 0;
	$w45 = 0;
	$w60 = 0;
	$w75 = 0;
	$w90 = 0;
	$w91 = 0;
	
	foreach($res_service_level[result] AS $res_service_level_r) {
	
    	if ($res_service_level_r['wait_time'] < 15) {
    	   $w15++;
    	}
    	
    	if ($res_service_level_r['wait_time'] < 30){
    	   $w30++;
    	}
    	
        if ($res_service_level_r['wait_time'] < 45){
    	   $w45++;
    	}
    	
    	if ($res_service_level_r['wait_time'] < 60){
    	   $w60++;
    	}
    	
    	if ($res_service_level_r['wait_time'] < 75){
    	   $w75++;
    	}
    	
    	if ($res_service_level_r['wait_time'] < 90){
    	   $w90++;
    	}
    	
    	$w91++;
	
	}

	$d30 = $w30 - $w15;
	$d45 = $w45 - $w30;
	$d60 = $w60 - $w45;
	$d75 = $w75 - $w60;
	$d90 = $w90 - $w75;
	$d91 = $w91 - $w90;
			
    $mas = array($w15,$d30,$d45,$d60,$d75,$d90,$d91);
    $call_second=array('15 წამში','30 წამში','45წამში','60 წამში','75 წამში','90 წამში','90+წამში');			
}
							
$unit[]="  მომართვა";
$series[] = array('name' => $name, 'unit' => $unit, 'quantity' => $quantity, 'cause' => $cause);
$series[] = array('name' => $name, 'unit' => $unit, 'call_count' => $call_count, 'agent' => $agentt);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count' => $answer_count, 'datetime' => $datetime);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count1' => $answer_count1, 'datetime1' => $datetime1);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count2' => $answer_count2, 'datetime2' => $datetime2);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count3' => $answer_count3, 'cause1' => $cause1);
$series[] = array('name' => $name, 'unit' => $unit, 'count1' => $count1, 'queue1' => $queue1);
$series[] = array('name' => $name, 'unit' => $unit, 'count2' => $count2, 'queue2' => $queue2);
$series[] = array('name' => $name, 'unit' => $unit, 'unanswer_call' => $unanswer_call, 'times' => $times);
$series[] = array('name' => $name, 'unit' => $unit, 'unanswer_count1' => $unanswer_count1, 'times2' => $times2);
$series[] = array('name' => $name, 'unit' => $unit, 'unanswer_count2' => $unanswer_call2, 'date1' => $date1);
$series[] = array('name' => $name, 'unit' => $unit, 'mas' => $mas, 'call_second' => $call_second);

echo json_encode($series);

?>