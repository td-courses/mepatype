<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$action	    = $_REQUEST['act'];
$user_id    = $_SESSION['USERID'];
$error  	= '';
$data	    = array();
 
switch ($action) {
    case 'get_add_page':
        $table               = $_REQUEST['table'];

        if($table == "mail"){
            $page		= GetMailPage('', $table);
        }elseif($table == 'clients'){
            $page       = GetClientsPage('', $table);
        }

		$data		= array('page'	=> $page);

    break;
	case 'get_edit_page':
           $id		            = $_REQUEST['id'];
           $table               = $_REQUEST['table'];
        
            if($table == "clients"){
                $page		    = GetClientsPage(client_details($id), $table);
            }elseif($table == "numbers"){
                $page		    = GetNumbersPage(number_detials($id), $table);
            }elseif($table == "fbm"){
                // $page		    = GetFbmPage(fbm_detials($id), $table);
               $error = 1;
            }elseif($table == "viber"){
                // $page		    = GetViberPage('');
                $error = 1;
            }elseif($table == "mail"){
                $page		    = GetMailPage(mail_detials($id), $table);
            }


           $data		        = array('page'	=> $page);
    break;
    case 'get_columns':
        $columnCount = 		$_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] = 	$_REQUEST['names'];
        $operators[] = 		$_REQUEST['operators'];
        $selectors[] = 		$_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $i++;
        }
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
			{
				continue;
            }
            else{
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    $g = array('field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for($j=0;$j<$columnCount;$j++)
        {
            if($res['data_type'][$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        $filtArr = array('fields'=>$new_data);
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';
        
    break;
    case 'get_clients':

        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT  `id`,
                                `name`
                        FROM    `client_accounts`
                        WHERE   `actived` = 1");

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'save_client':
        $id           = $_REQUEST['id'];
        $name         = $_REQUEST['name'];

        if(!empty($id)){
            edit_client($id, $name);
        }else{
            add_client($name);
        }

    break;
    case 'delete_client':
        $id     =   $_REQUEST['id'];
        $user_id    = $_SESSION['USERID'];

        if($user_id == 1){
            $db->setQuery(" UPDATE  `client_accounts`
                            SET     `actived`   =   0
                            WHERE   `id`        =   $id");

            $db->execQuery();
            $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
        }else{
            $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
        }
    break;
    case 'get_numbers':

        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT      `asterisk_queue`.`id`,
                                    `asterisk_queue`.`number`,
                                    `client_accounts`.`name`
                        FROM        `asterisk_queue`
                        LEFT JOIN   `client_accounts` ON `client_accounts`.`id` = `asterisk_queue`.`client_account_id`
                        ");

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'save_number':

        $id           = $_REQUEST['id'];
        $client         = $_REQUEST['client'];

        if(!empty($id)){
            edit_number($id, $client);
        }

    break;
    case 'get_socials':

        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT  `id`,
                                `name`
                        FROM    `client_accounts`");

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'get_social_names':

        $db->setQuery(" SELECT  `key`, `name` 
                        FROM    `source`
                        WHERE   `actived` = 1 AND `key` NOT IN ('phone', 'chat', 'fbc')");
        
        $result = $db->getResultArray();
        $data = $result;

    break;
    case 'fbm':

        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT      `fb_account`.`id`,
                                    `fb_account`.`name`,
                                    `client_accounts`.`name` AS `client`
                        FROM        `fb_account`
                        LEFT JOIN   `client_accounts` ON `client_accounts`.`id` = `fb_account`.`site_id`
                        WHERE       `fb_account`.`actived` = 1");

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'viber':

        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT      `viber_account`.`id`,
                                    `viber_account`.`name`,
                                    `client_accounts`.`name` AS client
                        FROM        `viber_account`
                        LEFT JOIN   `client_accounts` ON `client_accounts`.`id` = `viber_account`.`site_id`
                        WHERE       `viber_account`.`actived` = 1");

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'mail':

        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT      `mail_account`.`id`,
                                    `mail_account`.`mail`,
                                    `client_accounts`.`name` AS `client`
                        FROM        `mail_account`
                        LEFT JOIN   `client_accounts` ON `client_accounts`.`id` = `mail_account`.`site_id`
                        WHERE       `mail_account`.`actived` = 1");

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'save_mail':
       
        $id                 =   $_REQUEST['id'];
        $mail_name          =   $_REQUEST['mail_name'];
        $mail_mail          =   $_REQUEST['mail_mail'];
        $mail_url           =   $_REQUEST['mail_url'];
        $client             =   $_REQUEST['client'];
        $mail_password      =   $_REQUEST['mail_password'];

        if(!empty($id)){
            edit_mail($id, $mail_name, $mail_mail, $mail_url, $client, $mail_password);
        }else{
            add_mail($mail_name, $mail_mail, $mail_url, $client, $mail_password);
        }

    break;
    case 'delete_mail':

        $id     =   $_REQUEST['id'];
        $user_id    = $_SESSION['USERID'];

        if($user_id == 1){
            $db->setQuery(" UPDATE  `mail_account`
                            SET     `actived`   =   0
                            WHERE   `id`        =   $id");

            $db->execQuery();
            $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
        }else{
            $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
        }

    break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
 * ******************************
*/

function getSelectors($table)
{
	GLOBAL $db;
	$query = "
				SELECT `name`,`id` 
				FROM   $table 
				ORDER BY id ASC
	";
	
	$db -> setQuery($query);
	$result = $db -> getResultArray();
	
	$selectorData = array();
	
	foreach($result['result'] AS $option)
	{
		array_push($selectorData,array('text'=>$option['name'],'value'=>$option['id']));
	}
	
	return $selectorData;
}

function getClients_selector($id){

    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `client_accounts`
				   WHERE   `actived` = 1");
    
    $data = $db->getSelect($id);

    return $data;

}

function client_details($id){
    
    global $db;

    $db->setQuery(" SELECT  `id`,
                            `name`
                    FROM    `client_accounts`
                    WHERE   `id` = $id");
    $res = $db->getResultArray();

    return $res['result'][0];
    
}

function add_client($name){
    
    global $db;

    $user_id    = $_SESSION['USERID'];
    if($user_id == 1){
        $db->setQuery(" INSERT  INTO    `client_accounts`
                                    (`user_id`, `name`)
                                VALUES
                                    ('$user_id', '$name')");
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
    }else{
        $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
    }
}

function edit_client($id, $name){

    global $db;

    $user_id    = $_SESSION['USERID'];
    if($user_id == 1){
        $db->setQuery(" UPDATE  `client_accounts`
                        SET     `name`  = '$name'
                        WHERE   `id`    = $id");
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
    }else{
        $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
    }
}

function number_detials($id){
    global $db;

    $db->setQuery(" SELECT      `asterisk_queue`.`id`, 
                                `asterisk_queue`.`number`,
                                `asterisk_queue`.`client_account_id` AS `client_id`,
                                `client_accounts`.`name`
                                
                    FROM        `asterisk_queue`
                    LEFT JOIN   `client_accounts` ON `client_accounts`.`id` = `asterisk_queue`.`client_account_id`
                    WHERE       `asterisk_queue`.`id`   = $id");
    $res = $db->getResultArray();

    return $res['result'][0];
}

function edit_number($id, $client){

    global $db;

    $user_id    = $_SESSION['USERID'];
    if($user_id == 1){
        $db->setQuery(" UPDATE  `asterisk_queue`
                        SET     `client_account_id`  = '$client'
                        WHERE   `id`    = $id");

        $db->execQuery();

        $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
    }else{
        $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
    }
}

function mail_detials($id){
    global $db;

    $db->setQuery(" SELECT      `mail_account`.`id`,
                                `mail_account`.`name`,
                                `mail_account`.`mail`,
                                `mail_account`.`password`,
                                `mail_account`.`url`,
                                `mail_account`.`site_id`
                                
                    FROM        `mail_account`
                    WHERE       `mail_account`.`id`   = $id");
    $res = $db->getResultArray();

    return $res['result'][0];
}

function add_mail($mail_name, $mail_mail, $mail_url, $client, $mail_password){
    
    global $db;

    $user_id    = $_SESSION['USERID'];
    if($user_id == 1){
        $db->setQuery(" INSERT  INTO    `mail_account`
                                    (`name`, `site_id`, `mail`, `password`, `url`, `user_id`)
                                VALUES
                                    ('$mail_name', '$client', '$mail_mail', '$mail_password', '$mail_url', '$user_id')");
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
    }else{
        $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
    }
}

function edit_mail($id, $mail_name, $mail_mail, $mail_url, $client, $mail_password){

    global $db;

    $user_id    = $_SESSION['USERID'];
    if($user_id == 1){
        $db->setQuery(" UPDATE  `mail_account`
                        SET     `name`      =   '$mail_name',
                                `mail`      =   '$mail_mail',
                                `url`       =   '$mail_url',
                                `site_id`   =   '$client',
                                `password`  =   '$mail_password'
                        WHERE   `id`        =   $id");
                                
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით განხორციელდა');
    }else{
        $data = array('error' => 1, 'message' => 'მოთხოვნა უარყოფილია');
    }

}

/** 
 * ******************************
 * DIALOG HTML
 * ******************************
 */
function GetClientsPage($res = '', $table){
	$data = '
	<div id="dialog-form">
	    <fieldset style="text-align: center">
            <legend>ძირითადი ინფორმაცია</legend>
            <label>კლიენტის სახელი</label>
            <input type="text"  id="client_name" value="'.$res['name'].'" style="width: 300px; margin: 10px">
            <input type="hidden" id="hidden_client_id" value="'.$res['id'].'" />
            <input type="hidden" id="hidden_table_key" value="'.$table.'" />
        </fieldset>
    </div>
    ';
	return $data;
}

function GetNumbersPage($res = '', $table){
	$data = '
	<div id="dialog-form">
	    <fieldset>
            <legend>ძირითადი ინფორმაცია</legend>
            <table class="dialog_table">
                <tr>
                    <td>
                        <label>ნომერი</label>
                        <input id="client_number" type="text" value="'.$res['number'].'" disabled>
                    </td>
                    <td>
                        <label>კლიენტი</label>
                        <select style="width: 150px;" id="client_selector">'.getClients_selector($res['client_id']).'</select>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="hidden_number_id" value="'.$res['id'].'" />
            <input type="hidden" id="hidden_table_key" value="'.$table.'" />
        </fieldset>
    </div>
    ';
	return $data;
}

function GetMailPage($res = '', $table){

    $user_id = $_SESSION['USERID'];

    if($user_id == 1){
        $password = "value='".$res['password']."'";
    }else{
        $password = "value='***************' disabled";
    }

	$data = '
	<div id="dialog-form">
	    <fieldset>
            <legend>ძირითადი ინფორმაცია</legend>
            <table class="dialog_table">
                <tr>
                    <td>
                        <label>სახელი</label>
                        <input id="mail_name" type="text" value="'.$res['name'].'">
                    </td>
                    <td>
                        <label>ელ. ფოსტა</label>
                        <input id="mail_mail" type="text" value="'.$res['mail'].'">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>მისამართი</label>
                        <input id="mail_url" type="text" value="'.$res['url'].'">
                    </td>
                    <td>
                        <label>კლიენტი</label>
                        <select style="width: 150px;" id="client_selector">'.getClients_selector($res['site_id']).'</select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>პაროლი</label>
                        <input id="mail_password" type="text" '.$password.'>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="hidden_mail_id" value="'.$res['id'].'" />
            <input type="hidden" id="hidden_table_key" value="'.$table.'" />
        </fieldset>
    </div>
    ';
	return $data;
}

?>
