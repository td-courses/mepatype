
import Tdg from "../tdg.class.js";

export default class Menu extends Tdg{

  constructor(){
    super();

    this.state = {
      menu: []
    }

    this.menuSection = "section[menu]";

    this.appendTo = (e) => $(this.menuSection).append(e)

  }


  
  /**
   * @event onClick
   * @returns burger Element
   */
  createBurgerMenu = () => {

    let thisClass = this;

      let burgerEl = this.CreateElement({
        element: "burgerMenu",
        onClick: function() {
          thisClass.burgerMenuHandler(this)
        }
      })

      for(let i = 0; i < 3; i++){

        let burgerSpanEl = this.CreateElement({
          element: "span"
        })

        burgerEl.append(burgerSpanEl);

      }

      return burgerEl;

  }

  /**
   * 
   * @param {Event} e 
   */
  burgerMenuHandler = (e) => {
    
    if(!$(e).hasClass("active")){
      $(e).addClass("active")
      $(this.menuSection).attr("active", true)
      $(this.menuSection).animate({
        width: "300px"
      }).removeClass('remove-scroll')
      // this.state.menu.forEach(x => $(x).animate({
      //   "height": "auto"
      // }))
  }else{
    $(e).removeClass("active")
    $(this.menuSection).removeAttr("active").animate({
      width: "60px"
    }).addClass('remove-scroll')
    // this.state.menu.forEach(x => $(x).animate({
    //   "height": "44px"
    // }))
  }

  }


  buildMenuContent = (dataObj) => {
      let thisClass = this;

      let menu = this.CreateElement({
          element: "menu",
          attributes: [
            (dataObj.sub == "" && dataObj.url ? { url: dataObj.url } : ''),
            (dataObj.sub == "" && dataObj.page ? { page: dataObj.page } : ''),
            (dataObj.sub == "" && dataObj.page_id ? { "data-id": dataObj.page_id } : ''),
            // (dataObj.sub == "" && dataObj.param ? { param: new URLSearchParams(dataObj.param).toString() } : '' ),
            (dataObj.active ? "active" : ''),
            (dataObj.sub ? { "aria-open": false } : '')
          ],
          onClick: function (e) {
            thisClass.menuHandler(e.target, this, dataObj)
          } 
      })

      // MENU ICON
      let menuIcon = this.CreateElement({
        element: "icon",
        attributes: ["svg"],
        children: `<svg viewBox="0 0 24 24" width="24" height="24">
        <use xlink:href="Assets/images/icons/menu/menu.svg#${dataObj.icon}" />
      </svg>`
      })
      menu.append(menuIcon)

      // MENU ITEM
      let menuItem = this.CreateElement({
        element: "menuitem",
        children:  (dataObj.sub == "" && dataObj.page_id ? `<span><a href="?id=${dataObj.page_id}">${dataObj.name}</a></span>`  : `<span>${dataObj.name}</span>`),
        
      })
      menu.append(menuItem)


      // TOGGLE ARROW
      let menuArrow = this.CreateElement({
        element: "icon",
        attributes: ["font"]
      })

      // MENU SUBITEM
      if(typeof dataObj.sub == "object"){
        let subMenu
        dataObj.sub.map((x, i) => {
          subMenu = this.CreateElement({
            element: (typeof x.sub == "object" ? 'menu' : "submenu"),
            attributes: [(typeof x.sub == "object" ? {"aria-open": false} : "")],
            children: (typeof x.sub == "object" ? '' : (x.page_id ? `<span><a href="?id=${x.page_id}">${x.name}</a></span>`  : `<span>${x.name}</span>`))
          })

          menuItem.append(subMenu)
          menu.append(menuArrow)

          if(typeof x.sub == "object"){
           
              // MENU ITEM
              let menuItem = this.CreateElement({
                element: "menuitem",
                children: `<span>${x.name}</span>`
              })
              subMenu.append(menuItem)
              
              let menuArrow = this.CreateElement({
                element: "icon",
                attributes: ["font"],
                style: {
                  width: "16px"
                }
              })

              subMenu.append(menuArrow)
              

              x.sub.map((x, i) => {
                let subMenuItem = this.CreateElement({
                  element: (typeof x.sub == "object" ? 'menuitem' : "submenu"),
                  children: (x.sub == "" && x.page_id ? `<span><a href="?id=${x.page_id}">${x.name}</a></span>`  : `<span>${x.name}</span>`)
                })
                menuItem.append(subMenuItem)
    
            })
          }

        })

      }

      this.state.menu.push(menu)
      return menu;

  }

    /**
   * 
   * @param {Event} event 
   * @param {Object} dataObj 
   */
     menuHandler = (target, event, dataObj, sub = false) => {
    
      let parent = $(target).parent();
      let checkMenuParent = $(target).parents("menu");
    
      if(parent[0].localName == "section" || parent[0].localName == "submenu" || parent[0].localName == "span") return false;

      if(checkMenuParent[0].getAttribute("aria-open") != null) {

        // FOR TOGGLING ATTRIBUTE  - EASY WAY! USE IT!
        $(checkMenuParent[0]).attr("aria-open", (i, attr) => attr == "false" ? true : false)
       
        $(checkMenuParent[0]).children("menuitem").toggleClass("max-content")

        if(!$("burgermenu").hasClass("active")){
          this.burgerMenuHandler($("burgermenu"))
        }

      }else{
        window.location = `?id=${$(event).attr("data-id")}`
      }
    }


  createMenu = (dataObj) => {

    dataObj.forEach((x, i) => {

      let menuItem = this.buildMenuContent(x);

      this.appendTo(menuItem)

    })

  }


  initializeMenu = () =>  {
    
      this.appendTo(this.createBurgerMenu());

    let thisClass = this;
    
    $.ajax({
      data: {
        route: "Menu",
        act: "getMenuData"
      },
      success: function(data){

        thisClass.appendTo(thisClass.createMenu(data))

      }
    })

    }

  


}