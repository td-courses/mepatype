<?php
require_once('excel/PHPExcel/IOFactory.php');
require_once('classes/class.Mysqli.php');
error_reporting(E_ALL);

global $db;
$db = new dbClass();
$campaign_id = $_REQUEST['campaign_id'];
$db->setQuery("SELECT 	FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'call_date',
                        outgoing_campaign_request_base.phone_number,
                        outgoing_campaign_request_base.number_1,
                        outgoing_campaign_request.outgoing_campaign_type_id AS 'type_id',
                        CONCAT(outgoing_campaign_request_base.number_3,'( ',outgoing_campaign_request_base.number_1,' )') AS 'client',
                        compaign_request_processing.value,
                        user_info.name AS 'user_name',
                        TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.talk_time),'%i:%s') AS 'talk_time',
                        IF(ISNULL(ss.int_value),'უპასუხო','ნაპასუხები') AS 'status',
                        CASE
													WHEN compaign_quize_processing.int_value = 1 THEN 'კი'
													WHEN compaign_quize_processing.int_value = 2 THEN 'არა'
													ELSE 'არ არის შევსებული'
												END AS 'answer',
                        (CASE
                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 1 THEN '<p class=status_pink><img src=\"media/images/icons/play.png\"></p>'
                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 2 THEN '<p class=status_lightgreen><img src=\"media/images/icons/pause.png\"></p>'
                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 3 THEN '<p class=status_red><img src=\"media/images/icons/stop.png\"></p>'
                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 4 THEN '<p class=status_done><img src=\"media/images/icons/icon_check2.png\"></p>'

                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 5 THEN 'არ მოუსმინა'
                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 6 THEN 'ნაწილობრივ მოუსმინა'
                            WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 7 THEN 'მოუსმინა'
                        END) AS 'status2'

                FROM 		outgoing_campaign_request_base
                LEFT JOIN asterisk_call_log ON asterisk_call_log.id = outgoing_campaign_request_base.call_log_id
                LEFT JOIN compaign_request_processing ON compaign_request_processing.base_id = outgoing_campaign_request_base.id AND compaign_request_processing.processing_setting_detail_id = 140
                LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                LEFT JOIN compaign_request_processing AS ss ON ss.base_id = outgoing_campaign_request_base.id AND ss.processing_setting_detail_id = 136
                LEFT JOIN outgoing_campaign_request ON outgoing_campaign_request.id = outgoing_campaign_request_base.outgoing_campaign_request_id
                LEFT JOIN compaign_quize_processing ON compaign_quize_processing.base_id = outgoing_campaign_request_base.id AND compaign_quize_processing.processing_setting_detail_id = 13
                WHERE 	outgoing_campaign_request_base.outgoing_campaign_request_id = '$campaign_id' AND outgoing_campaign_request_base.actived = 1");
$data = $db->getResultArray();
$campaign_type = $data['result'][0]['type_id'];

if($campaign_type == 1){
    
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="campaign-AI-'.rand(10000,99999).'.xlsx"');
    header('Cache-Control: max-age=0');
    $objPHPExcel = new PHPExcel(); 
    $objPHPExcel->setActiveSheetIndex(0); 
    
    $rowCount = 2; 
    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ნომერი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'თარიღი და დრო'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'დასახელება'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'სტატუსი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'მოსმენის ხან.'); 
    foreach($data['result'] AS $item){
    
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $item['phone_number']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $item['call_date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $item['number_1']);  
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $item['status2']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $item['talk_time']); 
    
        $rowCount++; 
    } 
    $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold( true );
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
else if($campaign_type == 2){
    
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="campaign-'.rand(10000,99999).'.xlsx"');
header('Cache-Control: max-age=0');
$objPHPExcel = new PHPExcel(); 

    $objPHPExcel->setActiveSheetIndex(0); 
    
    $rowCount = 2; 
    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'თარიღი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ნომერი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'განმცხადებელი/კოპანია'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'კომენტარი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ოპერატორი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'საუბ.ხან'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'სტატუსი'); 
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'პასუხი'); 
    foreach($data['result'] AS $item){
    
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $item['call_date']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $item['phone_number']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $item['client']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $item['value']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $item['user_name']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $item['talk_time']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $item['status']); 
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $item['answer']); 
    
        $rowCount++; 
    } 
    $objPHPExcel->getActiveSheet()->getStyle("A1:H1")->getFont()->setBold( true );
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}

?>