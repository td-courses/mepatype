<?php
require 'includes/classes/class.Mysqli.php';
global $db;
$db      = new dbClass();
$act            = $_REQUEST['act'];
$user_id 		= $_SESSION['USERID'];
$original_name	= $_REQUEST['original'];
$type		    = $_REQUEST['ext'];
$new_name		= $_REQUEST['newName'];
$chatID	        = $_REQUEST['chatID'];

$new = $new_name.'.'.$type;
if($act == 'project_docs'){
    if (0 < $_FILES['file']['error']) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {
        if(move_uploaded_file($_FILES['file']['tmp_name'], 'media/uploads/documents/' . $new_name.'.'.$type))
        {
            echo 'uploaded';
        }
        else{
            echo 'error';
        }
    }
}
else if($act == 'upload_auto_dialer'){
    $parameter_id = $_REQUEST['parameter_id'];
    if (0 < $_FILES['file']['error']) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {
        if(move_uploaded_file($_FILES['file']['tmp_name'], 'media/upload_auto_dialer/' . $new_name.'.'.$type))
        {
            $query = "INSERT INTO file(`user_id`,`file_date`,`name`,`rand_name`) VALUES('$user_id',NOW(),'$original_name','$new')";
            $db->setQuery($query);
            $db->execQuery();

            $query = "UPDATE outgoing_campaign_settings_parameters SET welcome_file='$new', welcome_file_original='$original_name' WHERE id='$parameter_id'";
            $db->setQuery($query);
            $db->execQuery();

            echo $original_name;
        }
    }
}
else{
    if (0 < $_FILES['file']['error']) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {
        if(move_uploaded_file($_FILES['file']['tmp_name'], 'media/uploads/chats/' . $new_name.'.'.$type))
        {
            $query = "
            
            INSERT INTO chat_files(`file`,`file_orig`,`datetime`,`chat_id`,`sender_id`,`actived`) VALUES('$new','$original_name',NOW(),'$chatID','$user_id','1')
            
            ";
            $db->setQuery($query);
            $db->execQuery();
        }
    }
}
?>