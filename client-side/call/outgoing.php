<html>

<head>
    <link rel="stylesheet" href="vendors/apex-charts/apexcharts.css">
    <script src="vendors/apex-charts/apex.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script>
        kendo.pdf.defineFont({
            "DejaVu Sans": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
            "DejaVu Sans|Bold": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
            "DejaVu Sans|Bold|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "DejaVu Sans|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "WebComponentsIcons": "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
        });
    </script>

    <script>
        var aJaxURL = "server-side/call/outgoing.action.php";
        var Comunications_URL = "server-side/call/incomming.action.php";
        var CustomSaveURL = "server-side/view/automator.action.php";

        function LoadKendoTable(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'base_grid';
            var actions = '<button style="float:right;" id="outgoing_excel_export">ექსპორტი EXCEL</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 11;
            var columnsSQL = ["id:string", "call_date:date", "mom_auth:string", "operator:string", "department:string", "project:string", "user_name:string", "phone:string", "cal_stat:string", "shin_status:string", "gender:string"];
            var columnGeoNames = ["ID", "დარეკვის თარიღი", "მომართვის ავტორი", "ოპერატორი", "უწყება", "პროექტი", "მომართვის სტატუსი", "ტელეფონი", "ჩანაწერი", "შინაარს. სტატუსი", "სქესი"];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0, '', '', '', true, false, true, [2, 6, 10]);
        }
        $(document).on('click', '#outgoing_excel_export', function() {
            //         var start_date  = $("#start_date").val() + " 00:00:00";
            //         var end_date    = $("#end_date").val() + " 23:59:59";
            //         /* var call_sources = [];
            //         $('#call_sources option:selected').toArray().map(c => call_sources.push(c.value)); */
            // 
            //         var hidden = "&start_date="+start_date+"&end_date="+end_date;
            //         var win = window.open('includes/communication_export.php?act=export_outgoing'+hidden, '_blank');
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("base_grid", "გამავალი", [0, 8]);
                $("#loading1").hide();
            }, 200);
        });
        //new sms textarea key up
        $(document).on("keyup", "#newSmsText", function() {

            charCounter(this, "#smsCharCounter");

        });

        function charCounter(checkobj, updateobj) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(updateobj).data("limit");

            $(updateobj).val(`${textLength}/${charLimit}`);

            if (textLength === charLimit) {
                $(checkobj).blur();
            }

        }
        $(document).on('click', '.dialog_input_tabs li', function() {
            ///SWITCHING TABS HERE///
            var field_id = $(this).parent().attr('id');
            var tab_id = $(this).attr('data-id');
            $('#' + field_id + ' li').removeAttr('aria-selected');
            $(this).attr('aria-selected', 'true');
            /////////////////////////
            $("." + field_id).css('display', 'none');

            $("." + field_id + "[data-tab-id='" + tab_id + "']").css('display', 'block');

        });
        $(document).on("change", "#region", function() {
            console.log("region changeshi shemovarda");
            param = new Object();
            param.act = "get_region";
            param.region_id = $("#region").val();
            if(param.region_id == 0){
                $("#municip > option:selected").removeAttr("selected");
                $("#municip > option[value=0]").attr("selected","selected");
                $("#municip").trigger("chosen:updated");

                $("#temi").html("");
                $("#temi").trigger("chosen:updated");
                
                $("#village").html("");
                $("#village").trigger("chosen:updated");
            }
            else{
                $.ajax({
                    url: CustomSaveURL,
                    data: param,
                    success: function(data) {
                        $("#municip").html(data.page);
                        $("#municip").trigger("chosen:updated");

                    }
                });
            }

        });

        $(document).on("click", "#municip_chosen", function() {
            if($("#region option:selected").val() == 0 && $("#municip option:selected").val() == 0){
                param = new Object();
                param.act = "get_municip";
                param.id = $("#municip").val();
                $.ajax({
                    url: CustomSaveURL,
                    data: param,
                    success: function(data) {
                        $("#municip").html(data.page);
                        $("#municip").trigger("chosen:updated");
                    }
                });
            }
        });

        $(document).on("change", "#municip", function() {
            param = new Object();
            param.act = "get_municipipalitet";
            param.municipipalitet_id = $("#municip").val();
            param.parent_id = $("#municip > option:selected").attr("data-parent");
            if($("#region option:selected").val() != $("#municip > option:selected").attr("data-parent")){
                $("#region option").removeAttr("selected");
                $("#region option[value='"+param.parent_id+"'").prop("selected",true);
                $('#region').chosen("destroy").chosen();
            }
            if(param.municipipalitet_id == 0){
                $("#temi").html("");
                $("#temi").trigger("chosen:updated");
                $("#village").html("");
                $("#village").trigger("chosen:updated");
            }
            else{
                $.ajax({
                    url: CustomSaveURL,
                    data: param,
                    success: function(data) {
                        $("#temi").html(data.page);
                        $("#temi").trigger("chosen:updated");
                    }
                });            
            }
        });

        $(document).on("change", "#temi", function() {
            param = new Object();
            param.act = "region_4";
            param.cat_id = $("#temi").val();
            $.ajax({
                url: CustomSaveURL,
                data: param,
                success: function(data) {
                    $("#village").html(data.page);
                    $("#village").trigger("chosen:updated");
                }
            });
        });
        $(document).on("change", "#dep_0", function() {
            param = new Object();
            param.act = "dep_1";
            param.cat_id = $("#dep_0").val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_project_0").html(data.page);
                    $("#dep_project_0").trigger("chosen:updated");
                    if ($("#dep_project_0 option:selected").val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $("#dep_project_0").val();
                        $.ajax({
                            url: Comunications_URL,
                            data: param,
                            success: function(data) {
                                $("#dep_sub_project_0").html(data.page);
                                $("#dep_sub_project_0").trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });
        $(document).on('change', '#dep_sub_project_0', function() {
            param = new Object();
            param.act = "dep_4";
            param.cat_id = $("#dep_sub_project_0").val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_project_0").html(data.page.data);
                    $("#dep_project_0").trigger("chosen:updated");
                    $("#dep_0").html(data.page.data1);
                    $("#dep_0").trigger("chosen:updated");
                }
            });
        });
        $(document).on("change", "#dep_project_0", function() {
            param = new Object();
            param.act = "dep_3";
            param.cat_id = $("#dep_project_0").val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_0").html(data.page.data3);
                    $("#dep_0").trigger("chosen:updated");

                    $("#dep_sub_project_0").html(data.page.data2);
                    $("#dep_sub_project_0").trigger("chosen:updated");
                }
            });
            var project_id = $("#dep_project_0").val();
            $.ajax({
                url: Comunications_URL,
                data: "act=calculate_fields&project_id=" + project_id,
                success: function(data) {
                    $("#get_dep_additional .field_notifier").remove();
                    $("#get_dep_additional").append('<div class="field_notifier"><p><span id="field_done">' + data.r_fields + '</span>/<span id="field_no_done">' + data.fields_cc + '</span></p></div>');
                    if (data.r_fields == data.fields_cc) {
                        $("#get_dep_additional .field_notifier").css('background-color', '#21ff00');
                    }
                }
            });
        });
        $(document).ready(function() {
            var kendo = new kendoUI();

            GetDate('start_date');
            GetDate('end_date');

            kendo.kendoSelector('operators', 'server-side/call/incomming.action.php', 'get_operators', "ყველა ოპერატორი");
            kendo.kendoSelector('compaign_types', 'server-side/call/outgoing.action.php', 'get_compaign_types', "კამპ.ტიპი");

            var hidden = getFilterResult();

            LoadKendoTable(hidden);
            loadtabs(activep = "", activech = "");
            get_tabs_count();

        });

        function getFilterResult(pos) {
            var operators = $("#operators").data("kendoDropDownList");
            var selectedIndex = operators.select();
            var operators_dItem = operators.dataItem(selectedIndex);

            var all_calls = $("#compaign_types").data("kendoDropDownList");
            var selectedIndex = all_calls.select();
            var all_calls_dItem = all_calls.dataItem(selectedIndex);

            if (operators_dItem == undefined) {
                operators_dItem = new Object();
                operators_dItem.id = '';
            }

            if (pos == undefined) {
                pos = '';
            }
            if (all_calls_dItem == undefined) {
                all_calls_dItem = new Object();
                all_calls_dItem.id = '';
            }

            var hidden = "&start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + "&filter_operator=" + operators_dItem.id + "&filter_all_calls=" + all_calls_dItem.id + pos;

            return hidden;
        }
        $(document).on("click", "#loadtable", function() {
            var hidden = getFilterResult();

            LoadKendoTable(hidden)
        });

      
        // ------------------------------ START CONTRACT SERVICE ------------------------------------ 
        $(document).on("click", ".for_service_contract", function() {
            if($("#piradi_nomeri--129--1").val() == "" && $("#saidentifikacio_kodi___940--134--1").val() == "")
                alert("გთხოვთ შეავსოთ პირადი ნომერი ან საიდენტიფიკაციო კოდი ინფორმაციის მოსაძებნად");
            else{
                //buttons
                let buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                };

                $("#contract_dialog").html("<div id='kendo_contract_list'></div>");
                GetDialog("contract_dialog", 1300, "auto", buttons, 'center top');

                var hidden = ($("#piradi_nomeri--129--1").val() != "") ? "&code=" +  $("#piradi_nomeri--129--1").val() : "&code=" + $("#saidentifikacio_kodi___940--134--1").val();
                console.log(hidden);
                LoadKendoTable_contracts(hidden);

            }
        });

        $(document).on("dblclick", "#kendo_contract_list tr.k-state-selected", function() {
            // get data from selected row of kendo table
            var grid = $("#kendo_contract_list").data("kendoGrid");
            var dItem = grid.dataItem($(this));
            var data = {
                contract_code:  dItem.contract_code,
                service:        dItem.service,
                price:          dItem.price,
                balance:        dItem.balance,
                land_area:      dItem.land_area,
                fill_date:      dItem.fill_date,
                service_center: dItem.service_center,
                service_id:     dItem.service_id
            }

            $("#contract_dialog").dialog("close");
            $("#contract_dialog").html("");
            console.log(data);

            // write into the fields of contract fieldset 
            $("#_saxelshekrulebo_fartobi___383--148--1").val(data.land_area);
            $("#xelshekrulebis_kodi___221--145--1").val(data.contract_code);
            $("#_xelsh__shevsebis_tarighi--149--5").val(data.fill_date);
            $("#servis_centri___314--151--1").val(data.service_center);
            $("#xelshekrulebis_tanxa___649--147--1").val(data.price);
            $("#balansi___781--150--1").val(data.balance);
            $("#servisi___259--146--8").val(data.service_id);
            $("#servisi___259--146--8").trigger("chosen:updated");
        });

        $(document).on("click", "#contracts_excel", function(){
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_contract_list","კონტრაქტების სია",[]);
                $("#loading1").hide();
            }, 200)
        });

        function LoadKendoTable_contracts(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var aJaxURL = "server-side/call/incomming.action.php";
            var gridName = 'kendo_contract_list';
            var actions = '<button style="float:right;" id="contracts_excel">EXCEL რეპორტი</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 20;
            var columnsCount = 8;
            var columnsSQL = [
                "contract_code:string",
                "service:string",
                "price:string",
                "balance:string",
                "land_area:string",
                "fill_date:string",
                "service_center:string",
                "service_id:string"
            ];
            var columnGeoNames = [
                "ხელშეკრულების კოდი",
                "სერვისი",
                "ხელშეკრულების თანხა",
                "ბალანსი",
                "სახელშეკრულებო ფართობი",
                "ხელშ. შევსების თარიღი",
                "სერვის ცენტრი",
                "service_id"
            ];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0];
            var selectors              = [0, 0, 0, 0, 0, 0, 0, 0];

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END

            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list_contracts', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0, '', '', '', true, false, true,[7]);

        }

        
        // ------------------------------ END CONTRACT SERVICE ------------------------------------



        // service
        // $(document).on('click', '#service_ab', function() {
        //     check_beneficiary($("#abonentis_kodi___133--142--1").val());
        // });
        // $(document).on('click', '.service_icon', function() {
        //     var dat;
        //     var sevice_form = $(this).attr('id');
        //     var phone = $("#telefoni___183--124--1").val();
        //     var code = $("#abonentis_kodi___133--142--1").val();
        //     var pn = $("#piradi_nomeri--129--1").val();

        //     if (sevice_form == 'service_phone') {
        //         dat = 'act=abonent_sevice&phone=' + phone;
        //     } else if (sevice_form == 'service_ab') {
        //         dat = 'act=abonent_sevice&ab_num=' + code;
        //     } else if (sevice_form == 'service_pn') {
        //         dat = 'act=abonent_sevice&pn=' + pn;
        //     }
        //     $.ajax({
        //         url: Comunications_URL,
        //         data: dat,
        //         success: function(data) {
        //             var resp_code = data.code;
        //             if (resp_code == 2) {
        //                 var abonenti = data.abonent;
        //                 var ab_type = abonenti.type;

        //                 if (ab_type == 1) {
        //                     $("#piradi_nomeri--129--1").val(abonenti.legal_code);
        //                     $("#saxeli___899--127--1").val(abonenti.name);
        //                     $("#gvari___921--128--1").val(abonenti.lastname);
        //                     $("#E-mail___593--143--1").val(abonenti.email);
        //                     $("#abonentis_kodi___133--142--1").val(abonenti.code);

        //                     if (abonenti.sex == 1) {
        //                         $("#sqesi___949-2--130--6").prop('checked', true);
        //                     } else {
        //                         $("#sqesi___949-1--130--6").prop('checked', true);
        //                     }
        //                 } else {

        //                 }

        //             } else if (resp_code == 0) {
        //                 alert(data.message);
        //             } else if (resp_code == 1) {
        //                 alert('აბონენტი ვერ მოიძებნა');
        //             }
        //         }
        //     })
        // });



        function loadtabs(activep = "", activech = "") {

            obj = new Object;
            obj.act = "get_tabs";
            $.ajax({
                url: aJaxURL,
                data: obj,
                success: function(data) {
                    $("#tabs1").html(data.page);
                    GetTabs("tab_0");
                    if (activep == "") {
                        $("[id^=t_]").first().click();
                        $("[id^=t_3]").click();
                    } else {
                        $(activep).click();
                        if (activech != "") {
                            $(activech).click();
                        }
                    }
                    //quantity_ajax();
                }
            })
        }

        $(document).on("click", "#tabs1 li", function() {
            if (!$(this).hasClass("child")) { // parent
                deactive("li[id^='t_']");
                $('div[id^="tab_"]').css("display", "inline-block");
                $('div[id^="tab_"]').css("position", "relative");
                GetTabs("tab_" + $(this).attr("name"));
                active("#t_" + $(this).attr("name"));
                $('div[id^="tab_"]').not(".main_tab").not('.tab_' + $(this).attr("name")).css("display", "none");
                active("#tab_" + $(this).attr("name") + " ul li:first-child");
                var obj = new Object;
                obj.parent = $(this).attr("name");
                obj.child = $("#tab_" + $(this).attr("name") + " ul li:first-child").attr("name");
                obj.start_date = $('#start_date').val();
                obj.end_date = $('#end_date').val();
                //quantity_ajax();
                $("#activeparent").val("" + obj.parent);
                $("#activechild").val("" + obj.child);
                //LoadTable('index',colum_number,main_act,change_colum_main,obj);
            } else { // child  

                deactive("li[id^='t_']");
                active("#" + $(this).attr("id"));
                active("#t_" + $(this).attr("id_select"));

                var obj = new Object;
                obj.parent = $(this).attr("id_select");
                obj.child = $(this).attr("name");
                obj.start_date = $('#start_date').val();
                obj.end_date = $('#end_date').val();
                //quantity_ajax();
                $("#activeparent").val("" + obj.parent);
                $("#activechild").val("" + obj.child);

                var pos = "&parent=" + obj.parent + "&child=" + obj.child;
                var hidden = getFilterResult(pos);
                LoadKendoTable(hidden);

            }
        });
        $(document).on('click', '#save-automated-dialog, .save-incomming-main-dialog', function(e) {
            $.ajax({
                url: Comunications_URL,
                data: {
                    'act': 'get_inputs_ids',
                    'setting_id': 4
                },
                success: function(data) {
                    var query = 'act=save-custom&setting_id=4&processing_id=' + $("#hidden_incomming_call_id").val();
                    query += "&region=" + $("#region").val() + "&municip=" + $("#municip").val() + "&temi=" + $("#temi").val() + "&village=" + $("#village").val();
                    if (data.input_ids != '') {
                        var input_ids = data.input_ids;
                        var checkboxes = data.checkboxes;

                        var ready_to_save = 0;

                        input_ids.forEach(function(item, index) {
                            if ($("#" + item + "[data-nec='1']").val() == '' || $("#" + item + "[data-nec='1']").val() == '0') {
                                $("#" + item).css('border', '1px solid red');
                                var transformed = item.replace(/--/g, "__");
                                $("#" + transformed + "_chosen .chosen-single").css('border', '1px solid #e81f1f');
                                ready_to_save++;
                                alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                            } else {
                                if ($("#" + item).val() != '') {
                                    query += "&" + item + '=' + $("#" + item).val();
                                }

                            }

                        });

                        checkboxes.forEach(function(item, index) {
                            $("input[name='" + item + "']:checked").each(function() {
                                var post_data = this.id;
                                var check = post_data.split("--");

                                var check_id = check[0].split("-");

                                query += "&" + this.id + "=" + check_id[1];
                            });
                        });

                        //default deps//
                        var dep = $("#dep_0").val();
                        var dep_project = $("#dep_project_0").val();
                        var dep_sub_project = $("#dep_sub_project_0").val();
                        var dep_sub_type = $("#dep_sub_type_0").val();

                        if (dep_sub_type == '' || dep_sub_type == '0') {
                            ready_to_save++;
                            alert("შეავსეთ მომართვის ტიპი");
                        }

                        query += "&dep_0=" + dep;
                        query += "&dep_project_0=" + dep_project;
                        query += "&dep_sub_project_0=" + dep_sub_project;
                        query += "&dep_sub_type_0=" + dep_sub_type;

                        ////////////////

                        let dep_array = [];
                        let dep_project_array = [];
                        let dep_sub_project_array = [];
                        let dep_sub_type_array = [];

                        $(".dep").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_array.push("&dep_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_project").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_project_array.push("&dep_project_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_sub_project").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_sub_project_array.push("&dep_sub_project_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_sub_type").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            var vall = $(this).val();
                            if (vall == '' || vall == '0') {
                                ready_to_save++;
                                alert("შეავსეთ მომართვის ტიპი");
                                return false;
                            }
                            dep_sub_type_array.push("&dep_sub_type_" + rand_number + "=" + $(this).val());
                        });

                        /* console.log(dep_array);
                        console.log(dep_project_array);
                        console.log(dep_sub_project_array);
                        console.log(dep_sub_type_array); */

                        dep_array.forEach(function(name, index) {
                            query += dep_array[index] + dep_project_array[index] + dep_sub_project_array[index] + dep_sub_type_array[index];
                        });




                        query += "&dialogtype=" + $("#dialogType").val();
                        if (ready_to_save == 0) {
                            $.ajax({
                                url: CustomSaveURL,
                                type: "POST",
                                data: query,
                                dataType: "json",
                                success: function(data) {
                                    alert('ინფორმაცია შენახულია');
                                    $("#base_grid").data("kendoGrid").dataSource.read();
                                    $.ajax({
                                        url: Comunications_URL,
                                        data: "act=chat_end&chat_source=" + $("#chat_source").val() + "+&chat_id=" + $("#chat_original_id").val(),
                                        success: function(data) {
                                            if (typeof data.error != "undefined") {
                                                if (data.error != "") {
                                                    // alert(data.error);
                                                    //alert("Caucasus Jixv");
                                                } else {
                                                    $("#chat_close_dialog").dialog("close");
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        console.log(query);
                    }
                }
            });
        });

        function get_tabs_count() {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=get_tabs_numbers",
                dataType: "json",
                success: function(data) {
                    var numbers = data.numbers

                    if (numbers != '') {

                        numbers.forEach(function(num) {
                            $('#input_' + num.tree_id + " x").text(num.count);
                        });
                    }
                }
            });
        }
        $(document).on("click", ".open_dialog", function() {
            var number = $(this).attr("number");
            var ipaddres = $(this).attr("ipaddres");
            var ab_pin = $(this).attr("ab_pin");
            var check_save_chat = $(this).attr("check_save_chat");
            var source = $(this).attr("data-source");
            var calltype = $(this).attr("data-calltype");
            if ($('#imchat').val() == 1) {

            }
            if (number != "") {
                run(number, ipaddres, ab_pin, check_save_chat, source, calltype);
            }
        });


        function active(id) {
            $(id).attr("aria-selected", "true");
            $(id).css("cursor", "grabbing");
        }

        function deactive(id) {
            $(id).attr("aria-selected", "false");
            $(id).css("cursor", "pointer");
        }



        //  -------------------------------------------------------------------


        $(document).on("click", ".play_audio_complate", function() {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if ($call_date > $limit_date) {
                link = 'http://172.16.0.80:8000/' + link;
            } else {
                link = 'http://172.16.0.80:8000/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    class: "cancel-audio",
                    click: function() {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto", btn);
            console.log(link)
            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
            // $(".download").css( "background","#2dc100" );
            // $(".download4").css( "background","#2dc100" );
            // $(this).css( "background","#FF5555" );
            var file_col = $(this).parent()
            $(file_col).css('background-color', "#ff5555")


        });
        $(document).on('click', '#save-dialog-out-automated', function(e) {
            $("#add-edit-form-comunications").remove();
            $.ajax({
                url: Comunications_URL,
                data: {
                    'act': 'get_inputs_ids',
                    'setting_id': 4
                },
                success: function(data) {
                    var query = 'act=save-custom&setting_id=4&processing_id=' + $("#hidden_incomming_call_id").val();
                    query += "&region=" + $("#region").val() + "&municip=" + $("#municip").val() + "&temi=" + $("#temi").val() + "&village=" + $("#village").val();
                    if (data.input_ids != '') {
                        var input_ids = data.input_ids;
                        var checkboxes = data.checkboxes;

                        var ready_to_save = 0;

                        input_ids.forEach(function(item, index) {
                            if ($("#" + item + "[data-nec='1']").val() == '' || $("#" + item + "[data-nec='1']").val() == '0') {
                                $("#" + item).css('border', '1px solid red');
                                var transformed = item.replace(/--/g, "__");
                                $("#" + transformed + "_chosen .chosen-single").css('border', '1px solid #e81f1f');
                                ready_to_save++;
                                alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                            } else {
                                if ($("#" + item).val() != '') {
                                    query += "&" + item + '=' + $("#" + item).val();
                                }

                            }

                        });

                        checkboxes.forEach(function(item, index) {
                            $("input[name='" + item + "']:checked").each(function() {
                                var post_data = this.id;
                                var check = post_data.split("--");

                                var check_id = check[0].split("-");

                                query += "&" + this.id + "=" + check_id[1];
                            });
                        });

                        //default deps//
                        var dep = $("#dep_0").val();
                        var dep_project = $("#dep_project_0").val();
                        var dep_sub_project = $("#dep_sub_project_0").val();
                        var dep_sub_type = $("#dep_sub_type_0").val();

                        if (dep_sub_type == '' || dep_sub_type == '0') {
                            ready_to_save++;
                            alert("შეავსეთ მომართვის ტიპი");
                        }

                        query += "&dep_0=" + dep;
                        query += "&dep_project_0=" + dep_project;
                        query += "&dep_sub_project_0=" + dep_sub_project;
                        query += "&dep_sub_type_0=" + dep_sub_type;

                        ////////////////

                        ////////////////

                        let dep_array = [];
                        let dep_project_array = [];
                        let dep_sub_project_array = [];
                        let dep_sub_type_array = [];

                        $(".dep").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_array.push("&dep_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_project").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_project_array.push("&dep_project_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_sub_project").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_sub_project_array.push("&dep_sub_project_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_sub_type").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            var vall = $(this).val();
                            if (vall == '' || vall == '0') {
                                ready_to_save++;
                                alert("შეავსეთ მომართვის ტიპი");
                                return false;
                            }
                            dep_sub_type_array.push("&dep_sub_type_" + rand_number + "=" + $(this).val());
                        });

                        /* console.log(dep_array);
                        console.log(dep_project_array);
                        console.log(dep_sub_project_array);
                        console.log(dep_sub_type_array); */

                        dep_array.forEach(function(name, index) {
                            query += dep_array[index] + dep_project_array[index] + dep_sub_project_array[index] + dep_sub_type_array[index];
                        });
                        query += "&dialogtype=" + $("#dialogType").val();
                        if (ready_to_save == 0) {
                            $.ajax({
                                url: CustomSaveURL,
                                type: "POST",
                                data: query,
                                dataType: "json",
                                success: function(data) {
                                    alert('ინფორმაცია შენახულია');
                                    $("#kendo_incomming_table").data("kendoGrid").dataSource.read();
                                    $.ajax({
                                        url: Comunications_URL,
                                        data: "act=chat_end&chat_source=" + $("#chat_source").val() + "+&chat_id=" + $("#chat_original_id").val(),
                                        success: function(data) {
                                            if (typeof data.error != "undefined") {
                                                if (data.error != "") {
                                                    // alert(data.error);
                                                    //alert("Caucasus Jixv");
                                                } else {
                                                    $("#chat_close_dialog").dialog("close");
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        console.log(query);
                    }
                }
            });
        });
        $(document).on("dblclick", "#base_grid tr.k-state-selected", function() {
            var grid = $("#base_grid").data("kendoGrid");
            var dItem = grid.dataItem($(this));
            $.each($(".ui-dialog"), function(i, e) {
                $(e).remove();
            })
            if(dItem.id != "") {
                $.ajax({
                    url: "server-side/call/outgoing_company.action.php",
                    type: "POST",
                    data: "act=get_processing&id=" + dItem.id + "&type=outgoing",
                    dataType: "json",
                    success: function(data) {
                        $("#get_processing-dialog").html(data.page);

                        var buttons = {

                            done: {
                                text: "შენახვა",
                                class: "save-processing",
                                id: 'save-dialog-out-automated'
                            },
                            cancel: {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function() {
                                    $(this).dialog("close");
                                }
                            }
                        };

                        GetDialog("get_processing-dialog", "625", "auto", buttons, 'ceneter center');


                        $.ajax({
                            url: Comunications_URL,
                            data: {
                                'act': 'get_fieldsets',
                                'setting_id': 4
                            },
                            success: function(data) {

                                $(data.chosen_keys).chosen({
                                    search_contains: true
                                });

                                if (data.datetime_keys != '') {
                                    var datetimes = data.datetime_keys;

                                    datetimes.forEach(function(item, index) {
                                        GetDateTimes(item);
                                    });
                                }

                                if (data.date_keys != '') {
                                    var date = data.date_keys;

                                    date.forEach(function(item, index) {
                                        GetDate(item);
                                    });
                                }
                                if (data.multilevel_keys != '') {
                                    var date = data.multilevel_keys;
                                    var main = '';
                                    var secondary = '';
                                    var third = '';

                                    date.forEach(function(item, index) {
                                        if (index == 0) {
                                            main = item;
                                        } else if (index == 1) {
                                            secondary = item;
                                        } else {
                                            third = item;
                                        }
                                    });
                                }

                                $(document).on("change", "#" + main, function() {
                                    param = new Object();
                                    param.act = "cat_2";
                                    param.selector_id = $("#" + main).attr('id');
                                    param.cat_id = $("#" + main).val();
                                    $.ajax({
                                        url: Comunications_URL,
                                        data: param,
                                        success: function(data) {
                                            $("#" + secondary).html(data.page);
                                            $("#" + secondary).trigger("chosen:updated");

                                            if ($("#" + secondary + " option:selected").val() == 0) {
                                                param = new Object();
                                                param.act = "cat_3";
                                                param.cat_id = $("#" + secondary).val();
                                                $.ajax({
                                                    url: Comunications_URL,
                                                    data: param,
                                                    success: function(data) {
                                                        $("#" + third).html(data.page);
                                                        $("#" + third).trigger("chosen:updated");
                                                    }
                                                });
                                            }
                                        }
                                    });
                                });
                                $(document).on("change", "#" + secondary, function() {
                                    param = new Object();
                                    param.act = "cat_3";
                                    param.selector_id = $("#" + main).attr('id');
                                    param.cat_id = $("#" + secondary).val();
                                    $.ajax({
                                        url: Comunications_URL,
                                        data: param,
                                        success: function(data) {
                                            $("#" + third).html(data.page);
                                            $("#" + third).trigger("chosen:updated");
                                        }
                                    });
                                });

                                $(".dep").chosen();
                                $(".dep_project").chosen();
                                $(".dep_sub_project").chosen();
                                $(".dep_sub_type").chosen();
                                ///BLOCKING CONTRACT FIELDSET BEGIN
                                $(".dialog-tab-20 input").each(function(index) {
                                    $(this).prop('disabled', true);
                                });
                                $("#servisi___259--146--8").prop('disabled', true).trigger("chosen:updated");

                                ///BLOCKING CONTRACT FIELDSET END

                            }
                        });

                        $("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0,#region,#municip,#temi,#village").chosen();
                        $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming2_status_1, #incomming2_status_1_1, #inc_status_a, #company, #space_type, #chat_language, #s_u_status").chosen({
                            search_contains: true
                        });
                        // $('#hidden_base_id').val(data.id)

                        // console.log(data.hisdden_incomming_id)
                        // $('#hidden_incomming_call_id').val(data.hidden_incomming_id);

                        // var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
                        // GetDataTable("project_info_table", 'server-side/call/incomming.action.php', "get_list_info_project", 4, "&id="+$('#hidden_incomming_call_id').val(), 0, dLength, 1, "desc", '', "<'F'lip>");


                        var hidden = '&hidden=' + $('#hidden_incomming_call_id').val();
                        LoadKendoTable__Info_Projects(hidden);

                        setTimeout(function() {
                            var grid = $("#kendo_project_info_table").data("kendoGrid");
                            grid.hideColumn(0);

                            $("#kendo_project_info_table").kendoTooltip({
                                show: function(e) {
                                    if (this.content.text().length > 30) {
                                        this.content.parent().css("visibility", "visible");
                                    }
                                },
                                hide: function(e) {
                                    this.content.parent().css("visibility", "hidden");
                                },
                                filter: "td",
                                position: "right",
                                content: function(e) {
                                    // var dataItem = $("#kendo_project_info_table").data("kendoGrid").dataItem(e.target.closest("tr"));  
                                    // console.log("bottom ", dataItem.info)
                                    var str = e.target[0].innerHTML;
                                    var content = str.replace('</span>', '</span><br />');
                                    console.log(content);
                                    return content;
                                }
                            }).data("kendoTooltip");
                        }, 3000)


                    }
                });
            }
        });
        $(document).on("click", "#add_dep", function() {
            $.ajax({
                url: Comunications_URL,
                data: "act=add_dep",
                success: function(data) {
                    $("#dep_arrival").append(data.data);
                    var chosen = data.chosen_data;
                    $("#dep_" + chosen + ",#dep_project_" + chosen + ",#dep_sub_project_" + chosen + ",#dep_sub_type_" + chosen).chosen();
                }
            });
        });
        $(document).on("click", ".delete_dep", function() {
            var to_delete = $(this).attr('data-id');

            $("#dep_row_" + to_delete).remove();
        });
        $(document).on("change", ".dep", function() {
            var rand_number = $(this).attr('data-id');

            param = new Object();
            param.act = "dep_1";
            param.cat_id = $("#dep_" + rand_number).val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_project_" + rand_number).html(data.page);
                    $("#dep_project_" + rand_number).trigger("chosen:updated");
                    if ($("#dep_project_" + rand_number + " option:selected").val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $("#dep_project_" + rand_number).val();
                        $.ajax({
                            url: Comunications_URL,
                            data: param,
                            success: function(data) {
                                $("#dep_sub_project_" + rand_number).html(data.page);
                                $("#dep_sub_project_" + rand_number).trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });

        $(document).on("change", ".dep_project", function() {
            var rand_number = $(this).attr('data-id');

            param = new Object();
            param.act = "dep_3";
            param.cat_id = $("#dep_project_" + rand_number).val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_sub_project_" + rand_number).html(data.page.data2);
                    $("#dep_sub_project_" + rand_number).trigger("chosen:updated");

                    $("#dep_" + rand_number).html(data.page.data3);
                    $("#dep_" + rand_number).trigger("chosen:updated");
                }
            });

            var project_id = $("#dep_project_" + rand_number).val();
            $.ajax({
                url: Comunications_URL,
                data: "act=calculate_fields&project_id=" + project_id,
                success: function(data) {
                    $("#get_dep_additional_addit[data-id='" + rand_number + "'] .field_notifier").remove();
                    $("#get_dep_additional_addit[data-id='" + rand_number + "']").append('<div class="field_notifier"><p><span id="field_done">' + data.r_fields + '</span>/<span id="field_no_done">' + data.fields_cc + '</span></p></div>');
                    if (data.r_fields == data.fields_cc) {
                        $("#get_dep_additional_addit[data-id='" + rand_number + "'] .field_notifier").css('background-color', '#21ff00');
                    }
                }
            });
        });
    </script>
    <style>
        th [role="listbox"] {
            display: none;
        }

        .ui-tabs .ui-tabs-panel {
            display: block !important;
            border-width: 0;
            padding: 0.2em 0em;
            background: none;

        }

        .ui.tabs li[aria-selected="true"] input {
            background-color: green !important
        }


        .l_inline-tabs div {
            margin: 2px 7px;
        }

        #tabs1 div {
            border: none;
        }

        /* // #tabs1 li {
    //     border: none;
    //     background: none;
    //     outline: none;
    //     border-bottom: 1px solid #222422;
    //     cursor: pointer;
    //     margin-right: 11px;
    //     font-family: BPG;
    //     font-size: 11px;
    //     font-weight: bold;
    //     color: #222422;
    // } */

        /* -SMS DIALOG STYLES- */
        .new-sms-row {
            width: 100%;
            height: auto;
            margin-top: 11.3px;
            box-sizing: border-box;
        }

        .new-sms-row:last-child {
            margin-top: 4px;
        }

        .new-sms-row.grid {
            display: grid;
            grid-template-columns: repeat(2, 50%);
        }

        .nsrg-col {
            padding-right: 10px;
        }

        .nsrg-col:last-child {
            padding-right: 0;
        }

        .new-sms-input-holder {
            width: 100%;
            min-height: 27px;
            display: flex;
        }

        .new-sms-input-holder input {
            height: 18px;
            flex-grow: 1;
            font-size: 11.3px;
        }

        .new-sms-input-holder textarea {
            height: 75%;
            min-height: 20px;
            flex-grow: 1;
            font-size: 11.3px;
            resize: vertical;
        }

        #smsCopyNum {
            /*button: copy*/
            margin: 0 5px;
        }

        #smsTemplate {
            /*button: open template*/
            margin-right: 0;
        }

        #smsNewNum i {
            /*icon: add new number in send new sms child*/
            font-size: 14px;
        }

        #smsCharCounter {
            /*input: sms character counter*/
            width: 55px;
            height: 18px;
        }

        #smsCharCounter {
            /*input: send new sms character counter*/
            position: relative;
            top: 0;
            text-align: center;
        }

        #sendNewSms {
            /*button: send new message action inplement*/
            float: right;
        }

        .empty-sms-shablon {
            width: 100%;
            height: auto;
            padding: 10px 0;
            text-align: center;
            font-family: pvn;
            font-weight: bold;
        }

        #tabs1 {
            height: 76px !important;
            margin-bottom: 25px;
        }

        .label_td {
            width: 170px;
        }

        #info_table input {
            width: 230px !important;
        }

        #info_table select {
            width: 236px !important;
        }

        [aria-describedby="example_info"] {
            width: 100%
        }

        .l_tablist li {
            text-align: center;
        }

        .progress_grey {
            margin: 0;
            background-color: #c5c5c5;
            padding: 1px;
            border-radius: 10px;
            font-weight: 700;
        }

        .progress_yellow {
            margin: 0;
            background-color: #fef400;
            padding: 1px;
            border-radius: 10px;
            font-weight: 700;
        }

        .progress_green {
            margin: 0;
            background-color: #60f3a8;
            padding: 1px;
            border-radius: 10px;
            font-weight: 700;
        }

        .status_pink {
            margin: 0;
            background-color: #e67ef6;
            border-radius: 10px;
            font-weight: 700;
        }

        .status_lightgreen {
            margin: 0;
            background-color: #b8bc06;
            border-radius: 10px;
            font-weight: 700;
        }

        .status_red {
            margin: 0;
            background-color: #cd0d75;
            border-radius: 10px;
            font-weight: 700;
        }

        .status_done {
            margin: 0;
            background-color: #00c76c;
            border-radius: 10px;
            font-weight: 700;
        }

        .chosen-container {
            width: 95% !important;
        }

        .chosen-container#call_type_chosen {
            width: 370px !important;
        }

        #save-schedule {
            float: left;
            background-color: #614701;
            color: #fff;
            border-radius: 3px;
            margin-right: 806px;
        }
    </style>
</head>

<body>
    <div id="tabs" style=" border: 0px solid #aaaaaa;">
        <div class="callapp_filter_body" myvar="1">
            <div style="width: 100%;">
                <table style="width: 50%;">
                    <tr style="float: left;">
                        <td style="padding: 5px 0 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                        </td>
                        <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">-</td>
                        <td style="font-family: BPG; padding: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="font-family: BPG; width: 84px;">
                        </td>
                        <td>
                            <div class="k-content" style="font-family: BPG; margin-right: 10px;  width: 154px;">
                                <input id="operators" style="font-family: BPG !important; width: 100%;" />
                            </div>
                        </td>
                        <td>
                            <div class="k-content" style="font-family: BPG;margin-right: 10px; width: 154px; ">
                                <input id="compaign_types" style="font-family: BPG !important; width: 100%; " />
                            </div>
                        </td>
                        <td>
                            <button id="loadtable">ფილტრი</button>
                        </td>
                    </tr>

                    <tr>
                        <td colspan=6>
                            <!--<div id="tabs1" class="l_inline-tabs" style="display: inline-grid; width: 98%"></div>

                </td>-->
                    </tr>
                    <!-- <tr>
                    <td colspan=8 style="padding: 0 0 5px;text-align: center;">
                        <button style="" id="create_campaign">კამპანიის შექმნა</button>
                    </td>
                </tr> -->

                </table>

            </div>
        </div>

        <div class="l_responsible_fix" style="width: calc(100% - 250px)">
            <div id="base_grid"></div>
        </div>

        <!-- jQuery Dialog -->
        <div id="add-edit-form" class="form-dialog" title="კამპანია"></div>
        <div id="get_processing-dialog" class="form-dialog" title="დამუშავება"></div>
        <div id="contract_dialog" class="form-dialog" title="კონტრაქტების სია"></div>
        <div id="campaign-select-form" class="form-dialog" title="კამპანიის არჩევა"></div>
    </div>
    <div id="loading1">
        <p><img src="media/images/loader.gif" /></p>
    </div>
</body>
<style>
    #loading1 {
        z-index: 999;
        top: 45%;
        left: 45%;
        position: absolute;
        display: none;
        padding-top: 15px;
    }

    .container {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
</style>

</html>