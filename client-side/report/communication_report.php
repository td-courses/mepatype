<head>
<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
    <script src="js/jszip.min.js"></script>
    
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<style type="text/css">
		#callapp_footer{
			z-index:10!important;
			margin-left: 85px;
		}
        input,
        textarea {
            caret-color: #266dbb;
        }
		#loading1{
            z-index:999;
            top:45%;
            left:45%;
            position: absolute;
            display: none;
            padding-top: 15px;
        }
        @media screen and (max-width: 1400px) {
            html{
                zoom:90%;
            }
        }
		caption{
		    margin: 0;
			padding: 0;
			background: #f3f3f3;
			height: 40px;
			line-height: 40px;
			text-indent: 2px;
			font-family: "Trebuchet MS", Trebuchet, Arial, sans-serif;
			font-size: 140%;
			font-weight: bold;
			color: #000;
			text-align: left;
			letter-spacing: 1px;
			border-top: dashed 1px #c2c2c2;
			border-bottom: dashed 1px #c2c2c2;
		}

        .k-content{
            margin-right: 10px;
        }
        #kendo_project_info_table tr th:nth-child(1),
        #kendo_project_info_table tr td:nth-child(1){
            /* display: none; */
        }

        #kendo_project_info_table tr th:nth-child(2), 
        #kendo_project_info_table tr td:nth-child(2), 
        #kendo_project_info_table tr th:nth-child(3),
        #kendo_project_info_table tr td:nth-child(3){
            text-align: center;
            width: 100px;
        }

        #kendo_project_info_table tr th:nth-child(3){
            text-align: center;
        }

        #kendo_project_info_table tr td:nth-child(4){
            white-space: nowrap;
            
        }

        #kendo_project_info_table {
            height: 300px !important;
        }
    </style>
	<script type="text/javascript">
		var aJaxURL		= "server-side/report/communication_report.action.php";		//server side folder url											//tabs name
		var fName		= "add-edit-form";										//form name
		
		$(document).ready(function () {
			GetDate("start_time");
			GetDate("end_time");
            var start_date  = $("#start_time").val() + " 00:00:00";
            var end_date    = $("#end_time").val() + " 23:59:59";
            var hidden = "&start_date="+start_date+"&end_date="+end_date;
            LoadKendo_report(hidden);
            var kendo = new kendoUI();
            kendo.kendoMultiSelector('call_type', 'server-side/report/communication_report.action.php', 'get_call_source', 'კომ.არხი');
            kendo.kendoMultiSelector('department', 'server-side/report/communication_report.action.php', 'get_department', 'უწყება');
            kendo.kendoMultiSelector('project', 'server-side/report/communication_report.action.php', 'get_project', 'პროექტი');
            kendo.kendoMultiSelector('region', 'server-side/report/communication_report.action.php', 'get_region', 'რეგიონი');
            kendo.kendoMultiSelector('municip', 'server-side/report/communication_report.action.php', 'get_municip', 'მუნიციპალიტეტი');
            kendo.kendoMultiSelector('status', 'server-side/report/communication_report.action.php', 'get_status', 'მომ.სტატუსი');
            kendo.kendoMultiSelector('operator', 'server-side/report/communication_report.action.php', 'get_operator', 'ოპერატორი');
		});
        function LoadKendo_report(hidden){
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 				    'comm_report';
            var actions = 				    '<button id="communication_excel_export">EXCEL რეპორტი</button>';
            var editType = 		 		    "popup"; // Two types "popup" and "inline"
            var itemPerPage = 	 		    10;
            var columnsCount =			    11;
            var columnsSQL = 				["id:string","datatime:date","phone:string","author:string","department:string","project:string","region:string","municip:string","operator:string","status:string","type:string"];
            var columnGeoNames = 		    ["ID","თარიღი","ტელეფონი","მომ.ავტორი","უწყება","პროექტი","რაიონი","მუნიციპალიტეტი","ოპერატორი","მომ.სტატუსი","მომ.ტიპი"];

            var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
            var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL,'get_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden,1, '', '', '', false, false, false);
        }
        $(document).on('click','#get_report',function(){
            var start_date  = $("#start_time").val() + " 00:00:00";
            var end_date    = $("#end_time").val() + " 23:59:59";
            var call_type = [];
            $('#call_type option:selected').toArray().map(c => call_type.push(c.value));
            var department = [];
            $('#department option:selected').toArray().map(c => department.push(c.value));
            var project = [];
            $('#project option:selected').toArray().map(c => project.push(c.value));
            var region = [];
            $('#region option:selected').toArray().map(c => region.push(c.value));
            var municip = [];
            $('#municip option:selected').toArray().map(c => municip.push(c.value));
            var status = [];
            $('#status option:selected').toArray().map(c => status.push(c.value));
            var operator = [];
            $('#operator option:selected').toArray().map(c => operator.push(c.value));

            var hidden = "&start_date="+start_date+"&end_date="+end_date+"&department="+department+"&project="+project+"&region="+region+"&municip="+municip+"&status="+status+"&operator="+operator+"&call_type="+call_type;
            LoadKendo_report(hidden);
        });
        $(document).on('click','#communication_excel_export', function(){
            //             var start_date  = $("#start_time").val() + " 00:00:00";
                //             var end_date    = $("#end_time").val() + " 23:59:59";
                //             var call_type = [];
                //             $('#call_type option:selected').toArray().map(c => call_type.push(c.value));
                //             var department = [];
                //             $('#department option:selected').toArray().map(c => department.push(c.value));
                //             var project = [];
                //             $('#project option:selected').toArray().map(c => project.push(c.value));
                //             var region = [];
                //             $('#region option:selected').toArray().map(c => region.push(c.value));
                //             var municip = [];
                //             $('#municip option:selected').toArray().map(c => municip.push(c.value));
                //             var status = [];
                //             $('#status option:selected').toArray().map(c => status.push(c.value));
                //             var operator = [];
                //             $('#operator option:selected').toArray().map(c => operator.push(c.value));
                // 
                //             var hidden = "&start_date="+start_date+"&end_date="+end_date+"&department="+department+"&project="+project+"&region="+region+"&municip="+municip+"&status="+status+"&operator="+operator+"&call_type="+call_type;
                //             var win = window.open('includes/communication_export.php?act=export_campaign_data_excel'+hidden, '_blank');
                            

            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("comm_report","ჯამური რეპორტი",[0]);
                $("#loading1").hide();
            }, 200);
        });

    </script>
    
</head>

<body>
<div id="loading1">
    <p><img src="media/images/loader.gif" /></p>
</div>
<div id="tabs" >
<div class="l_responsible_fix">
	<div id="rest">
        <div id="button_area">
            <div class="left" style="width: 200px;">
                <label for="search_start" class="left" style="margin: 9px 0 0 9px; font-size: 12px;">დასაწყისი</label>
                <input type="text" name="search_start" id="start_time" value="" class="inpt right" style="width: 110px;"/>
            </div>
            <div class="left" style="width: 210px;">
                <label for="search_end" class="left" style="margin: 9px 0 0 9px; font-size: 12px;">დასასრული</label>
                <input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 110px;"/>
            </div>	
        </div>
        <div style="display:flex;">
            <div class="k-content">
                <select id="call_type" style="width: 180px !important; font-size: 12px;"></select>
            </div>
            <div class="k-content" >
                <select id="department" style="width: 180px !important; font-size: 12px;"></select>
            </div>
            <div class="k-content" >
                <select id="project" style="width: 180px !important; font-size: 12px;"></select>
            </div>
            <div class="k-content" >
                <select id="region" style="width: 180px !important; font-size: 12px;"></select>
            </div>
            <div class="k-content" >
                <select id="municip" style="width: 180px !important; font-size: 12px;"></select>
            </div>
            <div class="k-content" >
                <select id="status" style="width: 180px !important; font-size: 12px;"></select>
            </div>
            <div class="k-content" >
                <select id="operator" style="width: 180px !important; font-size: 12px;"></select>
            </div>
        </div>
       
        <br><br>
        <button id="get_report">რეპორტის ჩვენება</button>
        <div style="margin-top:20px" id="comm_report"></div>
    </div>
    
</div>
<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="ავტოპარკი">
</div>
</div>
</body>