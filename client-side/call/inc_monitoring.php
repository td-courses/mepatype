<head>
<style type="text/css">
    .high {
    height: 0px;
}

.dialog-grid {
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
}
.dialog-grid textarea,
.dialog-grid input {
    border-radius: 5px;
    border-color: #518bc5 !important;
}

.text {
    font-size: 15px;
    color: #03a2ef;
    font-weight: bold;
}

.tab_grid {
    display: grid;
    grid-auto-flow: column;
    margin: 30px;

}

.tab_grid div {
    padding: 10px;
    border-bottom: 4px solid #AAA;
    cursor: pointer;
}

.active {
    border-bottom: 4px solid #00b306a8 !important;
}

.dialog-grid {
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
}

.dialog-grid textarea,
.dialog-grid input,
.dialog-grid select {
    border-radius: 5px;
    border-color: #518bc5 !important;
    width: 100%;
    resize: vertical;
}

.text {
    font-size: 15px;
    color: #03a2ef;
    font-weight: bold;
}

.grid1 {
    display: grid;
    grid-auto-flow: column;
}

.grid2 {
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 50% 30% 20%;
    width: 95%;
}

.grid3 {
    display: grid;
    grid-auto-flow: row;
    grid-gap: 10px;
}

.grid4 {
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 23% 25% 51%;
    width: 97%;
}

.grid5 {
    display: grid;
    grid-auto-flow: row;
    grid-gap: 5px;
}

#add-edit-form input[disabled] {
    background-color: #46b2e638;
    color: #000;
    text-align: center;
}

.tab {
    font-size: 17px;
    font-family: BPG arial !important;
}

#appeal_button {
    margin-right: 472px;
    width: 120px;
    background: orange;
    color: #FFF;
}

#appeal_button[disabled] {
    background: #999;
}

.tab span {
    margin: 20px;
    background: #00b306a8;
    padding: 1px 6px;
    border-radius: 50%;
    color: #FFF;
}

[i_id='2'] span {
    background: red;
}

.ColVis,
.dataTable_buttons {
    display: none !important;
}


.r_row {
    border-color: #3242a8 !important;
    color: #3242a8 !important;
    margin: -5px;
    padding: 25px !important;
}

.monitor-grid {
    display: grid;
    grid-auto-flow: column;
    width: max-content;
    grid-gap: 20px;
    position: absolute;
    margin-top: 500px;
    text-align: center;
}

.big_card {
    padding: 15px;
    background: white;
    border-radius: 10px;
}

.big_card * {
    font-family: sans-serif;
    margin: 0;
    padding: 0;
}

.big_card .big_card__left {
    grid-column: 1/2;
    display: flex;
    flex-direction: column;
}

.big_card .big_card__left__title {
    color: #808080;
    font-family: pvn;
    font-weight: bold;
    font-size: 15px;
}

.big_card .big_card__left__amount {
    font-size: 46px;
    margin-top: 20px;
}

.big_card .big_card__right {
    grid-column: 2/3;
    text-align: right;
}

.big_card .big_card__right .big_card__total_container {
    margin: 12px;
}

.big_card .big_card__right .big_card__total_container__text {
    font-size: 15px !important;
}

.big_card .big_card__right .big_card__total_container__amount {
    margin-top: 4px;
    font-size: 42px;
}

.big_card .big_card__right .values_container ul {
    list-style: none;
}

.big_card .big_card__right .values_container ul li .value_name * {
    display: inline-block;
}

.big_card .big_card__right .values_container ul li .value_name .circle {
    width: 10px;
    height: 10px;
    background: red;
    border-radius: 100px;
}
.refresh_monitoring_input {
    background-color: 1bc9f8;
    border-radius: 2px;
    display: inline-block;
    cursor: pointer;
    color: #ffffff;
    font-family: arial;
    font-size: 14px;
    border: 0px;
    text-decoration: none;
    text-overflow: ellipsis;
    width: 100%;
}

#dialog-form fieldset input, #inputArea textarea, #dialog-form fieldset textarea, #dialog-form fieldset select{
	height: 28px !important;
}

.ui-dialog-title {
	font-family: pvn;
	color:#1BC9F8;
	font-weight: bold;
}
#right_side {
	right: unset;
}
.k-grid-content{
	overflow-y: auto !important;
}
.ui-dialog-titlebar-close {
	display:none;
}
#monitoring_detail_info table {
  border-collapse: collapse;
}

#monitoring_detail_info table, #monitoring_detail_info th, #monitoring_detail_info td {
    border: 1px solid black;
    vertical-align:middle; 
    text-align:center;

}
.callapp_filter_show {
    float: right;
    width: 100%;
}

.callapp_filter_show button {
    margin-bottom: 10px;
    border: none;
    background-color: white;
    color: #2681DC;
    font-weight: bold;
    cursor: pointer;
}

.callapp_filter_body {
    width: 100%;
    margin-bottom: 0px;
	margin-top: 40px;
}

.callapp_filter_show1 {
    float: right;
    width: 100%;
}

.callapp_filter_show1 button {
    margin-bottom: 10px;
    border: none;
    background-color: white;
    color: #2681DC;
    font-weight: bold;
    cursor: pointer;
}

.callapp_filter_body1 {
    width: 100%;
    padding: 5px;
    margin-bottom: 0px;
}

.callapp_filter_show2 {
    float: right;
    width: 100%;
}
.relative{
    position:relative;
}

.counters {
    display: grid;
    position: absolute;
    top: 200px;
    grid-auto-flow: column;
    grid-gap: 3vw;
}
.attendance{
    width: 23vw;
    height: 30vh;
}

.discipline{
    width: 23vw;
    height: 30vh;
}

.quality{
    width: 23vw;
    height: 30vh;
}
.filter_grid{
    display: grid;
    grid-auto-flow: column;
    width: 400px;
    margin: 15px;
    position: absolute;
    right: 245px;
}
.counter_text{
    font-size: 15px;
    text-align: center;
    margin-top: 15px;
}

 .counter_number {
    font-size: 40px;
    margin-left: 30%;
    margin-top: 15px;

}

.counter_indicator {
    width: 90%;
    height: 5%;
    background: #BBB;
    margin: auto;
    margin-top: 2%;
}

.attendance .counter_indicator div {
    background:#bf520d;
    height:100%;

}

.attendance .counter_text {
    color:#bf520d;
    font-weight: bold;
    font-size: 15px;
}

.quality .counter_indicator div {
    background:#bd0091;
    height:100%;

}

.quality .counter_text {
    color:#bd0091;
    font-weight: bold;
    font-size: 15px;
}

.discipline .counter_indicator div {
    background:#08c2a3;
    height:100%;

}

.discipline .counter_text {
    color:#08c2a3;
    font-weight: bold;
    font-size: 15px;
}

.quality .counter_number {
     color: #bd0091;
}

.attendance .counter_number {
    color: #bf520d;
}

.discipline .counter_number {
    color: #08c2a3;
}

.grid {
    display:grid;
    grid-template-columns: auto auto auto;
    font-size: 9px;
    margin-top: 10px;
    margin-left: 20px;
    grid-gap: 10px;
}
.shadow {
  -webkit-box-shadow: 3px 3px 5px 6px #ccc;  /* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
  -moz-box-shadow:    3px 3px 5px 6px #ccc;  /* Firefox 3.5 - 3.6 */
  box-shadow:         3px 3px 5px 6px #ccc;  /* Opera 10.5, IE 9, Firefox 4+, Chrome 6+, iOS 5 */
}

.callapp_filter_show2 button {
    margin-bottom: 10px;
    border: none;
    background-color: white;
    color: #2681DC;
    font-weight: bold;
    cursor: pointer;
}

.callapp_filter_body2 {
    width: 100%;
    padding: 5px;
    margin-bottom: 0px;
}

.callapp_filter_show3 {
    float: right;
    width: 100%;
}

.callapp_filter_show3 button {
    margin-bottom: 10px;
    border: none;
    background-color: white;
    color: #2681DC;
    font-weight: bold;
    cursor: pointer;
}

.callapp_filter_body3 {
    width: 100%;
    padding: 5px;
    margin-bottom: 0px;
}

.callapp_filter_show4 {
    float: right;
    width: 100%;
}

.callapp_filter_show4 button {
    margin-bottom: 10px;
    border: none;
    background-color: white;
    color: #2681DC;
    font-weight: bold;
    cursor: pointer;
}

.callapp_filter_body4 {
    width: 100%;
    padding: 5px;
    margin-bottom: 0px;
}

.callapp_filter_show_dialog {
    float: right;
    width: 100%;
}

.callapp_filter_show_dialog button {
    margin-bottom: 10px;
    border: none;
    background-color: white;
    color: #2681DC;
    font-weight: bold;
    cursor: pointer;
}

.callapp_filter_body_dialog {
    width: 100%;
    padding: 5px;
    margin-bottom: 0px;
}

.ui-tabs .ui-tabs-panel {
    
    border-width: 0;
    padding: 0.2em 0em;
    background: none;
        
}
.chosen-container {
    width: 95% !important;
}
#tabs1 div {
    border:none;
}
#tabs1 li {
    border: none;
    background: none;
    outline: none;
    border-bottom: 1px solid #222422;
    cursor: pointer;
    margin-right: 11px;
    font-family: pvn;
    font-size: 11px;
    font-weight: bold;
    color: #222422;
}

#tabs1 li[aria-selected="true"] {
    font-size: 13px;
    color: #0b5c9a;
    border-bottom: 2px solid #0b5c9a;
}
#tabs1{
    margin-bottom:15px;
}
    
  
.download {
	background-color:#4997ab;
	border-radius:2px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 98%;
	font-weight: normal !important;
}
 
.download1 {
   
	background: rgb(249, 155, 3);
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
}

.picture{
    height:22px;
    width:20px;
    
	background-image:url(monitoring_volume.png);
	background-size: 19px 25px;
	width: 100%;
            
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #408c99), color-stop(1, #599bb3));
	background:-moz-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-webkit-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-o-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:-ms-linear-gradient(top, #408c99 5%, #599bb3 100%);
	background:linear-gradient(to bottom, #408c99 5%, #599bb3 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#408c99', endColorstr='#599bb3',GradientType=0);
	background-color:#408c99;
}

#save-print{
    position: absolute;
    left: 17px;
}

.myButton:active {
	position:relative;
	top:1px;
}

.hidden{
	display: none;
}
.reqves_hist td{
    padding: 0 13px;
    border: inset 1px #B4D2E2;
    text-align: left;
}	
.reqves_hist td:nth-child(4){
    padding: 0 13px;
    border-right: inset 1px rgba(0, 0, 0, 0);
    text-align: left;
}
.high {
    height: 0px;
}
.callapp_head{
	font-family: pvn;
	font-weight: bold;
	font-size: 20px;
	color: #2681DC;
}
.callapp_head_hr{
	border: 1px solid #2681DC;
}
.callapp_refresh{
    padding: 5px;
    border-radius:3px;
    color:#FFF;
    background: #9AAF24;
    float: right;
    font-size: 13px;
    cursor: pointer;
}
.ui-widget-content {
    border: 1px solid #d0e5fb;
}
#example td:nth-child(14),#example1 td:nth-child(14),#example2 td:nth-child(14),#example3 td:nth-child(14) {
	padding:0px;
}

.monitor-grid {
    display: grid;
    grid-auto-flow: column;
    width: max-content;
    grid-gap: 40px;
    position: absolute;
    margin-top: 390px;
    text-align:center;
}

.monitor-grid .row {
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
    border: 1px solid transparent;
    padding: 15px;
}

.row {
    height: 16px;
    min-height: 80px;
}

.quality_row {
    border-color: #bd0091!important;
}


.quality_row div:first-child{
   color: #bd0091!important;
}

.attendance_row {
    border-color: #bf520d!important;
}

.attendance_row div:first-child{
    color: #bf520d!important;
}

.attendance_row div:first-child{
    color: #bf520d!important;
}

.discipline_row {
    border-color: #08c2a3!important;
}

.discipline_row div:first-child {
    color: #08c2a3!important;
}

.r_row {
    border-color: #3242a8!important;
    color:#3242a8!important;
    margin: -5px;
    padding: 25px!important;
}

.k-grid.k-grid-display-block {
	display: inline-block;
}

.k-grid-toolbar {
    padding: 0px 0px;
}
.k-grid-header{
	padding-right: 0px!important;
}

.communication_chat_style {
    position: inherit;
    left: 263px;
    width: 329px;
    float: left;
    height: 704px;
    margin: 0 0 0 2px !important;
    background-color: #fff !important;
    padding: 15px 0;
    transition: all 1s ease;
    z-index: 999999;
}

.chat_scroll {
    box-sizing: border-box;
    height: 755px;
    /* width: 288px; */
    overflow: auto;
    margin: 0;
    background: #f8f8f8 !important;
    border: 1px solid #dedede !important;
    border-radius: 0px;
    padding: 3px;
}

.chat_top #blocklogo {
    height: 45px;
    line-height: 45px;
    vertical-align: middle;
    /* display: flex; */
    table-layout: fixed;
    width: 99%;
    margin-left: 5px;
    background: #f4fffa;
    border: solid 1px #d6d6d6;
    /* margin-top: 5px; */
}

</style>
<script type="text/javascript">
		var aJaxURL		 = "server-side/call/inc_monitoring.action.php";
		var aJaxURL_inc	 = "server-side/call/incomming.action.php";	
		var aJaxURLsport = "server-side/call/action/action.action4.php"; 
		
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var tName	          = "table_";										//table name
		var fName	          = "add-edit-form";		
		var file_name         = '';
		var rand_file         = '';
	    var dialog            = "add-edit-form";
	    var colum_number      = 7;
	    var main_act          = "get_list_gasanawilebeli";
		var user_ID = '<?php echo $_SESSION['USERID']; ?>';
        
		$(document).ready(function () {
			GetButtons("add_new_monitoring", '', "");
			
			GetDateTimes2('start_date_main1');
			GetDateTimes2('end_date_main1');
			
			$("#end_date_main1").val(GetDateTime(2) + " 23:59");
			
            monitoring_table_operator();
			loadtabs(); 
			counters();
			
			$("#filter_button, #filter_button1, #filter_button_main").button();
			GetDateTimes2('start_date');
			GetDateTimes2('end_date');
            GetDateTimes2('start_date_main');
			GetDateTimes2('end_date_main');
			
			GetDateTimes2('start_date1');
			GetDateTimes2('end_date1');
			GetDateTimes2('start_date_dasrulebuli');
			GetDateTimes2('end_date_dasrulebuli');

            GetDateTimes2('start_date_g');
			GetDateTimes2('end_date_g');
			$("#start_date").val(GetDateTime(2) + " 00:00");
			$("#end_date").val(GetDateTime(2) + " 23:59");
            $("#start_date_main").val(GetDateTime(2) + " 00:00");
			$("#end_date_main").val(GetDateTime(2) + " 23:59");
			
			$("#start_date1").val(GetDateTime(2) + " 00:00");
			$("#end_date1").val(GetDateTime(2) + " 23:59");
			$("#start_date_dasrulebuli").val(GetDateTime(2) + " 00:00");
			$("#end_date_dasrulebuli").val(GetDateTime(2) + " 23:59");

            $("#start_date_g").val(GetDateTime(2) + " 00:00");
			$("#end_date_g").val(GetDateTime(2) + " 23:59");
           
		});

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };

        var monitoring_table_operator = () =>{
            $.ajax({
                url:aJaxURL,
                data:{
                    act:"monitoring_table_operator",
                    start:$("#start_date_main1").val(),
                    end:$("#end_date_main1").val()
                },
                success:function(data){
                    var names = data.names;
                    var k = data.k;
                    var k1 = data.k1;
                    var k2 = data.k2;
                    

                    $.each(names,function(k,v){
                        $(".operator_row").append(`<div user_id = "${k}">${v}</div>`);
                    });
                    $.each(k,function(k,v){
                        $(".quality_row").append(`<div user_id = "${k}">${v}%</div>`);
                    });
                    $.each(k1,function(k,v){
                        let randk = Math.floor(Math.random() * 10)/100;
                        $(".attendance_row").append(`<div user_id = "${k}">${Number(v+randk)}%</div>`);
                    });
                    $.each(k2,function(k,v){
                        $(".discipline_row").append(`<div user_id = "${k}">${v}%</div>`);
                    });

                    $.each(names,function(i,v){
                        let randk = Math.floor(Math.random() * 10)/100;
                        let r = (Number(k[i])+(Number(k1[i])+randk)+Number(k2[i]))/3;
                        $(".r_row").append(`<div user_id = "${k}">${r.toFixed(2)}%</div>`);
                    });
                    
                    
                }

            })
        }
		
		function LoadTable(tbl,col_num,act,change_colum,data,id){
        	start_date = $('#start_date').val();
            end_date   = $('#end_date').val();

            if(tbl == 'dasrulebuli'){
            	start_date = $('#start_date1').val();
                end_date   = $('#end_date1').val();
            }else if(tbl == 'shemowmebuli'){
            	start_date = $('#start_date_dasrulebuli').val();
                end_date   = $('#end_date_dasrulebuli').val();
            }else if(tbl == 'gasachivrebuli'){
            	start_date = $('#start_date_g').val();
                end_date   = $('#end_date_g').val();
            }
            var tablename= tName+tbl;
            if(tablename == 'table_index'){
                var gridName     = tablename;
                var actions      = '';
                var editType     = "popup"; // Two types "popup" and "inline"
                var itemPerPage  = 	40;
                var columnsCount =	col_num;
                var columnsSQL   = 	[
                                        "id:string",
                                        "date:date",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string"
                                    ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "თარიღი",
                                            "სულ",
                                            "შესაფას.",
                                            "%",
                                            "შეფასებ.",
                                            "%"
                                        ];
    
                var showOperatorsByColumns  =  [0,0,0,0,0,0,0]; 
                var selectors               =  [0,0,0,0,0,0,0];

                var locked                  =  [1,1,1,0,0,0,0];
                var lockable                =  [1,0,0,0,0,0,0]; 
    
                var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                //KendoUI CLASS CONFIGS END
                    
                const kendo = new kendoUI();
                kendo.loadKendoUI(aJaxURL,act,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&start_date="+start_date+"&end_date="+end_date, 0, locked, lockable);
            }else if(tablename == 'table_shesafasebeli'){
            	var gridName     = tablename;
                var actions      = '';
                var editType     = "popup"; // Two types "popup" and "inline"
                var itemPerPage  = 	20;
                var columnsCount =	12;
                var columnsSQL   = 	[
                                        "id:string",
                                        "date:date",
                                        "date1:date",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string",
                                        "text5:string",
                                        "text6:string",
                                        "text7:string",
                                        "text8:string"
                                    ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "ფორმირების<br>თარიღი",
                                            "მომართვის<br>თარიღი",
                                            "წყარო",
                                            "ტიპი",
                                            "ქმედება",
                                            "ხანგრძლივობა",
                                           	"ოპერატორი",
                                           	"განაწილება",
                                           	"სტატუსი",
                                           	"შედეგები",
                                           	"მონიტორინგი"
                                        ];
    
                var showOperatorsByColumns  =  [0,0,0,0,0,0,0,0,0,0,0,0]; 
                var selectors               =  [0,0,0,0,0,0,0,0,0,0,0,0]; 

                var locked                  =  [1,1,1,1,1,0,0,0,0,0,0,0];
                var lockable                =  [1,0,0,0,0,0,0,0,0,0,0,0];
    
                var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                //KendoUI CLASS CONFIGS END
                    
                const kendo = new kendoUI();
                kendo.loadKendoUI(aJaxURL,act,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&start_date="+start_date+"&end_date="+end_date, 0, locked, lockable);
            }else if(tablename == 'table_shemowmebuli'){
            	var gridName     = tablename;
                var actions      = '';
                var editType     = "popup"; // Two types "popup" and "inline"
                var itemPerPage  = 	40;
                var columnsCount =	14;
                var columnsSQL   = 	[
                                        "id:string",
                                        "date:date",
                                        "date1:date",
                                        "phone:string",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string",
                                        "text5:string",
                                        "text6:string",
                                        "text7:string",
                                        "text8:string",
                                        "text9:string"
                                    ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "ფორმირების<br>თარიღი",
                                            "მომართვის<br>თარიღი",
                                            "წყარო",
                                            "ტიპი",
                                            "ნომერი",
                                            "ქმედება",
                                            "ხან-ბა",
                                           	"ოპერატორი",
                                           	"განაწილება",
                                           	"სტატუსი",
                                           	"შედეგები",
                                           	"დახარვეზებული",
                                           	"მონიტორინგი"
                                        ];
    
                var showOperatorsByColumns  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 
                var selectors               = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 

                var locked                  =  [1,1,1,1,1,0,0,0,0,0,0,0,0,0];
                var lockable                =  [1,0,0,0,0,0,0,0,0,0,0,0,0,0];
    
                var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                //KendoUI CLASS CONFIGS END
                var number_dasrulebuli = $("#number_dasrulebuli").val();
                const kendo = new kendoUI();
                kendo.loadKendoUI(aJaxURL,act,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&start_date="+start_date+"&end_date="+end_date+"&number_dasrulebuli="+number_dasrulebuli, 0, locked, lockable);
                
            }else if(tablename == 'table_gasachivrebuli'){
            	var gridName     = tablename;
                var actions      = '';
                var editType     = "popup"; // Two types "popup" and "inline"
                var itemPerPage  = 	40;
                var columnsCount =	14;
                var columnsSQL   = 	[
                                        "id:string",
                                        "date:date",
                                        "date1:date",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string",
                                        "text5:string",
                                        "text6:string",
                                        "text7:string",
                                        "text8:string",
                                        "text9:string",
                                        "text10:string"
                                    ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "ფორმირების<br>თარიღი",
                                            "მომართვის<br>თარიღი",
                                            "წყარო",
                                            "ტიპი",
                                            "ქმედება",
                                            "ხან-ბა",
                                           	"ოპერატორი",
                                           	"განაწილება",
                                           	"სტატუსი",
                                           	"შედეგები",
                                           	"მონიტორინგი",
                                           	"მენეჯერი",
                                           	"ხელმძღვანელი"
                                        ];
    
                var showOperatorsByColumns  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 
                var selectors               = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 

                var locked                  = [1,1,1,1,1,0,0,0,0,0,0,0,0,0];
                var lockable                = [1,0,0,0,0,0,0,0,0,0,0,0,0,0];
    
                var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                //KendoUI CLASS CONFIGS END
                    
                const kendo = new kendoUI();
                kendo.loadKendoUI(aJaxURL,act,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&start_date="+start_date+"&end_date="+end_date, 0, locked, lockable);
            }else if(tablename == 'table_dasrulebuli'){
            	var gridName     = tablename;
                var actions      = '';
                var editType     = "popup"; // Two types "popup" and "inline"
                var itemPerPage  = 	40;
                var columnsCount =	14;
                var columnsSQL   = 	[
                                        "id:string",
                                        "date:date",
                                        "date1:date",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string",
                                        "text5:string",
                                        "text6:string",
                                        "text7:string",
                                        "text8:string",
                                        "text9:string",
                                        "text10:string"
                                    ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "ფორმირების<br>თარიღი",
                                            "მომართვის<br>თარიღი",
                                            "წყარო",
                                            "ტიპი",
                                            "ქმედება",
                                            "ხან-ბა",
                                           	"ოპერატორი",
                                           	"განაწილება",
                                           	"სტატუსი",
                                           	"შედეგები",
                                           	"მონიტორინგი",
                                           	"მენეჯერი",
                                           	"ხელმძღვანელი"
                                        ];
    
                var showOperatorsByColumns  = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 
                var selectors               = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; 

                var locked                  = [1,1,1,1,1,0,0,0,0,0,0,0,0,0];
                var lockable                = [1,0,0,0,0,0,0,0,0,0,0,0,0,0];
    
                var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                //KendoUI CLASS CONFIGS END
                    
                const kendo = new kendoUI();
                kendo.loadKendoUI(aJaxURL,act,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&start_date="+start_date+"&end_date="+end_date, 0, locked, lockable);
            }else if(tablename == 'table_manual_dialog_table'){
            	var gridName     = tablename;
                var actions      = '';
                var editType     = "popup"; // Two types "popup" and "inline"
                var itemPerPage  = 	40;
                var columnsCount =	11;
                var columnsSQL   = 	[
                                        "id:string",
                                        "date1:date",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string",
                                        "text5:string",
                                        "text6:string",
                                        "text7:string",
                                        "text8:string"
                                    ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "მომართვის<br>თარიღი",
                                            "წყარო",
                                            "ტიპი",
                                            "ქმედება",
                                            "ხან-ბა",
                                           	"ოპერატორი",
                                           	"განაწილება",
                                           	"სტატუსი",
                                           	"შედეგები",
                                           	"მონიტორინგი"
                                        ];
    
                var showOperatorsByColumns  = [0,0,0,0,0,0,0,0,0,0,0]; 
                var selectors               = [0,0,0,0,0,0,0,0,0,0,0]; 

                var locked                  = [1,1,1,1,1,0,0,0,0,0,0];
                var lockable                = [1,0,0,0,0,0,0,0,0,0,0];
    
                var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                //KendoUI CLASS CONFIGS END
                    
                const kendo = new kendoUI();

                var start_date = $("#start_date_dialog").val();
                var end_date   = $("#end_date_dialog").val();
                var main_id    = $("#hidde_monit_main_id").val();
                kendo.loadKendoUI(aJaxURL,act,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&start_date="+start_date+"&end_date="+end_date+"&id="+id+"&main_id="+main_id, 0, locked, lockable);
            }else{
            	
                GetDataTable(tName+tbl, aJaxURL, act, col_num, "&start_date="+start_date+"&end_date="+end_date, 0, "", 1, "desc", "", change_colum);
                
                $('.ColVis, .dataTable_buttons').css('display','none');
            }
           
        }
		
		
		$(document).on("click", "#fillter", function () {
	    	LoadTable();
	    });

		$(document).on("click", "#fillter1", function () {
	    	LoadTable1();
	    });

		$(document).on("click", "#fillter2", function () {
	    	LoadTable2();
	    });

		$(document).on("click", "#filter_button_main", function () {
			monitoring_table_operator();
			counters();
	    });

	    $(document).on("click", ".callapp_refresh", function () {
		    
	    	var tab = GetSelectedTab(tbName);

	    	if(tab == 0){
		    	LoadTable();
		    }else if(tab == 1){
		    	LoadTable1();
		    }else if(tab == 2){
		    	LoadTable2();
			}
	    });
	    
		$(document).on("click", "#play_audio", function () {
            var str = 1;
            var link = ($(this).attr("src"));
            link = 'http://172.16.0.80:8000/' + link;
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    class: "cancel-audio",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog2", "auto", "auto",btn);
            console.log(link)
            $("#audio_dialog2").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        })
	    $(document).on("click", "#jNotify", function () {
	    	var task_check_id = $("#task_check_id").val();
	    	$.ajax({
                url: aJaxURL3,
                type: "POST",
                data: "act=get_edit_page&id=" + task_check_id+"&viewed=viewed",
                success: function (data) {
                   $("#" + fName).html(data.page);
                   LoadDialog(fName);
                   LoadTable3();
                }
            });
        });
        
	    // Add - Save
	    $(document).on("click", "#save-dialog-monitoring", function () {
	    	param                      = new Object();
	    	param.id                   = $("#monit_hidde_id").val();
	    	param.monitoring_status_id = $("#monitoring_status_id").val();
	    	param.monitoring_coment    = $("#monitoring_coment").val();
	    	param.answer               = '';
	    	param.act                  = "save_incomming";
	    	
	    	var data = $('input[class=radio]:checked').map(function () { //Get Checked checkbox array
				 return $(this).attr('name');
	        }).get();
	        
	        for (var i = 0; i < data.length; i++) {
				var value     = $('input[name='+data[i]+']:checked').val();

				var level     = $('#question_level_'+data[i]).html();
				var comment   = escape_string($('#question_comment_'+data[i]).val());

				
				param.answer += "(1, NOW(),"+param.id+",'"+data[i]+"','"+value+"','"+level+"','"+comment+"'),";
			}
		    
			$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {       
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							$("#add-edit-form").dialog('close');
							var tab = $("li[id^='t_'][aria-selected=true]").attr('name');
							
							if(tab == 2){
					    		LoadTable('shesafasebeli', 13, "get_list_shesafasebeli", change_colum_main, obj);
						    }else if(tab == 3){
						    	LoadTable('shemowmebuli', 14, "get_list_shemowmebuli", change_colum_main, obj);
						    }else if(tab == 4){
						    	LoadTable('gasachivrebuli', 15, "get_list_gasachivrebuli", change_colum_main, obj);
							}else if(tab == 5){
								LoadTable('dasrulebuli', 15, "get_list_dasrulebuli", change_colum_main, obj);
							}
						}
					}
			    }
		    });
		});

	    function escape_string(str){
	      return str.replace(/['\"]/g, "");
	    }

	    $(document).on("click", "#chat_hist", function () {
		    if($("#history_source_id").val() == 4){
			    source = 'chat';
		    }else if($("#history_source_id").val() == 11){
		    	source = 'site_chat';
		    }else if($("#history_source_id").val() == 9){
		    	source = 'viber';
		    }else if($("#history_source_id").val() == 7){
		    	source = 'mail';
		    }else if($("#history_source_id").val() == 6){
		    	source = 'fbm';
		    }
        	$.ajax({
		        url: aJaxURL_inc,
			    data: {
					'act'		:'get_history',
					'ip' 		:$('#user_block').attr('ip'),
					'chat_id'	:$('#chat_original_id').val(),
					'source'	:source

				},
		        success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){

						}else{
							$('#log').html('');
							$('#log').html(data.sms);
							//jQuery("time.timeago").timeago();
							document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
							$('#chat_live').css('border-bottom','none');
							$('#chat_hist').css('border-bottom','2px solid #333');
						}
					}
			    }
		    });
        });
        
		$(document).on("click", ".radio", function () {
			
			var name          = $(this).attr('name');
			var checked_value = $('input[name='+name+']:checked').val();
			var click_count   = $('input[name='+name+']:checked').attr('click_count');
			var sum_level     = parseFloat($("#monitoring_level_sum").html());
			var level         = parseFloat($("#question_level_"+name).attr('level'));

			if(checked_value == '1'){
				if(click_count == 0){
    				var new_sum = sum_level + level;
    				$("#question_level_"+name).html(level.toFixed(2));
    				$('input[name='+name+']').attr('click_count',0);
    				$('input[name='+name+']:checked').attr('click_count',1);
    				$("#monitoring_level_sum").html(new_sum.toFixed(2));
				}
			}else{
				if(click_count == 0){
    				var new_sum = sum_level - level;
    				$("#question_level_"+name).html("-"+level.toFixed(2));
    				$('input[name='+name+']').attr('click_count',0);
    				$('input[name='+name+']:checked').attr('click_count',1);
    				$("#monitoring_level_sum").html(new_sum.toFixed(2));
				}
			}

			

		});
		
		$(document).on("click", "#refresh_button", function () {
	    	
	    	param 				= new Object();
	    	param.act			= "refresh";
	    	
	    	$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							 $("#search_end_my").val(data.date);
							 LoadTable();
						}
					}
			    }
		    });
		});

 		$(document).on("click", "#refresh_button1", function () {
	    	
	    	param 				= new Object();
	    	param.act			= "refresh";
	    	
	    	$.ajax({
		        url: aJaxURL3,
			    data: param,
		        success: function(data) {
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							 $("#search_end_my1").val(data.date);
							 LoadTable3();
						}
					}
			    }
		    });
		});
		
	    	
	    $(document).on("click", "#refresh-dialog", function () {
    	 	param 			= new Object();
		 	param.act		= "get_calls";
		 	
	    	$.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {       
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							$("#last_calls").html(data.calls);
							$( ".insert" ).button({
							      icons: {
							        primary: "ui-icon-plus"
							      }
							});
						}
					}
			    }
		    });
		});

	    function show_right_side(id){
	        $("#right_side fr").hide();
	        $("#" + id).show();
	        //$(".add-edit-form-class").css("width", "1260");
	        //$('#add-edit-form').dialog({ position: 'left top' });
	        hide_right_side();

	        var str = $("."+id).children('img').attr('src');
			str = str.substring(0, str.length - 4);
			$("#side_menu span").children('img').css('border-bottom','');
	        $("."+id).children('img').css('filter','brightness(0.1)');
	        $("."+id).children('img').css('border-bottom','2px solid #333');
	    }

	    function hide_right_side(){

			$(".info").children('img').css('filter','brightness(1.1)');
	        $(".task").children('img').css('filter','brightness(1.1)');
	        $(".record").children('img').css('filter','brightness(1.1)');
	        $(".file").children('img').css('filter','brightness(1.1)');
	        $(".file").children('img').css('filter','brightness(1.1)');
	        $(".sms").children('img').css('filter','brightness(1.1)');
	        $(".sport").children('img').css('filter','brightness(1.1)');
	        $(".crm").children('img').css('filter','brightness(1.1)');
	        $("#record fieldset").show();
	    }

	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    }); 

		function loadtabs(activep="", activech=""){
            
            obj = new Object;
            obj.act="get_tabs";
            $.ajax({
                url: aJaxURL,
                data:obj,
                success: function (data)
                {
                    $("#tabs1").html(data.page);
                    GetTabs("tab_0");
                    if(activep==""){
                        if(user_ID == "149"){
                            $("#tabs1").first().hide();
                            $("[id^=t_3]").first().click();
                        }
                        else
                            $("[id^=t_]").first().click();
                    } else{
                        $(activep).click();
                        if(activech!=""){
                            $(activech).click();
                        }
                    }
                    
                    var id = getUrlParameter('id');
                    if(id){
                        $("#t_3").click();
                        obj         = new Object;
                        obj.act     = "get_edit_page";
                        obj.id      = id;
                        obj.tab_id  = 3;
                        $.ajax({
                            url: aJaxURL,
                            data:obj,
                            success:function(data){
                                $("#add-edit-form").html(data.page);
                                var buttons = {
                                    "open": {
                                            text: "დისციპლინა",
                                            id: "open_disciplin",
                                            click: function () {
                                                console.log(12);
                                            }
                                    },
                                    "save": {
                                        text: "შენახვა",
                                        id: "save-dialog-monitoring",
                                        click: function () {
                                        }
                                    },
                                    "cancel": {
                                        text: "გაუქმება",
                                        id: "cancel-dialog",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                };
                                
                                GetDialog("add-edit-form", 1585, "auto", buttons, 'center top');
                                $("[data-select='chosen']").chosen({ search_contains: true });
                                $("[data-button='jquery-ui-button']").button();
                                $("#dep_0, #dep_project_0, #dep_sub_project_0, #dep_sub_type_0").chosen();
                                
                                $(".fieldset_row select").chosen();
                                $(".fieldset_row .chosen-container").css('width','182px');
                                $("#dep_0_chosen,#dep_project_0_chosen,#dep_sub_project_0_chosen,#dep_sub_type_0_chosen").css('width','110px')
                            
                                $("#region_chosen, #municip_chosen, #temi_chosen, #village_chosen").css('width','135px');
                                $("#add_dep, #get_dep_additional, #get_docs_project, #get_contact_project").css('display', 'none');
                                
                                $( "#info input").prop( "disabled", true );
                                $( "#info textarea").prop( "disabled", true );
                                $( "#info select").prop( "disabled", true ).trigger("chosen:updated");
                                
                                $("#monitoring_coment").autocomplete("server-side/seoy/monitoring_comment.action.php", {
                                    width: 300,
                                    multiple: true,
                                    matchContains: true
                                });
                                
                            }
                        });
                    }
                    //quantity_ajax();
                }
            })
        }

		$(document).on("click", "#tabs1 li", function() {
	        if(!$(this).hasClass("child")){
	            deactive("li[id^='t_']");
	            $('div[id^="tab_"]').css("display","block");
	            active("#t_"+$(this).attr("name") );
	            $('div[id^="tab_"]').not(".main_tab").not('.tab_'+$(this).attr("name")).css("display","none");
	            active("#tab_"+$(this).attr("name")+" ul li:first-child");
	            var obj        = new Object;
	            obj.parent     = $(this).attr("name");
	            obj.child      = $("#tab_"+$(this).attr("name")+" ul li:first-child").attr("name");
	            obj.start_date = $('#start_date').val();
	            obj.end_date   = $('#end_date').val();
	            
	            $("#activeparent").val(""+obj.parent);
	            $("#activechild").val(""+obj.child);
	            if($(this).attr('id')=='t_0'){
                    $("#tab-0").css('display','');
		            $("#tab-1").css('display','none');
					$("#tab-2").css('display','none');
		            $("#tab-3").css('display','none');
		            $("#tab-4").css('display','none');
		            $("#tab-5").css('display','none');
	            	LoadTable('index', colum_number, main_act, change_colum_main, obj);
	            }else if($(this).attr('id')=='t_1'){
                    $("#tab-0").css('display','none');
		            $("#tab-1").css('display','');
					$("#tab-2").css('display','none');
		            $("#tab-3").css('display','none');
		            $("#tab-4").css('display','none');
		            $("#tab-5").css('display','none');
	            	LoadTable('index', colum_number, main_act, change_colum_main, obj);
	            }else if($(this).attr('id')=='t_2'){
                    $("#tab-0").css('display','none');
                    $("#tab-1").css('display','none');
	            	$("#tab-1").css('display','none');
					$("#tab-2").css('display','');
		            $("#tab-3").css('display','none');
		            $("#tab-4").css('display','none');
		            $("#tab-5").css('display','none');
		            
		            obj     = new Object;
		            obj.act = "get_shesamowmebeli_result";
		            $.ajax({
		                url: aJaxURL,
		                data:obj,
		                success: function (data){
		                    $("#shesamowmebeli_request_count").html(data.page.shesamowmebeli_request_count);
		                    $("#shesamowmebeli_action_count_1").html(data.page.shesamowmebeli_action_count_1);
		                    $("#shesamowmebeli_action_count_2").html(data.page.shesamowmebeli_action_count_2);
		                    $("#shesamowmebeli_action_count_3").html(data.page.shesamowmebeli_action_count_3);
		                    $("#shesamowmebeli_action_count_4").html(data.page.shesamowmebeli_action_count_4);
							$("#shesamowmebeli_action_count_all").html(data.page.shesamowmebeli_action_count_all);
		                    $("#shesamowmebeli_auto_rate_count").html(data.page.shesamowmebeli_auto_rate_count);
		                    $("#shesamowmebeli_call_type_906").html(data.page.shesamowmebeli_call_type_906);
		                    $("#shesamowmebeli_call_type_936").html(data.page.shesamowmebeli_call_type_936);
		                    $("#shesamowmebeli_call_type_952").html(data.page.shesamowmebeli_call_type_952);
		                    $("#shesamowmebeli_call_type_1138").html(data.page.shesamowmebeli_call_type_1138);
		                    $("#shesamowmebeli_call_type_1312").html(data.page.shesamowmebeli_call_type_1312);
		                    $("#shesamowmebeli_call_type_all").html(data.page.shesamowmebeli_call_type_all);
							$("#shesamowmebeli_duration_count_1").html(data.page.shesamowmebeli_duration_count_1);
		                    $("#shesamowmebeli_duration_count_2").html(data.page.shesamowmebeli_duration_count_2);
		                    $("#shesamowmebeli_duration_count_3").html(data.page.shesamowmebeli_duration_count_3);
		                    $("#shesamowmebeli_duration_count_4").html(data.page.shesamowmebeli_duration_count_4);
							$("#shesamowmebeli_duration_count_5").html(data.page.shesamowmebeli_duration_count_5);
		                    $("#shesamowmebeli_duration_count_10").html(data.page.shesamowmebeli_duration_count_10);
		                    $("#shesamowmebeli_duration_count_15").html(data.page.shesamowmebeli_duration_count_15);
							$("#shesamowmebeli_duration_count_all").html(data.page.shesamowmebeli_duration_count_all);
							$("#shesamowmebeli_manual_rate_count").html(data.page.shesamowmebeli_manual_rate_count);
		                    $("#shesamowmebeli_operators_count_all").html(data.page.shesamowmebeli_operators_count_all);
							$("#shesamowmebeli_operators_filter").html(data.page.shesamowmebeli_operators_filter);
		                    $("#shesamowmebeli_operators_filter_count").html(data.page.shesamowmebeli_operators_filter_count);
		                    $("#shesamowmebeli_source_count_1").html(data.page.shesamowmebeli_source_count_1);
		                    $("#shesamowmebeli_source_count_4").html(data.page.shesamowmebeli_source_count_4);
		                    $("#shesamowmebeli_source_count_6").html(data.page.shesamowmebeli_source_count_6);
		                    $("#shesamowmebeli_source_count_7").html(data.page.shesamowmebeli_source_count_7);
		                    $("#shesamowmebeli_source_count_9").html(data.page.shesamowmebeli_source_count_9);
		                    $("#shesamowmebeli_source_count_11").html(data.page.shesamowmebeli_source_count_11);
							$("#shesamowmebeli_source_count_all").html(data.page.shesamowmebeli_source_count_all);
		                }
		            });
		            LoadTable('shesafasebeli', 13, "get_list_shesafasebeli", change_colum_main, obj);
	            }else if($(this).attr('id')=='t_3'){
                    $("#tab-0").css('display','none');
	            	$("#tab-1").css('display','none');
					$("#tab-2").css('display','none');
		            $("#tab-3").css('display','');
		            $("#tab-4").css('display','none');
		            $("#tab-5").css('display','none');
		            start_date = $('#start_date_dasrulebuli').val();
		            end_date   = $('#end_date_dasrulebuli').val();
		            
		            obj            = new Object;
		            obj.act        = "get_shemowmebuli_result";
		            obj.start_date = start_date;
		            obj.end_date   = end_date;
		            $.ajax({
		                url: aJaxURL,
		                data:obj,
		                success: function (data){
		                    $("#shemowmebuli_request_count").html(data.page.shemowmebuli_request_count);
		                    $("#shemowmebuli_shedegi_request_count").html(data.page.shemowmebuli_shedegi_request_count);
							$("#shemowmebuli_action_count_1").html(data.page.shemowmebuli_action_count_1);
		                    $("#shemowmebuli_action_count_2").html(data.page.shemowmebuli_action_count_2);
		                    $("#shemowmebuli_action_count_3").html(data.page.shemowmebuli_action_count_3);
		                    $("#shemowmebuli_action_count_4").html(data.page.shemowmebuli_action_count_4);
							$("#shemowmebuli_action_count_all").html(data.page.shemowmebuli_action_count_all);
		                    $("#shemowmebuli_auto_rate_count").html(data.page.shemowmebuli_auto_rate_count);
		                    $("#shemowmebuli_call_type_906").html(data.page.shemowmebuli_call_type_906);
		                    $("#shemowmebuli_shedegi_call_type_906").html(data.page.shemowmebuli_shedegi_call_type_906);
		                    $("#shemowmebuli_call_type_936").html(data.page.shemowmebuli_call_type_936);
		                    $("#shemowmebuli_shedegi_call_type_936").html(data.page.shemowmebuli_shedegi_call_type_936);
		                    $("#shemowmebuli_call_type_952").html(data.page.shemowmebuli_call_type_952);
		                    $("#shemowmebuli_shedegi_call_type_952").html(data.page.shemowmebuli_shedegi_call_type_952);
		                    $("#shemowmebuli_call_type_1138").html(data.page.shemowmebuli_call_type_1138);
		                    $("#shemowmebuli_shedegi_call_type_1138").html(data.page.shemowmebuli_shedegi_call_type_1138);
		                    $("#shemowmebuli_call_type_1312").html(data.page.shemowmebuli_call_type_1312);
		                    $("#shemowmebuli_shedegi_call_type_1312").html(data.page.shemowmebuli_shedegi_call_type_1312);
		                    $("#shemowmebuli_call_type_all").html(data.page.shemowmebuli_call_type_all);
		                    $("#shemowmebuli_shedegi_call_type_all").html(data.page.shemowmebuli_shedegi_call_type_all);
							$("#shemowmebuli_duration_count_1").html(data.page.shemowmebuli_duration_count_1);
		                    $("#shemowmebuli_duration_count_2").html(data.page.shemowmebuli_duration_count_2);
		                    $("#shemowmebuli_duration_count_3").html(data.page.shemowmebuli_duration_count_3);
		                    $("#shemowmebuli_duration_count_4").html(data.page.shemowmebuli_duration_count_4);
							$("#shemowmebuli_duration_count_5").html(data.page.shemowmebuli_duration_count_5);
		                    $("#shemowmebuli_duration_count_10").html(data.page.shemowmebuli_duration_count_10);
		                    $("#shemowmebuli_duration_count_15").html(data.page.shemowmebuli_duration_count_15);
							$("#shemowmebuli_duration_count_all").html(data.page.shemowmebuli_duration_count_all);
							$("#shemowmebuli_manual_rate_count").html(data.page.shemowmebuli_manual_rate_count);
		                    $("#shemowmebuli_operators_count_all").html(data.page.shemowmebuli_operators_count_all);
							$("#shemowmebuli_operators_filter").html(data.page.shemowmebuli_operators_filter);
		                    $("#shemowmebuli_operators_filter_count").html(data.page.shemowmebuli_operators_filter_count);
		                    $("#shemowmebuli_source_count_1").html(data.page.shemowmebuli_source_count_1);
		                    $("#shemowmebuli_shedegi_source_count_1").html(data.page.shemowmebuli_shedegi_source_count_1);
		                    $("#shemowmebuli_source_count_4").html(data.page.shemowmebuli_source_count_4);
		                    $("#shemowmebuli_shedegi_source_count_4").html(data.page.shemowmebuli_shedegi_source_count_4);
		                    $("#shemowmebuli_source_count_6").html(data.page.shemowmebuli_source_count_6);
		                    $("#shemowmebuli_shedegi_source_count_6").html(data.page.shemowmebuli_shedegi_source_count_6);
		                    $("#shemowmebuli_source_count_7").html(data.page.shemowmebuli_source_count_7);
		                    $("#shemowmebuli_shedegi_source_count_7").html(data.page.shemowmebuli_shedegi_source_count_7);
		                    $("#shemowmebuli_source_count_9").html(data.page.shemowmebuli_source_count_9);
		                    $("#shemowmebuli_shedegi_source_count_9").html(data.page.shemowmebuli_shedegi_source_count_9);
		                    $("#shemowmebuli_source_count_11").html(data.page.shemowmebuli_source_count_11);
		                    $("#shemowmebuli_shedegi_source_count_11").html(data.page.shemowmebuli_shedegi_source_count_11);
							$("#shemowmebuli_source_count_all").html(data.page.shemowmebuli_source_count_all);
							$("#shemowmebuli_shedegi_source_count_all").html(data.page.shemowmebuli_shedegi_source_count_all);
		                }
		            });
		            LoadTable('shemowmebuli', 13, "get_list_shemowmebuli", change_colum_main, obj);
	            }else if($(this).attr('id')=='t_4'){
                    $("#tab-0").css('display','none');
	            	$("#tab-1").css('display','none');
					$("#tab-2").css('display','none');
		            $("#tab-3").css('display','none');
		            $("#tab-4").css('display','');
		            $("#tab-5").css('display','none');
// 		            obj     = new Object;
// 		            obj.act = "get_gasachivrebuli_result";
// 		            obj.gasacivrebuli_operator_filter = '';
// 		            obj.gasacivrebuli_monitoring_filter = '';
// 		            $.ajax({
// 		                url: aJaxURL,
// 		                data:obj,
// 		                success: function (data){
// 		                    $("#gasachivrebuli_result_monitoring").html(data.page.monitorings);
// 		                    $("#gasachivrebuli_result_operator").html(data.page.operators);
		                   
// 		                }
// 		            });
		            LoadTable('gasachivrebuli', 15, "get_list_gasachivrebuli", change_colum_main, obj);
	            }else if($(this).attr('id')=='t_5'){
                    $("#tab-0").css('display','none');
	            	$("#tab-1").css('display','none');
					$("#tab-2").css('display','none');
		            $("#tab-3").css('display','none');
		            $("#tab-4").css('display','none');
		            $("#tab-5").css('display','');
		            LoadTable('dasrulebuli', 15, "get_list_dasrulebuli", change_colum_main, obj);
	            }
	        }
	    });

		$(document).on("click", ".gasachivrebuli_operator_filter, .gasachivrebuli_monitoring_filter", function () {
			var gasachivrebuli_operator_filter = $(".gasachivrebuli_operator_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var gasachivrebuli_monitoring_filter = $(".gasachivrebuli_monitoring_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	
	    	GetDataTable("table_gasachivrebuli", aJaxURL, "get_list_gasachivrebuli", 15, "gasachivrebuli_operator_filter="+gasachivrebuli_operator_filter+"&gasachivrebuli_monitoring_filter="+gasachivrebuli_monitoring_filter, 0, "", 1, "desc", "", change_colum_main);
	    	$('.ColVis, .dataTable_buttons').css('display','none');
	    });
	    
		function active(id){
	        $(id).attr( "aria-selected", "true");
	        $(id).css("cursor", "grabbing");
	    }
	    
	    function deactive(id){
	        $(id).attr( "aria-selected", "false");
	        $(id).css("cursor", "pointer");
	    }

	    $(document).on("click", "#callapp_show_filter_button", function () {
            if ($('.callapp_filter_body').attr('myvar') == 0) {
                $('.callapp_filter_body').css('display', 'block');
                $('.callapp_filter_body').attr('myvar', 1);
            } else {
                $('.callapp_filter_body').css('display', 'none');
                $('.callapp_filter_body').attr('myvar', 0);
            }
        });

	    $(document).on("click", "#callapp_show_filter_button1", function () {
            if ($('.callapp_filter_body1').attr('myvar1') == 0) {
                $('.callapp_filter_body1').css('display', 'block');
                $('.callapp_filter_body1').attr('myvar1', 1);
            } else {
                $('.callapp_filter_body1').css('display', 'none');
                $('.callapp_filter_body1').attr('myvar1', 0);
            }
        });

	    $(document).on("click", "#callapp_show_filter_button2", function () {
            if ($('.callapp_filter_body2').attr('myvar2') == 0) {
                $('.callapp_filter_body2').css('display', 'block');
                $('.callapp_filter_body2').attr('myvar2', 1);
            } else {
                $('.callapp_filter_body2').css('display', 'none');
                $('.callapp_filter_body2').attr('myvar2', 0);
            }
        });

	    $(document).on("click", "#callapp_show_filter_button3", function () {
            if ($('.callapp_filter_body3').attr('myvar3') == 0) {
                $('.callapp_filter_body3').css('display', 'block');
                $('.callapp_filter_body3').attr('myvar3', 1);
            } else {
                $('.callapp_filter_body3').css('display', 'none');
                $('.callapp_filter_body3').attr('myvar3', 0);
            }
        });

	    $(document).on("click", "#callapp_show_filter_button4", function () {
            if ($('.callapp_filter_body4').attr('myvar4') == 0) {
                $('.callapp_filter_body4').css('display', 'block');
                $('.callapp_filter_body4').attr('myvar4', 1);
            } else {
                $('.callapp_filter_body4').css('display', 'none');
                $('.callapp_filter_body4').attr('myvar4', 0);
            }
        });

	    $(document).on("click", "#callapp_show_filter_dialog", function () {
            if ($('.callapp_filter_body_dialog').attr('myvar_dialog') == 0) {
                $('.callapp_filter_body_dialog').css('display', 'block');
                $('.callapp_filter_body_dialog').attr('myvar_dialog', 1);
            } else {
                $('.callapp_filter_body_dialog').css('display', 'none');
                $('.callapp_filter_body_dialog').attr('myvar_dialog', 0);
            }
        });

	    $(document).on("click", "#filter_button_dialog", function () {
		    var id = '';
	    	obj       = new Object;
	    	obj.act   = "get_add_page";
            obj.id    = '';
            obj.start = $("#start_date_dialog").val();
            obj.end   = $("#end_date_dialog").val();

		    $.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                	$("#gasanawilebeli_html").html(data.page);
                	$('#formirebis_tarigi').html(data.former_date);
                	if($("#show_monitoring_table").attr('check_show') == 1){
                    	$('#show_table_div').css('display','');
                		LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, '');
            			$("#table_manual_dialog_table_length").css('top','3px');
                	}else{
                		$('#show_table_div').css('display','none');
                	}
                }
            })
	    });

	    $(document).on("click", "#refresh_all_monitoring_input", function () {
		    obj           = new Object;
	    	obj.act       = "get_add_page";
            obj.id        = '';
            obj.start     = $("#start_date_dialog").val();
            obj.end       = $("#end_date_dialog").val();
            obj.count_sum = $("#manual_call_type_percent").val();
		    $.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                	$("#gasanawilebeli_html").html(data.page);
                	$('#formirebis_tarigi').html(data.former_date);
                	if($("#show_monitoring_table").attr('check_show') == 1){
                    	$('#show_table_div').css('display','');
                		LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, '');
            			$("#table_manual_dialog_table_length").css('top','3px');
                	}else{
                		$('#show_table_div').css('display','none');
                	}
                }
            })
	    });

	    $(document).on("click", ".refresh_monitoring_input", function () {
		    var type_id     = $(this).attr('type_id');
	    	obj             = new Object;

	    	obj.act         = "get_monitoring_request_redact";
            obj.id          = $("#hidde_monit_main_id").val();
            obj.start       = $("#start_date_dialog").val();
            obj.end         = $("#end_date_dialog").val();
            obj.type        = $(this).attr('type_id');
            obj.input_value = $(".request_type[type_id="+type_id+"]").val();
            
	    	$.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                	$("#gasanawilebeli_html").html(data.page);
                	$('#formirebis_tarigi').html(data.former_date);

                	if($("#show_monitoring_table").attr('check_show') == '1'){
                    	$('#show_table_div').css('display','');
                		LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, '');
                	}else{
                		$('#show_table_div').css('display','none');
                	}
                }
            })
	    });

	    $(document).on("click", ".monitoring_call_type_checkbox, .call_department_checkbox, .monitoring_source_checkbox, .monitoring_action_checkbox, .radio_input_task_time", function () {
		    var type_id       = $(this).val();
			var checkbox_type = $(this).attr('class');
			var count_value   = 0;
			
			if(checkbox_type == 'monitoring_call_type_checkbox'){
				count_value = $(".request_type[type_id="+type_id+"]").val();
			}else if(checkbox_type == 'call_department_checkbox'){
				count_value = $("#call_department_checkbox"+type_id).val();
			}else if(checkbox_type == 'monitoring_source_checkbox'){
				count_value = $("#monitoring_source_checkbox"+type_id).val();
			}else if(checkbox_type == 'monitoring_action_checkbox'){
				count_value = $("#monitoring_action_checkbox"+type_id).val();
			}
			
		    obj               = new Object;
			obj.act           = "get_monitoring_request_checkbox";
            obj.id            = $("#hidde_monit_main_id").val();
            obj.start         = $("#start_date_dialog").val();
            obj.end           = $("#end_date_dialog").val();
            obj.type_id       = type_id;
            obj.checkbox_type = checkbox_type;
            obj.input_value   = count_value;
            
			if($(this).is(':checked')){
				if(checkbox_type == 'monitoring_call_type_checkbox'){
    				$(".request_type[type_id="+type_id+"]").attr('disabled',false);
    			    $(this).css('color','');
				}
				
				obj.check_checked = 1;
			}else{
				if(checkbox_type == 'monitoring_call_type_checkbox'){
    				$(".request_type[type_id="+type_id+"]").attr('disabled',"disabled");
    			    $(this).css('color','#cac9c9');
    			}
    			
				obj.check_checked = 0;
			}

			$.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                	$("#gasanawilebeli_html").html(data.page);
                	$('#formirebis_tarigi').html(data.former_date);

                	if($("#show_monitoring_table").attr('check_show') == '1'){
                    	$('#show_table_div').css('display','');
                		LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, '');
                	}else{
                		$('#show_table_div').css('display','none');
                	}
                }
            })
	    });
	    
	    $(document).on("click", "#table_index tr.k-state-selected", function () {
	    	
	    	var grid = $("#table_index").data("kendoGrid");
            var dItem = grid.dataItem($(this));
            table_hidde_id = dItem.id;
	    	all_count = dItem.text2;
	    	if(table_hidde_id != undefined && table_hidde_id != ''){
    		    $("#table_index tr").css('border','1px solid #A3D0E4');
    		    $(this).css('border', '3px solid rgb(91, 95, 86)');
    		      
    		    
    	    	obj     = new Object;
    	    	obj.act = "get_table_detail";
                obj.id  = table_hidde_id;
                $.ajax({
                    url: aJaxURL,
                    data:obj,
                    success:function(data){
                        $("#monitoring_table_detail_info").html(data.page);

                        var gridName     = "table_detail_info";
                        var actions      = '';
                        var editType     = "popup"; // Two types "popup" and "inline"
                        var itemPerPage  = 	40;
                        var columnsCount =	7;
                        var columnsSQL   = 	[
                                                "id:string",
                                                "date:date",
                                                "text:string",
                                                "text1:string",
                                                "text2:string",
                                                "text3:string",
                                                "text4:string"
                                            ];
                        var columnGeoNames  = 	[
                                                    "ID", 
                                                    "თარიღი",
                                                    "მონიტორინგი",
                                                    "განაწ.",
                                                    "%",
                                                    "შეფასებ.",
                                                    "%"
                                                ];
            
                        var showOperatorsByColumns  =  [0,0,0,0,0,0,0]; 
                        var selectors               =  [0,0,0,0,0,0,0]; 

                        var locked                  =  [1,1,0,0,0,0,0];
                        var lockable                =  [1,0,0,0,0,0,0];
            
                        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                        //KendoUI CLASS CONFIGS END
                            
                        const kendo = new kendoUI();
                        kendo.loadKendoUI(aJaxURL,"get_list_monitoring_detail_info",itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&id="+table_hidde_id+"&all_count="+all_count, 0, locked, lockable);
                    }
                })
	    	}
	    });
        
        $(document).on("click","#open_disciplin",function() {
            var buttons = {
                "appeal": {
                    text: "საჩივარი",
                    id: "appeal_button"
                },
                "save": {
                    text: "შენახვა",
                    id: "save-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                "cancel": {
                    text: "გაუქმება",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                    }
                }
            };
            var operator_id = $("#dialog_operator_id").val();
            $.ajax({
                url: "server-side/view/disorders.action.php",
                data: "act=get_user_info&user_id="+operator_id+"&phormer_id="+user_ID,
                success: function (response) {
                    var department =  response.result["result"][0]["name"];
                    var phormer =  response.result["result"][0]["phormer"];
                    $.ajax({
                        url: "server-side/view/disorders.action.php",
                        type: "POST",
                        data: "act=get_add_page&id="+operator_id+"&department="+department+"&phormer="+phormer,
                        dataType: "json",
                        success: function(data) {
                            GetDialog("add-edit-form-disciplin","40%","30vh",buttons);
                            $("#add-edit-form-disciplin").css("height","auto");
                            $("#add-edit-form-disciplin").html(data.page);
                            $("#operator_id").prop("disabled", true);
                            $("#group").prop("disabled", true);
                        }
                    });
                }
            });


        });

        $(document).on("click","#save-dialog",function(){

            obj             = new Object;
            obj.act         = "save_disorder";
            obj.status      = $("#status").val();
            obj.appeal      = $("#appeal").val();
            obj.comment     = $("#comment").val();
            obj.penalty     = $("#penalty").val();
            obj.disorder    = $("#disorder").val();
            obj.operator_id = $("#operator_id").val();
            obj.inc_id      = $("#comment_incomming_id").val();
            
            $.ajax({
                url: "server-side/view/disorders.action.php",
                data: obj,
                success: function (response) {
                }
            });
        });

	    $(document).on("click", "#add_new_monitoring", function () {
	    	table_hidde_id = ($(this).find("td").eq(0).html());

	    	if(table_hidde_id == undefined){
				table_hidde_id = '';
			}
			
	    	$("#hidde_monitoring_id").val(table_hidde_id);
	    	//tab_id = $("li[id^='t_'][aria-selected=true]").attr('name');
	    	
		    obj     = new Object;
	    	obj.act = "get_add_page";
            obj.id  = table_hidde_id;
            $.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                    $("#gasanawilebeli_html").html(data.page);
                    var buttons = {
        					"save": {
        			            text: "შენახვა",
        			            id: "save-dialog-gasanawilebeli",
        			            click: function () {
        			            	obj     = new Object;
        			    	    	obj.act = "save-dialog-gasanawilebeli";
        			                obj.id  = $("#hidde_monit_main_id").val();

        			                obj.start               = $("#start_date_dialog").val();
        			                obj.end                 = $("#end_date_dialog").val();
        			                obj.all_reques_period   = $("#all_reques_period").html();
        			                obj.ukve_daformirebuli  = $("#ukve_daformirebuli").html();
        			                obj.dasaformirebeli     = $("#dasaformirebeli").html();
        			                obj.info                = $("#manual_call_type_info_count").html();
        			                obj.problem             = $("#manual_call_type_problem_count").html();
        			                obj.claim               = $("#manual_call_type_claim_count").html();
        			                obj.empty               = $("#manual_call_type_empty_count").html();
        			                obj.other               = $("#manual_call_type_other_count").html();
        			                
        			                $.ajax({
        			                    url: aJaxURL,
        			                    data:obj,
        			                    success:function(data){
        			                    	$("#hidde_monitoring_id").val('');
        			                    	$("#add-edit-form-gasanawilebeli").dialog('close');
        			                    	LoadTable('index', colum_number, main_act, change_colum_main, obj);
        			            		}
        			                });
        			            }
        			        },
        			        "cancel": {
        			            text: "გაუქმება",
        			            id: "cancel-dialog",
        			            click: function () {
        			                $(this).dialog("close");
        			                $("#hidde_monitoring_id").val('');
        			            }
        			        }
        			    };
        			
        			GetDialog("add-edit-form-gasanawilebeli", 1225, "auto", buttons, 'center top');
        			
        			
        			$("#monitoring_users").attr('disabled','disabled');
    				$("#monitoring_users").trigger("chosen:updated");
    				GetDateTimes2("start_date_dialog");
    				GetDateTimes2("end_date_dialog");
    				
        			
        			
        			$("#filter_button_dialog, #ganawileba").button();

        			check = "check_all";
        			$("#" + check).on("click", function () {
        		    	$("#table_manual_dialog_table INPUT[type='checkbox']").prop("checked", $("#" + check).is(":checked"));
        		    });

        			$('#formirebis_tarigi').html(data.former_date);
        			$('#formeruser_name').html(data.former);
        			$("#filter_button_dialog").css('display','');
        			$('#start_date_dialog').val(data.start);
        			$('#end_date_dialog').val(data.end);
        			if($("#show_monitoring_table").attr('check_show') == 1){
                    	$('#show_table_div').css('display','');
                		LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, $("#hidde_monitoring_id").val());
                	}else{
                		$('#show_table_div').css('display','none');
                	}
                }
            });
	    });

	    
	    $(document).on("click", "#gasachivreba", function () {
	    	table_hidde_id = $("#monit_hidde_id").val();
	    	
	    	obj     = new Object;
	    	obj.act = "get_add_page_gasachivreba";
            obj.id  = table_hidde_id;
            $.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                    $("#add-edit-form-gasachivreba-dialog").html(data.page);
                    var buttons = {
        					"save": {
        			            text: "შენახვა",
        			            id: "save-dialog-gasachivreba",
        			            click: function () {
        			            	obj     = new Object;
        			    	    	obj.act = "save-dialog-gasachivreba";
        			                obj.id  = $("#monit_hidde_id").val();

        			                obj.monitoring_appealing_operator = $("#monitoring_appealing_operator").attr('user_id');
        			                obj.operator_yes_no               = $("#operator_appealing_yes").val();
        			                obj.operator_appealing_comment    = $("#operator_appealing_comment").val();
        			                
        			                obj.monitoring_appealing_manager  = $("#monitoring_appealing_manager").attr('user_id');
        			                obj.manager_yes_no                = $("#manager_appealing_yes").val();
        			                obj.manager_appealing_comment     = $("#manager_appealing_comment").val();

        			                obj.monitoring_appealing_call_center_manager = $("#monitoring_appealing_call_center_manager").attr('user_id');
        			                obj.call_center_yes_no                       = $("#call_center_manager_appealing_yes").val();
        			                obj.call_center_manager_appealing_comment    = $("#call_center_manager_appealing_comment").val();
        			                
        			                
        			                $.ajax({
        			                    url: aJaxURL,
        			                    data:obj,
        			                    success:function(data){
        			                    	$("#hidde_monitoring_id").val('');
        			                    	
        			                    	$("#add-edit-form-gasachivreba-dialog").dialog('close');
        			                    	if(data.check == 0){
        			                    		$("#add-edit-form").dialog('close');
        			                    	}else if(data.check == 1){
            			                    	$(".question_comment_idle, .radio").attr('disabled', false);
        			                    	}
        									var tab = $("li[id^='t_'][aria-selected=true]").attr('name');
        									
        									if(tab == 2){
        							    		LoadTable('shesafasebeli', 13, "get_list_shesafasebeli", change_colum_main, obj);
        								    }else if(tab == 3){
        								    	LoadTable('shemowmebuli', 13, "get_list_shemowmebuli", change_colum_main, obj);
        								    }else if(tab == 4){
        								    	LoadTable('gasachivrebuli', 15, "get_list_gasachivrebuli", change_colum_main, obj);
        									}else if(tab == 5){
        										LoadTable('dasrulebuli', 15, "get_list_dasrulebuli", change_colum_main, obj);
        									}
        			            		}
        			                });
        			            }
        			        },
        			        "cancel": {
        			            text: "გაუქმება",
        			            id: "cancel-dialog",
        			            click: function () {
        			                $(this).dialog("close");
        			            }
        			        }
        			    };
        			
        			GetDialog("add-edit-form-gasachivreba-dialog", 1020, "auto", buttons, 'center top');
        			$("#operator_appealing_yes, #manager_appealing_yes, #call_center_manager_appealing_yes").chosen();
                }
            });
	    });
	    
	    $(document).on("dblclick", "#table_index tr.k-state-selected", function () {
	    	var grid = $("#table_index").data("kendoGrid");
            var dItem = grid.dataItem($(this));

            $("#hidde_monitoring_id").val(dItem.id);
           
            if(dItem.id == ''){
                return false;
            }
	    	obj     = new Object;
	    	obj.act = "get_add_page";
            obj.id  = dItem.id;
            $.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                    $("#gasanawilebeli_html").html(data.page);
                    var buttons = {
        					"save": {
        			            text: "შენახვა",
        			            id: "save-dialog-gasanawilebeli",
        			            click: function () {
        			            }
        			        },
        			        "cancel": {
        			            text: "გაუქმება",
        			            id: "cancel-dialog",
        			            click: function () {
        			                $(this).dialog("close");
        			                $("#hidde_monitoring_id").val('');
        			            }
        			        }
        			    };
        			
        			GetDialog("add-edit-form-gasanawilebeli", 1180, "auto", buttons, 'center top');

        			GetDate("start_date_dialog");
        			GetDate("end_date_dialog");
        			$("#filter_button_dialog, #ganawileba").button();
        			if($("#show_monitoring_table").attr('check_show') == 1){
                    	$('#show_table_div').css('display','');
                    	LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, $("#hidde_monitoring_id").val());
                	}else{
                		$('#show_table_div').css('display','none');
                	}
        			$("#table_manual_dialog_table_wrapper .ui-dialog .ui-widget-header").css('background', '');
        			check = "check_all";
        			$("#" + check).on("click", function () {
        		    	$("#table_manual_dialog_table INPUT[type='checkbox']").prop("checked", $("#" + check).is(":checked"));
        		    });
        			$('#formirebis_tarigi').html(data.former_date);
        			$('#formeruser_name').html(data.former);
        			$('#start_date_dialog').val(data.start);
        			$('#end_date_dialog').val(data.end);
        			$("#filter_button_dialog").css('display','none');
                }
            });
	    });

	    $(document).on("dblclick", "#table_shesafasebeli tr.k-state-selected, #table_shemowmebuli tr.k-state-selected, #table_gasachivrebuli tr.k-state-selected, #table_dasrulebuli tr.k-state-selected", function () {

	    	tab_id = $("li[id^='t_'][aria-selected=true]").attr('name');
	    	
	    	if(tab_id == 2){
	    		var table = $("#table_shesafasebeli").data("kendoGrid");
		   		hide = table.dataItem($(this));
	    	}else if(tab_id == 3){
	    		var table = $("#table_shemowmebuli").data("kendoGrid");
		   		hide = table.dataItem($(this));
	    	}else if(tab_id == 4){
	    		var table = $("#table_gasachivrebuli").data("kendoGrid");
		   		hide = table.dataItem($(this));
	    	}else if(tab_id == 5){
	    		var table = $("#table_dasrulebuli").data("kendoGrid");
		   		hide = table.dataItem($(this));
	    	}
	    	
		    obj         = new Object;
	    	obj.act     = "get_edit_page";
            obj.id      = hide.id;
	    	obj.tab_id  = tab_id;
            $.ajax({
                url: aJaxURL,
                data:obj,
                success:function(data){
                    $("#add-edit-form").html(data.page);
                    var buttons = {
                        "open": {
                                text: "დისციპლინა",
                                id: "open_disciplin",
                                click: function () {
                                    console.log(12);
                                }
                        },
    					"save": {
    			            text: "შენახვა",
    			            id: "save-dialog-monitoring",
    			            click: function () {
    			            }
    			        },
    			        "cancel": {
    			            text: "გაუქმება",
    			            id: "cancel-dialog",
    			            click: function () {
    			                $(this).dialog("close");
    			            }
    			        }
    			    };
    			    
        			GetDialog("add-edit-form", 1585, "auto", buttons, 'center top');
        			$("[data-select='chosen']").chosen({ search_contains: true });
                    $("[data-button='jquery-ui-button']").button();
                    $("#dep_0, #dep_project_0, #dep_sub_project_0, #dep_sub_type_0").chosen();
                    
                    $(".fieldset_row select").chosen();
                    $(".fieldset_row .chosen-container").css('width','182px');
                    $("#dep_0_chosen,#dep_project_0_chosen,#dep_sub_project_0_chosen,#dep_sub_type_0_chosen").css('width','110px')
        		
                	$("#region_chosen, #municip_chosen, #temi_chosen, #village_chosen").css('width','135px');
					$("#add_dep, #get_dep_additional, #get_docs_project, #get_contact_project").css('display', 'none');
					
                    $( "#info input").prop( "disabled", true );
                    $( "#info textarea").prop( "disabled", true );
                    $( "#info select").prop( "disabled", true ).trigger("chosen:updated");
                    
					$("#monitoring_coment").autocomplete("server-side/seoy/monitoring_comment.action.php", {
						width: 300,
						multiple: true,
						matchContains: true
					});
        			
        		}
            });
	    });

	    $(document).on('click','.dialog_input_tabs li',function(){
            ///SWITCHING TABS HERE///
            var field_id = $(this).parent().attr('id');
            var tab_id   = $(this).attr('data-id');
            $('#'+field_id+' li').removeAttr('aria-selected');
            $(this).attr('aria-selected','true');
            /////////////////////////
            $("."+field_id).css('display','none');

            $("."+field_id+"[data-tab-id='"+tab_id+"']").css('display','block');

        });
        
	    function displayAmountStandartSubCat(status) {
			$("#card_type_id_holder").css({
				display: status
			});
			$("#for_card_type_id").css({
				display: status
			});
		}

	    function displayAmountInOutSubCat(status) {
			$("#for_transaction_date_time").css({
				display: status
			});
			$("#transaction_date_time").css({
				display: status
			});
			$("#for_card_16_number").css({
				display: status
			});
			$("#card_16_number_holder").css({
				display: status
			});
		}
		
	    $(document).on("click", ".sport", function () {
	    	GetTabs('tabs_sport_monitoring');
		    $("#tab_sport0").click();
	    });

	    $(document).on("click", "#tab_sport0", function () {
		    $("#tab_sport-0").css('display', '');
		    $("#tab_sport-1").css('display', 'none');
			GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc",'',"<'F'lip>");
			$("#table_sport_length").css('top','0px');
		});

		$(document).on("click", "#tab_sport1", function () {
			$("#tab_sport-1").css('display', '');
		    $("#tab_sport-0").css('display', 'none');
			GetDataTable("table_sport1", aJaxURLsport, "get_list_archive", 7, "", 0, "", 0, "desc",'',"<'F'lip>");
			$("#table_sport1_length").css('top','0px');
		});
		
	    $(document).on("click", ".sms", function () {
	    	
	    	GetDataTable("table_sms", aJaxURL_inc, "get_list_sms", 5, "&incomming_id="+$('#hidden_call_id').val(), 0, "", 2, "desc",'',"<'F'lip>");
	    	$("#table_sms_length").css('top','0px');
	    });

	    $(document).on("click", ".crm", function () {
		    $("#check_ab_crm_pin").val($("#personal_pin").val());
	    	GetDataTable("table_crm", aJaxURL_inc, "get_list_crm", 9, "start_crm="+$("#start_crm").val()+"&end_crm="+$("#end_crm").val()+"&pin="+$("#check_ab_crm_pin").val()+"&phone="+$("#check_ab_crm_phone").val(), 0, [[50, -1], [50, "ყველა"]], 0, "desc", "", "<'F'lip>");
	    	$("#table_crm_length").css('top','0px');
			$("#search_ab_crm_pin").button();
			GetDate('start_crm');
			GetDate('start_crm');
			$('.ColVis, .dataTable_buttons').css('display', 'none');
		});
		
	    $(document).on("click", "#show_monitoring_table", function () {
	    	var start_date = $("#start_date_dialog").val();
			var end_date = $("#end_date_dialog").val();

	    	if($(this).attr('check_show') == 0){
		    	$("#show_monitoring_table").html('დახურე ცხრილი');
				$(this).attr('check_show', '1');
				$("#show_table_div").css('display','');
			}else{
				$("#show_monitoring_table").html('მაჩვენე ცხრილი');
				$(this).attr('check_show', '0');
				$("#show_table_div").css('display','none');
			}

	    	LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, $("#hidde_monitoring_id").val());
		});

	    $(document).on("click", "#ganawileba", function () {
	    	var count = 0;
	    	kendo_RowNum("table_manual_dialog_table", function(i){
				count = i;
			});
	    	var buttons = {
				"save": {
		            text: "შენახვა",
		            id: "save-dialog-ganawileba",
		            click: function () {
		            }
		        },
		        "cancel": {
		            text: "გაუქმება",
		            id: "cancel-dialog",
		            click: function () {
		                $(this).dialog("close");
		            }
		        }
		    };
		    
	    	GetDialog("add-edit-form-ganawileba", 450, "auto", buttons, 'center');
	    	$("#monitoring_users").chosen();
	    	$("#add-edit-form-ganawileba, .add-edit-form-ganawileba-class").css('overflow','visible');
	    	$("#checked_count").html(count);
	    });

		$(document).on("click", "#search_ab_crm_pin", function () {
		    
	    	GetDataTable("table_crm", aJaxURL_inc, "get_list_crm", 9, "start_crm="+$("#start_crm").val()+"&end_crm="+$("#end_crm").val()+"&pin="+$("#check_ab_crm_pin").val()+"&phone="+$("#check_ab_crm_phone").val(), 0, [[50, -1], [50, "ყველა"]], 0, "desc", "", "<'F'lip>");
	    	$("#table_crm_length").css('top','0px');
			$('.ColVis, .dataTable_buttons').css('display', 'none');
		});

		$(document).on("click", "#search_ab_pin", function () {

			GetDataTable("table_history", aJaxURL, "get_list_history", 9, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&check_ab_pin="+$("#check_ab_pin").val(), 0, "", 2, "desc", '', "<'F'lip>");

			$("#table_history_length").css('top','0px');
			$("#table_history_wrapper").css('width','650px');
			
		});

		$(document).on("click", "#tanabrad", function () {
			if($('input[id=tanabrad]:checked').val() == 1){
				$("#monitoring_users").attr('disabled','disabled');
				$("#monitoring_users").trigger("chosen:updated");
			}else{
				$("#monitoring_users").attr('disabled',false);
				$("#monitoring_users").trigger("chosen:updated");
			}
			
		});

		$(document).on("click", "#manualurad", function () {
			if($('input[id=manualurad]:checked').val() == 2){
				$("#monitoring_users").attr('disabled',false);
				$("#monitoring_users").trigger("chosen:updated");
			}else{
				$("#monitoring_users").attr('disabled','disabled');
				$("#monitoring_users").trigger("chosen:updated");
			}
			
		});

		$(document).on("click", "#filter_button", function () {
			LoadTable('index', colum_number, main_act, change_colum_main, obj);
		});

		$(document).on("click", "#filter_button1", function () {
			LoadTable('dasrulebuli', 15, "get_list_dasrulebuli", change_colum_main, obj);
		});

		$(document).on("click", "#filter_button_shemowmebuli", function () {
			start_date = $('#start_date_dasrulebuli').val();
            end_date   = $('#end_date_dasrulebuli').val();
            
            obj            = new Object;
            obj.act        = "get_shemowmebuli_result";
            obj.start_date = start_date;
            obj.end_date   = end_date;
            $.ajax({
                url: aJaxURL,
                data:obj,
                success: function (data){
                    $("#shemowmebuli_request_count").html(data.page.shemowmebuli_request_count);
                    $("#shemowmebuli_shedegi_request_count").html(data.page.shemowmebuli_shedegi_request_count);
					$("#shemowmebuli_action_count_1").html(data.page.shemowmebuli_action_count_1);
                    $("#shemowmebuli_action_count_2").html(data.page.shemowmebuli_action_count_2);
                    $("#shemowmebuli_action_count_3").html(data.page.shemowmebuli_action_count_3);
                    $("#shemowmebuli_action_count_4").html(data.page.shemowmebuli_action_count_4);
					$("#shemowmebuli_action_count_all").html(data.page.shemowmebuli_action_count_all);
                    $("#shemowmebuli_auto_rate_count").html(data.page.shemowmebuli_auto_rate_count);
                    $("#shemowmebuli_call_type_906").html(data.page.shemowmebuli_call_type_906);
                    $("#shemowmebuli_shedegi_call_type_906").html(data.page.shemowmebuli_shedegi_call_type_906);
                    $("#shemowmebuli_call_type_936").html(data.page.shemowmebuli_call_type_936);
                    $("#shemowmebuli_shedegi_call_type_936").html(data.page.shemowmebuli_shedegi_call_type_936);
                    $("#shemowmebuli_call_type_952").html(data.page.shemowmebuli_call_type_952);
                    $("#shemowmebuli_shedegi_call_type_952").html(data.page.shemowmebuli_shedegi_call_type_952);
                    $("#shemowmebuli_call_type_1138").html(data.page.shemowmebuli_call_type_1138);
                    $("#shemowmebuli_shedegi_call_type_1138").html(data.page.shemowmebuli_shedegi_call_type_1138);
                    $("#shemowmebuli_call_type_1312").html(data.page.shemowmebuli_call_type_1312);
                    $("#shemowmebuli_shedegi_call_type_1312").html(data.page.shemowmebuli_shedegi_call_type_1312);
                    $("#shemowmebuli_call_type_all").html(data.page.shemowmebuli_call_type_all);
                    $("#shemowmebuli_shedegi_call_type_all").html(data.page.shemowmebuli_shedegi_call_type_all);
					$("#shemowmebuli_duration_count_1").html(data.page.shemowmebuli_duration_count_1);
                    $("#shemowmebuli_duration_count_2").html(data.page.shemowmebuli_duration_count_2);
                    $("#shemowmebuli_duration_count_3").html(data.page.shemowmebuli_duration_count_3);
                    $("#shemowmebuli_duration_count_4").html(data.page.shemowmebuli_duration_count_4);
					$("#shemowmebuli_duration_count_5").html(data.page.shemowmebuli_duration_count_5);
                    $("#shemowmebuli_duration_count_10").html(data.page.shemowmebuli_duration_count_10);
                    $("#shemowmebuli_duration_count_15").html(data.page.shemowmebuli_duration_count_15);
					$("#shemowmebuli_duration_count_all").html(data.page.shemowmebuli_duration_count_all);
					$("#shemowmebuli_manual_rate_count").html(data.page.shemowmebuli_manual_rate_count);
                    $("#shemowmebuli_operators_count_all").html(data.page.shemowmebuli_operators_count_all);
					$("#shemowmebuli_operators_filter").html(data.page.shemowmebuli_operators_filter);
                    $("#shemowmebuli_operators_filter_count").html(data.page.shemowmebuli_operators_filter_count);
                    $("#shemowmebuli_source_count_1").html(data.page.shemowmebuli_source_count_1);
                    $("#shemowmebuli_shedegi_source_count_1").html(data.page.shemowmebuli_shedegi_source_count_1);
                    $("#shemowmebuli_source_count_4").html(data.page.shemowmebuli_source_count_4);
                    $("#shemowmebuli_shedegi_source_count_4").html(data.page.shemowmebuli_shedegi_source_count_4);
                    $("#shemowmebuli_source_count_6").html(data.page.shemowmebuli_source_count_6);
                    $("#shemowmebuli_shedegi_source_count_6").html(data.page.shemowmebuli_shedegi_source_count_6);
                    $("#shemowmebuli_source_count_7").html(data.page.shemowmebuli_source_count_7);
                    $("#shemowmebuli_shedegi_source_count_7").html(data.page.shemowmebuli_shedegi_source_count_7);
                    $("#shemowmebuli_source_count_9").html(data.page.shemowmebuli_source_count_9);
                    $("#shemowmebuli_shedegi_source_count_9").html(data.page.shemowmebuli_shedegi_source_count_9);
                    $("#shemowmebuli_source_count_11").html(data.page.shemowmebuli_source_count_11);
                    $("#shemowmebuli_shedegi_source_count_11").html(data.page.shemowmebuli_shedegi_source_count_11);
					$("#shemowmebuli_source_count_all").html(data.page.shemowmebuli_source_count_all);
					$("#shemowmebuli_shedegi_source_count_all").html(data.page.shemowmebuli_shedegi_source_count_all);
                }
            });
            LoadTable('shemowmebuli', 13, "get_list_shemowmebuli", change_colum_main, obj);
		});

		function kendo_SelectedRows(kendoID, callback){

		    const selected = [];
		    const grid = $("#"+kendoID).data("kendoGrid");
		    const rows = grid.select();
		    rows.each(function(index, row) {
		        const item = grid.dataItem(row);
		        selected.push(item.id);
		    });

		    callback(selected);
		}

		function kendo_RowNum(kendoID, callback){

		    const selected = [];
		    const grid = $("#"+kendoID).data("kendoGrid");
		    const rows = grid.select();
		    var i = 0;
		    rows.each(function(index, row) {
		        i++;
		    });
            i=i/2;
		    callback(i);
		}
		
		
		$(document).on("click", "#save-dialog-ganawileba", function () {
			
			var requests;
			kendo_SelectedRows("table_manual_dialog_table", function(id){
				requests = id;
			});
			
			param = new Object();
            param.act                 = "change_responsible_person";
            param.type                = $('input[name=ganawileba_radio]:checked').val();
            param.monitoring_user_id  = $('#monitoring_users').val();
            param.checked_count       = $('#checked_count').html();
            param.hidde_monitoring_id = $('#hidde_monitoring_id').val();
            param.ids                 = requests;
            
	    	var link = GetAjaxData(param);
	    	
	    	if($("#checked_count").html()==0){
		    	alert('მონიშნულის სია ცარიელი');
	    	}else if($('input[id=manualurad]:checked').val() == 1 && $("#monitoring_users").val() == 0){
		    	alert('აირჩიე ოპერატორი');
	    	}else{
                $.ajax({
                    url: aJaxURL,
                    data:link,
                    success:function(data){
                    	if(typeof(data.error) != 'undefined'){
                        	if(data.error != ''){
                                alert(data.error);
    						}else{
                                $("#add-edit-form-ganawileba").dialog('close');
                                var start_date = $("#start_date_dialog").val();
                    			var end_date = $("#end_date_dialog").val();
                    			LoadTable('manual_dialog_table', 11, "get_list_manual_dialog_table", change_colum_main, obj, $("#hidde_monitoring_id").val());
    						}
                        }
					}
                });
	    	}
	    });
	    
		
	    function listen(file){
	    	$('#auau').each(function(){
	    	    this.pause(); // Stop playing
	    	    this.currentTime = 0; // Reset time
	    	});
	        var url = 'http://91.233.15.136:8181/' + file;
	        $("#auau source").attr('src',url);
	        $("#auau").load();
	    }

        function listenOutgoing(file) {
            $('#auau').each(function() {
                this.pause(); // Stop playing
                this.currentTime = 0; // Reset time
            });
            var url = 'http://172.16.0.80:8000/' + file;
            $("#auau source").attr('src', url);
            $("#auau").load();
        }


	    $(document).on("click", ".shesamowmebeli_request_filter, .shesamowmebeli_call_type_filter, .shesamowmebeli_call_source_filter, .shesamowmebeli_action_filter, .shesamowmebeli_duration_filter, .shemowmebuli_operator_filter", function () {
	    	var request_distribution_type = $(".shesamowmebeli_request_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_type = $(".shesamowmebeli_call_type_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_source = $(".shesamowmebeli_call_source_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_action = $(".shesamowmebeli_action_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_duration = $(".shesamowmebeli_duration_filter:checked").map(function () {
	            return this.value;
	        }).get();

	    	var request_operators = $(".shemowmebuli_operator_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	
	    	GetDataTable("table_shesafasebeli", aJaxURL, "get_list_shesafasebeli", 13, "request_distribution_type="+request_distribution_type+"&request_type="+request_type+"&request_source="+request_source+"&request_action="+request_action+"&request_duration="+request_duration+"&request_operators="+request_operators, 0, "", 1, "desc", "", change_colum_main);
	    	$('.ColVis, .dataTable_buttons').css('display','none');
	    });

	    $(document).on("click", ".shemowmebuli_request_filter, .shemowmebuli_call_type_filter, .shemowmebuli_call_source_filter, .shemowmebuli_action_filter, .shemowmebuli_duration_filter, .shemowmebuli_operator_filter", function () {
	    	var request_distribution_type = $(".shesamowmebeli_request_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_type = $(".shemowmebuli_call_type_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_source = $(".shemowmebuli_call_source_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_action = $(".shemowmebuli_action_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	var request_duration = $(".shemowmebuli_duration_filter:checked").map(function () {
	            return this.value;
	        }).get();

	    	var request_operators = $(".shemowmebuli_operator_filter:not(:checked)").map(function () {
	            return this.value;
	        }).get();

	    	start_date = $('#start_date_dasrulebuli').val();
            end_date   = $('#end_date_dasrulebuli').val();
	    	GetDataTable("table_shemowmebuli", aJaxURL, "get_list_shemowmebuli", 13, "request_distribution_type="+request_distribution_type+"&request_type="+request_type+"&request_source="+request_source+"&request_action="+request_action+"&request_duration="+request_duration+"&request_operators="+request_operators+"&start_date="+start_date+"&end_date="+end_date, 0, "", 1, "desc", "", change_colum_main);
	    	$('.ColVis, .dataTable_buttons').css('display','none');
	    });
		
	    function play(record){
	    	var buttons = {
	            	"cancel": {
	    	            text: "დახურვა",
	    	            id: "cancel-dialog",
	    	            click: function () {
	    	            	$('audio').each(function(){
	    	            	    this.pause(); // Stop playing
	    	            	    this.currentTime = 0; // Reset time
	    	            	}); 
	    	            	$(this).dialog("close");
	    	            }
	    	        }
	    	    };
			link = 'http://91.233.15.136:8181/' + record;
			GetDialog_audio("audio_dialog", "auto", "auto", buttons );
			$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download").css( "background","#408c99" );
            $(this).css( "background","#FF5555" )
        }


        function counters(){
            $.ajax({
                url:aJaxURL,
                data:{
                    act:"get_counters",
                    start:$("#start_date_main1").val(),
                    end:$("#end_date_main1").val()
                },
                success:function(data){
                    $(".quality").html(data.quality);
                    $(".attendance").html(data.attendance);
                    $(".discipline").html(data.discipline);
                    $(".quality .counter_indicator div").css("width",`${Number($(".quality .counter_number").html().substr(0,5)) < 100 ? Number($(".quality .counter_number").html().substr(0,5)) : 100 }%`);
                    $(".attendance .counter_indicator div").css("width",`${Number($(".attendance .counter_number").html().substr(0,5))  < 100 ? Number($(".attendance .counter_number").html().substr(0,5)) : 100}%`);
                    $(".discipline .counter_indicator div").css("width",`${Number($(".discipline .counter_number").html().substr(0,5))  < 100 ? Number($(".discipline .counter_number").html().substr(0,5)) : 100}%`);
                    $("#tabs").css('display','inline-block');
                }
            })
        }
</script>
</head>

<body>
<div id="tabs" style="width: 80%; display: none; position: absolute;">
        <div class="callapp_head">მონიტორინგი<hr class="callapp_head_hr"></div>
        <div id="add-edit-form-disciplin" class="form-dialog" title="დისციპლინა" style="display: block;">
        <!-- aJax -->
        </div>
        	<div id="tabs1"></div>
            <div id="tab-0">
            	<div class="callapp_filter_body" myvar="1">
                    <div id="filter_div1" style="height: 50px;">
                        <input style="width: 140px;" type="text" id="start_date_main1" value="<?php echo date('Y-m-d', strtotime('-7 day')).' 00:00'; ?>">-დან
                        <input style="width: 140px;" type="text" id="end_date_main1" value="" style="margin-left: 15px;">-მდე
                        <button id="filter_button_main">ფილტრი</button>
                    </div>
                </div>
            
                <div class="counters">
                    <div class="quality shadow relative">
                        
                        

                    </div>
                    <div class="attendance shadow relative">
                        
                    </div>
                    <div class="discipline shadow relative">
                       
                    </div>
                    
                </div>

                <div class="monitor-grid">
                    <div class="row operator_row " > 
                        <div style="font-size:25px">ოპერატორი</div>
                        
                    </div>
                    <div class="row quality_row" > 
                        <div style="font-size:25px">Kხრსხ</div>
                        
                    </div>
                    <div class="row attendance_row" > 
                        <div style="font-size:25px">Kდსწრ</div>

                    </div>
                    <div class="row discipline_row" > 
                        <div style="font-size:25px">Kდსცპ</div>

                    </div>
                    <div class="row r_row shadow" > 
                        <div style="font-size:25px">R</div>

                    </div>
                </div>
            </div>
            <div id="tab-1">
                
                <div class="callapp_filter_body" myvar="1">
                    <div id="filter_div" style="height: 50px;">
                        <input style="width: 140px;" type="text" id="start_date" value="">-დან
                        <input style="width: 140px;" type="text" id="end_date" value="" style="margin-left: 15px;">-მდე
                        <button id="filter_button">ფილტრი</button>
                    </div>
                </div>
                
                <div><button id="add_new_monitoring">დამატება</button></div>
                <div style="width: 100%; position: relative;">
                	<div style="width: 55%; float:left;" id="table_index"></div>
                	<div style="width: 44%; float:right;" id="monitoring_table_detail_info"></div>
                </div>
                
            </div>
            <div id="tab-2">
                
                    <div class="callapp_filter_body1" myvar1="1">
                        <div id="filter_div" style="height: 250px; overflow-y: scroll; display: none;">
                            <div style="width: 1250px; display: inline-flex; font-family: pvn; font-weight: bold;">
                                <div style="width: 19%;">
                                	<a style="width: 100%;">მომართვები</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shesamowmebeli_request_filter" type="checkbox" checked value="1">
                                            </td>
                                            <td style="width: 85px;">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ავტ. დაფორმირებული</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_request_filter" type="checkbox" checked value="2">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">მანუალ. დაფორმირებული</label>
                                            </td>
                                        </tr>
                                        <tr style="height:60px"></tr>
                                        <tr>
                                            <td colspan="2">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:center">შესრულების სავადაუდო ხ-ბა</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:center; font-size:15px;">00:00:00</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 4%;">
                                	<a id="shesamowmebeli_request_count" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_auto_rate_count"  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_manual_rate_count"  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 9%;">
                                    <a style="width: 100%;">ტიპის მიხედვით</a>
                                    <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shesamowmebeli_call_type_filter" type="checkbox" checked value="906">
                                            </td>
                                            <td style="width: 85px;">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ინფორმაცია</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_type_filter" type="checkbox" checked value="936">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">პრობლემა</label>
                                            </td>
                                       </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_type_filter" type="checkbox" checked value="952">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">პრეტენზია</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_type_filter" type="checkbox" checked value="1138">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0;">------------</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_type_filter" type="checkbox" checked value="1312">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">სხვა</label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 4%;">
                                	<a id="shesamowmebeli_call_type_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_call_type_906"  style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_call_type_936" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_call_type_952" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_call_type_1138" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_call_type_1312" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </div>
                                <div style="width: 11%;">
                                <a style="width: 100%;">წყაროების მიხედვით</a>
                                    <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input  class="shesamowmebeli_call_source_filter" type="checkbox" checked value="1">
                                            </td>
                                            <td style="width: 85px;">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ტელეფონი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_source_filter" type="checkbox" checked value="4">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ჩათი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_source_filter" type="checkbox" checked value="11">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">მესენჯერი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_source_filter" type="checkbox" checked value="9">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ვაიბერი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_source_filter" type="checkbox" checked value="6">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">FB მესენჯერი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_call_source_filter" type="checkbox" checked value="7">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ელ-ფოსტა</label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 4%;">
                                	<a id="shesamowmebeli_source_count_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_source_count_1" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_source_count_4" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_source_count_11" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_source_count_9" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_source_count_6" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_source_count_7" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div style="width: 11%;">
                                    <a style="width: 100%;">ქმედებების მიხედვით</a>
                                    <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input  class="shesamowmebeli_action_filter" type="checkbox" checked value="2">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">დავალება</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_action_filter" type="checkbox" checked value="4">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">დაბლოკილები</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_action_filter" type="checkbox" checked value="3">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ჰოლდი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_action_filter" type="checkbox" checked value="1">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">გამავალი</label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 4%;">
                                	<a id="shesamowmebeli_action_count_all" style="width: 100%; font-family: pvn; font-weight: bold;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_action_count_2" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_action_count_4" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_action_count_3" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_action_count_1" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 10%;">
                                <a style="width: 100%;">ხ-ბების მიხედვით</a>
                                <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="1">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">1 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="2">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">2 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="3">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">3 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="4">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">4 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="5">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">5 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="10">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">10 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shesamowmebeli_duration_filter" type="radio" name="shesamowmebeli_radio" value="15">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">15 წთ ></label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 4%;">
                                	<a id="shesamowmebeli_duration_count_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_1" style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_2" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_3" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_4" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_5" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_10" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shesamowmebeli_duration_count_15" style="padding: 7px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 17%; display: inline-flex; overflow-y: auto; height: 220px;">
                                    <div style="width: 80%;">
                                        <a style="width: 100%;">ოპერატორების მიხედვით</a>
                                        <table id="shesamowmebeli_operators_filter" style="width: 182px"></table>
                                    </div>
                                    <div style="width: 20%;">
                                    	<a id="shesamowmebeli_operators_count_all" style="width: 100%;">0</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
            	<div id="table_shesafasebeli" style="margin-top: 30px; width: 100%;"></div>
            </div>
            <div id="tab-3">
                
               <div class="callapp_filter_body2" myvar2="1" >
                    	<div id="filter_div" style="height: 250px; overflow-y: auto; margin-bottom: 10px; display: none;">
                        	<div style="width: 1250px; display: inline-flex;">
                        		<div style="width: 17%;"></div>
                        		<div style="width: 3%;"></div>
                        		<div style="width: 3%;"></div>
                        		<div style="width: 9%;"></div>
                        		<div style="width: 3%; font-size: 8px;">სულ</div>
                        		<div style="width: 4%; font-size: 8px;">შედეგი</div>
                        		<div style="width: 11%;"></div>
                        		<div style="width: 3%; font-size: 8px;">სულ</div>
                        		<div style="width: 4%; font-size: 8px;">შედეგი</div>
                        		<div style="width: 11%;"></div>
                        		<div style="width: 4%;"></div>
                        		<div style="width: 10%;"></div>
                        		<div style="width: 11%;"></div>
                        		<div style="width: 4%;"></div>
                        	</div>
                            <div style="width: 1250px; display: inline-flex; font-family: pvn; font-weight: bold;">
                                <div style="width: 17%;">
                                	<a style="width: 100%;">მომართვები</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shemowmebuli_request_filter" type="checkbox" checked value="1">
                                            </td>
                                            <td style="width: 85px;">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ავტ. დაფორმირებული</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_request_filter" type="checkbox" checked value="2">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">მანუალ. დაფორმირებული</label>
                                            </td>
                                        </tr>
                                        <tr style="height:7px"></tr>
                                        <tr>
                                            <td colspan="2">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:center">შესრულების სავადაუდო ხ-ბა</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td colspan="2">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; font-size: 15px; text-align:center">00:00:00</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td colspan="2">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:center">შესრულების სავადაუდო ხ-ბა</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; font-size: 15px; text-align:center">00:00:00</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 3%;">
                                	<a id="shemowmebuli_request_count" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_auto_rate_count" style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_manual_rate_count"  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 5%;">
                                	<a id="shemowmebuli_shedegi_request_count" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_auto_rate_count" style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_manual_rate_count" style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 9%;">
                                    <a style="width: 100%;">ტიპის მიხედვით</a>
                                    <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shemowmebuli_call_type_filter" type="checkbox" checked value="906">
                                            </td>
                                            <td style="width: 85px;">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ინფორმაცია</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_type_filter" type="checkbox" checked value="936">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">პრობლემა</label>
                                            </td>
                                       </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_type_filter" type="checkbox" checked value="952">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">პრეტენზია</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_type_filter" type="checkbox" checked value="1138">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0;">------------</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_type_filter" type="checkbox" checked value="1312">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">სხვა</label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 3%;">
                                	<a id="shemowmebuli_call_type_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_call_type_906" style="padding: 4px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_call_type_936" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_call_type_952" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_call_type_1138" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_call_type_1312" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 5%;">
                                	<a id="shemowmebuli_shedegi_call_type_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_call_type_906" style="padding: 4px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_call_type_936" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_call_type_952" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_call_type_1138" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_call_type_1312" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 11%;">
                                <a style="width: 100%;">წყაროების მიხედვით</a>
                                    <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shemowmebuli_call_source_filter" type="checkbox" checked value="1">
                                            </td>
                                            <td style="width: 85px;">
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ტელეფონი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_source_filter" type="checkbox" checked value="4">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ჩათი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_source_filter" type="checkbox" checked value="11">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">მესენჯერი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_source_filter" type="checkbox" checked value="9">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ვაიბერი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_source_filter" type="checkbox" checked value="6">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">FB მესენჯერი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_call_source_filter" type="checkbox" checked value="7">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ელ-ფოსტა</label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 3%;">
                                	<a id="shemowmebuli_source_count_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_source_count_1" style="padding: 3px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_source_count_4" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_source_count_11" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_source_count_9" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_source_count_6" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_source_count_7" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 5%;">
                                	<a id="shemowmebuli_shedegi_source_count_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_source_count_1" style="padding: 3px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">60</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_source_count_4" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_source_count_11" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_source_count_9" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_source_count_6" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_shedegi_source_count_7" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 12%;">
                                    <a style="width: 100%;">ქმედებების მიხედვით</a>
                                    <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shemowmebulu_action_filter" type="checkbox" checked value="2">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">დავალება</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebulu_action_filter"  type="checkbox" checked value="4">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">დაბლოკილები</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebulu_action_filter"  type="checkbox" checked value="3">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">ჰოლდი</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebulu_action_filter"  type="checkbox" checked value="1">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">გამავალი</label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 3%;">
                                	<a id="shemowmebuli_action_count_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_action_count_2" style="padding: 3px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_action_count_4" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_action_count_3" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_action_count_1" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 10%;">
                                <a style="width: 100%;">ხ-ბების მიხედვით</a>
                                <table>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td style="width: 25px;">
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="1">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">1 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="2">
                                            </td>
                                            <td>
                                                <label  style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">2 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="3">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">3 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="4">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">4 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="5">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">5 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="10">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">10 წთ-მდე</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <input class="shemowmebuli_duration_filter" type="radio" name="shesamowmebeli_radio" value="15">
                                            </td>
                                            <td>
                                                <label style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold;">15 წთ ></label>
                                            </td>
                                        </tr>
                                   </table>
                                </div>
                                <div style="width: 3%;">
                                	<a id="shemowmebuli_duration_count_all" style="width: 100%;">0</a>
                                    <table style="width: 100%;">
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_1" style="padding: 2px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_2" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_3" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_4" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_5" style="padding: 5px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_10" style="padding: 6px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                        <tr style="height:5px"></tr>
                                        <tr>
                                            <td>
                                                <label id="shemowmebuli_duration_count_15" style="padding: 7px 0 0 0; font-family: pvn; font-weight: bold; text-align:left">0</label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="width: 17%; display: inline-flex; overflow-y: auto; height: 220px;">
                                    <div style="width: 80%;">
                                        <a style="width: 100%;">ოპერატორების მიხედვით</a>
                                        <table id="shemowmebuli_operators_filter" style="width: 173px">
										</table>
                                    </div>
                                    <div style="width: 20%;">
                                    	<a id="shemowmebuli_operators_count_all" style="width: 100%;">0</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table style="width: 600px">
                    		<tr style="width: 100%;">
                    			<td>
                    				<input style="width: 150px;" type="text" id="start_date_dasrulebuli" value=""  autocomplete="off">
                    			</td>
                    			<td>
                    				<input style="width: 150px;" type="text" id="end_date_dasrulebuli" value=""  autocomplete="off">
                    			</td>
                    			<td>
                    				<input style="width: 150px;" type="text" id="number_dasrulebuli" value=""  autocomplete="off" placeholder="ნომერი">
                    			</td>
                    			<td>
                    				<button id="filter_button_shemowmebuli" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">ფილტრი</span></button>
                    			</td>
                    		</tr>
                    	</table>
                    </div>
                
                
            	<div style="width: 100%;" id="table_shemowmebuli"></div>
            </div>
            <div id="tab-4">
                <table style="width: 415px;margin-bottom: 18px;">
                    <tr style="width: 100%;">
                        <td>
                            <input style="width: 150px;" type="text" id="start_date_g" value=""  autocomplete="off">
                        </td>
                        <td>
                            <input style="width: 150px;" type="text" id="end_date_g" value=""  autocomplete="off">
                        </td>
                        <td>
                            <button id="filter_button_g" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text">ფილტრი</span></button>
                        </td>
                    </tr>
                </table>
                <div class="callapp_filter_show3" style="display:none;">
                	<button id="callapp_show_filter_button3">ფილტრი
                        <div id="shh"
                             style="background: url('media/images/icons/show.png'); width: 24px; height: 9px;background-position: 0 0px;margin-top: 5px;float: right;"></div>
                    </button>
                    <div class="callapp_filter_body3" myvar3="1">
                        <div id="filter_div">
                        	
                        	<div style="width: 1250px; display: inline-flex; font-family: pvn; font-weight: bold;">
                                <div id="gasachivrebuli_result_operator" style="width: 42%; height: 167px; overflow-y: auto;">
                                	
                                </div>
                                <div id="gasachivrebuli_result_operator" style="width: 10%; height: 167px; overflow-y: auto;">
                                	
                                </div>
                                <div id="gasachivrebuli_result_monitoring" style="width: 45%; height: 167px; overflow-y: auto;">
                                	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 100%;" id="table_gasachivrebuli"> </div>
            </div>
            <div id="tab-5">
                <div class="callapp_filter_show4">
                	<button id="callapp_show_filter_button4">ფილტრი
                        <div id="shh" style="background: url('media/images/icons/show.png'); width: 24px; height: 9px;background-position: 0 0px;margin-top: 5px;float: right;"></div>
                    </button>
                    <div class="callapp_filter_body4" myvar4="1">
                        <div id="filter_div" style="height: 45px;">
                        	<div id="filter_div" style="height: 50px;">
                                <input style="width: 150px;" type="text" id="start_date1" value="">-დან
                                <input style="width: 150px;" type="text" id="end_date1" value="" style="margin-left: 15px;">-მდე
                                <button id="filter_button1">ფილტრი</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            	<div style="width: 100%;" id="table_dasrulebuli"></div>
            </div>
       </div>
<div style="padding: .5em 1em;" id="add-edit-form-gasanawilebeli" class="form-dialog" title="მანუალურად ფორმირება">
	<fieldset>
    	<input type="hidden" id="hidde_monitoring_id" value="" />
    	<div style="width: 100%;">
    		<table style="width: 360px;">
    			<tr>
    				<td style="width: 200px"><label>ფორმირების თარიღი</label></td>
    				<td style="width: 200px"><label>დამფორმირებელი</label></td>
    			</tr>
    			<tr style="height: 2px;"></tr>
    			<tr>
    				<td><label style="font-size: 15px;" id="formirebis_tarigi"></label></td>
    				<td><label style="font-size: 15px;" id="formeruser_name"></label></td>
    			</tr>
    		</table>
    	</div>
    	<hr>
    	<div style="width: 100%;">
    		<div class="callapp_filter_show4">
            	<button id="callapp_show_filter_dialog">ფილტრი
                    <div id="shh" style="background: url('media/images/icons/show.png'); width: 24px; height: 9px;background-position: 0 0px;margin-top: 5px;float: right;"></div>
                </button>
                <div class="callapp_filter_body_dialog" myvar_dialog="1">
                    <div id="filter_div" style="height: 45px;">
                    	<div id="filter_div" style="height: 50px;">
                            <input style="width: 150px;" type="text" id="start_date_dialog" value="">-დან
                            <input style="width: 150px;" type="text" id="end_date_dialog" value="" style="margin-left: 15px;">-მდე
                            <button id="filter_button_dialog" style="background: #1bc9f8; color:#fff;">ფილტრი</button>
                        </div>
                    </div>
                </div>
            </div>
    	</div>
    	<div style="width: 100%;" id="gasanawilebeli_html">
    	</div>
    	<div id="show_table_div" style="width: 100%; margin-top:20px; display:none;">
            <button id="ganawileba" style="background: #1bc9f8; color:#fff;  margin-bottom:7px;">განაწილება</button>
            <div id="table_manual_dialog_table" style="width: 100%;"></div>
        </div>
    </fieldset>
</div>
<div id="add-edit-form-ganawileba" class="form-dialog" title="მომართვების განაწილება">
	<div style="width: 100%;">
		<table style="width: 100%; height: 90px; margin-top: 10px;">
			<tr>
				<td style="width: 100px;"><label style="font-size: 13px; font-family: pvn; font-weight: bold;">მონიშნულია</label></td>
				<td style="width: 30px;"><label id="checked_count" style="font-size: 13px; font-family: pvn; font-weight: bold; text-align:center;">0</label></td>
				<td style="width: 100px;"><label style="font-size: 13px; font-family: pvn; font-weight: bold;">მომართვა</label></td>
				<td></td>
			</tr>
			<tr style="height: 7px;"></tr>
			<tr>
				<td><label style="font-size: 13px; font-family: pvn; font-weight: bold;">განაწილდეს</label></td>
				<td><input type="radio" name="ganawileba_radio" id="tanabrad" value="1" checked></td>
				<td><label style="font-size: 13px; font-family: pvn; font-weight: bold;">თანაბრად</label></td>
				<td></td>
			</tr>
			<tr style="height: 5px;"></tr>
			<tr>
				<td></td>
				<td><input type="radio" name="ganawileba_radio" id="manualurad" value="2"></td>
				<td><label style="font-size: 13px; font-family: pvn; font-weight: bold;">მანუალურად</label></td>
				<td>
					<select style="width: 180px;" id="monitoring_users" class="idls object">
					<option value="0">აირჩიე</option>
                        <?php
                            global $db;
                            $db = new dbClass();
                            $db->setQuery(" SELECT users.`id`,
                                    			   user_info.`name`
                                            FROM  `users`
                                            JOIN   user_info ON user_info.user_id = users.id
                                            WHERE  users.`actived` = 1 AND users.group_id = 48");
                           
                            $result = $db->getResultArray();
                            
                            foreach ($result[result] AS $req){
                                $data .= "<option value='$req[id]'>$req[name]</option>";
                            }
                            echo $data;
                        ?>
					</select>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id="add-edit-form" class="form-dialog" title="მონიტორინგი"></div>
<div id="add-edit-form-history" class="form-dialog" title="შემომავალი"></div>
<div id="add-edit-form-gasachivreba-dialog" class="form-dialog" title="გასაჩუვრება"></div>
<div id="audio_dialog2" title="ჩანაწერი"></div>
</body>
	