<?php
require_once('class.Mysqli.php');
global $db;
$db = new dbClass();
class automator{
    public function getCampaignInputs($baseID){
        GLOBAL $db;

        $db->setQuery("	SELECT 		compaign_quize_processing.id,
                                    compaign_quize_processing.processing_setting_detail_id AS 'input_id',
                                    compaign_quize_processing.`value`,
                                    compaign_quize_processing.int_value,
                                    outgoing_campaign_inputs.field_type_id AS 'type'
                        FROM 		compaign_quize_processing
                        LEFT JOIN 	outgoing_campaign_inputs ON outgoing_campaign_inputs.id = compaign_quize_processing.processing_setting_detail_id
                        WHERE 		compaign_quize_processing.base_id='$baseID' AND compaign_quize_processing.processing_setting_detail_id != 0 AND compaign_quize_processing.base_id!=0");
        $values = $db->getResultArray();
        $saved_values = $values['result'];

        $db->setQuery(" SELECT  outgoing_campaign_request_id AS 'req_id'
                        FROM    outgoing_campaign_request_base
                        WHERE   id = '$baseID'");
        $req_id = $db->getResultArray();
        $req_id = $req_id['result'][0]['req_id'];
        $data .= $this->get_fieldset_inputs('',$saved_values,$inputs_by_tabs,$phone,$req_id);


        return $data;
    }
    public function getDialog($incomming_call_id,$phone,$mainTable = 'incomming_request_processing',$tableConnector = 'incomming_request_id',$source_id = 1){
        GLOBAL $db;

        $db->setQuery(" SELECT      processing_setting.id
                        FROM        processing_setting_by_source
                        LEFT JOIN   processing_setting ON processing_setting.id = processing_setting_by_source.processing_setting_id
                        
                        WHERE       processing_setting.processing_page_id = 1 AND processing_setting_by_source.actived = 1 AND processing_setting.actived = 1 AND processing_setting_by_source.source_id = '$source_id'");
        $processing_setting_id = $db->getResultArray();
        $processing_setting_id = $processing_setting_id['result'][0]['id'];
        $db->setQuery("	SELECT id,name
                        FROM processing_fieldset
                        WHERE actived = '1' AND id IN (SELECT fieldset_id FROM processing_fieldset_by_setting WHERE processing_setting_id='$processing_setting_id')");
        $fieldsets = $db->getResultArray();
        $db->setQuery("	SELECT 		$mainTable.id,
                                    $mainTable.processing_setting_detail_id AS 'input_id',
                                    $mainTable.`value`,
                                    $mainTable.int_value,
                                    processing_setting_detail.processing_field_type_id AS 'type'
                        
                        FROM 		$mainTable
                        LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id
                        WHERE 		$mainTable.$tableConnector='$incomming_call_id' AND $mainTable.processing_setting_detail_id != 0 AND $mainTable.$tableConnector!=0
                        
                        GROUP BY $mainTable.processing_setting_detail_id");
        $values = $db->getResultArray();
        $saved_values = $values['result'];

        $db->setQuery("	SELECT 		$mainTable.id,
                                    $mainTable.processing_setting_detail_id AS 'input_id',
                                    $mainTable.`value`,
                                    $mainTable.int_value,
                                    processing_setting_detail.processing_field_type_id AS 'type',
                                    $mainTable.additional_deps
                            
                        FROM 		$mainTable
                        LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id
                        WHERE 		$mainTable.$tableConnector='$incomming_call_id' AND $mainTable.processing_setting_detail_id = 0 AND $mainTable.additional_deps = 0 AND $mainTable.$tableConnector!=0");
        $deps = $db->getResultArray();

        $dep_val = 0;
        $dep_project_val = 0;
        $dep_sub_project_val = 0;
        $dep_project_type_val = 0;
        $dep_rand_n = 0;
        foreach($deps['result'] AS $item){
            if($item['value'] == 1){
                $dep_val = $item['int_value'];
            }
            if($item['value'] == 2){
                $dep_project_val = $item['int_value'];
            }
            if($item['value'] == 3){
                $dep_sub_project_val = $item['int_value'];
            }
            if($item['value'] == 4){
                $dep_project_type_val = $item['int_value'];
            }
            
        }
        $counter = 1;




        ///field notification block
        $db->setQuery("	SELECT 	COUNT(*) AS 'cc'
                        FROM 	mepa_project_inputs
                        WHERE 	cat_id = '$dep_project_val' AND actived = 1");
        $field_count = $db->getResultArray();

        $db->setQuery("	SELECT 	region_selector
                        FROM 	info_category
                        WHERE 	id = '$dep_project_val'");
        $regions_on = $db->getResultArray();
        $regions_on = $regions_on['result'][0]['region_selector'];

        if($regions_on == 1){
            $regions_on = 4;
        }

        $db->setQuery("	SELECT
                            $mainTable.id,
                            $mainTable.processing_setting_detail_id AS 'input_id',
                            $mainTable.`value`,
                            $mainTable.int_value,
                            processing_setting_detail.processing_field_type_id AS 'type' 
                        FROM
                        $mainTable
                            LEFT JOIN processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id 
                        WHERE
                        $mainTable.$tableConnector = '$incomming_call_id' 
                        AND $mainTable.$tableConnector!=0
                            AND $mainTable.additional_dialog_id = '0'
                            AND $mainTable.processing_setting_detail_id != 0
                            AND $mainTable.processing_setting_detail_id != 999999999");
        $ready_fields = $db->getResultArray();

        $db->setQuery("	SELECT
                            $mainTable.id,
                            $mainTable.processing_setting_detail_id AS 'input_id',
                            $mainTable.`value`,
                            $mainTable.int_value,
                            processing_setting_detail.processing_field_type_id AS 'type' 
                        FROM
                        $mainTable
                            LEFT JOIN processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id 
                        WHERE
                        $mainTable.$tableConnector = '$incomming_call_id' 
                        AND $mainTable.$tableConnector!=0
                            AND $mainTable.additional_dialog_id = '0'
                            AND $mainTable.processing_setting_detail_id != 0
                            AND $mainTable.processing_setting_detail_id = 999999999
                            AND $mainTable.int_value != 0");
        $regions_cc = $db->getResultArray();
        $fields_cc = $field_count['result'][0]['cc'] + $regions_on;
        
        $r_fields = $ready_fields['count'] + $regions_cc['count'];
        ////////////////////////////

        foreach($regions_cc['result'] AS $item){
            if($item['value'] == 1111){
                $reg = $item['int_value'];
            }
            else if($item['value'] == 2222){
                $municip = $item['int_value'];
            }
            else if($item['value'] == 3333){
                $temi = $item['int_value'];
            }
            else if($item['value'] == 4444){
                $village = $item['int_value'];
            }
        }

        //die('reg: '.$reg);
        foreach($fieldsets['result'] AS $fieldset){
            if($counter == 2){
                $data .= '
                <fieldset>
                    <legend>რეგიონი</legend>
                    <div class="fieldset_row">
                        <div class="col-3">
                            <label>რეგიონი</label>
                            <select id="region">'.$this->get_regions_1($reg).'</select>
                        </div>
                        <div class="col-3">
                            <label>მუნიციპალიტეტი</label>
                            <select id="municip">'.$this->get_regions_1_1($reg,$municip).'</select>
                        </div>
                        <div class="col-3">
                            <label>თემი</label>
                            <select id="temi">'.$this->get_regions_1_1($municip,$temi).'</select>
                        </div>
                        <div class="col-3">
                            <label>სოფელი</label>
                            <select id="village">'.$this->get_regions_1_1($temi,$village).'</select>
                        </div>
                    </div>
                </fieldset>
                <fieldset id="dep_arrival">
                    <legend>მომართვა</legend>
                    <div class="fieldset_row">
                        <div class="col-3-half">
                            <label>უწყება</label>
                            <select id="dep_0">
                            '.$this->get_dep_1($dep_val).'
                            </select>
                        </div>
                        <div class="col-3-half">
                            <label>პროექტი</label>
                            <select id="dep_project_0">
                            '.$this->get_dep_1_1($dep_val,$dep_project_val).'
                            </select>
                        </div>
                        <div class="col-3-half">
                            <label>ქვე პროექტი</label>
                            <select id="dep_sub_project_0">
                            '.$this->get_dep_1_1_1($dep_project_val,$dep_sub_project_val).'
                            </select>
                        </div>
                        <div class="col-3-half">
                            <label>მომართვის ტიპი</label>
                            <select id="dep_sub_type_0">
                            '.$this->get_project_type($dep_project_type_val).'
                            </select>
                        </div>
                        <div class="col-3-half" style="display: flex;">
                            <img style="margin-right:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/add2.png" id="add_dep">
                            <div id="get_dep_additional" style="position: relative;">
                                <div class="field_notifier">
                                    <p><span id="field_done">'.$r_fields.'</span>/<span id="field_no_done">'.$fields_cc.'</span></p>
                                </div>
                                <img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/additional_inputs.png">
                            </div>
                            <img id="get_docs_project" style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/documents.png">
                            <img id="get_contact_project" style="margin-left:1px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/contacts.png">
                        </div>
                    </div>';
                $db->setQuery("	SELECT 			*
                                    
                                FROM 				$mainTable
                                LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id
                                WHERE 			$mainTable.$tableConnector='$incomming_call_id' AND $mainTable.processing_setting_detail_id = 0 AND $mainTable.additional_deps != 0
                                AND $mainTable.$tableConnector!=0
                                GROUP BY $mainTable.additional_deps");

                $peps = $db->getResultArray();
                
                for($o=0;$o<$peps['count'];$o++){
                    $dep_val = 0;
                    $dep_project_val = 0;
                    $dep_sub_project_val = 0;
                    $dep_project_type_val = 0;
                    $dep_rand_n = 0;
                    $dep_rand_n = $peps['result'][$o]['additional_deps'];
                    $db->setQuery("	SELECT 			*
                                        
                                    FROM 				$mainTable
                                    LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id
                                    WHERE 			$mainTable.$tableConnector='$incomming_call_id' AND $mainTable.$tableConnector!=0 AND $mainTable.processing_setting_detail_id = 0 AND $mainTable.additional_deps != 0 AND $mainTable.additional_deps = '$dep_rand_n'");

                    $peps2 = $db->getResultArray();
                    foreach($peps2['result'] AS $item){
                        if($item['value'] == 1){
                            $dep_val = $item['int_value'];
                        }
                        if($item['value'] == 2){
                            $dep_project_val = $item['int_value'];
                        }
                        if($item['value'] == 3){
                            $dep_sub_project_val = $item['int_value'];
                        }
                        if($item['value'] == 4){
                            $dep_project_type_val = $item['int_value'];
                        }
                        
                    }
                    
                    ///field notification block
                    $db->setQuery("	SELECT COUNT(*) AS 'cc'
                                    FROM mepa_project_inputs
                                    WHERE cat_id = '$dep_project_val' AND actived = 1");
                    $field_count = $db->getResultArray();

                    $db->setQuery("	SELECT 	region_selector
                                    FROM 	info_category
                                    WHERE 	id = '$dep_project_val'");
                    $regions_on = $db->getResultArray();
                    $regions_on = $regions_on['result'][0]['region_selector'];

                    if($regions_on == 1){
                        $regions_on = 4;
                    }

                    $db->setQuery("	SELECT
                                        $mainTable.id,
                                        $mainTable.processing_setting_detail_id AS 'input_id',
                                        $mainTable.`value`,
                                        $mainTable.int_value,
                                        processing_setting_detail.processing_field_type_id AS 'type' 
                                    FROM
                                    $mainTable
                                        LEFT JOIN processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id 
                                    WHERE
                                    $mainTable.$tableConnector = '$incomming_call_id' 
                                        AND $mainTable.additional_dialog_id = '$dep_rand_n'
                                        AND $mainTable.processing_setting_detail_id != 0
                                        AND $mainTable.processing_setting_detail_id != 999999999");
                    $ready_fields = $db->getResultArray();

                    $db->setQuery("	SELECT
                                        $mainTable.id,
                                        $mainTable.processing_setting_detail_id AS 'input_id',
                                        $mainTable.`value`,
                                        $mainTable.int_value,
                                        processing_setting_detail.processing_field_type_id AS 'type' 
                                    FROM
                                    $mainTable
                                        LEFT JOIN processing_setting_detail ON processing_setting_detail.id = $mainTable.processing_setting_detail_id 
                                    WHERE
                                    $mainTable.$tableConnector = '$incomming_call_id' 
                                        AND $mainTable.additional_dialog_id = '$dep_rand_n'
                                        AND $mainTable.processing_setting_detail_id != 0
                                        AND $mainTable.processing_setting_detail_id = 999999999
                                        AND $mainTable.int_value != 0");
                    $regions_cc = $db->getResultArray();
                    $fields_cc = $field_count['result'][0]['cc'] + $regions_on;

                    $r_fields = $ready_fields['count'] + $regions_cc['count'];
                    ////////////////////////////

                    $data .= '<div class="fieldset_row" id="dep_row_'.$dep_rand_n.'">
                    <div class="col-3-half">
                        <label>უწყება</label>
                        <select id="dep_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep">
                        '.$this->get_dep_1($dep_val).'
                        </select>
                    </div>
                    <div class="col-3-half">
                        <label>პროექტი</label>
                        <select id="dep_project_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep_project">
                        '.$this->get_dep_1_1($dep_val,$dep_project_val).'
                        </select>
                    </div>
                    <div class="col-3-half">
                        <label>ქვე პროექტი</label>
                        <select id="dep_sub_project_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep_sub_project">
                        '.$this->get_dep_1_1_1($dep_project_val,$dep_sub_project_val).'
                        </select>
                    </div>
                    <div class="col-3-half">
                        <label>მომართვის ტიპი</label>
                        <select id="dep_sub_type_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep_sub_type">
                        '.$this->get_project_type($dep_project_type_val).'
                        </select>
                    </div>
                    <div class="col-3-half" style="display: flex;">
                    <div class="delete_dep" data-id="'.$dep_rand_n.'"><i style="font-size:28px; margin-top:20px; margin-left:0px;cursor:pointer;" class="fa fa-minus-circle" aria-hidden="true"></i></div>
                        
                        <div id="get_dep_additional_addit" style="position: relative;" data-id="'.$dep_rand_n.'">
                            <img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/additional_inputs.png">
                            <div class="field_notifier">
                                <p><span id="field_done">'.$r_fields.'</span>/<span id="field_no_done">'.$fields_cc.'</span></p>
                            </div>
                        </div>
                        <img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/documents.png">
                        <img style="margin-left:1px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/contacts.png">
                        
                        
                    </div>
                </div>';
                }
                $data .= '</fieldset>';
            }
            $inputs_by_tabs = array();
            $db->setQuery("	SELECT	id,name
                            FROM 	processing_input_tabs
                            WHERE 	actived = 1 AND field_id = '$fieldset[id]'");
            $tabs = $db->getResultArray();
            $data .='	<fieldset class="communication_right_side_info">
                            <legend>'.$fieldset[name].'</legend>';
                            if($tabs['count'] > 1){
                                $data .= '<ul class="dialog_input_tabs" id="dialog-tab-'.$fieldset[id].'">';
                                $tab_count = 0;
                                foreach($tabs['result'] AS $tab){
                                    if($tab_count == 0){
                                        $data .= '<li aria-selected="true" data-id="'.$tab['id'].'">'.$tab['name'].'</li>';
                                    }
                                    else{
                                        $data .= '<li data-id="'.$tab['id'].'">'.$tab['name'].'</li>';
                                    }
                                    $tab_count++;
                                    $inputs_by_tabs[] = $tab['id'];
                                }
                                $data .= '</ul>';
                            }
                            else{
                                $inputs_by_tabs[] = 1;
                            }
                                
                                $data .= '<div class="fieldset_row">
                                    '.$this->get_fieldset_inputs($fieldset[id],$saved_values,$inputs_by_tabs,$phone).'
                                </div>
                        </fieldset>';
            $counter++;
        }


        return $data;
    }
    private function get_cnobari_selector_options($table,$selector_type='selector',$key='',$input_id='',$selected=0,$saved_values=''){
        GLOBAL $db;
        if($table != ''){
            $db->setQuery("	SELECT 	id,
                                    name
                            FROM 	$table
                            WHERE 	actived = '1'");
            if($selector_type == 'selector'){
                
                return $db->getSelect($selected);
            }
            else if($selector_type == 'radio'){
                $output = '';
                $radio = $db->getResultArray();
                $output .= '<div class="checkbox_container">';
                foreach($radio['result'] AS $item){
                    
                }
                foreach($radio['result'] AS $item){
                    $show = 0;
                    foreach($saved_values AS $saved){
                        
                        if($saved['input_id'] == $input_id AND $saved['int_value'] == $item[id]){
                            $show++;
                            $output .= '<div class="lonely-checkbox"><input style="width: 13px!important;" checked type="radio" id="'.$key.'-'.$item[id].'--'.$input_id.'--6" name="'.$key.'" name="'.$key.'" value="'.$item[name].'">
                                <label style="margin-top: 6px;margin-right: 5px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                        }
                    }
                    if($show == 0){
                        $output .= '<div class="lonely-checkbox"><input type="radio" style="width: 13px!important;" id="'.$key.'-'.$item[id].'--'.$input_id.'--6" name="'.$key.'" name="'.$key.'" value="'.$item[name].'">
                                <label style="margin-top: 6px;margin-right: 5px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                    }
                    
                }
                $output .= '</div>';
                return $output;
            }
            else if($selector_type == 'checkbox'){
                //var_dump($saved_values);
                $output = '';
                $radio = $db->getResultArray();
                $output .= '<div class="checkbox_container">';
                foreach($radio['result'] AS $item){
                    $show = 0;
                    foreach($saved_values AS $saved){
                        
                        if($saved['input_id'] == $input_id AND $saved['int_value'] == $item[id]){
                            $show++;
                            $output .= '<div class="lonely-checkbox"><input style="width: 13px!important;height:12px!important;" checked type="checkbox" id="'.$key.'-'.$item[id].'--'.$input_id.'--7" name="'.$key.'" value="'.$item[name].'">
                                <label style="margin-right: 7px;margin-left: 3px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                        }
                    }
                    if($show == 0){
                        $output .= '<div class="lonely-checkbox"><input style="height:12px!important;width: 13px!important;" type="checkbox" id="'.$key.'-'.$item[id].'--'.$input_id.'--7" name="'.$key.'" value="'.$item[name].'">
                                <label style="margin-right: 7px;margin-left: 3px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                    }
                    
                }
                
                $output .= '</div>';
                return $output;
            }
            
        }
    
    }
    
    private function get_fieldset_inputs($field_id,$saved_values,$inputs_by_tabs,$pphone,$req_id = ''){
        //var_dump($saved_values);
        GLOBAL $db;
        if($field_id == ''){
            $db->setQuery("	SELECT 	id,
                                    `key`,
                                    name,
                                    multilevel_table,
                                    table_name,
                                    field_type_id AS 'type',
                                    position,
                                    multilevel_1,
                                    multilevel_2,
                                    multilevel_3,
                                    depth,
                                    necessary_input AS 'nec',
                                    '' AS 'class'

                            FROM 	outgoing_campaign_inputs
                            WHERE 	request_id = '$req_id' AND actived = '1'
                            ORDER BY position");
            $inputs = $db->getResultArray();

            $divider = 1;
            for($j=0;$j<$inputs['count'];$j++){
                if($inputs['result'][$j]['type'] == 11){
                    if($divider%3 == 0){
                        $inputs['result'][$j-1]['class'] = 'col-6';
                        $inputs['result'][$j-2]['class'] = 'col-6';
                    }
                    else{
                        $inputs['result'][$j-1]['class'] = 'col-12';
                    }
                }
                $divider++;
            }
            for($i=0;$i<$inputs['count'];$i++){
                if($tabs == $inputs['result'][$i]['tab_id']){
                    $nec = '';
                    $nec_is = 0;
                    $default = 'col-4';
                    if($inputs['result'][$i]['nec'] == 1){
                        $nec = '<span style="color:red;"> *</span>';
                        $nec_is = 1;
                    }
            
                    if($inputs['result'][$i]['class'] != ''){
                        $default = $inputs['result'][$i]['class'];
                    }
            
                    if($inputs['result'][$i]['type'] == 1 OR $inputs['result'][$i]['type'] == 4 OR $inputs['result'][$i]['type'] == 5)
                    {	
                        $show = 0;
                        $input_name = $inputs['result'][$i]['name'];
    
                        
                        $service_updater = '';
                        if($input_name == 'ტელეფონი'){
                            $service_updater = '<img class="service_icon" id="service_phone" src="media/images/icons/inner_call_2.png">';
                        }
                        else if($input_name == 'აბონენტის კოდი'){
                            $service_updater = '<img class="service_icon" id="service_ab" src="media/images/icons/inner_call_2.png">';
                        } 
                        else if($input_name == 'პირადი ნომერი'){
                            $service_updater = '<img class="service_icon for_service_contract" id="service_pn" src="media/images/icons/inner_call_2.png"> <img class="ferm_service" id="service_pn_ferm" src="media/images/icons/inner_call_2.png" style="transform:rotate(45deg);margin-top: -47px;margin-left: 124px;">';
                        }
                        else if ($input_name == 'საიდენტიფიკაციო კოდი') {
                            $service_updater = '<img class="service_icon for_service_contract" id="service_rs_id" src="media/images/icons/inner_call_2.png">';
                        }
                        
                        foreach($saved_values AS $saved){
                            $val = $saved[value];
                            if($input_name == 'ტელეფონი'){
                                $number = $pphone;
                                if($pphone != ''){
                                    $last = substr($number,-2);
                                    $middle = substr($number,-4,-2);
                                    $first = substr($number,-6,-4);
                                    $triple = substr($number,-9,-6);
        
                                    $formated_phone = $triple.'-'.$first.'-'.$middle.'-'.$last;
        
                                    $val = $formated_phone;
                                }
                                
                            }
    
                            
                            if($saved['input_id'] == $inputs['result'][$i]['id']){
                                $show++;
                                $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$val.'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" />'.$service_updater.'</div>';
                            }
                        }
                        if($show == 0){
                            $number = $pphone;
                            $val = '';
                            if($pphone != '' and $input_name == 'ტელეფონი'){
                                $last = substr($number,-2);
                                $middle = substr($number,-4,-2);
                                $first = substr($number,-6,-4);
                                $triple = substr($number,-9,-6);
    
                                $formated_phone = $triple.'-'.$first.'-'.$middle.'-'.$last;
    
                                $val = $formated_phone;
                            }
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$val.'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" />'.$service_updater.'</div>';
                        }
                    }
                    else if($inputs['result'][$i]['type'] == 8)
                    {
                        $cnobari_table = $inputs['result'][$i]['table_name'];
                        $show = 0;
                        foreach($saved_values AS $saved){
                            if($saved['input_id'] == $inputs['result'][$i]['id']){
                                $show++;
                                $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.$this->get_cnobari_selector_options($cnobari_table,'selector','','',$saved['int_value']).'</select></div>';
                            }
                        }
                        if($show == 0){
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.$this->get_cnobari_selector_options($cnobari_table,'selector','','',0).'</select></div>';
                        }
                        
                    }
                    else if($inputs['result'][$i]['type'] == 6)
                    {
                        $cnobari_table = $inputs['result'][$i]['table_name'];
            
                        $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.$this->get_cnobari_selector_options($cnobari_table,'radio',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
                    }
                    else if($inputs['result'][$i]['type'] == 7)
                    {
                        $cnobari_table = $inputs['result'][$i]['table_name'];
            
                        $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.$this->get_cnobari_selector_options($cnobari_table,'checkbox',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
                    }
                    else if($inputs['result'][$i]['type'] == 2)
                    {
                        $show = 0;
                        foreach($saved_values AS $saved){
                            if($saved['input_id'] == $inputs['result'][$i]['id']){
                                $show++;
                                $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content">'.$saved[value].'</textarea></div>';
                            }
                        }
                        if($show == 0){
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content"></textarea></div>';
                        }
                    }
                    else if($inputs['result'][$i]['type'] == 12)
                    {
                        $output .= '<div class="col-4"></div>';
                    }
                    else if($inputs['result'][$i]['type'] == 10){
                        $depth = $inputs['result'][$i]['depth'];
                        $level_1_id = 0;
                        $level_2_id = 0;
                        $level_3_id = 0;
                        for($k=1;$k<=$depth;$k++){
                            
                            if($k == 1){
                                $show = 0;
                                foreach($saved_values AS $saved){
                                    if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 1){
                                        $show++;
                                        $level_1_id = $saved['int_value'];
                                        $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1($saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                    }
                                }
                                if($show == 0){
                                    $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1(0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                }
                                
                            }
                            else if($k == 2){
                                $show = 0;
                                foreach($saved_values AS $saved){
                                    if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 2){
                                        $show++;
                                        $level_2_id = $saved['int_value'];
                                        $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_1_id,$saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                    }
                                }
                                if($show == 0){
                                    $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_1_id,0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                }
                            }
                            else{
                                $show = 0;
                                foreach($saved_values AS $saved){
                                    if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 3){
                                        $show++;
                                        $level_3_id = $saved['int_value'];
                                        $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_2_id,$saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                    }
                                }
                                if($show == 0){
                                    $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_2_id,0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                }
                            }
                        }
                    }
                    $divider++;
                }
                
            }
            return $output;
            
        }
        else if($field_id != ''){

        
            $db->setQuery("	SELECT 	id,
                                    `key`,
                                    name,
                                    multilevel_table,
                                    table_name,
                                    processing_field_type_id AS 'type',
                                    position,
                                    multilevel_1,
                                    multilevel_2,
                                    multilevel_3,
                                    depth,
                                    tab_id,
                                    necessary_input AS 'nec',
                                    '' AS 'class'
        
                            FROM 	processing_setting_detail
                            WHERE 	processing_fieldset_id = '$field_id' AND actived = '1'
                            ORDER BY position");
            $inputs = $db->getResultArray();
        
            $tab_count = count($inputs_by_tabs);
            $tabs_count = 0;
            foreach($inputs_by_tabs AS $tabs){
                $display = 'none';
                if($tabs_count == 0){
                    $display = 'block';
                    $tabs_count++;
                }
                $output .= '<div class="inp_tabs dialog-tab-'.$field_id.'" data-tab-id="'.$tabs.'"  style="display:'.$display.';">';
                $divider = 1;
                for($j=0;$j<$inputs['count'];$j++){
                    if($tabs == $inputs['result'][$j]['tab_id']){
                        if($inputs['result'][$j]['type'] == 11){
                            if($divider%3 == 0){
                                $inputs['result'][$j-1]['class'] = 'col-6';
                                $inputs['result'][$j-2]['class'] = 'col-6';
                            }
                            else{
                                $inputs['result'][$j-1]['class'] = 'col-12';
                            }
                        }
                        $divider++;
                    }
                    
                }
                for($i=0;$i<$inputs['count'];$i++){
                    if($tabs == $inputs['result'][$i]['tab_id']){
                        $nec = '';
                        $nec_is = 0;
                        $default = 'col-4';
                        if($inputs['result'][$i]['nec'] == 1){
                            $nec = '<span style="color:red;"> *</span>';
                            $nec_is = 1;
                        }
                
                        if($inputs['result'][$i]['class'] != ''){
                            $default = $inputs['result'][$i]['class'];
                        }
                
                        if($inputs['result'][$i]['type'] == 1 OR $inputs['result'][$i]['type'] == 4 OR $inputs['result'][$i]['type'] == 5)
                        {	
                            $show = 0;
                            $input_name = $inputs['result'][$i]['name'];
        
                            
                            $service_updater = '';
                            if($input_name == 'ტელეფონი'){
                                $service_updater = '<img class="service_icon" id="service_phone" src="media/images/icons/inner_call_2.png">';
                            }
                            else if($input_name == 'პირადი ნომერი'){
                                $service_updater = '<img class="service_icon for_service_contract" id="service_pn" src="media/images/icons/inner_call_2.png"> <img class="ferm_service" id="service_pn_ferm" src="media/images/icons/inner_call_2.png" style="transform:rotate(45deg);margin-top: -47px;margin-left: 124px;">';
                            }
                            else if($input_name == 'აბონენტის კოდი'){
                                $service_updater = '<img class="service_icon" id="service_ab" src="media/images/icons/inner_call_2.png">';
                            }
                            else if ($input_name == 'საიდენტიფიკაციო კოდი') {
                                $service_updater = '<img class="service_icon for_service_contract" id="service_rs_id" src="media/images/icons/inner_call_2.png">';
                            }
                            
                            foreach($saved_values AS $saved){
                                $val = $saved[value];
                                if($input_name == 'ტელეფონი'){
                                    $number = $pphone;
                                    if($pphone != ''){
                                        $last = substr($number,-2);
                                        $middle = substr($number,-4,-2);
                                        $first = substr($number,-6,-4);
                                        $triple = substr($number,-9,-6);
            
                                        $formated_phone = $triple.'-'.$first.'-'.$middle.'-'.$last;
            
                                        $val = $formated_phone;
                                    }
                                    
                                }
        
                                
                                if($saved['input_id'] == $inputs['result'][$i]['id']){
                                    $show++;
                                    $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$val.'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" />'.$service_updater.'</div>';
                                }
                            }
                            if($show == 0){
                                $number = $pphone;
                                $val = '';
                                if($pphone != '' and $input_name == 'ტელეფონი'){
                                    $last = substr($number,-2);
                                    $middle = substr($number,-4,-2);
                                    $first = substr($number,-6,-4);
                                    $triple = substr($number,-9,-6);
        
                                    $formated_phone = $triple.'-'.$first.'-'.$middle.'-'.$last;
        
                                    $val = $formated_phone;
                                }
                                $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$val.'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" />'.$service_updater.'</div>';
                            }
                        }
                        else if($inputs['result'][$i]['type'] == 8)
                        {
                            $cnobari_table = $inputs['result'][$i]['table_name'];
                            $show = 0;
                            foreach($saved_values AS $saved){
                                if($saved['input_id'] == $inputs['result'][$i]['id']){
                                    $show++;
                                    $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.$this->get_cnobari_selector_options($cnobari_table,'selector','','',$saved['int_value']).'</select></div>';
                                }
                            }
                            if($show == 0){
                                $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.$this->get_cnobari_selector_options($cnobari_table,'selector','','',0).'</select></div>';
                            }
                            
                        }
                        else if($inputs['result'][$i]['type'] == 6)
                        {
                            $cnobari_table = $inputs['result'][$i]['table_name'];
                
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.$this->get_cnobari_selector_options($cnobari_table,'radio',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
                        }
                        else if($inputs['result'][$i]['type'] == 7)
                        {
                            $cnobari_table = $inputs['result'][$i]['table_name'];
                
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.$this->get_cnobari_selector_options($cnobari_table,'checkbox',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
                        }
                        else if($inputs['result'][$i]['type'] == 2)
                        {
                            $show = 0;
                            foreach($saved_values AS $saved){
                                if($saved['input_id'] == $inputs['result'][$i]['id']){
                                    $show++;
                                    $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content">'.$saved[value].'</textarea></div>';
                                }
                            }
                            if($show == 0){
                                $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content"></textarea></div>';
                            }
                        }
                        else if($inputs['result'][$i]['type'] == 12)
                        {
                            $output .= '<div class="col-4"></div>';
                        }
                        else if($inputs['result'][$i]['type'] == 10){
                            $depth = $inputs['result'][$i]['depth'];
                            $level_1_id = 0;
                            $level_2_id = 0;
                            $level_3_id = 0;
                            for($k=1;$k<=$depth;$k++){
                                
                                if($k == 1){
                                    $show = 0;
                                    foreach($saved_values AS $saved){
                                        if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 1){
                                            $show++;
                                            $level_1_id = $saved['int_value'];
                                            $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1($saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                        }
                                    }
                                    if($show == 0){
                                        $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1(0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                    }
                                    
                                }
                                else if($k == 2){
                                    $show = 0;
                                    foreach($saved_values AS $saved){
                                        if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 2){
                                            $show++;
                                            $level_2_id = $saved['int_value'];
                                            $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_1_id,$saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                        }
                                    }
                                    if($show == 0){
                                        $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_1_id,0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                    }
                                }
                                else{
                                    $show = 0;
                                    foreach($saved_values AS $saved){
                                        if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 3){
                                            $show++;
                                            $level_3_id = $saved['int_value'];
                                            $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_2_id,$saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                        }
                                    }
                                    if($show == 0){
                                        $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_2_id,0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                    }
                                }
                            }
                        }
                        $divider++;
                    }
                    
                }
                $output .= '</div>';
                
            }
            return $output;
        }
    }
    private function get_regions_1($id){
        global $db;
        
        $db->setQuery("SELECT `id`,
                              `name`
                       FROM   regions
                       WHERE   actived = 1 AND `parent_id` = 0");
        
        $req = $db->getResultArray();
        $data .= '<option value="0" selected="selected">----</option>';
        foreach ($req[result] AS $res){
            if($res['id'] == $id){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            }else{
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
        }
        
        return $data;
        
    }
    private function get_regions_1_1($id,$child_id){
        global $db;
        
        $db->setQuery("SELECT  `id`,
                               `name`
                       FROM    regions
                       WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
        
        $req = $db->getResultArray();
        
        $data .= '<option value="0" selected="selected">----</option>';
        $i = 0;
        foreach($req[result] AS $res){
            if($res['id'] == $child_id){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            } else {
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
            $i = 1;
        }
        if($i == 0 && $id > 0){
            $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
        }
        
        return $data;
    }
    private function get_dep_1($id){
        global $db;
        
        $db->setQuery("SELECT `id`,
                              `name`
                       FROM   `info_category`
                       WHERE   actived = 1 AND `parent_id` = 0");
        
        $req = $db->getResultArray();
        $data .= '<option value="0" selected="selected">----</option>';
        foreach ($req[result] AS $res){
            if($res['id'] == $id){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            }else{
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
        }
        
        return $data;
        
    }
    private function get_dep_1_1($id,$child_id){
        global $db;
        
        $hidden = ' IN(121,126,129,132,135,138,141,145,151,154,157) ';

        $db->setQuery("SELECT  		`cat1`.id,
                                    `cat1`.`name`
                        FROM 		`info_category`
                        LEFT JOIN 	`info_category` AS `cat1` ON `cat1`.`parent_id` = `info_category`.`id`
                        
                        WHERE 	 	`cat1`.id NOT $hidden AND `info_category`.id NOT $hidden AND `info_category`.`parent_id` = 0 AND `info_category`.actived=1 AND cat1.actived = 1");
        
        $req = $db->getResultArray();
        
        $data .= '<option value="0" selected="selected">----</option>';
        $i = 0;
        foreach($req[result] AS $res){
            if($res['id'] == $child_id){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            } else {
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
            $i = 1;
        }
        if($i == 0 && $id > 0){
            $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
        }
        
        return $data;
    }
    private function get_dep_1_1_1($id,$child_id){
        global $db;
        if($id != 0){
            $db->setQuery("SELECT `id`,
                                    `name`
                            FROM   `info_category`
                            WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
        }
        else{
            $db->setQuery("SELECT id, `name`
            FROM info_category
           
           WHERE parent_id IN(SELECT id 
                                                 FROM info_category 
                                                WHERE parent_id IN(SELECT id 
                                                                   FROM info_category 
                                                                                       WHERE parent_id = 0 AND actived = 1) AND actived = 1) AND actived = 1");
        }
        
        
        $req = $db->getResultArray();
        
        $data2 .= '<option value="0" selected="selected">----</option>';
        $i = 0;
        
        foreach($req[result] AS $res){
            if($res['id'] == $child_id){
                $data2 .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            } else {
                $data2 .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
            $i = 1;
        }
        
        if($i == 0 && $id > 0){
            $data2 .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
        }
        
        $db->setQuery("	SELECT id,name
    
                        FROM info_category
                        WHERE id = (SELECT parent_id
                                                FROM info_category
                                                WHERE info_category.id = '$id')");
        $main = $db->getResultArray();
    
        $data3 .= '<option value="'.$main['result'][0]['id'].'" selected="selected">'.$main['result'][0]['name'].'</option>';
        
        if($child_id == ''){
            if($id == 0){
            
                $data = array('data2' => $data2, 'data3' => $this->get_dep_1(0));
            }
            else{
                $data = array('data2' => $data2, 'data3' => $data3);
            }
        }
        else{
            $data = $data2;
        }
        
        return $data;
    }
    private function get_project_type($id){
        global $db;
        $db->setQuery("SELECT `id`,
                              `name`
                       FROM   `mepa_project_types`
                       WHERE   actived = 1");
        
        $req = $db->getResultArray();
        
        $data .= '<option value="0" selected="selected">----</option>';
        $i = 0;
        
        foreach($req[result] AS $res){
            if($res['id'] == $id){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            } else {
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
            $i = 1;
        }
        
        if($i == 0 && $id > 0){
            $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
        }
        
        return $data;
    }
}


?>