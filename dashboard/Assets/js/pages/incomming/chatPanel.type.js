import Tdg from "../../tdg.class.js";

export default class ChatPanel extends Tdg{

    constructor(prop){
        super();

        self.chatPanel = this;

        this.state = {
            interface:this.CreateElement({
                element: "interface",
                attributes: ["chatpanel"]
            }),
            element: [],
            header: [],
            body: [],
            footer: [],
            prop: prop
        }

    }

    /**
     * INITIALIZE CHATPANEL 
     */
    init = async () => {

            await this.build()
            return this.state.interface

    }

    /**
     * BUILD CHATPANEL COMPONENTS
     */
    build = async () => {
        
        this.append(this.state.interface, await this.createChatHead().then((header) => { this.buildChatHead(); return header }))
        this.append(this.state.interface, await this.createChatBody())
        this.append(this.state.interface, await this.createChatFooter().then((footer) => { this.buildChatFooter(); return footer }))

        this.buildMessage({
            type: "income",
            name: "Sender",
            message: "გამარჯობა",
            avatar: "Assets/images/no-image.png"
        })
    }



    createChatHead = async () => {

        this.state.header = this.CreateElement({
            element: "chatHead"
        })

        return this.state.header;

    }

    buildChatHead = () => {

        this.state.header.name = this.CreateElement({
            element: "span",
            name: "name",
            text: "ანი გაბუნია"
        })

        this.state.header.icons = this.CreateElement({
            element: "span",
            name: "icons"
        }, this.CreateElement({
            element: "icon",
            attributes: ["svg"],
            actived: true,
            name: "current",
            title: "ჩატი",
            children: `<svg viewBox="0 0 16 16" width="14" height="14">
                            <use xlink:href="Assets/images/icons/icons.svg" />
                        </svg>`
        }), this.CreateElement({
            element: "icon",
            attributes: ["svg"],
            name: "history",
            title: "ისტორია",
            children: `<svg viewBox="0 0 16 16" width="14" height="14">
                            <use xlink:href="Assets/images/icons/icons.svg#tab-history" />
                        </svg>`
        }))

        this.state.header.toggle = this.CreateElement({
            element: "span",
            attributes: ["svg"],
            name: "toggle",
            children: `<svg viewBox="0 0 24 24" width="24" height="24">
                            <use xlink:href="Assets/images/icons/icons.svg#arrow-right" />
                        </svg>`,
            onclick: this.handleChatToggle
        })

        this.state.header.whois = this.CreateElement({
            element: "span",
            name: "whois",
            "grid-column": "3",
            children: "GABUNIA@GABUNIA"
        })

        this.append(this.state.header, this.state.header.name)
        this.append(this.state.header, this.state.header.icons)
        this.append(this.state.header, this.state.header.toggle)
        this.append(this.state.header, this.state.header.whois)

    }

    handleChatToggle = function(e) {
        e.preventDefault()

        this.toggleAttribute("actived")

        if(this.hasAttribute("actived")){
            self.WorkGround.state.interface.style.display = "none"
            self.chatPanel.state.interface.style.width = "auto"
            self.chatPanel.state.interface.setAttribute("grid-column", 2)
        }else{
            self.WorkGround.state.interface.style.display = ""
            self.chatPanel.state.interface.style.width = ""
            self.chatPanel.state.interface.removeAttribute("grid-column")
        }
    }

    createChatBody = async () => {

        this.state.body = this.CreateElement({
            element: "chatBody"
        })

        return this.state.body;

    }

    buildMessage = (data) => {

        let message = this.CreateElement({
            element: "message",
            type: data.type
        }, this.CreateElement({
            element: "avatar",
            children: `<img name"avatar" src=${data.avatar} />`
        }), this.CreateElement({
            element: "content"
        },  this.CreateElement({
            element: "span",
            name: "name",
            text: data.name
        }), this.CreateElement({
            element: "span",
            name: "datetime",
            text: "15:16:37"
        }), this.CreateElement({
            element: "span",
            name: "option",
            class: "k-icon k-i-more-vertical"
        }), this.CreateElement({
            element: "span",
            name: "message",
            text: data.message
        })))

        this.append(this.state.body, message)

    }

    createChatFooter = async () => {

        this.state.footer = this.CreateElement({
            element: "chatFooter"
        })

        return this.state.footer;

    }

    buildChatFooter = () => {

        this.state.footer.editor = this.CreateElement({
            element: "editor",
            name: "chat",
            contenteditable: true,
            onkeyup: (e) => {
                if(e.keyCode == 13){
                    if(this.state.footer.editor.innerHTML == '') return false;

                    e.preventDefault(); 
                    this.handleSendMessage();
                }
            },
            onkeydown: (e) => {
                if(this.state.footer.editor.innerHTML == '') return false;
                if(e.keyCode == 13){
                    e.preventDefault(); 
                }
            }
        })

        this.state.footer.address = this.CreateElement({
            element: "div",
            name: "address"
        })

        this.state.footer.tools = this.CreateElement({
            element: "div",
            name: "tools"
        }, this.CreateElement({
            element: "span",
            name: "template",
            text: "აირჩიე შაბლონი"
        }), this.CreateElement({
            element: "span",
            name: "copy"
        }), this.CreateElement({
            element: "span",
            name: "style"
        }), this.CreateElement({
            element: "span",
            name: "smile"
        }), this.CreateElement({
            element: "span",
            name: "photo"
        }), this.CreateElement({
            element: "span",
            name: "attachment"
        }), this.CreateElement({
            element: "span",
            attributes: ["svg"],
            name: "send",
            text: "გაგზავნა",
            children: `<svg viewBox="0 0 20 20" width="15" height="20">
                            <use xlink:href="Assets/images/icons/icons.svg#sendmessage" />
                        </svg>`,
            onclick: this.handleSendMessage
        }))

        this.append(this.state.footer, this.state.footer.address);
        this.append(this.state.footer, this.state.footer.editor);
        this.append(this.state.footer, this.state.footer.tools);

    }


    handleSendMessage = () => {
        
        if(this.state.footer.editor.innerHTML == '') return false;

        this.buildMessage({
            type: "sent",
            name: "ოპერატორი",
            message: this.state.footer.editor.innerHTML,
            avatar: "Assets/images/no-image.png"
        })

        this.state.footer.editor.innerHTML = '';
        this.state.body.scrollTop = this.state.body.scrollHeight

    }
}

