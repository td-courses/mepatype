// let editBtns = document.querySelectorAll('.post_menu_icon__edit')

// for (let i = 0; i < editBtns.length; i++) {
//     console.log(editBtns[i]);

let popUwindow = document.querySelector('.popup-window')
// }


const editPost = (button) => {
    let editBtn = button;
    let post = button.parentElement.parentElement.parentElement.parentElement;
    let typeOfPost = post.dataset.posttype;
    let postId = post.dataset.postid;
    let update_button = document.querySelectorAll('.post_editBtn')
    const popupFormPostBtn = document.querySelectorAll('.popupFormPostBtn')

    console.log(typeOfPost);
    console.log(postId);
    console.log(post);


    update_button.forEach(el => el.classList.add('display-block'))
    popupFormPostBtn.forEach(el => el.classList.remove('display-block'))



    let popUwindow = document.querySelector('.popup-window')

    let selectFormType = popUwindow.querySelector('#formSelect')
    selectFormType.value = typeOfPost
    formChange()


    popUwindow.style.display = "flex"



    let oldTags = document.querySelectorAll('.tagged')
    oldTags.forEach(tag => {
        tag.parentElement.remove()
    });







    let actAndPostId = {
        act: "get_edit_post",
        id: postId
    }

    let parser = new DOMParser();


    $.ajax({
        url: AjaxURL,
        data: actAndPostId,
        success: function (post) {
            let Post = post.posts[0]

            let form = document.querySelector('.popup-window__container__wrapper')
            form.setAttribute('data-postid', Post.id);




            let descriptionInp = document.querySelectorAll('.post_form__description  textarea')
            let questionInp = document.querySelector('.question input')
            let answaresContainer = document.querySelector('.answers ol')
            answaresContainer.innerHTML = "";

            let pollStartDateInp = document.querySelector('#pollStartDate')
            let pollEndDateInp = document.querySelector('#pollEndDate')

            let eventStartDateInp = document.querySelector('#eventStartDate')
            let eventEndDateInp = document.querySelector('#eventEndDate')


            let locationInp = document.querySelector('.event_form_locationInp')

            let adresatContainer = document.querySelector('.adresat_added ul')
            // adresatContainer.innerHTML = ""
            // adresatContainer.innerHTML += `<li class="add_in_input_btn__container">
            //                                     <p class="add_in_input_btn">დამატება +</p>
            //                                 </li>` 
            let youtubeUrl = document.querySelector('#youtube_url')
            let viewPreview = document.querySelector('.video_preview')
            let iframe = document.querySelector('.video_preview__video iframe')





            if (Post.post_image_name == "") {
                let image_preview = document.querySelector('.image_preview');
                image_preview.style.display = "none";
            } else {
                let imagePreview = document.querySelector('.image_preview__image');
                let image_preview = document.querySelector('.image_preview');
                var files = document.querySelector('#image_upload');

                image_preview.style.display = "block";
                imagePreview.style.backgroundImage = `url('media/uploads/posts/${Post.post_image_name}')`;
                files.dataset.imagename = Post.post_image_name;
            }



            if (youtubeUrl.value == "") {
                viewPreview.style.display = 'none';
            } else {
                viewPreview.style.display = 'block';
            }

            let youtubeUrlRow = `https://www.youtube.com/watch?v=${Post.post_youtube_video_url}`


            if (Post.post_youtube_video_url == "") {
                youtubeUrl.value = ""
            } else {
                youtubeUrl.value = youtubeUrlRow
            }


            if (youtubeUrl.value == "") {
                viewPreview.style.display = 'none';
            } else {
                viewPreview.style.display = 'block';

                if (iframe != 0) {
                    iframe.src = `https://www.youtube.com/embed/${Post.post_youtube_video_url}`
                }
            }



            descriptionInp.forEach(input => {
                input.value = Post.description
            })

            questionInp.value = Post.question


            Post.answers.answers.forEach(answer => {
                let anwareTemplate = parser.parseFromString(
                    `<li> 
                        <input value='${answer.text}' type="text">
                        <div class="delete_answer" onclick="deleteAnswer(event)">✖</div>
                    </li>`
                    , "text/html")
                answaresContainer.append(anwareTemplate.body.firstChild);
            })

            pollStartDateInp.value = Post.startAndEndDate.start
            pollEndDateInp.value = Post.startAndEndDate.end

            eventStartDateInp.value = Post.startAndEndDate.start
            eventEndDateInp.value = Post.startAndEndDate.end


            locationInp.value = Post.location;


            let tagedUsers = Post.marked_users
            let tagedGroups = Post.marked_groups
            let tagedDepartments = Post.marked_departments
            let addAdresatBtn = document.querySelector('.add_in_input_btn__container')

            console.log(tagedUsers);
            console.log(tagedDepartments);


            if (tagedUsers.length > 0 && tagedUsers[0].id != "") {
                for (let i = 0; i < tagedUsers.length; i++) {
                    let liTag = parser.parseFromString(`<li data-tagedid="${tagedUsers[i].id}" data-typeoftag='user'> 
                                                            <div class="tagged"> 
                                                                <p>${tagedUsers[i].name}</p>
                                                                <div class="delete_tag"  onclick = "deleteTag(event)">✖</div>
                                                            </div>
                                                        </li>`, "text/html")
                    let adresatContainer = document.querySelector('.adresat_added ul')

                    adresatContainer.insertBefore(liTag.body.firstElementChild, addAdresatBtn)
                }
            }


            if (tagedGroups.length > 0 && tagedGroups[0].id != "") {
                for (let i = 0; i < tagedGroups.length; i++) {
                    let liTag = parser.parseFromString(`<li data-tagedid="${tagedGroups[i].id}" data-typeoftag='group'> 
                                                            <div class="tagged"  style="background: #FAAF30; color: white"> 
                                                                <p style="color: white; font-family: noto-light, lexend-regular" >${tagedGroups[i].name}</p>
                                                                <div class="delete_tag"  onclick = "deleteTag(event)">✖</div>
                                                            </div>
                                                        </li>`, "text/html")
                    let adresatContainer = document.querySelector('.adresat_added ul')

                    adresatContainer.insertBefore(liTag.body.firstElementChild, addAdresatBtn)

                }
            }

            if (tagedDepartments.length > 0 && tagedDepartments[0].id != "") {
                for (let i = 0; i < tagedDepartments.length; i++) {
                    let liTag = parser.parseFromString(`<li data-tagedid="${tagedDepartments[i].id}" data-typeoftag='department'> 
                                                            <div class="tagged"  style="background: #00A57F; color:white "> 
                                                                <p style="color: white; font-family: noto-light, lexend-regular" >${tagedDepartments[i].name}</p>
                                                                <div class="delete_tag"  onclick = "deleteTag(event)">✖</div>
                                                            </div>
                                                        </li>`, "text/html")
                    let adresatContainer = document.querySelector('.adresat_added ul')

                    adresatContainer.insertBefore(liTag.body.firstElementChild, addAdresatBtn)
                }

            }
        }
    })
}



const updatePost = (updateBtn) => {
    updateBtn.addEventListener('click', (e) => {

        let postType = formSelect.value;
        let taggedUserArray = document.querySelectorAll('li[data-typeoftag="user"]');
        let taggedGroupArray = document.querySelectorAll('li[data-typeoftag="group"]');
        let taggedDepartmentArray = document.querySelectorAll('li[data-typeoftag="department"]');

        var imagePreview = document.querySelector('.image_preview__image');
        var files = document.querySelector('#image_upload');


        let taggedUserIdArray = [];
        let taggedGroupIdArray = [];
        let taggedDepartmentIdArray = [];
        let answers;
        let answersObj;
        let answersArray = [];
        let answersType;
        let youtubeUrl;
        let imageName;

        let postId = document.querySelector('.popup-window__container__wrapper').dataset.postid

        let startDate = document.getElementById('eventStartDate').value;
        let endDate = document.getElementById('eventEndDate').value;

        let pollStartDate = document.getElementById('pollStartDate').value;
        let pollEndDate = document.getElementById('pollEndDate').value;

        let youtubeUrlRow = document.querySelector('#youtube_url').value
        
 

 

        if (files.dataset.imagename != "" && youtubeUrlRow != "") {
            console.log("files.dataset.imagename");
            console.log(files.dataset.imagename);
            youtubeUrl = youtubeUrlRow.split('=')[1].split('&t')[0];
            imageName = files.dataset.imagename; 
        }

       
        imageName = files.value;

        //! file edit

        if (files.value == "") {
            imageName = "";
        } else {
            imageName = files.dataset.imagename;
        }


        //! youtube edit

        if (youtubeUrlRow) {
            youtubeUrl = youtubeUrlRow.split('=')[1].split('&t')[0];
        } else {
            youtubeUrl = "";
        }


        let checkbox = document.querySelector('.multiOrNotCheck');

        let question = document.querySelector('.second_popup__addQuestions__container .question input').value
        answers = document.querySelectorAll('ol li input')

        answers.forEach(answer => {
            answersObj = {
                text: answer.value,
                value: 0
            }
            answersArray.push(answersObj)
        })



        taggedUserArray.forEach(userTag => {
            taggedUserIdArray.push(userTag.dataset.tagedid)
        })


        taggedGroupArray.forEach(groupTag => {
            taggedGroupIdArray.push(groupTag.dataset.tagedid)
        })


        taggedDepartmentArray.forEach(departmentTag => {
            taggedDepartmentIdArray.push(departmentTag.dataset.tagedid)
        })



        let descriptionElements = document.querySelectorAll('.post_form__description textarea');
        let descriptionElement = [...descriptionElements].filter(input => input.value != "")
        let descriptionText;
        console.log("descriptionElements");
        console.log(descriptionElements);
        


        let selectFormType = popUwindow.querySelector('#formSelect')
        if (selectFormType.value == "post") {
            descriptionText = descriptionElements[0].value
            console.log("post");
            
        }
        if (selectFormType.value == "event") {
            descriptionText = descriptionElements[1].value
            console.log("event");
            
        }
        if (selectFormType.value == "poll") {
            descriptionText = descriptionElements[2].value
            console.log("poll");
            
        }




        let location = document.querySelector('.event_form_locationInp').value;





        if (checkbox.checked) {
            answersType = "checkbox"
        } else {
            answersType = "radio"
        }


        let post = {
            "type": postType,
            "description": descriptionText,
            "question": question,
            "answers": {
                "type": answersType,
                "answers": answersArray
            },
            "startAndEndDate": {
                "start": startDate,
                "end": endDate
            },
            "location": location,
            "post_image_name": "",
            "post_youtube_video_url": youtubeUrl,
            "poster_name": "",
            "poster_image_name": imageName,
            "image": imageName,
            "poster_departments": "",
            "time_after_posted": 0,
            "time_posted": moment().format('hh:mm'),
            "data_posted": moment(new Date()).format("DD-MM-YYYY"),
            "marked_groups": taggedGroupIdArray,
            "marked_users": taggedUserIdArray,
            "marked_departments": taggedDepartmentIdArray,
            "likes": {
                "sad": 0,
                "happy": 0,
                "neutral": 0,
                "love": 0,
                "amazed": 0
            },
            "comments": [

            ]
        }


        console.log(post);


        let actAndPost = {
            act: "edit_post",
            id: postId,
            posts: post
        }


        $.ajax({
            url: AjaxURL,
            data: actAndPost,
            success: function (post) {
                console.log(post);
                getAllPosts()

            }
        })





    })
}







const formChange = () => {
    const addBtn = document.querySelector('.add_button')
    const popup = document.querySelector('.popup-window')
    const popupQuestions = document.querySelector('.second_popup__addQuestions')
    const popup2 = document.querySelector('.popup-window__blacked-BG')
    const popupQuestionsBg = document.querySelector('.second_popup__addQuestions__blacked-BG')
    const formSelect = document.querySelector('#formSelect')
    let popUwindow = document.querySelector('.popup-window')


    const addQuestionBtn = document.querySelector('.addPollQuestionsBtn')
    document.querySelector('.eventPostBtn').style.display = "none";
    document.querySelector('.postPostBtn').style.display = "none";
    document.querySelector('.pollPostBtn').style.display = "none";





    const event_form = document.querySelector('.event_form')
    const post_form = document.querySelector('.post_form')
    const poll_form = document.querySelector('.poll_form')

    const popupWrapper = document.querySelector(".popup-window__container__wrapper")

    if (formSelect.value == 'event') {
        post_form.style.display = "none";
        event_form.style.display = "grid";
        poll_form.style.display = "none";

    }


    if (formSelect.value == 'post') {
        post_form.style.display = "grid";
        event_form.style.display = "none";
        poll_form.style.display = "none";
    }

    if (formSelect.value == 'poll') {
        post_form.style.display = "none";
        event_form.style.display = "none";
        poll_form.style.display = "grid";
    }

}