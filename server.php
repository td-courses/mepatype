<?php
header('Content-Type: text/html; charset=utf-8');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//  ini_set('display_errors', 1);
//  ini_set('display_startup_errors', 1);
 error_reporting(1);

set_time_limit(0);
date_default_timezone_set('Asia/Tbilisi');

include 'includes/classes/class.Mysqli.php';
$mysqli = new dbClass();

global $mysqli;
$action      = $_REQUEST['act'];
$esc_message = addslashes($_REQUEST['message']);
$ms          = $_REQUEST['message'];
$source      = $_REQUEST['source'];
$esc_name    = addslashes($_REQUEST['chat_name']);
$esc_mail    = addslashes($_REQUEST['chat_mail']);
$esc_phone   = addslashes($_REQUEST['chat_phone']);
$os          = $_REQUEST['os'];
$fb_text     = 'test';
$br          = $_REQUEST['browser'];
$user        = $_REQUEST['user_name'];
$user_id     = $_REQUEST['user_id'];
$chat_name   = $_REQUEST['chat_name'];
$idd         = $_REQUEST['id'];

//$check = $_REQUEST['check'];
//     $esc_message = $mysqli->real_escape_string($esc_message);
//     $esc_name = $mysqli->real_escape_string($esc_name);
//     $esc_mail = $mysqli->real_escape_string($esc_mail);
//     $esc_phone = $mysqli->real_escape_string($esc_phone);
$chat_id = $_REQUEST['chat_id'];
$ip = get_client_ip();
switch ($action) {
case 'mail_forward':
    $receivers = $_REQUEST['receivers'];
    $message_id = $_REQUEST['message_id'];
    $forward_text = $_REQUEST['forward_text'];
    $inc_id = $_REQUEST['inc_id'];
    require 'phpmailer2/Exception.php';
    require 'phpmailer2/PHPMailer.php';
    require 'phpmailer2/SMTP.php';
    
    //require_once('phpmailer/class.pop3.php');
    $mysqli->setQuery(" SELECT      mail.sender_address AS `sender_id`,
                                    mail_account.`name` AS `name`,
                                    mail_account.`mail` AS `mail`,
                                    mail_account.`id` AS mail_id,
                                    mail_account.`domain` AS `url`,
                                    mail_account.`password` AS `password`,
                                    mail.`subject`,
                                    mail_detail.body AS 'text',
                                    mail.id,
                                    mail_detail.user_id

                        FROM        mail_detail
                        LEFT JOIN   mail ON mail.id = mail_detail.mail_id
                        JOIN        mail_account ON mail_account.id = mail.account_id
                        WHERE       mail_detail.id='$message_id' LIMIT 1");
    $res          = $mysqli->getResultArray();
    $sender       = $res['result'][0]['sender_id'];
    $from         = $res['result'][0]['mail'];
    $subject      = 'FWD: '.$res['result'][0]['subject'];
    $from_mail_id = $res['result'][0]['mail_id'];
    $url          = $res['result'][0]['url'];
    $password     = $res['result'][0]['password'];
    $esc_message  = "<b>From: ".$sender."</b><br><br>"."\r\n".$res['result'][0]['text'];

    $mysqli->setQuery("SELECT signature
                       FROM   signature
                       WHERE  id = '$sign_id' AND actived = 1 LIMIT 1");

    $signature      = $mysqli->getResultArray();
    $mail_signature = html_entity_decode($signature[result][0][signature]);
    try {
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = false; 

        $mail->SMTPSecure = false;
        $mail->SMTPAutoTLS = false;
        $mail->Debugoutput = 'html';
        $mail->Host = $url;
        $mail->Port = 5003;

        $mail->CharSet = 'UTF-8';
        $mail->Subject  = $subject;
        $receivers = explode(',', $receivers);
        $rrr = array();
        foreach($receivers AS $receiver){
            $rec = trim($receiver);
            if(filter_var($rec, FILTER_VALIDATE_EMAIL)){
                $mail->addAddress($rec);
                $mysqli->setQuery("INSERT INTO sent_mail(address,`subject`,date,cc_address,bcc_address,`body`,`incomming_call_id`,`status`) 
                                          VALUES('$receiver','$subject',NOW(),'','','$forward_text','$inc_id','4')");
                $mysqli->execQuery();
            }
            else{
                $data['status'] = 'ERR-MAIL';
                die(json_encode($data));
            }
        }
        //die($esc_message);
        $mail->setFrom('info@rda.gov.ge');
        //$esc_message = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si",'<$1$2>',$esc_message);
        $mail->msgHTML($forward_text.'<br><br><br>'.$esc_message.'<br><br><br>'.$mail_signature);
        
        /* $mysqli->setQuery(" SELECT patch
                            FROM mail_attachments
                            WHERE messages_id='$message_id'");
        $attachs = $mysqli->getResultArray();

        foreach($attachs['result'] AS $item){
            $mail->AddAttachment(str_replace('/var/www/callappProduction/','',$item['patch']));
        } */
        

    
        $mail->IsHTML(true); 
        $mail->send();
        $res2 = array('status'=>'ok', 'from'=>$from, 'sender'=>'423', 'esc_message'=>$esc_message);


        //receivers
        //from
        
    }
    catch (phpmailerException $e) {
        $res2 = array('status'=>'3', 'error'=>$e->errorMessage());
    } catch (Exception $e) {
        $res2 = array('status'=>'2', 'error'=>$e->getMessage());
    }
    if($res2['status']=='ok'){
        
        $mysqli->query= ("
        insert mail_detail
        SET
            `datetime`      = NOW(),
            `mail_id` = '". $_REQUEST['chat_id']."',
            `user_id`       = '". $_REQUEST['user_id']."',
            `to`       = '". $_REQUEST['receivers']."',

            `mail_type_id`       = '1',
            `body`          = '".htmlspecialchars($esc_message)."'
                    ");
        $mysqli->execQuery();
        ////////////////----------------------------------------
        /* $mysqli->query=("
            UPDATE  mail_live
            SET     `user_id`  = '".$_REQUEST['user_id']."',
                    `last_datetime` = NOW(),
                    `mail_status_id`        = '2'
            WHERE   id=$chat_id
            ");
        $mysqli->execQuery(); */
        $data['status'] = 'OK';
    }
    die(json_encode($data));
break;
}
if($source=='fbm'){
    $fb_obj      = $_REQUEST['fb_object'];
    $links_array = $fb_obj['arr'];
    $fb_text     = str_replace("\n", '', $fb_obj['text_fb']);
    
    //$esc_message = $esc_message.' '.$_REQUEST['fb_attachment'];
    $mysqli->query= ("INSERT  fb_messages
                         SET `datetime`   = NOW(),
                             `fb_chat_id` = '". $_REQUEST['chat_id']."',
                             `user_id`    = '". $_REQUEST['user_id']."',
                             `time`       = NOW(),
                             `text`       = '".$esc_message."'");
    $mysqli->execQuery();
    ////////////////----------------------------------------
    $mysqli->query=("UPDATE  fb_chat
                        SET `last_user_id`  = '".$_REQUEST['user_id']."',
                            `last_datetime` = NOW(),
                            `status`        = '2'
                     WHERE   id             = $chat_id");
    $mysqli->execQuery();
    //////////////////--------------------------------------
    $mysqli->setQuery("SELECT sender_id,
                              fb_account.`token` AS `token`
                       FROM  `fb_chat`
                       JOIN   fb_account ON fb_account.id = fb_chat.account_id
                       WHERE  fb_chat.id = '$chat_id'");
    
    $res = $mysqli->getResultArray();
    
    $sender         = $res['result'][0]['sender_id'];
    $access_token   = $res['result'][0]['token'];
    $url            = "https://graph.facebook.com/v2.6/me/messages?access_token=$access_token";
    
    $ch     = curl_init($url);
    //$esc_message = str_replace("\n", '', $esc_message);
    
    $jsonData = '{
                "recipient" :   {"id":"'.$sender.'"},
                "message"   :   {
                    "text": "'.$esc_message.'"
                        
                }}';
    
    
    var_dump($jsonData);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    $errors = curl_error($ch);
    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
//     foreach ($links_array as $esc_message ){
        
//         if (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.doc') !== false || strpos($esc_message, '.doc') !== false  || strpos($esc_message, '.xlsx') !== false || strpos($esc_message, '.pdf') !== false || strpos($esc_message, '.pptx') !== false)) {
//             $jsonData = '{
//                 "recipient" :   {"id":"'.$sender.'"},
//                 "message"   :   {
                    
//                      "attachment":{
//                                       "type":"file",
//                                       "payload":{
//                                         "is_reusable": false,
//                                         "url":"'.$esc_message.'"
//                                       }
//                                   }
                                            
                                            
//                 }}';
//         }elseif  (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.jpg') !== false || strpos($esc_message, '.png') !== false  || strpos($esc_message, '.gif') !== false || strpos($esc_message, '.JPEG') !== false)){
//             $jsonData = '{
//                 "recipient" :   {"id":"'.$sender.'"},
//                 "message"   :   {
                    
//                      "attachment":{
//                                       "type":"image",
//                                       "payload":{
//                                         "is_reusable": false,
//                                         "url":"'.$esc_message.'"
//                                       }
//                                   }
                                            
                                            
//                 }}';
//         }elseif  (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.mp4') !== false || strpos($esc_message, '.3GP') !== false  || strpos($esc_message, '.WMV') !== false || strpos($esc_message, '.FLV') !== false)){
//             $jsonData = '{
//                 "recipient" :   {"id":"'.$sender.'"},
//                 "message"   :   {
                    
//                      "attachment":{
//                                       "type":"video",
//                                       "payload":{
//                                         "is_reusable": false,
//                                         "url":"'.$esc_message.'"
//                                       }
//                                   }
                                            
                                            
//                 }}';
//         } elseif  (strpos($esc_message, 'https://crm.my.ge') !== false && (strpos($esc_message, '.wav') !== false || strpos($esc_message, '.mp3') !== false  || strpos($esc_message, '.AAC') !== false || strpos($esc_message, '. FLAC') !== false)){
//             $jsonData = '{
//                 "recipient" :   {"id":"'.$sender.'"},
//                 "message"   :   {
                    
//                      "attachment":{
//                                       "type":"audio",
//                                       "payload":{
//                                         "is_reusable": false,
//                                         "url":"'.$esc_message.'"
//                                       }
//                                   }
                                            
                                            
//                 }}';
//         }
        
//         var_dump($jsonData);
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//         $result = curl_exec($ch);
//         $errors = curl_error($ch);
//         $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     }
    
    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result)   ;
    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result);
}elseif($source=='fbc'){
    $fb_obj      = $_REQUEST['fb_object'];
    $links_array = $fb_obj['arr'];
    $fb_text     = str_replace("\n", '', $fb_obj['text_fb']);
    

    $mysqli->query = (" SELECT  comment_id, feeds_id, post_id
                        FROM    fb_comments
                        WHERE `id` = '".$_REQUEST['chat_id']."'");

    $res = $mysqli->getResultArray(); 

    $feed_id = $res[result][0]['feeds_id'];
    $comment_id = $res[result][0]['comment_id'];
    $post_id = $res[result][0]['post_id'];

    //$esc_message = $esc_message.' '.$_REQUEST['fb_attachment'];
    $mysqli->query= ("INSERT  fb_comments
                         SET `time`   = NOW(),
                             `feeds_id` = '". $feed_id."',
                             `parent_id` = '".$comment_id."',
                             `post_id` = '".$post_id."',
                             `user_id`    = '". $_REQUEST['user_id']."',
                             `message`       = '".$esc_message."',
                             `status`        = '2'");
    $mysqli->execQuery();
    ////////////////----------------------------------------
    $mysqli->query=("UPDATE  fb_comments
                        SET `last_user_id`  = '".$_REQUEST['user_id']."',
                            `last_datetime` = NOW(),
                            `status`        = '2'
                     WHERE     id           = $chat_id");
    $mysqli->execQuery();
    // //////////////////--------------------------------------
    // $mysqli->setQuery("SELECT fb_user_id,
    //                           fb_account.`token` AS `token`
    //                    FROM  `fb_comments`
    //                    JOIN   fb_account ON fb_account.id = fb_chat.account_id
    //                    WHERE  fb_comments.feeds_id = '$chat_id'");
    
    // $res = $mysqli->getResultArray();
    
    // $sender         = $res['result'][0]['fb_user_id'];
    // $access_token   = $res['result'][0]['token'];
    // $url            = "https://graph.facebook.com/v2.6/me/messages?access_token=$access_token";
    
    // $ch     = curl_init($url);
    // //$esc_message = str_replace("\n", '', $esc_message);
    
    // $jsonData = '{
    //             "recipient" :   {"id":"'.$sender.'"},
    //             "message"   :   {
    //                 "text": "'.$esc_message.'"
                        
    //             }}';
    
    // var_dump($jsonData);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    // $result = curl_exec($ch);
    // $errors = curl_error($ch);
    // $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    // curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result)   ;
    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result);
}
// elseif($source=='fbm'  && $check == 1){

//     $mysqli->setQuery("SELECT sender_id,
//                               fb_account.`token` AS `token`
//                        FROM  `fb_chat`
//                        JOIN   fb_account ON fb_account.id = fb_chat.account_id
//                        where  fb_chat.id = '$chat_id'");
//     $res = $mysqli->getResultArray();

//     $sender         = $res['result'][0]['sender_id'];
//     $access_token   = $res['result'][0]['token'];
//     $url            = "https://graph.facebook.com/v3.3/me/message_attachments?access_token=$access_token";

//     $ch     = curl_init($url);

//     $jsonData = '{
//                     "recipient" :   {"id":"'.$sender.'"},
//                     "message":{
//                         "attachment":{
//                           "type":"image",
//                           "payload":{
//                             "is_reusable": true,
//                             "url":"'.$esc_message.'"
//                           }
//                         }
//                       }
//                     }';

//     var_dump($jsonData);
//     curl_setopt($ch, CURLOPT_POST, 1);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
//     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
//     $result = curl_exec($ch);
//     $errors = curl_error($ch);
//     $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     curl_close($ch);
//     var_dump($errors);
//     var_dump($response);
//     die( $result)   ;
//     curl_close($ch);
//     var_dump($errors);
//     var_dump($response);
//     die( $result);
// }
elseif($source=='fbm_block'){
    $chat_id = $_REQUEST['chat_id'];
    $mysqli->setQuery(" SELECT sender_id,
                               fb_account.`name` AS `token`
                        FROM  `fb_chat`
                        JOIN   fb_account ON fb_account.id = fb_chat.account_id
                        where  fb_account.id = '$chat_id'");
    $res = $mysqli->getResultArray();

    $access_token   = $res['result'][0]['token'];
    $url            = "https://graph.facebook.com/v3.2/me/blocked";
    $u_id           = $res['result'][0]['sender_id'];
    $ch             = curl_init($url);
    $jsonData       = '{
            "access_token"  :  "'. $access_token .'",
            "uid"           :  "'. $u_id .'"
        }';
    curl_setopt($ch, CURLOPT_POST, 1); //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    $errors = curl_error($ch);
    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    var_dump($errors);
    var_dump($response);
    die( $result)   ;
}elseif($source=='mail'){

    require 'phpmailer2/Exception.php';
    require 'phpmailer2/PHPMailer.php';
    require 'phpmailer2/SMTP.php';

    $chat_id        = $_REQUEST['chat_id'];
    $file_name      = $_REQUEST['ge'];
    $file_name      = explode(";",$file_name);
    $file_orig_name = explode(";",$_REQUEST['file_name']);
    $sign_id        = $_REQUEST['sign_id'];
    $receivers      = $_REQUEST['mail_addresats'];
    $mail_cc        = $_REQUEST['mail_cc'];
    $mail_bcc       = $_REQUEST['mail_bcc'];

    $receivers2      = $receivers;
    $mail_cc2        = $mail_cc;
    $mail_bcc2       = $mail_bcc;


    $mysqli->setQuery(" SELECT    mail.sender_address AS `sender_id`,
                                  mail_account.`name` AS `name`,
                                  mail_account.`mail` AS `mail`,
                                  mail.`subject`,
                                  mail_account.`id` AS mail_id,
                                  mail_account.`domain` AS `url`,
                                  mail_account.`password` AS `password`
                        FROM     `mail`
                        JOIN      mail_account ON mail_account.id = mail.account_id
                        LEFT JOIN mail_detail ON mail.id = mail_detail.mail_id
                        WHERE     mail.id = '$chat_id'
                        GROUP BY  mail.id");

    $res          = $mysqli->getResultArray();
    $sender       = $res['result'][0]['sender_id'];
    $from         = $res['result'][0]['mail'];
    $subject      = $res['result'][0]['subject'];
    $from_mail_id = $res['result'][0]['mail_id'];
    $url          = $res['result'][0]['url'];
    $password     = $res['result'][0]['password'];

    $mysqli->setQuery("SELECT signature
                       FROM   signature
                       WHERE  id = '$sign_id' AND actived = 1 LIMIT 1");

    $signature      = $mysqli->getResultArray();
    $mail_signature = html_entity_decode($signature[result][0][signature]);
   
    try {
        $mail = new PHPMailer;
        
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = false; 

        $mail->SMTPSecure = false;
        $mail->SMTPAutoTLS = false;
        $mail->Debugoutput = 'html';
        $mail->CharSet = 'UTF-8';
        $mail->Host = $url;
        $mail->Port = 5003;
       
        $mail->Subject  = $subject;
        if(count($file_name) > 0){
            for($i=0;$i<count($file_name)-1;$i++){
                $file = str_replace("http://172.16.0.85/upload/users/", "",$file_name[$i]);
                $mail->addStringAttachment(file_get_contents('http://172.16.0.85/upload/users/'. $file), $file_orig_name[$i]);
                
            }
        }
        //if($attachmet) {
            
            //$mail->AddAttachment('media/uploads/'. $attachmet);
            $esc_message = $esc_message.'<br><br>'.$mail_signature;
            //$esc_message = str_replace($attachmet,"",$esc_message);
       // }
       $receivers = explode(',', $receivers);
       $mail_cc = explode(',', $mail_cc);
       $mail_bcc = explode(',', $mail_bcc);
       $rrr = array();
       $mail->addAddress($sender);
       foreach($receivers AS $receiver){
            $rec = trim($receiver);
            if(!empty($rec)){
                if(filter_var($rec, FILTER_VALIDATE_EMAIL)){
                    $mail->addAddress($rec);
                }
                else{
                    $data['status'] = 'ERR-MAIL';
                    die(json_encode($data));
                }
            }
           
        }
        foreach($mail_cc AS $cc){
            $rec = trim($cc);
            $mail->AddCC($rec);
        }
        foreach($mail_bcc AS $bcc){
            $rec = trim($bcc);
            $mail->AddBCC($rec);
        }
        $mail->setFrom('info@rda.gov.ge');
        
        $mail->msgHTML( $esc_message);
        $mail->send();
        $res = array('status'=>'ok', 'from'=>$from, 'sender'=>$sender, 'esc_message'=>$esc_message);
    }
    catch (phpmailerException $e) {
        $res = array('status'=>'3', 'error'=>$e->errorMessage());
    } catch (Exception $e) {
        $res = array('status'=>'2', 'error'=>$e->getMessage());
    }
    if($res['status']=='ok'){
        
        $mysqli->query= ("
        insert mail_live_detail
        SET
            `datetime`      = NOW(),
            `mail_id` = '". $_REQUEST['chat_id']."',
            `user_id`       = '". $_REQUEST['user_id']."',
            `to`       = '". $receivers2."',
            `cc`       = '". $mail_cc2."',
            `bcc`       = '". $mail_bcc2."',
            `mail_type_id`       = '1',
            `body`          = '".$esc_message."'
                    ");
        $mysqli->execQuery();
        ////////////////----------------------------------------
        $mysqli->query=("
            UPDATE  mail_live
            SET     `user_id`  = '".$_REQUEST['user_id']."',
                    `last_datetime` = NOW(),
                    `mail_status_id`        = '2'
            WHERE   id=$chat_id
            ");
        $mysqli->execQuery();
    }
    die(json_encode($res));


}elseif($source=='viber'){
    $mysqli->query= ("INSERT viber_messages
                         SET `datetime`      = NOW(),
                             `viber_chat_id` = '". $_REQUEST['chat_id']."',
                             `user_id`       = '". $_REQUEST['user_id']."',
                             `time`          = NOW(),
                             `type`          = 'text',
                             `text`          = '".$_REQUEST['message']."'");
    $mysqli->execQuery();
    ////////////////----------------------------------------
    $mysqli->query=("UPDATE viber_chat
                        SET `last_user_id`  = '".$_REQUEST['user_id']."',
                            `last_datetime` = NOW(),
                            `status`        = '2'
                     WHERE   id             = $chat_id");
    
    $mysqli->execQuery();
    //////////////////--------------------------------------
    $mysqli->query=("SELECT sender_id, `viber_account`.token, `viber_account`.name
                     FROM  `viber_chat` 
                     JOIN   viber_account ON `viber_account`.id = `viber_chat`.account_id
                     WHERE  `viber_chat`.id = '$chat_id'");
    
    $res = $mysqli->getResultArray();
    
    $sender     = $res['result'][0]['sender_id'];
    $url        = "https://chatapi.viber.com/pa/send_message";
    $token      = $res['result'][0]['token'];
    $viber_name = $res['result'][0]['name'];
    $ch         = curl_init($url);

    $jsonData   = '{
                "receiver":"'.$sender.'",
                "sender":{
                    "name":"'.$viber_name.'"
                },
                "type":"text",
                "text":"'.$esc_message.'"
            }';
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
        array(  'Content-Type: application/json' ,
            'X-Viber-Auth-Token: '.$token));
        $result = curl_exec($ch);
        $errors = curl_error($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        var_dump($errors);
        var_dump($response);
        die($chat_id);
}else{

    if($chat_id == 0){
        if(strlen($chat_name) == 0){
            $gest_name = "სტუმარი $clientID";
        }else{
            $gest_name = $esc_name;
        }
        $mysqli->setQuery(" INSERT INTO `chat`
                                       (`join_date`, `answer_date`, `answer_user_id`, `name`, `email`, `phone`, `ip`, `country`, `device`, `browser`, `status`, `socet_id`)
                                 VALUES
                                       (NOW(), '', '', '$gest_name', '$esc_mail', '$esc_phone', '$ip', '', '$os', '$br', '1', '$clientID');");
        $mysqli->execQuery();
        $chat_id = $mysqli->getLastId();
        $json = array('chat_id_paste'=>$chat_id);
        echo json_encode($json,JSON_NUMERIC_CHECK);
    }else{
        $json = array('chat_id_paste'=>'');
        echo json_encode($json,JSON_NUMERIC_CHECK);
    }

    if($idd == 0){
        $mysqli->setQuery(" UPDATE  `chat`
                               SET  `answer_date`    = NOW(),
                                    `answer_user_id` = '$user_id',
                                    `status`         = '2',
                                    `seen`           = 0
                            WHERE   `id`='$chat_id' AND status = 1;");
        $mysqli->execQuery();
    }

    if($idd == 1){
        $mysqli->setQuery(" INSERT INTO `chat_details`
                                       (`chat_id`,`message_client`, `message_operator`, `operator_user_id`, `message_datetime`)
                                 VALUES
                                       ('$chat_id', '$esc_message', '', '', NOW());");

        $mysqli->execQuery();
    }else{
        $mysqli->setQuery(" UPDATE  `chat` 
                               SET  `last_person` = '1', 
                                    `seen`        = 0
                            WHERE   `id`          = '$chat_id';");
        $mysqli->execQuery();
        $mysqli->setQuery(" INSERT INTO `chat_details`
                                       (`chat_id`,`message_client`, `message_operator`, `operator_user_id`, `message_datetime`)
                                 VALUES
                                       ('$chat_id', '', '$esc_message', '$user_id', NOW());");

        $mysqli->execQuery();

        $mysqli->setQuery("UPDATE `chat` 
                              SET `last_user_id`           = '$user_id',
                                  `last_user_message_date` = NOW(), 
                                  `seen`                   = 0
                           WHERE   chat.`id`               = '$chat_id'");

        $mysqli->execQuery();
    }
}
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

?>
