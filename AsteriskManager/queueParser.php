<?php

ini_set('max_execution_time', 0);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('ignore_user_abort ', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once("asmanager.php");
require_once("mysqli.class.php");

$filter_queues = unserialize(QUEUES);

$db = new dbClass();
$am = new AsteriskManager();
$am->connect();

$channels   = get_channels($am);
$queues     = get_queues($am,$channels);
// var_dump($queues);

//$ext_states = get_ext_states($am);

/*active calls*/
$values = '';
foreach($filter_queues  as $qn) {
    $contador=1;
    if(!isset($queues[$qn]['members'])) continue;
    foreach($queues[$qn]['members'] as $key=>$val) {
            
        $stat   = "";
        $dur    = "";
        $clid   = "";
        $akey   = $queues[$qn]['members'][$key]['agent'];
        $aname  = $queues[$qn]['members'][$key]['name'];
        $aval   = $queues[$qn]['members'][$key]['type'];
        $myExt  = explode("/",$queues[$qn]['members'][$key][id]);
        $phone  = $channels[$key]['callerid'];
        
        if(array_key_exists($key,$channels)) {

            if($aval=="not in use") {
                $aval = "dialout";
            }

            $clid  = $channels[$key]['callerid'];
            $dur   = $channels[$key]['duration_str'];
        
        }

        $db->setQuery(" SELECT `users`.`id`
                        FROM   `users`
                        JOIN   `extention` ON `extention`.`id` = `users`.`extension_id`
                        WHERE  `extention`.`extention` = '$aname'
                        LIMIT  1");

        $dbResult   = $db->getResultArray();
        $userID     = $dbResult[result][0][id]; 

        $aval2 = preg_replace(" ","_",$aval);
        $mystringaval = $lang[$language][$aval2];

        if($mystringaval==""){
            $mystringaval = $aval;
        }
        
        global $values;
        $key = explode('/', $key);
        $values .= "('$userID', '$qn', '$key[1]', '$aname', '$phone', '$aval', '$dur'),";
        
        $contador++;
    }
}

$values = rtrim($values,',');

if(strlen($values) > 1){
    
    $db->setQuery(" INSERT INTO `asterisk_in_call` (`user_id`, `queue`, `ext`, `ext_name`, `phone`, `status`, `time`) 
                    VALUES $values
                    ON duplicate KEY UPDATE 
                        `user_id` = VALUES(user_id),
                        `phone` = VALUES(phone),
                        `status` = VALUES(status),
                        `time` = VALUES(time)");
    $db->execQuery();

}

/*calls in Queue*/
$values1 = '';
foreach($filter_queues as $qn) {

    $position=1;
    
    if(!isset($queues[$qn]['calls']))  continue;

    foreach($queues[$qn]['calls'] as $key=>$val) {

        $channel    = $queues[$qn]['calls'][$key]['channel'];
        $phone      = $channels[$channel]['callerid'];
        $wait_time  = $queues[$qn]['calls'][$key]['waittime'];
        
        global $values1;
        $values1 .= "('$qn', $position, '$phone', '$wait_time'),";
        
        $position++;

    }

}

$values1 = rtrim($values1,',');
$db->setQuery("TRUNCATE TABLE`asterisk_in_queue`;");
$db->execQuery();
if(strlen($values1) > 1){
    $db->setQuery("INSERT INTO `asterisk_in_queue` (`queue`, `position`, `number`, `wait_time`) VALUES $values1");
    $db->execQuery();

}

?>