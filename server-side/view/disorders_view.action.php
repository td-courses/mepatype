<?php
include('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$action	= $_REQUEST['act'];
$error	= '';
$data	= '';

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$departmetn_id	= $_REQUEST['id'];
	       $page		= GetPage(Getdisorder($departmetn_id));
           $data		= array('page'	=> $page);

		break;
	case 'get_list' :
	    $columnCount = $_REQUEST['count'];
	    $cols[]      = $_REQUEST['cols'];
		 
		$db->setQuery(" SELECT 	disorders.id,
								disorders.`datetime`,
                                disorders.`disorder`,
                                disorders.`penalty`,
                                disorders.`appeal`
					    FROM 	disorders
					    WHERE 	disorders.actived=1");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;
		
		break;
		
	case 'get_columns':
	    
	    $columnCount   = $_REQUEST['count'];
	    $cols[]        = $_REQUEST['cols'];
	    $columnNames[] = $_REQUEST['names'];
	    $operators[]   = $_REQUEST['operators'];
	    $selectors[]   = $_REQUEST['selectors'];
	    //$query = "SHOW COLUMNS FROM $tableName";
	    //$db->setQuery($query,$tableName);
	    //$res = $db->getResultArray();
	    $f=0;
	    foreach($cols[0] AS $col)
	    {
	        $column = explode(':',$col);
	        
	        $res[$f]['Field'] = $column[0];
	        $res[$f]['type'] = $column[1];
	        $f++;
	    }
	    $i = 0;
	    $columns = array();
	    $types = array();
	    foreach($res AS $item)
	    {
	        $columns[$i] = $item['Field'];
	        $types[$i] = $item['type'];
	        $i++;
	    }
	    
	    
	    $dat = array();
	    $a = 0;
	    for($j = 0;$j<$columnCount;$j++)
	    {
	        if(1==2)
	        {
	            continue;
	        }
	        else{
	            
	            if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
	            $op = false;
	            if($columns[$j] == 'id')
	            {
	                $width = "5%";
	            }
	            else if($columns[$j] == 'position'){
	                $width = "12%";
	            }
	            else{
	                $width = 'auto';
	            }
	            if($columns[$j] == 'inc_id')
	            {
	                $hidden = true;
	            }
	            else if($columns[$j] == 'docs_project_id')
	            {
	                $hidden = true;
	            }
	            else if($columns[$j] == 'id')
	            {
	                $hidden = true;
	            }
	            else{
	                $hidden = false;
	            }
	            if($res['data_type'][$j] == 'date')
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
	            }
	            else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
	            }
	            else
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
	            }
	            $a++;
	        }
	        array_push($dat,$g);
	        
	    }
	    
	    //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
	    
	    $new_data = array();
	    //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
	    for($j=0;$j<$columnCount;$j++)
	    {
	        if($types[$j] == 'date')
	        {
	            $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
	        }
	        else if($types[$j] == 'number'){
	            
	            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
	        }
	        else
	        {
	            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
	        }
	    }
	    
	    $filtArr = array('fields'=>$new_data);
	    
	    
	    
	    $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
	    
	    
	    //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
	    
	    $data = $kendoData;
	    //$data = '[{"gg":"sd","ads":"213123"}]';
	    
	    break;
	case 'save_disorder':
		$disorder_id 		= $_REQUEST['id'];
        $disorder  = $_REQUEST['disorder'];        
        $penalty   = $_REQUEST['penalty'];       
        $appeal    = $_REQUEST['appeal'];  
        $comment   = $_REQUEST['comment'];       
	
		if ($disorder_id == '') {
			Adddisorder( $disorder_id, $disorder, $penalty, $appeal, $comment);
		}else {
			Savedisorder($disorder_id, $disorder, $penalty, $appeal, $comment);
		}
		break;
	case 'disable':
		$department_id	= $_REQUEST['id'];
		Disabledepartment($department_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Adddisorder($disorder_id, $disorder, $penalty, $appeal, $comment){
    global $db;
    
	$user_id	= $_SESSION['USERID'];
	$db->setQuery("INSERT INTO  `disorders`	 
                           SET  `datetime` = NOW(),
        						`disorder` = '$disorder',
                                `penalty`  = '$penalty',
                                `user_id`  ='$user_id',
                                `appeal`   = '$appeal',
                                `comment`  = '$comment'");
	$db->execQuery();
}

function Savedisorder($disorder_id, $disorder, $penalty, $appeal, $comment){
    global $db;
	
	$user_id	= $_SESSION['USERID'];
	$db->setQuery("	UPDATE `disorders`
					   SET `datetime` = NOW(),
						   `disorder` = '$disorder',
                           `penalty` = '$penalty',
						   `user_id` ='$user_id',
                           `appeal`  = '$appeal',
                           `comment` = '$comment'
                     WHERE `id` = $disorder_id ");
	
	$db->execQuery();
}

function Disabledepartment($department_id){
    
	global $db;
	$db->setQuery("	UPDATE `disorders`
					   SET `actived` = 0
					WHERE  `id` = $department_id");
	
	$db->execQuery();
}

function CheckdepartmentExist($department_name){
    
    global $db;
    $db->setQuery("	SELECT `id`
					FROM   `department`
					WHERE  `name` = '$department_name' && `actived` = 1");
    
    $req = $db->getResultArray();
    $res = $req[result][0];
    
	if($res['id'] != ''){
		return true;
	}
	return false;
}


function Getdisorder($department_id){
    global $db;
	$db->setQuery(" SELECT  `disorders`.`id`,
						    `user_info`.`name`,
                            `disorders`.`datetime`,
                            `disorders`.`disorder`,
                            `disorders`.`penalty`,
                            `disorders`.`appeal`,
                            `disorders`.`comment`
					FROM    `disorders`
                    JOIN    `user_info` ON `user_info`.user_id = `disorders`.`user_id`
					WHERE   `disorders`.`id` = $department_id");

	$req = $db->getResultArray();
	$res = $req[result][0];
	return $res;
}

function GetPage($res = ''){
    
	$data = '
	<div id="dialog-form">
	    <fieldset style="border-radius: 0px;">
            <legend>ძირითადი ინფორმაცია</legend>
	    	<div class="dialog-grid">
                <div class="grid1">
                    <div>
                        <span>დააფორმირა :</span><br>
                        <span class="text">'.$res['name'].'</span>
                    </div>
                    <div>
                        <span>ფორმირების თარირი :</span><br>
                        <span class="text">'.$res['datetime'].'</span>
                    </div>
                </div>  
                <div class="grid2">
                    <span>დსცპ დარღვევა</span>

                    <textarea style="padding: 5px;" id="disorder">'.$res['disorder'].'</textarea>
                
                </div> 
                <div class="grid3">
                    <div>
                        <span>საჯარიმო ქულა</span><br>
                        <div style="display: flex;"><input style="margin-top: 5px;" id="penalty" type="number" class="idle" value="'.$res['penalty'].'" /> <span style="font-size: 17px; margin-top: 8px;">%</span></div>
                    </div>
                    <div>
                        <span>გასაჩივრების ვადა (დღე)</span><br>
                        <input style="margin-top: 5px; width: 100%;" id="appeal" type="number" class="idle" value="'.$res['appeal'].'" />
                    </div>
                </div> 
                <div class="grid2">
                    <span>კომენტარი</span>
                    <textarea style="padding: 5px;" id="comment">'.$res['comment'].'</textarea>
                </div>           
            
            </div>
			<!-- ID -->
			<input type="hidden" id="disorder_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>';
	
	return $data;
}

?>
