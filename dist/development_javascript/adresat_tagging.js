const addAdresatBtn = document.querySelectorAll('.add_in_input_btn')

const adresatAddBtn = document.querySelectorAll('.adresat__add_btn')


const adresatCreateUserGroupBtn = document.querySelector('.adresat__create_user_group_btn')
const adresatAddUser = document.querySelector('.adresat__add_btn__user')
const adresatAddUserToGroup = document.querySelector('.adresat__create_user_group_btn')


const adresatUsers = document.querySelector('.container__users')
const adresatGroups = document.querySelector('.container__groups')
const adresatDepartment = document.querySelector('.container__departments')


const taggedContainer = document.querySelectorAll('.adresat_added ul')

const closeAdresatBtn = document.querySelector('.close_adresatBtn')



let selectedUsers = [];
let users = [];

if (closeAdresatBtn) {
    closeAdresatBtn.addEventListener('click', () => {
        const adresatInputsContainer = document.querySelector('.adresat_inputs')
        adresatInputsContainer.classList.toggle('display-grid');
    })
}


for (let i = 0; i < addAdresatBtn.length; i++) {
    const adresatInputsContainer = document.querySelectorAll('.adresat_inputs')

    addAdresatBtn[i].addEventListener('click', () => {
        adresatInputsContainer[i].classList.toggle('display-grid');
    })
}



const getUsersDepartmentGroups = () => {

    let parser = new DOMParser();

    if (adresatUsers) {
        $.ajax({
            url: "server-side/call/action_action.php",
            data: "act=get_users",
            dataType: "json",
            success: function (users) {
                let User = users
                adresatUsers.innerHTML = "";

                for (let i = 0; i < User.length; i++) {
                    let userForAdresat = parser.parseFromString(
                        `<div class="adresat__users__container" data-userid='${User[i].id}' data-typeoftag="user" data-tagedId='${User[i].id}' onclick='markeUser(this)'>
                            <div class="user_image" style="background-image: url('media/uploads/file/${User[i].image_name}')"></div>
                            <div class="user_name">
                                <p>${User[i].username}</p>
                            </div>
                            <div class="user_departament" style="color: #808080d9">${User[i].department}</div>
                        </div>`,
                        "text/html");

                    adresatUsers.append(userForAdresat.body.firstElementChild);
                }
            }
        });



        $.ajax({
            url: "server-side/call/action_action.php",
            data: "act=get_groups",
            dataType: "json",
            success: function (groups) {
                let Group = groups.groups
                adresatGroups.innerHTML = "";

                if (Group) {
                    for (let i = 0; i < Group.length; i++) {
                        let groupsForAdresat = parser.parseFromString(
                            `<div class="adresat__users__container adresat__groups__container " data-groupid='${Group[i].id}'  data-typeoftag="group" data-tagedId='${Group[i].id}' onclick='markeUser(this)'>
                            <div class="user_image" style="background-image: url('media/uploads/file/${Group[i].image_name}')"></div>
                            <div class="user_name">
                                <p>${Group[i].name}</p>
                            </div>
                            <div class="user_departament" style="color: #808080d9">${Group[i].members}</div>
                            <div class="delete_group"  onclick = "deleteGroup(event)">✖</div>
                        </div>`,
                            "text/html");

                        adresatGroups.append(groupsForAdresat.body.firstElementChild);
                    }
                }
            }
        });





        $.ajax({
            url: "server-side/call/action_action.php",
            data: "act=get_departments",
            dataType: "json",
            success: function (departments) {
                let Departments = departments.departments
                adresatDepartment.innerHTML = "";

                for (let i = 0; i < Departments.length; i++) {
                    let departmentForAdresat = parser.parseFromString(
                        `<div class="adresat__users__container adresat__department__container " data-departmentid='${Departments[i].id}'  data-typeoftag="department" data-tagedId='${Departments[i].id}' onclick='markeUser(this)'>
                    <div class="user_image" style="background-image:url('media/uploads/file/${Departments[i].image_name}')"></div>
                    <div class="user_name">
                        <p>${Departments[i].name}</p>
                    </div>
                    <div class="user_departament" style="color: #808080d9">${Departments[i].members}</div>
                </div>`,
                        "text/html");

                    adresatDepartment.append(departmentForAdresat.body.firstElementChild);
                }
            }
        });
    }
}

getUsersDepartmentGroups()



const markeUser = (userElement) => {
    userElement.classList.toggle('active-adresat-link')
    let index = selectedUsers.indexOf(userElement);

    if (selectedUsers.includes(userElement) == true) {
        if (selectedUsers.indexOf(userElement) > -1) {
            selectedUsers.splice(index, 1);
        }
    } else {
        selectedUsers.push(userElement)
    }
}




let deleteTagBtn = document.querySelectorAll('.delete_tag')
let userNames = [];
let userIds = [];
let userDepartment = [];
let userImageUrls = [];


let selectedUsersObjects = [];
let selectedGroupObjects = [];
let selectedDepartmentObjects = [];
let selected;


if (adresatAddUser) {
    adresatAddUser.addEventListener('click', () => {
        selected = [];
        selected = document.querySelectorAll(".active-adresat-link");

        let selectedUsersElements = document.querySelectorAll(".active-adresat-link[data-typeoftag='user']");
        let selectedGroupsElements = document.querySelectorAll(".active-adresat-link[data-typeoftag='group']");
        let selectedDepartmentElements = document.querySelectorAll(".active-adresat-link[data-typeoftag='department']");



        selectedGroupsElements.forEach(groupEl => {
            let groupeObj = {
                groupId: groupEl.dataset.groupid,
                groupName: groupEl.children[1].childNodes[1].innerHTML
            }
            selectedGroupObjects.push(groupeObj)
        })

        selectedUsersElements.forEach(userEl => {
            let userObj = {
                usersId: userEl.dataset.userid,
                username: userEl.children[1].childNodes[1].innerHTML
            }
            selectedUsersObjects.push(userObj)
        })

        selectedDepartmentElements.forEach(userEl => {
            let departmentObj = {
                departmentsId: userEl.dataset.departmentid,
                departmentname: userEl.children[1].childNodes[1].innerHTML
            }
            selectedDepartmentObjects.push(departmentObj)
        })



        for (let i = 0; i < selectedUsersElements.length; i++) {
            let liTag = parser.parseFromString(`<li data-tagedid="${selectedUsersObjects[i].usersId}" onclick='markeUser(this)' data-typeoftag='user'> 
                                                    <div class="tagged"> 
                                                        <p>${selectedUsersObjects[i].username}</p>
                                                        <div class="delete_tag"  onclick = "deleteTag(event)">✖</div>
                                                    </div>
                                                </li>`, "text/html")

            let ulParentTag = addAdresatBtn[0].parentNode.parentNode;
            ulParentTag.insertBefore(liTag.body.firstElementChild, ulParentTag.childNodes[0])
        }


        for (let i = 0; i < selectedGroupsElements.length; i++) {
            let liTag = parser.parseFromString(`<li data-tagedid="${selectedGroupObjects[i].groupId}" onclick='markeUser(this)' data-typeoftag='group'> 
                                                    <div class="tagged"  style="background: #FAAF30; color: white"> 
                                                        <p style="color: white; font-family: noto-light, lexend-regular" >${selectedGroupObjects[i].groupName}</p>
                                                        <div class="delete_tag"  onclick = "deleteTag(event)">✖</div>
                                                    </div>
                                                </li>`, "text/html")

            let ulParentTag = addAdresatBtn[0].parentNode.parentNode;
            ulParentTag.insertBefore(liTag.body.firstElementChild, ulParentTag.childNodes[0])
        }

        for (let i = 0; i < selectedDepartmentElements.length; i++) {
            let liTag = parser.parseFromString(`<li data-tagedid="${selectedDepartmentObjects[i].departmentsId}" onclick='markeUser(this)' data-typeoftag='department'> 
                                                    <div class="tagged"  style="background: #00A57F; color:white "> 
                                                        <p style="color: white; font-family: noto-light, lexend-regular" >${selectedDepartmentObjects[i].departmentname}</p>
                                                        <div class="delete_tag"  onclick = "deleteTag(event)">✖</div>
                                                    </div>
                                                </li>`, "text/html")

            let ulParentTag = addAdresatBtn[0].parentNode.parentNode;
            ulParentTag.insertBefore(liTag.body.firstElementChild, ulParentTag.childNodes[0])
        }

        deleteTagBtn = document.querySelectorAll('.delete_tag')





        selected.forEach(selectedElem => {
            selectedElem.style.display = "none";
            selectedElem.classList.remove("active-adresat-link");
        })

        selectedGroupObjects = [];
        selectedDepartmentObjects = [];
        // userNames = [];
        selectedUsersObjects = []

        selected = [];
    })
}


if (adresatAddUserToGroup) {
    adresatAddUserToGroup.addEventListener('click', () => {
        let groupMembersIds = []
        let allSelectedUsersForGroup = document.querySelectorAll('.active-adresat-link[data-typeoftag="user"]')
        let allSelected = document.querySelectorAll('.active-adresat-link')

        let isAllowdToCreateGroup = true;



        allSelected.forEach(selected => {
            if (selected.dataset.typeoftag == "group" || selected.dataset.typeoftag == "department") {
                isAllowdToCreateGroup = false;

            } else {
                isAllowdToCreateGroup = true
            }
        })


        if (!isAllowdToCreateGroup || allSelected.length <= 1) {
            alert("ჯგუფის შექმნა შესაძლებელია მხოლოდ მომხმარებლებით და ჯგუფში უნდა იყოს 1 მომხმარებელზე მეტი")
        } else {
            allSelectedUsersForGroup.forEach(user => {
                user.classList.remove('active-adresat-link')
                groupMembersIds.push(user.dataset.userid)
            })


            let groupName = prompt("Enter Group Name");

            while (groupName == "") {
                alert("შეიყვანეთ ჯგუფის სახელი")
                groupName = prompt("Enter Group Name");
            }

            let group = {
                name: groupName,
                members: groupMembersIds
            }


            let groupAndAct = {
                act: "create_group",
                group: group
            }


            // if(groupName == ""){

            // }

            $.ajax({ //aaaq
                url: "server-side/call/action_action.php",
                data: groupAndAct,
                success: function (group) {
                    $.ajax({
                        url: "server-side/call/action_action.php",
                        data: "act=get_groups",
                        dataType: "json",
                        success: function (groups) {
                            let Group = groups.groups

                            console.log(Group);
                            const adresatGroups = document.querySelector('.container__groups')



                            adresatGroups.innerHTML = "";


                            for (let i = 0; i < Group.length; i++) {
                                let groupsForAdresat = parser.parseFromString(
                                    `<div class="adresat__users__container adresat__groups__container " data-groupid='${Group[i].id}'  data-typeoftag="group" data-tagedId='${Group[i].id}' onclick='markeUser(this)'>
                                                <div class="user_image" style="background-image: url('media/uploads/file/${Group[i].image_name}')"></div>
                                                <div class="user_name">
                                                    <p>${Group[i].name}</p>
                                                </div>
                                                <div class="user_departament" style="color: #808080d9">${Group[i].members}</div>
                                                <div class="delete_group"  onclick = "deleteGroup(event)">✖</div>
                                            </div>`,
                                    "text/html");


                                adresatGroups.append(groupsForAdresat.body.firstElementChild);

                            }





                        }
                    });
                }
            })





            let createdGroup = document.querySelectorAll('.created_goup')


            for (let i = 0; i < createdGroup.length; i++) {
                createdGroup[i].addEventListener('click', _ => {
                    createdGroup[i].classList.toggle('active-adresat-link')
                    selectedUsers.push(createdGroup[i])
                })
            }
        }

    })
}



const deleteTag = (e) => {
    let liTag = e.target.parentNode.parentNode;
    let taggedId = liTag.dataset.tagedid;
    let taggedType = liTag.dataset.typeoftag;
    liTag.remove();
    let showElement = document.querySelector(`.adresat__users__container[data-tagedid='${taggedId}'][data-typeoftag='${taggedType}`);
    showElement.style.display = "grid"
}



const deleteGroup = (e) => {
    let groupElement = e.target.parentNode;
    let groupId = groupElement.dataset.groupid;


    let deletGrou = {
        act: "delete_group",
        groupId: groupId
    }

    $.ajax({
        url: "server-side/call/action_action.php",
        data: deletGrou,
        success: function (group) {
            groupElement.remove();
        }
    })
}

