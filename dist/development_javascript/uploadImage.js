// const postBtns = document.querySelectorAll('.popupFormPostBtn')

// if (postBtns) {
//     postBtns.forEach(btn =>{
//         btn.addEventListener('click', () =>{
//             uploadImage();
//         })
//     })
// }


// const uploadImage = () => {
//     const imageUploadElement = document.querySelector('#image_upload')
//     var file_name = imageUploadElement.files[0].name;
//     var file_size = imageUploadElement.files[0].size;
//     var file_type = imageUploadElement.files[0].type.split('/')[1].toLowerCase();
//     let path = "media/uploads/posts";

    
//     if ($.inArray(file_type, ['png', 'jpg']) == -1) {
//         alert("დაშვებულია მხოლოდ 'png', 'jpg'  გაფართოება");
//     } else if (file_size > '15728639') {
//         alert("ფაილის ზომა 15MB-ზე მეტია");
//     } else {
//         $.ajaxFileUpload({
//             url: "server-side/call/action_action.php",
//             secureuri: false,
//             fileElementId: "image_upload",
//             dataType: 'json',
//             data: {
//                 act: "upload_image",
//                 file_name_original: file_name,
//                 file_type: file_type,
//                 file_size: file_size,
//                 path: path
//             },
//             success: function (imageName) {
//                 let imageUploadBtn = document.getElementById('image_upload')
//                 imageUploadBtn.setAttribute("data-imagename", imageName.file_name)
//             }
//         });
//     }
// }

$(document).on("change", "#image_upload", function () {
    var file_url = $(this).val();
    var file_name = this.files[0].name;
    var file_size = this.files[0].size;
    var file_type = file_url.split('.').pop().toLowerCase();
    let path = "media/uploads/posts"


    if ($.inArray(file_type, ['png', 'jpg']) == -1) {
        alert("დაშვებულია მხოლოდ 'png', 'jpg'  გაფართოება");
    } else if (file_size > '15728639') {
        alert("ფაილის ზომა 15MB-ზე მეტია");
    } else {
        $.ajaxFileUpload({
            url: "server-side/call/action_action.php",
            secureuri: false,
            fileElementId: "image_upload",
            dataType: 'json',
            data: {
                act: "upload_image",
                file_name_original: file_name,
                file_type: file_type,
                file_size: file_size,
                path: path
            },
            success: function (imageName) {
                let imageUploadBtn = document.getElementById('image_upload')
                imageUploadBtn.setAttribute("data-imagename", imageName.file_name)
            }
        });
    }
});