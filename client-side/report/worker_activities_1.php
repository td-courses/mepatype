<html>
<head>
	<script type="text/javascript">
		var aJaxURL	= "server-side/report/worker_activities_1.action.php";
       // var aJaxURL2	= "server-side/report/worker_activities_2.action.php";		//server side folder url
		var tName	= "example";													//table name
		var fName	= "add-edit-form";												//form name
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		    	
		$(document).ready(function () {        	
				
 						
			/* Add Button ID, Delete Button ID */
			

			$("#filter_button").button();
        $("#excel_export").button();
        GetDate("start_date");
        GetDate("end_date");

		GetDate("start_time");
		GetDate("end_time");
		$("#operator_id").chosen();

        LoadTable();
		GetButtons("add_button", "");			
			SetEvents("add_button", "", "", tName, fName, aJaxURL);
        var start_date = new Date();
        start_date.setHours(0,0,0,0)
        $("#start_date").datepicker().datepicker("setDate", start_date);
        $("#end_date").datepicker().datepicker("setDate", new Date());
		});
        
		function LoadTable(){
			var start_date2 = $('#start_time').val();
            var end_date2   = $('#end_time').val();
			
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list", 7,"start_date="+start_date2 + "&end_date=" + end_date2, 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
  	    	}, 90);
		}

		$(document).on("click", "#filter_button", function () {
			// start_date = $('#start_date').val();
            // end_date   = $('#end_date').val();

			start_date = $('#start_time').val();
            end_date   = $('#end_time').val();

			var operator = $("#operator_id").val();

			
			GetDataTable(tName, aJaxURL, "get_list", 7,"start_date="+start_date + "&end_date="+end_date+ "&operator=" + operator, 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
  	    	}, 90);
	    });
	        
		function LoadDialog(){
			var id		= $("#department_id").val();
			
			/* Dialog Form Selector Name, Buttons Array */

			var buttons={
				"cancel":{
					       text:"დახურვა",
					        id:"cancel-dialog",
					        click:function(){
						    $(this).dialog("close");
					       }
				}
			};
			GetDialog(fName, 900, "auto", buttons);

            GetDataTable("example2", aJaxURL, "get_list_2", 5,"start_date_1="+start_date + "&end_date_1=" + end_date+"&act_id="+$('#id_2').val()+"&start_time="+$('#start_time').val()+"&end_time="+$('#end_time').val(), 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
  	    	}, 90);
		}
		/*
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_department";
	    	param.id		= $("#department_id").val();
	    	param.name		= $("#name").val();
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

		*/
	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#FAFAFA');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });

	    $(document).on("click", "#show_log", function () {
	    	LoadTableLog();
	    	$("#example_wrapper,#example").css('display','none');
	    	$("#example_log_wrapper").css('display','block');
	    	$("#example_log").css('display','table');
	    	$('#add_button,#delete_button').attr('disabled','disabled');
	    	
	    	$(this).css('background','#2681DC');
            $(this).children('img').attr('src','media/images/icons/log_w.png');
    	
    		$("#show_table").css('background','#E6F2F8');
            $("#show_table").children('img').attr('src','media/images/icons/table.png');
	    	
	    });

	    $(document).on("click", "#show_table", function () {
	    	LoadTable();
	    	$("#example_log_wrapper,#example_log").css('display','none');
	    	$("#example_wrapper").css('display','block');
	    	$("#example").css('display','table');
	    	$('#add_button,#delete_button').removeAttr('disabled');
	    	
	    	$("#show_log").css('background','#E6F2F8');
            $("#show_log").children('img').attr('src','media/images/icons/log.png');

            $(this).css('background','#2681DC');
            $(this).children('img').attr('src','media/images/icons/table_w.png');
	    });
	   
    </script>
    <style type="text/css">
        #table_right_menu{
            position: relative;
            float: right;
            width: 70px;
            top: 42px;
        	z-index: 99;
        	border: 1px solid #E6E6E6;
        	padding: 4px;
        }
        
        .ColVis, .dataTable_buttons{
        	z-index: 100;
        }
        .callapp_head{
        	font-family: pvn;
        	font-weight: bold;
        	font-size: 20px;
        	color: #2681DC;
        }
        #example_log tbody .dataTables_empty{
        	text-align: center !important;
        }
        #example_log tbody td:last-child{
        	text-align: left !important;
        }
		#filter_div{
            margin-bottom: 10px;
        }
        #filter_div_1{
            margin-bottom: 10px;
        }
        #filter_button span{
            padding: 2px 8px 0px 8px;
        }

        #filter_button_1 span{
            padding: 2px 8px 0px 8px;
        }

        .wfm-report-activitie-color{
			width: 10px;
    		height: 10px;
			display: inline-block;
			margin-right: 5px;
			margin-top: 4px;
		}
    </style>
</head>

<body>



















<div id="tabs">
<div class="l_responsible_fix" style="width: 80%;">
<div class="callapp_head">თანამდებობა<hr class="callapp_head_hr"></div>

<div id="filter_div"  style="display: none;">
                <input type="text" id="start_date" value="">
                <input type="text" id="end_date" value="">
                <button id="filter_button_6">ფილტრი</button>
            </div>
<div id="button_area" style="display: none;">
	<button id="add_button"  style="display: none;">გაგზავნა</button>
	<!-- <button id="delete_button">წაშლა</button> -->
	
</div>


<div class="clear"></div>
			<div id="rest" style="width: 100%; float:none;">
				<div id="button_area">
	            	<div class="left" style="width: 200px;">
	            		<label for="search_start" class="left" style="margin: 9px 0 0 9px; font-size: 12px;">დასაწყისი</label>
	            		<input type="text" name="search_start" id="start_time" value="" class="inpt right" style="width: 110px; height: 28px;"/>
	            	</div>
	            	<div class="left" style="width: 210px;margin-right: 15px;">
	            		<label for="search_end" class="left" style="margin: 9px 0 0 9px; font-size: 12px;">დასასრული</label>
	            		<input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 110px; height: 28px;"/>
            		</div>	
					<div class="left" style="width: 210px;">
					<select id="operator_id" style="width: 281px;">
                                <option value="0">ყველა დეპარტამენტი</option>
                                <?php
                                $db->setQuery("SELECT department.`id`,
                                                      department.`name`
                                               FROM   department
                                            --    JOIN   user_info ON users.id = user_info.user_id
											--    JOIN   department ON user_info.dep_id = department.id
                                               WHERE  department.`actived` = 1 ");

                                $res = $db->getResultArray();
                                foreach($res[result] AS $req){
                                    $data .= "<option value='$req[id]'>$req[name]</option>";
                                }
                                echo $data;
                                ?>
                            </select>
            		</div>
					<div class="left" style="width: 210px;">
						
						<button style="margin-left:80px; height:28px;" id="filter_button">ფილტრი</button>
            		</div>	
            	</div>
            	
            		
            	
		<div class="clear"></div>







<table id="table_right_menu">
<tr>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
</td>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14">
</td>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
</td>
</tr>
</table>
    <table class="display" id="example" style="width: 100%;" >
        <thead>
            <tr id="datatable_header">
                <th>ID</th>
               
                <th style="width: 16%;">ოპერატორი</th>
                <th style="width: 16%;">ავტორიზაცია</th>
                <th style="width: 17%;">აქტივობა</th>
                <th style="width: 17%;">აქტ.ხანგძლივობა</th>
                <th style="width: 17%;">გასვლა</th>
                <th style="width: 17%;">სესიის ხან.</th>
            
            </tr>
        </thead>
        <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
        
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>

             
            </tr>
        </thead>
    </table>
    </div>
	</div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="თანამდებობა">
    	<!-- aJax -->
	</div>
</body>
</html>


