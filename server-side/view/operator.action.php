<?php 

include('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();


$action	= $_REQUEST['act'];
$error	= '';
$data	= '';
$user   = $_SESSION['USERID'];

switch($action){
    case "get_tabs" :
        $selected = $_REQUEST['selected'];
        $tabs = get_tabs($selected);
        $data = array("tabs" => $tabs);
    break;
    case "get_page" :

          $data = array("page" => get_page(get_operator($user)));  

    break;
    case "get_operator" :
        $user_id = $_SESSION['USERID'];
        $db->setQuery("SELECT `user_info`.`name` AS user, `group`.`name` AS `position`
                       FROM   `users`
                       JOIN   `user_info` ON `user_info`.user_id = `users`.id
                       JOIN   `group` ON `users`.group_id = `group`.id
                       WHERE  `users`.id = '$user_id' LIMIt 1 ");
        
        $req = $db->getResultArray();
        $res = $req[result][0];
        
        $data = array("user"=>$res['user'], "position" => $res['position']);
    break; 
    case "get_chart" :

        $divs = '';
        $count = 0;
        $db->setQuery("SELECT CASE
                                    WHEN source_id = 1 THEN 'ზარი'
                                    WHEN source_id = 11 THEN 'ჩათი'
                                    WHEN source_id = 6 THEN 'FB მესენჯერი'
                                    WHEN source_id = 10 THEN 'ვებ-ზარი'
                                    WHEN source_id = 7 THEN 'ელ-ფოსტა'
                                    WHEN source_id = 9 THEN 'ვაიბერი'
                                    ELSE 'სხვა'
                               END AS `name`,
                               CASE
                                    WHEN source_id = 1 THEN  'call'
                                    WHEN source_id = 11 THEN 'chat'
                                    WHEN source_id = 6 THEN  'fb'
                                    WHEN source_id = 10 THEN 'web'
                                    WHEN source_id = 7 THEN  'mail'
                                    WHEN source_id = 9 THEN  'viber'
                                    ELSE 'other'                       
                               END AS `icon`,
                               ROUND((COUNT(*) * 100 )/(SELECT COUNT(*) FROM incomming_call WHERE user_id = $user),2) AS `percent`,
                               COUNT(*) AS `count`
                      FROM     incomming_call 
                      WHERE    user_id = $user
                      GROUP BY source_id");
        
        $res = $db->getResultArray();
        $values = array();
        foreach ($res[result] AS $arr){
            $divs .='<div><div class="'.$arr['icon'].'_icon"></div>'.$arr['name'].'</div><div> '.$arr['percent'].' % </div><div>'.$arr['count'].'</div> ';
            $count += $arr['count'];
            $values[$arr['icon']] = $arr['count'];
        }


        $page = '<div id="container" style="min-width: 210px; height: 300px; max-width: 400px; margin: 0"></div>
                 <div class="pie-counter">'.$count.'</div>
                 <div class="pie-table-grid">'.$divs.'</div>';
    
        $data = array("html" =>$page, 'values' => $values);
        break;
    case "get_list_example_shemowmebuli":
        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];
        $tab_id = $_REQUEST['tab_id'] != "undefined" ? $_REQUEST['tab_id'] : 1;
		 
		$db->setQuery(" SELECT    monitoring.id,
                                  monitoring_main.datetime,
                                  FROM_UNIXTIME(monitoring.request_date),
                                  source.`name`,
                                  info_category.`name`,
                                  monitoring_action_type.`name`,
                                  SEC_TO_TIME(monitoring.call_duration),
                                  user_info.`name`,
                                  IF(monitoring.distribution_type=2,'მანუალური','ავტომატური'),
                                  CASE
                                      WHEN monitoring_status_id = 1 THEN 'შემოწმებული'
                                      WHEN monitoring_status_id = 2 THEN 'გასაჩივრებული'
                                      WHEN monitoring_status_id = 3 THEN 'დასრულებული'
                                  END,
                                  SUM(IF(monitoring_rate.question_level<0,0,monitoring_rate.question_level)),
                                  monitoring_user.`name`
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      source ON monitoring.source_id = source.id
                        JOIN      info_category ON monitoring.call_type_id = info_category.id
                        LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                        JOIN      monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                        JOIN      user_info ON monitoring.operator_id = user_info.user_id
                        JOIN      user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                        WHERE     monitoring.monitoring_status_id = '$tab_id' AND monitoring.operator_id = $user
                        GROUP BY  monitoring.id");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;
		
        break;
    case "op-grid":
       
        $db->setQuery(" SELECT   user_info.`name`,
                                 ROUND(SUM(monitoring_rate.question_level)/COUNT(DISTINCT monitoring.id),2) AS `k`
                        FROM     monitoring
                        JOIN     monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN     monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                        JOIN     user_info ON user_info.user_id = monitoring.operator_id
                        WHERE    monitoring.monitoring_main_id > 0 AND monitoring.monitoring_main_id > 0
                        AND      monitoring.actived = 1 AND monitoring.operator_id > 0 AND user_info.user_id = $user
                        GROUP BY monitoring.operator_id LIMIT 1");
        
        $req   = $db->getResultArray();
        $query = $req[result][0];
		 
		$db->setQuery("SELECT * 
                       FROM (

                        (SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0) AS `all`,
                        
                        (SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0 AND monitoring_status_id = 0) AS `shs`,
                        
                        (SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0 AND monitoring_status_id > 0) AS `shf`,
                        
                        (SELECT ROUND(((SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0 AND monitoring_status_id = 0) * 100) / (SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0),2)) AS `shs_perc`,
                        
                        (SELECT ROUND(((SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0 AND monitoring_status_id > 0) * 100) / (SELECT COUNT(*) FROM `monitoring` WHERE operator_id = $user AND monitoring_main_id > 0),2)) AS `shf_perc` )");

        $req = $db->getResultArray();
        
		$Result = $req[result][0];
		
        $html = '<div></div>
                 <div>სულ</div>
                 <div>შესაფასებელი</div>
                 <div>შეფასებული</div>

                 <div>მომართვები</div>
                 <div>'.$Result[0].'</div>
                 <div>'.$Result[1].'</div>
                 <div>'.$Result[2].'</div>

                 <div></div>
                 <div></div>
                 <div>'.$Result[3].' %</div>
                 <div>'.$Result[4].' %</div>';

		$data = array("html"	=> $html, "k" => $query['k']);

		
    break;

    case "get_list_example2":
        $columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
        $tab_id = $_REQUEST['tab_id'] != "undefined" ? $_REQUEST['tab_id'] : 1;
		 
		$db->setQuery(" SELECT '','დავალება' AS '1'  , '150' AS '2' , '100%' AS '3' , '150' AS '4' ,'145' AS '5', '97%' AS '5' , '100%' AS '6'
                        UNION ALL
                        SELECT '','დაბლოკვა' AS '1'  , '20' AS '2' , '100%' AS '3' , '20' AS '4' ,'20' AS '5', '100%' AS '5' , '99%' AS '6'
                        UNION ALL
                        SELECT '','ჰოლდი' AS '1'  , '98' AS '2' , '100%' AS '3' , '98' AS '4' ,'95' AS '5', '97%' AS '5' , '100%' AS '6'
                        UNION ALL
                        SELECT '','გამავალი ზარი' AS '1'  , '3' AS '2' , '100%' AS '3' , '3' AS '4' ,'3' AS '5', '100%' AS '5' , '80%' AS '6'");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;
    break;

    case "get_list_example3":
        $columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
        $tab_id = $_REQUEST['tab_id'] != "undefined" ? $_REQUEST['tab_id'] : 1;
		 
		$db->setQuery(" SELECT    monitoring.operator_id,
                                  info_category.`name`,
                                  COUNT(*) AS `sul`,
                                  0 AS `gegmiuri`,
                                  COUNT(*) AS `shesafasebeli`,
                                  SUM(IF(monitoring.monitoring_status_id>0,1,0)) AS `shemowmebuli`,
                                  ROUND(SUM(IF(monitoring.monitoring_status_id>0,1,0))/COUNT(*)*100,2) AS `shefasebuli_procenti`,
                                  ROUND(SUM((SELECT SUM(monitoring_rate.question_level)
                                             FROM   monitoring_rate
                                             WHERE  monitoring_rate.monitoring_id = monitoring.id))/SUM(IF(monitoring.monitoring_status_id>0,1,0)),2) AS `Kხრსხ`
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      users ON monitoring.operator_id = users.id
                        JOIN      info_category ON monitoring.call_type_id = info_category.id
                        WHERE     monitoring.call_type_id > 0 AND monitoring.operator_id = '$user' AND monitoring.monitoring_main_id > 0
                        GROUP BY  monitoring.call_type_id");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;
    break;


    case "get_list_example":
        $columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
        $tab_id = $_REQUEST['tab_id'] != "undefined" ? $_REQUEST['tab_id'] : 1;
        $user_id = $_SESSION['USERID'];
		$db->setQuery("SELECT 	 operator_disorder.id,
                                 operator_disorder.`datetime`,
                                 operator_disorder.`appeal_date`,
                                `user_info`.`name`,
                                `group`.`name`,
                                 disorders.`disorder`,
                                 disorders.`penalty`,
                                `users_point`.`point` AS `balance`
                        FROM 	 operator_disorder
                        JOIN     users ON users.id = operator_disorder.operator_id
                        JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                        JOIN    `user_info` ON `user_info`.user_id = users.id
                        JOIN    `group` ON `group`.id = users.group_id
                        JOIN    `users_point` ON  `users_point`.users_id = `users`.id
                        WHERE 	 operator_disorder.actived=1 AND operator_disorder.disorder_status_id = '$tab_id'
                        AND     `users`.id = '$user_id'");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;
    break;

    case "get_list_example_attendance":
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $tab_id      = $_REQUEST['tab_id'] != "undefined" ? $_REQUEST['tab_id'] : 1;
        $sql         = '';
        $year        = $_REQUEST['year'];
        $month       = $_REQUEST['month'];
        $day         = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $user        = $_SESSION['USERID'];



        
		$db->setQuery("SELECT  `attendance`.id AS log_id,
                                DATE( users_log.login),
                                user_info.`name`,
                               `group`.`name`,
                                SEC_TO_TIME(IFNULL(
                                                    TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.login ) ),
                                                    TIME_TO_SEC( TIMEDIFF (NOW(), users_log.login ) ))  )
                                                                                                            
                                                    AS `real_work_time`, -- ფაქტიური სამუშაო
                                                    
                                                    
                                                    
                                                    IF(work_shift.type = 1, SEC_TO_TIME(IF(TIMEDIFF(work_shift.end_date,work_shift.start_date) > '00:00:00', 

                                                    TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)  ,

                                                    TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC('24:00:00')
                                                                            ) - TIME_TO_SEC(`work_shift`.`timeout`) ), '00:00:00') AS `work_time`,  -- გეგმიური სამუშაო
        

        
                                if(SEC_TO_TIME(IFNULL(
                                                    TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.login ) ),
                                                    TIME_TO_SEC( TIMEDIFF (NOW(), users_log.login ) ))  ) <         -- ფაქტიური სამუშაო  < გეგმიური სამუშაო  => გეგმიური სამუშაო - ფაქტიური სამუშაო
                                                    
                                                    
                                                    
                                                    IF(work_shift.type = 1, SEC_TO_TIME(IF(TIMEDIFF(work_shift.end_date,work_shift.start_date) > '00:00:00', 

                                                    TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)  ,
                                                    
                                                    TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC('24:00:00')
                                 ) - TIME_TO_SEC(`work_shift`.`timeout`) ), '00:00:00'),
                                 TIMEDIFF(
                                        IF(work_shift.type = 1, SEC_TO_TIME(IF(TIMEDIFF(work_shift.end_date,work_shift.start_date) > '00:00:00', 
                                        
                                        TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)  ,
                                        
                                        TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC('24:00:00')
                                    ) - TIME_TO_SEC(`work_shift`.`timeout`) ), '00:00:00'),
                                    
                                        SEC_TO_TIME(IFNULL(
                                        TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.login ) ),
                                        TIME_TO_SEC( TIMEDIFF (NOW(), users_log.login ) ))  )), '' ),
                                if(
                                    SEC_TO_TIME(IFNULL(
                                                TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.login ) ),
                                                TIME_TO_SEC( TIMEDIFF (NOW(), users_log.login ) ))  ) >   -- ფაქტიური სამუშაო  > გეგმიური სამუშაო  =>  ფაქტიური სამუშაო - გეგმიური სამუშაო
                                                                        
                                                                        
                                                                        
                                                IF(work_shift.type = 1, SEC_TO_TIME(IF(TIMEDIFF(work_shift.end_date,work_shift.start_date) > '00:00:00', 

                                                TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)  ,
                                                
                                                TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC('24:00:00')
                                                ) - TIME_TO_SEC(`work_shift`.`timeout`) ), '00:00:00'),
                                                                    
                                                                    TIMEDIFF(                            
                                            SEC_TO_TIME(IFNULL(
                                                TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.login ) ),
                                                TIME_TO_SEC( TIMEDIFF (NOW(), users_log.login ) ))  ),
                                                
                                                IF(work_shift.type = 1, SEC_TO_TIME(IF(TIMEDIFF(work_shift.end_date,work_shift.start_date) > '00:00:00', 

                                                    TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)  ,

                                                    TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC('24:00:00')
                                                                            ) - TIME_TO_SEC(`work_shift`.`timeout`) ), '00:00:00')

        
                                            )
                                            , ''
        
                            ),
                            CONCAT('<span class=',IF(`disorder_type`.`id`=1,'status_gr','status_re'),'>',`disorder_type`.`name`,'</span>'),
                            `attendance`.comment,
                            `disorder_status`.`name`,
                            (SELECT COUNT(*) FROM `file` WHERE `user_log_id` = users_log.id)
        
                            FROM users
                            JOIN users_log ON users_log.user_id = users.id
                            JOIN user_info ON user_info.user_id = users.id
                            JOIN `group` ON `group`.id = users.group_id
                            JOIN work_graphic_cols ON work_graphic_cols.operator_id = users.id
                            JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
                            JOIN `attendance` ON `attendance`.user_log_id =  users_log.id
                            JOIN `disorder_type` ON `disorder_type`.id = `attendance`.`disorder_type_id`
                            LEFT JOIN `disorder_status` ON `disorder_status`.id = `attendance`.`disorder_status`
                            WHERE users.id = '$user' AND `attendance`.disorder_status = '$tab_id' AND users_log.login BETWEEN '$year-$month-01' AND '$year-$month-$day' AND work_graphic_cols.`col_id` = day(users_log.login) - 1 
                            
                            GROUP BY users.id, users_log.id");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;
    break;

    case 'get_columns':
        
        $columnCount   = $_REQUEST['count'];
        $cols[]        = $_REQUEST['cols'];
        $columnNames[] = $_REQUEST['names'];
        $operators[]   = $_REQUEST['operators'];
        $selectors[]   = $_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);
            
            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }
        
        
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
            {
                continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if($columns[$j] == 'id')
                {
                    $width = "5%";
                }
                else if($columns[$j] == 'position'){
                    $width = "12%";
                }
                else{
                    $width = 'auto';
                }
                if($columns[$j] == 'inc_id')
                {
                    $hidden = true;
                }
                else if($columns[$j] == 'docs_project_id')
                {
                    $hidden = true;
                }
                else if($columns[$j] == 'id')
                {
                    $hidden = true;
                }
                else{
                    $hidden = false;
                }
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>
                        [$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        
        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
        
        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for($j=0;$j<$columnCount;$j++)
        {
            if($types[$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else if($types[$j] == 'number'){
                
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        
        $filtArr = array('fields'=>$new_data);
        
        
        
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
        
        
        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
        
        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';
        
        break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);

function get_operator($id){

}

function get_page($res){
    return '<p>test</p>';
}

function get_tabs($selected){
    global $db;
    $data = "";
    $user = $_SESSION['USERID'];
    switch ($selected){
        case "t-1" :
            $query = "";
            break;
        case "t-2" :
            $query = "SELECT    '0' AS id,
                                'მთავარი' AS `name`,
                                '0' AS `count`

                      UNION ALL      
            
                      SELECT    monitoring_status_id AS `id`,
                                CASE
                                        WHEN monitoring_status_id = 1 THEN 'შემოწმებული'
                                        WHEN monitoring_status_id = 2 THEN 'გასაჩივრებული'
                                        WHEN monitoring_status_id = 3 THEN 'დასრულებული'
                                
                                END
                                AS `name`,
                                
                                SUM( IF( `monitoring`.`operator_id` = $user,1,0)) AS `count`
                    FROM  monitoring 
                    WHERE monitoring_main_id >= 0 AND monitoring_status_id > 0
                    GROUP BY monitoring_status_id";
            break;
        case "t-3" :
                $query = "SELECT    disorder_status.`id`, `name`,
                                    SUM( IF( `users_log`.`user_id` = $user,1,0)) AS `count`
                          FROM     `disorder_status` 
                          LEFT JOIN attendance ON attendance.disorder_status = disorder_status.id
                          LEFT JOIN users_log ON users_log.id = attendance.user_log_id 
                          WHERE    `disorder_status`.`actived` = 1 GROUP BY disorder_status.`id`";
            break;
        case "t-4" :
                $query = "SELECT    disorder_status.`id`, `name`,
                                    SUM( IF( `operator_disorder`.`operator_id` = $user,1,0)) AS `count`
                          FROM     `disorder_status` 
                          LEFT JOIN operator_disorder ON operator_disorder.disorder_status_id = disorder_status.id
                          WHERE    `disorder_status`.`actived` = 1 AND operator_disorder.actived = 1  GROUP BY disorder_status.`id` ";
            break;
        default :  $query = "SELECT    disorder_status.`id`, `name`,
                                       SUM( IF( `operator_disorder`.`operator_id` = $user,1,0)) AS `count`
                             FROM     `disorder_status` 
                             LEFT JOIN operator_disorder ON operator_disorder.disorder_status_id = disorder_status.id
                             WHERE    `disorder_status`.`actived` = 1 AND operator_disorder.actived = 1  GROUP BY disorder_status.`id` ";
    }
    if ($query != '') {
        $db->setQuery($query);
        $res = $db->getResultArray();
        foreach($res[result] AS $arr){
            if($arr['id'] == 1) $active = 'active';
            else $active = "";
            $data .= '<div class="tab '.$active.'" i_id = "'.$arr['id'].'" >'.$arr['name'].'<span>'.$arr['count'].'</span></div>';
        
        }
    }
    return $data;
}

?>