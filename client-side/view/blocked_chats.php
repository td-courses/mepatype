<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
.ui-widget-content {
	border: 0px solid #aaaaaa;
}
</style>
	<script type="text/javascript">
	    var aJaxURL	= "server-side/view/blocked_chats.action.php";		
		var tName	= "example";										
		var fName	= "add-edit-form";
		var tbName  = "tabs1";												
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		 	
		$(document).ready(function () {
			GetTabs(tbName);
			LoadTable('example');	
		});
        
		function LoadTable(tName){
			if(tName == 'example'){
				GetDataTable(tName, aJaxURL, "get_list_fb_blacklist", 5, "", 0, "", 1, "desc", "", change_colum_main);
				GetButtons("", "delete_button");
    			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
			}else if(tName == 'example1'){
				GetDataTable(tName, aJaxURL, "get_list_site_blacklist", 5, "", 0, "", 1, "desc", "", change_colum_main);
				GetButtons("", "delete_button1");
    			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
			}else if(tName == 'example2'){
				GetDataTable(tName, aJaxURL, "get_list_mail_blacklist", 5, "", 0, "", 1, "desc", "", change_colum_main);
				GetButtons("", "delete_button2");
    			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
			}
		}

		$(document).on("click", "#fb", function() {
			LoadTable('example');
        });
        
		$(document).on("click", "#site", function() {
			LoadTable('example1');
        });
        
		$(document).on("click", "#mail", function() {
			LoadTable('example2');
        });
        
        $(document).on("click", "#delete_button", function() {
	    	
	        var data = $(".check:checked").map(function () {
	            return this.value;
	        }).get();
	    	

	        for (var i = 0; i < data.length; i++) {
	            $.ajax({
	                url: aJaxURL,
	                type: "POST",
	                data: "act=disable&id=" + data[i] + "&source=1",
	                dataType: "json",
	                success: function (data) {
	                    if (data.error != ""){
	                        alert(data.error);
	                    }else{
							LoadTable('example');
	                    	$("#check-all").attr("checked", false);
	                    }
	                }
	            });
	        }
		});

		$(document).on("click", "#delete_button1", function() {
	    	
	        var data = $(".check:checked").map(function () {
	            return this.value;
	        }).get();
	    	

	        for (var i = 0; i < data.length; i++) {
	            $.ajax({
	                url: aJaxURL,
	                type: "POST",
	                data: "act=disable&id=" + data[i] + "&source=2",
	                dataType: "json",
	                success: function (data) {
		                    if (data.error != "") {
		                        alert(data.error);
		                    } else {

		                    	LoadTable('example1');
		                    	$("#check-all1").attr("checked", false);
		                    }
	                }
	            });
	        }
		});

		$(document).on("click", "#delete_button2", function() {
	    	
	        var data = $(".check:checked").map(function () {
	            return this.value;
	        }).get();
	    	

	        for (var i = 0; i < data.length; i++) {
	            $.ajax({
	                url: aJaxURL,
	                type: "POST",
	                data: "act=disable&id=" + data[i] + "&source=3",
	                dataType: "json",
	                success: function (data) {
		                    if (data.error != "") {
		                        alert(data.error);
		                    } else {

		                    	LoadTable('example2');
		                    	$("#check-all2").attr("checked", false);
		                    }
	                }
	            });
	        }
		});
		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
		
	</script>
</head>

<body>
    <div id="tabs">
		<div class="callapp_head">დაბლოკილი ჩატები<hr class="callapp_head_hr"></div>
		<table id="table_right_menu" style="top: 89px; left: -27px;">
            <tr>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
            	</td>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
            		<img alt="log" src="media/images/icons/log.png" height="14" width="14">
            	</td>
            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
        			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
        		</td>
        	</tr>
        </table>
		<div id="tabs1" style="width: 99%;">
        	<ul>
        		<li id="fb"><a href="#tab-0">მესენჯერი</a></li>
        		<li id="site"><a href="#tab-1">საიტი</a></li>
        		<li id="mail"><a href="#tab-2">მეილი</a></li>
        	</ul>
        	<div id="tab-0">
            	<div id="button_area">
        			<button id="delete_button">წაშლა</button>
        		</div>
                <table class="display" id="example">
                    <thead >
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 15%;">თარიღი</th>
                            <th style="width: 15%;">ოპერატორი</th>
                            <th style="width: 15%;">მომხმარებელი</th>
                            <th style="width: 15%;">User Id</th>
        					<th style="width: 55%;">მიზეზი</th>
                        	<th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                             	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
        					<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                          	<th>
                            	<div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all" name="check-all" />
                                    <label for="check-all"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div id="tab-1">
            	<div id="button_area">
        			<button id="delete_button1">წაშლა</button>
        		</div>
                <table class="display" id="example1">
                    <thead >
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 15%;">თარიღი</th>
                            <th style="width: 15%;">ოპერატორი</th>
                            <th style="width: 15%;">მომხმარებელი</th>
        					<th style="width: 55%;">მიზეზი</th>
                        	<th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                             	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
        					<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                          	<th>
                            	<div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all" name="check-all" />
                                    <label for="check-all"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div id="tab-2">
            	<div id="button_area">
        			<button id="delete_button2">წაშლა</button>
        		</div>
                <table class="display" id="example2">
                    <thead >
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 15%;">თარიღი</th>
                            <th style="width: 15%;">ოპერატორი</th>
                            <th style="width: 15%;">მომხმარებელი</th>
        					<th style="width: 55%;">მიზეზი</th>
                        	<th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                             	<input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
        					<th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                          	<th>
                            	<div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all" name="check-all" />
                                    <label for="check-all"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
   </div>
       
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="IP მისამართი">
    	<!-- aJax -->
	</div>
</body>
</html>