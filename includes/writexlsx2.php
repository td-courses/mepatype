<?php
ini_set('memory_limit', '-1');
set_time_limit(0);
require_once('excel/PHPExcel.php');
require_once('classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$objPHPExcel = new PHPExcel();

$data      = '';
$action    = $_REQUEST['act'];
$file_name = '';

if ($action=='create_excel_code') {

    $file_name = 'Campaign_Code_Template.xlsx';

    $b = 1;
    
    $objPHPExcel->getActiveSheet()->setCellValue("A".$b,'Mobile');
    $objPHPExcel->getActiveSheet()->setCellValue("B".$b,'CustomerID');
    $objPHPExcel->getActiveSheet()->setCellValue("C".$b,'CampaignCode');
    $objPHPExcel->getActiveSheet()->setCellValue("D".$b,'PinCode');
    $objPHPExcel->getActiveSheet()->setCellValue("E".$b,'UserName');
    $objPHPExcel->getActiveSheet()->setCellValue("F".$b,'FullName');
    $objPHPExcel->getActiveSheet()->setCellValue("G".$b,'PID');
    $objPHPExcel->getActiveSheet()->setCellValue("H".$b,'SameMobile');
    $objPHPExcel->getActiveSheet()->setCellValue("I".$b,'Language');
    $objPHPExcel->getActiveSheet()->setCellValue("J".$b,'PromeDate');

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="nova.xlsx"');
    header('Cache-Control: max-age=0');
    
}else if ($action=='create_excel') {

    $row_excel = array();
    $column    = array();

    $file_name  = 'outgoing_voice.xlsx';
    $request_id = $_REQUEST['request_id'];
    
    if (empty($_SESSION['last_id'])) {
        $db->setQuery(" SELECT name
                        FROM   autocall_request_voice
                        WHERE  request_id = '$request_id'
                        AND    actived = '1'
                        AND    voice_type_id <> '1'");
        
    }else{
        $db->setQuery(" SELECT name
                        FROM   autocall_request_voice
                        WHERE  request_id = (SELECT MAX(request_id) FROM autocall_request_voice)
                        AND    actived = '1'
                        AND    voice_type_id <> '1'");
    }
    
    $req = $db->getResultArray();
    
    $column[0]    = 'A';
    $column[1]    = 'B';
    $column[2]    = 'C';
    $column[3]    = 'D';
    $column[4]    = 'E';
    $column[5]    = 'F';
    $column[6]    = 'G';
    $column[7]    = 'H';
    $column[8]    = 'I';
    $column[9]    = 'J';
    $column[10]   = 'K';
    $column[11]   = 'L';
    $row_excel[0] = "ნომერი";
    
    $count = 1;
    foreach ($req[result] AS $res) {
        $row_excel[$count] = $res['name'];
        $count++;
    }
    
    $b = 1;
    $c = 0;
    
    
    foreach ($row_excel as $rx){
    
        $objPHPExcel->getActiveSheet()->setCellValue($column[$c].$b,$rx);
    
        $c++;
    }
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="DY_Report.xlsx"');
    header('Cache-Control: max-age=0');
    
}else{
    
}

$writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$writerObject->save($file_name);

$data['error'] = 'edede';

echo json_encode($data);
?>
