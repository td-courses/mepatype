<html>

<head>
    <link rel="stylesheet" href="vendors/apex-charts/apexcharts.css">
    <script src="vendors/apex-charts/apex.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script>
        kendo.pdf.defineFont({
            "DejaVu Sans": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
            "DejaVu Sans|Bold": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
            "DejaVu Sans|Bold|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "DejaVu Sans|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "WebComponentsIcons": "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
        });
    </script>

    <script>
        var aJaxURL = "server-side/call/nomrebi.action.php";
        var Comunications_URL = "server-side/call/incomming.action.php";
        var CustomSaveURL = "server-side/view/automator.action.php";

        
        $(document).on('click', '#outgoing_excel_export', function() {
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("base_grid", "გამავალი", [0, 8]);
                $("#loading1").hide();
            }, 200);
        });
        function LoadKendoContacts(hidden){
			var gridName = 				    'kendo_contacts';
			var actions = 				    '<button class="outgoing_button add_contact" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button delete_contact" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    5;
			var columnsSQL = 				["id:string","name:string","pos:string","phone:string","email:string"];
			var columnGeoNames = 		    ["ID","სახელი/გვარი","კომუნიკაციის ტიპი","ტელეფონი","E-mail"];
			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0
			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_contacts_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
        }
        $(document).on("dblclick", "#kendo_contacts tr.k-state-selected", function () {
            var grid = $("#kendo_contacts").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=add_contact&id="+dItem.id,
                dataType: "json",
                success: function (data) {
                    $('#add_contacts').html(data.page);
            }
        });
        });     
        $(document).ready(function() {
            var kendo = new kendoUI();
            GetDate('start_date');
            GetDate('end_date');
            kendo.kendoSelector('operators', 'server-side/call/incomming.action.php', 'get_operators', "ყველა ოპერატორი");
            kendo.kendoSelector('compaign_types', 'server-side/call/outgoing.action.php', 'get_compaign_types', "კამპ.ტიპი");
            var hidden = getFilterResult();
            var hidden1=getFilterResult2();
            LoadKendoTable(hidden);
            LoadKendoTable(hidden1);
            loadtabs(activep = "", activech = "");
            get_tabs_count();

        });
        function getFilterResult(pos) {
            var operators = $("#operators").data("kendoDropDownList");
            var selectedIndex = operators.select();
            var operators_dItem = operators.dataItem(selectedIndex);
            var all_calls = $("#compaign_types").data("kendoDropDownList");
            var selectedIndex = all_calls.select();
            var all_calls_dItem = all_calls.dataItem(selectedIndex);
            if (operators_dItem == undefined) {
                operators_dItem = new Object();
                operators_dItem.id = '';
            }
            if (pos == undefined) {
                pos = '';
            }
            if (all_calls_dItem == undefined) {
                all_calls_dItem = new Object();
                all_calls_dItem.id = '';
            }
            var hidden = "&start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + "&filter_operator=" + operators_dItem.id + "&filter_all_calls=" + all_calls_dItem.id + pos;
            return hidden;
        }
        function getFilterResult2(pos) {
            if (pos == undefined) {
                pos = '';
            }
            var hidden1 = "&start=" + $('#start_date_1').val() + "&end=" + $('#end_date_1').val()+ "&phone="+$("#hiddenphone").text()+ pos;
            return hidden1;
        }
        $(document).on("click", "#loadtable", function() {
            var hidden = getFilterResult();

            LoadKendoTable(hidden)
        });
        $(document).on("click", "#loadtable_2", function() {
            var hidden1 = getFilterResult2();

            LoadKendoTable1(hidden1)
        });
        $(document).on("click", "#contracts_excel", function(){
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_contract_list","კონტრაქტების სია",[]);
                $("#loading1").hide();
            }, 200)
        });
        $(document).on("dblclick", "#base_grid tr.k-state-selected", function() {
            
            var grid = $("#base_grid").data("kendoGrid");
            var dItem = grid.dataItem($(this));
            console.log(dItem);
            
            if(dItem.id != "") {
                $.ajax({
                    url: "server-side/call/nomrebi.action.php",
                    type: "POST",
                    data: { act: 'get_edit_page', 
                            phone: dItem.phone,
                            project: dItem.project,
                            id: dItem.id,
                            date:dItem.call_date} ,
                    dataType: "json",
                    success: function(data) {
                        $("#get_edit_page-dialog").html(data.page);

                        var buttons = {

                            done: {
                                text: "შენახვა",
                                class: "save-processing",
                                id: 'save-dialog-out-automated'
                            },
                            cancel: {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function() {
                                    $(this).dialog("close");
                                }
                            }
                        };

                        GetDialog("get_edit_page-dialog", "625", "auto", buttons, 'ceneter center');
                        GetDate('start_date_1');
                        GetDate('end_date_1');
                        var hidden1 = getFilterResult2();
                        LoadKendoTable1(hidden1+'&phone='+dItem.phone+'&startdate='+$('#start_date_1').val()+'&enddate='+$('#end_date_1').val());
                        

                    } 
                });
            }
        });       
        function LoadKendoTable(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'base_grid';
            var actions = '<button style="float:right;" id="outgoing_excel_export">ექსპორტი EXCEL</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 5;
            var columnsSQL = ["id:string", "call_date:date", "operator:string", "project:string", "phone:string"];
            var columnGeoNames = ["ID", "დარეკვის თარიღი", "უწყება", "კომუნიკაციის ტიპი", "ტელეფონი",];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0, '', '', '', true, false, true, [2, 6, 10]);
        }        
        function LoadKendoTable1(hidden1) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'sub_kendo';
            var actions = '';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 4;
            var columnsSQL = ["id:string", "call_date:date", "phone:string"];
            var columnGeoNames = ["ID", "დარეკვის თარიღი","ნომერი",];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list_2', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden1,0,0, '','', '', '', '', true, true, true, [2, 6, 10]);
        }
    </script>
    <style>
        #loading1 {
        z-index: 999;
        top: 45%;
        left: 45%;
        position: absolute;
        display: none;
        padding-top: 15px;
        }

        .container {
        display: flex;
        align-items: center;
        justify-content: space-between;
        }
        th [role="listbox"] {
            display: none;
        }

        .ui-tabs .ui-tabs-panel {
            display: block !important;
            border-width: 0;
            padding: 0.2em 0em;
            background: none;

        }

        .ui.tabs li[aria-selected="true"] input {
            background-color: green !important
        }


        .l_inline-tabs div {
            margin: 2px 7px;
        }

        #tabs1 div {
            border: none;
        }        
    </style>
</head>

<body>
    <div id="tabs" style=" border: 0px solid #aaaaaa;">
        <div class="callapp_filter_body" myvar="1">
            <div style="width: 100%;">
                <table style="width: 50%;">
                    <tr style="float: left;">
                        <td style="padding: 5px 0 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                        </td>
                        <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">-</td>
                        <td style="font-family: BPG; padding: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="font-family: BPG; width: 84px;">
                        </td>
                        <td>
                            <div class="k-content" style="font-family: BPG; margin-right: 10px;  width: 154px;">
                                <input id="operators" style="font-family: BPG !important; width: 100%;" />
                            </div>
                        </td>
                        <td>
                            <div class="k-content" style="font-family: BPG;margin-right: 10px; width: 154px; ">
                                <input id="compaign_types" style="font-family: BPG !important; width: 100%; " />
                            </div>
                        </td>
                        <td>
                            <button id="loadtable">ფილტრი</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3>
                    </tr></table></div>
        </div>
        <div class="l_responsible_fix" style="width: calc(100% - 250px)">
            <div id="base_grid"></div>
        </div>

        <!-- jQuery Dialog -->
        <div id="add-edit-form" class="form-dialog" title="კამპანია"></div>
        <div id="get_edit_page-dialog" class="form-dialog" title="ნომრები"></div>
        <div id="contract_dialog" class="form-dialog" title="კონტრაქტების სია"></div>
        <div id="campaign-select-form" class="form-dialog" title="კამპანიის არჩევა"></div>
    </div>
    <div id="loading1">
        <p><img src="media/images/loader.gif" /></p>
    </div>
</body>
</html>