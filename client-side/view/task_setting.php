<html>
<head>
    <script type="text/javascript">
        var aJaxURL = "server-side/view/task_setting.action.php";		//server side folder url


        $(document).ready(function () {
            loadContent();
            $("#saveSetting").button();
        });

        function loadContent() {
            $.ajax({
                url: aJaxURL,
                data: "act=get_setting",
                success: function (data) {
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            $('#time').val(data.time);
                            $('#freq').val(data.freq);

                            $("#call_vs").val(data.call_vs);
                            $("#web_call_vs").val(data.web_call_vs);
                            $("#chat_vs").val(data.chat_vs);
                            $("#chat_cb_vs").val(data.chat_cb_vs);
                            $("#chat_fb_vs").val(data.chat_fb_vs);
                            $("#viber_vs").val(data.viber_vs);
                            $("#chat_mail_vs").val(data.chat_mail_vs);
                            $("#video_call_vs").val(data.video_call_vs);
                            $("#avg_vs").val(data.avg_vs);

                            $("#call_sl").val(data.call_sl);
                            $("#web_call_sl").val(data.web_call_sl);
                            $("#chat_sl").val(data.chat_sl);
                            $("#chat_cb_sl").val(data.chat_cb_sl);
                            $("#chat_fb_sl").val(data.chat_fb_sl);
                            $("#viber_sl").val(data.viber_sl);
                            $("#chat_mail_sl").val(data.chat_mail_sl);
                            $("#video_call_sl").val(data.video_call_sl);

                            $("#call_sl_percent").val(data.call_sl_percent);
                            $("#web_call_sl_percent").val(data.web_call_sl_percent);
                            $("#chat_sl_percent").val(data.chat_sl_percent);
                            $("#chat_cb_sl_percent").val(data.chat_cb_sl_percent);
                            $("#chat_fb_sl_percent").val(data.chat_fb_sl_percent);
                            $("#viber_sl_percent").val(data.viber_sl_percent);
                            $("#chat_mail_sl_percent").val(data.chat_mail_sl_percent);
                            $("#video_call_sl_percent").val(data.video_call_sl_percent);

                            $("#call_asa").val(data.call_asa);
                            $("#web_call_asa").val(data.web_call_asa);
                            $("#chat_asa").val(data.chat_asa);
                            $("#chat_cb_asa").val(data.chat_cb_asa);
                            $("#chat_fb_asa").val(data.chat_fb_asa);
                            $("#viber_asa").val(data.viber_asa);
                            $("#chat_mail_asa").val(data.chat_mail_asa);
                            $("#video_call_asa").val(data.video_call_asa);

                        }
                    }
                }
            });
        }

        function saveSetting() {
            time = $('#time').val();
            freq = $('#freq').val();

            $.ajax({
                url: aJaxURL,
                data: "act=save_setting&time=" + time + "&freq=" + freq,
                success: function (data) {
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            loadContent();
                            alert('მონაცემები განახლებულია!');
                        }
                    }
                }
            });
        }
        function savevs() {

            param = new Object();

            param.act = "savevs";

            param.call_vs = $("#call_vs").val();
            param.web_call_vs = $("#web_call_vs").val();
            param.chat_vs = $("#chat_vs").val();
            param.chat_cb_vs = $("#chat_cb_vs").val();
            param.chat_fb_vs = $("#chat_fb_vs").val();
            param.viber_vs = $("#viber_vs").val();
            param.chat_mail_vs = $("#chat_mail_vs").val();
            param.video_call_vs = $("#video_call_vs").val();
            param.avg_vs = $("#avg_vs").val();


            param.call_sl = $("#call_sl").val();
            param.web_call_sl = $("#web_call_sl").val();
            param.chat_sl = $("#chat_sl").val();
            param.chat_cb_sl = $("#chat_cb_sl").val();
            param.chat_fb_sl = $("#chat_fb_sl").val();
            param.viber_sl = $("#viber_sl").val();
            param.chat_mail_sl = $("#chat_mail_sl").val();
            param.video_call_sl = $("#video_call_sl").val();
            param.call_sl_percent = $("#call_sl_percent").val();
            param.web_call_sl_percent = $("#web_call_sl_percent").val();
            param.chat_sl_percent = $("#chat_sl_percent").val();
            param.chat_cb_sl_percent = $("#chat_cb_sl_percent").val();
            param.chat_fb_sl_percent = $("#chat_fb_sl_percent").val();
            param.viber_sl_percent = $("#viber_sl_percent").val();
            param.chat_mail_sl_percent = $("#chat_mail_sl_percent").val();
            param.video_call_sl_percent = $("#video_call_sl_percent").val();


            param.call_asa = $("#call_asa").val();
            param.web_call_asa = $("#web_call_asa").val();
            param.chat_asa = $("#chat_asa").val();
            param.chat_cb_asa = $("#chat_cb_asa").val();
            param.chat_fb_asa = $("#chat_fb_asa").val();
            param.viber_asa = $("#viber_asa").val();
            param.chat_mail_asa = $("#chat_mail_asa").val();
            param.video_call_asa = $("#video_call_asa").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    if (typeof (data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {

                            alert('მონაცემები განახლებულია!');
                        }
                    }
                }
            });
        }
    </script>
</head>
<body>

<div id="tabs" style="width: 99%;">
	
    <div class="callapp_head">პარამეტრები
        <hr class="callapp_head_hr">
    </div>
    <div id="dialog-form">
        <fieldset style="display:none;">
            <legend>ძირითადი პარამეტრები</legend>
            <table class="dialog-form-table">
                <tr>
                    <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">დავალებების განახლების
                            დრო(წუთი)</label></td>
                    <td>
                        <input style="width: 50px;" type="number" min="0" max="99" id="time" class="idle" value="0"
                               title="მიუთითეთ წუთი!"/>
                    </td>
                </tr>
                <tr>
                    <td style="width: 250px;"><label for="freq" title="მიუთითეთ 1000-დან 10000-მდე!">აუდიო ჩანაწერის
                            მაქსიმალური სიხშირე</label></td>
                    <td>
                        <input style="width: 50px;" type="number" min="1000" max="10000" id="freq" class="idle"
                               value="2000" title="მიუთითეთ 1000-დან 10000-მდე!"/>
                    </td>
                </tr>
            </table>
            <button id="saveSetting" onclick="saveSetting()">განახლება</button>
        </fieldset>
    </div>


    <div id="dialog-form" style="width: 100%">
        <fieldset>
            <legend>ნაპასუხები vs უპასუხო</legend>
            <table>
            <tr  style="width: 100%">
            	<td>
                    <table class="dialog-form-table" style="float: left; margin-left: 25px;">
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ზარები</label></td>
                            <td>
                                <input style="width: 50px;" type="number" min="0" max="99" id="call_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვებ-ქოლი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="web_call_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ჩატი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr style="display:none;">
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">მესენჯერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_cb_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">FB მესენჯერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_fb_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვაიბერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="viber_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ელ-ფოსტა</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_mail_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr style="display:none;">
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვიდეო-ზარი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="video_call_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">საშუალო</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="avg_vs" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
        
                        </tr>
                    </table>
                </td>
			</tr>
			<tr  style="width: 100%">
				<td>
                	<legend>მომსახურების დონე</legend>
                
                    <table class="dialog-form-table" style="float: left; margin-left: 25px;">
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ზარები</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="call_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="call_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვებ-ქოლი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="web_call_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="web_call_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ჩატი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr style="display:none;">
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">მესენჯერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_cb_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_cb_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">FB მესენჯერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_fb_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_fb_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვაიბერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="viber_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="viber_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ელ-ფოსტა</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_mail_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_mail_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                        <tr style="display:none;">
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვიდეო-ზარი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="video_call_sl_percent" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">%</td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="video_call_sl" class="idle" value="0" />
                            </td>
        
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
                        </tr>
                    </table>
                </td>
			</tr>
			<tr  style="width: 100%">
				<td>
                	<legend>პასუხის საშუალო სიჩქარე</legend>
                
                    <table class="dialog-form-table" style="float: left; margin-left: 25px;">
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ზარები</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="call_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვებ-ქოლი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="web_call_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ჩატი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr style="display:none;">
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">მესენჯერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_cb_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">FB მესენჯერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_fb_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვაიბერი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="viber_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr>
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ელ-ფოსტა</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="chat_mail_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                        <tr style="display:none;">
                            <td style="width: 250px;"><label for="time" title="მიუთითეთ წუთი!">ვიდეო-ზარი</label></td>
                            <td>
                                <input style="width: 55px;" type="number" min="0" max="99" id="video_call_asa" class="idle" value="0" />
                            </td>
                            <td style="font-size: 30px; padding-left: 10px; padding-right: 10px">წმ</td>
        
                        </tr>
                    </table>
                </td>
			</tr>
			</table>
            <button id="savevs" onclick="savevs()">განახლება</button>
        </fieldset>



    </div>

</body>
</html>