<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
//error_reporting(E_ALL);
$action     = $_REQUEST['act'];
$error	    = '';
$filterID   = '';
$data	    = array();
$user_id    = $_SESSION['USERID'];

switch ($action) {
    case 'get_input_tabs' :
        $field_id   = $_REQUEST['field_id'];
        $page		= get_input_tabs('0',$field_id);
	    $data		= array('page'	=> $page);
    break;
    case 'get_municip':
        global $db;
        $id = $_REQUEST['id'];
        $db->setQuery(" SELECT  `id`,
                                `name`,
                                `parent_id`
                        FROM    regions
                        WHERE    actived = 1   AND `parent_id` IN(SELECT id FROM regions WHERE parent_id = 0)");
        
        $req = $db->getResultArray();
        
        $data .= '<option value="0" selected="selected">----</option>';
        $i = 0;
        foreach($req["result"] AS $res){
            if($res['id'] == $id){
                $data .= '<option value="' . $res['id'] . '" selected="selected" data-parent="'.$res["parent_id"].'">' . $res['name'] . '</option>';
            } else {
                $data .= '<option value="' . $res['id'] . '" data-parent="'.$res["parent_id"].'">' . $res['name'] . '</option>';
            }
            $i = 1;
        }
        if($i == 0 && $id > 0){
            $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
        }
	    $data		= array('page'	=> $data);
        break;
    case 'get_additional':
        $project_id = $_REQUEST['project_id'];
        $board      = $_REQUEST['info_board'];
        $inc_id     = $_REQUEST['inc_id'];
        $page       = get_additional_view($project_id,$board,$inc_id);
        $data		= array('page'	=> $page);
    break;
    case 'add_setting' :
        $page_id = $_REQUEST['page_id'];
        $page		= GetPage(0,$page_id);
		$data		= array('page'	=> $page);
    break;
    case 'get_position_increment_by_tab' :
        $tab_id = $_REQUEST['tab_id'];
        $field_id = $_REQUEST['field_id'];

        $db->setQuery(" SELECT 	IFNULL(MAX(position)+1,1) AS 'position'
                        FROM 	processing_setting_detail
                        WHERE	actived = 1 AND processing_fieldset_id = '$field_id' AND tab_id = '$tab_id'");
        $input_pos = $db->getResultArray();

        $input_pos = $input_pos['result'][0]['position'];

        $data['position'] = $input_pos;

    break;
    case 'region_2':
		$cat_id = $_REQUEST['cat_id'];
	    $page		= get_regions_1_1($cat_id,'');
	    $data		= array('page'	=> $page);
	    
        break;
    case 'get_region':
        $region_id  = $_REQUEST['region_id'];
        $page		= get_regions($region_id);
        $data		= array('page'	=> $page);
        
        break;


    case 'get_municipipalitet':
        $id         = $_REQUEST['municipipalitet_id'];
        $page		= get_municipipalitets($id);
        $data		= array('page'	=> $page);
        break;


    case 'region_4':
        $cat_id = $_REQUEST['cat_id'];
        $page		= get_regions_1_1($cat_id,'');
        $data		= array('page'	=> $page);
        
        break;

    case 'get_demo_view' :
        $field_id   = $_REQUEST['fieldset_id'];
        $page       = get_demo_view($field_id);
        $data		= array('page'	=> $page);
    break;

    case 'get_fieldsets':
		$field_id = $_REQUEST['fieldset_id'];
		$chosen_keys = '';

        $db->setQuery("	SELECT 	id,`key`,depth,processing_field_type_id AS 'type'

                        FROM 	processing_setting_detail
                        WHERE 	processing_fieldset_id = '$field_id' AND actived = '1' AND processing_field_type_id IN (8,10)
                        ORDER BY position");
        $selectors = $db->getResultArray();
        foreach($selectors['result'] AS $item){
            if($item['type'] == 10){
                $depth = $item['depth'];
                for($i=1;$i<=$depth;$i++){
                    $chosen_keys .= '#'.$item['key'].'_'.$i.'--'.$item['id'].'--'.$item['type'].',';

                    $multilevel_keys[] = $item['key'].'_'.$i.'--'.$item['id'].'--'.$item['type'];
                }
            }
            else{
                $chosen_keys .= '#'.$item['key'].'--'.$item['id'].'--'.$item['type'].',';
            }
            
        }
		

		$chosen_keys = substr($chosen_keys, 0, -1); 

		$data['chosen_keys'] = $chosen_keys;
		
			
	break;
    case 'get_multilevel_list' :

        $input_id = $_REQUEST['input_id'];
        $field_id = $_REQUEST['field_id'];

        if($input_id != '')
        {
            $db->setQuery(" SELECT  multilevel_table
                            FROM    processing_setting_detail
                            WHERE   id = '$input_id'");
            $multilevel_table = $db->getResultArray();
            $multilevel_table = $multilevel_table['result'][0]['multilevel_table'];
            $data['page'] = '';
            $db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM $multilevel_table WHERE actived = 1 AND parent_id = 0");
            
            $result = $db->getResultArray();
            foreach ($result[result] AS $cat_res){
                $data['page'] .= '<li>'.$cat_res[name];
                $db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM $multilevel_table WHERE actived = 1 AND parent_id = '$cat_res[id]'");
                $result_sub = $db->getResultArray();
                $ul_close = 0;
                $i=0;
                foreach ($result_sub[result] AS $cat1_res){
                    $i++;
                    if($i==1){
                        $data['page'] .= '<ul>';
                    }
                    $data['page'] .= '<li>'.$cat1_res[name];
                    $db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM $multilevel_table WHERE actived = 1 AND parent_id = '$cat1_res[id]'");
                    $result_sub_sub = $db->getResultArray();
                    $ul_close1 = 0;
                    $i1=0;
                    foreach ($result_sub_sub[result] AS $cat2_res){
                        $i1++;
                        if($i1==1){
                            $data['page'] .= '<ul>';
                        }
                        $data['page'] .= '<li>'.$cat2_res[name].'</li>';
                        $ul_close1 = 1;
                    }
                    if($ul_close1==1){
                        $data['page'] .= '</ul>';
                    }
                    $data['page'] .= '</li>';
                    $ul_close = 1;
                }
                if($ul_close==1){
                    $data['page'] .= '</ul>';
                }
                $data['page'] .= '</li>';
            }

        }
    break;
    case 'save_category':
        $cat_id 		      = $_REQUEST['id'];
        $category_value	      = $_REQUEST['category_value'];
        $category_select	  = $_REQUEST['category_select'];
        $sub_category_1_value = $_REQUEST['sub_category_1_value'];
        $sub_category_1_select= $_REQUEST['sub_category_1_select'];
        $sub_category_2_value = $_REQUEST['sub_category_2_value'];

        $input_id             = $_REQUEST['input_id'];
        $field_id             = $_REQUEST['field_id'];

        if($input_id == ''){
            $ID = $db->getIncrement('processing_setting_detail');
            $new_tableName = 'dir2_multilevel_NEWINPUTID_'.$ID;
            $db->setQuery("CREATE TABLE $new_tableName (
                            id INT(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
                            user_id INT(4),
                            parent_id INT(4),
                            client_id INT(4) DEFAULT 1,
                            name VARCHAR(70),
                            finally VARCHAR(10) DEFAULT 'No',
                            actived INT(2) DEFAULT 1
                            )");
            $db->execQuery();

            $db->setQuery("INSERT INTO processing_setting_detail(`user_id`,`datetime`,`name`,`processing_fieldset_id`,`processing_field_type_id`,`multilevel_table`) VALUES('$user_id',NOW(),'Temporary_multilevel_name','$field_id','10','$new_tableName')");
            $db->execQuery();

            if($cat_id == ''){
                AddCategory($category_value, $category_select, $sub_category_1_value,  $sub_category_1_select, $sub_category_2_value,$new_tableName);
            }else{
                SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value,$new_tableName);
            }
            
            $data['new_input_id'] = $ID;
        }
        else{
            $new_tableName = 'dir2_multilevel_NEWINPUTID_'.$input_id;
            $db->setQuery(" SELECT  multilevel_table
                            FROM    processing_setting_detail
                            WHERE   id = '$input_id'");
            $multilevel_table = $db->getResultArray();
            $multilevel_table = $multilevel_table['result'][0]['multilevel_table'];

            if($multilevel_table == ''){
                $db->setQuery("CREATE TABLE $new_tableName (
                    id INT(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
                    user_id INT(4),
                    parent_id INT(4),
                    client_id INT(4) DEFAULT 1,
                    name VARCHAR(70),
                    finally VARCHAR(10) DEFAULT 'No',
                    actived INT(2) DEFAULT 1
                    )");
                $db->execQuery();

                $db->setQuery("UPDATE processing_setting_detail SET multilevel_table='$new_tableName' WHERE id='$input_id'");
                $db->execQuery();

                $db->setQuery(" SELECT  multilevel_table
                                FROM    processing_setting_detail
                                WHERE   id = '$input_id'");
                $multilevel_table = $db->getResultArray();
                $multilevel_table = $multilevel_table['result'][0]['multilevel_table'];
            }

            if($cat_id == ''){
                AddCategory($category_value, $category_select, $sub_category_1_value,  $sub_category_1_select, $sub_category_2_value, $multilevel_table);
            }else{
                SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value, $multilevel_table);
            }
        }

        
    break;
    case 'get_edit_page':
        $id         = $_REQUEST['id'];
        $page_id = $_REQUEST['page_id'];
		$page		= GetPage($id,$page_id);
		$data		= array('page'	=> $page);

    break;
    case 'get_docs_project':
        $project_id = $_REQUEST['project_id'];
        $board      = $_REQUEST['info_board'];
        $inc_id     = $_REQUEST['inc_id'];
        $page       = get_pined_docs($project_id,$board,$inc_id);
        $data		= array('page'	=> $page);

    break;
    case 'add_tab':
        $field_id   = $_REQUEST['field_id'];

		$page		= GetAddTabPage($id);
		$data		= array('page'	=> $page);

    break;
    case 'save-custom-quize':
        $processing_id = $_REQUEST['baseID'];
        $tableToInsert = 'compaign_quize_processing';
        $ff = 1;
        $dd = 1;
        foreach($_REQUEST AS $key => $value){
            if($dd == 5){
                $dd = 1;
            }
            $forcheck = explode('_',$key);
            $forcheck_cc = count($forcheck) - 1;
            if($key == 'baseID' OR $key == 'act'){
                continue;
            }
            else{
                $detail_id = explode('--',$key);
                $text_value = '';
                $int_value = '';
                if($detail_id[2] == 1 OR $detail_id[2] == 2 OR $detail_id[2] == 3 OR $detail_id[2] == 4 OR $detail_id[2] == 5){
                    $text_value = $value;
                }
                else if($detail_id[2] == 6 OR $detail_id[2] == 7 OR $detail_id[2] == 8 OR $detail_id[2] == 9){
                    $int_value = $value;
                }
                else if($detail_id[2] == 10){
                    $selector_id = explode('_',$detail_id[0]);
                    $text_value = $selector_id[1];
                    $int_value = $value;
                }

                $values .= "('$user_id',NOW(),'$processing_id','$detail_id[1]','$text_value','$int_value',''),";
            }
        }
        //die('phone: '.$phone.' name: '.$firstname.' lastname: '.$lastname);
        $values = substr($values, 0, -1);
        $db->setQuery("DELETE FROM $tableToInsert WHERE base_id='$processing_id' AND additional_dialog_id IS NULL");
        $db->execQuery();
        $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`base_id`,`processing_setting_detail_id`,`value`,`int_value`,`additional_deps`) VALUES $values";

        $db->setQuery($insert_query);
        $db->execQuery();
    break;
    case 'save-custom':
        $setting_id     = $_REQUEST['setting_id'];
        $processing_id  = $_REQUEST['processing_id'];
        $dialogType     = $_REQUEST['dialogtype'];
        $user_id = $_SESSION['USERID'];
        /* if($processing_id == '' or $processing_id == 'undefined'){
            $phone = $_REQUEST['telefoni___183--124--1'];

            $phone = str_replace("-","",$phone);

            $date = date('Y-m-d H:i:s');
            
            $db->setQuery(" SELECT id
                            FROM asterisk_call_log
                            WHERE call_type_id='2' AND destination='$phone' AND (FROM_UNIXTIME(call_datetime) >= '$date' or FROM_UNIXTIME(call_datetime) <= '$date') ORDER BY id DESC LIMIT 1");

            $call_log_id = $db->getResultArray();

            $call_log_id = $call_log_id['result'][0]['id'];

            $db-> setQuery("  SELECT  id
                            FROM    incomming_call
                            WHERE   asterisk_incomming_id='$call_log_id'");
            $check = $db->getResultArray();

            if($check['count'] == 0){
                $db->setQuery("INSERT INTO incomming_call(`date`,`asterisk_incomming_id`,`source_id`) VALUES(NOW(),'$call_log_id','1')");
                $db->execQuery();
                $processing_id = $db->getLastId();
            }
            else{
                die('callapp');
            } 
        } */
        if($dialogType != 'autodialer' AND $dialogType != 'outgoing'){
            $db->setQuery(" SELECT  `data-table`
                        FROM    processing_setting
                        WHERE   id = '$setting_id'");
            $tableToInsert = $db->getResultArray();
            $tableToInsert = $tableToInsert['result'][0]['data-table'];
            $ff = 1;
            $dd = 1;
            foreach($_REQUEST AS $key => $value){
                $value = mres($value);
                if($dd == 5){
                    $dd = 1;
                }
                $forcheck = explode('_',$key);
                $forcheck_cc = count($forcheck) - 1;
                if($key == 'setting_id' OR $key == 'act' OR $key == 'processing_id'){
                    continue;
                }
                else if($key == 'region'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','1111','$value'),";
                }
                else if($key == 'municip'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','2222','$value'),";
                }
                else if($key == 'temi'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','3333','$value'),";
                }
                else if($key == 'village'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','4444','$value'),";
                }
                else if($forcheck[0] == 'dep' AND $forcheck[$forcheck_cc] == 0){
                    $values .= "('$user_id',NOW(),'$processing_id','0','$ff','$value',0),";
                    $ff++;
                }
                else if($forcheck[0] == 'dep' AND $forcheck[$forcheck_cc] != 0){
                    $values .= "('$user_id',NOW(),'$processing_id','0','$dd','$value','$forcheck[$forcheck_cc]'),";
                    $dd++;
                }
                else{
                    $detail_id = explode('--',$key);
                    $text_value = '';
                    $int_value = '';
                    if($detail_id[2] == 1 OR $detail_id[2] == 2 OR $detail_id[2] == 3 OR $detail_id[2] == 4 OR $detail_id[2] == 5){
                        $text_value = $value;
                    }
                    else if($detail_id[2] == 6 OR $detail_id[2] == 7 OR $detail_id[2] == 8 OR $detail_id[2] == 9){
                        $int_value = $value;
                    }
                    else if($detail_id[2] == 10){
                        $selector_id = explode('_',$detail_id[0]);
                        $text_value = $selector_id[1];
                        $int_value = $value;
                    }
                    //$text_value = mysql_real_escape_string($text_value);
                    $values .= "('$user_id',NOW(),'$processing_id','$detail_id[1]','$text_value','$int_value',''),";


                    if($detail_id[1] == 124){
                        $phone = $value;
                    }
                    if($detail_id[1] == 127){
                        $firstname = $value;
                    }
                    if($detail_id[1] == 128){
                        $lastname = $value;
                    }
                }
            }
            //die('phone: '.$phone.' name: '.$firstname.' lastname: '.$lastname);

            $db->setQuery(" SELECT id
                        FROM saved_users_by_phone
                        WHERE phone = '$phone'");

            $saved_user = $db->getResultArray();
            $user_name = $firstname.' '.$lastname;
            if($saved_user['count'] == 0){
                $db->setQuery("INSERT INTO saved_users_by_phone(`phone`,`name`) VALUES('$phone','$user_name')");
                $db->execQuery();
            }
            else{
                $db->setQuery("UPDATE saved_users_by_phone SET name='$user_name' WHERE phone='$phone'");
                $db->execQuery();
            }


            $values = substr($values, 0, -1);
            $db->setQuery("DELETE FROM $tableToInsert WHERE incomming_request_id='$processing_id' AND (additional_dialog_id IS NULL OR additional_dialog_id = 0)");
            $db->execQuery();
            $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`incomming_request_id`,`processing_setting_detail_id`,`value`,`int_value`,`additional_deps`) VALUES $values";

            $db->setQuery($insert_query);
            $db->execQuery();

            $values2 = substr($values2, 0, -1);
            $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`incomming_request_id`,`additional_dialog_id`,`processing_setting_detail_id`,`value`,`int_value`) VALUES $values2";

            $db->setQuery($insert_query);
            $db->execQuery();

            $db->setQuery(" UPDATE      incomming_call SET 
                                        user_id=  IF(ISNULL(user_id) OR user_id = 0,'$user_id', user_id)
                            WHERE id='$processing_id'");
            $db->execQuery();
            
            
            $db->setQuery(" SELECT  mail_chat_id
                            FROM    incomming_call
                            WHERE   id = '$processing_id'");
            $mail_id = $db->getResultArray();
            $mail_id = $mail_id['result'][0]['mail_chat_id'];

            if($mail_id != ''){
                $db->setQuery("UPDATE mail SET user_id = IF(ISNULL(user_id) OR user_id = 0,'$user_id', user_id), mail_status_id = 3 WHERE id = '$mail_id'");
                $db->execQuery();

                $db->setQuery("DELETE FROM mail_live WHERE id = '$mail_id'");
                $db->execQuery();
            }
        }
        else if($dialogType == 'outgoing'){
            $db->setQuery(" SELECT  `data-table`
                            FROM    processing_setting
                            WHERE   id = '$setting_id'");
            $tableToInsert = $db->getResultArray();
            $tableToInsert = $tableToInsert['result'][0]['data-table'];
            $ff = 1;
            $dd = 1;
            if($processing_id == ''){
                $userphone = $_REQUEST['telefoni___183--124--1'];
                $userphone = str_replace('-','',$userphone);

                $db->setQuery(" SELECT      incomming_call.id
                                FROM        asterisk_call_log
                                LEFT JOIN   incomming_call ON incomming_call.asterisk_incomming_id = asterisk_call_log.id
                                WHERE       asterisk_call_log.call_type_id = 2 AND asterisk_call_log.destination='$userphone' ORDER BY id DESC LIMIT 1");
                $processing_id = $db->getResultArray();
                $processing_id = $processing_id['result'][0]['id'];
            }
            foreach($_REQUEST AS $key => $value){
                if($dd == 5){
                    $dd = 1;
                }
                $forcheck = explode('_',$key);
                $forcheck_cc = count($forcheck) - 1;
                if($key == 'setting_id' OR $key == 'act' OR $key == 'processing_id'){
                    continue;
                }
                else if($key == 'region'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','1111','$value'),";
                }
                else if($key == 'municip'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','2222','$value'),";
                }
                else if($key == 'temi'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','3333','$value'),";
                }
                else if($key == 'village'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','4444','$value'),";
                }
                else if($forcheck[0] == 'dep' AND $forcheck[$forcheck_cc] == 0){
                    $values .= "('$user_id',NOW(),'$processing_id','0','$ff','$value',0),";
                    $ff++;
                }
                else if($forcheck[0] == 'dep' AND $forcheck[$forcheck_cc] != 0){
                    $values .= "('$user_id',NOW(),'$processing_id','0','$dd','$value','$forcheck[$forcheck_cc]'),";
                    $dd++;
                }
                else{
                    $detail_id = explode('--',$key);
                    $text_value = '';
                    $int_value = '';
                    if($detail_id[2] == 1 OR $detail_id[2] == 2 OR $detail_id[2] == 3 OR $detail_id[2] == 4 OR $detail_id[2] == 5){
                        $text_value = $value;
                    }
                    else if($detail_id[2] == 6 OR $detail_id[2] == 7 OR $detail_id[2] == 8 OR $detail_id[2] == 9){
                        $int_value = $value;
                    }
                    else if($detail_id[2] == 10){
                        $selector_id = explode('_',$detail_id[0]);
                        $text_value = $selector_id[1];
                        $int_value = $value;
                    }

                    $values .= "('$user_id',NOW(),'$processing_id','$detail_id[1]','$text_value','$int_value',''),";


                    if($detail_id[1] == 124){
                        $phone = $value;
                    }
                    if($detail_id[1] == 127){
                        $firstname = $value;
                    }
                    if($detail_id[1] == 128){
                        $lastname = $value;
                    }
                }
            }
            //die('phone: '.$phone.' name: '.$firstname.' lastname: '.$lastname);

            $db->setQuery(" SELECT id
                        FROM saved_users_by_phone
                        WHERE phone = '$phone'");

            $saved_user = $db->getResultArray();
            $user_name = $firstname.' '.$lastname;
            if($saved_user['count'] == 0){
                $db->setQuery("INSERT INTO saved_users_by_phone(`phone`,`name`) VALUES('$phone','$user_name')");
                $db->execQuery();
            }
            else{
                $db->setQuery("UPDATE saved_users_by_phone SET name='$user_name' WHERE phone='$phone'");
                $db->execQuery();
            }


            $values = substr($values, 0, -1);
            $db->setQuery("DELETE FROM $tableToInsert WHERE incomming_request_id='$processing_id' AND (additional_dialog_id IS NULL OR additional_dialog_id = 0)");
            $db->execQuery();
            $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`incomming_request_id`,`processing_setting_detail_id`,`value`,`int_value`,`additional_deps`) VALUES $values";

            $db->setQuery($insert_query);
            $db->execQuery();

            $values2 = substr($values2, 0, -1);
            $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`incomming_request_id`,`additional_dialog_id`,`processing_setting_detail_id`,`value`,`int_value`) VALUES $values2";

            $db->setQuery($insert_query);
            $db->execQuery();

            $db->setQuery("UPDATE incomming_call SET user_id=IF(ISNULL(user_id),'$user_id', user_id) WHERE id='$processing_id'");
            $db->execQuery();

        }
        else{
            $tableToInsert = 'compaign_request_processing';
            $ff = 1;
            $dd = 1;
            $userphone = $_REQUEST['telefoni___183--124--1'];
            $notModifiedPhone = explode('-', $_REQUEST['ast_source']);
            
            $userphone = '995'.str_replace('-','',$userphone);
            if($processing_id == ''){
                /* $db->setQuery(" SELECT  id 
                                FROM    outgoing_campaign_request_base 
                                WHERE   phone_number = '$userphone'
                                ORDER BY id DESC
                                LIMIT   1");
                $processing_id = $db->getResultArray(); */
                $processing_id = $notModifiedPhone[1];
            }
            
            foreach($_REQUEST AS $key => $value){
                if($dd == 5){
                    $dd = 1;
                }
                $forcheck = explode('_',$key);
                $forcheck_cc = count($forcheck) - 1;
                if($key == 'setting_id' OR $key == 'act' OR $key == 'processing_id'){
                    continue;
                }
                else if($key == 'region'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','1111','$value'),";
                }
                else if($key == 'municip'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','2222','$value'),";
                }
                else if($key == 'temi'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','3333','$value'),";
                }
                else if($key == 'village'){
                    $values2 .= "('$user_id',NOW(),'$processing_id','0','999999999','4444','$value'),";
                }
                else if($forcheck[0] == 'dep' AND $forcheck[$forcheck_cc] == 0){
                    $values .= "('$user_id',NOW(),'$processing_id','0','$ff','$value',0),";
                    $ff++;
                }
                else if($forcheck[0] == 'dep' AND $forcheck[$forcheck_cc] != 0){
                    $values .= "('$user_id',NOW(),'$processing_id','0','$dd','$value','$forcheck[$forcheck_cc]'),";
                    $dd++;
                }
                else{
                    $detail_id = explode('--',$key);
                    $text_value = '';
                    $int_value = '';
                    if($detail_id[2] == 1 OR $detail_id[2] == 2 OR $detail_id[2] == 3 OR $detail_id[2] == 4 OR $detail_id[2] == 5){
                        $text_value = $value;
                    }
                    else if($detail_id[2] == 6 OR $detail_id[2] == 7 OR $detail_id[2] == 8 OR $detail_id[2] == 9){
                        $int_value = $value;
                    }
                    else if($detail_id[2] == 10){
                        $selector_id = explode('_',$detail_id[0]);
                        $text_value = $selector_id[1];
                        $int_value = $value;
                    }

                    $values .= "('$user_id',NOW(),'$processing_id','$detail_id[1]','$text_value','$int_value',''),";


                    if($detail_id[1] == 124){
                        $phone = $value;
                    }
                    if($detail_id[1] == 127){
                        $firstname = $value;
                    }
                    if($detail_id[1] == 128){
                        $lastname = $value;
                    }
                }
            }
            //die('phone: '.$phone.' name: '.$firstname.' lastname: '.$lastname);

            $db->setQuery(" SELECT id
                        FROM saved_users_by_phone
                        WHERE phone = '$phone'");

            $saved_user = $db->getResultArray();
            $user_name = $firstname.' '.$lastname;
            if($saved_user['count'] == 0){
                $db->setQuery("INSERT INTO saved_users_by_phone(`phone`,`name`) VALUES('$phone','$user_name')");
                $db->execQuery();
            }
            else{
                $db->setQuery("UPDATE saved_users_by_phone SET name='$user_name' WHERE phone='$phone'");
                $db->execQuery();
            }


            $values = substr($values, 0, -1);
            $db->setQuery("DELETE FROM $tableToInsert WHERE base_id='$processing_id' AND additional_dialog_id IS NULL");
            $db->execQuery();
            $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`base_id`,`processing_setting_detail_id`,`value`,`int_value`,`additional_deps`) VALUES $values";

            $db->setQuery($insert_query);
            $db->execQuery();

            $values2 = substr($values2, 0, -1);
            $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`base_id`,`additional_dialog_id`,`processing_setting_detail_id`,`value`,`int_value`) VALUES $values2";

            $db->setQuery($insert_query);
            $db->execQuery();

            /* $db->setQuery("UPDATE incomming_call SET user_id='$user_id' WHERE id='$processing_id'");
            $db->execQuery(); */
        }


    break;
    case 'save-custom-addit':
        $info_board     = $_REQUEST['info_board'];
        $processing_id  = $_REQUEST['processing_id'];

        $tableToInsert = 'incomming_request_processing';
        
        foreach($_REQUEST AS $key => $value){
            $forcheck = explode('_',$key);
            $forcheck_cc = count($forcheck) - 1;
            if($key == 'setting_id' OR $key == 'info_board' OR $key == 'processing_id'){
                continue;
            }
            else if($key == 'region'){
                $values .= "('$user_id',NOW(),'$processing_id','$info_board','999999999','1111','$value'),";
            }
            else if($key == 'municip'){
                $values .= "('$user_id',NOW(),'$processing_id','$info_board','999999999','2222','$value'),";
            }
            else if($key == 'temi'){
                $values .= "('$user_id',NOW(),'$processing_id','$info_board','999999999','3333','$value'),";
            }
            else if($key == 'village'){
                $values .= "('$user_id',NOW(),'$processing_id','$info_board','999999999','4444','$value'),";
            }
            else{
                $detail_id = explode('--',$key);
                $text_value = '';
                $int_value = '';
                if($detail_id[2] == 1 OR $detail_id[2] == 2 OR $detail_id[2] == 3 OR $detail_id[2] == 4 OR $detail_id[2] == 5){
                    $text_value = $value;
                }
                else if($detail_id[2] == 6 OR $detail_id[2] == 7 OR $detail_id[2] == 8 OR $detail_id[2] == 9){
                    $int_value = $value;
                }
                else if($detail_id[2] == 10){
                    $selector_id = explode('_',$detail_id[0]);
                    $text_value = $selector_id[1];
                    $int_value = $value;
                }

                $values .= "('$user_id',NOW(),'$processing_id','$info_board','$detail_id[1]','$text_value','$int_value'),";
            }
        }

        $values = substr($values, 0, -1);
        $db->setQuery("DELETE FROM $tableToInsert WHERE incomming_request_id='$processing_id' AND additional_dialog_id='$info_board'");
        $db->execQuery();
        $insert_query = "INSERT INTO $tableToInsert(`user_id`,`datetime`,`incomming_request_id`,`additional_dialog_id`,`processing_setting_detail_id`,`value`,`int_value`) VALUES $values";
        
        $db->setQuery($insert_query);
        $db->execQuery();
    break;
    case 'add_multilevel':
        $id         = $_REQUEST['input_id'];

		$page		= multiLevelPage('',$id);
		$data		= array('page'	=> $page);

    break;
    case 'disable':
        $id = $_REQUEST['id'];
        $type = $_REQUEST['type'];

        if($type == 'fieldset'){
            $db->setQuery("UPDATE processing_fieldset SET actived = 0 WHERE id = '$id'");
        }
        else if($type == 'input'){
            $db->setQuery("UPDATE processing_setting_detail SET actived = 0 WHERE id = '$id'");
        }
        else if($type == 'setting'){
            $db->setQuery("UPDATE processing_setting SET actived = 0 WHERE id = '$id'");
            $db->execQuery();
            $db->setQuery("DELETE FROM processing_setting_by_source WHERE processing_setting_id='$id'");
        }
        $db->execQuery();
    break;
    case 'multilevel_edit':
        $cat_id     = $_REQUEST['id'];
        $input_id   = $_REQUEST['input_id'];
		$page		= multiLevelPage(GetCategory($cat_id,$input_id),$input_id);
		$data		= array('page'	=> $page);

    break;
    case 'save_setting':
        $id         = $_REQUEST['setting_id'];
        $values     = $_REQUEST['source_values'];
        $page_id    = $_REQUEST['page_id'];
        if($id != ''){
            $db->setQuery("UPDATE processing_setting_by_source SET actived='0' WHERE processing_setting_id = '$id'");
            $db->execQuery();
            foreach($values AS $item){
                $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                                FROM    processing_setting_by_source
                                WHERE   source_id = '$item' AND processing_setting_id = '$id'");
                $source = $db->getResultArray();
    
                if($source['result'][0]['cc'] == 0){
                    $db->setQuery("INSERT INTO processing_setting_by_source(`processing_setting_id`,`source_id`) VALUES('$id','$item')");
                    $db->execQuery();
                }
                else{
                    $db->setQuery("UPDATE processing_setting_by_source SET actived='1' WHERE processing_setting_id = '$id' AND source_id = '$item'");
                    $db->execQuery();
                }
            }
            $data['response'] = 'suc';
        }
        else{
            $new_setting_id = $db->getIncrement('processing_setting');
            $data['response'] = $new_setting_id;

            $db->setQuery("INSERT INTO processing_setting(`user_id`,`datetime`,`processing_page_id`) VALUES('$user_id',NOW(),$page_id)");
            $db->execQuery();
            foreach($values AS $item){
                $db->setQuery("INSERT INTO processing_setting_by_source(`processing_setting_id`,`source_id`) VALUES('$new_setting_id','$item')");
                $db->execQuery();
            }
            $data['response'] = 'suc';
        }
        
    break;
    case 'save_input_tab':
        $tab_name     = $_REQUEST['tab_name'];
        $field_id     = $_REQUEST['field_id'];
        
        $db->setQuery(" SELECT COUNT(*) AS 'cc'
                        FROM processing_input_tabs
                        WHERE actived = 1 AND name='$tab_name'");
        $exist_name = $db->getResultArray();

        if($exist_name['result'][0]['cc'] == 0){
            $db->setQuery("INSERT INTO processing_input_tabs(`name`,`field_id`) VALUES('$tab_name','$field_id')");
            $db->execQuery();
            $data['response'] = 'suc';
        }
        else{
            $data['err'] = 'exist_name';
        }
    break;
    case 'fieldset_editor':
        $id         = $_REQUEST['fieldset_id'];
		$page		= GetFieldsetEditor(getInputs($id),$id);
		$data		= array('page'	=> $page);

    break;
    case 'input_editor':
        $id         = $_REQUEST['input_id'];
        $field_id   = $_REQUEST['fieldset_id'];
		$page		= GetInputsEditor($id,$field_id);
		$data		= array('page'	=> $page);

    break;
    case 'add_selector':
        $id         = $_REQUEST['input_id'];
        $selector   = $_REQUEST['selector_id'];
		$page		= GetSelectorEditor($id,$selector);
		$data		= array('page'	=> $page);

    break;
    case 'save_fieldset':
        $id         = $_REQUEST['fieldset_id'];
        $field_name = $_REQUEST['field_name'];
        $field_key  = $_REQUEST['field_key'];
        $setting_id = $_REQUEST['setting_id'];

        if($id != ''){
            $db->setQuery("UPDATE processing_fieldset SET name = '$field_name' WHERE id = '$id'");
            $db->execQuery();
            $data['response'] = 'suc';
        }
        else{
            $db->setQuery(" SELECT      COUNT(*) AS 'cc'
                            FROM        processing_fieldset_by_setting
                            LEFT JOIN   processing_fieldset ON processing_fieldset.id = processing_fieldset_by_setting.fieldset_id
                            WHERE       processing_fieldset_by_setting.processing_setting_id = '$setting_id' AND processing_fieldset.key = '$field_key'");

            $count = $db->getResultArray();
            $increment = $db->getIncrement('processing_fieldset');
            if($count['result'][0]['cc'] == 0)
            {
                $db->setQuery("INSERT INTO processing_fieldset_by_setting(`processing_setting_id`,`fieldset_id`) VALUES('$setting_id','$increment')");
                $db->execQuery();

                $db->setQuery("INSERT INTO processing_fieldset(`user_id`,`datetime`,`key`,`name`) VALUES('$user_id',NOW(),'$field_key','$field_name')");
                $db->execQuery();

                $data['response'] = 'suc';
            }
            else{
                $field_key = $field_key.'-'.$increment;
                $db->setQuery("INSERT INTO processing_fieldset_by_setting(`processing_setting_id`,`fieldset_id`) VALUES('$setting_id','$increment')");
                $db->execQuery();

                $db->setQuery("INSERT INTO processing_fieldset(`user_id`,`datetime`,`key`,`name`) VALUES('$user_id',NOW(),'$field_key','$field_name')");
                $db->execQuery();

                $data['response'] = 'suc';
            }
        }
        
    break;
    case 'save_selector':
        $id         = $_REQUEST['input_id'];
        $selector   = $_REQUEST['selector_id'];
        $param_name = $_REQUEST['param_name'];
        if($id != ''){
            $db->setQuery(" SELECT  processing_setting_detail.table_name,
                                processing_setting_detail.key AS 'input_key',
                                processing_fieldset.key AS 'field_key'
                        FROM    processing_setting_detail
                        LEFT JOIN processing_fieldset ON processing_setting_detail.processing_fieldset_id = processing_fieldset.id
                        WHERE   processing_setting_detail.id = $id");
            $res = $db->getResultArray();
            $tableName = $res['result'][0]['table_name'];
            if($tableName == '')
            {
                $new_tableName = 'dir_'.$res['result'][0]['field_key'].'_'.$res['result'][0]['input_key'];

                //echo $new_tableName;

                $db->setQuery("CREATE TABLE `$new_tableName` (
                                            id INT(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
                                            user_id INT(4),
                                            datetime datetime,
                                            name VARCHAR(70),
                                            actived INT(2) DEFAULT 1
                                            )");
                $db->execQuery();
                $db->setQuery("INSERT INTO `$new_tableName` (`user_id`,`datetime`,`name`) VALUES('$user_id',NOW(),'$param_name')");
                $db->execQuery();
                $db->setQuery("UPDATE processing_setting_detail SET table_name='$new_tableName' WHERE id = '$id'");
                $db->execQuery();
                $data['response'] = 'suc';
            }
            else{
                if($selector != ''){
                    
                    $db->setQuery("UPDATE $tableName SET name = '$param_name' WHERE id = '$selector' ");
                    $answer = $db->execQuery();
                    if($answer){
                        $data['response'] = 'suc';
                    }
                }
                else{
                    $db->setQuery("INSERT INTO $tableName (`user_id`,`datetime`,`name`) VALUES('$user_id',NOW(),'$param_name')");
                    $answer = $db->execQuery();
                    
                    $data['response'] = 'suc';
                }
            }
            $data['input_id'] = '';
        }
        else{
            $field_id = $_REQUEST['field_id'];
            $db->setQuery(" SELECT `key`
                            FROM processing_fieldset
                            WHERE id = '$field_id'");
            $res = $db->getResultArray();
            $db->setQuery("INSERT INTO processing_setting_detail 
                                            (`user_id`,`datetime`,`name`,`processing_fieldset_id`) VALUES ('$user_id',NOW(),'temporary_name_sys','$field_id')");
            $db->execQuery();

            $increment = $db->getIncrement('processing_setting_detail');
            $increment = $increment - 1;
            $new_tableName = 'dir_'.$res['result'][0]['key'].'_newinputID_'.$increment;



            $db->setQuery("UPDATE processing_setting_detail SET table_name='$new_tableName' WHERE id='$increment'");
            $db->execQuery();



            $db->setQuery("CREATE TABLE `$new_tableName` (
                                        id INT(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
                                        user_id INT(4),
                                        datetime datetime,
                                        name VARCHAR(70),
                                        actived INT(2) DEFAULT 1
                                        )");
            $db->execQuery();
            $db->setQuery("INSERT INTO `$new_tableName` (`user_id`,`datetime`,`name`) VALUES('$user_id',NOW(),'$param_name')");
            $answer = $db->execQuery();

            $data['response'] = 'suc';
            $data['input_id'] = $increment;
        }
        
            

    break;
    case 'save_input':
        $id            = $_REQUEST['input_id'];
        $field_id      = $_REQUEST['field_id'];
        $name          = $_REQUEST['input_name'];
        $type          = $_REQUEST['input_type'];
        $position      = $_REQUEST['input_pos'];
        $key           = $_REQUEST['key'];
        $depth         = $_REQUEST['multilevel_deep'];
        $name_1        = $_REQUEST['name_1'];
        $name_2        = $_REQUEST['name_2'];
        $name_3        = $_REQUEST['name_3'];
        $nec           = $_REQUEST['nec'];
        $input_tab     = $_REQUEST['input_tab'];
        if($nec == 'true'){
            $nec = 1;
        }
        else{
            $nec = 0;
        }
		if($id != ''){

            $db->setQuery(" SELECT COUNT(*) AS 'cc'
                            FROM processing_setting_detail
                            WHERE id != '$id' AND `name` = '$name' AND processing_fieldset_id = '$field_id' AND tab_id='$input_tab' AND actived=1");
            $key_count = $db->getResultArray();
            if($key_count['result'][0]['cc'] == 0){
                $db->setQuery("UPDATE processing_setting_detail SET name = '$name', necessary_input='$nec', processing_field_type_id = '$type', position='$position', multilevel_1='$name_1', multilevel_2='$name_2', multilevel_3='$name_3', tab_id='$input_tab',depth='$depth',`key`='$key' WHERE id='$id'");
                $res = $db->execQuery();
                if($res){
                    $data['response'] = 'suc';
                }
            }
            else{
                $data['response'] = 'not_uniq';
            }
            
        }
        else{
            if($type == 11 or $type == 12){
                $key = rand(100,9999).rand(1234,8975);
            }
            $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                            FROM    processing_setting_detail
                            WHERE   `key` = '$key' AND tab_id = '$input_tab' AND processing_fieldset_id = '$field_id' AND actived = 1");
            $key_count = $db->getResultArray();
            $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                            FROM    processing_setting_detail
                            WHERE   processing_fieldset_id = '$field_id' AND tab_id = '$input_tab' AND position = '$position'");
            $count = $db->getResultArray();
            if($count['result'][0]['cc'] > 0){
                if($key_count['result'][0]['cc'] == 0){
                    $db->setQuery("UPDATE processing_setting_detail SET position = position + 1 WHERE processing_fieldset_id = '$field_id' AND position >= '$position' AND actived = 1 ORDER BY position DESC");
                    $db->execQuery();
                    $key = $key . '___' .rand(100,999);
                    $db->setQuery("INSERT INTO processing_setting_detail 
                                            (`user_id`,`datetime`,`name`,`processing_fieldset_id`,`processing_field_type_id`,`position`,`key`,`multilevel_1`,`multilevel_2`,`multilevel_3`,`depth`,`necessary_input`,`tab_id`) VALUES ('$user_id',NOW(),'$name','$field_id','$type','$position','$key','$name_1','$name_2','$name_3','$depth','$nec','$input_tab')");
                    $db->execQuery();
    
                    $data['response'] = 'suc';
                }
                else{
                    $data['response'] = 'not_uniq';
                }
                
            }
            else{
                
                if($key_count['result'][0]['cc'] == 0){
                    $key = $key . '___' .rand(100,999);
                    $db->setQuery("INSERT INTO processing_setting_detail 
                                            (`user_id`,`datetime`,`name`,`processing_fieldset_id`,`processing_field_type_id`,`position`,`key`,`multilevel_1`,`multilevel_2`,`multilevel_3`,`depth`,`necessary_input`,`tab_id`) VALUES ('$user_id',NOW(),'$name','$field_id','$type','$position','$key','$name_1','$name_2','$name_3','$depth','$nec','$input_tab')");
                    $db->execQuery();
                    $data['response'] = 'suc';
                }
                else{
                    $data['response'] = 'not_uniq';
                }
                
            }
            //$db->setQuery("INSERT INTO processing_setting_detail (`user_id`,`datetime`,`name`,`processing_fieldset_id`,`processing_field_type_id`,`position`) VALUES (1,2,3)");
        }

    break;


    case 'get_columns':
    
        $columnCount = 		$_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] = 	$_REQUEST['names'];
        $operators[] = 		$_REQUEST['operators'];
        $selectors[] = 		$_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }
        
        
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
            {
                continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if($columns[$j] == 'id')
                {
                    $width = "5%";
                }
                else if($columns[$j] == 'position'){
                    $width = "12%";
                }
                else{
                    $width = 'auto';
                }
                if($columns[$j] == 'inc_id')
                {
                    $hidden = true;
                }
                else{
                    $hidden = false;
                }
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        
        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
        
        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for($j=0;$j<$columnCount;$j++)
        {
            if($types[$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else if($types[$j] == 'number'){

                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        
        $filtArr = array('fields'=>$new_data);
        
        
        
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);

        
        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
        
        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';
        
    break;
    case 'get_list':
        $columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
        $page_id        = $_REQUEST['page_id'];
        $db->setQuery(" SELECT	processing_setting.id,
                                processing_setting.datetime,
                                GROUP_CONCAT( DISTINCT source.`name` SEPARATOR '; ') AS 'sources',
                                GROUP_CONCAT( DISTINCT processing_fieldset.`name` SEPARATOR '; ') AS 'fieldsets'

                        FROM processing_setting
                        LEFT JOIN processing_setting_by_source ON processing_setting_by_source.processing_setting_id = processing_setting.id AND processing_setting_by_source.actived = 1
                        LEFT JOIN processing_fieldset_by_setting ON processing_fieldset_by_setting.processing_setting_id = processing_setting.id AND processing_fieldset_by_setting.actived = 1
                        LEFT JOIN source ON source.id = processing_setting_by_source.source_id
                        LEFT JOIN processing_fieldset ON processing_fieldset.id = processing_fieldset_by_setting.fieldset_id
                        WHERE processing_setting.processing_page_id = '$page_id' AND processing_setting.actived = '1'
                        GROUP BY processing_setting.id
");
        $result = $db->getKendoList($columnCount,$cols);

        $data = $result;

    break;
    case 'get_fieldset_list':
        $columnCount = 		$_REQUEST['count'];
        $cols[]      =      $_REQUEST['cols'];
        $id          =      $_REQUEST['id'];
        $page_id     =      $_REQUEST['page_id'];
        $db->setQuery(" SELECT 	    processing_fieldset.id,
                                    processing_fieldset.name,
                                    GROUP_CONCAT(processing_setting_detail.name SEPARATOR ', ')
                        FROM 		processing_fieldset
                        LEFT JOIN   processing_setting_detail ON processing_setting_detail.processing_fieldset_id = processing_fieldset.id
                        WHERE 	    processing_fieldset.actived = '1' AND processing_fieldset.id IN (SELECT fieldset_id FROM processing_fieldset_by_setting WHERE processing_setting_id = '$id')
                                                
                        GROUP BY    processing_fieldset.id");
        $result = $db->getKendoList($columnCount,$cols);

        $data = $result;

    break;
    case 'get_fieldset_input_list':
        $columnCount = 		$_REQUEST['count'];
        $cols[]      =      $_REQUEST['cols'];
        $id          =      $_REQUEST['id'];
        $fieldset_id =      $_REQUEST['fieldset_id'];
        $db->setQuery(" SELECT      processing_setting_detail.id,
                                    processing_setting_detail.name,
                                    processing_field_type.name,
                                    processing_input_tabs.name,
                                    processing_setting_detail.position
                        FROM        processing_setting_detail
                        LEFT JOIN   processing_field_type ON processing_field_type.id = processing_setting_detail.processing_field_type_id
                        LEFT JOIN   processing_input_tabs ON processing_input_tabs.id = processing_setting_detail.tab_id
                        WHERE       processing_setting_detail.actived = '1'  AND processing_setting_detail.processing_fieldset_id = '$fieldset_id'");
        $result = $db->getKendoList($columnCount,$cols);

        $data = $result;

    break;
    case 'get_selectors_data':
        $id          = $_REQUEST['input'];
        $cols[]      = $_REQUEST['cols'];
        $columnCount = $_REQUEST['count'];
        $db->setQuery(" SELECT  table_name
                        FROM    processing_setting_detail
                        WHERE   id = '$id'");
        $table = $db->getResultArray();
        $tableName = $table['result'][0]['table_name'];

        $db->setQuery(" SELECT  id,
                                name
                        FROM    $tableName
                        WHERE   actived = '1'");
        $result = $db->getKendoList($columnCount,$cols);

        $data = $result;

    break;
    case 'get_pages':
        
        $db->setQuery(" SELECT  processing_page.id,
                                processing_page.name
                        FROM    processing_page
                        WHERE   actived='1'");
        $result = $db->getResultArray();
        $data['tabs'] = $result['result'];

    break;
}
$data['error'] = $error;
echo json_encode($data);

function getInputs($id){
    global $db;
    if($id != ''){
        $db->setQuery(" SELECT  name,
                                `key`
                        FROM    processing_fieldset
                        WHERE   id = '$id'");
                        $res = $db->getResultArray();

        return $res['result'][0];
    }
    else{
        return '';
    }
    

}
function get_input_types($id){
    global $db;
    $data = '';
    $db->setQuery("SELECT 	`id`, `name`
					FROM 	`processing_field_type`
					WHERE 	actived=1");
    
    
    $data = $db->getSelect($id);
    return $data;
}
function get_source($id,$page_id='',$setting_id=''){
    global $db;
    $data = '';
    if($setting_id != ''){
        $db->setQuery(" SELECT  id,
                                name
                        FROM    source
                        WHERE   actived = 1 AND id IN (SELECT source_id FROM processing_setting_by_source WHERE actived = 1 AND processing_setting_id = '$setting_id')
                        UNION ALL
                        SELECT  id,
                                name
                        FROM    source
                        WHERE   actived = 1 AND id NOT IN (SELECT source_id
                        FROM    processing_setting_by_source
                        
                        WHERE   processing_setting_id IN (SELECT id FROM processing_setting WHERE processing_page_id = '$page_id' AND actived = 1) AND actived = 1)");
        $data = $db->getSelect($id);
        return $data;
    }
    else{
        $db->setQuery(" SELECT  id,
                                name
                        FROM    source
                        WHERE   actived = 1 AND id NOT IN (SELECT source_id
                        FROM    processing_setting_by_source
                        
                        WHERE   processing_setting_id IN (SELECT id FROM processing_setting WHERE processing_page_id = 1 AND actived = 1) AND actived = 1)");
        $data = $db->getSelect($id);
        return $data;
    }
}
function get_demo_view($field_id){
    global $db;
    $db->setQuery("	SELECT id,name
                    FROM processing_fieldset
                    WHERE actived = '1' AND id = '$field_id'");
    $fieldsets = $db->getResultArray();
    foreach($fieldsets['result'] AS $fieldset){
        $inputs_by_tabs = array();
        $db->setQuery("	SELECT	id,name
                        FROM 	processing_input_tabs
                        WHERE 	actived = 1 AND field_id = '$fieldset[id]'");
        $tabs = $db->getResultArray();
        $demo .='	<fieldset class="communication_right_side_info">
                        <legend>'.$fieldset[name].'</legend>';
                        if($tabs['count'] > 1){
                            $demo .= '<ul class="dialog_input_tabs" id="dialog-tab-'.$fieldset[id].'">';
                            $tab_count = 0;
                            foreach($tabs['result'] AS $tab){
                                if($tab_count == 0){
                                    $demo .= '<li aria-selected="true" data-id="'.$tab['id'].'">'.$tab['name'].'</li>';
                                }
                                else{
                                    $demo .= '<li data-id="'.$tab['id'].'">'.$tab['name'].'</li>';
                                }
                                $tab_count++;
                                $inputs_by_tabs[] = $tab['id'];
                            }
                            $demo .= '</ul>';
                        }
                        else{
                            $inputs_by_tabs[] = 1;
                        }
                            
                            $demo .= '<div class="fieldset_row">
                                '.get_fieldset_inputs($fieldset[id],$saved_values,$inputs_by_tabs).'
                            </div>
                    </fieldset>';

        return $demo;
    }
}

function get_additional_view($project_id,$board,$inc_id){
    global $db;
    $db->setQuery("	SELECT 		incomming_request_processing.id,
                                incomming_request_processing.processing_setting_detail_id AS 'input_id',
                                incomming_request_processing.`value`,
                                incomming_request_processing.int_value,
                                processing_setting_detail.processing_field_type_id AS 'type'
                        
                    FROM 		incomming_request_processing
                    LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id
                    WHERE 		incomming_request_processing.incomming_request_id='$inc_id' AND incomming_request_processing.additional_dialog_id='$board'");
    $values = $db->getResultArray();
    $saved_values = $values['result'];

    $db->setQuery(" SELECT  region_selector
                    FROM    info_category
                    WHERE   id = '$project_id'");
    $region = $db->getResultArray();
    $region = $region['result'][0]['region_selector'];
    $demo .='	<div id="dialog-form">
                    <fieldset class="communication_right_side_info">
                        <legend>დამატებითი ველები</legend>
                        <div class="communication_right_side" id="right_side" style="width:100%; height:90%;min-height:200px;overflow:visible;">
                        <ff class="communication_right_side_info" id="demo_view">';
                        
                            $demo .= '<div class="fieldset_row">
                                '.get_additional_inputs($project_id,$saved_values,$inputs_by_tabs,$region,$inc_id,$board).'
                            </div>
                        </ff>
                        </div>
                    </fieldset>
                    <input type="hidden" id="addit_cat_id" value="'.$project_id.'">
                    <input type="hidden" id="addit_info_board" value="'.$board.'">
                </div>';

    return $demo;
    
}
function GetPage($id,$page_id){
    global $db;
    if($id != ''){
        $db->setQuery(" SELECT  GROUP_CONCAT(processing_setting_by_source.`source_id` SEPARATOR ',') AS 'sources'
                        FROM    processing_setting_by_source
                        WHERE   processing_setting_id = '$id' AND actived = '1'");

        $result = $db->getResultArray();

        $data  .= '
        <div id="dialog-form">
        <fieldset>
        <legend>დიალოგი წყაროს მიხედვით</legend>
        <table class="dialog-form-table" id="signature_dialog_form_main_table">
            <tr>
                <td style="width: 170px;"><label for="CallType" style="margin-top: 8px;">დიალოგის წყარო</label></td>
                <td style="width: 640px;">
                <select id="optional" multiple="multiple" data-placeholder="აირჩიეთ წყარო (ები)">
                    '.get_source($result[result][0][sources],$page_id,$id).'
                </select>
                </td>
            </tr>
            <tr style="height:5px;"></>
            
        </table>
        <div id="fieldsets_table"></div>

        </fieldset>

        <input type="hidden" id="setting_id" value="' . $id . '" />

        </div>';
    }
    else{
        $data  .= '
        <div id="dialog-form">
        <fieldset>
        <legend>დიალოგი წყაროს მიხედვით</legend>
        <table class="dialog-form-table" id="signature_dialog_form_main_table">
            <tr>
                <td style="width: 170px;"><label for="CallType" style="margin-top: 8px;">დიალოგის წყარო</label></td>
                <td style="width: 640px;">
                <select id="optional" multiple="multiple" data-placeholder="აირჩიეთ წყარო (ები)">
                    '.get_source($result[result][0][sources],$page_id,'').'
                </select>
                </td>
            </tr>
            <tr style="height:5px;"></>
            
        </table>
        </fieldset>

        <input type="hidden" id="page_id" value="' . $page_id . '" />

        </div>';
    }
    

	return $data;
}
function GetFieldsetEditor($res='',$id){
    global $db;
    if($id != '')
    {
        $display = 'block;';
    }
    else{
        $display = 'none;';
    }
    $data  .= '
    <div id="dialog-form" style="display:flex;">
        <div style="width:50%;"> 
            <fieldset>
                <legend>ძირითადი ინფორმაცია</legend>
                <table class="dialog-form-table" id="signature_dialog_form_main_table">
                    <tr>
                        <td style="width: 170px;"><label for="CallType" style="margin-top: 8px;">Fieldset დასახელება</label></td>
                        <td style="width: 300px;">
                            <input style="width:220px;" type="text" id="field_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['name'] . '" />
                        </td>
                    </tr>
                    <tr style="height:15px;"></>
                    
                    
                </table>
                <!-- ID -->
                <input type="hidden" id="fieldset_id" value="' . $id . '" />

            </fieldset>
            <fieldset style="display: '.$display.'">
                <legend>დამატებული ველები</legend>
                <div id="fieldset_inputs"></div>        
            </fieldset>
        </div>
        <div style="width: 50%; margin-left: 10px;">
            <fieldset style="height:100%;">
                <legend>DEMO VIEW</legend>
                <div class="communication_right_side" id="right_side" style="width:100%; height:90%;">
                    <ff class="communication_right_side_info" id="demo_view">'.get_demo_view($id).'</ff>
                </div>
            </fieldset>
        </div>
    </div>';

	return $data;
}
function get_additional_inputs($project_id,$saved_values,$inputs_by_tabs,$region,$inc_id,$board){
	GLOBAL $db;

	$db->setQuery("	SELECT 	id,
							`key`,
							name,
							field_type_id AS 'type',
							position,
							cat_id,
							necessary_input AS 'nec',
							'' AS 'class'

					FROM 	mepa_project_inputs
					WHERE 	cat_id = '$project_id' AND actived = '1'
					ORDER BY position");
	$inputs = $db->getResultArray();

		$divider = 1;
		for($j=0;$j<$inputs['count'];$j++){
            if($tabs == $inputs['result'][$j]['tab_id']){
                if($inputs['result'][$j]['type'] == 11){
                    if($divider%3 == 0){
                        $inputs['result'][$j-1]['class'] = 'col-6';
                        $inputs['result'][$j-2]['class'] = 'col-6';
                    }
                    else{
                        $inputs['result'][$j-1]['class'] = 'col-12';
                    }
                }
                $divider++;
            }
			
        }
        if($region == 1){
            /* $db->setQuery("	SELECT 		incomming_request_processing.id,
										incomming_request_processing.processing_setting_detail_id AS 'input_id',
										incomming_request_processing.`value`,
										incomming_request_processing.int_value
								
							FROM 		incomming_request_processing
                            WHERE 		incomming_request_processing.incomming_request_id='$inc_id' AND additional_dialog_id='$board' AND
                                        incomming_request_processing.processing_setting_detail_id = '999999999'");
            $values = $db->getResultArray();
            
            $reg = $values['result'][0]['int_value'] or 0;
            $municip = $values['result'][1]['int_value'];
            $temi = $values['result'][2]['int_value'];
            $village = $values['result'][3]['int_value'];

            $output .= '<div class="col-4"><label>რეგიონი</label><select id="region">'.get_regions_1($reg).'</select></div>';

            $output .= '<div class="col-4"><label>მუნიციპალიტეტი</label><select id="municip">'.get_regions_1_1($reg,$municip).'</select></div>';

            $output .= '<div class="col-4"><label>თემი</label><select id="temi">'.get_regions_1_1($municip,$temi).'</select></div>';

            $output .= '<div class="col-4"><label>სოფელი</label><select id="village">'.get_regions_1_1($temi,$village).'</select></div>'; */
        }
		for($i=0;$i<$inputs['count'];$i++){
			if($tabs == $inputs['result'][$i]['tab_id']){
				$nec = '';
				$nec_is = 0;
				$default = 'col-4';
				if($inputs['result'][$i]['nec'] == 1){
					$nec = '<span style="color:red;"> *</span>';
					$nec_is = 1;
				}
		
				if($inputs['result'][$i]['class'] != ''){
					$default = $inputs['result'][$i]['class'];
				}
		
				if($inputs['result'][$i]['type'] == 1 OR $inputs['result'][$i]['type'] == 4 OR $inputs['result'][$i]['type'] == 5)
				{	
					$show = 0;
					foreach($saved_values AS $saved){
						if($saved['input_id'] == $inputs['result'][$i]['id']){
							$show++;
							$output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$saved[value].'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" /></div>';
						}
					}
					if($show == 0){
						$output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" /></div>';
					}
				}
				else if($inputs['result'][$i]['type'] == 8)
				{
                    $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; "><option>----</option></select></div>';	
				}
				else if($inputs['result'][$i]['type'] == 6)
				{
					$cnobari_table = $inputs['result'][$i]['table_name'];
		
					$output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.get_cnobari_selector_options($cnobari_table,'radio',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
				}
				else if($inputs['result'][$i]['type'] == 7)
				{
					$cnobari_table = $inputs['result'][$i]['table_name'];
		
					$output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.get_cnobari_selector_options($cnobari_table,'checkbox',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
				}
				else if($inputs['result'][$i]['type'] == 2)
				{
					$cnobari_table = $inputs['result'][$i]['table_name'];
		
					$output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content"></textarea></div>';
				}
				else if($inputs['result'][$i]['type'] == 12)
				{
					$output .= '<div class="col-4"></div>';
				}
				else if($inputs['result'][$i]['type'] == 10){
					$depth = $inputs['result'][$i]['depth'];
					$level_1_id = 0;
					$level_2_id = 0;
					$level_3_id = 0;
					for($k=1;$k<=$depth;$k++){
						$output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; "><option>----</option></select></div>';
					}
                }
				$divider++;
			}
			
		}
	
	return $output;
}
function get_fieldset_inputs($field_id,$saved_values,$inputs_by_tabs){
	GLOBAL $db;

	$db->setQuery("	SELECT 	id,
							`key`,
							name,
							multilevel_table,
							table_name,
							processing_field_type_id AS 'type',
							position,
							multilevel_1,
							multilevel_2,
							multilevel_3,
							depth,
							tab_id,
							necessary_input AS 'nec',
							'' AS 'class'

					FROM 	processing_setting_detail
					WHERE 	processing_fieldset_id = '$field_id' AND actived = '1'
					ORDER BY position");
	$inputs = $db->getResultArray();

    $tab_count = count($inputs_by_tabs);
    $tabs_count = 0;
	foreach($inputs_by_tabs AS $tabs){
		$display = 'none';
		if($tabs_count == 0){
            $display = 'block';
            $tabs_count++;
        }
		$output .= '<div class="inp_tabs dialog-tab-'.$field_id.'" data-tab-id="'.$tabs.'"  style="display:'.$display.';">';
		$divider = 1;
		for($j=0;$j<$inputs['count'];$j++){
            if($tabs == $inputs['result'][$j]['tab_id']){
                if($inputs['result'][$j]['type'] == 11){
                    if($divider%3 == 0){
                        $inputs['result'][$j-1]['class'] = 'col-6';
                        $inputs['result'][$j-2]['class'] = 'col-6';
                    }
                    else{
                        $inputs['result'][$j-1]['class'] = 'col-12';
                    }
                }
                $divider++;
            }
			
		}
		for($i=0;$i<$inputs['count'];$i++){
			if($tabs == $inputs['result'][$i]['tab_id']){
				$nec = '';
				$nec_is = 0;
				$default = 'col-4';
				if($inputs['result'][$i]['nec'] == 1){
					$nec = '<span style="color:red;"> *</span>';
					$nec_is = 1;
				}
		
				if($inputs['result'][$i]['class'] != ''){
					$default = $inputs['result'][$i]['class'];
				}
		
				if($inputs['result'][$i]['type'] == 1 OR $inputs['result'][$i]['type'] == 4 OR $inputs['result'][$i]['type'] == 5)
				{	
					$show = 0;
					foreach($saved_values AS $saved){
						if($saved['input_id'] == $inputs['result'][$i]['id']){
							$show++;
							$output .= '<div style="pointer-events: none;" class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$saved[value].'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" /></div>';
						}
					}
					if($show == 0){
						$output .= '<div style="pointer-events: none;" class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" /></div>';
					}
				}
				else if($inputs['result'][$i]['type'] == 8)
				{
                    $output .= '<div style="pointer-events: none;" class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; "><option>----</option></select></div>';	
				}
				else if($inputs['result'][$i]['type'] == 6)
				{
					$cnobari_table = $inputs['result'][$i]['table_name'];
		
					$output .= '<div style="pointer-events: none;" class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.get_cnobari_selector_options($cnobari_table,'radio',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
				}
				else if($inputs['result'][$i]['type'] == 7)
				{
					$cnobari_table = $inputs['result'][$i]['table_name'];
		
					$output .= '<div style="pointer-events: none;" class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.get_cnobari_selector_options($cnobari_table,'checkbox',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
				}
				else if($inputs['result'][$i]['type'] == 2)
				{
					$cnobari_table = $inputs['result'][$i]['table_name'];
		
					$output .= '<div style="pointer-events: none;" class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content"></textarea></div>';
				}
				else if($inputs['result'][$i]['type'] == 12)
				{
					$output .= '<div style="pointer-events: none;" class="col-4"></div>';
				}
				else if($inputs['result'][$i]['type'] == 10){
					$depth = $inputs['result'][$i]['depth'];
					$level_1_id = 0;
					$level_2_id = 0;
					$level_3_id = 0;
					for($k=1;$k<=$depth;$k++){
						$output .= '<div style="pointer-events: none;" class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; "><option>----</option></select></div>';
					}
				}
				$divider++;
			}
			
		}
		$output .= '</div>';
		
	}
	return $output;
}
function get_cnobari_selector_options($table,$selector_type='selector',$key='',$input_id='',$selected=0,$saved_values=''){
	GLOBAL $db;
	if($table != ''){
		$db->setQuery("	SELECT 	id,
								name
						FROM 	$table
						WHERE 	actived = '1'");
		if($selector_type == 'selector'){
			
			return $db->getSelect($selected);
		}
		else if($selector_type == 'radio'){
			$output = '';
			$radio = $db->getResultArray();
			$output .= '<div style="pointer-events: none;" class="checkbox_container">';
			foreach($radio['result'] AS $item){
					$output .= '<div class="lonely-checkbox"><input type="radio" id="'.$key.'-'.$item[id].'--'.$input_id.'--6" name="'.$key.'" name="'.$key.'" value="'.$item[name].'">
							<label style="margin-top: 6px;margin-right: 5px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
				
			}
			$output .= '</div>';
			return $output;
		}
		else if($selector_type == 'checkbox'){
			//var_dump($saved_values);
			$output = '';
			$radio = $db->getResultArray();
			$output .= '<div style="pointer-events: none;" class="checkbox_container">';
			foreach($radio['result'] AS $item){
                $output .= '<div class="lonely-checkbox"><input style="height:12px!important;" type="checkbox" id="'.$key.'-'.$item[id].'--'.$input_id.'--7" name="'.$key.'" value="'.$item[name].'">
                        <label style="margin-right: 7px;margin-left: 3px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
			}
			
			$output .= '</div>';
			return $output;
		}
		
	}

}
function GetAddTabPage($field_id){
    global $db;
    $data  .= '
    <div id="dialog-form">
        <fieldset>
            <legend>ტაბი</legend>
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                <tr>
                    <td style="width: 170px;"><label for="CallType" style="margin-top: 8px;">ტაბის დასახელება</label></td>
                    <td style="width: 300px;">
                        <input style="width:220px;" type="text" id="tab_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="" />
                    </td>
                </tr>
                <tr style="height:5px;"></>
                
            </table>
            

        </fieldset>
    </div>';

	return $data;
}
function GetSelectorEditor($id,$selector){
    global $db;
	if($selector == ''){
        $val = '';
    }
    else{
        $db->setQuery(" SELECT  table_name
                        FROM    processing_setting_detail
                        WHERE   id = '$id'");
        $res = $db->getResultArray();
        $tableName = $res['result'][0]['table_name'];
        $db->setQuery(" SELECT  name
                        FROM    $tableName
                        WHERE   id = '$selector'");
        $res = $db->getResultArray();
        $val = $res['result'][0]['name'];
    }
    $data  .= '
    <div id="dialog-form">
        <fieldset>
            <legend>ინფორმაცია</legend>
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                <tr>
                    <td style="width: 170px;"><label for="CallType" style="margin-top: 8px;">პარამეტრის დასახელება</label></td>
                    <td style="width: 300px;">
                        <input style="width:220px;" type="text" id="param_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $val . '" />
                    </td>
                </tr>
                <tr style="height:5px;"></>
                
            </table>
            

        </fieldset>
		<!-- ID -->
        <input type="hidden" id="selector_id" value="' . $selector . '" />
    </div>';

	return $data;
}
function GetInputsEditor($id,$field_id){
    global $db;
    
    $db->setQuery("SELECT name,processing_field_type_id,position,`key`,depth,multilevel_1,multilevel_2,multilevel_3,necessary_input,tab_id FROM processing_setting_detail WHERE id='$id'");
    $result = $db->getResultArray();
    $vars = $result['result'][0];

    if($vars['processing_field_type_id'] == 6 or $vars['processing_field_type_id'] == 7 or $vars['processing_field_type_id'] == 8 or $vars['processing_field_type_id'] == 9){
        $displaySelectorsKendo = 'block;';
    }
    else{
        $displaySelectorsKendo = 'none;';
    }
    if($vars['necessary_input'] == 1){
        $necessary = 'checked';
    }

    if($vars['processing_field_type_id'] == 10){
        $displayMultiselect = 'contents;';
        $displayLevels = 'block;';
    }
    else{
        $displayMultiselect = 'none;';
        $displayLevels = 'none;';
    }


    if($vars['depth'] == 2){
        $selected2 = 'selected';
    }
    else if($vars['depth'] == 3){
        $selected3 = 'selected';
    }

    if($selected2 == 'selected' OR $selected3 == 'selected'){
        $displayTree = "block";
    }

    
    if($vars['position'] == ''){
        $db->setQuery(" SELECT 	IFNULL(MAX(position)+1,1) AS 'position'
                        FROM 	processing_setting_detail
                        WHERE	actived = 1 AND processing_fieldset_id = '$field_id'");
        $input_pos = $db->getResultArray();

        $input_pos = $input_pos['result'][0]['position'];
    }
    else{
        $input_pos = $vars['position'];
    }
    $data  .= '
    <div id="dialog-form">
        <fieldset>
            <legend>ველის პარამეტრები</legend>
            
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                
                <tr id="input_namer">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ველის დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" style="width:200px;" id="input_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $vars['name'] . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ველის ტიპი</label></td>
                    <td>
                        <select  id="input_type" style="width: 200px; height: 28px; ">'.get_input_types($vars['processing_field_type_id']).'</select>
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">აირჩიეთ ტაბი</label></td>
                    <td>
                        <select id="input_tab" style="width: 200px; height: 28px; ">'.get_input_tabs($vars['tab_id'],$field_id).'</select>
                        <div style="display: contents;" id="add_tab"><img style="border: none;margin-bottom: 3px;" src="media/images/icons/add.png"></div>
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">აუცილებელი ველი</label></td>
                    <td>
                        <input style="margin-top: 3px!important;" type="checkbox" id="necessary" '.$necessary.'>
                    </td>
                   
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">პოზიცია</label></td>
                    <td style="width: 300px;">
                        <input style="width:20px;" type="text" id="input_position" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $input_pos . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr style="display:none;">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">id-class</label></td>
                    <td style="width: 300px;">
                        <input type="text" id="input_key" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $vars['key'] . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr style="display:'.$displayMultiselect.'" class="multilevelpart_1">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">დონე</label></td>
                    <td>
                        <select id="multilevel_deep" style="width: 50px; height: 28px; ">
                            <option value="0">---</option>
                            <option '.$selected2.' value="2">2</option>
                            <option '.$selected3.' value="3">3</option>
                        </select>
                    </td>
                </tr>
                
            </table>
            <!-- ID -->
            <input type="hidden" id="input_id" value="' . $id . '" />
            <input type="hidden" id="field_id" value="' . $field_id . '" />
        </fieldset>
        <fieldset style="display:'.$displaySelectorsKendo.'" id="selectors_type_inputs">
            <legend>სელექტორი/CHECKBOX/RADIO</legend>
            <div id="selectors_type_kendo"></div>
        </fieldset>

        <fieldset style="display:'.$displayLevels.';" id="multilevelpart_2">
            <legend>დონეები</legend>
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                <tr style="display:'.$displayLevels.';" id="multi_level_1">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">დონე 1 დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" id="level_name_1" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$vars[multilevel_1].'" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr style="display:'.$displayLevels.';" id="multi_level_2">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">დონე 2 დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" id="level_name_2" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$vars[multilevel_2].'" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr style="display:'.$displayLevels.';" id="multi_level_3">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">დონე 3 დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" id="level_name_3" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$vars[multilevel_3].'" />
                    </td>
                </tr>
            </table>
            <table style="display:'.$displayTree.';" class="dialog-form-table" id="signature_dialog_form_main_table2">
                <tr>
                    <td>
                        <div id="button_area">
                            <button id="add_button" data-button="jquery-ui-button">დამატება</button>
                            <button class="toggle_all_categories" data-action="open">ყველას გახსნა</button>
                            <button class="toggle_all_categories" data-action="close">ყველას დახურვა</button>
                        </div>
                        <div class="container" style="margin-top:20px;">
                            <ul id="tree1"></ul>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
		
    </div>';

	return $data;
}
function multiLevelPage($res = '', $input_id){
    global $db;
    $hd = '';
    $ch1 = '';
    $ch2 = '';
    $hdd1 = '';
    $hdd2 = '';
    $hdd3 = '';
    $hdd4 = '';
    $hdd5 = '';
    $hdd6 = '';
    $hdd7 = '';
    $category_value         = $res['name'];
    $sub_category_value     = $res['id'];
    $category_select        = $res['parent'];
    $sub_category_1_select  = $res['grand_parent'];
    $sub_category_1_value   = $res['name'];
    $sub_category_2_value   = $res['name'];
    $hidden_class = 'style="display:none;"';
    if($res['id'] == ''){
        $hd = $hidden_class;
    }else{
        if($category_select == 0){
            $category_select = 0;
            $sub_category_1_select = 0;
            $sub_category_1_value = '';
            $sub_category_2_value = '';
            $hdd1 = '';
            $hdd2 = $hidden_class;
            $hdd3 = $hidden_class;
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = $hidden_class;
            $ch1 = '';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select == 0){
            $category_value = '';
            $sub_category_2_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = '';
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = '';
            $ch1 = 'checked';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select != 0){
            $category_select = $res['grand_parent'];
            $sub_category_value = $res['parent'];
            $category_value = '';
            $sub_category_1_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = $hidden_class;
            $hdd4 = '';
            $hdd5 = '';
            $hdd6 = '';
            $hdd7 = '';
            $ch1 = 'checked disabled';
            $ch2 = 'checked';
        }

    }
    if($input_id != ''){
        $db->setQuery(" SELECT  multilevel_table
                        FROM    processing_setting_detail
                        WHERE   id = '$input_id'");
        $multilevel_table = $db->getResultArray();
        $multilevel_table = $multilevel_table['result'][0]['multilevel_table'];
    }
    $data = '
	<div id="dialog-form">
	    <fieldset>
	    	
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="category">მთავარი კატეგორია</label></td>
					<td>
						<input '.$hdd1.' type="text" id="category_value" class="idls large" data-index="'. $res['id'] .'" value="' . $category_value . '" />
						<select '.$hd.' '.$hdd2.' id="category_select" class="idls large">' . Category($category_select,$multilevel_table)  . '</select>
					</td>
				    <td style="width: 170px;"><input '.$ch1.' style="margin-left: 15px;" type="checkbox" id="show_sub_category_1_value" /></td>
				</tr>
				<tr id="first_tr" '.$hd.' '.$hdd7.'>
					<td style="width: 170px;"><label for="sub_category_1">ქვე კატეგორია 1</label></td>
					<td>
						<input '.$hdd3.' type="text" id="sub_category_1_value" class="idls large" value="' . $sub_category_1_value . '" />
						<select '.$hdd4.' id="sub_category_1_select" class="idls large">' . Category1($category_select, $sub_category_value,$multilevel_table)  . '</select>
					</td>
				    <td style="width: 170px;"><input '.$ch2.' style="margin-left: 15px;" type="checkbox" id="show_sub_category_2_value" /></td>
				</tr>
				<tr id="second_tr" '.$hd.' '.$hdd5.'>
					<td style="width: 170px;"><label for="sub_category_2">ქვე კატეგორია 2</label></td>
					<td>
					    <input '.$hdd6.' type="text" id="sub_category_2_value" class="idls large" value="' . $sub_category_2_value . '" />
					</td>
				</tr>
			</table>
			<!-- ID -->
			<input type="hidden" id="cat_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
    return $data;
}

function get_input_tabs($id,$field){
    global $db;
    $data = '';
    $db->setQuery(" SELECT  id,
                            name
                    FROM 	processing_input_tabs
                    WHERE	id = 1
                    UNION ALL
                    SELECT  id,
                            name
                    FROM    processing_input_tabs
                    WHERE   actived = '1' AND field_id = '$field'");
    
    $data = $db->getSelect($id);
    return $data;
} 
function Category($id,$multilevel_table){
    global $db;
    if($multilevel_table != ''){
        $data = '';
        $db->setQuery("SELECT `id`,
                              `name`
                       FROM   $multilevel_table
                       WHERE   actived = 1 AND parent_id = 0");
        
        $req = $db->getResultArray();
    
    
        $data .= '<option value="0" selected="selected">----</option>';
        
        foreach ($req[result] AS $res){
            if($res['id'] == $id){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            } else {
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
        }
    }
    

    return $data;
}
function Category1($grand_parent, $parent,$multilevel_table){
    global $db;
    if($multilevel_table != ''){
        $data = '';
        $db->setQuery("SELECT `id`, 
                            `name`
                    FROM   $multilevel_table
                    WHERE  actived = 1 AND parent_id = '$grand_parent'");
        
        $req = $db->getResultArray();

        $data .= '<option value="0" selected="selected">----</option>';
        foreach ($req[result] AS $res){
            if($res['id'] == $parent){
                $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
            } else {
                $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
            }
        }
    }

    return $data;
}
function GetCategory($cat_id,$input_id){
    global $db;

    $db->setQuery(" SELECT  multilevel_table
                    FROM    processing_setting_detail
                    WHERE   id = '$input_id'");
    $multilevel_table = $db->getResultArray();
    $multilevel_table = $multilevel_table['result'][0]['multilevel_table'];

    $db->setQuery("SELECT `id`,
                          `name`,
                          `parent_id` as parent,
                          (SELECT `parent_id` FROM $multilevel_table WHERE `id` = parent) as grand_parent
                    FROM  $multilevel_table
                    WHERE `id` = $cat_id" );
    
    $res = $db->getResultArray();

    return $res[result][0];
}
function AddCategory($category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value,$table){
    global $db;
    $user_id = $_SESSION['USERID'];
    if($category_select == 0){
        $db->setQuery("INSERT INTO $table
                                  (`user_id`, `name`, `parent_id`, `client_id`)
                            VALUES
                                  ('$user_id', '$category_value', '0', '1')");
        $db->execQuery();
    }else if($sub_category_1_select == 0){
        $db->setQuery("INSERT INTO $table
                                  (`user_id`, `name`, `parent_id`, `client_id`)
                            VALUES
                                  ('$user_id', '$sub_category_1_value', '$category_select', '1')");
        
        $db->execQuery();
    }else{
        $db->setQuery("INSERT INTO $table
                                  (`user_id`, `name`, `parent_id`, `client_id`)
                            VALUES
                                  ('$user_id', '$sub_category_2_value', '$sub_category_1_select', '1')");
        
        $db->execQuery();
    }

}

function SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value,$table){
    global $db;
    $user_id	= $_SESSION['USERID'];
    if($category_value != ""){
        $db->setQuery("  UPDATE $table SET
                                `user_id` = '$user_id',
                                `name` = '$category_value',
                                `parent_id`	= 0
                         WHERE  `id` = $cat_id");
        $db->execQuery();
    }else if($sub_category_1_value != ""){
        $db->setQuery("  UPDATE $table SET
                                `user_id` = '$user_id',
                                `name` = '$sub_category_1_value',
                                `parent_id`	= $category_select
                         WHERE  `id` = $cat_id");
        $db->execQuery();
    }else{
        $db->setQuery("  UPDATE $table SET
            	                `user_id` = '$user_id',
            				    `name` = '$sub_category_2_value',
            				    `parent_id`	= $sub_category_1_select
        				 WHERE  `id` = $cat_id");
        $db->execQuery();
    }

}
function DisableCategory($cat_id,$table){
    global $db;
    
    $db->setQuery("UPDATE $table
				      SET `actived` = 0
				   WHERE  `id` = $cat_id");
    
    $db->execQuery();
}
function get_regions_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   regions
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

function get_regions($id){
    global $db;
    $db->setQuery(" SELECT id, name, parent_id
                    FROM regions
                    WHERE actived = 1 AND parent_id = $id");

    $req = $db->getResultArray();

    $data = '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req["result"] AS $res){
        $data .= '<option  value="' . $res['id'] . '" data-parent= "'.$res['parent_id'].'">' . $res['name'] . '</option>';
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
        
    }
    return $data;
}

function get_municipipalitets($id){
    global $db;

    $db->setQuery(" SELECT id, name, parent_id
                    FROM regions
                    WHERE actived = 1 AND parent_id = $id");

    $req = $db->getResultArray();

    $data = '<option value="0" selected="selected">----</option>';

    foreach($req["result"] AS $res){
        $data .= '<option  value="' . $res['id'] . '" data-parent= "'.$res['parent_id'].'">' . $res['name'] . '</option>';
    }
    return $data;
}




function get_regions_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    regions
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function mres($value)
{
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}
?>