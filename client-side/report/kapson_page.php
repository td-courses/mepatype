<html>
<head>
    <link rel="stylesheet" href="vendors/apex-charts/apexcharts.css">
    <script src="vendors/apex-charts/apex.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script>
        kendo.pdf.defineFont({
            "DejaVu Sans": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
            "DejaVu Sans|Bold": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
            "DejaVu Sans|Bold|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "DejaVu Sans|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "WebComponentsIcons": "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
        });
    </script>
    <script>
        var aJaxURL = "server-side/report/kapson_page.action.php";

        
        $(document).on('click', '#outgoing_excel_export', function() {
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("base_grid", "რეპორტი-", [0, 8]);
                $("#loading1").hide();
            }, 200);
        });
        $(document).ready(function() {
            var kendo = new kendoUI();
            GetDate('start_date');
            GetDate('end_date');
            var hidden = getFilterResult();
            LoadKendoTable(hidden);
            loadtabs(activep = "", activech = "");
            get_tabs_count();
        });
        function getFilterResult(pos) {

            if (pos == undefined) {
                pos = '';
            }
            var hidden = "&start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + pos;
            return hidden;
        }

        $(document).on("click", "#loadtable", function() {
            var hidden = getFilterResult();
            var communication_Types=$("#communicationTypes").val();
            LoadKendoTable(hidden)
        });
        $(document).on("click", "#contracts_excel", function(){
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_contract_list","კონტრაქტების სია",[]);
                $("#loading1").hide();
            }, 200)
        });  
        function LoadKendoTable(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'base_grid';
            var actions = '<button style="float:right;" id="outgoing_excel_export">ექსპორტი EXCEL</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 6;
            var columnsSQL = [ "day:date", "operator:string", "incom_answered:string","incom_rejected","outgoing_answered","outgoing_rejected", ];
            var columnGeoNames = [ "დღე",  "ოპერატორი", "შემომავალი-ნაპასუხები","შემომავალი-უპასუხო","გამავალი-ნაპასუხები","გამავალი-უპასუხო",];
            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0
            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            var date = new Date();
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators,showOperatorsByColumns, selectors, hidden, 0,'','','','','','', true, true, true, [2, 6, 10]);
        }        
    </script>
    <style>
        #base_grid {
            padding-top:15px;
            display: block;
            height: auto;
            border: 0 !important;
        }
    </style>
</head>
<body>
    <div id="tabs" style=" border: 0px solid #aaaaaa;">
        <div class="callapp_filter_body" myvar="1">
            <div style="width: 100%;">
                <table style="width: 50%;">
                    <tr style="float: left;">
                        <td style="padding: 5px 0 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                        </td>
                        <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">   -   </td>
                        <td style="font-family: BPG; padding: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="font-family: BPG; width: 84px;">
                        </td>
                        <td>
                            <button id="loadtable">ფილტრი</button>
                        </td>    
                    </tr>
                    <tr>
                        <td colspan=3>
                    </tr></table></div>
        </div>
        <div class="l_responsible_fix" style="width: calc(100% - 250px)">
            <div id="base_grid"></div>
        </div>

    </div>

</body>
</html>