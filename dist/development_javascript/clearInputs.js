const createPostBtn = document.querySelector('.add_button')

if (createPostBtn) {

    createPostBtn.addEventListener('click', () => {
        clearAllInputsAndTextareas();
    })
}else{
    
}



 
const clearAllInputsAndTextareas = () => {
    
    const inputs = document.querySelector('.popup-window').querySelectorAll('input');
    const youtubeUrlInp = document.querySelector('.popup-window').querySelectorAll('.youtube_url');
    const imagePreview = document.querySelector('.popup-window').querySelectorAll('.image_preview');
    const imagePreviewImage = document.querySelector('.popup-window').querySelectorAll('.image_preview__image');
    const textareas = document.querySelector('.popup-window').querySelectorAll('textarea');

    const imageUpload = document.querySelector('.popup-window').querySelector('#image_upload');
    
    const questionsInputs = document.querySelector('.second_popup__addQuestions__container').querySelectorAll('input');
    const questionsTextareas = document.querySelector('.second_popup__addQuestions__container').querySelectorAll('textarea');


    inputs.forEach(input => input.value = "");
    textareas.forEach(input => input.value = "");
    questionsInputs.forEach(input => input.value = "");
    questionsTextareas.forEach(input => input.value = "");
    youtubeUrlInp.forEach(input => input.value = "");

    imagePreviewImage.forEach(img => img.style.backgroundImage = "");
    imagePreview.forEach(img => img.style.display = "none");

    imageUpload.value = "";
    imageUpload.dataset.imagename = "";


    const tags = document.querySelectorAll('li[data-tagedid]')
    tags.forEach(tag => tag.remove())


}



const clearYoutubeUrl = () =>{
    document.querySelectorAll('#youtube_url').forEach(el => el.value = "")
    document.querySelector('.video_preview').style.display = "none";
}

const clearImage = () =>{
    document.querySelectorAll('#image_upload').forEach(el => el.value = "")
    document.querySelector('.image_preview').style.display = "none";
}