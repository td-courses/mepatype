<head>
	<script type="text/javascript">
		var aJaxURL		= "server-side/report/operators_asa.action.php";		//server side folder url
		var tName		= "table_";											//table name											//tabs name
		var fName		= "add-edit-form";										//form name
		var colum_number = 6;
		var main_act    = "get_list";
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var dialog            = "add-edit-form";
		var file_name = '';
		var rand_file = '';
		
		$(document).ready(function () {
			GetDate('date_start');
			GetDate('date_end');
			LoadTable('index',colum_number,main_act,change_colum_main);
		});


		function LoadTable(tbl,col_num,act,change_colum){
			
			GetDataTable(tName+tbl,aJaxURL,act,col_num,"date_start="+$('#date_start').val()+"&date_end="+$('#date_end').val(),0,"",1,"desc",'',change_colum);
			setTimeout(function(){
		    	$('.ColVis, .dataTable_buttons').css('display','none');
		    	}, 160);
		}
		
		function LoadTableLog(){
			
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName+'index_log', aJaxURL, "get_list_log", 6, "", 0, "", 2, "desc", "", change_colum_main);
			setTimeout(function(){
		    	$('.ColVis, .dataTable_buttons').css('display','none');
		    	}, 160);
		}
    	
	    $(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });

	    $(document).on("change", "#date_start,#date_end", function () {
	    	LoadTable('index',colum_number,main_act,change_colum_main);
	    });
    </script>
</head>

<body>

<div id="tabs"  style="position: absolute !important;display: inline-block; width: 75%;" >
<div class="callapp_head">ASA ოპერატორების მიხედვით<hr class="callapp_head_hr"></div>
<div class="callapp_tabs">

</div>

<div>
<input type="text" id="date_start" style="width: 125px;">-დან
<input type="text" id="date_end" style="width: 125px;margin-left: 15px;">-მდე
</div>

<table id="table_right_menu" style="top: 28px;">
<tr>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
</td>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14">
</td>
<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
</td>
</tr>
</table>
<table class="display" id="table_index" style="width: 100%;">
    <thead>
		<tr id="datatable_header">
           <th>ID</th>
			<th style="width:52%; word-break:break-all;">ოპერატორი</th>
			<th style="width:12%; word-break:break-all;">ნაპასუხები<br>ზარები</th>
			<th style="width:12%; word-break:break-all;">საუბრის ხ-ბა<br>(სთ:წთ:წმ)</th>
			<th style="width:12%; word-break:break-all;">საშ. საუბრ. ხ-ბა<br>(სთ:წთ:წმ)</th>
			<th style="width:12%; word-break:break-all;">ASA<br>(სთ:წთ:წმ)</th>
		</tr>
	</thead>
	<thead>
		<tr class="search_header">
			<th class="colum_hidden">
    			<input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 10px"/>
    		</th>
			<th>
				<input style="width:100%;" type="text" name="search_sum_cost" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input style="width:100%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input style="width:100%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
			</th>
			<th> 
				<input style="width:100%;" type="text" name="search_sum_cost" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input style="width:100%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
			</th>
	    </tr>
	</thead>
</table>      
		 
<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="გამავალი ზარი">
<!-- aJax -->
</div>

</body>

