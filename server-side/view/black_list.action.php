<?php
// require_once('../../includes/classes/core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$black_id 	= $_REQUEST['id'];
		
		$page		= GetPage(Getblack_list($black_id));

		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
	  
		$query = "SELECT
						black_list.id,
						black_list.`date`,
						black_list.`phone`,
						user_info.`name`,
						black_list.`comment`
					FROM
						black_list
					JOIN user_info ON black_list.user_id = user_info.user_id
					WHERE
						black_list.actived = 1";
								  
								  $mysqli->setQuery($query);
								  $data = $mysqli->getList($count,$hidden,1);
		break;
	case 'save_black_list':
		$black_id 	= $_REQUEST['id'];
		$phone		= $_REQUEST['phone'];
		$hidden_phone = $_REQUEST['hidden_phone'];
		$comment 	= $_REQUEST['comment'];
		if($phone != ''){
			if(!Checkblack_listExist($phone, $black_id)){
				if ($black_id == '') {
					Addblack_list($phone, $comment);
				}else {
					saveblack_list($black_id, $phone,$comment);
				}				
			} else {
				if($phone == $hidden_phone){
					if ($black_id == '') {
						Addblack_list($phone, $comment);
					}else {
						saveblack_list($black_id, $phone,$comment);
					}				
				}
				else
				$error = '"' . $phone . '" უკვე არის სიაში!';
			}
		}

		break;
	case 'disable':
		$black_id	= $_REQUEST['id'];
		Disableblack_list($black_id);
		$query = "SELECT  black_list.phone 
												FROM `black_list`
												WHERE id=$black_id";

		$mysqli -> setQuery($query);
		$result = $mysqli -> getResultArray();

		$res = $result['result'][0];  

		$remm_phone=$res['phone'];
		
		$_REQUEST['act']="black_list_remove";		
		require_once "../asterisk/asterisk.action.php";
		
		

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addblack_list($phone, $comment)
{
	GLOBAL $mysqli;
	$c_date		= date('Y-m-d H:i:s');
	$user_id	= $_SESSION['USERID'];
	$query = "INSERT INTO 	`black_list`
								(`user_id`,`date`,`phone`,`comment`)
					VALUES 		('$user_id','$c_date','$phone','$comment')";

	$mysqli -> setQuery($query);
	$mysqli -> execQuery();				

					

}

function Saveblack_list($black_id, $phone,$comment)
{
	GLOBAL $mysqli;
	$user_id	= $_SESSION['USERID'];
	$query = "UPDATE `black_list`
				 SET    `user_id`='$user_id',
				 		`phone` = '$phone',
						 `comment` = '$comment'
				 WHERE	`id` = $black_id";

	$mysqli -> setQuery($query);
	$mysqli -> execQuery();				 
	
}

function Disableblack_list($black_id)
{
	GLOBAL $mysqli;
	$query = "	UPDATE `black_list`
					SET    `actived` = 0
					WHERE	`id` = $black_id";
	$mysqli -> setQuery($query);
	$mysqli -> execQuery();
}

function Checkblack_listExist($phone)
{
	GLOBAL $mysqli;
	$query = "	SELECT `id`
											FROM   `black_list`
											WHERE  `phone` = '$phone' && `actived` = 1";
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];

	if($res['id'] != ''){
		return true;
	}
	return false;
}


function Getblack_list($black_id)
	{
		GLOBAL $mysqli;
	$query = "SELECT 	`id`,
													`date`,
													`phone`,
													`comment`
											FROM   	`black_list`
											WHERE  	`id` = $black_id";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];


	return $res;
}

function GetPage($res = '')
{
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="CallType">თარიღი</label></td>
					<td>
						<input type="text" id="date" class="idle address" onblur="this.className=\'idle address\'" value="' . $res['date'] . '"disabled="disabled" />
					</td>
				</tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">ნომერი</label></td>
					<td>
						<input type="text" id="phone" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['phone'] . '" />
					</td>
				</tr>
				<tr>
					<td style="width: 170px;"><label for="CallType">კომენტარი</label></td>
					<td>	
						<textarea  style="width: 225px; resize: none;" id="comment" class="idle" name="call_content" cols="300" rows="4">' . $res['comment'] . '</textarea>
					</td>
				</tr>
				
			</table>
			<!-- ID -->
			<input type="hidden" id="black_list_id" value="' . $res['id'] . '" />
			<input type="hidden" id="black_list_phone" value="' . $res['phone'] . '" />
        </fieldset>
    </div>
    ';
	return $data;
}

?>
