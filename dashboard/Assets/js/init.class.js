
import { Theme } from "./common/theme.class.js";
import Helper from "./helpers/helper.class.js";
import Menu from './layouts/menu.class.js';
import Header from './layouts/header.class.js';

class init {

    constructor() {

        // INITIALIZE THEME
        new Theme().setTheme();
        new Theme().onSwitchClick();

        // INITIALIZE AJAX SETUP
        this.ajaxSetup();

        // INITIALIZE SCREEN LOADER
        this.setLoading();
        this.isLoading = true;

        // INITIALIZE MENU
        // new Menu().initializeMenu()

        // new Header().init();


        // RUN SOCKET
        this.runSocket()
    }

    /**
     * @description AJAX SETUP
     */
    ajaxSetup = () => {

        $.ajaxSetup({
            url: "index.php",
            async: true,
            method: "POST",
            dataType: "json",
            beforeSend: function () {

            },
            complete: function () {

            },
            error: function (jqXHR, exception) {

                if (jqXHR.status === 0) {
                    location.reload(true);
                } else if (jqXHR.status == 404) {
                    window.location = "index.php";
                } else if (jqXHR.status == 500) {
                    location.reload(true);
                } else if (exception === "parsererror") {
                    console.log(jqXHR.responseText);
                } else if (exception === "timeout") {
                    location.reload(true);
                } else if (exception === "abort") {
                    location.reload(true);
                } else {
                    location.reload(true);
                }
            }
        })

    }

    runSocket = () => {

        $.socket = {
            Base: new WebSocket('ws://localhost:8080'),
            kendo: new WebSocket('ws://localhost:8080')
        }

        // window.onbeforeunload = function () {
        //     $.socket.close();
        // }


    }


    setLoading = () => {

        let loading = new Helper().CreateElement({
            element: "loading",
            attributes: ["callapp-text"],
            children: "callapp"
        })

        $("body").append(loading)
    }


    useCookie = () => {

        let Cookie = new Helper().getTheCookie();

        if (Cookie == null) {

            new Helper().makeCookie();

        }

    }

    removeLoading = () => {
        this.isLoading = false;
    }


}


new init();