<?php

use function PHPSTORM_META\elementType;

include('../../includes/classes/class.Mysqli.php');

$mysqli = new dbClass();
$action    = $_REQUEST['act'];
$error    = '';
$data    = array();

$startDate   =          $_REQUEST['start'] . ' 00:00:00';
$endDate     =          $_REQUEST['end'] . ' 23:59:59';
$cat1_id    =           $_REQUEST['cat1_id'];
$cat2_id    =           $_REQUEST['cat2_id'];
$cat3_id    =           $_REQUEST['cat3_id'];
$region_id  =           $_REQUEST['region_id'];
$municipaliteti_id  =   $_REQUEST['municipaliteti_id'];
$temi_id  =           $_REQUEST['temi_id'];
$sofeli_id  =           $_REQUEST['temi_id'];
$gender_id  =           $_REQUEST['gender_id'];
$fermeri_id  =           $_REQUEST['fermeri_id'];
$piradi_id  =           $_REQUEST['piradi_id'];


$cat1_filter = '';
$cat2_filter  = '';
$cat3_filter  = '';
$region_filter = '';
$municipaliteti_filter = '';
$temi_filter = '';
$sofeli_filter = '';
$gender_filter = '';
$fermeri_filter = '';
$piradi_filter = '';


if ($startDate != "" && $endDate != "") {
    $date_filter .= " AND DATE(IRP.datetime) >= DATE('$startDate') ";
    $date_filter .= " AND DATE(IRP.datetime) <= DATE('$endDate') ";
}
if ($cat1_id > 0) {
    $cat1_filter = " AND CAT1.int_value = $cat1_id";
}
if ($cat2_id > 0) {
    $cat2_filter = " AND CAT2.int_value = $cat2_id";
}
if ($cat3_id > 0) {
    $cat3_filter = " AND CAT3.int_value = $cat3_id";
}
if ($region_id > 0) {
    $region_filter = " AND Region.int_value = $region_id";
}
if ($municipaliteti_id > 0) {
    $municipaliteti_filter = " AND Municip.int_value = $municipaliteti_id";
}
if ($temi_id > 0) {
    $temi_filter = " AND Temi.int_value = $temi_id";
}
if ($sofeli_id > 0) {
    $sofeli_filter = " AND Sofeli.int_value = $sofeli_id";
}
if ($gender_id > 0) {
    $gender_filter = " AND Gender.int_value = $gender_id";
}
if ($fermeri_id > 0) {
    $fermeri_filter = " AND Fermeri.int_value = $fermeri_id";
}
if ($piradi_id > 0) {
    $piradi_filter = " AND Piradi.value = $piradi_id";
}

switch ($action) {
    case 'get_list':
        $filter = "";

        $id          =         $_REQUEST['hidden'];
        $columnCount =         $_REQUEST['count'];
        $json_data   =         json_decode($_REQUEST['add'], true);
        $itemPerPage =         $json_data['pageSize'];
        // $skip        =         $json_data['skip'];
        $cols[]      =          $_REQUEST['cols'];

        $query = ("SELECT IRP.incomming_request_id,
                        DATE(IRP.datetime) AS DATE, 
                        -- IF(ISNULL( Phone1.`value` ) OR Phone1.`value` = '', ACL.destination, Phone1.`value` ) AS Phone,
                        ACL.source,
                        SRC.name_ka AS Communication,
                        USR.`name` AS Operator,
                        ACS.`name` AS CallEnd, 
                        AQ.number AS Queue,
                        AEX.`name` AS Extention
                FROM incomming_request_processing AS IRP

                LEFT JOIN incomming_request_processing AS Phone1
                            ON Phone1.incomming_request_id = IRP.incomming_request_id
                            AND IRP.processing_setting_detail_id = 124

                LEFT JOIN incomming_call AS INC ON IRP.incomming_request_id = INC.id
                LEFT JOIN source AS SRC ON INC.source_id = SRC.id

                LEFT JOIN user_info AS USR ON IRP.user_id = USR.user_id

                LEFT JOIN asterisk_call_log AS ACL ON INC.asterisk_incomming_id = ACL.id
                LEFT JOIN asterisk_call_status AS ACS ON ACL.call_status_id = ACS.id

                LEFT JOIN asterisk_queue AS AQ ON ACL.queue_id = AQ.id

                LEFT JOIN asterisk_extension AS AEX ON ACL.extension_id = AEX.id

                LEFT JOIN incomming_request_processing AS CAT1 ON IRP.incomming_request_id = CAT1.incomming_request_id                
                            AND CAT1.`value` = 1 AND CAT1.int_value > 0

                LEFT JOIN incomming_request_processing AS CAT2 ON IRP.incomming_request_id = CAT2.incomming_request_id
                            AND CAT2.`value` = 2 AND CAT2.int_value > 0

                LEFT JOIN incomming_request_processing AS CAT3 ON IRP.incomming_request_id = CAT3.incomming_request_id
                            AND CAT3.`value` = 3 AND CAT3.int_value > 0

                 LEFT JOIN incomming_request_processing AS Region ON IRP.incomming_request_id = Region.incomming_request_id    
                            AND Region.`value` = 1111 AND Region.int_value > 0

                 LEFT JOIN incomming_request_processing AS Municip ON IRP.incomming_request_id = Municip.incomming_request_id
                            AND Municip.`value` = 2222 AND Municip.int_value > 0

                 LEFT JOIN incomming_request_processing AS Temi ON IRP.incomming_request_id = Temi.incomming_request_id
                            AND Temi.`value` = 3333 AND Temi.int_value > 0

                 LEFT JOIN incomming_request_processing AS Sofeli ON IRP.incomming_request_id = Sofeli.incomming_request_id
                            AND Sofeli.`value` = 4444 AND Sofeli.int_value > 0

                LEFT JOIN incomming_request_processing AS Gender ON IRP.incomming_request_id = Gender.incomming_request_id        
	                        AND Gender.processing_setting_detail_id = 130

                LEFT JOIN incomming_request_processing AS Fermeri ON IRP.incomming_request_id = Fermeri.incomming_request_id       
	                        AND Fermeri.processing_setting_detail_id = 152

                LEFT JOIN incomming_request_processing AS Piradi ON IRP.incomming_request_id = Piradi.incomming_request_id         
	                        AND Piradi.processing_setting_detail_id = 129

                -- WHERE IRP.incomming_request_id is not null $cat1_filter $cat2_filter $cat3_filter $date_filter $region_filter $municipaliteti_filter $temi_filter $sofeli_filter $gender_filter $fermeri_filter $piradi_filter
                GROUP BY IRP.incomming_request_id
                LIMIT 10");

        $mysqli->setQuery($query);
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;
    case 'get_list_for_chart':
        $filter = "";
        $id          =      $_REQUEST['hidden'];

        // if (!empty($startDate)) {
        //     $filter .= "AND DATE(incomming_call.date) >= '$startDate'";
        // }
        // if (!empty($endDate)) {
        //     $filter .= "AND DATE(incomming_call.date) <= '$endDate '";
        // }

        $query = "SELECT COUNT(incomming_call.id ) AS CallCount,
                        SUM(IF((incomming_request_processing.int_value = 2),(1),(0))) AS Male,
                        SUM(IF((incomming_request_processing.int_value = 1 ),(1),(0))) AS Female 
                    FROM
                        incomming_call
                        JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id 
                        AND incomming_request_processing.processing_setting_detail_id = 130";

        // $query = "SELECT    MONTH(incomming_call.date) as month,
        //                         COUNT(incomming_call.id) as callcount,
        //                         SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
        //                         SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female
        //                 FROM incomming_call
        //                 JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
        //                 JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 1
        //                 LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
        // 			   LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
        //                LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
        //                 -- WHERE asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt
        //                 GROUP BY month
        //                 ORDER BY month ASC";

        $mysqli->setQuery($query);
        $data = $mysqli->getResultArray();

        break;
        // case 'get_list_for_chart2':
        //     $filter = "";
        //     $id          =      $_REQUEST['hidden'];
        //     $startDate  = $_REQUEST['start_date'];
        // $endDate  = $_REQUEST['end_date'];

        //     if (!empty($startDate)) {
        //         $filter .= " AND DATE(incomming_call.date) >= '$startDate' ";
        //     }
        //     if (!empty($endDate)) {
        //         $filter .= " AND DATE(incomming_call.date) <= '$endDate' ";
        //     }

        //     $query = "SELECT    MONTH(incomming_call.date) as month,
        //                             COUNT(incomming_call.id) as callcount,
        //                             SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
        //                             SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male
        //                     FROM incomming_call
        //                     JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
        //                     JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 2
        //                     LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
        // 			        LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
        //                     LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
        //                     WHERE asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt
        //                     GROUP BY month
        //                     ORDER BY month ASC";

        //     $mysqli->setQuery($query);
        //     $data = $mysqli->getResultArray();

        //     break;


    case 'cat_2':
        $cat_id = $_REQUEST['cat_id'];
        $page        = getCategory($cat_id);
        $data        = array('page' => $page);
        break;

    case 'cat_3':
        $cat_id = $_REQUEST['cat_id'];
        $page        = getCategory2($cat_id);
        $data        = array('page' => $page);
        break;

    case 'municipaliteti':
        $region_id = $_REQUEST['region_id'];
        $page        = getMunicip($region_id);
        $data        = array('page' => $page);
        break;

    case 'temi':
        $municip_id = $_REQUEST['municip_id'];
        $page        = getTemi($municip_id);
        $data        = array('page' => $page);
        break;

    case 'sofeli':
        $sofeli_id = $_REQUEST['sofeli_id'];
        $page        = getSofeli($sofeli_id);
        $data        = array('page' => $page);
        break;

    case 'get_dialog':
        // $page = "<div id='hiddenPhone' style='display:none;'>$phone</div>
        //         <div id='operators' style='margin-left:40px;'>
        //         <span style='margin-left:20px;'>თარიღი</span>
        //         <input type = 'text' id = 'dialog_start_date' hasDatePicker value = '$startDate' class='datetimes'/> -დან
        //         <input type = 'text' id = 'dialog_end_date' hasDatePicker value = '$endDate' class='datetimes'/> -მდე
        //         <button id='loadDialogData' style='margin-left:20px;'>ფილტრი</button></div> 
        //         <div id='kendo_dialog'>  </div>";

        $page  =
            '<div id="get_dialog">
            <fieldset>
            <legend>მომართვის ავტორი</legend>
                <div class="div-inputs">
                    <label>ტელეფონი</label>
                    <input value="" style="height: 18px; width: 95%;" type="text" id="phone-input" class="dialog-input" autocomplete="off">
                </div>
                <div class="div-inputs">
                    <label>ტელეფონი 2</label>
                    <input value="" style="height: 18px; width: 95%;" type="text" id="phone2-input" class="dialog-input" autocomplete="off">
                </div>
                <div class="div-inputs">
                    <label>თარიღი</label>
                    <input value="" style="height: 18px; width: 95%;" type="text" id="date-input" class="dialog-input" autocomplete="off">
                </div>
                <div class="div-inputs">
                    <label>ოპერატორი</label>
                    <input value="" style="height: 18px; width: 95%;" type="text" id="operator-input" class="dialog-input" autocomplete="off">
                </div>
                <div class="div-inputs">
                    <label>რიგის ნომერი</label>
                    <input value="" style="height: 18px; width: 95%;" type="text" id="queue-input" class="dialog-input" autocomplete="off">
                </div>
                <div class="div-inputs">
                    <label>ექსთენშენი</label>
                    <input value="" style="height: 18px; width: 95%;" type="text" id="ext-input" class="dialog-input" autocomplete="off">
                </div>
            </fieldset>
        </div>';

        $data = array('page' => $page);
        break;
        break;
    case 'get_columns':

        $columnCount =         $_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] =     $_REQUEST['names'];
        $operators[] =         $_REQUEST['operators'];
        $selectors[] =         $_REQUEST['selectors'];
        $f = 0;
        foreach ($cols[0] as $col) {
            $column = explode(':', $col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        foreach ($res as $item) {
            $columns[$i] = $item['Field'];
            $i++;
        }

        $dat = array();
        $a = 0;
        for ($j = 0; $j < $columnCount; $j++) {
            if (1 == 2) {
                continue;
            } else {

                if ($operators[0][$a] == 1) $op = true;
                else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                //$op = false;

                if ($res['data_type'][$j] == 'date') {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
                } else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
                } else {
                    if ($columns[$j] == "inc_status") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 153);
                    } elseif ($columns[$j] == "audio_file") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 150);
                    } elseif ($columns[$j] == "id" || $columns[$j] == 'protocol_hidde_id') {
                        $g = array('field' => $columns[$j], 'hidden' => true, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 100);
                    } elseif ($columns[$j] == "inc_date") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 130);
                    } else {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
                    }
                }
                $a++;
            }
            array_push($dat, $g);
        }

        // array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for ($j = 0; $j < $columnCount; $j++) {
            if ($res['data_type'][$j] == 'date') {
                $new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
            } else {
                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
            }
        }

        $filtArr = array('fields' => $new_data);
        $kendoData = array('columnss' => $dat, 'modelss' => $filtArr);

        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';

        break;
    case "get_chart":

        $divs = '';
        $count = 0;
        $db->setQuery("SELECT CASE
                                        WHEN source_id = 1 THEN 'ზარი'
                                        WHEN source_id = 11 THEN 'ჩათი'
                                        WHEN source_id = 6 THEN 'FB მესენჯერი'
                                        WHEN source_id = 10 THEN 'ვებ-ზარი'
                                        WHEN source_id = 7 THEN 'ელ-ფოსტა'
                                        WHEN source_id = 9 THEN 'ვაიბერი'
                                        ELSE 'სხვა'
                                   END AS `name`,
                                   CASE
                                        WHEN source_id = 1 THEN  'call'
                                        WHEN source_id = 11 THEN 'chat'
                                        WHEN source_id = 6 THEN  'fb'
                                        WHEN source_id = 10 THEN 'web'
                                        WHEN source_id = 7 THEN  'mail'
                                        WHEN source_id = 9 THEN  'viber'
                                        ELSE 'other'                       
                                   END AS `icon`,
                                   ROUND((COUNT(*) * 100 )/(SELECT COUNT(*) FROM incomming_call WHERE user_id = $user),2) AS `percent`,
                                   COUNT(*) AS `count`
                          FROM     incomming_call 
                          WHERE    user_id = $user
                          GROUP BY source_id");

        $res = $db->getResultArray();
        $values = array();
        foreach ($res['result'] as $arr) {
            $divs .= '<div><div class="' . $arr['icon'] . '_icon"></div>' . $arr['name'] . '</div><div> ' . $arr['percent'] . ' % </div><div>' . $arr['count'] . '</div> ';
            $count += $arr['count'];
            $values[$arr['icon']] = $arr['count'];
        }


        $page = '<div id="container" style="min-width: 210px; height: 300px; max-width: 400px; margin: 0"></div>
                     <div class="pie-counter">' . $count . '</div>
                     <div class="pie-table-grid">' . $divs . '</div>';

        $data = array("html" => $page, 'values' => $values);
        break;
    default:
        $error = 'Action is Null';
}

function getCategory($category_id)
{
    global $mysqli;
    $data = '';

    if ($data == '') {
        $data = '<option value="" selected>კატეგორია 2</option>';
    }

    $mysqli->setQuery("SELECT `id`, `name`
    					FROM `info_category`
    					WHERE `info_category`.`actived` = 1 AND `info_category`.`parent_id` = '$category_id'");

    $result = $mysqli->getResultArray();

    foreach ($result['result'] as $key) {
        if ($key['id'] == $category_id) {
            $data .= '<option value="' . $key['id'] . '" selected="selected">' . $key['name'] . '</option>';
        } else {
            $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
        }
    }
    return $data;
}

function getCategory2($category_id)
{
    global $mysqli;
    $data = '';

    if ($data == '') {
        $data = '<option value="" selected>კატეგორია 3</option>';
    }

    $mysqli->setQuery("SELECT `id`, `name`
    					FROM `info_category`
    					WHERE `info_category`.`actived` = 1 AND `info_category`.`parent_id` = '$category_id'");

    $result = $mysqli->getResultArray();

    foreach ($result['result'] as $key) {
        if ($key['id'] == $category_id) {
            $data .= '<option value="' . $key['id'] . '" selected="selected">' . $key['name'] . '</option>';
        } else {
            $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
        }
    }
    return $data;
}

function getMunicip($region_id)
{
    global $mysqli;
    $data = '';

    $mysqli->setQuery("SELECT `id`, `name`
    					FROM `regions`
    					WHERE `regions`.`actived` = 1 AND `regions`.`parent_id` = '$region_id' ");

    $result = $mysqli->getResultArray();

    foreach ($result['result'] as $key) {
        if ($key['id'] == $region_id) {
            $data .= '<option value="' . $key['id'] . '" selected="selected">' . $key['name'] . '</option>';
        } else {
            $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
        }
    }
    return $data;
}

function getTemi($municip_id)
{
    global $mysqli;
    $data = '';

    if ($data == '') {
        $data = '<option value="" selected>თემი</option>';
    }

    $mysqli->setQuery("SELECT `id`, `name`
    					FROM `regions`
    					WHERE `regions`.`actived` = 1 AND `regions`.`parent_id` = '$municip_id' ");

    $result = $mysqli->getResultArray();

    foreach ($result['result'] as $key) {
        if ($key['id'] == $municip_id) {
            $data .= '<option value="' . $key['id'] . '" selected="selected">' . $key['name'] . '</option>';
        } else {
            $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
        }
    }
    return $data;
}

function getSofeli($sofeli_id)
{
    global $mysqli;
    $data = '';

    if ($data == '') {
        $data = '<option value="" selected>სოფელი</option>';
    }

    $mysqli->setQuery("SELECT `id`, `name`
    					FROM `regions`
    					WHERE `regions`.`actived` = 1 AND `regions`.`parent_id` = '$sofeli_id' ");

    $result = $mysqli->getResultArray();

    foreach ($result['result'] as $key) {
        if ($key['id'] == $sofeli_id) {
            $data .= '<option value="' . $key['id'] . '" selected="selected">' . $key['name'] . '</option>';
        } else {
            $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
        }
    }
    return $data;
}

function getSelectors($table)
{
    global $db;
    $db->setQuery("	SELECT		`directory__projects`.`status` as `name`,
									`directory__projects`.`status` as `id`
						FROM
									`directory__projects` 
						WHERE  		`status` <> ''
						GROUP BY 	`status`");
    $result = $db->getResultArray();

    $selectorData = array();

    foreach ($result['result'] as $option) {
        array_push($selectorData, array('text' => $option['name'], 'value' => $option['id']));
    }

    return $selectorData;
}


$data['error'] = $error;
$data["filterID"] = $filterID;

echo $error;

echo json_encode($data);
