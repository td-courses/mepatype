<?php
/* ******************************
 *	Request aJax actions
 * ******************************
*/
ini_set('display_errors', 'On');
error_reporting(E_ERROR);
require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$action            = $_REQUEST['act'];
$error             = '';
$data              = array();
$task_id           = $_REQUEST['id'];
$hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
$rand_file         = $_REQUEST['rand_file'];
$file_name         = $_REQUEST['file_name'];

switch ($action) {
    case 'get_add_page':
        $number = $_REQUEST['number'];
        $page = GetPage($res = '', $number);
        $data = array('page' => $page);

        break;
    case 'get_edit_page':

        if (isset($_REQUEST['manual_id'])) {
            if($_REQUEST['check']==1){
                $page = get_page_manual(Get_task($_REQUEST['id']),Getproduct($_REQUEST['id'], "autocall_request_base"), GetMainProduct($_REQUEST['id']), $_REQUEST['id'], get_from_file($_REQUEST['id']));
                $data = array('page' => $page);
            }else if($_REQUEST['check']==0){
                $page = get_page_manual_out(Getproduct($_REQUEST['id'], "autocall_request_base"), GetMainProduct($_REQUEST['id']), $_REQUEST['id'], '');
                $data = array('page' => $page);
            }


        } else if($_REQUEST['auto_dialer_id']){

            $page = get_page_auto_dialer(Getproduct($_REQUEST['id'], "autocall_request_base"), GetMainProduct($_REQUEST['id']), $_REQUEST['id'], '');
            $data = array('page' => $page);
        } else {
            $page = GetPage(Getincomming($task_id),'');

            $data = array('page' => $page);
        }


        break;

    case 'get_list_sms':
        $incomming_call_id = $_REQUEST['incomming_id'];
        $hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
        $count             = $_REQUEST['count'];
        $hidden            = $_REQUEST['hidden'];

        if ($incomming_call_id == '') {
            $where = "outgoing_id = '$hidde_outgoing_id'";
        }else{
            $where = "incomming_call_id = '$incomming_call_id'";
        }
        $db->setQuery(" SELECT id,
                               date,
                               phone,
                              `content`,
                               if(`status`=1,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
                        FROM  `sent_sms`
                        WHERE  $where");
        
        $data = $db->getList($count, $hidden, 1);
        break;

    case 'get_list' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);

        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
    					   FROM   `users`
    					   WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }

        $db->setQuery("SELECT	  `outgoing_call`.id,
            					  `outgoing_call`.id,
            					   outgoing_call.date AS datee,
            					   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
            					   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
            					   department.`name` AS object,
            					  `user_info`.`name` ,
            					  `users1`.`name`,
            					  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id	
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id 
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=1 AND outgoing_call.status_id IN(0,1) $filter");
        
        $data  = $db->getList($count, $hidden, 1);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];

        break;
    case 'get_list4_0' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery("SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        LEFT JOIN  user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        LEFT JOIN `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=2 AND outgoing_call.status_id IN(0,1) $filter");
        
        $data  = $db->getList($count, $hidden,1);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'get_list4_1' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=2 AND outgoing_call.status_id=2 $filter");
        
        $data  = $db->getList($count, $hidden);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'get_list4_2' :
        $count   = $_REQUEST['count'];
        $hidden  = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user    = $_SESSION['USERID'];
        $group   = checkgroup($user);
        
        $start   = $_REQUEST['start'];
        $end     = $_REQUEST['end'];
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=2 AND outgoing_call.status_id = 3  AND outgoing_call.date BETWEEN '$start' AND '$end' $filter");
        
        $data  = $db->getList($count, $hidden);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'get_list5_0' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        LEFT JOIN  user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        LEFT JOIN `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=3 AND outgoing_call.status_id IN(0,1) $filter");
        
        $data  = $db->getList($count, $hidden,1);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'get_list5_1' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=3 AND outgoing_call.status_id = 2 $filter");
        
        $data  = $db->getList($count, $hidden);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'get_list5_2' :
        $count   = $_REQUEST['count'];
        $hidden  = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user    = $_SESSION['USERID'];
        $group   = checkgroup($user);
        
        $start   = $_REQUEST['start'];
        $end     = $_REQUEST['end'];
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                            FROM   `users`
                            WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=3 AND outgoing_call.status_id = 3  AND outgoing_call.date BETWEEN '$start' AND '$end' $filter");
        
        $data  = $db->getList($count, $hidden);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
        
        
    case 'get_list6_0' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=4 AND outgoing_call.status_id IN(0,1) $filter");
        
        $data  = $db->getList($count, $hidden,1);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
        
    case 'get_list6_1' :
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user = $_SESSION['USERID'];
        $group = checkgroup($user);
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=4 AND outgoing_call.status_id = 2 $filter");
        
        $data  = $db->getList($count, $hidden);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'get_list6_2' :
        $count   = $_REQUEST['count'];
        $hidden  = $_REQUEST['hidden'];
        $user_id = $_SESSION['USERID'];
        $user    = $_SESSION['USERID'];
        $group   = checkgroup($user);
        
        $start   = $_REQUEST['start'];
        $end     = $_REQUEST['end'];
        
        $filter = '';
        if ($group != 1) {
            $db->setQuery("SELECT  users.id as person_id
                           FROM   `users`
                           WHERE  `users`.`id` = $user_id");
            $res_row = $db->getResultArray();
            $filter = 'AND (outgoing_call.task_recipient_id ='.$res_row[result][0][person_id].')';
        }
        
        $db->setQuery(" SELECT	  `outgoing_call`.id,
                                  `outgoing_call`.id,
                                   outgoing_call.date AS datee,
                                   if(ISNULL(outgoing_call.incomming_call_id), outgoing_call.`phone`, incomming_call.`phone`) AS datee,
                                   IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS personal_id,
                                   if(ISNULL(outgoing_call.incomming_call_id), info_category.`name`, cat.`name` ) AS category,
                                   department.`name` AS object,
                                  `user_info`.`name` ,
                                  `users1`.`name`,
                                  `status`.call_status
                        FROM      `outgoing_call`
                        LEFT JOIN  incomming_call ON outgoing_call.incomming_call_id = incomming_call.id
                        LEFT JOIN `info_category` ON outgoing_call.cat_1_1 = info_category.id
                        LEFT JOIN `info_category` AS cat ON incomming_call.cat_1_1 = cat.id
                        LEFT JOIN `department` ON outgoing_call.branch_id = department.id
                        LEFT JOIN  user_info ON outgoing_call.recepient_id = user_info.user_id
                        JOIN       user_info AS `users1` ON outgoing_call.user_id=users1.user_id
                        JOIN      `status` ON outgoing_call.status_id  = `status`.id
                        WHERE 	   outgoing_call.actived=1 AND outgoing_call.outgoing_type_id=4 AND outgoing_call.status_id = 3  AND outgoing_call.date BETWEEN '$start' AND '$end' $filter");
        
        $data  = $db->getList($count, $hidden);
        $count = $db->getResultArray();
        
        $data['count'] = $count[count];
        
        break;
    case 'save_outgoing':
        $user_id      = $_SESSION['USERID'];
        $my_site       = $_REQUEST['site_id'];
        $out_status_id = $_REQUEST['out_status_id'];
        if ($task_id == '') {
            Addtask($hidde_outgoing_id);
            
            $db->setQuery("SELECT status.`name`
                           FROM   status
                           WHERE  actived = 1 AND id = '$out_status_id'");
            
            $res_status  = $db->getResultArray();
            $status_name = $res_status[result][0][name];
            
            $data = array('task_id' => $hidde_outgoing_id, 'task_type' => 2, 'status_name' => $status_name);
        }else{
            $db->setQuery("SELECT status_id,
                                  outgoing_type_id
                           FROM   outgoing_call
                           WHERE  actived = 1 AND id = '$hidde_outgoing_id'");
            
            $res_info      = $db->getResultArray();
            $out_status_id = $res_info[result][0][status_id];
            $out_type_id   = $res_info[result][0][outgoing_type_id];
            
            Savetask($hidde_outgoing_id);
            
            $data = array('out_status_id'	=> $out_status_id, 'out_type_id' => $out_type_id);
        }

        break;

    case 'get_responsible_person_add_page':
        $page = GetResoniblePersonPage();
        $data = array('page' => $page);

        break;

    case 'change_responsible_person':
        $letters = json_decode('[' . $_REQUEST['lt'] . ']');
        $responsible_person = $_REQUEST['rp'];

        ChangeResponsiblePerson($letters, $responsible_person);

        break;
        
    case 'cat_2':
        $page		= get_cat_1_1($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);
        
        break;
    case 'cat_3':
        $page		= get_cat_1_1_1($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);
        break;
    case 'delete_file':
        $delete_id = $_REQUEST['delete_id'];
        $inc_id   = $_REQUEST['inc_id'];
        $out_id   = $_REQUEST['out_id'];
        
        $db->setQuery("DELETE FROM file
				       WHERE id = $delete_id");
        $db->execQuery();
        if ($inc_id == '') {
            $where = "AND outgoing_id = $out_id";
        }else{
            $where = "AND incomming_call_id = $inc_id";
        }
        $db->setQuery("	SELECT  `name`,
                        		`rand_name`,
                        		`id`,
                                `file_date`
                		FROM 	`file`
                		WHERE   actived = 1 $where");
        $increm = $db->getResultArray();

        $data1 = array();

        foreach($increm[result] AS $increm_row) {
            $data1 .= '<tr style="border-bottom: 1px solid #85b1de;">
                           <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
				           <td style="width:100%; display:block;word-wrap:break-word;">' . $increm_row[name] . '</td>
				           <td style="width: 30px;"><button type="button" value="../../media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
        				   <td style="width: 30px;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
 					   </tr>';
        }



        $data = array('page' => $data1);

        break;
    case 'delete_file1':
        $delete_id = $_REQUEST['delete_id'];
        $edit_id   = $_REQUEST['edit_id'];
        
        $db->setQuery("DELETE FROM file
				       WHERE id = $delete_id");
        $db->execQuery();
        
        $db->setQuery("	SELECT  `name`,
                    			`rand_name`,
                    			`id`
            			FROM 	`file`
            			WHERE   `crm_id` = $edit_id");
        
        $increm = $db->getResultArray();
        $data1 = '';


        foreach ($increm[result] AS $increm_row){
            $data .='<tr style="border-bottom: 1px solid #CCC;">
    						            <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[name].'</td>
    						            <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="../../media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
    						            <td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
    						         </tr>';
        }

        $data = array('page' => $data1);

        break;
    case 'up_now1':
        $user    = $_SESSION['USERID'];
        $edit_id = $_REQUEST['edit_id'];
        
        if($rand_file != ''){
            $db->setQuery("INSERT INTO `file`
                    				  (`user_id`, `crm_id`, `name`, `rand_name`)
                    		    VALUES
                                      ('$user', '$edit_id', '$file_name', '$rand_file');");
            
            $db->execQuery();
        }

        $db->setQuery("SELECT  `name`,
							   `rand_name`,
							   `id`
					   FROM    `file`
                       WHERE   actived = 1 AND `crm_id` = '$edit_id'");
        
        $increm = $db->getResultArray();
        $data1 = '';

        foreach($increm[result] AS $increm_row)	{
            $data1 .='<tr style="border-bottom: 1px solid #CCC;">
                         <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
			             <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row['name'].'</td>
			             <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="../../media/uploads/file/'.$increm_row['rand_name'].'" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download1" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row['rand_name'].'"> </td>
			             <td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row['id'].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete1"></button></td>
			           </tr>';
        }
        $data = array('page' => $data1);

        break;

    case 'up_now':
        $inc_id  = $_REQUEST['inc_id'];
        $task_id = $_REQUEST['task_id'];
        $user    = $_SESSION['USERID'];
        
        if ($inc_id != '') {
            $where = "AND incomming_call_id = '$inc_id'";
        }elseif ($task_id != ''){
            $where = "AND outgoing_id = '$task_id'";
        }
        
        if ($rand_file != ''){
            $db->setQuery("INSERT INTO  `file`
            		                   (`user_id`, `file_date`, `incomming_call_id`, `outgoing_id`, `name`, `rand_name`)
            					VALUES
                                       ('$user', now(), '$inc_id', '$task_id', '$file_name', '$rand_file');");
            $db->execQuery();
        }

        $db->setQuery("SELECT `name`,
				        	  `rand_name`,
				        	  `id`,
                              `file_date`
		        	   FROM   `file`
		        	   WHERE   actived = 1 $where");
        
        $increm = $db->getResultArray();
        $data = array();

        foreach ($increm[result] AS $increm_row){
            $data .='<tr style="border-bottom: 1px solid #CCC;">
                                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
    						            <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[name].'</td>
    						            <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="../../media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
    						            <td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
    						         </tr>';
        }

        $data = array('page' => $data);

        break;
    case 'save_comment'  :
        $comment            =   $_REQUEST['comment'];
        $incomming_call_id  =   $_REQUEST['incomming_call_id'];
        if($incomming_call_id==''){
            $incomming_call_id= 0;
        }
        $hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
        $db->setQuery(" INSERT INTO `chat_comment`
                                   (`user_id`, `incomming_call_id`, `task_id`, `outgoing_call_id`, `parent_id`, `comment`, `datetime`, `active`)
                             VALUES
                                   ('$user_id', '$incomming_call_id','', '$hidde_outgoing_id', '', '$comment', NOW(),'1');");
        $db->execQuery();
        
        $page		= "";
        $data		= array('page'	=> $page);
        break;
    case 'update_comment' :
        $comment    = $_REQUEST['comment'];
        $id         = $_REQUEST['id'];
        
        $db->setQuery("UPDATE `chat_comment`
                          SET `comment`    = '$comment'
                       WHERE  `id`        = $id");
        $db->execQuery();
        break;
    case 'delete_comment'  :
        $comment_id=$_REQUEST['comment_id'];
        
        $db->setQuery("UPDATE `chat_comment`
                          SET `active` = 0
                       WHERE  `id` = $comment_id");
        $db->execQuery();
        break;
    case 'get_add_question':
        $incomming_id      = $_REQUEST['incomming_call_id'];
        $hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
        $page		= Getquestion($incomming_id,$hidde_outgoing_id);
        $data		= array('page'	=> $page);
        
        break;
    case 'open_new_sms_dialog':
        
        $page		= get_new_sms_dialog();
        $data		= array('page'	=> $page, 'type' => $type);
        
        
        
        break;
    case 'get_sms_template_dialog':
        
        $data = array("html" => sms_template_dialog());
        
        break;
    case 'phone_directory':
        
        $count = 		$_REQUEST['count'];
        
        $db->setQuery("SELECT id,
							  id,
							  name,
							  CONCAT('<button class=\"download_shablon\" sms_id=\"', id ,'\" data-message=\"', message ,'\" >არჩევა</button>')
						FROM  sms
						WHERE actived = 1");
        
        $data = $db->getList($count, $hidden);
        
        break;
    case 'get_list_mail':
        $count             = $_REQUEST['count'];
        $hidden            = $_REQUEST['hidden'];
        $incomming_call_id = $_REQUEST['incomming_id'];
        $hidde_outgoing_id = $_REQUEST['hidde_outgoing_id'];
        
        if ($incomming_call_id == '') {
            $where = "outgoing_id = '$hidde_outgoing_id'";
        }else {
            $where = "incomming_call_id = '$incomming_call_id'";
        }
        $db->setQuery(" SELECT  id,
                               `date`,
                                CONCAT('to: ',`address`,'<br>cc: ',cc_address,'<br> bcc: ',bcc_address) as address,
                               `body`,
                                if(`status`=3,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
                        FROM   `sent_mail`
                        WHERE   $where AND `status` != 1");
        
        $data = $db->getList($count, $hidden);
        break;
    case '':
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
 * ******************************
 */
function sms_template_dialog() {
    
    return '
		<div class="sms-template-table-wrapper">
			<table class="display" id="sms_template_table" style="width: 100%;">
				<thead>
					<tr id="datatable_header">
						<th>ID</th>
						<th style="width: 10%;" >№</th>
						<th style="width:  75%;">დასახელება</th>
						<th style="width:  15%">ქმედება</th>
					</tr>
				</thead>
				<thead>
					<tr class="search_header">
						<th class="colum_hidden">
							<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
						</th>
						<th>
							<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
						<th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
						</th>
						<th>
        
						</th>
					</tr>
				</thead>
			</table>
		<div>';
    
}

function get_new_sms_dialog($type="phone") {
    
    // define adressee input structures
    $adressee_inputs = $type == 'phone' ? '<div class="new-sms-row">
												<label for="smsAddresseeByPhone">ტელეფონის ნომერი</label>
												<div class="new-sms-input-holder">
													<input type="text" id="smsAddresseeByPhone">
												</div>
											</div>'
        : '<div class="new-sms-row grid">
												<div class="nsrg-col">
													<label for="smsAddresseeByPIN">PIN კოდი</label>
													<div class="new-sms-input-holder">
														<input type="text" id="smsAddresseeByPIN">
													</div>
												</div>
												<div class="nsrg-col">
													<label for="smsAddresseeByIDNum">პირადი ნომერი</label>
													<div class="new-sms-input-holder">
														<input type="text" id="smsAddresseeByIDNum">
													</div>
												</div>
											</div>';
        
        //new sms send dialog content
        return '<fieldset style="display:block;" class="new-sms-send-container">
            
				<!-- label for fieldset -->
				<legend>SMS</legend>
            
				<!-- row unit -->
				<div class="new-sms-row">
					<button id="smsTemplate" data-button="jquery-ui-button">შაბლონი</button>
				</div>
            
				<!-- row unit -->
				'.$adressee_inputs.'
				    
				<!-- row unit -->
				<div class="new-sms-row">
					<label for="smsText">ტექსტი</label>
					<div class="new-sms-input-holder">
						<textarea id="newSmsText"></textarea>
					</div>
				</div>
				    
				<!-- row unit -->
				<div class="new-sms-row">
					<input type="text" id="smsCharCounter" data-limit="150" value="0/150">
					<button id="sendNewSms" data-button="jquery-ui-button">გაგზავნა</button>
				</div>
				    
			</fieldset>';
        
}

function getShablon(){
    global $db;
    $db->setQuery("SELECT 	sms.id,
        					sms.`name`,
        					sms.`message`
					FROM 	sms
					WHERE 	sms.actived=1 ");
    
    $data_count = $db->getNumRow();
    
    if($data_count > 0) {
        
        $data = '<table id="box-table-b1">
            		<tr class="odd">
            			<th style="width: 26px;">#</th>
            			<th style="width: 160px;">დასახელება</th>
            			<th>ქმედება</th>
            		</tr> ';
        
        $req=$db->getResultArray();
        foreach ($req[result] AS $res3){
            $data .= '<tr class="odd">
            				<td>' . $res3[id] . '</td>
            				<td style="width: 30px !important;">' . $res3['name'] . '</td>
            				<td style="font-size: 10px !important;">
            					<button style="width: 45px;" class="download_shablon" sms_id="' . $res3['id'] . '" data-message="' . $res3['message'] . '">არჩევა</button>
            				</td>
        			   </tr>';
            
        }
        $data.='</table>';
    } else {
        $data = '<div class="empty-sms-shablon">
					<p>შაბლონთა ჩამონათვალი ცარიელია</p>
				 </div>';
    }
    return $data;
}
function Get_task($id){
    global $db;
    $db->setQuery("SELECT id FROM `task` WHERE actived=1 and crm_id='$id'");
    $task_id_q = $db->getResultArray();
    $task_id   = $task_id_q[result][0]['id'];

    $db->setQuery("SELECT id, 
                          task_type_id,
                          department_id as task_department_id,
                          responsible_user_id as persons_id, 
                          priority_id, 
                          control_user_id,
                          phone as task_phone,
                          problem_comment 
                   FROM  `task` 
                   WHERE  actived=1 and id='$task_id'");
    $task_q = $db->getResultArray();
    return $task_q[result][0];

}

function get_from_file($id){
    global $db;
    $db->setQuery("	SELECT  id,
                            `name`,
							`rand_name`
					FROM 	`file`
					WHERE   `crm_id` = '$id'");
    
    $increm = $db->getResultArray();
    return $increm[result];
}

function GetMainProduct($id){
    global $db;
    $db->setQuery(" SELECT  arb.id,
                            ard.set_time,
                            ar.`name`,
                            ar.`code`,
                            arb.phone_number,
                            ard.`comment`,
                            arb.actived_status,
                            ar.id as request_id
                    from    autocall_request ar
                    JOIN    autocall_request_details ard on ard.request_id=ar.id
                    JOIN    autocall_request_base arb on arb.request_id=ard.id
                    WHERE   arb.actived=1 and ard.actived=1 and ar.actived=1 and arb.id='$id'");
    
    $res = $db->getResultArray();
    return $res[result][0];

}

function Getproduct($product_id, $tn){
    global $db;

    $db->setQuery("SELECT  * 
                   FROM   $tn
                   WHERE  id = $product_id");
    
    $res = $db->getResultArray();
    return $res[result][0];
}

function get_page_auto_dialer($res, $res1, $request_base_id, $str){
    $data = '';
    $data.='<div  id="dialog-form">
                <fieldset style="display:block; margin-right: 1px" >
                <legend>სტატუსის ფილტრი</legend>
                    <table class="dialog-form-table" style="width: 100%;">
                        <tr>
                            <td>სტატუსი</td>
                            <td>ქვე სტატუსი</td>
                            
                        </tr>
    			        <tr>
        			        <td style="width: 50%">
        			        <select onchange="sub_type_call(this.value)"  id="first_type" class="idle" style="width: 95%"  readonly>' . get_nick_first($res['tree_id']) . '</select>
        					
                            </td>
                            <td id="for_sub_change">
        			             <select  id="sub_type" class="idle" style="width: 95%"  readonly>' . get_nick($res['tree_id']) . '</select>
        					</td>
                        </tr>
    			
                    </table>	
                </fieldset>
                <fieldset style=" margin-right: 1px">
                <legend>ძირითადი ინფორმაცია</legend>
                    <table class="dialog-form-table" style="width: 100%;">
                        <tr>
                            <td>თარიღი</td>
                            <td>კამპანიის დასახელება</td>
                            <td>კამპანიის კოდი</td>
                        </tr>
        			    <tr>
        			        <td>
        						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="set_time"   value="' . $res1['set_time'] . '"  />
        					</td>
                           <td>
        						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="name"   value="' . $res1['name'] . '" />
        					</td>
        					<td>
        						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="code"   value="' . $res1['name'] . '" />
        					</td>
                        </tr>';


    $data .= get_number_tr($request_base_id, $res1['phone_number'], $res1['request_id']);


    $data .= '<td>კომენტარი</td>
				
				<tr>
				 <td colspan="3">
                  <textarea disabled="disabled" id="note" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res1['comment'] . ' </textarea>
                  </td>
				
                </tr>
                <td>შიდა კომენტარი</td>
                <tr>
				  <td colspan="3">
                  <textarea   id="comment" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res['comment'] . ' </textarea>
                  </td>
				
                </tr>
                
			 <input type="hidden" id="request_base_id" value="' . $request_base_id . '" />
			 
			 
   </table>	
   </fieldset>
   </div>   
   
   </div>';
    return $data;
}
function get_page_manual_out($res, $res1, $request_base_id, $str){
    $data = '';
    $data.='<div  id="dialog-form">
    <fieldset style="display:block; margin-right: 1px" >
    <legend>სტატუსის ფილტრი</legend>
    <table class="dialog-form-table" style="width: 100%;">
    
               
                <tr>
                    <td>სტატუსი</td>
                    <td>ქვე სტატუსი</td>
                    
                </tr>
			
			    <tr>
			        <td style="width: 50%">
			        <select disabled="disabled" onchange="sub_type_call(this.value)"  id="first_type" class="idle" style="width: 95%"  readonly>' . get_nick_first($res['tree_id']) . '</select>
					
                    </td>
                    <td id="for_sub_change">
			        <select disabled="disabled"  id="sub_type" class="idle" style="width: 95%"  readonly>' . get_nick($res['tree_id']) . '</select>
					
                    </td>
                   
										
				</tr>
			
   </table>	
   </fieldset>
   <fieldset style=" margin-right: 1px">
    <legend>ძირითადი ინფორმაცია</legend>
    <table class="dialog-form-table" style="width: 100%;">
    
               
                <tr>
                
                    <td>თარიღი</td>
                    <td>კამპანიის დასახელება</td>
                    <td>კამპანიის კოდი</td>
                    
                   
                </tr>
			
			    <tr>
			        <td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="set_time"   value="' . $res1['set_time'] . '"  />
					</td>
                   <td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="name"   value="' . $res1['name'] . '" />
					</td>
					<td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="code"   value="' . $res1['name'] . '" />
					</td>
                   
										
				</tr>
    ';


    $data .= get_number_tr($request_base_id, $res1['phone_number'], $res1['request_id']);


    $data .= '<td>კომენტარი</td>
				
				<tr>
				 <td colspan="3">
                  <textarea disabled="disabled" id="note" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res1['comment'] . ' </textarea>
                  </td>
				
                </tr>
                <td>შიდა კომენტარი</td>
                <tr>
				  <td colspan="3">
                  <textarea  disabled="disabled"  id="comment" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res['comment'] . ' </textarea>
                  </td>
				
                </tr>
                
			 <input type="hidden" id="request_base_id" value="' . $request_base_id . '" />
			 
			 
   </table>	
   </fieldset>
   </div>   
   
   </div>';
    return $data;
}

function get_page_manual($res_task,$res, $res1, $request_base_id, $increm)
{
    $data = '';
    $data = '
    <div id="side_menu" style="border: solid 1px #CCC;float: right; width: 615px;;margin-left: 2px; background: #EEE; color: #FFF;margin-bottom:2px;">
			<span class="info" style="margin-left: 10px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'info\')"><img style="border-bottom: 2px solid #333;filter: brightness(0.1);padding-bottom: 3px;" src="media/images/icons/info_blue.png" alt="24 ICON" height="20" width="20" title="ინფო"></span>
			<span class="task" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'task\')"><img style="padding-bottom: 3px;" src="media/images/icons/task_blue.png" alt="24 ICON" height="20" width="20" title="დავალება"></span>
			<span class="file" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'file\')"><img style="padding-bottom: 3px;" src="media/images/icons/file_blue.png" alt="24 ICON" height="20" width="20" title="ფაილი"></span>
			<span class="sms" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'sms\')"><img style="padding-bottom: 3px;" src="media/images/icons/sms_blue.png" alt="24 ICON" height="20" width="20" title="ფაილი"></span>
			<span class="crm" style="margin-left: 15px;display: block;padding: 8px 0px;  cursor: pointer; float:left;" onclick="show_right_side(\'crm\')"><img style="padding-bottom: 3px;" src="media/images/icons/folder_blue.png" alt="24 ICON" height="20" width="20" title="CRM"></span>
			<!--span class="history" style="margin-left: 10px;width: 70px;display: block;padding: 10px 5px;  cursor: pointer; float:left;" onclick="show_right_side(\'history\')"><img style="padding-bottom: 3px;" src="media/images/icons/history.png" alt="24 ICON" height="24" width="24" title="ინფო"><div style="text-align: center;">ჩატის ისტორია</div></span-->
			
	    </div>
  
   <div style="width: 615px;float: right;" id="right_side">
     <div class="tabs" id="info">        
				<div  id="dialog-form">
    <fieldset style="display:block; margin-right: 1px" >
    <legend>სტატუსის ფილტრი</legend>
    <table class="dialog-form-table" style="width: 100%;">
    
               
                <tr>
                    <td>სტატუსი</td>
                    <td>ქვე სტატუსი</td>
                    
                </tr>
			
			    <tr>
			        <td style="width: 50%">
			        <select onchange="sub_type_call(this.value)"  id="first_type" class="idle" style="width: 95%"  readonly>' . get_nick_first($res['tree_id']) . '</select>
					
                    </td>
                    <td id="for_sub_change">
			        <select  id="sub_type" class="idle" style="width: 95%"  readonly>' . get_nick($res['tree_id']) . '</select>
					
                    </td>
                   
										
				</tr>
			
   </table>	
   </fieldset>
   <fieldset style=" margin-right: 1px">
    <legend>ძირითადი ინფორმაცია</legend>
    <table class="dialog-form-table" style="width: 100%;">
    
               
                <tr>
                
                    <td>თარიღი</td>
                    <td>კამპანიის დასახელება</td>
                    <td>კამპანიის კოდი</td>
                    
                   
                </tr>
			
			    <tr>
			        <td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="set_time"   value="' . $res1['set_time'] . '"  />
					</td>
                   <td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="name"   value="' . $res1['name'] . '" />
					</td>
					<td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="code"   value="' . $res1['name'] . '" />
					</td>
                   
										
				</tr>
			';


    $data .= get_number_tr($request_base_id, $res1['phone_number'], $res1['request_id']);


    $data .= '<td>კომენტარი</td>
				
				<tr>
				 <td colspan="3">
                  <textarea disabled="disabled" id="note" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res1['comment'] . ' </textarea>
                  </td>
				
                </tr>
                <td>შიდა კომენტარი</td>
                <tr>
				  <td colspan="3">
                  <textarea   id="comment" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res['comment'] . ' </textarea>
                  </td>
				
                </tr>
                
			 <input type="hidden" id="request_base_id" value="' . $request_base_id . '" />
			 <input type="hidden" id="actived_status" value="' . $res1['actived_status']. '" />
			 
   </table>	
   </fieldset>
   </div>   
   
   </div>
   <div  id="dialog-form">
   <div class="tabs" style="display:none;"  id="task">
      </div>
   </div>
   
   <div class="tabs"  style="display:none;" id="record">
       <div id="dialog-form">
       <fieldset style="display:none; height: auto;" id="record" >
           <div style="margin-top: 10px;">
                <audio controls style="margin-left: 145px;" id="auau">
                  <source src="" type="audio/wav">
                  Your browser does not support the audio element.
                </audio>
           </div>
           <fieldset style="display:block !important; margin-top: 10px;">
                <legend>შემომავალი ზარი</legend>
	            <table style="margin: auto;">
	               <tr>
	                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                	   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                       <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ოპერატორი</td>
                       <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ვინ გათიშა</td>
                	   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
            	    </tr>
	                '.GetRecordingsSection($res).'
        	    </table>
            </fieldset>
      </fieldset>

       
   
      </div>
    </div>
    
    <div class="tabs" style="display:none;" id="file">
      <div id="dialog-form">
    <fieldset>
                    <legend>ფაილი</legend>

					<table style="float: right;text-align: center; border: 1px solid #ccc; width: 100%;">
					<tr>
						<td>
							<div class="file-uploader">
								<input id="choose_file3" type="file" name="choose_file3" class="input" style="display: none;">
								<button id="choose_button3" style="width: 100%;" class="center center ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"><span class="ui-button-text">აირჩიეთ ფაილი</span></button>
								<input id="hidden_inc" type="text" value="'. increment1('incomming_call') .'" style="display: none;">
							</div>
						</td>
					</tr>
				</table>
			     <table style="float: right;border: 1px solid #ccc;width: 100%;margin-top: 2px;text-align: center;">
			          <tr>
			           <td colspan="3">მიმაგრებული ფაილი</td>
			          </tr>
				</table>
				<table id="file_div" style="float: right; border: 1px solid #ccc; width: 100%;margin-top: 2px; text-align: center;">';

    foreach($increm AS $increm_row)	{
        $data .='
		        <tr style="border-bottom: 1px solid #CCC;">
		          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row['name'].'</td>
		          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/'.$increm_row['rand_name'].'" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download1" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row['rand_name'].'"> </td>
		          <td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row['id'].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete1"></button></td>
		        </tr>';
    }

    $data .= '
	 		</table>
				</fieldset>
      </div>
    
    </div>
    <div class="tabs" id="sms" style="display: none">
      <div id="dialog-form">
           <fieldset>
					<legend>SMS</legend>
					<div class="margin_top_10">           
					<div id="button_area">
						<button id="add_sms_phone" class="jquery-ui-button new-sms-button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" data-type="phone" aria-disabled="false" role="button"><span class="ui-button-text" style="margin-top: -3px">ტელეფონის ნომრით</span></button>
						<button id="add_sms_ip" class="jquery-ui-button new-sms-button-pi ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" data-type="ip"><span class="ui-button-text" style="margin-top: -3px">PIN კოდით</span></button>
					</div>
					<table class="display" id="table_sms" >
						<thead>
							<tr id="datatable_header">
								<th>ID</th>
								<th style="width: 20%;">თარიღი</th>
								<th style="width: 20%;">ადრესატი</th>
								<th style="width: 40%;">ტექსტი</th>
								<th style="width: 20%;">სტატუსი</th>
							</tr>
						</thead>
						<thead>
							<tr class="search_header">
								<th class="colum_hidden">
								<input type="text" name="search_id" value="ფილტრი" class="search_init" />
								</th>
								<th>
									<input type="text" name="search_number" value="ფილტრი" class="search_init" />
								</th>
								<th>
									<input type="text" name="search_date" value="ფილტრი" class="search_init" />
								</th>
								<th>
									<input type="text" name="search_date" value="ფილტრი" class="search_init" />
								</th>
								<th>
									<input type="text" name="search_date" value="ფილტრი" class="search_init" />
								</th>
							</tr>
						</thead>
					</table>
					</div>
				</fieldset>
      </div>
    </div>
    
    <div id="crm" class="tabs" style="display: none">
<div id="dialog-form">
<fieldset style="height: 450px; overflow-y: auto">

<legend>ფაილები</legend>
   <div class="input_fild">
           
        
            </div>
            <div class="display" style="overflow: auto; position: absolute; height: 70%;  width: 90%">
         <table class="display" id="example_all_file" style="margin-right: 10px; width: 100%">
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 60%">ფაილის სახელი</th>
                <th style="width: 40%">ტიპი</th>
               
                
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-file" name="check-all-file"/>
                        <label for="check-all-file"></label>
                    </div>
                </th>
            </tr>
            </thead>
           
        </table>
        </div>
 
</div>
</fieldset>
</div>


</div>';


    return $data;
}

function increment1($table){

    $result   		= mysql_query("SHOW TABLE STATUS LIKE '$table'");
    $row   			= mysql_fetch_array($result);
    $increment   	= $row['Auto_increment'];
    $next_increment = $increment+1;
    mysql_query("ALTER TABLE '$table' AUTO_INCREMENT=$next_increment");

    return $increment;
}

function get_number_tr($request_base_id, $phone_number, $request_id){
    global $db;
    $data = '';
    $c = 0;
    $c2 = 1;
    $data1 = '';
    $data2 = '';
    $data1_sum = '';
    $data2_sum = '';

    $count = 1;
    
    $db->setQuery("SELECT * FROM autocall_request_base WHERE id='$request_base_id'");
    $res_base = $db->getResultArray();
    $res_base = $res_base[result][0];
    
    $db->setQuery("SELECT `name` FROM autocall_request_voice WHERE request_id='$request_id' and actived=1 ORDER BY position");
    $res_voice = $db->getResultArray();
    
    foreach($res_voice[result] AS $res) {

        if ($c == 0) {
            $c++;
            $c2++;
            $data1 = '<tr><td>ნომერი</td>';
            $data2 = '<tr><td>
						<input disabled="disabled" class="idle" type="text" style="width: 95%;" id="phone_number"   value="' . $phone_number . '" />
					</td>';


        }
        if ($c2 == 3) {
            $data1 .= '<td>' . $res['name'] . '</td></tr>';
            $data2 .= '<td><input disabled="disabled" class="idle" type="text" style="width: 95%;" id="number_' . $count . '"   value="' . $res_base['number_' . $count] . '" /></td></tr>';

            $data1_sum = $data1;
            $data2_sum = $data2;
            $data .= $data1_sum .= $data2_sum;
            $data1 = '<tr>';
            $data2 = '<tr>';
            $c2 = 1;

        } else {
            $c2++;
            $data1 .= '<td>' . $res['name'] . '</td>';
            $data2 .= '<td><input disabled="disabled" class="idle" type="text" style="width: 95%;" id="number_' . $count . '"   value="' . $res_base['number_' . $count] . '" /></td>';


        }
        $count++;

    }

    if ($c2 != 1) {


        $data .= $data1 . '</tr>' . $data2 . '</tr>';
    }


    return $data;
}

function get_nick($nick_id){
    global $db;
    $data = '';

    $db->setQuery("SELECT id,parent_id, name FROM add_page_tree WHERE  actived=1  and parent_id <> 0 and id='$nick_id'");
    $req1 = $db->getResultArray();
    
    $db->setQuery("SELECT id,parent_id, name FROM add_page_tree WHERE  actived=1  and parent_id =" . $req1[result][0][parent_id]);
    $req = $db->getResultArray();
    
    foreach($req[result] AS $res) {
        if ($res['id'] == $nick_id) {
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }


    return $data;
}

function get_nick_first($nick_id){
    global $db;
    $data = '';

    $db->setQuery("SELECT id,parent_id, name FROM add_page_tree WHERE  actived=1 ORDER BY parent_id desc");
    $req = $db->getResultArray();
    $r = '';
    foreach($req[result] AS $res) {
        if ($res['id'] == $nick_id) {
            $rs = get_main_tree($res['parent_id']);
            $data .= '<option value="' . $res['parent_id'] . '" selected="selected">' . $rs . '</option>';
            $r = $res['parent_id'];
        } else if ($res['parent_id'] == 0 && $r != $res['id']) {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function get_main_tree($id){
    
    global $dbtre;
    $dbtre = new dbClass();
    $dbtre->setQuery("SELECT id, name FROM add_page_tree WHERE  actived=1  and id='$id'");
    
    $r = $dbtre->getResultArray();
    return $r[result][0]['name'];
}

function checkgroup($user){
    global $db;
    
    $db->setQuery("SELECT users.group_id
				   FROM   users
				   WHERE  users.id = $user");
    
    $res = $db->getResultArray();
    return $res[result][0]['group_id'];

}


function Addtask($hidde_outgoing_id){
    global $db;
    $user                = $_SESSION['USERID'];
    $incomming_call_id   = $_REQUEST['incomming_call_id'];
    $incomming_cat_1     = $_REQUEST['incomming_cat_1'];
    $incomming_cat_1_1   = $_REQUEST['incomming_cat_1_1'];
    $incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
    $phone               = $_REQUEST['phone'];
    $s_u_user_id         = $_REQUEST['s_u_user_id'];
    $s_u_mail            = $_REQUEST['s_u_mail'];
    $s_u_pid             = $_REQUEST['s_u_pid'];
    $s_u_name            = $_REQUEST['s_u_name'];
    $client_sex          = $_REQUEST['client_sex'];
    $s_u_status          = $_REQUEST['s_u_status'];
    $source_id           = $_REQUEST['source_id'];
    $out_status_id       = $_REQUEST['out_status_id'];
    $out_recepient_id    = $_REQUEST['out_recepient_id'];
    $call_content        = htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
    $call_description    = htmlspecialchars($_REQUEST['call_description'], ENT_QUOTES);
    $out_start_date      = $_REQUEST['out_start_date'];
    $out_end_date        = $_REQUEST['out_end_date'];
    $task_branch_id      = $_REQUEST['task_branch_id'];
    $my_site			 = $_REQUEST['site_id'];
    
    $db->setQuery("INSERT INTO `outgoing_call`
                           SET `id`                   = '$hidde_outgoing_id', 
        					   `user_id`              = '$user', 
        					   `date`                 =  NOW(), 
                               `start_date`           =  NOW(),
        					   `out_start`            = '$out_start_date', 
        					   `out_end`              = '$out_end_date', 
        					   `phone`                = '$phone', 
        					   `recepient_id`         = '$out_recepient_id',
        					   `client_user_id`       = '$s_u_user_id',
        					   `client_pid`           = '$s_u_pid',
        					   `client_name`          = '$s_u_name',
        					   `client_mail`          = '$s_u_mail',
        					   `client_satus`         = '$s_u_status',
        					   `client_sex`           = '$client_sex', 
        					   `cat_1`                = '$incomming_cat_1', 
        					   `cat_1_1`              = '$incomming_cat_1_1',
        					   `cat_1_1_1`            = '$incomming_cat_1_1_1',
        					   `status_id`            = '$out_status_id',
        					   `branch_id`            = '$task_branch_id', 
        					   `source_id`            = '$source_id', 
        				       `outgoing_comment`     = '$call_description',
        					   `call_content`         = '$call_content',
        					   `actived`              = '1'");
    
    $db->execQuery();
    $site_array = explode(",",$my_site);
    
    $db->setQuery("DELETE FROM outgoing_call_site WHERE outgoing_call_id = '$hidde_outgoing_id'");
    $db->execQuery();
    
//    foreach ($site_array AS $site_id){
//        $db->setQuery("INSERT INTO outgoing_call_site (outgoing_call_id, site_id) values ('$hidde_outgoing_id', $site_id)");
//        $db->execQuery();
//    }

}

function Savetask($hidde_outgoing_id){
    global $db;
    global $error;
    $user = $_SESSION['USERID'];
    
    $db->setQuery("SELECT users.id
			       FROM  `users`
			       WHERE `users`.`id` = $user");
    
    $res_row = $db->getResultArray();

    //if ($res_row[result][0][id] == $persons_id){

        $c_date              = date('Y-m-d H:i:s');
        $incomming_call_id   = $_REQUEST['incomming_call_id'];
        $incomming_cat_1     = $_REQUEST['incomming_cat_1'];
        $incomming_cat_1_1   = $_REQUEST['incomming_cat_1_1'];
        $incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
        $phone               = $_REQUEST['phone'];
        $s_u_user_id         = $_REQUEST['s_u_user_id'];
        $s_u_mail            = $_REQUEST['s_u_mail'];
        $s_u_pid             = $_REQUEST['s_u_pid'];
        $s_u_name            = $_REQUEST['s_u_name'];
        $client_sex          = $_REQUEST['client_sex'];
        $s_u_status          = $_REQUEST['s_u_status'];
        $source_id           = $_REQUEST['source_id'];
        $out_status_id       = $_REQUEST['out_status_id'];
        $out_recepient_id    = $_REQUEST['out_recepient_id'];
        $call_content        = htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
        $call_description    = htmlspecialchars($_REQUEST['call_description'], ENT_QUOTES);
        $out_start_date      = $_REQUEST['out_start_date'];
        $out_end_date        = $_REQUEST['out_end_date'];
        $task_branch_id      = $_REQUEST['task_branch_id'];
        $my_site			 = $_REQUEST['site_id'];
        
        $db->setQuery("UPDATE `outgoing_call` 
                          SET `start_date`           = NOW(),
                              `out_start`            = '$out_start_date',
                              `out_end`              = '$out_end_date',
                              `asterisk_outgoing_id` = '',
                              `phone`                = '$phone',
                              `recepient_id`         = '$out_recepient_id',
                              `client_user_id`       = '$s_u_user_id',
                              `client_pid`           = '$s_u_pid',
                              `client_name`          = '$s_u_name',
                              `client_mail`          = '$s_u_mail',
                              `client_satus`         = '$s_u_status',
                              `client_sex`           = '$client_sex',
                              `cat_1`                = '$incomming_cat_1',
                              `cat_1_1`              = '$incomming_cat_1_1',
                              `cat_1_1_1`            = '$incomming_cat_1_1_1',
                              `status_id`            = '$out_status_id',
                              `branch_id`            = '$task_branch_id',
                              `source_id`            = '$source_id',
                              `outgoing_comment`     = '$call_description',
                              `call_content`         = '$call_content',
                              `actived`              = 1 
                        WHERE `id`                   = '$hidde_outgoing_id';");
        
        $db->execQuery();
        $site_array = explode(",",$my_site);
        
        $db->setQuery("DELETE FROM outgoing_call_site WHERE outgoing_call_id = '$hidde_outgoing_id'");
        $db->execQuery();
        
//        foreach ($site_array AS $site_id){
//            $db->setQuery("INSERT INTO outgoing_call_site (outgoing_call_id, site_id) values ('$hidde_outgoing_id', $site_id)");
//            $db->execQuery();
//        }

//     }else{
//         $error = 'თქვენ არ გაქვთ შეცვლის უფლება';
//     }
}

function web_site($id,$check_site){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        if($check_site == 1){
            $db->setQuery("SELECT id
                           FROM  `incoming_call_site`
                           WHERE incoming_call_site.incomming_call_id = '$id' AND site_id = '$value[id]'");
        }else{
            $db->setQuery("SELECT id
                           FROM  `outgoing_call_site`
                           WHERE outgoing_call_site.outgoing_call_id = '$id' AND site_id = '$value[id]'");
        }
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getclientstatus($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">აქტიური</option>
                  <option value="2">პასიური</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2" selected="selected">პასიური</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2">პასიური</option>';
    }
    
    return $data;
}

function get_s_u_sex($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2" selected="selected">მამრობითი</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }
    
    return $data;
}


function Getsource($object_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT 	`id`,
                            `name`
				    FROM 	`source`
				    WHERE 	 actived=1");
    
    
    $data = $db->getSelect($object_id);
    
    return $data;
}
function Gettask_type($task_type_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`, `name`
				   FROM   `task_type`
				   WHERE   actived=1 AND id = 1");


    $data = $db->getSelect($task_type_id);

    return $data;
}

function get_recepient($id){
    global $db;
    $data = '';
    $db->setQuery(" SELECT  users.`id`,
                    	   `name`
                    FROM   `users`
                    JOIN    user_info ON users.id = user_info.user_id
                    WHERE   actived = 1");
    
    $data = $db->getSelect($id);
    
    return $data;
}

function get_out_status($id){
    global $db;
    $data = '';
    $db->setQuery(" SELECT id,
                          `name` 
                    FROM  `status`
                    WHERE  actived = 1");
    $data = $db->getSelect($id);
    
    return $data;
}

function get_cat_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $data = $db->getSelect($id);
    
    return $data;
    
}

function get_cat_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `info_category`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function get_cat_1_1_1($id,$child_id){
    
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="'.$res['id'].'" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="'.$res['id'].'">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function Getdepartment($id){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`, `name`
				   FROM `department`
				   WHERE actived=1");
    
    
    $data = $db->getSelect($id);
}

function get_select($id,$table,$row='name',$where='actived = 1'){
    global $db;
    $data = '<option value="0" >-------------</option>';
    
    $db->setQuery("SELECT `id`,
                         `$row`
                   FROM   `$table`
                   WHERE   $where");
    
    $data = $db->getSelect($id);
    
    return $data;
}


function Getincomming($task_id){
    global $db;
    $db->setQuery("SELECT	  outgoing_call.id,
                			  outgoing_call.incomming_call_id,
                			  outgoing_call.asterisk_outgoing_id,
        					  incomming_call.asterisk_incomming_id,
                              IFNULL(outgoing_call.incomming_call_id,outgoing_call.id) AS `for_site`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.cat_1,incomming_call.cat_1) AS `cat_1`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.cat_1_1,incomming_call.cat_1_1) AS `cat_1_1`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.cat_1_1_1,incomming_call.cat_1_1_1) AS `cat_1_1_1`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.phone,incomming_call.phone) AS `phone`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_user_id,incomming_call.client_user_id) AS `client_user_id`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_mail,incomming_call.client_mail) AS `client_mail`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_pid,incomming_call.client_pid) AS `client_pid`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_name,incomming_call.client_name) AS `client_name`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_sex,incomming_call.client_sex) AS `client_sex`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.client_satus,incomming_call.client_satus) AS `client_satus`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.source_id,incomming_call.source_id) AS `source_id`,
        					  IF(ISNULL(outgoing_call.incomming_call_id),outgoing_call.call_content,incomming_call.call_content) AS `call_content`,
        					  outgoing_call.status_id AS `status_id`,
        					  outgoing_call.branch_id,
        					  outgoing_call.recepient_id,
        					  outgoing_call.out_start,
                              outgoing_call.date,
        					  outgoing_call.out_end,
                              user_info.`name` AS `operator`,
        					  outgoing_call.outgoing_comment,
                              incomming_call.lid_comment,
                              incomming_call.lid 
                    FROM 	  outgoing_call
                    LEFT JOIN incomming_call  ON incomming_call.id = outgoing_call.incomming_call_id
                    LEFT JOIN user_info ON user_info.user_id = outgoing_call.user_id
                    WHERE     outgoing_call.id = '$task_id'");
    
    $res = $db->getResultArray();

    return $res[result][0];
}


function GetPage($res = '', $number){
    $lid_comment_hide = "display:none;";
    if($res[lid]==1){
        $lid_comment_hide = '';
    }
    global $db;
    $data = '';
    
    if (is_null($res[incomming_call_id])) {

        $db->setQuery("	SELECT  `name`,
                    	        `rand_name`,
                    	        `id`,
                                `file_date`
            	        FROM 	`file`
            	        WHERE   `outgoing_id` = '$res[id]'");
        $increm = $db->getResultArray();
    } else {

        $db->setQuery("	SELECT  `name`,
                    	        `rand_name`,
                    	        `id`
            	        FROM 	`file`
                        WHERE   `incomming_call_id` = '$res[incomming_call_id]'");
        $increm = $db->getResultArray();
    }

    if ($res[id]=='') {
        $db->setQuery("INSERT INTO outgoing_call SET user_id = ''");
        $db->execQuery();
        $hidde_id = $db->getLastId();
        $db->setQuery("DELETE FROM outgoing_call WHERE id = $hidde_id");
        $db->execQuery();
    }else{
        $hidde_id = $res[id];
    }
    
    $disable = '';
    if ($res['incomming_call_id'] != '' || !empty($res['incomming_call_id']) || $res['incomming_call_id'] != null) {
        $disable='disabled="disabled"';
        $check_site=1;
    }
    
    
    $data .= '<div id="dialog-form">
    		    <div id="side_menu" style="border: solid 1px #CCC; float: right; width: 653px; margin-left: 2px; background: #EEE; color: #FFF; margin-bottom:2px;">
        			<span class="info" style="margin-left: 10px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side1(\'info\')"><img style="border-bottom: 2px solid #333;filter: brightness(0.1);padding-bottom: 3px;" src="media/images/icons/info_blue.png" alt="24 ICON" height="20" width="20" title="ინფო"></span>
                    <span class="task" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side1(\'task\')"><img style="padding-bottom: 3px;" src="media/images/icons/task_blue.png" alt="24 ICON" height="20" width="20" title="დავალება"></span>
                    
                    <span class="sms" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side1(\'sms\')"><img style="padding-bottom: 3px;" src="media/images/icons/sms_blue.png" alt="24 ICON" height="20" width="20" title="ესემეს"></span>
                    <span class="client_mail" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side1(\'client_mail\')"><img style="padding-bottom: 3px;" src="media/images/icons/client_email_blue.png" alt="24 ICON" height="20" width="20" title="ელ. ფოსტა"></span>
                    
                    <span class="chat_question" style="margin-left: 15px; display: block; padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side1(\'chat_question\')"><img style="padding-bottom: 3px;" src="media/images/icons/operator_comments_blue.png" alt="24 ICON" height="20" width="20" title="კომენტარები"></span>
        			<span class="file" style="margin-left: 15px;display: block;padding: 10px 0px;  cursor: pointer; float:left;" onclick="show_right_side1(\'file\')"><img style="padding-bottom: 3px;" src="media/images/icons/file_blue.png" alt="24 ICON" height="20" width="20" title="ფაილები"></span>
        		</div>
                <div style="width: 653px;float: right;" id="right_side">
    				<fieldset style="display:block; height: auto; padding:5px" id="info">
                        <table width="100%" class="dialog-form-table">
                            <tr style="height:0px">
    							<td style="width: 196px;"><label for="d_number">კატეგორია</label></td>
    							<td style="width: 197px;"><label for="d_number">ქვე კატეგორია1</label></td>
							    <td style="width: 197px;"><label for="d_number">ქვე კატეგორია2</label></td>
                            </tr>
    						<tr style="height:0px">
                                <td><select  id="incomming_cat_1"     style="width: 190px; height: 18px; " '.$disable.'>'.get_cat_1($res['cat_1']).'</select></td>
                                <td><select  id="incomming_cat_1_1"   style="width: 191px; height: 18px; " '.$disable.'>'.get_cat_1_1($res['cat_1'],$res['cat_1_1']).'</select></td>
                                <td><select  id="incomming_cat_1_1_1" style="width: 191px; height: 18px; " '.$disable.'>'.get_cat_1_1_1($res['cat_1_1'],$res['cat_1_1_1']).'</select></td>
                            </tr>
                            <tr style="height:5px"></tr>
                            
                        </table>
                        <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                        <table width="100%" class="dialog-form-table">
                           
                            <tr style="height:5px"></tr>
                           
                            <tr style="height:5px"></tr>
                            <tr style="height:0px">
    							<td><label for="d_number">კლიენტის სტატუსი</label></td>
    						</tr>
    						<tr style="height:0px">
                                <td>
                                    <select  id="s_u_status" style="width: 190px; height: 18px; " '.$disable.'>'.Getclientstatus($res['client_satus']).'</select>
    							</td>
    						</tr>
                            <tr style="height:5px"></tr>
                            <tr style="height:0px; '.$lid_comment_hide.'">
    							<td colspan="3" style=" width: 180px;">ლიდის კომენტარი</td>
                            </tr>
    					    <tr style="height:0px; '.$lid_comment_hide.'">
    						    <td colspan="3">
        							<textarea  style=" resize: vertical;width: 98%; height: 30px;" id="lid_comment" class="idle" name="call_content" '.$disable.'>' . $res['lid_comment'] . '</textarea>
        						</td>
    					    </tr>
                        </table>
                        <table width="100%" class="dialog-form-table">
                            <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                            <tr style="height:0px">
    							<td colspan="3" style="width: 180px;">საუბრის შინაარსი</td>
                            </tr>
    					    <tr style="height:0px">
    						    <td colspan="3">
        							<textarea  style="resize: vertical;width: 98%; height: 30px;" id="call_content" class="idle" name="call_content" '.$disable.'>' . $res['call_content'] . '</textarea>
        						</td>
    					    </tr>
    					</table>
                        <hr style="border: 2px solid #FFF; margin-left: -6px; width: 103%;">
                        <table width="100%" class="dialog-form-table">
                           <tr style="height:0px">
    							<td style="width: 196px;"><label for="d_number">გამავალი ზარის სტატუსი</label></td>
                                <td style="width: 197px;"><label for="d_number">განყოფილება</label></td>
    							<td style="width: 197px;"><label for="d_number">პასუხისმგებელი პ.</label></td>
                           </tr>
    					   <tr style="height:0px">
                                <td><select  id="out_status_id"     style="width: 190px; height: 18px; ">'.get_out_status($res['status_id']).'</select></td>
                                <td><select id="task_branch_id" style="width: 190px;" data-select="chosen">'.get_select($res['branch_id'],'department').'</select></td>
                                <td><select  id="out_recepient_id"   style="width: 191px; height: 18px; ">'.get_recepient($res['recepient_id']).'</select></td>
                           </tr>
                           <tr style="height:5px;"></tr>
                		   <tr style="height:0px;">
                                <td colspan="3"><label>პერიოდი</label></td>
                		   </tr>
                           <tr style="height:0px;">
                                <td><input id="out_start_date" style="width:150px;display: inline;" type="text" value="'.$res['out_start'].'" '.$dis.'><span style="vertical-align: unset;"> -დან</span></td>
                                <td colspan="2"><input id="out_end_date"   style="width:150px;display: inline;" type="text" value="'.$res['out_end'].'" '.$dis.'><span style="vertical-align: unset;"> -მდე</span></td>
                		   </tr>
                           <tr style="height:5px"></tr>
                           <tr style="height:0px">
    							<td colspan="3" style="width: 180px;">კომენტარი</td>
                           </tr>
    					   <tr style="height:0px">
    						    <td colspan="3">
        							<textarea  style="resize: vertical;width: 98%; height: 30px;" id="call_description" class="idle" name="call_content">' . $res['outgoing_comment'] . '</textarea>
        						</td>
    					   </tr>
                        </table>
                        <div style="width:100%; box-sizing: border-box;" id="check_history" style="padding-top: 15px;">
    					</div>
    				 </fieldset>
                     <fieldset style="display:none; height: auto;" id="chat_question">
                         <p>ახალი კომენატრი</p>
        	             <textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; width:440px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea > <button style="height: 50px; top: 76px; position: absolute;" id="add_comment">დამატება</button>
                         <div style="min-height: auto; max-height: 390px; overflow-y: scroll; border: 1px solid #dbdbdb;">'.get_comment($res['incomming_call_id'], $res['id']).'</div>
                         <input type=hidden id="comment_incomming_id" value="'.$res['id'].'"/>
                     </fieldset>
                     <fieldset style="display:none; height: auto;" id="call_resume">
                         <table width="100%" class="dialog-form-table">
                            <tr style="height:0px">
    							<td style="width: 205px;"><label for="d_number">ოპერატორი</label></td>
    							<td style="width: 205px;"><label for="d_number">თარიღი</label></td>
                                <td style="width: 205px;"><label for="d_number">მომართვის ნომერი</label></td>
    						</tr>
    						<tr style="height:0px">
                                <td>
    								<input style="height: 18px; width: 160px;" type="text" id="my_operator_name" class="idle" value="'.$res[operator].'" disabled="disabled"/>
                                </td>
    							<td>
    								<input style="height: 18px; width: 160px;" type="text" id="call_datetime" class="idle" value="'.$res['date'].'" disabled="disabled"/>
                                </td>
                                <td>
    								<input style="height: 18px; width: 160px;" type="text" id="incomming_call_id1" class="idle" value="'.$res[id].'" disabled="disabled"/>
                                </td>
                            </tr>
                        </table>
                     </fieldset>
                     <fieldset style="display:none; height: 653px;" id="client_mail">
                        <div class="margin_top_10"> 
                            <div style="float:right;">
                                 <a style="text-decoration: none; color: #019acc; font-family: Arial;" href="index.php?pg=102" target="_blank">ყველა ესემესი</a> 
        			             <a href="index.php?pg=102" target="_blank"><img style="margin-top: 4px;" src="media/images/icons/new-tab-button.png" width="11" height="9" alt=""></a>
        			        </div>           
            	            <div id="button_area">
                                <button id="add_mail">ახალი E-mail</button>
                            </div>
                            
                            <table class="display" id="table_mail" >
                                <thead>
                                    <tr id="datatable_header">
                                        <th>ID</th>
                                        <th style="width: 20%;">თარიღი</th>
                                        <th style="width: 25%;">ადრესატი</th>
                                        <th style="width: 35%;">გზავნილი</th>
                                        <th style="width: 20%;">სტატუსი</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr class="search_header">
                                        <th class="colum_hidden">
                                    	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            
        	            </div>
                     </fieldset>
                     <fieldset style="display:none; height: 653px;" id="sms">
    					<div class="margin_top_10"> 
                        <div style="float:right;">
                             <a style="text-decoration: none; color: #019acc; font-family: Arial;" href="index.php?pg=68" target="_blank">ყველა ესემესი</a> 
    			             <a href="index.php?pg=68" target="_blank"><img style="margin-top: 4px;" src="media/images/icons/new-tab-button.png" width="11" height="9" alt=""></a>
    			        </div>           
    					<div id="button_area">
    						<button id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
    					</div>
    					<table class="display" id="table_sms" >
    						<thead>
    							<tr id="datatable_header">
    								<th>ID</th>
    								<th style="width: 20%;">თარიღი</th>
    								<th style="width: 15%;">ადრესატი</th>
    								<th style="width: 50%;">ტექსტი</th>
    								<th style="width: 15%;">სტატუსი</th>
    							</tr>
    						</thead>
    						<thead>
    							<tr class="search_header">
    								<th class="colum_hidden">
    								<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
    								</th>
    							</tr>
    						</thead>
    					</table>
    					</div>
    				 </fieldset>
    				 <fieldset style="display:none; height: auto;" id="task">
                        <div id="task_button_area" style="height: 30px;"> 
                            <button id="add_task_button">ახალი დავალება</button>
                        </div>
        	            <table class="display" id="task_table" style="width: 100%; margin-left:0;">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 20%;">დასაწყისი</th>
                                    <th style="width: 20%;">დასასრული</th>
                                    <th style="width: 20%;">სტატუსი</th>
                                    <th style="width: 20%;">პასუხისმგებელი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                        <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </fieldset>
    				<fieldset style="display:none; height: auto;" id="record" >
    	               <div style="margin-top: 10px;">
                            <audio controls style="margin-left: 145px;" id="auau">
                              <source src="" type="audio/wav">
                              Your browser does not support the audio element.
                            </audio>
                       </div>
                       <fieldset style="display:block !important; margin-top: 10px;">
                            <legend>შემომავალი ზარი</legend>
            	            <table style="margin: auto;">
            	               <tr>
            	                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                            	   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ოპერატორი</td>
                                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ვინ გათიშა</td>
                            	   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
                        	    </tr>
            	                '.GetRecordingsSection($res).'
                    	    </table>
        	            </fieldset>
                        <fieldset style="display:block !important; margin-top: 10px;">
                            <legend>გამავალი ზარი</legend>
            	            <table style="margin: auto;">
            	               <tr>
            	                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                            	   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ოპერატორი</td>
                            	   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
                        	    </tr>
            	                '.GetRecordingsSection1($res).'
                    	    </table>
        	            </fieldset>
        	      </fieldset>
                  <fieldset style="display:none; height: auto;" id="file">
                        <table style="float: right;text-align: center; border: 1px solid #ccc; width: 100%;">
    					<tr>
    						<td>
    							<div class="file-uploader">
    								<input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
    								<button id="choose_button" style="width: 100%;" class="center">აირჩიეთ ფაილი</button>
    								<input id="hidden_inc" type="text" value="" style="display: none;">
    							</div>
    						</td>
    					</tr>
    				</table>
    			     <table style="float: right;border: 1px solid #ccc;width: 100%;margin-top: 2px;text-align: center;">
    			          <tr>
    			           <td colspan="4">მიმაგრებული ფაილი</td>
    			          </tr>
    				</table>
    				<table id="file_div" style="float: right; border: 1px solid #ccc; width: 100%;margin-top: 2px; text-align: center;">';
    
    		            foreach ($increm[result] AS $increm_row){
    						$data .='<tr style="border-bottom: 1px solid #CCC;">
                                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[file_date].'</td>
    						            <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">'.$increm_row[name].'</td>
    						            <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="../../media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
    						            <td style="height: 20px;vertical-align: middle;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
    						         </tr>';
    					}
    
        	 $data .= '
        	 		</table>
        		</fieldset>
                
    	 		</div>
    	 		<input id="hidden_user" type="text" value="'. $res['user_id'] .'" style="display: none;">
    	 		<input id="note" type="text" value="'. $res['note'] .'" style="display: none;">
                <input id="ast_source" type="text" value="'. $res['ast_source'] .'" style="display: none;">
    	 </div>';
        	 $data .= '<input type="hidden" id="outgoing_call_id" value="' . $res['id'] . '" />
                       <input type="hidden" id="hidde_outgoing_id" value="' . $hidde_id . '" />
                       <input type="hidden" id="incomming_call_id" value="' . $res[incomming_call_id] . '"/>';

    return $data;
}

function get_comment($incomming_id, $out_id){
    global $db;
    $where ='';
    if($incomming_id =='' || $incomming_id == 0){
        $where = "AND outgoing_call_id = '$out_id'";
    }else{
        $where = "AND incomming_call_id = '$incomming_id'";
    }
    $data="";
    $db->setQuery(" SELECT   `id`,
                             `user_id`,
                             `incomming_call_id`,
                             `comment`,
                             `datetime`
                    FROM     `chat_comment`
                    WHERE     parent_id = 0  AND active = 1  $where
                    ORDER BY `datetime` DESC");
    $req = $db->getResultArray();
    foreach($req[result] AS $res){
        
        $data .='<div style="margin-top: 30px;">
                    <input type=hidden id="hidden_comment_id_'.$res['id'].'" value="'.$res['id'].'"/>
                    <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                    <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; "> '.$res['comment'].'</span>
                    <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                    <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg">
                    <br/><br/><span id="get_answer" class="'. $res['id'] .'"  style="color: #369; font-size:15px;     font-size: 11px;
                    margin-left: 10px;  ">პასუხის გაცემა</span>
                </div>
        <div style="margin-left: 50px; margin-top: 10px;">
            '.get_answers($res['id'], $out_id).'
        </div>';
    }
    
    
    return $data;
}

function Getquestion($incomming_id,$out_id){
    $data = '
        <p>ახალი კომენატრი</p>
        <textarea id="add_ask" style="resize:none; width:480px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea > <button style="top: 76px; position: absolute; height: 50px;" id="add_comment">დამატება</button>
            '.get_comment($incomming_id,$out_id).'
    ';
    return $data;
}

function get_user($id){
    global $db;
    $data   ='';
    $db->setQuery("SELECT `username`
                   FROM   `users`
                   WHERE   id = $id AND actived = 1 ");
    
    $res    = $db->getResultArray();
    $data   = $res[result][0]['username'];
    return $data;
}

function get_answers($id){
    global $db;
    $data= '';
    
    $db->setQuery(" SELECT `id`,
                           `user_id`,
                           `comment`,
                           `datetime`
                    FROM   `chat_comment`
                    WHERE   parent_id = '$id' AND active = 1");
    $req = $db->getResultArray();
    
    foreach($req[result] AS $res){
        $data .='<div>
                    <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                    <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; ">'.$res['comment'].'</span>
                    <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                    <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
    }
    return $data;
}

function GetRecordingsSection($res){
    global $db;
    
    if ($res[phone]==''){
        $num_row = 0;
    }else{
        $db->setQuery(" SELECT  	SEC_TO_TIME(asterisk_incomming.duration) AS `time`,
                                    CONCAT(DATE_FORMAT(asterisk_incomming.call_datetime, '%Y/%m/%d/'),`file_name`) AS `userfield`,
                                    asterisk_incomming.call_datetime,
                                    user_info.`name`,
                				    IF(asterisk_incomming.disconnect_cause = 4, 'ოპერატორმა', 'მომხმარებელმა') AS `completed`
                        FROM 		asterisk_incomming
                        LEFT JOIN   user_info ON user_info.user_id = asterisk_incomming.user_id
                        WHERE		asterisk_incomming.id ='$res[asterisk_incomming_id]'");
        
        $num_row = $db->getNumRow();
        $req     = $db->getResultArray();
    }
    
    if ($num_row == 0){
        $data .= '<tr>
                      <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
    }else{
        foreach($req[result] AS $res2){
            $src = $res2['userfield'];
            $data .= '
    		      <tr>
                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[call_datetime].'</td>
                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[time].'</td>
                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[name].'</td>
                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[completed].'</td>
                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$src.'\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
        	      </tr>';
        }
    }
    
    return $data;
}

function GetRecordingsSection1($res){
    global $db;
    if ($res[phone]==''){
        
    }else{
        
        $ph = "CAST(`phone` AS SIGNED) LIKE '%$res[phone]%'";
        
        $db->setQuery("SELECT    SEC_TO_TIME(asterisk_outgoing.duration) AS `time`,
                                 asterisk_outgoing.file_name AS `userfield`,
                                 asterisk_outgoing.call_datetime,
                                 user_info.`name`
                       FROM      asterisk_outgoing
                       LEFT JOIN user_info ON asterisk_outgoing.user_id = user_info.user_id
                       WHERE   $ph");
    }
    
    if ($db->getNumRow() == 0){
        $data .= '<tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=4>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
    }
    
    $req = $db->getResultArray();
    foreach($req[result] AS $res2){
        $src = $res2['userfield'];
        $data .= '<tr>
                     <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[call_datetime].'</td>
            	     <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[time].'</td>
                     <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[name].'</td>
            	     <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$src.'\')"><span style="color: #ff9800;">მოსმენა</span></td>
        	      </tr>';
    }
    
    return $data;
}

function ChangeResponsiblePerson($letters, $responsible_person){
    global $db;
    $o_date = date('Y-m-d H:i:s');
    
    foreach ($letters as $letter) {

       $db->setQuery("UPDATE outgoing_call
					     SET outgoing_call.`status_id`  = 2,
							 outgoing_call.`start_date` = NOW(),
							 outgoing_call.recepient_id = '$responsible_person'
					  WHERE  outgoing_call.id 		    = '$letter'");
       $db->execQuery(); 
    }
}

function GetPersons($persons_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`, `name`
							FROM `crystal_users`
							WHERE actived=1");



    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $persons_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }


    return $data;
}

function GetResoniblePersonPage(){
    $data ='<div id="dialog-form">
    			<fieldset>
    				<table width="100%" class="dialog-form-table" cellpadding="10px" >
    					<tr>
    						<th>
    							<select id="responsible_person" class="idls address">' . get_recepient('') . '</select>
    						</th>
    					</tr>
    				</table>
    			</fieldset>
    		</div>';
    
    return $data;

}

?>