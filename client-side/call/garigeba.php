<?php
require_once("AsteriskManager/config.php");
include("AsteriskManager/sesvars.php");

if(isset($_SESSION['QSTATS']['hideloggedoff'])) {
    $ocultar=$_SESSION['QSTATS']['hideloggedoff'];
} else {
    $ocultar="false";
}

?>
<head>
    <link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
    <script type="text/javascript" src="js/MyWebSocket.js?v=1"></script>
    <script type="text/javascript" src="js/jquery.timeago.js"></script>

    <script src="https://kendo.cdn.telerik.com/2020.1.114/js/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="js/kendoUI/kendo.all.min.js"></script>
<script type="text/javascript" language="javascript" src="js/kendoUI/kendo.main.class.js"></script>
<script src="https://kendo.cdn.telerik.com/2020.1.114/js/pako_deflate.min.js"></script>

<link rel="stylesheet" href="media/css/kendoUI/kendo.common.min.css">

    <?PHP if($_SESSION['chat_off_on'] == ''){$_SESSION['chat_off_on'] = 1;}?>
    <script type="text/javascript">
var aJaxURL =                   "server-side/call/waiters.action.php";
    function LoadKendoTable($hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var aJaxURL =                   "server-side/call/waiters.action.php";
        var gridName = 				    'crm_grid';
        var actions = 				    '';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    6;
        var columnsCount =			    10;
        var columnsSQL = 				["id:string","inc_id:string","create_date:date","name:string","com_source:string","category:string","type:string","contact_count:string","contact_source:string","progress:string"];
        var columnGeoNames = 		    ["ID","შემ.ID","თარიღი","ტელეფონი","ოპერატორი","სახელი/გვარი","პროექტი","ფართის ტიპი","კომენტარი","მენეჯერი"];

        var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_list&main_status=250',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,$hidden);
    }
    $(document).ready(function () {
        
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_sub_tabs&status=250",
            dataType: "json",
            success: function (data) {
                var tabs = data.tabs;
                tabs.forEach(function(tab){
                    $("#status_tabs").append('<li id="'+tab.id+'" aria-selected="false">'+tab.name+'<p style="margin:0;">'+tab.cc+'</p></li>')
                });
                $('.status_tabs li:first-child').click();
                var post_data = "&sub_status="+$('.status_tabs li:first-child').attr('id');
                LoadKendoTable(post_data);

            }
        });

    });

    $(document).on("dblclick", "#crm_grid tr.k-state-selected", function () {
        var grid = $("#crm_grid").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_edit_page&id="+dItem.inc_id+"&crm_id="+dItem.id,
            dataType: "json",
            success: function (data) {
                $("#add-edit-form-comunications").html(data.page);
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                };
                LoadDialog('add-edit-form-comunications');
            }
        });
    });
</script>
    <script type="text/javascript">

        var aJaxURL		     = "server-side/call/waiters.action.php";		//server side folder url

        var tName	         = "table_index";										//table name
        var fName	         = "add-edit-form-comunications";
        var tbName		     = "tabs";										//form name
        var file_name        = '';
        var rand_file        = '';
        var tab              = 0;
        var fillter          = '';
        var fillter_all      = '';
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var Comunications_URL = "server-side/call/waiters.action.php";
        //////////////////
        function loadDndData() {
            param 	  = new Object();
            param.act = "check_user_dnd_on_local";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                        }else{
                            if(data.status==0){
                                $("#incallToggleDnd").attr('src','media/images/icons/comunication/Phone_ON.png');
                            }else if(data.status==1){
                                $("#incallToggleDnd").attr('src','media/images/icons/comunication/Phone_INSIDE.png');
                            }else if(data.status==2){
                                $("#incallToggleDnd").attr('src','media/images/icons/comunication/Phone_OFF.png');
                            }

                            if(data.muted == 1){
                                $('#micr').attr('src','media/images/icons/5.png');
                                $(this).attr('value','1');
                                micr_val = 1;
                            }else{
                                $('#micr').attr('src','media/images/icons/4.png');
                                $(this).attr('value','0');
                                micr_val = 0;
                            }

                            if(data.queries_unrid_count>0){
                                $("#client_conversation_count_hint").html(data.queries_unrid_count);
                                $("#client_conversation_count_hint").css('display','');
                            }else{
                                $("#client_conversation_count_hint").html(data.queries_unrid_count);
                                $("#client_conversation_count_hint").css('display','none');
                            }
                            setTimeout(loadDndData, 1000);
                        }
                    }
                }
            });
        }
        //////////////////////////////
        $(document).ready(function () {
            // loadDndData();
            check_user_comunication();
            // GetButtons("add_button");
            GetDate('start_date');
            GetDate('end_date');
            $('#operator_id,#tab_id,#user_info_id').chosen({ search_contains: true });
            $('.callapp_filter_body').css('display','block');
            $.session.clear();
            $.session.set("checker_st", "1");
            // runAjax();
            LoadTable();
            // GetButtons("add_button", "delete_button","refresh");
            SetEvents("", "", "", "table_index", fName, aJaxURL);
            toggleChatStatus('#chat_status', <?PHP echo $_SESSION['chat_off_on'];?>);
            // $(".checkbox_check").css('display','none');
            // $("#delete_button").css('display','none');

            // $('#flesh_panel').css('width', '300px');
            $("#show_station").css('background','#ccc');
            // loadDndData1();
        });

        function loadDndData1() {

            // define request data
            const data = {
                act:"load_dnd_data",
                getOptions: $("#callapp_op_activitie_choose").find("li").length === 1
            }

            // send ajax request
            $.post(aJaxURL, data, responce => {

                // restruct responce
                const { error, options, dndStatus } = responce;

                // check for error
                if(error !== "") {
                    // alert(error);
                    alert("We where in Munxern");
                } else {

                    // customize dnd button
                    if(options !== "") {
                        $("#callapp_op_activitie_choose").html(options);
                    }

                    // make check interval
                    setTimeout(loadDndData, 1000);

                }
            });
        }
        function toggleChatStatus(selector, status) {
            $D.sessionStatus = status === 2 ? 1 : 0;
            const dataStatus = status === 2 ? "on" : "off";
            $(selector).val(status).attr("data-status", dataStatus).data("status", dataStatus);
        }


        function displayAmountStandartSubCat(status) {
            $("#card_type_id_holder").css({
                display: status
            });
            $("#for_card_type_id").css({
                display: status
            });
        }

        function displayAmountInOutSubCat(status) {
            $("#for_transaction_date_time").css({
                display: status
            });
            $("#transaction_date_time").css({
                display: status
            });
            $("#for_card_16_number").css({
                display: status
            });
            $("#card_16_number_holder").css({
                display: status
            });
        }


        $(document).on("click", "#filter_dam_shem", function () {
            fillter	= $("#filter_dam_shem").val();
            LoadTable();
        });
        $(document).on("click", "#filter_transf_shem", function () {
            fillter	= $("#filter_transf_shem").val();
            LoadTable();
        });
        $(document).on("click", "#filter_dam_gam", function () {
            fillter	= $("#filter_dam_gam").val();
            LoadTable();
        });
        $(document).on("click", "#filter_transf_gam", function () {
            fillter	= $("#filter_transf_gam").val();
            LoadTable();
        });
        $(document).on("click", "#filter_unansver", function () {
            fillter	= $("#filter_unansver").val();
            LoadTable();
        });
        $(document).on("click", "#filter_dau_gam", function () {
            fillter	= $("#filter_dau_gam").val();
            LoadTable();
        });
        $(document).on("click", "#noansverdone", function () {
            fillter	= $("#noansverdone").val();
            LoadTable();
        });
        $(document).on("click", "#filter_dau_shem", function () {
            fillter	= $("#filter_dau_shem").val();
            LoadTable();
        });
        $(document).on("click", "#filter_chati1", function () {
            fillter	= $("#filter_chati1").val();
            LoadTable();
        });
        $(document).on("click", "#filter_chati2", function () {
            fillter	= $("#filter_chati2").val();
            LoadTable();
        });
        $(document).on("click", "#filter_chati3", function () {
            fillter	= $("#filter_chati3").val();
            LoadTable();
        });
        $(document).on("click", "#filter_saubr", function () {
            fillter	= $("#filter_saubr").val();
            LoadTable();
        });
        $(document).on("click", "#filter_all1", function () {
            fillter	= $("#filter_all1").val();
            LoadTable();
            $('#fi_call,#fi_chat').css('display','table');
        });
        $(document).on("click", "#filter_all2", function () {
            fillter	= $("#filter_all2").val();
            LoadTable();
            $('#fi_chat').css('display','none');
            $('#fi_call').css('display','table');
        });
        $(document).on("click", "#filter_all3", function () {
            fillter	= $("#filter_all3").val();
            LoadTable();
            $('#fi_call').css('display','none');
            $('#fi_chat').css('display','table');
        });


        $(document).on("click", ".client_history", function () {
            $("#client_history_user_id").val($("#phone").val());
            GetDate("date_from");
            GetDate("date_to");
            $("#search_client_history").button();
            
            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("table_history", aJaxURL, "get_list_history", 6, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
           
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", "#control_tab_id_4_0", function () {
            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id="+$("#incomming_call_id").val()+"&history_user_id="+$("#client_history_user_id").val()+"&client_history_filter_date="+$("#client_history_filter_date").val()+"&client_history_filter_activie="+$("#client_history_filter_activie").val(), 0, dLength, 2, "desc",'', "<'F'lip>");
            $("#example4_0_length").css('top','0px');
            $("#example4_0_wrapper").css('width','1000px');
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", "#search_client_orders", function () {
            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id="+$("#incomming_call_id").val()+"&history_user_id="+$("#client_history_user_id").val()+"&client_history_filter_date="+$("#client_history_filter_date").val()+"&client_history_filter_activie="+$("#client_history_filter_activie").val(), 0, dLength, 2, "desc",'', "<'F'lip>");
            $("#example4_0_length").css('top','0px');
            $("#example4_0_wrapper").css('width','1000px');
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", "#control_tab_id_4_1", function () {
            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id="+$("#incomming_call_id").val()+"&history_user_id="+$("#client_history_user_id").val()+"&client_history_filter_site="+$("#client_history_filter_site").val()+"&client_history_filter_group="+$("#client_history_filter_group").val()+"&client_history_filter_transaction_type="+$("#client_history_filter_transaction_type").val(), 0, dLength, 2, "desc",'', "<'F'lip>");
            $("#example4_1_length").css('top','0px');
            $("#example4_1_wrapper").css('width','790px');
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", "#search_client_transaction", function () {
            var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id="+$("#incomming_call_id").val()+"&history_user_id="+$("#client_history_user_id").val()+"&client_history_filter_site="+$("#client_history_filter_site").val()+"&client_history_filter_group="+$("#client_history_filter_group").val()+"&client_history_filter_transaction_type="+$("#client_history_filter_transaction_type").val(), 0, dLength, 2, "desc",'', "<'F'lip>");
            $("#example4_1_length").css('top','0px');
            $("#example4_1_wrapper").css('width','790px');
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("click", "#search_client_history", function () {
        	var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
            GetDataTable("table_history", aJaxURL, "get_list_history", 6, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
           
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');
            }, 90);
        });

        $(document).on("change, focus", "[data-hendler='error']", function() {
            var inputId = $(this).attr("id");
            $(`label[for="${inputId}"]`).removeClass("red-color");
        });


        $(document).on("change", "#tab_id", function () {
            if($(this).val()==5){
                $(".checkbox_check").css('display','');
                $("#delete_button").css('display','');
            }else{
                $(".checkbox_check").css('display','none');
                $("#delete_button").css('display','none');
            }
        });
        $(document).on("click", "#loadtable", function () {
            LoadTable();
            // $('.callapp_filter_body').css('display','none');
            // $('.callapp_filter_body').attr('myvar',0);
            // $("#shh").css('transform', 'rotate(180deg)');
        });

        $(document).on("click", ".callapp_refresh", function () {
            fillter = '';
            LoadTable();
        });
        $(document).on("click", ".comunication_button", function () {
            if($("#comunication_table_check").val()==0){
                $("#comunication_table_check").val(1);
                $("#comucation_status").css('display','');
            }else{
                $("#comunication_table_check").val(0);
                $("#comucation_status").css('display','none');
            }
        });

        function LoadTable(){
            AJ = aJaxURL;
            columcaunt = 10;

            /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
            GetDataTable("table_index", AJ, "get_list", columcaunt, "start="+$('#start_date').val()+"&end="+$('#end_date').val()+"&operator_id="+$('#operator_id').val()+"&user_info_id="+$("#user_info_id").val()+"&tab_id="+$('#tab_id').val()+"&filter_1="+$('#filter_1:checked').val()+"&filter_2="+$('#filter_2:checked').val()+"&filter_3="+$('#filter_3:checked').val()+"&filter_4="+$('#filter_4:checked').val()+"&filter_5="+$('#filter_5:checked').val()+"&filter_6="+$('#filter_6:checked').val()+"&filter_7="+$('#filter_7:checked').val()+"&filter_8="+$('#filter_8:checked').val()+"&filter_9="+$('#filter_9:checked').val()+"&filter_10="+$('#filter_10:checked').val()+"&filter_11="+$('#filter_11:checked').val()+"&filter_12="+$('#filter_12:checked').val(), 0, "", 2, "desc",'',change_colum_main);
            setTimeout(function(){
                $('.ColVis, .dataTable_buttons').css('display','none');

                // $('#table_index td:nth-child(10)').css('padding',0);
                // $('#table_index td:nth-child(11)').css('padding',0);

            }, 90);
        }


        ///////////////////////////////////---------------aqedan---------------------------/////////////////////////////






        $(document).on("click", "#chat_hist", function () {
            $.ajax({
                url: aJaxURL,
                data: {
                    'act'		:'get_history',
                    'ip' 		:$('#user_block').attr('ip'),
                    'chat_id'	:$('#chat_original_id').val(),
                    'source'	:$("#chat_source").val(),
                    'chat_name' :$("#chat_name").val()

                },
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){

                        }else{
                            $('#log').html('');
                            $('#log').html(data.sms);
                            jQuery("time.timeago").timeago();
                            document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
                            $('#chat_live').css('border-bottom','2px solid transparent');
                            $('#chat_hist').css('border-bottom','2px solid #333');
                        }
                    }
                }
            });
        });
        /////////////////////////////////// aqamde//////////////////////////////////////
        $(document).on("click", "#user_block_save", function () {
            $.ajax({
                url: aJaxURL,
                data: "act=block_ip&ip="+$("#user_block").attr('ip')+"&pin="+$("#user_block").attr('pin')+"&chat_comment="+$('#block_comment').val()+"&chat_id="+$('#chat_original_id').val(),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){

                        }else{
                            $('#log').html('');
                            $('#add_blocked_ip').dialog("close");

                        }
                    }
                }
            });
        });
        $(document).on("click", "#user_block_pin", function () {
            $.ajax({
                url: aJaxURL,
                data: "act=block_pin&ip="+$("#user_block").attr('ip')+"&pin="+$("#user_block").attr('pin')+"&chat_comment="+$('#block_comment').val()+"&chat_id="+$('#chat_original_id').val(),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){

                        }else{
                            $('#log').html('');
                            $('#add_blocked_ip').dialog("close");

                        }
                    }
                }
            });
        });
        /////////////////////////////////////////////////////////////////////////////////
        $(document).on("change", "#transfer_resp_ext_id", function () {
            var value = $(this).val();
            $.ajax({
                url: aJaxURL,
                data: "act=get_resp_user&extention="+value,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){

                        }else{
                            $('#transfer_resp_id').html(data.name);
                            $('#transfer_resp_id').attr('val', data.id);
                        }
                    }
                }
            });
        });
        /////////////////////////////////////////////////////////////////////////////////
        $(document).on("change", "#check_transfer", function () {
            var value = $('input[name=check_transfer]:checked').val();
            if(value == 1){
                $("#transfer_resp_ext_id").css('display', '');
                $("#transfer_resp_id").css('display', '');
            }else{
                $("#transfer_resp_ext_id").css('display', 'none');
                $("#transfer_resp_id").css('display', 'none');
            }
        });
        ////////////////////////////////////////////////////////////////////////////////
        $(document).on("click", "#user_block", function () {
            if($("#source_id").val()==6 || $("#source_id").val()==7 || $("#source_id").val()==9 || $("#source_id").val()==11 || $("#source_id").val()==12){
                var buttons = {
                    "chat_block_locked": {
                        text: "დაბლოკვა",
                        id: "chat_block_locked",
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel",
                        click: function () {
                            $(this).dialog('close');
                        }
                    }

                };
                GetDialog("numlock_dialog", 280, "auto", buttons);
                $("#numlock_dialog").html('<span>კომენტარი</span><textarea style="width: 245px; height: 80px; resize: none;" id="chat_block_reason" class="idle"></textarea>');
            }else{
                if($(this).attr('ip') != ''){
                    var buttons = {

                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        },
                        "save": {
                            text: "IP_ით დაბლოკვა",
                            id: "user_block_save"
                        },

                        "save_pin": {
                            text: "PIN_ით დაბლოკვა",
                            id: "user_block_pin"
                        }

                    };
                    //////////////////////////////////////////////////////////////////////////

                    /* Dialog Form Selector Name, Buttons Array */
                    GetDialog("add_blocked_ip", 400, "auto",buttons );
                }else{
                    alert('ჯერ მონიშნეთ აქტიური ჩატი!');
                }
            }
        });
        //////////////////////////////////////////////////////////////////////////



        $(document).on("click", "#num_block", function LoadDialog2(){
            var buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel",
                    click: function () {
                        $(this).dialog("close");
                    }
                },
                "num_block_locked": {
                    text: "დაბლოკვა",
                    id: "num_block_locked",
                }

            };
            GetDialog("numlock_dialog", 280, "auto", buttons);
            $("#numlock_dialog").html('<span>კომენტარი</span><textarea style="width: 245px; height: 80px; resize: none;" id="block_comment2" class="idle"></textarea>');
        });


        $(document).on("click", "#save-edit-form-sport", function () {

            param 					= new Object();
            param.act				= "save-edit-form-sport";
            param.end_date			= $("#add_sport_dialog #action_end_date").val();
            param.start_date		= $("#add_sport_dialog #action_start_date").val();
            param.id				= $("#add_sport_dialog #action_sport_id").val();
            param.act_sport_id		= $("#add_sport_dialog #act_id").val();
            param.category_id		= $("#add_sport_dialog #action_category_id").val();
            param.game_list 		= $("#add_sport_dialog #game_list").val();
            param.info				= $("#add_sport_dialog #action_info").val();

            $.ajax({
                url: aJaxURLsport,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != "undefined"){
                        if(data.error != ""){
                            alert(data.error);
                        }else{
                            GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc",'',"<'F'lip>");
                            $("#add_sport_dialog").dialog('close');
                            $("#table_sport_length").css('top', '0px');
                        }
                    }
                }
            });
        });

        /*	function getchats(){
			clearTimeout(t);

            var t = setTimeout(function(){


	    			getchats();


	    		}, 1000000);
		}*/
        $(document).on("click", "#search_ab_pin", function () {

            GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#check_ab_phone").val()+"&s_u_user_id="+$("#check_ab_user_id").val(), 0, "", 2, "desc", '', "<'F'lip>");

            $("#table_history_length").css('top','0px');
        });
        $(document).on("click", "#tab_sport0", function () {
            GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc",'',"<'F'lip>");
            $("#table_sport_length").css('top','0px');
        });

        $(document).on("click", "#tab_sport1", function () {
            GetDataTable("table_sport1", aJaxURLsport, "get_list_archive", 7, "", 0, "", 0, "desc",'',"<'F'lip>");
            $("#table_sport1_length").css('top','0px');
        });

        $(document).on("click", ".download6", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://pbx.my.ge/records/' + link;
            } else{
                link = 'http://pbx.my.ge/records/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto",btn);

            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download1").css( "background","#F99B03" );
            $(this).css( "background","#33dd33" );

        });


        $(document).on("click", ".download2", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://pbx.my.ge/records/' + link;
            } else{
                link = 'http://pbx.my.ge/records/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto",btn);
            //	alert('hfgj');
            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download").css( "background","#408c99" );
            $(this).css( "background","#FF5555" );

        });

        $(document).on("click", ".download4", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://212.72.155.183:8000/' + link;
            } else{
                link = 'http://212.72.155.183:8000/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto",btn);
            //	alert('hfgj');
            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download").css( "background","#2dc100" );
            $(".download4").css( "background","#2dc100" );
            $(this).css( "background","#FF5555" );

        });


        //ჩანაწერები
        $(document).on("click", ".download", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://pbx.my.ge/records/' + link;
            } else{
                link = 'http://pbx.my.ge/records/' + link;
            }

            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto",btn);

            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download").css( "background","#2dc100" );
            $(".download4").css( "background","#2dc100" );
            $(this).css( "background","#FF5555" );

        });

        $(document).on("click", ".download1", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://pbx.my.ge/records/' + link;
            } else{
                link = 'http://pbx.my.ge/records/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto",btn);

            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download1").css( "background","#F99B03" );
            $(".download3").css( "background","#ce8a14" );
            $(".download5").css( "background","#4980AF" );
            $(".download").css( "background","#2dc100" );
            $(this).css( "background","#5757FF" );

        });

        $(document).on("click", ".download3", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://pbx.my.ge/records/' + link;
            } else{
                link = 'http://pbx.my.ge/records/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto",btn);

            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download3").css( "background","#ce8a14" );
            $(".download5").css( "background","#4980AF" );
            $(".download1").css( "background","#F99B03" );
            $(".download").css( "background","#2dc100" );
            $(this).css( "background","#5757FF" );

        });

        $(document).on("click", ".download5", function () {
            var str = 1;
            var link = ($(this).attr("str"));
            $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
            $limit_date = new Date("2018-06-29 19:45:24");
            if($call_date > $limit_date){
                link = 'http://pbx.my.ge/records/' + link;
            } else{
                link = 'http://pbx.my.ge/records/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };

            GetDialog_audio("audio_dialog", "auto", "auto",btn);

            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
            $(".download5").css( "background","#4980AF" );
            $(".download3").css( "background","#ce8a14" );
            $(".download1").css( "background","#F99B03" );
            $(".download").css( "background","#2dc100" );
            $(this).css( "background","#5757FF" );

        });
        // end record

        // 		 $(button).on("click", ".download", function (){
        //         	  $(this).css( "background","#FF5555" );
        //          });

        function CloseDialog(){
            $("#" + fName).dialog("close");
        }

        // Add - Save

        $(document).on("click", "#lid", function () {
            if($('input[id=lid]:checked').val()==1){
                $("#lid_comment").css('display','');
            }else{
                $("#lid_comment").css('display','none');
            }
        });



        // open send sms dialog


        $(document).on("click", ".client_conversation", function (event) {
            param 	  = new Object();
            param.act = "get_client_conversation_count";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                        }else{
                            $("#client_conversation_count_hint").html(0);
                            $("#client_conversation_count_hint").css('display','none');

                            GetDataTable("table_question", aJaxURL, "get_list_quest", 5, "source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), 0, "", 0, "desc",'',"<'F'lip>");
                            $("#table_question_length").css('top','0px');
                        }
                    }
                }
            });

        });






        $(document).on("click", "#refresh", function () {
            fillter='';
            LoadTable(fillter);
        });
        $(document).on("click", "#refresh_button", function () {
            fillter_all='';
            LoadTable1();

        });

        $(document).on("click", "#refresh_button1", function () {

            param 				= new Object();
            param.act			= "refresh";

            $.ajax({
                url: aJaxURL4,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            // alert(data.error);
                            alert("Serjio");
                        }else{
                            $("#search_end_my1").val(data.date);
                            LoadTable4();
                        }
                    }
                }
            });
        });


        $(document).on("click", "#refresh_button2", function () {

            param 				= new Object();
            param.act			= "refresh";

            $.ajax({
                url: aJaxURL6,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            // alert(data.error);
                            alert("Console log");
                        }else{
                            $("#search_end_my2").val(data.date);
                            LoadTable6();
                        }
                    }
                }
            });
        });


        $(document).on("click", "#ipaddres_block", function () {
            var buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel",
                    click: function () {
                        $(this).dialog("close");
                    }
                },

                "ipaddr": {
                    text: "დაბლოკვა",
                    id: "ipaddr"
                }

            };


            /* Dialog Form Selector Name, Buttons Array */
            GetDialog("ipaddr_dialog", 400, "auto",buttons );
        });

        $(document).on("click", "#ipaddr", function () {
            parame 				= new Object();
            parame.ipaddres		= $("#ipaddres").val();
            parame.ipaddr_block_comment = $("#ipaddr_block_comment").val();
            parame.act		        = "ipaddr_check"

            $.ajax({
                url: aJaxURL,
                data: parame,
                success: function(data) {
                    if(data.check == '0'){
                        alert('ესეთი IP უკვე დაბლოკილია!');
                    }else{
                        $("#ipaddr_dialog").dialog("close");
                    }
                }
            });
        });

        $(document).on("click", "#num_block_locked", function () {
            parame 				= new Object();
            parame.phone			= $("#phone").val();
            parame.ipaddres			= $("#ipaddres").val();

            parame.act		= "num_check"
            $.ajax({
                url: aJaxURL,
                data: parame,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            // alert(data.error);
                            alert("Loading");
                        }else{
                            if(data.check==1){
                                param 				= new Object();
                                param.phone			= $("#phone").val();
                                param.block_comment = $("#block_comment2").val();

                                param.act		= "black_list_add";
                                if(param.phone == ""){
                                    alert("შეავსეთ ტელეფონის ნომერი!");
                                }else {
                                    $.ajax({
                                        url: asterisk_url,
                                        data: param,
                                        success: function(data) {
                                            if(typeof(data.error) != 'undefined'){
                                                if(data.error != ''){
                                                    // alert(data.error);
                                                    alert("Container");
                                                }else{
                                                    LoadTable();
                                                    $("#numlock_dialog").dialog("close");

                                                }
                                            }
                                        }
                                    });

                                    $.ajax({
                                        url: aJaxURL,
                                        data: param,
                                        success: function(data) {
                                            if(typeof(data.error) != 'undefined'){
                                                if(data.error != ''){
                                                    // alert(data.error);
                                                    alert("Hello there");
                                                }else{
                                                    LoadTable();
                                                    $("#numlock_dialog").dialog("close");

                                                }
                                            }
                                        }
                                    });
                                }
                            }else{
                                alert('ნომერი უკვე არის დაბლოკილთა სიაში');
                            }
                        }
                    }
                }
            });


        });





        $(document).on("click", "#chat_block_locked", function () {

            parame 			         = new Object();
            parame.act		         = "black_list_add_chat"
            parame.source_id         = $("#source_id").val();
            parame.id                = $("#id").val();
            parame.chat_block_reason = $("#chat_block_reason").val();
            parame.site_chat_id      = $("#site_chat_id").val();

            if(parame.chat_block_reason == ''){
                alert('შეავსეთ კომენტარი !!!');
            }else if(parame.id == ''){
                alert('აირჩიეთ შესაბამისი ჩატი !!!')
            }else{
                $.ajax({
                    url: aJaxURL,
                    data: parame,
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                LoadTable();
                                $("#numlock_dialog").dialog("close");
                                alert('ჩატი წარმატებით დაიბლოკა');

                                param 			= new Object();
                                param.source	= 'fbm_block';
                                param.chat_id	= $('#chat_original_id').val();

                                if($("#source_id").val()=='6'){
                                    $.ajax({
                                        url: "server.php",
                                        data: param,
                                        dataType: "json",
                                        success: function(data) {
                                            console.log('დაიბლოკა');
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
            }
        });





        $(document).on("change", "#check_state", function () {
            alert(1)
        });
        ////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////

        $(document).on("click", ".open_dialog", function () {
            var number   		= $(this).attr("number");
            var ipaddres 		= $(this).attr("ipaddres");
            var ab_pin   		= $(this).attr("ab_pin");
            var check_save_chat = $(this).attr("check_save_chat");
            var source          = $(this).attr("data-source");
            if($('#imchat').val() == 1){

            }
            if(number != ""){
                run(number,ipaddres,ab_pin,check_save_chat, source);
            }
        });

        $(document).on("click", ".insert", function () {
            var phone = $(this).attr("number");

            if(phone != ""){
                $('#telefoni___183--124--1').val(phone);
            }
        });

        $(document).on("change", "#task_type_id",function(){
            var task_type = $("#task_type_id").val();

            if(task_type == 1){
                $("#task_department_id").val(37);
            }

        });
        $(document).on("change", "#task_type", function () {
            param        = new Object();
            param.act    = "task_type_changed";
            param.cat_id = $('#task_type').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#task_status_id").html(data.page);
                    $('#task_status_id').trigger("chosen:updated");
                    $('#task_branch').trigger("chosen:updated");
                    $('#task_status_id').trigger("change");
                }
            });
        });

        $(document).on("change", "#task_status_id", function () {
            param = new Object();
            param.act = "get_task_status_2";
            param.cat_id = $('#task_status_id').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function (data) {
                    $("#task_status_id_2").html(data.page);
                    $('#task_status_id_2').trigger("chosen:updated");
                }
            });
        });


        $(document).on("click","#send_message", function(){
            param 		 = new Object();
                            param.act	 = "seen";
                            param.id     = $("#chat_original_id").val();

                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function(data) {
                                }
                            });
        })

        $(document).on("keyup","#send_message", function(e) {
                    if($("#source_id").val() == 4){

                        if (e.keyCode == 8 || e.keyCode == 13) {

                            param 		 = new Object();
                            param.act	 = "focusout";
                            param.id     = $("#chat_original_id").val();

                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function(data) {
                                }
                            });
                        }else{
                            param 		 = new Object();
                            param.act	 = "focusin";
                            param.id     = $("#chat_original_id").val();

                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function(data) {
                                }
                            });
                        }
                    }
                });


        $(document).on("click", "#refresh-dialog", function () {
            param 			= new Object();
            param.act		= "get_calls";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            // alert(data.error);
                            alert("Jimsher G");
                        }else{
                            $("#last_calls").html(data.calls);
                            $( ".insert" ).button({
                                icons: {
                                    primary: "ui-icon-plus"
                                }
                            });
                        }
                    }
                }
            });

        });


        //
        function my_filter(){
            var myhtml = '';
            if($.session.get("operator_id")=='on'){
                myhtml += '<span>ოპერატორი<close cl="operator_id">X</close></span>';
            }else{
                $('#operator_id option:eq(0)').prop('selected', true);
            }
            if($.session.get("tab_id")=='on'){
                myhtml += '<span>ტაბი<close cl="tab_id">X</close></span>';
            }else{
                $('#tab_id option:eq(0)').prop('selected', true);
            }
            if($.session.get("filter_1")=='on'){
                myhtml += '<span>შე. დამუშავებული<close cl="filter_1">X</close></span>';
            }
            if($.session.get("filter_2")=='on'){
                myhtml += '<span>შე. დაუმუშავებელი<close cl="filter_2">X</close></span>';
            }
            if($.session.get("filter_3")=='on'){
                myhtml += '<span>შე. უპასუხო<close cl="filter_3">X</close></span>';
            }
            if($.session.get("filter_4")=='on'){
                myhtml += '<span>შეხვედრა<close cl="filter_4">X</close></span>';
            }
            if($.session.get("filter_5")=='on'){
                myhtml += '<span>ინტერნეტი<close cl="filter_5">X</close></span>';
            }
            if($.session.get("filter_6")=='on'){
                myhtml += '<span>ტელეფონი<close cl="filter_6">X</close></span>';
            }
            if($.session.get("filter_7")=='on'){
                myhtml += '<span>გამცხადებელი<close cl="filter_7">X</close></span>';
            }

            $('.callapp_tabs').html(myhtml);
            LoadTable('index',colum_number,main_act,change_colum_main);
            $('#operator_id, #tab_id').trigger("chosen:updated");
        }




        $(document).on("click", "#search_ab_crm_pin", function () {

            GetDataTable("table_crm", aJaxURL, "get_list_crm", 9, "start_crm="+$("#start_crm").val()+"&end_crm="+$("#end_crm").val()+"&pin="+$("#check_ab_crm_pin").val()+"&phone="+$("#check_ab_crm_phone").val(), 0, [[50, -1], [50, "ყველა"]], 0, "desc", "", "<'F'lip>");
            SetEvents("", "", "", "table_crm", "crm_dialog", aJaxURL_crm);
            $("#table_crm_length").css('top','0px');

            setTimeout(function () {$('.ColVis, .dataTable_buttons').css('display', 'none');}, 90);
        });

        $(document).on("click", ".crm", function () {
            $("#check_ab_crm_pin").val($("#personal_pin").val());
            GetDataTable("table_crm", aJaxURL, "get_list_crm", 9, "start_crm="+$("#start_crm").val()+"&end_crm="+$("#end_crm").val()+"&pin="+$("#check_ab_crm_pin").val()+"&phone="+$("#check_ab_crm_phone").val(), 0, [[50, -1], [50, "ყველა"]], 0, "desc", "", "<'F'lip>");
            SetEvents("", "", "", "table_crm", "crm_dialog", aJaxURL_crm);
            $("#table_crm_length").css('top','0px');
            $("#search_ab_crm_pin").button();
            GetDate('start_crm');
            GetDate('start_crm');

            setTimeout(function () {$('.ColVis, .dataTable_buttons').css('display', 'none');}, 90);
        });



        $(document).on("click", ".newsnews", function () {
            GetDataTable("table_news", aJaxURL, "get_list_news", 6, "", 0, "", 0, "desc",'',"<'F'lip>");
            $("#table_news_length").css('top','0px');
            setTimeout(function () {$('.ColVis, .dataTable_buttons').css('display', 'none');}, 90);
        });

        $(document).on("change", "#chat_status", function () {

            toggleChatStatus('#chat_status', Number($(this).val()));

            $.ajax({
                url: aJaxURL,
                data: 'act=chat_on_off&value='+$(this).val(),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            // alert(data.error);
                            alert("Funny moments");
                        }else{

                        }
                    }
                }
            });
        });
        $(document).on("click", "#callapp_show_filter_button", function () {
            if($('.callapp_filter_body').attr('myvar') == 0){
                $('.callapp_filter_body').css('display','block');
                $('.callapp_filter_body').attr('myvar',1);

                $('#go_exel').css('top','264px');
                $("#shh").css('transform', 'rotate(0)');
            }else{
                $('.callapp_filter_body').css('display','none');
                $('.callapp_filter_body').attr('myvar',0);

                $('#go_exel').css('top','171px');
                $("#shh").css('transform', 'rotate(180deg)');
                
            }
        });
        $(document).on("click", "#callapp_show_filter_button2", function () {
            if($('.callapp_filter_body2').attr('myvar') == 0){
                $('.callapp_filter_body2').css('display','block');
                $('.callapp_filter_body2').attr('myvar',1);

                $("#shh2").css('transform', 'rotate(0)');
            }else{
                $('.callapp_filter_body2').css('display','none');
                $('.callapp_filter_body2').attr('myvar',0);

                $("#shh2").css('transform', 'rotate(180deg)');
                
            }
        });


        $(document).on("click", "#show_copy_prit_exel", function () {
            if($(this).attr('myvar') == 0){
                $('.ColVis,.dataTable_buttons').css('display','block');
                $(this).css('background','#2681DC');
                $(this).children('img').attr('src','media/images/icons/select_w.png');
                $(this).attr('myvar','1');
            }else{
                $('.ColVis,.dataTable_buttons').css('display','none');
                $(this).css('background','#E6F2F8');
                $(this).children('img').attr('src','media/images/icons/select.png');
                $(this).attr('myvar','0');
            }
        });
        $(document).on("click", "#show_station", function () {
            $(".flash_menu_link").css('background','rgb(255, 255, 255)');
            $(".flash_menu_link").css('color','rgb(38, 129, 220)');
            $(this).css('background','#ccc');
            $(this).css('color','#000');
            $("#whoami").val(1)
            if($("#check_state").val()==0){
                $('#pirveli').html('რიგი');
                $('#meore').html('შიდა ნომერი');
                $('#mesame').html('თანამშრომელი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ნომერი');
                $('#meeqvse').html('დრო');
                $('#meshvide').html('სახელი');

                $('#q_pirveli').html('რიგი');
                $('#q_meore').html('პოზიცია');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('სახელი');
                $('#q_mexute').html('ლოდინის დრო');
            }else{
                $('#mini_pirveli').html('შიდა ნომერი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');
                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('ნომერი');
            }
            // runAjax();
        });
        $(document).on("click", "#show_chat", function () {
            $(".flash_menu_link").css('background','rgb(255, 255, 255)');
            $(".flash_menu_link").css('color','rgb(38, 129, 220)');
            $(this).css('background','#ccc');
            $(this).css('color','#000');
            $("#whoami").val(0)
            if($("#check_state").val()==0){
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('ბრაუ.');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('ქვეყანა');
                $('#q_mesame').html('ბრაუზერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            }else{
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სახელი');
            }
            // runAjax();
        });
        $(document).on("click", "#show_site", function () {

            $(".flash_menu_link").css('background','rgb(255, 255, 255)');
            $(".flash_menu_link").css('color','rgb(38, 129, 220)');
            $(this).css('background','#ccc');
            $(this).css('color','#000');
            $("#whoami").val(2);
            if($("#check_state").val()==0){
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            }else{
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სურათი');
                $('#m_q_mesame').html('სახელი');
            }
            // runAjax();
        });

        $(document).on("click", "#show_mail", function () {

            $(".flash_menu_link").css('background','rgb(255, 255, 255)');
            $(".flash_menu_link").css('color','rgb(38, 129, 220)');
            $(this).css('background','#ccc');
            $(this).css('color','#000');
            $("#whoami").val(3);
            if($("#check_state").val()==0){
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            }else{
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სურათი');
                $('#m_q_mesame').html('სახელი');
            }
            // runAjax();
        });
        $(document).on("click", "#show_fbm", function () {
            $(".flash_menu_link").css('background','rgb(255, 255, 255)');
            $(".flash_menu_link").css('color','rgb(38, 129, 220)');
            $(this).css('background','#ccc');
            $(this).css('color','#000');
            $("#whoami").val(4);
            if($("#check_state").val()==0){
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            }else{
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სახელი');
            }
            // runAjax();
        });

        $(document).on("click", "#show_video", function () {
            $(".flash_menu_link").css('background','rgb(255, 255, 255)');
            $(".flash_menu_link").css('color','rgb(38, 129, 220)');
            $(this).css('background','#ccc');
            $(this).css('color','#000');
            $("#whoami").val(6);
            if($("#check_state").val()==0){
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            }else{
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სახელი');
            }
            // runAjax();
        });
       

        function listen(file){
            $('#auau').each(function(){
                this.pause(); // Stop playing
                this.currentTime = 0; // Reset time
            });
            var url = 'http://212.72.155.183:8000/' + file;
            $("#auau source").attr('src',url);
            $("#auau").load();
        }



        $(document).on("click", "#micr", function () {

            if(micr_val == 1){
                $('#micr').attr('src','media/images/icons/4.png');
                $(this).attr('value','0');
                micr_val = 0;
            }else{
                $('#micr').attr('src','media/images/icons/5.png');
                $(this).attr('value','1');
                micr_val = 1;
            }

            param 		         = new Object();
            param.act	         = "save_micr";
            param.activeStatus   = micr_val;

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                        }else{

                        }
                    }
                }
            });
        });





        // toggle dnd button
        $(document).on("click", "#incallToggleDnd", function() {

            // define extention from php session
            const phpSesExten = '<?php echo $_SESSION['EXTENSION']; ?>';

            if(phpSesExten === 0 || phpSesExten === "0" || phpSesExten === undefined || phpSesExten === null) {
                alert("თქვენ არ გაქვთ არჩეული ექსთენშენი, ამიტომ მოქმედების განხორციელება შეუძლებელია");
            }else{
                var activeStatus = $("#dnd_on_local_status").val();
                if(activeStatus==0){
                    $("#dnd_on_local_status").val(1)
                    var action = 'local';
                    activeStatus = 1;
                }else if(activeStatus == 1){
                    $("#dnd_on_local_status").val(2)
                    var action = 'dndon';
                    activeStatus = 2;
                }else if(activeStatus == 2){
                    $("#dnd_on_local_status").val(0)
                    var action = 'dndoff';
                    activeStatus = 0;
                }

                param 		         = new Object();
                param.act	         = "change_incall_dnd_status";
                param.activeStatus   = $("#dnd_on_local_status").val();

                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                            }else{
//     					$.post("AsteriskManager/dndAction.php", {
//     						action: action
//     					}, result => {
                                if(activeStatus==0){
                                    $("#incallToggleDnd").attr('src','media/images/icons/comunication/Phone_ON.png');
                                }else if(activeStatus==1){
                                    $("#incallToggleDnd").attr('src','media/images/icons/comunication/Phone_INSIDE.png');
                                }else if(activeStatus==2){
                                    $("#incallToggleDnd").attr('src','media/images/icons/comunication/Phone_OFF.png');
                                }
                                //});
                            }
                        }
                    }
                });
            }
        });

        // open activitie chooe container
        $(document).on("click", ".callapp-op-activitie-wrapper", function(event) {

            // stop propagation
            event.stopPropagation();

            // define open status
            const openStatus = $("#callapp_op_activitie_choose").data("active");

            // check active status
            if(openStatus) {
                $("#callapp_op_activitie_choose").data("active", false).removeClass("on");
            } else {
                $("#callapp_op_activitie_choose").data("active", true).addClass("on");
            }

        });

        // choose activitie
        $(document).on("click", "#callapp_op_activitie_choose li:not(.is-active)", function() {

            // reactivate activitie in list
            $("#callapp_op_activitie_choose li").removeClass("is-active");
            $(this).addClass("is-active");

            // define activitie data
            $D.chosedActivitieId = $(this).data("id");
            const activitieColor = $(this).data("color");
            const activitieName = $(this).find(".activitie-name").text();

            // define request data
            const data = {
                act: "change_incall_activitie_status",
                activitieId: $D.chosedActivitieId,
                type: "on"
            };

            // send ajax request
            $.post(aJaxURL, data, responce => {

                // restruct responce
                const { error } = responce;

                // check for error
                if(error !== "") {
                    alert(error);
                } else {

                    // change color label
                    $(".callapp-op-activitie-color").css({background: activitieColor});

                    // fix selected activitie
                    $(".callapp-op-activitie-choose-receiver").text(activitieName);

                    // start time count
                    $D.activitieOnTimer();

                    // chat status off
                    toggleChatStatus('#chat_status', 1);

                    // on dnd
                    $.post("AsteriskManager/dndAction.php", {
                        action: "dndon"
                    });
                }

            });

        });

        // zero fixer
        function zeroFixer(a) {
            return a < 10 ? `0${a}` : a;
        }

        //open template dialog
        $(document).on("click", "#smsTemplate", function() {

            //request data
            let data = {
                act: "get_sms_template_dialog"
            };

            //buttons
            let buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };

            //ajax request
            $.post("server-side/call/waiters.action.php", data, structure => {

                //open dialog malibu
                $("#sms-templates").html(structure.html);
                GetDialog("sms-templates","530","auto", buttons, "right+100 top");

                GetDataTable("sms_template_table", aJaxURL, "phone_directory", 4, "", 0, "", 2, "desc", '', "<'F'lip>");
                $("#sms_template_table_length").css('top','0px');
            });

        });
        function LoadDialog(fNm){
            $("#loading").show();
            if(fNm=='add-edit-form-comunications'){

                var buttons =
                    {
                        // "ipaddres_block": {
                        //     text: "IP დაბლოკვა",
                        //     id: "ipaddres_block"
                        // },
                        // "num_block": {
                        //     text: "ნომრის დაბლოკვა",
                        //     id: "num_block"
                        // },
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                var buttons1 = {
                                    "done": {
                                        text: "კი",
                                        id: "save-yes",
                                        click: function () {
                                            $("#add-edit-form-comunications").dialog("close");
                                            $(this).dialog("close");
                                        }
                                    },
                                    "no": {
                                        text: "არა",
                                        id: "save-no",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                };
                                GetDialog("hint_close", 300, "auto", buttons1);
                                $("#save-yes").css('float', 'right');
                                $("#save-no").css('float', 'right');
                            }
                        },
                        "done": {
                            text: "შენახვა",
                            id: "save-dialog",
                            class: "save-incomming-main-dialog2"
                        }

                    };


                /* Dialog Form Selector Name, Buttons Array */
                var width = 1000;
                GetDialog("add-edit-form-comunications", '700', "auto",buttons,'center top');
                $(".save-incomming-main-dialog2").button();
                $('.add-edit-form-class .ui-dialog-titlebar-close').remove();
                $('.add-edit-form-class').css('left','84.5px');
                $('.add-edit-form-class').css('top','0px');
                $('.ui-dialog-buttonset').css('width','100%');
                $("#cancel-dialog").css('float','right');
                $("#save-dialog").css('float','right');
                $("#search_ab_pin").button();
                $('#add_comment').button();
                GetDateTimes("vizit_datetime");
                $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming_status_1, #incomming_status_1_1, #inc_status_a, #company, #manager, #space_type, #chat_language, #s_u_status").chosen({ search_contains: true });
                var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
                
                $("#my_site_resume_chosen").css('width','560px');
                GetDate('start_check');
                GetDate('end_check');
                //GetDataTable("table_history", Comunications_URL, "get_list_history", 7, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id=''", 0, dLength, 1, "desc", '', "<'F'lip>");

                $("#add_project_info").button();
                GetDataTable("project_ifo_table", Comunications_URL, "get_list_info_project", 4, "&id="+$('#hidde_incomming_call_id').val(), 0, dLength, 1, "desc", '', "<'F'lip>");
				$("#project_ifo_table_length").css('top','0px');
				$("#project_ifo_table_length select").css('height','16px');
				$("#project_ifo_table_length select").css('width','56px');
                $("#table_history_length").css('top','0px');
                $("#table_sms_length").css('top','0px');

                $(".jquery-ui-button").button();

                $("#call_content").off();
                $("#call_content1").off();
                $("#send_message").off();

               

                $.ajax({
                    url: Comunications_URL,
                    data: {
                        'act'		:'get_all_chat',
                        'chat_id'	:$('#chat_original_id').val(),
                        'source'	:$("#chat_source").val()
                    },
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
            
                            }else{
                                $('#log').html('');
                                $('#log').html(data.sms);
                                chat_detail_id=data.chat_detail_id;
                                $("#chat_name").val(data.name);
                                if(data.chat_user_id =='' ){
                                    $("#start_chat_wrap").show();
                                }
                                else{
                                    $("#start_chat_wrap").hide();
                                }
                                
                                document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
                                jQuery("time.timeago").timeago();
                                $('#chat_hist').css('border-bottom','2px solid transparent');
                                $('#chat_live').css('border-bottom','2px solid #333');
                                all_chats = 1;
                                var source = $('#chat_source').val() || 'phone';
        						var index = $('#incoming_chat_tabs a[href="#incoming_chat_tab_'+source).parent().index();
        						$("#incoming_chat_tabs").tabs("option", "active", index);
                            }
                        }
                    }
                });

                $("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });
                $("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });

                $("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), {
                    width: 360,
                    multiple: true,
                    matchContains: true
                });

                //////////
                var id = $("#incomming_id").val();
                var cat_id = $("#category_parent_id").val();

                if(id != '' && cat_id == 407){
                    $("#additional").removeClass('hidden');
                }
                if($('#ipaddres').val() == 0 || $('#ipaddres').val() == ''){
                    $('#ipaddres_block').css('display','none');
                }
                GetDateTimes("problem_date");
                GetDateTimes("transaction_date_time");


                if($("#tab_id").val() == 5){
                    $("#num_block, .save-incomming-main-dialog2").css('display','none');
                }else{
                    $("#num_block, .save-incomming-main-dialog2").css('display','');
                }


                if(($('#imchat').val()==1 && $('#ast_source').val() == '')){
                    $('#source_id').val(4);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else if(($('#fb_chat').val()>0 && $('#ast_source').val() == '')){
                    $('#source_id').val(6);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else if(($('#mail_chat').val()>0 && $('#ast_source').val() == '')){
                    $('#source_id').val(7);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else if(($('#viber_chat').val()>0 && $('#ast_source').val() == '')){
                    $('#source_id').val(14);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else{
                    //$('#source_id option:eq(1)').prop('selected', true);
                    $('#chatcontent,#gare_div').css('display','none');
                    $('#chatistema').css('width','210px');
                    $('.add-edit-form-class').css('width','879px');
                    $("#cancel-chat").css('display','none');
                    $('.add-edit-form-class .ui-dialog-title').html('ზარის დამუშავება');
                    $('.add-edit-form-class .ui-dialog-titlebar').css('background', '#673AB7');
                    $('.add-edit-form-class .ui-dialog-title').css('color', '#fff');
                }

               

                jQuery("time.timeago").timeago();
                //getchats();
                $('#choose_button').button();


                ///////////////////
                // ai dunia      //
                ///////////////////
                // if($('#add-edit-form-comunications input').length != 0){

                    /*
                $.ajax({
                    url: "server-side/call/getchats.action.php",
                    data: "act=getchats",
                    dataType: "json",
                    success: function(data) {
                        if(data.count != 0 && $D.sessionStatus == 1){
                            $('#chat_count').css('background','#FFCC00');
                            setTimeout(function(){
                                $('#chat_count').css('background','#F44336');
                                }, 400);
                            var audio = new Audio('sms.mp3');
                            if(micr_val == 0){
                                audio.play();
                                }
                        }else{
                            $('#chat_count').css('background','none');
                        }

                        $("#incoming_chat_tab_chat").html(data.page.chat);
                        $("#incoming_chat_tab_site").html(data.page.site);
                        $("#incoming_chat_tab_mail").html(data.page.mail);
                        $("#incoming_chat_tab_fbm").html(data.page.fbm);
                        $('#chat_count').attr('first_id', data.first_id);
                    }
                });
                */
                //     if($('#chat_user_id').attr('ext') !='0'){
                //         $.ajax({
                //             url: "AsteriskManager/checkme.php",
                //             data: "ext="+$('#chat_user_id').attr('ext'),
                //             dataType: "json",
                //             success: function(data) {
                //                 //console.log(data.phone);
                //                 $('#chat_call_live').attr('src','media/images/icons/'+data.icon);
                //                 if(data.phone != '' && data.phone != null){
                //                     if(data.ipaddres == '2004'){
                //                         iporphone = data.ipaddres;
                //                     }else{
                //                         iporphone = data.phone;
                //                     }
                //                     $('#chat_call_queue').css('display','table-row');
                //                     $('#chat_call_number').html(iporphone);
                //                     $('#chat_call_duration').html(data.duration);
                //                     $('#chat_call_number').attr('extention',$('#chat_user_id').attr('ext'));
                //                     $('#chat_call_number').attr('number',data.phone);
                //                     $('#chat_call_number').attr('ipaddres',data.ipaddres);
                //                 }else{
                //                     $('#chat_call_queue').css('display','none');
                //                     $('#chat_call_number').html('');
                //                     $('#chat_call_duration').html('');
                //                 }
                //             }
                //         });
                //     }else{
                //         $('#main_call_chat').css('display','none');
                //     }
                // }
                all_chats= 1;
                $( "#incoming_chat_tabs").tabs();

                ////////////////////
            }else if(fNm=='crm_dialog'){
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                };
                GetDialog("crm_dialog", 900, "auto", buttons, "top");
                GetDataTable("example_all_file", Comunications_URL_crm, "get_list", 3, "request_table_id=" + $("#request_table_id").val(), 0, "", 1, "desc", "", '');
                SetEvents("", "", "", "example_all_file", "crm_file_dialog", Comunications_URL_crm_file);
                setTimeout(function () {
                    $('.ColVis, .dataTable_buttons').css('display', 'none');
                }, 90);
            }else if(fNm=='crm_file_dialog'){
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                };
                GetDialog("crm_file_dialog", "auto", "auto", buttons, "top");

            }else{
                $( "#incoming_chat_tabs").tabs();
                var buttons = {
                    //
                    // "ipaddres_block": {
                    //     text: "IP დაბლოკვა",
                    //     id: "ipaddres_block"
                    // },
                    // "num_block": {
                    //     text: "ნომრის დაბლოკვა",
                    //     id: "num_block"
                    // },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            var buttons1 = {
                                "done": {
                                    text: "კი",
                                    id: "save-yes",
                                    click: function () {
                                        $("#add-edit-form-comunications").dialog("close");
                                        $(this).dialog("close");
                                    }
                                },
                                "no": {
                                    text: "არა",
                                    id: "save-no",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            };
                            GetDialog("hint_close", 300, "auto", buttons1);
                            $("#save-yes").css('float', 'right');
                            $("#save-no").css('float', 'right');
                        }
                    },
                    "done": {
                        text: "შენახვა",
                        id: "save-dialog",
                        class: "save-incomming-main-dialog2"
                    }

                };


                /* Dialog Form Selector Name, Buttons Array */
                var width = 1000;
                GetDialog("add-edit-form-comunications", width, "auto",buttons );
                $(".save-incomming-main-dialog2").button();
                $('.add-edit-form-class .ui-dialog-titlebar-close').remove();
                $('.add-edit-form-class').css('left','84.5px');
                $('.add-edit-form-class').css('top','0px');
                $('.ui-dialog-buttonset').css('width','100%');
                $("#cancel-dialog").css('float','right');
                $("#save-dialog").css('float','right');
                $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming_status_1, #incomming_status_1_1, #inc_status_a, #company, #space_type, #chat_language,  #s_u_status").chosen({ search_contains: true });

                $("#search_ab_pin").button();
                $('#add_comment').button();
                $("#my_site_resume_chosen").css('width','560px')
                var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
                GetDate('start_check');
                GetDate('end_check');
                //GetDataTable("table_history", Comunications_URL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
                $("#add_project_info").button();
                GetDataTable("project_ifo_table", Comunications_URL, "get_list_info_project", 4, "&id="+$('#hidde_incomming_call_id').val(), 0, dLength, 1, "desc", '', "<'F'lip>");
                $("#project_ifo_table_length").css('top','0px');
				$("#project_ifo_table_length select").css('height','16px');
				$("#project_ifo_table_length select").css('width','56px');
                $("#table_history_length").css('top','0px');
                $(".jquery-ui-button").button();
                GetTabs('tabs_sport');

                $("#call_content").off();
                $("#call_content1").off();
                $("#send_message").off();

                $("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });

                $("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });

                $("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id="+$("#source_id").val()+"&site="+$("#my_site").chosen().val(), {
                    width: 360,
                    multiple: true,
                    matchContains: true
                });

                var id = $("#incomming_id").val();

                if($("#tab_id").val() == 5){
                    $("#num_block, .save-incomming-main-dialog2").css('display','none');
                }else{
                    $("#num_block, .save-incomming-main-dialog2").css('display','');
                }

                if(($('#imchat').val()==1 && $('#ast_source').val() == '')){
                    $('#source_id').val(4);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else if(($('#fb_chat').val()>0 && $('#ast_source').val() == '')){
                    $('#source_id').val(6);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else if(($('#mail_chat').val()>0 && $('#ast_source').val() == '')){
                    $('#source_id').val(7);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else if(($('#viber_chat').val()>0 && $('#ast_source').val() == '')){
                    $('#source_id').val(14);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display','none');
                }else{
                    //$('#source_id option:eq(1)').prop('selected', true);
                    $('#chatcontent,#gare_div').css('display','none');
                    $('#chatistema').css('width','210px');
                    $('.add-edit-form-class').css('width','879px');
                    $("#cancel-chat").css('display','none');
                    $('.add-edit-form-class .ui-dialog-title').html('ზარის დამუშავება');
                    $('.add-edit-form-class .ui-dialog-titlebar').css('background', '#673AB7');
                    $('.add-edit-form-class .ui-dialog-title').css('color', '#fff');
                }
				


                jQuery("time.timeago").timeago();
                //getchats();
                $('#choose_button').button();


                ///////////////////
                // ai dunia      //
                ///////////////////
                if($('#add-edit-form-comunications input').length != 0){

                    /*
                    $.ajax({
                        url: "server-side/call/getchats.action.php",
                        data: "act=getchats",
                        dataType: "json",
                        success: function(data) {
                            if(data.count != 0 && $D.sessionStatus == 1){
                                $('#chat_count').css('background','#FFCC00');
                                setTimeout(function(){
                                    $('#chat_count').css('background','#F44336');
                                    }, 400);
                                var audio = new Audio('sms.mp3');
                                if(micr_val == 0){
                                    audio.play();
                                    }
                            }else{
                                $('#chat_count').css('background','none');
                            }

                            $("#incoming_chat_tab_chat").html(data.page.chat);
                            $("#incoming_chat_tab_site").html(data.page.site);
                            $("#incoming_chat_tab_mail").html(data.page.mail);
                            $("#incoming_chat_tab_fbm").html(data.page.fbm);

                            $('#chat_count').attr('first_id', data.first_id);
                        }
                    });
                    if($('#chat_user_id').attr('ext') !='0'){
                        $.ajax({
                            url: "AsteriskManager/checkme.php",
                            data: "ext="+$('#chat_user_id').attr('ext'),
                            dataType: "json",
                            success: function(data) {
                                //console.log(data.phone);
                                $('#chat_call_live').attr('src','media/images/icons/'+data.icon);
                                if(data.phone != '' && data.phone != null){
                                    if(data.ipaddres == '2004'){
                                        iporphone = data.ipaddres;
                                    }else{
                                        iporphone = data.phone;
                                    }
                                    $('#chat_call_queue').css('display','table-row');
                                    $('#chat_call_number').html(iporphone);
                                    $('#chat_call_duration').html(data.duration);
                                    $('#chat_call_number').attr('extention',$('#chat_user_id').attr('ext'));
                                    $('#chat_call_number').attr('number',data.phone);
                                    $('#chat_call_number').attr('ipaddres',data.ipaddres);
                                }else{
                                    $('#chat_call_queue').css('display','none');
                                    $('#chat_call_number').html('');
                                    $('#chat_call_duration').html('');
                                }
                            }
                        });
                    }else{
                        $('#main_call_chat').css('display','none');
                    }
                    */
                }
            }
        }
        // download sms shablon
        $(document).on("click", ".download_shablon", function () {

            //define variables
            var message = $(this).data("message");
            var sms_id   = $(this).attr("sms_id");

            //check length of message text
            if(message != "" && message.length <= 150){

                $('#newSmsText').val(message);
                $('#smsCharCounter').val((message.length)+'/150');

                // set choosed sms id
                $D.choosedSmsId = sms_id;

            }else if(message.length > 150) {
                alert("შაბლონის ტექსტი შეიცავს დასაშვებზე (150) მეტ სიმბოლოს (" + message.length + ")");
            }

            //close sms template dialog
            $("#sms-templates").dialog("close");

        });


        // send sms

        //new sms textarea key up
        $(document).on("keyup", "#newSmsText", function() {

            charCounter(this, "#smsCharCounter");

        });

        //new sms textarea focus
        $(document).on("keydown", "#newSmsText", function(e) {

            if(e.key !== "Backspace" && e.key !== "Delete") {
                textAreaFocusAllowed(this, "#smsCharCounter");
            }

        });
        //textarea character counter
        function charCounter(checkobj, updateobj) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(updateobj).data("limit");

            $(updateobj).val(`${textLength}/${charLimit}`);

            if(textLength === charLimit) {
                $(checkobj).blur();
            }

        }

        //text area focus allowed
        function textAreaFocusAllowed(checkobj, limitinput) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(limitinput).data("limit");

            if(textLength >= charLimit) {
                $(checkobj).blur();
            }

        }

        $(document).on("click", "#web_chat_checkbox", function() {
            if($("input[id='web_chat_checkbox']:checked").val()==1){
                $("#VebChatImg").attr('src','media/images/icons/comunication/Chat.png');
            }else{
                $("#VebChatImg").attr('src','media/images/icons/comunication/Chat_OFF.png');
            }
        });

        $(document).on("click", "#site_chat_checkbox", function() {
            if($("input[id='site_chat_checkbox']:checked").val()==1){
                // $("#siteImg").attr('src','media/images/icons/comunication/my_site.ico');
            }else{
                // $("#siteImg").attr('src','media/images/icons/comunication/OFF-my.png');
            }
        });

        $(document).on("click", "#messanger_checkbox", function() {
            if($("input[id='messanger_checkbox']:checked").val()==1){
                $("#MessengerImg").attr('src','media/images/icons/comunication/Messenger.png');
            }else{
                $("#MessengerImg").attr('src','media/images/icons/comunication/Messenger_OFF.png');
            }
        });

        $(document).on("click", "#mail_chat_checkbox", function() {
            if($("input[id='mail_chat_checkbox']:checked").val()==1){
                $("#MailImg").attr('src','media/images/icons/comunication/E-MAIL.png');
            }else{
                $("#MailImg").attr('src','media/images/icons/comunication/E-MAIL_OFF.png');
            }
        });

        $(document).on("click", "#zomm_div", function() {
            if($(this).attr('check_zoom')==0){
                $(this).attr('check_zoom', '1');
                $(this).html('<<');
                $("#right_side").css('z-index','-1');
                $("#chat_scroll").css('width','907px');
            }else{
                $(this).attr('check_zoom', '0');
                $(this).html('>>');
                $("#right_side").css('z-index','0');
                $("#chat_scroll").css('width','288px');
            }
        });


        $(document).on("click", ".comunication_checkbox", function() {
            param 		              = new Object();
            param.act	              = "save_user_comunication";

            param.web_chat_checkbox   = $("input[id='web_chat_checkbox']:checked").val();
            param.site_chat_checkbox  = $("input[id='site_chat_checkbox']:checked").val();
            param.messanger_checkbox  = $("input[id='messanger_checkbox']:checked").val();
            param.mail_chat_checkbox  = $("input[id='mail_chat_checkbox']:checked").val();
            param.video_call_checkbox = $("input[id='video_call_checkbox']:checked").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                        }else{
                        }
                    }
                }
            });
        });

        // $(document).on("change", "#from_mail_id", function() {
        // 	param 	  = new Object();
        // 	param.act = "get_signature";

        // 	param.site_id   = $(this).val();

        // 	$.ajax({
        //         url: aJaxURL,
        // 	    data: param,
        //         success: function(data) {
        // 			if(typeof(data.error) != 'undefined'){
        // 				if(data.error != ''){
        // 				}else{
        // 					old_text = $("#input").val().split("<br>");
        // 					text     = old_text[1];
        // 					$("#input").html(text+"<br><br><br><br>"+data.signature);
        // 					setTimeout(() => {
        // 						var activeEditor = tinyMCE.get('input');
        // 						var content = text+"<br><br><br><br>"+data.signature;
        // 						activeEditor.setContent(content);
        // 				    }, 100);
        // 				}
        // 			}
        // 	    }
        //     });
        // });

        function check_user_comunication(){
            param 	  = new Object();
            param.act = "check_user_comunication";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                        }else{
                            if(data.web_chat_checkbox==1){
                                $('#web_chat_checkbox').attr('checked','checked');
                                $("#VebChatImg").attr('src','media/images/icons/comunication/Chat.png');
                            }

                            if(data.site_chat_checkbox==1){
                                $('#site_chat_checkbox').attr('checked','checked');
                                // $("#siteImg").attr('src','media/images/icons/comunication/my_site.ico');
                            }

                            if(data.messanger_checkbox==1){
                                $('#messanger_checkbox').attr('checked','checked');
                                $("#MessengerImg").attr('src','media/images/icons/comunication/Messenger.png');
                            }

                            if(data.mail_chat_checkbox==1){
                                $('#mail_chat_checkbox').attr('checked','checked');
                                $("#MailImg").attr('src','media/images/icons/comunication/E-MAIL.png');
                            }

                            if(data.video_call_checkbox==1){
                                $('#video_call_checkbox').attr('checked','checked');
                                $("#VideoImg").attr('src','media/images/icons/comunication/Video.png');
                            }
                        }
                    }
                }
            });
        }



        $(document).on("click", "#smsPhoneDir", () => {

            //define data
            let data = {
                act: "phone_dir_list",
                type: 2
            };

            //buttons
            let buttons = {
                "choose": {
                    text: "არჩევა",
                    id: "choose-abonents",
                    click: function () {

                        //execute function whitch collects checked items value
                        choosePhoneFromDir(this);

                    }
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };

            //send dialog opening request to server side
            $.post(aJaxURL, data, result => {

                //insert received html structure in relevant dialog
                $("#phone-directory-list").html(result.html);

                //open relevant dialog
                GetDialog("phone-directory-list", 960, "auto", buttons, 'center top');
                GetDataTable("phoneDirectoryList", aJaxURL, "get_phone_dir_list", 4, "&type="+$("#view_type").val(), 0, "", 2, "desc", '', "");
            });

        });

        //get conclusion container data
        function getConclusionContainerData(input) {

            // define variables
            const inputWidth = $(input).outerWidth();
            const charLength = $(input).text().length;
            const nextInput = $(input).next("br").next(".input-div");
            const nextLine = nextInput.length;

            // return data
            return {
                nextInput,
                nextLine,
                inputWidth,
                charLength
            }

        }
        $(document).on("click", ".itm-unit:not(.itm-unit-active)", function() {
            //define variables
            let selector = $(this).data("selector");

            //deactivate all menu items and activate relevant one
            $(".itm-unit").removeClass("itm-unit-active");
            $(this).addClass("itm-unit-active");

            //deactivate info sections and activate menu relevant one
            $(".inc-info-section").removeClass("is-active");
            $(`.inc-info-section[data-selector='${selector}']`).addClass("is-active");

            //stylize chosen objects
            setTimeout(function() {
                $(".iiss-sc-unit").find(".chosen-container").css({width:"100%"});
            }, 10);
        });
        function infoEmailButInit(category) {

            let modifiedCategory = category.charAt(0).toUpperCase() + category.slice(1);
            return `mailTo${modifiedCategory}`;

        }
        //function which chooses numbers which was selected in phone directory
        function choosePhoneFromDir(dialogobj) {

            //define variables
            let checkBoxCollector = [];
            let uncheckedCollector = [];
            var receiver = '';
            if($("#view_type").val() == 1){
                var receiver = $("#smsAddressee");
            }else{
                var receiver = $("#mail_address");
            }

            let receiverData = receiver.val();
            let returnData;

            //collect checkbox checked values
            $(".pdl-check").each(function (i) {

                //check if checkbox is checked
                if ($(this).is(":checked")) {

                    //get value of checked inut and reach to data collector
                    checkBoxCollector.push($(this).val().trim());
                }

            });

            //collect checkbox unchecked values
            $(".pdl-check").each(function (i) {

                //check if checkbox is checked
                if (!$(this).is(":checked")) {

                    //get value of checked inut and reach to data collector
                    uncheckedCollector.push($(this).val().trim());
                }

            });
            //check if there is already data in receiver input
            if (receiverData !== "") {

                checkBoxCollector = checkBoxCollector.concat(receiverData.split(",").filter(function (item) {
                    return checkBoxCollector.indexOf(item.trim()) < 0;
                }));

            }

            //filter data by unchecked box values
            checkBoxCollector = checkBoxCollector.filter(function (item) {
                return uncheckedCollector.indexOf(item) < 0;
            });

            //join array with comma
            returnData = checkBoxCollector.join();

            //send collected data to receiver
            receiver.val(returnData);

            //close dialog of phone directory list
            $(dialogobj).dialog("close");

        }

        $(document).on("click", "#choose_button_mail", function () {
            $("#choose_mail_file").click();
        });

        $(document).on("change", "#choose_mail_file", function () {
            var file_url = $(this).val();
            var file_name = this.files[0].name;
            var file_size = this.files[0].size;
            var file_type = file_url.split('.').pop().toLowerCase();
            var path = "../../mailmyge/file_attachment/";

            if ($.inArray(file_type, ['pdf', 'pptx', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv']) == -1) {
                alert("დაშვებულია მხოლოდ 'pdf', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv' გაფართოება");
            } else if (file_size > '15728639') {
                alert("ფაილის ზომა 15MB-ზე მეტია");
            } else {
                $.ajaxFileUpload({
                    url: "server-side/upload/file.action.php",
                    secureuri: false,
                    fileElementId: "choose_mail_file",
                    dataType: 'json',
                    data: {
                        act: "upload_file_mail",
                        button_id: "choose_mail_file",
                        table_name: 'mail',
                        file_name: Math.ceil(Math.random() * 99999999999),
                        file_name_original: file_name,
                        file_type: file_type,
                        file_size: file_size,
                        path: path,
                        table_id: $("#hidden_increment").val(),

                    },
                    success: function (data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                var tbody = '';
                                $("#choose_mail_file").val('');
                                for (i = 0; i <= data.page.length; i++) {
                                    tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                                    tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                                    tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                                    tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
                                    tbody += "<input type='hidden' class=\"attachment_address\" value='"+ data.page[i].rand_name + "'>";
                                    $("#paste_files1").html(tbody);
                                }


                            }
                        }

                    }
                });
            }
        });

        function delete_file1(id) {
            $.ajax({
                url: "server-side/upload/file.action.php",
                data: "act=delete_file1&file_id=" + id + "&table_name=mail",
                success: function (data) {

                    var tbody = '';
                    if (data.page.length == 0) {
                        $("#paste_files1").html('');
                    }else{
                        for (i = 0; i <= data.page.length; i++) {
                            tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                            tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                            tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                            tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
                            tbody += "<input type='hidden' class=\"attachment_address\" value='"+ data.page[i].rand_name + "'>";
                            $("#paste_files1").html(tbody);
                        }
                    }
                }
            });
        }





        //chat question functions

        


        $(document).on("click","#start_chat",function(){
            $.ajax({
                url: aJaxURL,
                data:{
                    act:"set_user",
                    name:$("#chat_name").val(),
                    id : $('#chat_original_id').val()
                },
                success:function(data){
                    var name = $("#chat_name").val();
                    $("#send_message").html(`გამარჯობა ${name}, რით შემიძლია დაგეხმაროთ ?`);
                    $("#send_chat_program").trigger("click");
                    $("#start_chat_wrap").hide();
                }
            })
        })

        function get_sub_tab4(id, control_tab) {
            document.getElementById('control_tab_4_0').style.display = "none";
            document.getElementById('control_tab_4_1').style.display = "none";
            document.getElementById(control_tab).style.display = "block";

            document.getElementById("control_tab_id_4_0").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_0").style.borderBottomColor = "gray";

            document.getElementById("control_tab_id_4_1").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_1").style.borderBottomColor = "gray";

            document.getElementById(id).style.borderBottomWidth = "2px";
            document.getElementById(id).style.borderBottomColor = "#0b5c9a";
            document.getElementById(id).children[0].style.color = "black";


        }
        //chat question functions END
    </script>
    <style type="text/css">
        .callapp_tabs{
            margin-top: 5px;
            /* margin-bottom: 5px; */
            float: right;
            width: 100%;

        }
        .callapp_tabs span{
            color: #FFF;
            border-radius: 5px;
            padding: 5px;
            float: left;
            margin: 0 3px 0 3px;
            background: #2681DC;
            font-weight: bold;
            font-size: 11px;
            margin-bottom: 2px;
        }

        .callapp_tabs span close{
            cursor: pointer;
            margin-left: 5px;
        }

        .callapp_head{
            font-family: pvn;
            font-weight: bold;
            font-size: 20px;
            color: #2681DC;
        }
        .callapp_head select {
            position: relative;
            top: -2px;
        }

        #chatcontent hr {
            border-top-width: 0px;
        }

        .callapp_head_hr{
            border: 1px solid #2681DC;
        }
        .callapp_refresh{
            padding: 5px;
            border-radius:8px;
            color:#FFF;
            background: #9AAF24;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }
        .comunication_button{
            padding: 5px;
            border-radius:8px;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }
        .callapp_dnd_button {
            width: 40px; height: 40px;
            background: none;
            border: none;
            cursor: pointer;
            margin: 0 5px;
            position: relative;
            color: #7f8c8d;
            font-size: 21px;
        }
        
        .callapp_filter_body{
            width: 1030px;
            height: 187px;
            padding: 8px 0px 0px 0px;
            margin-bottom: 0px;
        }
        .callapp_filter_body span {
            float: left;
            margin-right: 10px;
            height: 22px;
        }
        .callapp_filter_body span label {
            color: #555;
            font-weight: bold;
            /* margin-left: 20px; */
        }
     
        .callapp_filter_header{
            color: #2681DC;
            font-family: pvn;
            font-weight: bold;
        }

        .ColVis, .dataTable_buttons{
            z-index: 50;
        }
        /* #flesh_panel{
            height: auto;
            width: 300px;
            position: absolute;
            top: 0;
            padding: 15px;
            right: 2px;
            z-index: 49;
            background: #FFF;
        } */
       
       
        #table_right_menu{
            top: 39px;
            z-index: 1;
        }
      
        /* #table_index_wrapper .ui-widget-header{
            height: 55px;
        } */

        /* -SMS DIALOG STYLES- */
        .new-sms-row {
            width: 100%; height: auto;
            margin-top: 11.3px;
            box-sizing: border-box;
        }

        .new-sms-row:last-child {
            margin-top: 4px;
        }

        .new-sms-row.grid {
            display: grid;
            grid-template-columns: repeat(2, 50%);
        }

        .nsrg-col {
            padding-right: 10px;
        }

        .nsrg-col:last-child {
            padding-right: 0;
        }

        .new-sms-input-holder {
            width: 100%; min-height: 27px;
            display: flex;
        }

        .new-sms-input-holder input {
            height: 18px;
            flex-grow: 1;
            font-size: 11.3px;
        }

        .new-sms-input-holder textarea {
            height: 75%;
            min-height: 20px;
            flex-grow: 1;
            font-size: 11.3px;
            resize: vertical;
        }

        #smsCopyNum { /*button: copy*/
            margin: 0 5px;
        }

        #smsTemplate { /*button: open template*/
            margin-right: 0;
        }

        #smsNewNum i{ /*icon: add new number in send new sms child*/
            font-size: 14px;
        }

        #smsCharCounter { /*input: sms character counter*/
            width: 55px; height: 18px;
        }

        #smsCharCounter { /*input: send new sms character counter*/
            position: relative;
            top: 0;
            text-align: center;
        }

        #sendNewSms { /*button: send new message action inplement*/
            float: right;
        }

        .empty-sms-shablon {
            width: 100%; height: auto;
            padding: 10px 0;
            text-align: center;
            font-family: pvn;
            font-weight: bold;
        }

        #box-table-b1{
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            text-align: center;
            border-collapse: collapse;
            border-top: 7px solid #70A9D2;
            border-bottom: 7px solid #70A9D2;
            width: 300px;
        }

        #box-table-b th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #start_chat_wrap{
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
            background: #e6f6ec;
            opacity: 0.8;
            display: none;
        }
        #start_chat{
            margin-top: 60px;
    margin-left: 84px;
    padding: 0px 9px;
    border: none;
    border-radius: 7px;
    background: #009658;
    color: #FFF;
    cursor: pointer;
    position: absolute;
        }
        #start_chat:hover{
            background: #01c26e;
        }

        #box-table-b1 th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #box-table-b td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        #box-table-b1 td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        .download_shablon {

            width: 100%;
            height: 20px;
            background-color:#4997ab;
            border-radius:2px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:14px;
            border:0px;
            text-decoration:none;
            text-overflow: ellipsis;
        }
        #comucation_status{
            position: absolute;
            z-index: 25000000;
            background: #fff;
            width: 199px;
            display: block;
            box-shadow: 0px 0px 25px #888888;
            left:-55%;
        }

        .download_shablon:hover {
            background-color:#ffffff;
            color:#4997ab;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
        }

        .ui-dialog-titlebar-close {
            display: none;
        }

        .imagespan{
            padding: 0px 0px 0px 10px;
        }

        .comunicationTr{
            border-top: 0.5px #c1cbd0 solid;
        }

        .comunication_name{
            font-family: pvn;
            font-weight: bold;
            font-size: 13px;
            color: #2681DC;
        }

        
        .download10{
        	background-color:#7d3daf;
        	border-radius:0px;
        	display:inline-block;
        	cursor:pointer;
        	color:#ffffff !important;
        	font-size:12px;
        	border:0px !important;
        	text-decoration:none;
        	text-overflow: ellipsis;
        	width: 100%;
        	font-weight: normal !important;
        }
        th [role="listbox"]{
            display: none;
        }
        .status_tabs{
            display:-webkit-inline-box;
            text-align: center;
        }
        .status_tabs li{
            padding: 10px;
            margin-right:10px;
            border-radius: 15px;
            cursor: pointer;
        }
        .status_tabs li[aria-selected="true"]{
            background-color:yellow;
            padding: 10px;
            margin-right:10px;
            border-radius: 15px;
            cursor: pointer;
        }

    </style>
    <script>
        $(document).on('click', '.status_tabs li', function(){
            $(".status_tabs li").each(function() {
                $(this).attr('aria-selected','false');
            });
            $(this).attr('aria-selected','true')
            var sub_status = $(this).attr('id');
            var post_data = "&sub_status="+sub_status;
            LoadKendoTable(post_data);
        });
        $(document).on('click', '#create_campaign', function(){
            // ACTION
            param = new Object();
            param.act   = "create_campaign";
            var compaignPhones = [];
			var entityGrid = $("#crm_grid").data("kendoGrid");
			var rows = entityGrid.select();
			rows.each(function(index, row) {
				var selectedItem = entityGrid.dataItem(row);
				// selectedItem has EntityVersionId and the rest of your model
				compaignPhones.push(selectedItem.name);
			});
            param.numbers = compaignPhones;
            
             $.ajax({
                 url: 'server-side/call/outgoing_company.action.php',
                 data: param,
                 success: function(data){
                     alert('კამპანია წარმატებით შეიქმნა');
                 }
            })
        });
    </script>
</head>

<body>
<div id="tabs"  >
    
    <div class="callapp_filter_show" style="">
        <ul class="status_tabs" id="status_tabs">
        </ul>
        <br>
        <button id="create_campaign">კამპანიის შექმნა</button>
        
        <div class="clear"></div>
        <div style="position:relative;">
            
            <div id="crm_grid"></div>
        </div>
    </div>
    <style>
        
        .tb_head td{
            border-right: 1px solid #E6E6E6;
        }
        /* #show_flesh_panel,#show_flesh_panel_right{
            float: left;
            cursor: pointer;
        } */
        .td_center{
            text-align: center !important;
        }

        /* The container checkbox*/
        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox checkmark*/
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #fff;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark {
            background-color: #fff;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark {
            background-color: #fff;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid #f80aea;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        /*------------------------------------------*/
        /* Create a custom checkbox checkmark1*/

        .checkmark1 {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #fff;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark1 {
            background-color: #fff;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark1 {
            background-color: #fff;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark1:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark1:after {
            display: block;
        }
        #chat_typing{
            display:none;
            float: right;
            margin-right: 5px;
            position: absolute;
            bottom: 170px;
            left: 308px;
            font-size: 15px;
            /* font-weight: bold; */
        }
        /* Style the checkmark/indicator */
        .container .checkmark1:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid #f29d09;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        /*------------------------------------------*/
        /* Create a custom checkbox checkmark2*/

        .checkmark2 {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #fff;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark2 {
            background-color: #fff;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark2 {
            background-color: #fff;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark2:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark2:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark2:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid #2196f3;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        /*------------------------------------------*/

        /* Create a custom checkbox checkmark3*/

        .checkmark3 {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #fff;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark3 {
            background-color: #fff;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark3 {
            background-color: #fff;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark3:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark3:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark3:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid #e81d42;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        /*------------------------------------------*/

        /* Create a custom checkbox checkmark3*/

        .checkmark4 {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #fff;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark4 {
            background-color: #fff;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark4 {
            background-color: #fff;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark4:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark4:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark4:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid #030202;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        /*------------------------------------------*/

        /* Create a custom checkbox checkmark3*/

        .checkmark5 {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #fff;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark5 {
            background-color: #fff;
        }

        /* When the checkbox is checked, add a blue background */
        .container input:checked ~ .checkmark5 {
            background-color: #fff;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark5:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark5:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark5:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid #009895;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        #incoming_chat_tabs .ui-dialog .ui-widget-header {
            border: none;
            background: #fff;
        }

        #incoming_chat_tabs .ui-tabs .ui-tabs-nav {
            margin: 0;
            padding: .0em .0em 0;
        }
        /*------------------------------------------*/
    </style>
    <!-- <div id="flesh_panel">
        <div class="callapp_head" style="text-align: right;"><img id="show_flesh_panel" title="პანელის გადიდება" alt="arrow" src="media/images/icons/arrow_left.png" height="18" width="18">ქოლ-ცენტრი<hr class="callapp_head_hr"></div>
        <table style="display:none;" id="comucation_status">
            <tr>
                <td colspan="3"><div style="padding: 12px 0px 0px 10px; font-size:16px" class="callapp_head" style="text-align: left;">კომუნიკაციის არხები</div></td>
            </tr>
            <tr style="height: 6px"></tr>
            <tr class="comunicationTr">
                <td style="width: 60px;"><span class="imagespan"><img id="VebChatImg" src="media/images/icons/comunication/Chat_OFF.png" height="35" width="35"></span></td>
                <td style="padding: 10px 0px 0px 0px; width: 100px;"><span style="color: #f80aea;" class="comunication_name">ვებ-ჩათი</span></td>
                <td style="padding: 5px 0 0 0px;"><label class="container">
                        <input class="comunication_checkbox" type="checkbox" id="web_chat_checkbox" value="1">
                        <span style="border: 1px #f80aea solid;" class="checkmark"></span>
                    </label></td>
            </tr> -->
            <!--    	<tr class="comunicationTr">-->
            <!--    		<td><span class="imagespan"><img id="siteImg" src="media/images/icons/comunication/OFF-my.png" height="35" width="35"></span></td>-->
            <!--    		<td style="padding: 10px 0px 0px 0px;"><span style="color:#f29d09;" class="comunication_name">my მესენენჯერი</span></td>-->
            <!--    		<td style="padding: 5px 0 0 0px;"><label class="container">-->
            <!--              <input class="comunication_checkbox" type="checkbox" id="site_chat_checkbox" value="1">-->
            <!--              <span style="border: 1px #f29d09 solid;" class="checkmark1"></span>-->
            <!--            </label></td>-->
            <!--    	</tr>-->
            <!-- <tr class="comunicationTr">
                <td><span class="imagespan"><img id="MessengerImg" src="media/images/icons/comunication/Messenger_OFF.png" height="35" width="35"></span></td>
                <td style="padding: 10px 0px 0px 0px;"><span style="color: #2196f3;" class="comunication_name">მესენჯერი</span></td>
                <td style="padding: 5px 0 0 0px;"><label class="container">
                        <input class="comunication_checkbox" type="checkbox" id="messanger_checkbox" value="1">
                        <span style="border: 1px #2196f3 solid;" class="checkmark2"></span>
                    </label></td>
            </tr>
            <tr class="comunicationTr">
                <td><span class="imagespan"><img id="MailImg" src="media/images/icons/comunication/E-MAIL_OFF.png" height="35" width="35"></span></td>
                <td style="padding: 10px 0px 0px 0px;"><span style="color: #e81d42;" class="comunication_name">ელ-ფოსტა</span></td>
                <td style="padding: 5px 0 0 0px;"><label class="container">
                        <input class="comunication_checkbox" type="checkbox" id="mail_chat_checkbox" value="1">
                        <span style="border: 1px #e81d42 solid;" class="checkmark3"></span>
                    </label></td>
            </tr> -->

            <!-- <tr class="comunicationTr" style="height: 40px; display:none;">
    	    <td><span class="imagespan"><img id="VideoImg" src="media/images/icons/comunication/Video_OFF.png" height="35" width="35"></span></td>
    		<td style="padding: 10px 0px 0px 0px;"><span style="color: #009895;" class="comunication_name">ვიდეო ზარი</span></td>
    		<td style="padding: 5px 0 0 0px;"><label class="container">
              <input class="comunication_checkbox" type="checkbox" id="video_call_checkbox" value="1">
              <span style="border: 1px #009895 solid;" class="checkmark5"></span>
            </label></td>
    	</tr> 
        </table>
        <table id="flesh_panel_table" style="margin-bottom: 0px; width: 100%;">

            <thead>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td id="show_station" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;text-decoration: underline;"><img src="media/images/icons/comunication/phone.png" height="27" width="27"></td>
                            <td id="show_chat" 	  class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Chat.png" height="30" width="30"></td>
                            <td id="show_fbm"     class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Messenger.png" height="30" width="30"></td>
                            <td id="show_mail"    class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important; "><img src="media/images/icons/comunication/E-MAIL.png" height="30" width="30"></td>
                            <td id="show_video"    class="flash_menu_link" style="display:none; cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Video.png" height="30" width="30"></td> 
                        </tr>
                    </table>
                </td>


            </tr>
            <tr class="tb_head" style="border: 1px solid #E6E6E6;">
                <td style="width:85px" id="mini_pirveli">შიდა ნომერი</td>
                <td colspan = "2"  style="width: 120px;" id="mini_meore">მომ. ავტორი</td>
                <td style="width: 90px;" id="mini_mesame">სტატუსი</td>
            </tr>
            </thead>
            <tbody id="mini_call_ext">
            </tbody>
            <thead>
            <tr>
                <td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;"></td>
            </tr>
            <tr>
                <td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;">რიგი</td>
            </tr>
            <tr class="tb_head" style="border: 1px solid #E6E6E6;">
                <td id="m_q_pirveli">პოზიცია</td>
                <td colspan="3" id="m_q_mesame">ნომერი</td>
            </tr>
            </thead>
            <tbody id="mini_call_queue">
            </tbody>

        </table>



    </div> -->
    
    <div id="loading" style="z-index: 999999;padding: 160px 0;height: 100%;width: 100%;position: fixed;background: #f9f9f9;top: 0;">
        <div class="loading-circle-1">
        </div>
        <div class="loading-circle-2">
        </div>
    </div>
    <input id="check_state" type="hidden" value="1" />
    <input id="whoami" type="hidden" value="1" />
    <!-- jQuery Dialog -->
    <div id="loading_search_my_client_history" style="display:none; z-index: 999999;padding: 155px 0;height: 100%;width: 75%;position: fixed;top: 0;">
        <div class="loading-circle-1">
        </div>
        <div class="loading-circle-2">
        </div>
    </div>

    <div id="get_processing-dialog" class="form-dialog" title="დამუშავება"></div>
<div id="dialog_active_chat" aria-data="false"></div>

</body>
