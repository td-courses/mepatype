<?php 
	session_start();

?>


<?php if ($_SESSION['USERID'] == ''): ?>
	<?php require_once('includes/login.php');?>
<?php else: ?>
	<?php require_once('includes/functions.php');?>

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>MEPA</title>

<!-- 			<link rel="shortcut icon" type="image/ico" href="media/images/icons/hb.ico"/> -->
			<link rel="stylesheet" type="text/css" media="screen" href="media/css/reset/reset.css" />
			<!--[if IE]>
				<link rel="stylesheet" type="text/css" media="screen" href="css/reset/ie.css" />
			<![endif]-->

			<!-- font awesome -->
			<script defer src="js/fontawesome-all.js"></script>

			<?php echo GetJs();?>
			<?php echo GetCss();?>
			
			<script type="text/javascript">
				console.log("start");
				$(document).ready(function (){
					runAjax();
					AjaxSetup();

					
					var used_ext = localStorage.getItem('used_ext');
					localStorage.setItem('used_ext', <?php if($_SESSION['EXTNUM'] == 0){ echo 999999;} else{echo $_SESSION['EXTNUM'];} ?>);
				});
			</script>
			<style>
#service_pn_ferm {cursor: pointer;}
#kendo_project_info_table .k-grid-header .k-header>.k-link {
	padding-bottom: 25px;
    line-height: inherit;
    white-space: normal;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    left: 0;
	right: 0;
	display: unset;
    font-size: 11px;
}

#kendo_project_info_table .k-grid-header thead>tr:not(:only-child)>th{
	text-align: center;
}

#project_info_view_table select, #kendo_project_info_table select {
	font-size: 12px !important;
    font-family: BPG !important;
    background: #ffffffa1 !important;
	border: 1px solid #58585800 !important;
	border-radius: 0 !important;
    box-sizing: border-box;
    padding: 5px;
}



count_sub {
	color: #fff;
    background-color: #ec1515;
    padding: 2px 5px 1px 5px;
    border-radius: 13px;
    font-size: 11px;
    font-family: monospace;
    font-weight: bold;
    margin-left: 10px;
	display: none;
}


				/* Google Material Icons */
				@font-face {
					font-family: 'Material Icons';
					font-style: normal;
					font-weight: 400;
					src: url(media/fonts/material_icons/MaterialIcons-Regular.eot); /* For IE6-8 */
					src: local('Material Icons'),
						local('MaterialIcons-Regular'),
						url(media/fonts/material_icons/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2'),
						url(media/fonts/material_icons/MaterialIcons-Regular.woff) format('woff'),
						url(media/fonts/material_icons/MaterialIcons-Regular.ttf) format('truetype');
				}
				#table_right_menu{
					z-index:1px;
				}
				#table_right_menu{
					display:none!important;
				}
				*{
					outline: none;
				}
				.material-icons {
					font-family: 'Material Icons';
					font-weight: normal;
					font-style: normal;
					font-size: 24px;  /* Preferred icon size */
					display: inline-block;
					line-height: 1;
					text-transform: none;
					letter-spacing: normal;
					word-wrap: normal;
					white-space: nowrap;
					direction: ltr;
					
					/* Support for all WebKit browsers. */
					-webkit-font-smoothing: antialiased;
					/* Support for Safari and Chrome. */
					text-rendering: optimizeLegibility;
					
					/* Support for Firefox. */
					-moz-osx-font-smoothing: grayscale;
					
					/* Support for IE. */
					font-feature-settings: 'liga';
				}
				#news{
					display: block;
					position: fixed !important;
					bottom: 10px !important;
					right: 10px !important;
					background: #DB3340;
					width: 200px;
					height: 40px;
					border-radius:3px;
					color: #E8B71A;
					text-align:center;
					z-index: 9999;
				}
				#news span{
					display: block;
					padding-top: 7px;
				}
				#news_action{
					display: block;
					position: fixed !important;
					bottom: 55px !important;
					right: 10px !important;
					background: #DB3340;
					width: 200px;
					height: 40px;
					border-radius:3px;
					color: #E8B71A;
					text-align:center;
					z-index: 9999;
				}
				#news_action span{
					display: block;
					padding-top: 7px;
				}

				#tabel_id {
					width: 100%;
				}
				#logout_comment {
					width: 100%; min-height: 50px;
					resize: vertical;
					box-sizing: border-box;
				}
				label[for="logout_comment"] {
					margin-top: 10px;
				}
				#save_id, #undo_id {
					font-family: pvn;
					font-size: 13px;
					line-height: 1.4;
					color: #636e72;
					background: #E6F2F8;
					border: 1px solid #A3D0E4;
				}

				#save_id:hover, #undo_id:hover {
					color: #2d3436;
				}
			

				.ui-dialog-titlebar-close span {
					position: absolute;
					left: 50%;
				}

				.activitie-screen-disabler {
					display: none;
					width: 100%; height: 100%;
					position: fixed;
					top: 0; left: 0;
					z-index: 99999;
					background: rgba(0,0,0, .80);
					user-select: none;
				}
				.activitie-screen-disabler.on {
					display: flex;
					align-items: center;
					justify-content: center;
				}
				.activitie-time-counter {
					text-align: center;
				}
				.activitie-time-counter span {
					font-size: 330px;
					color: #ecf0f1;
				}
				#returnFromActivitie {
					margin-left: 50%;
					transform: translate(-50%);
					background: #3498db;
					font-family: pvn;
					font-size: 16px;
					padding: 9px 17px;
					border-radius: 5px;
					margin-top: 13px;
					color: #ecf0f1;
					border: none;
					cursor: pointer;
					transition: background .1s ease;
				}
				#returnFromActivitie:hover {
					background: #2980b9;
				}
				.activitie-cur-time {
					display: flex;
					width: 200px; height: 50px;
					position: absolute;
					bottom: 0; right: 0;
					align-items: center;
					justify-content: center;
					font-size: 30px;
					color: #f1c40f;
				}
				.livestate-action-circ {
					width: 10px; height: 10px;
					float: left;
					position: relative;
					top: 2px;
					margin-right: 3px;
				}
            </style>
            <script type="text/javascript">
            	var logout = true;
				var counter = 0;
				function play2() {
					var audio = new Audio('audio.mp3');
					audio.play();
				}
				function play3() {
					var audio2 = new Audio('videocall.mp3');
					audio2.play();
				}
                $(document).ready(function (){

					// checker1();
					// checkActivitieStatus();

        		    function checker1(){
        				setTimeout(function(){

                        	$.ajax({
                        		async: true,
            		            url: "checker.php",
            		            type: "POST",
            		            data: "act=get_checker&logout=1",
            		            dataType: "json",
            		            success: function (data) {

									if(data.alertCount > 0){
										var audio = new Audio('sms.mp3');
										audio.play();
									}

            		            	if(logout==true) {

                  				       if(data.logout==1){
              					   			buttons = {
              					   				 "logout1": {
              					   			            text: "არა",
              					   			            id: "logout1"
              					   			       },
              					   					"login": {
              					   			            text: "დიახ",
              					   			            id: "login"
              					   			       }
              						   			};

          	    						if(data.time==0){
          	        						logout=true; window.location.href="index.php?act=logout";
          	        					}
              					   		GetDialog("logout", "auto", "auto",buttons);
              	   							$("#logout").html('თქვენი სამუშაო გრაფიკით განსაზღვრული დრო ამოიწურა!გსურთ სისტემაში დარჩენა?<br> <div style="margin: 5px 0px 0 146px; padding: 5px 10px; display: inline-block;">დარჩენილი დრო </div> <div style="margin: 5px auto; padding: 5px 10px; border: solid 1px; display: inline-block;">'+data.time+'</div><div style="margin: 5px auto; padding: 5px 10px; display: inline-block;">წმ</div>');
                  				    	}else{
              				    		//$("#logout").dialog("close");
              					    	}

              				    	}

                		            if(data.count != 0){
                		            	$("#new_task").attr('data-badge',data.count);
                		            }else{
                 		            	$("#new_task").removeAttr('data-badge');
                 		            }

                		            if(data.no_wiev != 0){
                		            	$("#new_task").attr('data-badge1',data.no_wiev);
                		            }else{
                 		            	$("#new_task").removeAttr('data-badge1');
                 		            }
                 		            
                		            if(data.count != 0 || data.no_wiev != 0){
                		            	$("#new_task").attr('class','badge2');
                		            }else{
                 		            	$("#new_task").removeClass('badge2');
                		            }
                		            
                		        }
            		        });
                           	checker1();
                        }, 10000);


                    }

                    function checker(){
                    	setTimeout(function(){
                    		$.ajax({
                    			async: true,
            		            url: "checker.php",
            		            type: "POST",
            		            data: "act=get_checker",
            		            dataType: "json",
            		            success: function (data) {
          		            	   	$( "#newsmain" ).html(data.count);
            		            }
            		        });
                    		checker();
                        	}, 10000);
					}

					// check activitie status
					function checkActivitieStatus() {

						// define request data
						const data = {
							act: "check_activitie_status"
						}

						// send ajax request
						$.post("server-side/call/incomming.action.php", data, responce => {

							// restruct responce
							const { error, activitieId, activitieStatus, secondsPassed } = responce;

							// check for error
							if(error !== "") {
								alert(error);
							} else {

								// define if activitie counter is on
								const activitieCounterIsOn = $(".activitie-screen-disabler").hasClass("on");

								// check for activitie id
								if(activitieStatus === "on" && !activitieCounterIsOn) {
									$D.activitieOnTimer(secondsPassed);
									$D.chosedActivitieId = activitieId;
								} else if(activitieStatus === "off" && activitieCounterIsOn) {

									// close activite timer
									$D.activitieOffTimer();
								}

								// make check interval
								setTimeout(checkActivitieStatus, 1000);

							}

						});
					}

    				$(document).on("click", "#news", function () {
    					location.href='index.php?pg=13#tab-1'
    				});
    				
    				$(document).on("click", "#news_action", function () {
    					location.href='index.php?pg=47'
    				});

					$(document).on("click", ".logout", function () {

						$.post("server-side/logout.action.php", {act: "logout_save"}, () => {
							window.location.href="index.php?act=logout";
						});

					});


                $(document).on("click", "#logout1", function () {

                	window.location.href="index.php?act=logout";
                });

                $(document).on("click", "#login", function () {
                	logout = false;
                	$("#logout").dialog("close");
				});

				// return from activitie
				$(document).on("click", "#returnFromActivitie", function() {

					// define request data
					const data = {
						act: "change_incall_activitie_status",
						activitieId: $D.chosedActivitieId,
						type: "off"
					};

					// send ajax request
					$.post("server-side/call/incomming.action.php", data, responce => {

						// restruct responce
						const { error, onlineStatus, dndStatus } = responce;

						// check for error
						if(error !== ""){
							alert(error);
						}else{
							$D.activitieOffTimer();
							toggleChatStatus('#chat_status', onlineStatus === 1 ? 2 : 1);

							// toggle dnd
							$.post("AsteriskManager/dndAction.php", {
								action: dndStatus === 1 ? "dndon" : "dndoff"
							});
						}

					});
				});

				// clcik on everything and everywhere
				$(window).click(function() {

					// close choose activitie
					$("#callapp_op_activitie_choose").data("active", false).removeClass("on");
				});

				// desable context menu on screen disabler
				$(document).on("contextmenu", ".activitie-screen-disabler", () => false);

			});




			var ajaxtimer;
			var already_clicked = 0;	
			localStorage.setItem('already_clicked', 0);	
			localStorage.setItem('dialog_opened', 0);
			localStorage.setItem('extension', 0);
			localStorage.setItem('number', 0);
			$(document).on('click', '#save-dialog-out', function(){

				param = new Object();

				param.act = "save_incomming";
				param.id	=	$('#hidden_base_id').val();
				param.hidden_incomming_call_id = $("#hidden_incomming_call_id").val();
				param.ipaddres = $("#ipaddres").val();
				param.processing_start_date = $("#processing_start_date").val();

				param.incomming_cat_1 = $("#incomming_cat_1").val();
				param.incomming_cat_1_1 = $("#incomming_cat_1_1").val();
				param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();

				param.incomming_status_1 = $("#incomming2_status_1").val();
				param.incomming_status_1_1 = $("#incomming2_status_1_1").val();

				param.chat_language = $("#chat_language").val();
				param.chat_site     = $("#chat_site_id").val();
				param.inc_status_a    = $("#inc_status_a").val();
				param.company    = $("#company").val();
				param.space_type    = $("#space_type").val();

				param.fb_link = $("#fb_link").val();
				param.viber_address = $("#viber_address").val();
				param.vizit_datetime = $("#vizit_datetime").val();
				param.vizit_location = $("#vizit_location").val();
				param.client_comment = $("#client_comment").val();
				param.out_comment     = $("#out_comment").val();
				param.s_u_mail = $("#client_mail").val();
				param.s_u_name = $("#client_name").val();
				param.lid = $("input[id=check_lid]:checked").val();
				param.phone = $("#phone").val();
				param.phone1 = $("#phone1").val();

				param.source_id = $("#source_id").val();
				param.call_content = $("#call_content").val();
				param.lid_comment = $("#lid_comment").val();

				$.ajax({
					url: 'server-side/call/outgoing_company.action.php',
					data: param,
					success: function(data){
						console.log('done', data);
						$('#get_processing-dialog').dialog("close");
						
						var req_id = $('#request_id').val();
						get_tabs_count(req_id);

						$("#grid2").data("kendoGrid").dataSource.read();
						// var grid2 = $("#grid2").data("kendoGrid").dataSource.read();
						// 	grid2.refresh();
					}
				})

			});
			function runAjax() {
				
    try {clearTimeout(ajaxtimer);}
    catch(err) {}
	




	var dialog_opened 	= localStorage.getItem('dialog_opened');
	var extension 		= localStorage.getItem('extension');
	var number 			= localStorage.getItem('number');
	if(extension != 0 && number != 0){
		//alert('satesto ooooo');
		$("#phone_dial").html('<div id="dialog_to_open" style="cursor: pointer; padding: 7px 10px;"  class="open_dialog" data-source="phone" extension="'+extension+'" number="'+number+'">'+number+'</div>');
		if(dialog_opened == 0){
			$("#dialog_to_open").click();
			$("#phone_dial").html();
			localStorage.setItem('already_clicked', 1);
			localStorage.setItem('dialog_opened', 1);
			localStorage.setItem('extension', 0);
			localStorage.setItem('number', 0);
		}
	}
	
    $.ajax({
        async: true,
        dataType: "JSON",
        url: 'AsteriskManager/liveState.json',
        data: {
            'sesvar'	:	'hideloggedoff',
            'value'		:	true,
            'stst'		:	1,
            'checkState'		:	$("#check_state").val(),
            'chat_id'			:	$('#chat_original_id').val(),
            'chat_detail_id'	:	chat_detail_id,
            'whoami' 			: 	$('#whoami').val()
		},
        success: function(data) {
			var scroll_numbers = $('#section_active').scrollTop();
			if(data.crm_counts_sum == 0){
				$('.dashboard-152 a .clearfix').html("<count>"+data.crm_counts_sum+"</count>");
			}else{
				$('.dashboard-152 a .clearfix').html("<count>"+data.crm_counts_sum+"</count>");
			}

			data.crm_counts.forEach(x => {
				if(x.count > 0){
					 $('#statusx'+x.status_id).html(x.count);
					 $('#statusx'+x.status_id).css('display', 'inline-block')
				}else{
					$('#statusx'+x.status_id).html("0")
				}
			});


            //////////////////////////--------start flash paneli-------------////////////////////////////
// console.log(data);
            let comunication = $(".active_comunication").attr('source');
            let html;
            let arr          = data.users;
            let queue_mail   = data.queue.mail;
            let queue_chat   = data.queue.chat;
			let queue_fb     = data.queue.fb;
			let queue_fbc     = data.queue.fbc;
			let queue_phone  = data.queue.phone;
			let queue_viber  = data.queue.viber;
			let queue_video  = data.queue.video;

			let chat_displey        = data.permission.chat_permission;
            let fbm_displey         = data.permission.fbm_permission;
            let mail_displey        = data.permission.mail_permission;
            let Instagram_displey   = data.permission.Instagram_permission;
            let fbc_displey			= data.permission.fbc_permission;
            let whatsapp_displey    = data.permission.whatsapp_permission;
            let viber_displey       = data.permission.viber_permission;
            let skype_displey       = data.permission.skype_permission;
            let video_displey       = data.permission.video_permission;
            //console.log(queue_phone.length);
            $("#fbm_chat_count_open .fabtext").html(queue_fb.length);
            $("#fb_comments .fabtext").html(queue_fbc.length);
			$("#mail_chat_count_open .fabtext").html(queue_mail.length);
			$("#phone_chat_count_open .fabtext").html(queue_phone.length);
			$("#chat_count_open .fabtext").html(queue_chat.length);
			$("#viber_chat_count_open .fabtext").html(queue_viber.length);
			$("#video_chat_count_open .fabtext").html(queue_video.length);

			$("li[aria-controls='incoming_chat_tab_chat'] .fabtext").html(queue_chat.length);
			$("li[aria-controls='incoming_chat_tab_mail'] .fabtext").html(queue_mail.length);
			$("li[aria-controls='incoming_chat_tab_fbm'] .fabtext").html(queue_fb.length);
			$("li[aria-controls='incoming_chat_tab_fbc'] .fabtext").html(queue_fbc.length);
			$("li[aria-controls='incoming_chat_tab_viber'] .fabtext").html(queue_viber.length);
			//$("li[aria-controls='incoming_chat_tab_fbm'] .fabtext").html(queue_fb.length);
			

			counter++;
			if(queue_chat.length > 0 && $(".comunication_bar[source='chat']").attr('disabled') != 'disabled')
			{
				if(counter%3==0)
				{
					play2();
				}
			}
			if(queue_video.length > 0 && $(".comunication_bar[source='webcall']").attr('disabled') != 'disabled')
			{
				if(counter%3==0)
				{
					play3();
				}
			}
            switch(comunication){
                case "phone": 
					// console.log(data.users['1'])
						
						// let users = JSON.parse(data.users);
						
						html = `	
								<div id="section_active">
								<div class="flesh_tabs_head">
									<div class="flesh_tabs_title flesh_th">აქტიური</div>
								</div>
								<div class="flesh_tabs_wrap_queue_active flesh_tabs_head_queue flash_phone2">
									<div class="flesh_tabs_queue">რიგი</div>
									<div class="flesh_tabs_queue">დეპარტამენტი</div>
									<div class="flesh_tabs_queue">თანამშრომელი</div>
									<div class="flesh_tabs_queue">სტატუსი</div>
									<div class="flesh_tabs_queue">დრო</div>
									<div class="flesh_tabs_queue">აბონენტის ინფო.</div>
								</div>`;
					html += `<div class="flesh_tabs_wrap flesh_tabs_body">`;
						var already_clicked = 0;
						for(let i=0; i<arr.length; i++){
							let phone = arr[i].phone;
							let chat  = arr[i].chat;
							let fb    = arr[i].fb;
							let fbc   = arr[i].fbc;
							let mail  = arr[i].mail;
							let viber = arr[i].viber;
							
							
							for(p=0; p<phone.length; p++){
								if(isNull(phone[p].ext) != ''){
									var out = '';
									if(phone[p].type == 'out'){
										out = '_out';
									}
									work_status_title = 0;
									console.log(phone[p].status);
											if(phone[p].status == 'paused_2')
												work_status_title = 'შესვენება';
											if(phone[p].status == 'paused_3')
												work_status_title = 'გარკვევა';
											if(phone[p].status == 'paused_4')
												work_status_title = 'რეგისტრაცია';
											if(phone[p].status == 'paused_5')
												work_status_title = 'დამუშავება';
											if(phone[p].status == 'paused_8')
												work_status_title = 'ონლაინ ჩათი/ზარი';
											if(phone[p].status == 'paused_9')
												work_status_title = 'ონლაინ ჩათი';
											if(phone[p].status == 'not in use')
												work_status_title = 'აქტიური';
											if(phone[p].status == 'busy')
												work_status_title = 'საუბრობს';
											if(phone[p].status == 'ringing')
												work_status_title = 'რეკავს';

									let status = ` <div class="flex">
														<div title="${work_status_title}" class="phone_icon"style="background-image:url('media/images/icons/comunication/${isNull(phone[p].status).replace(/\s/g, '')}`+out+`.png?v=1.4')"></div>
														<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
													<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
													<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
													<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
													<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
													</div>`;
									if(isNull(arr[i].username) != ''){
										status = `<div class="flex">
													<div title="${work_status_title}" class="phone_icon"style="background-image:url('media/images/icons/comunication/${isNull(phone[p].status).replace(/\s/g, '')}`+out+`.png?v=1.1')"></div>
													<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
													<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
													<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
													<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
													<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
												</div>`;
									}
									arrow_call = '';
									if(phone[p].time){
										arrow_call = '<img src="media/images/icons/comunication/'+phone[p].arrow_call+'.png"/>';
									}
									
									html += ` <div class="flesh_tabs" >${isNull(phone[p].queue)}</div>
												<div class="flesh_tabs" >${isNull(phone[p].dep)}</div>
											<div class="flesh_tabs" title="${isNull(arr[i].username)}  (${isNull(phone[p].ext)}) ${(phone[p].queues) ? '('+isNull(phone[p].queues)+')' : '' }"> ${isNull(arr[i].username)} (${isNull(phone[p].ext)}) ${(phone[p].count) ? '('+isNull(phone[p].count)+')' : '' } </div>
											<div class="flesh_tabs" title="">${isNull(status)}</div>
											<div class="flesh_tabs" >${isNull(phone[p].time)}`+arrow_call+`</div>
											<div style="cursor: pointer;"  class="open_dialog" data-source="phone" extension="${isNull(phone[p].ext)}" number="${isNull(phone[p].number)}" >${isNull(phone[p].info)}</div>
											`;
									var used_ext = localStorage.getItem('used_ext');
									if(phone[p].status != 'busy' && phone[p].ext == used_ext && isNull(arr[i].username))
									{
										localStorage.setItem('already_clicked', 0);
									}
									var already_clicked = localStorage.getItem('already_clicked');
									
									// if(phone[p].ext == used_ext && isNull(arr[i].username) != '' && already_clicked == 0 && phone[p].type == 'in'){   //999 SHOULD BE REPLACED EVERYWHERE BY EXTENSION FROM LOCAL STORAGE
									// 	//alert('first: '+already_clicked);
										
									// 	if(isNull(phone[p].number) != '' && phone[p].number != used_ext && phone[p].status == 'busy'){
									// 		$("#phone_dial").html('');
									// 		$("#phone_dial").html(`<div id="dialog_to_open_`+phone[p].number+`" style="cursor: pointer; padding: 7px 10px;"  class="open_dialog" data-source="phone" extension="${isNull(phone[p].ext)}" number="${isNull(phone[p].number)}">${isNull(phone[p].number)}</div>`);
									// 		already_clicked++;
									// 		localStorage.setItem('already_clicked', already_clicked);
									// 		var dialog_opened = localStorage.getItem('dialog_opened');
									// 		if(dialog_opened == 0){
									// 			$("#dialog_to_open_"+phone[p].number).click();
									// 			$("#phone_dial").html();
									// 			$.ajax({
									// 				url: "AsteriskManager/dndAction.php",
									// 				data: {
									// 					action: 'dndon',
									// 					activity_id: 5
									// 				},
									// 				success: function(res) {
									// 					var statusBG = $('#status-place > a > x');
									// 					var statusName = $('#status-place > a > span');
									// 					$('#aStatus li[data-id=8]').css("background-color", "");
									// 					statusBG.css("background-color", "#a9009f");
									// 					statusName.text('დამუშავება');

									// 					$('.icon_status_color').css("background-color", "#a9009f");
									// 					console.log(res);
									// 				},
									// 				error: function(e){
									// 					alert("dnd error ajax");
									// 					console.log(e);
									// 				}
									// 			});
									// 			$.ajax({
									// 				url: 'includes/menu.action.php',
									// 				data: {
									// 					act: "activity_status",
									// 					status_id: 5
									// 				},
									// 				success: function(res) {

									// 				}
									// 			});
												
									// 			localStorage.setItem('dialog_opened', 1);
									// 		}
									// 		/* else{
									// 			localStorage.setItem('extension', phone[p].ext);
									// 			localStorage.setItem('number', phone[p].number);
									// 		} */
											
									// 		//alert('second: '+already_clicked);
									// 	}
									// }
									
									if(phone[p].ext == used_ext && isNull(arr[i].username) != '' && already_clicked == 0 && phone[p].type == 'in-autodialer'){
										if(isNull(phone[p].number) != '' && phone[p].number != used_ext && phone[p].status == 'busy'){
											$("#phone_dial").html('');
											$("#phone_dial").html(`<div id="dialog_to_open" style="cursor: pointer; padding: 7px 10px;"  class="open_dialog" data-source="phone" data-calltype="autodialer" extension="${isNull(phone[p].ext)}" number="${isNull(phone[p].number)}">${isNull(phone[p].number)}</div>`);
											already_clicked++;
											localStorage.setItem('already_clicked', already_clicked);
											var dialog_opened = localStorage.getItem('dialog_opened');
											if(dialog_opened == 0){
												
												localStorage.setItem('dialog_opened', 1);
												$("#dialog_to_open").click();
												//$("#phone_dial").html();
											}
											/* else{
												localStorage.setItem('extension', phone[p].ext);
												localStorage.setItem('number', phone[p].number);
											} */
											
											//alert('second: '+already_clicked);
										}
									}
									else if(phone[p].ext == used_ext && isNull(arr[i].username) != '' && already_clicked == 0 && phone[p].type == 'out'){   //999 SHOULD BE REPLACED EVERYWHERE BY EXTENSION FROM LOCAL STORAGE
										//alert('first: '+already_clicked);
										console.log(phone[p].number);
										
										if(isNull(phone[p].number) != '' && phone[p].number.length >= 9 && phone[p].number != used_ext && phone[p].status == 'busy' && phone[p].exist_call == 1){
											already_clicked++;
											localStorage.setItem('already_clicked', already_clicked);
											var dialog_opened = localStorage.getItem('dialog_opened');
											if(dialog_opened == 0){
												var phone_number = phone[p].number.substr(phone[p].number.length - 9);
												$.ajax({
													url: 'server-side/call/outgoing_company.action.php',
													type: "POST",
													data: "act=get_processing&id="+phone_number,
													dataType: "json",
													success: function (data) {
														
														$("#get_processing-dialog").html(data.page);
														$.ajax({
															url: Comunications_URL,
															data: {
																'act'		: 'get_fieldsets',
																'setting_id': 4
															},
															success: function(data) {
																$(data.chosen_keys).chosen({ search_contains: true });

																if(data.datetime_keys != ''){
																	var datetimes = data.datetime_keys;

																	datetimes.forEach(function(item,index){
																		GetDateTimes(item);
																	});
																}

																if(data.date_keys != ''){
																	var date = data.date_keys;

																	date.forEach(function(item,index){
																		GetDate(item);
																	});
																}
																if(data.multilevel_keys != ''){
																	var date = data.multilevel_keys;
																	var main = '';
																	var secondary = '';
																	var third = '';

																	date.forEach(function(item,index){
																		if(index == 0){
																			main = item;
																		}
																		else if(index == 1){
																			secondary = item;
																		}
																		else{
																			third = item;
																		}
																	});
																}

																$(document).on("change", "#"+main, function () {
																	param = new Object();
																	param.act = "cat_2";
																	param.selector_id = $("#"+main).attr('id');
																	param.cat_id = $("#"+main).val();
																	$.ajax({
																		url: Comunications_URL,
																		data: param,
																		success: function (data) {
																			$("#"+secondary).html(data.page);
																			$("#"+secondary).trigger("chosen:updated");

																			if ($("#"+secondary+" option:selected").val() == 0) {
																				param = new Object();
																				param.act = "cat_3";
																				param.cat_id = $("#"+secondary).val();
																				$.ajax({
																					url: Comunications_URL,
																					data: param,
																					success: function (data) {
																						$("#"+third).html(data.page);
																						$("#"+third).trigger("chosen:updated");
																					}
																				});
																			}
																		}
																	});
																});
																$(document).on("change", "#"+secondary, function () {
																	param = new Object();
																	param.act = "cat_3";
																	param.selector_id = $("#"+main).attr('id');
																	param.cat_id = $("#"+secondary).val();
																	$.ajax({
																		url: Comunications_URL,
																		data: param,
																		success: function (data) {
																		$("#"+third).html(data.page);
																		$("#"+third).trigger("chosen:updated");
																		}
																	});
																});

																$(".dep").chosen();
																$(".dep_project").chosen();
																$(".dep_sub_project").chosen();
																$(".dep_sub_type").chosen();
																///BLOCKING CONTRACT FIELDSET BEGIN
																$(".dialog-tab-20 input").each(function(index){  
																	$(this).prop('disabled',true);
																});
																$("#servisi___259--146--8").prop('disabled',true).trigger("chosen:updated");

																///BLOCKING CONTRACT FIELDSET END

															}
														});
														$("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0,#region,#municip,#temi,#village").chosen();
														var buttons = {
															
															done: {
																text: "შენახვა",
																class: "save-processing",
																id: 'save-automated-dialog'
															},
															cancel: {
																text: "დახურვა",
																id: "cancel-dialog",
																click: function () {
																	localStorage.setItem('dialog_opened', 0);
																	$(this).dialog("close");
																}
															}
														};

														GetDialog("get_processing-dialog","619","auto",buttons,'ceneter center');
														$("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming2_status_1, #incomming2_status_1_1, #inc_status_a, #company, #space_type, #chat_language, #s_u_status").chosen({ search_contains: true });
														$(".ui-dialog-titlebar-close").remove();
													},
													error: function (jqXHR, exception) {
														
														if(jqXHR.responseText = 'base_error_not_found')
														{
															localStorage.setItem('dialog_opened', 0);
														}
													}
												});
												localStorage.setItem('dialog_opened', 1);
											}
											/* else{
												localStorage.setItem('extension', phone[p].ext);
												localStorage.setItem('number', phone[p].number);
											} */
											
											//alert('second: '+already_clicked);
										}
									}
								}

							}
		
						}
						
					
					html += `</div></div>`;
					//console.log(html);
					html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
					html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue flash_phone">
					
										<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
										<div class="flesh_tabs_queue" >რიგი</div>
										<div class="flesh_tabs_queue" >მომ.ავტორი</div>
										<div class="flesh_tabs_queue" >სახელი და გვარი</div>
										<div class="flesh_tabs_queue" >დრო</div>

									</div>`;

						html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq flash_phone">`;

							for(let q_p = 0; q_p < queue_phone.length; q_p ++){
								html += ` 
									<div class="flesh_queue" >${queue_phone[q_p].position}</div>
									<div class="flesh_queue">${isNull(queue_phone[q_p].queue)}</div>
									<div class="flesh_queue">${isNull(queue_phone[q_p].number)}</div>
									<div class="flesh_queue">${isNull(queue_phone[q_p].info)}</div>
									<div class="flesh_queue">${isNull(queue_phone[q_p].time)}</div>`;
							}

							html += `</div></div>`;




					break;
				case "fbm": 
							// let users = JSON.parse(data.users);
					html = `
						
						
						<div id="section_active">
							<div class="flesh_tabs_head">
								<div class="flesh_tabs_title flesh_th">აქტიური</div>
							</div>
							<div class="flesh_tabs_wrap_fb flesh_tabs_head">
									<div class="flesh_tabs_queue" >ფოტო</div>
									<div class="flesh_tabs_queue" >მომ.ავტორი</div>
									<div class="flesh_tabs_queue" >სტატუსი</div>
									<div class="flesh_tabs_queue" >ოპერატორი</div>
									<div class="flesh_tabs_queue" >ხ-ბა</div>
									<div class="flesh_tabs_queue" >სულ ხ-ბა</div>
						</div>`;
						html += `<div class="flesh_tabs_wrap_fb flesh_tabs_body">`;

						for(let i=0; i<arr.length; i++){
							let phone = arr[i].phone;
							let chat  = arr[i].chat;
							let fb    = arr[i].fb;
							let fbc   = arr[i].fbc;
							let mail  = arr[i].mail;
							let viber = arr[i].viber;
							
							let status = '';

							let call_status = isNull(phone.status).replace(/\s/g, '');

							if(call_status == ''){call_status = 'unavailable';}
							
							//if(fb.length > 0){
								
								status = `
									<div class="flex">
										<div class="phone_icon"style="background-image:url('media/images/icons/comunication/${call_status}.png?v=1.4')"></div>
										<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
										<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
										<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
										<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
										<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
									</div>
								`;
							//}
							for(f=0;f<fb.length;f++){
								let img = `<div class="fb_img" style="background-image:url('${fb[f].picture}')"></div>`
								if(fb[f].picture == ''){
									img = `<div class="fb_img"></div>`
								}
								
								html += ` 
								<div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${img}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(fb[f].author)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(status)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(arr[i].username)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(fb[f].duration)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(fb[f].all_duration)}</div>`;
							}

						}
						html += `</div></div>`;
						html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
						html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue">
						
						<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
						<div class="flesh_tabs_queue" >მომ.ავტორი</div>
						<div class="flesh_tabs_queue" >დრო</div>

						
					</div>`;

					html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq">`;

						for(let q_f = 0; q_f < queue_fb.length; q_f ++){
							html += ` 
								<div class="flesh_queue clickmetostart" data-source="fbm" chat_id = "${queue_fb[q_f].id}"  >${queue_fb[q_f].position}</div>
								<div class="flesh_queue clickmetostart" data-source="fbm" chat_id = "${queue_fb[q_f].id}"   title="${isNull(queue_fb[q_f].number)}">${isNull(queue_fb[q_f].number)}</div>
								<div class="flesh_queue clickmetostart" data-source="fbm" chat_id = "${queue_fb[q_f].id}"  >${isNull(queue_fb[q_f].time)}</div>`;
						}

						html += `</div></div>`;
						
						
						
						break;
					
				case "chat":
                    
					html = `
					
					<div id="section_active">
						<div class="flesh_tabs_head">
							<div class="flesh_tabs_title flesh_th">აქტიური</div>
						</div>
						<div class="flesh_tabs_wrap_chat flesh_tabs_head">
							<div class="flesh_tabs_queue" >ბრაუზერი</div>
							<div class="flesh_tabs_queue" >მომ.ავტორი</div>
							<div class="flesh_tabs_queue" >სტატუსი</div>
							<div class="flesh_tabs_queue" >ოპერატორი</div>
							<div class="flesh_tabs_queue" >ხ-ბა</div>
							<div class="flesh_tabs_queue" >სულ ხ-ბა</div>
					</div>`;
					html += `<div class="flesh_tabs_wrap_chat flesh_tabs_body">`;

					for(let i=0; i<arr.length; i++){
						let phone = arr[i].phone;
						let chat = arr[i].chat;
						let fb = arr[i].fb;
						let fbc   = arr[i].fbc;
						let mail = arr[i].mail;
						let viber = arr[i].viber;
						let status = '';
						//if(chat.length > 0){
							//console.log(chat.length);
							let call_status = isNull(phone.status).replace(/\s/g, '');

							if(call_status == ''){call_status = 'unavailable';}

							status = `
								<div class="flex">
									<div class="phone_icon"style="background-image:url('media/images/icons/comunication/${call_status}.png?v=1.4')"></div>
									<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
									<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
									<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
									<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
									<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
								</div>
							`;
								
						//}
						for(c=0;c<chat.length;c++){
							

							let browser = `<div class="flex">
							<div class="browser_icon" style="background-image:url('media/images/icons/${chat[c].country}')"></div>
										<div class="browser_icon" style="background-image:url('media/images/icons/${chat[c].browser}.png?v=1.4')"></div>
										<div class="browser_icon" style="background-image:url('media/images/icons/${chat[c].os}.png?v=1.4')"></div>
										
							</div>`;
							let duration = `<div class="flex" style="justify-content: space-between;">
											<div>${chat[c].duration}</div>
											<div class="duration_arrow" style="background-image:url('media/images/icons/${chat[c].duration_arrow == "1" ? "out_chat.png?v=1.4" : "inc_chat.png?v=1.4"}')"></div>
							</div>`;
							html += ` 
							<div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${browser}</div>
							<div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}">${isNull(chat[c].author)}</div>
							<div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(status)}</div>
							<div class="flesh_tabs clickmetostart" data-source="chat" data-hoverer="parent" chat_id = "${chat[c].id}" >${isNull(arr[i].username)} <div class="kkkm" data-hoverer="child" style="position:absolute;display:none;padding:4px !important;right:40vw;border-radius:10px;opacity:0.7;background:#333;color:white;text-align:center;">${isNull(arr[i].username)}</div> </div>
							
							<div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(duration)}</div>
							<div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(chat[c].all_duration)}</div>`;
						}
						

						

							}
							
							html += `</div></div>`;
							html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
							html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue">
					
										<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
										<div class="flesh_tabs_queue" >მომ.ავტორი</div>
										<div class="flesh_tabs_queue" >დრო</div>

									</div>`;

						html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq">`;

							for(let q_c = 0; q_c < queue_chat.length; q_c ++){
								html += ` 
									<div class="flesh_queue clickmetostart" data-source="chat" chat_id = "${queue_chat[q_c].id}"  >${queue_chat[q_c].position}</div>
									<div class="flesh_queue clickmetostart" data-source="chat" chat_id = "${queue_chat[q_c].id}"  >${isNull(queue_chat[q_c].number)}</div>
									<div class="flesh_queue clickmetostart" data-source="chat" chat_id = "${queue_chat[q_c].id}"  >${isNull(queue_chat[q_c].time)}</div>`;
							}

							html += `</div></div>`;

							
					break;
                case "mail": 
					html = `<div id="section_active">
					<div class="flesh_tabs_head">
							<div class="flesh_tabs_title flesh_th">აქტიური</div>
						</div>
					<div class="flesh_tabs_wrap_mail flesh_tabs_head">
						
					
						<div class="flesh_tabs_queue" >მომ.ავტორი</div>
						<div class="flesh_tabs_queue" >სტატუსი</div>
						<div class="flesh_tabs_queue" >ოპერატორი</div>
						<div class="flesh_tabs_queue" >ხ-ბა</div>
						<div class="flesh_tabs_queue" >სულ ხ-ბა</div>
						
					</div>`;
					html += `<div class="flesh_tabs_wrap_mail flesh_tabs_body">`;

					for(let i=0; i<arr.length; i++){
						let phone = arr[i].phone;
						let chat  = arr[i].chat;
						let fb    = arr[i].fb;
						let fbc   = arr[i].fbc;
						let mail  = arr[i].mail;
						let viber = arr[i].viber;

						let call_status = isNull(phone.status).replace(/\s/g, '');

						if(call_status == ''){call_status = 'unavailable';}
						
						//if(mail.length > 0){
							let status = `
								<div class="flex">
									<div class="phone_icon"style="background-image:url('media/images/icons/comunication/${call_status}.png?v=1.4')"></div>
									<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
									<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
									<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
									<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
									<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
								</div>
							`;
						//}
						for(m=0;m<mail.length;m++){

							html += ` 
							<div class="flesh_tabs clickmetostart" data-source="mail" chat_id = "${mail[m].id}">${isNull(mail[m].author)}</div>
							<div class="flesh_tabs clickmetostart" data-source="mail" chat_id = "${mail[m].id}" >${isNull(status)}</div>
							<div class="flesh_tabs clickmetostart" data-source="mail" chat_id = "${mail[m].id}" >${isNull(arr[i].username)}</div>
							<div class="flesh_tabs clickmetostart" data-source="mail" chat_id = "${mail[m].id}"  >${isNull(mail[m].duration)}</div>
							<div class="flesh_tabs clickmetostart" data-source="mail" chat_id = "${mail[m].id}" >${isNull(mail[m].all_duration)}</div>`;
						}
							}
							
							html += `</div></div>`;
							html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
								html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue">
								
								<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
								<div class="flesh_tabs_queue" >მომ.ავტორი</div>
								<div class="flesh_tabs_queue" >დრო</div>

								
							</div>`;
						html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq">`;

							for(let q_m = 0; q_m < queue_mail.length; q_m ++){
								html += ` 
									<div class="flesh_queue clickmetostart" data-source="mail" chat_id = "${queue_mail[q_m].id}"  >${queue_mail[q_m].position}</div>
									<div class="flesh_queue clickmetostart" data-source="mail" chat_id = "${queue_mail[q_m].id}"  >${isNull(queue_mail[q_m].number)}</div>
									<div class="flesh_queue clickmetostart" data-source="mail" chat_id = "${queue_mail[q_m].id}"  >${isNull(queue_mail[q_m].time)}</div>`;
							}

							html += `</div></div>`;
					
					
					
					break;
				case "fbc":      
                    // let users = JSON.parse(data.users);
					html = `
					
					
						<div id="section_active">
							<div class="flesh_tabs_head">
								<div class="flesh_tabs_title flesh_th">აქტიური</div>
							</div>
							<div class="flesh_tabs_wrap_queue_active_fb flesh_tabs_head">
							<div class="flesh_tabs_queue" >ფოტო</div>
							<div class="flesh_tabs_queue" >მომ.ავტორი</div>
							<div class="flesh_tabs_queue" >სტატუსი</div>
							<div class="flesh_tabs_queue" >ოპერატორი</div>
							<div class="flesh_tabs_queue" >ხ-ბა</div>
							<div class="flesh_tabs_queue" >სულ ხ-ბა</div>
						</div>`;
						html += `<div class="flesh_tabs_wrap_fb flesh_tabs_body">`;

						for(let i=0; i<arr.length; i++){
							let phone = arr[i].phone;
							let chat  = arr[i].chat;
							let fb    = arr[i].fb;
							let fbc   = arr[i].fbc;
							let mail  = arr[i].mail;
							let viber = arr[i].viber;
							let status = '';

							//html = fbc;
							let call_status = isNull(phone.status).replace(/\s/g, '');

							if(call_status == ''){call_status = 'unavailable';}
							
							//if(fbc.length > 0){
								status = `
									<div class="flex">
										<div class="phone_icon"style="background-image:url('media/images/icons/comunication/${call_status}.png?v=1.4')"></div>
										<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
										<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
										<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
										<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
										<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
									</div>
								`;
							//}
							for(fc=0;fc < fbc.length; fc++){
								
								let img = `<div class="fb_img" style="background-image:url('${fbc[fc].picture}')"></div>`
								if(fbc[fc].picture == ''){
									img = `<div class="fb_img"></div>`
								}

								html += ` 
								<div class="flesh_tabs clickmetostart" data-source="fbc" chat_id = "${fbc[fc].id}"  >${img}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbc" chat_id = "${fbc[fc].id}"  >${isNull(fbc[fc].author)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbc" chat_id = "${fbc[fc].id}"  >${isNull(status)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbc" chat_id = "${fbc[fc].id}"  >${isNull(arr[i].username)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbc" chat_id = "${fbc[fc].id}"  >${isNull(fbc[fc].duration)}</div>
								<div class="flesh_tabs clickmetostart" data-source="fbc" chat_id = "${fbc[fc].id}"  >${isNull(fbc[fc].all_duration)}</div>`;
							}

						}
						html += `</div></div>`;
						
							html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
							html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue">
					
										<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
										<div class="flesh_tabs_queue" >მომ.ავტორი</div>
										<div class="flesh_tabs_queue" >დრო</div>

									</div>`;

							html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq">`;

							for(let q_fc = 0; q_fc < queue_fbc.length; q_fc ++){
								html += ` 
									<div class="flesh_queue clickmetostart" data-source="fbc" chat_id = "${queue_fbc[q_fc].id}"  >${queue_fbc[q_fc].position}</div>
									<div class="flesh_queue clickmetostart" data-source="fbc" chat_id = "${queue_fbc[q_fc].id}"  >${isNull(queue_fbc[q_fc].number)}</div>
									<div class="flesh_queue clickmetostart" data-source="fbc" chat_id = "${queue_fbc[q_fc].id}"  >${isNull(queue_fbc[q_fc].time)}</div>`;
							}

							html += `</div></div>`;
						
						
						break;
            	case "viber": 
                    // let users = JSON.parse(data.users);
                    html = `
						<div id="section_active">
						<div class="flesh_tabs_head">
							<div class="flesh_tabs_title flesh_th">აქტიური</div>
						</div>
						<div class="flesh_tabs_wrap_fb flesh_tabs_head">
						<div class="flesh_tabs_queue" >ფოტო</div>
						<div class="flesh_tabs_queue" >მომ.ავტორი</div>
						<div class="flesh_tabs_queue" >სტატუსი</div>
						<div class="flesh_tabs_queue" >ოპერატორი</div>
						<div class="flesh_tabs_queue" >ხ-ბა</div>
						<div class="flesh_tabs_queue" >სულ ხ-ბა</div>
						
					</div>`;
					html += `<div class="flesh_tabs_wrap_fb flesh_tabs_body">`;

					for(let i=0; i<arr.length; i++){
						let phone = arr[i].phone;
						let chat  = arr[i].chat;
						let fb    = arr[i].fb;
						let fbc   = arr[i].fbc;
						let mail  = arr[i].mail;
						let viber = arr[i].viber;
						let status = '';

						let call_status = isNull(phone.status).replace(/\s/g, '');

						if(call_status == ''){call_status = 'unavailable';}
						
						//if(viber.length > 0){
							status = `
								<div class="flex">
									<div class="phone_icon"style="background-image:url('media/images/icons/comunication/${call_status}.png?v=1.4')"></div>
									<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
									<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
									<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
									<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
									<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
								</div>
							`;
						//}
						for(v=0;v<viber.length;v++){
							

							let img = `<div class="fb_img" style="background-image:url('${viber[v].picture}')"></div>`
							if(viber[v].picture == ''){
								img = `<div class="fb_img"></div>`
							}
							
							html += ` 
							<div class="flesh_tabs clickmetostart" data-source="viber" chat_id = "${viber[v].id}"  >${img}</div>
							<div class="flesh_tabs clickmetostart" data-source="viber" chat_id = "${viber[v].id}"  >${isNull(viber[v].author)}</div>
							<div class="flesh_tabs clickmetostart" data-source="viber" chat_id = "${viber[v].id}"  >${isNull(status)}</div>
							<div class="flesh_tabs clickmetostart" data-source="viber" chat_id = "${viber[v].id}"  >${isNull(arr[i].username)}</div>
							<div class="flesh_tabs clickmetostart" data-source="viber" chat_id = "${viber[v].id}"  >${isNull(viber[v].duration)}</div>
							<div class="flesh_tabs clickmetostart" data-source="viber" chat_id = "${viber[v].id}"  >${isNull(viber[v].all_duration)}</div>`;
						}

					}
					html += `</div></div>`;
					html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
					html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue">
					
					<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
					<div class="flesh_tabs_queue" >მომ.ავტორი</div>
					<div class="flesh_tabs_queue" >დრო</div>

					
					</div>`;

					html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq">`;

					for(let q_v = 0; q_v < queue_viber.length; q_v ++){
						html += ` 
							<div class="flesh_queue clickmetostart" data-source="viber" chat_id = "${queue_viber[q_v].id}"  >${queue_viber[q_v].position}</div>
							<div class="flesh_queue clickmetostart" data-source="viber" chat_id = "${queue_viber[q_v].id}"  >${isNull(queue_viber[q_v].number)}</div>
							<div class="flesh_queue clickmetostart" data-source="viber" chat_id = "${queue_viber[q_v].id}"  >${isNull(queue_viber[q_v].time)}</div>`;
					}

					html += `</div></div>`;
					
                
                
					break;
				case "video": 
                    
                    
                    // let users = JSON.parse(data.users);
                    html = `
					
					
					<div id="section_active">
					<div class="flesh_tabs_head">
						<div class="flesh_tabs_title flesh_th">აქტიური</div>
					</div>
					<div class="flesh_tabs_wrap_fb flesh_tabs_head">
					<div class="flesh_tabs_queue" >ფოტო</div>
                    <div class="flesh_tabs_queue" >მომ.ავტორი</div>
                    <div class="flesh_tabs_queue" >სტატუსი</div>
                    <div class="flesh_tabs_queue" >ოპერატორი</div>
                    <div class="flesh_tabs_queue" >ხ-ბა</div>
                    <div class="flesh_tabs_queue" >სულ ხ-ბა</div>
                    
					</div>`;
					html += `<div class="flesh_tabs_wrap_fb flesh_tabs_body">`;

					for(let i=0; i<arr.length; i++){
						let phone = arr[i].phone;
						let chat  = arr[i].chat;
						let fb    = arr[i].fb;
						let fbc   = arr[i].fbc;
						let mail  = arr[i].mail;
						let viber = arr[i].viber;
						let video = arr[i].video;
						let status = '';

						let call_status = isNull(phone.status).replace(/\s/g, '');

						if(call_status == ''){call_status = 'unavailable';}
						
						//if(viber.length > 0){
							status = `
								<div class="flex">
									<div class="phone_icon"style="background-image:url('media/images/icons/comunication/${call_status}.png?v=1.4')"></div>
									<div ${chat_displey} class="chat_icon">${chat.length > 0 ? `<div class="blink_counter chat_color">${isNull(chat.length)}</div>` : ``}</div>
									<div ${fbm_displey} class="fb_icon">${fb.length > 0 ? `<div class="blink_counter fb_color">${isNull(fb.length)}</div>` : ``}</div>
									<div ${mail_displey} class="mail_icon">${mail.length > 0 ? `<div class="blink_counter mail_color">${isNull(mail.length)}</div>` : ``}</div>
									<div ${fbc_displey} class="fbc_icon">${fbc.length > 0 ? `<div class="blink_counter fbc_color">${isNull(fbc.length)}</div>` : ``}</div>
									<div ${viber_displey} class="viber_icon">${viber.length > 0 ? `<div class="blink_counter viber_color">${isNull(viber.length)}</div>` : ``}</div>
									<div ${video_displey} class="viber_icon">${video.length > 0 ? `<div class="blink_counter video_color">${isNull(video.length)}</div>` : ``}</div>
								</div>
							`;
						//}
						for(v = 0; v < video.length; v++){
							

							let img = `<div class="fb_img" style="background-image:url('${video[v].picture}')"></div>`
							if(video[v].picture == ''){
								img = `<div class="fb_img"></div>`
							}
							
							html += ` 
							<div class="flesh_tabs clickmetostart" data-source="video" data-open-video="${video[v].status_id}" chat_id = "${video[v].id}"  >${img}</div>
							<div class="flesh_tabs clickmetostart" data-source="video" data-open-video="${video[v].status_id}" chat_id = "${video[v].id}"  >${isNull(video[v].author)}</div>
							<div class="flesh_tabs clickmetostart" data-source="video" data-open-video="${video[v].status_id}" chat_id = "${video[v].id}"  >${isNull(status)}</div>
							<div class="flesh_tabs clickmetostart" data-source="video" data-open-video="${video[v].status_id}" chat_id = "${video[v].id}"  >${isNull(arr[i].username)}</div>
							<div class="flesh_tabs clickmetostart" data-source="video" data-open-video="${video[v].status_id}" chat_id = "${video[v].id}"  >${isNull(video[v].duration)}</div>
							<div class="flesh_tabs clickmetostart" data-source="video" data-open-video="${video[v].status_id}" chat_id = "${video[v].id}"  >${isNull(video[v].all_duration)}</div>`;
						}

					}
					html += `</div></div>`;
					html += `<div id="section_waiting"><div class="flesh_tabs_title">რიგი</div>`
					html += `<div class="flesh_tabs_wrap_queue flesh_tabs_head_queue">
					
					<div class="flesh_tabs_queue" style="margin-right: -1px;">#</div>
					<div class="flesh_tabs_queue" >მომ.ავტორი</div>
					<div class="flesh_tabs_queue" >დრო</div>

					
					</div>`;

					html += `<div class="flesh_tabs_wrap_queue flesh_tabs_bodyq">`;

						for(let q_v = 0; q_v < queue_video.length; q_v ++){
							html += ` 
								<div class="flesh_queue clickmetostart" data-source="video" data-open-video="0" chat_id = "${queue_video[q_v].id}"  >${queue_video[q_v].position}</div>
								<div class="flesh_queue clickmetostart" data-source="video" data-open-video="0" chat_id = "${queue_video[q_v].id}"  >${isNull(queue_video[q_v].number)}</div>
								<div class="flesh_queue clickmetostart" data-source="video" data-open-video="0" chat_id = "${queue_video[q_v].id}"  >${isNull(queue_video[q_v].time)}</div>`;
						}

						html += `</div></div>`;
						
						
					
					break;
				default:
					
					
					for(let i=0; i<arr.length; i++){
						
						let phone = arr[i].phone;
						for(p=0; p<phone.length; p++){
							var used_ext = localStorage.getItem('used_ext');
							if(phone[p].status != 'busy' && phone[p].ext == used_ext && isNull(arr[i].username))
							{
								localStorage.setItem('already_clicked', 0);
							}
                            if(isNull(phone[p].ext) != ''){
								
								var already_clicked = localStorage.getItem('already_clicked');
								if(phone[p].ext == used_ext && isNull(arr[i].username) != '' && already_clicked == 0 && phone[p].type == 'in'){   //999 SHOULD BE REPLACED EVERYWHERE BY EXTENSION FROM LOCAL STORAGE
									//alert('first: '+already_clicked);
									if(isNull(phone[p].number) != '' && phone[p].number != used_ext && phone[p].status == 'busy'){
										$("#phone_dial").html('');
										$("#phone_dial").html(`<div id="dialog_to_open_`+phone[p].number+`" style="cursor: pointer; padding: 7px 10px;"  class="open_dialog" data-source="phone" extension="${isNull(phone[p].ext)}" number="${isNull(phone[p].number)}">${isNull(phone[p].number)}</div>`);
										already_clicked++;
										localStorage.setItem('already_clicked', already_clicked);
										var dialog_opened = localStorage.getItem('dialog_opened');
										if(dialog_opened == 0){
											var source_name = $("#chat_source").val();
											//alert(123);
											if(source_name == 'chat'){
												var baseID = $("#ast_source").val();
												var base;
												baseID = baseID.split("-");

												if(baseID != ''){
													base = baseID[1];
												}
												$.ajax({
													url: "server-side/call/incomming.action.php",
													data: {
														'act'		: 'get_inputs_ids',
														'setting_id': 4,
														'base_id': base
													},
													success: function(data) {
														var dialogtt = $("#dialogType").val();
														var idd = $("#hidden_incomming_call_id").val();
														if(dialogtt == 'autodialer'){
															idd = '';
														}
														var query = 'act=save-custom&setting_id=4&processing_id='+idd;
														var query2 = 'act=save-custom-quize&baseID='+base;
														query += "&region="+$("#region").val()+"&municip="+$("#municip").val()+"&temi="+$("#temi").val()+"&village="+$("#village").val();
														if(data.input_ids != ''){
															var input_ids   = data.input_ids;
															var checkboxes  = data.checkboxes;
															var campaign_ids = data.campaign_ids;
															var campaignCheckboxes = data.campaignCheckboxes;
															var ready_to_save = 0;

															campaign_ids.forEach(function(item,index){
																if($("#"+item+"[data-nec='1']").val() == '' || $("#"+item+"[data-nec='1']").val() == '0')
																{
																	$("#"+item).css('border','1px solid red');
																	var transformed = item.replace(/--/g, "__");
																	$("#"+transformed+"_chosen .chosen-single").css('border','1px solid #e81f1f');
																	//ready_to_save++;
																	//alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
																}
																else{
																	if($("#"+item).val() != ''){
																		query2 += "&"+item+'='+$("#"+item).val();
																	}
																}
																
															});
															campaignCheckboxes.forEach(function(item,index){
																$("input[name='"+item+"']:checked").each(function() {
																	var post_data = this.id;
																	var check = post_data.split("--");

																	var check_id = check[0].split("-");

																	query2 += "&"+this.id+"="+check_id[1];
																});
															});
															input_ids.forEach(function(item,index){
																if($("#"+item+"[data-nec='1']").val() == '' || $("#"+item+"[data-nec='1']").val() == '0')
																{
																	$("#"+item).css('border','1px solid red');
																	var transformed = item.replace(/--/g, "__");
																	$("#"+transformed+"_chosen .chosen-single").css('border','1px solid #e81f1f');
																	//ready_to_save++;
																	//alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
																}
																else{
																	if($("#"+item).val() != ''){
																		query += "&"+item+'='+$("#"+item).val();
																	}
																}
																
															});

															checkboxes.forEach(function(item,index){
																$("input[name='"+item+"']:checked").each(function() {
																	var post_data = this.id;
																	var check = post_data.split("--");

																	var check_id = check[0].split("-");

																	query += "&"+this.id+"="+check_id[1];
																});
															});
															
															//default deps//
															var dep = $("#dep_0").val();
															var dep_project = $("#dep_project_0").val();
															var dep_sub_project = $("#dep_sub_project_0").val();
															var dep_sub_type = $("#dep_sub_type_0").val();

															query += "&dep_0="+dep;
															query += "&dep_project_0="+dep_project;
															query += "&dep_sub_project_0="+dep_sub_project;
															query += "&dep_sub_type_0="+dep_sub_type;
															
															////////////////

															$(".dep").each(function(item){
																var rand_number = $(this).attr('data-id');
																query += "&dep_"+rand_number+"="+$(this).val();
															});
															$(".dep_project").each(function(item){
																var rand_number = $(this).attr('data-id');
																query += "&dep_project_"+rand_number+"="+$(this).val();
															});
															$(".dep_sub_project").each(function(item){
																var rand_number = $(this).attr('data-id');
																query += "&dep_sub_project_"+rand_number+"="+$(this).val();
															});
															$(".dep_sub_type").each(function(item){
																var rand_number = $(this).attr('data-id');
																query += "&dep_sub_type_"+rand_number+"="+$(this).val();
															});
																
															


															query += "&dialogtype="+$("#dialogType").val();
															if(ready_to_save == 0){
																/* $.ajax({
																	url: CustomSaveURL,
																	type: "POST",
																	data: query2,
																	dataType: "json",
																	success: function (data) {
																	}
																}); */
																$.ajax({
																	url: "server-side/view/automator.action.php",
																	type: "POST",
																	data: query,
																	dataType: "json",
																	success: function (data) {
																		alert('ინფორმაცია შენახულია');
																		/* $("#kendo_incomming_table").data("kendoGrid").dataSource.read();
																		var source_name = $("#chat_source").val();
																		if(source_name == 'chat'){
																			$.ajax({
																				url: Comunications_URL,
																				data: "act=chat_end&chat_source=" +$("#chat_source").val()+ "+&chat_id=" + $("#chat_original_id").val(),
																				success: function (data) {
																				if (typeof data.error != "undefined") {
																					if (data.error != "") {
																					// alert(data.error);
																					//alert("Caucasus Jixv");
																					} else {
																					$("#chat_close_dialog").dialog("close");
																					}
																				}
																				}
																			});
																		} */
																		
																	}
																});
															}
															//console.log(query);
															console.log(query2);
														}
													}
												});
											}
											$("#dialog_to_open_"+phone[p].number).click();
											$("#phone_dial").html();
											$.ajax({
													url: "AsteriskManager/dndAction.php",
													data: {
														action: 'dndon',
														activity_id: 5,
													},
													success: function(res) {
														var statusBG = $('#status-place > a > x');
														var statusName = $('#status-place > a > span');
														$('#aStatus li[data-id=8]').css("background-color", "");
														statusBG.css("background-color", "#a9009f");
														statusName.text('დამუშავება');

														$('.icon_status_color').css("background-color", "#a9009f");
														console.log(res);
													},
													error: function(e) {
														alert("dnd error ajax");
														console.log(e);
													}
												});
												$.ajax({
													url: 'includes/menu.action.php',
													data: {
														act: "activity_status",
														status_id: 5
													},
													success: function(res) {

													}
												});
											localStorage.setItem('dialog_opened', 1);
											
										}
										/* else{
											localStorage.setItem('extension', phone[p].ext);
											localStorage.setItem('number', phone[p].number);
										} */
										
										//alert('second: '+already_clicked);
									}
								}
								else if(phone[p].ext == used_ext && isNull(arr[i].username) != '' && already_clicked == 0 && phone[p].type == 'in-autodialer'){
									if(isNull(phone[p].number) != '' && phone[p].number != used_ext && phone[p].status == 'busy'){
										$("#phone_dial").html('');
										$("#phone_dial").html(`<div id="dialog_to_open" style="cursor: pointer; padding: 7px 10px;"  class="open_dialog" data-source="phone" data-calltype="autodialer" extension="${isNull(phone[p].ext)}" number="${isNull(phone[p].number)}">${isNull(phone[p].number)}</div>`);
										already_clicked++;
										localStorage.setItem('already_clicked', already_clicked);
										var dialog_opened = localStorage.getItem('dialog_opened');
										if(dialog_opened == 0){
											
											localStorage.setItem('dialog_opened', 1);
											$("#dialog_to_open").click();
											//$("#phone_dial").html();
										}
										/* else{
											localStorage.setItem('extension', phone[p].ext);
											localStorage.setItem('number', phone[p].number);
										} */
										
										//alert('second: '+already_clicked);
									}
								}
								else if(phone[p].ext == used_ext && isNull(arr[i].username) != '' && already_clicked == 0 && phone[p].type == 'out'){   //999 SHOULD BE REPLACED EVERYWHERE BY EXTENSION FROM LOCAL STORAGE
									//alert('first: '+already_clicked);
									console.log(phone[p].number);
									if(isNull(phone[p].number) != '' && phone[p].number.length >= 9 && phone[p].number != used_ext && phone[p].status == 'busy' && phone[p].exist_call == 1){
										$("#phone_dial").html('');
										$("#phone_dial").html(`<div id="dialog_to_open_`+phone[p].number+`" style="cursor: pointer; padding: 7px 10px;"  class="open_dialog" data-source="phone" data-calltype="outgoing" extension="${isNull(phone[p].ext)}" number="${isNull(phone[p].number)}">${isNull(phone[p].number)}</div>`);
										already_clicked++;
										localStorage.setItem('already_clicked', already_clicked);
										var dialog_opened = localStorage.getItem('dialog_opened');
										if(dialog_opened == 0){
											$("#dialog_to_open_"+phone[p].number).click();
											$("#phone_dial").html();
											localStorage.setItem('dialog_opened', 1);
										}
										/* else{
											localStorage.setItem('extension', phone[p].ext);
											localStorage.setItem('number', phone[p].number);
										} */
									}
								}
                            }
                        }
					}
				break;
            }
			
            $("#flesh_panel").html(html);

			// hover tool tips for flash panel chat section
			$("div[data-hoverer='parent'").hover(function(){
				//$("div[data-hoverer='child'").show();
				$(this).children().show();
			},function(){
				$("div[data-hoverer='child'").hide();
			});

			$('#section_active').scrollTop(scroll_numbers);
            ///////////////////////----------end flesh paneli----------------//////////////////////////
			
            if($("#gare_div").css("display") == "none"){
                $(".add-edit-form-comunications-class").css("width","941");
            }

            if(all_chats ==2 && !is_dialog_open())	{
                all_chats = 0;
            }
			
            if($('#add-edit-form-comunications input').length != 0 && all_chats){
                $.ajax({
                    async: true,
                    url: "server-side/call/getchats.action.php",
                    data: {
                        'act':'getchats',
                        'chat_id'		:$('#chat_original_id').val(),
                        'chat_detail_id':1,
                        'source'		:$("#chat_source").val()
                    },
                    dataType: "json",
                    success: function(data) {
                        $('#chat_flag,#chat_browser,#chat_device').css('display','none');
                        if(data.chat_seen == 1){
                            $("#span_seen").css('display', '');
                            // document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
                        }else{
                            $("#span_seen").css('display', 'none');
                        }
                        if(data.chat_typing == 1){
                            $("#chat_typing").show();
                        }else{
                            $("#chat_typing").hide();
                        }
                        if(data.chat_detail_id && data.chat_detail_id != chat_detail_id){
                            $('#log').append(data.sms);
                            chat_detail_id=data.chat_detail_id;
                            document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
                        }
                        if(data.count != 0 && $D.sessionStatus == 1){
                            $('#chat_count').css('background','#FFCC00');
                            setTimeout(function(){
                                $('#chat_count').css('background','#F44336');
                            }, 400);
                            var audio = new Audio('sms.mp3');
							audio.play();

                        }else{
                            $('#chat_count').css('background','none');
                        }

                        if(data.chat_status == "12"){
                            $("#chat_close_text").show();
                        }
                        else{
                            $("#chat_close_text").hide();
                        }

                        $("#incoming_chat_tab_chat1").html(data.page.chat);
                        $("#incoming_chat_tab_chat_queue").html(data.page.chat_queue);

                        if(data.page.chat_color_status == 1){
                            $(".incoming_chat_tab_chat").css('background-color','#ffeb3b');
                            setTimeout(function(){
                                $(".incoming_chat_tab_chat").css('background-color','#fff');
                            }, 400);
                        }else{
                            $(".incoming_chat_tab_chat").css('background-color','#fff');
                        }


                        $("#incoming_chat_tab_mail1").html(data.page.mail);
                        $("#incoming_chat_tab_mail_queue").html(data.page.mail_queue);

                        if(data.page.mail_color_status == 1){
                            $(".incoming_chat_tab_mail").css('background-color','#ffeb3b');
                            setTimeout(function(){
                                $(".incoming_chat_tab_mail").css('background-color','#fff');
                            }, 400);
                        }else{
                            $(".incoming_chat_tab_mail").css('background-color','#fff');
                        }

                        $("#incoming_chat_tab_fbm1").html(data.page.fbm);
                        $("#incoming_chat_tab_fbm_queue").html(data.page.fbm_queue);

                        if(data.page.fbm_color_status == 1){
                            $(".incoming_chat_tab_fbm").css('background-color','#ffeb3b');
                            setTimeout(function(){
                                $(".incoming_chat_tab_fbm").css('background-color','#fff');
                            }, 400);
                        }else{
                            $(".incoming_chat_tab_fbm").css('background-color','#fff');
						}
						
						$("#incoming_chat_tab_fbc1").html(data.page.fbc);
                        $("#incoming_chat_tab_fbc_queue").html(data.page.fbc_queue);

                        if(data.page.fbc_color_status == 1){
                            $(".incoming_chat_tab_fbc").css('background-color','#febc1b');
                            setTimeout(function(){
                                $(".incoming_chat_tab_fbc").css('background-color','#fff');
                            }, 400);
                        }else{
                            $(".incoming_chat_tab_fbc").css('background-color','#fff');
                        }

                        $("#incoming_chat_tab_viber1").html(data.page.viber);
                        $("#incoming_chat_tab_viber_queue").html(data.page.viber_queue);

                        if(data.page.fbm_color_status == 1){
                            $(".incoming_chat_tab_viber").css('background-color','#ffeb3b');
                            setTimeout(function(){
                                $(".incoming_chat_tab_viber").css('background-color','#fff');
                            }, 400);
                        }else{
                            $(".incoming_chat_tab_viber").css('background-color','#fff');
                        }

                        $("#incoming_chat_video1").html(data.page.video);
						$("#incoming_chat_video_queue").html(data.page.video_queue);
						
						$("#incoming_chat_tab_phone1").html(data.page.phone);
                        $("#incoming_chat_tab_phone_queue").html(data.page.phone_queue);

                        if(all_chats==1){
                            $("#incoming_chat_tabs").tabs();
                        }

                        all_chats = 2;
						$('#main_call_chat').css('display','none');
                        $("#loading").hide();
						ajaxtimer = setTimeout(runAjax, 1000);
						
                    }
                });

            }else{
                ajaxtimer = setTimeout(runAjax, 1000);
                $("#loading").hide();
            }
			

        }

    }).done(function(data) {

    });

}

        </script>
		</head>
		<body >

			<div id="npm"></div>
			<!--<div id="loading" style="z-index: 999999;padding: 160px 0;height: 100%;width: 100%;position: fixed;background: #463e29c2;top: 0;">
			    <div class="loading-circle-1">
			    </div>
			    <div class="loading-circle-2">
			    </div>
			</div>-->
			<?php require_once('includes/pages.php'); ?>

			<div id="newsmain">

			</div>
			<!-- jQuery Dialog -->
        	<div id="logout_dialog" title="ქმედება">
        	</div>
        	<!-- jQuery Dialog -->
        	<div id="logout" title="ავტომატური გამოსვლა სისტემიდან">
			</div>

			<!-- activitie screen disabler -->
			<div class="activitie-screen-disabler">
				<div>
					<div class="activitie-time-counter">
						<span>00:00:00</span>
					</div>
					<button id="returnFromActivitie">
						აქტივობიდან დაბრუნება
					</button>
				</div>
				<div class="activitie-cur-time">
					00:00:00
				</div>
			</div>
			<span style="display:none;" id="phone_dial"></span>
		</body>
		
		<script type="text/javascript" src="dist/js/production/main.min.js"></script> 

	</html>
<?php endif;?>
