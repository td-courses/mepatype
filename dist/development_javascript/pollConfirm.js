// const confirmPollBtn = document.querySelectorAll('.confirmPollBtn')


const confirmPoll = (confirmButton) => {
    let selectedValues;
    var selectedChecksValues = []
    var checkboxes = confirmButton.parentElement.querySelectorAll('input[type=checkbox]:checked')
    var selectedRadioValues = []
    var radio = confirmButton.parentElement.querySelector('input[type=radio]:checked')
    var pollId = confirmButton.parentElement.parentElement.parentElement.dataset.postid;
    let inputType;
    let pollsContainer = confirmButton.previousElementSibling;
    let pollsRadioTemplate;

    for (var i = 0; i < checkboxes.length; i++) {
        selectedChecksValues.push(checkboxes[i].value)
    }


    // for (var i = 0; i < radio.length; i++) {
    //     selectedRadioValues.push(radio[i].value)
    // }



    if (!radio) {
        selectedValues = selectedChecksValues
        inputType = "checkbox"
    } else {
        selectedValues = radio.value
        inputType = "radio"
    }


    let pollToAdd = {
        act: "set_survey_radio",
        id: pollId,
        text: selectedValues,
        inputType: inputType
    }


    console.log(pollToAdd);



    var AjaxURL = "server-side/call/action_action.php";
    let parser = new DOMParser();

    $.ajax({
        url: AjaxURL,
        data: pollToAdd,
        success: function (answers) {

            let Answers = answers.answers.answers;
            console.log("Answers");
            console.log(Answers);

            pollsContainer.innerHTML = "";

            let type = answers.answers.type
            for (let i = 0; i < Answers.length; i++) {

                let checked;
                if (Answers[i].status == 'check') {
                    checked = 'checked'
                } else {
                    checked = ''
                }

                pollsRadioTemplate = parser.parseFromString(`
                                        <div class="choise">
                                            <div class="choise__input">
                                                <label class="checkmark_container">
                                                    <p>${Answers[i].text}</p>
                                                    <input type="${type}" ${checked} name="check" value="${Answers[i].text}"> 
                                                    <span class="checkmark checkmark__${type}"></span>
                                                </label>
                                                <div class="single_pole">

                                                    <svg class="pole_stick__filled" width="334" height="4" viewBox="0 0 334 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M0 2H334" stroke="#CECECE" stroke-width="3"></path>
                                                        <path class="filled-bar" data-filled="${Answers[i].votes}" d="M0 2L334 2.00003" stroke="black" stroke-width="3" style="stroke-dashoffset: 230.46px;"></path>
                                                    </svg>
                                                    <p class="polle_procent">${Answers[i].votes}%</p>
                                                </div>
                                            </div>
                                        </div>
                                    `, "text/html")
                pollsContainer.append(pollsRadioTemplate.body.firstElementChild)


            }


            let path = document.querySelectorAll('.filled-bar');

            path.forEach(path => {
                let polleProcent = path.parentElement.parentElement.querySelector('.polle_procent');
                console.log(path.getTotalLength());

                let pathFilled = path.dataset.filled;
                polleProcent.innerHTML = `${pathFilled}%`;
                let length = path.getTotalLength();

                let oneProcent = length * 0.1 * 0.1;
                let filledProcent = 100 - pathFilled


                path.style.strokeDashoffset = oneProcent * filledProcent


            })



        }
    })


}
