<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$action     = $_REQUEST['act'];
$error        = '';
$filterID   = '';
$data        = array();
$user_id    = $_SESSION['USERID'];

switch ($action) {
    case 'get_call_source':
        $db->setQuery(" SELECT  id,
                                name
                        FROM    source
                        WHERE   actived = 1
                        ORDER BY id ASC");
        $data = $db->getResultArray();
        break;
    case 'get_department':
        $db->setQuery(" SELECT  id,name
                        FROM    info_category
                        WHERE   actived = 1 AND parent_id = 0
                        ORDER BY id ASC");
        $data = $db->getResultArray();
        break;
    case 'get_project':
        $db->setQuery(" SELECT  	`cat1`.id,
                                    `cat1`.`name`
                        FROM 		`info_category`
                        LEFT JOIN 	`info_category` AS `cat1` ON `cat1`.`parent_id` = `info_category`.`id`
                        WHERE 	 	`info_category`.`parent_id` = 0 AND `info_category`.actived=1");
        $data = $db->getResultArray();
        break;
    case 'get_region':
        $db->setQuery(" SELECT  id,
                                name
                        FROM    regions

                        WHERE   actived = 1 AND parent_id = 0
                        ORDER BY id ASC");
        $data = $db->getResultArray();
        break;
    case 'get_municip':
        $db->setQuery(" SELECT 	id,
                                name
                        FROM 	regions
                        WHERE 	actived = 1 AND parent_id != 0 AND parent_id IN(SELECT id FROM regions WHERE actived = 1 AND parent_id = 0)
                        ORDER BY id ASC");
        $data = $db->getResultArray();
        break;
    case 'get_status':
        $db->setQuery("SELECT   id,
                                name
                        FROM    crm_status
                        WHERE   actived = 1
                        ORDER BY id ASC");
        $data = $db->getResultArray();
        break;
    case 'get_operator':
        $db->setQuery(" SELECT 	users.id,
                                user_info.name
                        FROM 	users
                        JOIN 	user_info ON user_info.user_id = users.id
                        WHERE 	users.actived = 1
                        ORDER BY id ASC");
        $data = $db->getResultArray();
        break;
    case 'get_columns':

        $columnCount =         $_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] =     $_REQUEST['names'];
        $operators[] =         $_REQUEST['operators'];
        $selectors[] =         $_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f = 0;
        foreach ($cols[0] as $col) {
            $column = explode(':', $col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach ($res as $item) {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }


        $dat = array();
        $a = 0;
        for ($j = 0; $j < $columnCount; $j++) {
            if (1 == 2) {
                continue;
            } else {

                if ($operators[0][$a] == 1) $op = true;
                else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if ($columns[$j] == 'id') {
                    $width = "5%";
                } else if ($columns[$j] == 'position') {
                    $width = "12%";
                } else {
                    $width = 'auto';
                }
                if ($columns[$j] == 'inc_id') {
                    $hidden = true;
                } else if ($columns[$j] == 'id') {
                    $hidden = true;
                } else {
                    $hidden = false;
                }
                if ($res['data_type'][$j] == 'date') {
                    $g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
                } else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
                } else {
                    $g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
                }
                $a++;
            }
            array_push($dat, $g);
        }

        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for ($j = 0; $j < $columnCount; $j++) {
            if ($types[$j] == 'date') {
                $new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
            } else if ($types[$j] == 'number') {

                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'number');
            } else {
                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
            }
        }

        $filtArr = array('fields' => $new_data);



        $kendoData = array('columnss' => $dat, 'modelss' => $filtArr);


        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';

        break;
    case 'get_list':
        $columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];

        $start_date     = $_REQUEST['start_date'];
        $end_date       = $_REQUEST['end_date'];
        $department     = $_REQUEST['department'];
        $project        = $_REQUEST['project'];
        $region         = $_REQUEST['region'];
        $municip        = $_REQUEST['municip'];
        $status         = $_REQUEST['status'];
        $operator       = $_REQUEST['operator'];
        $call_type      = $_REQUEST['call_type'];
        if ($department != '') {
            $department_filter = " AND uwyeba.int_value IN($department)";
        }
        if ($project != '') {
            $project_filter = " AND project.int_value IN($project)";
        }
        if ($region != '') {
            $region_filter = " AND region.int_value IN($region)";
        }
        if ($municip != '') {
            $municip_filter = " AND municip.int_value IN($municip)";
        }
        if ($status != '') {
            $status_filter = " AND incomming_request_processing.int_value IN($status) OR compaign_request_processing.int_value IN($status)";
        }
        if ($operator != '') {
            $operator_filter = " AND incomming_call.user_id IN($operator)";
        }
        if ($call_type != '') {
            $call_type_filter = " AND incomming_call.source_id IN($call_type)";
        }
        $db->setQuery(" SELECT  IF(asterisk_call_log.call_status_id = 9, '', incomming_call.id),
                                incomming_call.date,
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN incomming_call.phone
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.phone
                                    WHEN NOT ISNULL(incomming_call.fb_chat_id)  THEN fb_chat.sender_name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN proc_phone.value
                                    WHEN NOT ISNULL(incomming_call.viber_chat_id)  THEN viber_chat.sender_name
                                END AS `phone`,									
                                CASE
                                WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN mail.sender_name
                                END AS `mom_auth`,	
                                IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba_dat.name,uwyeba_dat2.name) as 'uwyeba',
                                IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),project_dat.name,project_dat2.name)as 'project',
                                regions_2.name AS 'region',
                                municip_2.name AS 'municip',
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(IFNULL(IFNULL(user_info1.`name`,user_info.`name`), ''), '(',asterisk_extension.number, ')'),user_info1.`name`),user_info.name)
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN user_info1.name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN user_info1.name
                                END AS `user_name`,

                                
                                
                                
                                CASE
                                    WHEN incomming_request_processing.int_value = 1 OR compaign_request_processing.int_value = 1 THEN 'გადაცემულია გასარკვევად'
                                    WHEN incomming_request_processing.int_value = 2 OR compaign_request_processing.int_value = 2 THEN 'გარკვევის პროცესშია'
                                    WHEN incomming_request_processing.int_value = 3 OR compaign_request_processing.int_value = 3 THEN 'დასრულებულია'
                                END AS `status`,
                                mepa_project_types.name AS `mom_type`

                        FROM        incomming_call					
                        LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                        LEFT JOIN	asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                        LEFT JOIN	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
                        LEFT JOIN   asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
                        LEFT JOIN   user_info ON user_info.user_id = asterisk_call_log.user_id
                        LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                        LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                        LEFT JOIN   info_category ON info_category.id = incomming_call.cat_1
                        LEFT JOIN   `chat` ON `chat`.id = incomming_call.chat_id
                        LEFT JOIN   fb_chat ON fb_chat.id = incomming_call.fb_chat_id
                        LEFT JOIN   mail ON mail.id = incomming_call.mail_chat_id
                        LEFT JOIN   viber_chat ON viber_chat.id = incomming_call.viber_chat_id
                        LEFT JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 136
                        LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = incomming_call.id AND proc_name.processing_setting_detail_id = 127
                        LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = incomming_call.id AND proc_lastname.processing_setting_detail_id = 128
                        LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = incomming_call.id AND proc_phone.processing_setting_detail_id = 124
                        LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = incomming_call.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
                        LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = incomming_call.id AND project.processing_setting_detail_id = 0 AND project.value = 2
                        LEFT JOIN 	incomming_request_processing AS `region` ON region.incomming_request_id = incomming_call.id AND region.processing_setting_detail_id = 999999999 AND region.value = 1111
                        LEFT JOIN 	incomming_request_processing AS `municip` ON municip.incomming_request_id = incomming_call.id AND municip.processing_setting_detail_id = 999999999 AND municip.value = 2222
                        LEFT JOIN 	incomming_request_processing AS `m_type` ON m_type.incomming_request_id = incomming_call.id AND m_type.processing_setting_detail_id = 0 AND m_type.value = 4


                        LEFT JOIN   compaign_request_processing ON compaign_request_processing.base_id = outgoing_campaign_request_base.id AND compaign_request_processing.processing_setting_detail_id = 136
                        LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
                        LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
                        LEFT JOIN   compaign_request_processing AS `proc_phone2` ON proc_phone2.base_id = outgoing_campaign_request_base.id AND proc_phone2.processing_setting_detail_id = 124
                        LEFT JOIN 	compaign_request_processing AS `uwyeba2` ON uwyeba2.base_id = outgoing_campaign_request_base.id AND uwyeba2.processing_setting_detail_id = 0 AND uwyeba2.value = 1
                        LEFT JOIN 	compaign_request_processing AS `project2` ON project2.base_id = outgoing_campaign_request_base.id AND project2.processing_setting_detail_id = 0 AND project2.value = 2
                        LEFT JOIN 	info_category AS `uwyeba_dat` ON uwyeba_dat.id = uwyeba.int_value
                        LEFT JOIN 	info_category AS `project_dat` ON project_dat.id = project.int_value

                        LEFT JOIN 	info_category AS `uwyeba_dat2` ON uwyeba_dat2.id = uwyeba2.int_value
                        LEFT JOIN 	info_category AS `project_dat2` ON project_dat2.id = project2.int_value

                        LEFT JOIN		regions AS `regions_2` ON regions_2.id = region.int_value
                        LEFT JOIN		regions AS `municip_2` ON municip_2.id = municip.int_value
                        LEFT JOIN       mepa_project_types ON mepa_project_types.id = m_type.int_value
                        WHERE       (asterisk_call_log.call_type_id = 1 OR asterisk_call_log.call_type_id = 5 OR ISNULL(asterisk_call_log.call_type_id)) AND NOT ISNULL(incomming_call.source_id) AND (incomming_call.noted!=2 OR incomming_call.noted IS NULL ) AND DATE(incomming_call.date) BETWEEN '$start_date' AND '$end_date'  
                        $department_filter $project_filter $region_filter $municip_filter $status_filter $operator_filter $call_type_filter
                        GROUP BY  	`incomming_call`.`id`,uwyeba.additional_deps
                        ORDER BY 	`incomming_call`.`id` DESC");


        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
        break;
}

$data['error'] = $error;
echo json_encode($data);
