<?php

session_start();

// REQUIRED FILES
require_once('../classes/class.Mysqli.php');
require_once('../libraries/phpmailer/PHPMailerAutoload.php');
require_once('../libraries/phpmailer/class.smtp.php');

global $db;
$db = new dbClass();

$action = $_REQUEST['act'];
$error  = '';

$user_id = $_SESSION['USERID'];

switch ($action) {
    case 'mail_send':

        $subject        = $_REQUEST['subject'];
        $msg            = $_REQUEST['text'];
        $account_id     = $_REQUEST['account_id'];
        $file           = $_REQUEST['file'];
        $sign_id        = $_REQUEST['sign'];
        $to             = $_REQUEST['to'];

        $db->setQuery(" SELECT      `name`, `mail`, `id`, `domain`, `password`
                        FROM        `mail_account`
                        WHERE       `id` = '$account_id'");
        $res            = $db->getResultArray();
        
        $from           = $res['result'][0]['mail'];
        $domain         = $res['result'][0]['domain'];
        $password       = $res['result'][0]['password'];
   
    
        $db->setQuery(" SELECT `text`
                        FROM   `mail_signature`
                        WHERE  `id` = '$sign_id' AND `actived` = 1 
                        LIMIT   1");
    
        $signature      = $db->getResultArray();
        $signature      = html_entity_decode($signature['result'][0]['text']);

        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->SMTPDebug = 3;
            $mail->SMTPAuth = false; 
            $mail->Username   = $from;
            $mail->Password   = $password;  
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
            $mail->Debugoutput = 'html';
            $mail->Host = $domain;
            $mail->Port = 25;
    
            $mail->CharSet = 'UTF-8';
            $mail->Subject  = $subject;

            // მიმღები მისამართები
            foreach($to AS $to){
                if(filter_var($to, FILTER_VALIDATE_EMAIL)){
                    $mail->addAddress($to);
                }
            }

            $mail->setFrom($from);

            // ხელმოწერის მიბმა
            $msg = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si",'<$1$2>',$msg);
            $mail->msgHTML($msg.'<br><br><br>'.$signature);
            
            // ფიალის მიბმა
            // $mysqli->setQuery(" SELECT patch
            //                     FROM mail_attachments
            //                     WHERE messages_id='$message_id'");
            // $attachs = $mysqli->getResultArray();
    
            // foreach($file AS $item){
            //     $mail->AddAttachment("../../".$item);
            // }
        
            $mail->IsHTML(true); 
            $mail->send();
            $res2 = array('status'=>'ok', 'from'=>$from, 'sender'=>'423', 'esc_message'=>$esc_message);
        }
        catch (phpmailerException $e) {
            $res2 = array('status'=>'3', 'error'=>$e->errorMessage());
        } catch (Exception $e) {
            $res2 = array('status'=>'2', 'error'=>$e->getMessage());
        }

        // if($res2['status']=='ok'){
            
        //     $db->query= ("INSERT    `mail_messages`
        //                     SET
        //                             `datetime`      = NOW(),
        //                             `user_id`       = '$user_id',
        //                             `time`          = NOW(),
        //                             `text`          = '$text'
        //                 ");
        //     $db->execQuery();
           
        //     $data['status'] = 'OK';
        // }
        die(json_encode($data));
    break;
    default:
        $error = '';

}



?>