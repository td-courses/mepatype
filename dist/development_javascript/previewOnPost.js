
// <div class="drag-and-drop_image">
//    <input type="file" onchange="previewFiles()"><br>
//    <div id="preview"></div>
// </div>    

const removeImageAndPreview = () => {
  var preview = document.querySelector('.image_preview');
  var imagePreview = document.querySelector('.image_preview__image');
  var files = document.querySelector('#image_upload').files;

  preview.style.display = "none";
  files = ''
}


function preventDefaults(e) {
  e.preventDefault();
  e.stopPropagation();
}



var youtubeUrlInp = document.querySelector('#youtube_url');



if (youtubeUrlInp) {
  var viewPreview = document.querySelector('.video_preview');
  var iframe = document.querySelector('.video_preview__video iframe');

  var preview = document.querySelector('.image_preview');
  var imagePreview = document.querySelector('.image_preview__image');
  var files = document.querySelector('#image_upload');

  if (youtubeUrlInp.value == "") {
    viewPreview.style.display = 'none';
  } else {
    viewPreview.style.display = 'block';
  }



  youtubeUrlInp.addEventListener('input', () => {


    if (youtubeUrlInp.value == "") {
      viewPreview.style.display = 'none';
    } else {
      viewPreview.style.display = 'block';
    }

    let parsedUrl = youtubeUrlInp.value.split('=')[1].split('&t')[0];
    let hasPlaylist = youtubeUrlInp.value.split('PLe');

    if(hasPlaylist.length >= 2){
      alert("ვიდეოლინკები, ფლეილისტებიდან, არ გამოჩნდება");
      youtubeUrlInp.value = "";
      viewPreview.style.display = 'none';
    }else{
      iframe.src = `https://www.youtube.com/embed/${parsedUrl}`;
    }

  })
}




function previewFiles() {

  var preview = document.querySelector('.image_preview');
  var imagePreview = document.querySelector('.image_preview__image');
  var files = document.querySelector('#image_upload').files;

  function readAndPreview(file) {

    // Make sure `file.name` matches our extensions criteria
    if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
      var reader = new FileReader();

      reader.addEventListener("load", function () {
        preview.style.display = "block"
        imagePreview.style.backgroundImage = `url('${this.result}')`;
      }, false);

      reader.readAsDataURL(file);
    }

  }

  if (files) {
    [].forEach.call(files, readAndPreview);
  }

}

