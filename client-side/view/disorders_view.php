<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
.dialog-grid{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
    padding-right: 10px;
    padding-bottom: 8px;
    padding-left: 10px;
}

.dialog-grid textarea, .dialog-grid input {
    border-radius: 5px;
    border-color:#518bc5!important;
}

.text {
    font-size: 10px;
    color: #03a2ef;
    font-weight: bold;
}

.grid1{
    display: grid;
    grid-auto-flow: column;
}
.grid2{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 5px;
}

.grid3{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 5px;
    width: 100%;
}
</style>
	<script type="text/javascript">
    	var aJaxURL	= "server-side/view/disorders_view.action.php";		//server side folder url
    	var tName	= "example";													//table name
    	var fName	= "add-edit-form";												//form name
    	var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
    	    	
    	$(document).ready(function () {        	
    		LoadTable();	
    		GetButtons("add_button", "delete_button");			
    		SetEvents("add_button", "", "", tName, fName, aJaxURL);
    	});
        
    	function LoadTable(hidden){
    		//KendoUI CLASS CONFIGS BEGIN
			var aJaxURL	        = "server-side/view/disorders_view.action.php";
            var gridName        = 'example';
            var actions         = '';
            var editType        = "popup"; // Two types "popup" and "inline"
            var itemPerPage     =  40;
            var columnsCount    =  5;
            var columnsSQL      =  [
                                       "id:string",
                                       "date:date",
                                       "name:string",
                                       "level:string",
                                       "appeal:string"
                                   ];
            
            var columnGeoNames  =  [
                                       "ID", 
                                       "დამატების თარიღი",
                                       "დსცპ დარღვევა",
                                       "საჯარო ქულა",
                                       "გასაჩივრების ვადა"
                                   ];

            var showOperatorsByColumns  =   [0,0,0,0,0]; 
            var selectors               =   [0,0,0,0,0]; 

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END
                
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL,'get_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden, 1, '', '', '', true, true, true);
    		
    	}

    	$(document).on("dblclick", "#example tr.k-state-selected", function () {
            var grid = $("#example").data("kendoGrid");
            var dItem = grid.dataItem($(this));
           
            if(dItem.id == ''){
                return false;
            }
            
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_edit_page",
                    id: dItem.id
                },
                success: function(data){
                    $("#add-edit-form").html(data.page);
                    LoadDialog(fName);
                }
            })
            
        });

		$(document).on("click","#delete_button",function(){
            var grid = $("#example").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());
            $.ajax({
				url: aJaxURL,
				data: 'act=disable&id='+dItem.id,
				success: function(data) {
                    $("#example").data("kendoGrid").dataSource.read();
				}
			});
            
        });
        
    	function LoadDialog(fName){
    		var id		= $("#department_id").val();
    		GetDialog(fName, 400, "auto", "");
    	}

    	
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_disorder";
	    	param.id		= $("#disorder_id").val();
	    	param.disorder		= $("#disorder").val();
            param.penalty		= $("#penalty").val();
            param.appeal		= $("#appeal").val();
            param.comment		= $("#comment").val();
	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

	   
    </script>
</head>

<body>
    <div id="tabs" style="width: 80%; display: inline-block; position: absolute;">
    	<div id="button_area">
        	<button id="add_button">დამატება</button>
        	<button id="delete_button">წაშლა</button>
        </div>
		<div id="example"></div>
        <div id="add-edit-form" class="form-dialog" title="განყოფილებები">
        	<!-- aJax -->
    	</div>
	</div>
</body>
</html>


