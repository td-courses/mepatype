// const innerChat = document.querySelector("#innerChat");
const innerChat = document.querySelector(".profile__menu");
const callAppsSidemenu = document.querySelector(".callapps-info");



const callBtn = document.querySelectorAll(".call-icons");
const usersChatContainer = document.querySelector(".callapps-info__user-and-groupes");

// const user = document.querySelectorAll('')

const searchInp = document.querySelector(".callapps-info__search__input");
const chatUsers = document.querySelectorAll(".chat_users");

const tabChat = document.querySelector("#chat-tab-chat");
const tabGroup = document.querySelector("#chat-tab-groups");
const tabAddGroup = document.querySelector("#chat-tab-add-groups");



const formContent = document.querySelector(".callapps-info__main");




const addGroupButton = document.querySelector(".add_group_button");

const messageSendBtn = document.querySelector(".sending-btn");


// const resieveMessages




const sendMessage = () => {
    const message = document.querySelector(".sending-input").value;
    const messageInputElement = document.querySelector(".sending-input");
    const messagesContainer = document.querySelector('.messages')

    let parser = new DOMParser()

    let sentMessageElement = parser.parseFromString(`
        <div class="sent_message">
            <p>${message}</p>
        </div>`, "text/html")


    let activeChatUser = JSON.parse(localStorage.getItem('active_chat_user'));
    let chatId = activeChatUser.chatId;
    let userId = activeChatUser.userId;


    let actChatUserIdsAndMessage = {
        act: "send_message",
        chat_id: chatId,
        user_id: userId,
        text: message
    }

    var AjaxURL = "includes/local_chat.php";

    if (isEmpty(message)) {
        return;
    } else {
        $.ajax({
            url: AjaxURL,
            data: actChatUserIdsAndMessage,
            success: function (chatid) {
                let Id = chatid.chat_id;
                let selectedChatUser = document.querySelector(`.chat_users[data-userid = "${userId}"]`)
                selectedChatUser.dataset.chatid = Id;



                messagesContainer.append(sentMessageElement.body.firstChild);
                messageInputElement.value = ""
                messagesContainer.scrollTop = messagesContainer.scrollHeight;
            }
        })
    }





}


function isEmpty(str) {
    return !str.trim().length;
}



messageSendBtn.addEventListener('click', () => {
    sendMessage();
})


$('.sending-input').on("keyup", function (e) {
    if (e.keyCode == 13) {
        sendMessage()
    }
});




let userInChat;

innerChat.addEventListener('click', _ => {
    const btnApiLink = innerChat.getAttribute("data-call-api-link");
    const addButtonScrolled = document.querySelector('.add_button');
    userInChat = document.querySelectorAll('.chat_users')

    if (addButtonScrolled) {
        addButtonScrolled.classList.toggle('z-index-zero');
    }
    
    callAppsSidemenu.classList.toggle("callapps-sidemenu-collapsed")
    

    let isCollapsed = callAppsSidemenu.classList.contains("callapps-sidemenu-collapsed")
    var parser = new DOMParser();

    if (isCollapsed) {
        let parser = new DOMParser();
        var AjaxURL = "includes/local_chat.php";

        $.ajax({
            url: AjaxURL,
            data: "act=get_users",
            dataType: "json",
            success: function (users) {
                usersChatContainer.innerHTML = "";
                let User = users.result

                for (let i = 0; i < User.length; i++) {
                    let template = parser.parseFromString(
                        `<li onclick="goToChat(this)" class="chat_users ${User[i].reed_status}" data-chatid = ${User[i].chat_id} data-userid = ${User[i].user_id} data-amountofmessages = "1"> 
                            <div class="chat_users__profile-image" style="background-image: url(media/uploads/file/${User[i].image})"></div>
                            <div class="chat_users__user-name"><p>${User[i].name}</p></div>
                            <div class="chat_users__last-message"><p>${User[i].last_message}</p></div>
                        </li>`, "text/html")

                    usersChatContainer.append(template.body.firstChild);
                    // usersChatContainer.scrollTop = usersChatContainer.scrollHeight; 
                    const messagesContainer = document.querySelector('.messages')
                    messagesContainer.scrollTop = messagesContainer.scrollHeight;


                }

            }
        })
    } else {
        usersChatContainer.innerHTML = ""
    }
})





setInterval(() => {
    let parser = new DOMParser();
    var AjaxURL = "includes/local_chat.php";

    $.ajax({
        url: AjaxURL,
        data: "act=get_users",
        dataType: "json",
        success: function (users) {
            usersChatContainer.innerHTML = "";
            let User = users.result

            for (let i = 0; i < User.length; i++) {
                let template = parser.parseFromString(
                    `<li onclick="goToChat(this)" class="chat_users ${User[i].reed_status}" data-chatid = ${User[i].chat_id} data-userid = ${User[i].user_id} data-amountofmessages = "1"> 
                        <div class="chat_users__profile-image" style="background-image: url(media/uploads/file/${User[i].image})"></div>
                        <div class="chat_users__user-name"><p>${User[i].name}</p></div>
                        <div class="chat_users__last-message"><p>${User[i].last_message}</p></div>
                    </li>`, "text/html")

                usersChatContainer.append(template.body.firstChild);
                // usersChatContainer.scrollTop = usersChatContainer.scrollHeight; 
                // const messagesContainer = document.querySelector('.messages')
                // messagesContainer.scrollTop = messagesContainer.scrollHeight;
            }

        }
    })
}, 1000);







const goToChat = (userElement) => {
    const chatId = userElement.dataset.chatid;
    const userId = userElement.dataset.userid;
    console.log(userId);

    const coutn = userElement.dataset.amountofmessages;

    let activeChatUser = {
        "chatId": chatId,
        "userId": userId
    }

    localStorage.setItem("active_chat_user", JSON.stringify(activeChatUser))


    formContent.classList.add('slide-chat')
    const messagesContainer = document.querySelector('.messages')
    messagesContainer.scrollTop = messagesContainer.scrollHeight;

    userElement.classList.add('selected_chat_user')



    let actUseIdMessage = {
        act: "get_messages",
        chat_id: chatId,
        count: coutn
    }




    let parser = new DOMParser();
    var AjaxURL = "includes/local_chat.php";

    $.ajax({
        url: AjaxURL,
        data: actUseIdMessage,
        success: function (messages) {
            usersChatContainer.innerHTML = "";
            let searchInp = document.querySelector('.callapps-info__search input')
            // messagesContainer.innerHTML += "<div class='get_more_messages' height='2rem'><p></p></div>";

            let Message = messages.result;
            let name = messages.username

            searchInp.value = name;
            searchInp.disabled = true;

            // console.log(messages);
            


            Message.forEach(message => {
                let messageElement = parser.parseFromString(`
                        <div class="${message.type} chat-message" data-messagedate = "${message.date}" data-messagedate = "${message.time}">
                            <p>${message.text}</p>
                            <span class="message_sent_time">${message.datetime}</span>
                        </div>`, "text/html")


                messagesContainer.append(messageElement.body.firstChild)
                messagesContainer.scrollTop = messagesContainer.scrollHeight;

            })
        }
    });



    // let callappsInfo = document.querySelector('.callapps-info')

    // var vs = messagesContainer.scrollHeight > messagesContainer.clientHeight;
    // console.log(vs);

    // function messageLazyLoad(entry, observer) {

        // console.log(entry);
    //     if (entry[0].isIntersecting) { 

    //     }
    // }
    // // let messageObserver = new IntersectionObserver(messageLazyLoad);
 

    // let options = {
    //     root: messagesContainer,
    //     rootMargin: '0px'
    // }

    // let messageObserver = new IntersectionObserver(messageLazyLoad, options);

    // messageObserver.observe(document.querySelector(".get_more_messages"));




    setInterval(() => {
        var AjaxURL = "includes/local_chat.php";


        let selectedChatUser = document.querySelector('.selected_chat_user')

        let chatId = userElement.dataset.chatid;
        let userId = userElement.dataset.userid;

        let actAndData = {
            act: "check_messages",
            user_id: userId,
            chat_id: chatId
        }

        $.ajax({
            url: AjaxURL,
            data: actAndData,
            success: function (message) {
                let Message = message.result
                let lenght = message.length


                if (lenght == 0) {
                    let nothing = 0;
                } else {

                    Message.forEach(message => {
                        const messagesContainer = document.querySelector('.messages')
                        let parser = new DOMParser()

                        let resevedMessageElement = parser.parseFromString(`
                            <div class="recieved_message">
                                <p>${message.text}</p>
                            </div>`, "text/html")


                        messagesContainer.append(resevedMessageElement.body.firstChild);
                        messagesContainer.scrollTop = messagesContainer.scrollHeight;

                    })

                    // for (let i = 0; i < Message.length; i++) {
                    //     // const element = Message[i];

                    // }

                }


            }
        })
    }, 1000);

}








const goBackTochatsBtn = document.querySelector('.goBackTochatsBtn')

goBackTochatsBtn.addEventListener('click', _ => {
    let searchInp = document.querySelector('.callapps-info__search input')
    searchInp.value = '';
    searchInp.disabled = false;
    formContent.classList.remove('slide-chat')
    localStorage.removeItem('active_chat_user')
    // clearInterval(resieveMessages);

    // let selectedChatUser = document.querySelector('.selected_chat_user')
    // selectedChatUser.classList.remove("selected_chat_user")
})







searchInp.addEventListener('input', () => {
    const chatUsers = document.querySelectorAll(".chat_users");
    // const chatUsers = chatUser.parentNode;


    for (let i = 0; i < chatUsers.length; i++) {
        let name = chatUsers[i].querySelector(".chat_users__user-name > p");

        let txtValue = name.textContent || name.innerText;
        if (txtValue.toUpperCase().indexOf(searchInp.value.toUpperCase()) > -1) {
            chatUsers[i].style.display = "";
        } else {
            chatUsers[i].style.display = "none";
        }
    }
})




