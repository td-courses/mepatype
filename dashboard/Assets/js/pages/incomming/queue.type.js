import Tdg from "../../tdg.class.js";
import Input from "../../components/input/input.class.js";
import ChatPanel from "./chatPanel.type.js";
import WorkGround from "./workGround.type.js";


export default class Queue extends Tdg{

    constructor(prop){
        super();

        // SET Queue TO SELF
        self.Queue = this;

        this.state = {
            dom:  self.IncommingModal.state.dom,
            interface: this.CreateElement({
                element: "interface",
                attributes: ["Queue"]
            }),
            element: [],
            sources: [],
            queues: [],
            prop: prop

        }

    }

    /**
     * INITIALIZE Queue
     */
    init = async () => {


        await this.build()
        
        return this.state.interface

    }
    
    /**
     * BUILD Queue COMPONENTS
     * @returns {element} Queue COMPONENTS
     */
    build = async () => {

        // BUILD SOURCES
        this.append(this.state.interface, await this.buildSourceBlock())
        
        // BUILD QUEUES
        this.append(this.state.interface, await this.buildList())

        // BUILD TAKEN SECTION
        this.append(this.state.element.list, await this.createTakenSection())

        // BUILD WAITER SECTION
        this.append(this.state.element.list, await this.createWaiterSection())

        await this.buildSource();

    }



    /**
     * @returns {Element} SOURCE BLOCK ---------------------------------------------------------------
     */
    buildSourceBlock = async () => {
        this.state.element.source = this.CreateElement({
            element: "block",
            name: "source"
        })

        return this.state.element.source
    }
    

    buildSource = async () => {

        this.state.data = await this.getResponse({
            route: "source",
            act: "get",
            ns: "Helpers"})

        this.state.data.length > 0 && this.state.data.forEach(source => this.createSourceItem(source))

    }

    createSourceItem = async (source) => {

        let sourceEl = this.CreateElement({
            element: "icon",
            attributes: ["svg"],
            name: source.key,
            alt: source.name,
            "source-id": source.id,
            children: `<svg viewBox="0 0 24 24" width="24" height="24">
                            <use xlink:href="Assets/images/icons/icons.svg#${source.key}" />
                        </svg>`,
            onclick:  this.handleSourceClick
            
        })

        this.state.sources.push(sourceEl);
        this.append(this.state.element.source, sourceEl)

        if(source.id == 1){
            this.state.sources[0].click()
        }

    }

    handleSourceClick = function() {

        self.Queue.state.element.source.childNodes.forEach(x => x.removeAttribute("active"))
        this.setAttribute("active", true);
      
        let key = this.getAttribute("name");

        self.Helper.setSourceColor(key)

        self.Queue.state.queues.length > 0 && self.Queue.state.queues.forEach(x => x.remove())

        self.Queue.runSocket();
        

    }


    /**
     * @returns {Element} LIST BLOCK  ---------------------------------------------------------------
     */
    buildList = async () => {
        this.state.element.list = this.CreateElement({
            element: "block",
            name: "list"
        })


        this.append(this.state.element.list, this.createSearchSection());

        return this.state.element.list
    }

    // SEARCH SECITON
    createSearchSection = () => {

        this.state.element.search = this.CreateElement({
            element: "div",
            name: "search"
        })

        this.append(this.state.element.search, this.buildQueueSearch())

        return this.state.element.search
        
    }

    buildQueueSearch = () => {

        this.state.element.QueueSearch = new Input({
            type: "text",
            placeholder: "ძიება",
            oninput: function(){
                console.log(this.value)
            }
        }).build()

        return this.state.element.QueueSearch;

    }

    // TAKEN SECTION
    createTakenSection = () => {

        this.state.element.taken = this.CreateElement({
            element: "div",
            name: "taken",
            children: this.CreateElement({
                element: "span",
                type: "title",
                text: "აქტიური"
            })
        })

        return this.state.element.taken

    }
    

    // WAITER SECTION
    createWaiterSection = () => {

        this.state.element.waiter = this.CreateElement({
            element: "div",
            name: "waiter",
            children: this.CreateElement({
                element: "span",
                type: "title",
                text: "რიგი"
            })
        })

        return this.state.element.waiter

    }

    buildQueueItem = (conv) => {

       let QueueItem = this.CreateElement({
                element: 'conv',
                aria: {
                    taken: conv.taken,
                    active: conv.active,
                    new: conv.newMessage
                },
                dataset: {
                    img: conv.imgUrl,
                    name: conv.name,
                    lastmessage: conv.lastMessage,
                    id: conv.id,
                    source: conv.sourceKey
                },
                onclick: this.getConverstation.bind(this, conv)

            }, 
            this.CreateElement({
                element: "img",
                name: "img",
                src: conv.imgUrl,
                alt: "no image",
                draggable: false
            }), 

            this.CreateElement({
                element: "div",
                name: "group-1"
            }, this.CreateElement({
                element:"span",
                name: "name",
                text: conv.name,
                title: conv.name
            }),this.CreateElement({
                element: "span",
                name: "lastmessage",
                text: conv.lastMessage,
                title: conv.lastMessage
            })), 
            
            this.CreateElement({
                element: "div",
                name: "group-2"
            }, this.CreateElement({
                element: "span",
                name: "datetime",
                text: conv.lastDateTime
            }),
            this.CreateElement({
                element: "span",
                name: "newmessage",
                aria: {
                    status: conv.newMessage
                } 
            }))
        
        )

        this.state.queues.push(QueueItem);

        if(conv.type == "taken"){
            this.append(this.state.element.taken, QueueItem)
        }else if(conv.type == "waiter"){
            this.append(this.state.element.waiter, QueueItem)
        }else{
            console.warn("Type is not Vaild", conv)
        }
    }


    getConverstation = async (data, e) => {

        self.Queue.state.queues.length > 0 && self.Queue.state.queues.forEach(x => x.removeAttribute("actived"))

        e.currentTarget.setAttribute("actived", true)

        self.chatPanel && self.chatPanel.state.interface.remove()
        self.WorkGround && self.WorkGround.state.interface.remove()
        
        this.append(this.state.dom, await new ChatPanel({ id: data.id, source: data.sourceKey}).init())
        this.append(this.state.dom, await new WorkGround(this.state.prop).init())

    }


    runSocket = async (key) => {


        let data = [
            {
                type: "taken",
                name: "DATO KUCIA",
                lastMessage: "გამარჯობა",
                lastDateTime: '10:30',
                imgUrl: 'Assets/images/no-image.png',
                active: true,
                newMessage: false,
                sourceKey: "chat",
                id: 1,
                incommingId: 1
            },
            {
                type: "waiter",
                name: "DATO KUCIA",
                lastMessage: "გამარჯობა",
                lastDateTime: '10:30',
                imgUrl: 'Assets/images/no-image.png',
                active: true,
                newMessage: false,
                sourceKey: "chat",
                id: 2,
                incommingId: 1
            },
            {
                type: "waiter",
                name: "DATO KUCIA",
                lastMessage: "გამარჯობა",
                lastDateTime: '10:30',
                imgUrl: 'Assets/images/no-image.png',
                active: true,
                newMessage: false,
                sourceKey: "chat",
                id: 3,
                incommingId: 1
            },
            {
                type: "taken",
                name: "DATO KUCIA",
                lastMessage: "გამარჯობა",
                lastDateTime: '10:30',
                imgUrl: 'Assets/images/no-image.png',
                active: true,
                newMessage: false,
                sourceKey: "chat",
                id: 4,
                incommingId: 1
            },
            {
                type: "waiter",
                name: "DATO KUCIA",
                lastMessage: "გამარჯობა",
                lastDateTime: '10:30',
                imgUrl: 'Assets/images/no-image.png',
                active: true,
                newMessage: true,
                sourceKey: "chat",
                id: 5,
                incommingId: 1
            },
            {
                type: "waiter",
                name: "DATO KUCIA",
                lastMessage: "გამარჯობა",
                lastDateTime: '10:30',
                imgUrl: 'Assets/images/no-image.png',
                active: true,
                newMessage: true,
                sourceKey: "chat",
                id: 6,
                incommingId: 1
            }
        ]

        data.length > 0 && data.forEach(item => this.buildQueueItem(item))

    }



}