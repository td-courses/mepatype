<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$user_id		         = $_SESSION['USERID'];
include('../../includes/classes/wsdl.class.php');

$action = $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action){
    case 'get_users':
        $db->setQuery(" SELECT users.id,
                        user_info.name as username,
			            GROUP_CONCAT(department.`name`) as department,
			            user_info.image as image_name
                        FROM  users 
                        JOIN  user_info ON user_info.user_id=users.id
                        LEFT JOIN department ON department.id=user_info.dep_id
                        WHERE  users.actived=1 AND users.id!='$user_id'
                        GROUP BY users.id
                       ");
        $result=$db->getResultArray();
        $data=$result['result'];
        break;

    case 'get_groups':
        $db->setQuery("SELECT activities_group.id, 
                        activities_group.name,
                        GROUP_CONCAT(user_info.`name`) as members 
                        FROM activities_group
                        LEFT JOIN activities_group_detail ON activities_group.id=activities_group_detail.activites_group_id
                        JOIN user_info ON user_info.user_id=activities_group_detail.user_id
                        WHERE activities_group.active=1
                        GROUP BY activities_group.id");
        $result=$db->getResultArray();
        $count=0;
        foreach ($result['result'] as $row){
            $data['groups'][$count]['id']=$row['id'];
            $data['groups'][$count]['name']=$row['name'];
            $data['groups'][$count]['members']=explode(",",$row['members']);
            $count++;
        }
        break;

    case 'get_departments':
        $db->setQuery(" SELECT department.id, 
                        department.name,
                        GROUP_CONCAT(user_info.`name`) as members 
                        FROM department
                        LEFT JOIN user_info ON user_info.user_id=department.user_id
                        WHERE department.actived=1
                        GROUP BY department.`name`");
        $result=$db->getResultArray();
        $count=0;
        foreach ($result['result'] as $row){
            $data['departments'][$count]['id']=$row['id'];
            $data['departments'][$count]['name']=$row['name'];
            $data['departments'][$count]['members']=explode(",",$row['members']);
            $count++;
        }

        break;
    case 'create_group':
        $group=$_REQUEST['group'];
        $name=$group['name'];
        $members=$group['members'];
        $db->setQuery("INSERT INTO activities_group (name) VALUES ('$name')");
        $db->execQuery();
        $db->setQuery("SELECT MAX(id) as id FROM activities_group");
        $result=$db->getResultArray();
        $last_id=$result['result'][0]['id'];

        $quer="INSERT INTO activities_group_detail (user_id, activites_group_id) VALUES  ";
        foreach ($members as $row){
            $quer.=" ('$row','$last_id'),";
        }

        $quer=rtrim($quer,',');
        $quer.=";";

        $db->setQuery($quer);
        $db->execQuery();

        $db->setQuery("SELECT activities_group.id, 
                        activities_group.name,
                        GROUP_CONCAT(user_info.`name`) as members 
                        FROM activities_group
                        LEFT JOIN activities_group_detail ON activities_group.id=activities_group_detail.activites_group_id
                        JOIN user_info ON user_info.user_id=activities_group_detail.user_id
                        WHERE activities_group.active=1 and activities_group.id='$last_id'
                        GROUP BY activities_group.id");
        $result=$db->getResultArray();
        $count=0;
        foreach ($result['result'] as $row){
            $data['groups'][$count]['id']=$row['id'];
            $data['groups'][$count]['name']=$row['name'];
            $data['groups'][$count]['members']=explode(",",$row['members']);
            $count++;
        }


        break;


    case 'delete_group':
        $groupId=$_REQUEST['groupId'];
        $db->setQuery("UPDATE  activities_group SET active=0 WHERE id='$groupId'");
        $db->execQuery();
        break;
    case 'add_comment':
        $post_id=$_REQUEST['post_id'];
        $text=$_REQUEST['text'];
        $db->setQuery("INSERT INTO posts_comments (posts_id,user_id, datetime, name, image_name, department_id,data_posted, text)
                       SELECT '$post_id' as posts_id,'$user_id', NOW(), user_info.name,user_info.image,user_info.dep_id,NOW(), '$text' as text
                       FROM user_info 
                       WHERE user_info.user_id='$user_id'
                      ");
        $db->execQuery();

        $db->setQuery("SELECT max(id) as id FROM posts_comments");
        $db->getResultArray();
        $last_ids=$db->getResultArray();
        $last_id=$last_ids['result'][0]['id'];

        $db->setQuery("INSERT INTO posts_comments_likes (posts_comments_id, user_id, likes)
                                   values ('$last_id','$user_id','0')");
        $db->execQuery();


        $db->setQuery("SELECT GROUP_CONCAT(posts_comments.id)                                                                                as comments_id,
       GROUP_CONCAT(posts_comments.user_id)                                                                           as user_id,
       GROUP_CONCAT(posts_comments.name)                                                                              as name,
       GROUP_CONCAT(posts_comments.image_name)                                                                        as image_name,
       IF(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()) > 60,
          IF(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()) > 24,
             GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts_comments.datetime, NOW()), ' დღის წინ'),
             GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()), ' საათის წინ')),
          GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()),
                       ' წუთის წინ'))                                                                                 as time_after_posted,
       GROUP_CONCAT(posts_comments.time_posted)                                                                       as time_posted,
       GROUP_CONCAT(posts_comments.data_posted)                                                                       as data_posted,
       GROUP_CONCAT(posts_comments.text)                                                                      as text,
       (SELECT sum(posts_comments_likes.likes)
        FROM posts_comments_likes
        WHERE posts_comments_id = posts_comments.id)                                                                  as likes,
       GROUP_CONCAT(department1.name)                                                                                 as department
FROM posts_comments
         LEFT JOIN department as department1 ON posts_comments.department_id = department1.id
where posts_comments.id = '$last_id'
");

         $data= $db->getResultArray();
         $data['user_id']=$user_id;

        break;

    case 'add_comment_like':
        $comment_id=$_REQUEST['comment_id'];
        $db->setQuery("SELECT id FROM posts_comments_likes WHERE posts_comments_id='$comment_id' AND user_id='$user_id'");
        IF($db->getNumRow()>0){
            $db->setQuery("UPDATE posts_comments_likes SET likes=IF(likes='0','1','0') WHERE user_id='$user_id' AND posts_comments_id='$comment_id'");
            $db->execQuery();
        }else{
            $db->setQuery("INSERT INTO posts_comments_likes (posts_comments_id, user_id, likes) VALUES ('$comment_id','$user_id',1)");
            $db->execQuery();
        }
        $db->setQuery("SELECT SUM(posts_comments_likes.likes) as likes
                       FROM posts_comments
                       LEFT JOIN posts_comments_likes ON posts_comments_likes.posts_comments_id=posts_comments.id
                       WHERE posts_comments.id='$comment_id'");
        $data=$db->getResultArray();

        break;

    case 'delete_comment':
        $comment_id=$_REQUEST['comment_id'];
        $db->setQuery("DELETE FROM posts_comments WHERE id='$comment_id'");
        $db->execQuery();
        break;
    case 'edit_post':
        $id=$_REQUEST['id'];
        $posts=$_REQUEST['posts'];
        $type=$_REQUEST['posts']['type'];
        $url=$_REQUEST['posts']['post_youtube_video_url'];
        $image=$_REQUEST['posts']['poster_image_name']; //$_REQUEST['posts']['post_image_name'];
        $description=$_REQUEST['posts']['description'];
        $question=$_REQUEST['posts']['question'];
        $location=$_REQUEST['posts']['location'];
        $poster_name=$_REQUEST['posts']['poster_name'];
        $poster_image_name=$_REQUEST['posts']['poster_image_name'];
        $poster_departments=$_REQUEST['posts']['poster_departments'];
        $time_posted=$_REQUEST['posts']['time_posted'];
        $data_posted=$_REQUEST['posts']['data_posted'];

        $db->setQuery("UPDATE posts 
                      SET user_id='$user_id',
                          type='$type',
                          description='$description',
                          url='$url',
                          image='$image',
                          question='$question', 
                          location='$location',
                          poster_name='$poster_name', 
                          poster_image_name='$poster_image_name', 
                          poster_departments='$poster_departments',
                          time_posted='$time_posted', 
                          data_posted='$data_posted', 
                          datetime=NOW()
                      WHERE id='$id' ");
        $db->execQuery();


        $marked_groups = $_REQUEST['posts']['marked_groups'];
        $db->setQuery("DELETE FROM posts_marked_groups WHERE posts_id='$id'");
        $db->execQuery();
        $quer="INSERT INTO posts_marked_groups (activites_group_id, posts_id) VALUES ";
        $check=0;
        foreach ($marked_groups as $row){
            $check++;
            $quer.=" ('$row','$id'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }

        $marked_users = $_REQUEST['posts']['marked_users'];
        $db->setQuery("DELETE FROM posts_marked_users WHERE posts_id='$id'");
        $db->execQuery();
        $quer="INSERT INTO posts_marked_users (user_id, posts_id) VALUES ";
        $check=0;
        foreach ($marked_users as $row){
            $check++;
            $quer.=" ('$row','$id'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }

        $marked_departments = $_REQUEST['posts']['marked_departments'];
        $db->setQuery("DELETE FROM posts_marked_departments WHERE posts_id='$id'");
        $db->execQuery();
        $quer="INSERT INTO posts_marked_departments (department_id, posts_id) VALUES ";
        $check=0;
        foreach ($marked_departments as $row){
            $check++;
            $quer.=" ('$row','$id'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }
        $posts_startAndEndDate_end=$_REQUEST['posts']['startAndEndDate']['end'];
        $posts_startAndEndDate_start=$_REQUEST['posts']['startAndEndDate']['start'];
        $db->setQuery("UPDATE  posts_startAndEndDate 
                       SET  start='$posts_startAndEndDate_start',
                           `end`='$posts_startAndEndDate_end' 
                       WHERE posts_id='$id'
                          ");
        $db->execQuery();

        $db->setQuery("SELECT id FROM posts_answers WHERE posts_id='$id'");
        $res=$db->getResultArray();
        $posts_answers_id=$res['result'][0]['id'];

        $db->setQuery("DELETE FROM posts_answers WHERE posts_id='$id'");
        $db->execQuery();
        $answers_type=$_REQUEST['posts']['answers']['type'];
        $db->setQuery("INSERT INTO posts_answers (posts_id, type) VALUES('$id','$answers_type')");
        $db->execQuery();

        $db->setQuery("SELECT id as id FROM posts_answers WHERE posts_answers.posts_id='$id'");
        $res=$db->getResultArray();
        $answers_type_id=$res['result'][0]['id'];

        $db->setQuery("DELETE FROM posts_answers_details WHERE posts_answers_id='$answers_type_id'");
        $db->execQuery();

        $answers_array=$_REQUEST['posts']['answers']['answers'];
        $quer="INSERT INTO posts_answers_details (posts_answers_id, text, votes) VALUES  ";
        $check=0;
        foreach ($answers_array as $row){
            $check++;
            $text=$row['text'];
            $votes=$row['votes'];
            $quer.=" ('$answers_type_id','$text','$votes'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }


        break;
    case 'add_posts':
        $posts=$_REQUEST['posts'];
        $url = $_REQUEST['posts']['post_youtube_video_url'];
        $type=$_REQUEST['posts']['type'];
        $image=$_REQUEST['posts']['post_image_name'];
        $description=$_REQUEST['posts']['description'];
        $question=$_REQUEST['posts']['question'];
        $location=$_REQUEST['posts']['location'];
        $poster_name=$_REQUEST['posts']['poster_name'];
        $poster_image_name=$_REQUEST['posts']['poster_image_name'];
        $poster_departments=$_REQUEST['posts']['poster_departments'];
        $time_posted=$_REQUEST['posts']['time_posted'];
        $data_posted=$_REQUEST['posts']['data_posted'];
        $db->setQuery("INSERT INTO posts (user_id, type, url, image, description, question, location, poster_name, poster_image_name, poster_departments, time_posted, data_posted, datetime) 
                                  VALUES ('$user_id','$type','$url','$image','$description','$question','$location','$poster_name','$poster_image_name','$poster_departments','$time_posted','$data_posted',NOW())");
        $db->execQuery();

        $db->setQuery("SELECT max(id) as id FROM posts");
        $res=$db->getResultArray();
        $last_posts_id=$res['result'][0]['id'];

        $answers_type=$_REQUEST['posts']['answers']['type'];
        $db->setQuery("INSERT INTO posts_answers (posts_id, type) VALUES('$last_posts_id','$answers_type')");
        $db->execQuery();

        $db->setQuery("SELECT max(id) as id FROM posts_answers");
        $res=$db->getResultArray();
        $answers_type_id=$res['result'][0]['id'];

        $answers_array=$_REQUEST['posts']['answers']['answers'];
        $quer="INSERT INTO posts_answers_details (posts_answers_id, text, votes) VALUES  ";
        $check=0;
        foreach ($answers_array as $row){
            $check++;
            $text=$row['text'];
            $votes=$row['votes'];
        $quer.=" ('$answers_type_id','$text','$votes'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }


        $marked_groups = $_REQUEST['posts']['marked_groups'];
        $quer="INSERT INTO posts_marked_groups (activites_group_id, posts_id) VALUES ";
        $check1=0;
        foreach ($marked_groups as $row){
            $check1++;
            $quer.=" ('$row','$last_posts_id'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check1>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }

        $marked_users = $_REQUEST['posts']['marked_users'];
        $quer="INSERT INTO posts_marked_users (user_id, posts_id) VALUES ";
        $check2=0;
        foreach ($marked_users as $row){
            $check2++;
            $quer.=" ('$row','$last_posts_id'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check2>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }

        $marked_departments = $_REQUEST['posts']['marked_departments'];
        $quer="INSERT INTO posts_marked_departments (department_id, posts_id) VALUES ";
        $check3=0;
        foreach ($marked_departments as $row){
            $check3++;
            $quer.=" ('$row','$last_posts_id'),";
        }
        $quer=rtrim($quer,',');
        $quer.=";";
        IF($check3>0){
            $db->setQuery("$quer");
            $db->execQuery();
        }

        if($check1 == 0 and $check2 == 0 and $check3 == 0)
        {
            $sql = "SELECT `id` FROM `users` WHERE `actived`=1" AND `id` != $user_id;
            $db->setQuery($sql);
            $users = $db->getResultArray();
            $querNEW="INSERT INTO posts_marked_users (user_id, posts_id) VALUES ";
            foreach($users['result'] AS $row)
            {
                $querNEW.=" ('$row[id]','$last_posts_id'),";
            }
            $querNEW=rtrim($querNEW,',');
            $querNEW.=";";
            $db->setQuery($querNEW);
            $db->execQuery();
        }

        $posts_startAndEndDate_end=$_REQUEST['posts']['startAndEndDate']['end'];
        $posts_startAndEndDate_start=$_REQUEST['posts']['startAndEndDate']['start'];
        $db->setQuery("INSERT INTO posts_startAndEndDate (posts_id, start, `end`) VALUES ($last_posts_id, '$posts_startAndEndDate_start','$posts_startAndEndDate_end')");
        $db->execQuery();

        $likes_sad=$_REQUEST['posts']['likes']['sad'];
        $likes_happy=$_REQUEST['posts']['likes']['happy'];
        $likes_neutral=$_REQUEST['posts']['likes']['neutral'];
        $likes_love=$_REQUEST['posts']['likes']['love'];
        $likes_amazed=$_REQUEST['posts']['likes']['amazed'];
        $db->setQuery("INSERT INTO posts_likes (posts_id, sad, happy, neutral, love, amazed) 
                                   VALUES ('$last_posts_id','$likes_sad','$likes_happy','$likes_neutral','$likes_love','$likes_amazed')");
        $db->execQuery();


        $data['post_id']=$last_posts_id;
        $data['user_id']=$user_id;



        break;

    case 'get_edit_post':
        $id=$_REQUEST['id'];
        $db->setQuery("
 SELECT posts.id,
       posts.user_id,
       posts.type,
       posts.description,
       posts.question,
       posts.location,
       posts.url,
       posts.image,
       user.poster_name,
       user.poster_image_name,
       user.poster_departments,
       IF(TIMESTAMPDIFF(MINUTE, posts.datetime, NOW()) > 60, IF(TIMESTAMPDIFF(HOUR, posts.datetime, NOW()) > 24,
                                                                GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts.datetime, NOW()),
                                                                             ' დღის წინ'),
                                                                GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts.datetime, NOW()),
                                                                             ' საათის წინ')),
          GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts.datetime, NOW()), ' წუთის წინ')) as time_after_posted1,
       posts.time_posted                                                            as time_posted1,
       posts.data_posted                                                            as data_posted1,
       posts_answers.type                                                           as type1,
       posts_answers_details.text                                                   as text,
       posts_answers_details.votes                                                  as votes,
       posts_answers_details.sum_votes                                              as sum_votes,
       posts_answers_details.posts_answers_details_id                               as posts_answers_details_id,
       department.name                                                              as marked_departments,
       department.department_id                                                     as marked_departments_id,
       activities_group.name                                                        as marked_groups,
       activities_group.activities_group_id                                         as marked_groups_id,
       user_info.name                                                               as marked_users,
       user_info.user_info_id                                                       as marked_users_id,
       posts_likes.sad                                                              as sad,
       posts_likes.happy                                                            as happy,
       posts_likes.neutral                                                          as neutral,
       posts_likes.love                                                             as love,
       posts_likes.amazed                                                           as amazed,
       posts_comments.name                                                          as name,
       posts_comments.image_name                                                    as image_name,
       posts_comments.time_after_posted                                             as time_after_posted,
       posts_comments.time_posted                                                   as time_posted,
       posts_comments.data_posted                                                   as data_posted,
       posts_comments.text                                                          as text1,
       posts_comments.likes                                                         as likes,
       posts_comments.department                                                    as department,
       posts_comments.comments_id                                                   as comments_id,
       posts_comments.comments_user_id                                              as comments_user_id,
       posts_startAndEndDate.end                                                    as `end`,
       posts_startAndEndDate.start                                                  as `start`


FROM posts
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_answers_details.id)    as posts_answers_details_id,
                           GROUP_CONCAT(posts_answers_details.text)  as text,
                           GROUP_CONCAT(posts_answers_details.votes) as votes,
                           SUM(posts_answers_details.votes)          as sum_votes
                    FROM posts
                             LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                             JOIN posts_answers_details ON posts_answers.id = posts_answers_details.posts_answers_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_answers_details ON posts_answers_details.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_answers.type) as type
                    FROM posts
                             LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_answers ON posts_answers.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(department.id) as department_id,
                           GROUP_CONCAT(department.name) as name
                    FROM posts
                             LEFT JOIN posts_marked_departments ON posts_marked_departments.posts_id = posts.id
                             JOIN department ON department.id = posts_marked_departments.department_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as department ON department.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(activities_group.id) as activities_group_id,
                           GROUP_CONCAT(activities_group.name) as name
                    FROM posts
                             LEFT JOIN posts_marked_groups ON posts_marked_groups.posts_id = posts.id
                             JOIN activities_group ON activities_group.id = posts_marked_groups.activites_group_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as activities_group ON activities_group.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(user_info.id) as user_info_id,
                           GROUP_CONCAT(user_info.name) as name

                    FROM posts
                             LEFT JOIN posts_marked_users ON posts_marked_users.posts_id = posts.id
                             JOIN user_info ON user_info.user_id = posts_marked_users.user_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as user_info ON user_info.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_likes.sad)     as sad,
                           GROUP_CONCAT(posts_likes.happy)   as happy,
                           GROUP_CONCAT(posts_likes.neutral) as neutral,
                           GROUP_CONCAT(posts_likes.love)    as love,
                           GROUP_CONCAT(posts_likes.amazed)  as amazed
                    FROM posts
                             LEFT JOIN posts_likes ON posts_likes.posts_id = posts.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_likes ON posts_likes.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_comments.user_id)                                                  as comments_user_id,
                           GROUP_CONCAT(posts_comments.id)                                                       as comments_id,
                           GROUP_CONCAT(posts_comments.name)                                                     as name,
                           GROUP_CONCAT(posts_comments.image_name)                                               as image_name,
                           IF(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()) > 60,
                              IF(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()) > 24,
                                 GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts_comments.datetime, NOW()), ' დღის წინ'),
                                 GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()), ' საათის წინ')),
                              GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()),
                                           ' წუთის წინ'))                                                        as time_after_posted,
                           GROUP_CONCAT(posts_comments.time_posted)                                              as time_posted,
                           GROUP_CONCAT(posts_comments.data_posted)                                              as data_posted,
                           GROUP_CONCAT(posts_comments.text, ' ,. ')                                             as text,
                           GROUP_CONCAT((SELECT sum(posts_comments_likes.likes)
                                          FROM posts_comments_likes
                                          WHERE posts_comments_id = posts_comments.id))                          as likes,
                           GROUP_CONCAT(department1.name)                                                        as department
                    FROM posts
                             LEFT JOIN posts_comments ON posts_comments.posts_id = posts.id
                             JOIN department as department1 ON posts_comments.department_id = department1.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_comments ON posts_comments.id = posts.id
         LEFT JOIN (SELECT posts.id, posts_startAndEndDate.start, posts_startAndEndDate.end
                    FROM posts
                             LEFT JOIN posts_startAndEndDate ON posts_startAndEndDate.posts_id = posts.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_startAndEndDate ON posts_startAndEndDate.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           user.name                      as poster_name,
                           user.image                     as poster_image_name,
                           GROUP_CONCAT(department1.name) as poster_departments
                    FROM posts
                             LEFT JOIN user_info as user ON user.user_id = posts.user_id
                             LEFT JOIN department as department1 ON department1.user_id = user.user_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as user ON user.id = posts.id

WHERE posts.actived = 1 AND posts.id='$id'
GROUP BY posts.id
order by posts.id desc
                       ");

        $result=$db->getResultArray();
        $count=0;
        foreach ($result['result'] as $row){
            $post_check_id=$row['id'];
            $db->setQuery("SELECT IF ($user_id IN (posts_likes_details.user_id),'true' ,'false' ) as like_status 
                           FROM posts_likes
                           LEFT JOIN posts_likes_details ON posts_likes.id=posts_likes_details.posts_likes_id
                           WHERE posts_likes.posts_id='$post_check_id'");
            $like_res=$db->getResultArray();
            $data['posts'][$count]['like_status']=$like_res['result'][0]['like_status'];
            $data['posts'][$count]['id']=$row['id'];
            $data['posts'][$count]['user_id']=$row['user_id'];
            $data['posts'][$count]['type']=$row['type'];
            $data['posts'][$count]['description']=$row['description'];
            $data['posts'][$count]['post_youtube_video_url']=$row['url'];
            $data['posts'][$count]['post_image_name']=$row['image'];
            $data['posts'][$count]['question']=$row['question'];
            $data['posts'][$count]['location']=$row['location'];
            $data['posts'][$count]['poster_name'] = $row['poster_name'];
            $data['posts'][$count]['poster_image_name'] = $row['poster_image_name'];
            $data['posts'][$count]['poster_departments'] = $row['poster_departments'];
            $data['posts'][$count]['time_after_posted'] = $row['time_after_posted1'];
            $data['posts'][$count]['time_posted'] = $row['time_posted1'];
            $data['posts'][$count]['data_posted'] = $row['data_posted1'];
            $data['posts'][$count]['startAndEndDate']['start'] = $row['start'];
            $data['posts'][$count]['startAndEndDate']['end'] = $row['end'];
            $data['posts'][$count]['answers']['type'] = $row['type1'];
            $answer_text = explode(",",$row['text']);
            $answer_votes = explode(",",$row['votes']);
            $posts_answers_details_ids=explode(",",$row['posts_answers_details_id']);
            $c=0;
            foreach ($answer_text as $res){
                $data['posts'][$count]['answers']['answers'][$c]['text']=$res;
                $data['posts'][$count]['answers']['answers'][$c]['votes']=$row['sum_votes']>0 ? round($answer_votes[$c]/$row['sum_votes']*100) : 0;
                $posts_answers_details_id=$posts_answers_details_ids[$c];
                $db->setQuery("SELECT id FROM posts_survey_details WHERE posts_answer_details_id='$posts_answers_details_id' AND user_id='$user_id'");
                IF($db->getNumRow()>0){
                    $data['posts'][$count]['answers']['answers'][$c]['status']='check';
                }else{
                    $data['posts'][$count]['answers']['answers'][$c]['status']='uncheck';
                }

                $c++;
            }


            $marked_groups=explode(",",$row['marked_groups']);
            $marked_groups_id=explode(",",$row['marked_groups_id']);
            $marked_users=explode(",",$row['marked_users']);
            $marked_users_id=explode(",",$row['marked_users_id']);
            $marked_departments=explode(",",$row['marked_departments']);
            $marked_departments_id=explode(",",$row['marked_departments_id']);
            $c=0;
            foreach ($marked_groups_id as $row){
                $data['posts'][$count]['marked_groups'][$c]['id']=$row;
                $data['posts'][$count]['marked_groups'][$c]['name']=$marked_groups[$c];
                $c++;
            }

            $c=0;
            foreach ($marked_users_id as $row){
                $data['posts'][$count]['marked_users'][$c]['id']=$row;
                $data['posts'][$count]['marked_users'][$c]['name']=$marked_users[$c];
                $c++;
            }

            $c=0;
            foreach ($marked_departments_id as $row){
                $data['posts'][$count]['marked_departments'][$c]['id']=$row;
                $data['posts'][$count]['marked_departments'][$c]['name']=$marked_departments[$c];
                $c++;
            }

            $data['posts'][$count]['likes']['sad']=$row['sad'];
            $data['posts'][$count]['likes']['happy']=$row['happy'];
            $data['posts'][$count]['likes']['neutral']=$row['neutral'];
            $data['posts'][$count]['likes']['love']=$row['love'];
            $data['posts'][$count]['likes']['amazed']=$row['amazed'];

            $answer_name=explode(",",$row['name']);
            $answer_image_name=explode(",",$row['image_name']);
            $answer_department=explode(",",$row['department']);
            $answer_time_after_posted=explode(",",$row['time_after_posted']);
            $answer_time_posted=explode(",",$row['time_posted']);
            $answer_data_posted=explode(",",$row['data_posted']);
            $answer_text=explode(" ,. ,",$row['text1']);
            $answer_likes=explode(",",$row['likes']);
            $comments_id=explode(",",$row['comments_id']);
            $comments_user_id=explode(",",$row['comments_user_id']);
            $c=0;
            $max=max($answer_likes);
            $check_istop=0;
            foreach ($answer_name as $res){
                $comment_id_for_check=$comments_id[$c];
                $db->setQuery("SELECT user_id FROM posts_comments_likes WHERE posts_comments_likes.posts_comments_id='$comment_id_for_check' AND posts_comments_likes.likes>0");
                $like_status_array=$db->getResultArray();
                $like_status=0;
                foreach ($like_status_array['result'] as $like_status){
                    $like_status['user_id']==$user_id ? $like_status++ : '';
                }
                $like_status>0 ? $data['posts'][$count]['comments'][$c]['like_status']=true : $data['posts'][$count]['comments'][$c]['like_status']=false;
                $data['posts'][$count]['comments'][$c]['user_id']=$comments_user_id[$c];
                $data['posts'][$count]['comments'][$c]['comment_id']=$comments_id[$c];
                $data['posts'][$count]['comments'][$c]['name']=$res;
                $data['posts'][$count]['comments'][$c]['image_name']=$answer_image_name[$c];
                $data['posts'][$count]['comments'][$c]['department']=$answer_department[$c];
                $data['posts'][$count]['comments'][$c]['time_after_posted']=$answer_time_after_posted[$c];
                $data['posts'][$count]['comments'][$c]['time_posted']=$answer_time_posted[$c];
                $data['posts'][$count]['comments'][$c]['data_posted']=$answer_data_posted[$c];
                $data['posts'][$count]['comments'][$c]['text']=$answer_text[$c];
                $data['posts'][$count]['comments'][$c]['likes']=$answer_likes[$c];
                IF($answer_likes[$c]==$max && $check_istop==0){
                    $data['posts'][$count]['comments'][$c]['isTop']=true;
                    $check_istop++;
                }else{
                    $data['posts'][$count]['comments'][$c]['isTop']=false;
                }
                $c++;
            }

            $count++;

        }
        $data['user_id']=$user_id;
        break;
    case 'get_posts':
    $count=$_REQUEST['count']*20;
    $last_post_id=$_REQUEST['last_post_id'];
        $db->setQuery("
 SELECT posts.id,
       posts.user_id,
       posts.type,
       posts.description,
       posts.question,
       posts.location,
       posts.url as post_youtube_video_url,
       posts.image as post_image_name,
       user.poster_name,
       user.poster_image_name,
       user.poster_departments,
       IF(TIMESTAMPDIFF(MINUTE, posts.datetime, NOW()) > 60, IF(TIMESTAMPDIFF(HOUR, posts.datetime, NOW()) > 24,
                                                                GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts.datetime, NOW()),
                                                                             ' დღის წინ'),
                                                                GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts.datetime, NOW()),
                                                                             ' საათის წინ')),
          GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts.datetime, NOW()), ' წუთის წინ')) as time_after_posted1,
       posts.time_posted                                                            as time_posted1,
       posts.data_posted                                                            as data_posted1,
       posts_answers.type                                                           as type1,
       posts_answers_details.text                                                   as text,
       posts_answers_details.votes                                                  as votes,
       posts_answers_details.sum_votes                                              as sum_votes,
       posts_answers_details.posts_answers_details_id                               as posts_answers_details_id,
       department.name                                                              as marked_departments,
       activities_group.name                                                        as marked_groups,
       user_info.name                                                               as marked_users,
       posts_likes.sad                                                              as sad,
       posts_likes.happy                                                            as happy,
       posts_likes.neutral                                                          as neutral,
       posts_likes.love                                                             as love,
       posts_likes.amazed                                                           as amazed,
       posts_comments.name                                                          as name,
       posts_comments.image_name                                                    as image_name,
       posts_comments.time_after_posted                                             as time_after_posted,
       posts_comments.time_posted                                                   as time_posted,
       posts_comments.data_posted                                                   as data_posted,
       posts_comments.text                                                          as text1,
       posts_comments.likes                                                         as likes,
       posts_comments.department                                                    as department,
       posts_comments.comments_id                                                   as comments_id,
       posts_comments.comments_user_id                                              as comments_user_id,
       posts_startAndEndDate.end                                                    as `end`,
       posts_startAndEndDate.start                                                  as `start`


FROM posts
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_answers_details.id)    as posts_answers_details_id,
                           GROUP_CONCAT(posts_answers_details.text)  as text,
                           GROUP_CONCAT(posts_answers_details.votes) as votes,
                           SUM(posts_answers_details.votes)          as sum_votes
                    FROM posts
                             LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                             JOIN posts_answers_details ON posts_answers.id = posts_answers_details.posts_answers_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_answers_details ON posts_answers_details.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_answers.type) as type
                    FROM posts
                             LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_answers ON posts_answers.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(department.name) as name,
                           GROUP_CONCAT(department.user_id) as user_id
                    FROM posts
                             LEFT JOIN posts_marked_departments ON posts_marked_departments.posts_id = posts.id
                             JOIN department ON department.id = posts_marked_departments.department_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as department ON department.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(activities_group_detail.user_id) as user_id,
                           GROUP_CONCAT(activities_group.name) as name
                    FROM posts
                             LEFT JOIN posts_marked_groups ON posts_marked_groups.posts_id = posts.id
                             JOIN activities_group ON activities_group.id = posts_marked_groups.activites_group_id
                             LEFT JOIN activities_group_detail ON activities_group_detail.activites_group_id=activities_group.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as activities_group ON activities_group.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(user_info.user_id) as user_id,
                           GROUP_CONCAT(user_info.name) as name

                    FROM posts
                             LEFT JOIN posts_marked_users ON posts_marked_users.posts_id = posts.id
                             JOIN user_info ON user_info.user_id = posts_marked_users.user_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as user_info ON user_info.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_likes.sad)     as sad,
                           GROUP_CONCAT(posts_likes.happy)   as happy,
                           GROUP_CONCAT(posts_likes.neutral) as neutral,
                           GROUP_CONCAT(posts_likes.love)    as love,
                           GROUP_CONCAT(posts_likes.amazed)  as amazed
                    FROM posts
                             LEFT JOIN posts_likes ON posts_likes.posts_id = posts.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_likes ON posts_likes.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           GROUP_CONCAT(posts_comments.user_id)                                                  as comments_user_id,
                           GROUP_CONCAT(posts_comments.id)                                                       as comments_id,
                           GROUP_CONCAT(posts_comments.name)                                                     as name,
                           GROUP_CONCAT(posts_comments.image_name)                                               as image_name,
                           IF(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()) > 60,
                              IF(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()) > 24,
                                 GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts_comments.datetime, NOW()), ' დღის წინ'),
                                 GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()), ' საათის წინ')),
                              GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()),
                                           ' წუთის წინ'))                                                        as time_after_posted,
                           GROUP_CONCAT(posts_comments.time_posted)                                              as time_posted,
                           GROUP_CONCAT(posts_comments.data_posted)                                              as data_posted,
                           GROUP_CONCAT(posts_comments.text, '~')                                             as text,
                           GROUP_CONCAT((SELECT sum(posts_comments_likes.likes)
                                         FROM posts_comments_likes
                                         WHERE posts_comments_id = posts_comments.id))                          as likes,
                           GROUP_CONCAT(department1.name)                                                        as department
                    FROM posts
                             LEFT JOIN posts_comments ON posts_comments.posts_id = posts.id
                             JOIN department as department1 ON posts_comments.department_id = department1.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_comments ON posts_comments.id = posts.id
         LEFT JOIN (SELECT posts.id, posts_startAndEndDate.start, posts_startAndEndDate.end
                    FROM posts
                             LEFT JOIN posts_startAndEndDate ON posts_startAndEndDate.posts_id = posts.id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as posts_startAndEndDate ON posts_startAndEndDate.id = posts.id
         LEFT JOIN (SELECT posts.id,
                           user.name                      as poster_name,
                           user.image                     as poster_image_name,
                           GROUP_CONCAT(department1.name) as poster_departments
                    FROM posts
                             LEFT JOIN user_info as user ON user.user_id = posts.user_id
                             LEFT JOIN department as department1 ON department1.id = user.dep_id
                    WHERE posts.actived = 1
                    GROUP BY posts.id) as user ON user.id = posts.id
                    LEFT JOIN user_info AS ses_user ON ses_user.user_id = '$user_id' 
                    LEFT JOIN `department` AS `user_dep` ON `user_dep`.id =  ses_user.dep_id
WHERE posts.actived = 1 AND posts.id < $last_post_id AND ((( FIND_IN_SET(`user_dep`.`name`, department.`name`)>0 OR  FIND_IN_SET('$user_id', activities_group.user_id)>0 OR  FIND_IN_SET('$user_id',user_info.user_id)>0) OR (department.user_id IS NULL AND activities_group.user_id IS NULL AND user_info.user_id IS NULL)) OR '$user_id'=1 OR posts.user_id='$user_id')
GROUP BY posts.id
order by posts.id desc
LIMIT 20                  ");

        $result=$db->getResultArray();
        $count=0;
        foreach ($result['result'] as $row){
            $post_check_id=$row['id'];
            $db->setQuery("SELECT IF (FIND_IN_SET('$user_id',GROUP_CONCAT(posts_likes_details.user_id))>0,'true' ,'false' ) as like_status 
                           FROM posts_likes
                           LEFT JOIN posts_likes_details ON posts_likes.id=posts_likes_details.posts_likes_id
                           WHERE posts_likes.posts_id='$post_check_id' 
                           GROUP BY posts_likes.id");

            $like_res=$db->getResultArray();
            $data['posts'][$count]['like_status']=$like_res['result'][0]['like_status'];
            $data['posts'][$count]['id']=$row['id'];
            $data['posts'][$count]['user_id']=$row['user_id'];
            $data['posts'][$count]['type']=$row['type'];
            $data['posts'][$count]['description']=$row['description'];
            $data['posts'][$count]['post_youtube_video_url']=$row['post_youtube_video_url'];
            $data['posts'][$count]['post_image_name']=$row['post_image_name'];
            $data['posts'][$count]['question']=$row['question'];
            $data['posts'][$count]['location']=$row['location'];
            $data['posts'][$count]['poster_name']=$row['poster_name'];
            $data['posts'][$count]['poster_image_name']=$row['poster_image_name'];
            $data['posts'][$count]['poster_departments']=$row['poster_departments'];
            $data['posts'][$count]['time_after_posted']=$row['time_after_posted1'];
            $data['posts'][$count]['time_posted']=$row['time_posted1'];
            $data['posts'][$count]['data_posted']=$row['data_posted1'];
            $data['posts'][$count]['startAndEndDate']['start']=$row['start'];
            $data['posts'][$count]['startAndEndDate']['end']=$row['end'];
            $data['posts'][$count]['answers']['type']=$row['type1'];
            $answer_text=explode(",",$row['text']);
            $answer_votes=explode(",",$row['votes']);
            $posts_answers_details_ids=explode(",",$row['posts_answers_details_id']);
            $c=0;
            foreach ($answer_text as $res){
                $data['posts'][$count]['answers']['answers'][$c]['text']=$res;
                $data['posts'][$count]['answers']['answers'][$c]['votes']=$row['sum_votes']>0 ? round($answer_votes[$c]/$row['sum_votes']*100) : 0;
                $posts_answers_details_id=$posts_answers_details_ids[$c];
                $db->setQuery("SELECT id FROM posts_survey_details WHERE posts_answer_details_id='$posts_answers_details_id' AND user_id='$user_id'");
                IF($db->getNumRow()>0){
                    $data['posts'][$count]['answers']['answers'][$c]['status']='check';
                }else{
                    $data['posts'][$count]['answers']['answers'][$c]['status']='uncheck';
                }

                $c++;
            }
            $countF = "SELECT id FROM users WHERE actived=1";
            $db->setQuery($countF);
            $cc = $db->getResultArray();

            $countMarkedUsers = count(explode(",",$row['marked_users']));

            if($cc['count'] == $countMarkedUsers)
            {
                $usersC = '';
            }
            else
            {
                $usersC = $row['marked_users'];
            }

            $data['posts'][$count]['marked_groups']=explode(",",$row['marked_groups']);
            $data['posts'][$count]['marked_users']=explode(",",$usersC);
            $data['posts'][$count]['marked_departments']=explode(",",$row['marked_departments']);
            $data['posts'][$count]['likes']['sad']=$row['sad'];
            $data['posts'][$count]['likes']['happy']=$row['happy'];
            $data['posts'][$count]['likes']['neutral']=$row['neutral'];
            $data['posts'][$count]['likes']['love']=$row['love'];
            $data['posts'][$count]['likes']['amazed']=$row['amazed'];

            $answer_name=explode(",",$row['name']);
            $answer_image_name=explode(",",$row['image_name']);
            $answer_department=explode(",",$row['department']);
            $answer_time_after_posted=explode(",",$row['time_after_posted']);
            $answer_time_posted=explode(",",$row['time_posted']);
            $answer_data_posted=explode(",",$row['data_posted']);
            $answer_text=explode("~",$row['text1']);
            $answer_likes=explode(",",$row['likes']);
            $comments_id=explode(",",$row['comments_id']);
            $comments_user_id=explode(",",$row['comments_user_id']);
            $c=0;
            $max=max($answer_likes);
            $check_istop=0;
            IF($row['name']!=null){
                foreach ($answer_name as $res){
                    $comment_id_for_check=$comments_id[$c];
                    $db->setQuery("SELECT user_id FROM posts_comments_likes WHERE posts_comments_likes.posts_comments_id='$comment_id_for_check' AND posts_comments_likes.likes>0");
                    $like_status_array=$db->getResultArray();
                    $like_status=0;
                    foreach ($like_status_array['result'] as $like_status){
                        $like_status['user_id']==$user_id ? $like_status++ : '';
                    }
                    $like_status>0 ? $data['posts'][$count]['comments'][$c]['like_status']=true : $data['posts'][$count]['comments'][$c]['like_status']=false;
                    $data['posts'][$count]['comments'][$c]['user_id']=$comments_user_id[$c];
                    $data['posts'][$count]['comments'][$c]['comment_id']=$comments_id[$c];
                    $data['posts'][$count]['comments'][$c]['name']=$res;
                    $data['posts'][$count]['comments'][$c]['image_name']=$answer_image_name[$c];
                    $data['posts'][$count]['comments'][$c]['department']=$answer_department[$c];
                    $data['posts'][$count]['comments'][$c]['time_after_posted']=$answer_time_after_posted[$c];
                    $data['posts'][$count]['comments'][$c]['time_posted']=$answer_time_posted[$c];
                    $data['posts'][$count]['comments'][$c]['data_posted']=$answer_data_posted[$c];
                    $data['posts'][$count]['comments'][$c]['text']=ltrim($answer_text[$c],',');
                    $data['posts'][$count]['comments'][$c]['likes']=$answer_likes[$c];
                    IF($answer_likes[$c]==$max && $check_istop==0){
                        $data['posts'][$count]['comments'][$c]['isTop']=true;
                        $check_istop++;
                    }else{
                        $data['posts'][$count]['comments'][$c]['isTop']=false;
                    }
                    $c++;
                }
            }


            $count++;

        }
        $data['user_id']=$user_id;

        break;
        case "get_emoji" :

            $post_id = $_REQUEST["post_id"];

            $query = "SELECT `posts_likes`.posts_id, `posts_likes_details`.user_id, GROUP_CONCAT(`user_info`.`name`) AS `users_arr`, COUNT(`posts_likes_details`.type) AS `amount`, `posts_likes_details`.`type`

                        FROM `posts_likes`
                        
                        JOIN `posts_likes_details` ON `posts_likes_details`.posts_likes_id = `posts_likes`.id
                        JOIN `user_info` ON `user_info`.user_id = `posts_likes_details`.user_id
                        WHERE `posts_likes`.posts_id = $post_id
                        GROUP BY `posts_likes_details`.`type`";

            $db->setQuery($query);
            $result=$db->getResultArray();
            $data = array();
            foreach ($result['result'] as $row){
                $arr_user = explode(",",$row['users_arr']);
                $data[$row['type']] =  array(
                    "users" => $arr_user,
                    "amount"=> $row['amount']

                );
            }



        break;
        case 'get_posts_bytype':
            $count=$_REQUEST['count']*20;
            $fil = '';
            $type = $_REQUEST['type'];
            if($type == "poll")
                $fil = "AND `posts`.`type` = 'poll' "; 

            if($type == "event")
                $fil = "AND `posts`.`type` = 'event' "; 
                $db->setQuery("
         SELECT posts.id,
               posts.user_id,
               posts.type,
               posts.description,
               posts.question,
               posts.location,
               posts.url as post_youtube_video_url,
               posts.image as post_image_name,
               user.poster_name,
               user.poster_image_name,
               user.poster_departments,
               IF(TIMESTAMPDIFF(MINUTE, posts.datetime, NOW()) > 60, IF(TIMESTAMPDIFF(HOUR, posts.datetime, NOW()) > 24,
                                                                        GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts.datetime, NOW()),
                                                                                     ' დღის წინ'),
                                                                        GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts.datetime, NOW()),
                                                                                     ' საათის წინ')),
                  GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts.datetime, NOW()), ' წუთის წინ')) as time_after_posted1,
               posts.time_posted                                                            as time_posted1,
               posts.data_posted                                                            as data_posted1,
               posts_answers.type                                                           as type1,
               posts_answers_details.text                                                   as text,
               posts_answers_details.votes                                                  as votes,
               posts_answers_details.sum_votes                                              as sum_votes,
               posts_answers_details.posts_answers_details_id                               as posts_answers_details_id,
               department.name                                                              as marked_departments,
               activities_group.name                                                        as marked_groups,
               user_info.name                                                               as marked_users,
               posts_likes.sad                                                              as sad,
               posts_likes.happy                                                            as happy,
               posts_likes.neutral                                                          as neutral,
               posts_likes.love                                                             as love,
               posts_likes.amazed                                                           as amazed,
               posts_comments.name                                                          as name,
               posts_comments.image_name                                                    as image_name,
               posts_comments.time_after_posted                                             as time_after_posted,
               posts_comments.time_posted                                                   as time_posted,
               posts_comments.data_posted                                                   as data_posted,
               posts_comments.text                                                          as text1,
               posts_comments.likes                                                         as likes,
               posts_comments.department                                                    as department,
               posts_comments.comments_id                                                   as comments_id,
               posts_comments.comments_user_id                                              as comments_user_id,
               posts_startAndEndDate.end                                                    as `end`,
               posts_startAndEndDate.start                                                  as `start`
        
        
        FROM posts
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(posts_answers_details.id)    as posts_answers_details_id,
                                   GROUP_CONCAT(posts_answers_details.text)  as text,
                                   GROUP_CONCAT(posts_answers_details.votes) as votes,
                                   SUM(posts_answers_details.votes)          as sum_votes
                            FROM posts
                                     LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                                     JOIN posts_answers_details ON posts_answers.id = posts_answers_details.posts_answers_id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as posts_answers_details ON posts_answers_details.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(posts_answers.type) as type
                            FROM posts
                                     LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as posts_answers ON posts_answers.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(department.name) as name,
                                   GROUP_CONCAT(department.user_id) as user_id
                            FROM posts
                                     LEFT JOIN posts_marked_departments ON posts_marked_departments.posts_id = posts.id
                                     JOIN department ON department.id = posts_marked_departments.department_id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as department ON department.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(activities_group_detail.user_id) as user_id,
                                   GROUP_CONCAT(activities_group.name) as name
                            FROM posts
                                     LEFT JOIN posts_marked_groups ON posts_marked_groups.posts_id = posts.id
                                     JOIN activities_group ON activities_group.id = posts_marked_groups.activites_group_id
                                     LEFT JOIN activities_group_detail ON activities_group_detail.activites_group_id=activities_group.id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as activities_group ON activities_group.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(user_info.user_id) as user_id,
                                   GROUP_CONCAT(user_info.name) as name
        
                            FROM posts
                                     LEFT JOIN posts_marked_users ON posts_marked_users.posts_id = posts.id
                                     JOIN user_info ON user_info.user_id = posts_marked_users.user_id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as user_info ON user_info.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(posts_likes.sad)     as sad,
                                   GROUP_CONCAT(posts_likes.happy)   as happy,
                                   GROUP_CONCAT(posts_likes.neutral) as neutral,
                                   GROUP_CONCAT(posts_likes.love)    as love,
                                   GROUP_CONCAT(posts_likes.amazed)  as amazed
                            FROM posts
                                     LEFT JOIN posts_likes ON posts_likes.posts_id = posts.id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as posts_likes ON posts_likes.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   GROUP_CONCAT(posts_comments.user_id)                                                  as comments_user_id,
                                   GROUP_CONCAT(posts_comments.id)                                                       as comments_id,
                                   GROUP_CONCAT(posts_comments.name)                                                     as name,
                                   GROUP_CONCAT(posts_comments.image_name)                                               as image_name,
                                   IF(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()) > 60,
                                      IF(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()) > 24,
                                         GROUP_CONCAT(TIMESTAMPDIFF(DAY, posts_comments.datetime, NOW()), ' დღის წინ'),
                                         GROUP_CONCAT(TIMESTAMPDIFF(HOUR, posts_comments.datetime, NOW()), ' საათის წინ')),
                                      GROUP_CONCAT(TIMESTAMPDIFF(MINUTE, posts_comments.datetime, NOW()),
                                                   ' წუთის წინ'))                                                        as time_after_posted,
                                   GROUP_CONCAT(posts_comments.time_posted)                                              as time_posted,
                                   GROUP_CONCAT(posts_comments.data_posted)                                              as data_posted,
                                   GROUP_CONCAT(posts_comments.text, '~')                                             as text,
                                   GROUP_CONCAT((SELECT sum(posts_comments_likes.likes)
                                                 FROM posts_comments_likes
                                                 WHERE posts_comments_id = posts_comments.id))                          as likes,
                                   GROUP_CONCAT(department1.name)                                                        as department
                            FROM posts
                                     LEFT JOIN posts_comments ON posts_comments.posts_id = posts.id
                                     JOIN department as department1 ON posts_comments.department_id = department1.id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as posts_comments ON posts_comments.id = posts.id
                 LEFT JOIN (SELECT posts.id, posts_startAndEndDate.start, posts_startAndEndDate.end
                            FROM posts
                                     LEFT JOIN posts_startAndEndDate ON posts_startAndEndDate.posts_id = posts.id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as posts_startAndEndDate ON posts_startAndEndDate.id = posts.id
                 LEFT JOIN (SELECT posts.id,
                                   user.name                      as poster_name,
                                   user.image                     as poster_image_name,
                                   GROUP_CONCAT(department1.name) as poster_departments
                            FROM posts
                                     LEFT JOIN user_info as user ON user.user_id = posts.user_id
                                     LEFT JOIN department as department1 ON department1.id = user.dep_id
                            WHERE posts.actived = 1
                            GROUP BY posts.id) as user ON user.id = posts.id
        
        WHERE posts.actived = 1 $fil AND ((( FIND_IN_SET('$user_id',department.user_id)>0 OR  FIND_IN_SET('$user_id', activities_group.user_id)>0 OR  FIND_IN_SET('$user_id',user_info.user_id)>0) OR (department.user_id IS NULL AND activities_group.user_id IS NULL AND user_info.user_id IS NULL)) OR '$user_id'=1 OR posts.user_id='$user_id')
        GROUP BY posts.id
        order by posts.id desc
        LIMIT 20                  ");
        
                $result=$db->getResultArray();
                $count=0;
                foreach ($result['result'] as $row){
                    $post_check_id=$row['id'];
                    $db->setQuery("SELECT IF (FIND_IN_SET('$user_id',GROUP_CONCAT(posts_likes_details.user_id))>0,'true' ,'false' ) as like_status 
                                   FROM posts_likes
                                   LEFT JOIN posts_likes_details ON posts_likes.id=posts_likes_details.posts_likes_id
                                   WHERE posts_likes.posts_id='$post_check_id' 
                                   GROUP BY posts_likes.id");
        
                    $like_res=$db->getResultArray();
                    $data['posts'][$count]['like_status']=$like_res['result'][0]['like_status'];
                    $data['posts'][$count]['id']=$row['id'];
                    $data['posts'][$count]['user_id']=$row['user_id'];
                    $data['posts'][$count]['type']=$row['type'];
                    $data['posts'][$count]['description']=$row['description'];
                    $data['posts'][$count]['post_youtube_video_url']=$row['post_youtube_video_url'];
                    $data['posts'][$count]['post_image_name']=$row['post_image_name'];
                    $data['posts'][$count]['question']=$row['question'];
                    $data['posts'][$count]['location']=$row['location'];
                    $data['posts'][$count]['poster_name']=$row['poster_name'];
                    $data['posts'][$count]['poster_image_name']=$row['poster_image_name'];
                    $data['posts'][$count]['poster_departments']=$row['poster_departments'];
                    $data['posts'][$count]['time_after_posted']=$row['time_after_posted1'];
                    $data['posts'][$count]['time_posted']=$row['time_posted1'];
                    $data['posts'][$count]['data_posted']=$row['data_posted1'];
                    $data['posts'][$count]['startAndEndDate']['start']=$row['start'];
                    $data['posts'][$count]['startAndEndDate']['end']=$row['end'];
                    $data['posts'][$count]['answers']['type']=$row['type1'];
                    $answer_text=explode(",",$row['text']);
                    $answer_votes=explode(",",$row['votes']);
                    $posts_answers_details_ids=explode(",",$row['posts_answers_details_id']);
                    $c=0;
                    foreach ($answer_text as $res){
                        $data['posts'][$count]['answers']['answers'][$c]['text']=$res;
                        $data['posts'][$count]['answers']['answers'][$c]['votes']=$row['sum_votes']>0 ? round($answer_votes[$c]/$row['sum_votes']*100) : 0;
                        $posts_answers_details_id=$posts_answers_details_ids[$c];
                        $db->setQuery("SELECT id FROM posts_survey_details WHERE posts_answer_details_id='$posts_answers_details_id' AND user_id='$user_id'");
                        IF($db->getNumRow()>0){
                            $data['posts'][$count]['answers']['answers'][$c]['status']='check';
                        }else{
                            $data['posts'][$count]['answers']['answers'][$c]['status']='uncheck';
                        }
        
                        $c++;
                    }
        
                    $data['posts'][$count]['marked_groups']=explode(",",$row['marked_groups']);
                    $data['posts'][$count]['marked_users']=explode(",",$row['marked_users']);
                    $data['posts'][$count]['marked_departments']=explode(",",$row['marked_departments']);
                    $data['posts'][$count]['likes']['sad']=$row['sad'];
                    $data['posts'][$count]['likes']['happy']=$row['happy'];
                    $data['posts'][$count]['likes']['neutral']=$row['neutral'];
                    $data['posts'][$count]['likes']['love']=$row['love'];
                    $data['posts'][$count]['likes']['amazed']=$row['amazed'];
        
                    $answer_name=explode(",",$row['name']);
                    $answer_image_name=explode(",",$row['image_name']);
                    $answer_department=explode(",",$row['department']);
                    $answer_time_after_posted=explode(",",$row['time_after_posted']);
                    $answer_time_posted=explode(",",$row['time_posted']);
                    $answer_data_posted=explode(",",$row['data_posted']);
                    $answer_text=explode("~",$row['text1']);
                    $answer_likes=explode(",",$row['likes']);
                    $comments_id=explode(",",$row['comments_id']);
                    $comments_user_id=explode(",",$row['comments_user_id']);
                    $c=0;
                    $max=max($answer_likes);
                    $check_istop=0;
                    IF($row['name']!=null){
                        foreach ($answer_name as $res){
                            $comment_id_for_check=$comments_id[$c];
                            $db->setQuery("SELECT user_id FROM posts_comments_likes WHERE posts_comments_likes.posts_comments_id='$comment_id_for_check' AND posts_comments_likes.likes>0");
                            $like_status_array=$db->getResultArray();
                            $like_status=0;
                            foreach ($like_status_array['result'] as $like_status){
                                $like_status['user_id']==$user_id ? $like_status++ : '';
                            }
                            $like_status>0 ? $data['posts'][$count]['comments'][$c]['like_status']=true : $data['posts'][$count]['comments'][$c]['like_status']=false;
                            $data['posts'][$count]['comments'][$c]['user_id']=$comments_user_id[$c];
                            $data['posts'][$count]['comments'][$c]['comment_id']=$comments_id[$c];
                            $data['posts'][$count]['comments'][$c]['name']=$res;
                            $data['posts'][$count]['comments'][$c]['image_name']=$answer_image_name[$c];
                            $data['posts'][$count]['comments'][$c]['department']=$answer_department[$c];
                            $data['posts'][$count]['comments'][$c]['time_after_posted']=$answer_time_after_posted[$c];
                            $data['posts'][$count]['comments'][$c]['time_posted']=$answer_time_posted[$c];
                            $data['posts'][$count]['comments'][$c]['data_posted']=$answer_data_posted[$c];
                            $data['posts'][$count]['comments'][$c]['text']=ltrim($answer_text[$c],',');
                            $data['posts'][$count]['comments'][$c]['likes']=$answer_likes[$c];
                            IF($answer_likes[$c]==$max && $check_istop==0){
                                $data['posts'][$count]['comments'][$c]['isTop']=true;
                                $check_istop++;
                            }else{
                                $data['posts'][$count]['comments'][$c]['isTop']=false;
                            }
                            $c++;
                        }
                    }
        
        
                    $count++;
        
                }
                $data['user_id']=$user_id;
        
                break;
    case "notification":
        $lastPostId = $_REQUEST['lastPostId'];
        $postType = $_REQUEST['postType'];
        $aditSQL = '';
        if(!empty($postType))
        {
            $aditSQL = " AND posts.type = '$postType'";
        }
        $db->setQuery("SELECT
        posts.`id`,
        `user_info`.`image` AS `poster_image_name`,
        `user_info`.`name` AS `poster_name`,
        `posts`.`description` AS `post_description`,
        '1 წუთი' AS `post_time` 
    FROM
        posts
        LEFT JOIN `user_info` ON `user_info`.`user_id` = posts.user_id
    
        LEFT JOIN `user_info` AS `ses_user` ON `ses_user`.`user_id` = '$user_id'
        LEFT JOIN `department` AS `user_dep` ON `user_dep`.id = `ses_user`.dep_id
        
        LEFT JOIN posts_marked_users ON posts_marked_users.posts_id = posts.id
        LEFT JOIN user_info AS `marked` ON user_info.user_id = posts_marked_users.user_id
        
        LEFT JOIN `posts_marked_departments` ON `posts_marked_departments`.posts_id = posts.id
        LEFT JOIN `department` ON `posts_marked_departments`.department_id = department.id 

        LEFT JOIN `posts_marked_groups` ON `posts_marked_groups`.posts_id = posts.id
        LEFT JOIN `activities_group_detail` ON `activities_group_detail`.activites_group_id  = `posts_marked_groups`.activites_group_id
    WHERE
        posts.id >= $lastPostId $aditSQL
        AND ( 
        
           `ses_user`.dep_id = `posts_marked_departments`.department_id 
		
        OR `activities_group_detail`.user_id = `ses_user`.`user_id`

        OR `posts_marked_users`.user_id = `ses_user`.user_id

        OR ( `activities_group_detail`.user_id is null AND `ses_user`.dep_id is null AND `posts_marked_users`.user_id is null)

        
   
        

        
        ) 
    GROUP BY
        posts.id");

        $res = $db->getResultArray();

        if($res['count'] <= 0)
        {

        }

        $res2 = $db->getResultArray();
        $posts = $res2['result'];


        // for($i = 0; $i<$res['count']; $i++){
        //     $data['notification'][$i]['id'] = $posts[$i]['id'];
        //     $data['notification'][$i]['poster_image_name'] = $posts[$i]['poster_image_name'];
        //     $data['notification'][$i]['poster_name'] = $posts[$i]['poster_name'];
        //     $data['notification'][$i]['post_description'] = $posts[$i]['post_description'];
        //     $data['notification'][$i]['post_time'] = "8 წუთი"; //$posts[$i]['post_time'];
        // }

        $data = array (
            "notifications" => $posts
        );




        

        break;
    case 'add_like':
        $id   =$_REQUEST['id'];
        $type =$_REQUEST['type'];
        $db->setQuery("SELECT id FROM posts_likes WHERE posts_id='$id'");
        $res=$db->getResultArray();
        $posts_likes_id=$res['result'][0]['id'];

        $db->setQuery("SELECT type FROM posts_likes_details WHERE user_id='$user_id' AND posts_likes_id='$posts_likes_id'");
        IF($db->getNumRow()>0){
            $res=$db->getResultArray();
            $res_type=$res['result'][0]['type'];
            IF($res_type==$type){
                $db->setQuery("SELECT $res_type FROM posts_likes WHERE posts_id='$id'");
                $result=$db->getResultArray();
                $count=(int)$result['result'][0][$res_type];
                $count--;
                if($count<0) $count = 0;
                $db->setQuery("UPDATE posts_likes
                       SET $res_type='$count'
                       WHERE posts_id=$id
                      
                     ");
                $db->execQuery();

                $db->setQuery("DELETE FROM posts_likes_details WHERE user_id='$user_id' AND posts_likes_id='$posts_likes_id'");
                $db->execQuery();

            }else{
                $db->setQuery("SELECT $type FROM posts_likes WHERE posts_id='$id'");
                $result=$db->getResultArray();
                $count=(int)$result['result'][0][$type];
                $count++;
                $db->setQuery("UPDATE posts_likes
                       SET $type='$count'
                       WHERE posts_id=$id
                      
                     ");
                $db->execQuery();

                $db->setQuery("SELECT $res_type FROM posts_likes WHERE posts_id='$id'");
                $result=$db->getResultArray();
                $count=(int)$result['result'][0][$res_type];
                $count--;
                $db->setQuery("UPDATE posts_likes
                       SET $res_type='$count'
                       WHERE posts_id=$id
                      
                     ");
                $db->execQuery();

                $db->setQuery("UPDATE posts_likes_details SET type='$type' WHERE user_id='$user_id' AND posts_likes_id='$posts_likes_id'");
                $db->execQuery();
            }

        }else{


            $db->setQuery("SELECT $type FROM posts_likes WHERE posts_id='$id'");
            $result=$db->getResultArray();
            $count=(int)$result['result'][0][$type];
            $count++;
            $db->setQuery("UPDATE posts_likes
                       SET $type='$count'
                       WHERE posts_id=$id
                      
                     ");
            $db->execQuery();

            $db->setQuery("INSERT INTO posts_likes_details (type, user_id,posts_likes_id) VALUES ('$type','$user_id','$posts_likes_id')");
            $db->execQuery();
        }

        $db->setQuery("SELECT  sad, happy, neutral, love, amazed FROM posts_likes WHERE posts_id=$id");
        $final_result=$db->getResultArray();

        foreach ($final_result['result'] as $row){
            $data['likes'][0]['sad']=$row['sad'];
            $data['likes'][0]['happy']=$row['happy'];
            $data['likes'][0]['neutral']=$row['neutral'];
            $data['likes'][0]['love']=$row['love'];
            $data['likes'][0]['amazed']=$row['amazed'];
        }

        break;

    case 'set_survey_radio':


        IF($_REQUEST['inputType']=='checkbox'){

            $post_id=$_REQUEST['id'];
            $pollsNameToAdds=$_REQUEST['text'];
            $db->setQuery("SELECT GROUP_CONCAT(posts_answer_details_id) as  posts_answer_details_id FROM posts_survey_details WHERE user_id='$user_id' and posts_id='$post_id'");
            $res=$db->getResultArray();
            $posts_answer_details_ids=$res['result'][0]['posts_answer_details_id'];
            IF($posts_answer_details_ids!=null){

                $db->setQuery("SELECT GROUP_CONCAT(text) as text FROM posts_answers_details WHERE posts_answers_details.id IN ($posts_answer_details_ids)");
                $res=$db->getResultArray();
                $old_texts=explode(",",$res['result'][0]['text']);
                $add_array=array_diff($pollsNameToAdds,$old_texts);
                if(sizeof($add_array)>0){
                    foreach ($add_array as $add){
                        $db->setQuery("SELECT posts_answers_details.id as id FROM posts_answers 
                                       JOIN posts_answers_details ON posts_answers.id=posts_answers_details.posts_answers_id 
                                       WHERE posts_answers.actived=1 AND  posts_answers_details.text='$add' AND posts_answers.posts_id='$post_id'");
                        $add_survey_array=$db->getResultArray();
                        $add_posts_answers_detail_id=$add_survey_array['result'][0]['id'];
                        $db->setQuery("INSERT INTO posts_survey_details (posts_id, user_id, posts_answer_details_id)
                                       VALUES ('$post_id','$user_id','$add_posts_answers_detail_id')");
                        $db->execQuery();
                    }
                }
                foreach ($pollsNameToAdds as $pollsNameToAdd){
                    $db->setQuery("SELECT posts_answers_details.votes, posts_answers_details.id 
                           FROM posts_answers 
                           JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           WHERE posts_answers.posts_id='$post_id' AND  posts_answers_details.text='$pollsNameToAdd' LIMIT 1");
                    $res=$db->getResultArray();
                    $count=(int)$res['result'][0]['votes'];
                    $posts_answer_details_id=$res['result'][0]['id'];
                    $count++;

                    $db->setQuery("UPDATE posts_answers
                           JOIN   posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           SET    votes='$count'
                           WHERE  posts_answers.posts_id='$post_id' AND posts_answers_details.text='$pollsNameToAdd'");
                    $db->execQuery();
                }
                foreach ($old_texts as $old_text){

                        $db->setQuery("SELECT posts_answers_details.votes, posts_answers_details.id 
                           FROM posts_answers 
                           JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           WHERE posts_answers.posts_id='$post_id' AND posts_answers_details.text='$old_text' LIMIT 1");
                        $res=$db->getResultArray();
                        $count=(int)$res['result'][0]['votes'];
                        $posts_answer_details_id=$res['result'][0]['id'];
                        $count--;

                        $db->setQuery("UPDATE posts_answers
                           JOIN   posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           SET    votes='$count'
                           WHERE  posts_answers.posts_id='$post_id' AND posts_answers_details.text='$old_text'");
                        $db->execQuery();


                        $delete_array=$pollsNameToAdds==null ? $old_texts : array_diff($old_texts,$pollsNameToAdds);

                        foreach ($delete_array as $del){
                            $db->setQuery("SELECT GROUP_CONCAT(id) as id FROM posts_answers_details  WHERE text='$del' AND id IN ($posts_answer_details_ids)");
                            $res=$db->getResultArray();
                            $del_ids=$res['result'][0]['id'];
                            $db->setQuery("DELETE FROM posts_survey_details 
                                           WHERE posts_survey_details.posts_answer_details_id IN ($del_ids) AND posts_survey_details.user_id='$user_id'");
                            $db->execQuery();
                        }





                }


            }else{
                foreach ($pollsNameToAdds as $pollsNameToAdd){
                    $db->setQuery("SELECT posts_answers_details.votes, posts_answers_details.id 
                           FROM posts_answers 
                           JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           WHERE posts_answers.posts_id='$post_id' AND posts_answers_details.text='$pollsNameToAdd' LIMIT 1");
                    $res=$db->getResultArray();
                    $count=(int)$res['result'][0]['votes'];
                    $posts_answer_details_id=$res['result'][0]['id'];
                    $count++;
                    $db->setQuery("UPDATE posts_answers
                           JOIN   posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           SET    votes='$count'
                           WHERE  posts_answers.posts_id='$post_id' AND posts_answers_details.text='$pollsNameToAdd'");
                    $db->execQuery();
                    foreach ($res['result'] as $row){
                        $posts_answer_details_id=$row['id'];
                        $db->setQuery("INSERT INTO posts_survey_details (posts_id, user_id, posts_answer_details_id)
                                   VALUES ('$post_id','$user_id','$posts_answer_details_id')");
                        $db->execQuery();
                    }
                }

            }

            $db->setQuery("SELECT posts.id,
                           GROUP_CONCAT(posts_answers_details.text)   as text,
                           GROUP_CONCAT(posts_answers_details.votes)  as votes
                    FROM posts
                             LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                             JOIN posts_answers_details ON posts_answers.id = posts_answers_details.posts_answers_id
                    WHERE posts.actived = 1 AND posts.id='$post_id'
                    GROUP BY posts.id");
            $result=$db->getResultArray();

            foreach ($result['result'] as $row){

                $answer_text=explode(",",$row['text']);
                $answer_votes=explode(",",$row['votes']);
                $c=0;
                $sum=0;
                foreach ($answer_text as $res){
                    $db->setQuery("SELECT posts_answers.id 
                                   FROM posts_answers
                                   JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                                   JOIN posts_survey_details ON posts_survey_details.posts_answer_details_id=posts_answers_details.id
                                   WHERE posts_answers_details.text='$res' AND posts_survey_details.user_id='$user_id' AND posts_answers.posts_id='$post_id' AND posts_answers.actived=1");
                    IF($db->getNumRow()>0){
                        $data['answers']['answers'][$c]['status']='check';
                    }else{
                        $data['answers']['answers'][$c]['status']='uncheck';
                    }
                    $data['answers']['type']='checkbox';
                    $data['answers']['answers'][$c]['text']=$res;
                    $data['answers']['answers'][$c]['votes']=$answer_votes[$c];
                    $sum+=$answer_votes[$c];
                    $c++;
                }
                $c=0;
                foreach ($answer_votes as $row){
                    $data['answers']['answers'][$c]['votes']=$sum>0?round($row/$sum*100):0;
                    $c++;
                }
            }


        }else{

            $post_id=$_REQUEST['id'];
            $pollsNameToAdd=$_REQUEST['text'];
            $db->setQuery("SELECT posts_answer_details_id FROM posts_survey_details WHERE user_id='$user_id' and posts_id='$post_id'");
            IF($db->getNumRow()>0){
                $res=$db->getResultArray();
                $posts_answer_details_id=$res['result'][0]['posts_answer_details_id'];
                $db->setQuery("SELECT text FROM posts_answers_details WHERE posts_answers_details.id='$posts_answer_details_id'");
                $res=$db->getResultArray();
                $old_text=$res['result'][0]['text'];
                IF($pollsNameToAdd==$old_text){

                }else{
                    $db->setQuery("SELECT posts_answers_details.votes, posts_answers_details.id 
                           FROM posts_answers 
                           JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           WHERE posts_answers.posts_id='$post_id' AND posts_answers_details.id= $posts_answer_details_id LIMIT 1");
                    $res=$db->getResultArray();
                    $count=(int)$res['result'][0]['votes'];
                    $posts_answer_details_id=$res['result'][0]['id'];
                    $count--;

                    $db->setQuery("UPDATE posts_answers
                           JOIN   posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           SET    votes='$count'
                           WHERE  posts_answers.posts_id='$post_id' AND posts_answers_details.text='$old_text'");
                    $db->execQuery();

                    $db->setQuery("SELECT posts_answers_details.votes, posts_answers_details.id 
                           FROM posts_answers 
                           JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           WHERE posts_answers.posts_id='$post_id' AND  posts_answers_details.text='$pollsNameToAdd' LIMIT 1");
                    $res=$db->getResultArray();
                    $count=(int)$res['result'][0]['votes'];
                    $posts_answer_details_id=$res['result'][0]['id'];
                    $count++;

                    $db->setQuery("UPDATE posts_answers
                           JOIN   posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           SET    votes='$count'
                           WHERE  posts_answers.posts_id='$post_id' AND posts_answers_details.text='$pollsNameToAdd'");
                    $db->execQuery();

                    $db->setQuery("UPDATE posts_survey_details 
                               SET    posts_answer_details_id='$posts_answer_details_id'
                               WHERE  posts_id='$post_id' AND user_id='$user_id'
                               ");
                    $db->execQuery();
                }


            }else{
                $db->setQuery("SELECT posts_answers_details.votes, posts_answers_details.id 
                           FROM posts_answers 
                           JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           WHERE posts_answers.posts_id='$post_id' AND posts_answers_details.text='$pollsNameToAdd' LIMIT 1");
                $res=$db->getResultArray();
                $count=(int)$res['result'][0]['votes'];
                $posts_answer_details_id=$res['result'][0]['id'];
                $count++;
                $db->setQuery("UPDATE posts_answers
                           JOIN   posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                           SET    votes='$count'
                           WHERE  posts_answers.posts_id='$post_id' AND posts_answers_details.text='$pollsNameToAdd'");
                $db->execQuery();

                $db->setQuery("INSERT INTO posts_survey_details (posts_id, user_id, posts_answer_details_id)
                           VALUES ('$post_id','$user_id','$posts_answer_details_id')");
                $db->execQuery();
            }

            $db->setQuery("SELECT posts.id,
                           GROUP_CONCAT(posts_answers_details.text)   as text,
                           GROUP_CONCAT(posts_answers_details.votes)  as votes
                    FROM posts
                             LEFT JOIN posts_answers ON posts_answers.posts_id = posts.id
                             JOIN posts_answers_details ON posts_answers.id = posts_answers_details.posts_answers_id
                    WHERE posts.actived = 1 AND posts.id='$post_id'
                    GROUP BY posts.id");
            $result=$db->getResultArray();

            foreach ($result['result'] as $row){
                $answer_text=explode(",",$row['text']);
                $answer_votes=explode(",",$row['votes']);
                $c=0;
                $sum=0;
                $data['answers']['type']='radio';
                foreach ($answer_text as $res){
                    $db->setQuery("SELECT posts_answers.id 
                                   FROM posts_answers
                                   JOIN posts_answers_details ON posts_answers_details.posts_answers_id=posts_answers.id
                                   JOIN posts_survey_details ON posts_survey_details.posts_answer_details_id=posts_answers_details.id AND posts_survey_details.user_id='$user_id'
                                   WHERE posts_answers_details.text='$res' AND posts_survey_details.user_id='$user_id' AND posts_answers.posts_id='$post_id' AND posts_answers.actived=1");
                    IF($db->getNumRow()>0){
                        $data['answers']['answers'][$c]['status']='check';
                    }else{
                        $data['answers']['answers'][$c]['status']='uncheck';
                    }
                    $data['answers']['answers'][$c]['text']=$res;
                    $data['answers']['answers'][$c]['votes']=$answer_votes[$c];
                    $sum+=$answer_votes[$c];
                    $c++;
                }
                $c=0;
                foreach ($data['answers']['answers'] as $row){
                    $data['answers']['answers'][$c]['votes']=$sum>0 ? round($row['votes']/$sum*100):0;
                    $c++;
                }
            }


        }

    break;
    case 'upload_image':

        if(!empty($_FILES))
        {
            $info = pathinfo($_FILES['image_upload']['name']);
            $i = 0;
            $path='';
            do {
                $image_name = $info['filename'] . ($i ? "($i)" : ""). "." . $info['extension'] ;
                $i++;
                $path = '../../media/uploads/posts/' . $image_name;
            } while(file_exists($path));
            if(move_uploaded_file($_FILES['image_upload']['tmp_name'], $path))
            {
                $data['file_name']=$image_name;
            }
            @unlink ($_FILES ['image_upload']);
        }
        else
        {

        }

        break;

    case 'delete_post':
        $id=$_REQUEST['id'];
        $db->setQuery("UPDATE posts SET actived=0 WHERE  id='$id'");
        $db->execQuery();
        break;
    default:
        $data="";
}


echo json_encode($data);


?>