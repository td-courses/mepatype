<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript">
            //var tablesColumns = setColumns();
            var aJaxURL = "server-side/report/kaps_category.action.php";
            $(document).ready(function() {
                $("#category_id, #subCategory,#thirdCategory").chosen();
            });
            $(document).on("change", "#category_id", function() {
                console.log(123);
                param = new Object();
                param.act = 'get_category';
                param.category_id = $("#category_id").val();
                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        $("#subCategory").html(data.page);
                        $("#subCategory").trigger("chosen:updated");
                    }
                });
            });
            $(document).on("change", "#subCategory", function() {
                console.log(123);
                param = new Object();
                param.act = 'get_category';
                param.category_id = $("#subCategory").val();
                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        $("#thirdCategory").html(data.page);
                        $("#thirdCategory").trigger("chosen:updated");
                    }
                });
            });
        </script>
    </head>

    <body>
        <div id="tabs">
            <table>
                <tr>
                    <td style="width: 200px;">
                        <select id="category_id" style="width: 185px;">
                            <?php
                            $data = '<option value="0">აირჩიე კატეგორია</option>';
                            global $db;
                            $db->setQuery(" SELECT id, 
                                                `name`
                                            FROM   info_category
                                            WHERE  parent_id=0 AND actived=1");
                            $res = $db->getResultArray();
                            foreach ($res['result'] as $req) {
                                $data .= '<option value="' . $req['id'] . '">' . $req['name'] . '</option>';
                            }
                            echo $data;
                            ?>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <select id="subCategory" style="width: 185px;">
                            <option value="0">აირჩიეთ ქვეკატეგორია</option>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <select id="thirdCategory" style="width: 185px;">
                            <option value="0">აირჩიეთ ქვეკატეგორია</option>
                        </select>
                    </td>
                </tr>
            </table>   
        </div>
    </body>
</html>