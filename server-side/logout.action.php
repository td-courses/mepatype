<?php
/* ******************************
 *	Request aJax actions
 * ******************************
*/

require_once('../includes/classes/class.Mysqli.php');
require_once('../includes/classes/authorization.class.php');
$logout = new Authorization();
global $db;
$db = new dbClass();

$action = $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'logout':
		$page		= GetPage();
		$data		= array('page'	=> $page);

        break;
    case 'logout_save':
        $tabel_id = $_REQUEST['tabel_id'];
		$logout_comment = $_REQUEST['logout_comment'];
        SaveLogout($tabel_id, $logout_comment);
        $logout->logout();
        break;
    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
 * ******************************
 */

function SaveLogout($tabel_id, $logout_comment){
    global $db;
    $user		= $_SESSION['USERID'];

		// old code

    /*if($tabel_id == 1){
        mysql_query("
                     UPDATE  `worker_action` SET
                            `end_date`=NOW(),
                            `actived`='4'
                    WHERE   (`actived`= 4 or `actived`= 1) AND `person_id`='$user' AND date(start_date) = date(NOW())
                    ");
    }elseif($tabel_id == 2){
        mysql_query("
                    UPDATE  `worker_action` SET
                            `end_date`=NOW(),
                            `actived`='4'
                    WHERE   (`actived`= 4 or `actived`= 1) AND `person_id`='$user' AND date(start_date) = date(NOW())
                    ");
    }*/

		// new code

		$db->setQuery("UPDATE   `worker_action` 
                          SET   `end_date`= NOW(),
    							`actived` = '4',
    							`comment` = '$logout_comment'
    				   WHERE   (`actived` = 4 or `actived`= 1) AND `person_id`='$user' AND date(start_date) = date(NOW())");
		
		$db->execQuery();
}

function GetStatus($id=0){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`, `name`
				   FROM   `work_activities`
				   WHERE   actived=1");

    $data = $db->getSelect($id);

    return $data;
}


function GetPage()
{
	$data  = '
	<div id="dialog-form">
		<fieldset style="margin-top: 5px;">
				<label for="tabel_id">აქტივობა</label>
	       <select id="tabel_id">'.GetStatus().'</select>

				 <label for="logout_comment">კომენტარი</label>
				 <textarea id="logout_comment"></textarea>
	    </fieldset>
    </div>';

	return $data;
}



?>
