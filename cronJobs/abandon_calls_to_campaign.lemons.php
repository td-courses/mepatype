<?php

        error_reporting(E_ALL);

        /**
         * CRON AUTOREGISTER unanswered and missed Calls from Asterisk to Campaign
         * @global $db
         */
        include '../includes/classes/class.Mysqli.php';
        $db = new dbClass();

        // კამპანიის ლოდინის პერიოდის შემოწმება
        $db->setQuery(" SELECT  p.to_call_time*60 AS 'max_off'
                        FROM    `outgoing_campaign_settings_parameters` AS `p`
                        JOIN    `outgoing_campaign_request` AS `req` ON `req`.outgoing_campaign_settings_parameters_id = p.id AND req.id = 3
                        WHERE req.outgoing_campaign_active_status_id IN(1,2)");

        $wait_time_check = $db->getResultArray();

        if($db->getNumRow() > 0){

                $wait_time = $wait_time_check[result][0][max_off];
                if($wait_time_check[count] == 0){
                        $wait_time = 0; 
                }
                
                // ასტერისკის ჩანაწერებში გამოტოვებული ზარის შემოწმება
                $db->setQuery(" SELECT asterisk_call_log.source,UNIX_TIMESTAMP(NOW()) - asterisk_call_log.call_datetime
                FROM  asterisk_call_log
                LEFT JOIN asterisk_call_log AS `ast_call_log` ON asterisk_call_log.source = ast_call_log.source
                AND ast_call_log.call_datetime > asterisk_call_log.call_datetime AND ast_call_log.call_status_id IN(6,7,8)
                AND ast_call_log.call_type_id = 1
                WHERE asterisk_call_log.call_status_id = 9 
                AND   asterisk_call_log.call_type_id = 1
                AND   UNIX_TIMESTAMP(NOW()) - asterisk_call_log.call_datetime > $wait_time
                AND   DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = CURDATE()
                AND   ISNULL(ast_call_log.id)
                AND   asterisk_call_log.source 
                NOT IN( SELECT outgoing_campaign_request_base.phone_number 
                        FROM   outgoing_campaign_request_base
                        JOIN   outgoing_campaign_request_details ON outgoing_campaign_request_details.id = outgoing_campaign_request_base.outgoing_campaign_request_details_id AND outgoing_campaign_request_base.actived = 1
                        AND outgoing_campaign_request_details.actived = 1
                        AND DATE(outgoing_campaign_request_details.datetime) = CURDATE())");
                $res = $db->getResultArray();
                
                


                if( $res[count] > 0){
                        $db->setQuery("INSERT 
                                        INTO `outgoing_campaign_request_details`
                                                (`outgoing_campaign_request_id`, `datetime`, `comment`)
                                        VALUES 
                                                (3, NOW(), 'ავტომატურად ატვირთული')");
                        $db->execQuery();
                        $d_ID = $db->getLastId();

                        $q = '';
                        foreach($res[result] as $r){
                                $q .= "(3, '$d_ID', '$r[source]', 1, 3, 1),";
                        }
                        $h = rtrim($q, ',');
                        
                        $db->setQuery("INSERT 
                                        INTO `outgoing_campaign_request_base`
                                                (`outgoing_campaign_request_id`, `outgoing_campaign_request_details_id`, `phone_number`, `tree_id`, `sub_tree_id`, `outgoing_campaign_request_status_id`)
                                        VALUES
                                                $h");
                        $db->execQuery();
                }


                $db->setQuery(" SELECT  id,
                                        outgoing_campaign_settings_group_schedule_id AS 'camp_id'

                                FROM    outgoing_campaign_request
                                WHERE   actived = 1");
                $campaigns = $db->getResultArray();

                foreach($campaigns['result'] AS $camp){
                        $db->setQuery(" SELECT  outgoing_campaign_settings_group_schedule_details.operator_id
                                        FROM    outgoing_campaign_settings_group_schedule_details
                                        JOIN    users ON users.id = outgoing_campaign_settings_group_schedule_details.operator_id AND users.logged = 1 AND users.extension_id IS NOT NULL AND users.actived = 1
                                        LEFT JOIN asterisk_extension ON asterisk_extension.id = users.extension_id
                                        JOIN 		asteriskcdrdb.queues ON asteriskcdrdb.queues.member = asterisk_extension.number AND asteriskcdrdb.queues.`status` = 'not in use'
                                        WHERE   outgoing_campaign_settings_group_schedule_details.outgoing_campaign_settings_group_schedule_id = $camp[camp_id]");
                        $added_operators = $db->getResultArray();

                        /* $db->setQuery(" SELECT  outgoing_campaign_request_base.id,
                                                                outgoing_campaign_request_base.phone_number,
                                                                outgoing_campaign_request_base.operator_id
                                        FROM    outgoing_campaign_request_base
                                        LEFT JOIN users ON users.id = outgoing_campaign_request_base.operator_id
                                        LEFT JOIN asterisk_extension ON asterisk_extension.id = users.extension_id
                                        LEFT JOIN asteriskcdrdb.queues ON asteriskcdrdb.queues.member = asterisk_extension.number
                                        WHERE   outgoing_campaign_request_base.call_status = 0 AND outgoing_campaign_request_base.outgoing_campaign_request_id = $camp[id]
                                                                AND (users.logged = 0 OR users.extension_id IS NULL OR users.actived = 0 OR asteriskcdrdb.queues.`status` != 'not in use')
                                                                
                                        ORDER BY outgoing_campaign_request_base.id"); */
                        $db->setQuery(" SELECT  id,phone_number,operator_id
                                        FROM    outgoing_campaign_request_base
                                        WHERE   call_status = 0 AND outgoing_campaign_request_id = $camp[id]
                                        ORDER BY id ASC");
                        $notDialered = $db->getResultArray();



                        $operator_array = array();
                        foreach($added_operators['result'] AS $operator){
                                array_push($operator_array,$operator['operator_id']);
                        }

                        
                        $i = 0;
                        foreach($notDialered['result'] AS $person){
                                if($i < count($operator_array)){
                                        $db->setQuery(" SELECT          asteriskcdrdb.queues.`status` AS 'status',
                                                                        users.work_activities_id
                                                        FROM            users
                                                        LEFT JOIN       asterisk_extension ON asterisk_extension.id = users.extension_id
                                                        LEFT JOIN       asteriskcdrdb.queues ON asteriskcdrdb.queues.member = asterisk_extension.number
                                                        WHERE           users.id = $operator_array[$i]");
                                        $status = $db->getResultArray();

                                        $activity = $status["result"][0]["work_activities_id"];
                                        $status = $status['result'][0]['status'];
                                        if($status != 'busy' && $activity == 1){
                                                $db->setQuery("UPDATE outgoing_campaign_request_base SET operator_id = '$operator_array[$i]' WHERE id='$person[id]'");
                                                $db->execQuery();
                                        }
                                        
                                        $i++;
                                }
                                else{
                                        $i = 0;
                                        if($operator_array[$i] != ''){
                                                $db->setQuery(" SELECT          asteriskcdrdb.queues.`status` AS 'status',
                                                                                users.work_activities_id
                                                                FROM            users
                                                                LEFT JOIN       asterisk_extension ON asterisk_extension.id = users.extension_id
                                                                LEFT JOIN       asteriskcdrdb.queues ON asteriskcdrdb.queues.member = asterisk_extension.number
                                                                WHERE           users.id = $operator_array[$i]");
                                                $status = $db->getResultArray();

                                                $activity = $status["result"][0]["work_activities_id"];
                                                $status = $status['result'][0]['status'];
                                                if($status != 'busy' && $activity == 1){
                                                        $db->setQuery("UPDATE outgoing_campaign_request_base SET operator_id = '$operator_array[$i]' WHERE id='$person[id]'");
                                                        $db->execQuery();
                                                }
                                        }
                                        
                                        $i++;
                                }   
                        }
                        
                }
                
                
        }
        
        // კამპანიის ნომრების ატვირთვის პერიოდის შემოწმება
        $db->setQuery(" SELECT  p.missed_upload_time, 
                                SEC_TO_TIME(TIME_TO_SEC(p.missed_upload_time) + 600) AS `upload_interval`
                        FROM    `outgoing_campaign_settings_parameters` AS `p`
                        JOIN    `outgoing_campaign_request` AS `req` ON `req`.outgoing_campaign_settings_parameters_id = p.id AND req.id = 1
                        WHERE req.outgoing_campaign_active_status_id IN(1,2)");

        $upload_time_check = $db->getResultArray();


        if($db->getNumRow() > 0){


                $upload_time = $upload_time_check[result][0][missed_upload_time];
                $upload_interval = $upload_time_check[result][0][upload_interval];

                if($upload_time_check[count] == 0 || !$upload_time){
                        $upload_time = "00:00:00"; 
                }

                if(date('H:i:s') > $upload_time && date('H:i:s') < $upload_interval){

                        $db->setQuery(" SELECT          asterisk_call_log.source
                        FROM            asterisk_call_log
                        LEFT JOIN       asterisk_call_log AS `ast_call_log` ON asterisk_call_log.source = ast_call_log.source
                                        AND ast_call_log.call_datetime > asterisk_call_log.call_datetime AND ast_call_log.call_status_id IN(6,7,8)
                                        AND ast_call_log.call_type_id = 1
                        WHERE asterisk_call_log.call_status_id = 11
                                        AND   asterisk_call_log.call_type_id = 1
                                        AND   asterisk_call_log.wait_time > 9
                                        AND   DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= DATE_SUB(CURDATE(), INTERVAL 1 DAY)
                                        AND   ISNULL(ast_call_log.id)
                                        AND   asterisk_call_log.source 
                                        NOT IN( SELECT outgoing_campaign_request_base.phone_number 
                                                FROM   outgoing_campaign_request_base
                                                JOIN   outgoing_campaign_request_details ON outgoing_campaign_request_details.id = outgoing_campaign_request_base.outgoing_campaign_request_details_id AND outgoing_campaign_request_base.actived = 1
                                                AND outgoing_campaign_request_details.actived = 1
                                                AND DATE(outgoing_campaign_request_details.datetime) >= DATE_SUB(CURDATE(), INTERVAL 1 DAY))");

                        $missed_res = $db->getResultArray();
                        if( $missed_res[count] > 0){
                                $db->setQuery("INSERT 
                                                INTO `outgoing_campaign_request_details`
                                                        (`outgoing_campaign_request_id`, `datetime`, `comment`)
                                                VALUES 
                                                        (1, NOW(), 'ავტომატურად ატვირთული')");
                                $db->execQuery();
                                $d_ID = $db->getLastId();

                                $q = '';
                                foreach($missed_res[result] as $r){
                                        $q .= "(1, '$d_ID', '$r[source]', 1, 3, 1),";
                                }
                                $h = rtrim($q, ',');
                                
                                $db->setQuery("INSERT 
                                                INTO `outgoing_campaign_request_base`
                                                        (`outgoing_campaign_request_id`, `outgoing_campaign_request_details_id`, `phone_number`, `tree_id`, `sub_tree_id`, `outgoing_campaign_request_status_id`)
                                                VALUES
                                                        $h");
                                $db->execQuery();
                        }

                }
}

?>