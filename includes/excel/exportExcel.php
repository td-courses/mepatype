<?php
require_once 'PHPExcel/IOFactory.php';

$pattern = $_POST['pattern'];
$patternArr = json_decode($pattern);

$data = $_POST['sData'];
$dataArr = json_decode($data);

$columnNames = $_POST['sColumnNames'];
$columnNamesArr = json_decode($columnNames);

$columns = $_POST['columns'];


// CUSTOM
$details = $_POST['sDetails'];
$detailsArr = json_decode($details,true);
//die($detailsArr);

if($data && !$details && !$pattern){

    $ExcelObject = new PHPExcel();
    $ExcelObject->setActiveSheetIndex(0);
  
    foreach ($columnNamesArr as $f => $row) {
        $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($f, 1, $row);
    }

    $excel_row = 2;
    foreach ($dataArr as $row) {
        $cleanRows1 = preg_grep("/<p onclick/", $row, PREG_GREP_INVERT);
        $cleanRows2 = preg_grep("/<p class/", $cleanRows1, PREG_GREP_INVERT);
        //$cleanRows3 = preg_grep("/<div /", $cleanRows2, PREG_GREP_INVERT);
        $cleanRows4 = preg_grep("/<button class/", $cleanRows2, PREG_GREP_INVERT);

        
        for($i = 0; $i < $columns; $i++) {
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($i, $excel_row, strip_tags($cleanRows4[$i]));
        }
        $excel_row++;
    }

    $object_Writer = PHPExcel_IOFactory::createWriter($ExcelObject, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Export_data.xls"');
    return $object_Writer->save('php://output');

}elseif($details && !$pattern){

    // var_dump($detailsArr);

    $ExcelObject = new PHPExcel();
    $ExcelObject->setActiveSheetIndex(0);
    $excel_row = 2;
    
    foreach ($detailsArr as $row) {
        for($i = 0; $i < $columns; $i++) {
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($i, $excel_row, $row[$i]);
        }

        //  CUSTOM DETAILS
        $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 1, $excel_row, $row[11]->inc_name);
       
        if($row[11]->category_name_1){
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 2, $excel_row, $row[11]->category_name_1);
        }else{
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 2, $excel_row, '');

        }
        if($row[11]->category_name_1_1){

            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 3, $excel_row, $row[11]->category_name_1_1);

        }else{
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 3, $excel_row, '');

        }
        if($row[11]->category_name_1_1_1 !== null){

            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 4, $excel_row, $row[11]->category_name_1_1_1);
        }else{
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 4, $excel_row, '');

        }
        if($row[11]->region !== null){

            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 5, $excel_row, $row[11]->region);
        }else{
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 5, $excel_row, '');

        }

        if($row[11]->city !== null){

            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 6, $excel_row, $row[11]->city);
        }else{
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($columns + 6, $excel_row, '');

        }
// // //// DETAILS END

        $excel_row++;
    }

  

    $object_Writer = PHPExcel_IOFactory::createWriter($ExcelObject, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Export_data.xls"');
    return $object_Writer->save('php://output');

}elseif($pattern){

    $ExcelObject = new PHPExcel();
    $ExcelObject->setActiveSheetIndex(0);
  
    $patternCount = count($patternArr,COUNT_NORMAL);

    $ArrKey = array_search("#", $patternArr);
    $ArrKey1 = array_search("ID", $patternArr);
    
    if($ArrKey1 !== false && $ArrKey !== false){
           unset($patternArr[$ArrKey]);
           unset($patternArr[$ArrKey1]);

           $ArrKey2 = array_search("ID", $patternArr);
            if($ArrKey2 !== false ){
             unset($patternArr[$ArrKey2]);

             foreach ($patternArr as $f => $row) {
                $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($f - 2, 1, $row);
            }
            }else{
                foreach ($patternArr as $f => $row) {
                    $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($f - 1, 1, $row);
                }
            }
       }else{

        foreach ($patternArr as $f => $row) {
            $ExcelObject->getActiveSheet()->setCellValueByColumnAndRow($f, 1, $row);
        }
        
       }

  
    $object_Writer = PHPExcel_IOFactory::createWriter($ExcelObject, 'Excel5');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="DataTemplate.xls"');
    return $object_Writer->save('php://output');

}else{
    print "Data cant found";
}
