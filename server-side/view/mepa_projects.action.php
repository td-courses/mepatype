<?php
require_once '../../includes/excel/PHPExcel/IOFactory.php';
include('../../includes/classes/class.Mysqli.php');

global $db;
$db = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
$user_id    = $_SESSION['USERID'];
switch ($action) {
    case 'get_add_page':
		$page		= GetPage();
        $data		= array('page'	=> $page);

		break;
	case 'get_docs_list':
		$id = $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 	id,
								name,
								`datetime`,
								CONCAT('<a target=\"blank\" style=\"color:blue;\" href=',link,'>',link,'</a>') AS 'link',
								CONCAT('<a target=\"blank\" style=\"color:blue;\" href=\"\media/uploads/documents/',file,'\">ნახვა</a>') AS 'act'
						FROM 	mepa_project_docs
						WHERE 	cat_id='$id' AND actived='1'");
		$result = $db->getKendoList($columnCount,$cols);

        $data = $result;
	break;
	case 'get_sub_cat':
        $data['page'] = get_sub_cat($_REQUEST['category_id']);

	break;
	case 'get_main_docs':
		$id = $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 	mepa_project_docs.id,
								cat2.id,
								mepa_project_docs.file_original,
								CONCAT('<a target=\"blank\" style=\"color:blue;\" href=',mepa_project_docs.link,'>',mepa_project_docs.link,'</a>') AS 'link',
								mepa_project_docs.datetime,
								info_category.name AS `dep_name`,
								cat2.name
								

						FROM mepa_project_docs
						LEFT JOIN info_category AS cat2 ON cat2.id = mepa_project_docs.cat_id
						LEFT JOIN info_category AS cat1 ON cat1.parent_id = cat2.id
						LEFT JOIN info_category ON info_category.id = cat2.parent_id
						WHERE mepa_project_docs.actived = 1
						GROUP BY mepa_project_docs.id");
		$result = $db->getKendoList($columnCount,$cols);

        $data = $result;
	break;
	case 'get_main_contacts':
		$id = $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 		mepa_project_contacts.id,
								cat2.id,
								CONCAT(mepa_project_contacts.firstname,' ',mepa_project_contacts.lastname),
								mepa_project_contacts.phone,
								mepa_project_contacts.email,
								mepa_project_contacts.position,
								info_category.name AS `dep_name`,
								cat2.name

						FROM 			mepa_project_contacts
						LEFT JOIN info_category AS cat2 ON cat2.id = mepa_project_contacts.cat_id
						LEFT JOIN info_category AS cat1 ON cat1.parent_id = cat2.id
						LEFT JOIN info_category ON info_category.id = cat2.parent_id
						WHERE 		mepa_project_contacts.actived = 1
						GROUP BY mepa_project_contacts.id");
		$result = $db->getKendoList($columnCount,$cols);

        $data = $result;
	break;
	case 'get_input_list':
		$id 			= $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 		mepa_project_inputs.id,
									mepa_project_inputs.name,
									processing_field_type.name,
									mepa_project_inputs.position
						FROM 		mepa_project_inputs
						LEFT JOIN 	processing_field_type ON processing_field_type.id = mepa_project_inputs.field_type_id
						WHERE 		mepa_project_inputs.cat_id='$id' AND mepa_project_inputs.actived='1'");
		$result = $db->getKendoList($columnCount,$cols);

        $data = $result;
	break;
	case 'save_input':
        $id            = $_REQUEST['input_id'];
        $field_id      = $_REQUEST['field_id'];
        $name          = $_REQUEST['input_name'];
        $type          = $_REQUEST['input_type'];
        $position      = $_REQUEST['input_pos'];
        $key           = $_REQUEST['key'];
        $depth         = $_REQUEST['multilevel_deep'];
        $name_1        = $_REQUEST['name_1'];
        $name_2        = $_REQUEST['name_2'];
        $name_3        = $_REQUEST['name_3'];
        $nec           = $_REQUEST['nec'];
		$input_tab     = $_REQUEST['input_tab'];
		$cat_id		   = $_REQUEST['cat_id'];
        if($nec == 'true'){
            $nec = 1;
        }
        else{
            $nec = 0;
        }
		if($id != ''){

            $db->setQuery(" SELECT COUNT(*) AS 'cc'
                            FROM mepa_project_inputs
                            WHERE id != '$id' AND `name` = '$name' AND cat_id = '$cat_id' AND actived=1");
            $key_count = $db->getResultArray();
            if($key_count['result'][0]['cc'] == 0){
                $db->setQuery("UPDATE mepa_project_inputs SET name = '$name', necessary_input='$nec', `field_type_id` = '$type', position='$position',`key`='$key' WHERE id='$id'");
                $res = $db->execQuery();
                if($res){
                    $data['response'] = 'suc';
                }
            }
            else{
                $data['response'] = 'not_uniq';
            }
            
        }
        else{
            if($type == 11 or $type == 12){
                $key = rand(100,9999).rand(1234,8975);
            }
            $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                            FROM    mepa_project_inputs
                            WHERE   `key` = '$key' AND cat_id = '$cat_id' AND actived = 1");
            $key_count = $db->getResultArray();
            $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                            FROM    mepa_project_inputs
                            WHERE   cat_id = '$cat_id' AND position = '$position'");
            $count = $db->getResultArray();
            if($count['result'][0]['cc'] > 0){
                if($key_count['result'][0]['cc'] == 0){
                    $db->setQuery("UPDATE mepa_project_inputs SET position = position + 1 WHERE cat_id = '$cat_id' AND position >= '$position' AND actived = 1 ORDER BY position DESC");
                    $db->execQuery();
                    $key = $key . '___' .rand(100,999);
                    $db->setQuery("INSERT INTO mepa_project_inputs 
                                            (`user_id`,`datetime`,`name`,`cat_id`,`field_type_id`,`position`,`key`,`necessary_input`) VALUES ('$user_id',NOW(),'$name','$cat_id','$type','$position','$key','$nec')");
                    $db->execQuery();
    
                    $data['response'] = 'suc';
                }
                else{
                    $data['response'] = 'not_uniq';
                }
                
            }
            else{
                
                if($key_count['result'][0]['cc'] == 0){
                    $key = $key . '___' .rand(100,999);
                    $db->setQuery("INSERT INTO mepa_project_inputs 
                                            (`user_id`,`datetime`,`name`,`cat_id`,`field_type_id`,`position`,`key`,`necessary_input`) VALUES ('$user_id',NOW(),'$name','$cat_id','$type','$position','$key','$nec')");
                    $db->execQuery();
                    $data['response'] = 'suc';
                }
                else{
                    $data['response'] = 'not_uniq';
                }
                
            }
        }

    break;
	case 'input_editor':
        $id         = $_REQUEST['input_id'];
        $cat_id   	= $_REQUEST['cat_id'];
		$page		= GetInputsEditor($id,$cat_id);
		$data		= array('page'	=> $page);

    break;
	case 'get_contacts_list':
		$id = $_REQUEST['id'];
		$columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
		$db->setQuery("	SELECT 	id,
								CONCAT(firstname,' ',lastname),
								position,
								phone,
								email
						FROM mepa_project_contacts
						WHERE cat_id='$id' AND actived='1'");
		$result = $db->getKendoList($columnCount,$cols);

        $data = $result;
	break;
	case 'add_docs':
		$page		= GetDocsPage();
		$data		= array('page'	=> $page);

		break;

	case 'add_contact':
		$id 		= $_REQUEST['id'];
		$page		= GetContactPage(getContact($id));
		$data		= array('page'	=> $page);

		break;
	case 'save_document':
		$name 			= $_REQUEST['name'];
		$link 			= $_REQUEST['link'];
		$file_original 	= $_REQUEST['file_original'];
		$file_new 		= $_REQUEST['file_new'];
		$cat_id 		= $_REQUEST['cat_id'];

		$db->setQuery("INSERT INTO `mepa_project_docs`(`datetime`,`user_id`,`name`,`link`,`file_original`,`file`,`cat_id`) VALUES(NOW(),'$user_id','$name','$link','$file_original','$file_new','$cat_id')");
		$db->execQuery();

		$data['responce'] = 'suc';
	break;
	case 'save_contact':
		$firstname 	   = $_REQUEST['firstname'];
		$lastname 	   = $_REQUEST['lastname'];
		$contact_pos   = $_REQUEST['contact_pos'];
		$phone 		   = $_REQUEST['phone'];
		$contact_email = $_REQUEST['contact_email'];
		$cat_id 	   = $_REQUEST['cat_id'];
		$contact_id    = $_REQUEST['contact_id'];

		if($contact_id == ''){
			$db->setQuery("INSERT INTO `mepa_project_contacts`(`datetime`,`user_id`,`firstname`,`lastname`,`position`,`phone`,`email`,`cat_id`) 
														VALUES(NOW(),'$user_id','$firstname','$lastname','$contact_pos','$phone','$contact_email','$cat_id')");
			$db->execQuery();
			$data['responce'] = 'suc';
		}
		else{
			$db->setQuery("UPDATE mepa_project_contacts SET firstname='$firstname',
															lastname='$lastname',
															position='$contact_pos',
															phone='$phone',
															email='$contact_email'
														WHERE id='$contact_id'");
			$db->execQuery();
			$data['responce'] = 'suc';						
		}
		

		
	break;
	case 'get_edit_page':
		$cat_id		= $_REQUEST['id'];
		$page		= GetPage(GetCategory($cat_id));

		$data		= array('page'	=> $page);

		break;
	case 'get_edit_docs':
		$cat_id		= $_REQUEST['id'];
		$page		= GetDocPage(GetCategory($cat_id));

		$data		= array('page'	=> $page);

		break;
	case 'get_edit_contacts':
		$cat_id		= $_REQUEST['id'];
		$page		= GetContactsPage(GetCategory($cat_id));

		$data		= array('page'	=> $page);

		break;
	case 'save_category':
		$cat_id 		      = $_REQUEST['id'];
		$category_value	      = $_REQUEST['category_value'];
		$category_select	  = $_REQUEST['category_select'];
		$sub_category_1_value = $_REQUEST['sub_category_1_value'];
		$sub_category_1_select= $_REQUEST['sub_category_1_select'];
		$sub_category_2_value = $_REQUEST['sub_category_2_value'];
		
		///

		$send_sms 			= $_REQUEST["send_sms"];
		$sms_text 			= $_REQUEST["sms_text"];
		$task_form 			= $_REQUEST["task_form"];
		$department 		= $_REQUEST["department"];
		$user 				= $_REQUEST["user"];
		$operator_task 		= $_REQUEST["operator_task"];
		$task_type          = $_REQUEST["task_type"];
		$send_email 	    = $_REQUEST["send_email"];
		$email_address      = $_REQUEST["email_address"];
		$email_text         = $_REQUEST["email_address"];

		$region_selector    = $_REQUEST["region_selector"];

		

		if($send_sms == "1"){
			$send_sms = 1;
		}
		else {
			$send_sms = 0;
		}
		if($task_form == "1"){
			$task_form = 1;
		}
		else {
			$task_form = 0;
		}
		if($operator_task == "1"){
			$operator_task = 1;
		}
		else {
			$operator_task = 0;
		}
		if($send_email == "1"){
			$send_email = 1;
		}
		else {
			$send_email = 0;
		}

		if($region_selector == 'true'){
			$region = 1;
		}
		else{
			$region = 0;
		}


		///

// 		if($cat_name != '' && $cat_id == ''){
//     		if(!CheckCategoryExist($cat_name, $par_id)){
//     		    AddCategory($cat_name, $par_id);
//     		} else {
//     			$error = '"' . $cat_name . '" უკვე არის სიაში!';
//     		}
// 		}else{
//	        SaveCategory($cat_id, $cat_name, $par_id);
// 		}
		if($cat_id == ''){
			AddCategory($category_value, $category_select, $sub_category_1_value,  $sub_category_1_select, $sub_category_2_value,$send_sms,$sms_text,$task_form,$department,$user,$operator_task,$task_type,$email_address,$email_text,$send_email,$region);
		}else{
			SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value,$send_sms,$sms_text,$task_form,$department,$user,$operator_task,$task_type,$email_address,$email_text,$send_email,$region);
		}
		break;
	case 'get_list':
		$multilevel_table = 'info_category';

		$kendo = $_REQUEST['kendo_list'];

		if($kendo == 1){
			$columnCount    = $_REQUEST['count'];
			$cols[]         = $_REQUEST['cols'];
			$page_id        = $_REQUEST['page_id'];
			$db->setQuery("SELECT  	`cat2`.id,	`info_category`.`name`,
									`cat1`.`name`,
									`cat2`.`name`, IF(`cat2`.task = 1,'კი','არა'), user_info.`name`, IF(`cat2`.operators_task = 1,'კი','არა'), IF(`cat2`.send_sms = 1,'კი','არა'), `cat2`.sms_text, 
									IF( `cat2`.send_email=1,'კი','არა'), `cat2`.email_address, `cat2`.email_text
							FROM 	`info_category`
							LEFT JOIN `info_category` AS `cat1` ON `cat1`.`parent_id` = `info_category`.`id`
							LEFT JOIN `info_category` AS `cat2` ON `cat2`.`parent_id` = `cat1`.`id`
							left join department ON department.id = `cat2`.department_id
							left join user_info ON user_info.user_id = `cat2`.users_id
							WHERE 	 		`info_category`.`parent_id` = 0 AND `info_category`.actived=1 AND cat1.actived = 1");
			$result = $db->getKendoList($columnCount,$cols);

			$data = $result;
		}
		else{
			$data['page'] = '';
		$db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM $multilevel_table WHERE actived = 1 AND parent_id = 0");
		
		$result = $db->getResultArray();
		foreach ($result[result] AS $cat_res){
			$data['page'] .= '<li>'.$cat_res[name];
			$db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM $multilevel_table WHERE actived = 1 AND parent_id = '$cat_res[id]'");
			$result_sub = $db->getResultArray();
			$ul_close = 0;
			$i=0;
			foreach ($result_sub[result] AS $cat1_res){
				$i++;
				if($i==1){
					$data['page'] .= '<ul>';
				}
				$data['page'] .= '<li>'.$cat1_res[name];
				$db->setQuery("SELECT id,CONCAT(`name`,'<img my_id=',id,' style=\"margin-left: 10px;margin-right: 10px;\" class=\"cat_img cat_edit\" alt=\"img\" src=\"media/images/icons/edit.svg\"><img my_id=',id,' class=\"cat_img cat_delete\" alt=\"img\" src=\"media/images/icons/delete.svg\">') AS `name` FROM $multilevel_table WHERE actived = 1 AND parent_id = '$cat1_res[id]'");
				$result_sub_sub = $db->getResultArray();
				$ul_close1 = 0;
				$i1=0;
				foreach ($result_sub_sub[result] AS $cat2_res){
					$i1++;
					if($i1==1){
						$data['page'] .= '<ul>';
					}
					$data['page'] .= '<li>'.$cat2_res[name].'</li>';
					$ul_close1 = 1;
				}
				if($ul_close1==1){
					$data['page'] .= '</ul>';
				}
				$data['page'] .= '</li>';
				$ul_close = 1;
			}
			if($ul_close==1){
				$data['page'] .= '</ul>';
			}
			$data['page'] .= '</li>';
			}
		}
		

        

        break;
	
	case 'disable':
		$id = $_REQUEST['id'];
		$type = $_REQUEST['type'];

		if($type == 'docs'){
			$db->setQuery("UPDATE mepa_project_docs SET actived = 0 WHERE id = '$id'");
		}
		else if($type == 'contacts'){
			$db->setQuery("UPDATE mepa_project_contacts SET actived = 0 WHERE id = '$id'");
		}
		else if($type == 'inputs'){
			$db->setQuery("UPDATE mepa_project_inputs SET actived = 0 WHERE id = '$id'");
		}
		else{
			DisableCategory($id);
		}
		$db->execQuery();
	break;
		case 'get_columns':
    
			$columnCount = 		$_REQUEST['count'];
			$cols[] =           $_REQUEST['cols'];
			$columnNames[] = 	$_REQUEST['names'];
			$operators[] = 		$_REQUEST['operators'];
			$selectors[] = 		$_REQUEST['selectors'];
			//$query = "SHOW COLUMNS FROM $tableName";
			//$db->setQuery($query,$tableName);
			//$res = $db->getResultArray();
			$f=0;
			foreach($cols[0] AS $col)
			{
				$column = explode(':',$col);
	
				$res[$f]['Field'] = $column[0];
				$res[$f]['type'] = $column[1];
				$f++;
			}
			$i = 0;
			$columns = array();
			$types = array();
			foreach($res AS $item)
			{
				$columns[$i] = $item['Field'];
				$types[$i] = $item['type'];
				$i++;
			}
			
			
			$dat = array();
			$a = 0;
			for($j = 0;$j<$columnCount;$j++)
			{
				if(1==2)
				{
					continue;
				}
				else{
					
					if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
					$op = false;
					if($columns[$j] == 'id')
					{
						$width = "5%";
					}
					else if($columns[$j] == 'position'){
						$width = "12%";
					}
					else{
						$width = 'auto';
					}
					if($columns[$j] == 'inc_id')
					{
						$hidden = true;
					}
					else if($columns[$j] == 'docs_project_id')
					{
						$hidden = true;
					}
					else if($columns[$j] == 'id')
					{
						$hidden = true;
					}
					else{
						$hidden = false;
					}
					if($res['data_type'][$j] == 'date')
					{
						$g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
					}
					else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
					{
						$g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
					}
					else
					{
						$g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
					}
					$a++;
				}
				array_push($dat,$g);
				
			}
			
			//array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
			
			$new_data = array();
			//{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
			for($j=0;$j<$columnCount;$j++)
			{
				if($types[$j] == 'date')
				{
					$new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
				}
				else if($types[$j] == 'number'){
	
					$new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
				}
				else
				{
					$new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
				}
			}
			
			$filtArr = array('fields'=>$new_data);
			
			
			
			$kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
	
			
			//$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
			
			$data = $kendoData;
			//$data = '[{"gg":"sd","ads":"213123"}]';
			
		break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/
function GetContactPage($res = ''){
	$data = '
	<div id="dialog-form">
		<fieldset>
			<legend>კონტაქტი</legend>
			<table class="dialog-form-table" id="signature_dialog_form_main_table">
                
                <tr id="input_namer">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">სახელი</label></td>
                    <td style="width: 300px;">
                        <input type="text" style="width:200px;" id="firstname" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$res[firstname].'" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">გვარი</label></td>
                    <td>
						<input type="text" style="width:200px;" id="lastname" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$res[lastname].'" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">თანამდებობა</label></td>
                    <td>
						<input type="text" style="width:200px;" id="contact_pos" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$res[position].'" />
                    </td>
				</tr>
				<tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ტელეფონი</label></td>
                    <td>
						<input type="text" style="width:200px;" id="phone" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$res[phone].'" />
                    </td>
				</tr>
				<tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">E-mail</label></td>
                    <td>
						<input type="text" style="width:200px;" id="contact_email" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="'.$res[email].'" />
                    </td>
                </tr>
			</table>
			<input type="hidden" id="contact_id" value="'.$res[id].'">
		</fieldset>
	</div>';
    return $data;
}
function GetDocsPage(){
    $data = '
	<div id="dialog-form">
		<fieldset>
			<legend>დოკუმენტი</legend>
			<table class="dialog-form-table" id="signature_dialog_form_main_table">
                
                <tr id="input_namer">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" style="width:200px;" id="doc_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ლინკი</label></td>
                    <td>
						<input type="text" style="width:200px;" id="doc_link" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ფაილი</label></td>
                    <td>
						<input type="file" style="width:200px;" id="doc_file" class="idle"/>
                    </td>
                </tr>
			</table>
			<input type="hidden" id="file_orig">
			<input type="hidden" id="file">

		</fieldset>
	</div>';
    return $data;
}
function GetContactsPage($res = '')
{

	if($res['send_sms'] == 1)
        $sms_check = 'checked';
        
    if($res['send_email'] == 1)
		$email_check = 'checked';

	if($res['task'] == 1)
		$task_check = 'checked';
	
	if($res['operators_task'] == 1)
		$operators_task_check = 'checked';

    $hd = '';
    $ch1 = '';
    $ch2 = '';
    $hdd1 = '';
    $hdd2 = '';
    $hdd3 = '';
    $hdd4 = '';
    $hdd5 = '';
    $hdd6 = '';
    $hdd7 = '';
    $category_value         = $res['name'];
    $sub_category_value     = $res['id'];
    $category_select        = $res['parent'];
    $sub_category_1_select  = $res['grand_parent'];
    $sub_category_1_value   = $res['name'];
    $sub_category_2_value   = $res['name'];
    $hidden_class = 'style="display:none;"';
    if($res['id'] == ''){
        $hd = $hidden_class;
    }else{
        if($category_select == 0){
            $category_select = 0;
            $sub_category_1_select = 0;
            $sub_category_1_value = '';
            $sub_category_2_value = '';
            $hdd1 = '';
            $hdd2 = $hidden_class;
            $hdd3 = $hidden_class;
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = $hidden_class;
            $ch1 = '';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select == 0){
            $category_value = '';
            $sub_category_2_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = '';
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = '';
            $ch1 = 'checked';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select != 0){
            $category_select = $res['grand_parent'];
            $sub_category_value = $res['parent'];
            $category_value = '';
            $sub_category_1_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = $hidden_class;
            $hdd4 = '';
            $hdd5 = '';
            $hdd6 = '';
            $hdd7 = '';
            $ch1 = 'checked disabled';
            $ch2 = 'checked';
        }

    }

    $data = '
	<div id="dialog-form">
		<ul class="dialog_input_tabs cat_tabs">
			<li data-id="1">ფუქნციონალი</li>
			<li aria-selected="true" data-id="4">კონტაქტები</li>
		</ul>
	    <fieldset class="fields_cat" id="field_1" style="display:none;">
			<legend>ფუნქციონალი</legend>
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="category">უწყება</label></td>
					<td>
						<input disabled '.$hdd1.' type="text" id="category_value" class="idls large" data-index="'. $res['id'] .'" value="' . $category_value . '" />
						<select disabled '.$hd.' '.$hdd2.' id="category_select" class="idls large">' . Category($category_select)  . '</select>
					</td>
				    <td style="width: 170px;"><input disabled '.$ch1.' style="margin-left: 15px!important;margin-top: 4px!important;" type="checkbox" id="show_sub_category_1_value" /></td>
				</tr>
				<tr id="first_tr" '.$hd.' '.$hdd7.'>
					<td style="width: 170px;"><label for="sub_category_1">პროექტი</label></td>
					<td>
						<input disabled '.$hdd3.' type="text" id="sub_category_1_value" class="idls large" value="' . $sub_category_1_value . '" />
						<select disabled '.$hdd4.' id="sub_category_1_select" class="idls large">' . Category1($category_select, $sub_category_value)  . '</select>
					</td>
				    <td style="width: 170px;"><input disabled '.$ch2.' style="margin-left: 15px!important;margin-top: 4px!important;" type="checkbox" id="show_sub_category_2_value" /></td>
				</tr>
			</table>
			
		</fieldset>
		<fieldset class="fields_cat" id="field_3" style="display:none;">
			<legend>დოკუმენტაცია</legend>
	    	
			<div id="kendo_docs"></div>
		</fieldset>
		<fieldset class="fields_cat" id="field_4">
			<legend>კონტაქტები</legend>
	    	
			<div id="kendo_contacts"></div>
		</fieldset>
		<input type="hidden" id="cat_id" value="' . $res['id'] . '" />
    </div>
    ';
    return $data;
}
function GetDocPage($res = '')
{

	if($res['send_sms'] == 1)
        $sms_check = 'checked';
        
    if($res['send_email'] == 1)
		$email_check = 'checked';

	if($res['task'] == 1)
		$task_check = 'checked';
	
	if($res['operators_task'] == 1)
		$operators_task_check = 'checked';

    $hd = '';
    $ch1 = '';
    $ch2 = '';
    $hdd1 = '';
    $hdd2 = '';
    $hdd3 = '';
    $hdd4 = '';
    $hdd5 = '';
    $hdd6 = '';
    $hdd7 = '';
    $category_value         = $res['name'];
    $sub_category_value     = $res['id'];
    $category_select        = $res['parent'];
    $sub_category_1_select  = $res['grand_parent'];
    $sub_category_1_value   = $res['name'];
    $sub_category_2_value   = $res['name'];
    $hidden_class = 'style="display:none;"';
    if($res['id'] == ''){
        $hd = $hidden_class;
    }else{
        if($category_select == 0){
            $category_select = 0;
            $sub_category_1_select = 0;
            $sub_category_1_value = '';
            $sub_category_2_value = '';
            $hdd1 = '';
            $hdd2 = $hidden_class;
            $hdd3 = $hidden_class;
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = $hidden_class;
            $ch1 = '';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select == 0){
            $category_value = '';
            $sub_category_2_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = '';
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
            $hdd7 = '';
            $ch1 = 'checked';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select != 0){
            $category_select = $res['grand_parent'];
            $sub_category_value = $res['parent'];
            $category_value = '';
            $sub_category_1_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = $hidden_class;
            $hdd4 = '';
            $hdd5 = '';
            $hdd6 = '';
            $hdd7 = '';
            $ch1 = 'checked disabled';
            $ch2 = 'checked';
        }

    }

    $data = '
	<div id="dialog-form">
		<ul class="dialog_input_tabs cat_tabs">
			<li data-id="1">ფუქნციონალი</li>
			<li aria-selected="true" data-id="3">დოკუმენტაცია</li>
		</ul>
	    <fieldset class="fields_cat" id="field_1" style="display:none;">
			<legend>ფუნქციონალი</legend>
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="category">უწყება</label></td>
					<td>
						<input disabled '.$hdd1.' type="text" id="category_value" class="idls large" data-index="'. $res['id'] .'" value="' . $category_value . '" />
						<select disabled '.$hd.' '.$hdd2.' id="category_select" class="idls large">' . Category($category_select)  . '</select>
					</td>
				    <td style="width: 170px;"><input disabled '.$ch1.' style="margin-left: 15px!important;margin-top: 4px!important;" type="checkbox" id="show_sub_category_1_value" /></td>
				</tr>
				<tr id="first_tr" '.$hd.' '.$hdd7.'>
					<td style="width: 170px;"><label for="sub_category_1">პროექტი</label></td>
					<td>
						<input disabled '.$hdd3.' type="text" id="sub_category_1_value" class="idls large" value="' . $sub_category_1_value . '" />
						<select disabled '.$hdd4.' id="sub_category_1_select" class="idls large">' . Category1($category_select, $sub_category_value)  . '</select>
					</td>
				    <td style="width: 170px;"><input disabled '.$ch2.' style="margin-left: 15px!important;margin-top: 4px!important;" type="checkbox" id="show_sub_category_2_value" /></td>
				</tr>
			</table>
			
		</fieldset>
		<fieldset class="fields_cat" id="field_3">
			<legend>დოკუმენტაცია</legend>
	    	
			<div id="kendo_docs"></div>
		</fieldset>
		<fieldset class="fields_cat" id="field_4" style="display:none;">
			<legend>კონტაქტები</legend>
	    	
			<div id="kendo_contacts"></div>
		</fieldset>
		<input type="hidden" id="cat_id" value="' . $res['id'] . '" />
    </div>
    ';
    return $data;
}
function GetPage($res = '')
{
	GLOBAL $db;
	if($res['send_sms'] == 1)
        $sms_check = 'checked';
        
    if($res['send_email'] == 1)
		$email_check = 'checked';

	if($res['task'] == 1)
		$task_check = 'checked';
	
	if($res['operators_task'] == 1)
		$operators_task_check = 'checked';
	
	
	$sub_category_1_select  = $res['grand_parent'];

	$db->setQuery("	SELECT 	region_selector
					FROM 	info_category
					WHERE 	id='$res[parent]'");
	$region_checker = $db->getResultArray();

	if($region_checker['result'][0]['region_selector'] == 1){
		$region_check = 'checked';
	}
	else{
		$db->setQuery("	SELECT 	region_selector
					FROM 	info_category
					WHERE 	id='$res[id]'");
		$region_checker = $db->getResultArray();
		if($region_checker['result'][0]['region_selector'] == 1){
			$region_check = 'checked';
		}
	}
		


    $hd = '';
    $ch1 = '';
    $ch2 = '';
    $hdd1 = '';
    $hdd2 = '';
    $hdd3 = '';
    $hdd4 = '';
    $hdd5 = '';
    $hdd6 = '';
    $hdd7 = '';
    $category_value         = $res['name'];
    $sub_category_value     = $res['id'];
    $category_select        = $res['parent'];
    
    $sub_category_1_value   = $res['name'];
    $sub_category_2_value   = $res['name'];
    $hidden_class = 'style="display:none;"';
    if($res['id'] == ''){
        $hd = $hidden_class;
    }else{
        if($category_select == 0){
            $category_select = 0;
            $sub_category_1_select = 0;
            $sub_category_1_value = '';
            $sub_category_2_value = '';
            $hdd1 = '';
            $hdd2 = $hidden_class;
            $hdd3 = $hidden_class;
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
			$hdd7 = $hidden_class;
			$hdd8 = $hidden_class;
            $ch1 = '';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select == 0){
            $category_value = '';
            $sub_category_2_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = '';
            $hdd4 = $hidden_class;
            $hdd5 = $hidden_class;
            $hdd6 = $hidden_class;
			$hdd7 = '';
			$hdd8 = '';
            $ch1 = 'checked';
            $ch2 = '';
        }else if($category_select != 0 && $sub_category_1_select != 0){
            $category_select = $res['grand_parent'];
            $sub_category_value = $res['parent'];
            $category_value = '';
            $sub_category_1_value = '';
            $hdd1 = $hidden_class;
            $hdd2 = '';
            $hdd3 = $hidden_class;
            $hdd4 = '';
            $hdd5 = '';
            $hdd6 = '';
			$hdd7 = '';
			$hdd8 = '';
            $ch1 = 'checked disabled';
            $ch2 = 'checked';
        }

    }

    $data = '
	<div id="dialog-form">
		<ul class="dialog_input_tabs cat_tabs" '.$hdd8.' '.$hd.'>
			<li aria-selected="true" data-id="1">ფუქნციონალი</li>
			<li data-id="2">დამ.ველები</li>
			<li data-id="3">დოკუმენტაცია</li>
			<li data-id="4">კონტაქტები</li>
		</ul>
	    <fieldset class="fields_cat" id="field_1">
			<legend>ფუნქციონალი</legend>
	    	<table class="dialog-form-table">
				<tr>
					<td style="width: 170px;"><label for="category">უწყება</label></td>
					<td>
						<input '.$hdd1.' type="text" id="category_value" class="idls large" data-index="'. $res['id'] .'" value="' . $category_value . '" />
						<select '.$hd.' '.$hdd2.' id="category_select" class="idls large">' . Category($category_select)  . '</select>
					</td>
				    <td style="width: 170px;"><input '.$ch1.' style="margin-left: 15px!important;margin-top: 4px!important;" type="checkbox" id="show_sub_category_1_value" /></td>
				</tr>
				<tr id="first_tr" '.$hd.' '.$hdd7.'>
					<td style="width: 170px;"><label for="sub_category_1">პროექტი</label></td>
					<td>
						<input '.$hdd3.' type="text" id="sub_category_1_value" class="idls large" value="' . $sub_category_1_value . '" />
						<select '.$hdd4.' id="sub_category_1_select" class="idls large">' . Category1($category_select, $sub_category_value)  . '</select>
					</td>
				    <td style="width: 170px;"><input '.$ch2.' style="margin-left: 15px!important;margin-top: 4px!important;" type="checkbox" id="show_sub_category_2_value" /></td>
				</tr>
				<tr id="second_tr" '.$hd.' '.$hdd5.'>
					<td style="width: 170px;"><label for="sub_category_2">ქვე პროექტი</label></td>
					<td>
					    <input '.$hdd6.' type="text" id="sub_category_2_value" class="idls large" value="' . $sub_category_2_value . '" />
					</td>
				</tr>
				<tr '.$hdd8.'>
					<td><span style="margin-top: 0px;"> რეგიონების სელექტორის ჩართვა </span></td>
					<td><input '.$region_check.' id="region_selector" style="margin-top: 2px!important;" type="checkbox" name="" value = "1" ></td>
				</tr>
			</table>
			<div id="description" '.$hdd5.' '.$hd.' >
				<h5 style="margin-left:140px;font-weight:bold;">აღწერა<h5>
				<div style="display:flex;flex-direction: column;margin-left: 140px;">

					<div style="display:flex;justify-content: space-between;width: 200px;"><input id="task_form" '.$task_check.' type="checkbox" name="" value = 1 >	<span style="margin-top: -5px;"> ფორმირდება დავალება </span></div>
					<div id="group_div" style="margin-top: 10px;margin-bottom: 14px;">	
							<div   style="display:flex;justify-content: space-between;width: 310px;    margin-bottom: 3px;">
								<span style="margin-top: 5px;">დავალების ტიპი</span>
								
								<select id="tasktype" style="width:172px;    border: solid 1px #85b1de;" data-select="chosen">
										'.gettasktype($res['task_type']).'
								</select>
							</div>
							<div   style="display:flex;justify-content: space-between;width: 310px;    margin-bottom: 3px;">
								<span style="margin-top: 5px;">ჯგუფი</span>
								
								<select id="department" style="width:172px;    border: solid 1px #85b1de;" data-select="chosen">
										'.getgroup($res['department_id']).'
								</select>
							</div>
							<div style="display:flex;justify-content: space-between;width: 310px;    margin-bottom: 3px;">
								<span style="margin-top: 5px;">მომხმარებელი</span>
									
								<select id="user" style="width:172px;    border: solid 1px #85b1de;" data-select="chosen">
								'.getusers($res['users_id']).'
								</select>
							</div>
							
						<div style="display:flex;justify-content: space-between;width: 310px;margin-bottom: 8px;"><input id="operator_task" style="margin-top: 2px!important;" '.$operators_task_check.' type="checkbox" name="" user_id="'.$_SESSION['USERID'].'" value = "1" >	<span style="margin-top: 0px;"> საკუთარ თავზე დაფორმირება </span></div>
						
						<div style="display:flex;justify-content: space-between;width: 310px;margin-bottom: 3px;">
							<span style="margin-top: 6px;">სხვა ნომერი</span>
							
							<input id="task_phone_dictation" type="text" style="width: 172px;border: solid 1px #85b1de!important;" value="" autocomplete="off">
						</div>
					</div>

					<div style="display:flex;justify-content: space-between;width: 133px;"><input id="send_sms" '.$sms_check.' type="checkbox" name="" value = 1 >	<span style="margin-top: -5px;"> იგზავნება SMS</span></div>
					<div id="sms_text_par" style="display:none;justify-content: space-between;width: 310px;margin-bottom: 3px;">
						<textarea id="sms_text" style="height: 0px;opacity:0;width:100%;margin-top:10px;padding: 5px;" placeholder="ტექსტი ...">'.$res['sms_text'].'</textarea>
					</div>
					<div style="display:flex;justify-content: space-between;width: 133px;margin-top: 22px;"><input id="send_email" '.$email_check.' type="checkbox" name="" value = 1 >	<span style="margin-top: -5px;"> იგზავნება Email</span></div>
					<div id="email_text_wrapper" style="height: 0px;opacity:0;" >
						<div style="display:flex;justify-content: space-between;width: 310px;margin-top: 10px;">
							<span style="margin-top: 6px;">მისამართი</span>
							
							<input id="email_address" type="text" style="width: 172px;border: solid 1px #85b1de!important;" value = "'.$res['email_address'].'" />
						</div>
						<div style="display:flex;justify-content: space-between;width: 310px; margin-top:10px;">
							<textarea id="email_text" style="height: 80px; width:100%;padding: 5px;" placeholder="ტექსტი ...">'.$res['email_text'].'</textarea>
                        </div>
                    </div>
                </div>
			</div>
			
		</fieldset>
		<fieldset class="fields_cat" id="field_2" style="display:none;">
			<legend>დამ.ველები</legend>
			<div id="fieldset_inputs"></div>
		</fieldset>
		<fieldset class="fields_cat" id="field_3" style="display:none;">
			<legend>დოკუმენტაცია</legend>
	    	
			<div id="kendo_docs"></div>
		</fieldset>
		<fieldset class="fields_cat" id="field_4" style="display:none;">
			<legend>კონტაქტები</legend>
	    	
			<div id="kendo_contacts"></div>
		</fieldset>
		<input type="hidden" id="cat_id" value="' . $res['id'] . '" />
    </div>
    ';
    return $data;
}
function Category($id)
{
	GLOBAL $db;
    $data = '';
	$db->setQuery("	SELECT `id`, `name`
					FROM `info_category`
					WHERE actived = 1 AND parent_id = 0");
	$result = $db->getResultArray();

    $data .= '<option value="0" selected="selected">----</option>';
    foreach($result['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}
function Category1($grand_parent, $parent)
{
	GLOBAL $db;
    $data = '';
	$db->setQuery("	SELECT `id`, `name`
					FROM `info_category`
					WHERE actived = 1 AND parent_id = '$grand_parent'");
	$result = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($result['result'] AS $res){
        if($res['id'] == $parent){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}
function gettasktype($id){
	GLOBAL $db;
    $data = '';
	$db->setQuery("	SELECT `id`, `name`
					FROM `task_type` 
					WHERE actived = 1");
	$result = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($result['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
	}
	return $data;
}
function getgroup($id) {
	GLOBAL $db;
	$data = '';
	$db->setQuery("	SELECT `id`, `name`
					FROM `department`
					WHERE actived = 1");
	$result = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($result['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
	}
	return $data;
}
function getusers($id) {
	GLOBAL $db;
	$data = '';
	$db->setQuery("	SELECT `user_id` AS id, `name`
					FROM `user_info`");
	$result = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($result['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
	}
	return $data;
}
function GetCategory($cat_id)
{
	GLOBAL $db;
	$db->setQuery("	SELECT `id`,
						`name`,
						`parent_id` as parent,
						(SELECT `parent_id` FROM `info_category` WHERE `id` = parent) as grand_parent,
						`send_sms`,
						`sms_text`,
						`task`,
						`task_type`,
						`department_id`,
						`users_id`,
						`operators_task`,
						`send_email`,
						`email_address`,
						`email_text`,
						`region_selector`
					FROM   `info_category`
					WHERE  `id` = '$cat_id'");
	$result = $db->getResultArray();

    return $result['result'][0];
}
function AddCategory($category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value){
    global $db;
    $user_id = $_SESSION['USERID'];
    if($category_select == 0){
        $db->setQuery("INSERT INTO `info_category`
                                  (`user_id`, `name`, `parent_id`)
                            VALUES
                                  ('$user_id', '$category_value', '0')");
        $db->execQuery();
    }else if($sub_category_1_select == 0){
        $db->setQuery("INSERT INTO `info_category`
                                  (`user_id`, `name`, `parent_id`)
                            VALUES
                                  ('$user_id', '$sub_category_1_value', '$category_select')");
        
        $db->execQuery();
    }else{
        $db->setQuery("INSERT INTO `info_category`
                                  (`user_id`, `name`, `parent_id`)
                            VALUES
                                  ('$user_id', '$sub_category_2_value', '$sub_category_1_select')");
        
        $db->execQuery();
    }

}
function SaveCategory($cat_id, $category_value, $category_select, $sub_category_1_value, $sub_category_1_select, $sub_category_2_value,$send_sms,$sms_text,$task_form,$department,$user,$operator_task,$task_type,$email_address,$email_text,$send_email,$region)
{
	GLOBAL $db;
    $user_id	= $_SESSION['USERID'];
    $data = array("exists" => false);

    if($category_value != ""){

      //$data_count = mysql_num_rows(mysql_query("SELECT id FROM info_category WHERE name = '$category_value' AND parent_id=0"));

		$db->setQuery("	UPDATE 	`info_category` SET
								`user_id` = '$user_id',
								`name` = '$category_value',
								`parent_id`	= 0
						WHERE  `id` = $cat_id");
		$db->execQuery();

    }else if($sub_category_1_value != ""){

      //$data_count = mysql_num_rows(mysql_query("SELECT id FROM info_category WHERE name = '$sub_category_1_value' AND parent_id='$category_select'"));
      	$db->setQuery("	UPDATE 	`info_category` SET
								`user_id` = '$user_id',
								`name` = '$sub_category_1_value',
								`parent_id`	= $category_select,
								`region_selector` = $region
						WHERE  `id` = $cat_id");
		$db->execQuery();

    }else{

      //$data_count = mysql_num_rows(mysql_query("SELECT id FROM info_category WHERE name = '$sub_category_2_value' AND parent_id='$sub_category_1_select'"));
      	$db->setQuery("	UPDATE 	`info_category` SET
								`user_id` = '$user_id',
								`name` = '$sub_category_2_value',
								`parent_id`	= $sub_category_1_select,
								`send_sms` = '$send_sms',
								`sms_text` = '$sms_text',
								`task`		= '$task_form',
								`department_id` = '$department',
								`users_id` = '$user',
								`operators_task` = '$operator_task',
								`task_type`     = '$task_type',
								`email_address` = '$email_address',
								`email_text`    = '$email_text',
								`send_email`    = '$send_email'
						WHERE  `id` = $cat_id");
		$db->execQuery();

		$db->setQuery("	UPDATE 	`info_category` SET
								`region_selector` = '$region'
								
						WHERE  `id` = $sub_category_1_select");
		$db->execQuery();
    }

}
function DisableCategory($cat_id){
    global $db;
    
    $db->setQuery("UPDATE `info_category`
				      SET `actived` = 0
				   WHERE  `id` = $cat_id");
    
    $db->execQuery();
}
function getContact($id){
	GLOBAL $db;
	$db->setQuery("	SELECT	id,
							firstname,
							lastname,
							position,
							phone,
							email
					FROM 	mepa_project_contacts
					WHERE 	id='$id'");
	$result = $db->getResultArray();

	return $result['result'][0];
}
function get_input_types($id){
    global $db;
    $data = '';
    $db->setQuery("SELECT 	`id`, `name`
					FROM 	`processing_field_type`
					WHERE 	actived=1 AND id NOT IN (6,7,8,9,10)");
    
    
    $data = $db->getSelect($id);
    return $data;
}
function GetInputsEditor($id,$cat_id){
    global $db;
    
    $db->setQuery("SELECT name,field_type_id,position,`key`,depth,multilevel_1,multilevel_2,multilevel_3,necessary_input FROM mepa_project_inputs WHERE id='$id'");
    $result = $db->getResultArray();
    $vars = $result['result'][0];

    if($vars['field_type_id'] == 6 or $vars['field_type_id'] == 7 or $vars['field_type_id'] == 8 or $vars['field_type_id'] == 9){
        $displaySelectorsKendo = 'block;';
    }
    else{
        $displaySelectorsKendo = 'none;';
    }
    if($vars['necessary_input'] == 1){
        $necessary = 'checked';
    }

    if($vars['field_type_id'] == 10){
        $displayMultiselect = 'contents;';
        $displayLevels = 'block;';
    }
    else{
        $displayMultiselect = 'none;';
        $displayLevels = 'none;';
    }


    if($vars['depth'] == 2){
        $selected2 = 'selected';
    }
    else if($vars['depth'] == 3){
        $selected3 = 'selected';
    }

    if($selected2 == 'selected' OR $selected3 == 'selected'){
        $displayTree = "block";
    }

    
    if($vars['position'] == ''){
        $db->setQuery(" SELECT 	IFNULL(MAX(position)+1,1) AS 'position'
                        FROM 	mepa_project_inputs
                        WHERE	actived = 1 AND cat_id = '$cat_id'");
        $input_pos = $db->getResultArray();

        $input_pos = $input_pos['result'][0]['position'];
    }
    else{
        $input_pos = $vars['position'];
    }
    $data  .= '
    <div id="dialog-form">
        <fieldset>
            <legend>ველის პარამეტრები</legend>
            
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                
                <tr id="input_namer">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ველის დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" style="width:200px;" id="input_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $vars['name'] . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ველის ტიპი</label></td>
                    <td>
                        <select  id="input_type" style="width: 200px; height: 28px; ">'.get_input_types($vars['field_type_id']).'</select>
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">აუცილებელი ველი</label></td>
                    <td>
                        <input style="margin-top: 3px!important;" type="checkbox" id="necessary" '.$necessary.'>
                    </td>
                   
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">პოზიცია</label></td>
                    <td style="width: 300px;">
                        <input style="width:20px;" type="text" id="input_position" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $input_pos . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr style="display:none;">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">id-class</label></td>
                    <td style="width: 300px;">
                        <input type="text" id="input_key" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $vars['key'] . '" />
                    </td>
                </tr>
                
            </table>
            <!-- ID -->
            <input type="hidden" id="input_id" value="' . $id . '" />
            <input type="hidden" id="cat_id" value="' . $cat_id . '" />
        </fieldset>
		
    </div>';

	return $data;
}
function get_sub_cat($id){
    global $db;
    
    $data = '';
    $db->setQuery("SELECT `id`, 
                          `name`
				   FROM   `info_category`
				   WHERE actived = 1 AND parent_id = '$id'");

    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}
?>
