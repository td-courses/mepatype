import Table from "./components/table/table.class.js"
import Selector from "./components/selector/selector.class.js"
import MultiSelector from "./components/multiselector/multiselector.class.js"
import DateTime from "./components/datetime/datetime.class.js"
import Input from "./components/input/input.class.js"
import Helper from "./helpers/helper.class.js"

export default class Tdg extends Helper {

    constructor(prop){
        super()

        self.Tdg = this
        this.state = {
            prop: prop
        }


    }
    
/**
 * @description SET STATE
 * @param {Object} param 
 */
    setState = (param = null) => {
        
        if(param != null || param != undefined || param != '' || param != 'object') this.state = Object.assign(this.state, param)

        if(typeof param === "object"){
            $.each(param, (f, v) => {
                if(typeof v === "function") return v()
            })
        }
        
        window.state = this.state;
    }

    updateState = (param = null) => {
        if(param != null || param != undefined || param != '') this.state = Object.assign(this.state, param)
    }

    removeLoading = () => {
        $("loading").fadeOut()
    }


    prototypes = {
        callback: (cb) => {
            console.log(cb)
        }
    }


    /**
     * ADD COMPONENT IN DOM
     */
    append = (to, what, prop = {}, callback) => {

        this.append.__proto__ = this.prototypes

        

        if(typeof to != 'undefined' && typeof what != 'string') {
            
            if(typeof what == 'object' && typeof what.length == 'undefined') to.append(what)

            if(what.length > 0){
                
                what.map((i, x) => to.append(x))
            }

        }

        if(typeof to != 'undefined' && typeof what == 'string') $(to).append(what)


        if(prop.setTimeout){
            setTimeout(function(){
                what.remove()
            }, prop.setTimeout)
        }

        let type = (typeof what != 'undefined' && typeof what != 'string' && typeof what.length == 'undefined' ? what.getAttribute("type") : null);
    

        switch(type){
            case "table":
                new Table(what, prop.template, callback).init();
            break;
            case "selector":
                new Selector(what, callback).init();
            break;
            case "multiselector":
                new MultiSelector(what).init();
            break;
            case "datetime":
                new DateTime(what).init();
            break;
            case "date":
                new DateTime(what).init("date");
            break;
            case "time":
                new DateTime(what).init("time");
            break;
        }



    }


}


const callback = () => {

    console.log("AEE")

}
