<?php
error_reporting(E_ALL);
class curl{

    private $bankingURL;
    private $postData;

    public function curl($bankingURL, $postData){

        $this->bankingURL   = $bankingURL;
        $this->postData     = $postData;

    }

    public function getData(){
        
        $Curl            = curl_init();
        $xml_post_string = $this->postData;
        $headers         = array('Content-Type: text/xml;charset=UTF-8',
                                 'Connection: Keep-Alive',
                                 'SOAPAction: "http://tempuri.org/IService1/SendSMS"',
                                 'User-Agent: Apache-HttpClient/4.5.5 (Java/12.0.1)');

        $url             = $this->bankingURL;

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml_post_string,
            CURLOPT_HTTPHEADER => $headers
        );

        curl_setopt_array( $Curl, $options );
        $Result = curl_exec($Curl);

        curl_close($Curl);

        $arr = array('soap-env:', 'xmlns:', 'ns2:');
        $Result = str_ireplace($arr, '', $Result);
        $xml    = simplexml_load_string($Result);
        $json   = json_encode($xml);
        $resArray  = json_decode($json,TRUE);

        return $Result;
    }
    
}

?>