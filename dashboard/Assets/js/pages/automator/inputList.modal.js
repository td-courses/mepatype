import Tdg from "../../tdg.class.js";


export default class inputListModal extends Tdg {

    constructor(prop){
        super()

        self.inputListModal = this;
        
        this.state = {
            dom: this.CreateElement({
                element: "interface",
                attributes: ["inputList"],
                style: {
                    display: 'block',
                    padding: '10px'
                }
            }),
            interface: [],
            element: [],
            inputs: [],
            id: prop.id
        }


    }

    // INITIALIZE BUILD
    init = () => {

        this.build()
        return this.state.dom

    }

    /**
     * BUILD INCOMMING COMPONENTS
     */
    // 
    build = async () => {

        if(typeof this.state.id != 'undefined'){
            this.state.data = await this.getResponse({
                route: "Automator",
                act: "getfieldsetName",
                fieldsetID: this.state.id
            })
        }

        this.state.element.divSeparatedDemoView = this.CreateElement({
            element: "div",
            style: {
                display: "grid",
                "grid-auto-flow": "column dense",
                "grid-template-columns": "1fr 1fr",
                "gap": "10px"
            }
        });

        this.append(this.state.dom, this.state.element.divSeparatedDemoView);

        /* this.CreateElement({
            element: "fieldset",
            children: this.CreateElement({
                element: "legend",
                text: "ძირითადი ინფორმაცია"
            })
        }) */

        this.append(this.state.element.divSeparatedDemoView, this.inputWorkingArea(this.state.id));
        this.append(this.state.element.divSeparatedDemoView, this.inputDemoArea());

    }


    inputWorkingArea = () => {
        
        this.state.element.workingArea = this.CreateElement({
            element: "div",
            name: "workingArea",
            style: {
                display: "grid",
                gap: "10px"
            }
        })
        
        this.state.element.addedInputsFieldset = this.CreateElement({
            element: "fieldset",
            children: this.CreateElement({
                element: "legend",
                text: "დამატებული ველები"
            })
        })

        this.state.element.mainInfoFieldset = this.CreateElement({
            element: "fieldset",
            children: this.CreateElement({
                element: "legend",
                text: "ძირითადი ინფორმაცია"
            })
        })

        this.buildFieldnameAndKendo(this.state.id);


        this.append(this.state.element.workingArea, this.state.element.mainInfoFieldset)
        this.append(this.state.element.workingArea, this.state.element.addedInputsFieldset)

        return this.state.element.workingArea;

    }

    inputDemoArea = () => {
        this.state.element.demoArea = this.CreateElement({
            element: "div",
            name: "demoArea"
        })

        this.state.element.demoViewFieldset = this.CreateElement({
            element: "fieldset",
            children: this.CreateElement({
                element: "legend",
                text: "DEMO VIEW"
            })
        })


        this.append(this.state.element.demoArea, this.state.element.demoViewFieldset)

        return this.state.element.demoArea;
    }

    createInput = (x) => {
      
        /* this.state.inputs[Object.keys(x)[0]] = this.CreateElement({
            element: "input",
            title: x.title,
            style: {
                width: "270px"
            },
            placeholder: x.title,
            value: x[Object.keys(x)[0]]
        })

        return this.state.inputs[Object.keys(x)[0]] */


        this.state.inputs.fieldsetNameInput = this.CreateElement({
            element: "input",
            id: "fieldsetName",
            title: "Fieldset დასახელება",
            style: {
                width: "270px"
            },
            placeholder: "Fieldset დასახელება",
            value: x.fieldsetName
        })

        return this.state.inputs.fieldsetNameInput
    }

    buildFieldnameAndKendo = async () => {

        this.state.data.length > 0 && this.state.data.forEach(x => {
            this.append(this.state.element.mainInfoFieldset, this.createInput(x))
        })

        this.append(this.state.element.addedInputsFieldset, this.createTableInputsByFieldset())


    }



    createTableInputsByFieldset = () => {

        this.state.element.InputsByFieldsetTable = this.CreateElement({
            element: "kendo",
            type: "table",
            column: [
                {
                    field: "id",
                    size: 60,
                    hidden: false
                    
                },
                {
                    field: "ველი"
                },
                {
                    field: "ტიპი"
                },
                {
                    field: "ტაბი"
                },
                {
                    field: "პოზიცია"
                }
            ],
            data: {
                route: "Automator",
                act: "getInputList",
                fieldsetID: this.state.id
            },
            option: {
                header: false,
                numeric: false,
                footer: true
            },
            ondblclick: (cb) => {
               


            },
            callback: (cb) => {
                // console.log(cb)
            }
        });
        return this.state.element.InputsByFieldsetTable;

    }



}