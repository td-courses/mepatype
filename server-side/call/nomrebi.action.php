<?php
include('../../includes/classes/class.Mysqli.php');
require_once('../../includes/classes/dialog_automator.class.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
global $db;
$db = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
switch ($action) {
    case 'get_add_page':
        $page		= GetPage();
        $data		= array('page'	=> $page);
    break;
    case 'get_columns':
        $columnCount = 		$_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] = 	$_REQUEST['names'];
        $operators[] = 		$_REQUEST['operators'];
        $selectors[] = 		$_REQUEST['selectors'];
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
            {
                continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if($columns[$j] == 'id')
                {
                    $width = "5%";
                }
                else if($columns[$j] == 'shin_status'){
                    $width = "12%";
                }
                else{
                    $width = 'auto';
                }
                if($columns[$j] == 'inc_id')
                {
                    $hidden = true;
                }
                else if($columns[$j] == 'id' OR $columns[$j] == 'req_id'){
                    $hidden = true;
                }
                else{
                    $hidden = false;
                }
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        $new_data = array();
        for($j=0;$j<$columnCount;$j++)
        {
            if($types[$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else if($types[$j] == 'number'){

                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        $filtArr = array('fields'=>$new_data);
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
        $data = $kendoData;
    break;
    case 'get_list' :
        $id          =      $_REQUEST['hidden'];
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'], true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $out_type       = $_REQUEST['filter_all_calls'];
		$operator	    = $_REQUEST['filter_operator'];
		$start        	= $_REQUEST['start'] . ' 00:00:00';
        $end         	= $_REQUEST['end'] . ' 23:59:59';
        $parent         = $_REQUEST['parent'];
        $child          = $_REQUEST['child'];
		$user	      	= $_SESSION['USERID'];
            $db->setQuery(" SELECT  `incomming_call`.`id`,
                                    `incomming_call`.`date`,
                                    '' AS test,
                                    `source`.`name_ka` AS communication_type,
                                    `incomming_call`.`phone`
                            FROM `incomming_call`
                            LEFT JOIN `source` ON `source`.`id`=`incomming_call`.`source_id`
                            WHERE `incomming_call`.`date` BETWEEN '$start' AND '$end' ");
        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;
	break;
    case 'get_list_2' :
        $id          =      $_REQUEST['hidden'];
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'], true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $out_type       = $_REQUEST['filter_all_calls'];
		$operator	    = $_REQUEST['filter_operator'];
        $parent         = $_REQUEST['parent'];
        $child          = $_REQUEST['child'];
		$user	      	= $_SESSION['USERID'];
        $phone          = $_REQUEST['phone'];
        $startDate      = $_REQUEST['start'];
        $endDate        = $_REQUEST['end'];
            $db->setQuery(" SELECT id,date,phone 
                            FROM incomming_call
                            WHERE phone = '$phone' AND DATE(`date`) BETWEEN '$startDate' AND '$endDate' ");
        $result = $db->getKendoList($columnCount,$cols);
        //$data = array('data'=>$arr,'total'=>$rowCount['result'][0]['cc']);
        //$data = var_dump($result);
        $data = $result;
	break;
    case 'get_processing': 
        $phone     = $_REQUEST['phone'];
        $type   = $_REQUEST['type'];
        $dialog = new automator();
        if($type == 'outgoing'){
            $callT = 'outgoing';
            $page = $dialog->getDialog($id,$phone,'incomming_request_processing','incomming_request_id');
        }
        else{
            $callT = 'autodialer';
            $page = $dialog->getDialog($id,$phone,'compaign_request_processing','base_id');
        }
        $pre_page = 'asdasd';
        $data		= array('page'	=> $pre_page.$page.$after_page);
    break;
    case 'get_edit_page':
        $phone = $_REQUEST['phone'];
        $type   = $_REQUEST['type'];
        $id = $_REQUEST['id'];
        $project = $_REQUEST['project'];
        $date=$_REQUEST['date'];
        $end_date=date("Y-m-d h:i:sa");
        $page = '<div id="hiddenphone" style="display:none" >'.$phone.'</div><div id="datetime"><span style="margin-left:20px;" >დრო</span><input type = "text" value="'.$date.'" hasDatePicker id = "start_date_1" class="datetimes"/> -დან<input type = "text" id = "end_date_1" class="datetimes"  value="'.$end_date.'" hasDatePicker"/> -მდე<button id="loadtable_2" style="margin-left:20px;">ფილტრი</button></div>
        <div id="sub_kendo"> </div>';
        $data = array('page'=> $page );
    break;
    default:
        $error = 'Action is Null';
}

echo json_encode($data);
?>