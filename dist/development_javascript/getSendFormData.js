const postPostBtn = document.querySelector('.postPostBtn')
const pollPostBtn = document.querySelector('.pollPostBtn')
const eventPostBtn = document.querySelector('.eventPostBtn')


const formSelect = document.querySelector('#formSelect')



const imageUploadBtns = document.querySelectorAll('.imageUpoadBtn');
imageUploadBtns.forEach(btn => {
    btn.addEventListener('click', () => {
        let originalUploadBtn = document.querySelector('#image_upload');
        originalUploadBtn.click();
    })
})

const youtubeBtns = document.querySelectorAll('.youtubeBtn');
youtubeBtns.forEach(btn => {
    btn.addEventListener('click', (e) => {
        let originalYoutubeInp = document.querySelector('#youtube_url');
        originalYoutubeInp.classList.toggle('display-block');
    })
})





if (postPostBtn) {
    postPostBtn.addEventListener('click', (e) => {

        if ($('.emoji_input').val() == "") {
            alert("გთხოვთ შეავსოთ აღწერის ველი მაინც")
            document.querySelector('.popup-window').classList.add('display-flex');
        } else {
            document.querySelector('.popup-window').classList.remove('display-flex');
            let postType = formSelect.value;
            let taggedUserArray = document.querySelectorAll('li[data-typeoftag="user"]');
            let taggedGroupArray = document.querySelectorAll('li[data-typeoftag="group"]');
            let taggedDepartmentArray = document.querySelectorAll('li[data-typeoftag="department"]');
            let taggedUserIdArray = [];
            let taggedGroupIdArray = [];
            let taggedDepartmentIdArray = [];

            taggedUserArray.forEach(userTag => {
                taggedUserIdArray.push(userTag.dataset.tagedid)
            })


            taggedGroupArray.forEach(groupTag => {
                taggedGroupIdArray.push(groupTag.dataset.tagedid)
            })


            taggedDepartmentArray.forEach(departmentTag => {
                taggedDepartmentIdArray.push(departmentTag.dataset.tagedid)
            })


            let desciprtionText = document.querySelector('.post_form__description textarea').value;
            let rowUrl = document.querySelector('#youtube_url').value
            let youtubeUrl
            if (rowUrl == "") {
                youtubeUrl = '';
            } else {
                youtubeUrl = rowUrl.split('=')[1].split('&t')[0];
            }

            // https://www.youtube.com/watch?v=aTF7oChs3WY

            let imageUploadBtn = document.getElementById('image_upload')
            let imageName = imageUploadBtn.dataset.imagename




            let post = {
                "type": postType,
                "description": desciprtionText,
                "question": "",
                "answers": {
                    "type": "sigle",
                    "answers": [
                        {
                            "text": "კი",
                            "votes": 5
                        },
                        {
                            "text": "არა",
                            "votes": 6
                        },
                        {
                            "text": "არ ვიცი",
                            "votes": 7
                        }
                    ]
                },
                "startAndEndDate": {
                    "start": "",
                    "end": ""
                },
                "location": "",
                "post_image_name": imageName,
                "post_youtube_video_url": youtubeUrl,
                "poster_name": "",
                "poster_image_name": "",
                "poster_departments": "",
                "time_after_posted": 0,
                "time_posted": moment().format('hh:mm'),
                "data_posted": moment(new Date()).format("DD-MM-YYYY"),
                "marked_groups": taggedGroupIdArray,
                "marked_users": taggedUserIdArray,
                "marked_departments": taggedDepartmentIdArray,
                "likes": {
                    "sad": 0,
                    "happy": 0,
                    "neutral": 0,
                    "love": 0,
                    "amazed": 0
                },
                "comments": [

                ]
            }


            let lastPostId = document.querySelectorAll('.main__posts')[0].dataset.postid

            let actAndPost = {
                act: "add_posts",
                posts: post,
                lastPostId: lastPostId
            }


            $.ajax({
                url: AjaxURL,
                data: actAndPost,
                success: function (post) {
                    console.log(post);
                    getAllPosts()


                }
            })
        }
    })

}




if (pollPostBtn) {
    pollPostBtn.addEventListener('click', (e) => {
        let answaresAreEmpty = true;
        let allAnswaresText = document.querySelectorAll('.answers ol li input')

        allAnswaresText.forEach(answare => {
            if (answare.value.trim("") == 0) {
                answaresAreEmpty = true;
                return answaresAreEmpty;
            } else {
                answaresAreEmpty = false;
            }
        })


        if (answaresAreEmpty) {
            alert("გთხოვთ შეავსოთ კითხვის და ყველა პასუხის ველი")
            document.querySelector('.popup-window').classList.add('display-flex');
        } else {
            document.querySelector('.popup-window').classList.remove('display-flex');
            let postType = formSelect.value;
            let taggedUserArray = document.querySelectorAll('li[data-typeoftag="user"]');
            let taggedGroupArray = document.querySelectorAll('li[data-typeoftag="group"]');
            let taggedDepartmentArray = document.querySelectorAll('li[data-typeoftag="department"]');
            let taggedUserIdArray = [];
            let taggedGroupIdArray = [];
            let taggedDepartmentIdArray = [];
            let answers;
            let answersObj;
            let answersArray = [];
            let answersType;



            let startDate = document.getElementById('pollStartDate').value;
            let endDate = document.getElementById('pollEndDate').value;

            let checkbox = document.querySelector('.multiOrNotCheck');
            console.log(checkbox.checked);


            let question = document.querySelector('.second_popup__addQuestions__container .question input').value
            answers = document.querySelectorAll('ol li input')


            answers.forEach(answer => {
                answersObj = {
                    text: answer.value,
                    value: 0
                }
                answersArray.push(answersObj)
            })


            taggedUserArray.forEach(userTag => {
                taggedUserIdArray.push(userTag.dataset.tagedid)
            })


            taggedGroupArray.forEach(groupTag => {
                taggedGroupIdArray.push(groupTag.dataset.tagedid)
            })


            taggedDepartmentArray.forEach(departmentTag => {
                taggedDepartmentIdArray.push(departmentTag.dataset.tagedid)
            })


            let desciprtionText = document.querySelector('.poll_form__description textarea').value;
            let rowUrl = document.querySelector('#youtube_url').value
            let youtubeUrl
            if (rowUrl == "") {
                youtubeUrl = '';
            } else {
                youtubeUrl = rowUrl.split('=')[1].split('&t')[0];
            }

            // https://www.youtube.com/watch?v=aTF7oChs3WY

            let imageUploadBtn = document.getElementById('image_upload')
            let imageName = imageUploadBtn.dataset.imagename



            if (checkbox.checked) {
                answersType = "checkbox"
            } else {
                answersType = "radio"
            }


            let post = {
                "type": postType,
                "description": desciprtionText,
                "question": question,
                "answers": {
                    "type": answersType,
                    "answers": answersArray
                },
                "startAndEndDate": {
                    "start": startDate,
                    "end": endDate
                },
                "location": "",
                "post_image_name": imageName,
                "post_youtube_video_url": youtubeUrl,
                "poster_name": "",
                "poster_image_name": "",
                "poster_departments": "",
                "time_after_posted": 0,
                "time_posted": moment().format('hh:mm'),
                "data_posted": moment(new Date()).format("DD-MM-YYYY"),
                "marked_groups": taggedGroupIdArray,
                "marked_users": taggedUserIdArray,
                "marked_departments": taggedDepartmentIdArray,
                "likes": {
                    "sad": 0,
                    "happy": 0,
                    "neutral": 0,
                    "love": 0,
                    "amazed": 0
                },
                "comments": [

                ]
            }


            console.log(post);


            let lastPostId = document.querySelectorAll('.main__posts')[1].dataset.postid

            let actAndPost = {
                act: "add_posts",
                posts: post,
                lastPostId: lastPostId
            }


            $.ajax({
                url: AjaxURL,
                data: actAndPost,
                success: function (post) {
                    console.log(post);
                    getAllPosts()

                }
            })


        }


    })
}



if (eventPostBtn) {
    eventPostBtn.addEventListener('click', (e) => {

        if ($('.emoji_input').val() == "") {
            alert("გთხოვთ შეავსოთ აღწერის ველი მაინც")
            document.querySelector('.popup-window').classList.add('display-flex');
        } else {
            let postType = formSelect.value;
            let taggedUserArray = document.querySelectorAll('li[data-typeoftag="user"]');
            let taggedGroupArray = document.querySelectorAll('li[data-typeoftag="group"]');
            let taggedDepartmentArray = document.querySelectorAll('li[data-typeoftag="department"]');
            let taggedUserIdArray = [];
            let taggedGroupIdArray = [];
            let taggedDepartmentIdArray = [];

            let startDate = document.getElementById('eventStartDate').value;
            let endDate = document.getElementById('eventEndDate').value;


            taggedUserArray.forEach(userTag => {
                taggedUserIdArray.push(userTag.dataset.tagedid)
            })

            taggedGroupArray.forEach(groupTag => {
                taggedGroupIdArray.push(groupTag.dataset.tagedid)
            })

            taggedDepartmentArray.forEach(departmentTag => {
                taggedDepartmentIdArray.push(departmentTag.dataset.tagedid)
            })


            let desciprtionText = document.querySelector('.event_form__description textarea').value;
            let location = document.querySelector('.event_form_locationInp').value;
            let rowUrl = document.querySelector('#youtube_url').value
            let youtubeUrl
            if (rowUrl == "") {
                youtubeUrl = '';
            } else {
                youtubeUrl = rowUrl.split('=')[1].split('&t')[0];
            }

            // https://www.youtube.com/watch?v=aTF7oChs3WY

            let imageUploadBtn = document.getElementById('image_upload')
            let imageName = imageUploadBtn.dataset.imagename




            let post = {
                "type": postType,
                "description": desciprtionText,
                "question": "",
                "answers": {
                    "type": "",
                    "answers": ""
                },
                "startAndEndDate": {
                    "start": startDate,
                    "end": endDate
                },
                "location": location,
                "post_image_name": imageName,
                "post_youtube_video_url": youtubeUrl,
                "poster_name": "",
                "poster_image_name": "",
                "poster_departments": "",
                "time_after_posted": 0,
                "time_posted": moment().format('hh:mm'),
                "data_posted": moment(new Date()).format("DD-MM-YYYY"),
                "marked_groups": taggedGroupIdArray,
                "marked_users": taggedUserIdArray,
                "marked_departments": taggedDepartmentIdArray,
                "likes": {
                    "sad": 0,
                    "happy": 0,
                    "neutral": 0,
                    "love": 0,
                    "amazed": 0
                },
                "comments": [
                ]
            }




            let lastPostId = document.querySelectorAll('.main__posts')[1].dataset.postid

            let actAndPost = {
                act: "add_posts",
                posts: post,
                lastPostId: lastPostId
            }


            $.ajax({
                url: AjaxURL,
                data: actAndPost,
                success: function (post) {
                    console.log(post);
                    getAllPosts()

                }
            })

        }
    })
}









const getAllPosts = () => {

    setTimeout(() => {


        $(function emojiLike() {
            $(".like-btn").hover(function () {
                $(".reaction-icon").each(function (i, e) {

                    $(e).addClass("show");

                });
            }, function () {
                $(".reaction-icon").removeClass("show")
            });
        })
    }, 2000);


    var AjaxURL = "server-side/call/action_action.php";

    let actAndLastPostId = {
        act: "get_posts",
        last_post_id: 10000000000000
    }


    $.ajax({
        url: AjaxURL,
        data: actAndLastPostId,
        dataType: "json",
        success: function (result) {

            const popUpwindow = document.querySelector('.popup-window')
            const mainPostContainer = document.querySelector('.posts')

            popUpwindow.style.display = "none";
            mainPostContainer.innerHTML = "";

            let allPostsContainer = document.querySelector('.posts');
            let user_id = result.user_id;





            result.posts.forEach((post, i) => {
                let markedGroups = "";
                let markedUsers = "";
                let markedDepartments = "";

                let topRatedComment = "";
                let otherComments = "";

                let deleteComentBtn = ""

                let answers = "";
                let postEditButton = "";

                let youtubeEmbed = ''

                let image = ''

                let addCommentsElement = '';


                let seeMoreCommentsBtn = '';



                addCommentsElement = `<div class="post__add_comment">
                                        <div class="post__add_comment__container">
                                            <textarea  name="" id="" cols="30" rows="3"></textarea>
                                            <div class="add_comment_btn" onmouseup="comment(this)" ><svg width="32" height="27" viewBox="0 0 32 27" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M0.985937 26.0247L31.1691 13.0829L0.985937 0.141113L0.971558 10.207L22.5412 13.0829L0.971558 15.9589L0.985937 26.0247Z"
                                                        fill="white" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>`


                if (post.post_youtube_video_url) {
                    youtubeEmbed = `<iframe width="871" height="490" src="https://www.youtube.com/embed/${post.post_youtube_video_url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
                } else {
                    youtubeEmbed = ''
                }

                if (post.post_image_name) {
                    image = `<div class="post__image" onmouseup="viewImage(this)" style="background-image: url('media/uploads/posts/${post.post_image_name}')"></div>`
                } else {
                    image = ''
                }


                if (post.post_youtube_video_url && post.post_image_name) {
                    youtubeEmbed = `<iframe width="871" height="490" src="https://www.youtube.com/embed/${post.post_youtube_video_url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <div class="post__image" onmouseup="viewImage(this)" style="background-image: url('media/uploads/posts/${post.post_image_name}')"></div>`
                    image = ''
                } else {
                    // youtubeEmbed = ''
                }

                if (user_id == post.user_id) {
                    postEditButton = `<div class="post_menu_icon" onmouseup="postDropDown(this)">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path
                                                    d="M12 18c1.657 0 3 1.343 3 3s-1.343 3-3 3-3-1.343-3-3 1.343-3 3-3zm0-9c1.657 0 3 1.343 3 3s-1.343 3-3 3-3-1.343-3-3 1.343-3 3-3zm0-9c1.657 0 3 1.343 3 3s-1.343 3-3 3-3-1.343-3-3 1.343-3 3-3z" />
                                            </svg>
                                        </div>
                                        <ul class="post_menu_icon__dropdown__content navigation_dropdown__content">
                                            <li class="links" id="post_menu_icon__edit" onmouseup="editPost(this)"> <svg xmlns="http://www.w3.org/2000/svg"
                                                    width="24" height="24" viewBox="0 0 24 24">
                                                    <path
                                                        d="M18.363 8.464l1.433 1.431-12.67 12.669-7.125 1.436 1.439-7.127 12.665-12.668 1.431 1.431-12.255 12.224-.726 3.584 3.584-.723 12.224-12.257zm-.056-8.464l-2.815 2.817 5.691 5.692 2.817-2.821-5.693-5.688zm-12.318 18.718l11.313-11.316-.705-.707-11.313 11.314.705.709z" />
                                                </svg>
                                                <p> შესწორება </p>
                                            </li>
                                            <li class="links" id="post_menu_icon__delete" onmouseup="deletePost(this)"> <svg xmlns="http://www.w3.org/2000/svg"
                                                    width="24" height="24" viewBox="0 0 24 24">
                                                    <path
                                                        d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z" />
                                                </svg>
                                                <p> წაშლა </p>
                                            </li>
                                        </ul>`
                } else {
                    postEditButton = ""
                }


                let emojiLikes = `<a class="like-btn"  >
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.0543 7.61363V8.61362H11.0543H16.2123C16.9422 8.61362 17.5594 8.87794 17.9237 9.37694L17.9726 9.44401L18.032 9.50208C18.3447 9.80805 18.5471 10.3914 18.4619 11.1029L17.5069 16.4761L17.5051 16.4859L17.5036 16.4957C17.3374 17.5528 16.4221 18.3506 15.3727 18.3506H6.05664V10.1855C6.08686 10.1093 6.12033 10.0194 6.15533 9.92242C6.24652 9.6699 6.36725 9.31668 6.5056 8.90227C6.78314 8.07097 7.14078 6.96586 7.49342 5.86464C7.84636 4.76252 8.19542 3.66077 8.45618 2.83478C8.58658 2.42172 8.69494 2.07749 8.77071 1.83647L8.85244 1.5763C9.33506 1.6009 9.59686 1.71591 9.73666 1.83805C9.86743 1.95229 10.0543 2.21597 10.0543 2.91885V7.61363ZM8.65525 1.3839C8.51537 1.34005 8.29206 1.27005 7.93553 1.15831L8.65525 1.38389L8.84597 1.44366C8.84596 1.44368 8.84595 1.44369 8.84594 1.4437C8.81312 1.4334 8.7557 1.41539 8.65525 1.3839Z" stroke="#00A57F" stroke-width="2"/>
                                    <path d="M1.25848 18.3506V10.961H1.65756V18.3506H1.25848Z" stroke="#00A57F" stroke-width="2"/>
                                    </svg>
    
                                    <div class="reaction-box">
                                        
                                        <div class="reaction-icon" 
                                             style="background-image: url('media/img/svg/emojis/Group 31.svg');" 
                                             data-liketype="sad"
                                            onclick="likeReaction(this)">
                                            <label>Sad</label>
                                        </div>
                                        <div class="reaction-icon" 
                                             style="background-image: url('media/img/svg/emojis/Group 30.svg');" 
                                             data-liketype="neutral"
                                            onclick="likeReaction(this)">
                                            <label>Neutral</label>
                                        </div>
                                        
                                        <div class="reaction-icon" 
                                             style="background-image: url('media/img/svg/emojis/Group 32.svg');" 
                                             data-liketype="amazed"
                                            onclick="likeReaction(this)">
                                            <label>Wow</label>
                                        </div>
                                        <div class="reaction-icon" 
                                             style="background-image: url('media/img/svg/emojis/Group 34.svg');" 
                                             data-liketype="love"
                                             onclick="likeReaction(this)">
                                            
                                            <label>Love</label>
                                        </div>
                                        <div class="reaction-icon" 
                                             style="background-image: url('media/img/svg/emojis/Group 29.svg');" 
                                             data-liketype="happy"
                                            onclick="likeReaction(this)">
                                            <label>like</label>
                                        </div> 
                                    </div>
                                </a>`



                if (post.answers.type == "checkbox") {

                    post.answers.answers.forEach(answare => {
                        let checked;
                        if (answare.status == 'check') {
                            checked = 'checked'
                        } else {
                            checked = ''
                        }

                        answers += `<div class="choise">
                                            <div class="choise__input">
                                            <label class="checkmark_container">
                                                    <p>${answare.text}</p> 
                                                    <input type="checkbox" ${checked} value="${answare.text}">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <div class="single_pole">
    
                                                    <svg class="pole_stick__filled" width="334" height="4" viewBox="0 0 334 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M0 2H334" stroke="#CECECE" stroke-width="3"></path>
                                                    <path class="filled-bar" data-filled="${answare.votes}" d="M0 2L334 2.00003" stroke="black" stroke-width="3" style="/*! opacity: 0; */"></path>
                                                    </svg>
    
                                                    <p class="polle_procent"></p>
    
                                                </div>
                                            </div>
    
                                        </div>`;

                    });

                }

                if (post.answers.type == "radio") {


                    post.answers.answers.forEach(answare => {
                        let checked;
                        if (answare.status == 'check') {
                            checked = 'checked'
                        } else {
                            checked = ''
                        }

                        answers += `<div class="choise">
                                            <div class="choise__input">
                                            <label class="radio_container checkmark_container"> 
                                                    <input type="radio" name="check" ${checked} value="${answare.text}"> 
                                                    <p>${answare.text}</p>
                                                    <span class="checkmark checkmark__radio"></span>
                                            </label>
                                                <div class="single_pole"> 
    
                                                    <svg class="pole_stick__filled" width="334" height="4" viewBox="0 0 334 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M0 2H334" stroke="#CECECE" stroke-width="3"></path>
                                                        <path class="filled-bar" data-filled="${answare.votes}" d="M0 2L334 2.00003" stroke="black" stroke-width="3" style="/*! opacity: 0; */"></path>
                                                    </svg>
    
                                                    <p class="polle_procent"></p>
    
                                                </div>
                                            </div>
    
                                        </div>`;


                    });

                }



                let uniqueGroupNames = [...new Set(post.marked_groups)]


                for (let i = 0; i < uniqueGroupNames.length; i++) {
                    markedGroups += `<p>${uniqueGroupNames[i]}</p>`

                }

                post.marked_users.forEach(groupName => {
                    markedUsers += `<p>${groupName}</p>`
                })

                post.marked_departments.forEach(groupName => {
                    markedDepartments += `<p>${groupName}</p>`
                })




                if (post.comments == null) {
                    topRatedComment = "";
                    otherComments = "";
                } else {




                    post.comments.forEach(comment => {
                        if (post.comments.length > 1) {
                            seeMoreCommentsBtn = `<p class="post__top-commen__text__see-more" href="">იხილეთ მეტი (${post.comments.length - 1})</p>`
                        } else {
                            seeMoreCommentsBtn = ``
                        }

                        if (user_id == comment.user_id) {
                            deleteComentBtn = `<p class="delete-comment-btn" onmouseup = "deleteComment(this)" > წაშლა </p>`
                        } else {
                            deleteComentBtn = ""
                        }



                        if (comment.isTop) {
                            topRatedComment = ` <div class="post__top-comment"  data-commentid="${comment.comment_id}">
                                                    <div class="post__top-comment__image" style="background-image: url(media/uploads/file/${comment.image_name}); background-size: cover; background-position: center"></div>
                                                    <div class="post__top-commen__text">
                                                        <div class="post__top-commen__text__name">
                                                            <p>${comment.name}</p>
                                                        </div>
                                                        <div class="post__top-commen__text__department">
                                                            <p>${comment.department}</p>
                                                        </div>
                                                        <div class="post__top-commen__text__time">${comment.time_after_posted}</div>
                                                        <div class="post__top-commen__text__date">${comment.data_posted}</div>
                                                        <div class="post__top-commen__text__text">
                                                            <p>${comment.text} </p>
                                                        </div>
                                                        <div class="post__top-commen__text__likes">
                                                            <p data-likedcomment=${comment.like_status} onmouseup = "like(this)" > მოწონება (${comment.likes})  </p>
                                                            ${deleteComentBtn}
                                                        </div>
    
                                                        ${seeMoreCommentsBtn}
    
                                                    </div>
        
                                                </div>`
                        } else {
                            otherComments += `<div class="post__other-comment" data-commentid="${comment.comment_id}"">
                                                <div class="post__other-comment__image" style="background-image: url(media/uploads/file/${comment.image_name}); background-size: cover; background-position: center"></div>
                                                <div class="post__other-comment__text">
                                                    <div class="post__other-comment__text__name">
                                                        <p>${comment.name}</p>
                                                    </div>
                                                    <div class="post__other-comment__text__department">
                                                        <p>${comment.department}</p>
                                                    </div>
                                                    <div class="post__other-comment__text__time">${comment.time_after_posted}</div>
                                                    <div class="post__other-comment__text__date">${comment.data_posted}</div>
                                                    <div class="post__other-comment__text__text">
                                                        <p>${comment.text} </p>
                                                    </div>
                                                    <div class="post__other-comment__text__likes">
                                                        <p data-likedcomment=${comment.like_status} onmouseup="like(this)"> მოწონება (${comment.likes})  </p>
                                                        ${deleteComentBtn}
                                                    </div>
                                                </div>
                                            </div>`
                        }



                    })
                }



                // let topRatedCommentObject = post.comments.reduce((max, p) => p.likes < max ? p.likes : max, post.comments[0]);






                pollTemplate = parser.parseFromString(
                    `<div class="main__posts poll__post" data-posttype=${post.type} data-postid=${post.id} data-liked='${post.like_status}'>
                    <div class="post">
                        <div class="post__poster__image" style="background-image: url('media/uploads/file/${post.poster_image_name}')"></div>
    
                        <div class="post__poster-info">
                            <div class="post__poster-info__text">
                                <div class="post__poster-info__text__name">
                                    <a href="">${post.poster_name}</a>
                                </div>
                                <div class="post__poster-info__text__position">
                                    <p> ${post.poster_departments}  </p>
                                </div>
                                <div class="post__poster-info__text__time-posted">
                                    <p> ${post.time_after_posted}  </p>
                                </div>
                            </div>
                        </div>
    
                        <div class="post__group">
                            <div class="group">
                                <div class="group__icon"></div>
                                <div class="group__arrow"></div>
                                <div class="group__names">
                                ${markedGroups}
                                </div>
                            </div>
            
                            <div class="group markdUsers">
                                <div class="group__icon"></div>
                                <div class="group__arrow"></div>
                                <div class="group__names">
                                    ${markedUsers}
                                </div> 
                            </div>
            
                            <div class="group markedDepartments">
                                <div class="group__icon"></div>
                                <div class="group__arrow"></div>
                                <div class="group__names">
                                ${markedDepartments}
                                </div>
                            </div>
                        </div>
    
    
                        <div class="post__time">
                            <div class="time">
                                <div class="post__time__date">
                                    <p>${post.data_posted}</p>
                                </div>
                                <div class="post__time__time">
                                    <p>${post.time_posted}</p>
                                </div>
                            </div>
                            ${postEditButton}
                        </div>
    
                        <div class="post__poll">
                            <p class="description"> <p class="description">${post.description}</p></p>
                            <div class="post__start-end-date">
                                <p class="post__start-end-date__start"> ${post.startAndEndDate.start}</p>
                                <p class="post__start-end-date__end"> ${post.startAndEndDate.end}</p>
                            </div>
                            ${youtubeEmbed}
                            ${image}
                            <p class="description poll_question">${post.question}</p>
                            <div class="polls__container">
                                ${answers}
                            </div>
                            <button class="popupform__button confirmPollBtn" onclick="confirmPoll(this)">დადასტურება</button>
                        </div>
    
                        <div class="post__smiles" onclick='getUserHowLiked(this)'>
                            <ul>
                                <li>
                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 29.svg');">
                                    </div>
                                    <p data-likeTypeamount = "happy">${post.likes.happy}</p>
                                </li>
                                <li>
                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 31.svg');">
                                    </div>
                                    <p data-likeTypeamount = "sad">${post.likes.sad}</p>
                                </li> 
                                <li>
                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 30.svg');">
                                    </div>
                                    <p data-likeTypeamount = "neutral">${post.likes.neutral}</p>
                                </li>
                                <li>
                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 34.svg');">
                                    </div>
                                    <p data-likeTypeamount = "love">${post.likes.love}</p>
                                </li>
                                <li>
                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 32.svg');">
                                    </div>
                                    <p data-likeTypeamount = "amazed">${post.likes.amazed}</p>
                                </li>
                            </ul>
                        </div>
    
    
                        <div class="post__actions">
                            <ul>
    
                                <li>
                                    <div class="action_icon">
                                        ${emojiLikes}
    
                                    </div>
                                </li>
                                <li>
                                    <div class="action_icon commentCollapseBtn" onmouseup="showCommentInput()" id="commentCollapseBtn">
                                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path class="comment-action-fill"
                                                d="M3.98421 14.8081H3.57636L3.28485 15.0933L1.03712 17.2926V2.29092C1.03712 1.79673 1.45192 1.35989 2.01067 1.35989H17.799C18.3578 1.35989 18.7726 1.79673 18.7726 2.29092V13.877C18.7726 14.3712 18.3578 14.8081 17.799 14.8081H3.98421Z"
                                                stroke="#00A57F" stroke-width="2" />
                                        </svg>
    
                                    </div>
                                </li>
                            </ul>
                        </div>
    
    
                        ${topRatedComment}
    
    
    
                        ${addCommentsElement}
    
                        <div class="post__other-comment__container">
                            ${otherComments} 
                        </div>
                    </div>
    
                </div>
            </div>`,
                    "text/html")




                postTemplate = parser.parseFromString(
                    `<div class="main__posts" data-posttype=${post.type} data-postid=${post.id} data-liked='${post.like_status}'>
                                    <div class="post">
                                        <div class="post__poster__image" style="background-image: url('media/uploads/file/${post.poster_image_name}')"></div>
                            
                                        <div class="post__poster-info">
                                            <div class="post__poster-info__text">
                                                <div class="post__poster-info__text__name">
                                                    <a href="">${post.poster_name}</a>
                                                </div>
                                                <div class="post__poster-info__text__position">
                                                    <p> ${post.poster_departments}  </p>
                                                </div>
                                                <div class="post__poster-info__text__time-posted">
                                                    <p> ${post.time_after_posted}</p>
                                                </div>
                                            </div>
                                        </div>
                            
                                        <div class="post__group">
                                            <div class="group">
                                                <div class="group__icon"></div>
                                                <div class="group__arrow"></div>
                                                <div class="group__names">
                                                ${markedGroups}
                                                </div>
                                            </div>
                            
                                            <div class="group markdUsers">
                                                <div class="group__icon"></div>
                                                <div class="group__arrow"></div>
                                                <div class="group__names">
                                                    ${markedUsers}
                                                </div>
                                            </div>
                            
                                            <div class="group markedDepartments">
                                                <div class="group__icon"></div>
                                                <div class="group__arrow"></div>
                                                <div class="group__names">
                                                ${markedDepartments}
                                                </div>
                                            </div>
                                        </div>
                            
                            
                                        <div class="post__time">
                                            <div class="time">
                                                <div class="post__time__date">
                                                    <p>${post.data_posted}</p>
                                                </div>
                                                <div class="post__time__time">
                                                    <p>${post.time_posted}</p>
                                                </div>
                                            </div>
                                            ${postEditButton}
                                        </div>
                            
                                        <div class="post__text">
                                             <p class="description">${post.description}</p>
                                            ${image}
                                            ${youtubeEmbed}
                                        </div>
                            
                                        <div class="post__smiles" onclick='getUserHowLiked(this)'>
                                            <ul>
                                                <li>
                                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 29.svg');">
                                                    </div>
                                                    <p data-likeTypeamount = "happy">${post.likes.happy}</p>
                                                </li>
                                                <li>
                                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 31.svg');">
                                                    </div>
                                                    <p data-likeTypeamount = "sad">${post.likes.sad}</p>
                                                </li> 
                                                <li>
                                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 30.svg');">
                                                    </div>
                                                    <p data-likeTypeamount = "neutral">${post.likes.neutral}</p>
                                                </li>
                                                <li>
                                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 34.svg');">
                                                    </div>
                                                    <p data-likeTypeamount = "love">${post.likes.love}</p>
                                                </li>
                                                <li>
                                                    <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 32.svg');">
                                                    </div>
                                                    <p data-likeTypeamount = "amazed">${post.likes.amazed}</p>
                                                </li>
                                            </ul>
                                        </div>
                            
                            
                                        <div class="post__actions">
                                            <ul>
                            
                                                <li>
                                                    <div class="action_icon">
                                                    ${emojiLikes}
                            
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="action_icon commentCollapseBtn" onmouseup="showCommentInput()" id="commentCollapseBtn">
                                                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path class="comment-action-fill"
                                                                d="M3.98421 14.8081H3.57636L3.28485 15.0933L1.03712 17.2926V2.29092C1.03712 1.79673 1.45192 1.35989 2.01067 1.35989H17.799C18.3578 1.35989 18.7726 1.79673 18.7726 2.29092V13.877C18.7726 14.3712 18.3578 14.8081 17.799 14.8081H3.98421Z"
                                                                stroke="#00A57F" stroke-width="2" />
                                                        </svg>
                            
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                            
                            
                                        ${topRatedComment}
                            
                            
                            
                                        ${addCommentsElement}
                                        <div class="post__other-comment__container">
                                            ${otherComments}
                                        </div>
                                    </div>
                            
                                </div>`,
                    "text/html")




                eventTemplate = parser.parseFromString(
                    `<div class="main__posts" data-posttype=${post.type} data-postid=${post.id} data-liked='${post.like_status}'>
                                        <div class="post">
                                            <div class="post__poster__image" style="background-image: url('media/uploads/file/${post.poster_image_name}')"></div>
                                
                                            <div class="post__poster-info">
                                                <div class="post__poster-info__text">
                                                    <div class="post__poster-info__text__name">
                                                        <a href="">${post.poster_name}</a>
                                                    </div>
                                                    <div class="post__poster-info__text__position">
                                                        <p> ${post.poster_departments}  </p>
                                                    </div>
                                                    <div class="post__poster-info__text__time-posted">
                                                        <p> ${post.time_after_posted}</p>
                                                    </div>
                                                </div>
                                            </div>
                                
                                            <div class="post__group">
                                                <div class="group">
                                                    <div class="group__icon"></div>
                                                    <div class="group__arrow"></div>
                                                    <div class="group__names">
                                                    ${markedGroups}
                                                    </div>
                                                </div>
                                
                                                <div class="group markdUsers">
                                                    <div class="group__icon"></div>
                                                    <div class="group__arrow"></div>
                                                    <div class="group__names">
                                                        ${markedUsers}
                                                    </div>
                                                </div>
                                
                                                <div class="group markedDepartments">
                                                    <div class="group__icon"></div>
                                                    <div class="group__arrow"></div>
                                                    <div class="group__names">
                                                    ${markedDepartments}
                                                    </div>
                                                </div>
                                            </div>
                                
                                
                                            <div class="post__time">
                                                <div class="time">
                                                    <div class="post__time__date">
                                                        <p>${post.data_posted}</p>
                                                    </div>
                                                    <div class="post__time__time">
                                                        <p>${post.time_posted}</p>
                                                    </div>
                                                </div>
                                                ${postEditButton}
                                            </div>
                                
                                            <div class="post__text">
                                                <p class="post__text__description"> ${post.description} </p>
                                                ${image}
                                                
                                                ${youtubeEmbed}
    
                                                <div class="event_info"> 
                                                    <div class="post__location">
                                                        <p class="post__location__start"> <svg class='location_icon' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg> ${post.location} </p>
                                                    </div>
    
                                                    
                                                    <div class="post__start-end-date">
                                                        <p class="post__start-end-date__start"> ${post.startAndEndDate.start}</p>
                                                        <p class="post__start-end-date__end"> ${post.startAndEndDate.end}</p>
                                                    </div>
                                                </div>
                                               
    
                                               
                                            </div>
                                
                                            <div class="post__smiles" onclick='getUserHowLiked(this)'>
                                                <ul>
                                                    <li>
                                                        <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 29.svg');">
                                                        </div>
                                                        <p data-likeTypeamount = "happy">${post.likes.happy}</p>
                                                    </li>
                                                    <li>
                                                        <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 31.svg');">
                                                        </div>
                                                        <p data-likeTypeamount = "sad">${post.likes.sad}</p>
                                                    </li> 
                                                    <li>
                                                        <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 30.svg');">
                                                        </div>
                                                        <p data-likeTypeamount = "neutral">${post.likes.neutral}</p>
                                                    </li>
                                                    <li>
                                                        <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 34.svg');">
                                                        </div>
                                                        <p data-likeTypeamount = "love">${post.likes.love}</p>
                                                    </li>
                                                    <li>
                                                        <div class="smiles" style="background-image: url('media/img/svg/emojis/Group 32.svg');">
                                                        </div>
                                                        <p data-likeTypeamount = "amazed">${post.likes.amazed}</p>
                                                    </li>
                                                </ul>
                                            </div>
                                
                                
                                            <div class="post__actions">
                                                <ul>
                                
                                                    <li>
                                                        <div class="action_icon">
                                                        ${emojiLikes}
                                
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="action_icon commentCollapseBtn" onmouseup="showCommentInput()" id="commentCollapseBtn">
                                                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path class="comment-action-fill"
                                                                    d="M3.98421 14.8081H3.57636L3.28485 15.0933L1.03712 17.2926V2.29092C1.03712 1.79673 1.45192 1.35989 2.01067 1.35989H17.799C18.3578 1.35989 18.7726 1.79673 18.7726 2.29092V13.877C18.7726 14.3712 18.3578 14.8081 17.799 14.8081H3.98421Z"
                                                                    stroke="#00A57F" stroke-width="2" />
                                                            </svg>
                                
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                
                                
                                            ${topRatedComment}
                                
                                
                                
                                            ${addCommentsElement}
                                            <div class="post__other-comment__container">
                                                ${otherComments}
                                            </div>
                                        </div>
                                
                                    </div>`,
                    "text/html")





                if (allPostsContainer) {


                    if (post.type == "post") {
                        allPostsContainer.append(postTemplate.body.firstElementChild);
                    }


                    if (post.type == "poll") {
                        allPostsContainer.append(pollTemplate.body.firstElementChild);
                    }


                    if (post.type == "event") {
                        allPostsContainer.append(eventTemplate.body.firstElementChild);
                    }

                }
            });
        }
    });
}

