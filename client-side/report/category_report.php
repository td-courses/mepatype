<head>
<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
	<style type="text/css">
		#callapp_footer{
			z-index:10!important;
			margin-left: 85px;
		}
        input,
        textarea {
            caret-color: #266dbb;
        }
		#loading1{
            z-index:999;
            top:45%;
            left:45%;
            position: absolute;
            display: none;
            padding-top: 15px;
        }
        @media screen and (max-width: 1400px) {
            html{
                zoom:90%;
            }
        }
		caption{
		    margin: 0;
			padding: 0;
			background: #f3f3f3;
			height: 40px;
			line-height: 40px;
			text-indent: 2px;
			font-family: "Trebuchet MS", Trebuchet, Arial, sans-serif;
			font-size: 140%;
			font-weight: bold;
			color: #000;
			text-align: left;
			letter-spacing: 1px;
			border-top: dashed 1px #c2c2c2;
			border-bottom: dashed 1px #c2c2c2;
		}
		div, caption, td, th, h2, h3, h4 {
			font-size: 11px;
			font-family: verdana,sans-serif;
			voice-family: "\"}\"";
			voice-family: inherit;
			color: #333;
		}
		table th,table td{
    		color: #333;
            font-family: pvn;
            background: #E6F2F8;
			border: 1px solid #A3D0E4;
			vertical-align: middle;
		}
		table td{
			word-wrap: break-word;
        }
		table {
			padding: 10px;
            text-align: left;
            vertical-align: middle;
			margin: 0 auto;
            clear: both;
            border-collapse: collapse;
            table-layout: fixed;
            border: 1px solid #E6E6E6;
		}
		a{
			cursor: pointer;
		}
    </style>
    <script src="js/highcharts.js"></script>
     <script src="js/exporting.js"></script>
	<script type="text/javascript">
		var aJaxURL		= "server-side/report/category_report.action.php";		//server side folder url
		var tName		= "example";											//table name
		var tbName		= "tabs";												//tabs name
		var fName		= "add-edit-form";										//form name
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		var file_name 	= '';
		var rand_file 	= '';
		
		$(document).ready(function () {
			GetDate("start_time");
			GetDate("end_time");
			$("#show_report,#technik_info_but").button();
			LoadTable();
			SetEvents("", "", "", tName, fName, aJaxURL);
		});
		function drawFirstLevel(level){


			var chart_conatiner;
			var title_text;
			switch(level){
				case 1:
					chart_conatiner = 'chart_container1';
					title_text = 'ზარის კატეგორია';
				break;

				case 2:
					chart_conatiner = 'chart_container2';
					title_text = 'ზარის ქვე-კატეგორია 1';
				break;

				case 3:
					chart_conatiner = 'chart_container3';
					title_text = 'ზარის ქვე-კატეგორია 2';
				break;
			}
			var options = {
				chart: {
					renderTo: chart_conatiner,
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: null,
				},
				title: {
					text: title_text
				},
				tooltip: {
					formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
					}
				},
				plotOptions: {
					pie: {
						size:'90%',
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							color: '#000000',
							connectorColor: '#FA3A3A',
							formatter: function() {
								return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
							}
						},
						point: {
							events: {
								click: function() {
								}
							}
						}
					}
				},
				series: [{
					type: 'pie',
					name: 'კატეგორიები',
					data: []
				}]
			}
			
			start_time = $('#start_time').val();
			end_time = $('#end_time').val();
			$.getJSON("server-side/report/category_report.action.php?act=get_pie&start_time="+start_time + "&end_time=" + end_time + "&cat_level="+level, function(json) {
				options.series[0].data = json;
				chart = new Highcharts.Chart(options);
			});
		}
        function LoadTable(){

			start_time = $('#start_time').val();
			end_time = $('#end_time').val();
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list", 8, "start_time="+start_time + "&end_time=" + end_time, 0, "", 1, "desc", "", change_colum_main);
			setTimeout(function(){
    	    	$('.ColVis, .dataTable_buttons').css('display','none');
			}, 90);
			drawFirstLevel(1);
			drawFirstLevel(2);
			drawFirstLevel(3);
			drawFirstLevel(4);
		}
		$(document).on("click", "#show_report", function () {
			// $('#loading1').css('display','block');
			$('#tabs').css('height','800px');
            LoadTable();
        });
		$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });
		$(document).on("click", "button", function (e) {
			var tbl_id = $(this).attr('id');
			var table = tbl_id.substr(0,tbl_id.length - 4);

			var tableToExcel = (function() {
		          var uri = 'data:application/vnd.ms-excel;base64,'
		            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
		            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
		            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		          return function(table, name) {
		            if (!table.nodeType) table = document.getElementById(table)
		            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
		            window.location.href = uri + base64(format(template, ctx))
		          }
		        })();
			tableToExcel(table, 'excel export')
		});
    </script>
    
</head>

<body>
<div id="loading1">
    <p><img src="media/images/loader.gif" /></p>
</div>
<div id="tabs" >
		<div class="callapp_head">ზარის კატეგორიები<hr class="callapp_head_hr"></div>
			<div class="clear"></div>
	<div id="rest">
				<h2>თარიღის არჩევა</h2>
				<hr>
				<div id="button_area">
	            	<div class="left" style="width: 200px;">
	            		<label for="search_start" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასაწყისი</label>
	            		<input type="text" name="search_start" id="start_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
	            	</div>
	            	<div class="right" style="width: 210px;">
	            		<label for="search_end" class="left" style="margin: 6px 0 0 9px; font-size: 12px;">დასასრული</label>
	            		<input type="text" name="search_end" id="end_time" value="" class="inpt right" style="width: 110px; height: 16px;"/>
            		</div>	
            	</div>

        <input style="margin-left: 15px;" id="show_report" name="show_report" type="submit" value="რეპორტების ჩვენება">

        <table id="table_right_menu">
                    <tr>
                        <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
                        </td>
                        <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14">
                        </td>
                        <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
                        </td>
                    </tr>
                </table>
                <table class="display" id="example">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 1%;">№</th>
                            <th style="width: 12%;">თარიღი</th>
                            <th style="width: 12%;">ტელ.ნომერი</th>
                            <th style="width: 12%;">ზარის ქვე-კატეგორია 1</th>
                            <th style="width: 12%;">ზარის ქვე-კატეგორია 2</th>
                            <th style="width: 12%;">ზარის ქვე-კატეგორია 3</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                        </tr>
                    </thead>
                </table>
                <br>
                <div id="chart_container1" style="margin:auto"></div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="clear"></div>
                <div id="chart_container2" style="margin:auto"></div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="clear"></div>
                <div id="chart_container3" style="margin:auto"></div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="clear"></div>
                <div id="chart_container4" style="margin:auto"></div>
		
		<div class="clear"></div>
		<div class="clear"></div>
		<div class="clear"></div>
		<div class="clear"></div>
		<div class="clear"></div>
		<div class="clear"></div>
		<div class="clear"></div>
		 </div>
		
<!-- jQuery Dialog -->
<div id="add-edit-form" class="form-dialog" title="ავტოპარკი">
</div>
</div>
</body>