class kendoUI{
	loadKendoUI(url, action, itemPerPage, columnsCount, columnsSQL, gridName, actButtons, editType, columnNames, filtersCustomOperators, showOperatorsByColumns, selectorsForFilters, hidden='', filter=0, locked = '', lockable = '', freezeing_rows=0, pdfEnabled = false, excelEnabled = false, CampaignEnabled = false,hiddenColumns=[]){
		this.ajaxURL = url;
		this.daction = action;
		this.itemPerPage = itemPerPage;
		this.columnsCount = columnsCount;
		this.columnsSQL = columnsSQL;
		this.gridName = gridName;
		this.actButtons = actButtons;
		this.editType = editType;
		this.hidden = hidden;
		this.freezeing_rows = freezeing_rows;
		this.pdfEnabled = pdfEnabled;
		this.excelEnabled = excelEnabled;
		this.CampaignEnabled = CampaignEnabled;
		this.hiddenColumns = hiddenColumns;

		if(filtersCustomOperators !== ''){
			this.customOperators = JSON.parse(filtersCustomOperators);
		}
		
		if(filter == 0){
			this.filterable = JSON.parse('{"mode": "row"}');
		}
		else{
			this.filterable = true;
		}

		//  LOADING COLUMNS AND MODELS BEGIN
		this.getColumnsURL = this.ajaxURL+"?act=get_columns&count="+this.columnsCount;
		var self = this;
		$.ajax({
			url: this.getColumnsURL,
			data: {cols: columnsSQL, names: columnNames, operators: showOperatorsByColumns, selectors: selectorsForFilters, locked: locked, lockable: lockable},
			dataType: "json",
			success: function(kendoData) {
				self.generateGrid(kendoData);
			}
		});


		
		// LOADING COLUMNS AND MODELS END
	}

	generateGrid(kendoData){
		this.dataSource = new kendo.data.DataSource({
			transport: {
				read:  {
					url: this.ajaxURL + "?act="+this.daction+"&count="+this.columnsCount+"&cols="+this.columnsSQL+this.hidden,
					dataType: "json"
				},
				update: {
					url: this.ajaxURL + "?act=save_priority",
					dataType: "json"
				},
				destroy: {
					url: this.ajaxURL + "?act=disable",
					dataType: "json"
				},
				create: {
					url: this.ajaxURL + "/Products/Create",
					dataType: "json"
				},
				
				parameterMap: function(data, options, operation) {
					
					if (operation !== "read" && options.models) {
						return {models: kendo.stringify(options.models)};
					}
					else {
						return {add: kendo.stringify(data)};
					}
				}
			},
			batch: true,
			pageSize: this.itemPerPage,
			
			schema: {
				model: kendoData.modelss,
				total: 'total',
				data: function (data) {
					return data.data;
				}
			},
			serverFiltering: false,
			serverPaging: false
		});
		
		$("#"+this.gridName).empty();

		var self = this;
		var onDataBound = (arg) => { 
			var KendoisLoaded = true;
			self.getStatus(KendoisLoaded, this.gridName);
			
		}

		var freeze = function (e){
			e.sender.element.find(".customHeaderRowStyles").remove();
            var items = e.sender.items();
            e.sender.element.height(e.sender.options.height);   
            items.each(function(){
              var row = $(this);
              var dataItem = e.sender.dataItem(row);
              if(dataItem.id2 == '<img style="margin-bottom: 3px;" src="media/images/icons/star.png">'){
                  var item = row.clone();                
                  item.addClass("customHeaderRowStyles");
                  var thead = e.sender.element.find(".k-grid-header table thead");
                  thead.append(item);
                    e.sender.element.height(e.sender.element.height() + row.height());                
                  row.hide();
              }
			});
			
		}


		var customTools = [];
		var AddInCampaignButton = '<a id="select_campaign" role="button" class="k-button k-button-icontext k-grid-dictionary-add"><span class="k-icon k-i-dictionary-add"></span> კამპანიაში დამატება</a>';
		
		if(this.CampaignEnabled){
			customTools.push({ 	template: AddInCampaignButton	});
		}
		if(this.actButtons != ''){
			customTools.push({	template: this.actButtons	});
		}
		if(this.pdfEnabled){
			customTools.push({ 	name: "pdf"	 })
		}
		if(this.excelEnabled){
			customTools.push({	name: "excel"	})
		}
	
		if(this.freezeing_rows == 0){
			$("#"+this.gridName).kendoGrid({
				toolbar: customTools,
				pdf: {
					allPages: true,
					avoidLinks: true,
					paperSize: "A4",
					margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
					landscape: true,
					repeatHeaders: true,
					template: $("#page-template").html(),
					scale: 0.8
				},
				dataSource: this.dataSource,
				selectable: "multiple",
				allowCopy: true,
				persistSelection: true,
				//height: 350,
				sortable: true,
				filterable: true,
				pageable: {
					refresh: true,
					pageSizes: true,
					buttonCount: 5
				},
				// toolbar: this.actButtons,
				columns: kendoData.columnss,
				editable: this.editType,
				dataBound: onDataBound
			});
		}
		else{
			$("#"+this.gridName).kendoGrid({
				toolbar: customTools,
				pdf: {
					allPages: true,
					avoidLinks: true,
					paperSize: "A4",
					margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
					landscape: true,
					repeatHeaders: true,
					template: $("#page-template").html(),
					scale: 0.8
				},
				dataSource: this.dataSource,
				selectable: "multiple",
				allowCopy: true,
				persistSelection: true,
				height: 400,
				sortable: true,
				filterable: true,
				pageable: {
					refresh: true,
					pageSizes: true,
					buttonCount: 5
				},
				columns: kendoData.columnss,
				editable: this.editType,
				dataBound: freeze
			});
		}
		var grid = $("#" + this.gridName).data("kendoGrid");
		for (let i = 0; i < this.hiddenColumns.length; i++) {
			grid.hideColumn(this.hiddenColumns[i]);
		}
	}
		
	getStatus(KendoisLoaded, gridName){
		if(gridName == "project_table"){ // დროებითია
			excel_upload()
		}


  
		// $("#"+gridName).kendoTooltip({  
		//   show: function(e){  
		// 	  if(this.content.text().length > 10){  
		// 	  this.content.parent().css("visibility", "visible");  
		// 	  }  
		//   },  
		//   hide:function(e){  
		// 	  this.content.parent().css("visibility", "hidden");  
		//   },  
		//   filter: "td", 
		//   position: "right",  
		//   content: function(e){  
		// 	  var content = e.target[0].innerText;  
		// 	  return content;  
		//   }  
		//   }).data("kendoTooltip");

		console.log(KendoisLoaded, gridName)
	}

	kendoSelector(selectorName, url, action, title){
		$("#"+selectorName).kendoDropDownList({
			serverFiltering: true,
			filter: "contains",
			dataTextField: "name",
			dataValueField: "id",
			dataSource:  new kendo.data.DataSource({
				transport: {
					read: {
						url: url + "?act="+ action,
						dataType: "json"
					}
				},
				schema: {
					total: 'total',
					data: function (data) {
						var addCustomObject = {"id": "", "name": title}
						data = data.result;

						if( data != null){
							var data = [addCustomObject].concat(data);
							data.slice = () => JSON.parse(JSON.stringify(data));
						}else{
							data = {"id": "", "name": title};
							var data = [].concat(data);
							data.slice = () => JSON.parse(JSON.stringify(data));
						}
						return data;
					}
				},
			})
			// filter: "startswith",
			
		});
	}

	kendoMultiSelector(selectorName, url, action, title, value = 0){
		var preSelected = [];
		if(value != 0){
			preSelected = value.split(',');

			console.log(preSelected);
		}
		$("#"+selectorName).kendoMultiSelect({
			
			placeholder: title,
			dataTextField: "name",
			dataValueField: "id",
			headerTemplate: '<div class="dropdown-header k-widget k-header">' +
					'<span></span>' +
					'<span></span>' +
				'</div>',
			footerTemplate: '',
			itemTemplate: '<span class="k-state-default" style="font-size: 13px">#: data.name #</span>',
			tagTemplate:  '<span>#:data.name#</span>',
			dataSource: new kendo.data.DataSource({
				transport: {
					read: {
						dataType: "json",
						url: url +"?act=" + action,
					}
				},
				/* requestEnd: function (e) {
                    var widget = $("#"+selectorName).getKendoMultiSelect();
                    var dataSource = e.sender;
					alert(e.type);
                    if (e.type === "create") {
                        var newValue = e.response[0].id;
						alert(newValue);
                        dataSource.one("sync", function () {
                            widget.value(widget.value().concat([newValue]));
                        });
                    }
                }, */
				schema: {
					data: (res) => {
						res = res.result;
						res.slice = () => JSON.parse(JSON.stringify(res));
						return res;
					}
				}
			}),
			value: preSelected,
			noDataTemplate: $("#noDataTemplate").html(),
			height: 300
		});
	}

	saveExcelManual(element_id, title, hiddenCount = []){
			// get Data And Columns By id
			var kendoCols = $("#" + element_id).data("kendoGrid").options.columns;
			var kendoData = $("#" + element_id).data("kendoGrid")._data;


			var properties = []; // properties only in view( no prototype and length from object )
			var columns = []; 
			var Data = [];

			// check filters are on or not
		

			// check the hidden columns AND get columns' titles
			for (let i = 0; i < kendoCols.length; i++) {
				var bool = true;
				for (let j = 0; j < hiddenCount.length; j++) {
					if(i == hiddenCount[j]){
						bool = false;
					}
				}
				if(bool){
					properties.push(kendoCols[i].field);
					columns.push(kendoCols[i].title);
				}
			}
			
			// filter kendo data By dislpayed columns
			for (let i = 0; i < kendoData.length; i++) {
				var tmp = []; // each row converted from object to array (without non-nessesery properties)
				var kendoAsArray  = Object.entries(kendoData[i]); // converting each property to array

				for (let j = 0; j < properties.length; j++) {
					for (let k = 0; k < kendoAsArray.length; k++) {
						if(kendoAsArray[k][0] == properties[j]){
							tmp.push(kendoAsArray[k][1]);
						}
					}
				}
				Data.push(tmp);
			}

			var nowDatee = new Date().toISOString();
			var colsWidth = [];
			var cols = [];
			var rows = [];

			// define width and columns as object properties
			for (let i = 0; i < columns.length; i++) {
				colsWidth.push({ autoWidth: true });
				cols.push({ value: columns[i], bold: true });
			}
			// columns as first row
			rows.push({ cells: cols });
			
			// get rows from filted kendo Data
			for (let i = 0; i < Data.length; i++) {
				var tmp=[];
				for (let j = 0; j < Data[i].length; j++) {
					var data = Data[i][j];
					var withoutHtml = data;
					if(data != null)
						withoutHtml = data.replace(/<\/?[^>]+(>|$)/g, "");
					tmp.push({value: withoutHtml });
				}
				rows.push({cells: tmp});
			}
			
			// create workbook for excel
			var workbook = new kendo.ooxml.Workbook({
				sheets: [{columns: colsWidth, title: title, rows: rows}]
			});
			// print it
			kendo.saveAs({
				dataURI: workbook.toDataURL(),
				fileName: title + nowDatee + ".xlsx"
			});
	}
}

