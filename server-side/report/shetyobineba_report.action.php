<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
-$start      = $_REQUEST['start'];
$end        = $_REQUEST['end'];


$start_end      = $_REQUEST['start'] . ' 00:00';
$end_end        = $_REQUEST['end'] . ' 23:59';

$count         = $_REQUEST["count"];
$action     = $_REQUEST['act'];
$departament = $_REQUEST['departament'];
$type       = $_REQUEST['type'];
$call_type       = $_REQUEST['call_type'];
$category   = $_REQUEST['category'];
$s_category = $_REQUEST['sub_category'];
$done         = $_REQUEST['done'];
$name         = $_REQUEST['name'];
$title         = $_REQUEST['title'];
$source_info   = $_REQUEST['source_info'];
$selector_data = $_REQUEST['rep_selector'];

$text[0]     = "შემოსული  ზარები კატეგორიების მიხედვით";
$text[1]     = "'$departament'- შემოსული ზარები  ქვე-კატეგორიების მიხედვით";
$text[2]     = "'$departament'- შემოსული ზარები ქვე-ქვე-კატეგორიების  მიხედვით";
$text[3]     = "'$departament'- შემოსული  ქვე–კატეგორიის მიხედვით";

// GET call type filter
$call_type_filter = "";
if ($call_type == 0) {
    $call_type_filter = " ";
} else if ($call_type == 1) {
    $call_type_filter = " AND (asterisk_call_log.call_type_id IN(1) OR ISNULL(asterisk_call_log.call_type_id)) ";
} else if ($call_type == 2) {
    $call_type_filter = " AND asterisk_call_log.call_type_id IN(2)  ";
}

// GET source type filter
$selector_data = explode(",", $selector_data);
$source_id = '';
foreach ($selector_data as $item) {
    if ($item == 'ზარები') {
        $source_id .= '1,';
    }
    if ($item == 'ჩატი') {
        $source_id .= '4,';
    }
    if ($item == 'Messenger') {
        $source_id .= '6,';
    }
    if ($item == 'Mail') {
        $source_id .= '7,';
    }
}
$source_id = rtrim($source_id, ",");
if (empty($source_id)) {
    $where_cl = '';
} else {
    $where_cl = " incomming_call.source_id IN ($source_id) AND ";
}

$region_id = $_REQUEST['region_id'];
$raion_id  = $_REQUEST['raion_id'];
$sofel_id  = $_REQUEST['sofel_id'];
$region_filt = '';
$raion_filt  = '';
$sofel_filt  = '';


if ($region_id > 0) {
    $region_filt = " AND region.int_value = $region_id";
}

if ($raion_id > 0) {
    $raion_filt = " AND raion.int_value = $raion_id";
}

if ($sofel_id > 0) {
    $sofel_filt = " AND sofeli.int_value = $sofel_id";
}

// $db->setQuery(" SELECT  COUNT(incomming_call.id) AS `count`
//                 FROM   `incomming_request_processing` AS irp
//                 JOIN    info_category ON info_category.id = irp.int_value AND info_category.parent_id = 0
//                 JOIN    incomming_call ON incomming_call.id = irp.incomming_request_id AND incomming_call.date BETWEEN '$start_end' AND '$end_end'
//                 WHERE   irp.processing_setting_detail_id = 0 AND irp.`value` = 1");
// $db->setQuery(" SELECT  COUNT(*) AS `count`
//                 FROM   `incomming_request_processing` AS irp
//                 JOIN    info_category ON info_category.id = irp.int_value AND info_category.parent_id = 0
//                 JOIN    incomming_call ON incomming_call.id = irp.incomming_request_id
//                 WHERE   irp.processing_setting_detail_id = 0 AND irp.`value` = 1
//                 AND     irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end'");
// $res_all_count = $db->getResultArray();
// $count_all_cat = $res_all_count[result][0][count];

//------------------------------------------------query-------------------------------------------
switch ($done) {
    case  1:
        $db->setQuery(" SELECT  COUNT(*) AS `count`
                                FROM   (SELECT  MAX(IF(irp.`value` = 1,irp.int_value,0)) AS `cat`,
                                                MAX(IF(irp.`value` = 2,irp.int_value,0)) AS `cat1`,
                                                MAX(IF(irp.`value` = 3,irp.int_value,0)) AS `cat2`,
                                                irp.incomming_request_id
                                        FROM    `incomming_request_processing` AS irp
                                        JOIN     info_category ON info_category.id = irp.int_value
                                        JOIN     incomming_call ON  incomming_call.id = irp.incomming_request_id

                                        WHERE    irp.processing_setting_detail_id = 0 AND irp.`value` IN(1,2,3)
                                        AND      irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end'
                                        GROUP BY irp.incomming_request_id, irp.additional_deps) AS inc_proc_cat
                                                
                                JOIN      incomming_call ON incomming_call.id = inc_proc_cat.incomming_request_id				
                                JOIN      info_category ON info_category.id = inc_proc_cat.cat

                                LEFT JOIN info_category AS info_category1 ON info_category1.id = inc_proc_cat.cat1
                                LEFT JOIN asterisk_call_log ON  asterisk_call_log.id = incomming_call.asterisk_incomming_id  

                                JOIN incomming_request_processing AS irp3 ON irp3.incomming_request_id = inc_proc_cat.incomming_request_id 
                                    AND 		irp3.value = 4 AND irp3.int_value = 3	
                                LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = inc_proc_cat.incomming_request_id AND region.`value` = 1111 AND region.int_value > 0
        						LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = inc_proc_cat.incomming_request_id AND raion.`value` = 2222 AND raion.int_value > 0
                                LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = inc_proc_cat.incomming_request_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0

                                WHERE      info_category.`name` = '$departament' AND incomming_call.date BETWEEN '$start_end' AND '$end_end' $call_type_filter $region_filt $raion_filt $sofel_filt");
        $res_all_count = $db->getResultArray();
        $count_all_cat = $res_all_count[result][0][count];
        $db->setQuery(" SELECT  IFNULL(info_category1.`name`,'არ არის მითითებული') AS `name`,
                                COUNT(*),
                                ROUND(COUNT(*)/$count_all_cat*100, 2) AS `percent`,
                                IFNULL(COUNT(irp3.id),0) AS `shetyobineba`
                        FROM   (SELECT  MAX(IF(irp.`value` = 1,irp.int_value,0)) AS `cat`,
                                        MAX(IF(irp.`value` = 2,irp.int_value,0)) AS `cat1`,
                                        MAX(IF(irp.`value` = 3,irp.int_value,0)) AS `cat2`,
                                        irp.incomming_request_id
                                FROM    `incomming_request_processing` AS irp
                                JOIN     info_category ON info_category.id = irp.int_value
                                JOIN     incomming_call ON $where_cl incomming_call.id = irp.incomming_request_id

                                WHERE    irp.processing_setting_detail_id = 0 AND irp.`value` IN(1,2,3)
                                AND      irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end'
                                GROUP BY irp.incomming_request_id, irp.additional_deps) AS inc_proc_cat
                                        
                        JOIN      incomming_call ON incomming_call.id = inc_proc_cat.incomming_request_id				
                        JOIN      info_category ON info_category.id = inc_proc_cat.cat

                        LEFT JOIN info_category AS info_category1 ON info_category1.id = inc_proc_cat.cat1
                        LEFT JOIN asterisk_call_log ON  asterisk_call_log.id = incomming_call.asterisk_incomming_id  

                        JOIN incomming_request_processing AS irp3 ON irp3.incomming_request_id = inc_proc_cat.incomming_request_id 
                            AND 		irp3.value = 4 AND irp3.int_value = 3	
						LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = inc_proc_cat.incomming_request_id AND region.`value` = 1111 AND region.int_value > 0
        				LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = inc_proc_cat.incomming_request_id AND raion.`value` = 2222 AND raion.int_value > 0
                        LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = inc_proc_cat.incomming_request_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                        
                        WHERE      info_category.`name` = '$departament' AND incomming_call.date BETWEEN '$start_end' AND '$end_end' $call_type_filter $region_filt $raion_filt $sofel_filt
                        GROUP BY  inc_proc_cat.cat1");

        $text[0] = $text[1];
        break;
    case  2:
        $db->setQuery(" SELECT  COUNT(irp.incomming_request_id) AS `count`
                        FROM   `incomming_request_processing` AS irp
                        JOIN    info_category ON info_category.id = irp.int_value AND info_category.parent_id > 0
                        JOIN    incomming_call ON incomming_call.id = irp.incomming_request_id
                        LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = irp.incomming_request_id AND region.`value` = 1111 AND region.int_value > 0
        				LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = irp.incomming_request_id AND raion.`value` = 2222 AND raion.int_value > 0
                        LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = irp.incomming_request_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                        WHERE   $where_cl irp.processing_setting_detail_id = 0 AND irp.`value` = 1 AND irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end' $region_filt $raion_filt $sofel_filt");
        $res_all_count = $db->getResultArray();
        $count_all_cat = $res_all_count[result][0][count];
        $db->setQuery("  SELECT  IFNULL(info_category2.`name`,'არ არის მითითებული') AS `name`,
                        COUNT(*),
                        ROUND(COUNT(*)/$count_all_cat*100, 2) AS `percent`,
                        IFNULL(COUNT(irp3.id),0) AS `shetyobineba`
                        FROM   (SELECT  MAX(IF(irp.`value` = 1,irp.int_value,0)) AS `cat`,
                                        MAX(IF(irp.`value` = 2,irp.int_value,0)) AS `cat1`,
                                        MAX(IF(irp.`value` = 3,irp.int_value,0)) AS `cat2`,
                                        irp.incomming_request_id
                                FROM    `incomming_request_processing` AS irp
                                JOIN     info_category ON info_category.id = irp.int_value
                                JOIN     incomming_call ON $where_cl incomming_call.id = irp.incomming_request_id	
                                WHERE    irp.processing_setting_detail_id = 0 AND irp.`value` IN(1,2,3)
                                AND      irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end'
                                GROUP BY irp.incomming_request_id, irp.additional_deps) AS inc_proc_cat

                        JOIN      incomming_call ON incomming_call.id = inc_proc_cat.incomming_request_id
                        JOIN      info_category ON info_category.id = inc_proc_cat.cat
                        LEFT JOIN info_category AS info_category1 ON info_category1.id = inc_proc_cat.cat1
                        LEFT JOIN info_category AS info_category2 ON info_category2.id = inc_proc_cat.cat2
                        LEFT JOIN asterisk_call_log ON  asterisk_call_log.id = incomming_call.asterisk_incomming_id  
				
                        JOIN incomming_request_processing AS irp3 ON irp3.incomming_request_id = inc_proc_cat.incomming_request_id 
                            AND 		irp3.value = 4 AND irp3.int_value = 3
                        LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = inc_proc_cat.incomming_request_id AND region.`value` = 1111 AND region.int_value > 0
        				LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = inc_proc_cat.incomming_request_id AND raion.`value` = 2222 AND raion.int_value > 0
                        LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = inc_proc_cat.incomming_request_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                        WHERE     info_category.`name` = '$departament' AND IFNULL(info_category1.`name`,'არ არის მითითებული') = '$type' 
                            AND incomming_call.date BETWEEN '$start_end' AND '$end_end' $call_type_filter $region_filt $raion_filt $sofel_filt
                        GROUP BY  inc_proc_cat.cat2");

        $text[0] = $text[2];
        break;
    default:
        $db->setQuery("SELECT       COUNT(*) AS `count`
                        FROM        `incomming_request_processing` AS irp
                        JOIN        info_category ON info_category.id = irp.int_value AND info_category.parent_id = 0
                        JOIN        incomming_call ON incomming_call.id = irp.incomming_request_id

                        JOIN incomming_request_processing AS irp3 ON irp3.incomming_request_id = irp.incomming_request_id 
                            AND 		irp3.value = 4 AND irp3.int_value = 3	
                        LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = irp.incomming_request_id AND region.`value` = 1111 AND region.int_value > 0
        				LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = irp.incomming_request_id AND raion.`value` = 2222 AND raion.int_value > 0    
                        LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = irp.incomming_request_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                        LEFT JOIN asterisk_call_log ON  asterisk_call_log.id = incomming_call.asterisk_incomming_id  

                        WHERE       $where_cl irp.processing_setting_detail_id = 0 AND irp.`value` = 1 $call_type_filter
                        AND         irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end' $region_filt $raion_filt $sofel_filt");


        $res_all_count = $db->getResultArray();
        $count_all_cat = $res_all_count[result][0][count];
        $db->setQuery(" SELECT  info_category.name,
                                IFNULL(MAIN.count,0),
                                IFNULL(MAIN.percent,0)
                        FROM    (   SELECT      info_category.`name`,
                                                COUNT(irp3.id) AS `count`,
                                                ROUND(COUNT(irp3.id) / $count_all_cat * 100, 2) AS `percent`,
                                                irp.int_value,
                                                COUNT(irp3.id) AS `shetyobineba` 
                                    FROM        `incomming_request_processing` AS irp
                                    JOIN        info_category ON info_category.id = irp.int_value AND info_category.parent_id = 0
                                    JOIN        incomming_call ON incomming_call.id = irp.incomming_request_id
	
                                    LEFT JOIN incomming_request_processing AS irp3 ON irp3.incomming_request_id = irp.incomming_request_id 
                                        AND 		irp3.value = 4 AND irp3.int_value = 3	
                                    LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = irp.incomming_request_id AND region.`value` = 1111 AND region.int_value > 0
        							LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = irp.incomming_request_id AND raion.`value` = 2222 AND raion.int_value > 0   
                                    LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = irp.incomming_request_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0 
						            LEFT JOIN asterisk_call_log ON  asterisk_call_log.id = incomming_call.asterisk_incomming_id  

                                    WHERE       $where_cl irp.processing_setting_detail_id = 0 AND irp.`value` = 1 $call_type_filter
                                    AND         irp.int_value > 0 AND incomming_call.date BETWEEN '$start_end' AND '$end_end' $region_filt $raion_filt $sofel_filt
                                    GROUP BY    irp.int_value) AS MAIN
                                    
                        RIGHT JOIN info_category ON info_category.id = MAIN.int_value
                        WHERE       info_category.parent_id = 0 AND info_category.actived = 1 AND IFNULL(MAIN.shetyobineba,0) > 0");

        break;
}
///----------------------------------------------act------------------------------------------
switch ($action) {
    case "get_list":
        $data = $db->getList($count, "no");
        echo json_encode($data);
        return 0;
        break;
    case 'get_category':
        $rows = array();
        $result = $db->getResultArray(MYSQLI_NUM);
        foreach ($result["result"] as $r) {
            $row[0] = $r[0];
            $row[1] = (float) $r[1];
            $rows['data'][] = $row;
        }
        $rows['text'] = $text[0];
        echo json_encode($rows);
        break;
    case 'get_in_page':

        if ($_REQUEST[rid] == 'არ აქვს კატეგორია') {
            $rid = 'AND incomming_call.cat_1_1_1 = 999';
        } else {
            $rid = "AND cat_1_1_1.`name` = '$_REQUEST[rid]'";
        }


        $db->setQuery(" SELECT      IF(asterisk_call_log.call_status_id = 9, '', incomming_call.id),
                                        incomming_call.id,
                                        incomming_call.date,
                                        incomming_call.client_name,
                                        incomming_call.phone,
                                       
                                        IFNULL(user_info1.`name`,user_info.`name`) AS `user_name`,
                                        TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.talk_time),'%i:%s') AS `duration`,
                                        CASE
                                            WHEN (NOT ISNULL(incomming_call.`chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                                THEN concat(_utf8 '<button class=\'download7\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშავებ.ჩატი</span></button>')
                                            WHEN (NOT ISNULL(incomming_call.`chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                                THEN concat(_utf8 '<button class=\'download8\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Chat.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშავე.ჩატი</span></button>')
                                            
                                            WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                                THEN concat(_utf8 '<button class=\'download15\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშა.მეილი</span></button>')
                                            WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                                THEN concat(_utf8 '<button class=\'download15\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/E-MAIL.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშავ.მეილი</span></button>')
                                            
                                            WHEN (NOT ISNULL(incomming_call.`chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                                THEN concat(_utf8 '<button class=\'download10\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუშ.ვაიბერი</span></button>')
                                            WHEN (NOT ISNULL(incomming_call.`chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                                THEN concat(_utf8 '<button class=\'download10\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშა.ვაიბერი</span></button>')
                                            
                                            WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                                THEN concat(_utf8 '<button class=\'download11\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.მესენჯერი</span></button>')
                                            WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                                THEN concat(_utf8 '<button class=\'download11\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Messenger.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუ.მესენჯერი</span></button>')
                                            
                                            WHEN (NOT ISNULL(incomming_call.`messenger_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                                THEN concat(_utf8 '<button class=\'download13\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/CB.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.მესენჯერი</span></button>')
                                            WHEN (NOT ISNULL(incomming_call.`messenger_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                                THEN concat(_utf8 '<button class=\'download13\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/CB.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუ.მესენჯერი</span></button>')
                                            
                                            WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                                THEN concat(_utf8 '<button class=\'download14\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Video.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმუ. ვიდეო</span></button>')
                                            WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                                THEN concat(_utf8 '<button class=\'download14\' ><img style=\"float: left;\" src=\"media/images/icons/comunication/Video.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დამუშ. ვიდეო</span></button>')
                                            
                                            
                                            WHEN `asterisk_call_log`.`call_status_id` = 9
                                                THEN concat('<button class=\'download2\' str=',CONCAT(DATE_FORMAT(asterisk_call_log.call_datetime,'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format),'><span style=\"vertical-align: -1px; font-size: 10px;\">უპასუხო</span></button>')
                                            WHEN `asterisk_call_log`.`call_status_id` IN(6,7,8)
                                                THEN IF (isnull(incomming_call.`user_id`),concat('<button class=\'download4\' str=',CONCAT(DATE_FORMAT(asterisk_call_log.call_datetime,'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format),'><img style=\"float: left;\" src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">დაუმ.შემომავა.</span></button>'),IF(incomming_call.transfer=1, concat('<button class=\'download5\' str=',CONCAT(DATE_FORMAT(asterisk_call_log.call_datetime,'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format),'><span style=\"vertical-align: -1px; font-size: 10px;\">გადართული  </span></button>'),concat('<button class=\'download\' str=',CONCAT(DATE_FORMAT(asterisk_call_log.call_datetime,'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format),'><img style=\"float: left;\" src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"vertical-align: -1px; font-size: 10px;\">შემომავ. ზარი</span></button>')))
                                            ELSE '<button style=\"height: 22px; background-color: #2dc100;\" class=\"download15\"\><span style=\"vertical-align: 1px; font-size: 13px;\">მანუალური</span></button>მანუალური'
                                            END AS `file_name`
                            FROM        incomming_call
                            LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                            LEFT JOIN   asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = incomming_call.asterisk_incomming_id
                            LEFT JOIN   user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                            LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                            JOIN        source ON incomming_call.source_id = source.id
                            JOIN	    info_category AS cat_1 ON incomming_call.cat_1 = cat_1.id
                            LEFT JOIN	info_category AS cat_1_1 ON incomming_call.cat_1_1 = cat_1_1.id
                            LEFT JOIN	info_category AS cat_1_1_1 ON incomming_call.cat_1_1_1 = cat_1_1_1.id
                            LEFT JOIN   my_web_site ON incomming_call.web_site_id = my_web_site.id 
                            WHERE       $where_cl DATE(incomming_call.date) BETWEEN '$start' AND '$end'
                            $rid
            			    AND cat_1_1.`name` = '$_REQUEST[category]'
                            AND cat_1.`name` = '$_REQUEST[type]'");

        $data = $db->getList($count, 0);
        echo json_encode($data);
        return 0;
        break;
    default:
        echo "Action Is Null!";
        break;
}
