<html>
<style type="text/css">
.high {
    height: 0px;
}
.ColVis, .dataTable_buttons{
	display:none;
}

.ui-dialog .ui-widget-header {
	border: 1px solid #f3f3f3;
}
.add-edit-form-class .ui-dialog-title {
	font-family: pvn;
	color:#1BC9F8;
	font-weight: bold;
}

.add-edit-form-class .ui-dialog-titlebar-close {
	display:none;
}
</style>
<script type="text/javascript">
	var aJaxURL	= "server-side/view/monitoring_shablon.action.php";		//server side folder url
	var tName	= "example";													//table name
	var fName	= "add-edit-form";												//form name
	var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
	    	
	$(document).ready(function () {        	
		LoadTable();
		GetButtons("add_button", "");			
		SetEvents("add_button", "", "", tName, fName, aJaxURL);	
	});
    
	function LoadTable(hidden){
		//KendoUI CLASS CONFIGS BEGIN
		var aJaxURL	= "server-side/view/monitoring_shablon.action.php";
        var gridName        = 	'example';
        var actions         = 	'';
        var editType        =   "popup"; // Two types "popup" and "inline"
        var itemPerPage     = 	40;
        var columnsCount    =	7;
        var columnsSQL      = 	[
                                    "id:string",
                                    "date:date",
                                    "name:string",
                                    "user_name:string",
                                    "levepercent:string",
                                    "type:string",
                                    "action:string"
                                ];
        var columnGeoNames  = 	[
                                    "ID", 
                                    "შექმნის თარიღი",
                                    "შაბლონის დასახელება",
                                    "ავტორი",
                                    "შეფასების %",
                                    "ტიპი",
                                    "მოქმედია"
                                ];

        var showOperatorsByColumns  =   [0,0,0,0,0,0,0]; 
        var selectors               =   [0,0,0,0,0,0,0]; 

        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END
            
        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden, 1);
		
	}
	
	function LoadDialog(){
		var button = {
            "save": {
                text: "შენახვა",
                id: "save"
            },
            "cancel": {
                text: "გაუქმება",
                id: "cancel-dialog",
                click: function () {
                    $(this).dialog("close");
                }
            }
        }
        
		GetDialog(fName, 1190, "auto", button, "center top");
	}

	$(document).on("dblclick", "#example tr.k-state-selected", function () {
        var grid = $("#example").data("kendoGrid");
        var dItem = grid.dataItem($(this));
       
        if(dItem.id == ''){
            return false;
        }
        
        $.ajax({
            url: aJaxURL,
            data: {
                act: "get_edit_page",
                id: dItem.id
            },
            success: function(data){
                $("#add-edit-form").html(data.page);
                LoadDialog('add-edit-form');
            }
        })
        
    });
	// Add - Save
    $(document).on("click", "#save", function () {
		var req_type_array = [];
		var department_ids = '';
		var source_ids     = '';
		var action_ids     = '';
		
		$(".call_type_checkbox").each(function(){
			var this_val = $(this).val();
			if($(this).prop('checked') && $(".request_type[type_id="+this_val+"]").val() != '' && $(".request_type[type_id="+this_val+"]").val() != 0){
				var id = this_val+':'+$(".request_type[type_id="+this_val+"]").val();
				req_type_array.push(id);
			}
		});
		
		$(".call_department_checkbox").each(function(){
			if($(this).prop('checked')){
				department_ids += $(this).val()+',';
			}
		});

		$(".call_source_checkbox").each(function(){
			if($(this).prop('checked')){
				source_ids += $(this).val()+',';
			}
		});

		$(".call_action_checkbox").each(function(){
			if($(this).prop('checked')){
				action_ids += $(this).val()+',';
			}
		});
		
		param 			          = new Object();
		param.act                 = "save_shablon";
    	param.id                  = $("#shablon_id").val();
		param.name		          = $("#shablon_name").val();
    	param.all_request_percent = $("#all_request_percent").val();
		param.type_ids            = JSON.stringify(req_type_array);
		param.department_ids      = department_ids;
		param.source_ids          = source_ids;
		param.action_ids          = action_ids;
		param.monitoring_radio	  = $('input[name=monitoring_radio]:checked').val();
		
    	if($('input[id=distribute_random]:checked').val() != 1){
        	param.distribute_random	            = 0;
        }else{
    		param.distribute_random	            = 1;
    	}

    	if(param.name == ""){
			alert("შეავსეთ დასახელება!");
		}else if(param.all_request_percent == '' || param.all_request_percent == '0'){
			alert('შეავსე პროცენტი');
		}else if(parseFloat($("#request_types_sum_percent").text()) < 100  && param.distribute_random == 0) {
			alert('პროცენტების ჯამი ნაკლებია 100 %-ზე');
		}else{
		    $.ajax({
		        url: aJaxURL,
			    data: param,
		        success: function(data) {			        
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							LoadTable();
			        		CloseDialog(fName);
						}
					}
			    }
		    });
		}
	});

    $(document).on("click", ".active_shablon", function () {
	    param 	  = new Object();
		param.act = "active_shablon";
    	param.id  =  $(this).val();
    	
    	$.ajax({
	        url: aJaxURL,
		    data: param,
	        success: function(data) {			        
				if(typeof(data.error) != 'undefined'){
					if(data.error != ''){
						alert(data.error);
					}else{
						LoadTable();
		        	}
				}
		    }
	    });
	});

    $(document).on("click", ".call_type_checkbox", function () {
    	this_val = $(this).val();
    	if(!$(this).prop('checked')){
        	
    		$(".request_type[type_id="+this_val+"]").val('');
    		$(".request_type[type_id="+this_val+"]").prop('disabled',true);
			$("#request_types_sum_percent").html(SumPercent());
    	}else{
    		$(".request_type[type_id="+this_val+"]").prop('disabled',false);
    	}
    });

    $(document).on("click", "#distribute_random", function () {
        if($('input[id=distribute_random]:checked').val() == 1){
        	$('input[type=checkbox]').prop('checked',false);
        	$('input[type=checkbox]').attr('disabled','disabled');
        	$('#add-edit-form input[type=radio]').prop('checked',false);
        	$('#add-edit-form input[type=radio]').attr('disabled','disabled');
        	
        	$("#request_types_sum_percent").html(0);
        	$(".request_type").val('');
        	$(this).prop('checked',true);
        	$(this).attr('disabled',false);
        }else{
        	$('input[type=checkbox]').attr('disabled',false);
        	$('input[type=checkbox]').prop('checked',true);
        	$('#add-edit-form input[type=radio]').attr('disabled',false);
        	$('#add-edit-form input[type=radio]').prop('checked',false);
        	$(this).prop('checked',false);
        }
    });

    $(document).on("keyup",".request_type",function(){
    	var totalpercent = SumPercent();
    	if(parseFloat(totalpercent) > 100){
        	alert('პროცენტების ჯამი არ უნდა აღემატებოდეს 100%-ს');
        	$(this).val('');
        	var totalpercent = SumPercent();
        	$("#request_types_sum_percent").html(totalpercent);
    	}else{
        	$("#request_types_sum_percent").html(totalpercent);
    	}
    });

    
    
    function SumPercent() {
    	var sum = 0;
    	
    	$(".request_type").each(function(){
        	
        	var val = $(this).val();
        	if(val == ''){
            	val = 0;
        	}else{
            	val = $(this).val();
            }
            
			sum += parseFloat(val);
		});
		
    	return sum.toFixed(2);
    }
    

</script>
</head>

<body>
    <div id="tabs" style="width: 80%; display: inline-block; position: absolute;">
    	<div id="button_area" style="height: 30px; margin-top: 60px;">
			<button id="add_button">დამატება</button>
		</div>
        <div id="example"></div>
    </div>   
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ავტომატური ფორმირება" style="padding: 0.5em 1em;">
    	<!-- aJax -->
	</div>
</body>
</html>




