const sideMenu = document.querySelector("#sideMenu");
const collapseBtn = document.querySelector(".collapse_button");
const logo = document.querySelector(".logo");
let sidemenuLinks = document.querySelectorAll(".sidemenu_links__link");


const navigationBar = document.querySelector(".navigation__container")




window.onload = () => {
    setTimeout(() => {
        let sidemenuLinks = document.querySelectorAll(".sidemenu_links__link");
        let allMainSubLinks = document.querySelectorAll(`.subLink`)
        let allMainSubSubLinks = document.querySelectorAll(`.subSubLink`)
        let linksWithSubMenu = document.querySelectorAll(".with-subMenu");
        let linksWithSubSubMenu = document.querySelectorAll(".with-subSubMenu");
        let linksWithSubSubSubMenu = document.querySelectorAll(".with-subsubSubMenu");
        let main = document.querySelector(".main")
        let tabs = document.querySelector("#tabs")

        const sidemenuSettings = document.querySelector(".sidemenu_settings");



        let amountOfClicks = 0;


        let url_string = window.location.href; //window.location.href
        let url = new URL(url_string);


        const navigationSitemap = document.querySelector(".navigation__container__sitemap")



        let id = url.searchParams.get("pg");

        var AjaxURL = "server-side/getFullPath.php";
        $.ajax({
            url: AjaxURL,
            data: `page_id=${id}`,
            dataType: "text",
            success: function (path) {

                let sitemap = `<p>${path}</p>`

                navigationSitemap.innerHTML = sitemap;

            }
        })




        linksWithSubMenu.forEach(linkWithSub => {
            linkWithSub.addEventListener('click', () => {
                let mainsSubLinks = document.querySelectorAll(`[data-subto='${linkWithSub.dataset.id}']`)
                let otherLinks = differentEelements(sidemenuLinks, linkWithSub);

                console.log(amountOfClicks);
                sideMenu.classList.add("first-collapsed")


                if (amountOfClicks % 2 != 0) {
                    sideMenu.classList.remove("first-collapsed")
                }

                //! REMOV OTHER LINKS ACTIVE STYLES
                otherLinks.forEach(otherLink => {
                    otherLink.classList.remove("active-link");
                })

                linksWithSubSubMenu.forEach(otherLink => {
                    otherLink.classList.remove("active-link");
                })




                //! TOGGLE ACTIVE LINK ON THE MAIN LINKS
                linkWithSub.classList.add("active-link")

                //! CLOSE THE SECOND COLAPSED SECTION
                sideMenu.classList.remove("second-collapsed")
                sideMenu.classList.remove("third-collapsed")

                allMainSubLinks.forEach(link => {
                    link.style.display = "none"
                });
                mainsSubLinks.forEach(link => {
                    link.style.display = "block"

                });

                amountOfClicks++;

            })
        });

        linksWithSubSubMenu.forEach(linkWithSubSub => {
            linkWithSubSub.addEventListener('click', () => {
                let subSubLinks = document.querySelectorAll(`[data-subto='${linkWithSubSub.dataset.id}']`)
                let otherLinks = differentEelements(linksWithSubSubMenu, linkWithSubSub);

                //! REMOV OTHER LINKS ACTIVE STYLES
                otherLinks.forEach(otherLink => {
                    otherLink.classList.remove("active-link");
                })

                sideMenu.classList.remove("third-collapsed")

                linkWithSubSub.classList.toggle("active-link")
                sideMenu.classList.toggle("second-collapsed")


                allMainSubSubLinks.forEach(link => {
                    link.style.display = "none"
                });
                subSubLinks.forEach(link => {
                    link.style.display = "block"

                });
            })
        });


        linksWithSubSubSubMenu.forEach(linkWithSubSubSub => {
            linkWithSubSubSub.addEventListener('click', () => {
                let subSubLinks = document.querySelectorAll(`[data-subto='${linkWithSubSubSub.dataset.id}']`)
                let otherLinks = differentEelements(linksWithSubSubMenu, linkWithSubSubSub);

                //! REMOV OTHER LINKS ACTIVE STYLES
                otherLinks.forEach(otherLink => {
                    otherLink.classList.remove("active-link");
                })

                linkWithSubSubSub.classList.toggle("active-link")
                sideMenu.classList.toggle("third-collapsed")


                // allMainSubSubLinks.forEach(link => {
                //     link.style.display = "none"
                // });
                subSubLinks.forEach(link => {
                    link.style.display = "block"

                });
            })
        });



        const showMoreCommentBtn = document.querySelectorAll(".post__top-commen__text__see-more")
        const hiddenOtherComments = document.querySelectorAll(".post__other-comment")




        if (sideMenu) {
            const collapsibleArrow = document.querySelectorAll(".collapsible_arrow");
            // let navigation__container__profile = localStorage.getItem('navigation__container__profile');
            let isCollapsed = localStorage.getItem('leftsidemenu_isCollapsed');
            const main = document.querySelector(".main")



            //! ADDING EVENTLISTENER FOR THE SIDEMENU COLLAPSEBLE BUTTON
            collapsibleArrow.forEach(arrow => {
                arrow.addEventListener("click", () => {
                    let collapsedLinks = arrow.nextElementSibling;



                    if (collapsedLinks.style.maxHeight) {
                        collapsedLinks.style.maxHeight = null;
                        collapsedLinks.style.marginTop = "0";
                        collapsedLinks.style.opacity = 0;
                        if (tabs) {
                            tabs.classList.add('colapsed-tabs')
                            tabs.style.width = "85% !important"
                            tabs.style.left = "5.5% !important"
                        }
                        if (main) {
                            main.style.padding = "6vh 6.5rem 0 6.5rem";
                        }
                    } else {
                        collapsedLinks.style.maxHeight = collapsedLinks.scrollHeight + 20 + "px";
                        collapsedLinks.style.marginTop = "3rem";
                        collapsedLinks.style.opacity = '1';
                        if (tabs) {
                            tabs.classList.remove('colapsed-tabs')
                        }
                        if (main) {
                            main.style.padding = "6vh 6.5rem 0 24rem";
                        } 
                    }
                });
            });



            //! IF THE LOCALHOST VERIABLE "lefsidemenu_isColapsed" APPLY THE CALSSES
            if (isCollapsed == "true") {
                // sidemenuLinks.classList.add("colapsed-links-notifications")
                sideMenu.classList.add("colapsed-sideMenu")
                collapseBtn.classList.add("colapsed-sideMenu-collapseBtn")
                logo.classList.add("colapsed-sideMenu-logo")
                sidemenuSettings.classList.add("colapsed-sideMenu-settings")
                if (tabs) {
                    tabs.classList.add('colapsed-tabs') 
                   
                }
                if (main) { 
                    main.style.padding = "6vh 6.5rem 0 6.5rem";
                }
                navigationBar.classList.add("navigation-collapsed")

            } else {
                // sidemenuLinks.classList.add("colapsed-links-notifications")
                sideMenu.classList.remove("colapsed-sideMenu")
                collapseBtn.classList.remove("colapsed-sideMenu-collapseBtn")
                logo.classList.remove("colapsed-sideMenu-logo")
                // sidemenuSettings.classList.remove("colapsed-sideMenu-settings")
                if (tabs) {
                    tabs.classList.remove('colapsed-tabs') 
                }
                if (main) {
                    main.style.padding = "6vh 6.5rem 0 23rem";
                }
                navigationBar.classList.remove("navigation-collapsed")
            }


            //! EVENT LISTENER FOR COLLAPSE BUTTON 
            collapseBtn.addEventListener("click", _ => {
                if (sideMenu.classList.contains("colapsed-sideMenu")) {
                    //! ===== FOR SIDE MENIU
                    sideMenu.classList.remove("colapsed-sideMenu")
                    collapseBtn.classList.remove("colapsed-sideMenu-collapseBtn")
                    logo.classList.remove("colapsed-sideMenu-logo")
                    // sidemenuSettings.classList.remove("colapsed-sideMenu-settings")

                    //! ===== FOR NAVIGATION BAR
                    navigationBar.classList.remove("navigation-collapsed")
                    if (tabs) {
                        tabs.classList.add('colapsed-tabs')
                    }
                    if (main) {
                        main.style.padding = "6vh 6.5rem 0 23rem";
                    }


                    localStorage.clear()
                    localStorage.setItem('leftsidemenu_isCollapsed', false);
                } else {
                    //! ===== FOR SIDE MENIU
                    sideMenu.classList.add("colapsed-sideMenu")
                    collapseBtn.classList.add("colapsed-sideMenu-collapseBtn")
                    logo.classList.add("colapsed-sideMenu-logo")
                    sidemenuSettings.classList.add("colapsed-sideMenu-settings")

                    //! ===== FOR NAVIGATION BAR
                    navigationBar.classList.add("navigation-collapsed")
                    if (tabs) {
                        tabs.classList.remove('colapsed-tabs')
                    }
                    if (main) {
                        main.style.padding = "6vh 6.5rem 0 6.5rem";
                    }


                    localStorage.clear()
                    localStorage.setItem('leftsidemenu_isCollapsed', true);
                }
            })




        }


    }, 500);
}












const differentEelements = (array, element) => {
    const others = [...array];
    const notselected = others.filter(el => el != element);
    return notselected;
}



