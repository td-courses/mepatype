<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

header('Content-Type: application/json');
$start_time = $_REQUEST['start'];
$end_time   = $_REQUEST['end'].' 23:59';
$agent      = $_REQUEST['agent'];
$queue      = $_REQUEST['queuet'];

$quantity   = array();
$cause      = array();
$cause1     = array();

$name       = array();
$agentt     = array();

$call_count = array();
$name[]     = '';


//-----------------------ნაპასუხები ზარები ოპერატორების მიხედვით
if($_REQUEST['act']=='answer_call_operator'){
	$db->setQuery(" SELECT   asterisk_call_log.source AS `agent`,
                             COUNT(*) AS `count`
                    FROM     asterisk_call_log
                    WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 13
                    AND      asterisk_call_log.source IN($agent)
                    AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                    GROUP BY asterisk_call_log.source");
	
	$ress = $db->getResultArray();
	foreach($ress[result] AS $row1){
	    $call_count[] = (float)$row1[count];
    	$agentt[]	  = $row1[agent];
    }
}
//------------------------------ ნაპასუხები ზარები კვირის დღეების მიხედვით
if($_REQUEST['act']=='answer_call_week'){
		$db->setQuery(" SELECT   CASE
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 1 THEN 'კვირა'
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 2 THEN 'ორშაბათი'
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 3 THEN 'სამშაბათი'
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 4 THEN 'ოთხშაბათი'
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 5 THEN 'ხუთშაბათი'
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 6 THEN 'პარასკევი'
									WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 7 THEN 'შაბათი'
								 END AS `date`,
                                 COUNT(*) AS `answer_count1`
                        FROM     asterisk_call_log
                        WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 13
                        AND      asterisk_call_log.source IN($agent)
                        AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                        GROUP BY DAYOFWEEK(FROM_UNIXTIME(call_datetime))");
		
		$res3 = $db->getResultArray();
		foreach($res3[result] AS $row3){
            $answer_count1[] = (float)$row3[answer_count1];
    		$datetime1[]	 = $row3[date];
    	}
    }
	
	//------------------------------ უპასუხო ზარები კვირის დღეების მიხედვით
if($_REQUEST['act']=='unanswer_call_week'){	
	$db->setQuery(" SELECT   CASE
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 1 THEN 'კვირა'
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 2 THEN 'ორშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 3 THEN 'სამშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 4 THEN 'ოთხშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 5 THEN 'ხუთშაბათი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 6 THEN 'პარასკევი'
								WHEN DAYOFWEEK(FROM_UNIXTIME(call_datetime)) = 7 THEN 'შაბათი'
							 END AS `date`,
                             COUNT(*) AS `unanswer_count`
                    FROM     asterisk_call_log
                    WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 12
                    AND      asterisk_call_log.source IN($agent)
                    AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                    GROUP BY DAYOFWEEK(FROM_UNIXTIME(call_datetime))");
	
	$res10 = $db->getResultArray();
	foreach($res10[result] AS $row10){
	    $unanswer_call2[] = (float)$row10[unanswer_count];
		$date1[]		  = $row10[date];
	}
}


//------------------------------ უპასუხო ზარები დღეების მიხედვით
if($_REQUEST['act']=='unanswer_call_day'){
       $db->setQuery("  SELECT   DATE(FROM_UNIXTIME(call_datetime)) AS `datetime`,
						         COUNT(*) AS `unanswer_call`
                        FROM     asterisk_call_log
                        WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 12
                        AND      asterisk_call_log.source IN($agent)
                        AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                        GROUP BY DATE(FROM_UNIXTIME(call_datetime))");
		
        $res8 = $db->getResultArray();
        foreach($res8[result] AS $row8){
            $unanswer_call[] = (float)$row8[unanswer_call];
        	$times[]		 = $row8[datetime];
        }
}
		
//------------------------------ ნაპასუხები ზარები დღეების მიხედვით		
if($_REQUEST['act']=='answer_call_day'){
    $db->setQuery(" SELECT   DATE(FROM_UNIXTIME(call_datetime)) AS `datetime`,
					         COUNT(*) AS `answer_count2`
                    FROM     asterisk_call_log
                    WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 13
                    AND      asterisk_call_log.source IN($agent)
                    AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                    GROUP BY DATE(FROM_UNIXTIME(call_datetime))");
    
    $res4 = $db->getResultArray();
    foreach($res4[result] AS $row4){

	$answer_count2[] = (float)$row4[answer_count2];
	$datetime2[]	 = $row4[datetime];
	}
}
//------------------------------ უპასუხო ზარები საათების მიხედვით
if($_REQUEST['act']=='unanswer_call_hour'){			
	$db->setQuery("SELECT   CASE		
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 0 AND HOUR(FROM_UNIXTIME(call_datetime)) < 1 THEN '00:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 1 AND HOUR(FROM_UNIXTIME(call_datetime)) < 2 THEN '01:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 2 AND HOUR(FROM_UNIXTIME(call_datetime)) < 3 THEN '02:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 3 AND HOUR(FROM_UNIXTIME(call_datetime)) < 4 THEN '03:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 4 AND HOUR(FROM_UNIXTIME(call_datetime)) < 5 THEN '04:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 5 AND HOUR(FROM_UNIXTIME(call_datetime)) < 6 THEN '05:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 6 AND HOUR(FROM_UNIXTIME(call_datetime)) < 7 THEN '06:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 7 AND HOUR(FROM_UNIXTIME(call_datetime)) < 8 THEN '07:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 8 AND HOUR(FROM_UNIXTIME(call_datetime)) < 9 THEN '08:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 9 AND HOUR(FROM_UNIXTIME(call_datetime)) < 10 THEN '09:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 10 AND HOUR(FROM_UNIXTIME(call_datetime)) < 11 THEN '10:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 11 AND HOUR(FROM_UNIXTIME(call_datetime)) < 12 THEN '11:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 12 AND HOUR(FROM_UNIXTIME(call_datetime)) < 13 THEN '12:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 13 AND HOUR(FROM_UNIXTIME(call_datetime)) < 14 THEN '13:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 14 AND HOUR(FROM_UNIXTIME(call_datetime)) < 15 THEN '14:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 15 AND HOUR(FROM_UNIXTIME(call_datetime)) < 16 THEN '15:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 16 AND HOUR(FROM_UNIXTIME(call_datetime)) < 17 THEN '16:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 17 AND HOUR(FROM_UNIXTIME(call_datetime)) < 18 THEN '17:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 18 AND HOUR(FROM_UNIXTIME(call_datetime)) < 19 THEN '18:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 19 AND HOUR(FROM_UNIXTIME(call_datetime)) < 20 THEN '19:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 20 AND HOUR(FROM_UNIXTIME(call_datetime)) < 21 THEN '20:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 21 AND HOUR(FROM_UNIXTIME(call_datetime)) < 22 THEN '21:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 22 AND HOUR(FROM_UNIXTIME(call_datetime)) < 23 THEN '22:00'
								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 23 AND HOUR(FROM_UNIXTIME(call_datetime)) < 24 THEN '23:00'
                            END AS `times`,
							COUNT(*) AS `unanswer_count`
                   FROM     asterisk_call_log
                   WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 12
                   AND      asterisk_call_log.source IN($agent)
                   AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                   GROUP BY HOUR(FROM_UNIXTIME(call_datetime))");
	
	$res9 = $db->getResultArray();
	foreach($res9[result] AS $row9){
	    $unanswer_count1[] = (float)$row9[unanswer_count];
		$times2[]		   = $row9[times];
	}
}
	//------------------------------ ნაპასუხები ზარები საათების მიხედვით
if($_REQUEST['act']=='answer_call_hour'){
    	$db->setQuery("SELECT   CASE		
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 0 AND HOUR(FROM_UNIXTIME(call_datetime)) < 1 THEN '00:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 1 AND HOUR(FROM_UNIXTIME(call_datetime)) < 2 THEN '01:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 2 AND HOUR(FROM_UNIXTIME(call_datetime)) < 3 THEN '02:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 3 AND HOUR(FROM_UNIXTIME(call_datetime)) < 4 THEN '03:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 4 AND HOUR(FROM_UNIXTIME(call_datetime)) < 5 THEN '04:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 5 AND HOUR(FROM_UNIXTIME(call_datetime)) < 6 THEN '05:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 6 AND HOUR(FROM_UNIXTIME(call_datetime)) < 7 THEN '06:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 7 AND HOUR(FROM_UNIXTIME(call_datetime)) < 8 THEN '07:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 8 AND HOUR(FROM_UNIXTIME(call_datetime)) < 9 THEN '08:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 9 AND HOUR(FROM_UNIXTIME(call_datetime)) < 10 THEN '09:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 10 AND HOUR(FROM_UNIXTIME(call_datetime)) < 11 THEN '10:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 11 AND HOUR(FROM_UNIXTIME(call_datetime)) < 12 THEN '11:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 12 AND HOUR(FROM_UNIXTIME(call_datetime)) < 13 THEN '12:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 13 AND HOUR(FROM_UNIXTIME(call_datetime)) < 14 THEN '13:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 14 AND HOUR(FROM_UNIXTIME(call_datetime)) < 15 THEN '14:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 15 AND HOUR(FROM_UNIXTIME(call_datetime)) < 16 THEN '15:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 16 AND HOUR(FROM_UNIXTIME(call_datetime)) < 17 THEN '16:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 17 AND HOUR(FROM_UNIXTIME(call_datetime)) < 18 THEN '17:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 18 AND HOUR(FROM_UNIXTIME(call_datetime)) < 19 THEN '18:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 19 AND HOUR(FROM_UNIXTIME(call_datetime)) < 20 THEN '19:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 20 AND HOUR(FROM_UNIXTIME(call_datetime)) < 21 THEN '20:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 21 AND HOUR(FROM_UNIXTIME(call_datetime)) < 22 THEN '21:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 22 AND HOUR(FROM_UNIXTIME(call_datetime)) < 23 THEN '22:00'
    								WHEN HOUR(FROM_UNIXTIME(call_datetime)) >= 23 AND HOUR(FROM_UNIXTIME(call_datetime)) < 24 THEN '23:00'
                                END AS `times`,
    							COUNT(*) AS `answer_count`
                       FROM     asterisk_call_log
                       WHERE    asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 13
                       AND      asterisk_call_log.source IN($agent)
                       AND      asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start_time') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end_time')
                       GROUP BY HOUR(FROM_UNIXTIME(call_datetime))");
    	$res2 = $db->getResultArray();
    	foreach($res2[result] AS $row2){
            $answer_count[] = (float)$row2[answer_count];
    		$datetime[] 	= $row2[times];
		}
}

							
$unit[]="  ზარი";
$series[] = array('name' => $name, 'unit' => $unit, 'quantity' => $quantity, 'cause' => $cause);
$series[] = array('name' => $name, 'unit' => $unit, 'call_count' => $call_count, 'agent' => $agentt);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count' => $answer_count, 'datetime' => $datetime);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count1' => $answer_count1, 'datetime1' => $datetime1);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count2' => $answer_count2, 'datetime2' => $datetime2);
$series[] = array('name' => $name, 'unit' => $unit, 'answer_count3' => $answer_count3, 'cause1' => $cause1);
$series[] = array('name' => $name, 'unit' => $unit, 'count1' => $count1, 'queue1' => $queue1);
$series[] = array('name' => $name, 'unit' => $unit, 'count2' => $count2, 'queue2' => $queue2);
$series[] = array('name' => $name, 'unit' => $unit, 'unanswer_call' => $unanswer_call, 'times' => $times);
$series[] = array('name' => $name, 'unit' => $unit, 'unanswer_count1' => $unanswer_count1, 'times2' => $times2);
$series[] = array('name' => $name, 'unit' => $unit, 'unanswer_count2' => $unanswer_call2, 'date1' => $date1);
$series[] = array('name' => $name, 'unit' => $unit, 'mas' => $mas, 'call_second' => $call_second);

echo json_encode($series);

?>