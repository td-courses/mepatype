<?php

use Routers\dbClass;

class Incomming extends dbClass{

    private $colCount;
    private $cols;

    public function getList(){
        $this->colCount = $_REQUEST['count'];
        $this->cols     = $_REQUEST['cols'];
        parent::setQuery("  SELECT	incomming_call.id,
                                    incomming_call.date,
                                    user_info.name,
                                    CASE
                                        WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN incomming_call.phone
                                        WHEN NOT ISNULL(incomming_call.chat_id) THEN chat.sender_name
                                    END AS client,
				                    processing.`value`->'$.inputs.abonenti___079',
                                    processing.`value`->'$.inputs.abonentis_nomeri___019',
                                    '' AS centr,
                                    info_category.name AS cat,
                                    dir_momartvis_info_newinputID_136.`name`


                                    
                                    

                            FROM 		incomming_call
                            LEFT JOIN	users ON users.id = incomming_call.user_id
                            LEFT JOIN	user_info ON user_info.user_id = users.id
                            LEFT JOIN	chat ON chat.id = incomming_call.chat_id
                            LEFT JOIN processing ON processing.processing_page_id = 1 AND processing.row_id = incomming_call.id
                            LEFT JOIN dir_momartvis_info_newinputID_136 ON dir_momartvis_info_newinputID_136.id = processing.`value`->'$.selectors.reagireba_statusi_'
                            LEFT JOIN   info_category ON info_category.id = processing.`value`->'$.multilevel.zaris_kategoriebi___594___1'
                            WHERE 	incomming_call.actived = 1

                            ORDER BY id DESC

                            LIMIT 200");
        $callList = parent::getKendoList($this->colCount, $this->cols);

        return $callList;
    }
}

?>