<?php

include('../../includes/classes/class.Mysqli.php');
require_once('../../includes/classes/dialog_automator.class.php');
global $db;
$db = new dbClass();

$action 	= $_REQUEST['act'];
$error		= '';
$data		= '';

//incomming
$incom_id	= $_REQUEST['id'];
$tab_id	    = $_REQUEST['tab_id'];

$start 	= 	$_REQUEST['start'];
$end 	= 	$_REQUEST['end'];

if($start != '' && $end != ''){
    //$start = $db->convertDateTime($start);
    //$end = $db->convertDateTime($end);
}
switch ($action) {
    case 'get_add_page':
        $user_id   = $_SESSION[USERID];
        $id 	   = $_REQUEST['id'];
        $count_sum = $_REQUEST['count_sum'];
        
        $db->setQuery("SELECT  monitoring_shablon.id 
                       FROM    monitoring_shablon 
                       WHERE   monitoring_shablon.actived = 1");
        
        $res        = $db->getResultArray();
        $shablon_id = $res[result][0][id];
        
        if ($id == '') {
            if ($count_sum > 0) {
                $count = $count_sum;
            }else{
                $count = 0;
            }
            $db->setQuery("INSERT INTO `monitoring_main` (`user_id`) VALUES (1)");
            $db->execQuery();
            
            $incrment_main_id = $db->getLastId();
            
            $db->setQuery("DELETE FROM monitoring_main WHERE id = '$incrment_main_id'");
            $db->execQuery();
            
            $db->setQuery("SELECT   user_info.`name`, 
                    			    NOW() AS `datetime`, 
                    			    CONCAT(DATE(NOW()),' 00:00') AS `start`, 
                    			    NOW() AS `end`  
                            FROM    users 
                            JOIN    user_info ON user_info.user_id = users.id
                            where   users.id = '$user_id'");
            
            $req         = $db->getResultArray();
            $user_curent = $req[result][0];
            
            if ($start == '') {
                $start       = $user_curent[start];
                $end         = $user_curent[end];
            }
            
            $former      = $user_curent[name];
            $form_date   = $user_curent[datetime];
            
            calculation_monitoring_base_caunt($start, $end, $id, $count, $incrment_main_id);
            
            $page = calculation_monitoring_page($start, $end, $id, $incrment_main_id, $shablon_id);
            
            $data = array('page' => $page, 'former' => $former, 'former_date' => $form_date, 'start' => $start, 'end' => $end);
        }else{
            
            $db->setQuery(" SELECT    COUNT(*) all_count,
                                      monitoring_main.start_date,
                                      monitoring_main.end_date,
                                      monitoring_main.all_request AS all_count_req,
                                      monitoring_main.daformirebuli,
                                      monitoring_main.dasaformirebeli,
                                      SUM(IF(asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 60,1,0)) AS `ert_wutamde`,
                            	      SUM(IF(asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 120,1,0)) AS `or_wutamde`,
                            	      SUM(IF(asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 180,1,0)) AS `sam_wutamde`,
                            	      SUM(IF(asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 240,1,0)) AS `otx_wutamde`,
                            	      SUM(IF(asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 300,1,0)) AS `xut_wutamde`,
                            	      SUM(IF(asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 600,1,0)) AS `at_wutamde`,
                            	      SUM(IF(asterisk_call_log.talk_time >= 900,1,0)) AS `txutme_meti`,
                                      user_info.name AS `former`,
                                      monitoring_main.datetime AS `former_date` 
                            FROM      monitoring
                            LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                            JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                            LEFT JOIN user_info ON monitoring_main.user_id = user_info.user_id
                            WHERE     monitoring.call_type_id > 0 AND monitoring.operator_id > 0
                            AND       monitoring_main.id = '$id'");
            
            $res          = $db->getResultArray();
            $req          = $res[result][0];
            $sum_duration = $req[ert_wutamde]+$req[or_wutamde]+$req[sam_wutamde]+$req[otx_wutamde]+$req[xut_wutamde]+$req[at_wutamde]+$req[txutme_meti];
            $page = GetPage_gasanawilebeli($req, $id, $id, $sum_duration);
            
            $data = array('page' => $page, 'former' => $req[former], 'former_date' => $req[former_date], 'start' => $req[start_date], 'end' => $req[end_date]);
        }
        
        
        
        break;
        
    case 'get_edit_page':
        $id     = $_REQUEST['id'];
        $tab_id = $_REQUEST['tab_id'];
        $operator_id = $_SESSION["USERID"];
        $db->setQuery("UPDATE monitoring set seen = 1 WHERE id = $id AND operator_id = $operator_id AND seen = 0");
        $db->execQuery();
        $page   = GetPage(Getincomming($id), $tab_id);
        $data   = array('page'	=> $page);
        
        break;
    case 'get_columns':
        
        $columnCount 	= 	$_REQUEST['count'];
        $cols[] 		=	$_REQUEST['cols'];
        $columnNames[] 	= 	$_REQUEST['names'];
        $operators[] 	= 	$_REQUEST['operators'];
        $selectors[] 	= 	$_REQUEST['selectors'];
        $lockedArr[] 	= 	$_REQUEST['locked'];
        $lockableArr[]	= 	$_REQUEST['lockable'];
        
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);
            
            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        
        $i = 0;
        $columns = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $i++;
        }
        
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
            {
                continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                if($lockedArr[0][$a] == '1') {
                    $locked = true;
                }else {
                    $locked = false;
                }
                if($lockableArr[0][$a] == '1') {
                    $lockable = true;
                }else {
                    $lockable = false;
                }
                
                $op = false;
                
                if($columns[$j] == 'id'){
                    $hidden = true;
                }else{
                    $hidden = false;
                }
                
                $width = 'auto';
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j], 'hidden' => $hidden,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' => ["MM/dd/yyyy h:mm:ss"], 'locked' => $locked[0][$a], 'lockable' => $lockable, 'width' => 150);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j], 'hidden' => $hidden,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]), 'locked' => $locked, 'lockable' => $lockable, 'width' => 150);
                }
                else
                {
                    $g = array('field'=>$columns[$j], 'hidden' => $hidden,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true,'dataSource' => json_decode('this.dataSource')), 'locked' => $locked, 'width' => 150);
                }
                $a++;
            }
            array_push($dat,$g);
        }
        
        $new_data = array();
        for($j=0;$j<$columnCount;$j++)
        {
            if($res['data_type'][$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        
        $filtArr = array('fields'=>$new_data);
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
        $data = $kendoData;
        
        break;
    case 'get_shesamowmebeli_result':
        
        //$page = get_shesamowmebeli_result();
        $data = array('page' => '');
        
        break;
    case 'get_shemowmebuli_result':
        $start_date = $_REQUEST['start_date'];
        $end_date   = $_REQUEST['end_date'];
        
        //$page = get_shemowmebuli_result($start_date, $end_date);
        $data = array('page' => '');
        
        break;
    case 'get_gasachivrebuli_result':
        
        //$page = get_gasachivrebuli_result();
        $data = array('page' => '');
        
        break;
    case 'get_monitoring_request_redact':
        $user_id       = $_SESSION[USERID];
        $id 	       = $_REQUEST['id'];
        $start 	       = $_REQUEST['start'];
        $end 	       = $_REQUEST['end'];
        $type 	       = $_REQUEST['type'];
        $request_count = $_REQUEST['input_value'];
        
        
        $db->setQuery("SELECT GROUP_CONCAT(monitoring_temp.id) AS `id` 
                       FROM   monitoring_temp
                       JOIN   monitoring ON monitoring_temp.monitoring_id = monitoring.id
                       WHERE  monitoring.call_type_id = '$type' AND monitoring_temp.monitoring_main_id = '$id'");
        
        $req = $db->getResultArray();
        $ids = $req[result][0];
        
        $db->setQuery("DELETE FROM monitoring_temp WHERE id IN($ids[id])");
        $db->execQuery();
        
        $db->setQuery("SELECT id
                       FROM   monitoring
                       WHERE  FROM_UNIXTIME(monitoring.request_date) BETWEEN '$start' AND '$end'
                       AND    ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = $type
                       AND    monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                       LIMIT  $request_count");
        
        $res_type_ids = $db->getResultArray();
        
        foreach($res_type_ids[result] AS $row) {
            $values .='('.$id.','.$row[id].', 2),';
        }
        
        $values = substr($values, 0, -1);
        
        $db->setQuery("INSERT INTO monitoring_temp
                                  (monitoring_main_id, monitoring_id, distribution_type)
                            VALUES
                                   $values");
        $db->execQuery();
        $incrment_main_id = $id;
        $id = '';
        $page = calculation_monitoring_page($start, $end, $id, $incrment_main_id, $shablon_id);
        
        $data = array('page' => $page);
        
        break;
        
    case 'get_monitoring_request_checkbox':
        $user_id       = $_SESSION[USERID];
        $id 	       = $_REQUEST['id'];
        $start 	       = $_REQUEST['start'];
        $end 	       = $_REQUEST['end'];
        $checkbox_type = $_REQUEST['checkbox_type'];
        $type_id 	   = $_REQUEST['type_id'];
        $value         = $_REQUEST['input_value'];
        $check_checked = $_REQUEST['check_checked'];
        
        if ($checkbox_type == 'monitoring_call_type_checkbox') {
            $filter = "AND monitoring.call_type_id = '$type_id'";
        }else if($checkbox_type == 'call_department_checkbox'){
            $filter = "AND monitoring.department_id = '$type_id'";
        }else if($checkbox_type == 'monitoring_source_checkbox'){
            $filter = "AND monitoring.source_id = '$type_id'";
        }else if($checkbox_type == 'monitoring_action_checkbox'){
            $filter = "AND monitoring.action_type_id = '$type_id'";
        }else if($checkbox_type == 'radio_input_task_time'){
            if ($check_checked == 1) {
                if ($type_id > 10) {
                    $filter = "AND source_id = 1 AND asterisk_call_log.talk_time <= '900'";
                }else{
                    $duration = $type_id*60;
                    $filter = "AND source_id = 1 AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time >= '$duration'";
                }
                
                $db->setQuery("SELECT    GROUP_CONCAT(monitoring_temp.id) AS `id`
                               FROM      monitoring_temp
                               JOIN      monitoring ON monitoring_temp.monitoring_id = monitoring.id
                               LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                               WHERE     monitoring_temp.actived = 1 AND monitoring_temp.monitoring_main_id = '$id' $filter");
                
                $req = $db->getResultArray();
                $ids = $req[result][0];
                if ($ids[id] != '') {
                    $db->setQuery("UPDATE monitoring_temp
                                      SET actived = 0
                                   WHERE  id IN($ids[id])");
                    $db->execQuery();
                }
            }
        }
        
        if ($check_checked == 1 && $checkbox_type != 'radio_input_task_time') {
            
            $db->setQuery("UPDATE monitoring_temp
                           JOIN   monitoring ON monitoring_temp.monitoring_id = monitoring.id
                              SET monitoring_temp.actived = 1
                           WHERE  monitoring_temp.actived = 0
                           AND    monitoring_temp.monitoring_main_id = '$id' $filter");
            $db->execQuery();
            
        }else if($check_checked == 0 && $checkbox_type != 'radio_input_task_time'){

                $db->setQuery("UPDATE monitoring_temp
                               JOIN   monitoring ON monitoring_temp.monitoring_id = monitoring.id
                                  SET monitoring_temp.actived = 0
                               WHERE  monitoring_temp.actived = 1 AND monitoring_temp.monitoring_main_id = '$id' $filter");
                $db->execQuery();
            
        }
        
        $incrment_main_id = $id;
        $id = '';
        $page = calculation_monitoring_page($start, $end, $id, $incrment_main_id, $shablon_id);
        
        $data = array('page' => $page);
        
        break;
    case 'get_monitoring_request_filter':
        $user_id = $_SESSION[USERID];
        $id 	 = $_REQUEST['id'];
        $start 	 = $_REQUEST['start'];
        $end 	 = $_REQUEST['end'];
        $page    = calculation_monitoring_base($start, $end, $id);
        
        
        $data = array('page' => $page);
        
        break;
    case 'get_tabs' :
        
        $page		= get_tabs();
        $data		= array('page'	=> $page);
        break;
    case 'get_table_detail' :
        
        $page = get_table_detail();
        $data = array('page'	=> $page);
        break;
    case 'get_add_page_gasachivreba' :
        
        $page = get_gasachivreba();
        $data = array('page' => $page);
        break;
    case 'get_list_gasanawilebeli' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $start_date = $_REQUEST['start_date'];
        $end_date    = $_REQUEST['end_date'];
        $db->setQuery("SELECT    monitoring_main.id,
                			     monitoring_main.datetime,
                			     COUNT(*),
                			     COUNT(*) AS 'shesafasebeli',
                			     CONCAT(ROUND(COUNT(*)/COUNT(*)*100),'%'),
                			     SUM(IF(monitoring.monitoring_status_id IN(1,2,3),1,0)),
                			     ROUND(SUM(IF(monitoring.monitoring_status_id IN(1,2,3) && monitoring.check_appeal_status = 0, 1, 0))/COUNT(*)*100,2) AS `shefasebuli_procenti`
                        FROM     monitoring_main
                        JOIN     monitoring ON monitoring_main.id = monitoring.monitoring_main_id
                        AND      monitoring_main.datetime BETWEEN '$start_date' AND '$end_date'
                        AND      monitoring.operator_id > 0
                        GROUP BY monitoring_main.id");
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        
        break;

        case "monitoring_table_operator":
           $start = $_REQUEST['start'];
           $end = $_REQUEST['end'];
           
           $db->setQuery("  SELECT    users.id,user_info.`name`,
                    				  IFNULL(ROUND(SUM(monitoring_rate.question_level)/COUNT(DISTINCT monitoring.id),2),'0.00') AS `k`,
                    				  users_point.point AS `k2`
                            FROM      monitoring
                            JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                            JOIN      monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                            JOIN      users ON users.id = monitoring.operator_id
                            JOIN      user_info ON users.id = user_info.user_id
                            LEFT JOIN users_point ON users.id = users_point.users_id
                            WHERE     monitoring.monitoring_main_id > 0 AND monitoring.monitoring_main_id > 0
                            AND       monitoring.actived = 1 AND monitoring.operator_id > 0 AND DATE(monitoring_main.datetime) BETWEEN '$start' AND '$end'
                            GROUP BY  monitoring.operator_id ");
           
            $res = $db->getResultArray();
            $names = array();
            $k = array();
            $k1 = array();
            $k2 = array();
            foreach($res[result] AS $arr){
                $names[$arr['id']] = $arr['name'];
                $k[$arr['id']] = $arr['k'];
                $k1[$arr['id']] = 100;
                $k2[$arr['id']] = $arr['k2'];
            }

            $data = array("names" => $names, "k" => $k, "k1"=> $k1, "k2" => $k2);
        break;
    case 'get_list_monitoring_detail_info' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $id          = $_REQUEST['id'];
        
        
        
        $db->setQuery(" SELECT   users.id,
                                 DATE(monitoring_main.datetime),
                    		     user_info.`name`,
                    		     COUNT(*),
                    		     ROUND(COUNT(*)/100*100,2),
                    		     SUM(IF(monitoring.monitoring_status_id IN(1,2,3),1,0)),
                    		     ROUND(SUM(IF(monitoring.monitoring_status_id IN(1,2,3) && monitoring.check_appeal_status = 0,1,0))/COUNT(*)*100,2)
                        FROM     monitoring_main
                        JOIN     monitoring ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN     users ON users.id = monitoring.monitoring_user_id
                        JOIN     user_info ON users.id = user_info.user_id
                        AND      monitoring_main.id = '$id' AND monitoring.operator_id > 0
                        GROUP BY monitoring.monitoring_user_id");
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        break;
    case 'get_list_shesafasebeli' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $start_datem = $_REQUEST['start_date'];
        $user_id = $_SESSION[USERID];

        $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
        $user_info = $db->getResultArray();
        $request_distribution_type = $_REQUEST['request_distribution_type'];
        $request_type              = $_REQUEST['request_type'];
        $request_source            = $_REQUEST['request_source'];
        $request_action            = $_REQUEST['request_action'];
        $request_duration          = $_REQUEST['request_duration'];
        $request_operators         = $_REQUEST['request_operators'];
        
        $filt   = '';
        $filt1  = '';
        $filt2  = '';
        $filt3  = '';
        $filt4  = '';
        $filt5  = '';
        $filter = '';
        
        if ($user_info[result][0][group_id] != 31 && $user_info[result][0][group_id] != 47) {
            if ($user_info[result][0][group_id] == 48) {
                $filter = "AND monitoring.monitoring_user_id = '$user_id'";
            }else{
                $filter = "AND (monitoring.operator_id = '$user_id' OR monitoring.monitoring_user_id = '$user_id')";
            }
        }
//         if ($request_distribution_type != '') {
//             $filt = " AND monitoring.distribution_type NOT IN($request_distribution_type)";
//         }
        
//         if ($request_type != '') {
//             $filt1 = " AND monitoring.call_type_id NOT IN($request_type)";
//         }
        
//         if ($request_source != '') {
//             $filt2 = " AND monitoring.source_id NOT IN($request_source)";
//         }
        
//         if ($request_action != '') {
//             $filt3 = " AND monitoring.action_type_id NOT IN($request_action)";
//         }
        
//         if ($request_duration != '') {
//             if ($request_duration <= 10) {
//                 $filt4 = " AND monitoring.call_duration < $request_duration";
//             }else{
//                 $filt4 = " AND (monitoring.call_duration > $request_duration OR AND monitoring.call_duration = 0)";
//             }
            
//         }
        
//         if ($request_operators != '') {
//             $filt5 = " AND monitoring.operator_id NOT IN($request_operators)";
//         }
        
        $db->setQuery(" SELECT    monitoring.id,
                                  monitoring_main.datetime,
                                  FROM_UNIXTIME(monitoring.request_date),
                                  source.`name`,
                                  mepa_project_types.`name`,
                                  monitoring_action_type.`name`,
                                  SEC_TO_TIME(asterisk_call_log.talk_time),
                                  user_info.`name`,
                                  IF(monitoring.distribution_type=2,'მანუალური','ავტომატური'),
                                  IF(ISNULL(monitoring.monitoring_user_id),'გასანაწილებელი','განაწილებელი'),
                                  SUM(IF(monitoring_rate.question_level<0,0,monitoring_rate.question_level)),
                                  monitoring_user.`name`
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      source ON monitoring.source_id = source.id
                        LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                        LEFT JOIN mepa_project_types ON monitoring.call_type_id = mepa_project_types.id
                        LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                        LEFT JOIN monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                        JOIN      user_info ON monitoring.operator_id = user_info.user_id
                        JOIN      user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                        WHERE     monitoring.monitoring_status_id = 0 AND monitoring.operator_id > 0 $filter
                        GROUP BY monitoring.id");
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        break;
    case 'get_list_shemowmebuli' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $start_datem = $_REQUEST['start_date'];
        
        $start_date = $_REQUEST['start_date'];
        $end_date   = $_REQUEST['end_date'];
        $number_dasrulebuli = $_REQUEST['number_dasrulebuli'];

        $user_id = $_SESSION[USERID];
        if($user_id == 149 && ($start_date < '2021-06-01 00:00' || $end_date > '2021-07-31 23:59')){
            global $error;
            $error = "თქვენ იუზერს არ აქვს წვდომა დროის ამ მონაკვეთზე";
            break;
        }
        

        if($number_dasrulebuli != ''){
            $number_dasrulebuli_filter = " AND incomming_call.phone = '$number_dasrulebuli' ";
        }
        
        $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
        $user_info = $db->getResultArray();
        
        $request_distribution_type = $_REQUEST['request_distribution_type'];
        $request_type              = $_REQUEST['request_type'];
        $request_source            = $_REQUEST['request_source'];
        $request_action            = $_REQUEST['request_action'];
        $request_duration          = $_REQUEST['request_duration'];
        $request_operators         = $_REQUEST['request_operators'];
        
        $filt  = '';
        $filt1 = '';
        $filt2 = '';
        $filt3 = '';
        $filt4 = '';
        $filt5 = '';
        
        if ($request_distribution_type != '') {
            $filt = " AND monitoring.distribution_type NOT IN($request_distribution_type)";
        }
        
        if ($request_type != '') {
            $filt1 = " AND monitoring.call_type_id NOT IN($request_type)";
        }
        
        if ($request_source != '') {
            $filt2 = " AND monitoring.source_id NOT IN($request_source)";
        }
        
        if ($request_action != '') {
            $filt3 = " AND monitoring.action_type_id NOT IN($request_action)";
        }
        
        if ($request_duration != '') {
            if ($request_duration <= 10) {
                $filt4 = " AND monitoring.call_duration < $request_duration";
            }else{
                $filt4 = " AND (monitoring.call_duration > $request_duration OR AND monitoring.call_duration = 0)";
            }
            
        }
        
        if ($request_operators != '') {
            $filt5 = " AND monitoring.operator_id NOT IN($request_operators)";
        }
        
        $filter = '';
        
        $group = $user_info[result][0][group_id];
        if ($user_info[result][0][group_id] != 31 && $user_info[result][0][group_id] != 47 && $user_id != 149 ) {
            $filter = "AND (monitoring.operator_id = '$user_id' OR monitoring.monitoring_user_id = '$user_id')";
        }
        
        $db->setQuery(" SELECT    monitoring.id,
                                  monitoring_main.datetime,
                                  FROM_UNIXTIME(monitoring.request_date),
                                  source.`name`,
                                  mepa_project_types.`name`,
                                  incomming_call.phone,
                                  monitoring_action_type.`name`,
                                  SEC_TO_TIME(asterisk_call_log.talk_time),
                                  user_info.`name`,
                                  IF(monitoring.distribution_type=2,'მანუალური','ავტომატური'),
                                  'შემოწმებული',
                                  SUM(IF(monitoring_rate.question_level<0,0,monitoring_rate.question_level)),
                                  GROUP_CONCAT(DISTINCT SUBSTRING_INDEX(monitoring_question.question,'.',1)),
                                  IF($group='34', '', monitoring_user.`name`)
                                  
                        FROM      monitoring
                        LEFT JOIN incomming_call ON incomming_call.id = monitoring.incomming_call_id
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      source ON monitoring.source_id = source.id
                        LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                        LEFT JOIN mepa_project_types ON monitoring.call_type_id = mepa_project_types.id
                        LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                        JOIN      monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                        LEFT JOIN monitoring_rate AS `errored_rate` ON errored_rate.id = monitoring_rate.id AND errored_rate.question_level < 0
                        LEFT JOIN monitoring_question  ON errored_rate.question_id = monitoring_question.id
                        JOIN      user_info ON monitoring.operator_id = user_info.user_id
                        JOIN      user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                        WHERE     monitoring.monitoring_status_id = 1 AND monitoring.operator_id > 0
                        AND       monitoring.rate_date BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') $filter $filt $filt1 $filt2 $filt3 $filt4 $filt5 $number_dasrulebuli_filter
                        GROUP BY  monitoring.id");
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        break;
    case 'get_list_gasachivrebuli' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        
        
        $data  = '';
        
        $user_id = $_SESSION[USERID];
        
        $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
        
        $user_info = $db->getResultArray();
        
        $filter = '';
        $group = $user_info[result][0][group_id];
        if ($user_info[result][0][group_id] != 31 && $user_info[result][0][group_id] != 46 && $user_info[result][0][group_id] != 47) {
            $filter = "AND (monitoring.operator_id = '$user_id' OR monitoring.monitoring_user_id = '$user_id')";
        }else if ($user_info[result][0][group_id] == 46){
            $filter = "AND monitoring_appeal.manager_id = $user_id";
        }
        
        $db->setQuery(" SELECT    monitoring.id,
                                  monitoring_main.datetime,
                                  FROM_UNIXTIME(monitoring.request_date),
                                  source.`name`,
                                  mepa_project_types.`name`,
                                  monitoring_action_type.`name`,
                                  SEC_TO_TIME(asterisk_call_log.talk_time),
                                  user_info.`name`,
                                  IF(monitoring.distribution_type=2,'მანუალური','ავტომატური'),
                                  'გასაჩივრებული',
                                  SUM(IF(monitoring_rate.question_level<0,0,monitoring_rate.question_level)),
                                  IF($group='34', '', monitoring_user.`name`),
                                  IFNULL(manager1.`name`,manager.`name`),
                                  'თათული ტოხვაძე'
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      source ON monitoring.source_id = source.id
                        LEFT JOIN mepa_project_types ON monitoring.call_type_id = mepa_project_types.id
                        LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                        LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                        LEFT JOIN monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                        JOIN      user_info ON monitoring.operator_id = user_info.user_id
                        JOIN      user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                        LEFT JOIN monitoring_appeal ON monitoring.id = monitoring_appeal.monitoring_id
                        LEFT JOIN user_info AS manager1 ON monitoring_appeal.manager_id = manager1.user_id
                        LEFT JOIN user_info AS `manager` ON manager.user_id = user_info.manager_id
                        WHERE     monitoring.monitoring_status_id = 2 AND monitoring.operator_id > 0 $filter
                        GROUP BY  monitoring.id");
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        break;
        
    case 'get_list_dasrulebuli' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $start_datem = $_REQUEST['start_date'];
        
        $fillter = $_REQUEST['fillter'];
        $start   = $_REQUEST['start_date'];
        $end     = $_REQUEST['end_date'];
        
        $user_id = $_SESSION[USERID];
        
        $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
        
        $user_info = $db->getResultArray();
        
        $filter = '';
        $group  = $user_info[result][0][group_id];
        if ($user_info[result][0][group_id] != 31 && $user_info[result][0][group_id] != 46 && $user_info[result][0][group_id] != 47) {
            $filter = "AND (monitoring.operator_id = '$user_id' OR monitoring.monitoring_user_id = '$user_id')";
        }else if ($user_info[result][0][group_id] == 46){
            $filter = "AND monitoring_appeal.manager_id = $user_id";
        }
        
        $db->setQuery(" SELECT    monitoring.id,
                                  monitoring_main.datetime,
                                  FROM_UNIXTIME(monitoring.request_date),
                                  source.`name`,
                                  mepa_project_types.`name`,
                                  monitoring_action_type.`name`,
                                  SEC_TO_TIME(asterisk_call_log.talk_time),
                                  user_info.`name`,
                                  IF(monitoring.distribution_type=2,'მანუალური','ავტომატური'),
                                  'დასრულებული',
                                  SUM(IF(monitoring_rate.question_level<0,0,monitoring_rate.question_level)),
                                  IF($group='34', '', monitoring_user.`name`),
                                  IFNULL(manager1.`name`,manager.`name`),
                                  'თათული ტოხვაძე'
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      source ON monitoring.source_id = source.id
                        LEFT JOIN mepa_project_types ON monitoring.call_type_id = mepa_project_types.id
                        LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                        LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                        JOIN      monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                        JOIN      user_info ON monitoring.operator_id = user_info.user_id
                        JOIN      user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                        LEFT JOIN monitoring_appeal ON monitoring.id = monitoring_appeal.monitoring_id
                        LEFT JOIN user_info AS manager1 ON monitoring_appeal.manager_id = manager1.user_id
                        LEFT JOIN user_info AS `manager` ON manager.user_id = user_info.manager_id
                        WHERE     monitoring.monitoring_status_id = 3 AND monitoring.operator_id > 0 AND FROM_UNIXTIME(monitoring.finish_date) BETWEEN '$start' AND '$end' $filter
                        GROUP BY  monitoring.id");
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        break;
    case 'get_list_manual_dialog_table' :
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $start_datem = $_REQUEST['start_date'];
        
        $fillter = 	$_REQUEST['fillter'];
        
        $id         = $_REQUEST['id'];
        $start_date = $_REQUEST['start_date'];
        $end_date   = $_REQUEST['end_date'];
        $main_id    = $_REQUEST['main_id'];
        if ($id == '') {
            $db->setQuery("  SELECT    monitoring.id,
                                      FROM_UNIXTIME(monitoring.request_date),
                                      source.`name`,
                                      mepa_project_types.`name`,
                                      monitoring_action_type.`name`,
                                      SEC_TO_TIME(asterisk_call_log.talk_time),
                                      user_info.`name`,
                                      IF(monitoring_temp.distribution_type=2,'ავტომატური','მანუალური'),
                                      IF(ISNULL(monitoring.monitoring_user_id),'გასანაწილებელი','განაწილებელი'),
                                      '',
                                      monitoring_user.`name`
                            FROM      monitoring
                            JOIN      monitoring_temp ON monitoring.id = monitoring_temp.monitoring_id
                            JOIN      source ON monitoring.source_id = source.id
							LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                            LEFT JOIN mepa_project_types ON monitoring.call_type_id = mepa_project_types.id
                            LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                            LEFT JOIN user_info ON monitoring.operator_id = user_info.user_id
                            LEFT JOIN user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                            WHERE     monitoring_temp.actived = 1 AND monitoring.operator_id > 0 AND monitoring_temp.monitoring_main_id = $main_id");
        }else{
            $db->setQuery(" SELECT    monitoring.id,
                                      FROM_UNIXTIME(monitoring.request_date),
                                      source.`name`,
                                      mepa_project_types.`name`,
                                      monitoring_action_type.`name`,
                                      SEC_TO_TIME(asterisk_call_log.talk_time),
                                      user_info.`name`,
                                      IF(monitoring.distribution_type=2,'ავტომატური','მანუალური'),
                                      IF(ISNULL(monitoring.monitoring_user_id),'გასანაწილებელი','განაწილებელი'),
                                      SUM(IF(monitoring_rate.question_level<0,0,monitoring_rate.question_level)),
                                      monitoring_user.`name`
                            FROM      monitoring
                            JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                            JOIN      source ON monitoring.source_id = source.id
                            LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                            LEFT JOIN mepa_project_types ON monitoring.call_type_id = mepa_project_types.id
                            LEFT JOIN monitoring_action_type ON monitoring.action_type_id = monitoring_action_type.id
                            LEFT JOIN monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                            LEFT JOIN user_info ON monitoring.operator_id = user_info.user_id
                            LEFT JOIN user_info AS monitoring_user ON monitoring.monitoring_user_id = monitoring_user.user_id
                            WHERE     monitoring_main.id = $id AND monitoring.operator_id > 0 
                            GROUP BY  monitoring.id");
        }
        
        $result = $db->getKendoList($columnCount,$cols);
        
        $data = $result;
        
        break;
    
    case 'save_incomming':
        $id                = $_REQUEST['id'];
        $answer            = $_REQUEST['answer'];
        $monitoring_coment = $_REQUEST['monitoring_coment'];
        $user_id           = $_SESSION['USERID'];
        
        $monitoring_status_id = $_REQUEST['monitoring_status_id'];
        $column               = '';
        
        
        if ($monitoring_status_id == 1) {
            $column = "rate_date = IF(ISNULL(rate_date),UNIX_TIMESTAMP(NOW()),rate_date),";
        }elseif ($monitoring_status_id == 2){
            $column = "protest_date = IF(ISNULL(protest_date),UNIX_TIMESTAMP(NOW()),protest_date),";
        }elseif ($monitoring_status_id == 3){
            $column = "finish_date = IF(ISNULL(finish_date),UNIX_TIMESTAMP(NOW()),finish_date),";
        }
        
        $values  = substr($answer, 0, -1);
        
        $db->setQuery("UPDATE  monitoring
                          SET  $column
                               monitoring.monitoring_status_id = '$monitoring_status_id',
                              `comment`                        = '$monitoring_coment'
			           WHERE   id                              = '$id'");
        $db->execQuery();
        $db->setQuery("DELETE FROM monitoring_rate
                       WHERE  monitoring_rate.monitoring_id = '$id'");
        $db->execQuery();
        $db->setQuery("INSERT INTO `monitoring_rate`
                                (`user_id`, `datetime`, `monitoring_id`, `question_id`, `status_id`, `question_level`, `comment`) 
                          VALUES 
                                $values");
        $db->execQuery();

        
        break;
        
    case 'save-dialog-gasachivreba':
        $id                                       = $_REQUEST['id'];
        $monitoring_appealing_operator            = $_REQUEST['monitoring_appealing_operator'];
        $operator_yes_no                          = $_REQUEST['operator_yes_no'];
        $operator_appealing_comment               = $_REQUEST['operator_appealing_comment'];
        $monitoring_appealing_manager             = $_REQUEST['monitoring_appealing_manager'];
        $manager_yes_no                           = $_REQUEST['manager_yes_no'];
        $manager_appealing_comment                = $_REQUEST['manager_appealing_comment'];
        $monitoring_appealing_call_center_manager = $_REQUEST['monitoring_appealing_call_center_manager'];
        $call_center_yes_no                       = $_REQUEST['call_center_yes_no'];
        $call_center_manager_appealing_comment    = $_REQUEST['call_center_manager_appealing_comment'];
        
        $db->setQuery("SELECT id
                       FROM   monitoring_appeal
                       WHERE  monitoring_id = '$id'
                       AND    actived = 1");
        
        $status = 2;
        $check = $db->getNumRow();
        if ($check == 0) {
            
            $db->setQuery("INSERT INTO `monitoring_appeal`
                                    (`user_id`, `monitoring_id`, `operator_agree`, `operator_comment`, `manager_id`, `call_center_manager_id`) 
                              VALUES 
                                    ('$monitoring_appealing_operator', '$id', '$operator_yes_no', '$operator_appealing_comment',  '$monitoring_appealing_manager', '$monitoring_appealing_call_center_manager')");
            $db->execQuery();
            $db->setQuery("UPDATE monitoring
                              SET protest_date         = UNIX_TIMESTAMP(NOW()),
                                  monitoring_status_id = 2
                           WHERE  id                   = '$id'");
            $db->execQuery();
        }else{
            
            $db->setQuery("UPDATE `monitoring_appeal` 
                            SET `manager_agree`               = '$manager_yes_no',
                                `manager_comment`             = '$manager_appealing_comment',
                                `call_center_manager_agree`   = '$call_center_yes_no',
                                `call_center_manager_comment` = '$call_center_manager_appealing_comment'
                          WHERE `monitoring_id`               = '$id'");
            $db->execQuery();
            $status = 2;
            
            if ($manager_yes_no == 1 && $call_center_yes_no == 0) {
                $status = 3;
                
                $db->setQuery("UPDATE monitoring
                                SET finish_date          = UNIX_TIMESTAMP(NOW()),
                                    monitoring_status_id = $status
                             WHERE  id                   = '$id'");
                $db->execQuery();
            }elseif ($call_center_yes_no == 1){
                $status = 3;
                
                $db->setQuery("UPDATE monitoring
                                SET finish_date          = UNIX_TIMESTAMP(NOW()),
                                    monitoring_status_id = $status
                             WHERE  id                   = '$id'");
                $db->execQuery();
            }
            
         }
        
         $check        = 0;
         if ($call_center_yes_no == 2) {
            $check        = 1;
            
            $db->setQuery("UPDATE monitoring
                            SET protest_date         = NOW(),
                                monitoring_status_id = $status,
                                check_appeal_status  = 1
                         WHERE  id                   = '$id'");
            $db->execQuery();
         }
         
         $data = array("check" => $check);
        break;
        
    case 'get_percent':
        $id     = $_REQUEST['id'];
        $answer = $_REQUEST['answer'];
        
        $values  = substr($answer, 0, -1);
        
        mysql_query("DELETE FROM monitoring_percent
                     WHERE  monitoring_percent.incomming_id = '$id'");
        
        mysql_query("INSERT INTO `monitoring_percent` 
					            (`incomming_id`, `question_id`, `question_ansver`) 
		                  VALUES 
					            $values");
        
        
        $percent = mysql_fetch_array(mysql_query("SELECT CONCAT(ROUND(COUNT(*)/(SELECT COUNT(*) 
                            											        FROM  `monitoring_question` 
                            											        WHERE  actived = 1)*100,2),'%') AS `percent`
                                                  FROM   monitoring_percent
                                                  WHERE  incomming_id = '$id' AND question_ansver = 1"));
        
        $data		= array('percent'	=> $percent[percent]);
        
        break;
        case "get_counters":
            $start = $_REQUEST['start'];
            $end   = $_REQUEST['end'];
            $db->setQuery(" SELECT   ROUND(SUM(monitoring_rate.question_level)/COUNT(DISTINCT monitoring.id),2) AS `percent`
                            FROM     monitoring
                            JOIN     monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                            JOIN     monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                            WHERE    monitoring.monitoring_main_id > 0
                            AND      monitoring.actived = 1 AND monitoring.operator_id > 0 AND DATE(monitoring_main.datetime) BETWEEN '$start' AND '$end' LIMIT 1 ;");
            
            $req = $db->getResultArray();
            $query = $req[result][0];
            
            $db->setQuery("SELECT   COUNT(*) AS `shesafasebeli`,
                                    SUM(IF(monitoring.monitoring_status_id IN(1,3),1,0)) AS `shefasebuli`,
                                    SUM(IF(monitoring.monitoring_status_id = 2,1,0)) AS `gasachivrebuli`
                            FROM     monitoring
                            JOIN     monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                            WHERE    monitoring.monitoring_main_id > 0
                            AND      monitoring.actived = 1 AND DATE(monitoring_main.datetime) BETWEEN '$start' AND '$end' LIMIT 1;");
            $req1 = $db->getResultArray();
            $query1 = $req1[result][0];
            
            $quality = '<div class="counter_text">ხარისხის კოეფიციენტი</div>
                         <div class="counter_number">'.$query['percent'].'%</div>
                             <div class="counter_indicator">
                                 <div></div>
                             </div>
                         <div class="grid">
                             <div>შესაფასებელი</div>  <div>შეფასებული</div> <div>გასაჩივრებული</div>
                             <div>'.$query1['shesafasebeli'].'</div>  <div>'.$query1['shefasebuli'].'</div>               <div>'.$query1['gasachivrebuli'].'</div> 
                                 
                         </div>';  
            
            $db->setQuery("SELECT ROUND((SUM(point) / COUNT(id)), 2 ) AS `percent` FROM users_point ;");
            
            $req2  = $db->getResultArray();
            $query = $req2[result][0];
            
            $db->setQuery(" SELECT COUNT(id) AS `all`,
                                   SUM(IF(disorder_status_id = 2,1,0)) AS `disorders`
                            FROM   operator_disorder
                            WHERE  actived = 1;");
            
            $req3   = $db->getResultArray();
            $query1 = $req3[result][0];
            
            $discipline = ' <div class="counter_text">დისციპლინის კოეფიციენტი</div>
                            <div class="counter_number">'.$query['percent'].'%</div>
                            <div class="counter_indicator">
                                <div></div>
                            </div>
                            
                            <div class="grid">
                                <div>შემთხვევა</div>  <div>გასაჩივრებული</div> <div></div> 
                                <div>'.$query1['all'].'</div>  <div>'.$query1['disorders'].'</div>               <div></div> 
                                    
                            </div>';
             

            $db->setQuery("SELECT  ROUND(SUM(TIME_TO_SEC(time)) / 3600) AS `time` ,  ROUND((SUM(TIME_TO_SEC(time)) / 3600) + 110) AS `real`, ROUND(( ((SUM(TIME_TO_SEC(time)) / 3600) + 110) / (SUM(TIME_TO_SEC(time)) / 3600)) * 100) AS `percent`  FROM (
			
								
                         SELECT  SEC_TO_TIME(IF(TIMEDIFF(t1.end_date,t1.`start_date`) > '00:00:00', 
                                 TIME_TO_SEC(t1.end_date) - TIME_TO_SEC(t1.`start_date`),
                                 TIME_TO_SEC(t1.end_date) - TIME_TO_SEC(t1.`start_date`) + TIME_TO_SEC('24:00:00')) - TIME_TO_SEC(`t1`.`timeout`)) AS `time`,
                                 IFNULL(`t1`.`timeout`,'00:00:00') AS `timeout`
                         FROM    users
                         JOIN (	SELECT work_graphic_rows.id , work_graphic_rows.operator_id
                                FROM   work_graphic_rows 
                                JOIN   work_graphic ON work_graphic.id = work_graphic_rows.work_graphic_id
                                WHERE  work_graphic_rows.actived = 1 
                                AND    work_graphic.`year` = YEAR(CURDATE()) 
                                AND    work_graphic.`month` = MONTH(CURDATE())
                              ) AS `rows` ON `rows`.operator_id = users.id
                         JOIN       work_graphic_cols ON work_graphic_cols.work_graphic_rows_id = `rows`.id
                         JOIN       work_shift ON work_shift.id = work_graphic_cols.work_shift_id
                         LEFT JOIN `work_shift` AS t1 ON work_graphic_cols.work_shift_id = t1.`id` AND t1.type=1 
                         WHERE 	    work_graphic_cols.actived = 1
                         ORDER BY   col_id) AS x ");

            $req4  = $db->getResultArray();
            $query = $req4[result][0];
            
            $attendance = '<div class="counter_text">დასწრების კოეფიციენტი</div>
                            <div class="counter_number">'.$query['percent'].'%</div>
                            <div class="counter_indicator">
                                <div></div>
                            </div>
                            <div class="grid">
                                <div>გეგ.სთ</div>  <div>ფაქტ. სთ</div> <div>სხვაობა სთ</div>
                                <div>'.$query['time'].'</div>  <div>'.$query['real'].'</div>               <div>'.($query['time']-$query['real']).'</div> 
                            </div>';

             $data = array("quality" => $quality, "discipline" => $discipline,"attendance" => $attendance);
        break;
        
    case 'change_responsible_person':
        $ids                 = $_REQUEST['ids'];
        $type                = $_REQUEST['type'];
        $checked_count       = $_REQUEST['checked_count'];
        $monitoring_user_id  = $_REQUEST['monitoring_user_id'];
        $hidde_monitoring_id = $_REQUEST['hidde_monitoring_id'];
        
        if ($type == 2) {
            $db->setQuery("UPDATE monitoring
                              SET monitoring_user_id = '$monitoring_user_id'
                           WHERE  id IN($ids)
                           AND    actived = 1");
            $db->execQuery();
        }else{
           $db->setQuery("  SELECT users.`id`,
                    			   user_info.`name`
                            FROM  `users`
                            JOIN   user_info ON user_info.user_id = users.id
                            WHERE  users.`actived` = 1 AND users.group_id = 48");
            
            $check_operator_caunt = $db->getNumRow();
            $result_users         = $db->getResultArray();
            if ($check_operator_caunt > 0) {
                
                $limit       = intval($checked_count/$check_operator_caunt);
                $delta_limit = intval($checked_count%$check_operator_caunt);
                $i           = 0;
                foreach ($result_users[result] AS $row) {
                    if ($delta_limit > 0) {
                        $update_limit = $limit + 1;
                        $delta_limit --;
                    }else{
                        $update_limit = $limit;
                    }
                    
                    
                    if ($hidde_monitoring_id == '') {
                        $db->setQuery("SELECT   monitoring_id
                                       FROM     monitoring_temp
                                       WHERE    monitoring_id IN($ids)
                                       AND      actived = 1
                                       ORDER BY monitoring_id ASC
                                       LIMIT $i, $update_limit");
                        $req_ids = $db->getResultArray();
                    }else{
                        
                        $db->setQuery("SELECT   id AS `monitoring_id`
                                        FROM     monitoring
                                        WHERE    id IN($ids)
                                        ORDER BY id ASC
                                        LIMIT $i, $update_limit");
                        
                        
                        $req_ids = $db->getResultArray();
                    }
                    
                    $monit_user_id = $row[id];
                    foreach($req_ids[result] AS $row1) {
                        $db->setQuery("UPDATE monitoring
                                          SET monitoring_user_id = '$monit_user_id'
                                       WHERE  id IN($row1[monitoring_id])");
                        $db->execQuery();
                    }
                    
                    $i+=$update_limit;
                }
                
                
            }else {
                global $error;
                $error = 'მონიტორინგის არცერთი იუზერი არ არის ავტორიზებული';
            }
        }
        
        break;
    case 'save-dialog-gasanawilebeli':
        $user_id = $_SESSION[USERID];
        $id      = $_REQUEST['id'];
        
        $start              = $_REQUEST['start'];
        $end                = $_REQUEST['end'];
        $all_reques_period  = $_REQUEST['all_reques_period'];
        $ukve_daformirebuli = $_REQUEST['ukve_daformirebuli'];
        $dasaformirebeli    = $_REQUEST['dasaformirebeli'];
        $info               = $_REQUEST['info'];
        $problem            = $_REQUEST['problem'];
        $claim              = $_REQUEST['claim'];
        $empty              = $_REQUEST['empty'];
        $other              = $_REQUEST['other'];
        
        $db->setQuery("INSERT INTO `monitoring_main`
                                  (`id`, `user_id`, `datetime`, `start_date`, `end_date`, `all_request`, `daformirebuli`, `dasaformirebeli`,  `monitoring_shablon_id`, `actived`) 
                            VALUES 
                                  ('$id', '$user_id', NOW(), '$start', '$end', '$all_reques_period', '$ukve_daformirebuli', '$dasaformirebeli', 3, 1)");
        $db->execQuery();
        
        $db->setQuery("UPDATE monitoring
                          SET monitoring_status_id = 0,
                              monitoring_main_id   = '$id',
                              distribution_type    = '2'
                       WHERE  id IN(SELECT monitoring_id
                                    FROM   monitoring_temp
                                    WHERE  actived = 1 AND monitoring_main_id = '$id')");
        $db->execQuery();
        
        break;
    case 'get_users':
        $req = mysql_query("SELECT id, `name`
                            FROM crystal_users
                            where actived = 1 AND group_id = 1");
        $page .= '<option value="0">----------------</option>';
        while($res = mysql_fetch_assoc($req)){
            $page .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $data		= array('page'	=> $page);
        break;
    case 'get_list_history' :
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];
        
        $start_check  = $_REQUEST['start_check'];
        $end_check    = $_REQUEST['end_check'];
        $phone        = $_REQUEST['phone'];
        $check_ab_pin = $_REQUEST['check_ab_pin'];
        
        $filt = '';
        if($phone == '' && $check_ab_pin == ''){
            $filt="AND inc.pin = 'fddhgdhgd'";
        }
        
        if ($phone != '' && $check_ab_pin != '') {
            $filt="AND inc.pin = '$check_ab_pin' OR inc.phone = '$check_ab_pin'";
        }
        
        if ($phone != '' && $check_ab_pin != '') {
            $filt="AND (inc.pin = '$check_ab_pin' OR inc.phone = '$check_ab_pin')";
        }
        
        if ($phone != '' && $check_ab_pin == '') {
            $filt="AND inc.phone = '$phone'";
        }
        
        if ($phone == '' && $check_ab_pin != '') {
            $filt="AND inc.pin = '$check_ab_pin'";
        }
        
        $rResult = mysql_query("SELECT     `inc`.`id` AS `task_id`,
                                           `inc`.`date` AS `date`,
                                            IF(`inc`.`pin` = '000000', '', `inc`.`pin`) AS `pin`,
                                            IF(`inc`.`phone` = '2004', `inc`.`ipaddres`, `inc`.`phone`) AS `phone`,
                                           `crystal_users`.`name`,
                                           `inc`.`call_content`,
                                            task.problem_comment,
                                            IF(ISNULL(task.`status`), 'დასრულებული', status.`call_status`) AS `status`,
                                            CONCAT('<p onclick=play(', '\'', asterisk_incomming.file_name, '\'',  ')>მოსმენა</p>')
                                FROM       `incomming_call` AS `inc`
                                LEFT JOIN  `crystal_users` ON `inc`.`user_id` = `crystal_users`.`id`
                                LEFT JOIN  `asterisk_incomming` ON `inc`.`uniqueid` = `asterisk_incomming`.`id`
                                LEFT JOIN  	task  ON inc.id=task.incomming_call_id
                                LEFT JOIN  	status  ON task.status=status.id
                                WHERE       DATE(inc.date) BETWEEN '$start_check' AND '$end_check' $filt");
        
        $data = array("aaData"	=> array());
        
        while ( $aRow = mysql_fetch_array( $rResult ) ){
            $row = array();
            $row1 = array();
            for ( $i = 0 ; $i < $count ; $i++ ){
                $row[] = $aRow[$i];
            }
            $data['aaData'][] = $row;
        }
        
        break;
    case 'get_notification_count' :
        $user_id = $_SESSION["USERID"];
        $db->setQuery(" SELECT  COUNT(DISTINCT monitoring.id) AS count
                        FROM monitoring
                        JOIN monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id AND monitoring_rate.question_level < 0
                        WHERE monitoring.seen = 0 AND monitoring.operator_id = $user_id AND monitoring.monitoring_user_id > 0 AND monitoring.monitoring_status_id = 1 AND monitoring.monitoring_main_id > 0
        ");
        $res = $db->getResultArray()["result"];
        $data = $res;
        break;
    case 'get_list_notification':
        $user_id = $_SESSION["USERID"];
        $db->setQuery(" SELECT  incomming_call.id,
                                monitoring.id AS monitoring_id,
                                FROM_UNIXTIME(monitoring.request_date) AS date,
                                SUM(monitoring_rate.question_level) AS qula
                        FROM monitoring
                        LEFT JOIN monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id AND monitoring_rate.question_level > 0
                        LEFT JOIN incomming_call ON incomming_call.id = monitoring.incomming_call_id
                        WHERE monitoring.seen = 0 AND monitoring.operator_id = $user_id AND monitoring.monitoring_user_id > 0 AND monitoring.monitoring_status_id = 1 AND monitoring.monitoring_main_id > 0
                        GROUP BY monitoring.id
                        HAVING qula < 100
                        ");
        $res = $db->getResultArray();

        $data = array("notification_data" => $res["result"]);
        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;
echo json_encode($data);


/* ******************************
 *	Request Functions
 * ******************************
 */

function get_tabs(){
    global $db;
    
    $user = $_SESSION['USERID'];
    $db->setQuery(" SELECT users.group_id
                    FROM  `users`
                    WHERE  users.id = $user");
    
    $result = $db->getResultArray();
    if ($result[result][0][group_id] == 34) {
        $db->setQuery(" SELECT id,
                               name,
                               parent_id
                        FROM  `monitoring_tabs`
                        WHERE  actived=1 and parent_id = 0 AND id IN(3,4,5)");
    }else{
        $db->setQuery(" SELECT '0' AS id,
                               'მთავარი' AS `name`,
                               '0' AS `parent_id`
            
                        UNION ALL
                
                        SELECT id,
                               name,
                               parent_id
                        FROM  `monitoring_tabs`
                        WHERE  actived=1 and parent_id = 0");
    }
    
    
    
    $req = $db->getResultArray();
    $datafull="";
    $data2='';
    $data='<div id="tab_0" class="main_tab" name="0"><ul>';
    foreach ($req[result] AS $arr) {
        if($arr['parent_id']==0){
            $data   .=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" href="#tab-'.$arr['id'].'" >
                                <a>'.$arr['name'].'</a>
                            </li>';
            $data2  .='</ul>
                                </div><div id="tab_'.$arr['id'].'" class="tab_'.$arr['id'].'" name="'.$arr['id'].'">
                            <ul>';
        }
    }
    $data.='</ul></div>';
    $data2.='</ul></div>';
    $datafull .=  $data;
    $datafull .=  $data2;
    return $datafull;
}

function get_table_detail(){
    $id = $_REQUEST['id'];
    
    
    $data='<div style="width:100%;" id="table_detail_info"></div>';
    
    return $data;
}

function get_gasachivreba(){
    global $db;
    $id      = $_REQUEST['id'];
    $user_id = $_SESSION[USERID];
    
    $db->setQuery("SELECT id,
                          manager_id
                   FROM   monitoring_appeal
                   WHERE  monitoring_id = '$id'
                   AND    actived = 1");
    
    $res_check     = $db->getResultArray();
    $check_count   = $db->getNumRow();
    $check_manager = $res_check[result][0];
    if ($check_manager == 0) {
        $db->setQuery("SELECT    user_info.user_id AS op_id,
                        		 user_info.`name` AS `op_name`,
                        		 manager.user_id AS `manager_id`,
				                 manager.`name` AS `manager_name`,
                        		 '102' AS `call_center_manager_user_id`,
                        		 'თათული ტოხვაძე' AS `call_center_manager_name`,
                                 NOW() AS `datetime`,
                                 '2' AS `operator_agree`
                       FROM      monitoring
                       JOIN      user_info ON user_info.user_id = monitoring.operator_id
                       LEFT JOIN user_info AS manager ON manager.user_id = user_info.manager_id
                       WHERE     monitoring.id = '$id'");
        $req = $db->getResultArray();
        $res = $req[result][0];
        
    }else{
        $db->setQuery(" SELECT    user_info.user_id AS op_id,
            					  user_info.`name` AS `op_name`,
            					  monitoring_appeal.operator_agree,
            					  monitoring_appeal.operator_comment,
            					  manager.user_id AS `manager_id`,
            					  manager.`name` AS `manager_name`,
            					  monitoring_appeal.manager_agree,
            					  monitoring_appeal.manager_comment,
            				      call_center_manager.user_id AS `call_center_manager_user_id`,
            				      call_center_manager.`name` AS `call_center_manager_name`,
            					  monitoring_appeal.call_center_manager_agree,
            					  monitoring_appeal.call_center_manager_comment,
            					  FROM_UNIXTIME(monitoring.protest_date) AS `datetime`
                        FROM      monitoring_appeal
                        JOIN      user_info ON user_info.user_id = monitoring_appeal.user_id
                        JOIN      monitoring ON monitoring.id = monitoring_appeal.monitoring_id
                        LEFT JOIN user_info AS `manager` ON manager.user_id = monitoring_appeal.manager_id
                        LEFT JOIN user_info AS `call_center_manager` ON call_center_manager.user_id = monitoring_appeal.call_center_manager_id
                        WHERE     monitoring_appeal.monitoring_id = '$id'");
        
        $req = $db->getResultArray();
        $res = $req[result][0];
    }

    $dis_operator            = 'disabled="disabled"';
    $dis_manager             = 'disabled="disabled"';
    $dis_call_center_manager = 'disabled="disabled"';
    $manager_user_id         = '';
    $manager_user_name       = '';
    $db->setQuery("SELECT    users.group_id,
                             user_info.`name`
                   FROM     `users`
                   LEFT JOIN user_info ON user_info.user_id = users.id
                   WHERE     users.id = '$user_id'");
    
    $req         = $db->getResultArray();
    $check_group = $req[result][0];
    
    if ($user_id == $res[op_id] || $user_id == 1) {
        $dis_operator = "";
    }
    
    if ($user_id == $res[manager_id] || $user_id == 1) {
        $dis_manager   = "";
    }
    
    if ($res[manager_id] == '' || $res[manager_id] == 0){
        $manager_user_id   = $user_id;
        $manager_user_name = $check_group[name];
    }else{
        $manager_user_id   = $res[manager_id];
        $manager_user_name = $res[manager_name];
    }
    
    if ($user_id == $res[call_center_manager_user_id]) {
        $dis_call_center_manager = "";
    }
    
    $data='<table style="width: 100%;">
                <tr style="width: 100%; height:0px">
                    <td style="width: 30%;">გასაჩივრების თარიღი</td>
                    <td style="width: 20%;"></td>
                    <td style="width: 50%;"></td>
                </tr>
                <tr style="height:0px">
                    <td><input style="width: 90%;" type="text" id="monitoring_appealing_date" class="idle" value="'.$res[datetime].'" disabled="disabled"/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height:20px;"></tr>
                <tr style="height:0px">
                    <td>ოპერატორი</td>
                    <td>ეთანხმება</td>
                    <td>კომენტარი</td>
                </tr>
                <tr style="height:0px">
                    <td><input style="width: 90%;" type="text" id="monitoring_appealing_operator" class="idle" user_id="'.$res[op_id].'" value="'.$res[op_name].'" disabled="disabled"/></td>
                    <td><select id="operator_appealing_yes" class="idle" style="width: 130px;" '.$dis_operator.'>' . get_apperling_status($res[operator_agree]) . '</select></td>
                    <td><textarea  style="width: 100%; resize: vertical;" id="operator_appealing_comment" class="idle" name="call_content" cols="300" rows="8" '.$dis_operator.'>' . $res['operator_comment'] . '</textarea></td>
                </tr>
                <tr style="height:5px;"></tr>
                <tr style="height:0px">
                    <td>მენეჯერი</td>
                    <td>ეთანხმება</td>
                    <td>კომენტარი</td>
                </tr>
                <tr style="height:0px">
                    <td><input style="width: 90%;" type="text" id="monitoring_appealing_manager" class="idle" user_id="'.$manager_user_id.'" value="'.$manager_user_name.'" disabled="disabled"/></td>
                    <td><select id="manager_appealing_yes" class="idle" style="width: 130px;" '.$dis_manager.'>' . get_apperling_status($res[manager_agree]) . '</select></td>
                    <td><textarea  style="width: 100%; resize: vertical;" id="manager_appealing_comment" class="idle" name="call_content" cols="300" rows="8" '.$dis_manager.'>' . $res['manager_comment'] . '</textarea></td>
                </tr>
                <tr style="height:5px;"></tr>
                <tr style="height:0px">
                    <td>ქოლ-ცენტრის ხელმძღვანელი</td>
                    <td>ეთანხმება</td>
                    <td>კომენტარი</td>
                </tr>
                <tr style="height:0px">
                    <td><input style="width: 90%;" type="text" id="monitoring_appealing_call_center_manager" class="idle" user_id="'.$res[call_center_manager_user_id].'" value="'.$res[call_center_manager_name].'" disabled="disabled"/></td>
                    <td><select id="call_center_manager_appealing_yes" class="idle" style="width: 130px;" '.$dis_call_center_manager.'>' . get_apperling_status($res[call_center_manager_agree]) . '</select></td>
                    <td><textarea  style="width: 100%; resize: vertical;" id="call_center_manager_appealing_comment" class="idle" name="call_content" cols="300" rows="8" '.$dis_call_center_manager.'>' . $res['call_center_manager_comment'] . '</textarea></td>
                </tr>
            </table>';
    
    return $data;
}

function checkgroup($user){
    $res = mysql_fetch_assoc(mysql_query("SELECT users.group_id
                                          FROM   users
                                          WHERE  users.id = $user"));
    return $res['group_id'];
    
}

function get_apperling_status($id){
    
    $data .= '<option value="0" selected="selected">------</option>';
    
    
    if ($id == 1){
        $data .= '<option value="1" selected="selected">კი</option>
                  <option value="2">არა</option>';
    }else if($id == 2){
        $data .= '<option value="1">კი</option>
                  <option value="2" selected="selected">არა</option>
                  ';
    }else{
        $data .= '<option value="1">კი</option>
                  <option value="2">არა</option>';
    }
    
    
    return $data;
    
}

function note_incomming(){
    global $error;
    $incom_id	= $_REQUEST['id'];
    $user		= $_SESSION['USERID'];
    $group		= checkgroup($user);
    
    if ($group==2 || $group==3) {
        mysql_query("UPDATE `incomming_call`
            SET
            `noted`    ='1'
            WHERE `id`='$incom_id'
            ");
        
    }else{
        $error='ეს ფუნქცია თქვენთვის შეზღუდულია';
    }
}


function Getcall_status($status){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `call_status`
						FROM 	`status`
						WHERE 	actived=1");
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        
        if($res['id'] == $status){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['call_status'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['call_status'] . '</option>';
        }
    }
    return $data;
}
function Getpay_type($pay_type_id){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `name`
						FROM 	`pay_type`
						WHERE 	actived=1");
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $pay_type_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    return $data;
}
function Get_bank($bank_id){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `name`
						FROM 	`bank`
						WHERE 	actived=1");
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $bank_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getcard_type($card_type_id){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `name`
					FROM 	`card_type`
					WHERE 	actived=1");
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $card_type_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}
function Getpay_aparat($pay_aparat_id){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `name`
					FROM 	`pay_aparat`
					WHERE 	actived=1");
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $pay_aparat_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getobject($object_id){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `name`
					FROM 	`object`
					WHERE 	actived=1");
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $object_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getsource($object_id){
    $data = '';
    $req = mysql_query("SELECT 	`id`, `name`
					FROM 	`source`
					WHERE 	actived=1");
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $object_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getcategory($category_id, $old=''){
    
    $data = '';
   
    if($old > 1588401) {
        $where = 'WHERE actived=1 AND `parent_id` != 0  AND sub_cat is null ';
    }
    else {
        $where = "";
    }
    $req = mysql_query("SELECT `id`, `name`, parent_id
						FROM `info_category`
						$where "); // 
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $category_id){
            $data .= '<option value="' . $res['id'] . '" parent_id = "'.$res['parent_id'].'" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '" parent_id = "'.$res['parent_id'].'" >' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getcategory1($category_id){
    
    if($category_id == 0 ){
        $and = "";
    }
    else {
        $and = "AND parent_id=$category_id";
    }



    $data = '';
    $req = mysql_query("SELECT `id`, `name`,`task`,`department_id`,`c_users_id`,`operators_task`,`sub_cat`, parent_id, sms_text, task_type, email_address, email_text
        FROM `info_category`
        WHERE actived=1 AND `sub_cat` = 1 $and ");
    

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        $user = ($res['operators_task'] == '1' ? $_SESSION['USERID'] : $res['c_users_id']);
        if($res['id'] == $category_id){
            $data .= '<option value="' . $res['id'] . '"  dep_id="'.$res['department_id'].'" sms_text="'.$res["sms_text"].'" task_type="'.$res["task_type"].'" email_address="'.$res["email_address"].'" email_text = "'.$res["email_text"].'" par_id="'.$res['parent_id'].'" c_id="'.$user.'" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '" dep_id="'.$res['department_id'].'" sms_text="'.$res["sms_text"].'" task_type="'.$res["task_type"].'" email_address="'.$res["email_address"].'" email_text = "'.$res["email_text"].'" par_id="'.$res['parent_id'].'" c_id="'.$user.'" >' . $res['name'] . '</option>';
        }
    }
    return $data;
}

function Getcategory1_edit($category_id, $old=''){
    if ($category_id == ''){
        $and = "AND id=$category_id";
    }
    else $and = '';

    if($old > 1588401) {
        $where = "WHERE actived=1 AND `sub_cat` = 1";
    }
    else {
        $where = "";
    }
    $data = '';
    $req = mysql_query("SELECT `id`, `name`,`task`,`department_id`,`c_users_id`,`operators_task`,`sub_cat`, parent_id, sms_text, task_type, email_address, email_text
        FROM `info_category`
        $where  "); //  
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        $user = ($res['operators_task'] == '1' ? $_SESSION['USERID'] : $res['c_users_id']);
        if($res['id'] == $category_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected" dep_id="'.$res['department_id'].'" sms_text="'.$res["sms_text"].'" task_type="'.$res["task_type"].'" email_address="'.$res["email_address"].'" email_text = "'.$res["email_text"].'" par_id="'.$res['parent_id'].'" c_id="'.$user.'" >' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '" dep_id="'.$res['department_id'].'" sms_text="'.$res["sms_text"].'" task_type="'.$res["task_type"].'" email_address="'.$res["email_address"].'" email_text = "'.$res["email_text"].'" par_id="'.$res['parent_id'].'" c_id="'.$user.'" >' . $res['name'] . '</option>';
        }
    }
    return $data;
}

function Getcall_type($call_type_id, $old=''){
    
    $data = '';

    if($old > 1588401) {
        $req = mysql_query("SELECT `id`, parent_id, `name`
					FROM `info_category`
                    WHERE  actived=1 && parent_id=0 AND `info_category`.actived=1");
    }
    else {
        $req = mysql_query("SELECT `id`,  `name`
                        FROM `call_type`
                        WHERE  actived=1 ");
    }
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $call_type_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    return $data;
}

function Getdepartment($task_department_id){
    $data = '';
    $req = mysql_query("SELECT `id`, `name`
					    FROM `department`
					    WHERE actived=1 ");
    
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $task_department_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getpriority($priority_id){
    $data = '';
    $req = mysql_query("SELECT `id`, `name`
						FROM `priority`
						WHERE actived=1 ");
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $priority_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Gettask_type($task_type_id){
    
    $data = '';
    $req = mysql_query("SELECT `id`, `name`
					    FROM `task_type`
					    WHERE actived=1 ");
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $task_type_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getpersons($persons_id){
    
    $data = '';
    $req = mysql_query("SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88103)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(1505)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(502)
                        UNION ALL
	                    SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88110)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id NOT IN(88103, 1505, 502, 88110)");
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $persons_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Get_control($persons_id){
    $data = '';
    $req = mysql_query("SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88103)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(1505)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(502)
                        UNION ALL
	                    SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id IN(88110)
                        UNION ALL
                        SELECT `id`, `name`
                        FROM   `crystal_users`
                        WHERE   actived=1 AND id NOT IN(88103, 1505, 502, 88110)");
    
    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $persons_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function get_calculation_checkbox($start, $end){
    $user_id = $_SESSION[USERID];
    $req = mysql_fetch_array(mysql_query("  SELECT COUNT(*) all_count,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 906,1,0)) AS `informacia`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 936,1,0)) AS `problema`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 952,1,0)) AS `pretenzia`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 1138,1,0)) AS `carieli`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 1312,1,0)) AS `sxva`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 1,1,0)) AS `phone`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 4,1,0)) AS `live_chat`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 11,1,0)) AS `site_chat`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 9,1,0)) AS `viber`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 6,1,0)) AS `fb`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 7,1,0)) AS `mail`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 1,1,0)) AS `outgoing`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 2,1,0)) AS `task`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 3,1,0)) AS `hold`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 4,1,0)) AS `block`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 60,1,0)) AS `ert_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 120,1,0)) AS `or_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 180,1,0)) AS `sam_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 240,1,0)) AS `otx_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 300,1,0)) AS `xut_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 600,1,0)) AS `at_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration >= 900,1,0)) AS `txutme_meti`
                                            FROM   monitoring
                                            JOIN   monitoring_temp ON monitoring_temp.monitoring_id = monitoring.id
                                            WHERE  DATE(FROM_UNIXTIME(monitoring.request_date)) BETWEEN '$start' AND '$end'
                                            AND    monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                                            AND    monitoring_temp.actived = 1"));
                                        
    $manual_call_type_percent             = $req[all_count];
    $manual_call_type_information_percent = $req[informacia];
    $manual_call_type_problem_percent     = $req[problema];
    $manual_call_type_claim_percent       = $req[pretenzia];
    $manual_call_type_empty_percent       = $req[carieli];
    $manual_call_type_other_percent       = $req[sxva];
    
    $manual_mail_chat_label_label_id      = $req[all_count];
    $manual_phone_label_id                = $req[phone];
    $manual_chat_label_id                 = $req[live_chat];
    $manual_site_chat_label_id            = $req[site_chat];
    $manual_viber_chat_label_id           = $req[viber];
    $manual_fb_chat_label_id              = $req[fb];
    $manual_mail_chat_label_id            = $req[mail];
    
    
    $manual_action_filter_out_label       = $req[outgoing];
    $manual_action_filter_task_label      = $req[task];
    $manual_action_filter_hold_label      = $req[hold];
    $manual_action_filter_block_label     = $req[block];
    $manual_all_action_label              = $req[outgoing]+$req[task]+$req[hold]+$req[block];
    
    $manual_duration_filter_1             = $req[ert_wutamde];
    $manual_duration_filter_2             = $req[or_wutamde];
    $manual_duration_filter_3             = $req[sam_wutamde];
    
    $manual_duration_filter_4             = $req[otx_wutamde];
    $manual_duration_filter_5             = $req[xut_wutamde];
    $manual_duration_filter_10            = $req[at_wutamde];
    $manual_duration_filter_15            = $req[txutme_meti];
    
    $data = array('manual_call_type_percent'             => $manual_call_type_percent,
                  'manual_call_type_information_percent' => $manual_call_type_information_percent,
                  'manual_call_type_problem_percent'     => $manual_call_type_problem_percent,
                  'manual_call_type_claim_percent'       => $manual_call_type_claim_percent,
                  'manual_call_type_empty_percent'       => $manual_call_type_empty_percent,
                  'manual_call_type_other_percent'       => $manual_call_type_other_percent,
                  'manual_mail_chat_label_label_id'      => $manual_mail_chat_label_label_id,
                  'manual_phone_label_id'                => $manual_phone_label_id,
                  'manual_chat_label_id'                 => $manual_chat_label_id,
                  'manual_site_chat_label_id'            => $manual_site_chat_label_id,
                  'manual_viber_chat_label_id'           => $manual_viber_chat_label_id,
                  'manual_fb_chat_label_id'              => $manual_fb_chat_label_id,
                  'manual_mail_chat_label_id'            => $manual_mail_chat_label_id,
                  'manual_duration_filter_1'             => $manual_duration_filter_1,
                  'manual_duration_filter_2'             => $manual_duration_filter_2,
                  'manual_duration_filter_3'             => $manual_duration_filter_3,
                  'manual_duration_filter_4'             => $manual_duration_filter_4,
                  'manual_duration_filter_5'             => $manual_duration_filter_5,
                  'manual_duration_filter_10'            => $manual_duration_filter_10,
                  'manual_duration_filter_15'            => $manual_duration_filter_15);
    return $data;
}

function get_shesamowmebeli_result(){
    global $db;
    $user_id = $_SESSION[USERID];
    $filter = '';
    
    
    $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
    $user_info = $db->getResultArray();
    if ($user_info[result][0][group_id] != 1) {
        $filter = "AND (monitoring.operator_id = '$user_id' OR monitoring.monitoring_user_id = '$user_id')";
    }
    
    $db->setQuery(" SELECT    COUNT(*) all_count,
                    		  operator.id,
                    		  operator.`name`
                    FROM      monitoring
                    JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                    JOIN      user_info ON monitoring.monitoring_user_id = user_info.user_id
                    JOIN      user_info AS `operator` ON monitoring.operator_id = `operator`.user_id
                    WHERE     monitoring.call_type_id > 0
                    AND       monitoring.monitoring_status_id = 0
                    AND       NOT ISNULL(monitoring.monitoring_main_id) $filter
                    GROUP BY  monitoring.operator_id");
    $res = $db->getResultArray();
    $data  = '';
    $data1 = '';
    
    $sum_operators_count = 0;
    foreach($res[result] AS $row){
        $sum_operators_count += $row[all_count];
        $data .= '<tr style="height:5px"></tr>
                  <tr>
                      <td>
                         <input class="shemowmebuli_operator_filter" type="checkbox" checked value="'.$row[id].'">
                      </td>
                      <td>
                         <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                      </td>
                      <td>
                         <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold; text-align:left">'.$row[all_count].'</label>
                       </td>
                  </tr>';

    }
    
    $db->setQuery(" SELECT    COUNT(*) all_count,
        					  SUM(IF(monitoring.distribution_type = 1,1,0)) AS `auto`,
        					  SUM(IF(monitoring.distribution_type = 2,1,0)) AS `manual`,
        					  SUM(IF(monitoring.call_type_id = 906,1,0)) AS `informacia`,
            				  SUM(IF(monitoring.call_type_id = 936,1,0)) AS `problema`,
            				  SUM(IF(monitoring.call_type_id = 952,1,0)) AS `pretenzia`,
            				  SUM(IF(monitoring.call_type_id = 1138,1,0)) AS `carieli`,
            				  SUM(IF(monitoring.call_type_id = 1312,1,0)) AS `sxva`,
            				  SUM(IF(monitoring.source_id = 1,1,0)) AS `phone`,
            				  SUM(IF(monitoring.source_id = 4,1,0)) AS `live_chat`,
            				  SUM(IF(monitoring.source_id = 11,1,0)) AS `site_chat`,
            				  SUM(IF(monitoring.source_id = 9,1,0)) AS `viber`,
            				  SUM(IF(monitoring.source_id = 6,1,0)) AS `fb`,
            				  SUM(IF(monitoring.source_id = 7,1,0)) AS `mail`,
            				  SUM(IF(monitoring.action_type_id = 1,1,0)) AS `outgoing`,
            				  SUM(IF(monitoring.action_type_id = 2,1,0)) AS `task`,
            				  SUM(IF(monitoring.action_type_id = 3,1,0)) AS `hold`,
            				  SUM(IF(monitoring.action_type_id = 4,1,0)) AS `block`,
            				  SUM(IF(monitoring.call_duration > 0 AND monitoring.call_duration < 60,1,0)) AS `ert_wutamde`,
            				  SUM(IF(monitoring.call_duration > 0 AND monitoring.call_duration < 120,1,0)) AS `or_wutamde`,
            				  SUM(IF(monitoring.call_duration > 0 AND monitoring.call_duration < 180,1,0)) AS `sam_wutamde`,
            				  SUM(IF(monitoring.call_duration > 0 AND monitoring.call_duration < 240,1,0)) AS `otx_wutamde`,
            				  SUM(IF(monitoring.call_duration > 0 AND monitoring.call_duration < 300,1,0)) AS `xut_wutamde`,
            				  SUM(IF(monitoring.call_duration > 0 AND monitoring.call_duration < 600,1,0)) AS `at_wutamde`,
            				  SUM(IF(monitoring.call_duration >= 900,1,0)) AS `txutme_meti` 
                    FROM      monitoring
                    JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                    JOIN      users ON monitoring.monitoring_user_id = users.id
                    WHERE     monitoring.call_type_id > 0
                    AND       monitoring.monitoring_status_id = 0
                    AND       NOT ISNULL(monitoring.monitoring_main_id) $filter");
    $res = $db->getResultArray();
    $req = $res[result][0];
    $all_action   = $req[outgoing]+$req[task]+$req[hold]+$req[block];
    $all_duration = $req[phone];
    
    $array = array( 'shesamowmebeli_request_count'          => $req[all_count],
                    'shesamowmebeli_manual_rate_count'      => $req[manual],
                    'shesamowmebeli_auto_rate_count'        => $req[auto],
                    'shesamowmebeli_call_type_all'          => $req[all_count],
                    'shesamowmebeli_call_type_906'          => $req[informacia],
                    'shesamowmebeli_call_type_936'          => $req[problema],
                    'shesamowmebeli_call_type_952'          => $req[pretenzia],
                    'shesamowmebeli_call_type_1138'         => $req[carieli],
                    'shesamowmebeli_call_type_1312'         => $req[sxva],
                    'shesamowmebeli_source_count_all'       => $req[all_count],
                    'shesamowmebeli_source_count_1'         => $req[phone],
                    'shesamowmebeli_source_count_4'         => $req[live_chat],
                    'shesamowmebeli_source_count_11'        => $req[site_chat],
                    'shesamowmebeli_source_count_9'         => $req[viber],
                    'shesamowmebeli_source_count_6'         => $req[fb],
                    'shesamowmebeli_source_count_7'         => $req[mail],
                    'shesamowmebeli_action_count_all'       => $all_action,
                    'shesamowmebeli_action_count_2'         => $req[task],
                    'shesamowmebeli_action_count_4'         => $req[block],
                    'shesamowmebeli_action_count_3'         => $req[hold],
                    'shesamowmebeli_action_count_1'         => $req[outgoing],
                    'shesamowmebeli_duration_count_all'       => $all_duration,
                    'shesamowmebeli_duration_count_1'       => $req[ert_wutamde],
                    'shesamowmebeli_duration_count_2'       => $req[or_wutamde],
                    'shesamowmebeli_duration_count_3'       => $req[sam_wutamde],
                    'shesamowmebeli_duration_count_4'       => $req[otx_wutamde],
                    'shesamowmebeli_duration_count_5'       => $req[xut_wutamde],
                    'shesamowmebeli_duration_count_10'      => $req[at_wutamde],
                    'shesamowmebeli_duration_count_15'      => $req[txutme_meti],
                    'shesamowmebeli_operators_count_all'    => $sum_operators_count,
                    'shesamowmebeli_operators_filter'       => $data,
                    'shesamowmebeli_operators_filter_count' => $data1);
    
    return $array;
}

function get_shemowmebuli_result($start_date, $end_date){
    global $db;
    $user_id = $_SESSION[USERID];
    
    $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
    
    $filter = '';
    $user_info = $db->getResultArray();
    if ($user_info[result][0][group_id] != 1) {
        $filter = "AND (monitoring.operator_id = '$user_id' OR monitoring.monitoring_user_id = '$user_id')";
    }
    
    $db->setQuery(" SELECT    COUNT(*) all_count,
                    		  operator.id,
                    		  operator.`name`
                    FROM      monitoring
                    JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                    JOIN      user_info ON monitoring.monitoring_user_id = user_info.user_id
                    JOIN      user_info AS `operator` ON monitoring.operator_id = `operator`.user_id
                    WHERE     monitoring.call_type_id > 0
                    AND       monitoring.monitoring_status_id = 1 AND monitoring.rate_date BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                    AND       NOT ISNULL(monitoring.monitoring_main_id) $filter
                    GROUP BY  monitoring.operator_id");
    
    $res = $db->getResultArray();
    $data  = '';
    
    $sum_operators_count = 0;
    foreach ($res[result] AS $row){
        $sum_operators_count += $row[all_count];
        $data .= '<tr style="height:5px"></tr>
                  <tr>
                      <td>
                         <input class="shemowmebuli_operator_filter" type="checkbox" checked value="'.$row[id].'">
                      </td>
                      <td>
                         <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                      </td>
                      <td>
                         <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold; text-align:left">'.$row[all_count].'</label>
                       </td>
                  </tr>';
        
    }
    
    $db->setQuery(" SELECT    COUNT(*) AS `percent_count`,
                              SUM(IF(monitoring.monitoring_status_id = 1,1,0)) all_count,
        					  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.distribution_type = 1,1,0)) AS `auto`,
        					  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.distribution_type = 2,1,0)) AS `manual`,
        					  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_type_id = 906,1,0)) AS `informacia`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_type_id = 936,1,0)) AS `problema`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_type_id = 952,1,0)) AS `pretenzia`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_type_id = 1138,1,0)) AS `carieli`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_type_id = 1312,1,0)) AS `sxva`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.source_id = 1,1,0)) AS `phone`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.source_id = 4,1,0)) AS `live_chat`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.source_id = 11,1,0)) AS `site_chat`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.source_id = 9,1,0)) AS `viber`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.source_id = 6,1,0)) AS `fb`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.source_id = 7,1,0)) AS `mail`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.action_type_id = 1,1,0)) AS `outgoing`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.action_type_id = 2,1,0)) AS `task`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.action_type_id = 3,1,0)) AS `hold`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.action_type_id = 4,1,0)) AS `block`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration > 0 AND monitoring.call_duration < 60,1,0)) AS `ert_wutamde`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration > 0 AND monitoring.call_duration < 120,1,0)) AS `or_wutamde`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration > 0 AND monitoring.call_duration < 180,1,0)) AS `sam_wutamde`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration > 0 AND monitoring.call_duration < 240,1,0)) AS `otx_wutamde`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration > 0 AND monitoring.call_duration < 300,1,0)) AS `xut_wutamde`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration > 0 AND monitoring.call_duration < 600,1,0)) AS `at_wutamde`,
            				  SUM(IF(monitoring.monitoring_status_id = 1 AND monitoring.call_duration >= 900,1,0)) AS `txutme_meti`
                    FROM      monitoring
                    JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                    JOIN      monitoring_rate ON monitoring_rate.monitoring_id = monitoring.id
                    JOIN      users ON monitoring.monitoring_user_id = users.id
                    WHERE     monitoring.call_type_id > 0 AND monitoring.rate_date BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                    AND       monitoring.monitoring_status_id IN(1) $filter
                    AND       NOT ISNULL(monitoring.monitoring_main_id)");
    
    $res = $db->getResultArray();
    $req = $res[result][0];
    $all_action   = $req[outgoing]+$req[task]+$req[hold]+$req[block];
    $all_duration = $req[phone];
    
    $array = array( 'shemowmebuli_request_count'          => $req[all_count],
                    'shemowmebuli_shedegi_request_count'          => round($req[all_count]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_manual_rate_count'      => $req[manual],
                    'shemowmebuli_shedegi_manual_rate_count'      => round($req[manual]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_auto_rate_count'        => $req[auto],
                    'shemowmebuli_shedegi_auto_rate_count'        => round($req[auto]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_call_type_all'          => $req[all_count],
                    'shemowmebuli_shedegi_call_type_all'          => round($req[all_count]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_call_type_906'          => $req[informacia],
                    'shemowmebuli_shedegi_call_type_906'          => round($req[informacia]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_call_type_936'          => $req[problema],
                    'shemowmebuli_shedegi_call_type_936'          => round($req[problema]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_call_type_952'          => $req[pretenzia],
                    'shemowmebuli_shedegi_call_type_952'          => round($req[pretenzia]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_call_type_1138'         => $req[carieli],
                    'shemowmebuli_shedegi_call_type_1138'         => round($req[carieli]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_call_type_1312'         => $req[sxva],
                    'shemowmebuli_shedegi_call_type_1312'         => round($req[sxva]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_all'       => $req[all_count],
                    'shemowmebuli_shedegi_source_count_all'       => round($req[all_count]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_1'         => $req[phone],
                    'shemowmebuli_shedegi_source_count_1'         => round($req[phone]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_4'         => $req[live_chat],
                    'shemowmebuli_shedegi_source_count_4'         => round($req[live_chat]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_11'        => $req[site_chat],
                    'shemowmebuli_shedegi_source_count_11'        => round($req[site_chat]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_9'         => $req[viber],
                    'shemowmebuli_shedegi_source_count_9'         => round($req[viber]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_6'         => $req[fb],
                    'shemowmebuli_shedegi_source_count_6'         => round($req[fb]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_source_count_7'         => $req[mail],
                    'shemowmebuli_shedegi_source_count_7'         => round($req[mail]/$req[percent_count]*100,2).'%',
                    'shemowmebuli_action_count_all'       => $all_action,
                    'shemowmebuli_action_count_2'         => $req[task],
                    'shemowmebuli_action_count_4'         => $req[block],
                    'shemowmebuli_action_count_3'         => $req[hold],
                    'shemowmebuli_action_count_1'         => $req[outgoing],
                    'shemowmebuli_duration_count_all'       => $all_duration,
                    'shemowmebuli_duration_count_1'       => $req[ert_wutamde],
                    'shemowmebuli_duration_count_2'       => $req[or_wutamde],
                    'shemowmebuli_duration_count_3'       => $req[sam_wutamde],
                    'shemowmebuli_duration_count_4'       => $req[otx_wutamde],
                    'shemowmebuli_duration_count_5'       => $req[xut_wutamde],
                    'shemowmebuli_duration_count_10'      => $req[at_wutamde],
                    'shemowmebuli_duration_count_15'      => $req[txutme_meti],
                    'shemowmebuli_operators_count_all'    => $sum_operators_count,
                    'shemowmebuli_operators_filter'       => $data,
                    'shemowmebuli_operators_filter_count' => $data1);
    
    return $array;
}

function get_gasachivrebuli_result(){
    $user_id = $_SESSION[USERID];
    
    
    $sum_operators_count = 0;
    while ($row = mysql_fetch_array($res)){
        $sum_operators_count += $row[all_count];
        $data .= '<tr style="height:5px"></tr>
                  <tr>
                      <td>
                         <input class="shemowmebuli_operator_filter" type="checkbox" checked value="'.$row[id].'">
                      </td>
                      <td>
                         <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                      </td>
                      <td>
                         <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold; text-align:left">'.$row[all_count].'</label>
                       </td>
                  </tr>';
        
    }
    
    $req = mysql_query("SELECT    crystal_users.`id`,
                                  crystal_users.`name`,
                                  COUNT(*) AS `sul`,
            					  SUM(IF(monitoring_appeal.call_center_manager_agree = 1 AND monitoring_appeal.manager_agree = 1,1,0)) AS `dadasturebuli`,
            					  SUM(IF(monitoring_appeal.call_center_manager_agree = 2 OR monitoring_appeal.manager_agree = 2,1,0)) AS `dakoreqtirebuli`,
            					  COUNT(*)-SUM(IF(monitoring_appeal.call_center_manager_agree = 1 AND monitoring_appeal.manager_agree = 1,1,0))-SUM(IF(monitoring_appeal.call_center_manager_agree = 2 OR monitoring_appeal.manager_agree = 2,1,0)) AS `ar_ganxilula`
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      crystal_users ON monitoring.monitoring_user_id = crystal_users.id
                        LEFT JOIN monitoring_appeal ON monitoring.id = monitoring_appeal.monitoring_id
                        WHERE     monitoring.call_type_id > 0
                        AND       monitoring.monitoring_status_id = 2
                        AND       NOT ISNULL(monitoring.monitoring_main_id)
                        GROUP BY  crystal_users.id");
    
    
    $res = mysql_query("SELECT    crystal_users.`id`,
                                  crystal_users.`name`,
                                  COUNT(*) AS `sul`,
            					  SUM(IF(monitoring_appeal.call_center_manager_agree = 1 AND monitoring_appeal.manager_agree = 1,1,0)) AS `dadasturebuli`,
            					  SUM(IF(monitoring_appeal.call_center_manager_agree = 2 OR monitoring_appeal.manager_agree = 2,1,0)) AS `dakoreqtirebuli`,
            					  COUNT(*)-SUM(IF(monitoring_appeal.call_center_manager_agree = 1 AND monitoring_appeal.manager_agree = 1,1,0))-SUM(IF(monitoring_appeal.call_center_manager_agree = 2 OR monitoring_appeal.manager_agree = 2,1,0)) AS `ar_ganxilula`
                        FROM      monitoring
                        JOIN      monitoring_main ON monitoring_main.id = monitoring.monitoring_main_id
                        JOIN      crystal_users ON monitoring.operator_id = crystal_users.id
                        LEFT JOIN monitoring_appeal ON monitoring.id = monitoring_appeal.monitoring_id
                        WHERE     monitoring.call_type_id > 0
                        AND       monitoring.monitoring_status_id = 2
                        AND       NOT ISNULL(monitoring.monitoring_main_id)
                        GROUP BY  crystal_users.id");
    
    
    while ($row = mysql_fetch_array($req)) {
        
        $sum_all1             += $row[sul];
        $sum_dadasturebuli1   += $row[dadasturebuli];
        $sum_dakoreqtirebuli1 += $row[dakoreqtirebuli];
        $sum_ar_ganxilula1    += $row[ar_ganxilula];
        
        $tr .= '<tr>
                    <td style="width: 20px;">
                        <input class="gasachivrebuli_monitoring_filter"  type="checkbox" checked value="'.$row[id].'">
                    </td>
                    <td style="width: 180px;">
                        <label  style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                    </td>
                    <td style="width: 60px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row[sul].'</label>
                    </td>
                    <td style="width: 60px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row[dadasturebuli].'</label>
                    </td>
                    <td style="width: 60px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row[dakoreqtirebuli].'</label>
                    </td>
                    <td style="width: 60px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row[ar_ganxilula].'</label>
                    </td>
                </tr>
                <tr style="height:5px"></tr>';
    }
    
    $data = '<table style="width: 500px;">
                <tr style="height:5px"></tr>
                <tr>
                    <td colspan="2" style="width: 200px;">
                    </td>
                    <td style="width: 60px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">სულ</label>
                    </td>
                    <td style="width: 60px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">დადასტურდა</label>
                    </td>
                    <td style="width: 120px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">დაკორექტირდა</label>
                    </td>
                    <td style="width: 100px;">
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">არ განხილულა</label>
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td colspan="2">
                        <label  style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">მომართვები</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_all1.'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_dadasturebuli1.'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_dakoreqtirebuli1.'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_ar_ganxilula1.'</label>
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                '.$tr.'
            </table>';
    
    $sum_all = 0;
    while ($row1 = mysql_fetch_array($res)) {
        $sum_all             += $row1[sul];
        $sum_dadasturebuli   += $row1[dadasturebuli];
        $sum_dakoreqtirebuli += $row1[dakoreqtirebuli];
        $sum_ar_ganxilula    += $row1[ar_ganxilula];
        $tr1 .= '<tr>
                    <td>
                        <input class="gasachivrebuli_operator_filter"  type="checkbox" checked value="'.$row1[id].'">
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$row1[name].'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row1[sul].'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row1[dadasturebuli].'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row1[dakoreqtirebuli].'</label>
                    </td>
                    <td>
                        <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$row1[ar_ganxilula].'</label>
                    </td>
                </tr>
                <tr style="height:5px"></tr>';
    }
    
    $data1 = '<table style="width: 500px;">
                    <tr style="height:5px"></tr>
                    <tr>
                        <td colspan="2" style="width: 200px;">
                        </td>
                        <td style="width: 60px;">
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">სულ</label>
                        </td>
                        <td style="width: 60px;">
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">დადასტურდა</label>
                        </td>
                        <td style="width: 120px;">
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">დაკორექტირდა</label>
                        </td>
                        <td style="width: 100px;">
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">არ განხილულა</label>
                        </td>
                    </tr>
                    <tr style="height:5px"></tr>
                    <tr>
                        
                        <td colspan="2">
                            <label  style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">ოპერატორების მიხედვით</label>
                        </td>
                        <td>
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_all.'</label>
                        </td>
                        <td>
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_dadasturebuli.'</label>
                        </td>
                        <td>
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_dakoreqtirebuli.'</label>
                        </td>
                        <td>
                            <label  style="padding: 2px 0 0 0; font-weight: bold; text-align: center;">'.$sum_ar_ganxilula.'</label>
                        </td>
                    </tr>
                    <tr style="height:5px"></tr>
                    '.$tr1.'
'.$tr1.'
                </table>';
    $all_action   = $req[outgoing]+$req[task]+$req[hold]+$req[block];
    $all_duration = $req[phone];
    
    $array = array('operators'   => $data,
                   'monitorings' => $data1);
    
    return $array;
}

function get_calculation_result($start, $end, $id, $count=0){
    $user_id = $_SESSION[USERID];
    
    $res_info = mysql_fetch_array(mysql_query("SELECT COUNT(*) all_count,
                                                      SUM(IF(ISNULL(monitoring.monitoring_main_id),1,0)) AS `dasaformirebeli`,
                                                      SUM(IF(ISNULL(monitoring.monitoring_main_id),0,1)) AS `daformirebuli`
                                               FROM   monitoring
                                               WHERE  DATE(FROM_UNIXTIME(monitoring.request_date)) BETWEEN '$start' AND '$end'
                                               AND    monitoring.call_type_id > 0"));
    
    $res = mysql_fetch_array(mysql_query("SELECT SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 906,1,0)) AS `informacia`,
                                                 SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 936,1,0)) AS `problema`,
                                                 SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 952,1,0)) AS `pretenzia`,
                                                 SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 1138,1,0)) AS `carieli`,
                                                 SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 1312,1,0)) AS `sxva`
                                          FROM   monitoring
                                          WHERE  DATE(FROM_UNIXTIME(monitoring.request_date)) BETWEEN '$start' AND '$end'
                                          AND    monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)"));
    
    $req = mysql_fetch_array(mysql_query("  SELECT COUNT(*) all_count,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id),1,0)) AS `dasaformirebeli`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id),0,1)) AS `daformirebuli`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 906,1,0)) AS `informacia`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 936,1,0)) AS `problema`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 952,1,0)) AS `pretenzia`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 1138,1,0)) AS `carieli`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = 1312,1,0)) AS `sxva`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 1,1,0)) AS `phone`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 4,1,0)) AS `live_chat`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 11,1,0)) AS `site_chat`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 9,1,0)) AS `viber`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 6,1,0)) AS `fb`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.source_id = 7,1,0)) AS `mail`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 1,1,0)) AS `outgoing`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 2,1,0)) AS `task`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 3,1,0)) AS `hold`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.action_type_id = 4,1,0)) AS `block`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 60,1,0)) AS `ert_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 120,1,0)) AS `or_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 180,1,0)) AS `sam_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 240,1,0)) AS `otx_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 300,1,0)) AS `xut_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration > 0 AND monitoring.call_duration < 600,1,0)) AS `at_wutamde`,
                                                   SUM(IF(ISNULL(monitoring.monitoring_main_id) AND monitoring.call_duration >= 900,1,0)) AS `txutme_meti`
                                            FROM   monitoring
                                            JOIN   monitoring_temp ON monitoring_temp.monitoring_id = monitoring.id
                                            WHERE  DATE(FROM_UNIXTIME(monitoring.request_date)) BETWEEN '$start' AND '$end'
                                            AND    monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                                            AND    monitoring_temp.actived = 1"));
    
    $all_request         = $res_info[all_count];
    $ukve_dafformirebuli = $res_info[daformirebuli];
    $dasaformirebeli     = $res_info[dasaformirebeli];
    $info_count_real      = $res[informacia];
    $problem_count_real   = $res[problema];
    $claim_count_real     = $res[pretenzia];
    $empty_count_real     = $res[carieli];
    $other_count_real     = $res[sxva];
    
    $ganawilebuli_info    = $req[informacia]; 
    $ganawilebuli_problem = $req[problema];
    $ganawilebuli_claim   = $req[pretenzia];
    $ganawilebuli_empty   = $req[carieli]; 
    $ganawilebuli_other   = $req[sxva];
    
    $page = GetPage_gasanawilebeli($req, $all_request, $info_count_real, $problem_count_real, $claim_count_real, $empty_count_real, $other_count_real, $ukve_dafformirebuli, $dasaformirebeli, $ganawilebuli_info, $ganawilebuli_problem, $ganawilebuli_claim, $ganawilebuli_empty, $ganawilebuli_other, $id);
    return $page;
}

function calculation_monitoring_base_caunt($start, $end, $id, $count=0, $increment_id){
    global $db;
    
    $db->setQuery("DELETE FROM monitoring_temp");
    $db->execQuery();
    
    $db->setQuery(" SELECT id,
                    	   distribute_random,
                           all_request_percent
                    FROM   monitoring_shablon
                    WHERE  monitoring_shablon.actived = 1");
    
    $check_query = $db->getNumRow();
    $shablon_res = $db->getResultArray();
    
    $shablon_id        = $shablon_res[result][0][id];
    $distribute_random = $shablon_res[result][0][distribute_random];
    $all_percent       = $shablon_res[result][0][all_request_percent];
    
    if ($count == 0) {
        $db->setQuery(" SELECT ROUND($all_percent/100*COUNT(*)) AS `all_count`
                        FROM   monitoring
                        WHERE  FROM_UNIXTIME(monitoring.request_date) BETWEEN '$start' AND '$end'
                        AND    monitoring.call_type_id > 0 AND monitoring.operator_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                        AND    monitoring.call_type_id IN(SELECT type_id FROM monitoring_shablon_type_percent WHERE shablon_id = '$shablon_id')
                        AND    monitoring.department_id IN(SELECT department_id FROM monitoring_shablon_department WHERE shablon_id = '$shablon_id')
                        AND    monitoring.source_id IN(SELECT source_id FROM monitoring_shablon_source WHERE shablon_id = '$shablon_id')");
        
        $res = $db->getResultArray();
        $all_count = $res[result][0][all_count];
    }else{
        $all_count = $count;
    }
    
    if ($check_query == 1 && $distribute_random == 0) {
        
        $db->setQuery(" SELECT  ROUND(monitoring_shablon_type_percent.percent/100*$all_count) AS request_type_count,
                        		monitoring_shablon_type_percent.type_id
                        FROM    monitoring_shablon_type_percent
                        WHERE   monitoring_shablon_type_percent.shablon_id = '$shablon_id'");
        
        $res_type = $db->getResultArray();
        
    }elseif ($check_query == 1 && $shablon_res[result][0][distribute_random] == 1){
        
        $db->setQuery(" SELECT ROUND(0.05*$all_count) AS request_type_count,
                        	   id AS `type_id`
                        FROM  `mepa_project_types`
                        WHERE  actived = 1");
        
        $res_type = $db->getResultArray();
        
    }else{
        $res_type = '';
    }
    
    
    foreach ($res_type[result] AS $row_temp){
        
        $db->setQuery("INSERT INTO `monitoring_count_temp`
                                  (`monitoring_main_id`, `type_id`, `count`) 
                            VALUES
                                  ('$increment_id', '$row_temp[type_id]', '$row_temp[request_type_count]')");
        
        $db->execQuery();
    }
    
    
    $db->setQuery(" SELECT    call_type_id,
                    		  COUNT(*) AS `real_count`,
            				  CASE
            					 WHEN COUNT(*) >= monitoring_count_temp.count AND monitoring_count_temp.count > 0 THEN monitoring_count_temp.count
            					 WHEN COUNT(*) < monitoring_count_temp.count AND monitoring_count_temp.count > 0 THEN COUNT(*)
            					 ELSE 0
            				  END AS `us_count`
                    FROM      monitoring
                    LEFT JOIN monitoring_count_temp ON monitoring_count_temp.type_id = monitoring.call_type_id AND monitoring_count_temp.monitoring_main_id = '$increment_id'
                    WHERE     FROM_UNIXTIME(monitoring.request_date) BETWEEN '$start' AND '$end'
                    AND       ISNULL(monitoring.monitoring_main_id) AND monitoring.operator_id > 0
                    AND       monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                    AND       monitoring.call_type_id IN(SELECT type_id FROM monitoring_shablon_type_percent WHERE shablon_id = '$shablon_id')
                    AND       monitoring.department_id IN(SELECT department_id FROM monitoring_shablon_department WHERE shablon_id = '$shablon_id')
                    AND       monitoring.source_id IN(SELECT source_id FROM monitoring_shablon_source WHERE shablon_id = '$shablon_id')
                    GROUP BY  monitoring.call_type_id");
    
    $res_real_count_request_type = $db->getResultArray();
    
    foreach ($res_real_count_request_type[result] AS $row_real_count){
        
        $db->setQuery(" UPDATE `monitoring_count_temp` 
                           SET  us_count    = $row_real_count[us_count],
                               `not_count`  = IF(count-$row_real_count[us_count] < 0, 0, count-$row_real_count[us_count]),
                               `real_count` = $row_real_count[real_count]
                        WHERE   type_id     = $row_real_count[call_type_id]");
        
        $db->execQuery();
        
        $db->setQuery("INSERT INTO `monitoring_count_real_count`
                                  (`monitoring_main_id`, `type_id`, `real_count`) 
                            VALUES 
                                  ('$increment_id', '$row_real_count[call_type_id]', '$row_real_count[real_count]')");
        $db->execQuery();
    }
    
    $db->setQuery(" SELECT  type_id AS `call_type_id`,
                            real_count AS `real_count`,
                            us_count AS `us_count`
                    FROM    monitoring_count_temp
                    WHERE   real_count = 0 AND monitoring_count_temp.monitoring_main_id = '$increment_id'");
    
    $res_real_count_request_type1 = $db->getResultArray();
    
    foreach ($res_real_count_request_type1[result] AS $row_real_count1){
        
        $db->setQuery(" UPDATE `monitoring_count_temp`
                           SET  us_count    = $row_real_count1[us_count],
                               `not_count`  = IF(count-$row_real_count1[us_count] < 0, 0, count-$row_real_count1[us_count]),
                               `real_count` = $row_real_count1[real_count]
                        WHERE   type_id     = $row_real_count1[call_type_id]");
        
        $db->execQuery();
        
        $db->setQuery("INSERT INTO `monitoring_count_real_count`
                                   (`monitoring_main_id`, `type_id`, `real_count`)
                            VALUES
                                   ('$increment_id', '$row_real_count1[call_type_id]', '$row_real_count1[real_count]')");
        $db->execQuery();
    }
    
    $db->setQuery("SELECT SUM(not_count) AS not_used
                   FROM  `monitoring_count_temp` 
                   WHERE  monitoring_main_id = $increment_id");
    
     $sum_not_used = $db->getResultArray();
    
     $not_used = $sum_not_used[result][0][not_used];
   
     $db->setQuery("SELECT  (monitoring_count_temp.real_count - monitoring_count_temp.us_count) AS sold,
                    	     monitoring_count_temp.id
                    FROM     monitoring_shablon_type_percent 
                    JOIN     monitoring_count_temp ON monitoring_count_temp.type_id = monitoring_shablon_type_percent.type_id AND monitoring_count_temp.monitoring_main_id = '$increment_id'
                    WHERE    monitoring_shablon_type_percent.shablon_id = '$shablon_id' AND monitoring_count_temp.real_count - monitoring_count_temp.us_count > 0
                    ORDER BY monitoring_shablon_type_percent.percent DESC");
     
     $distributed_res = $db->getResultArray();
     
     $all_count_not_us = $not_used;
     foreach ($distributed_res[result] AS $row_not_us){
         if ($all_count_not_us > 0) {
             if ($all_count_not_us <= $row_not_us[sold]) {
                 $db->setQuery("UPDATE `monitoring_count_temp`
                                   SET `count`     = `count` + $all_count_not_us,
                                       `us_count`  = `us_count` + $all_count_not_us
                                WHERE   id         = $row_not_us[id]");
                 $db->execQuery();
                 
                 $all_count_not_us = 0;
             }else{
                 $db->setQuery("UPDATE `monitoring_count_temp`
                                   SET `count`     = `count` + $row_not_us[sold],
                                       `us_count`  = `us_count` + $row_not_us[sold]
                                WHERE   id         = $row_not_us[id]");
                 $db->execQuery();
                 
                 $all_count_not_us = $all_count_not_us - $row_not_us[sold];
             }
         }
     }
     
     $db->setQuery(" SELECT type_id, us_count
                     FROM  `monitoring_count_temp`
                     WHERE  us_count > 0 AND monitoring_main_id = '$increment_id'");
     
     $res_for_temp = $db->getResultArray();
     
     foreach ($res_for_temp[result] AS $row_monit_temp){
         
         $db->setQuery(" SELECT id
                         FROM   monitoring
                         WHERE  FROM_UNIXTIME(monitoring.request_date) BETWEEN '$start' AND '$end'
                         AND    ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = $row_monit_temp[type_id]
                         AND    monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                         AND    monitoring.operator_id > 0
                         AND    monitoring.call_type_id IN(SELECT type_id FROM monitoring_shablon_type_percent WHERE shablon_id = '$shablon_id')
                         AND    monitoring.department_id IN(SELECT department_id FROM monitoring_shablon_department WHERE shablon_id = '$shablon_id')
                         AND    monitoring.source_id IN(SELECT source_id FROM monitoring_shablon_source WHERE shablon_id = '$shablon_id')
                         LIMIT  $row_monit_temp[us_count]");
         
         $res_us_count = $db->getResultArray();
         
         foreach($res_us_count[result] AS $row_us) {
             $values .='('.$increment_id.','.$row_us[id].', 2),';
         }
     }
     
     $values = substr($values, 0, -1);
     if ($values != '') {
         $db->setQuery("INSERT INTO monitoring_temp
                                   (monitoring_main_id, monitoring_id, distribution_type)
                             VALUES
                                   $values");
     }
     
     
     $db->execQuery();
     
     $db->setQuery("DELETE FROM monitoring_count_temp WHERE monitoring_count_temp.monitoring_main_id = $increment_id");
     $db->execQuery();
}

function ganawileba_operatorebis_mixedvit($ganawilebuli_count, $type_id, $start, $end){
    $check_operators = mysql_query("SELECT   operator_id,
                                             COUNT(*) AS `count`
                                    FROM     monitoring
                                    WHERE    FROM_UNIXTIME(monitoring.request_date) BETWEEN '$start' AND '$end'
                                    AND      ISNULL(monitoring.monitoring_main_id) AND monitoring.call_type_id = $type_id
                                    AND      monitoring.call_type_id > 0 AND monitoring.operator_id > 0 AND ISNULL(monitoring.monitoring_main_id) AND monitoring.operator_id > 0
                                    GROUP BY monitoring.operator_id
                                    ORDER BY COUNT(*) ASC");
    
    $num_row           = mysql_num_rows($check_operators);
    $op_count          = ($ganawilebuli_count-($ganawilebuli_count % $num_row)) / $num_row;
    $op_count_delta    = $ganawilebuli_count % $num_row;
    
    $op_arr            = array();
    $op_i              = 0;
    $op_delta          = 0;
    $check_info        = $ganawilebuli_count;
    $op_mnishvnelovani = 0;
    
    while ($op_row = mysql_fetch_array($check_operators)) {
        
        if ($op_count > 0) {
            
            if ($op_count_delta > 0) {
                $op_mnishvnelovani = 1;
                $op_count_delta --;
            }else{
                $op_mnishvnelovani = 0;
            }
            
            if (($op_count + $op_delta + $op_mnishvnelovani) > $op_row[count]) {
                $op_arr[$op_i]['gasanawilebeli_count'] = $op_row[count];
                $op_delta                              = ($op_count + $op_delta + $op_mnishvnelovani) - $op_row[count];
                $op_arr[$op_i]['user_id']              = $op_row[operator_id];
            }else{
                $op_arr[$op_i]['gasanawilebeli_count'] = ($op_count + $op_delta + $op_mnishvnelovani);
                $op_arr[$op_i]['user_id']              = $op_row[operator_id];
                $op_delta                              = 0;
            }
            
        }else{
            
            if ($check_info > 0) {
                $op_arr[$op_i]['gasanawilebeli_count'] = 1;
                $op_arr[$op_i]['user_id']    = $op_row[operator_id];
                $check_info --;
            }else{
                $op_arr[$op_i]['gasanawilebeli_count'] = 0;
                $op_arr[$op_i]['user_id']    = $op_row[operator_id];
            }
            
        }
        
        $op_i ++;
    }
    
    return $op_arr;
}

function calculation_monitoring_page($start, $end, $id, $incrment_main_id, $shablon_id){
    global $db;
    
    $user_id = $_SESSION[USERID];
    
    $db->setQuery(" SELECT COUNT(*) all_count,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id),1,0)) AS `dasaformirebeli`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id),0,1)) AS `daformirebuli`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 60 AND monitoring_temp.id > 0,1,0)) AS `ert_wutamde`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 120 AND monitoring_temp.id > 0,1,0)) AS `or_wutamde`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 180 AND monitoring_temp.id > 0,1,0)) AS `sam_wutamde`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 240 AND monitoring_temp.id > 0,1,0)) AS `otx_wutamde`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 300 AND monitoring_temp.id > 0,1,0)) AS `xut_wutamde`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time > 0 AND asterisk_call_log.talk_time < 600 AND monitoring_temp.id > 0,1,0)) AS `at_wutamde`,
                           SUM(IF(ISNULL(monitoring.monitoring_main_id) AND asterisk_call_log.talk_time >= 900 AND monitoring_temp.id > 0,1,0)) AS `txutme_meti`,
                           (SELECT monitoring_radio_duration FROM monitoring_shablon WHERE actived = 1) AS `monitoring_radio_duration`
                    FROM   monitoring
                    LEFT JOIN monitoring_temp ON monitoring_temp.monitoring_id = monitoring.id AND monitoring_temp.monitoring_main_id = '$incrment_main_id' AND monitoring_temp.actived = 1
                    LEFT JOIN asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
                    WHERE  FROM_UNIXTIME(monitoring.request_date) BETWEEN '$start' AND '$end'
                    AND    monitoring.call_type_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                    AND monitoring.operator_id > 0");
    
    $res          = $db->getResultArray();
    $req          = $res[result][0];
    $sum_duration = $req[ert_wutamde]+$req[or_wutamde]+$req[sam_wutamde]+$req[otx_wutamde]+$req[xut_wutamde]+$req[at_wutamde]+$req[txutme_meti];
    
    $page         = GetPage_gasanawilebeli($req, $id, $incrment_main_id, $sum_duration);
    
    return $page;
}

function GetPage_gasanawilebeli($res, $id, $increm_id, $sum_duration){
    $all_request          = $res[all_count];
    $dasaformirebeli      = $res[dasaformirebeli];
    $ukve_dafformirebulit = $res[daformirebuli];
    
    $check1 = '';
    $check2 = '';
    $check3 = '';
    $check4 = '';
    $check5 = '';
    $check10 = '';
    $check15 = '';
    
    if ($res[monitoring_radio_duration] == 1) {
        $check1 = 'checked';
    }elseif ($res[monitoring_radio_duration] == 2){
        $check2 = 'checked';
    }elseif ($res[monitoring_radio_duration] == 3){
        $check3 = 'checked';
    }elseif ($res[monitoring_radio_duration] == 4){
        $check4 = 'checked';
    }elseif ($res[monitoring_radio_duration] == 5){
        $check5 = 'checked';
    }elseif ($res[monitoring_radio_duration] == 10){
        $check10 = 'checked';
    }elseif ($res[monitoring_radio_duration] == 15){
        $check15 = 'checked';
    }
    
    $data  .= '
	<div id="dialog-form" >
            <input type="hidden" value="'.$increm_id.'" id="hidde_monit_main_id">
            <div style="width: 100%; display: inline-flex; margin-top:5px;     font-family: BPG; font-weight: bold;">
                <div style="width: 220px; margin-top: 10px;">
                	<a style="width: 100%;">მომართვები</a><a id="all_reques_period" style="width: 100%; margin-left: 58px;">'.$all_request.'</a>
                    <table style="width: 100%;">
                        <tr style="height:5px"></tr>
                        <tr>
                            <td style="width: 150px;">
                                <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold; color: #cac9c9;">უკვე დაფორმირებული</label>
                            </td>
                            <td>
                                <label id="ukve_daformirebuli" style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$ukve_dafformirebulit.'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <label  style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">დასაფორმირებელი</label>
                            </td>
                            <td>
                                <label id="dasaformirebeli" style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold;">'.$dasaformirebeli.'</label>
                            </td>
                        </tr>
                        <tr style="height:10px"></tr>
                        <tr>
                            <td colspan="2" >
                                <button style="width: 96%;cursor: pointer;border: 0px;color: #fff;height: 25px;padding: 0px 0px 0px 0px;border-top: 1px solid #84b5d9; background: #8BC34A;font-family: pvn;" id="show_monitoring_table" check_show="0">მაჩვენე ცხრილი</button>
                            </td>
                        </tr>
                        <tr style="height:30px"></tr>
                        <tr>
                            <td colspan="2">
                                <label style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold; text-align:center">შესრულების სავადაუდო ხ-ბა</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label id="shesrulebis_savaraudo_dro" style="padding: 2px 0 0 0;font-family: BPG; font-weight: bold; text-align:center; font-size:15px;">00:00:00</label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 27%;">
                    <table class="dialog-form-table" style="margin-right: 30px;">
                        '.Getmonitoring_type($increm_id, $id).'
                    </table>
                </div>
                <div style="width: 25%;">
                    <table class="dialog-form-table" style="float: left;">
                        '.Getmonitoring_project($increm_id, $id).'
                    </table>
                </div>
                <div style="width: 17%;">
                    <table class="dialog-form-table" style="float: left;">
                        '.Getmonitoring_source($increm_id, $id).'
                    </table>
                </div>
                <div style="width: 18%;">
                    <table class="dialog-form-table" style="float: left;">
                        '.Getmonitoring_action($increm_id, $id).'
                    </table>
                </div>
                
                <div style="width: 13%;">
                    <table class="dialog-form-table" style="float: left;">
                        <tr style="height:5px;"></tr>
                        <tr>
                            <td colspan="2" style="width: 100px;">
                                <a style="width: 100%;">ხ-ბას მიხედვით</a>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold; padding: 6px 0 3px 1px;">'.$sum_duration.'</label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 25px;">
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio1" '.$check1.' value="1">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">1 წთ-მდე</label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[ert_wutamde].'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio2" '.$check2.' value="2">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">2 წთ-მდე</label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[or_wutamde].'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio3" '.$check3.' value="3">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">3 წთ-მდე</label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[sam_wutamde].'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio4" '.$check4.' value="4">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">4 წთ-მდე</label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[otx_wutamde].'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio5" '.$check5.' value="5">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">5 წთ-მდე</label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[xut_wutamde].'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio10" '.$check10.' value="10">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">10 წთ-მდე</label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[at_wutamde].'</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input class="radio_input_task_time" name="monitoring_radio" type="radio" id="radio15" '.$check15.' value="15">
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">15 წთ ></label>
                            </td>
                            <td>
                                <label style="font-family: BPG; font-weight: bold;">'.$res[txutme_meti].'</label>
                            </td>
                        </tr>
                   </table>
                </div>
            </div>
     </div>';
    
    return $data;
}

function Getmonitoring_type($increm_id, $id=0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT  mepa_project_types.id, 
                				mepa_project_types.`name`, 
                				IFNULL(monit.count,0) AS `count`, 
                				IFNULL(monitoring_count_real_count.real_count,0) AS real_count
                        FROM    mepa_project_types
                        LEFT JOIN    (SELECT   COUNT(*) AS `count`,
                        					   call_type_id
        							  FROM     monitoring_temp
        							  JOIN     monitoring ON monitoring.id = monitoring_temp.monitoring_id
        							  WHERE    monitoring.call_type_id > 0 AND monitoring.operator_id > 0 AND ISNULL(monitoring.monitoring_main_id)
                                      AND      monitoring_temp.actived = 1
        							  GROUP BY monitoring.call_type_id) AS `monit` ON monit.call_type_id = mepa_project_types.id
                        LEFT JOIN monitoring_count_real_count ON monitoring_count_real_count.type_id = mepa_project_types.id AND monitoring_count_real_count.monitoring_main_id = '$increm_id'
                        WHERE mepa_project_types.actived = 1
                        ORDER BY  mepa_project_types.id ASC");
    }else{
        $db->setQuery(" SELECT    mepa_project_types.id, 
            					  mepa_project_types.`name`,
            					  COUNT(monitoring.call_type_id) AS `count`,
            					  IFNULL(monitoring_count_real_count.real_count,0) AS `real_count`
                        FROM     `mepa_project_types`
                        LEFT JOIN monitoring ON monitoring.call_type_id = mepa_project_types.id
                        AND       monitoring.monitoring_main_id = '$increm_id'
                        LEFT JOIN monitoring_count_real_count ON monitoring_count_real_count.type_id = mepa_project_types.id
                        AND       monitoring_count_real_count.monitoring_main_id = '$increm_id'
                        WHERE     mepa_project_types.actived = 1
                        GROUP BY  mepa_project_types.id
                        ORDER BY  mepa_project_types.id ASC");
    }
        
    $req = $db->getResultArray();
    
    $dis = '';
    if ($id > 0) {
        $dis = "disabled";
    }
    
    $sum_real_count = 0;
    $sum_count = 0;
    
    foreach ($req[result] AS $row){
        if ($row[count] > 0) {
            $check     = 'checked';
            if ($id > 0) {
                $dis = "disabled";
            }else{
                $dis = '';
            }
        }else{
            $check = "";
            $dis   = "disabled";
        }
        $data .= '<tr>
                        <td style="width: 20px;">
                            <input class="monitoring_call_type_checkbox" type="checkbox"  '.$check.' value="'.$row[id].'" '.$dis.'>
                        </td>
                        <td>
                            <label style="font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                        </td>
                        <td>
                            <label style="font-family: BPG; font-weight: bold; padding:0px;" id="request_type_real_count'.$row[id].'">'.$row[real_count].'</label>
                        </td>
                        <td style="width: 55px;">
                            <input placeholder="0.00" type_id="'.$row[id].'" class="idle request_type" style="width: 30px; height:20px; text-align: center;" type="text" value="'.$row[count].'" '.$dis.'>
                        </td>
                        <td>
                            <button style="width: 30px; background: none;" class="refresh_monitoring_input" id="refresh_monitoring_input_types" type_id="'.$row[id].'"><img style="width: 18px; height: 18px; padding: 0px; border: solid 0px #85b1de !important;" src="media/images/monitoring_refresh.png"></button>
                        </td>
                  </tr>';
        
        $sum_real_count += $row[real_count];
        $sum_count += $row[count];
    }
        
    $data1  = ' <tr style="height:5px;"></tr>
                <tr>
                    <td colspan="2">
                        <a style="width: 100%;">ტიპის მიხედვით</a>
                    </td>
                    <td style="width: 30px;">
                        <label style="width: 100%; margin-top: 7px;">'.$sum_real_count.'</label>
                    </td>
                    <td style="width: 55px;">
                        <input placeholder="0.00" class="idle" id="manual_call_type_percent" style="width: 30px; height:21px; text-align: center;" type="text" value="'.$sum_count.'">
                    </td>
                    <td>
                        <button style="width: 30px;  background: none;" id="refresh_all_monitoring_input" id="refresh_monitoring_input_types" type_id="'.$row[id].'"><img style="width: 18px; height: 18px; padding: 0px; border: solid 0px #85b1de;" src="media/images/monitoring_refresh.png"></button>
                    </td>
                </tr>';
    
    $data = $data1.$data;
    return $data;
}

function Getmonitoring_project($increm_id, $id=0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT    info_category.id, 
                				  info_category.`name`, 
                				  IFNULL(monit.count,0) AS `count`
                        FROM      info_category
                        LEFT JOIN (SELECT  COUNT(*) AS `count`,
										   department_id
        						  FROM     monitoring_temp
        						  JOIN     monitoring ON monitoring.id = monitoring_temp.monitoring_id
        						  WHERE    monitoring.call_type_id > 0 AND monitoring.department_id> 0
        						  AND      monitoring.operator_id > 0 AND ISNULL(monitoring.monitoring_main_id) AND monitoring_temp.monitoring_main_id = '$increm_id'
                                  AND      monitoring_temp.actived = 1
        						  GROUP BY monitoring.department_id) AS `monit` ON monit.department_id = info_category.id
                        WHERE     info_category.parent_id = 0 AND info_category.actived = 1 AND info_category.name != ''
                        ORDER BY  info_category.id ASC"); 
    }else{
        $db->setQuery(" SELECT    info_category.id, 
                                  info_category.`name`,
                        		  IFNULL(COUNT(monitoring.department_id),0) AS `count`
                        FROM      info_category
                        LEFT JOIN monitoring ON monitoring.department_id = info_category.id AND monitoring.monitoring_main_id = '$increm_id'
                        WHERE     info_category.parent_id = 0 AND info_category.actived = 1 AND info_category.name != ''
                        GROUP BY  info_category.id
                        ORDER BY  info_category.id ASC");
    }
    
    $req = $db->getResultArray();
    
    $dis = '';
    
    if ($id > 0) {
        $dis = "disabled";
    }
    
    $sum_count = 0;
    
    foreach ($req[result] AS $row){
        if ($row[count] > 0) {
            $check     = 'checked';
            if ($id > 0) {
                $dis = "disabled";
            }else{
                $dis = '';
            }
        }else{
            $check = "";
            $dis   = "disabled";
        }
        $data .= '<tr>
                        <td style="width: 20px;">
                            <input class="call_department_checkbox" type="checkbox" '.$check.' value="'.$row[id].'" '.$dis.'>
                        </td>
                        <td>
                            <label style="font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                        </td>
                        <td>
                            <label id="call_department_checkbox'.$row[id].'" style="font-family: BPG; font-weight: bold;">'.$row[count].'</label>
                        </td>
                  </tr>';
        
        $sum_count += $row[count];
    }
     
    $data1  = ' <tr style="height:5px;"></tr>
                <tr>
                    <td colspan="2">
                        <a style="width: 100%;">უწყების მიხედვით</a>
                    </td>
                    <td style="width: 30px;">
                        <label style="width: 100%; margin-top: 7px;">'.$sum_count.'</label>
                    </td>
                </tr>';
    
    $data = $data1.$data;
    
    return $data;
    
}

function Getmonitoring_source($increm_id, $id=0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT    source.id, 
                				  source.`name`, 
                				  IFNULL(monit.count,0) AS `count`
                        FROM      source
                        LEFT JOIN  (SELECT   COUNT(*) AS `count`,
											 source_id
        							FROM     monitoring_temp
        							JOIN     monitoring ON monitoring.id = monitoring_temp.monitoring_id
        							WHERE    monitoring.call_type_id > 0 AND monitoring.source_id> 0
        							AND      monitoring.operator_id > 0 AND ISNULL(monitoring.monitoring_main_id) AND monitoring_temp.monitoring_main_id = '$increm_id'
                                    AND      monitoring_temp.actived = 1
        							GROUP BY monitoring.source_id) AS `monit` ON monit.source_id = source.id

                        WHERE     source.actived = 1
                        ORDER BY  source.id ASC");
    }else{
        $db->setQuery(" SELECT    source.id, 
                                  source.`name`,
                        		  IFNULL(COUNT(monitoring.source_id),0) AS `count`
                        FROM      source
                        LEFT JOIN monitoring ON monitoring.source_id = source.id AND monitoring.monitoring_main_id = '$increm_id'
                        WHERE     source.actived = 1
                        GROUP BY  source.id
                        ORDER BY  source.id ASC");
    }
    
    $req = $db->getResultArray();
    
    $dis = '';
    
    if ($id > 0) {
        $dis = "disabled";
    }
    
    $sum_count = 0;
    $check     = '';
    foreach ($req[result] AS $row){
        
        if ($row[count] > 0) {
            $check = "checked";
            $dis   = "";
        }else{
            $check = "";
            $dis   = "disabled";
        }
        
        $data .= '<tr>
                        <td style="width: 20px;">
                            <input class="monitoring_source_checkbox" type="checkbox" '.$check.' value="'.$row[id].'" '.$dis.'>
                        </td>
                        <td>
                            <label style="font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                        </td>
                        <td>
                            <label id="monitoring_source_checkbox'.$row[id].'" style="font-family: BPG; font-weight: bold;">'.$row[count].'</label>
                        </td>
                  </tr>';
        
        $sum_count += $row[count];
    }
    
    $data1  = ' <tr style="height:5px;"></tr>
                <tr>
                    <td colspan="2">
                        <a style="width: 100%;">წყაროს მიხედვით</a>
                    </td>
                    <td style="width: 30px;">
                        <label style="width: 100%; margin-top: 7px;">'.$sum_count.'</label>
                    </td>
                </tr>';
    
    $data = $data1.$data;
    
    return $data;
}

function Getmonitoring_action($increm_id, $id=0){
    global $db;
    if ($id == 0) {
        $db->setQuery("  SELECT     monitoring_action_type.id, 
                				    monitoring_action_type.`name`, 
                				    IFNULL(monit.count,0) AS `count`
                         FROM       monitoring_action_type
                         LEFT JOIN (SELECT   COUNT(*) AS `count`,
            								 monitoring.action_type_id
                    				FROM     monitoring_temp
                    				JOIN     monitoring ON monitoring.id = monitoring_temp.monitoring_id
                    				WHERE    monitoring.call_type_id > 0 AND monitoring.action_type_id> 0
                    				AND      monitoring.operator_id > 0 AND ISNULL(monitoring.monitoring_main_id) AND monitoring_temp.monitoring_main_id = '$increm_id'
                                    AND      monitoring_temp.actived = 1
                    				GROUP BY monitoring.action_type_id) AS `monit` ON monit.action_type_id = monitoring_action_type.id
                         WHERE      monitoring_action_type.actived = 1
                         ORDER BY   monitoring_action_type.id ASC");
    }else{
        $db->setQuery(" SELECT    monitoring_action_type.id, 
                                  monitoring_action_type.`name`,
                        		  IFNULL(COUNT(monitoring.action_type_id),0) AS `count`
                        FROM      monitoring_action_type
                        LEFT JOIN monitoring ON monitoring.action_type_id = monitoring_action_type.id AND monitoring.monitoring_main_id = 346
                        WHERE     monitoring_action_type.actived = 1
                        GROUP BY  monitoring_action_type.id
                        ORDER BY  monitoring_action_type.id ASC");
    }
    
    $req = $db->getResultArray();
    
    $dis = '';
    
    if ($id > 0) {
        $dis = "disabled";
    }
    
    $sum_count = 0;
    $check     = '';
    foreach ($req[result] AS $row){
        if ($row[count] > 0) {
            $check = "checked";
            $dis = "";
        }else{
            $check = "";
            $dis = "disabled";
        }
        $data .= '<tr>
                        <td style="width: 20px;">
                            <input class="monitoring_action_checkbox"  type="checkbox" '.$check.' value="'.$row[id].'" '.$dis.'>
                        </td>
                        <td>
                            <label style="font-family: BPG; font-weight: bold;">'.$row[name].'</label>
                        </td>
                        <td>
                            <label id="monitoring_action_checkbox'.$row[id].'" style="font-family: BPG; font-weight: bold;">'.$row[count].'</label>
                        </td>
                  </tr>';
        
        $sum_count += $row[count];
    }
    
    $data1  = ' <tr style="height:5px;"></tr>
                <tr>
                    <td colspan="2" style="width: 125px;">
                        <a style="width: 100%;">ქმედების მიხედვით</a>
                    </td>
                    <td style="width: 30px;">
                        <label style="width: 100%; margin-top: 7px;">'.$sum_count.'</label>
                    </td>
                </tr>';
    
    $data = $data1.$data;
    
    return $data;
}

function Getincomming($incom_id){
    global $db;
    $db->setQuery("SELECT   incomming_call.id AS id,
                            incomming_call.phone AS `phone`,
                            monitoring.id AS `monit_id`,
                            monitoring.comment,
                            IFNULL(FROM_UNIXTIME(monitoring.rate_date), NOW()) AS `rate_date`,
                            monitoring.monitoring_status_id,
                            monitoring.action_type_id,
                            CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format) AS `file_name`,
                            user_info.`name` AS op_name,
                            user_info.user_id AS op_id,
						    monit_user.`name` AS `monit_name`,
    						incomming_call.fb_chat_id,
							incomming_call.fb_comment_id,
    						incomming_call.mail_chat_id,
                            incomming_call.viber_chat_id,
                            incomming_call.chat_id,
                            crm.crm_phone,
                            crm.datetime
                 FROM 	    incomming_call
                 JOIN       monitoring ON monitoring.incomming_call_id = incomming_call.id
                 JOIN       user_info ON user_info.user_id = monitoring.operator_id
                 JOIN       user_info AS `monit_user` ON monit_user.user_id = monitoring.monitoring_user_id
                 LEFT JOIN  asterisk_call_log ON asterisk_call_log.id = monitoring.asterisk_incomming_id
				 LEFT JOIN  asterisk_call_record ON asterisk_call_log.id = asterisk_call_record.asterisk_call_log_id
                 LEFT JOIN  crm ON crm.incomming_request_id = incomming_call.id
                 WHERE      monitoring.id = '$incom_id'");
    
    $res = $db->getResultArray();
    
    return $res[result][0];
}

function GetPage($res='', $tab_id){
    global $db;
    $user_id = $_SESSION[USERID];
    $display_gasachivreba = '';
    if ($tab_id <= 2) {
        $display_gasachivreba = "display:none;";
    }
    $monit_user_name = $res[monit_name];
    
    $call_file_name  = $res[file_name];
    
    $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
    $user_info = $db->getResultArray();
    
    if ($user_info[result][0][group_id] == 34) {
        $monit_name = '';
    }else{
        $monit_name = $monit_user_name;
    }
    
    if($res['fb_comment_id']){$chat_source ='fbc';}
    
    $sms = '';
    
    $chat_disable = '';
    $chat_select = '';
    $border = 'border-bottom: 2px solid #333 !important;';
    
    if($chat_source == "fbc"){
        $db->setQuery("	SELECT	message, time
						FROM	fb_feeds
						WHERE	id = (SELECT feeds_id FROM fb_comments WHERE id = '".$mychat_id."')");
        
        $post_arr = $db->getResultArray();
        
        $post = '<div class="chat_fb_comment_post"><div class="chat_fb_coment_post_title"><span style="font-size: 11px;color: #878787;text-align: right;">'.$post_arr[result][0][time].'</span></div><div class="chat_fb_comment_post_inner"> '.$post_arr[result][0][message].'</div></div>';
        
    }else{
        $post = '';
    }
    
    
    
    if($res[chat_id] > 0){
        $db->setQuery(" SELECT  chat.`name`,
                                chat.`device`,
                                chat.`browser`,
                                chat.`ip`,
                                chat_details.message_client,
                                chat_details.message_operator,
                                chat_details.message_datetime,
                                IF(chat_details.operator_user_id > 0,'ოპერატორი',chat.name)AS `op_name`,
                                chat_details.id,
                                chat.seen,
                                chat.status
                        FROM `chat`
                        LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
                        
                        WHERE `chat`.id = $res[chat_id]
                        ORDER BY chat_details.message_datetime");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        
        $db->setQuery("SELECT  `chat_details`.`id`
            FROM    `chat_details`
            WHERE   `chat_details`.`chat_id` = '$res[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]'
            ORDER BY chat_details.id DESC LIMIT 1");
        
        $req            = $db->getResultArray();
        $ge             = $req[result][0];
        $chat_detail_id = $ge[id];
        
        foreach($res1[result] AS $req){
            if (strpos($req[message_client], 'შემოუერთდა ჩატს.') === FALSE) {
                $name = $req[op_name];
            }else{
                $name = '';
            }
            if($req[message_client]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[op_name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">'.$req[message_operator].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                            <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">'.$name.'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">'.$req[message_client].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
        }
        
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border = 'display:none;';
        }
        
        $db->setQuery(" SELECT	IF(who_rejected = 1, 'ოპერატორმა გათიშა', 'აბონენტმა გათიშა') AS rejecter
            FROM   `chat`
            WHERE  `answer_user_id` != 0 AND `status` > 2
            AND     id               = '$res[chat_id]'");
        
        $rejecter_res = $db->getResultArray();
        $rejecter     = $rejecter_res[result][0]['rejecter'];
        
        $sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$rejecter.'</td></tr>';
    }
    
    if($res[fb_chat_id] > 0){
        $db->setQuery("SELECT     fb_chat.last_user_id,
                                    fb_messages.datetime,
                                    fb_chat.sender_name AS `name`,
                                    fb_messages.text,
                                    (SELECT url from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media,
                                    fb_messages.id,
                                    IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_messages.user_id),'') as operator,
                                    (SELECT type from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media_type
                        FROM      `fb_chat`
                        LEFT JOIN `fb_messages` ON fb_chat.id = fb_messages.fb_chat_id# AND chat_details.message_operator != 'daixura'
                        WHERE     `fb_chat`.id = $res[fb_chat_id]
                        ORDER BY   fb_messages.datetime");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        
        foreach($res1[result] AS $req){
            if ($req[media_type] == 'fallback') {
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'file'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'video'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }else{
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
            }
            
            if($req[operator]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">'.$req[name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">'.$req[text].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[operator].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">'.$req[text].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
            $chat_detail_id=$req['id'];
        }
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border       = 'display:none;';
        }
    }
    
    
    if($res[fb_comment_id] > 0){
        $db->setQuery("SELECT     fb_comments.last_user_id,
                                    fb_comments.time,
                                    fb_comments.fb_user_name AS `name`,
                                    fb_comments.message,
                                    (SELECT url from fb_attachments WHERE messages_id=fb_comments.id LIMIT 1) as media,
                                    fb_comments.id,
                                    IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_comments.user_id),'') as operator,
                                    (SELECT type from fb_attachments WHERE messages_id=fb_comments.id LIMIT 1) as media_type
                        FROM      `fb_comments`
                        WHERE     `fb_comments`.id = $res[fb_comment_id]
                        ORDER BY   fb_comments.time");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        
        foreach($res1[result] AS $req){
            if ($req[media_type] == 'fallback') {
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'file'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'video'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }else{
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
            }
            
            if($req[operator]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">'.$req[name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">'.$req[text].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[operator].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">'.$req[text].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
            $chat_detail_id=$req['id'];
        }
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border       = 'display:none;';
        }
    }
    
    
    if($res[mail_chat_id] > 0){
        
        $db->setQuery(" SELECT 		mail.user_id,
                                    mail_detail.datetime,
                                    IF(mail.sender_name = '', mail.sender_address, mail.sender_name) AS `name`,
                                    mail_detail.body,
                                    mail_detail.id,
                                    IFNULL((SELECT `name` from user_info WHERE user_info.user_id = mail_detail.user_id),'') as operator,
                                    mail.subject
                                    
                        FROM      `mail`
                        LEFT JOIN `mail_detail` ON mail.id = mail_detail.mail_id
                        WHERE     `mail`.id = '$res[mail_chat_id]'
                        ORDER BY   mail_detail.datetime");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        foreach($res1[result] AS $req){
            
            $db->setQuery("SELECT `name` FROM `mail_attachment` WHERE mail_detail_id='$req[id]'");
            $fq          = $db->getResultArray();
            $req['text'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req['text']);
            
            foreach($fq[result] AS $file){
                if ($file[check_mesage] == 1) {
                    $req['text']      = $req[text]. "<a href='$file[name]' target='_blank'>$file[name]</a><br>";
                }else{
                    $req['text'] = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[name].'"',  $req['text']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
                    $req['text'] = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $req['text']);
                    $file[patch]    = str_replace('/var/www/html/','',$file[name]);
                    $req['text']     = $req['text']. "<a href='https://crm.my.ge/$file[name]'>$file[name]</a><br>";
                }
            }
            
            $subject = '';
            if($req[subject] != ''){
                $subject = 'თემა: '.$req[subject];
            }
            
            if($req[operator]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold; font-size: 12px;">'.$req[name].'</p>
                                    <p style="font-weight: bold; font-size: 12px;">'.$subject.'</p>
                                    <p style="padding-bottom: 7px; line-height: 20px;font-size: 12px;">'.$req[text].'</p>
                                    <div style="padding-top: 7px;">
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[operator].'</p>
                                    <p style="padding-top: 7px; line-height: 20x; font-size: 12px;">'.$req[text].'</p>
                                    <div style="text-align: right; padding-top: 7px;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
            $chat_detail_id=$req['id'];
        }
        
        
        $db->setQuery("	SELECT     mail.sender_address AS `name`
            FROM      `mail`
            LEFT JOIN `mail_detail` ON mail.id = mail_detail.mail_id
            WHERE     `mail`.id = '$res[mail_chat_id]'");
        
        $mail_info = $db->getResultArray();
        
        if ($res[client_mail] == '') {
            $mail_name = $mail_info[result][0][name];
        }else{
            $mail_name = $res[client_mail];
        }
        
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border = 'display:none;';
        }
    }
    
    $displayseen = "none";
    $displayleave = "none";

    if($req["seen"] == 1) {
        $displayseen = "block";
    }
    if($req["status"] == 12) {
        $displayleave = "block";
    }
    
    $data  .= ' <div id="dialog-form">
                    <div style="display: flex; flex-direction: row;">
                        <div class="communication_chat_style" id="gare_div">
                	        <fieldset class="chat_top">
                	           <table id="blocklogo" >
                                    <tr>
                                        <td>
            								<img id="chat_live" src="media/images/icons/lemons_chat.png" alt="ლაივი" title="მიმდინარე ჩატი" width="20px" style="filter: brightness(0.1);  border-bottom: 2px solid #333 !important;">
            							</td>
                                    </tr>
                                </table>
                	        </fieldset>
                            <fieldset class="communication_chat">
                               <!-- <span check_zoom="0" id="zomm_div" style="float: right; cursor: pointer;">>></span> -->
                                <div style="float:left;height:650px;" id="chatcontent" class="l_chatcontent" >
                                    <div class="chat_scroll" id="chat_scroll" style="height:650px;">
            						   <table style="width: 100%;" id="log">
            						   '.$post.'
                                        '.$sms.'
            						   </table>
            						   <div style="display:flex;flex-direction: column;">
            						   		<span id="span_seen" style="display:' . $displayseen . ';text-align: end; float:right; margin-right:5px;">&#10004; seen</span>
            						   		<span id="chat_close_text" style="display:' . $displayleave . '; text-align: end; float:right; margin-right:5px;">მომხმარებელმა დატოვა ჩატი</span>
            							</div>
            						</div>
            					 </div>
                            </fieldset>
            			</div>
                        <div class="communication_fieldset_style">

                            <!-- navigation -->

                             <div id="side_menu" class="communication_side_menu" style="width:100%;">
                                <span style="float: left;margin-right:1%;" class="info communication_side_menu_button" onclick="show_right_side(\'info\')"><img class="communication_side_menu_img"  style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/lemons_info.png" alt="24 ICON" title="ინფო"></span>

                                <span style="float: left;margin-right:1%;" class="sms communication_side_menu_button" onclick="show_right_side(\'sms\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_sms.png" alt="24 ICON" title="ესემეს"></span>
                                    
                                <span style="float: left;margin-right:1%;" class="incomming_mail communication_side_menu_button" onclick="show_right_side(\'incomming_mail\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_mail.png" alt="24 ICON" title="ელ. ფოსტა"></span>
                
                                <span style="float: left;margin-right:1%;" class="chat_question communication_side_menu_button" onclick="show_right_side(\'chat_question\')"><img class="communication_side_menu_img" src="media/images/icons/chat_question.png" alt="24 ICON" title="კომენტარები"></span>   

                                <span class="record communication_side_menu_button" onclick="show_right_side(\'record\')"><img class="communication_side_menu_img" src="media/images/icons/inc_record.png" alt="24 ICON"  title="ჩანაწერები"></span>
                                
                            </div>

                            <!-- here goes sections -->
                            <div class="communication_right_side" id="right_side" style="float:left;">
                                <fr style="display:block;width:100%; height: 756px; overflow-y: scroll;" id="info">';
                                    $tt = new automator();
                                    $data .= $tt->getDialog($res[id],$res['phone'],'incomming_request_processing','incomming_request_id',1);
                                    $data.='
                                </fr>  

                                <fr style="display:none;width:100%; height: 756px; overflow-y: scroll;" id="sms">
                                    <div id="task_button_area" style="margin-left: 7px;">
                                        <button id="add_sms_phone" disabled class="jquery-ui-button new-sms-button" data-type="phone"  style="display:none">ტელეფონის ნომრით</button>
                                    </div>
                                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                                        <table class="display" id="table_sms" >
                                            <thead>
                                                <tr id="datatable_header">
                                                    <th>ID</th>
                                                    <th style="width: 20%;">თარიღი</th>
                                                    <th style="width: 15%;">ადრესატი</th>
                                                    <th style="width: 50%;">ტექსტი</th>
                                                    <th style="width: 15%;">სტატუსი</th>
                                                </tr>
                                            </thead>
                                            <thead>
                                                <tr class="search_header">
                                                    <th class="colum_hidden">
                                                    <input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </fr>  

                                <fr style="display:none;width:100%; height: 756px; overflow-y: scroll;" id="chat_question">
                                    <div style="margin-left: 7px;"><p  style="display:none">ახალი კომენატრი</p></div>
                                    <table  style="display:none">
                                        <tr>
                                            <td><textarea disabled  id="add_ask" style="border: 1px solid #dbdbdb; resize:none; margin-left: 7px; width:510px; height:50px; display:inline-block;" rows="3"></textarea > </td>
                                            <td><button  disabled style="height: 50px; margin-left: 9px;" id="add_comment">დამატება</button></td>
                                        </tr>
                                    </table>

                                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; border: 1px solid #dbdbdb;">'.get_comment($res['id']).'</div>
                                    <input type=hidden id="comment_incomming_id" value="'.$res['id'].'"/>
                                </fr>  

                                <fr style="display:none;width:100%; height: 756px; overflow-y: scroll;" id="incomming_mail">
                                    <div style="margin-left: 7px;" id="task_button_area" style="display:none">
                                        <button id="add_mail" disabled  style="display:none">ახალი E-mail</button>
                                    </div>
                                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                                        <table class="display" id="table_mail" >
                                            <thead>
                                                <tr id="datatable_header">
                                                    <th>ID</th>
                                                    <th style="width: 20%;">თარიღი</th>
                                                    <th style="width: 25%;">ადრესატი</th>
                                                    <th style="width: 35%;">გზავნილი</th>
                                                    <th style="width: 20%;">სტატუსი</th>
                                                </tr>
                                            </thead>
                                            <thead>
                                                <tr class="search_header">
                                                    <th class="colum_hidden">
                                                        <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                                    </th>
                                                    <th>
                                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </fr>

                                <ff style="display:none; height: 656px;" id="record" >
                                    <div style="margin-top: 10px;">
                                        <audio controls style="margin-left: 7px; width: 97%" id="auau">
                                        <source src="" type="audio/wav">
                                        Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                    <fieldset>
                                        <legend>გამავალი</legend>
                                        <table style="margin: auto; width: 97%;">
                                    <tr>
                                        <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">თარიღი</td>
                                        <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ხანგძლივობა</td>
                                        <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ოპერატორი</td>
                                        <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">მოსმენა</td>
                                        </tr>
                                        ' . GetOutGoingRecordingsSection($res['crm_phone'],$res['crm_date']) . '
                                    </table>
                                    </fieldset>
                                    
                                </ff>
                            </div>
                            <!-- here ends sections -->

                        </div>
                    </div>
                    <div style="width: 630px; position: absolute; right: 0px; top: 15px; ">
                        <fieldset style="width: 615px; height:680px;overflow:scroll; background: #fff;">
                            <table style="width: 100%;" class="dialog-form-table">
                				<tr style="height: 0px;">
                					<td style="width: 200px;"><label for="d_number">შემფასებელი</label></td>
                					<td style="width: 200px;"><label for="d_number">შეფასების თარიღი</label></td>
                					<td style="width: 200px;"><label for="d_number">ოპერატორი</label></td>
                                    <td rowspan="2" style="width: 200px;"><button style="height: 27px; margin-left: 10px; margin-top: 20px;" src="'.$call_file_name.'" id="play_audio">მოსმენა</button></td>
                				</tr>
                	    		<tr>
                					<td>
                                        <input type="text" id="shemfasebeli" class="idle" value="'.$monit_name.'" disabled="disabled"/>
                                    </td>
                					<td>
                                        <input type="text" id="shefasebis_tarigi" class="idle" value="'.$res['rate_date'].'" disabled="disabled"/>
                                    </td>
                                    <td>
                                        <input type="text" id="shesafasebeli_operatori" class="idle" value="'.$res['op_name'].'" disabled="disabled"/>
                                    </td>
                                    <td>
                                        <input type="text" hidden="hidden" id="dialog_operator_id" class="idle" value="'.$res['op_id'].'" disabled="disabled"/>
                                    </td>
                				</tr>
                           </table>
                           '.Getquestion($res, $display_gasachivreba, $monit_user_name).'
                        </fieldset>
                    </div>
                	<input id="hidden_user" type="text" value="'. $res['user_id'] .'" style="display: none;">
                	<input id="hidden_call_id" type="text" value="'. $res['id'] .'" style="display: none;">
                	<input id="note" type="text" value="'. $res['note'] .'" style="display: none;">
                    <input id="monit_hidde_id" type="text" value="'. $res['monit_id'] .'" style="display: none;">
                </div>';
    
    return $data;
}


function Getquestion($res, $display_gasachivreba, $monit_user_name){
    global $db;
    $user_id    = $_SESSION['USERID'];
    
    $db->setQuery("SELECT `name` 
                   FROM    user_info 
                   WHERE   user_id = '$user_id'");
    $check_user_name = $db->getResultArray();
    
    $disabled = '';
    if ($monit_user_name != $check_user_name[result][0][name] && $res[monitoring_status_id] > 0) {
        
        if ($user_id != 102){
            $disabled = 'disabled="disabled"';
        }
        
    }
    
    $db->setQuery("SELECT    IFNULL(monitoring_question.id, monitoring_rate.question_id) AS `question_id`,
            				  monitoring_question.question AS `question`,
            				  IFNULL(monitoring_rate.question_level,monitoring_question.`level`) AS `monit_level`,
            				  monitoring_question.`level` AS `level`,
            				  monitoring_rate.status_id AS `status_id`,
            				  monitoring_rate.`comment` AS `comment`
                    FROM      monitoring_question
                    LEFT JOIN monitoring_rate ON monitoring_rate.question_id = monitoring_question.id AND monitoring_rate.monitoring_id = '$res[monit_id]'
                    WHERE     monitoring_question.actived = 1
                    ORDER BY  monitoring_question.id ASC");
    
    $ress = $db->getResultArray();
    
    $i = 0;
    $j = 0;
    $sum_leve = 100;
    foreach( $ress[result] AS $res2){
        $j += 1;
        
        
        if ($res2[status_id] == '0') {
            $sum_leve-=$res2[level];
            $data_ansver = '<td><input class="radio" type="radio"  name="'.$res2[question_id].'" value="1" click_count="0" '.$disabled.'/></td>
                            <td><input class="radio" type="radio"  name="'.$res2[question_id].'" value="0" click_count="1" '.$disabled.' checked/></td>';
            
        }elseif ($res2[status_id] == '1'){
            $i+=1;
            $data_ansver = '<td><input class="radio" type="radio"  name="'.$res2[question_id].'" value="1" click_count="1" '.$disabled.' checked/></td>
                            <td><input class="radio" type="radio"  name="'.$res2[question_id].'" value="0" click_count="0" '.$disabled.'/></td>';
        }else{
            $data_ansver = '<td><input class="radio" type="radio"  name="'.$res2[question_id].'" value="1" click_count="1" '.$disabled.' checked/></td>
                            <td><input class="radio" type="radio"  name="'.$res2[question_id].'" value="0" click_count="0" '.$disabled.'/></td>';
        }
        
        if ($res[monitoring_status_id] == '0') {
            $status_moni = '<option value="0" selected="selected">შესამოწმებელი</option>
                            <option value="1">შემოწმებული</option>
                            <option value="2">გასაჩივრებული</option>
                            <option value="3">დასრულებული</option>';
            
        }elseif ($res[monitoring_status_id] == '1'){
            $status_moni = '<option value="0">შესამოწმებელი</option>
                            <option value="1" selected="selected">შემოწმებული</option>
                            <option value="2">გასაჩივრებული</option>
                            <option value="3">დასრულებული</option>';
        }elseif($res[monitoring_status_id] == '2'){
            $status_moni = '<option value="0">შესამოწმებელი</option>
                            <option value="1">შემოწმებული</option>
                            <option value="2" selected="selected">გასაჩივრებული</option>
                            <option value="3">დასრულებული</option>';
        }elseif($res[monitoring_status_id] == '3'){
            $status_moni = '<option value="0">შესამოწმებელი</option>
                            <option value="1">შემოწმებული</option>
                            <option value="2">გასაჩივრებული</option>
                            <option value="3" selected="selected">დასრულებული</option>';
        }else{
            $status_moni = '<option value="0" selected="selected">შესამოწმებელი</option>
                            <option value="1">შემოწმებული</option>
                            <option value="2">გასაჩივრებული</option>
                            <option value="3">დასრულებული</option>';
        }
        
        $data .= '<tr>
                      <td style="vertical-align:middle;">'.$res2[question].'</td>
                	  '.$data_ansver.'
                      <td style="vertical-align: middle;"><label level="'.$res2[level].'" id="question_level_'.$res2[question_id].'">'.$res2[monit_level].'</label></td>
                      <td><input style="width: 246px;" type="text" id="question_comment_'.$res2[question_id].'" class="question_comment_idle" value="'.$res2[comment].'" '.$disabled.'/></td>
                  </tr>
                  <tr style="height:15px;"></tr>';
    }
    
    $percent = round($i/$j*100,2).'%';
    $monitoring_data = '<div style="width: 100%;  height: 750px;">
                            <table style="width: 100%; margin-top:20px;">
                                <tr style="height: 0px;">
                					<td style="width: 220px; font-weight: bold;"><label>შეფასების კრიტერიუმი</label></td>
                					<td style="width: 40px; font-weight: bold;"><label>კი</label></td>
                					<td style="width: 40px; font-weight: bold;"><label>არა</label></td>
                                    <td style="width: 50px; font-weight: bold;"><label>ქულა</label></td>
                                    <td style="width: 250px; font-weight: bold;"><label>კომენტარი</label></td>
                				</tr>
                                <tr style="height:15px;"></tr>
                                '.$data.'
                                
                                
                                <tr style="height: 0px;">
                					<td style="width: 220px; font-weight: bold;"><label></label></td>
                					<td colspan="2" style="width: 40px; font-weight: bold;"><label style="font-family: pvn; font-weight: bold;">შეფასება</label></td>
                					<td style="width: 50px; font-weight: bold;"><label id="monitoring_level_sum" style="font-family: pvn; font-weight: bold;">'.$sum_leve.'</label></td>
                                    <td style="width: 250px; font-weight: bold;"><label style="font-family: pvn; font-weight: bold;">ქულა</label></td>
                				</tr>
                                <tr style="height:40px;"></tr>
                                <tr>
    							     <td colspan="5">კომენტარი</td>
    						    </tr>
                                <tr>
    							     <td colspan="5">
    								    <textarea  style="width: 99%; height: 90px; resize: none;" id="monitoring_coment" class="idle" name="call_content" cols="300" rows="8">' . $res['comment'] . '</textarea>
    							     </td>
    						    </tr>
                                <tr style="height:10px;"></tr>
                                <tr>
                                    <td colspan="5">სტატუსი</td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                         <select style="width: 160px;" id="monitoring_status_id" class="idls object">
                                            '.$status_moni.'
                                         </select>
                                    </td>
                                    <td>
                                         <button id="gasachivreba" style="'./*$display_gasachivreba.*/' background: #FFC000; border: none; color: #fff; height: 25px; width: 120px; float: right;">საჩივარი</button>
                                    </td>
                                </tr>
                            </table>
                        </div>';
    
    return $monitoring_data;
}

function GetOutGoingRecordingsSection($phone,$crm_date)
{
	global $db;
	$db->setQuery(" SELECT SEC_TO_TIME(asterisk_call_log.talk_time) AS `time`,
					CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format) AS `userfield`,
					FROM_UNIXTIME(asterisk_call_log.call_datetime) AS `call_datetime`,
					user_info.`name`
				FROM asterisk_call_log
				LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
				JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
				WHERE asterisk_call_log.destination = '$phone' AND asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_datetime >= '$crm_date' ORDER BY asterisk_call_log.id DESC LIMIT 1");

	$num_row = $db->getNumRow();
	$req     = $db->getResultArray();

	if ($num_row == 0) {
		$data .= '<tr>
				  <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
			  </tr>';
	} else {
		foreach ($req[result] as $res2) {
			$src = $res2['userfield'];
			$data .= '
			  <tr>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[call_datetime] . '</td>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[time] . '</td>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[name] . '</td>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listenOutgoing(\'' . $src . '\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
			  </tr>';
		}
	}

	return $data;
}

function GetRecordingsSection($res){
    if ($res[phone]==''){
        
    }else{
        $req = mysql_query("SELECT  	DATE_FORMAT(asterisk_incomming.call_datetime,'%d/%m/%y %H:%i:%s') AS `time`,
                                        asterisk_incomming.file_name AS `userfield`,
                                        asterisk_incomming.call_datetime
                            FROM 		asterisk_incomming
                            WHERE		asterisk_incomming.id ='$res[inc_uniqueid]'");
    }
    
    if (mysql_num_rows($req) == 0){
        $data .= '<tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=3>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
    }
    
    while( $res2 = mysql_fetch_assoc($req)){
        $src = $res2['userfield'];
        $data .= '
		      <tr>
                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[call_datetime].'</td>
        	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[time].'</td>
        	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$src.'\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
    	      </tr>';
    }
    
    
    
    return $data;
}
function get_user($id){
    global $db;
    $data   ='';
    $db->setQuery("SELECT `name`
                   FROM   `user_info`
                   WHERE   user_id = $id");
    
    $res    = $db->getResultArray();
    $data   = $res[result][0]['name'];
    return $data;
}

function get_answers($id,$hidden_id){
    global $db;
    $data= '';
    $db->setQuery("SELECT `id`,
                          `user_id`,
                          `incomming_call_id`,
                          `comment`,
                          `datetime`
                    FROM  `chat_comment`
                    WHERE  parent_id = $id AND incomming_call_id = $hidden_id AND active = 1");
    
    $req = $db->getResultArray();
    
    foreach($req[result] AS $res){
        $data .='<div>
                        <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                        <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; ">'.$res['comment'].'</span>
                        <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
    }
    
    return $data;
    
}

function get_comment($hidden_id){
    global $db;
    $data = "";
    $db->setQuery("SELECT   `id`,
                            `user_id`,
                            `incomming_call_id`,
                            `comment`,
                            `datetime`
                   FROM     `chat_comment`
                   WHERE     parent_id = 0 AND incomming_call_id = $hidden_id AND active = 1
                   ORDER BY `datetime` DESC");
    
    $req = $db->getResultArray();
    foreach ($req[result] AS $res){
        $data .='<div style="margin-top: 15px; padding:10px;">
                        <input type=hidden id="hidden_comment_id_'.$res['id'].'" value="'.$res['id'].'"/>
						<span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .'
						<img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; display:none;margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer;display:none;" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"> <br><br>
                        <div style="border: 1px #D7DBDD solid; padding: 14px; border-radius:20px; background-color:#D7DBDD; "> '.$res['comment'].'</div>
                        <br/><span id="get_answer" class="'. $res['id'] .'"  style="color: #369; font-size:15px;     font-size: 11px;
                        margin-left: 10px;  ">პასუხის გაცემა</span>
                 </div>
                 <div style="margin-left: 50px; margin-top: 10px;">
                    '.get_answers($res['id'],$hidden_id).'
                 </div>';
    }
    
    
    return $data;
}

function GetRecordingsSection1($res){
    if ($res[phone]==''){
        
    }else{
        $req = mysql_query("SELECT  DATE_FORMAT(asterisk_outgoing.call_datetime,'%d/%m/%y %H:%i:%s') AS `time`,
                                    asterisk_outgoing.file_name AS `userfield`,
                                    asterisk_outgoing.call_datetime
                            FROM 	asterisk_outgoing
                            WHERE 	asterisk_outgoing.uniqueid='$res[task_uniqueid]'");
    }
    
    if (mysql_num_rows($req) == 0){
        $data .= '<tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=3>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
    }
    
    while( $res2 = mysql_fetch_assoc($req)){
        $src = $res2['userfield'];
        $data .= '<tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[call_datetime].'</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2[time].'</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$src.'\')"><span style="color: #ff9800;">მოსმენა</span></td>
        	      </tr>';
    }
    
    
    return $data;
}

?>
