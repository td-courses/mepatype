const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync").create();
const minCss = require("gulp-clean-css");
const babel = require("gulp-babel");
const sourcemaps = require("gulp-sourcemaps");
const concat = require("gulp-concat");
const uglify = require('gulp-uglify');
const connect = require('gulp-connect-php');

// const nodemon = require('gulp-nodemon');

// const jsDevelopmentFolder = "./dist/development/javascript/**/*.js"
// const sassDevelopmentFolder = "./development/scss/*.scss"
const jsDevelopmentFolder = "dist/development_javascript/**/*.js"
const sassDevelopmentFolder = "dist/development_scss/*.scss"

const jsProductionFolder = "dist/js/production"
const cssProductionFolder = "dist/css/production"

//
// ─── ES6 TO ES5 AND UGLIFY ──────────────────────────────────────────────────────
//
function javascript() {
  return gulp
    .src(jsDevelopmentFolder)
    .pipe(sourcemaps.init())
    .pipe(
      babel({
        presets: ["@babel/env"]
      })
    )
    // .pipe(uglify())
    .pipe(concat("main.min.js"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(jsProductionFolder));

}

//
// ─── SCSS TO CSS AND MINIFY ────────────────────────────────────────────────────────────────
// 
//.pipe(minCss())

function style() {
  return gulp
    .src(sassDevelopmentFolder)
    .pipe(sass())
    .pipe(minCss())
    .pipe(gulp.dest(cssProductionFolder))
    .pipe(browserSync.stream());
}


//
// ─── START THE SERVER AND USE THE FUNCTIONS UP TOP ──────────────────────────────
//    
function watch() {
  browserSync.init({
    port: 8080,
    open: false,

    server: {
      proxy: '192.168.11.163:80'
    }
  });

  // files: ["dis/production_css/main.css"],
 

  //, "/production/javascript"
  gulp.watch("dist/development_scss/**/*.scss", style);
  gulp.watch(jsDevelopmentFolder, javascript).on('change', browserSync.reload);
  // gulp.watch("**/*.php").on('change', browserSync.reload);
}




//exports
exports.javascript = javascript;
exports.style = style;
exports.watch = watch;

/*
function styleWithOutMin() {
    return gulp.src("./public/sass/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("./public/css"));
}
*/
