    <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
include('classes/class.Mysqli.php');

global $db;
$db      = new dbClass();
$user_id = $_SESSION['USERID'];
$action  = $_REQUEST['act'];
$data	= array();
switch ($action){

    case 'get_users':
        $searchByName  = $_REQUEST['search'];
        $now = time();
		$db->setQuery("
			
				UPDATE users SET last_actived=UNIX_TIMESTAMP(NOW()) WHERE id='$user_id'
			
		");
		$db->execQuery();
        if(empty($searchByName))
        {
            $db->setQuery("SELECT IF(local_chat.id IS NULL , 0, local_chat.id) as chat_id,
                        user_info.user_id,
                        IF(local_chat.last_message IS NULL ,'',local_chat.last_message) as last_message,
                        IF(local_chat.receiver_user_id='$user_id',local_chat.receiver_see_status,local_chat.sender_see_status) as reed_status,
                        user_info.name,
                        user_info.image
                        FROM   users
                        JOIN   user_info ON user_info.user_id=users.id
                        LEFT   JOIN local_chat ON (user_info.user_id=local_chat.receiver_user_id OR user_info.user_id=local_chat.sender_user_id) AND (local_chat.receiver_user_id='$user_id' OR local_chat.sender_user_id='$user_id')
                        WHERE  users.actived=1 AND users.id!='$user_id'
                        GROUP BY users.id
                        order by local_chat.datetime desc");
        }
        else
        {
            $db->setQuery("SELECT IF(local_chat.id IS NULL , 0, local_chat.id) as chat_id,
                        user_info.user_id,
                        IF(local_chat.last_message IS NULL ,'',local_chat.last_message) as last_message,
                        IF(local_chat.receiver_user_id='$user_id',local_chat.receiver_see_status,local_chat.sender_see_status) as reed_status,
                        user_info.name,
                        user_info.image
                        FROM   users
                        JOIN   user_info ON user_info.user_id=users.id
                        LEFT   JOIN local_chat ON (user_info.user_id=local_chat.receiver_user_id OR user_info.user_id=local_chat.sender_user_id) AND (local_chat.receiver_user_id='$user_id' OR local_chat.sender_user_id='$user_id')
                        WHERE  users.actived=1 AND users.id!='$user_id' AND user_info.name LIKE '%$searchByName%'
                        GROUP BY users.id
                        order by local_chat.datetime desc");
        }



            $query = "
            
            SELECT local_chat.id,
                   local_chat.last_message,
                   user_info.name,
                   user_info.image,
            
            FROM local_chat
            
            
            ";


        $data=$db->getResultArray();
        break;
    case 'get_messages':
        $count=$_REQUEST['count']*30;
        $chat_id=$_REQUEST['chat_id'];
        $db->setQuery("SELECT 
                       local_chat.id,
                       local_chat_details.datetime, 
                       local_chat_details.message as text,
                       IF(local_chat_details.user_id='$user_id','sent_message','recieved_message') as type
                       FROM local_chat
                       JOIN local_chat_details ON local_chat.id=local_chat_details.local_chat_id
                       WHERE local_chat_details.local_chat_id='$chat_id' AND local_chat.actived=1
                       order by local_chat_details.datetime 
                       ");
        $data=$db->getResultArray();
        $db->setQuery("UPDATE local_chat 
                           SET 
                               sender_see_status=IF(sender_user_id='$user_id','read',local_chat.sender_see_status),
                               receiver_see_status=IF(receiver_user_id='$user_id','read',local_chat.receiver_see_status)
                           WHERE id='$chat_id'   
                           ");
        $db->execQuery();

        $db->setQuery("SELECT MAX(id) as max_id FROM local_chat_details WHERE local_chat_details.local_chat_id='$chat_id' AND user_id!='$user_id'");
        $res1=$db->getResultArray();
        $_SESSION['local_chat_details_id']=$res1['result'][0]['max_id'];

        $db->setQuery("SELECT 
                       user_info.name
                       FROM local_chat
                       JOIN user_info ON user_info.user_id=local_chat.sender_user_id OR local_chat.receiver_user_id=user_info.user_id
                       WHERE local_chat.id='$chat_id' AND user_info.user_id!='$user_id'
                       ");
        $res=$db->getResultArray();
        $data['username']=$res['result'][0]['name'];
        break;

    case 'send_message':
        $chat_id            = $_REQUEST['chat_id'];
        $receiver_user_id   = $_REQUEST['user_id'];
        $text               = $_REQUEST['text'];
        $message_type       = $_REQUEST['type'];
        $fileName           = $_REQUEST['filename'];

        IF($text!="" && $text!=null){
           if($message_type == 'file')
           {
               IF($chat_id=='0'){
                   $db->setQuery("INSERT INTO local_chat (receiver_user_id, sender_user_id, datetime, receiver_see_status,sender_see_status, last_message, actived) 
                           VALUES  ('$receiver_user_id','$user_id',NOW(),'unread','read','$fileName','1') ");
                   $db->execQuery();
                   $db->setQuery("SELECT max(id) as id FROM local_chat");
                   $res=$db->getResultArray();
                   $chat_id=$res['result'][0]['id'];
               }else{
                   $db->setQuery("UPDATE local_chat 
                               SET datetime=NOW(),
                                   sender_see_status=IF(sender_user_id='$user_id','read','unread'),
                                   receiver_see_status=IF(receiver_user_id='$user_id','read','unread'),
                                   last_message='$fileName'
                               WHERE id='$chat_id'   
                               ");
                   $db->execQuery();
               }
           }
           else{
               IF($chat_id=='0'){
                   $db->setQuery("INSERT INTO local_chat (receiver_user_id, sender_user_id, datetime, receiver_see_status,sender_see_status, last_message, actived) 
                           VALUES  ('$receiver_user_id','$user_id',NOW(),'unread','read','$text','1') ");
                   $db->execQuery();
                   $db->setQuery("SELECT max(id) as id FROM local_chat");
                   $res=$db->getResultArray();
                   $chat_id=$res['result'][0]['id'];
               }else{
                   $db->setQuery("UPDATE local_chat 
                               SET datetime=NOW(),
                                   sender_see_status=IF(sender_user_id='$user_id','read','unread'),
                                   receiver_see_status=IF(receiver_user_id='$user_id','read','unread'),
                                   last_message='$text'
                               WHERE id='$chat_id'   
                               ");
                   $db->execQuery();
               }
           }


            $db->setQuery("INSERT INTO local_chat_details (local_chat_id, user_id, datetime, message, actived) 
                                               VALUES ('$chat_id','$user_id',NOW(),'$text',1)"  );
            $db->execQuery();
        }

        $data['chat_id']= $chat_id;

        break;
    case 'check_messages':
        $chat_id=$_REQUEST['chat_id'];
        $receiver_user_id=$_REQUEST['user_id'];

        $db->setQuery("SELECT IF(local_chat.sender_user_id='$user_id',local_chat.sender_see_status,local_chat.receiver_see_status) as reed_status,
                              IF(local_chat.sender_user_id='$user_id','sender','receiver') as status
                       FROM local_chat WHERE id='$chat_id'");
        $res=$db->getResultArray();
        IF($res['result'][0]['reed_status']=='unread'){
            $max_id=$_SESSION['local_chat_details_id'];

            $db->setQuery("SELECT 
                       local_chat.id,
                       local_chat_details.datetime, 
                       local_chat_details.message as text,
                       IF(local_chat_details.user_id='$user_id','sent_message','recieved_message') as type
                       FROM local_chat
                       JOIN local_chat_details ON local_chat.id=local_chat_details.local_chat_id
                       WHERE local_chat_details.local_chat_id='$chat_id' AND local_chat.actived=1 AND local_chat_details.user_id = '$receiver_user_id' AND local_chat_details.id>'$max_id'
                       order by local_chat_details.datetime 
                       ");
            $data=$db->getResultArray();


            if($res['result'][0]['status']=='sender'){
                $db->setQuery("UPDATE local_chat 
                           SET sender_see_status='read'
                           WHERE id='$chat_id'   
                           ");
                $db->execQuery();
            }else{
                $db->setQuery("UPDATE local_chat 
                           SET  receiver_see_status='read'
                           WHERE id='$chat_id'   
                           ");
                $db->execQuery();
            }

            $db->setQuery("SELECT MAX(id) as max_id FROM local_chat_details WHERE local_chat_details.local_chat_id='$chat_id' AND user_id='$receiver_user_id' ");
            $res1=$db->getResultArray();
            $_SESSION['local_chat_details_id']=$res1['result'][0]['max_id'];

        }

        break;

    default:
        $data='';
}

echo json_encode($data);


?>