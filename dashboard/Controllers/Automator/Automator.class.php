<?php

use Routers\dbClass;

class Automator extends dbClass{

    private $colCount;
    private $cols;

    public function getLevel(){
        $parentID = $_REQUEST['id'];
        $inputKey = $_REQUEST['inputKey'];

        $tempSelectorName = explode("--", $inputKey);
        $selectorID = $tempSelectorName[1];

        parent::setQuery("  SELECT  table_name
                            FROM    processing_setting_detail
                            WHERE   id = '$selectorID'");
        $selectorTableName = parent::getResultArray();
        $selectorTableName = $selectorTableName['result'][0]['table_name'];

        parent::setQuery("  SELECT  id,name
                            FROM    `$selectorTableName`
                            WHERE   actived = 1 AND parent_id = '$parentID'");
        $level_data = parent::getResultArray();

        if($level_data['count'] == 0){
            $level_data['result'][0] = array("id" => 0, "name" => "არ აქვს კატეგორია");
        }

        return $level_data['result'];
        
    }

    public function getFieldsetList(){
        $this->colCount = $_REQUEST['count'];
        $this->cols     = $_REQUEST['cols'];
        $pageID         = $_REQUEST['pageId'];
        parent::setQuery("  SELECT	processing_fieldset.id,
                                    processing_fieldset.name,
                                    GROUP_CONCAT(processing_setting_detail.name SEPARATOR ', ') AS inputs

                            FROM    processing_fieldset_by_page
                            JOIN	processing_fieldset ON processing_fieldset.id = processing_fieldset_by_page.fieldset_id
                            JOIN	processing_setting_detail ON processing_setting_detail.processing_fieldset_id = processing_fieldset.id
                            WHERE 	processing_fieldset_by_page.actived = 1 AND processing_fieldset_by_page.processing_page_id = '$pageID' AND processing_fieldset.actived = 1

                            GROUP BY processing_fieldset.id");
        $callList = parent::getKendoList($this->colCount, $this->cols);

        return $callList;
    }

    public function getInputList(){
        $this->colCount = $_REQUEST['count'];
        $this->cols     = $_REQUEST['cols'];
        $fieldsetID     = $_REQUEST['fieldsetID'];
        parent::setQuery("  SELECT      processing_setting_detail.id,
                                        processing_setting_detail.name,
                                        processing_field_type.name,
                                        processing_input_tabs.name,
                                        processing_setting_detail.position


                            FROM        processing_setting_detail
                            LEFT JOIN   processing_field_type ON processing_field_type.id = processing_setting_detail.processing_field_type_id
                            LEFT JOIN   processing_input_tabs ON processing_input_tabs.id = processing_setting_detail.tab_id
                            WHERE       processing_setting_detail.actived = '1'  AND processing_setting_detail.processing_fieldset_id = '$fieldsetID'");
        $callList = parent::getKendoList($this->colCount, $this->cols);

        return $callList;
    }

    public function getPageList(){
        $this->colCount = $_REQUEST['count'];
        $this->cols     = $_REQUEST['cols'];
        parent::setQuery("  SELECT 	processing_page.id,
                                    processing_page.datetime,
                                    processing_page.`name`,
                                    GROUP_CONCAT(processing_fieldset.name SEPARATOR ', ') AS fieldsets
                                    
                            FROM 	processing_page
                            LEFT JOIN	processing_fieldset_by_page ON processing_fieldset_by_page.processing_page_id = processing_page.id
                            LEFT JOIN	processing_fieldset ON processing_fieldset.id = processing_fieldset_by_page.fieldset_id
                            WHERE 	processing_page.actived = 1
                            GROUP BY processing_page.id");
        $callList = parent::getKendoList($this->colCount, $this->cols);

        return $callList;
    }


    public function getfieldsetName(){
        $fieldsetID     = $_REQUEST['fieldsetID'];
        parent::setQuery("  SELECT	processing_fieldset.name AS 'fieldsetName',
                                    'Fieldset დასახელება3454' AS 'title'

                            FROM 	processing_fieldset
                            WHERE 	processing_fieldset.id = '$fieldsetID'");
        $filedsetName = parent::getResultArray();

        return $filedsetName['result'];
    }

    public function getGeneratedInputs(){
        $pageKey    = $_REQUEST['pageKey'];
        $processing = $_REQUEST['id'];
        $inputJson  = array();
        parent::setQuery("  SELECT	processing_fieldset.id,
                                    processing_fieldset.name

                            FROM	processing_page
                            JOIN	processing_fieldset_by_page ON processing_fieldset_by_page.processing_page_id = processing_page.id
                            JOIN	processing_fieldset ON processing_fieldset.id = processing_fieldset_by_page.fieldset_id
                            WHERE 	processing_page.`key` = '$pageKey' AND processing_fieldset.actived = 1
                            ORDER BY processing_fieldset.position");
        $fieldsets = parent::getResultArray();


        $fieldsetIterator = 0;
        foreach($fieldsets['result'] AS $field){
            $fieldsetData = array("fieldsetName" => $field['name'], "tabs" => array());
            array_push($inputJson, $fieldsetData);
            parent::setQuery("  SELECT 	processing_input_tabs.id,
                                        processing_input_tabs.`name`
                                FROM 	processing_input_tabs
                                WHERE 	processing_input_tabs.field_id = '$field[id]' AND processing_input_tabs.actived = 1");
            $tabs = parent::getResultArray();
            if($tabs['count'] > 0){
                $tabIterator = 0;
                foreach($tabs['result'] AS $tab){
                    $tabData = array("tabName" => $tab['name'], "inputs" => array());
                    array_push($inputJson[$fieldsetIterator]['tabs'], $tabData);
                    parent::setQuery("  SELECT 	processing_setting_detail.id,
                                                CONCAT(processing_setting_detail.`key`,'--',processing_setting_detail.id,'--',processing_field_type.id) AS `key`,
                                                processing_setting_detail.name,
                                                processing_setting_detail.table_name,
                                                processing_field_type.`key` AS type,
                                                processing_setting_detail.input_size,
                                                processing_setting_detail.multilevel_1,
                                                processing_setting_detail.multilevel_2,
                                                processing_setting_detail.multilevel_3

                                        FROM 	processing_setting_detail
                                        JOIN	processing_field_type ON processing_field_type.id = processing_setting_detail.processing_field_type_id
                                        WHERE 	processing_setting_detail.actived = 1 AND processing_setting_detail.tab_id = '$tab[id]' AND processing_field_type.actived = 1
                                        ORDER BY processing_setting_detail.position");

                    $inputs = parent::getResultArray();
                    $inputIterator = 0;
                    if (is_array($inputs['result']) || is_object($inputs['result']))
                    {
                        foreach($inputs['result'] AS $input){
                            $key_cutted = explode('--', $input['key']);

                            $inputData = array("input_id" => (int)$input['id'], "input_key" => $input['key'], "input_name" => $input['name'], "input_type" => $input['type'], "input_size" => (int)$input['input_size']);
                            array_push($inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'], $inputData);

                            if($input['type'] == 'radio' || $input['type'] == 'checkbox' || $input['type'] == 'multiselect' || $input['type'] == 'select'){

                                parent::setQuery("  SELECT  value
                                                    FROM    processing
                                                    WHERE   row_id = '$processing'");
                                $jsonData = parent::getResultArray();
                                $jsonData = json_decode($jsonData['result'][0]['value'],true);


                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['input_value'] = empty($jsonData['selectors'][$key_cutted[0]]) ? '' : (int)$jsonData['selectors'][$key_cutted[0]];
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['input_parameters'] = array();
                                if(empty($input['table_name'])) $input['table_name'] = 'mail_type';

                                parent::setQuery("  SELECT  id, name
                                                    FROM    `$input[table_name]`
                                                    WHERE   actived = 1");
                                $parameters = parent::getResultArray();
                                foreach($parameters['result'] AS $param){
                                    $DIR_Params = array("id" => $param['id'], "name" => $param['name']);
                                    array_push($inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['input_parameters'], $DIR_Params);
                                }
                            }
                            else if($input['type'] == 'multilevelselect'){

                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_1_name'] = $input['multilevel_1'];
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_2_name'] = $input['multilevel_2'];
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_3_name'] = $input['multilevel_3'];

                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_1_value'] = '';
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_2_value'] = '';
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_3_value'] = '';

                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_1_parameters'] = array();
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_2_parameters'] = array();
                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_3_parameters'] = array();
                                
                                if(empty($input['table_name'])) $input['table_name'] = 'mail_type';
                                parent::setQuery("  SELECT  id, name
                                                    FROM    `$input[table_name]`
                                                    WHERE   actived = 1 AND parent_id = 0");
                                $parameters_level_1 = parent::getResultArray();
                                foreach($parameters_level_1['result'] AS $param){
                                    $multilevel_Params = array("id" => $param['id'], "name" => $param['name']);
                                    array_push($inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_1_parameters'], $multilevel_Params);
                                }



                                parent::setQuery("  SELECT id,name
                                                    FROM `$input[table_name]`
                                                    WHERE actived = 1 AND parent_id IN (SELECT	id
                                                                                        FROM `$input[table_name]`
                                                                                        WHERE actived = 1 AND parent_id IN (SELECT id FROM `$input[table_name]` WHERE actived = 1 AND parent_id = 0))");
                                $parameters_level_3 = parent::getResultArray();

                                foreach($parameters_level_3['result'] AS $param){
                                    $multilevel_Params = array("id" => $param['id'], "name" => $param['name']);
                                    array_push($inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['level_3_parameters'], $multilevel_Params);
                                }
    
                            }
                            else{
                                parent::setQuery("  SELECT  value
                                                    FROM    processing
                                                    WHERE   row_id = '$processing'");
                                $jsonData = parent::getResultArray();
                                $jsonData = json_decode($jsonData['result'][0]['value'],true);

                                $inputJson[$fieldsetIterator]['tabs'][$tabIterator]['inputs'][$inputIterator]['input_value'] = empty($jsonData['inputs'][$key_cutted[0]]) ? '' : $jsonData['inputs'][$key_cutted[0]];
                            }
                            $inputIterator++;
                        }
                    }

                    $tabIterator++;
                }
            }
            else{
                parent::setQuery("  SELECT 	processing_setting_detail.id,
                                            CONCAT(processing_setting_detail.`key`,'--',processing_setting_detail.id,'--',processing_field_type.id) AS `key`,
                                            processing_setting_detail.name,
                                            processing_setting_detail.table_name,
                                            processing_field_type.`key` AS type,
                                            processing_setting_detail.input_size,
                                            processing_setting_detail.multilevel_1,
                                            processing_setting_detail.multilevel_2,
                                            processing_setting_detail.multilevel_3

                                    FROM 	processing_setting_detail
                                    JOIN	processing_field_type ON processing_field_type.id = processing_setting_detail.processing_field_type_id
                                    WHERE 	processing_setting_detail.actived = 1 AND processing_setting_detail.processing_fieldset_id = '$field[id]' AND processing_field_type.actived = 1
                                    ORDER BY processing_setting_detail.position");

                $inputs = parent::getResultArray();
                $inputJson[$fieldsetIterator]['inputs'] = array();
                $inputIterator = 0;
                if (is_array($inputs['result']) || is_object($inputs['result']))
                {
                    foreach($inputs['result'] AS $input){
                        $key_cutted = explode('--', $input['key']);


                        $inputData = array("input_id" => (int)$input['id'], "input_key" => $input['key'], "input_name" => $input['name'], "input_type" => $input['type'], "input_size" => (int)$input['input_size']);
                        array_push($inputJson[$fieldsetIterator]['inputs'], $inputData);

                        if($input['type'] == 'radio' || $input['type'] == 'checkbox' || $input['type'] == 'multiselect' || $input['type'] == 'select'){
                            parent::setQuery("  SELECT  value
                                                FROM    processing
                                                WHERE   row_id = '$processing'");
                            $jsonData = parent::getResultArray();
                            $jsonData = json_decode($jsonData['result'][0]['value'],true);


                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['input_value'] = empty($jsonData['selectors'][$key_cutted[0]]) ? '' : (int)$jsonData['selectors'][$key_cutted[0]];
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['input_parameters'] = array();
                            if(empty($input['table_name'])) $input['table_name'] = 'mail_type';
                            parent::setQuery("  SELECT  id, name
                                                FROM    `$input[table_name]`
                                                WHERE   actived = 1");
                            $parameters = parent::getResultArray();
                            foreach($parameters['result'] AS $param){
                                $DIR_Params = array("id" => $param['id'], "name" => $param['name']);
                                array_push($inputJson[$fieldsetIterator]['inputs'][$inputIterator]['input_parameters'], $DIR_Params);
                            }
                        }
                        else if($input['type'] == 'multilevelselect'){
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_1_name'] = $input['multilevel_1'];
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_2_name'] = $input['multilevel_2'];
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_3_name'] = $input['multilevel_3'];


                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_1_value'] = '';
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_2_value'] = '';
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_3_value'] = '';



                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_1_parameters'] = array();
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_2_parameters'] = array();
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_3_parameters'] = array();
                            if(empty($input['table_name'])) $input['table_name'] = 'mail_type';
                            parent::setQuery("  SELECT  id, name
                                                FROM    `$input[table_name]`
                                                WHERE   actived = 1 AND parent_id = 0");
                            $parameters_level_1 = parent::getResultArray();
                            foreach($parameters_level_1['result'] AS $param){
                                $multilevel_Params = array("id" => $param['id'], "name" => $param['name']);
                                array_push($inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_1_parameters'], $multilevel_Params);
                            }


                            parent::setQuery("  SELECT id,name
                                                FROM `$input[table_name]`
                                                WHERE actived = 1 AND parent_id IN (SELECT	id
                                                                                    FROM `$input[table_name]`
                                                                                    WHERE actived = 1 AND parent_id IN (SELECT id FROM `$input[table_name]` WHERE actived = 1 AND parent_id = 0))");
                            $parameters_level_3 = parent::getResultArray();

                            foreach($parameters_level_3['result'] AS $param){
                                $multilevel_Params = array("id" => $param['id'], "name" => $param['name']);
                                array_push($inputJson[$fieldsetIterator]['inputs'][$inputIterator]['level_3_parameters'], $multilevel_Params);
                            }
                        }
                        else{
                            parent::setQuery("  SELECT  value
                                                FROM    processing
                                                WHERE   row_id = '$processing'");
                            $jsonData = parent::getResultArray();
                            $jsonData = json_decode($jsonData['result'][0]['value'],true);
                            $inputJson[$fieldsetIterator]['inputs'][$inputIterator]['input_value'] = empty($jsonData['inputs'][$key_cutted[0]]) ? '' : $jsonData['inputs'][$key_cutted[0]];
                        }

                        $inputIterator++;
                    }
                }

            }
            
            
            $fieldsetIterator++;
        }

        return $inputJson;

    }

    public function saveModalData(){

        $pageKey    = $_REQUEST['pageKey'];
        $rowID      = $_REQUEST['id'];

        $savingDataArray = array();

        $savingDataArray['inputs'] = array();
        $savingDataArray['selectors'] = array();
        $savingDataArray['multilevel'] = array();

        foreach($_REQUEST AS $key => $value){

            if($key == 'route' OR $key == 'act' OR $key == 'pageKey' OR $key == 'id'){
                continue;
            }
            else{
                $tempKey = explode('--', $key); //Exploding key for input parameters
                $inputID = $tempKey[1]; //Input ID in DB
                $inputTypeID = $tempKey[2]; //Getting input type id

                switch ($inputTypeID){
                    case 1: //Text input
                        $savingDataArray['inputs'][$tempKey[0]] = $value;
                        break;
                    case 2: //TextArea
                        $savingDataArray['inputs'][$tempKey[0]] = $value;
                        break; 
                    case 4://Date
                    
                        break;
                    case 5://Datetime
                
                        break;
                    case 6://Radio
                
                        break;
                    case 7://Checkbox
                
                        break;
                    case 8://Selector
                        $savingDataArray['selectors'][$tempKey[0]] = $value;
                        break;
                    case 10://Multilevel selector
                        $savingDataArray['multilevel'][$tempKey[0].'___'.$tempKey[3]] = $value;
                        break;
                    
                }
            }
            
        }


        parent::setQuery("  SELECT  COUNT(*) AS cc,
                                    (SELECT id FROM processing_page WHERE `key` = '$pageKey') AS page_id
                            FROM    processing
                            WHERE   row_id = '$rowID'");
        $isAlreadySaved = parent::getResultArray();
        $pageID         = $isAlreadySaved['result'][0]['page_id'];
        $isAlreadySaved = $isAlreadySaved['result'][0]['cc'];
        


        $tempData = json_encode($savingDataArray, JSON_UNESCAPED_UNICODE );


        if($isAlreadySaved == 0){
            parent::setQuery("INSERT INTO processing SET    datetime = NOW(),
                                                            processing_page_id = '$pageID',
                                                            row_id = '$rowID',
                                                            `value` = '$tempData'");
            parent::execQuery();
        }
        else{
            parent::setQuery("UPDATE processing SET `value` = '$tempData' WHERE row_id = '$rowID'");
            parent::execQuery();
        }
        return $savingDataArray;

    }


}

?>