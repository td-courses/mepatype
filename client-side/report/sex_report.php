<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        .pages {
            display: flex;
            justify-content: space-between;
            margin-top: 50px;
            margin-bottom: 40px;
        }

        .sections {
            display: flex;
            justify-content: center;
            width: 100%;
            min-height: 15vh;
            height: auto;
        }

        .sections>div {
            height: auto;
            border: 0 !important;
        }

        .Page-buttons {
            background-color: #EEECEC;
            transform: skew(-18deg, 0deg);
            margin-right: 2%;
            letter-spacing: 1px;
        }

        .half {
            width: 48%;
            margin: 0 1%;
        }

        .chosen-container,
        .chosen-container-multi {
            z-index: 1000;
        }

        #container,
        #container2 {
            width: 500px;
            height: 400px;
            margin: 0px !important;
        }


        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

        .activePage {
            background: #fef400 !important;
        }

        #loading1 {
            z-index: 999;
            top: 45%;
            left: 45%;
            position: absolute;
            display: none;
            padding-top: 15px;
        }
    </style>

    <link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>


    <script type="text/javascript">
        var tablesColumns = setColumns();
        var aJaxURL = "server-side/report/sex_report.action.php";
        var CustomSaveURL = "server-side/view/automator.action.php";
        var file_name = '';
        var rand_file = '';
        var tab = 0;
        var fillter = '';
        var fillter_all = '';
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var SelectedItem = "";
        var currentDate = new Date().getFullYear();

        $(document).ready(function() {
            var kendo = new kendoUI();
            GetDate("start_date");
            GetDate("end_date");
            $("#region_id, #raion_id,#sofel_id, #call_type, #type_selector").chosen();
            $("#page1").show();
            $("#page2").hide();
            $("#page3").hide();
            $("#page4").hide();
            $("#page5").hide();
            $("#page6").hide();

            $("#first").addClass("activePage"); // active 1st button 
            $(".first").show(); // show first page

            $("#callsType").chosen();
            $("#callsType_chosen").hide();
            LoadKendoTable_incomming("kendo_incomming_table1", tablesColumns[1]);

            highCharts("get_list_for_chart", "container1");
            highCharts("get_list_for_chart2", "container2");


        });

        $(document).on("click", "#communication_pdf_export", function() {
            // printcontent($("#kendo_incomming_table1").html());
            var ind = 0;
            $(".Page-buttons").each(function(index) {
                if ($(this).hasClass("activePage")) {
                    ind = index + 1;
                }
            });

            var kendoCols = [
                "null",
                "<tr style='font-size:1.3vw;'> <td>პერიოდი</td> <td>შემოსული ზარები</td> <td>მამრობითი</td> <td>მდედრობითი</td> </tr>",
                "<tr style='font-size:1.3vw;'> <td>პერიოდი</td> <td>გამავალი ზარები</td> <td>მამრობითი</td> <td>მდედრობითი</td> </tr>",
                "<tr style='font-size:1.3vw;'> <td>პერიოდი</td> <td>მამრობითი</td> <td>მდედრობითი</td> <td> ტელეფონი </td> <td>ელ. ფოსტა</td> <td>საიტის ჩათი</td> <td>ვიდეო ზარი</td> </tr>",
                "<tr style='font-size:1.3vw;'> <td>უწყება</td> <td>მამრობითი</td> <td>მდედრობითი</td> </tr>",
                "<tr style='font-size:1.3vw;'> <td>პროექტი</td> <td>მამრობითი</td> <td>მდედრობითი</td> </tr>",
                "<tr style='font-size:1.3vw;'> <td>მომართვის ტიპი</td> <td>მამრობითი</td> <td>მდედრობითი</td> </tr>"
            ]
            var kendoData = $("#kendo_incomming_table" + ind).data("kendoGrid").options.dataSource._data;
            var kendoRows = "";

            switch (ind) {
                case 1:
                    for (let i = 0; i < kendoData.length; i++) {
                        kendoRows += "<tr> <td>" + kendoData[i].period + "</td> <td>" + kendoData[i].incomcalls + "</td> <td>" + kendoData[i].female + "</td> <td>" + kendoData[i].male + "</td>  </tr>";
                    }

                    break;
                case 2:
                    for (let i = 0; i < kendoData.length; i++) {
                        kendoRows += "<tr> <td>" + kendoData[i].period + "</td> <td>" + kendoData[i].incomcalls + "</td> <td>" + kendoData[i].female + "</td> <td>" + kendoData[i].male + "</td>  </tr>";
                    }

                    break;
                case 3:
                    for (let i = 0; i < kendoData.length; i++) { // here
                        kendoRows += "<tr> <td>" + kendoData[i].period + "</td>  <td>" + kendoData[i].male + "</td> <td>" + kendoData[i].female + "</td> <td>" + kendoData[i].phone + "</td> <td>" + kendoData[i].email + "</td> <td>" + kendoData[i].sitechat + "</td> <td>" + kendoData[i].webcall + "</td>  </tr>";
                    }

                    break;
                case 4:
                    for (let i = 0; i < kendoData.length; i++) { // here
                        kendoRows += "<tr> <td>" + kendoData[i].uwyeba + "</td>  <td>" + kendoData[i].females + "</td> <td>" + kendoData[i].males + "</td>  </tr>";
                    }

                    break;
                case 5:
                    for (let i = 0; i < kendoData.length; i++) { // here
                        kendoRows += "<tr> <td>" + kendoData[i].project + "</td>  <td>" + kendoData[i].females + "</td> <td>" + kendoData[i].males + "</td>  </tr>";
                    }

                    break;
                case 6:
                    for (let i = 0; i < kendoData.length; i++) { // here
                        kendoRows += "<tr> <td>" + kendoData[i].momartva + "</td>  <td>" + kendoData[i].females + "</td> <td>" + kendoData[i].males + "</td>  </tr>";
                    }

                    break;

                default:
                    break;
            }
            printcontent(kendoCols[ind], kendoRows);

        });

        function printcontent(cols, data) {
            var mywindow = window.open('', '', '');
            mywindow.document.write('<html><title>Print</title><style> table{margin:auto;} td{border:1px solid #333;text-align:center;} </style><body>');
            mywindow.document.write('<table>' + cols + data + '</table>');
            mywindow.document.write('</body></html>');
            mywindow.document.close();
            mywindow.print();

            return true;
        }
        $(document).on("click", "#communication_excel_export", function() {
            // // PHP EXCEL VER
            var ind = 0;
            $(".Page-buttons").each(function(index) {
                if ($(this).hasClass("activePage")) {
                    ind = index + 1;
                }
            });
            //             var col = "";
            //             //col = $("#kendo_incomming_table" + ind).data("kendoGrid").options.dataSource._filter.filters;
            // 
            //             var excelURL = "includes/communication_export.php?act=sex_report_tab_" + ind + "&start_date=" + $(
            //                 '#start_date').val() + "&end_date=" + $('#end_date').val() + "&callsType=" + $("#callsType").val() + "&kendoFilters=" + JSON.stringify(col);
            //             var exl = window.open(excelURL, '_blank');

            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_incomming_table" + ind, "სქესის რეპორტი", [0]);
                $("#loading1").hide();
            }, 200);
        });

        function setColumns() {
            // first element of array is null DONT CANGE IT
            var kendoTableData = [{
                    sql: null,
                    geo: null,
                    action: null
                },
                {
                    sql: [
                        "id:string",
                        "period:string",
                        "incomcalls:string",
                        "male:string",
                        "female:string"
                    ],
                    geo: [
                        "ID",
                        "პერიოდი",
                        "შემოსული ზარები",
                        "მამრობითი",
                        "მდედრობითი"
                    ],
                    action: "get_list"
                },
                {
                    sql: [
                        "id:string",
                        "period:string",
                        "incomcalls:string",
                        "female:string",
                        "male:string"
                    ],
                    geo: [
                        "ID",
                        "პერიოდი",
                        "გამავალი ზარები",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_2"
                },
                {
                    sql: [
                        "id:string",
                        "period:string",
                        "male:string",
                        "female:string",
                        "phone:string",
                        "email:string",
                        "sitechat:string",
                        "webcall:string",
                    ],
                    geo: [
                        "ID",
                        "პერიოდი",
                        "მამრობითი",
                        "მდედრობითი",
                        "ტელეფონი",
                        "ელ.ფოსტა",
                        "საიტის ჩატი",
                        "ვიდეო ზარი"
                    ],
                    action: "get_list_3"
                },
                {
                    sql: [
                        "uwyeba:string",
                        "females:string",
                        "males:string"
                    ],
                    geo: [
                        "უწყება",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_4"
                },
                {
                    sql: [
                        "project:string",
                        "females:string",
                        "males:string"
                    ],
                    geo: [
                        "პროექტი",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_5"
                },
                {
                    sql: [
                        "momartva:string",
                        "females:string",
                        "males:string"
                    ],
                    geo: [
                        "მომართვის ტიპი",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_6"
                }
            ];
            return kendoTableData;
        }

        function highCharts(act, container) {
            param = new Object();
            param.start_date = $("#start_date").val();
            param.end_date = $("#end_date").val();
            param.region_id = $("#region_id").val();
            param.raion_id = $("#raion_id").val();
            param.sofel_id = $("#sofel_id").val();
            param.act = act;
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            var callcount = [];
                            var malecount = [];
                            var femalecount = [];
                            var months = [];
                            var monthname = '';
                            data['result'].forEach(myFunction);


                            function myFunction(item, index) {
                                callcount.push(Number(item.callcount));
                                malecount.push(Number(item.male))
                                femalecount.push(Number(item.female));;
                                if (item.month == 1) {
                                    monthname = "იან";
                                } else if (item.month == 2) {
                                    monthname = "თებ";
                                } else if (item.month == 3) {
                                    monthname = "მარ";
                                } else if (item.month == 4) {
                                    monthname = "აპრ";
                                } else if (item.month == 5) {
                                    monthname = "მაი";
                                } else if (item.month == 6) {
                                    monthname = "ინვ";
                                } else if (item.month == 7) {
                                    monthname = "ინლ";
                                } else if (item.month == 8) {
                                    monthname = "აგვ";
                                } else if (item.month == 9) {
                                    monthname = "სექ";
                                } else if (item.month == 10) {
                                    monthname = "ოქტ";
                                } else if (item.month == 11) {
                                    monthname = "ნოე";
                                } else if (item.month == 12) {
                                    monthname = "დეკ";
                                }
                                months.push(monthname)
                            }
                            Highcharts.chart(container, {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: currentDate
                                },
                                subtitle: {
                                    text: 'წელი'
                                },
                                xAxis: {
                                    categories: months,
                                    crosshair: true
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Rainfall (mm)'
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y:.1f} მომართვა</b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                plotOptions: {
                                    column: {
                                        pointPadding: 0.2,
                                        borderWidth: 0
                                    }
                                },
                                series: [{
                                    name: 'შემოსული ზარები',
                                    data: callcount

                                }, {
                                    name: 'მამრობითი',
                                    data: malecount

                                }, {
                                    name: 'მდედრობითი',
                                    data: femalecount

                                }]
                            });

                            $(".highcharts-credits").remove();
                            $(".highcharts-axis-title").remove();
                        }
                    }
                }
            });
        }

        function LoadKendoTable_incomming(gridname, cols) {
            //KendoUI CLASS CONFIGS BEGIN
            var aJaxURL = "server-side/report/sex_report.action.php";
            var gridName = gridname;
            var actions = '<button style="float:right;" id="communication_excel_export">EXCEL რეპორტი</button> <button style="float:right;margin:0 5px;" id="communication_pdf_export">PDF რეპორტი</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 40;
            var columnsSQL = cols.sql;
            var columnGeoNames = cols.geo;
            // alert error if sql and georgian cols quantrity is not same
            if (columnGeoNames.length != columnsSQL.length) {
                alert("error -> LoadKendoTable_incomming() geocols and sqlcols length not equal");
                return;
            }
            var columnsCount = columnGeoNames.length;
            var showOperatorsByColumns = [];
            var selectors = [];
            for (let i = 0; i < columnsCount; i++) {
                showOperatorsByColumns.push(0);
                selectors.push(0);
            }
            var filtersCustomOperators =
                '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI $(selector).addClass(className); CONFIGS END
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, cols.action, itemPerPage, columnsCount,
                columnsSQL, gridName, actions, editType, columnGeoNames,
                filtersCustomOperators, showOperatorsByColumns, selectors, "&table=" + gridname +
                "&start_date=" + $('#start_date').val() + "&end_date=" + $('#end_date').val() + "&callsType=" + $("#callsType").val() + "&raion_id=" + $("#raion_id").val() + "&region_id=" + $("#region_id").val() + "&sofel_id=" + $("#sofel_id").val(),
                1, '', '', '', false, false, false);
        }

        // Navigation on sub pages
        $(document).on("click", ".Page-buttons", function() {
            // hide all pages and remove all active class on buttons
            $(".Page-buttons").each(function() {
                $(this).removeClass("activePage");
                $("#page" + $(this).val()).hide();
            });

            // show call type select in last 3 three tabes only
            if ($(this).val() == 4 || $(this).val() == 5 || $(this).val() == 6) {
                $("#callsType_chosen").show();
            } else {
                $("#callsType_chosen").hide();
            }

            // show selected page and add active class on selected button
            $("#page" + $(this).val()).show();
            $(this).addClass("activePage");

            var kendoTable = $("#kendo_incomming_table" + $(this).val()).attr("name");

            if ($("#" + kendoTable).is(":empty")) {
                LoadKendoTable_incomming(kendoTable, tablesColumns[$(this).val()]);
            }
        });

        // filter kendo table one by one
        $(document).on("click", "#loadtablenew", function() {
            var ind = 0;
            // which id is actived empty and resend LoadKendoTable_incomming
            $(".Page-buttons").each(function(index) {
                if ($(this).hasClass("activePage")) {
                    ind = index + 1;
                }
            });

            var kendoTable = $("#kendo_incomming_table" + ind).attr("name");
            $("#kendo_incomming_table" + ind).empty();
            LoadKendoTable_incomming(kendoTable, tablesColumns[ind]);
            if (ind == 1) {
                highCharts("get_list_for_chart", "container1");
            } else if (ind == 2) {
                highCharts("get_list_for_chart2", "container2");
            }
        });

        $(document).on("change", "#region_id", function() {
            param = new Object();
            param.act = "region_2";
            param.cat_id = $("#region_id").val();
            $.ajax({
                url: CustomSaveURL,
                data: param,
                success: function(data) {
                    $("#raion_id").html(data.page);
                    $("#raion_id").trigger("chosen:updated");
                }
            });
        });
        $(document).on("change", "#raion_id", function() {
            param = new Object();
            param.act = "region_2";
            param.cat_id = $("#raion_id").val();
            $.ajax({
                url: CustomSaveURL,
                data: param,
                success: function(data) {
                    $("#sofel_id").html(data.page);
                    $("#sofel_id").trigger("chosen:updated");
                }
            });
        });
    </script>
</head>

<body>
    <div id="tabs">
        <!-- BUTTONS FOR PAGE SWITCH -->
        <div style="display: flex;padding: 20px 5px;">
            <button class="Page-buttons" value="1" id="first">შემომავალი ზარი</button>
            <button class="Page-buttons" value="2">გამავალი ზარი</button>
            <button class="Page-buttons" value="3">წყაროების მიხედვით</button>
            <button class="Page-buttons" value="4">უწყება</button>
            <button class="Page-buttons" value="5">პროექტი</button>
            <button class="Page-buttons" value="6">მომართვის ტიპი</button>
        </div>

        <!-- FILTER -->
        <div class="callapp_filter_show">
            <div class="callapp_filter_body" myvar="1">
                <div style="width: 100%;">
                    <table>
                        <tr>
                            <td style="padding: 5px 0 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                            </td>
                            <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">-
                            </td>
                            <td style="padding: 5px 10px 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 84px;">
                            </td>
                            <td style="width: 200px;">
                                <select id="region_id" style="width: 185px;">
                                    <?php
                                    $data = '<option value="0">აირჩიე რეგიონი</option>';
                                    global $db;
                                    $db->setQuery("  SELECT  id,
                                                        	   `name`
                                                        FROM   `regions` 
                                                        WHERE   parent_id = 0 AND actived = 1");

                                    $res = $db->getResultArray();

                                    foreach ($res[result] as $req) {
                                        $data .= '<option value="' . $req[id] . '">' . $req[name] . '</option>';
                                    }
                                    echo $data;
                                    ?>
                                </select>
                            </td>
                            <td style="width: 212px;">
                                <select id="raion_id" style="width: 185px;">
                                    <option value="0">აირჩიე რაიონი</option>
                                </select>
                            </td>
                            <td style="width: 212px;">
                                <select id="sofel_id" style="width: 185px;">
                                    <option value="0">აირჩიე სოფელი</option>
                                </select>
                            </td>
                            <td style="width:100px;height:30px !important;display:flex;align-items:center;">
                                <select data-placeholder="ზარის ტიპი" id="callsType" multiple>
                                    <option value="1">შემომავალი</option>
                                    <option value="20">გამავალი</option>
                                    <option value="4">ჩატი</option>
                                    <option value="7">მეილი</option>
                                    <option value="10">ვიდეო ზარი</option>
                                </select>
                            </td>
                            <td>
                                <div class="k-content">
                                    <button id="loadtablenew">ფილტრი</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <!-- KENDO TABLES' OUTPUTS -->
        <div class="pages first">
            <!-- PAGE 1 -->
            <div id="page1" class="sections">
                <div id="kendo_incomming_table1" name="kendo_incomming_table1" class="half"></div>

                <figure class="highcharts-figure half">
                    <div id="container1"></div>

                </figure>

            </div>

            <!-- PAGE 2 -->
            <div id="page2" class="sections">
                <div id="kendo_incomming_table2" name="kendo_incomming_table2" class="half"></div>

                <figure class="highcharts-figure half">
                    <div id="container2"></div>

                </figure>

            </div>

            <!-- PAGE 3 -->
            <div id="page3" class="sections">
                <div id="kendo_incomming_table3" name="kendo_incomming_table3"></div>
            </div>

            <!-- PAGE 4 -->
            <div id="page4" class="sections">
                <div id="kendo_incomming_table4" name="kendo_incomming_table4"></div>
            </div>


            <!-- PAGE 5 -->
            <div id="page5" class="sections">
                <div id="kendo_incomming_table5" name="kendo_incomming_table5"></div>
            </div>


            <!-- PAGE 6 -->
            <div id="page6" class="sections">
                <div id="kendo_incomming_table6" name="kendo_incomming_table6"></div>
            </div>

        </div>

    </div>
    <div id="loading1">
        <p><img src="media/images/loader.gif" /></p>
    </div>
</body>

</html>