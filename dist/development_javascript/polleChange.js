
setTimeout(() => {
    let path = document.querySelectorAll('.filled-bar');

    path.forEach(path => {
        let polleProcent = path.parentElement.parentElement.querySelector('.polle_procent');
        
        let pathFilled = path.dataset.filled;
        polleProcent.innerHTML = `${pathFilled}%`;
        let length = path.getTotalLength();

        let oneProcent = length * 0.1 * 0.1;
        let filledProcent = 100 - pathFilled


        path.style.strokeDashoffset = oneProcent * filledProcent 


    })

}, 2000);

