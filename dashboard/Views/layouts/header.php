<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- BABEL -->
    <!-- <script src="Assets/js/lib/babel.min.js"></script> -->

    <!-- LIB -->
    <script src="Assets/js/lib/jquery.min.js"></script>
    <script src="Assets/js/lib/crypto-js.min.js"></script>
    <script src="Assets/js/init.class.js" type="module"></script>
    <script src="Assets/js/lib/apexcharts.js"></script>
    <script  src="Assets/js/lib/jBox/jBox.all.js"></script>

    <!-- KENDO -->
    <script type="text/javascript" language="javascript" src="Assets/js/lib/kendoUI/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="Assets/js/lib/kendoUI/kendo.all.min.js"></script>
    <script type="text/javascript" language="javascript" src="Assets/js/lib/kendoUI/pako_deflate.min.js"></script>
    <script type="text/javascript" language="javascript" src="Assets/js/helpers/kendo.main.class.js"></script>
 
    <link rel="stylesheet" href="Assets/css/lib/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="Assets/css/lib/kendoUI/kendo.default-v2.min.css" />
    <link rel="stylesheet" href="Assets/css/lib/jBox/jBox.all.css" />
    
    <link rel="stylesheet" href="Assets/css/style.css">

</head>
<body>

