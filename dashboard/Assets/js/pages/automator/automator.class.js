import { kendo__appendToHeader } from "../../helpers/kendo.helper.js";
import Tdg from "../../tdg.class.js";
import PageModal from "./page.modal.js";

export default class Automator extends Tdg{
    constructor(){
        super();
        self.Automator = this;
        this.state = {
            sectionName: document.querySelector("section[automator]"),
            element: []
        }
        this.removeLoading();
        this.init();

       
    }

    init = () => {

        this.build()

    }


    build = () => {

      this.append(this.state.sectionName, this.createTablePages(), false, (cb) => {
         kendo__appendToHeader(cb, this.CreateElement({
             element: "HI",
             children: "123"
         }), this.CreateElement({
            element: "HI",
            children: "123"
        }))
      });

    }


    createTablePages = () => {

        this.state.element.pageTable = this.CreateElement({
            element: "kendo",
            type: "table",
            column: [
                {
                    field: "id",
                    hidden: false
                },
                {
                    field: "შექმნის თარიღი",
                    size: 160
                },
                {
                    field: "იდენტიფიკატორი",
                    size: 160
                },
                {
                    field: "Fieldset სია"
                }
            ],
            data: {
                route: "Automator",
                act: "getPageList"
            },
            option: {
                header: true,
                numeric: false,
                footer: true
            },
            ondblclick: (cb) => {
               
                this.buildModal(null,  {
                    width: '60vw',
                    height: '50vh',
                    content: new PageModal({
                        ...cb
                    }).init(),
                    buttons: {
                        save: {
                            name: "შენახვა",
                            onclick: function(modal){
                                console.log(self.Automator.getResponse({
                                    route: "Automator",
                                    act: "getGeneratedInputs",
                                    pageKey: "incommingRequest"
                                }))
                                alert(213)
                            }
                        }
                    },
                    confirmOnCancel: true,
                    draggable: 'title',
                    title: 'გვერდის Fieldset ცნობარი'
                })

            },
            callback: (cb) => {
                // console.log(cb)
            }
        });
        return this.state.element.pageTable;

    }

    


}