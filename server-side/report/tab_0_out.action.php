<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
header('Content-Type: application/json');
$start  = $_REQUEST['start'];
$end    = $_REQUEST['end'].' 23:59';
$agent  = $_REQUEST['agent'];
$queuet = $_REQUEST['queuet'];

$db->setQuery("SELECT COUNT(*) AS `count`,
            		 'ნაპასუხები' AS `cause`
               FROM   asterisk_call_log
               WHERE  asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 13
               AND    asterisk_call_log.source IN($agent)
               AND    asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end')
               UNION ALL
               SELECT COUNT(*) AS `count`,
                	 'უპასუხო' AS `cause`
               FROM   asterisk_call_log
               WHERE  asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_status_id = 12
               AND    asterisk_call_log.source IN($agent)
               AND    asterisk_call_log.call_datetime >= UNIX_TIMESTAMP('$start') AND asterisk_call_log.call_datetime <= UNIX_TIMESTAMP('$end')");

$result = $db->getResultArray();

$row  = array();
$rows = array();
foreach($result[result] AS $r) {
    $row[0] = $r[cause].': '.$r[count];
    $row[1] = (float)$r[count];
	array_push($rows,$row);
}

echo json_encode($rows);

?>