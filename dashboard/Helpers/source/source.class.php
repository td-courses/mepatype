<?php


use Routers\dbClass;

class Source extends dbClass{

    public function get(){

        $query = "SELECT `id`, `name`, `key` FROM source WHERE actived = 1 AND id != 7";

        parent::setQuery($query);
        $response = parent::getResultArray();

        return $response['result'];

    }

}