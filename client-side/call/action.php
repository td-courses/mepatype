<head>
</head>

<body>



<div class="second_popup__addQuestions">
	<div class="second_popup__addQuestions__blacked-BG"></div>
	<div class="second_popup__addQuestions__container">
		<!-- <div class="title">
			<h1 class="title__text">ახალი შეკითხვა</h1>
		</div> -->

		<div class="second_popup__addQuestions__container__inputs">
			<div class="question">
				<p class="input_disc">კითხვა</p>
				<input type="text">
			</div>
			<label class="checkmark_container">
				<p>მულტი სელექტი</p>
				<input type="checkbox" class="multiOrNotCheck">
				<span class="checkmark"></span>
			</label>
			<div class="answers">
				<p class="input_disc">აღწერა</p>
				<ol class="" type="1">  
					<li><input type="text"><div class="delete_answer" onclick="deleteAnswer(event)">✖</div></li>
					<li><input type="text"><div class="delete_answer" onclick="deleteAnswer(event)">✖</div></li>
					<li><input type="text"><div class="delete_answer" onclick="deleteAnswer(event)">✖</div></li>
				</ol>
			</div>
			<div class="addMoreAnswers">+</div>
			<button class="popupform__button second_popupform__button">დამატება</button>
		</div>
	</div>
</div>

<div class="popup-window"> 

	<div class="popup-window__blacked-BG"></div>
	<div class="popup-window__container">
		<div class="popup-window__container__wrapper">
			<!-- <div class="title">
				<h1 class="title__text">პოსტი</h1>
			</div> -->
			<div class="content">
				<select id="formSelect">
					<option value="post">
						<p> პოსტი</p>
					</option>
					<option value="event">
						<p> სიახლეები</p>	
					</option>
					<option value="poll">
						<p> გამოკითხვა </p>
					</option>
					<option value="inic">
						<p> ინიციატივა </p>
					</option>
				</select>

				<input type="file" id="image_upload" name="image_upload" onchange="previewFiles()"></input>
				<input type="text" id="youtube_url" placeholder="ჩააკოპირეთ youtube ლინკი"></input>
				<div class="image_preview">
					<div class="image_preview__image" onmouseup="viewImage(this)" width="100%" height="100%"></div>
					<div class="image_preview__closeBtn" onmouseup="clearImage()"><svg fill="white" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg></div>
				</div>
				<div class="video_preview">
					<div class="video_preview__video" width="100%" height="100%">
						<iframe width="10rem" height="10rem" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
					<div class="image_preview__closeBtn" onmouseup="clearYoutubeUrl()"><svg fill="white" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg></div>
				</div>


				<div class="popup_form__inputs post_form__adresat adresat">
						<p class="input_disc">ადრესატი</p>
						<div class="adresat_add_in_input_btn ">
							<div class="adresat_added">
								<ul>
									<!-- <li>
										<div class="tagged">
											<p>ყველას</p>
											<div class="delete_tag" onclick="deleteTag(event)">&#10006;</div>
										</div>
									</li> -->

									<li class="add_in_input_btn__container">
										<p class="add_in_input_btn">დამატება +</p>
									</li>
									<!-- <li>
										<input type="text">
									</li> -->
								</ul>




							</div>
						</div>




						<div class="adresat_inputs">
							<div class="close_adresatBtn"> 
								<svg fill="white" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg>
							</div>
							<div id="adresat__groups" class="adresat_inputs__containers">
								<div class="adresat__create_group_btn adresat__create_user_group_btn" style="cursor: pointer">
									<p> ჯგუფის შექმნა <span> + </span></p>
								</div>
								<div class="adresat__add_btn adresat__add_btn__user " style="cursor: pointer">
									<p> მონიშვნა <span> + </span></p>
								</div>
								<div class="adresat__created__groups"></div>
								<p style="font-weight: bold; padding: 1rem 1rem;">მომხმარებლი</p>
								<div class="adresat__users container__users" id="adresat__users">

								</div>
							</div> 
							<div id="adresat__users" class="adresat_inputs__containers">
								<div class="adresat__created__groups"></div>
								<p style="font-weight: bold; padding: 1rem 1rem;">ჯგუფი</p>
								<div class="adresat__users container__groups" id="adresat__groups">
								
								</div>
							</div>
							<div id="adresat__departaments" class="adresat_inputs__containers">
								<!-- <div class="adresat__add_btn">
									<p> დეპარტამენტის მონიშვნა <span> + </span></p>
								</div> -->
								<p style="font-weight: bold; padding: 1rem 1rem;">დეპარტამენტი</p>
								<div class="adresat__users container__departments" id="adresat__department">
									
								</div>
							</div>
						</div>
					</div> 

				<div class="post_form popup_form" action="" method="post">
					

					<div class="popup_form__inputs  post_form__description">
						<p class="input_disc">აღწერა</p>
						<textarea class="emoji_input" placeholder="შეიყვანეთ აღწერა" name="" id=""></textarea>

						<div class="emojiBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3.5 8c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm-7 0c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm3.501 10c-2.801 0-4.874-1.846-6.001-3.566l.493-.493c1.512 1.195 3.174 1.931 5.508 1.931 2.333 0 3.994-.736 5.506-1.931l.493.493c-1.127 1.72-3.199 3.566-5.999 3.566z"/></svg></div>
						<div class="imageUpoadBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16 12c0 2.206-1.794 4-4 4s-4-1.794-4-4 1.794-4 4-4 4 1.794 4 4zm8-7v14c0 2.761-2.238 5-5 5h-14c-2.761 0-5-2.239-5-5v-14c0-2.761 2.239-5 5-5h14c2.762 0 5 2.239 5 5zm-6 7c0-3.313-2.687-6-6-6s-6 2.687-6 6 2.687 6 6 6 6-2.687 6-6zm3-8c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1z"/></svg></div>
						<div class="youtubeBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.441 16.892c-2.102.144-6.784.144-8.883 0-2.276-.156-2.541-1.27-2.558-4.892.017-3.629.285-4.736 2.558-4.892 2.099-.144 6.782-.144 8.883 0 2.277.156 2.541 1.27 2.559 4.892-.018 3.629-.285 4.736-2.559 4.892zm-6.441-7.234l4.917 2.338-4.917 2.346v-4.684z"/></svg></div>
					</div>
					<button class="popupform__button postPostBtn popupFormPostBtn" id="postPostBtn" onmouseup="getAllPosts()">დაპოსტვა</button>
					<button class="popupform__button postPostBtn post_editBtn" onmouseup="updatePost(this)">რედაქტირება</button>
				</div> 

				<div class="inic_form" style="display: none;" action="" method="post">
					

					<div class="popup_form__inputs  post_form__description2">
						<p class="input_disc">აღწერა</p>
						<textarea class="emoji_input3" placeholder="შეიყვანეთ აღწერა" name="" id=""></textarea>

						<div class="emojiBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3.5 8c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm-7 0c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm3.501 10c-2.801 0-4.874-1.846-6.001-3.566l.493-.493c1.512 1.195 3.174 1.931 5.508 1.931 2.333 0 3.994-.736 5.506-1.931l.493.493c-1.127 1.72-3.199 3.566-5.999 3.566z"/></svg></div>
						<div class="attachPostCLICK" style="position: absolute;bottom: 4px;right: 25%;font-size: 2rem;cursor: pointer;">
							<i style="font-size:19px; color:#00a57f; " class="fas fa-paperclip"></i>
							<label><input class="attachPost" style="position:absolute; bottom:2000px;" id="attachPost" type="file"></label>
						</div>
						<div class="attach_file">
							
						</div>
						<div class="imageUpoadBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16 12c0 2.206-1.794 4-4 4s-4-1.794-4-4 1.794-4 4-4 4 1.794 4 4zm8-7v14c0 2.761-2.238 5-5 5h-14c-2.761 0-5-2.239-5-5v-14c0-2.761 2.239-5 5-5h14c2.762 0 5 2.239 5 5zm-6 7c0-3.313-2.687-6-6-6s-6 2.687-6 6 2.687 6 6 6 6-2.687 6-6zm3-8c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1z"/></svg></div>
						<div class="youtubeBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.441 16.892c-2.102.144-6.784.144-8.883 0-2.276-.156-2.541-1.27-2.558-4.892.017-3.629.285-4.736 2.558-4.892 2.099-.144 6.782-.144 8.883 0 2.277.156 2.541 1.27 2.559 4.892-.018 3.629-.285 4.736-2.559 4.892zm-6.441-7.234l4.917 2.338-4.917 2.346v-4.684z"/></svg></div>
					</div>
					<button style="width:170px;" class="popupform__button inicPostBtn popupFormPostBtn" id="inicPostBtn" onmouseup="getAllPosts()">დაპოსტვა</button>
					<button class="popupform__button postPostBtn post_editBtn" onmouseup="updatePost(this)">რედაქტირება</button>
				</div> 

				<div class="event_form" style="display: none;" action="" method="post">


					<div class="popup_form__inputs  event_form__description post_form__description">
						<p class="input_disc">აღწერა</p>
                        <div class="divblock">
						    <textarea class="emoji_input2" placeholder="შეიყვანეთ აღწერა" name="" id="" rows="3"></textarea>

                            <div class="emojiBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3.5 8c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm-7 0c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm3.501 10c-2.801 0-4.874-1.846-6.001-3.566l.493-.493c1.512 1.195 3.174 1.931 5.508 1.931 2.333 0 3.994-.736 5.506-1.931l.493.493c-1.127 1.72-3.199 3.566-5.999 3.566z"/></svg></div>
                            <div class="imageUpoadBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16 12c0 2.206-1.794 4-4 4s-4-1.794-4-4 1.794-4 4-4 4 1.794 4 4zm8-7v14c0 2.761-2.238 5-5 5h-14c-2.761 0-5-2.239-5-5v-14c0-2.761 2.239-5 5-5h14c2.762 0 5 2.239 5 5zm-6 7c0-3.313-2.687-6-6-6s-6 2.687-6 6 2.687 6 6 6 6-2.687 6-6zm3-8c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1z"/></svg></div>
                            <div class="youtubeBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.441 16.892c-2.102.144-6.784.144-8.883 0-2.276-.156-2.541-1.27-2.558-4.892.017-3.629.285-4.736 2.558-4.892 2.099-.144 6.782-.144 8.883 0 2.277.156 2.541 1.27 2.559 4.892-.018 3.629-.285 4.736-2.559 4.892zm-6.441-7.234l4.917 2.338-4.917 2.346v-4.684z"/></svg></div>
                        </div>
                        </div>

					<div class="popup_form__inputs event_form__adresat">
						<p class="input_disc">ლოკაცია</p>
						<input class="event_form_locationInp" placeholder="შეიყვნაეთ სიახლეს ლოკაცია" type="text" placeholder=" ">
					</div>

					<div class="startEnd_time">
						<p class="input_disc">დასაწყისი და დასასრულის თარიღი</p>
						<input type="input"  autocomplete="off" placeholder="დასაწყისი თარიღი და დრო" name="" id="eventStartDate">
						<input type="input"  autocomplete="off" placeholder="დასასრული თარიღი და დრო" name="" id="eventEndDate">
					</div>

					
					<button class="popupform__button eventPostBtn  popupFormPostBtn" onclick="getAllPosts()">დაპოსტვა</button>
					<button class="popupform__button postPostBtn post_editBtn" onmouseup="updatePost(this)">რედაქტირება</button>


				</div>

				<div class="post_form poll_form" action="" method="post">
					<div class="addPollQuestionsBtn input_disc"> <p> დამატე შეკითხვა + </p></div>
					<div class="start-and-end-date startEnd_time"> 
						<p class="input_disc"> დასაწყისი და დასასრული თარიღი </p>
						<input type="input" name="" id="pollStartDate">
						<input type="input" name="" id="pollEndDate">
					</div>
					<div class="popup_form__inputs  post_form__description poll_form__description" style="grid-row: 1/3">
						<p class="input_disc">აღწერა</p>
                        <div class="divblock">
						    <textarea class="emoji_input" placeholder="შეიყვანეთ აღწერა" name="" id="" rows="3"></textarea>
						    <div class="emojiBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3.5 8c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm-7 0c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm3.501 10c-2.801 0-4.874-1.846-6.001-3.566l.493-.493c1.512 1.195 3.174 1.931 5.508 1.931 2.333 0 3.994-.736 5.506-1.931l.493.493c-1.127 1.72-3.199 3.566-5.999 3.566z"/></svg></div>
						    <div class="imageUpoadBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16 12c0 2.206-1.794 4-4 4s-4-1.794-4-4 1.794-4 4-4 4 1.794 4 4zm8-7v14c0 2.761-2.238 5-5 5h-14c-2.761 0-5-2.239-5-5v-14c0-2.761 2.239-5 5-5h14c2.762 0 5 2.239 5 5zm-6 7c0-3.313-2.687-6-6-6s-6 2.687-6 6 2.687 6 6 6 6-2.687 6-6zm3-8c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1z"/></svg></div>
						    <div class="youtubeBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.441 16.892c-2.102.144-6.784.144-8.883 0-2.276-.156-2.541-1.27-2.558-4.892.017-3.629.285-4.736 2.558-4.892 2.099-.144 6.782-.144 8.883 0 2.277.156 2.541 1.27 2.559 4.892-.018 3.629-.285 4.736-2.559 4.892zm-6.441-7.234l4.917 2.338-4.917 2.346v-4.684z"/></svg></div>
                        </div>
                    </div>
					<button class="popupform__button pollPostBtn  popupFormPostBtn" onclick="getAllPosts()">დაპოსტვა</button>
					<button class="popupform__button postPostBtn post_editBtn" onmouseup="updatePost(this)">რედაქტირება</button>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="likes-popup-window">
	<div class="likes-popup-window__blacked-BG"></div>
	<div class="likes-popup-window__container">
		<div class="likes-popup-window__container__emojis">
			<ul>
				<li class="active-emoji" onclick="showReactionUsers(this)" data-typeemoji = "happy">
					<div class="emoji emoji__popUp happy"></div>
					<span></span>
				</li>
				<li class="" onclick="showReactionUsers(this)" data-typeemoji = "love">
					<div class="emoji emoji__popUp love"></div>
					<span></span>
				</li>
				<li class="" onclick="showReactionUsers(this)" data-typeemoji = "amazed">
					<div class="emoji emoji__popUp amazed"></div>
					<span></span>
				</li>
				<li class="" onclick="showReactionUsers(this)" data-typeemoji = "neutral">
					<div class="emoji emoji__popUp neutral"></div>
					<span></span>
				</li>
				<li class="" onclick="showReactionUsers(this)" data-typeemoji = "sad">
					<div class="emoji emoji__popUp sad"></div>
					<span></span>
				</li>
			</ul>
		</div>

		<div class="likes-popup-window__container__users">
			<ul>
				
			</ul>
		</div>
	</div>
</div>

<div class="image_viewer">
	<div class="image_viewer__bg"></div>
	<div class="image_viewer__container">
		<div class="close_image_viewer"> 
			<svg fill="white" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M12 11.293l10.293-10.293.707.707-10.293 10.293 10.293 10.293-.707.707-10.293-10.293-10.293 10.293-.707-.707 10.293-10.293-10.293-10.293.707-.707 10.293 10.293z"/></svg>
		</div>
		<img class="image_viewer__container__image" src="" alt="">
	</div>
</div>



<main class="main">
<span id="pageType" style="display:none;"></span>
	<div class="main__container">
		<nav class="activation_navigation">
			<div class="activation_navigation__container">
				<!-- <div class="activation_navigation__container__search">
					<form action="">
						<div class="icon"></div>
						<input placeholder="ძიება" type="text" class="input">
					</form>
				</div> -->
				<div class="add_button">
					<div class="add_button__button ">
						<p class="">დამატება</p>
					</div>
					<p class="add_button__plus">+</p>
				</div>
				<div class="activation_navigation__container__tablinks_container">
					<div class="post_tabs tablinks_active" id="git statu" href="" data-tabType="all">
						<p> აქტივობების ჟურნალი </p>
					</div>
					<div class="post_tabs" href="" id="eventtabposts" data-tabType="event">
						<p> სიახლეები </p>
					</div>
					<div class="post_tabs" href="" id="polltabsposts" data-tabType="poll">
						<p> გამოკითხვა </p>
					</div>
					<div class="post_tabs" id="inici" href="" data-tabType="ini">
						<p> ინიციატივები </p>
					</div>
				</div>
			
			</div>
			
		</nav>
		
	</div>

	
		
		
		<div class="posts">
		</div>


		<div class="get_more_post_btn">more posts</div>

		
</main>


<script>
	$(function () {
		$('#datetimepicker4').datetimepicker();
		$('#datetimepicker5').datetimepicker();
	});
</script>
</body>

