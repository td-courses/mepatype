<html>
<head>
	<script type="text/javascript">
		var aJaxURL	     = "server-side/info/user_language.action.php";		//server side folder url
		var tName	     = "example";											//table name
		var fName	     = "add-edit-form";										//form name
		var colum_number = 1;
	    var main_act     = "get_list";
	    
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

		$(document).ready(function () {
			LoadTable();

			/* Add Button ID, Delete Button ID */
			GetButtons("add_button", "delete_button");

			SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);

			$("#restore_button").button();
			GetTabs("tabs");
// 			$("#current").button();
// 			$("#archive").button();


		});

		function LoadTable(action = "get_list"){
			/* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, action, 2, "", 0, "", 1, "asc", "", change_colum_main);
			setTimeout(function(){
		    	$('.ColVis, .dataTable_buttons').css('display','none');
		    }, 90);
		}

		function LoadDialog(){
			var id		= $("#pers_id").val();
			if(id != ""){
				$("#lname_fname").attr("disabled", "disabled");
			}

			GetButtons("choose_button");
			GetButtons("choose_buttondisabled");
			
			/* Dialog Form Selector Name, Buttons Array */
			GetDialog(fName, 450, "auto", "");

			if( $("#position").val() == 13 ){
					$("#passwordTR").removeClass('hidden');
			}
			$( "#accordion" ).accordion({
				active: false,
				collapsible: true,
				heightStyle: "content",
				activate: function(event, ui) {
					$("#is_user").val();
				}
			});
			$('#position,#dep_id,#service_center_id,#branch_id,#ext, #site_ids').chosen({ search_contains: true });
		}

	    // Add - Save
		$(document).on("click", "#save-dialog", function () {
			
			param = new Object();

            //Action
	    	param.act	= "save_pers";
		    param.id	= $("#pers_id").val();
		    param.position		= $("#person_position").val();
		    
			if(param.position == ""){
				alert("შეავსეთ თანამდებობა");
            } else{
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {
						if(typeof(data.error) != "undefined"){
							if(data.error != ""){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}

		});

	
 /* Enable Event */
 $(document).on("click", "#restore_button", function () {
    	
        var data = $(".check:checked").map(function () {
            return this.value;
        }).get();
    	
		
        for (var i = 0; i < data.length; i++) {
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=enable&id=" + data[i] ,
                dataType: "json",
                success: function (data) {
	                    if (data.error != "") {
	                        alert(data.error);
	                    } else {
	                    	LoadTable('get_list_archive');
	                        $(".check:checked").attr("checked", false);
	                    }
                }
            });
        }

    });


		$(document).on("click","#current",function(){
			$("#restore_button").css("display","none");
			$("#add_button").css("display","block");
			$("#delete_button").css("display","block");
			LoadTable();
		})

		$(document).on("click","#archive", function(){
			$("#add_button").css("display","none");
			$("#delete_button").css("display","none");
			$("#restore_button").css("display","block");
			LoadTable('get_list_archive');
		})

		function change_btn_color(id) {
            document.getElementById("current").style.backgroundColor = "white";
            document.getElementById("archive").style.backgroundColor = "white";
            document.getElementById(id).style.backgroundColor = "#a9a9a940";
            document.getElementById(id).style.marginBottom = "-2px";
            loadtabs(activep = "", activech = "");

        }
    </script>
	<style>
	.label_td{
		width:170px;
	}
	#info_table input{
		width: 230px !important;
	}
	#info_table select{
		width: 236px !important;
	}
	[aria-describedby="example_info"]{
		width: 100%
	}
    
	</style>
</head>

<body>
<div id="tabs" style=" border: 0px solid #aaaaaa;">
	<div class="callapp_head">ენა<hr class="callapp_head_hr"></div>
	<table id="table_right_menu" style="top: 80px; left: -1px;">
    	<tr>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
    			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
    		</td>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
    			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
    		</td>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
    			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
    		</td>
    	</tr>
    </table>
  
    <div id="button_area">
        <button id="add_button">დამატება</button>
		<button id="restore_button" style="display:none">აღდგენა</button>
        <button id="delete_button" style="margin-left:5px;float: none;">წაშლა</button>
    </div>
	<div class="l_responsible_fix" style="width: 82%;">
<table class="display" id="example" >
    <thead>
        <tr id="datatable_header">
            <th>ID</th>
            <th style="width:100%">თანამდებობა</th>
          
          
            <th class="check">#</th>
        </tr>
    </thead>
    <thead>
        <tr class="search_header">
            <th class="colum_hidden">
            	<input type="text" name="search_id" value="ფილტრი" class="search_init" />
            </th>
            <th>
                <input type="text" name="search_name" value="ფილტრი" class="search_init" />
            </th>
           
            <th>
            	<div class="callapp_checkbox">
                    <input type="checkbox" id="check-all" name="check-all" />
                    <label for="check-all"></label>
                </div>
            </th>
        </tr>
    </thead>
</table>
</div>
</div>

    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="ენა">
    	<!-- aJax -->
	</div>
    
</body>
<style>
.container {
  display: flex;
  align-items: center;
  justify-content:space-between;
}
</style>
</html>