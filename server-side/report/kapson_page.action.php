<?php
include('../../includes/classes/class.Mysqli.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
global $db;
$db = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();
switch ($action) {
    case 'get_columns':
        $columnCount = 		$_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] = 	$_REQUEST['names'];
        $selectors[] = 		$_REQUEST['selectors'];
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
            {
                continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if($columns[$j] == 'id')
                {
                    $width = "5%";
                }
                else if($columns[$j] == 'shin_status'){
                    $width = "12%";
                }
                else{
                    $width = 'auto';
                }
                if($columns[$j] == 'inc_id')
                {
                    $hidden = true;
                }
                else if($columns[$j] == 'id' OR $columns[$j] == 'req_id'){
                    $hidden = true;
                }
                else{
                    $hidden = false;
                }
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        $new_data = array();
        for($j=0;$j<$columnCount;$j++)
        {
            if($types[$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else if($types[$j] == 'number'){

                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        $filtArr = array('fields'=>$new_data);
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
        $data = $kendoData;
    break;
    case 'get_list' :
        $id          =      $_REQUEST['hidden'];
        $columnCount = 		$_REQUEST['count'];
        $itemPerPage = 		$json_data['pageSize'];
        $cols[]      =      $_REQUEST['cols'];
		$start        	= $_REQUEST['start'];
        $end         	= $_REQUEST['end'];
            $db->setQuery(" SELECT  DATE(FROM_UNIXTIME(  asterisk_call_log.call_datetime ))   AS 'day',
                                                                             user_info.`name` AS 'operator',
                                                    SUM(IF( call_status_id IN ( 6, 7, 8 ), 1, 0 )) AS ' incom_answered ',
                                                    IFNULL(asterisk_call_missedx.raod,0) AS ' incom_rejected ',
                                                    SUM(IF( call_status_id IN ( 13 ), 1, 0 )) AS 'outgoing_answered',
                                                    SUM(IF( call_status_id IN ( 12 ), 1, 0 )) AS 'outgoing_rejected'
            
                                                    FROM asterisk_call_log 
                                                    LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                                                    LEFT JOIN (
                                                                SELECT  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS timex,asterisk_call_missed.user_id,count(*) AS raod ,asterisk_call_log.id AS asterisk_call_log_id
                                                                FROM asterisk_call_missed JOIN asterisk_call_log ON asterisk_call_log.id = asterisk_call_missed.asterisk_call_log_id 
                                                                WHERE NOT ISNULL(asterisk_call_missed.user_id) AND NOT ISNULL(asterisk_call_log.id)
                                                                GROUP BY DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) ,asterisk_call_missed.user_id
                                                                ) AS asterisk_call_missedx ON asterisk_call_missedx.timex = DATE(FROM_UNIXTIME( asterisk_call_log.call_datetime )) AND asterisk_call_log.user_id = asterisk_call_missedx.user_id
                                                    WHERE DATE( FROM_UNIXTIME( asterisk_call_log.call_datetime )) BETWEEN '$start' AND '$end' AND asterisk_call_log.user_id > 0 AND NOT ISNULL(asterisk_call_log.user_id) AND call_type_id IN(1,2)
                                                    GROUP BY DATE(FROM_UNIXTIME( asterisk_call_log.call_datetime )), asterisk_call_log.user_id
                                                    ORDER BY  DATE(FROM_UNIXTIME( asterisk_call_log.call_datetime ))");
        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;
	break;
    default:
        $error = 'Action is Null';
}
echo json_encode($data);
?>