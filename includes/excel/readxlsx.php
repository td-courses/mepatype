<?php
require_once('PHPExcel.php');
require_once('class.Mysqli.php');

$filename     = "simactivated.xls";
$type         = PHPExcel_IOFactory::identify($filename);
$objReader    = PHPExcel_IOFactory::createReader($type);
$objPHPExcel  = $objReader->load($filename);

$act = 'sim';

switch ($act) {
  case 'registered':
    $values = '';
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    
      $j=0;
      $data = $worksheet->toArray();
    
      if($data[0][0] == 'FDNo' && $data[0][1] == 'FMNo' && $data[0][2] == 'RegNo' && $data[0][3] == 'RequestNo' && $data[0][4] == 'ProductState' &&
        $data[0][5] == 'PartnerIDCode' && $data[0][6] == 'PartnerIDCodeType' && $data[0][7] == 'PartnerName' && $data[0][8] == 'FiscSiteName' && 
        $data[0][9] == 'FiscRegDate' && $data[0][10] == 'FiscDeregDate' && $data[0][11] == 'FiscDeregReason' && $data[0][12] == 'DistrictID' && $data[0][13] == 'DistrictName' &&
        $data[0][14] == 'ProductName' && $data[0][15] == 'Subscription' && $data[0][16] == 'IsCredit' ){
    
        foreach($data as $row){
          if ($j>0) {
            $i = 0;
            $value .= '(NOW(), NULL,';
            while ($i < 17) {
              $cell = htmlentities($row[$i], ENT_QUOTES);
              if ($i == 16) {
                $value .= '\''.$cell.'\'';
              }else if($i == 9 || $i == 10){
                $datetime = dateConvert($cell);
                $value .= '\''.$datetime.'\',';
              }else{
                $value .= '\''.$cell.'\',';
              }
              $i++;
            }
            $value .= '),';
          }
          $j++;
        }
          
      }else {
        echo 'არასწორი მონაცემები ფაილში';
      }
      
    }
    $values .= substr($value, 0, -1);

    $db = new dbClass();
    $db->setQuery("INSERT INTO `ECR_registered`
                              (`datetime`, `user_id`, `FDNo`, `FMNo`, `RegNo`, `RequestNo`,
                                `ProductState`, `PartnerIDCode`, `PartnerIDCodeType`,
                                `PartnerName`, `FiscSiteName`, `FiscRegDate`, 
                                `FiscDeregDate`, `FiscDeregReason`, `DistrictID`, `DistrictName`, 
                                `ProductName`, `Subscription`, `IsCredit`)
                        VALUES $values");
    $db->execQuery();

    break;

  case 'deregistered':
    $values = '';
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    
      $j=0;
      $data = $worksheet->toArray();
    
      if($data[0][0] == 'FDNo' && $data[0][1] == 'FMNo' && $data[0][2] == 'RegNo' && $data[0][3] == 'RequestNo' && $data[0][4] == 'ProductState' &&
        $data[0][5] == 'PartnerIDCode' && $data[0][6] == 'PartnerIDCodeType' && $data[0][7] == 'PartnerName' && $data[0][8] == 'FiscSiteName' && 
        $data[0][9] == 'FiscRegDate' && $data[0][10] == 'FiscDeregDate' && $data[0][11] == 'FiscDeregReason' && $data[0][12] == 'DistrictID' && $data[0][13] == 'DistrictName' &&
        $data[0][14] == 'ProductName' && $data[0][15] == 'Subscription' && $data[0][16] == 'IsCredit' ){
    
        foreach($data as $row){
          if ($j>0) {
            $i = 0;
            $value .= '(NOW(), NULL,';
            while ($i < 17) {
              $cell = htmlentities($row[$i], ENT_QUOTES);
              if ($i == 16) {
                $value .= '\''.$cell.'\'';
              }else if($i == 9 || $i == 10){
                $datetime = dateConvert($cell);
                $value .= '\''.$datetime.'\',';
              }else{
                $value .= '\''.$cell.'\',';
              }
              $i++;
            }
            $value .= '),';
          }
          $j++;
        }
          
      }else {
        echo 'არასწორი მონაცემები ფაილში';
      }
      
    }
    $values .= substr($value, 0, -1);

    $db = new dbClass();
    $db->setQuery("INSERT INTO `ECR_deregistered`
                              (`datetime`, `user_id`, `FDNo`, `FMNo`, `RegNo`, `RequestNo`,
                              `ProductState`, `PartnerIDCode`, `PartnerIDCodeType`,
                              `PartnerName`, `FiscSiteName`, `FiscRegDate`, 
                              `FiscDeregDate`, `FiscDeregReason`, `DistrictID`, `DistrictName`, 
                              `ProductName`, `Subscription`, `IsCredit`)
                        VALUES $values");
    $db->execQuery();

    break;

  case 'sim':
    $values = '';
    
    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    
      $j=0;
      $data = $worksheet->toArray();
    
      if($data[0][0] == 'ID' && $data[0][1] == 'ICC' && $data[0][2] == 'GSM' && $data[0][3] == 'IMSI' && $data[0][4] == 'State' &&
        $data[0][5] == 'Mobile Operator' && $data[0][6] == 'Partner ID' && $data[0][7] == 'Partner Name' && $data[0][8] == 'RcvDate' && 
        $data[0][9] == 'Date Registration' && $data[0][10] == 'Subscription' && $data[0][11] == 'FDNo' && $data[0][12] == 'RequestNo' && $data[0][13] == 'IsCredit'){
    
        foreach($data as $row){
          if ($j>0) {
            $i = 0;
            $value .= '(NOW(), NULL,';
            while ($i <= 13) {
              $cell = htmlentities($row[$i], ENT_QUOTES);
              if ($i == 13) {
                $value .= '\''.$cell.'\'';
              }else if($i == 8 || $i == 9){
                $datetime = dateConvert($cell);
                $value .= '\''.$datetime.'\',';
              }else{
                $value .= '\''.$cell.'\',';
              }
              $i++;
            }
            $value .= '),';
          }
          $j++;
        }
          
      }else {
        echo 'არასწორი მონაცემები ფაილში';
      }
      
    }
    $values .= substr($value, 0, -1);

    $db = new dbClass();
    $db->setQuery("INSERT INTO `ECR_SIM` (`datetime`, `user_id`, `ecr_id`, `ICC`, `GSM`, `IMSI`, `State`,
                               `MobileOperator`, `PartnerID`, `PartnerName`, `RcvDate`, 
                               `DateRegistration`, `Subscription`, `FDNo`, `RequestNo`, `IsCredit`) 
                        VALUES $values");
    $db->execQuery();

    break;
  
  default:
    # code...
    break;
}

function dateConvert($datetime){

  $old_date = date('m/d/Y H:i');
  $old_date_timestamp = strtotime($datetime);
  $new_date = date('Y-m-d H:i:s', $old_date_timestamp); 

  return $new_date;

}

?>