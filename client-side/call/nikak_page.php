<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script>
        kendo.pdf.defineFont({
            "DejaVu Sans": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
            "DejaVu Sans|Bold": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
            "DejaVu Sans|Bold|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "DejaVu Sans|Italic": "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
            "WebComponentsIcons": "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
        });
    </script>
    <link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>


    <script type="text/javascript">
        var tablesColumns = setColumns();
        var aJaxURL = "server-side/call/nikak_page.action.php";
        // var CustomSaveURL = "server-side/view/automator.action.php";
        var file_name = '';
        var rand_file = '';
        var tab = 0;
        var fillter = '';
        var fillter_all = '';
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var SelectedItem = "";
        var currentDate = new Date().getFullYear();

        $(document).ready(function() {
            var kendo = new kendoUI();

            GetDate("start_date");
            GetDate("end_date");

            $("#cat1_id, #cat2_id,#cat3_id, #cat3_id, #region_id, #municipaliteti_id, #temi_id, #sofeli_id, #gender_id, #fermeri_id").chosen();

            $("#page1").show();
            $("#page2").hide();
            $("#page3").hide();

            $("#first").addClass("activePage"); // active 1st button 
            $(".first").show(); // show first page

            var hidden = '' // MainKendoFilter();
            LoadMainKendo(hidden);

            highCharts("get_list_for_chart", "container1");
            highCharts("get_list_for_chart2", "container2");


        });

        function setColumns() {
            // first element of array is null DONT CANGE IT
            var kendoTableData = [{
                    sql: null,
                    geo: null,
                    action: null
                },
                {
                    sql: [
                        "id:string",
                        "period:string",
                        "incomcalls:string",
                        "male:string",
                        "female:string"
                    ],
                    geo: [
                        "ID",
                        "პერიოდი",
                        "შემოსული ზარები",
                        "მამრობითი",
                        "მდედრობითი"
                    ],
                    action: "get_list"
                },
                {
                    sql: [
                        "id:string",
                        "period:string",
                        "incomcalls:string",
                        "female:string",
                        "male:string"
                    ],
                    geo: [
                        "ID",
                        "პერიოდი",
                        "გამავალი ზარები",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_2"
                },
                {
                    sql: [
                        "id:string",
                        "period:string",
                        "male:string",
                        "female:string",
                        "phone:string",
                        "email:string",
                        "sitechat:string",
                        "webcall:string",
                    ],
                    geo: [
                        "ID",
                        "პერიოდი",
                        "მამრობითი",
                        "მდედრობითი",
                        "ტელეფონი",
                        "ელ.ფოსტა",
                        "საიტის ჩატი",
                        "ვიდეო ზარი"
                    ],
                    action: "get_list_3"
                },
                {
                    sql: [
                        "uwyeba:string",
                        "females:string",
                        "males:string"
                    ],
                    geo: [
                        "უწყება",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_4"
                },
                {
                    sql: [
                        "project:string",
                        "females:string",
                        "males:string"
                    ],
                    geo: [
                        "პროექტი",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_5"
                },
                {
                    sql: [
                        "momartva:string",
                        "females:string",
                        "males:string"
                    ],
                    geo: [
                        "მომართვის ტიპი",
                        "მდედრობითი",
                        "მამრობითი"
                    ],
                    action: "get_list_6"
                }
            ];
            return kendoTableData;
        }

        function highCharts(act, container) {
            param = new Object();
            // param.start_date = $("#start_date").val();
            // param.end_date = $("#end_date").val();
            // param.cat1_id = $("#cat1_id").val();
            // param.cat2_id = $("#cat2_id").val();
            // param.cat3_id = $("#cat3_id").val();
            param.act = act;
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            alert(data.error);
                        } else {
                            var callcount = [];
                            var malecount = [];
                            var femalecount = [];
                            data['result'].forEach(myFunction);


                            function myFunction(item, index) {
                                callcount.push(Number(item.callcount));
                                malecount.push(Number(item.male))
                            }
                            Highcharts.chart(container, {
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: currentDate
                                },
                                subtitle: {
                                    text: 'წელი'
                                },
                                xAxis: {
                                    categories: callcount,
                                    crosshair: true
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Rainfall (mm)'
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y:.1f} მომართვა</b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                plotOptions: {
                                    column: {
                                        pointPadding: 0.2,
                                        borderWidth: 0
                                    }
                                },
                                series: [{
                                    name: 'შემოსული ზარები',
                                    data: callcount

                                }, {
                                    name: 'მამრობითი',
                                    data: malecount

                                }, {
                                    name: 'მდედრობითი',
                                    data: femalecount

                                }]
                            });

                            $(".highcharts-credits").remove();
                            $(".highcharts-axis-title").remove();
                        }
                    }
                }
            });
        }


        // Navigation on sub pages
        $(document).on("click", ".Page-buttons", function() {
            // hide all pages and remove all active class on buttons
            $(".Page-buttons").each(function() {
                $(this).removeClass("activePage");
                $("#page" + $(this).val()).hide();
            });

            // show selected page and add active class on selected button
            $("#page" + $(this).val()).show();
            $(this).addClass("activePage");

            var kendoTable = $("#kendo_incomming_table" + $(this).val()).attr("name");

            if ($("#" + kendoTable).is(":empty")) {
                MainKendo_incomming(kendoTable, tablesColumns[$(this).val()]);
            }
        });

        // filter kendo table one by one
        $(document).on("click", "#loadtablenew", function() {
            var ind = 0;
            // which id is actived empty and resend MainKendo_incomming
            $(".Page-buttons").each(function(index) {
                if ($(this).hasClass("activePage")) {
                    ind = index + 1;
                }
            });

            var kendoTable = $("#kendo_incomming_table" + ind).attr("name");
            $("#kendo_incomming_table" + ind).empty();
            MainKendo_incomming(kendoTable, tablesColumns[ind]);
            if (ind == 1) {
                highCharts("    ", "container1");
            } else if (ind == 2) {
                highCharts("get_list_for_chart2", "container2");
            }
        });

        $(document).on("change", "#cat1_id", function() {
            param = new Object();
            param.act = "cat_2";
            param.cat_id = $("#cat1_id").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#cat2_id").html(data.page);
                    $("#cat2_id").trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#cat2_id", function() {
            param = new Object();
            param.act = "cat_3";
            param.cat_id = $("#cat2_id").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#cat3_id").html(data.page);
                    $("#cat3_id").trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#region_id", function() {
            param = new Object();
            param.act = "municipaliteti";
            param.region_id = $("#region_id").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#municipaliteti_id").html(data.page);
                    $("#municipaliteti_id").trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#municipaliteti_id", function() {
            param = new Object();
            param.act = "temi";
            param.municip_id = $("#municipaliteti_id").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#temi_id").html(data.page);
                    $("#temi_id").trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#temi_id", function() {
            param = new Object();
            param.act = "sofeli";
            param.sofeli_id = $("#sofeli_id").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#sofeli_id").html(data.page);
                    $("#sofeli_id").trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#gender_id", function() {
            param = new Object();
            param.gender_id = $("#gender_id").val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {}
            });
        });

        $(document).on("change", "#fermeri_id", function() {
            param = new Object();
            param.fermeri_id = $("#fermeri_id").val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {}
            });
        });

        $(document).on("click", "#loadtable", function() {

            var hidden = MainKendoFilter();
            LoadMainKendo(hidden)
        });

        $(document).on("dblclick", "#base_grid tr.k-state-selected", function() {

            var grid = $("#base_grid").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());


            param = new Object();
            param.act = 'get_dialog'
            param.sub_id = dItem.id;

            $.each($(".ui-dialog"), function(i, e) {
                $(e).remove();
            })
            if (dItem.id != "") {
                $.ajax({
                    url: aJaxURL,
                    type: "POST",
                    data: param,
                    dataType: "json",
                    success: function(data) {
                        $("#get_dialog").html(data.page);


                        var buttons = {

                            done: {
                                text: "შენახვა",
                                class: "save-processing",
                                id: 'save-dialog-out-automated'
                            },
                            cancel: {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function() {
                                    $(this).dialog("close");
                                }
                            }
                        };

                        GetDialog("get_dialog", "600", "auto", buttons, 'ceneter center');

                    }
                });
            }
        });

        function LoadMainKendo(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'base_grid';
            var actions = '<button style="float:right;" id="outgoing_excel_export">ექსპორტი EXCEL</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 8;
            var columnsSQL = [
                "id:string",
                "date:date",
                "phone:string",
                "communication_type:string",
                "operator:string",
                "call_end:string",
                "queue:string",
                "extention:string"

            ];
            var columnGeoNames = [
                "ID",
                "თარიღი",
                "ტელეფონი",
                "კომუნიკაციის ტიპი",
                "ოპერატორი",
                "ზარის დასრულება",
                "რიგის ნომერი",
                "ექსთეშენი"

            ];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0, '', '', '', '', '', '', true, false, true, []);
        }

        function MainKendoFilter(pos) {

            if (pos == undefined) {
                pos = '';
            }

            var cat1_id = $("#cat1_id").val();
            var cat2_id = $("#cat2_id").val();
            var cat3_id = $("#cat3_id").val();

            var region_id = $('#region_id').val();
            var municipaliteti_id = $('#municipaliteti_id').val();
            var temi_id = $('#temi_id').val();
            var sofeli_id = $('#sofeli_id').val();
            var gender_id = $('#gender_id').val();
            var fermeri_id = $("#fermeri_id").val();
            var piradi_id = $("#piradi_id").val();

            if (cat1_id == undefined) {
                cat1_id.id = '';
            }
            if (cat2_id == undefined) {
                cat2_id.id = '';
            }
            if (cat3_id == undefined) {
                cat3_id.id = '';
            }
            if (region_id == undefined) {
                region_id.id = '';
            }
            if (municipaliteti_id == undefined) {
                municipaliteti_id.id = '';
            }
            if (temi_id == undefined) {
                temi_id.id = '';
            }
            if (sofeli_id == undefined) {
                sofeli_id.id = '';
            }
            if (gender_id == undefined) {
                gender_id.id = '';
            }
            if (piradi_id == undefined) {
                piradi_id.id = '';
            }
            if (fermeri_id == undefined) {
                fermeri_id.id = '';
            }

            var hidden = "&start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + "&cat1_id=" + cat1_id + "&cat2_id=" + cat2_id + "&cat3_id=" + cat3_id + "&region_id=" + region_id + "&municipaliteti_id=" + municipaliteti_id + "&temi_id=" + temi_id + "&sofeli_id=" + sofeli_id + "&gender_id=" + gender_id + "&fermeri_id=" + fermeri_id + "&piradi_id=" + piradi_id + pos;
            return hidden;
        }
    </script>
</head>

<body>
    <div id="tabs">

        <!-- FILTER -->
        <div class="callapp_filter_show">
            <div class="callapp_filter_body" myvar="1">
                <div style="width: 100%;">
                    <table>
                        <tr>
                            <td style="padding: 5px 0 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                            </td>
                            <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">-
                            </td>
                            <td style="padding: 5px 10px 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 84px;">
                            </td>
                            <td style="width: 200px;">
                                <select id="cat1_id" style="width: 185px;">
                                    <?php
                                    $data = '<option value="" selected>კატეგორია 1</option>';
                                    global $db;
                                    $db->setQuery("SELECT info_category.id, info_category.`name`
                                                    FROM info_category
                                                    WHERE info_category.parent_id = 0 AND info_category.actived = 1
                                                    ");

                                    $result = $db->getResultArray();

                                    foreach ($result['result'] as $key) {
                                        $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
                                    }
                                    echo $data;
                                    ?>
                                </select>
                            </td>
                            <td style="width: 212px;">
                                <select id="cat2_id" style="width: 185px;">
                                    <option value="">კატეგორია 2</option>
                                </select>
                            </td>
                            <td style="width: 212px;">
                                <select id="cat3_id" style="width: 185px;">
                                    <option value="">კატეგორია 3</option>
                                </select>
                            </td>
                            <td>
                                <div class="k-content">
                                    <button id="loadtable">ფილტრი</button>
                                </div>
                                <div class="k-content">
                                    <button id="loadtablenew">ფილტრი2</button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div>
            <table style="margin-top:50px">
                <tr>
                    <td style="width: 212px;">
                        <select id="region_id" style="width: 185px;">
                            <?php
                            $data = '<option value="">რეგიონი</option>';
                            global $db;
                            $db->setQuery("SELECT id, name
                                            FROM regions
                                            WHERE parent_id = 0 AND actived = 1
                                            ");

                            $result = $db->getResultArray();

                            foreach ($result['result'] as $key) {
                                $data .= '<option value="' . $key['id'] . '">' . $key['name'] . '</option>';
                            }
                            echo $data;
                            ?>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <select id="municipaliteti_id" style="width: 185px;">
                            <option value="">მუნიციპალიტეტი</option>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <select id="temi_id" style="width: 185px;">
                            <option value="">თემი</option>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <select id="sofeli_id" style="width: 185px;">
                            <option value="">სოფელი</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table style="margin-top:20px">
                <tr>
                    <td style="width: 212px;">
                        <select id="gender_id" style="width: 185px;">
                            <option value="">სქესი</option>
                            <option value="1">მდედრობითი</option>
                            <option value="2">მამრობითი</option>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <select id="fermeri_id" style="width: 185px;">
                            <option value="">ფერმერია?</option>
                            <option value="1">დიახ</option>
                            <option value="2">არა</option>
                        </select>
                    </td>
                    <td style="width: 212px;">
                        <input id="piradi_id" placeholder="პირადი ნომერი" maxlength="11" minlength="11" type="text" />
                    </td>
                </tr>
            </table>
        </div>

        <div class=" l_responsible_fix" style="width: calc(100% - 250px); margin-top:65px">
            <div id="base_grid"></div>
        </div>

        <div id="get_dialog" class="form-dialog" title="დიალოგი"></div>

        <!-- BUTTONS FOR PAGE SWITCH -->
        <div style="display: flex;padding: 20px">
            <button class="Page-buttons" value="1" id="first">შემომავალი ზარი</button>
            <button class="Page-buttons" value="2">გამავალი ზარი</button>
            <button class="Page-buttons" value="3">CRM</button>
        </div>

        <!-- KENDO TABLES' OUTPUTS -->
        <div class="pages first">
            <!-- PAGE 1 -->
            <div id="page1" class="sections">
                <div id="kendo_incomming_table1" name="kendo_incomming_table1" class="half"></div>

                <figure class="highcharts-figure half">
                    <div id="container1"></div>
                </figure>

            </div>

            <!-- PAGE 2 -->
            <div id="page2" class="sections">
                <div id="kendo_incomming_table2" name="kendo_incomming_table2" class="half"></div>

                <figure class="highcharts-figure half">
                    <div id="container2"></div>
                </figure>

            </div>

            <!-- PAGE 3 -->
            <div id="page3" class="sections">
                <div id="kendo_incomming_table3" name="kendo_incomming_table3"></div>
            </div>

        </div>

    </div>

    <div id="loading1">
        <p><img src="media/images/loader.gif" /></p>
    </div>
</body>
<style type="text/css">
    .pages {
        display: flex;
        justify-content: space-between;
        margin-top: 50px;
        margin-bottom: 40px;
    }

    .sections {
        display: flex;
        justify-content: center;
        width: 100%;
        min-height: 15vh;
        height: auto;
    }

    .sections>div {
        height: auto;
        border: 0 !important;
    }

    .Page-buttons {
        background-color: #EEECEC;
        transform: skew(-18deg, 0deg);
        margin-right: 2%;
        letter-spacing: 1px;
    }

    .half {
        margin-top: 220px;
        position: relative;
    }


    #container,
    #container2 {
        width: 500px;
        height: 400px;
        margin: 0px !important;
    }


    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    .activePage {
        background: #fef400 !important;
    }

    #loading1 {
        z-index: 999;
        top: 45%;
        left: 45%;
        position: absolute;
        display: none;
        padding-top: 15px;
    }

    #piradi_id {
        width: 190px;
        height: 26px;
    }

    #get_dialog>fieldset {
        display: flex;

    }
</style>

</html>