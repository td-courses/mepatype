<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

include('../../includes/classes/wsdl.class.php');

$action = $_REQUEST['act'];
$start = $_REQUEST['start'];
$end = $_REQUEST['end'];
$error	= '';
$data	= array();
$user_id	             = $_SESSION['USERID'];
//incomming
$incom_id			 = $_REQUEST['id'];
$call_date			 = $_REQUEST['call_date'];
$hidden_user		 = $_REQUEST['hidden_user'];
$hidden_inc			 = $_REQUEST['hidden_inc'];
$imchat			     = $_REQUEST['imchat'];
$chat_id             = $_REQUEST['chat_id'];
$ipaddres            = $_REQUEST['ipaddres'];
$ii			         = $_REQUEST['ii'];
$phone				 = $_REQUEST['phone'];
// file
$rand_file			= $_REQUEST['rand_file'];
$file				= $_REQUEST['file_name'];

switch ($action) {
    case 'get_answered_calls':
        $page		= GetPage();
		$data		= array('page'	=> $page);
    break;
    case 'get_noanswer_calls':
        $page		= GetPageNoanswer();
		$data		= array('page'	=> $page);
    break;
    case 'get_missed_calls':
        $page		= GetPageMissed();
		$data		= array('page'	=> $page);
    break;
    case 'get_noanswer_missed_calls':
        $page		= GetPageNoanswerMissed();
		$data		= array('page'	=> $page);
    break;
    case 'get_missed_det_calls':
        $page		= GetPageMissedDet();
		$data		= array('page'	=> $page);
    break;
    case 'get_nonwork_calls':
        $page		= GetPageNonwork();
		$data		= array('page'	=> $page);
    break;
    case 'get_transfer_calls':
        $page		= GetPageTransfer();
		$data		= array('page'	=> $page);
    break;
    case 'get_audio_calls':
        $id = $_REQUEST['call_log'];
        $page		= GetPageAudio(getAudioFile($id));
		$data		= array('page'	=> $page);
    break;
    case 'get_operator_left':
        $page		= GetPageOperatorLeft();
		$data		= array('page'	=> $page);
    break;

    case 'get_answered_chats':
        $page		= GetPageAnsChats();
		$data		= array('page'	=> $page);
    break;
    case 'get_answered_fb':
        $page		= GetPageAnsFB();
		$data		= array('page'	=> $page);
    break;
    case 'get_answered_mail':
        $page		= GetPageAnsMail();
		$data		= array('page'	=> $page);
    break;
    case 'get_answered_viber':
        $page		= GetPageAnsViber();
		$data		= array('page'	=> $page);
    break;
    case 'transfer':
        $operator = $_REQUEST['operator'];
        if($operator==0)
        {
            $db->setQuery("SELECT A.id AS 'id',
                                    FROM_UNIXTIME(A.call_datetime) AS 'datetime',
                                    A.source AS 'phone',
                                    IF(ISNULL(B.user_id),ext_1.number,oper_1.name) AS 'operator_1',
                                    IF(ISNULL(A.user_id),ext_2.number,oper_2.name) AS 'operator_2',
                                    (CASE
                                        WHEN A.call_status_id = 6 OR A.call_status_id = 7 OR A.call_status_id = 8 THEN 'ნაპასუხები'
                                        WHEN A.call_status_id = 3 OR A.call_status_id = 4 OR A.call_status_id = 5 THEN 'მიმდინარე'
                                        ELSE 'უპასუხო'
                                    END) AS 'call_status',
                                    SEC_TO_TIME(B.talk_time) AS 'talk_time_1',
                                    SEC_TO_TIME(A.wait_time) AS 'wait_time',
                                    SEC_TO_TIME(A.talk_time) AS 'talk_time_2',
                                    SEC_TO_TIME(B.talk_time+A.wait_time+A.talk_time) AS 'total_time'

                            FROM asterisk_call_log A
                            LEFT JOIN asterisk_call_log B ON B.id=A.transfered

                            LEFT JOIN user_info oper_1 ON oper_1.user_id=B.user_id
                            LEFT JOIN user_info oper_2 ON oper_2.user_id=A.user_id

                            LEFT JOIN asterisk_extension ext_1 ON ext_1.id = B.extension_id
                            LEFT JOIN asterisk_extension ext_2 ON ext_2.id = A.extension_id


                            WHERE A.transfered!=0  AND A.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')");
        }
        else
        {
            $db->setQuery("SELECT A.id AS 'id',
                                    FROM_UNIXTIME(A.call_datetime) AS 'datetime',
                                    A.source AS 'phone',
                                    IF(ISNULL(B.user_id),ext_1.number,oper_1.name) AS 'operator_1',
                                    IF(ISNULL(A.user_id),ext_2.number,oper_2.name) AS 'operator_2',
                                    (CASE
                                        WHEN A.call_status_id = 6 OR A.call_status_id = 7 OR A.call_status_id = 8 THEN 'ნაპასუხები'
                                        WHEN A.call_status_id = 3 OR A.call_status_id = 4 OR A.call_status_id = 5 THEN 'მიმდინარე'
                                        ELSE 'უპასუხო'
                                    END) AS 'call_status',
                                    SEC_TO_TIME(B.talk_time) AS 'talk_time_1',
                                    SEC_TO_TIME(A.wait_time) AS 'wait_time',
                                    SEC_TO_TIME(A.talk_time) AS 'talk_time_2',
                                    SEC_TO_TIME(B.talk_time+A.wait_time+A.talk_time) AS 'total_time'

                            FROM asterisk_call_log A
                            LEFT JOIN asterisk_call_log B ON B.id=A.transfered

                            LEFT JOIN user_info oper_1 ON oper_1.user_id=B.user_id
                            LEFT JOIN user_info oper_2 ON oper_2.user_id=A.user_id

                            LEFT JOIN asterisk_extension ext_1 ON ext_1.id = B.extension_id
                            LEFT JOIN asterisk_extension ext_2 ON ext_2.id = A.extension_id


                            WHERE A.transfered!=0 AND A.user_id='$operator' AND A.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')");
        }
        
        
        
        $data = $db->getList(10,$hidden,0);
    break;
    case 'nonwork':
        
        $db->setQuery("SELECT asterisk_call_log.id AS 'id',
                                FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'datetime',
                                asterisk_call_log.source AS 'phone'

                        FROM asterisk_call_log

                        WHERE asterisk_call_log.call_status_id=11 and asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')");
        
        
        $data = $db->getList(5,$hidden,0);
    break;
    case 'missed_det':
        $operator = $_REQUEST['operator'];
        $call_id = $_REQUEST['call_id'];
        $link = 'href="#" class="report_report_missed_det"';
        
        $db->setQuery("SELECT asterisk_call_missed.id AS 'id',
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'datetime',
                                    user_info.name,
                                    asterisk_call_log.source AS 'phone',
                                    1 AS 'qty'

                        FROM asterisk_call_missed
                        LEFT JOIN asterisk_call_log ON asterisk_call_log.id=asterisk_call_missed.asterisk_call_log_id
                        LEFT JOIN user_info ON user_info.user_id=asterisk_call_missed.user_id
                        WHERE asterisk_call_missed.user_id='$operator' AND asterisk_call_log.id='$call_id'");
        
        
        $data = $db->getList(5,$hidden,0);
    break;
    case 'get_missed':
        $operator = $_REQUEST['operator'];
        $link = 'href="#" class="report_report_missed_det"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_missed.id AS 'id',
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'datetime',
                                    user_info.name,
                                    asterisk_call_log.source AS 'phone',
                                    CONCAT('<a $link data-call-id=',asterisk_call_log.id,' data-operator=',asterisk_call_missed.user_id,'>',SUM(1),'</a>') AS 'qty'

                            FROM asterisk_call_missed
                            LEFT JOIN asterisk_call_log ON asterisk_call_log.id=asterisk_call_missed.asterisk_call_log_id
                            LEFT JOIN user_info ON user_info.user_id=asterisk_call_missed.user_id
                            WHERE asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')
                            GROUP BY asterisk_call_log.id, asterisk_call_missed.user_id");
        }
        else
        {
            $db->setQuery("SELECT asterisk_call_missed.id AS 'id',
                                        FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'datetime',
                                        user_info.name,
                                        asterisk_call_log.source AS 'phone',
                                        CONCAT('<a $link data-call-id=',asterisk_call_log.id,' data-operator=',asterisk_call_missed.user_id,'>',SUM(1),'</a>') AS 'qty'

                            FROM asterisk_call_missed
                            LEFT JOIN asterisk_call_log ON asterisk_call_log.id=asterisk_call_missed.asterisk_call_log_id
                            LEFT JOIN user_info ON user_info.user_id=asterisk_call_missed.user_id
                            WHERE asterisk_call_missed.user_id='$operator' AND asterisk_call_missed.datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')
                            GROUP BY asterisk_call_log.id, asterisk_call_missed.user_id");
        }
        
        $data = $db->getList(5,$hidden,0);
    break;
    case 'noanswer_missed':
        $operator = $_REQUEST['operator'];
        $missed_noanswer = 'href="#" class="report_noanswer_missed"';
        $db->setQuery("SELECT   asterisk_call_missed.id AS 'id',
                                FROM_UNIXTIME(asterisk_call_missed.datetime) AS 'datetime',
                                CONCAT(user_info.name,' (',asterisk_extension.number,')') AS 'operator',
                                asterisk_call_log.source AS 'phone',
                                1 AS 'count'
                        FROM asterisk_call_missed

                        LEFT JOIN user_info ON user_info.user_id=asterisk_call_missed.user_id
                        LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_missed.extension_id
                        LEFT JOIN asterisk_call_log ON asterisk_call_log.id=asterisk_call_missed.asterisk_call_log_id
                        WHERE asterisk_call_missed.asterisk_call_log_id='$operator'");
        $data = $db->getList(7,$hidden,0);
    break;
    case 'get_no_answered_list':
        $operator = $_REQUEST['operator'];
        $missed_noanswer = 'href="#" class="report_noanswer_missed"';
        $db->setQuery("SELECT asterisk_call_log.id,
                                FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'datetime',
                                asterisk_call_log.source AS 'mobile',
                                SEC_TO_TIME(asterisk_call_log.wait_time) AS 'wait_time',
                                CONCAT('<a $missed_noanswer data-id=',asterisk_call_log.id,'>',SUM(IF(asterisk_call_missed.asterisk_call_log_id=asterisk_call_log.id,1,0)),'</a>') AS 'missed_cnt'

                        FROM asterisk_call_log
                        LEFT JOIN asterisk_call_missed ON asterisk_call_missed.asterisk_call_log_id = asterisk_call_log.id
                        WHERE asterisk_call_log.call_status_id=9 AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')

                        GROUP BY asterisk_call_log.id");
        $data = $db->getList(7,$hidden,0);
    break;

    case 'get_answered_list':
        $operator = $_REQUEST['operator'];
        $audio = 'href="#" class="report_audio"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')");
        }
		else if($operator == 'uc')
		{
			$db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') AND ISNULL(user_info.name) AND ISNULL(asterisk_extension.name)");
		}
        else
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE asterisk_call_log.user_id='$operator' AND (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')");
        }
        $data = $db->getList(7,$hidden,0);
        //$data = $db->getResultArray();
    break;
    case 'get_answered_list_chat':
        $operator = $_REQUEST['operator'];
        $audio = 'href="#" class="report_audio"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8)");
        }
        else
        {
            $db->setQuery("SELECT chat.id AS 'id',
                                        chat.join_date AS 'join_date',
                                        user_info.name AS 'operator',
                                        chat.name AS 'client_name',
                                        chat.last_request_datetime AS 'last_action',
                                        CONCAT('<a class=clickmetostart data-source=chat chat_id=',chat.id,'>ჩატის ნახვა</a>') AS 'action'

                            FROM chat
                            LEFT JOIN user_info ON user_info.user_id=chat.last_user_id
                            WHERE chat.last_user_id='$operator' AND (chat.`status`=2 OR chat.`status`=3  OR chat.`status`=4) AND chat.join_date BETWEEN '$start' AND '$end'");
        }
        
        $data = $db->getList(6,$hidden,0);
    break;
    case 'get_answered_list_fb':
        $operator = $_REQUEST['operator'];
        $audio = 'href="#" class="report_audio"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8)");
        }
        else
        {
            $db->setQuery("SELECT fb_chat.id AS 'id',
                                        fb_chat.first_datetime AS 'join_date',
                                        fb_chat.last_datetime AS 'last_date',
                                        user_info.name AS 'operator',
                                        fb_chat.sender_name AS 'client',
                                        fb_account.`name` AS 'საიტი',
                                        SEC_TO_TIME(UNIX_TIMESTAMP(fb_chat.last_datetime) - UNIX_TIMESTAMP(fb_chat.first_datetime)) AS 'time',
                                        CONCAT('<a class=clickmetostart data-source=fbm chat_id=',fb_chat.id,'>ჩატის ნახვა</a>') AS 'action'

                            FROM fb_chat
                            LEFT JOIN user_info ON user_info.user_id=fb_chat.last_user_id
                            LEFT JOIN fb_account ON fb_account.id=fb_chat.account_id

                            WHERE fb_chat.last_user_id='$operator' AND (fb_chat.`status`=2 OR fb_chat.`status`=3) AND fb_chat.first_datetime BETWEEN '$start' AND '$end'");
        }
        
        $data = $db->getList(8,$hidden,0);
    break;
    case 'get_answered_list_mail':
        $operator = $_REQUEST['operator'];
        $audio = 'href="#" class="report_audio"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8)");
        }
        else
        {
            $db->setQuery("SELECT mail_chat.id AS 'id',
                                        mail_chat.first_datetime AS 'join_date',
                                        mail_chat.last_datetime AS 'last_date',
                                        user_info.name AS 'operator',
                                        mail_chat.sender_name AS 'client',
                                        mail_account.`name` AS 'საიტი',
                                        SEC_TO_TIME(UNIX_TIMESTAMP(mail_chat.last_datetime) - UNIX_TIMESTAMP(mail_chat.first_datetime)) AS 'time',
                                        CONCAT('<a class=clickmetostart data-source=fbm chat_id=',mail_chat.id,'>ჩატის ნახვა</a>') AS 'action'

                            FROM mail_chat
                            LEFT JOIN user_info ON user_info.user_id=mail_chat.last_user_id
                            LEFT JOIN mail_account ON mail_account.id=mail_chat.account_id

                            WHERE mail_chat.last_user_id='$operator' AND (mail_chat.`status`=2 OR mail_chat.`status`=3) AND mail_chat.first_datetime BETWEEN '$start' AND '$end'");
        }
        
        $data = $db->getList(8,$hidden,0);
    break;
    case 'get_answered_list_viber':
        $operator = $_REQUEST['operator'];
        $audio = 'href="#" class="report_audio"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8)");
        }
        else
        {
            $db->setQuery("SELECT viber_chat.id AS 'id',
                                        viber_chat.first_datetime AS 'join_date',
                                        viber_chat.last_datetime AS 'last_date',
                                        user_info.name AS 'operator',
                                        viber_chat.sender_name AS 'client',
                                        viber_account.`name` AS 'საიტი',
                                        SEC_TO_TIME(UNIX_TIMESTAMP(viber_chat.last_datetime) - UNIX_TIMESTAMP(viber_chat.first_datetime)) AS 'time',
                                        CONCAT('<a class=clickmetostart data-source=fbm chat_id=',viber_chat.id,'>ჩატის ნახვა</a>') AS 'action'

                            FROM viber_chat
                            LEFT JOIN user_info ON user_info.user_id=viber_chat.last_user_id
                            LEFT JOIN viber_account ON viber_account.id=viber_chat.account_id

                            WHERE viber_chat.last_user_id='$operator' AND (viber_chat.`status`=2 OR viber_chat.`status`=3)");
        }
        
        $data = $db->getList(8,$hidden,0);
    break;
    case 'operator_left':
        $operator = $_REQUEST['operator'];
        $audio = 'href="#" class="report_audio"';
        if($operator == 0)
        {
            $db->setQuery("SELECT asterisk_call_log.id,
                                    FROM_UNIXTIME(asterisk_call_log.call_datetime),
                                    asterisk_call_log.source,
                                    user_info.name,
                                    asterisk_extension.number,
                                    SEC_TO_TIME(asterisk_call_log.talk_time),
                                    CONCAT('<a $audio data-audio=',asterisk_call_log.id,'>მოსმენა</a>') AS 'action'

                            FROM asterisk_call_log
                            LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                            WHERE (asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8)");
        }
        else
        {
            $db->setQuery("SELECT chat.id AS 'id',
                                        chat.join_date AS 'join_date',
                                        user_info.name AS 'operator',
                                        chat.name AS 'client_name',
                                        chat.last_request_datetime AS 'last_action',
                                        CONCAT('<a class=clickmetostart data-source=chat chat_id=',chat.id,'>ჩატის ნახვა</a>') AS 'action'

                            FROM chat
                            LEFT JOIN user_info ON user_info.user_id=chat.last_user_id
                            WHERE chat.last_user_id='$operator' AND chat.`status`=4 AND chat.join_date BETWEEN '$start' AND '$end'");
        }
        
        $data = $db->getList(6,$hidden,0);
    break;
    case 'get_list':
        $type = $_REQUEST['type'];
        $r = rand(11111,99999);
        $r2 = $r+1;
        if($type == 'call')
        {
            $answers = 'href="#" class="report_answer"';
            $noanswer = 'href="#" class="report_noanswer"';
            $missed = 'href="#" class="report_missed"';
            $nonwork = 'href="#" class="nonwork"';
            $transfer = 'href="#" class="transfer"';
            $db->setQuery("SELECT 0 AS 'id',
                                    'საერთო' AS 'operator',
                                    SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8 OR asterisk_call_log.call_status_id=9 OR asterisk_call_log.call_status_id=12,1,0)) AS 'phone',
                                    CONCAT('<a $answers data-operator=0>',SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,1,0)),'</a>') AS 'answered',
                                    CONCAT(ROUND(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,1,0))*100/(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8 OR asterisk_call_log.call_status_id=9 OR asterisk_call_log.call_status_id=12,1,0))),1),'%') AS '%',
                                    CONCAT('<a $noanswer data-operator=0>',SUM(IF(asterisk_call_log.call_status_id=9,1,0)),'</a>') AS 'no_answer',
                                    CONCAT(ROUND(SUM(IF(asterisk_call_log.call_status_id=9 OR asterisk_call_log.call_status_id=12,1,0))*100/(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8 OR asterisk_call_log.call_status_id=9 OR asterisk_call_log.call_status_id=12,1,0))),1),'%') AS '%',
                                    
                                    CONCAT('<a $missed data-operator=0>',SUM(IF(NOT ISNULL(missed_calls.id),1,0)),'</a>') AS 'missed',
                                    CONCAT(ROUND((SELECT COUNT(DISTINCT asterisk_call_log_id) FROM asterisk_call_missed WHERE DATE(FROM_UNIXTIME(datetime)) BETWEEN '$start' AND '$end')*100/(SELECT COUNT(*) FROM asterisk_call_missed WHERE DATE(FROM_UNIXTIME(datetime)) BETWEEN '2020-05-04 00:00:00' AND '2020-5-7 23:59:59'),1),'%') AS '%',
                                    CONCAT('<a $transfer data-operator=0>',SUM(IF(asterisk_call_log.transfered!=0,1,0)),'</a>') AS 'transfered',
                                    CONCAT(ROUND(SUM(IF(asterisk_call_log.transfered!=0,1,0))*100/(SUM(1)+SUM(IF(NOT ISNULL(missed_calls.id),1,0))),1),'%') AS '%',
                                    SUM(IF(asterisk_call_log.call_status_id=9,1,0)) AS 'caller_off',
                                    '0%' AS '%',
                                    0 AS 'operator_off',
                                    '0%' AS '%',
                                
                                    0 AS 'system_off',
                                    '0%' AS '%',
                                    0 AS 'system_error',
                                    '0%' AS '%',
                                    CONCAT('<a $nonwork>',SUM(IF(asterisk_call_log.call_status_id=11,1,0)),'</a>') AS 'nonwork_reject',
                                    SEC_TO_TIME(ROUND(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,asterisk_call_log.talk_time,0))/(SELECT COUNT(*) FROM asterisk_call_log WHERE (call_status_id=6 OR call_status_id=7 OR call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')))) AS 'AVG_TALK',
                                                SEC_TO_TIME(ROUND(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,asterisk_call_log.wait_time,0))/(SELECT COUNT(*) FROM asterisk_call_log WHERE (call_status_id=6 OR call_status_id=7 OR call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')))) AS 'AVG_WAIT'
                            FROM asterisk_call_log 
                            
                            LEFT JOIN (SELECT id,datetime,asterisk_call_log_id FROM asterisk_call_missed GROUP BY asterisk_call_log_id ) AS missed_calls ON missed_calls.asterisk_call_log_id = asterisk_call_log.id
                            WHERE asterisk_call_log.call_type_id = 1 AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')
                            UNION ALL
							
							
                            SELECT              IF(ISNULL(asterisk_call_log.user_id),IF(ISNULL(asterisk_extension.name),$r2,$r),asterisk_call_log.user_id) AS 'id',
                                                IF(ISNULL(user_info.name),IF(ISNULL(asterisk_extension.name),'უცნობი',asterisk_extension.name),user_info.name) AS 'operator',
                                                '' AS 'phone',
                                                CONCAT('<a $answers data-operator=',IF(ISNULL(user_info.name),'uc',asterisk_call_log.user_id),'>',SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,1,0)),'</a>') AS 'answered',
                                                
                                                CONCAT(ROUND((SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,1,0))*100)/(SELECT COUNT(*) FROM asterisk_call_log WHERE (call_status_id=6 OR call_status_id=7 OR call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')),1),'%') AS '%',
                                                
                                                '' AS 'no_answer',
                                                
                                                '' AS '%',
                                                
                                                CONCAT('<a $missed data-operator=',asterisk_call_log.user_id,'>',(SELECT COUNT(DISTINCT asterisk_call_log_id) FROM asterisk_call_missed WHERE user_id=asterisk_call_log.user_id AND asterisk_call_missed.datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')),'</a>') AS 'missed',
                                                CONCAT(ROUND(((SELECT COUNT(DISTINCT asterisk_call_log_id) FROM asterisk_call_missed WHERE user_id=asterisk_call_log.user_id AND asterisk_call_missed.datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end'))*100)/(SELECT COUNT(*) FROM asterisk_call_missed WHERE asterisk_call_missed.datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')),1),'%') AS '%',
                                                
                                                CONCAT('<a $transfer data-operator=',asterisk_call_log.user_id,'>',SUM(IF(asterisk_call_log.transfered!=0,1,0)),'</a>') AS 'transfered',
                                                '0%' AS '%',
                                                
                                                0 AS 'caller_off',
                                                '0%' AS '%',
                                                
                                                0 AS 'operator_off',
                                                '0%' AS '%',
                                                
                                                0 AS 'system_off',
                                                '0%' AS '%',
                                
                                                0 AS 'system_error',
                                                '0%' AS '%',
                                                '' AS 'nonwork_reject',
                                                SEC_TO_TIME(ROUND(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,asterisk_call_log.talk_time,0))/(SELECT COUNT(*) FROM asterisk_call_log WHERE (call_status_id=6 OR call_status_id=7 OR call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')))) AS 'AVG_TALK',
                                                SEC_TO_TIME(ROUND(SUM(IF(asterisk_call_log.call_status_id=6 OR asterisk_call_log.call_status_id=7 OR asterisk_call_log.call_status_id=8,asterisk_call_log.wait_time,0))/(SELECT COUNT(*) FROM asterisk_call_log WHERE (call_status_id=6 OR call_status_id=7 OR call_status_id=8) AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end')))) AS 'AVG_WAIT'
                                                
    
                                        FROM asterisk_call_log
    
                                        LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                                        LEFT JOIN (SELECT id,datetime,asterisk_call_log_id FROM asterisk_call_missed GROUP BY asterisk_call_log_id ) AS missed_calls ON missed_calls.asterisk_call_log_id = asterisk_call_log.id
                                        LEFT JOIN asterisk_extension ON asterisk_extension.id=asterisk_call_log.extension_id
                                        WHERE asterisk_call_log.call_type_id = 1 AND FROM_UNIXTIME(asterisk_call_log.call_datetime) BETWEEN '$start' AND '$end'
    
                                        GROUP BY IF(ISNULL(user_info.name),asterisk_extension.name,user_info.name)");
    
            
    
            $data = $db->getList(22,$hidden,0);
        }
        if($type == 'chat')
        {
            $answers = 'href="#" class="report_answer"';
            $noanswer = 'href="#" class="report_noanswer"';
            $operator_left = 'href="#" class="report_operator_left"';
            $db->setQuery("SELECT 0 AS 'id',
                                        'საერთო' AS 'operator',
                                        SUM(1) AS 'income_chats',
                                        SUM(IF(chat.`status`=2 OR chat.`status`=3  OR chat.`status`=4,1,0)) AS 'answered_chats',
                                        '0%' AS '%',
                                        SUM(IF(chat.`status`=1,1,0)) AS 'unanswered_chats',
                                        '0%' AS '%',
                                        SUM(IF(chat.`status`=3,1,0)) AS 'user_left',
                                        '0%' AS '%',
                                        SUM(IF(chat.`status`=4,1,0)) AS 'operator_left',
                                        '0%' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date))/COUNT(*))) AS 'AVG_TALK_TIME'

                            FROM chat

                            WHERE NOT ISNULL(chat.last_user_id) AND chat.`status`!=12 AND chat.join_date BETWEEN '$start' AND '$end'
                            UNION ALL
                            SELECT chat.id AS 'id',
                                        user_info.name,
                                        '' AS 'income_chats',
                                        CONCAT('<a $answers data-operator=',chat.last_user_id,'>',SUM(IF(chat.`status`=2 OR chat.`status`=3  OR chat.`status`=4,1,0)),'</a>') AS 'answered_chats',
                                        CONCAT(ROUND((SUM(IF(chat.`status`=2 OR chat.`status`=3  OR chat.`status`=4,1,0))*100)/(SELECT COUNT(*) FROM chat WHERE (chat.`status`=2 OR chat.`status`=3  OR chat.`status`=4) AND chat.join_date BETWEEN '$start' AND '$end'),1),'%') AS '%',
                                        
                                        '' AS 'unanswered_chats',
                                        '' AS '%',
                                        
                                        SUM(IF(chat.`status`=3,1,0)) AS 'user_left',
                                        CONCAT(ROUND((SUM(IF(chat.`status`=3,1,0))*100)/(SELECT COUNT(*) FROM chat WHERE chat.`status`=3 AND chat.join_date BETWEEN '$start' AND '$end'),1),'%') AS '%',
                                        
                                        CONCAT('<a $operator_left data-operator=',chat.last_user_id,'>',SUM(IF(chat.`status`=4,1,0)),'</a>') AS 'operator_left',
                                        CONCAT(ROUND((SUM(IF(chat.`status`=4,1,0))*100)/(SELECT COUNT(*) FROM chat WHERE chat.`status`=4 AND chat.join_date BETWEEN '$start' AND '$end'),1),'%') AS '%',
                                        
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date))/COUNT(*))) AS 'AVG_TALK_TIME'

                            FROM chat
                            LEFT JOIN user_info ON user_info.user_id=chat.last_user_id


                            WHERE chat.last_user_id!='' AND chat.join_date BETWEEN '$start' AND '$end'
                            GROUP BY chat.last_user_id");
    
            
    
            $data = $db->getList(22,$hidden,0);
        }
        if($type == 'fb')
        {
            $answers = 'href="#" class="report_answer"';
            $noanswer = 'href="#" class="report_noanswer"';
            $operator_left = 'href="#" class="report_operator_left"';
            $db->setQuery("SELECT 0 AS 'id',
                                        'საერთო' AS 'operator',
                                        COUNT(*) AS 'income_fb',
                                        SUM(IF(fb_chat.status=2 OR fb_chat.status=3,1,0)) AS 'answered',
                                        '0%' AS '%',
                                        SUM(IF(fb_chat.status=1,1,0)) AS 'unanswered',
                                        '0%' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(fb_chat.last_datetime) - UNIX_TIMESTAMP(fb_chat.first_datetime))/COUNT(*))) AS 'AVG_TIME'

                            FROM fb_chat
                            WHERE fb_chat.first_datetime BETWEEN '$start' AND '$end'
                            
                            UNION ALL
                            SELECT user_info.user_id AS 'id',
                                        user_info.name AS 'operator',
                                        '' AS 'income_fb',
                                        
                                        CONCAT('<a $answers data-operator=',user_info.user_id,'>',SUM(IF(fb_chat.status=2 OR fb_chat.status=3,1,0)),'</a>') AS 'answered',
                                        CONCAT(ROUND((SUM(IF(fb_chat.status=2 OR fb_chat.status=3,1,0))*100)/(SELECT COUNT(*) FROM fb_chat WHERE (fb_chat.status=2 OR fb_chat.status=3) AND fb_chat.first_datetime BETWEEN '$start' AND '$end'),1),'%') AS '%',
                                        
                                        '' AS 'unanswered',
                                        '' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(fb_chat.last_datetime) - UNIX_TIMESTAMP(fb_chat.first_datetime))/COUNT(*))) AS 'AVG_TIME'

                            FROM fb_chat
                            LEFT JOIN user_info ON user_info.user_id=fb_chat.last_user_id

                            WHERE fb_chat.last_user_id!='' AND fb_chat.first_datetime BETWEEN '$start' AND '$end'
                            GROUP BY fb_chat.last_user_id ");
    
            
    
            $data = $db->getList(22,$hidden,0);
        }
        if($type == 'mail')
        {
            $answers = 'href="#" class="report_answer"';
            $noanswer = 'href="#" class="report_noanswer"';
            $operator_left = 'href="#" class="report_operator_left"';
            $db->setQuery("SELECT 0 AS 'id',
                                        'საერთო' AS 'operator',
                                        COUNT(*) AS 'income_fb',
                                        SUM(IF(mail_chat.status=2 OR mail_chat.status=3,1,0)) AS 'answered',
                                        '0%' AS '%',
                                        SUM(IF(mail_chat.status=1,1,0)) AS 'unanswered',
                                        '0%' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(mail_chat.last_datetime) - UNIX_TIMESTAMP(mail_chat.first_datetime))/COUNT(*))) AS 'AVG_TIME'

                            FROM mail_chat
                            WHERE mail_chat.first_datetime BETWEEN '$start' AND '$end'
                            UNION ALL
                            SELECT user_info.user_id AS 'id',
                                        user_info.name AS 'operator',
                                        '' AS 'income_fb',
                                        
                                        CONCAT('<a $answers data-operator=',user_info.user_id,'>',SUM(IF(mail_chat.status=2 OR mail_chat.status=3,1,0)),'</a>') AS 'answered',
                                        CONCAT(ROUND((SUM(IF(mail_chat.status=2 OR mail_chat.status=3,1,0))*100)/(SELECT COUNT(*) FROM mail_chat WHERE mail_chat.status=2 OR mail_chat.status=3),1),'%') AS '%',
                                        
                                        '' AS 'unanswered',
                                        '' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(mail_chat.last_datetime) - UNIX_TIMESTAMP(mail_chat.first_datetime))/COUNT(*))) AS 'AVG_TIME'

                            FROM mail_chat
                            LEFT JOIN user_info ON user_info.user_id=mail_chat.last_user_id

                            WHERE mail_chat.last_user_id!='' AND mail_chat.first_datetime BETWEEN '$start' AND '$end'

                            
                            GROUP BY mail_chat.last_user_id ");
    
            
    
            $data = $db->getList(22,$hidden,0);
        }
        if($type == 'viber')
        {
            $answers = 'href="#" class="report_answer"';
            $noanswer = 'href="#" class="report_noanswer"';
            $operator_left = 'href="#" class="report_operator_left"';
            $db->setQuery("SELECT 0 AS 'id',
                                        'საერთო' AS 'operator',
                                        COUNT(*) AS 'income_fb',
                                        SUM(IF(viber_chat.status=2 OR viber_chat.status=3,1,0)) AS 'answered',
                                        '0%' AS '%',
                                        SUM(IF(viber_chat.status=1,1,0)) AS 'unanswered',
                                        '0%' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(viber_chat.last_datetime) - UNIX_TIMESTAMP(viber_chat.first_datetime))/COUNT(*))) AS 'AVG_TIME'

                            FROM viber_chat
                            
                            UNION ALL
                            SELECT user_info.user_id AS 'id',
                                        user_info.name AS 'operator',
                                        '' AS 'income_fb',
                                        
                                        CONCAT('<a $answers data-operator=',user_info.user_id,'>',SUM(IF(viber_chat.status=2 OR viber_chat.status=3,1,0)),'</a>') AS 'answered',
                                        CONCAT(ROUND((SUM(IF(viber_chat.status=2 OR viber_chat.status=3,1,0))*100)/(SELECT COUNT(*) FROM viber_chat WHERE viber_chat.status=2 OR viber_chat.status=3),1),'%') AS '%',
                                        
                                        '' AS 'unanswered',
                                        '' AS '%',
                                        SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(viber_chat.last_datetime) - UNIX_TIMESTAMP(viber_chat.first_datetime))/COUNT(*))) AS 'AVG_TIME'

                            FROM viber_chat
                            LEFT JOIN user_info ON user_info.user_id=viber_chat.last_user_id

                            WHERE viber_chat.last_user_id!=''

                            
                            GROUP BY viber_chat.last_user_id ");
    
            
    
            $data = $db->getList(22,$hidden,0);
        }
    break;
	    
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;
echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/

function getAudioFile($call_log_id)
{
    global $db;
    $db->setQuery("SELECT asterisk_call_record.name AS 'name',
                          asterisk_call_record.format AS 'format',
                          DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/') AS 'datetime'
                   FROM asterisk_call_record
                   LEFT JOIN asterisk_call_log ON asterisk_call_log.id=asterisk_call_record.asterisk_call_log_id
                   WHERE asterisk_call_log_id='$call_log_id'");
    $res2 = $db->getResultArray();
    return $res2;
}

function GetPage($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table2" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 20%;">თარიღი</th>
                            <th style="width: 15%;">წყარო</th>
                            <th style="width: 15%;">ადრესატი</th>
                            <th style="width: 15%;">ექსთენშენი</th>
                            <th style="width: 15%;">დრო</th>
                            <th style="width: 20%;">ქმედება</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}


function GetPageNoanswer($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table3" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 30%;">თარიღი და დრო</th>
                            <th style="width: 25%;">ტელეფონი</th>
                            <th style="width: 22.5%;">ლოდინის ხ-ბა</th>
                            <th style="width: 22.5%;">გამოტ. რ-ბა</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}

function GetPageMissed($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table4" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 30%;">თარიღი</th>
                            <th style="width: 25%;">ოპერატორი (შიდა ნომერი)</th>
                            <th style="width: 30%;">ტელეფონი</th>
                            <th style="width: 15%;">რ-ბა</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageNoanswerMissed($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table5" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 30%;">თარიღი და დრო</th>
                            <th style="width: 25%;">ოპერატორი (შიდა ნომერი)</th>
                            <th style="width: 30%;">ტელეფონი</th>
                            <th style="width: 15%;">რ-ბა</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageMissedDet($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table6" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 30%;">თარიღი და დრო</th>
                            <th style="width: 25%;">ოპერატორი (შიდა ნომერი)</th>
                            <th style="width: 30%;">ტელეფონი</th>
                            <th style="width: 15%;">რ-ბა</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageNonwork($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table7" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 70%;">თარიღი და დრო</th>
                            <th style="width: 30%;">ტელეფონი</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageTransfer($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table8" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 15%;">თარიღი და დრო</th>
                            <th style="width: 15%;">ტელეფონი</th>
                            <th style="width: 10%;">ოპერატორი/th>
                            <th style="width: 10%;">გადამისამართდა</th>
                            <th style="width: 10%;">ზარის სტატუსი</th>
                            <th style="width: 10%;">ხან-ბა 1</th>
                            <th style="width: 10%;">ლოდინის ხ-ბა</th>
                            <th style="width: 10%;">ხან-ბა 2</th>
                            <th style="width: 10%;">სულ ხ-ბა</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>      
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                                                 
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageOperatorLeft($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table9" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 25%;">თარიღი და დრო</th>
                            <th style="width: 15%;">ოპერატორი</th>
                            <th style="width: 15%;">მომხმარებელი/th>
                            <th style="width: 25%;">ბოლო მოწერის თარიღი</th>
                            <th style="width: 20%;">ქმედება</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>      
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                                    
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageAnsChats($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table2" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 20%;">თარიღი და დრო</th>
                            <th style="width: 20%;">ოპერატორი</th>
                            <th style="width: 15%;">მომხმარებელი</th>
                            <th style="width: 15%;">ბოლოს მოწერის დრო</th>
                            <th style="width: 30%;">ქმედება</th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>      
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            
                                                 
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageAnsFB($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table2" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 10%;">დაწყების დრო</th>
                            <th style="width: 10%;">დასრულების დრო</th>
                            <th style="width: 10%;">ოპერატორი</th>
                            <th style="width: 10%;">მომხმარებელი</th>
                            <th style="width: 15%;">საიტი</th>
                            <th style="width: 15%;">ჩატის ხანგძლივობა</th>
                            <th style="width: 30%;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>      
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                                                 
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageAnsMail($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table2" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 10%;">დაწყების დრო</th>
                            <th style="width: 10%;">დასრულების დრო</th>
                            <th style="width: 10%;">ოპერატორი</th>
                            <th style="width: 10%;">მომხმარებელი</th>
                            <th style="width: 15%;">საიტი</th>
                            <th style="width: 15%;">ჩატის ხანგძლივობა</th>
                            <th style="width: 30%;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>      
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                                                 
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageAnsViber($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<table class="display" id="info_table2" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 10%;">დაწყების დრო</th>
                            <th style="width: 10%;">დასრულების დრო</th>
                            <th style="width: 10%;">ოპერატორი</th>
                            <th style="width: 10%;">მომხმარებელი</th>
                            <th style="width: 15%;">საიტი</th>
                            <th style="width: 15%;">ჩატის ხანგძლივობა</th>
                            <th style="width: 30%;">ქმედება</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>      
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>  
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                                                 
                            
                        </tr>
                    </thead>
                    
                </table>
			
        </fieldset>
 	    
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}
function GetPageAudio($res = '')
{
    global $db;
	$data = '
	<div id="dialog-form">
    <audio controls="" autoplay="" style="width:500px;"><source src="http://92.241.76.198:8081/'.$res[result][0][datetime].$res[result][0][name].'.'.$res[result][0][format].'" type="audio/wav"> Your browser does not support the audio element.</audio>
		
    </div>
    ';
    
	return $data;
}
?>
