<?php
//require_once '../includes/classes/core.php';
require_once('../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$act = $_REQUEST['act'];
global $data;

switch ($act) {
    
    case 'main':
        
        $db->setQuery("SELECT     IFNULL(ROUND(t1.`call`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`call`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100,2),'0.00') as `call_percent`,
                                  IFNULL(ROUND((t1.`chat`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`chat`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `chat_percent`,
                                  IFNULL(ROUND((t1.`messenger`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`messenger`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `messenger_percent`,
                                  IFNULL(ROUND((t1.`fb`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`fb`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `fb_percent` ,
                                  IFNULL(ROUND((t1.`viber`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`viber`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `viber_percent` ,
                                  IFNULL(ROUND((t1.`mail`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`mail`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `mail_percent` ,
                                  IFNULL(ROUND((t1.`video`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`video`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `video_percent`  ,
                                  IFNULL(ROUND((t1.`webcall`/(t1.`call`+t1.chat+t1.messenger+t1.fb+t1.viber+t1.mail+t1.video+t1.webcall)*100-t2.`webcall`/(t2.`call`+t2.chat+t2.messenger+t2.fb+t2.viber+t2.mail+t2.video+t2.webcall)*100),2),'0.00') as `webcall_percent`
                        FROM      dashboard_count t1
                        LEFT JOIN dashboard_count t2 ON t2.date=DATE_SUB(CURDATE(),INTERVAL 1 DAY)
                        WHERE     t1.date = CURDATE()");
        
        $datachart1=array();
        
        $res=$db->getResultArray();
        foreach ($res['result'] as $row) {
            
            
            $datachart1[call_percent]=$row['call_percent'];
            $datachart1[chat_percent]=$row['chat_percent'];
            $datachart1[messenger_percent]=$row['messenger_percent'];
            $datachart1[fb_percent]=$row['fb_percent'];
            $datachart1[viber_percent]=$row['viber_percent'];
            $datachart1[mail_percent]=$row['mail_percent'];
            $datachart1[video_percent]=$row['video_percent'];
            $datachart1[webcall_percent]=$row['webcall_percent'];
            
            $datachart1[call_color]=$row['call_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[chat_color]=$row['chat_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[messenger_color]=$row['messenger_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[fb_color]=$row['fb_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[viber_color]=$row['viber_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[mail_color]=$row['mail_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[video_color]=$row['video_percent']  >= 0 ? "#3ab591" : "red";
            $datachart1[webcall_color]=$row['webcall_percent']  >= 0 ? "#3ab591" : "red";
            
            $datachart1[call_transform]=$row['call_percent']  >= 0 ? "1" : "-1";
            $datachart1[chat_transform]=$row['chat_percent']  >= 0 ? "1" : "-1";
            $datachart1[messenger_transform]=$row['messenger_percent']  >= 0 ? "1" : "-1";
            $datachart1[fb_transform]=$row['fb_percent']  >= 0 ? "1" : "-1";
            $datachart1[viber_transform]=$row['viber_percent']  >= 0 ? "1" : "-1";
            $datachart1[mail_transform]=$row['mail_percent']  >= 0 ? "1" : "-1";
            $datachart1[video_transform]=$row['video_percent']  >= 0 ? "1" : "-1";
            $datachart1[webcall_transform]=$row['webcall_percent']  >= 0 ? "1" : "-1";
            
        }
        
        $db->setQuery(" SELECT  call_vs,
                                chat_vs,
                                chat_fb_vs,
                                chat_mail_vs,
                                video_call_vs,
                                chat_cb_vs,
                                viber_vs,
                                web_call_vs,
                                avg_vs
                        FROM    dashboard");
        
        $vs = array();
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            $vs[call_vs] = $row['call_vs'];
            $vs[chat_fb_vs] = $row['chat_fb_vs'];
            $vs[chat_mail_vs] = $row['chat_mail_vs'];
            $vs[video_call_vs] = $row['video_call_vs'];
            $vs[chat_cb_vs] = $row['chat_cb_vs'];
            $vs[viber_vs] = $row['viber_vs'];
            $vs[chat_vs] = $row['chat_vs'];
            $vs[web_call_vs] = $row['web_call_vs'];
            $vs[call_vs_avg] = $row['avg_vs'];
            
            //            $avg = ((int)$row[call_vs] + (int)$row[chat_fb_vs] + (int)$row[chat_mail_vs] + (int)$row[video_call_vs] + (int)$row[chat_cb_vs] + (int)$row[viber_vs] + (int)$row[chat_vs] + (int)$row[web_call_vs]) / 8;
            //            $avg = number_format((float)$avg, 0, '.', '');
            
            
            
        }
        
        // SL
        $db->setQuery("SELECT  call_sl,
                                    chat_sl,
                                    chat_fb_sl,
                                    chat_mail_sl,
                                    video_call_sl,
                                    chat_cb_sl,
                                    viber_sl,
                                    web_call_sl,
                                    call_sl_percent,
                                    chat_sl_percent,
                                    chat_fb_sl_percent,
                                    chat_mail_sl_percent,
                                    video_call_sl_percent,
                                    chat_cb_sl_percent,
                                    viber_sl_percent,
                                    web_call_sl_percent
                            FROM    dashboard");
        
        $sl = array();
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            $sl[call_sl] = $row['call_sl'];
            $sl[chat_fb_sl] = $row['chat_fb_sl'];
            $sl[chat_mail_sl] = $row['chat_mail_sl'];
            $sl[video_call_sl] = $row['video_call_sl'];
            $sl[chat_cb_sl] = $row['chat_cb_sl'];
            $sl[viber_sl] = $row['viber_sl'];
            $sl[chat_sl] = $row['chat_sl'];
            $sl[web_call_sl] = $row['web_call_sl'];
            
            $avg = ((int)$row[call_sl] + (int)$row[chat_fb_sl] + (int)$row[chat_mail_sl] + (int)$row[video_call_sl] + (int)$row[chat_cb_sl] + (int)$row[viber_sl] + (int)$row[chat_sl] + (int)$row[web_call_sl]) / 8;
            $avg = number_format((float)$avg, 0, '.', '');
            $sl[call_sl_avg] = $avg;
            
            $sl[call_sl_percent] = $row['call_sl_percent'];
            $sl[chat_fb_sl_percent] = $row['chat_fb_sl_percent'];
            $sl[chat_mail_sl_percent] = $row['chat_mail_sl_percent'];
            $sl[video_call_sl_percent] = $row['video_call_sl_percent'];
            $sl[chat_cb_sl_percent] = $row['chat_cb_sl_percent'];
            $sl[viber_sl_percent] = $row['viber_sl_percent'];
            $sl[chat_sl_percent] = $row['chat_sl_percent'];
            $sl[web_call_sl_percent] = $row['web_call_sl_percent'];
            
            $avg = ((int)$row[call_sl] + (int)$row[chat_fb_sl_percent] + (int)$row[chat_mail_sl_percent] + (int)$row[video_call_sl_percent] + (int)$row[chat_cb_sl_percent] + (int)$row[viber_sl_percent] + (int)$row[chat_sl_percent] + (int)$row[web_call_sl_percent]) / 8;
            $avg = number_format((float)$avg, 0, '.', '');
            $sl[call_sl_avg_percent] = $avg;
            
        }
        
        
        //ASA
        $db->setQuery("SELECT  call_asa,
                                    chat_asa,
                                    chat_fb_asa,
                                    chat_mail_asa,
                                    video_call_asa,
                                    chat_cb_asa,
                                    viber_asa,
                                    web_call_asa
                            FROM    dashboard");
        
        $asa = array();
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            $asa[call_asa] = $row['call_asa'];
            $asa[chat_fb_asa] = $row['chat_fb_asa'];
            $asa[chat_mail_asa] = $row['chat_mail_asa'];
            $asa[video_call_asa] = $row['video_call_asa'];
            $asa[chat_cb_asa] = $row['chat_cb_asa'];
            $asa[viber_asa] = $row['viber_asa'];
            $asa[chat_asa] = $row['chat_asa'];
            $asa[web_call_asa] = $row['web_call_asa'];
            
            $avg = ((int)$row[call_asa] + (int)$row[chat_fb_asa] + (int)$row[chat_mail_asa] + (int)$row[video_call_asa] + (int)$row[chat_cb_asa] + (int)$row[viber_asa] + (int)$row[chat_asa] + (int)$row[web_call_asa]) / 8;
            $avg = number_format((float)$avg, 0, '.', '');
            $asa[call_asa_avg] = $avg;
            
        }
        
        
        $db->setQuery(" SELECT  `call` as call_count ,
                                `chat` ,
                                `messenger` as site_chat,
                                `fb` as fb_chat,
                                `viber` as viber_chat,
                                `mail` as mail_count,
                                `video` as video_call,
                                `webcall` as web_call_count
                        FROM    `dashboard_count`
                        WHERE   `date` = CURDATE()");
        $chart1 = array();
        $sum_data_chart1 = 0;
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            $sum_data_chart1 = $row[call_count] + $row[mail_count] + $row[web_call_count] + $row[fb_chat] + $row[viber_chat] + $row[site_chat] + $row[video_call] + $row[chat];
            array_push($chart1, (int)$row[call_count]);
            array_push($chart1, (int)$row[chat]);
            array_push($chart1, (int)$row[site_chat]);
            array_push($chart1, (int)$row[fb_chat]);
            array_push($chart1, (int)$row[mail_count]);
            array_push($chart1, (int)$row[viber_chat]);
            array_push($chart1, (int)$row[video_call]);
            array_push($chart1, (int)$row[web_call_count]);
            
        }
        
        $chart4 = array();
        
        $db->setQuery("SELECT 	 `dashboard_count`.`call`
                       FROM      `dashboard_count`
                       WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                       ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart4, (int)$row[call]);
            
        }
        
        $chart8 = array();
        
        $db->setQuery("SELECT 	 `dashboard_count`.`chat`
                       FROM      `dashboard_count`
                       WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                       ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart8, (int)$row[chat]);
            
        }
        
        $chart7 = array();
        
        $db->setQuery("SELECT 	 `dashboard_count`.`messenger`
                       FROM      `dashboard_count`
                       WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                       ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart7, (int)$row[messenger]);
            
        }
        
        $chart9 = array();
        
        $db->setQuery("SELECT 	 `dashboard_count`.`fb`
                       FROM      `dashboard_count`
                       WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                       ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart9, (int)$row[fb]);
            
        }
        
        $chart10 = array();
        
        $db->setQuery("SELECT 	 `dashboard_count`.`viber`
                       FROM      `dashboard_count`
                       WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                       ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart10, (int)$row[viber]);
            
        }
        
        $chart11 = array();
        
        $db->setQuery(" SELECT 	  `dashboard_count`.`mail`
                        FROM      `dashboard_count`
                        WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                        ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart11, (int)$row[mail]);
            
        }
        
        $chart12 = array();
        
        $db->setQuery(" SELECT 	  `dashboard_count`.`video`
                        FROM      `dashboard_count`
                        WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                        ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart12, (int)$row[video]);
            
        }
        
        $chart34 = array();
        
        $db->setQuery(" SELECT 	  `dashboard_count`.`webcall`
                        FROM      `dashboard_count`
                        WHERE     `dashboard_count`.`date` > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                        ORDER BY  `dashboard_count`.`date` ASC");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart34, (int)$row[webcall]);
            
        }
        
        ///vs
        $vs_percent = array();
        $vs_seriesData = array();
        
        $db->setQuery("SELECT   (ROUND(100- (call_missed + webcall_missed)/((`call` - call_missed) + call_missed + chat + messenger + fb + viber + mail + video + (webcall - webcall_missed)) * 100,0)) AS precent,
                                call_missed + webcall_missed AS missed,
                                `call` - call_missed  + chat + messenger + fb + viber + mail + video + ( webcall - webcall_missed ) AS answer,
                                IF((`call` - call_missed + chat + messenger + fb + viber + mail + video + webcall - webcall_missed) >= (call_missed + webcall_missed), '#3ab591','red' ) AS color,
            
                            	 IFNULL(ROUND((`call` - call_missed) / `call` * 100, 2 ), 0) AS `call`,
                            	 IFNULL(ROUND((webcall - webcall_missed) / `webcall` * 100, 2), 0) AS webcall,
                            	 IF(IFNULL(ROUND((`call` - call_missed) / `call` * 100, 2 ), 0 ) >= (SELECT IFNULL(ROUND((`call` - call_missed) / `call` * 100, 2), 0)
																									 FROM   dashboard_count
																									 WHERE  date = DATE_SUB(CURDATE(),INTERVAL 1 DAY)),'#3ab591','red') AS color_call_diff,
                            	 IF(IFNULL(ROUND((`call` - call_missed) / `call` * 100, 2 ), 0 ) >= call_vs, '#3ab591', 'red') AS color_call,
                            	 IF(IFNULL( ROUND( ( webcall - webcall_missed ) / `webcall` * 100, 2 ), 0 ) >= (SELECT IFNULL( ROUND( ( `webcall` - webcall_missed ) / `webcall` * 100, 2 ), 0 )
																												FROM   dashboard_count
																												WHERE  date = DATE_SUB(CURDATE(),INTERVAL 1 DAY)), '#3ab591', 'red') AS color_web_call_diff,
                            	 IF(IFNULL(ROUND((webcall - webcall_missed) / `webcall` * 100, 2 ), 0) >= web_call_vs, '#3ab591', 'red') AS color_web_call,
                            	 IF(IFNULL(ROUND((`call` - call_missed) / `call` * 100, 2 ), 0 ) >= ( SELECT IFNULL( ROUND( ( `call` - call_missed ) / `call` * 100, 2 ), 0 )
																									  FROM   dashboard_count
																									  WHERE  date = DATE_SUB(CURDATE(),INTERVAL 1 DAY)),'1','-1') AS call_transform,
                            	 IF(IFNULL( ROUND( ( webcall - webcall_missed ) / `webcall` * 100, 2 ), 0 ) >= (SELECT IFNULL( ROUND( ( `webcall` - webcall_missed ) / `webcall` * 100, 2 ),0 )
																												FROM   dashboard_count
																												WHERE  date = DATE_SUB(CURDATE(),INTERVAL 1 DAY)),'1','-1') AS web_call_transform,
                            	 IFNULL( ROUND( ( `call` - call_missed ) / `call` * 100, 2 ), 0 ) - (SELECT IFNULL( ROUND( ( `call` - call_missed ) / `call` * 100, 2 ), 0 )
																									 FROM   dashboard_count
																									 WHERE  date = DATE_SUB(CURDATE(),INTERVAL 1 DAY)) AS `call_inc`,
            
                            	 IFNULL( ROUND( ( webcall - webcall_missed ) / `webcall` * 100, 2 ), 0 ) - (SELECT IFNULL( ROUND( ( `webcall` - webcall_missed ) / `webcall` * 100, 2 ), 0 )
																											FROM   dashboard_count
																											WHERE  date = DATE_SUB(CURDATE(),INTERVAL 1 DAY)) AS webcall_inc
                        FROM   dashboard_count
                        JOIN   dashboard
                        WHERE  date = CURDATE()");
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($vs_percent, $row[precent]);
            array_push($vs_percent, $row[color]);
            array_push($vs_percent, $row[call]);
            array_push($vs_percent, $row[webcall]);
            array_push($vs_percent, $row[color_call]);
            array_push($vs_percent, $row[color_web_call]);
            array_push($vs_percent, $row[call_transform]);
            array_push($vs_percent, $row[web_call_transform]);
            array_push($vs_percent, $row[call_inc]);
            array_push($vs_percent, $row[webcall_inc]);
            array_push($vs_seriesData, (int)$row[missed]);
            array_push($vs_seriesData, (int)$row[answer]);
            array_push($vs_percent, $row[color_call_diff]);
            array_push($vs_percent, $row[color_web_call_diff]);
            
        }
        
        $db->setQuery(" SELECT 	   ROUND((`dashboard_count`.`call` - `dashboard_count`.`call_missed`) / `dashboard_count`.`call` * 100 , 2) AS `missed_call`,
                                   ROUND((`dashboard_count`.`webcall` - `dashboard_count`.`webcall_missed`) / `dashboard_count`.`webcall` * 100 , 2) AS `missed_webcall`
                        FROM      `dashboard_count`
                        WHERE     `dashboard_count`.date > DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                        ORDER BY  `dashboard_count`.`date` ASC");
        
        $chart13 = array();
        $chart35 = array();
        
        $res=$db->getResultArray();
        foreach  ($res['result'] as $row) {
            
            
            array_push($chart13, (int)$row[missed_call]);
            array_push($chart35, (int)$row[missed_webcall]);
            
        }
        
        //SL
        
        $db->setQuery("SELECT  DATE(FROM_UNIXTIME(DSH1.datetime)) AS `datetime`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 1 AND `DSH1`.duration < dashboard.call_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 1 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `call`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 2 AND `DSH1`.duration < dashboard.chat_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 2 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `chat`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 3 AND `DSH1`.duration < dashboard.chat_cb_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 3 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `messenger`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 4 AND `DSH1`.duration < dashboard.chat_fb_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 4 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `fb`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 5 AND `DSH1`.duration < dashboard.viber_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 5 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `viber`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 6 AND `DSH1`.duration < dashboard.chat_mail_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 6 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `mail`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 7 AND `DSH1`.duration < dashboard.video_call_sl AND `DSH1`.`status` = 1, 1, 0)) / SUM(IF(`DSH1`.source_id = 7 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `video`,
							   IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 8 AND `DSH1`.duration < dashboard.web_call_sl AND `DSH1`.`status` = 1, 1 , 0)) / SUM(IF(`DSH1`.source_id = 8 AND `DSH1`.`status` = 1, 1, 0)) * 100, 2), '0.00') AS `webcall`
                      FROM 	  `dashboard_chat_durations` AS `DSH1`
                      JOIN    `dashboard`
                      WHERE    DATE(FROM_UNIXTIME(`DSH1`.`datetime`))> DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                      GROUP BY DATE(FROM_UNIXTIME(`DSH1`.`datetime`))");
        
        $chart20 = array();
        $chart21 = array();
        $chart22 = array();
        $chart23 = array();
        $chart24 = array();
        $chart25 = array();
        $chart26 = array();
        $chart36 = array();
        $sl_color = array();
        $counter = 0;
        $call = 0;
        $chat = 0;
        $messenger = 0;
        $fb = 0;
        $viber = 0;
        $mail = 0;
        $video = 0;
        $webcall = 0;
        $avg_last=0;
        
        $numResults = $db->getNumRow();
        $sl_res =$db->getResultArray();
        foreach ($sl_res['result'] as $row) {
            
            
            if (++$counter == $numResults) {
                
                
                array_push($sl_color, ($row['call'] - $call >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['chat'] - $chat >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['messenger'] - $messenger >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['fb'] - $fb >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['viber'] - $viber >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['mail'] - $mail >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['video'] - $video >= 0) ? "#3ab591" : "red");
                array_push($sl_color, ($row['webcall'] - $webcall >= 0) ? "#3ab591" : "red");
                
                array_push($sl_color, ($row['call'] - $call >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['chat'] - $chat >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['messenger'] - $messenger >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['fb'] - $fb >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['viber'] - $viber >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['mail'] - $mail >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['video'] - $video >= 0) ? "1" : "-1");
                array_push($sl_color, ($row['webcall'] - $webcall >= 0) ? "1" : "-1");
                
                $avg = ((int)$row[call] + (int)$row[chat] + (int)$row[messenger] + (int)$row[fb] + (int)$row[viber] + (int)$row[mail] + (int)$row[video] + (int)$row[webcall]) / 8;
                $avg = number_format((float)$avg, 0, '.', '');
                array_push($sl_color, $avg);
                
                array_push($sl_color, ($avg - $avg_last >= 0) ? "#3ab591" : "red");
                
                
                $sl[sl_call] = $row['call'];
                $sl[sl_chat] = $row['chat'];
                $sl[sl_messenger] = $row['messenger'];
                $sl[sl_fb] = $row['fb'];
                $sl[sl_viber] = $row['viber'];
                $sl[sl_video] = $row['video'];
                $sl[sl_mail] = $row['mail'];
                $sl[sl_webcall] = $row['webcall'];
                
                $sl[sl_call_diff] = number_format((float)$row['call'] - $call, 2, '.', '');
                $sl[sl_chat_diff] = number_format((float)$row['chat'] - $chat, 2, '.', '');
                $sl[sl_messenger_diff] = number_format((float)$row['messenger'] - $messenger, 2, '.', '');
                $sl[sl_fb_diff] = number_format((float)$row['fb'] - $fb, 2, '.', '');
                $sl[sl_viber_diff] = number_format((float)$row['viber'] - $viber, 2, '.', '');
                $sl[sl_mail_diff] = number_format((float)$row['mail'] - $mail, 2, '.', '');
                $sl[sl_video_diff] = number_format((float)$row['video'] - $video, 2, '.', '');
                $sl[sl_webcall_diff] = number_format((float)$row['webcall'] - $webcall, 2, '.', '');
            }
            
            if ($counter == $numResults - 1) {
                $call = $row['call'];
                $chat = $row['chat'];
                $messenger = $row['messenger'];
                $fb = $row['fb'];
                $viber = $row['viber'];
                $video = $row['video'];
                $mail = $row['mail'];
                $webcall = $row['webcall'];
                
                $avg_last = ((int)$row[call] + (int)$row[chat] + (int)$row[messenger] + (int)$row[fb] + (int)$row[viber] + (int)$row[mail] + (int)$row[video] + (int)$row[webcall]) / 8;
                $avg_last = number_format((float)$avg_last, 0, '.', '');
            }
            
            
            array_push($chart20, (int)$row[call]);
            array_push($chart21, (int)$row[chat]);
            //            array_push($chart22, (int)$row[messenger]);
            array_push($chart23, (int)$row[fb]);
            array_push($chart24, (int)$row[viber]);
            array_push($chart25, (int)$row[mail]);
            //            array_push($chart26, (int)$row[video]);
            array_push($chart36, (int)$row[webcall]);
            
        }
        
        $chart2 = array();
        array_push($chart2, end($chart20));
        array_push($chart2, end($chart21));
        //        array_push($chart2, end($chart22));
        array_push($chart2, end($chart23));
        array_push($chart2, end($chart24));
        array_push($chart2, end($chart25));
        //        array_push($chart2, end($chart26));
        array_push($chart2, end($chart36));
        
        ////wait times
        /// asa
        
        $db->setQuery("SELECT  	  DATE(FROM_UNIXTIME(DSH1.datetime)) AS `datetime`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 1 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 1 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `call`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 2 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 2 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `chat`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 3 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 3 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `messenger`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 4 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 4 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `fb`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 5 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 5 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `viber`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 6 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 6 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `mail`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 7 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 7 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `video`,
                                      IFNULL(ROUND(SUM(IF(`DSH1`.source_id = 8 AND `DSH1`.`status` = 1, `DSH1`.`duration`, 0)) / SUM(IF(`DSH1`.source_id = 8 AND `DSH1`.`status` = 1, 1, 0)), 0), '0.00') AS `webcall`
                                      FROM 	`dashboard_chat_durations` AS `DSH1`
                                      JOIN  `dashboard`
                                      WHERE DATE(FROM_UNIXTIME(`DSH1`.`datetime`))> DATE_SUB(CURDATE(),INTERVAL 7 DAY)
                                      GROUP BY DATE(FROM_UNIXTIME(`DSH1`.`datetime`))");
        
        $chart27 = array();
        $chart28 = array();
        $chart29 = array();
        $chart30 = array();
        $chart31 = array();
        $chart32 = array();
        $chart33 = array();
        $chart37 = array();
        $counter = 0;
        $call = 0;
        $chat = 0;
        $messenger = 0;
        $fb = 0;
        $viber = 0;
        $mail = 0;
        $video = 0;
        $webcall = 0;
        
        $wait_res= $db->getResultArray();
        foreach ($wait_res['result'] as $row) {
            
            
            if (++$counter == $numResults) {
                
                
                $asa[asa_call_color] = ($row['call'] - $call >= 0) ? "#3ab591" : "red";
                $asa[asa_chat_color] = ($row['chat'] - $chat >= 0) ? "#3ab591" : "red";
                $asa[asa_messenger_color] = ($row['messenger'] - $messenger >= 0) ? "#3ab591" : "red";
                $asa[asa_fb_color] = ($row['fb'] - $fb >= 0) ? "#3ab591" : "red";
                $asa[asa_viber_color] = ($row['viber'] - $viber >= 0) ? "#3ab591" : "red";
                $asa[asa_mail_color] = ($row['mail'] - $mail >= 0) ? "#3ab591" : "red";
                $asa[asa_video_color] = ($row['video'] - $video >= 0) ? "#3ab591" : "red";
                $asa[asa_webcall_color] = ($row['webcall'] - $webcall >= 0) ? "#3ab591" : "red";
                
                $asa[asa_call_transform] = ($row['call'] - $call >= 0) ? "1" : "-1";
                $asa[asa_chat_transform] = ($row['chat'] - $chat >= 0) ? "1" : "-1";
                $asa[asa_messenger_transform] = ($row['messenger'] - $messenger >= 0) ? "1" : "-1";
                $asa[asa_fb_transform] = ($row['fb'] - $fb >= 0) ? "1" : "-1";
                $asa[asa_viber_transform] = ($row['viber'] - $viber >= 0) ? "1" : "-1";
                $asa[asa_mail_transform] = ($row['mail'] - $mail >= 0) ? "1" : "-1";
                $asa[asa_video_transform] = ($row['video'] - $video >= 0) ? "1" : "-1";
                $asa[asa_webcall_transform] = ($row['webcall'] - $webcall >= 0) ? "1" : "-1";
                
                
                $asa[asa_call] = $row['call'];
                $asa[asa_chat] = $row['chat'];
                $asa[asa_messenger] = $row['messenger'];
                $asa[asa_fb] = $row['fb'];
                $asa[asa_viber] = $row['viber'];
                $asa[asa_video] = $row['video'];
                $asa[asa_mail] = $row['mail'];
                $asa[asa_webcall] = $row['webcall'];
                
                $asa[asa_call_diff] = number_format(100 - ($call / $row['call'] * 100), 2, '.', '');
                $asa[asa_chat_diff] = number_format(100 - ($chat / $row['chat'] * 100), 2, '.', '');
                $asa[asa_messenger_diff] = number_format(100 - ($messenger / $row['messenger'] * 100), 2, '.', '');
                $asa[asa_fb_diff] = number_format(100 - ($fb / $row['fb'] * 100), 2, '.', '');
                $asa[asa_viber_diff] = number_format(100 - ($viber / $row['viber'] * 100), 2, '.', '');
                $asa[asa_mail_diff] = number_format(100 - ($mail / $row['mail'] * 100), 2, '.', '');
                $asa[asa_video_diff] = number_format(100 - ($video / $row['video'] * 100), 2, '.', '');
                $asa[asa_webcall_diff] = number_format(100 - ($webcall / $row['webcall'] * 100), 2, '.', '');
                
                $avg = ((int)$row[call] + (int)$row[chat] + (int)$row[messenger] + (int)$row[fb] + (int)$row[viber] + (int)$row[mail] + (int)$row[video] + (int)$row[webcall]) / 8;
                $avg = number_format((float)$avg, 0, '.', '');
                $asa[asa_real] = $avg;
                
                $asa[asa_real_color] = $asa[call_asa_avg] >= $asa[asa_real] ? "red" : "#3ab591";
            }
            
            if ($counter == $numResults - 1) {
                $call = $row['call'];
                $chat = $row['chat'];
                $messenger = $row['messenger'];
                $fb = $row['fb'];
                $viber = $row['viber'];
                $video = $row['video'];
                $mail = $row['mail'];
                $webcall = $row['webcall'];
            }
            
            
            array_push($chart27, (int)$row[call]);
            array_push($chart28, (int)$row[chat]);
            array_push($chart29, (int)$row[messenger]);
            array_push($chart30, (int)$row[fb]);
            array_push($chart31, (int)$row[viber]);
            array_push($chart32, (int)$row[mail]);
            array_push($chart33, (int)$row[video]);
            array_push($chart37, (int)$row[webcall]);
            
        }
        
        $chart5 = array();
        array_push($chart5, end($chart27));
        //        array_push($chart5, end($chart28));
        array_push($chart5, end($chart29));
        array_push($chart5, end($chart30));
        array_push($chart5, end($chart31));
        array_push($chart5, end($chart32));
        //        array_push($chart5, end($chart33));
        array_push($chart5, end($chart37));
        
        /////
        
        $data = array(
            'datachart1'=>$datachart1,
            'vs' => $vs,
            'sl' => $sl,
            'asa' => $asa,
            'sl_color' => $sl_color,
            'main' =>
            array(
                'chart1' =>
                array('typeData' => 'pie',
                    'name' => 'chart1',
                    'sum_data' => $sum_data_chart1,
                    'widthChart' => '240',
                    'heightChart' => '240',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => true,
                    'xaxis_labels' => false,
                    'sparkline' => false,
                    'titleText' => '',
                    'colorsData' => array('#2dc100', '#f80aea',  '#2196f3', '#4c76be', '#e81d42', '#15e5b6'),
                    'seriesData' => $chart1,
                    'categoriesData' => array('ზარები', 'ჩატი',  'FB მესენჯერი', 'FB კომენტარი', 'ელ-ფოსტა',  'ვებ-ქოლი')
                ),
                'chart2' =>
                array('typeData' => 'bar',
                    'widthChart' => '250',
                    'heightChart' => '225',
                    'dataLabels_enable' => true,
                    'yaxis_enable' => true,
                    'xaxis_labels' => false,
                    'sparkline' => false,
                    'xshow' => true,
                    'titleText' => '',
                    'colorsData' => array('#2dc100', '#f80aea',  '#2196f3', '#4c76be', '#e81d42', '#15e5b6'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart2
                    )
                    ),
                    'categoriesData' => array('ზარები', 'ჩატი',  'FB მესენჯერი', 'FB კომენტარი', 'ელ-ფოსტა',  'ვებ-ქოლი')
                ),
                'chart3' =>
                array('typeData' => 'donut',
                    'name' => 'chart3',
                    'widthChart' => '240',
                    'heightChart' => '240',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => true,
                    'xaxis_labels' => false,
                    'sparkline' => false,
                    'titleText' => '',
                    'colorsData' => array('#c13379', '#3ab591'),
                    'seriesData' => $vs_seriesData,
                    'categoriesData' => array('უპასუხო', 'ნაპასუხები'),
                    'vs_percent' => $vs_percent
                ),
                'chart4' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'sparkline' => true,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart4
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart5' =>
                array('typeData' => 'bar',
                    'widthChart' => '250',
                    'heightChart' => '225',
                    'dataLabels_enable' => true,
                    'yaxis_enable' => true,
                    'xaxis_labels' => false,
                    'sparkline' => false,
                    'xshow' => true,
                    'titleText' => '',
                    'colorsData' => array('#2dc100', '#f80aea',  '#2196f3', '#4c76be', '#e81d42', '#15e5b6'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart5
                    )
                    ),
                    'categoriesData' => array('ზარები', 'ჩატი',  'FB მესენჯერი', 'FB კომენტარი', 'ელ-ფოსტა',  'ვებ-ქოლი')
                ),
                'chart6' =>
                array('typeData' => 'area',
                    'widthChart' => '300',
                    'heightChart' => '300',
                    'dataLabels_enable' => true,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'yaxis_enable' => true,
                    'titleText' => '',
                    'colorsData' => array('#ffa500', '#525ddc', '#a34b4b', '#007b6e', '#333333', '#d695d4'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(2520, 1510, 1322, 12960, 15664, 34831, 115477, 208191, 288467, 248858)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart7' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart7
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart8' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'sparkline' => true,
                    'xaxis_labels' => false,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart8
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart9' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart9
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart10' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'titleText' => '',
                    'sparkline' => true,
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart10
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart11' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart11
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart12' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart12
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart13' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart13
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart14' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(100, 100, 100, 100, 100, 100, 100)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart15' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(100, 100, 100, 100, 100, 100, 100)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart16' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(100, 100, 100, 100, 100, 100, 100)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart17' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(100, 100, 100, 100, 100, 100, 100)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart18' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(100, 100, 100, 100, 100, 100, 100)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart19' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => array(100, 100, 100, 100, 100, 100, 100)
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart20' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart20
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart21' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart21
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart22' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart22
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart23' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart23
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart24' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart24
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart25' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart25
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart26' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart26
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart27' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart27
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart28' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart28
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart29' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart29
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart30' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart30
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart31' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart31
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart32' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart32
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart33' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart33
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart34' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart34
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart35' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart35
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart36' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart36
                    )
                    ),
                    'categoriesData' => array()
                ),
                'chart37' =>
                array('typeData' => 'area',
                    'widthChart' => '100',
                    'heightChart' => '60',
                    'dataLabels_enable' => false,
                    'yaxis_enable' => false,
                    'xaxis_labels' => false,
                    'sparkline' => true,
                    'titleText' => '',
                    'colorsData' => array('#212121'),
                    'seriesData' => array(array('name' => '',
                        'data' => $chart37
                    )
                    ),
                    'categoriesData' => array()
                )
                
                
            )
        );
        
        
        break;
        
        
    default:
        $data = '';
        break;
        
}
echo json_encode((object)$data);


?>