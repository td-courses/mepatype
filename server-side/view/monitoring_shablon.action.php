<?php
include('../../includes/classes/class.Mysqli.php');

global $db;

$db=new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= '';

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);
		
        break;
	case 'get_edit_page':
		$id	  = $_REQUEST['id'];
		$page = GetPage(Getmonitoring_shablon($id));
		$data = array('page' => $page);
		
		break;
	case 'active_shablon':
	    $id	  = $_REQUEST['id'];
	    $db->setQuery("UPDATE monitoring_shablon
                          SET actived = 0
                       WHERE  actived = 1");
	    $db->execQuery();
	    
	    $db->setQuery("UPDATE monitoring_shablon
                          SET actived = 1
                       WHERE  id = '$id'");
	    
	    $db->execQuery();
	    
	    break;
	case 'get_list' :
	    $columnCount = $_REQUEST['count'];
	    $cols[]      = $_REQUEST['cols'];
			
		$db->setQuery(" SELECT monitoring_shablon.id,
                               monitoring_shablon.`datetime`, 
                			   monitoring_shablon.`name`,
                			   user_info.`name`,
                			   CONCAT(monitoring_shablon.all_request_percent,'%'),
                			   IF(monitoring_shablon.distribute_random = 1,'რანდომ','კონფოგურაცია'),
                			   CONCAT('<input class=\"active_shablon\" name=\"monitoring_action_radio\" type=\"radio\" value=\"',monitoring_shablon.id,'\"', IF(monitoring_shablon.actived = 1, 'checked', ''),'/>')
                        FROM   monitoring_shablon
                        JOIN   user_info ON user_info.user_id = monitoring_shablon.user_id");

		$result = $db->getKendoList($columnCount,$cols);
		
		$data = $result;

		break;
	case 'get_columns':
	    
	    $columnCount   = $_REQUEST['count'];
	    $cols[]        = $_REQUEST['cols'];
	    $columnNames[] = $_REQUEST['names'];
	    $operators[]   = $_REQUEST['operators'];
	    $selectors[]   = $_REQUEST['selectors'];
	    //$query = "SHOW COLUMNS FROM $tableName";
	    //$db->setQuery($query,$tableName);
	    //$res = $db->getResultArray();
	    $f=0;
	    foreach($cols[0] AS $col)
	    {
	        $column = explode(':',$col);
	        
	        $res[$f]['Field'] = $column[0];
	        $res[$f]['type'] = $column[1];
	        $f++;
	    }
	    $i = 0;
	    $columns = array();
	    $types = array();
	    foreach($res AS $item)
	    {
	        $columns[$i] = $item['Field'];
	        $types[$i] = $item['type'];
	        $i++;
	    }
	    
	    
	    $dat = array();
	    $a = 0;
	    for($j = 0;$j<$columnCount;$j++)
	    {
	        if(1==2)
	        {
	            continue;
	        }
	        else{
	            
	            if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
	            $op = false;
	            if($columns[$j] == 'id')
	            {
	                $width = "5%";
	            }
	            else if($columns[$j] == 'position'){
	                $width = "12%";
	            }
	            else{
	                $width = 'auto';
	            }
	            if($columns[$j] == 'inc_id')
	            {
	                $hidden = true;
	            }
	            else if($columns[$j] == 'docs_project_id')
	            {
	                $hidden = true;
	            }
	            else if($columns[$j] == 'id')
	            {
	                $hidden = true;
	            }
	            else{
	                $hidden = false;
	            }
	            if($res['data_type'][$j] == 'date')
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
	            }
	            else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
	            }
	            else
	            {
	                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
	            }
	            $a++;
	        }
	        array_push($dat,$g);
	        
	    }
	    
	    //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
	    
	    $new_data = array();
	    //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
	    for($j=0;$j<$columnCount;$j++)
	    {
	        if($types[$j] == 'date')
	        {
	            $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
	        }
	        else if($types[$j] == 'number'){
	            
	            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
	        }
	        else
	        {
	            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
	        }
	    }
	    
	    $filtArr = array('fields'=>$new_data);
	    
	    
	    
	    $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);
	    
	    
	    //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
	    
	    $data = $kendoData;
	    //$data = '[{"gg":"sd","ads":"213123"}]';
	    
	    break;
	case 'save_shablon':
	    $id	                       = $_REQUEST['id'];
	    $user_id                   = $_SESSION['USERID'];
	    $type_ids                  = json_decode($_REQUEST['type_ids']);
	    $name	                   = $_REQUEST['name'];
	    $all_request_percent       = $_REQUEST['all_request_percent'];
	    $distribute_random	       = $_REQUEST['distribute_random'];
	    $monitoring_radio          = $_REQUEST['monitoring_radio'];
	    
	    $department_ids = $request = explode(',',rtrim($_REQUEST['department_ids'],','));
	    $source_ids                = explode(',',rtrim($_REQUEST['source_ids'],','));
	    $action_ids                = explode(',',rtrim($_REQUEST['action_ids'],','));
	    
	    
	    
	    if ($id == '') {
	        
	        $db->setQuery("INSERT INTO `monitoring_shablon`
                    	           SET `user_id`                   = '$user_id',
                        	           `name`                      = '$name',
                        	           `datetime`                  = NOW(),
                        	           `all_request_percent`       = '$all_request_percent',
                        	           `distribute_random`         = '$distribute_random',
                        	           `monitoring_radio_duration` = '$monitoring_radio'");
	        
	        $db->execQuery();
	        $id = $db->getLastId();
	    }else{
	        
	        $db->setQuery("UPDATE   `monitoring_shablon`
            	              SET   `user_id`                   = '$user_id',
                    	            `name`                      = '$name',
                    	            `all_request_percent`       = '$all_request_percent',
                    	            `distribute_random`         = '$distribute_random',
                    	            `monitoring_radio_duration` = '$monitoring_radio'
            	            WHERE   `id`                        = '$id'");
            	        
	        $db->execQuery();
	    }
	    
	    $db->setQuery("DELETE FROM monitoring_shablon_action_type
	                   WHERE shablon_id = '$id'");
	    $db->execQuery();
	    
	    $db->setQuery("DELETE FROM monitoring_shablon_source
	                   WHERE shablon_id = '$id'");
	    $db->execQuery();
	    
	    $db->setQuery("DELETE FROM monitoring_shablon_department
	                   WHERE shablon_id = '$id'");
	    $db->execQuery();
	    
	    $db->setQuery("DELETE FROM monitoring_shablon_type_percent
	                   WHERE shablon_id = '$id'");
	    $db->execQuery();
	    
	    
	    if ($distribute_random == 0) {
	       
	       foreach($type_ids AS $item){
    	        $request = explode(':',$item);
    	        $type_id = $request[0];
    	        $value   = $request[1];
    	        
    	        $db->setQuery("INSERT INTO `monitoring_shablon_type_percent`
                                          (`shablon_id`, `type_id`, `percent`, `actived`) 
                                    VALUES 
    	                                  ('$id', '$type_id', '$value', '1')");
    	        $db->execQuery();
    	    }
    	    
    	    foreach($department_ids AS $dep_id){
    	        $db->setQuery("INSERT INTO `monitoring_shablon_department`
                                          (`shablon_id`, `department_id`, `actived`) 
                                    VALUES 
    	                                  ('$id', $dep_id, '1')");
    	        $db->execQuery();
    	    }
    	    
    	    foreach($source_ids AS $sorce_id){
    	        $db->setQuery("INSERT INTO `monitoring_shablon_source`
                                          (`shablon_id`, `source_id`, `actived`) 
                                    VALUES 
                                          ('$id', $sorce_id, '1')");
    	        $db->execQuery();
    	    }
    	    
    	    foreach($action_ids AS $action_id){
    	        $db->setQuery("INSERT INTO `monitoring_shablon_action_type`
                                          (`shablon_id`, `action_id`, `actived`) 
                                    VALUES 
                                          ('$id', '$action_id', '1');");
    	        $db->execQuery();
    	    }
	    }
	    break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);

function Getmonitoring_type($id=0, $check=0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT id, `name`
                        FROM  `mepa_project_types`
                        WHERE  actived = 1
                        ORDER BY  mepa_project_types.id ASC");
        
        $req = $db->getResultArray();
        $dis = '';
        if ($check == 1) {
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_type_checkbox" type="checkbox" checked value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                            <td style="width: 55px;">
                                <input placeholder="0.00" type_id="'.$row[id].'" class="idle request_type" style="width: 30px; height:21px; text-align: center;" id="call_type_information_percent" type="text" value="" '.$dis.'>
                            </td>
                            <td>
                                <label style="padding: 3px 0 0 0;">%</label>
                            </td>
                      </tr>'; 
        }
        
        $data.='<tr>
                    <td></td>
                    <td></td>
                    <td id="request_types_sum_percent" style="text-align: center;">0.00</td>
                    <td>
                        <label style="padding: 3px 0 0 0;">%</label>
                    </td>
                </tr>';
    }else{
        $db->setQuery(" SELECT    mepa_project_types.id,
                                  mepa_project_types.`name`,
                                  IFNULL(monitoring_shablon_type_percent.type_id,0) AS `type_id`,
                                  monitoring_shablon_type_percent.percent 
                        FROM     `mepa_project_types`
                        LEFT JOIN monitoring_shablon_type_percent ON monitoring_shablon_type_percent.type_id = mepa_project_types.id
                        AND       monitoring_shablon_type_percent.shablon_id = $id
                        WHERE     mepa_project_types.actived = 1
                        ORDER BY  mepa_project_types.id ASC");
        
        $req = $db->getResultArray();
        
        $dis       = '';
        $dis_input = '';
        if ($check == 1) {
            $dis       = "disabled";
            $dis_input = "disabled";
        }
        $sum_percent = 0.00;
        foreach ($req[result] AS $row){
            $checked = "checked";
            if ($row[type_id] == 0) {
                $checked   = '';
                $dis_input = "disabled";
            }else{
                $dis_input = "";
            }
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_type_checkbox" type="checkbox" '.$checked.' value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                            <td style="width: 55px;">
                                <input placeholder="0.00"  type_id="'.$row[id].'" class="idle request_type" style="width: 30px; height:21px; text-align: center;" id="call_type_information_percent" type="text" value="'.$row[percent].'" '.$dis_input.'>
                            </td>
                            <td>
                                <label style="padding: 3px 0 0 0;">%</label>
                            </td>
                      </tr>';
            $sum_percent += $row[percent];
        }
        
        $data.='<tr>
                    <td></td>
                    <td></td>
                    <td id="request_types_sum_percent" style="text-align: center;">'.number_format($sum_percent, 2, '.', '').'</td>
                    <td>
                        <label style="padding: 3px 0 0 0;">%</label>
                    </td>
                </tr>';
        
    }
    
    return $data;
}

function Getmonitoring_project($id=0, $check = 0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT id, `name` 
                        FROM   info_category 
                        WHERE  parent_id = 0 AND actived = 1 AND name != ''
                        ORDER BY info_category.id ASC");
        
        $req = $db->getResultArray();
        
        $dis = '';
        if ($check == 1) {
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_department_checkbox" type="checkbox" checked value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                      </tr>';
        }
        
    }else{
        $db->setQuery(" SELECT    info_category.id, 
                                  info_category.`name`,
                                  IFNULL(monitoring_shablon_department.department_id,0) AS `check`
                        FROM      info_category
                        LEFT JOIN monitoring_shablon_department ON monitoring_shablon_department.department_id = info_category.id 
                        AND       monitoring_shablon_department.shablon_id = '$id' AND monitoring_shablon_department.actived = 1
                        WHERE     info_category.parent_id = 0 AND info_category.actived = 1
                        ORDER BY info_category.id ASC");
        
        $req = $db->getResultArray();
        
        $dis = '';
        if ($check == 1) {
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            
            $checked = "checked";
            if ($row[check] == 0) {
                $checked = '';
            }
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_department_checkbox" type="checkbox" '.$checked.' value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                      </tr>';
        }
    }
    
    return $data;
}

function Getmonitoring_source($id=0, $check = 0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT   id, `name`
                        FROM     source
                        WHERE    actived = 1
                        ORDER BY source.id ASC");
        
        $req = $db->getResultArray();
        
        $dis = '';
        if ($check == 1) {
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            $checked = "checked";
            if ($row[check] == 1) {
                $checked = '';
            }
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_source_checkbox" type="checkbox" checked value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                      </tr>';
        }
        
    }else{
        $db->setQuery(" SELECT    source.id, 
                                  source.`name`,
                        		  IFNULL(monitoring_shablon_source.source_id,0) AS `check`
                        FROM      source
                        LEFT JOIN monitoring_shablon_source ON monitoring_shablon_source.source_id = source.id AND monitoring_shablon_source.shablon_id = '$id'
                        AND       monitoring_shablon_source.actived = 1
                        WHERE     source.actived = 1
                        ORDER BY  source.id ASC");
        
        $req = $db->getResultArray();
        
        $dis = '';
        if ($check == 1) {
            
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            $checked = "checked";
            if ($row[check] == 0) {
                $checked = '';
            }
            
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_source_checkbox" type="checkbox" '.$checked.' value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                      </tr>';
        }
    }
    
    return $data;
}

function Getmonitoring_action($id=0, $check = 0){
    global $db;
    if ($id == 0) {
        $db->setQuery(" SELECT id, `name`
                        FROM   monitoring_action_type
                        WHERE  actived = 1
                        ORDER BY  monitoring_action_type.id ASC");
        
        $req = $db->getResultArray();
        
        $dis = '';
        if ($check == 1) {
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_action_checkbox" type="checkbox" checked value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                      </tr>';
        }
        
    }else{
        $db->setQuery(" SELECT    monitoring_action_type.id, monitoring_action_type.`name`, IFNULL(monitoring_shablon_action_type.id,0) AS `check`
                        FROM      monitoring_action_type
                        LEFT JOIN monitoring_shablon_action_type ON monitoring_action_type.id = monitoring_shablon_action_type.action_id
                        AND       monitoring_shablon_action_type.shablon_id = '$id'
                        WHERE     monitoring_action_type.actived = 1
                        ORDER BY  monitoring_action_type.id ASC");
        
        $req = $db->getResultArray();
        
        $dis = '';
        if ($check == 1) {
            $dis = "disabled";
        }
        foreach ($req[result] AS $row){
            $checked = "checked";
            if ($row[check] == 0) {
                $checked = '';
            }
            $data .= '<tr>
                            <td style="width: 20px;">
                                <input class="call_action_checkbox" type="checkbox" '.$checked.' value="'.$row[id].'" '.$dis.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">'.$row[name].'</label>
                            </td>
                      </tr>';
        }
    }
    
    return $data;
}

function Getmonitoring_shablon($id){
    
    global $db;
    $db->setQuery("SELECT monitoring_shablon.id,
                          user_info.`name` AS `user_name`,
                          monitoring_shablon.`name`,
                          monitoring_shablon.`datetime`,
                         `all_request_percent`,
                         `distribute_random`,
                         `monitoring_radio_duration`
                   FROM   monitoring_shablon
                   JOIN   user_info ON monitoring_shablon.user_id = user_info.user_id
                   WHERE  monitoring_shablon.id = $id");
    
    $req = $db->getResultArray();
    $res = $req[result][0];
    return $res;
}

function GetPage($res = ''){
    global $db;
    $user      = $_SESSION['USERID'];
    $user_name = $res[user_name];
    $date      = $res[datetime];
    
    $check1  = "";
    $check2  = "";
    $check3  = "";
    $check4  = "";
    $check5  = "";
    $check10 = "";
    $check15 = "";
    
    if ($res[id] == '') {
        $db->setQuery(" SELECT `name`,
                    		    NOW() AS `date`
                        FROM    user_info
                        WHERE   user_id = '$user'");
        
        $req        = $db->getResultArray();
        $users_info = $req[result][0];
        
        $user_name = $users_info[name];
        $date      = $users_info[date];
        
     }else{
        if ($res[monitoring_radio_duration] == 1){$check1 = "checked"; }
        if ($res[monitoring_radio_duration] == 2){$check2 = "checked"; }
        if ($res[monitoring_radio_duration] == 3){$check3 = "checked"; }
        if ($res[monitoring_radio_duration] == 4){$check4 = "checked"; }
        if ($res[monitoring_radio_duration] == 5){$check5 = "checked"; }
        if ($res[monitoring_radio_duration] == 10){$check10 = "checked"; }
        if ($res[monitoring_radio_duration] == 15){$check15 = "checked"; }
        
        
        if ($res[distribute_random] == 1){$check_random = "checked"; $disabled_from_random = "disabled";}
    }
    
    $data = '
        <div id="dialog-form" style="margin-top: 10px;">
            <table class="dialog-form-table" style="float:left;">
                <tr style="height:0px">
        			<td style="font-size: 10px;"><label for="CallType">შექმინის თარიღი</label></td>
                    <td style="font-size: 10px;"><label for="CallType">ავტორი</label></td>
                    <td style="font-size: 10px;"><label for="CallType">შაბლონის დასახელება</label></td>
        		</tr>
                <tr style="height:0px">
        			<td style="width: 170px;">
        				<input style="width: 150px;" type="text" id="date" class="idle" value="'.$date.'" />
        			</td>
                    <td style="width: 270px;">
        				<input style="width: 250px;" type="text" id="user_name" class="idle" value="'.$user_name.'" />
        			</td>
        			<td style="width: 320px;">
        				<input style="width: 300px;" type="text" id="shablon_name" class="idle" value="' . $res['name'] . '" />
        			</td>
        		</tr>
            </table>
            <div style="width: 100%; display: inline-flex; margin-top:40px; font-family: pvn; font-weight: bold;">
                <div style="width: 16%;">
                    <table class="dialog-form-table"> 
                        <tr style="height:22px"></tr> 
                        <tr>
                            <td colspan="2"><a style="width: 100%; font-family: pvn;">საერთო მომართვები</a></td>
                        </tr>
                        <tr style="height:7px"></tr>
                        <tr>
                            <td colspan="2">
                                <input class="idle" style="width: 60px; margin-top: 10px; margin-left:35px; text-align-last: center;" id="all_request_percent" type="text" value="'.$res[all_request_percent].'"> %
                            </td>
                        </tr>
                        <tr style="height:36px"></tr>
                        
                        <tr>
                            <td style="width: 20px;">
                                <input id="distribute_random" type="checkbox" '.$check_random.' value="1">
                            </td>
                            <td>
                                <label style="padding: 0px 0 0 0; font-size: 10px;">რანდომ პრინციპით</label>
                            </td>
                        </tr>
                    </table>  
                </div>
                
                <div style="width: 24%;">
                    <a style="width: 100%;">ტიპის მიხედვით</a>
                    <table class="dialog-form-table" style="margin-right: 30px;">
                        '.Getmonitoring_type($res[id], $res[distribute_random]).'
                    </table>
                </div>
                <div style="width: 18%;">
                    <a style="width: 100%;">უწყების მიხედვით</a>
                    <table class="dialog-form-table" style="float: left;">
                        '.Getmonitoring_project($res[id], $res[distribute_random]).'
                    </table>
                </div>
                <div style="width: 14%;">
                <a style="width: 100%;">წყაროს მიხედვით</a>
                    <table class="dialog-form-table" style="float: left;">
                        '.Getmonitoring_source($res[id], $res[distribute_random]).'
                    </table>
                </div>
                <div style="width: 14%;">
                    <a style="width: 100%;">ქმედების მიხედვით</a>
                    <table class="dialog-form-table" style="float: left;">
                        '.Getmonitoring_action($res[id], $res[distribute_random]).'
                    </table>
                </div>
                
                <div style="width: 14%;">
                <a style="width: 100%; float: left;">ხ-ბას მიხედვით</a>
                    <table class="dialog-form-table" style="float: left;">
                        <tr style="height:5px"></tr>
                        <tr>
                            <td style="width: 25px;">
                                <input name="monitoring_radio" type="radio" id="radio1" '.$check1.' value="1" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">1 წთ-მდე</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input name="monitoring_radio" type="radio" id="radio2" '.$check2.' value="2" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">2 წთ-მდე</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input name="monitoring_radio" type="radio" id="radio3" '.$check3.' value="3" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">3 წთ-მდე</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input name="monitoring_radio" type="radio" id="radio4" '.$check4.' value="4" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">4 წთ-მდე</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input name="monitoring_radio" type="radio" id="radio5" '.$check5.' value="5" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">5 წთ-მდე</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input name="monitoring_radio" type="radio" id="radio10" '.$check10.' value="10" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">10 წთ-მდე</label>
                            </td>
                        </tr>
                        <tr style="height:5px"></tr>
                        <tr>
                            <td>
                                <input name="monitoring_radio" type="radio" id="radio15" '.$check15.' value="15" '.$disabled_from_random.'>
                            </td>
                            <td>
                                <label style="font-family: pvn; font-weight: bold;">15 წთ ></label>
                            </td>
                        </tr>
                   </table>
                </div>
            </div>
            <!-- ID -->
        	<input type="hidden" id="shablon_id" value="' . $res['id'] . '" />
        </div>';
    
    return $data;
}
?>
