import Tdg from "../../tdg.class.js";
import ChatPanel from "./chatPanel.type.js";
import WorkGround from "./workGround.type.js";
import Queue from "./queue.type.js";

export default class IncommingModal extends Tdg {

    constructor(prop){
        super()
        
        self.IncommingModal = this
        this.state = {
            dom: this.CreateElement({
                element: "interface",
                attributes: ["incomming"]
            }),
            interface: [],
            prop: prop,
        }


    }

    // INITIALIZE BUILD
    init = () => {

        this.build()
        return this.state.dom

    }

    /**
     * BUILD INCOMMING COMPONENTS
     */
    
    build = async () => {

        this.append(this.state.dom, await new Queue(this.state.prop).init())

        this.append(this.state.dom, await new ChatPanel(this.state.prop).init())

        this.append(this.state.dom, await new WorkGround(this.state.prop).init())

    }




}