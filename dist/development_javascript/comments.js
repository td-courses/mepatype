const comment = (commentBtn) => {
    // let commentBtn = e.target;
    let post = commentBtn.parentElement.parentElement.parentElement
    let postId = commentBtn.parentElement.parentElement.parentElement.parentElement.dataset.postid

    let commentElement = post.querySelector('.post__add_comment__container > textarea');
    let comment = post.querySelector('.post__add_comment__container > textarea').value;

    commentElement.focus()


    let commentsContainers = post.querySelector('.post__other-comment__container')

    // console.log(postId)


    let actAndCommentPostId = {
        act: "add_comment",
        text: comment,
        post_id: postId
    }




    var AjaxURL = "server-side/call/action_action.php";
    $.ajax({
        url: AjaxURL,
        data: actAndCommentPostId,
        success: function (commentArray) {
            let parser = new DOMParser();

            let comment = commentArray.result[0];
            
            let postedComment = parser.parseFromString(`
                                <div class="post__other-comment" data-commentid="${comment.comments_id}">
                                    <div class="post__other-comment__image" style="background-image: url(media/uploads/file/${comment.image_name}); background-size: cover; background-position: center"></div>
                                    <div class="post__other-comment__text">
                                        <div class="post__other-comment__text__name">
                                            <p>${comment.name}</p>
                                        </div>
                                        <div class="post__other-comment__text__department">
                                            <p>${comment.department}</p>
                                        </div>
                                        <div class="post__other-comment__text__time">${comment.time_after_posted}</div>
                                        <div class="post__other-comment__text__date">${comment.data_posted}</div>
                                        <div class="post__other-comment__text__text">
                                            <p>${comment.text} </p>
                                        </div>
                                        <div class="post__other-comment__text__likes">
                                            <p onmouseup="like(this)" > მოწონება (${comment.likes})  </p>
                                            <p class="delete-comment-btn" onmouseup = "deleteComment(this)" > წაშლა </p>
                                        </div>
                                    </div>
                                </div>`, "text/html")
            let commentsContainers = post.querySelector('.post__other-comment__container')


            commentsContainers.style.display = "block"

            commentsContainers.append(postedComment.body.firstChild)
            commentElement.value = ""
        }
    })
}




const like = (likeBtn) => {
    let commentId = likeBtn.parentElement.parentElement.parentElement.dataset.commentid;
    let actAndCommentId = {
        act: "add_comment_like",
        comment_id: commentId
    }


    var AjaxURL = "server-side/call/action_action.php";
    $.ajax({
        url: AjaxURL,
        data: actAndCommentId,
        success: function (comment) {
            let commentLikes = comment.result[0].likes;
            likeBtn.style.color = "#05E989"
            likeBtn.innerHTML = `მოწონება (${commentLikes})`;
        }
    })
}



const deleteComment = (deleteBtn) => {
    let commentId = deleteBtn.parentElement.parentElement.parentElement.dataset.commentid;
    let actAndCommentId = {
        act: "delete_comment",
        comment_id: commentId
    }
 

    var AjaxURL = "server-side/call/action_action.php";
    $.ajax({
        url: AjaxURL,
        data: actAndCommentId,
        success: function (comment) {
            let Comment = deleteBtn.parentElement.parentElement.parentElement;
  
            Comment.style.display = "none"
        }
    })
}