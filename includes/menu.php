<?php
//include('classes/core.php');
include('classes/class.Mysqli.php');
?>
<?PHP if ($_SESSION['chat_off_on'] == '') {
    $_SESSION['chat_off_on'] = 1;
} ?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <title>Menu</title>
    <!-- <link rel="stylesheet" href="media/css/menu/text.css" />
	<link rel="stylesheet" href="media/css/menu/960_fluid.css" />
	<link rel="stylesheet" href="media/css/menu/main.css" />
	<link rel="stylesheet" href="media/css/menu/bar_nav.css" />
	<link rel="stylesheet" href="media/css/menu/side_nav.css" />
	<link rel="stylesheet" href="media/css/menu/skins/theme_blue.css" /> -->
    <link rel="stylesheet" href="">
    <?php require_once('includes/functions.php'); ?>

    <!-- <link rel="stylesheet" href="dist/css/main.css">   -->

    <script>
        $(document).ready(function(){
            $('#toggle_nav_btn').click(function(){

                if($(this).attr("data-status") == 0){
                    $(this).attr("data-status", 1);
                    $("#nav-icon3").addClass('open');
                    $(".navigation__container__sitemap").css("transform", "translate(-16%, 3px)");
            }else{
                $(this).attr("data-status", 0);
                $("#nav-icon3").removeClass('open');
                $(".navigation__container__sitemap").css("transform", "translate(0px, 3px)");
            }
            });

            $('.comunication_bar').map((i, x) => {
                if(i != 0){
                    if(x.getAttribute('disabled')){
                        x.children[0].children[0].style.filter = "grayscale(1)"
                        x.children[0].children[1].style.backgroundColor = "#4B4B4B"
                    }
                }
                
            })


        });
    </script>

</head>
<style type="text/css">

.clearfix count {
    color: #fff;
    background-color: #ec1515;
    line-height: 18px;
    right: 34px;
    position: absolute;
    top: 10px;
    padding: 2px 7px 1px 7px;
    border-radius: 100%;
    font-size: 11px;
    font-family: monospace;
    font-weight: bold;
}


.navigation__container__sitemap {
    transform: translate(0, 3px);
    transition: .4s;
}
.navigation__container__sitemap{
    min-width: 400px !important;
    max-width: 400px !important;
}

.navigation__container__logo {
    min-width: 162px;
    max-width: 162px;
}
    #send_chat_program {
        margin-left: 10px;
        width: 35px;
        height: 35px;
        border-radius: 10px;
        margin-top: 0;
        border: solid 1px #7bb4aa;
        box-shadow: none;
        background-color: #7bb4aa;
        background-image: url(media/images/icons/send_fff.png);
        background-repeat: no-repeat, repeat !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
        background-size: 50% !important;
        transition: 0.2s;
        position: absolute;
        right: 5px;
        bottom: 5px;
        cursor: pointer;
    }

    #user_block {
        display: none !important;
    }

    #choose_button5 {
        position: absolute;
        bottom: 6px;
        right: 76px;
        font-size: 18px!important;
        border: none;
        background: transparent;
        color: #999999;
        cursor: pointer;
        padding-top: 2px;
    }

    .my_mail_file {
        position: relative;
        display: flex;
        padding: 5px 10px 4px 15px;
        border: 1px solid #AAA;
        border-radius: 10px;
        margin: 5px;
        box-shadow: 8px 7px 7px -5px #AAA;
        height: 30px;
    }

    .delete_upload_file {
        position: absolute;
        padding: 0px 4px;
        padding-bottom: 2px;
        border-radius: 50%;
        background-color: #A3D0E4;
        color: #ffffff;
        font-size: 12px;
        cursor: pointer;
        z-index: 999;
        right: -6px;
        top: -6px;
    }

    

    #email_attachment_files::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #email_attachment_files::-webkit-scrollbar {
        height: 7px;
        background-color: #F5F5F5;
    }

    #email_attachment_files::-webkit-scrollbar-thumb {
        background-color: #A3D0E4;
        background-image: -webkit-linear-gradient(45deg,
                rgba(255, 255, 255, .2) 25%,
                transparent 25%,
                transparent 50%,
                rgba(255, 255, 255, .2) 50%,
                rgba(255, 255, 255, .2) 75%,
                transparent 75%,
                transparent)
    }

    #send_message::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #send_message::-webkit-scrollbar {
        width: 7px;
        background-color: #F5F5F5;
    }

    #send_message::-webkit-scrollbar-thumb {
        background-color: #A3D0E4;
        background-image: -webkit-linear-gradient(45deg,
                rgba(255, 255, 255, .2) 25%,
                transparent 25%,
                transparent 50%,
                rgba(255, 255, 255, .2) 50%,
                rgba(255, 255, 255, .2) 75%,
                transparent 75%,
                transparent)
    }

    #paste_files1 div,
    #paste_files1 a {
        min-height: 41px;
        overflow: auto;
    }

    #ul_tab li {
        display: inline;
        margin: 13px;
        color: #0b5c9a;
        border-bottom: 1px solid gray;
        padding-bottom: 7px;
        cursor: pointer;
    }




    .search_button {
        padding: 2px;
        border-radius: 0px;
        background: #009688;
        cursor: pointer;
        float: left;
    }

    #zomm_div {
        float: right;
        cursor: pointer;
        background: #C8D6DC;
        border-radius: 50%;
        padding: 5px;
        border: 1px solid silver;
        margin-bottom: 10px;
        font-weight: bold;
    }

    #table_index tr td:nth-child(11) {
        padding: 0 0 !important;
    }


    #incoming_chat_tabs {
        boreder: none;
    }

    #incoming_chat_tabs .ui-tabs-panel {
        padding: 0;
    }

    /* .hovered_image:hover{ */
    /* 	position: absolute; */
    /*     width: 60px; */
    /*     height: 60px; */
    /*     margin: -30px 2px; */
    /* } */
    .hovered_image {
        transition: transform .2s;
        display: block;
        border-radius: 50%;
        margin: auto;
        width: 20px;
    }

    .play_audio_complate {
        width: 20px;
        height: 22px;
        display: inline-block;
        margin: 0 -2px 0 3px;
        background-image: url("media/images/icons/inc_play_button.png");
        background-size: 21px;
        background-position: center;
        background-repeat: no-repeat;
    }
    .play_audio_complate_disabled {
        width: 20px;
        height: 22px;
        display: inline-block;
        margin: 0 -2px 0 3px;
        background-image: url("media/images/icons/inc_play_button.png");
        background-size: 21px;
        background-position: center;
        background-repeat: no-repeat;
        opacity: 0.3;
    }
    .play_audio_complate:hover{
        cursor: pointer;
    }

    .download_audio_complate {
        width: 20px;
        height: 22px;
        display: inline-block;
        margin: 0 2px;
        background-image: url("media/images/icons/inc_download_button.png");
        background-size: 17px;
        background-position: center;
        background-repeat: no-repeat;
    }
    .download_audio_complate_disabled {
        width: 20px;
        height: 22px;
        display: inline-block;
        margin: 0 2px;
        background-image: url("media/images/icons/inc_download_button.png");
        background-size: 17px;
        background-position: center;
        background-repeat: no-repeat;
        opacity: 0.3;
    }
    .download_audio_complate:hover{
        cursor: pointer;
    }


    .download {
        background-color: #2dc100;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
        font-weight: normal !important;
        margin-left: -14px;
        grid-template-columns: 0% 94%;
        justify-content: center;
    }

    .download1 {
        background-color: #ce8a14;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 14px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
    font-weight: normal !important;
    margin-left: -14px;
    grid-template-columns: 0% 94%;
    justify-content: center;
    }

    [name="task_table_length"] {
        margin-top: -32px;
    }

    .download2 {
        background-color: #d35454;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 20% 76%;
        justify-content: center;
        height: 30px;
        text-align: left;
    }

    .download4 {
        background-color: #2dc100;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        text-decoration: none;
        border: 0px !important;
        text-overflow: ellipsis;
        width: 128%;
        font-weight: normal !important;
        margin-left: -14px;
        grid-template-columns: 19% 74%;
        justify-content: center;
        text-align: left;
    }

    .download3 {
        background-color: #ce8a14;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
    font-weight: normal !important;
    margin-left: -14px;
    grid-template-columns: 5% 94%;
    justify-content: center;
    }

    .download5 {
        background-color: #4980AF;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
    font-weight: normal !important;
    margin-left: -14px;
    grid-template-columns: 0% 94%;
    justify-content: center;
    }

    .download6 {
        background-color: #CE5F14;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 98%;
        font-weight: normal !important;
    }

    .download7 {
        background-color: #f80aea;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
        font-weight: normal !important;
        margin-left: -14px;
        grid-template-columns: 18% 75%;
        justify-content: center;
        text-align: left;
    }

    .download8 {
        background-color: #f80aea;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
             width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 19% 77%;
        justify-content: center;
        text-align: left;
    }

    .download9 {
        background-color: #D1EC38;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 8% 92%;
        justify-content: center;
    }

    .download10 {
        background-color:#7d3daf;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 8% 88%;
        justify-content: center;
        
    }

    .download11 {
        background-color: #2196f3;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
        font-weight: normal !important;
        margin-left: -14px;
        grid-template-columns: 20% 73%;
        justify-content: center;
        text-align: left;
    }

    .download12 {
        background-color: #3A549F;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 8% 92%;
        justify-content: center;
    }

    .download13 {
        background-color: #030202;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 8% 92%;
        justify-content: center;
    }

    .download14 {
        background-color: #089b98;
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 8% 92%;
        justify-content: center;
    }

    #chatcontent a {
        color: #AAA;
        text-decoration: none;
        cursor: pointer;
    }

    #chatcontent a:hover {
        color: #333;

    }

    .download15 {
        background-color: #e81d42;
        border-radius: 0px;
        display: inline-block;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
        width: 128%;
        font-weight: normal !important;
        margin-left: -14px;
        grid-template-columns: 18% 75%;
        justify-content: center;
        text-align: left;
    }
    .download_videocall {
        background-color: hsl(230deg 50% 52%);
        border-radius: 0px;
        display: grid;
        cursor: pointer;
        color: #ffffff !important;
        font-size: 12px;
        border: 0px !important;
        text-decoration: none;
        text-overflow: ellipsis;
         width: 128%;
        font-weight: normal !important;
        margin-left: -12px;
        grid-template-columns: 8% 88%;
        justify-content: center;
    }
    .download_videocall img {
        filter: brightness(0) invert(1);
    }

    #chat_count {
        -webkit-transition: background 1s;
        -moz-transition: background 1s;
        -o-transition: background 1s;
        transition: background 1s;
    }

    .hidden {
        display: none;
    }

    #chat_status {
        width: 135px;
        height: 26px;
        padding: 0px 0px 4px 0px;
        border-radius: 7px;
        background-repeat: no-repeat !important;
        padding-left: 20px !important;
        background-position-x: 5px !important;
        background-position-y: 4px !important;
        background-size: 15px 15px !important;
        border-radius: 7px;
        padding-top: 6px;
        float: right;
        font-family: BPG;
        font-weight: bold;
    }

    #chat_status[data-status="on"] {
        background: url(media/images/icons/green_status.png);
    }

    #chat_status[data-status="off"] {
        background: url(media/images/icons/red_status.png);
    }

    #chat_user {
        width: 82px;
        display: block;
        font-weight: bold;
        font-size: 12px;
        margin-left: 5px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    #table_index td:nth-child(10) {
        padding: 0px;
    }

    .sms-template-table-wrapper {
        width: 100%;
        max-height: 500px;
        overflow: hidden;
        overflow-y: scroll;
    }

    /*choose activitie wrapper*/
    .callapp-op-activitie-wrapper {
        display: grid;
        width: 190px;
        height: 26px;
        float: right;
        grid-template-columns: 26px auto 26px;
        border: 1px solid grey;
        border-radius: 5px;
    }

    .callapp-op-activitie-color-holder {
        display: flex;
        align-items: center;
        justify-content: center;
        ;
    }

    .callapp-op-activitie-color {
        width: 15px;
        height: 15px;
        border-radius: 100%;
        background: #bdc3c7;
    }

    .callapp-op-activitie-choose-wrapper {
        position: relative;
    }

    .callapp-op-activitie-choose-receiver {
        display: flex;
        height: 33px;
        align-items: center;
        justify-content: flex-start;
        font-family: BPG;
        font-size: 14px;
        color: black;
        cursor: default;
        user-select: none;
    }

    #callapp_op_activitie_choose {
        width: 190px;
        position: absolute;
        top: 28px;
        left: -26px;
        background: white;
        border: 1px solid #2980b9;
        list-style: none;
        padding: 0;
        z-index: 999999;
        visibility: hidden;
        transform: scale(.8);
        opacity: 0;
        transition: transform .2s ease, opacity .1s ease;
    }

    #callapp_op_activitie_choose.on {
        visibility: visible;
        opacity: 1;
        transform: scale(1);
    }

    #callapp_op_activitie_choose li {
        display: flex;
        padding: 5px;
        box-sizing: border-box;
        border-bottom: 1px solid #bdc3c7;
        cursor: default;
    }

    #callapp_op_activitie_choose li.is-active {
        background: #3498db;
    }

    #callapp_op_activitie_choose li:last-child {
        border-bottom: none;
    }

    #callapp_op_activitie_choose li:hover {
        background: #3498db;
    }

    #callapp_op_activitie_choose li .activitie-name {
        font-family: pvn;
        font-size: 13px;
        color: black;
        letter-spacing: .3px;
    }

    #callapp_op_activitie_choose li.is-active .activitie-name {
        color: #ecf0f1;
    }

    #callapp_op_activitie_choose li:hover .activitie-name {
        color: #ecf0f1;
    }

    .activitie-color-block {
        width: 15px;
        height: 15px;
        margin-right: 5px;
    }

    #dialog_emojis {
        position: absolute;
        cursor: pointer;
        bottom: 10px;
        right: 50px;
        background: transparent;
    }

    #emoji_close {
        float: left;
        font-size: 15px;
        cursor: pointer;
        color: red;
        width: -webkit-fill-available;
        text-align: center;
        padding: 5px;
        border: 1px solid #CCC;
        margin-bottom: 5px;
        background: #EEE;
    }

    #emoji_wrapper {
        height: 0;
        width: 100%;
        position: absolute;
        z-index: 20;
        overflow: auto;
        transition: height 0.2s;
        background: #EEE;
    }

    #emoji_wrapper::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #emoji_wrapper::-webkit-scrollbar {
        width: 7px;
        background-color: #F5F5F5;
    }

    #emoji_wrapper::-webkit-scrollbar-thumb {
        background-color: #A3D0E4;
        background-image: -webkit-linear-gradient(45deg,
                rgba(255, 255, 255, .2) 25%,
                transparent 25%,
                transparent 50%,
                rgba(255, 255, 255, .2) 50%,
                rgba(255, 255, 255, .2) 75%,
                transparent 75%,
                transparent)
    }


    #chat_scroll::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #chat_scroll::-webkit-scrollbar {
        width: 7px;
        background-color: #F5F5F5;
    }

    #chat_scroll::-webkit-scrollbar-thumb {
        background-color: #A3D0E4;
        background-image: -webkit-linear-gradient(45deg,
                rgba(255, 255, 255, .2) 25%,
                transparent 25%,
                transparent 50%,
                rgba(255, 255, 255, .2) 50%,
                rgba(255, 255, 255, .2) 75%,
                transparent 75%,
                transparent)
    }



    #emoji_wrapper span {
        font-size: 15px;
        cursor: pointer;
        line-height: 30px;
    }

    .callapp-op-activitie-choose-togbutton {
        overflow: hidden;
    }

    .callapp-op-activitie-choose-togbutton button {
        width: 25px;
        height: 26px;
        background: none;
        border: none;
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
        position: relative;
        top: -1px;
        font-size: 18px;
        color: #fff;
        box-sizing: border-box;
        padding-top: 6px;
    }

    .callapp-op-activitie-wrapper:hover button {
        background: #dfe4ea;
    }

    .callapp-op-activitie-choose-togbutton button i {
        color: red;
    }

    .red-color {
        color: red;
    }

    .file_text {
        width: max-content;
        display: flex;
        margin-left: 10px;
    }

    .hero-image {

        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        margin: 5px;
        width: 19px;
    }

    #signatures{
        position: absolute;
        bottom: 8px;
        right: 105px;
        font-size: 18px!important;
        border: none;
        background: transparent;
        color: #999999;
        cursor: pointer;
        border: 1px solid #e0e0e0!important;
    }

    .panel_hidden {
        width: 0 !important;
        opacity: 0 !important;
        transition: width 0.5s, opacity 0.4s;
        display: none !important;
    }



    .comuncations_wrap {
        margin-left: -15px;
    }

    .callapps-sidemenu__container {
        border: 1px solid #ccc;
    }

  

    .flesh_counters {
        background: #fbad16;
        border-radius: 50%;
        color: #FFF !important;
        min-width: 15px;
        min-height: 15px;
        text-align: center;
        margin-top: -15px;
        margin-left: 5px;
    }

    .flex {
        display: flex;
    }

    .clickmetostart {
        cursor: pointer;
    }

    .clickmetostart {
        cursor: pointer;
    }

    .fixed-sidebar-left {
        position: relative;
        top: 79px ! important;
        left: 0;
        width: 236px;
        margin-left: 0;
        bottom: 0;
        z-index: 1;
        border-right: none!important;
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }
    <?php
    if($_GET['pg'] == 14){
        echo '
        .pull-left img {
            vertical-align: middle;
            margin-bottom: 5px;
        }
        ';
    }
    ?>
    
    .fixed-sidebar-left .right-nav-text {
        opacity: 1;
        -webkit-transition: opacity 0.2s ease 0s;
        -moz-transition: opacity 0.2s ease 0s;
        transition: opacity 0.2s ease 0s;
        margin-left: 20px;
        font-size: 13px;
        font-family: BPG;
        line-height: 46px;
        /* font-family: noto-light, lexend-regular; */
    }

    .fixed-sidebar-left .side-nav {
        border: none;
        height: 100%;
        width: 100%;
        position: relative;
        border-radius: 0;
        margin: 0;
        overflow-y: auto;
        overflow-x: hidden;
        background-color: #fff;
        backface-visibility: hidden;
    }

    .fixed-sidebar-left .side-nav li {
        width: 225px;
    }

    .fixed-sidebar-left .side-nav li.navigation-header>span,
    .fixed-sidebar-left .side-nav li.navigation-header>i {
        padding: 11px 15px;
    }

    .fixed-sidebar-left .side-nav li.navigation-header>span {
        font-size: 11px;
        display: block;
        text-transform: uppercase;
        font-weight: 500;
        color: #dc0030;
    }

    .fixed-sidebar-left .side-nav li.navigation-header>i {
        display: none;
        font-size: 13px;
    }

    .fixed-sidebar-left .side-nav li a[aria-expanded="true"] {
        background: rgba(33, 33, 33, 0.05);
    }

    .fixed-sidebar-left .side-nav li a[aria-expanded="true"]>i {
        color: #e2724c;
    }

    .fixed-sidebar-left .side-nav li a[aria-expanded="true"] .pull-right i:before {
        content: '\f2f8';
    }

    .fixed-sidebar-left .side-nav li a {
        width: 100%;
        text-transform: capitalize;
        -webkit-transition: 0.3s ease;
        -moz-transition: 0.3s ease;
        transition: 0.3s ease;
    }

    .side-nav {
        font-size: 13px
    }

    .side-nav a:hover {
        text-decoration: none;
    }

    .fixed-sidebar-left .side-nav li a .pull-left>i {
        font-size: 18px;
        width: 18px;
    }

    .fixed-sidebar-left .side-nav li a .pull-right i {
        color: #878787;
        font-size: 20px;
    }

    .fixed-sidebar-left .side-nav>li>a {
        color: #212121;
        padding: 0px 12px;
        line-height: 40px;
    }

    .fixed-sidebar-left .side-nav>li>a img {
        width: 18px;
    }

    .fixed-sidebar-left .side-nav>li>a:hover {
        background: rgba(33, 33, 33, 0.05);
    }

    .fixed-sidebar-left .side-nav>li>a:focus,
    .fixed-sidebar-left .side-nav>li>a:active {
        background: rgba(33, 33, 33, 0.05);
        color: #212121;
    }

    .fixed-sidebar-left .side-nav>li>a:focus>i,
    .fixed-sidebar-left .side-nav>li>a:active>i {
        color: #e2724c;
    }

    .fixed-sidebar-left .side-nav>li a.active-page {
        background: rgba(33, 33, 33, 0.05);
    }

    .fixed-sidebar-left .side-nav>li>ul>li a {
        padding: 7px 15px 7px 51px;
        display: block;
        color: #878787;
        /* font-family: noto-light, lexend-regular!important; */
    }

    .fixed-sidebar-left .side-nav>li>ul>li a:hover,
    .fixed-sidebar-left .side-nav>li>ul>li a:focus,
    .fixed-sidebar-left .side-nav>li>ul>li a:active {
        background: rgba(33, 33, 33, 0.05);
        color: #878787;
    }

    .fixed-sidebar-left .side-nav>li>ul>li>ul>li a {
        padding-left: 62px;
        color: #878787;
    }

    .fixed-sidebar-left .side-nav>li>ul>li>ul>li a:hover,
    .fixed-sidebar-left .side-nav>li>ul>li>ul>li a:focus,
    .fixed-sidebar-left .side-nav>li>ul>li>ul>li a:active {
        color: #878787;
    }

    .fixed-sidebar-left .side-nav>li>a.active {
        background: #dc0030;
        color: #fff;
    }

    .fixed-sidebar-left .side-nav>li>a.active>i {
        color: #fff;
    }

    .fixed-sidebar-left .side-nav>li>a.active .pull-right>i {
        color: #fff;
    }

    .fixed-sidebar-left .side-nav>li ul.collapse li a.active {
        color: #212121;
    }

    a.toggle-left-nav-btn,
    a#toggle_mobile_nav,
    a#toggle_mobile_search {
        color: #878787;
        position: relative;
        line-height: 66px;
    }

    a.toggle-left-nav-btn i,
    a#toggle_mobile_nav i,
    a#toggle_mobile_search i {
        font-size: 21px;
        vertical-align: middle;
    }

    a#toggle_mobile_nav {
        display: inline-block;
    }

    .top-fixed-nav .page-wrapper {
        margin-left: 0;
        padding-top: 134px;
    }

    .top-fixed-nav .fixed-sidebar-left {
        width: 100%;
        bottom: inherit;
    }

    .top-fixed-nav .fixed-sidebar-left:hover {
        width: 100%;
    }

    .top-fixed-nav .fixed-sidebar-left:hover .collapse.in {
        display: none;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv {
        overflow: visible !important;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv .slimScrollBar {
        display: none !important;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul {
        overflow: visible !important;
        width: 100% !important;
        height: auto !important;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul>li {
        width: auto;
    }

    li[loading]{
        text-align: center;
        padding: 6px;
        background-color: #3f51b585;
        color: #fff;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul>li>a {
        text-align: center;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul>li>a>i {
        display: block;
        margin-bottom: 10px;
        margin-right: 0 !important;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul>li>a>.pull-right {
        display: none;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul li {
        position: relative;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul .collapse-level-1 {
        background: #ebebeb;
        height: auto !important;
        left: 0;
        position: absolute !important;
        z-index: 1;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul .collapse-level-1.two-col-list {
        width: 450px;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul .collapse-level-1 li {
        float: left;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul .collapse-level-1 li>a i:before {
        content: "\f105";
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul .collapse-level-1 li>ul {
        background: #ebebeb;
        left: 225px;
        position: absolute;
        top: 0;
        z-index: 1;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul>li:hover>ul {
        display: block;
    }

    .top-fixed-nav .fixed-sidebar-left>.slimScrollDiv>ul>li>ul>li:hover>ul {
        display: block;
    }

    .top-fixed-nav.slide-nav-toggle .page-wrapper {
        margin-left: 0;
        padding-top: 60px;
    }

    .top-fixed-nav.slide-nav-toggle .fixed-sidebar-left {
        top: -14px;
        width: 100%;
    }

    .top-fixed-nav.slide-nav-toggle .fixed-sidebar-left:hover {
        width: 100%;
    }

    .scrollable-nav {
        position: relative;
    }

    .scrollable-nav .fixed-sidebar-left {
        position: absolute;
        left: 0t;
    }

    .wrapper {
        background: #fff;
    }

    .wrapper.no-transition .fixed-sidebar-left,
    .wrapper.no-transition .page-wrapper {
        /*Only for Setting Panel*/
        -webkit-transition: all 0s ease;
        -moz-transition: all 0s ease;
        transition: all 0s ease;
    }

    .wrapper.box-layout {
        max-width: 1400px;
        margin: 0 auto;
    }

    .wrapper.box-layout .navbar-fixed-top {
        margin: 0 auto;
        max-width: 1400px;
    }

    .wrapper.box-layout .fixed-sidebar-left {
        left: auto;
    }

    .wrapper.box-layout .fixed-sidebar-right {
        width: 0;
        overflow: hidden;
    }

    .wrapper.box-layout .fixed-sidebar-right .right-sidebar {
        width: 300px;
    }

    .wrapper.box-layout.open-right-sidebar .fixed-sidebar-right {
        margin-right: -300px;
        width: 300px;
    }

    .wrapper.box-layout.top-fixed-nav .fixed-sidebar-left {
        max-width: 1300px;
    }

    .wrapper.darkmode .card-view {
        background: #212121;
    }

    .wrapper.theme-1-active .navbar.navbar-inverse,
    .wrapper.theme-1-active .fixed-sidebar-left .side-nav {
        background: #fff;
    }

    .wrapper.theme-2-active .navbar.navbar-inverse,
    .wrapper.theme-2-active .navbar.navbar-inverse.navbar-fixed-top .mobile-only-brand {
        background: #eaedee;
    }

    .wrapper.theme-2-active .fixed-sidebar-left .side-nav {
        background: #fff;
    }

    .wrapper.theme-3-active .navbar.navbar-inverse,
    .wrapper.theme-3-active .navbar.navbar-inverse.navbar-fixed-top .mobile-only-brand {
        background: #464341;
    }

    .wrapper.theme-3-active .fixed-sidebar-left .side-nav {
        background: #fff;
    }

    .wrapper.theme-4-active .navbar.navbar-inverse,
    .wrapper.theme-4-active .navbar.navbar-inverse.navbar-fixed-top .mobile-only-brand {
        background: #212121;
    }

    .wrapper.theme-4-active .fixed-sidebar-left .side-nav {
        background: #333;
    }

    .wrapper.theme-5-active .navbar.navbar-inverse,
    .wrapper.theme-5-active .navbar.navbar-inverse.navbar-fixed-top .mobile-only-brand {
        background: #284152;
    }

    .wrapper.theme-5-active .fixed-sidebar-left .side-nav {
        background: #375468;
    }

    .wrapper.theme-6-active .navbar.navbar-inverse,
    .wrapper.theme-6-active .navbar.navbar-inverse.navbar-fixed-top .mobile-only-brand {
        background: #351e27;
    }

    .wrapper.theme-6-active .fixed-sidebar-left .side-nav {
        background: #462b36;
    }

    .wrapper.theme-4-active .fixed-sidebar-left,
    .wrapper.theme-5-active .fixed-sidebar-left,
    .wrapper.theme-6-active .fixed-sidebar-left {
        border-right: 1px solid rgba(255, 255, 255, 0.1);
    }

    .wrapper.theme-4-active .fixed-sidebar-left .side-nav>li>a,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav>li>a,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav>li>a {
        color: #fff;
    }

    .wrapper.theme-4-active .fixed-sidebar-left .side-nav>li>a:hover,
    .wrapper.theme-4-active .fixed-sidebar-left .side-nav>li>a:focus,
    .wrapper.theme-4-active .fixed-sidebar-left .side-nav>li>a:active,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav>li>a:hover,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav>li>a:focus,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav>li>a:active,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav>li>a:hover,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav>li>a:focus,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav>li>a:active {
        color: #fff;
    }

    .wrapper.theme-4-active .fixed-sidebar-left .side-nav li a:hover,
    .wrapper.theme-4-active .fixed-sidebar-left .side-nav li a:focus,
    .wrapper.theme-4-active .fixed-sidebar-left .side-nav li a:active,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav li a:hover,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav li a:focus,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav li a:active,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav li a:hover,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav li a:focus,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav li a:active {
        background: rgba(255, 255, 255, 0.05);
    }

    .wrapper.theme-4-active .fixed-sidebar-left .side-nav li a.active-page,
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav li a.active-page,
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav li a.active-page {
        background: rgba(255, 255, 255, 0.05);
    }

    .wrapper.theme-4-active .fixed-sidebar-left .side-nav li a[aria-expanded="true"],
    .wrapper.theme-5-active .fixed-sidebar-left .side-nav li a[aria-expanded="true"],
    .wrapper.theme-6-active .fixed-sidebar-left .side-nav li a[aria-expanded="true"] {
        background: rgba(255, 255, 255, 0.05);
    }

    .wrapper.theme-4-active .fixed-sidebar-left hr.light-grey-hr,
    .wrapper.theme-5-active .fixed-sidebar-left hr.light-grey-hr,
    .wrapper.theme-6-active .fixed-sidebar-left hr.light-grey-hr {
        border-color: rgba(255, 255, 255, 0.1);
    }

    .wrapper.theme-3-active .navbar.navbar-inverse.navbar-fixed-top,
    .wrapper.theme-4-active .navbar.navbar-inverse.navbar-fixed-top,
    .wrapper.theme-5-active .navbar.navbar-inverse.navbar-fixed-top,
    .wrapper.theme-6-active .navbar.navbar-inverse.navbar-fixed-top {
        border-bottom: 1px solid rgba(255, 255, 255, 0.1);
    }

    .wrapper.theme-3-active .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text,
    .wrapper.theme-4-active .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text,
    .wrapper.theme-5-active .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text,
    .wrapper.theme-6-active .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text {
        color: #fff;
    }

    .wrapper.theme-3-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:active,
    .wrapper.theme-3-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:hover,
    .wrapper.theme-3-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:focus,
    .wrapper.theme-4-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:active,
    .wrapper.theme-4-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:hover,
    .wrapper.theme-4-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:focus,
    .wrapper.theme-5-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:active,
    .wrapper.theme-5-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:hover,
    .wrapper.theme-5-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:focus,
    .wrapper.theme-6-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:active,
    .wrapper.theme-6-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:hover,
    .wrapper.theme-6-active .navbar.navbar-inverse.navbar-fixed-top .nav>li>a:focus {
        color: #fff;
    }

    .wrapper.theme-1-active.pimary-color-red .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-red .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-red .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-red .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-red .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-red .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #dc0030;
    }

    .wrapper.theme-1-active.pimary-color-red .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-red .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-red .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-red .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-red .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-red .fixed-sidebar-left .side-nav>li>a.active {
        background: #dc0030;
    }

    .wrapper.theme-1-active.pimary-color-blue .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-blue .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-blue .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-blue .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-blue .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-blue .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #0f4fa8;
    }

    .wrapper.theme-1-active.pimary-color-blue .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-blue .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-blue .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-blue .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-blue .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-blue .fixed-sidebar-left .side-nav>li>a.active {
        background: #0f4fa8;
    }

    .wrapper.theme-1-active.pimary-color-green .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-green .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-green .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-green .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-green .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-green .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #09a275;
    }

    .wrapper.theme-1-active.pimary-color-green .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-green .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-green .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-green .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-green .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-green .fixed-sidebar-left .side-nav>li>a.active {
        background: #09a275;
    }

    .wrapper.theme-1-active.pimary-color-yellow .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-yellow .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-yellow .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-yellow .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-yellow .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-yellow .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #f2b701;
    }

    .wrapper.theme-1-active.pimary-color-yellow .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-yellow .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-yellow .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-yellow .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-yellow .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-yellow .fixed-sidebar-left .side-nav>li>a.active {
        background: #f2b701;
    }

    .wrapper.theme-1-active.pimary-color-pink .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-pink .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-pink .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-pink .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-pink .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-pink .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #b10058;
    }

    .wrapper.theme-1-active.pimary-color-pink .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-pink .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-pink .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-pink .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-pink .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-pink .fixed-sidebar-left .side-nav>li>a.active {
        background: #b10058;
    }

    .wrapper.theme-1-active.pimary-color-orange .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-orange .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-orange .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-orange .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-orange .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-orange .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #e57d04;
    }

    .wrapper.theme-1-active.pimary-color-orange .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-orange .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-orange .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-orange .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-orange .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-orange .fixed-sidebar-left .side-nav>li>a.active {
        background: #e57d04;
    }

    .wrapper.theme-1-active.pimary-color-gold .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-gold .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-gold .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-gold .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-gold .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-gold .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #c4996d;
    }

    .wrapper.theme-1-active.pimary-color-gold .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-gold .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-gold .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-gold .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-gold .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-gold .fixed-sidebar-left .side-nav>li>a.active {
        background: #c4996d;
    }

    .wrapper.theme-1-active.pimary-color-silver .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-2-active.pimary-color-silver .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-3-active.pimary-color-silver .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-4-active.pimary-color-silver .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-5-active.pimary-color-silver .fixed-sidebar-left .side-nav li.navigation-header>span,
    .wrapper.theme-6-active.pimary-color-silver .fixed-sidebar-left .side-nav li.navigation-header>span {
        color: #83878d;
    }

    .wrapper.theme-1-active.pimary-color-silver .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-2-active.pimary-color-silver .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-3-active.pimary-color-silver .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-4-active.pimary-color-silver .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-5-active.pimary-color-silver .fixed-sidebar-left .side-nav>li>a.active,
    .wrapper.theme-6-active.pimary-color-silver .fixed-sidebar-left .side-nav>li>a.active {
        background: #83878d;
    }

    .slide-nav-toggle .navbar.navbar-inverse.navbar-fixed-top .nav-header {
        width: 44px;
    }

    .slide-nav-toggle .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text {
        opacity: 0;
        visibility: hidden;
    }

    .slide-nav-toggle.sidebar-hover .navbar.navbar-inverse.navbar-fixed-top .nav-header {
        width: 225px;
    }

    .slide-nav-toggle.sidebar-hover .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text {
        opacity: 1;
        visibility: visible;
    }

    .slide-nav-toggle .fixed-sidebar-left {
        width: 54px;
    }

    .slide-nav-toggle .fixed-sidebar-left .side-nav li.navigation-header>span {
        display: none;
    }

    .slide-nav-toggle .fixed-sidebar-left .side-nav li.navigation-header>i {
        display: block;
    }

    .slide-nav-toggle .fixed-sidebar-left .collapse.in {
        display: none;
    }

    .slide-nav-toggle .fixed-sidebar-left:hover {
        width: 225px;
    }

    .slide-nav-toggle .fixed-sidebar-left:hover .side-nav li.navigation-header>span {
        display: block;
    }

    .slide-nav-toggle .fixed-sidebar-left:hover .side-nav li.navigation-header>i {
        display: none;
    }

    .slide-nav-toggle .fixed-sidebar-left:hover .collapse.in {
        display: block;
    }

    .slide-nav-toggle .fixed-sidebar-left:hover .right-nav-text {
        opacity: 1;
    }

    .slide-nav-toggle .fixed-sidebar-left .right-nav-text {
        opacity: 0;
    }

    .slide-nav-toggle .page-wrapper {
        margin-left: 44px;
    }

    .arr-align {
        margin-top: 12px;
    }


/* Icon 3 */


#nav-icon1, #nav-icon2, #nav-icon3, #nav-icon4 {
  width: 19px;
  height: 19px;
  position: relative;
  margin: 2px auto;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .5s ease-in-out;
  -moz-transition: .5s ease-in-out;
  -o-transition: .5s ease-in-out;
  transition: .5s ease-in-out;
  cursor: pointer;
}

#nav-icon1 span, #nav-icon3 span, #nav-icon4 span {
  display: block;
  position: absolute;
  height: 2px;
  width: 100%;
  background: #5f5f5fc7;
  border-radius: 9px;
  opacity: 1;
  left: 0;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .25s ease-in-out;
  -moz-transition: .25s ease-in-out;
  -o-transition: .25s ease-in-out;
  transition: .25s ease-in-out;
}

#nav-icon3 span:nth-child(1) {
    top: 12px;
  width: 0%;
  left: 50%;
}

#nav-icon3 span:nth-child(2),#nav-icon3 span:nth-child(3) {
  top: 6px;
}

#nav-icon3 span:nth-child(4) {
    top: 12px;
  width: 0%;
  left: 50%;
}

#nav-icon3.open span:nth-child(1) {
  top:0;
  width: 100%;
 left: 0;
}

#nav-icon3.open span:nth-child(2) {
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
}
#nav-icon3 span:nth-child(2) {
    transform: rotate(45deg);
}

#nav-icon3.open span:nth-child(3) {
    -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
}
#nav-icon3 span:nth-child(3) {
    transform: rotate(-45deg);
}

#nav-icon3.open span:nth-child(4) {
 top: 12px;
 width: 100%;
 left: 0;
}



/* ///////////////////////////////////////////////////////////////////////////////////// */
#source_switch {
    right: 4px;
    margin-top: -25px;
    width: 62px;
    height: 24px;
}
#microphone_switch {
    right: 4px;
    margin-top: -25px;
    width: 62px;
    height: 24px;
}
  .toggle{
    position: absolute;
    border: 1px solid #444249;
    border-radius: 20px;
    -webkit-transition: border-color .6s ease-out;
    transition: border-color .6s ease-out;
    box-sizing: border-box;
    background: none;
}

.toggle.toggle-on{
    /* border-color: rgba(137, 194, 217, .4); */
    -webkit-transition: all .5s .15s ease-out;
    transition: all .5s .15s ease-out;
}

.toggle-button{
    position: absolute;
    top: 4px;
    width: 16px;
    bottom: 4px;
    right: 39px;
    background-color: #444249;
    border-radius: 19px;
    cursor: pointer;
    -webkit-transition: all .3s .1s, width .1s, top .1s, bottom .1s;
    transition: all .3s .1s, width .1s, top .1s, bottom .1s;
}

.toggle-on .toggle-button{
    top: 3px;
    width: 54px;
    bottom: 3px;
    right: 3px;
    border-radius: 23px;
    background-color: #444249;
    /* box-shadow: 0 0 16px #4b7a8d; */
    -webkit-transition: all .2s .1s, right .1s;
    transition: all .2s .1s, right .1s;
}


.toggle-text-on{
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    line-height: 23px;
    text-align: center;
    font-family: 'Quicksand', sans-serif;
    font-size: 12px;
    font-weight: bold;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    color: rgba(0,0,0,0);
}

.toggle-on .toggle-text-on{
    color: #fff;
    -webkit-transition: color .3s .15s;
    transition: color .3s .15s;
}

.toggle-text-off{
    position: absolute;
    top: 0;
    bottom: 0;
    right: 6px;
    line-height: 22px;
    text-align: center;
    font-family: 'Quicksand', sans-serif;
    font-size: 12px;
    font-weight: bold;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    cursor: pointer;
    color: #444249;
}

.toggle-on .toggle-text-off{
    color: rgba(0,0,0,0);
}

/* used for streak effect */
.glow-comp{
    position: absolute;
    opacity: 0;
    top: 10px;
    bottom: 10px;
    left: 10px;
    right: 10px;
    border-radius: 6px;
    background-color: rgba(75, 122, 141, .1);
    box-shadow: 0 0 12px rgba(75, 122, 141, .2);
    -webkit-transition: opacity 4.5s 1s;
    transition: opacity 4.5s 1s;
}

.toggle-on .glow-comp{
    opacity: 1;
    -webkit-transition: opacity 1s;
    transition: opacity 1s;
}

#change_project_status {
    margin-left: 13px;
}


.k-grid-header th .k-grid-filter {
    top: 7px;
    right: 0;
}
.k-grid-header .k-header {
    position: relative;
    vertical-align: middle !important;
    cursor: default;
    padding:0;
}

#notif_count1{
    position:absolute; 
    margin:-5px 0 0 0; 
}
#notif_count{
    border: 1px solid rgb(234, 46, 52);
    background-color: rgb(234, 46, 52);  
    color: white;
    font-size: 10px;
    margin-left: 12px; 
    margin-top:5px; 
    border-radius: 100%;
    padding: 3px;
}
#notification_board{
    height: 40vh;
    border:1px solid rgba(2, 38, 130, 0.46);
    background-color: #fff;
    top:7vh;    
    right:2.5%;
    width: 15%;
    position: absolute;
    overflow: scroll;
    overflow-x: hidden;
}
.notification_board_line{
    width: 100%;
    height: 7vh;
    border-bottom: 1px solid rgba(2,38,130,0.46);
    display: flex;
    justify-content: space-around;
    align-items: center;
}
.notification_board_line:hover{
    background-color: Lightgray;
    cursor:pointer;
}
.notification-element::before{
    display: none;
}
/* .k-reset {
    padding-top: 10px !important;
} */

</style>

<!-- <body  ng-app="myApp" ng-controller="myCtrl"> -->

<nav class="navigation">
    
    <div id="notification_board" style="display: none;">
    </div>
    <div class="navigation__container">


        <div class="navigation__container__logo logo_container">
            <div class="logo">Mepa</div>
            <!--<img src="media/logo/logo.png" style="width:100px;">-->
        </div>
        <div class="navigation__container__sitemap col-md-3">

            <a id="toggle_nav_btn" data-status="0" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);">
            <div id="nav-icon3">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            </a>
            
        </div>

        <div class="navigation__container__clock col-md-3" style="display:flex;">
            <p class="clock" id="clock" style="margin:0 auto;"> </p>
        </div>

        <div class="navigation__container__profile col-md-4" style="display:flex;">

            <div class="profile__notifications " style="margin-left:auto;" id="innerChat" data-notificationAmount="15" data-call-api-link="https://jsonplaceholder.typicode.com/users">
                <div class="navigation_dropdown__icon"></div>

            </div>

            <div class="profile__notifications navigation_dropdown notification_bell notification-element" data-notificationAmount=""  id="notifications_parent">
                <div class="navigation_dropdown__icon" id="notifications">
                    <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.0762 18.2673L17.2591 16.4665V9.41296C17.2591 9.41293 17.2591 9.4129 17.2591 9.41286C17.2593 7.92918 16.7077 6.49818 15.7108 5.3934C14.8145 4.40002 13.6148 3.73311 12.3029 3.48922V2.04519C12.3029 1.74829 12.1838 1.46447 11.9734 1.25591C11.7631 1.0475 11.4788 0.931222 11.1834 0.931222C10.8879 0.931222 10.6036 1.0475 10.3933 1.25591C10.1829 1.46447 10.0639 1.74829 10.0639 2.04519V3.48922C8.75195 3.73311 7.55229 4.40002 6.65592 5.3934C5.65903 6.49818 5.10741 7.92918 5.10767 9.41286C5.10767 9.4129 5.10767 9.41293 5.10767 9.41296V16.4665L3.29059 18.2673C3.14665 18.41 3.10305 18.6254 3.18018 18.8127C3.2573 19.0001 3.4399 19.1224 3.64255 19.1224H18.7242C18.9268 19.1224 19.1094 19.0001 19.1866 18.8127C19.2637 18.6254 19.2201 18.41 19.0762 18.2673ZM12.1456 1.0979L12.494 0.746402L12.1456 1.0979C12.3999 1.34992 12.5419 1.69076 12.5419 2.04519V2.91335C12.5419 3.13753 12.6912 3.3343 12.907 3.3948C15.5604 4.13846 17.4981 6.55354 17.4981 9.41286V16.1666C17.4981 16.2999 17.5514 16.4278 17.6462 16.5217L20.5004 19.3504H1.86632L4.72058 16.5217C4.81533 16.4278 4.86863 16.2999 4.86863 16.1666V9.41286C4.86863 8.05255 5.31636 6.72886 6.14462 5.64268C6.97294 4.55643 8.13682 3.76684 9.46017 3.39468C9.67582 3.33403 9.82481 3.13736 9.82481 2.91335V2.04519C9.82481 1.69076 9.96684 1.34991 10.2211 1.0979C10.4756 0.845741 10.8216 0.703278 11.1834 0.703278C11.5451 0.703278 11.8912 0.845741 12.1456 1.0979ZM13.5249 21.5783C13.3614 21.9145 13.1247 22.214 12.8284 22.4537C12.3646 22.829 11.7835 23.0343 11.1835 23.0342H11.1833C10.5832 23.0343 10.0022 22.829 9.53836 22.4537C9.24208 22.214 9.00533 21.9145 8.84187 21.5783H9.10722C9.27934 21.8954 9.5235 22.17 9.82187 22.3794C10.2203 22.6591 10.6961 22.809 11.1834 22.809C11.6707 22.809 12.1464 22.6591 12.5449 22.3794C12.8433 22.17 13.0874 21.8954 13.2595 21.5783H13.5249Z" fill="#00A57F" stroke="#00A57F" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <div class="profile__menu inner-chat-icon">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 1c-6.338 0-12 4.226-12 10.007 0 2.05.739 4.063 2.047 5.625.055 1.83-1.023 4.456-1.993 6.368 2.602-.47 6.301-1.508 7.978-2.536 9.236 2.247 15.968-3.405 15.968-9.457 0-5.812-5.701-10.007-12-10.007zm-5 11.5c-.829 0-1.5-.671-1.5-1.5s.671-1.5 1.5-1.5 1.5.671 1.5 1.5-.671 1.5-1.5 1.5zm5 0c-.829 0-1.5-.671-1.5-1.5s.671-1.5 1.5-1.5 1.5.671 1.5 1.5-.671 1.5-1.5 1.5zm5 0c-.828 0-1.5-.671-1.5-1.5s.672-1.5 1.5-1.5c.829 0 1.5.671 1.5 1.5s-.671 1.5-1.5 1.5z" />
                    </svg>
                </div>
                <!-- <ul class="navigation_dropdown__content">
                    <li class="links"><a href="">დროპის მენიუს ლინკი</a></li>
                    <li class="links"><a href="">დროპის მენიუს ლინკი</a></li>
                    <li class="links"><a href="">დროპის მენიუს ლინკი</a></li>
                    <li class="links"><a href="">დროპის მენიუს ლინკი</a></li>
                </ul> -->
            </div>
            <div id="custom_nav" class="profile__image" style="margin-right: -26px;">
            
            <div class="navigation_dropdown__icon navigation-user-image">
                     <?php 
                    $db      = new dbClass();
                    $userID = $_SESSION['USERID'];
                    $db->setQuery(" SELECT      `work_activities`.`name`,
                                                `work_activities`.`color`
                                    FROM
                                                `users`
                                    INNER JOIN  `work_activities` ON `users`.`work_activities_id` = `work_activities`.`id`
                                    WHERE       `users`.id = $userID
                                    ");
                        
                    $status = $db->getResultArray();

                    print '<span class="icon_status_color" style="background-color:'.$status[result][0][color].'"></span>';
                    
                   ?>
              
                </div>
                <ul class="navigation_dropdown__content" id="profile-menu">
                <li>
                    <a href="#"><i class="zmdi zmdi-account"></i><span>პროფილი</span></a>
                </li>
                <li class="divider"></li>
                <li data-status="0">
                    <a id="source_submenu_click" href="#"><img src="media/images/icons/comunication/comunication.png" /><span>კომ. არხები</span></a>
                    <ul class="main-submenu" id="source_submenu">
                  
                       
                    </ul>
                </li>
                <li class="divider"></li>
                <li data-status="0" id="status-place">
                    <a href="#">

                    <?php 

                    
                        $db->setQuery(" SELECT      `work_activities`.`name`,
                                                    `work_activities`.`color`
                                        FROM
                                                    `users`
                                        INNER JOIN  `work_activities` ON `users`.`work_activities_id` = `work_activities`.`id`
                                        WHERE       `users`.id = $userID
                                        ");
                            
                        $status = $db->getResultArray();

                        if($status[result][0]){
                        print '<x style="background-color:'.$status[result][0][color].'" ></x><span>'.$status[result][0][name].'</span>';
                    }else{
                        print '<x style="border: 1px solid #000;" ></x><span>სტატუსი</span>';

                    }
                   ?>

                  </a>
                    <ul class="main-submenu nowrap" id="aStatus">
                    <?php 
                        $db->setQuery("SELECT   `name`,
                                                `id`,
                                                `color`,
                                                `dnd`
                                    FROM   `work_activities`
                                    WHERE   actived = 1");
                        
                        $activities= $db->getResultArray();
                        foreach ($activities[result] as $activity){
                            print '
                                  <li id="'.$activity[id].'" data-dnd="'.$activity[dnd].'" data-id="'.$activity[id].'" data-color="'.$activity[color].'" data-name="'.$activity[name].'"><a href="#"><x style=" background-color:'.$activity[color].'" ></x><span>'.$activity[name].'</span></a></li>
                            ';
                        }
                    ?>

                    </ul>
                </li>
                <li class="divider"></li>
                <?php 
                        $db      = new dbClass();
                        $db->setQuery(" SELECT
                                            muted
                                        FROM
                                            `user_mute_check`
                                        WHERE `user_id` = $_SESSION[USERID]");
                        
                        $mic = $db->getResultArray();

                      
                            if($mic[result][0][muted] != 0){
                                $clsa = 'toggle-on';
                            }
                            print "
                            <li  class='mic'>
                    <a href='#'><i class='zmdi zmdi-volume-up'></i><span>ხმა</span></a>
                    <div class='toggle ".$clsa."' id='microphone_switch' data-status='".$mic[result][0][muted]."' data-id='mic'>
                                    <div class='toggle-text-off'>OFF</div>
                                    <div class='glow-comp'></div>
                                    <div class='toggle-button'></div>
                                    <div class='toggle-text-on'>ON</div>
                                </div>
                </li>
                         ";

                    ?>
                
                <li class="divider"></li>
                <li>
                    <a href="index.php?act=logout"><i class="zmdi zmdi-power"></i><span>გასვლა</span></a>
                </li>
                </ul>

        </div>
    </div>
</nav>


<div class="wrapper">
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">

        </ul>
    </div>
</div>


<aside class="callapps-sidemenu">
    <div class="callapps-sidemenu__container">
        <input id="imoriginalchat" type="hidden" value="0">
        <input id="comunication_table_check" type="hidden" value="0">
        <input id="dnd_on_local_status" type="hidden" value="0">
        <ul class="comuncations_wrap">
            <!-- <li class="comunication_bar" source="phone"> -->
                <!--  ar vici rato iyo data-call-api-link="https://jsonplaceholder.typicode.com/users"  <span data-source='fbm' id="phone_open" chat_id="0"  style="position:sticky; z-index: 100;cursor: pointer; display: block;  margin-top: -7px; background: none;"> -->
                <!-- <div class="call-icons" data-call-api-link="phone" style="background-image: url('media/images/icons/comunication/phone.png');margin-top: 3px;margin-bottom: 3px;" data-apiRoute="/somethin"></div>
                <span class='fabtext'></span>
            </li> -->
            <?php
            $db      = new dbClass();

            $user_id = $_SESSION['USERID'];

            $db->setQuery(" SELECT      `source`.`key`,
                                        `source`.`background_image_name`,
                                        `users_source_status`.`status`
                            FROM        `source`
                            LEFT JOIN   `users_source_status` ON `users_source_status`.`source_id` = `source`.`id` AND `users_source_status`.`user_id` = '$user_id'
                            WHERE       `source`.`actived` = 1 AND `source`.`key` != '0'
                            ORDER BY `source`.`position`");

            $comunicatin_result = $db->getResultArray();

            foreach ($comunicatin_result[result] as $source) {
                $id = '';
                if ($source[key] == 'chat') {
                    $id = 'chat_count_open';
                } else {
                    $id = $source[key] . '_chat_count_open';
                }
                if($source['status'] == 1 OR $source['key'] == 'phone'){
                    $data_comunicatin_source .= ' 
                        <li class="comunication_bar" source="' . $source[key] . '" >
                            <span data-source="fbm" id="' . $id . '" chat_id="0"  style="position:sticky; z-index: 100;cursor: pointer; display: block;  margin-top: -7px; background: none;">
                            <div class="call-icons" data-call-api-link="' . $source[key] . '"
                            style="background-image: url(\'media/images/icons/comunication/' . $source[background_image_name] . '\')"
                            data-apiRoute="/somethin"></div>
                            <span class=\'fabtext\' >0</span>
                        </li>';
                }else{

                    $data_comunicatin_source .= ' 
                        <li class="comunication_bar" source="' . $source[key] . '" disabled="true">
                            <span data-source="fbm" id="' . $id . '" chat_id="0"  style="position:sticky; z-index: 100;cursor: pointer; display: block;  margin-top: -7px; background: none;">
                            <div class="call-icons" data-call-api-link="' . $source[key] . '"
                            style="background-image: url(\'media/images/icons/comunication/' . $source[background_image_name] . '\')"
                            data-apiRoute="/somethin"></div>
                            <span class=\'fabtext\' >0</span>
                        </li>';

                }

            }

            echo $data_comunicatin_source;
            ?>

        </ul>
        <div id="flesh_panel" class="panel_hidden">


        </div>
    </div>
    <!-- chat_users -->
    <div class="callapps-info ">
        <div class="callapps-info__container">
            <div class="callapps-info__search">
                <div class="callapps-info__search__icon"></div>
                <input placeholder="ძიება" type="text" class="callapps-info__search__input">
            </div>

            <div class="callapps-info__main">
                <div class="callapps-info__user-and-groupes">
                    <!-- <ul>
                        <li class="chat_users">
                            <div class="chat_users__profile-image"></div>
                            <div class="chat_users__user-name">
                                <p>სახელი სახელაშვილი</p>
                            </div>
                            <div class="chat_users__last-message">
                                <p>ბოლოს რაც მოგწერე აუცილებლად ნახე ოკ ?</p>
                                <input type="hidden" name="user_id">
                            </div>
                        </li>
                    </ul> -->


                    <div class="textfield_outlined post_form__groupe">
                        <input name="added-group" type="text" placeholder=" ">
                        <p>ჯგუფის სახელი</p>
                    </div>
                    <p class="add_group_button">დამატება</p>



                    <ul class="add_to_group">

                    </ul>

                </div>


                <div class="chat-messages">
                    <div class="goBackTochatsBtn"><svg width="11" height="19" viewBox="0 0 11 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.78305 0.775604L1.2793 9.27935L9.78305 17.7831" stroke="#121212" />
                        </svg>
                    </div>
                    <div class="chat-messages__container">
                        <div class="chat-messages__container__messages">
                            <div class="messages">
                                <!-- <p class="loadMoreMessages" style="text-align:center">see more messages</p> -->
                            </div>
                            <div class="emojfiles">
                                <label><input class="fileupchat" style="position:absolute; top:200px;" id="fileupchat" type="file"></label>
                                <div id="fileupchatclip"><i style="font-size:19px; color:#00a57f; cursor:pointer; margin-bottom:1px;" class="fas fa-paperclip"></i></div>
                                <div style="position:unset; margin-left:10px;" class="emojiBtn"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm3.5 8c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm-7 0c.828 0 1.5.671 1.5 1.5s-.672 1.5-1.5 1.5-1.5-.671-1.5-1.5.672-1.5 1.5-1.5zm3.501 10c-2.801 0-4.874-1.846-6.001-3.566l.493-.493c1.512 1.195 3.174 1.931 5.508 1.931 2.333 0 3.994-.736 5.506-1.931l.493.493c-1.127 1.72-3.199 3.566-5.999 3.566z" />
                                    </svg></div>
                            </div>
                            <div class="sender_cotainer">
                                <textarea class="sending-input" name="" id=""></textarea>

                                <div class="sending-btn"><svg width="32" height="27" viewBox="0 0 32 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.985937 26.0247L31.1691 13.0829L0.985937 0.141113L0.971558 10.207L22.5412 13.0829L0.971558 15.9589L0.985937 26.0247Z" fill="white" />
                                    </svg></div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

</aside>

<body style="background-color: #fff;" ng-app="myApp" ng-controller="myCtrl">





    <!-- <aside class="popup_insert">
<div class="popup_insert__conntrollers">
    <div class="popup_insert__conntrollers__expand-btn"></div>
    <div class="popup_insert__conntrollers__close-btn"></div>
</div>

<div class="popup_insert__fomr_container">
    <form action="">
        <input type="text">
        <input type="text">
        <input type="text">
        <input type="text">
        <input type="text">
    </form>
</div>
</aside> -->

    <!--cominications -->


    <div id="crm_dialog" class="form-dialog" title="ატვირთული ბაზები"></div>
    <div id="crm_file_dialog" class="form-dialog" title="ფაილი"></div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form-sms" class="form-dialog" title="ახალი ესემესი">
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form-task" class="form-dialog" title="დავალება">
    </div>
    <!-- jQuery Dialog -->
    <div id="sms-templates" class="form-dialog" title="sms შაბლონები">
    </div>

    <!-- jQuery Dialog -->
    <div id="phone-directory-list" class="form-dialog" title="სატელეფონო ცნობარი">
    </div>

    <!-- jQuery Dialog -->
    <div id="last_calls" title="ბოლო ზარები">
    </div>
    <!-- jQuery Dialog -->
    <div id="audio_dialog" title="ჩანაწერი">
    </div>

    <!-- jQuery Dialog -->
    <div id="numlock_dialog" title="დაბლოკვა">
    </div>




    <!-- jQuery Dialog -->
    <div id="chat_close_dialog" class="form-dialog" title="დახურვა">
        <div id="dialog-form">
            <fieldset>
                გსურთ ჩატის დახურვა?
            </fieldset>
        </div>
    </div>

    <!-- jQuery Dialog -->
    <div id="show_hint" class="form-dialog" title="შეტყობინება">
        <div style="padding-top: 15px; text-align: center;" id="dialog-form">
            მონაცემები შენახულია!
        </div>
    </div>

    <!-- jQuery Dialog -->
    <div id="hint_close" class="form-dialog" title="შეტყობინება">
        <div style="padding-top: 15px; text-align: center;" id="dialog-form">
            გსურთ დიალოგის დახურვა ?
        </div>
    </div>

    <!-- jQuery Dialog -->
    <div id="hint_chat_close" class="form-dialog" title="შეტყობინება">
        <div style="padding-top: 15px; text-align: center;" id="dialog-form">
            ნამდვილად გსურთ ჩატის დახურვა დამუშავების გარეშე?
        </div>
    </div>


    <!-- jQuery Dialog -->
    <div id="add_blocked_ip" class="form-dialog" title="შავ სიაში დამატება">
        <div id="dialog-form">
            <fieldset>
                <table style="width:100%;margin-bottom:5px;" id="blocklogo">
                    <tr>
                        <td>
                            <label for="">კომენტარი</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea style="width: 99%;resize: vertical;" id="block_comment" class="idle"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
    <!-- jQuery Dialog -->
    <div id="ipaddr_dialog" class="form-dialog" title="შავ სიაში დამატება">
        <div id="dialog-form">
            <fieldset>
                <table style="width:100%;margin-bottom:5px;">
                    <tr>
                        <td>
                            <label for="">კომენტარი</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea style="width: 99%;resize: vertical;" id="ipaddr_block_comment" class="idle"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
    <div id="add-edit-form-answer" class="form-dialog" title="პასუხი">
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form-mail" class="form-dialog" title="ახალი E-mail">
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form-mail-shablon" class="form-dialog" title="E-mail შაბლონი">
    </div>
    <div id="phone-directory-list" class="form-dialog" title="სატელეფონო ცნობარი">
    </div>
    <div style='position:fixed;bottom: 5px;right: 5px;width: 40px;display: block;'>
        <!--        <input id="imoriginalchat" type="hidden" value="0">-->
        <!--        <input id="comunication_table_check" type="hidden" value="0">-->
        <!--        <input id="dnd_on_local_status" type="hidden" value="0">-->
        <!---->
        <!--        <span data-source='chat' id="chat_count_open" chat_id="0"  style="position:sticky; z-index: 100;cursor: pointer; display: block; padding: 7px; margin-top: 10px; background: none;">-->
        <!--		<img src="media/images/icons/comunication/Chat.png" height="35" width="35">-->
        <!--		<span class='fabtext' style='width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>-->
        <!--	</span>-->
        <!---->
        <!--        <span data-source='fbm' id="fb_chat_count_open" chat_id="0" style="position:sticky;z-index: 100;cursor: pointer; display: block; padding: 7px; margin-top: 10px; background: none;">-->
        <!--		<img src="media/images/icons/comunication/Messenger.png" height="35" width="35">-->
        <!--		<span class='fabtext'  style='width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>-->
        <!--	</span>-->
        <!--	<span data-source='site' id="site_chat_count_open" chat_id="0" style="position sticky; z-index: 100;;cursor: pointer; padding: 7px; margin-top: 10px; background: none;">-->
        <!--		<img src="media/images/icons/comunication/my_site.ico" height="35" width="35">-->
        <!--		<span class='fabtext'  style='display:none; width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 112px;    right: 0;'></span>-->
        <!--	</span>-->
        <!--        <span data-source='mail' id="mail_chat_count_open" chat_id="0" style="position:sticky;z-index: 100;cursor: pointer; display: block; padding: 7px; margin-top: 10px; background: none;">-->
        <!--		<img src="media/images/icons/comunication/E-MAIL.png" height="35" width="35">-->
        <!--		<span class='fabtext'  style='width: 8px;  padding-right:8px; z-index:1;  padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>-->
        <!--	</span>-->
        <!-- <span data-source='video' id="video_chat_count_open" chat_id="0" style="position:sticky;z-index: 100;cursor: pointer; display:none; padding: 7px; margin-top: 10px; background: none;">
    <img src="media/images/icons/comunication/Video.png" height="35" width="35">
    <span class='fabtext' style='width: 8px;  padding-right:8px; z-index:1;   padding: 3px;    text-align: center;    border: solid 1px #ccc;    font-size: 8px;   border-radius: 50%;    display: block;    position: absolute;    top: 0;    right: 0;'></span>
</span>-->
    </div>
    <a id="downloader" href="" style="display:none" target="_blank"></a>
    <div id="add_sport_dialog" class="form-dialog" title="სპორტი"></div>
    <div id="loading" style="z-index: 999999;padding: 160px 0;height: 100%;width: 100%;position: fixed;background: #f9f9f9;top: 0;">
        <div class="loading-circle-1">
        </div>
        <div class="loading-circle-2">
        </div>
    </div>
    <input id="check_state" type="hidden" value="1" />
    <input id="whoami" type="hidden" value="1" />
    <!-- jQuery Dialog -->
    <div id="loading_search_my_client_history" style="display:none; z-index: 999999;padding: 155px 0;height: 100%;width: 75%;position: fixed;top: 0;">
        <div class="loading-circle-1">
        </div>
        <div class="loading-circle-2">
        </div>
    </div>
    <div id="add-edit-form-comunications" class="form-dialog" title="შემომავალი ზარი">

    </div>
    <div id="info_project_dialog" class="form-dialog " title="პროექტები" style="margin-top:2px;">
        <div class="dialog_proect_select">
            <span>აირჩიე ცნობარი</span>
            <select id="project_select">
                <option disabled>აირჩიე</option>
                <option value="1">პროექტები</option>
            </select>
        </div>
    
        <div class="cnobari_projects">
            <div class="callapp_dialog_filters" style="padding: 5px 10px; display: none;">
                <button id="callapp_show_filter_button2" style="background: #fff;" myvar='0'>ფილტრი <div id="shh2" style="background: url('media/images/icons/lemons_filter.png') no-repeat;float: right; transform: rotate(0); margin-bottom: 0; ">
                    </div></button>
                <div class="callapp_filter_body2" style="width: 100%; height: auto">
                    <div style="width:100%;">
                        <table class="filter_multiselect_table">
                            <tr>
                                <td id="filter_multiselect"><span>გაფილტრე</span>
                                    <ul id="filter_multiselect_items" data-status="0">
                                        <input type="text" placeholder="ძებნა" id="multiselect_filter_search">
                                    </ul>
                                </td>
                                <td id="filter_multiselected_items"></td>
                            </tr>
                        </table>
                        <ul id="column_names"></ul>
                        <div style="overflow: auto;width: 100%;">
                            <table class="filter_multiselect_table2">
                                <tr id="dialog_filters"></tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="overflow: auto;">

                <div id="project_info_view_table" style="width: 100%; "></div>
                
                <!-- <table class="display" id="project_ifo_view_table" style="width: 100%; margin-left:0;">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 10%">N</th>
                            <th style="width: 20%;">სტატუსი</th>
                            <th style="width: 70%;">პროექტი</th>
                            <th style="width: 70%;">კატეგორია</th>
                            <th style="width: 70%;">ხედი</th>
                            <th style="width: 70%;">სექტორი</th>
                            <th style="width: 70%;">სართული</th>
                            <th style="width: 70%;">ბინის ნომერი</th>
                            <th style="width: 70%;">საძინებელი</th>
                            <th style="width: 70%;">საცხოვრებელი (კვ.მ)</th>
                            <th style="width: 70%;">საზაფხულო (კვ.მ)</th>
                            <th style="width: 70%;">აივანი</th>
                            <th style="width: 70%;">საერთო ფართი (კვ.მ)</th>
                            <th style="width: 70%;">ფასი ერთიანის</th>
                            <th style="width: 70%;">ფასი სულ</th>
                            <th style="width: 70%;">სტანდარტული წინასწარ</th>
                            <th style="width: 70%;">საკადასტრო კოდი</th>
                            <th class="check">#</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th><input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;" /></th>
                            <th>
                                <div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all-chat" name="check-all-chat" />
                                    <label for="check-all-chat"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table> -->
            </div>
        </div>
    </div>
    <!--end comunications-->


    <!-- JQuery Dialog -->

    <div id="crm-dialog" class="form-dialog" title="CRM"></div>


</body>

<script>

var menuURL     = "includes/menu.action.php";
var phoneDndUrl = "AsteriskManager/dndAction.php";
var x =1;


    $(document).on("click","#notifications_parent",function(){
        var counter = $("#notif_count").text();
        if(counter > 0){
            $("#notification_board").slideToggle(400);
            $.ajax({
                url: "server-side/call/inc_monitoring.action.php",
                data: { act: "get_list_notification" },
                    success: function (data) {
                        $("#notification_board").html("");
                        data.notification_data.forEach((e) => {
                            var mon_id = e.monitoring_id;
                            $("#notification_board").append("<a href='index.php?pg=148&id="+mon_id+"'> <div notification-id='" + mon_id + "'   class='notification_board_line' > <p>" + e.id + "</p> <p>" + e.date + "</p> <p>" + e.qula + "</p> </div> </a>");
                        });
                    }, 
                    error: function (e) { 
                        console.log("ajax error on #notification click");
                        console.log(e);
                    }
            });
        }
    });


    setInterval(
        function(){
            $.ajax({
                url: "server-side/call/inc_monitoring.action.php",
                data: {act:"get_notification_count"},
                success: function (data) {
                    if(data[0]["count"] == 0){
                        $("#notif_count1").remove();
                    }
                    else if (1 == x || data[0]["count"] != $("#notif_count").text()) {
                        $("#notif_count1").remove();
                        $("<span id='notif_count1' ><span id='notif_count' >" + data[0]["count"] + "</span></span>").insertBefore("#notifications");
                        x = 2
                    }
                }
            });
        }, 
    3000);

   $(document).on('click',"#profile-menu li",function(){
       if($(this).attr("data-status") == 0){
            $("#profile-menu li .main-submenu").css("display", "none")
            $("#profile-menu li ").attr("data-status", 0)

            $(this).children(".main-submenu").css("display", "block");
            $(this).attr("data-status", 1)
        }else{
            $(this).children(".main-submenu").css("display", "none");
            $(this).attr("data-status", 0)
        }

    })

    $(document).on("click","#custom_nav", function(e){
        $("#profile-menu").addClass("dropdown-droped")
   })
   $(document).on('click', '#source_submenu_click', function(){
        $("#source_submenu").empty();
        $.ajax({
            url: menuURL,
            data: "act=check_dnd",
            beforeSend: function(){
                
                $("#source_submenu").append("<li loading>Loading...</li>");
            },
            success: function(data) {
                $("li[loading]").remove();
                $("#source_submenu").html('');

                console.log(data);

                data.forEach(function(item){

                    console.log(item.status)
                    var cls = '';
                    if(item.status != 0){
                        cls = "toggle-on";
                    }
                    $("#source_submenu").append("<li data-status='0'><a href='#'><img src='media/images/icons/comunication/"+item.background_image_name+"' /><span>"+item.name+"</span></a><div class='toggle "+cls+"' id='source_switch' data-status='"+item.status+"' data-id='"+item.id+"'><div class='toggle-text-off'>OFF</div><div class='glow-comp'></div><div class='toggle-button'></div><div class='toggle-text-on'>ON</div></div></li>");

                });
            }
        });

   });
   $(document).on('click', "#aStatus li", function(){
        const id = $(this).attr('data-id');
        const color = $(this).attr('data-color');
        const name = $(this).attr('data-name');
        const dnd = $(this).attr('data-dnd');
        var statusBG = $('#status-place > a > x');
        var statusName = $('#status-place > a > span');
        var used_ext = localStorage.getItem('used_ext');
        
        if(used_ext != 999999 && used_ext != 0){
            $.ajax({
                url: menuURL,
                data: {
                    act: "activity_status",
                    status_id: id
                },
                beforeSend: function(){
                    $('#aStatus li[data-id='+id+']').css("background-color", "#ccc")
                },
                success: function(res){
                    $('#aStatus li[data-id='+id+']').css("background-color", "")
                    statusBG.css("background-color", color);
                    statusName.text(name);

                    $('.icon_status_color').css("background-color", color);
                    
                    $.ajax({
                        url: menuURL,
                        data: {
                            act: "source_status",
                            source_id: 1,
                            source_status: 0
                        },
                        success: function(res){
                        }
                    });

                    var dndact = 'dndoff'
                    if(dnd == 1){
                        dndact = 'dndon';
                    }
                    $.ajax({
                        url: phoneDndUrl,
                        data: {
                            action: dndact,
                            activity_id: id
                        },
                        success: function(res){
                        }
                    });
                    
                }
            });
        }
        else{
            alert("ამ ქმედებისთვის არჩეული უნდა გქონდეთ ექსთენშენი");
        }
    });

 
    
    $(document).on('click','.toggle',function(e){
        e.preventDefault(); 
       
        var used_ext = localStorage.getItem('used_ext');
        if(used_ext != 999999 && used_ext != 0){
            var id = $(this).attr("data-id");
            var status = $(this).attr("data-status");
            var dndStatus   = '';
            let setStatus;

            if(status == 0){
                $(this).attr("data-status", 1)
                $(this).addClass('toggle-on');
                setStatus = 1;
            }else{
                
                $(this).attr("data-status", 0);
                $(this).removeClass('toggle-on');
                setStatus = 0;
            }

            if(id == 1){
               
                $.ajax({
                    url: menuURL,
                    data: {
                        act: "source_status",
                        source_id: id,
                        source_status: setStatus
                    },
                    error: function(xhr, error){
                        $(this).attr("data-status", 0);
                        $(this).removeClass('toggle-on');
                    }
                });

                var status = $(this).attr("data-status");

                if(status == 0){
                    dndStatus = 'dndon';
                }
                if(status == 1){
                    dndStatus = 'dndoff';
                }
                $.ajax({
                    url: phoneDndUrl,
                    data: {
                        action: dndStatus
                    },
                    error: function(xhr, error){
                        $(this).attr("data-status", 0);
                        $(this).removeClass('toggle-on');
                    }
                });
            }
            else if(id != "mic"){
                $.ajax({
                    url: menuURL,
                    data: {
                        act: "source_status",
                        source_id: id,
                        source_status: setStatus
                    },
                    success: function(res){
                        $('.comunication_bar').map((i, x) => {
                           
                            if(x.getAttribute('source') == res.key){
                                if(setStatus == 0){
                                    x.children[0].children[0].style.filter = "grayscale(1)"
                                    x.children[0].children[1].style.backgroundColor = "#4B4B4B"
                                    x.setAttribute('disabled', true);
                                }else{
                                    x.children[0].children[0].style.filter = ""
                                    x.children[0].children[1].style.backgroundColor = ""
                                    x.removeAttribute('disabled');
                                }
                            }
                        })

                        $('.comunication_bar').removeClass('active_comunication');
                        $("#flesh_panel").addClass("panel_hidden");

                    },
                    error: function(xhr, error){
                        $(this).attr("data-status", 0);
                        $(this).removeClass('toggle-on');
                    }
                });
            }
            else{
                $.ajax({
                    url: menuURL,
                    data: {
                        act: "save_micr",
                        activeStatus: setStatus
                    },
                    success: function(data) {
                    console.log("saved")
                    },
                    error: function(xhr, err){
                        console.log(xhr, err)
                    }
                });
            }
        }
        else{
            alert("ამ ქმედებისთვის არჩეული უნდა გქონდეთ ექსთენშენი");
        }
        


});




</script>

<!-- <script defer type="text/javascript" src="dist/js/production/main.min.js"></script>   -->

</html>