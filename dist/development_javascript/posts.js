
const showCommentInput = () =>{
    const commentCollapseBtn = document.querySelectorAll(".commentCollapseBtn");
    const svgCommentIcon = document.querySelector(".comment-action-fill");



    if (commentCollapseBtn) {
        commentCollapseBtn.forEach(btn => {
            btn.addEventListener('click', () => {
                const newCommentElement = btn.parentElement.parentElement.parentElement.parentElement.querySelector(".post__add_comment");

                console.log(newCommentElement);
                svgCommentIcon.classList.toggle("fill-if-active-in-post");
                newCommentElement.classList.toggle("display-block");
            })
        });

    }
}



// setTimeout(() => {
//     const commentCollapseBtn = document.querySelectorAll(".commentCollapseBtn");
//     const svgCommentIcon = document.querySelector(".comment-action-fill");



//     if (commentCollapseBtn) {
//         commentCollapseBtn.forEach(btn => {
//             btn.addEventListener('click', () => {
//                 const newCommentElement = btn.parentElement.parentElement.parentElement.parentElement.querySelector(".post__add_comment");

//                 console.log(newCommentElement);
//                 svgCommentIcon.classList.toggle("fill-if-active-in-post");
//                 newCommentElement.classList.toggle("display-block");
//             })
//         });

//     }

// }, 1000);





const likeReaction = (reaction) =>{
    let likeType = reaction.dataset.liketype;
    let likeBtn = reaction.parentElement.parentElement;

    let post = likeBtn.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    let postId = post.dataset.postid;

    let likeSvg = likeBtn.querySelector("svg")


    console.log(likeBtn)
    console.log(likeType)


    let likedTypeAndPost = {
        act: "add_like",
        id: postId,
        type: likeType
    } 

    console.log(likedTypeAndPost);
    

    $.ajax({
        url: AjaxURL,
        data: likedTypeAndPost,
        success: function (likes) {
            let Likes = likes.likes[0];

            let happy = post.querySelector("[data-liketypeamount='happy']")
            let sad = post.querySelector("[data-liketypeamount='sad']")
            let neutral = post.querySelector("[data-liketypeamount='neutral']")
            let love = post.querySelector("[data-liketypeamount='love']")
            let amazed = post.querySelector("[data-liketypeamount='amazed']")
            
            happy.innerHTML = Likes.happy;
            sad.innerHTML = Likes.sad;
            neutral.innerHTML = Likes.neutral;
            love.innerHTML = Likes.love;
            amazed.innerHTML = Likes.amazed;
            


            likeSvg.classList.toggle("fill-if-active-in-post");
        }})
}




// const likeReaction = (post) =>{



// }