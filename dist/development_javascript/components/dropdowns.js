const navigationDropdownIcon = document.querySelectorAll(".navigation_dropdown__icon");
const navigationDropdownContent = document.querySelectorAll(".navigation_dropdown__content");

 

//! CHECK IF THERE EVEN IS A NVAIGTION BAR
if (navigationDropdownIcon) {
    
    //! GIVE EVERY ELEMENT OF ICONS "CLICK" EVENTLISTENER
    navigationDropdownIcon.forEach(icon => {
        icon.addEventListener('click', (event) => {
            let dropdownContent = icon.nextElementSibling;
            dropdownContent.classList.toggle("dropdown-droped");

            let otherContents = differentEelements(navigationDropdownContent, dropdownContent)

            otherContents.forEach(otherElement => {
                if (otherElement.classList.contains('dropdown-droped')) {
                    otherElement.classList.remove("dropdown-droped");
                }
            })


            //! REMOVE THE TOGGLE CLASS WHEN OTHER ELEMENTS ARE CLICKED
            let dropedElement = document.querySelector(".dropdown-droped");
            let body =  document.querySelector("body:not(.navigation)");

            if (dropedElement) {
                body.addEventListener('click', _ => {
                    dropedElement.classList.remove('dropdown-droped');
                })
            }

            //! PREVENT EVENT BUBBLING
            event.stopPropagation();
        })
    });


    // let otherElements = document.querySelector("body:not(.navigation)");

}


const postDropDown = (dropElement) =>{
    
    let collapsedElement = dropElement.nextElementSibling;

    dropElement.addEventListener('click', _ =>{
        collapsedElement.classList.toggle('dropdown-droped__post')    
    })
}




const differentEelements = (array, element) => {
    const others = [...array];
    const notselected = others.filter(el => el != element);
    return notselected;
}