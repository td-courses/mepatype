<?php

namespace IO\Routes\flashpanel;

use IO\Request;

class phoneTaken extends Request {
    
    public $response;
    protected $request;
    protected $class;

    private $route  = 'Queue';
    private $act    = 'flashPanel';
    private $req;


    public function RUN() {

        $this->req    = array('route' => $this->route, 'act' => $this->act);
        
        $this->class    = new Request(json_encode($this->req));
        $this->request  = $this->class->sendRequest();
        $this->response = $this->class->getResponse();

    }

    public function GET() {
        return $this->response;
    }
    
}