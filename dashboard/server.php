<?php

require 'vendor/autoload.php';
require 'Configs/config.php';

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use IO\Base;
use IO\Routes\Queue;

$loop = React\EventLoop\Factory::create();

$server = new Ratchet\App('localhost', 8080, '0.0.0.0', $loop);

$server->route('/', new Base($loop), array('*'));
// $server->route('/Queue', new Queue($loop), array('*'));

$server->run();


// $Queue = new Queue();

// $server = IoServer::factory(new HttpServer(new WsServer($Queue)), 8080);

// $server->loop->addPeriodicTimer(5, function () use ($Queue) {
    
//     foreach($Queue->clients as $client)
//     {
//         $client->send(json_encode($Queue->phoneTaken));    
//     }
//     });
    
    
// $server->run();
