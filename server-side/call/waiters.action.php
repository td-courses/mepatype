<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);



/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
require_once('../../includes/classes/dialog_automator.class.php');

global $db;
$db = new dbClass();



include('../../includes/classes/wsdl.class.php');

$action = $_REQUEST['act'];
$error	= '';
$filterID = '';
$data	= array();
$user_id	         = $_SESSION['USERID'];
//incomming
$incom_id	         = $_REQUEST['id'];
$crm_id	             = $_REQUEST['crm_id'];
$call_date			 = $_REQUEST['call_date'];
$hidden_user		 = $_REQUEST['hidden_user'];
$hidden_inc			 = $_REQUEST['hidden_inc'];
$imchat			     = $_REQUEST['imchat'];
$chat_id             = $_REQUEST['chat_id'];
$ipaddres            = $_REQUEST['ipaddres'];
$ii			         = $_REQUEST['ii'];
$phone				 = $_REQUEST['phone'];

// file
$rand_file			= $_REQUEST['rand_file'];
$file				= $_REQUEST['file_name'];
$main_status        = $_REQUEST['main_status']; //VERY VERY VERY VERYYYYY IMPORTANT
$optData = array();

switch ($action) {
	case 'get_sub_tabs':
		$status		= $_REQUEST['status'];

		$db->setQuery(" SELECT  info_status.id,
                                info_status.name,
                                (SELECT COUNT(*) FROM crm WHERE sub_status_id=info_status.id) AS 'cc'
                        FROM info_status
                        WHERE info_status.parent_id='$status'");
		$result = $db->getResultArray();
		$data['tabs'] = $result['result'];

		break;
	case 'get_add_page':
		$number		= $_REQUEST['number'];
		$ipaddres   = $_REQUEST['ipaddres'];
		$ab_pin     = $_REQUEST['ab_pin'];

		$page		= GetPage(getlastphone($number), $number, '', $ipaddres, $ab_pin, getOptionals($chat_id));
		$data		= array('page'	=> $page);

		break;

	case 'disable':
		$incom_id = $_REQUEST['id'];

		$db->setQuery("UPDATE `incomming_call`
						SET `noted`= 2
					   WHERE id=$incom_id");
		$db->execQuery();
		break;
	case 'get_signature':
		$site_id = $_REQUEST['site_id'];

		$db->setQuery(" SELECT signature 
                        FROM  `signature`
                        WHERE  account_id = '$site_id' AND actived = 1
                        LIMIT 1");

		$res = $db->getResultArray();

		$data = array('signature' => $res[result][0][signature]);

		break;

	case 'save_user_comunication':
		$user	             = $_SESSION['USERID'];
		$web_chat_checkbox   = $_REQUEST['web_chat_checkbox'];
		$site_chat_checkbox  = $_REQUEST['site_chat_checkbox'];
		$messanger_checkbox  = $_REQUEST['messanger_checkbox'];
		$mail_chat_checkbox  = $_REQUEST['mail_chat_checkbox'];
		$video_call_checkbox = $_REQUEST['video_call_checkbox'];

		$db->setQuery("SELECT id, user_id, chat, site, messenger, mail, cf, video 
                       FROM   users_cumunication
                       WHERE  user_id = '$user'");

		$check = $db->getNumRow();
		$req   = $db->getResultArray();
		$row   = $req[result][0];
		if ($check == 0) {
			if ($web_chat_checkbox != 1) {
				$web_chat_checkbox = 0;
			}

			if ($site_chat_checkbox != 1) {
				$site_chat_checkbox = 0;
			}

			if ($messanger_checkbox != 1) {
				$messanger_checkbox = 0;
			}

			if ($mail_chat_checkbox != 1) {
				$mail_chat_checkbox = 0;
			}

			if ($video_call_checkbox != 1) {
				$video_call_checkbox = 0;
			}

			$db->setQuery("INSERT INTO `users_cumunication` 
                                    (`user_id`, `chat`, `site`, `messenger`, `mail`, `video`) 
                              VALUES 
                                    ('$user', '$web_chat_checkbox', '$site_chat_checkbox', '$messanger_checkbox', '$mail_chat_checkbox', '$video_call_checkbox');");

			$db->execQuery();
		} else {
			if ($web_chat_checkbox != 1) {
				$web_chat_checkbox = 0;
			}

			if ($site_chat_checkbox != 1) {
				$site_chat_checkbox = 0;
			}

			if ($messanger_checkbox != 1) {
				$messanger_checkbox = 0;
			}

			if ($mail_chat_checkbox != 1) {
				$mail_chat_checkbox = 0;
			}

			if ($video_call_checkbox != 1) {
				$video_call_checkbox = 0;
			}

			$db->setQuery("UPDATE `users_cumunication`
                              SET `chat`      = '$web_chat_checkbox',
                    			  `site`     = '$site_chat_checkbox',
                    			  `messenger` = '$messanger_checkbox',
                    			  `mail`      = '$mail_chat_checkbox',
                    			  `video`     = '$video_call_checkbox'
                           WHERE  `id`        = '$row[id]'");
			$db->execQuery();
		}
		break;

	case 'check_user_comunication':
		$user = $_SESSION['USERID'];

		$db->setQuery("SELECT chat, site, messenger, mail, cf, video
	                   FROM   users_cumunication
	                   WHERE  user_id = '$user'");

		$check = $db->getNumRow();
		$req   = $db->getResultArray();
		$row   = $req[result][0];
		if ($check == 0) {
			$data = array(
				'web_chat_checkbox'   => 0,
				'site_chat_checkbox' => 0,
				'messanger_checkbox'  => 0,
				'mail_chat_checkbox'  => 0,
				'video_call_checkbox' => 0
			);
		} else {
			$data = array(
				'web_chat_checkbox'   => $row[chat],
				'site_chat_checkbox' => $row[site],
				'messanger_checkbox'  => $row[messenger],
				'mail_chat_checkbox'  => $row[mail],
				'video_call_checkbox' => $row[video]
			);
		}
		break;
	case 'load_dnd_data':

		$user_id = $_SESSION["USERID"];
		$get_options = $_REQUEST["getOptions"];

		$options = $get_options == "true" ? get_activitie_options() : "";
		$data = array('options' => $options);

		break;
	case 'focusin':
		$id = $_REQUEST["id"];
		$db->setQuery("UPDATE chat SET writes_op = 1 WHERE id = '$id'");
		$db->execQuery();
		break;
	case 'get_client_conversation_count':
		$user = $_SESSION['USERID'];
		// 	    $db->setQuery("SELECT   queries.id, 
		// 					            queries_history.id AS `queries_id`
		//                       FROM      queries
		//                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user'
		//                       WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
		// 	    $res = $db->getResultArray();
		// 	    foreach ($res[result] AS $req){
		// 	        $db->setQuery("INSERT INTO `queries_history`
		//                     	              (`user_id`, `datetime`, `queries_id`, `status`, `actived`)
		//                     	        VALUES
		//                     	              ('$user', NOW(), '$req[id]', 1, 1)");
		// 	        $db->execQuery();
		// 	    }

		$data = array('count' => 0);

		break;
	case 'focusout':
		$id = $_REQUEST["id"];
		$db->setQuery("UPDATE chat SET writes_op = 0 WHERE id = '$id'");
		$db->execQuery();
		break;
	case 'seen':
		$id = $_REQUEST["id"];
		$db->setQuery("UPDATE chat SET seen_op = 1 WHERE id = '$id'");
		$db->execQuery();
		break;
	case 'get_resp_user':

		$extention = $_REQUEST['extention'];

		$user_name = mysql_fetch_array(mysql_query("SELECT `id`, `name`
                                                     FROM   crystal_users
                                                     WHERE  last_extension = '$extention'
	                                                 AND    logged = 1"));
		$data = array('id' => $user_name[id], 'name' => $user_name[name]);
		break;

	case 'get_edit_page':
		$imby 		= $_REQUEST['imby'];
		$k_phone 	= $_REQUEST['k_phone'];

		$page		= GetPage(Getincomming($crm_id, $chat_id), $k_phone, $imby, '', '', getOptionals($chat_id));
		$data		= array('page'	=> $page);

		break;
	case 'chat_on_off':
		$_SESSION['chat_off_on'] = $_REQUEST['value'];
		$user	= $_SESSION['USERID'];
		if ($_REQUEST['value'] == 2) {
			mysql_query("UPDATE `crystal_users` SET `online`='1' WHERE (`id`='$user');");
		} else {
			mysql_query("UPDATE `crystal_users` SET `online`='0' WHERE (`id`='$user');");
		}

		break;
	case 'save_micr':
		$user	= $_SESSION['USERID'];
		$status = $_REQUEST['activeStatus'];
		$db->setQuery("SELECT * FROM `user_mute_check` WHERE user_id = $user");
		$check = $db->getNumRow();
		if ($check == 0) {
			$db->setQuery("INSERT INTO user_mute_check SET user_id='$user', `muted`='$status' ");
			$db->execQuery();
		} else {
			$db->setQuery("UPDATE `user_mute_check` SET `muted`='$status' WHERE `user_id`='$user';");
			$db->execQuery();
		}

		break;
	case 'logout':

		$c_date	= date('Y-m-d H:i:s');
		$user	= $_SESSION['USERID'];

		$rResult = mysql_fetch_array(mysql_query("SELECT  MIN(ABS(UNIX_TIMESTAMP(person_work_graphic.`end`)-UNIX_TIMESTAMP(NOW()))) AS end_date
												FROM person_work_graphic
												WHERE person_work_graphic.person_id='$user'"));

		if ($rResult['end_date'] < 15 && $rResult['end_date'] != null) {
			$data['logout']	= 1;
			$data['time']	= $rResult['end_date'];
			mysql_query("UPDATE `crystal_users` SET `online`='0' WHERE (`id`='$user');");
		} else {
			$data['logout']	= 0;
			$data['time']	= $rResult['end_date'];
		}

		break;
	case 'get_columns':

		$columnCount = 		$_REQUEST['count'];
		$cols[] =           $_REQUEST['cols'];
		$columnNames[] = 	$_REQUEST['names'];
		$operators[] = 		$_REQUEST['operators'];
		$selectors[] = 		$_REQUEST['selectors'];
		//$query = "SHOW COLUMNS FROM $tableName";
		//$db->setQuery($query,$tableName);
		//$res = $db->getResultArray();
		$f = 0;
		foreach ($cols[0] as $col) {
			$column = explode(':', $col);

			$res[$f]['Field'] = $column[0];
			$res[$f]['type'] = $column[1];
			$f++;
		}
		$i = 0;
		$columns = array();
		foreach ($res as $item) {
			$columns[$i] = $item['Field'];
			$i++;
		}


		$dat = array();
		$a = 0;
		for ($j = 0; $j < $columnCount; $j++) {
			if (1 == 2) {
				continue;
			} else {

				if ($operators[0][$a] == 1) $op = true;
				else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
				$op = false;
				if ($columns[$j] == 'id') {
					$width = "5%";
				} else {
					$width = 'auto';
				}
				if ($columns[$j] == 'inc_id' or $columns[$j] == 'id') {
					$hidden = true;
				} else {
					$hidden = false;
				}
				if ($res['data_type'][$j] == 'date') {
					$g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
				} else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
				{
					$g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
				} else {
					$g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
				}
				$a++;
			}
			array_push($dat, $g);
		}

		//array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

		$new_data = array();
		//{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
		for ($j = 0; $j < $columnCount; $j++) {
			if ($res['data_type'][$j] == 'date') {
				$new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
			} else {
				$new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
			}
		}


		$filtArr = array('fields' => $new_data);



		$kendoData = array('columnss' => $dat, 'modelss' => $filtArr);


		//$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

		$data = $kendoData;
		//$data = '[{"gg":"sd","ads":"213123"}]';

		break;
	case 'get_list':

		$columnCount = 		$_REQUEST['count'];
		$cols[]      =      $_REQUEST['cols'];
		$substatus   =      $_REQUEST['sub_status'];


		$start = $_REQUEST["start_time"];
		$end = $_REQUEST["end_time"];
		if($main_status == 263){
			$date_filt = "AND 		DATE(crm.datetime) BETWEEN '$start' AND '$end'";
		}
		$db->setQuery(" SELECT 		crm.id                   AS id,
										crm.incomming_request_id AS inc_id,
										crm.datetime             AS datetime,
										CASE
											WHEN NOT ISNULL(crm.user_id) THEN user_info_crm.name
											WHEN NOT ISNULL(incomming_call.user_id) THEN user_info_inc.name
											WHEN NOT ISNULL(asterisk_call_log.user_id) THEN user_info.name
											ELSE user_info_inc.name
										END AS `operator`,
										CASE
											WHEN NOT ISNULL(crm.crm_phone) THEN crm.crm_phone
											WHEN NOT ISNULL(incomming_call.phone) THEN incomming_call.phone
											WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IFNULL(incomming_call.phone,crm.crm_phone)
											WHEN NOT ISNULL(incomming_call.chat_id) THEN `chat`.phone
											WHEN NOT ISNULL(incomming_call.video_call_id) THEN incomming_call.phone
										END     AS phone,
										CASE
											WHEN NOT ISNULL(proc_name.value) THEN proc_name.value
											WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
											WHEN NOT ISNULL(incomming_call.chat_id) THEN `chat`.name
											WHEN NOT ISNULL(incomming_call.mail_chat_id) THEN `mail`.`sender_name`
											WHEN NOT ISNULL(incomming_call.video_call_id) THEN CONCAT(proc_name.value,' ',proc_lastname.value)
										END     AS `mom_auth`,
										IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),project_dat.name,project_dat2.name) AS 'project',
										IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id), mom_type_dat.name, mom_type_dat2.name)         AS `mom_type_dat`,
										user_info2.name           AS `resp_operator`
										
							FROM 		crm
							LEFT JOIN 	incomming_call ON incomming_call.id = crm.incomming_request_id
							LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
							LEFT JOIN	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
							LEFT JOIN 	user_info ON user_info.user_id = asterisk_call_log.user_id
							LEFT JOIN 	user_info AS user_info_crm ON user_info_crm.user_id = crm.user_id
							LEFT JOIN 	user_info AS user_info2 ON user_info2.user_id = crm.resp_user_id
							LEFT JOIN 	user_info AS user_info_inc ON user_info_inc.user_id = incomming_call.user_id
							
							LEFT JOIN   `chat` ON `chat`.id = incomming_call.chat_id
							LEFT JOIN	user_info AS user_chat ON user_chat.user_id = chat.last_user_id
							LEFT JOIN   fb_chat ON fb_chat.id = incomming_call.fb_chat_id
							LEFT JOIN   mail ON mail.id = incomming_call.mail_chat_id
							LEFT JOIN   viber_chat ON viber_chat.id = incomming_call.viber_chat_id
							LEFT JOIN 	incomming_call_info ON incomming_call_info.incomming_call_id = crm.incomming_request_id
							LEFT JOIN 	incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND
												incomming_request_processing.processing_setting_detail_id = 136
							LEFT JOIN 	incomming_request_processing AS `proc_name` 
										ON proc_name.incomming_request_id = incomming_call.id AND proc_name.processing_setting_detail_id = 127
							LEFT JOIN 	incomming_request_processing AS `proc_lastname`
										ON proc_lastname.incomming_request_id = incomming_call.id AND proc_lastname.processing_setting_detail_id = 128
							LEFT JOIN 	incomming_request_processing AS `proc_phone`
										ON proc_phone.incomming_request_id = incomming_call.id AND proc_phone.processing_setting_detail_id = 124
							LEFT JOIN 	incomming_request_processing AS `uwyeba` 
										ON uwyeba.incomming_request_id = incomming_call.id AND uwyeba.processing_setting_detail_id = 0 AND
										uwyeba.value = 1
							LEFT JOIN 	incomming_request_processing AS `project`
										ON project.incomming_request_id = incomming_call.id AND project.processing_setting_detail_id = 0 AND project.value = 2

							LEFT JOIN 	incomming_request_processing AS `mom_type` ON mom_type.incomming_request_id = incomming_call.id AND mom_type.processing_setting_detail_id = 0 AND mom_type.value = 4
							LEFT JOIN 	compaign_request_processing AS `mom_type2` ON mom_type2.base_id = outgoing_campaign_request_base.id AND mom_type2.processing_setting_detail_id = 0 AND mom_type2.value = 4

							LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
							LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
							LEFT JOIN 	compaign_request_processing AS `project2` ON project2.base_id = outgoing_campaign_request_base.id AND project2.processing_setting_detail_id = 0 AND project2.value = 2


							LEFT JOIN 	info_category AS `project_dat` ON project_dat.id = project.int_value
							LEFT JOIN 	info_category AS `project_dat2` ON project_dat2.id = project2.int_value
							LEFT JOIN 	mepa_project_types AS `mom_type_dat` ON mom_type_dat.id = mom_type.int_value
							LEFT JOIN 	mepa_project_types AS `mom_type_dat2` ON mom_type_dat2.id = mom_type2.int_value
							WHERE 		crm.actived = 1
							$date_filt
							AND 		crm.status_id = '$main_status'
							GROUP BY 	crm.id
							ORDER BY	crm.id DESC");
		$result = $db->getKendoList($columnCount, $cols);

		$data = $result;
		break;
	case 'get_list_history':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$start_check  = $_REQUEST['start_check'];
		$end_check    = $_REQUEST['end_check'];
		$phone        = $_REQUEST['phone'];
		$s_u_user_id  = $_REQUEST['s_u_user_id'];

		$filt = '';
		if ($phone == '' && $s_u_user_id == '') {
			$filt = "AND inc.client_user_id = 'fddhgdhgd'";
		}

		if ($phone != '' && $s_u_user_id != '') {
			$filt = "AND inc.client_user_id = '$s_u_user_id' OR inc.phone = '$phone'";
		}

		if ($phone != '' && $s_u_user_id == '') {
			$phone2 = substr($phone, 3);
			$filt = "AND (inc.phone = '$phone' OR inc.phone = '$phone2')";
		}

		if ($phone == '' && $s_u_user_id != '') {
			$filt = "AND inc.client_user_id = '$s_u_user_id'";
		}

		$db->setQuery(" SELECT    `inc`.`id` AS `task_id`,
                        		  `inc`.`date` AS `date`,
								  IF(asterisk_call_log.call_type_id = 1,'შემომავალი','გამავალი') AS `type`,
                        		   IF(`inc`.`phone` = '2004', `inc`.`ipaddres`, `inc`.`phone`) AS `phone`,
                        	       user_info.`name`,
								   proc_comment.value AS 'comment',
								  CASE
										WHEN incomming_request_processing.int_value = 1 THEN 'გადაცემულია გასარკვევად'
										WHEN incomming_request_processing.int_value = 2 THEN 'გარკვევის პროცესშია'
										WHEN incomming_request_processing.int_value = 3 THEN 'დასრულებულია'
									END AS `status`
                        FROM      `incomming_call` AS `inc`
                        JOIN		asterisk_call_log ON asterisk_call_log.id = inc.asterisk_incomming_id
						LEFT JOIN  	user_info ON `inc`.`user_id` = user_info.user_id
						LEFT JOIN  	user_info AS user_info_2 ON `asterisk_call_log`.`user_id` = user_info.user_id
                        LEFT JOIN  inc_status  ON inc.inc_status_id = inc_status.id
						LEFT JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = inc.id AND incomming_request_processing.processing_setting_detail_id = 136
						LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = inc.id AND proc_name.processing_setting_detail_id = 127
						LEFT JOIN   incomming_request_processing AS `proc_comment` ON proc_comment.incomming_request_id = inc.id AND proc_comment.processing_setting_detail_id = 140
						LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = inc.id AND proc_lastname.processing_setting_detail_id = 128
						LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = inc.id AND proc_phone.processing_setting_detail_id = 124
						LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = inc.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
						LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = inc.id AND project.processing_setting_detail_id = 0 AND project.value = 2
						WHERE      DATE(inc.date) BETWEEN '$start_check' AND '$end_check' $filt
						GROUP BY inc.id
						UNION ALL
												SELECT     `crm`.`id` AS `task_id`,
												`crm`.datetime AS `date`,
												'CRM' AS `type`,
												IF(`inc`.`phone` = '2004', `inc`.`ipaddres`, `inc`.`phone`) AS `phone`,
												user_info.`name`,
												proc_comment.value AS 'comment',
												info_status.`name` AS `status`
						FROM      `incomming_call` AS `inc`
						JOIN       crm ON crm.incomming_request_id = inc.id
						LEFT JOIN  user_info ON `crm`.`resp_user_id` = user_info.user_id
						LEFT JOIN  info_status  ON crm.status_id = info_status.id
						LEFT JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = inc.id AND incomming_request_processing.processing_setting_detail_id = 136
						LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = inc.id AND proc_name.processing_setting_detail_id = 127
						LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = inc.id AND proc_lastname.processing_setting_detail_id = 128
						LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = inc.id AND proc_phone.processing_setting_detail_id = 124
						LEFT JOIN   incomming_request_processing AS `proc_comment` ON proc_comment.incomming_request_id = inc.id AND proc_comment.processing_setting_detail_id = 140
						LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = inc.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
						LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = inc.id AND project.processing_setting_detail_id = 0 AND project.value = 2
						WHERE      DATE(`crm`.datetime) BETWEEN '$start_check' AND '$end_check' $filt
						");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_filter_details':
		$filter_name	= $_REQUEST['filterName'];

		$db->setQuery("	SELECT 	column_name, id
						FROM 	directory_column_titles 
						WHERE	title_geo = '$filter_name'
						ORDER BY id 
						LIMIT 1 ");

		$column_name = $db->getResultArray();
		$column = $column_name[result][0]['column_name'];
		$filterID = $column;

		$db->setQuery("	SELECT 	COUNT(id) AS `count`,  $column AS `name`
						FROM	directory__projects
						GROUP BY $column ");

		$data = $db->getResultArray();


		break;
	case 'get_list_info_project':
		$id          =      $_REQUEST['hidden'];
		$columnCount = 		$_REQUEST['count'];
		$json_data   = 		json_decode($_REQUEST['add'], true);
		$itemPerPage = 		$json_data['pageSize'];
		$skip        = 		$json_data['skip'];
		$cols[]      =      $_REQUEST['cols'];

		$db->setQuery("SELECT   incomming_call_info.id,
                			    'პროქტები' AS `name`,
								directory__projects.`status`,
                				CONCAT('<span class=\"kendo_hover_line\">პროექტი:</span> ', directory__projects.project, ' <span class=\"kendo_hover_line\">კატეგორია:</span> ', directory__projects.category, ' <span class=\"kendo_hover_line\">ხედი:</span> ', directory__projects.`view`, ' <span class=\"kendo_hover_line\">ნომერი:</span> ', directory__projects.number, ' <span class=\"kendo_hover_line\">სართული:</span> ', directory__projects.floor, ' <span class=\"kendo_hover_line\">სექტორი:</span> ', directory__projects.sector, ' <span class=\"kendo_hover_line\">საკადასტრო კოდი:</span> ', directory__projects.cadr_code, ' <span class=\"kendo_hover_line\">საზაფხულო ფართი:</span> ', directory__projects.saz_far, ' <span class=\"kendo_hover_line\">საცხოვრებელი ფართი:</span> ', directory__projects.sacx_far, ' <span class=\"kendo_hover_line\">ფასი:</span> ', directory__projects.price, ' <span class=\"kendo_hover_line\">აივანი:</span> ', directory__projects.balcony, ' <span class=\"kendo_hover_line\">საერთო ფართი:</span> ', directory__projects.saerto_far, ' <span class=\"kendo_hover_line\">სრული ფასი:</span> ', directory__projects.full_price, ' <span class=\"kendo_hover_line\">განვადების ფასი:</span> ', directory__projects.ganv_price, ' <span class=\"kendo_hover_line\">სტანდარტული წინასწარ:</span> ', directory__projects.stand_winaswar, ' <span class=\"kendo_hover_line\">სტატუსი:</span> ', directory__projects.`status`) AS `info`
                        FROM   `incomming_call_info`
                        JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
                        JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
                        WHERE  `incomming_call_info`.`actived` = 1 AND incomming_call.id = $id");

		$result = $db->getKendoList($columnCount, $cols);
		$data = $result;

		break;
	case 'get_list_project_filter':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		$filterArr	  = $_REQUEST['FilterArr'];
		$filterArrD	  = json_decode($filterArr);
		$id           = $_REQUEST['id'];

		$query = "	SELECT  `directory__projects`.`id`,
							`directory__projects`.`id`,
							directory__projects.status, 
							directory__projects.project, 
							directory__projects.category,
							directory__projects.`view`,  
							directory__projects.sector,
							directory__projects.floor, 
							directory__projects.number, 
							directory__projects.sleeping_room, 
							directory__projects.sacx_far, 
							directory__projects.saz_far, 
							directory__projects.balcony,
							directory__projects.saerto_far,
							directory__projects.full_price,
							directory__projects.ganv_price, 
							directory__projects.price, 
							directory__projects.stand_winaswar, 
							directory__projects.cadr_code 
							FROM 	`directory__projects`
							WHERE ";

		$count = count($filterArrD, COUNT_NORMAL);

		foreach ($filterArrD as $key => $d) {
			// $query = $d->columnName." = '".$d->filterValue."' ";
			$cart[] = array('Column' => $d->columnName, 'Value' => $d->filterValue);

			$QueryArray = [];
			foreach ($cart as $innerArray) {
				$key = array_search($innerArray['Column'], array_column($QueryArray, 'Column'));

				if ($key === false) {
					array_push($QueryArray, $innerArray);
				} else {
					$QueryArray[$key]['Value'] .= "', '" . $innerArray['Value'];
				}
			}
		}

		foreach ($QueryArray as $q => $d) {
			$columns = $d["Column"];
			$values = $d['Value'];
			if ($q == 0) {
				$query .= " `directory__projects`.`" . $columns . "` IN ('" . $values . "')";
			} else {
				$query .= " AND `directory__projects`.`" . $columns . "` IN ('" . $values . "')";
			}
		}




		$db->setQuery($query);

		$data = $db->getList(18, $hidden, 1);

		break;
	case 'get_list_project':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$id           = $_REQUEST['id'];

		$db->setQuery(" SELECT  `directory__projects`.`id`,
								`directory__projects`.`id`,
								directory__projects.status, 
								directory__projects.project, 
								directory__projects.category,
								directory__projects.`view`,  
								directory__projects.sector,
								directory__projects.floor, 
								directory__projects.number, 
								directory__projects.sleeping_room, 
								directory__projects.sacx_far, 
								directory__projects.saz_far, 
								directory__projects.balcony,
								directory__projects.saerto_far,
								directory__projects.full_price,
								directory__projects.ganv_price, 
								directory__projects.price, 
								directory__projects.stand_winaswar, 
								directory__projects.cadr_code 
						FROM 	`directory__projects`
                        WHERE 	`directory__projects`.`actived` = 1");

		$data = $db->getList($count, $hidden, 1);

		break;
	case 'add_project':
		$id      = $_REQUEST['id'];
		//$letters = json_decode('[' . $_REQUEST['lt'] . ']');
		$letters = explode(",", $_REQUEST['lt']);

		//$db->setQuery("DELETE FROM incomming_call_info WHERE incomming_call_id = $id");
		//$db->execQuery();

		foreach ($letters as $pr_id) {
			$db->setQuery("INSERT INTO `incomming_call_info`
                                      (`user_id`, `datetime`, `incomming_call_id`, `directory_tables_id`, `directory_row_id`, `actived`) 
                                VALUES 
                                      (1, NOW(), $id, 3, '$pr_id', 1);");
			$db->execQuery();
		}

		break;
	case 'get_list_client_order':
		$count                         = $_REQUEST['count'];
		$hidden                        = $_REQUEST['hidden'];
		$history_user_id               = $_REQUEST['history_user_id'];
		$incomming_call_id             = $_REQUEST['incomming_call_id'];
		$client_history_filter_date    = $_REQUEST['client_history_filter_date'];
		$client_history_filter_activie = $_REQUEST['client_history_filter_activie'];

		$db->setQuery("SELECT  id,
                			   production_id,
                			   title,
                			   create_date,
                			   prom_color,
                			   '' AS `moqmedeba`,
                			   block_date,
                			   '' AS `ar_vici`,
                			   '' AS `ar_vici1`,
                			   '' AS `ar_vici1`
                        FROM  `my_order`
                        WHERE  my_user_id = $history_user_id");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_list_client_transaction':
		$count                                  = $_REQUEST['count'];
		$hidden                                 = $_REQUEST['hidden'];

		$history_user_id                        = $_REQUEST['history_user_id'];
		$incomming_call_id                      = $_REQUEST['incomming_call_id'];
		$client_history_filter_site             = $_REQUEST['client_history_filter_site'];
		$client_history_filter_group            = $_REQUEST['client_history_filter_group'];
		$client_history_filter_transaction_type = $_REQUEST['client_history_filter_transaction_type'];

		$db->setQuery("SELECT  id,
                			   site_name,
                              `comment`,
                			  `name`,
                			  `product_id`,
                			  `money_before`,
                			  `money_after`,
                			  `quantity`,
                			  `expense`,
                			  `insert_date`
                        FROM  `my_transaction`
                        WHERE  my_user_id = $history_user_id");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_user_log':
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		$incomming_call_id = $_REQUEST['incomming_call_id'];
		$db->setQuery("SELECT `logs`.id,
                			  `logs`.date,
                			   IF(`logs`.`event`=1,'რედაქტირება','დამატება'),
                			   user_info.`name`,
                			    CASE
                					WHEN collumn = 'phone' THEN	'ტელეფონი'
                					WHEN collumn = 'cat_1' THEN 'კატეგორია'
                					WHEN collumn = 'cat_1_1' THEN 'ქვე კატეგორია1'
                					WHEN collumn = 'cat_1_1_1' THEN 'ქვე კატეგორია2'
                					WHEN collumn = 'call_content' THEN 'საუბრის შინაარსი'
                					WHEN collumn = 'inc_status' THEN 'რეაგირება'
                					WHEN collumn = 'client_user_id' THEN 'User ID'
                					WHEN collumn = 'client_pid' THEN 'საიდ. კოდი / პ.ნ'
                					WHEN collumn = 'client_name' THEN 'სახელი,გვარი / დასახელება'
                					WHEN collumn = 'client_mail' THEN 'ელ. ფოსტა'
                					WHEN collumn = 'client_satus' THEN 'სტატუსი'
                					WHEN collumn = 'client_sex' THEN 'სქესი'
									WHEN collumn = 'source' THEN 'ინფორმაციის წყარო'
                				END AS `collumt`,
                			   `logs`.new_value,
                			   `logs`.old_value
                        FROM   `logs`
                        JOIN   user_info ON user_info.user_id = `logs`.user_id
                        WHERE  `table` = 'incomming_call' AND row_id = '$incomming_call_id' AND new_value != '' ");

		$data = $db->getList($count, $hidden);

		break;
	case 'get_list_news':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$db->setQuery(" SELECT action.id,
                    	       action.start_date,
                    	       action.end_date,
                    	       action.`name`,
                              (SELECT GROUP_CONCAT(my_web_site.`name`) 
                               FROM  `action_site`
                               JOIN   my_web_site ON action_site.site_id = my_web_site.id
                               WHERE  action_site.action_id = action.id) AS `site`,
                    	       action.content
            	        FROM   action
            	        WHERE  action.actived=1 AND action.end_date >= DATE(CURDATE())");

		$data = $db->getList($count, $hidden, 1);

		break;
	case 'get_list_crm':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];

		$start_crm  = $_REQUEST['start_crm'];
		$end_crm    = $_REQUEST['end_crm'];
		$phone      = $_REQUEST['phone'];
		$pin        = $_REQUEST['pin'];

		$db->setQuery("SELECT 	   ar.id,
                                   ard.set_time,
                                   arb.number_8,
                                   arb.number_2,
            					   arb.number_3,
            					   arb.phone_number,
            					   arb.number_5,
                                   ar.comment_main,
                                   CONCAT(IFNULL(tr.`name`,''),IF(ISNULL(tr.`name`),'','/'),IFNULL(add_page_tree.`name`,'')) AS `status`
                        FROM 	   autocall_request ar 
                        left JOIN  autocall_request_details ard ON ar.id = ard.request_id
                        LEFT JOIN  autocall_request_base arb ON arb.request_id = ard.id
                        LEFT JOIN  add_page_tree ON arb.tree_id = add_page_tree.id
                        LEFT JOIN  add_page_tree AS `tr` ON add_page_tree.parent_id = tr.id
                        WHERE      ar.call_type_id = 4 AND ar.actived=1 AND ard.actived = 1 
						AND       ((arb.number_2 = '$pin' AND arb.number_2 != '')  OR (arb.phone_number = '$phone' AND arb.phone_number != ''))
                        AND        DATE(ard.set_time) BETWEEN '$start_crm' AND '$end_crm'");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_list_quest':
		$count = 		$_REQUEST['count'];
		$hidden = 		$_REQUEST['hidden'];



		$db->setQuery(" SELECT 	queries.id,
                                queries.datetime,
                                user_info.`name`,
								queries.`quest`,
                                queries.`answer`
					    FROM 	queries
                        LEFT JOIN user_info ON user_info.user_id = queries.user_id
					    WHERE 	queries.actived=1 ");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_list_mail':
		$count             = $_REQUEST['count'];
		$hidden            = $_REQUEST['hidden'];
		$incomming_call_id = $_REQUEST['incomming_id'];

		$db->setQuery(" SELECT  id,
                    	            `date`,
                    	            CONCAT('to: ',`address`,'<br>cc: ',cc_address,'<br> bcc: ',bcc_address) as address,
                    	            `body`,
                    	            if(`status`=3,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
            	            FROM   `sent_mail`
            	            WHERE   incomming_call_id = '$incomming_call_id' AND `status` != 1");

		$data = $db->getList($count, $hidden);
		break;
	case 'save_incomming':
		$incom_id     = $_REQUEST['id'];
		$id           = $_REQUEST['hidde_incomming_call_id'];
		$crm_id       = $_REQUEST['crm_id'];
		$user		  = $_SESSION['USERID'];
		$hidden_user  = $_REQUEST['hidden_user'];

		if ($id == '') {
			Addincomming($incom_id);
		} else {
			Saveincomming($incom_id, $crm_id);
		}

		break;
	case "set_user":
		$db->setQuery("UPDATE `chat` SET
					`last_user_id` = '$user_id',
			        `status`='2'
					WHERE  `id`='$_REQUEST[id]';");

		$db->execQuery();



		break;
	case 'move_incomming':
		$note = $_REQUEST['note'];

		note_incomming();

		break;
	case 'close_chat':
		$last_request_res = mysql_fetch_assoc(mysql_query(
			"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"
		));
		$timedifference = $last_request_res['timedifference'];
		$who_rejected = $last_request_res['who_rejected'];
		$query_addition = '';
		if ($timedifference >= 10 && $who_rejected == '') {
			$query_addition .= ", `who_rejected` = 0";
		} else if ($timedifference < 10 && $who_rejected == '') {
			$query_addition .= ", `who_rejected` = 1";
		}
		mysql_query("UPDATE `chat` SET
			        `status`='4'$query_addition
			        WHERE  `id`='$_REQUEST[chat_id]';");

		break;
	case 'black_list_add':

		num_block_incomming();

		break;
	case 'num_check':

		$phone = $_REQUEST['phone'];

		$check_num = mysql_num_rows(mysql_query("SELECT phone
											   FROM black_list
											   WHERE phone='$phone' AND actived=1
											   LIMIT 1"));
		if ($check_num == 0 && $phone != '2004') {
			$check = 1;
		} else {
			$check = 0;
		}
		$data = array('check' => $check);

		break;

	case 'black_list_add_chat':
		$user              = $_SESSION['USERID'];
		$source_id         = $_REQUEST['source_id'];
		$chat_block_reason = $_REQUEST['chat_block_reason'];
		$id                = $_REQUEST['id'];

		if ($source_id == 6) {
			$db->setQuery("SELECT fb_chat_id FROM incomming_call WHERE id = '$id'");
			$fb = $db->getResultArray();
			$db->setQuery("SELECT sender_id, sender_name FROM fb_chat WHERE id = '$fb[result][0][fb_chat_id]'");
			$sender = $db->getResultArray();
			$db->setQuery("SELECT id FROM `fb_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
			$check_block = $db->getResultArray();
			if ($check_block == 0) {
				mysql_query("INSERT INTO `fb_chat_black_list` 
                                        (`user_id`, `datetime`, `sender_id`, `name`, `reason`) 
                                  VALUES 
	                                    ('$user', NOW(), '$sender[result][0][sender_id]', '$sender[result][0][sender_name]', '$chat_block_reason');");

				mysql_query("UPDATE fb_chat SET status = 3 WHERE id = $fb[result][0][fb_chat_id]");
			} else {
				global $error;
				$error = 'მომხმარებელი უკვე დაბლოკილია';
			}
		} else if ($source_id == 7) {
			$db->setQuery("SELECT mail_chat_id FROM incomming_call WHERE id = '$id'");
			$mail = $db->getResultArray();
			$db->setQuery("SELECT sender_id FROM mail_chat WHERE id = '$mail[result][0][mail_chat_id]'");
			$sender = $db->getResultArray();
			$db->setQuery("SELECT id FROM `mail_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
			$check_block = $db->getNumRow();
			if ($check_block == 0) {
				mysql_query("INSERT INTO `mail_chat_black_list`
                    	                (`user_id`, `datetime`, `sender_id`, `reason`)
                    	          VALUES
	                                    ('$user', NOW(), '$sender[0][sender_id][sender_id]', '$chat_block_reason');");

				mysql_query("UPDATE mail_chat SET status = 3 WHERE id = $mail[0][sender_id][mail_chat_id]");
			} else {
				global $error;
				$error = 'მომხმარებელი უკვე დაბლოკილია';
			}
		} else if ($source_id == 9) {
			$db->setQuery("SELECT site_chat_id FROM incomming_call WHERE id = '$id'");
			$site = $db->getResultArray();
			$db->setQuery("SELECT sender_id, sender_name FROM site_chat WHERE id = '$site[result][0][site_chat_id]'");
			$sender = $db->getResultArray();
			$db->setQuery("SELECT id FROM `site_chat_black_list` WHERE sender_id = '$sender[result][0][sender_id]' AND actived = 1");
			$check_block = $db->getNumRow();
			if ($check_block == 0) {
				$db->setQuery("INSERT INTO `site_chat_black_list`
                    	                (`user_id`, `datetime`, `sender_id`, `name`, `reason`)
                    	          VALUES
	                                    ('$user', NOW(), '$sender[result][0][sender_id]', '$sender[result][0][sender_name]', '$chat_block_reason');");
				$db->execQuery();
				$db->setQuery("UPDATE site_chat SET status = 3 WHERE id = $site[result][0][site_chat_id]");
				$db->execQuery();
			} else {
				global $error;
				$error = 'მომხმარებელი უკვე დაბლოკილია';
			}
		}

		$data = array('check' => $check);

		break;
	case 'ipaddr_check':

		// 	    $ipaddres		= $_REQUEST['ipaddres'];
		// 	    $ipaddr_block_comment = $_REQUEST['ipaddr_block_comment'];
		// 	    mysql_close();
		// 	    mysql_connect('212.72.155.176','root','Gl-1114');
		// 	    mysql_select_db('webcall');
		// 	    $check_num=mysql_num_rows(mysql_query(" SELECT phone
		//                                     	        FROM blocked_ip
		//                                     	        WHERE ip='$ipaddres'
		//                                     	        LIMIT 1"));
		// 	    if ($check_num==0 && $ipaddres != '0') {
		// 	        mysql_query("INSERT INTO `blocked_ip`
		// 	                     (`clients_id`, `ip`, `comment`, `status`)
		//                          VALUES
		//                          ('7', '$ipaddres', '$ipaddr_block_comment', '1');");
		// 	        $check=1;
		// 	    }else {
		// 	        $check=0;
		// 	    }
		// 	    $data = array('check' => $check);

		break;

	case 'delete_file':
		$delete_id = $_REQUEST['delete_id'];
		$edit_id   = $_REQUEST['edit_id'];

		$db->setQuery("DELETE FROM file WHERE id = $delete_id");
		$db->execQuery();
		$db->setQuery("SELECT  `name`,
							   `rand_name`,
							   `id`,
                               `file_date`
					   FROM    `file`
					   WHERE   `incomming_call_id` = $edit_id");

		$increm = $db->getResultArray();
		$data1 = '';

		foreach ($increm[result] as $increm_row) {
			$data1 .= '<tr style="border-bottom: 1px solid #CCC;">
                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[file_date] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[name] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
        	          	<td style="height: 20px;vertical-align: middle;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none;  height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
        			  </tr>';
		}

		$data = array('page' => $data1);

		break;

	case 'up_now':
		$user    = $_SESSION['USERID'];
		$edit_id = $_REQUEST['edit_id'];

		if ($rand_file != '') {
			$db->setQuery("INSERT INTO 	`file`
                    				   (`user_id`, `file_date`, `incomming_call_id`, `name`, `rand_name`)
                    			VALUES
                    				   ('$user',NOW(), '$edit_id', '$file', '$rand_file');");
			$db->execQuery();
		}

		$db->setQuery("	SELECT  `name`,
								`rand_name`,
								`id`,
                                `file_date`
						FROM 	`file`
						WHERE   `incomming_call_id` = $edit_id");

		$increm = $db->getResultArray();
		$data1 = '';

		foreach ($increm[result] as $increm_row) {
			$data1 .= '<tr style="border-bottom: 1px solid #CCC;">
                        <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[file_date] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[name] . '</td>
        				<td style="border-left: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
        	          	<td style="height: 20px;vertical-align: middle;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none;  height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
        			  </tr>';
		}

		$data = array('page' => $data1);

		break;

	case 'cat_2':
		$page		= get_cat_1_1($_REQUEST['cat_id'], '');
		$data		= array('page'	=> $page);

		break;
	case 'cat_3':
		$page		= get_cat_1_1_1($_REQUEST['cat_id'], '');
		$data		= array('page'	=> $page);

		break;
	case 'status_2':
		$page		= get_status_1_1($_REQUEST['status_id'], '');
		$data		= array('page'	=> $page);

		break;

	case 'block_ip':
		if ($_REQUEST[ip] != '') {
			mysql_query("INSERT INTO `blocked`
                    (`user_id`, `datetime`,`ip`,`comment`)
                    VALUES
					('$_SESSION[USERID]',NOW(),'$_REQUEST[ip]','$_REQUEST[chat_comment]');");
			$last_request_res = mysql_fetch_assoc(mysql_query(
				"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"
			));
			$timedifference = $last_request_res['timedifference'];
			$who_rejected = $last_request_res['who_rejected'];
			$query_addition = '';
			if ($timedifference >= 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 0";
			} else if ($timedifference < 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 1";
			}
			mysql_query("UPDATE `chat` SET
        	                `status`='3'$query_addition
        	         WHERE  `id`='$_REQUEST[chat_id]';");
		} else {
			$error = 'IP მისამართი არასწორია!';
		}
		break;
	case 'block_pin':
		if ($_REQUEST[pin] != '') {
			mysql_query("INSERT INTO `blocked`
                    (`user_id`, `datetime`,`pin`,`comment`)
                    VALUES
					('$_SESSION[USERID]',NOW(),'$_REQUEST[pin]','$_REQUEST[chat_comment]');");
			$last_request_res = mysql_fetch_assoc(mysql_query(
				"SELECT `last_request_datetime`,
					`status`,
					TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
					who_rejected
				from chat 
				where `id` = '$_REQUEST[chat_id]';"
			));
			$timedifference = $last_request_res['timedifference'];
			$who_rejected = $last_request_res['who_rejected'];
			$query_addition = '';
			if ($timedifference >= 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 0";
			} else if ($timedifference < 10 && $who_rejected == '') {
				$query_addition .= ", `who_rejected` = 1";
			}
			mysql_query("UPDATE `chat` SET
        	                `status`='3'$query_addition
        	         WHERE  `id`='$_REQUEST[chat_id]';");
		} else {
			$error = 'IP მისამართი არასწორია!';
		}
		break;
	case 'chat_end':
		if ($_REQUEST[chat_id] != '' && $_REQUEST[chat_id] != '0') {
			if ($_REQUEST[chat_source] == "fbc") {
				$db->setQuery("UPDATE fb_comments SET `status` = '12' WHERE id = $_REQUEST[chat_id] ");
				$db->execQuery();
			} else {
				$db->setQuery("UPDATE chat SET `status` = '12' WHERE id = $_REQUEST[chat_id] ");
				$db->execQuery();
			}
		} else {
			$error = 'შეცდომა: ჩატი ვერ დაიხურა!';
		}
		break;

	case 'save_comment':
		$comment            = $_REQUEST['comment'];
		$incomming_call_id  = $_REQUEST['incomming_call_id'];
		$user_id	        = $_SESSION['USERID'];
		$db->setQuery(" INSERT INTO `chat_comment`
                                   (`user_id`, `incomming_call_id`, `parent_id`, `comment`, `datetime`, `active`)
                             VALUES
                                   ('$user_id', '$incomming_call_id', '0', '$comment', NOW(),'1');");
		$db->execQuery();

		break;

	case 'update_comment':
		$comment = $_REQUEST['comment'];
		$id      = $_REQUEST['id'];
		$db->setQuery("	SELECT 		incomming_call.user_id 
						FROM 		chat_comment 
						LEFT JOIN 	incomming_call ON incomming_call.id = chat_comment.incomming_call_id
						WHERE 		chat_comment.id = '$id'");
		$resp_user = $db->getResultArray();
		$resp_user = $resp_user['result'][0]['user_id'];
		$user_group = $_SESSION['USERGR'];
		if ($user_id != $resp_user and $user_group != 1 and $user_group != 31) {
			$error = 'თქვენ არ გაქვთ ცვლილების უფლება :' . $user_group;
		} else {
			$db->setQuery("	UPDATE 	`chat_comment`
							SET 	`comment` = '$comment'
							WHERE  	`id`      = $id");
			$db->execQuery();
		}


		break;

	case 'delete_comment':

		$comment_id = $_REQUEST['comment_id'];
		$db->setQuery("	SELECT 		incomming_call.user_id 
						FROM 		chat_comment 
						LEFT JOIN 	incomming_call ON incomming_call.id = chat_comment.incomming_call_id
						WHERE 		chat_comment.id = '$comment_id'");
		$resp_user = $db->getResultArray();
		$resp_user = $resp_user['result'][0]['user_id'];
		if ($user_id != $resp_user) {
			$error = 'თქვენ არ გაქვთ წაშლის უფლება';
		} else {
			$db->setQuery("	UPDATE 	`chat_comment`
							SET 	`active` = 0
							WHERE  	`id`     = $comment_id");
			$db->execQuery();
		}

		break;

	case 'get_add_question':
		$id   = $_REQUEST['incomming_call_id'];
		$page = Getquestion($id);
		$data = array('page'	=> $page);
		break;

	case 'get_list_sms':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		$incomming_id = $_REQUEST['incomming_id'];

		if ($incomming_id != '') {
			$db->setQuery(" SELECT id,
                                   date,
                                   phone,
                                  `content`,
                                   if(`status`=1,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
                            FROM  `sent_sms`
                            WHERE  incomming_call_id = '$incomming_id'");

			$data = $db->getList($count, $hidden);
		} else {
			$data = '';
		}
		break;

	case 'get_all_chat':
		if ($_REQUEST['source'] == "mail") {
			$db->setQuery("SELECT  	IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,IF(c.sender_name='', c.sender_id, c.sender_name)))) as name,
                					'',
                					'',
                					'',
                					m.text AS `message_client`,
                					m.text AS `message_operator`,
                					m.datetime AS message_datetime,
                					'',
                					m.id,
									c.sender_id,
                					'',
                                    m.subject AS `subject`,
                                    IFNULL(m.user_id,0) AS `check`
                		FROM mail_messages as m
                		JOIN mail_chat as c on m.mail_chat_id=c.id
                		LEFT JOIN user_info as u on m.user_id=u.user_id
                		WHERE m.mail_chat_id =  $_REQUEST[chat_id]
                		ORDER BY m.id ASC");
		} elseif ($_REQUEST['source'] == "fbm") {
			$db->setQuery(" SELECT    IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.sender_name))) as name,
    								  '',
    								  '',
    								  '',
    								  m.text AS `message_client`,
    								  m.text AS `message_operator`,
    								  m.datetime AS message_datetime,
    								  '',
    								  m.id,
    								  '',
    								 (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) as media,
                                      IFNULL(m.user_id,0) AS `check`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    					    FROM      fb_messages as m
    						JOIN      fb_chat as c on m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     m.fb_chat_id =  $_REQUEST[chat_id]
    						ORDER BY  m.id ASC");
		} elseif ($_REQUEST['source'] == "fbc") {

			$db->setQuery(" SELECT    IF(c.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.fb_user_name))) as name,
										'',
										'',
										'',
										c.message AS `message_client`,
										c.message AS `message_operator`,
										c.time AS message_datetime,
										'',
										c.id,
										'',
									(SELECT url from fb_attachments WHERE messages_id=c.id LIMIT 1) as media,
										IFNULL(c.user_id,0) AS `check`,
										(SELECT type from fb_attachments WHERE messages_id=c.id LIMIT 1) as media_type
							FROM      fb_comments as c
							JOIN      fb_feeds as f on c.feeds_id = f.id
							LEFT JOIN user_info as u on c.user_id = u.user_id
							WHERE     c.comment_id = (SELECT comment_id FROM fb_comments WHERE id = $_REQUEST[chat_id]) 
								OR 	  c.parent_id = (SELECT comment_id FROM fb_comments WHERE id = $_REQUEST[chat_id])
							ORDER BY  c.id ASC");
		} elseif ($_REQUEST['source'] == "viber") {
			$db->setQuery(" SELECT    IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name,c.sender_name))) as name,
                        	          '',
                        	          '',
                        	          '',
                        	          m.text AS `message_client`,
                        	          m.text AS `message_operator`,
                        	          m.datetime AS message_datetime,
                        	          '',
                        	          m.id,
                        	          '',
                        	          m.media,
                        	          IFNULL(m.user_id,0) AS `check`,
                        	          '' as media_type
                	        FROM      viber_messages as m
                	        JOIN      viber_chat as c on m.viber_chat_id=c.id
                	        LEFT JOIN user_info as u on m.user_id=u.user_id
                	        WHERE     m.viber_chat_id =  $_REQUEST[chat_id]
                	        ORDER BY  m.id ASC");
		} else {
			$db->setQuery("UPDATE `chat` 
                              SET `answer_date`    = NOW(),
                                  `answer_user_id` = '$_SESSION[USERID]'
                           WHERE  `id`             = '$_REQUEST[chat_id]'
                           AND     status          = 1");

			$db->execQuery();
			$name           = '';
			$os             = '';
			$bro            = '';
			$ip             = '';
			$sms            = '';
			$chat_detail_id = 0;

			$db->setQuery(" SELECT     chat.`name`,
                                       chat.`device`,
                                       chat.`browser`,
                                       chat.`ip`,
                                       chat_details.message_client,
                                       chat_details.message_operator,
                                       chat_details.message_datetime,
                                       IF(ISNULL(chat_nikname.`name`),'საიტის ოპერატორი',chat_nikname.name) AS `op_name`,
                                       chat_details.id,
    								   chat.pin,
									   chat_details.edited,
									   chat_details.edit_date
                            FROM      `chat`
                            LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
                            LEFT JOIN `chat_nikname` ON chat_nikname.user_id = chat_details.operator_user_id AND chat_nikname.actived = 1
    						WHERE     `chat`.id = $_REQUEST[chat_id] AND `chat_details`.`actived` = 1
                            ORDER BY   chat_details.message_datetime ASC");
		}

		$res = $db->getResultArray();

		foreach ($res[result] as $req) {
			$subject = '';
			if ($_REQUEST['source'] == "mail") {
				$subject_text = str_replace('Re:', '', $req[subject]);
				if ($subject_text != '') {
					$subject = 'თემა: ' . $subject_text;
				}
				$req['message_client'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req[message_client]);
				$db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch`, `check_mesage` FROM `mail_attachments` WHERE messages_id=$req[id]");
				$fq = $db->getResultArray();
				foreach ($fq[result] as $file) {
					if ($file[check_mesage] == 1) {
						$req['message_operator'] = $req[message_operator] . "<a href='$file[patch]' target='_blank'>$file[name]</a><br>";
					} else {
						$file[patch] = str_replace('/var/www/html/', '', $file[patch]);
						$req['message_client'] = preg_replace('/(?<=cid:' . $file[name] . ').*?(?=>)/', $file[patch] . '"',  $req['message_client']); //preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
						$req[message_client] = str_replace('cid:' . $file[name], 'https://crm.my.ge/', $req['message_client']);
						$req[message_client] = $req[message_client] . "<a href='https://crm.my.ge/$file[patch]' target='_blank'>$file[name]</a><br>";
					}
				}
			}

			$gif_url = preg_grep('/(?<=http).*?(?=.gif)/', array($req[message_client], '123'));
			if ($gif_url[0])
				$req[message_client] = "<a href='$gif_url[0]' target='_blank'><img style='border-radius: 10%;max-height: 200px;' src='$gif_url[0]'></a>";

			if ($req[media_type] == 'fallback') {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'file') {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} elseif ($req[media_type] == 'video') {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
			} else {
				if (!empty($req['media'])) $req[message_client] = $req[message_client] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
			}


			$name = $req[name];
			$email = $req[sender_id];

			if ($name == '') $name = 'უცნობი';
			$os  = $req[device];
			$bro = $req[browser];
			$ip  = $req[ip];
			$pin = $req[pin];

			if ($req[edited] == 1) {
				$edited = '<p class="edit_date" title="გაგზავნის თარიღი: ' . $req[message_datetime] . '" id="' . $req[id] . '" style="color: #878787; width: 100%;"><img src="media/images/icons/l_edit.png" style=" height: 14px;margin-bottom: 1px;margin-right: 4px;">' . $req[edit_date] . '</p>';
			}
			if ($req[edited] == 0) {
				$edited = '<p class="edit_date" id="' . $req[id] . '" style="color: #878787; width: 100%;">' . $req[message_datetime] . '</p>';
			}

			$color = 'color:#2681DC;';
			if ($req[check] > 0 && $_REQUEST['source'] != 'chat') {
				$color = '';
			}
			if ($req[message_client] == '' && $_REQUEST['source'] == 'chat') {
				if ($req[message_operator] != '') {
					$sms .= '<tr class="chat_message_id" data-text="' . $req[message_operator] . '" data-id="' . $req[id] . '" style="height: 17px;">
							<td colspan="2" style=" word-break: break-word;     text-align: right;">
							<span class="chat_message_edit_button" ><span class="open_edit" data-id="' . $req[id] . '"><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: -5px;"> </div><div style="background-color: #00000085;padding: 2px;position: absolute;border-radius: 100%;margin-left: 5px;"></div></span><div class="chat_message_edit_block" id="' . $req[id] . '" data-status="0" style="display: none"><div class="sms_edit" data-id="' . $req[id] . '"><img src="media/images/icons/l_edit.png" style="
							margin-right: 3px;
						"> რედაქტირება</div><div class="sms_delete" data-id="' . $req[id] . '"><img src="media/images/icons/l_delete.png" style="
						margin-right: 7px;
					">წაშლა</div></div></span>
								<div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE; ">
									<p style="text-align: left; font-weight: bold; font-size: 12px;">' . $req[op_name] . '</p>
									<p class="operator_message"  id="' . $req[id] . '" style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; text-align: left; line-height: 20x; font-size: 12px;">' . $req[message_operator] . '</p>
									<div style="text-align: right; padding-top: 7px;">
										' . $edited . '
									
									</div>
								</div>
							</td>
						</tr>
						<tr style="height:10px;"></tr>';
				}
			} else {
				if ($_REQUEST['source'] == 'chat') {
					if ($req[message_client] != '') {
						$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 12px;">' . $name . '</p>
                                            <p style="font- family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[message_client] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req[message_datetime] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
    							<tr style="height:10px;"></tr>';
					}
				} else {
					if ($req[check] > 0) {
						if ($req[message_operator] != '') {
							$sms .= '<tr style="height: 17px;">
                        	            <td colspan="2" style=" word-break: break-word;">
                                            <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                                	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $name . '</p>
                                                <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[message_operator] . '</p>
                                                <div style="text-align: right; padding-top: 7px;">
                                                    <p style="color: #878787; width: 100%;">' . $req[message_datetime] . '</p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
    								<tr style="height:10px;"></tr>';
						}
					} else {



						if ($req[message_client] != '') {
							$sms .= '<tr style="height: 17px;">
                        	            <td colspan="2" style=" word-break: break-word;">
                                            <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                                	            <p style="font-weight: bold;font-size: 12px;">' . $name . '</p>
    											<p style="overflow-x: auto; font-weight: bold; font-size: 12px;">' . $subject . '</p>
    											<p style="overflow-x: auto; font-weight: 100; font-size: 12px;">' . $email . '</p>
                                                <p style="font-family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[message_client] . '</p>
                                                <div>
                                                    <p style="width: 100%; color: #878787;">' . $req[message_datetime] . '</p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
    								<tr style="height:10px;"></tr>';
						}
					}
				}
			}
		}

		$sms =  str_replace('https://e.mail.ru/', '',  $sms);
		$sms =  str_replace('https://touch.mail.ru/cgi-bin/', '',  $sms);

		if ($_REQUEST['source'] == "fbm") {
			$db->setQuery("SELECT `id` FROM `fb_messages` WHERE  `fb_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} elseif ($_REQUEST['source'] == "fbc") {
			$db->setQuery("SELECT `id` FROM `fb_comments` WHERE  `id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} elseif ($_REQUEST['source'] == "viber") {
			$db->setQuery("SELECT `id` FROM `viber_messages` WHERE  `viber_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} elseif ($_REQUEST['source'] == "mail") {
			$db->setQuery("SELECT `id` FROM `mail_messages` WHERE  `mail_chat_id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
		} else {
			$db->setQuery("SELECT `chat_details`.`id`
                           FROM   `chat_details`
                           WHERE  `chat_details`.`chat_id` = '$_REQUEST[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]' ORDER BY chat_details.id DESC LIMIT 1");

			$ge = $db->getResultArray();
			$chat_detail_id = $ge[result][0][id];
			$db->setQuery("SELECT IFNULL(`last_user_id`,'') AS `last_user_id` FROM `chat` WHERE  `id` = '$_REQUEST[chat_id]' ORDER BY id DESC LIMIT 1");

			$us = $db->getResultArray();
			$last_user_id = $us[result][0]['last_user_id'];
		}




		$data = array('name' => $name, 'chat_user_id' => $last_user_id, 'os' => $os, 'bro' => $bro, 'pin' => $pin, 'ip' => $ip, 'sms' => $sms, 'chat_detail_id' => $chat_detail_id);

		break;
	case 'chat_action':

		$smsID = $_REQUEST['id'];
		$smsAction = $_REQUEST['chatAction'];
		$smsText = $_REQUEST['text'];

		if ($smsAction == "delete") {
			$db->setQuery("	UPDATE `chat_details` 
								SET 	`actived`   = 0,
										`edited` 	= 0
								WHERE  `id` 		= $smsID ");
		} else {

			$db->setQuery("	UPDATE `chat_details` 
								SET 	`message_operator` = '$smsText',
										`edited`	= 1,
										`edit_date` = NOW()
								WHERE  `id` 		= $smsID ");
		}
		$db->execQuery();
		break;
	case 'get_history':
		if ($_REQUEST['source'] == 'site') {
			// 			$res = mysql_query("SELECT   `text`,
			// 										  IFNULL(u.`name`,`sender_name`) as name,
			// 										  m.datetime as 'datetime',
			// 										  m.media,
			//                                           m.user_id,
			//                                           c.first_datetime,
			//                                           c.id AS `cat_id`
			// 								FROM     `site_chat` as c
			// 								JOIN     `site_messages` as m ON m.site_chat_id=c.id
			// 								LEFT JOIN crystal_users as u on u.id=m.user_id 
			// 								WHERE     c.sender_id in (SELECT sender_id FROM site_chat WHERE id='".$_REQUEST['chat_id']."') AND `status` in (1,2,3)");
			// 			$imID = 0;
			// 			while ($req = mysql_fetch_array($res)){
			// 				if(!empty($ge['media'])) $ge[text] = $ge[text]. "<a href='$ge[media]' target='_blank'><img style='border-radius: 10%;max-height: 200px;' src='$ge[media]'></a>";
			// 				if($imID != $req[cat_id]){
			// 				    $sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------'.$req[first_datetime].'-------</td></tr>';
			// 				}
			// 				if($req['user_id']==''){
			// 				    $sms .= '<tr style="height: 17px;">
			//                     	            <td colspan="2" style=" word-break: break-word;">
			//          font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
			//                             	            <p style="font-weight: bold;font-size: 14px;">'.$req[name].'</p>
			//                                             <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">'.$req[text].'</p>
			//                                             <div>
			//                                                 <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
			//                                             </div>
			//                                         </div>
			//                                     </td>
			//                                 </tr>
			//                                 <tr style="height:10px;"></tr>';
			// 				}else{
			// 				    $sms .= '<tr style="height: 17px;">
			//                     	            <td colspan="2" style=" word-break: break-word;">
			//                                         <div style="float: right!important; background: rgba(80, 181, 0, 0.24) none repeat scroll 0 0; border-radius: 15px;padding: 7px 15px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word;box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);">
			//                             	            <p style="text-align: right; font-weight: bold; font-size: 14px;">'.$req[name].'</p>
			//                                             <p style="padding-top: 7px; line-height: 20x; font-size: 14px;">'.$req[text].'</p>
			//                                             <div style="text-align: right; padding-top: 7px;">
			//                                                 <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
			//                                             </div>
			//                                         </div>
			//                                     </td>
			//                                 </tr>
			//                                 <tr style="height:10px;"></tr>';
			// 				}
			// 				$imID = $req[cat_id];
			// 			}
			// 			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == 'fbm') {
			$db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    						FROM     `fb_chat` as c
    						JOIN     `fb_messages` as m ON m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM fb_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				if ($req[media_type] == 'fallback') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'file') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'video') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} else {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
				}
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}

				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
				}

				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == 'fbc') {
			$db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  (SELECT url from fb_attachments WHERE messages_id=m.id LIMIT 1) AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      (SELECT type from fb_attachments WHERE messages_id=m.id LIMIT 1) as media_type
    						FROM     `fb_chat` as c
    						JOIN     `fb_messages` as m ON m.fb_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM fb_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				if ($req[media_type] == 'fallback') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'file') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} elseif ($req[media_type] == 'video') {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
				} else {
					if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
				}
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}

				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 14px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
				}

				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == 'viber') {
			$db->setQuery(" SELECT   `text`,
    								  IFNULL(u.`name`,`sender_name`) as name,
    								  m.datetime as 'datetime',
    								  m.`media` AS `media`,
                                      m.user_id,
                                      c.first_datetime,
                                      c.id AS `cat_id`,
                                      '' as media_type
    						FROM     `viber_chat` as c
    						JOIN     `viber_messages` as m ON m.viber_chat_id=c.id
    						LEFT JOIN user_info as u on m.user_id=u.user_id
    						WHERE     c.sender_id in (SELECT sender_id FROM viber_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY  m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				if (!empty($req['media'])) $req[text] = $req[text] . "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";

				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}

				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$req['name'].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[text].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[time].'</p><time class="timeago" datetime="'.$req[time].'"></time></td></tr>';
				}

				$imID = $req[cat_id];
			}
			$data = array('name' => $name, 'sms' => $sms);
		} elseif ($_REQUEST['source'] == "mail") {
			$db->setQuery("SELECT  	IF(m.user_id='0','აუტომოპასუხე',(IFNULL(u.name, IF(c.sender_name='', c.sender_id, c.sender_name)))) as name,
                        		        m.text,
                        		        m.text,
                        		        m.datetime,
                        		        m.id,
                        		        m.subject AS `subject`,
                                        m.user_id,
                                        c.first_datetime,
										c.sender_id AS `sender_id`,
                                        c.id AS `cat_id`
            		        FROM        mail_messages as m
            		        JOIN        mail_chat as c on m.mail_chat_id=c.id
            		        LEFT JOIN   user_info as u on m.user_id=u.user_id
            		        WHERE       c.sender_id = (SELECT mail_chat.sender_id FROM mail_chat WHERE id='" . $_REQUEST['chat_id'] . "') AND `status` in (1,2,3)
                            ORDER BY m.datetime ASC");
			$imID = 0;
			$res = $db->getResultArray();
			foreach ($res[result] as $req) {
				$name = $req[0];
				$subject_text = str_replace('Re:', '', $req[subject]);
				if ($subject_text != '') {
					$subject = 'თემა: ' . $subject_text;
				}

				$db->setQuery("SELECT `name`, SUBSTRING_INDEX(patch,  'htdocs/',-1) AS `patch`, `check_mesage` FROM `mail_attachments` WHERE messages_id=$req[id]");
				$fq = $db->getResultArray();
				foreach ($fq[result] as $file) {
					if ($file[check_mesage] == 1) {
						$req['text']      = $req[text] . "<a href='$file[patch]' target='_blank'>$file[name]</a><br>";
					} else {
						$file[patch] = str_replace('/var/www/html/', '', $file[patch]);
						$req['text']    = preg_replace('/(?<=cid:' . $file[name] . ').*?(?=>)/', $file[patch] . '"',  $req['text']); //preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
						$req['text']      = str_replace('cid:' . $file[name], 'https://crm.my.ge/', $req['text']);
						$req['text']      = $req[text] . "<a href='https://crm.my.ge/$file[patch]' target='_blank'>$file[name]</a><br>";
					}
				}
				if ($imID != $req[cat_id]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[first_datetime] . '-------</td></tr>';
				}
				if ($req[user_id] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                            	            <p style="font-weight: bold;font-size: 14px;">' . $req[name] . '</p>
                                            <p style="overflow-x: auto; font-weight: bold;font-size: 14px;">' . $subject . '</p>
                                            
											
											<p style="overflow-x: auto; padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 14px;">' . $req[text] . '</p>
                                            <div>
                                                <p style="width: 100%; color: #878787;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				} else {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[name] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[text] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req['datetime'] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
				}
				$imID = $req[cat_id];
				//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$name.'</td></tr>'.$subject.'<tr style="height: 17px;"><td colspan=2 style=" word-break: break-word;">'.$req[1].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[3].'</p><time class="timeago" datetime="'.$req[3].'"></time></td></tr>';
			}

			$sms =  str_replace('https://e.mail.ru/', '',  $sms);
			$sms =  str_replace('https://touch.mail.ru/cgi-bin/', '',  $sms);

			$data = array('name' => $name, 'sms' => $sms);
		} else {
			$name = '';
			$os = '';
			$bro = '';
			$ip = '';
			$sms = '';
			$chat_name = $_REQUEST['chat_name'];
			$db->setQuery("SELECT  chat.`name`,
									chat.`device`,
									chat.`browser`,
									chat.`ip`,
									chat_details.message_client,
									chat_details.message_operator,
									chat_details.message_datetime,
									IF(ISNULL(chat_nikname.`name`),'ოპერატორი',chat_nikname.name) AS `name`,
									chat.join_date,
									chat.id
							FROM `chat`
							LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
							LEFT JOIN `chat_nikname` ON chat_nikname.user_id = chat_details.operator_user_id AND chat_nikname.actived = 1
							WHERE `chat`.ip = '$_REQUEST[ip]' AND chat.`name` = '$chat_name'
                            ORDER BY chat_details.message_datetime ASC");
			$imID = 0;
			$res = $db->getResultArray(MYSQLI_NUM);
			foreach ($res[result] as $req) {
				$name = $req[0];
				$os = $req[1];
				$bro = $req[2];
				$ip = $req[3];
				if ($imID != $req[9]) {
					$sms .= '<tr style="height: 20px;"><td colspan=2 style="font-weight:bold;text-align: center;">------' . $req[8] . '-------</td></tr>';
				}
				if ($req[4] == '') {
					$sms .= '<tr style="height: 17px;">
                    	            <td colspan="2" style=" word-break: break-word;">
                                        <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                            	            <p style="text-align: right; font-weight: bold; font-size: 12px;">' . $req[0] . '</p>
                                            <p style="font-family: BPG_baner; overflow-x: auto; padding-top: 7px; line-height: 20x; font-size: 12px;">' . $req[5] . '</p>
                                            <div style="text-align: right; padding-top: 7px;">
                                                <p style="color: #878787; width: 100%;">' . $req[6] . '</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height:10px;"></tr>';
					//$sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold; word-break: break-word;">'.$req[7].'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[5].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[6].'</p><time class="timeago" datetime="'.$req[6].'"></time></td></tr>';
				} else {
					//$sms .= '<tr style="height: 17px; background: #e9e9e9;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$name.'</td></tr><tr style="height: 17px;"><td colspan=2 >'.$req[4].'</td></tr><tr><td colspan=2 style="border-bottom:2px dotted #DBDBDB;text-align: right;"><p>'.$req[6].'</p><time class="timeago" datetime="'.$req[6].'"></time></td></tr>';

					$sms .= '<tr style="height: 17px;">
                	            <td colspan="2" style=" word-break: break-word;">
                                    <font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                        	            <p style="overflow-x: auto; font-weight: bold;font-size: 12px;">' . $name . '</p>
                                        <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">' . $req[4] . '</p>
                                        <div>
                                            <p style="width: 100%; color: #878787;">' . $req[6] . '</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height:10px;"></tr>';
				}
				$imID = $req[9];
			}
			$data = array('name' => $name, 'os' => $os, 'bro' => $bro, 'ip' => $ip, 'sms' => $sms);
		}
		break;

	case 'check_user_dnd_on_local':
		$user_id = $_SESSION["USERID"];
		$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
		if ($db->getNumRow() == 0) {
			$db->setQuery("SELECT muted FROM `user_mute_check` where user_id = '$user_id'");
			if ($db->getNumRow() == 0) {
				$muted = 0;
			} else {
				$row1  = $db->getResultArray();
				$muted = $row1[result][0][muted];
			}

			$db->setQuery("SELECT    COUNT(*) AS `unrid`
        	               FROM      queries
	                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user_id'
        	               WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
			$res_queries = $db->getResultArray();

			$data = array("status" => 0, 'muted' => $muted, 'queries_unrid_count' => $res_queries[result][0][unrid]);
		} else {
			$db->setQuery("SELECT muted FROM `user_mute_check` where user_id = '$user_id'");
			if ($db->getNumRow() == 0) {
				$muted = 0;
			} else {
				$row1  = $db->getResultArray();
				$muted = $row1[result][0][muted];
			}
			$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
			$row = $db->getResultArray();

			$db->setQuery("SELECT    COUNT(*) AS `unrid`
        	               FROM      queries
	                       LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$user_id'
        	               WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
			$res_queries = $db->getResultArray();

			$data = array("status" => $row[result][0][dndlocalon_status], 'muted' => $muted, 'queries_unrid_count' => $res_queries[result][0][unrid]);
		}





		break;
	case 'change_incall_dnd_status':

		$user_id 	  	= $_SESSION["USERID"];
		$active_status 	= $_REQUEST["activeStatus"];


		$db->setQuery("SELECT dndlocalon_status FROM `users_call_dndlocalon` where user_id = '$user_id'");
		if ($db->getNumRow() == 0) {
			$db->setQuery("INSERT INTO `users_call_dndlocalon` 
                                        (`user_id`, `dndlocalon_status`) 
                                   VALUES 
                                        ('$user_id', '$active_status')");
			$db->execQuery();
		} else {
			$db->setQuery("UPDATE users_call_dndlocalon
    							 SET dndlocalon_status = '$active_status'
    						  WHERE  user_id           = '$user_id'");
			$db->execQuery();
		}
		$data = array("active" => true);
		break;
	case 'change_incall_activitie_status':

		$user_id 	  	= $_SESSION["USERID"];
		$seesion_id		= session_id();
		$activitie_id 	= $_REQUEST["activitieId"];
		$type			= $_REQUEST["type"];
		$data 			= array();

		// define activitie statuses
		$crystal_users_activitie_id = $type == "on" ? $activitie_id : 0;

		$db->setQuery("UPDATE users
						      SET work_activities_id = '$crystal_users_activitie_id'
						   WHERE  id                 = '$user_id'");
		$db->execQuery();

		if ($type == "on") {

			// insert new activitie data
			$db->setQuery("INSERT INTO work_activities_log 
                                         (`user_id`, `session_id`, `work_activities_id`, `start_datetime`)
							        VALUES 
                                         ('$user_id', '$seesion_id', '$activitie_id', NOW())");
			$db->execQuery();

			// define dnd status
			$db->setQuery("SELECT dnd FROM work_activities WHERE id = '$activitie_id' AND actived = 1");
			$dnd_status_assoc = $db->getResultArray();

			$dnd_status = $dnd_status_assoc[result][0]["dnd"];

			$db->setQuery("UPDATE users
							      SET saved_dnd = dnd,
								      dnd       = '$dnd_status'
							    WHERE id        = '$user_id'");
			$db->execQuery();
		} else if ($type == "off") {

			$db->setQuery("UPDATE work_activities_log
							      SET end_datetime = NOW()

							   WHERE  session_id         = '$seesion_id'
							   AND    work_activities_id = '$activitie_id'
							   AND    ISNULL(end_datetime)");
			$db->execQuery();

			$db->setQuery("UPDATE users
							      SET dnd = saved_dnd
							   WHERE  id = '$user_id'");
			$db->execQuery();
		}

		// define online and dnd status
		$db->setQuery("SELECT online, dnd from users WHERE id = '$user_id'");

		$online_status_assoc = $db->getResultArray();
		$online_status       = $online_status_assoc[result][0]["online"];
		$dnd_status          = $online_status_assoc[result][0]["dnd"];

		// define responce data
		$data["onlineStatus"] = (int)$online_status;
		$data["dndStatus"] = (int)$dnd_status;

		break;
	case 'check_activitie_status':

		$user_id          = $_SESSION["USERID"];

		$activitie_data   = get_activitie_status_data($user_id);
		$activitie_id     = $activitie_data["id"];
		$activitie_status = $activitie_data["status"];
		$seconds_passed   = get_activitie_seconds_passed($user_id, $activitie_id);

		$data = array(
			"activitieId" => $activitie_id,
			"activitieStatus" => $activitie_status,
			"secondsPassed" => $seconds_passed
		);

		break;
	case 'check_activitie_status_off':

		$user_id = $_SESSION["USERID"];
		$activitie_data = get_activitie_status_data($user_id);
		$activitie_id = $activitie_data["id"];
		$activitie_status = $activitie_data["status"];
		$seconds_passed = get_activitie_seconds_passed_off($user_id, $activitie_id);

		$data = array(

			"secondsPassed" => $seconds_passed
		);

		break;

	case 'open_new_sms_dialog':

		$type = $_REQUEST["type"];


		if ($_REQUEST['crm'] == 1) {
			$crm_base_id = $_REQUEST['crm_base_id'];
			$crm_phone = mysql_fetch_assoc(mysql_query("SELECT  * FROM autocall_request_base WHERE id='$crm_base_id'"));
			$page		= get_new_sms_dialog_crm($type, $crm_phone);
			$data		= array('page'	=> $page, 'type' => $type);
		} else {
			$page		= get_new_sms_dialog($type);
			$data		= array('page'	=> $page, 'type' => $type);
		}


		break;
	case 'get_shablon':

		$data		 = array('shablon' => getShablon());
		break;

	case 'send_new_sms':

		$send_type = $_REQUEST["sendType"];
		$addressee = $_REQUEST["addressee"];
		$sms_text = $_REQUEST["smsText"];

		$json = array(
			'Number' => $addressee,
			'Message' => $sms_text
		);

		$json = json_encode($json);

		$opts = array(
			'http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Authorization: Basic EE9F474A-B34C-4502-A352-8DC8DA53E647',
				'header'  => 'Content-type:application/json; charset=utf-8',
				'content' => $json
			)
		);

		$context  = stream_context_create($opts);
		$result = file_get_contents('http://192.168.40.19:8089/api/Smses/Send', false, $context);

		$data = array('response' => $result);

		break;
	case 'get_sms_template_dialog':

		$data = array(
			"html" => sms_template_dialog()
		);

		break;
	case 'phone_directory':

		$count = 		$_REQUEST['count'];

		$db->setQuery("SELECT id,
							  id, 
							  name, 
							  CONCAT('<button class=\"download_shablon\" sms_id=\"', id ,'\" data-message=\"', message ,'\" >არჩევა</button>')
						FROM  sms
						WHERE actived = 1");

		$data = $db->getList($count, $hidden);

		break;

	case 'get_phone_dir_list':

		$count  = $_REQUEST['count'];
		$hidden = $_REQUEST['hidden'];
		$type   = $_REQUEST['type'];


		$db->setQuery("SELECT 	 persons_phone.id,
                    	         persons_phone.name,
                    	         person_position AS position,
                    	         IF($type=1, persons_phone.phone_number, persons_phone.mail) AS number
            	       FROM      persons_phone
            	       LEFT JOIN position ON position.id=persons_phone.position_id
            	       WHERE     persons_phone.actived = 1");

		$res = $db->getResultArray(MYSQLI_NUM);

		$data = array("aaData"	=> array());

		foreach ($res[result] as $aRow) {
			$row = array();
			for ($i = 0; $i < $count; $i++) {
				/* General output */
				$row[] = $aRow[$i];
				if ($i == ($count - 1)) {
					$row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="pdl_checkbox_' . $aRow[$hidden] . '" name="check_' . $aRow[$hidden] . '" value="' . $aRow[3] . '" class="pdl-check" />
                                  <label for="pdl_checkbox_' . $aRow[$hidden] . '"></label>
                              </div>';
				}
			}
			$data['aaData'][] = $row;
		}

		break;

	case 'phone_dir_list':

		//data variables
		$type = $_REQUEST['type'];
		$html = phoneDirList($type);
		$data = array('html'	=> $html);

		break;
	case 'view_crm_request':

		$id = $_REQUEST['id'];


		$db->setQuery("SELECT id FROM `crm_views` WHERE `user_id` = '$user_id' AND `crm_id` = '$id'");

		$check = $db->getNumRow();

		if ($check == 0) {
			$db->setQuery("	INSERT	INTO	`crm_views` 
									SET	`user_id` 	= '$user_id',
										`crm_id`	 	= '$id'");
			$db->execQuery();
			$data = array("status" => 1, "message" => "ქმედება წარმატებით განხორციელდა");
		} else {
			$data = array("status" => 0, "message" => "უკვე ნანახია");
		}


		break;
	case 'save_crm':

		$id 		= $_REQUEST['crm_id'];
		$comment	= $_REQUEST['comment'];
		$status		= $_REQUEST['status'];
		$crm_phone	= $_REQUEST['crm_phone'];


		$db->setQuery("	UPDATE 	`crm`
						SET		`status_id` 	= '$status',
								`res_comment` 	= '$comment',
								`resp_user_id`  = '$user_id',
								`crm_phone`		= '$crm_phone'
						WHERE	`id` 			= '$id'
					");

		$db->execQuery();

		$db->setQuery("SELECT id FROM `crm_views` WHERE `user_id` = '$user_id' AND `crm_id` = '$id'");

		$check = $db->getNumRow();

		if ($check == 0) {
			$db->setQuery("	INSERT	INTO	`crm_views` 
									SET	`user_id` 	= '$user_id',
										`crm_id`	= '$id'");
			$db->execQuery();
		}

		$data = array("status" => 1, "message" => "ქმედება წარამტებით განხორციელდა");
		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;
$data["filterID"] = $filterID;
echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/
function phoneDirList($type)
{
	if ($type == 1) {
		$address = 'ტელ. ნომერი';
	} else {
		$address = 'მეილი';
	}
	return '<table class="display" id="phoneDirectoryList" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 27%;">სახელი</th>
                        <th style="width: 28%;">თანამდებობა</th>
                        <th style="width: 42%;">' . $address . '</th>
                        <th style="width: 3%;">#</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                           <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <div class="callapp_checkbox">
                              <input type="checkbox" id="pdl_check_all" name="pdl_check_all" />
                              <label for="pdl_check_all"></label>
                            </div>
                        </th>
                    </tr>
                </thead>
            </table>
            <input type="hidden" id="view_type" value="' . $type . '" />';
}
function checkgroup($user)
{
	$res = mysql_fetch_assoc(mysql_query("  SELECT crystal_users.group_id
											FROM    crystal_users
											WHERE  crystal_users.id = $user"));
	return $res['group_id'];
}

function Addincomming($incom_id)
{
	global $db;


	$ext                 = $_SESSION['EXTENSION'];
	$incom_id	         = $_REQUEST['id'];
	$user		         = $_SESSION['USERID'];
	$c_date		         = date('Y-m-d H:i:s');

	$call_date			 = $_REQUEST['call_date'];
	$hidden_user		 = $_REQUEST['hidden_user'];
	$hidden_inc			 = $_REQUEST['hidden_inc'];
	$imchat			     = $_REQUEST['imchat'];
	$chat_id             = $_REQUEST['chat_id'];
	$ipaddres            = $_REQUEST['ipaddres'];
	$ii			         = $_REQUEST['ii'];
	$phone				 = $_REQUEST['phone'];
	$incomming_cat_1	 = $_REQUEST['incomming_cat_1'];
	$incomming_cat_1_1	 = $_REQUEST['incomming_cat_1_1'];
	$incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
	$incomming_status_1	 = $_REQUEST['incomming_status_1'];
	$incomming_status_1_1 = $_REQUEST['incomming_status_1_1'];
	$source			     = $_REQUEST['source'];
	$my_site			 = $_REQUEST['site_id'];
	$s_u_mail			 = $_REQUEST['s_u_mail'];

	$chat_language		 = $_REQUEST['chat_language'];
	$chat_site			 = $_REQUEST['chat_site'];
	$inc_status_a		 = $_REQUEST['inc_status_a'];
	$company			 = $_REQUEST['company'];
	$space_type			 = $_REQUEST['space_type'];

	$s_u_mail			= $_REQUEST['s_u_mail'];
	$ab_person_name     = $_REQUEST['s_u_name'];
	$lid        		= $_REQUEST['lid'];
	$phone1        		= $_REQUEST['phone1'];
	$fb_link        	= $_REQUEST['fb_link'];
	$viber_address      = $_REQUEST['viber_address'];
	$vizit_datetime     = $_REQUEST['vizit_datetime'];
	$vizit_location     = $_REQUEST['vizit_location'];
	$client_comment     = $_REQUEST['client_comment'];
	$out_comment     = $_REQUEST['out_comment'];

	$client_sex			 = $_REQUEST['client_sex'];
	$client_birth_year	 = $_REQUEST['client_birth_year'];
	$s_u_status			 = $_REQUEST['s_u_status'];
	$inc_status			 = $_REQUEST['inc_status'];
	$call_content		 = htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
	$source_id			 = $_REQUEST['source_id'];
	$personal_pin		 = $_REQUEST['s_u_user_id'];
	$personal_id		 = $_REQUEST['s_u_pid'];
	$lid_comment         = $_REQUEST['lid_comment'];
	$processing_start_date  = $_REQUEST['processing_start_date'];

	$db->setQuery("INSERT INTO `incomming_call`
                           SET `id`             = '$incom_id',
                               `date`           = NOW(),
                               `processing_start_date` = '$processing_start_date',
                               `processing_end_date`   = NOW(),
                               `user_id`        = '$user',
                               `extension`      = '$ext',
                               `phone`          = '$phone',

                               `phone1`         = '$phone1',
                               `fb_link`        = '$fb_link',
                               `viber_address`  = '$viber_address',
                               `vizit_date`     = '$vizit_datetime',
                               `vizit_location` = '$vizit_location',
                               `client_comment` = '$client_comment',
							   `out_comment`	= '$out_comment',
                               `client_name`    = '$ab_person_name',
                               `client_mail`    = '$s_u_mail',
                               `lid`            = '$lid',
                               `client_user_id` = '$personal_pin',
                               `client_pid`     = '$personal_id',
                               `client_satus`   = '$s_u_status',
                               `cat_1`          = '$incomming_cat_1',
                               `cat_1_1`        = '$incomming_cat_1_1',
                               `cat_1_1_1`      = '$incomming_cat_1_1_1',
							   `status_1`          = '$incomming_status_1',
                               `status_1_1`        = '$incomming_status_1_1',
							   `chat_language` 	= '$chat_language',
							   `chat_site`		= '$chat_site',
							   `inc_status`		= '$inc_status_a',
							   `company`		= '$company',
							   `space_type`		= '$space_type',
                               `lid_comment`    = '$lid_comment',
                               `call_content`   = '$call_content'");
	$db->execQuery();

	$site_array = explode(",", $my_site);
	$db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incom_id'");
	$db->execQuery();

	//    foreach($site_array AS $site_id){
	//        $db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incom_id', $site_id)");
	//        $db->execQuery();
	//    }

	if ($lid == 1) {
		$db->setQuery("SELECT *
                       FROM   outgoing_call
                       WHERE  incomming_call_id = '$incom_id' AND outgoing_type_id = 4");

		$check = $db->getNumRow();
		if ($check == 0) {
			$db->setQuery("INSERT INTO `outgoing_call`
                                   SET `user_id`           = '$user',
                                       `date`              =  NOW(),
                                       `start_date`        =  NOW(),
                                       `incomming_call_id` = '$incom_id',
                                       `phone`             = '$phone',
                                       `outgoing_type_id`  = '4',
                                       `status_id`         = '1',
                                       `lid_comment`       = '$lid_comment'
                                       `actived`           = '1'");
			$db->execQuery();
		}
	}
}

function Saveincomming($incom_id, $crm_id)
{
	global $error;
	global $db;

	$ext                 = $_SESSION['EXTENSION'];
	$incom_id	         = $_REQUEST['id'];
	$crm_id	             = $_REQUEST['crm_id'];
	$user		         = $_SESSION['USERID'];
	$c_date		         = date('Y-m-d H:i:s');

	$call_date			 = $_REQUEST['call_date'];
	$hidden_user		 = $_REQUEST['hidden_user'];
	$hidden_inc			 = $_REQUEST['hidden_inc'];
	$imchat			     = $_REQUEST['imchat'];
	$chat_id             = $_REQUEST['chat_id'];
	$ipaddres            = $_REQUEST['ipaddres'];
	$ii			         = $_REQUEST['ii'];
	$phone				 = $_REQUEST['phone'];
	$incomming_cat_1	 = $_REQUEST['incomming_cat_1'];
	$incomming_cat_1_1	 = $_REQUEST['incomming_cat_1_1'];
	$incomming_cat_1_1_1 = $_REQUEST['incomming_cat_1_1_1'];
	$incomming_status_1	 = $_REQUEST['incomming_status_1'];
	$incomming_status_1_1	 = $_REQUEST['incomming_status_1_1'];
	$source			    = $_REQUEST['source'];
	$my_site			= $_REQUEST['site_id'];

	$chat_language		 = $_REQUEST['chat_language'];
	$chat_site			 = $_REQUEST['chat_site'];
	$inc_status_a		 = $_REQUEST['inc_status_a'];
	$company		 	= $_REQUEST['company'];
	$manager		 	= $_REQUEST['manager'];
	$manager_comment    = $_REQUEST['manager_comment'];
	$space_type			= $_REQUEST['space_type'];

	$s_u_mail			= $_REQUEST['s_u_mail'];
	$ab_person_name     = $_REQUEST['s_u_name'];
	$lid        		= $_REQUEST['lid'];
	$phone1        		= $_REQUEST['phone1'];
	$fb_link        	= $_REQUEST['fb_link'];
	$viber_address      = $_REQUEST['viber_address'];
	$vizit_datetime     = $_REQUEST['vizit_datetime'];
	$vizit_location     = $_REQUEST['vizit_location'];
	$client_comment     = $_REQUEST['client_comment'];
	$out_comment		= $_REQUEST['out_comment'];
	$client_sex			= $_REQUEST['client_sex'];
	$client_birth_year	= $_REQUEST['client_birth_year'];
	$s_u_status			= $_REQUEST['s_u_status'];
	$inc_status			= $_REQUEST['inc_status'];
	$call_content		= htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
	$source_id			= $_REQUEST['source_id'];
	$personal_pin		= $_REQUEST['s_u_user_id'];
	$personal_id		= $_REQUEST['s_u_pid'];

	$lid_comment        = $_REQUEST['lid_comment'];

	//if ($ext=0 || $_REQUEST[imchat] == '1'){
	//if ($hidden_user == $user || $_REQUEST['imchat'] == '1' || $hidden_user == '' || $user == 1){

	if ($phone != '' && $phone != '2004' && ($personal_pin != '' || $ab_person_name != '')) {
		$db->setQuery("SELECT phone,
                	                  id,
                	                  name
                	           FROM   abonent_pin
                	           WHERE  abonent_pin.phone = '$phone'
                	           LIMIT 1");

		$check_ab_pin = $db->getResultArray();
		$ab_inf_id = $check_ab_pin[result][0][id];
		if ($check_ab_pin[result][0][phone] == '' && $check_ab_pin[result][0][name] == '') {

			$db->setQuery("INSERT INTO `abonent_pin`
	                                          (`user_id`, `datetime`, `phone`, `pin`, `name`)
	                                    VALUES
	                                          ('$user', NOW(), '$phone', '$personal_pin', '$ab_person_name')");
			$db->execQuery();
		} else {
			$db->setQuery("UPDATE `abonent_pin`
                                      SET `user_id`  = $user,
                                          `datetime` = NOW(),
                                          `pin`      = '$personal_pin',
                                          `name`     = '$ab_person_name'
                                   WHERE  `id`       = '$ab_inf_id'");

			$db->execQuery();
		}
	}

	if ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'fbm') {
		$db->setQuery("SELECT last_user_id FROM fb_chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();

		if ($user == $check_user[result][0][last_user_id]) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($check_user[result][0][last_user_id] == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'fbc') {
		$db->setQuery("SELECT last_user_id FROM fb_comments WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		$last_user_id = $check_user[result][0][last_user_id];

		if ($user == $last_user_id) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($last_user_id == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'mail') {
		$db->setQuery("SELECT last_user_id FROM mail_chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		$last_user_id = $check_user[result][0][last_user_id];

		if ($user == $last_user_id) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($last_user_id == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'chat') {
		$db->setQuery("SELECT last_user_id FROM chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		if ($user == $check_user[result][0][last_user_id]) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($check_user[result][0][last_user_id] == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} elseif ($_REQUEST['ii'] == 1 && $_REQUEST['source'] == 'viber') {
		$db->setQuery("SELECT last_user_id FROM viber_chat WHERE id = '$chat_id'");
		$check_user = $db->getResultArray();
		if ($user == $check_user[result][0][last_user_id]) {
			$user_update = "`user_id` = '$user',";
		} else {
			if ($check_user[result][0][last_user_id] == '') {
				$user_update = "`user_id` = '$user',";
			} else {
				$user_update = "";
			}
		}
	} else {
		if ($_REQUEST['ii'] == 1 && $phone != '') {
			$user_update = "`user_id` = '$user',";
		}
	}


	if ($_REQUEST['source'] == 'fbm') {
		if ($user != 1) {
			$db->setQuery("UPDATE fb_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'fbc') {
		if ($user != 1) {
			$db->setQuery("UPDATE fb_comments SET last_user_id = '$user', `status` = 3 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'mail') {
		if ($user != 1) {
			$db->setQuery("UPDATE mail_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'chat') {
		if ($user != 1) {
			$db->setQuery("UPDATE chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	} elseif ($_REQUEST['source'] == 'viber') {
		if ($user != 1) {
			$db->setQuery("UPDATE viber_chat SET last_user_id = '$user', `status` = 2 WHERE id = '$chat_id' AND `status` < 2");
			$db->execQuery();
		}
	}

	$db->setQuery("UPDATE `incomming_call`
                              SET `user_id`           = '$user',
                                  `extension`         = '$ext',
                                  `phone`             = '$phone',
                                  `client_user_id`    = '$personal_pin',
                                  `client_pid`        = '$personal_id',
                                  `client_name`       = '$ab_person_name',
                                  `client_mail`       = '$s_u_mail',

                                  `phone1`            = '$phone1',
                                  `fb_link`           = '$fb_link',
                                  `viber_address`     = '$viber_address',
                                  `vizit_date`        = '$vizit_datetime',
                                  `vizit_location`    = '$vizit_location',
                                  `client_comment`    = '$client_comment',
								  `out_comment`			= '$out_comment',

                                  `client_satus`      = '$s_u_status',
                                  `client_sex`        = '$client_sex',
                                  `client_birth_year` = '$client_birth_year',
                                  `cat_1`             = '$incomming_cat_1',
                                  `cat_1_1`           = '$incomming_cat_1_1',
                                  `cat_1_1_1`         = '$incomming_cat_1_1_1',
								  
								  `chat_language` 		= '$chat_language',
							  	  `chat_site`			= '$chat_site',
							   	  `inc_status`		= '$inc_status_a',
							   	  `company`			= '$company',
                                  `inc_status_id`     = '$inc_status',
                                  `source_id`         = '$source_id',
								  `space_type`		= '$space_type',
                                  `lid`               = '$lid',
                                  `lid_comment`       = '$lid_comment',
                                  `call_content`      = '$call_content'
                           WHERE  `id`                = '$incom_id'");

	$db->execQuery();
	$db->setQuery("UPDATE crm SET status_id='$incomming_status_1', sub_status_id='$incomming_status_1_1', manager_id='$manager', comment='$manager_comment' WHERE id='$crm_id'");
	$db->execQuery();
	$db->setQuery("UPDATE `incomming_call`
			                  SET  processing_end_date = NOW()
			               WHERE  `id` = '$incom_id' AND NOT ISNULL(asterisk_incomming_id) AND ISNULL(processing_end_date)");
	$db->execQuery();

	$site_array = explode(",", $my_site);
	$db->setQuery("DELETE FROM incoming_call_site WHERE incomming_call_id = '$incom_id'");
	$db->execQuery();

	//			foreach ($site_array AS $site_id){
	//			    $db->setQuery("INSERT INTO incoming_call_site (incomming_call_id, site_id) values ('$incom_id', $site_id)");
	//			    $db->execQuery();
	//			}

	if ($lid == 1) {
		$db->setQuery("SELECT * 
                               FROM   outgoing_call
                               WHERE  incomming_call_id = '$incom_id' AND outgoing_type_id = 4");

		$check = $db->getNumRow();
		if ($check == 0) {
			$db->setQuery("INSERT INTO `outgoing_call`
                    			           SET `user_id`           = '$user',
                        			           `date`              =  NOW(),
                        			           `start_date`        =  NOW(),
                                               `incomming_call_id` = '$incom_id',
                        			           `phone`             = '$phone',
                        			           `outgoing_type_id`  = '4',
                        			           `status_id`         = '1',
                                               `lid_comment`       = '$lid_comment',
                        			           `actived`           = '1'");
			$db->execQuery();
		}
	}

	// 		}else{
	// 			$error='თქვენ არ გაქვთ შეცვლის უფლება';
	// 		}
	if (($chat_id != '' || $chat_id != 0)) {

		$source              	= $_REQUEST['source'];

		if ($_REQUEST['ii'] == 1 && $source == 'site') {
			$db->setQuery("UPDATE `site_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
			                     SET  processing_end_date = NOW()
			                  WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'fbm') {
			$db->setQuery("UPDATE `fb_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
            			          SET  processing_end_date = NOW()
            			       WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'fbc') {
			$db->setQuery("UPDATE `fb_comments` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
            			          SET  processing_end_date = NOW()
            			       WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'mail') {
			$db->setQuery("UPDATE `mail_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
			                     SET   processing_end_date = NOW()
			                   WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} elseif ($_REQUEST['ii'] == 1 && $source == 'viber') {
			$db->setQuery("UPDATE `viber_chat` SET `status`='3' WHERE (`id`='$chat_id')");
			$db->execQuery();

			$db->setQuery("UPDATE `incomming_call`
			                      SET  processing_end_date = NOW()
			                   WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
			$db->execQuery();
		} else {
			if ($_REQUEST['ii'] == 1 && $phone != '2004') {
				$db->setQuery("SELECT `last_request_datetime`,
            							  `status`,
            							   TIME_TO_SEC(TIMEDIFF(NOW(),last_request_datetime)) as timedifference,
            							   who_rejected
            						from   chat 
            						where `id` = '$chat_id';");
				$last_request_res = $db->getResultArray();
				$timedifference   = $last_request_res[result][0]['timedifference'];
				$who_rejected     = $last_request_res[result][0]['who_rejected'];
				$query_addition   = '';

				if ($timedifference >= 10 && $who_rejected == '') {
					$query_addition .= ", `who_rejected` = 0";
				} else if ($timedifference < 10 && $who_rejected == '') {
					$query_addition .= ", `who_rejected` = 1";
				}

				$db->setQuery("UPDATE `chat` SET
    									  `status` = '4' 
    				                       $query_addition
    							   WHERE  `id`     = '$chat_id';");
				$db->execQuery();

				$db->setQuery("UPDATE `incomming_call`
    				                  SET  processing_end_date = NOW()
    				               WHERE  `id` = '$incom_id' AND (ISNULL(processing_end_date) OR processing_end_date='')");
				$db->execQuery();
			}
		}
	}
	// 	}else{
	// 		$error='თქვენ არ გაქვთ არჩეული ექსთენშენი, გთხოვთ გახვიდეთ სისტემიდან და აირიოთ ';
	// 	}

}

function note_incomming()
{
	global $error;
	$incom_id	= $_REQUEST['id'];
	$user		= $_SESSION['USERID'];
	$group		= checkgroup($user);

	//      $inc_user = mysql_fetch_array(mysql_query("SELECT user_id 
	//                                                 FROM   incomming_call
	//                                                 WHERE  id = $incom_id"));

	//      if ($group==2 || $group==3) {

	mysql_query("UPDATE `incomming_call`
					    SET `noted`              = '1',
                            `monitoring_user_id` =  $user
							 WHERE `id`          = '$incom_id'");

	// 	}elseif ($group==1 && $inc_user[user_id] == $user){

	// 	    mysql_query("UPDATE `incomming_call`
	//             	        SET `noted`              ='1',
	//             	            `monitoring_user_id` = $user
	//             	     WHERE  `id`                 = '$incom_id'");
	// 	}else{
	// 	     $error='ეს ფუნქცია თქვენთვის შეზღუდულია';
	//  	}

}

function num_block_incomming()
{
	global $error;
	$phone			= $_REQUEST['phone'];
	$block_comment	= $_REQUEST['block_comment'];
	$user			= $_SESSION['USERID'];
	$group			= checkgroup($user);
	$c_date			= date('Y-m-d H:i:s');

	if ($group == 2 || $group == 1 || $group == 3) {
		mysql_query("INSERT INTO `black_list`
						(`user_id`,`date`,`phone`,`comment`)
					VALUES
						('$user','$c_date', '$phone','$block_comment');");
	} else {
		$error = 'ეს ფუნქცია თქვენთვის შეზღუდულია';
	}
}

function get_comment($hidden_id)
{
	global $db;
	$data = "";
	$db->setQuery("SELECT   `id`,
                            `user_id`,
                            `incomming_call_id`,
                            `comment`,
                            `datetime`
                   FROM     `chat_comment`
                   WHERE     parent_id = 0 AND incomming_call_id = $hidden_id AND active = 1
                   ORDER BY `datetime` DESC");

	$req = $db->getResultArray();
	foreach ($req[result] as $res) {
		$data .= '<div style="margin-top: 15px; padding:10px;">
                        <input type=hidden id="hidden_comment_id_' . $res['id'] . '" value="' . $res['id'] . '"/>
						<span style="color: #369; font-weight: bold;">' . get_user($res['user_id']) . '</span> ' . $res['datetime'] . '
						<img id="edit_comment" my_id="' . $res['id'] . '" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="' . $res['id'] . '" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"> <br><br>
                        <div style="border: 1px #D7DBDD solid; padding: 14px; border-radius:20px; background-color:#D7DBDD; "> ' . $res['comment'] . '</div>
                        <br/><span id="get_answer" class="' . $res['id'] . '"  style="color: #369; font-size:15px;     font-size: 11px;
                        margin-left: 10px;  ">პასუხის გაცემა</span>
                 </div>
                 <div style="margin-left: 50px; margin-top: 10px;">
                    ' . get_answers($res['id'], $hidden_id) . '
                 </div>';
	}


	return $data;
}


function getOptionals($chat_id)
{


	global $db;

	$chat_source = $_REQUEST["source"];
	$chat_id = $_REQUEST['chat_id'];

	if ($chat_source) {
		$db->setQuery("	SELECT 	id, name
					FROM	source
					WHERE 	`key` = '$chat_source'	");
		$source_res = $db->getResultArray();
		$chat_source_a_id = $source_res[result][0][id];
		$chat_source_a = $source_res[result][0][name];
	} else {
		$chat_source_a = "Source not found";
	}


	if ($chat_source == "fbm" && $chat_id != '') {
		$db->setQuery("	SELECT 	fb_account.`name`, fb_account.`id`
						FROM 	fb_account 
						JOIN 	fb_chat ON `fb_chat`.`id` = $chat_id AND `fb_chat`.`account_id` = `fb_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0][name];
		$chat_site_id = $result_source[result][0][id];
	} elseif ($chat_source == "viber" && $chat_id != '') {
		$db->setQuery("	SELECT 	viber_account.`name`, viber_account.`id`
						FROM 	viber_account 
						JOIN 	viber_chat ON `viber_chat`.`id` = $chat_id AND `viber_chat`.`account_id` = `viber_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0][name];
		$chat_site_id = $result_source[result][0][id];
	} elseif ($chat_source == "mail" && $chat_id != '') {
		$db->setQuery("	SELECT 	mail_account.`name`, mail_account.`id`
						FROM 	mail_account 
						JOIN 	mail_chat ON `mail_chat`.`id` = $chat_id AND `mail_chat`.`account_id` = `mail_account`.`id`");
		$result_source = $db->getResultArray();
		$chat_site = $result_source[result][0][name];
		$chat_site_id = $result_source[result][0][id];
	} else {
		$chat_site = "ID not found";
	}

	$optData = array('chat_source_a_id' => $chat_source_a_id, 'chat_source_a' => $chat_source_a, 'chat_site' => $chat_site, 'chat_site_id' => $chat_site_id, 'chat_source' => $chat_source);


	return $optData;
}

function web_site($id)
{
	global $db;
	$data = '';

	$db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");

	$res = $db->getResultArray();
	foreach ($res[result] as $value) {
		$db->setQuery("SELECT id
	                   FROM  `incoming_call_site`
	                   WHERE incoming_call_site.incomming_call_id = '$id' AND site_id = '$value[id]'");
		$check = $db->getNumRow();

		if ($check > 0) {
			$data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
		} else {
			$data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
		}
	}

	return $data;
}

function get_search_site($id)
{
	global $db;
	$data = '';

	$db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");

	$data = $db->getSelect($id);

	return $data;
}

function get_search_transaction_type($id)
{
	global $db;
	$data = '';

	$db->setQuery("SELECT `name` AS `id`, `name`
				   FROM   `transaction_type`
				   WHERE   actived = 1");

	$data = $db->getSelect($id);

	return $data;
}


function Getclientstatus($id)
{

	if ($id == 1) {
		$data .= '<option value="0">----</option>
                  <option value="1" selected="selected">აქტიური</option>
                  <option value="2">პასიური</option>';
	} elseif ($id == 2) {
		$data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2" selected="selected">პასიური</option>';
	} else {
		$data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2">პასიური</option>';
	}

	return $data;
}

function get_s_u_sex($id)
{

	if ($id == 1) {
		$data .= '<option value="0">----</option>
                  <option value="1" selected="selected">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
	} elseif ($id == 2) {
		$data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2" selected="selected">მამრობითი</option>';
	} else {
		$data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
	}

	return $data;
}

function Getincstatus($object_id)
{
	global $db;
	$data = '';
	$db->setQuery("SELECT `id`,
                          `name`
				   FROM   `inc_status`
				   WHERE  `actived` = 1");

	$data = $db->getSelect($object_id);

	return $data;
}

function Getsource($object_id)
{
	global $db;
	$data = '';
	$db->setQuery("SELECT 	`id`,
                            `name`
				    FROM 	`source`
				    WHERE 	 actived=1");


	$data = $db->getSelect($object_id);

	return $data;
}

function get_transfer_ext($ext, $check)
{
	$data = '';
	if ($check == 1) {
		$req = mysql_query("SELECT `id`,
                                   `name`
                            FROM   `crystal_users`
                            WHERE  `group_id` IN(1,2,3) AND `actived` = 1");

		$data .= '<option value="0" selected="selected">----</option>';
		while ($res = mysql_fetch_assoc($req)) {
			if ($res['id'] == $ext) {
				$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
			} else {
				$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
			}
		}
	} else {
		$req = mysql_query("SELECT   extention_login.extention 
                            FROM    `extention_login` 
                            JOIN     crystal_users ON crystal_users.last_extension = extention_login.extention
                            WHERE    extention_login.id < 11 AND crystal_users.logged = 1
                            ORDER BY extention_login.extention ASC");


		$data .= '<option value="0" selected="selected">----</option>';
		while ($res = mysql_fetch_assoc($req)) {
			if ($res['extention'] == $ext) {
				$data .= '<option value="' . $res['extention'] . '" selected="selected">' . $res['extention'] . '</option>';
			} else {
				$data .= '<option value="' . $res['extention'] . '">' . $res['extention'] . '</option>';
			}
		}
	}
	return $data;
}

function Getcategory($category_id)
{

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `category`
						WHERE actived=1 && parent_id=0 ");


	$data .= '<option value="0" selected="selected">----</option>';
	while ($res = mysql_fetch_assoc($req)) {
		if ($res['id'] == $category_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_lang($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `user_language`
                   WHERE   actived = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == 1) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_inc_status($id)
{
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `inc_status`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_company($id)
{
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `company`
					WHERE   `actived` = 1 AND `disabled` = 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
function get_manager($id)
{
	global $db;

	$db->setQuery("	SELECT		users.id,
								user_info.name
					FROM 		users
					LEFT JOIN 	user_info ON user_info.user_id = users.id
					WHERE 		users.actived = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
function get_space_type($id)
{
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `space_type`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';

	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_cat_1($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");

	$req = $db->getResultArray();
	$data .= '<option value="0" selected="selected">----</option>';
	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_cat_1_1($id, $child_id)
{
	global $db;

	$db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `info_category`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}

function get_cat_1_1_1($id, $child_id)
{
	global $db;
	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;

	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}

	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}




// ####################

function get_status_1($id)
{
	global $db;

	$db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_status`
                   WHERE   actived = 1 AND `parent_id` = 0");

	$req = $db->getResultArray();
	$data .= '<option value="0" selected="selected">----</option>';
	foreach ($req[result] as $res) {
		if ($res['id'] == $id) {
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function get_status_1_1($id, $child_id)
{
	global $db;

	$db->setQuery("SELECT  `id`,
                           `name`,
						   `comment`
                   FROM    `info_status`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");

	$req = $db->getResultArray();

	$data .= '<option value="0" selected="selected">----</option>';
	$i = 0;
	foreach ($req[result] as $res) {
		if ($res['id'] == $child_id) {
			$data .= '<option title="' . $res['comment'] . '" value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option title="' . $res['comment'] . '" value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		$i = 1;
	}
	if ($i == 0 && $id > 0) {
		$data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
	}

	return $data;
}


function get_crm_status($id)
{
	global $db;

	$db->setQuery("SELECT `id`, `name` FROM `info_status` WHERE `parent_id` = 0 AND `actived` = 1 ");
	$res = $db->getResultArray();

	$data = '<option value=0>----</option>';

	foreach ($res['result'] as $opt) {
		if ($opt[id] == $id) {

			$data .= '<option selected value=' . $opt[id] . '>' . $opt[name] . '</option>';
		} else {
			$data .= '<option value=' . $opt[id] . '>' . $opt[name] . '</option>';
		}
	}

	return $data;
}


// ####################




function Getcomunication_tab()
{

	global $db;
	$db->setQuery("SELECT `key`,
                           background_image_name,
                           actived
                  FROM    `source`
				  WHERE    actived = 1 AND `key` != '0'
				  ORDER BY position");

	$req = $db->getResultArray();

	foreach ($req[result] as $res) {
		$data .= '<li class="incoming_chat_tab_' . $res[key] . '"><a href="#incoming_chat_tab_' . $res[key] . '"><img src="media/images/icons/comunication/' . $res[background_image_name] . '" height="21" width="21"><span class="' . $res[key] . '_chat fabtext" >0</span></a></li>';
	}

	return $data;
}



function getlastphone($phone)
{
	global $db;
	if ($phone == '') {
		$res = '';
	} else {
		$db->setQuery("SELECT    	incomming_call.id AS id,
                                    incomming_call.`date` AS call_date,
            						incomming_call.`date` AS record_date,
            						incomming_call.phone AS `phone`,
                                    incomming_call.ipaddres,
            						incomming_call.asterisk_incomming_id,
            						user_info.`name` AS operator,
            						IF(ISNULL(asterisk_call_log.user_id),chat_user_name.`name`,user_info.`name`) AS operator,
            						IF(ISNULL(asterisk_call_log.user_id),chat.answer_user_id,asterisk_call_log.user_id ) as `user_id`,
            						/* incomming_call.inc_status_id AS `inc_status`, */
            						incomming_call.cat_1,
            						incomming_call.cat_1_1,
                                    incomming_call.cat_1_1_1,
									incomming_call.status_1,
            						incomming_call.status_1_1,
            						incomming_call.space_type,
            						incomming_call.chat_language,
            						incomming_call.inc_status,
            						incomming_call.company,
            						incomming_call.call_content AS call_content,
            						incomming_call.source_id,
            						incomming_call.client_user_id AS personal_pin,
            						incomming_call.client_pid AS personal_id,
            						incomming_call.web_site_id,
            						incomming_call.client_name,

                                    incomming_call.client_comment,
									incomming_call.out_comment,
                                    incomming_call.vizit_location,
                                    incomming_call.vizit_date,
                                    incomming_call.fb_link,
                                    incomming_call.viber_address,
                                    incomming_call.phone1,

            						incomming_call.client_satus,
            						incomming_call.client_sex,
                                    incomming_call.client_mail,
            						incomming_call.chat_id,
                                    incomming_call.lid,
                                    incomming_call.lid_comment,
                                    asterisk_call_log.source AS `ast_source`,
                                    chat.ip,
            						site_chat_id,
            						fb_chat_id,
            						mail_chat_id,
                                    messenger_chat_id,
                                    video_call_id
                        FROM 	    incomming_call
                        LEFT JOIN   asterisk_call_log on asterisk_call_log.id = incomming_call.asterisk_incomming_id
                        LEFT JOIN   users ON users.id=asterisk_call_log.user_id
                        LEFT JOIN   user_info ON users.id = user_info.user_id
                        LEFT JOIN   chat on incomming_call.chat_id = chat.id
                        LEFT JOIN   users AS chat_user ON chat_user.id=chat.answer_user_id
                        LEFT JOIN   user_info AS chat_user_name ON chat_user.id=chat_user_name.user_id
                        WHERE      	incomming_call.phone = '$phone'
                        ORDER BY	incomming_call.id DESC
                        LIMIT 1");
		$res = $db->getResultArray();
	}
	return $res[result][0];
}


function Getincomming($crm_id, $chat_id)
{
	global $db;

	$wh = "crm.id = $crm_id";


	$db->setQuery("SELECT    	incomming_call.id AS id,
                                crm.id AS 'crm_id',
                                crm.manager_id AS 'manager',
								incomming_call.asterisk_incomming_id,
								crm.crm_phone,
                                IF(NOT ISNULL(incomming_call.asterisk_incomming_id),
                                    CASE 
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) <= incomming_call.processing_start_date 
                    						THEN SEC_TO_TIME(UNIX_TIMESTAMP(processing_end_date)-UNIX_TIMESTAMP(processing_start_date))
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) > incomming_call.processing_start_date
                    						AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) >= incomming_call.processing_end_date
                    						THEN '00:00:00'
                    					WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) > incomming_call.processing_start_date
                    						AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) < incomming_call.processing_end_date 
                    						THEN SEC_TO_TIME(UNIX_TIMESTAMP(incomming_call.processing_end_date) - (UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time))
                    			    END,
                                '00:00:00') AS `processed_time`,
        						user_info.`name` AS operator,
								crm.status_id AS 'status_1',
								crm.res_comment,
								crm.comment,
								crm.datetime AS crm_date,
								FROM_UNIXTIME(call_datetime) AS call_date,
                                asterisk_call_log.source AS `ast_source`,
                                SEC_TO_TIME(asterisk_call_log.talk_time) AS `duration`,
			                    SEC_TO_TIME(asterisk_call_log.wait_time) AS `wait_time`,
								incomming_call.chat_id,
								incomming_call.fb_chat_id,
								incomming_call.mail_chat_id
                    FROM 	    crm
                    LEFT JOIN	incomming_call ON crm.incomming_request_id = incomming_call.id
                    LEFT JOIN   asterisk_call_log on asterisk_call_log.id = incomming_call.asterisk_incomming_id
                    LEFT JOIN   users ON users.id=asterisk_call_log.user_id
                    LEFT JOIN   user_info ON users.id = user_info.user_id
					WHERE      	$wh 
					LIMIT 1");

	$req = $db->getResultArray();
	$res = $req[result][0];

	return $res;
}

function GetPage($res = '', $k_phone, $imby, $ipaddres, $ab_pin, $optData = '')
{
	global $db;
	if ($res[id] == '') {
		$db->setQuery("INSERT INTO incomming_call
                               SET user_id = ''");
		$db->execQuery();
		$hidde_inc_id = $db->getLastId();
		$db->setQuery("DELETE FROM incomming_call WHERE id = '$hidde_inc_id'");
		$db->execQuery();
		$processing_start_date = date('Y-m-d H:i:s');
	} else {
		$hidde_inc_id = $res[id];
		$db->setQuery("UPDATE incomming_call
	                      SET processing_start_date = NOW()
                       WHERE (ISNULL(processing_end_date) OR processing_end_date = '') 
                       AND    id = '$hidde_inc_id'");
		$db->execQuery();
	}

	$db->setQuery("SELECT    COUNT(*) AS `unrid`
                   FROM      queries
                   LEFT JOIN queries_history ON queries_history.queries_id = queries.id AND queries_history.user_id = '$_SESSION[USERID]'
                   WHERE     ISNULL(queries_history.id) AND queries.actived = 1");
	$res_queries = $db->getResultArray();

	if ($res_queries[result][0][unrid] > 0) {
		$queries_display = "";
		$queries_count   = $res_queries[result][0][unrid];
	} else {
		$queries_display = "display:none";
		$queries_count   = '';
	}

	$phone = $_REQUEST['phone'];
	$num   = 0;
	$lid_check = '';
	if ($res[phone] == "") {
		if ($number == '') {
			$num = $phone;
		} else {
			$num = $number;
		}
	} else {
		$num = $res[phone];
	}
	$lid_comment_hidde = "display:none;";
	if ($res[id] != '') {
		$dis = 'disabled="disabled"';
	} else {
		$dis = '';
	}
	if ($res[lid] == 1) {
		$lid_check = 'checked';
		$lid_comment_hidde = '';
	}
	if ($ab_pin == '' || $res[mail_chat_id] > 0) {
		$ab_pin = $res[personal_pin];
	}

	$person_pin = $ab_pin;

	$crm_count = 0;
	if ($person_pin != '' || $num != '') {

		$db->setQuery("SELECT 	  COUNT(*) AS `count`  
                       FROM 	  outgoing_compaign_request ar 
                       LEFT JOIN  outgoing_compaign_request_details ard ON ar.id = ard.request_id
                       LEFT JOIN  aoutgoing_compaign_base arb ON arb.request_id = ard.id
                       WHERE      ar.call_type_id = 4 AND ar.actived=1 AND ard.actived = 1 
                       AND       ((arb.number_2 = '$person_pin' AND arb.number_2 != '')  OR (arb.phone_number = '$num' AND arb.phone_number != ''))
                       AND        DATE(ard.set_time) >=CURDATE() - INTERVAL 7 DAY AND DATE(ard.set_time) <= CURDATE()");

		//$check_crm_count = $db->getResultArray();
		$crm_count       = $check_crm_count[result][0][count];
	}

	if ($crm_count == 0) {
		$crm_display = 'display:none;';
	} else {
		$crm_display = '';
	}
	$person_name = $res['client_name'];
	$person_user = $res['personal_pin'];

	if ($res['person_name'] == '') {

		$db->setQuery(" SELECT   `name` AS `name`,
                                 `pin`  AS `user_id`
                        FROM     `abonent_pin`
                        WHERE   (`pin` != '' OR `name` != '') AND phone = '$num' AND phone != 'Anonymous'
                        ORDER BY  id DESC
                        LIMIT 1");

		$pers_info   = $db->getResultArray();
		$person_name = $pers_info[result][0][name];
		$person_user = $pers_info[result][0][user_id];
	}

	$db->setQuery("SELECT  `comment`
				   FROM    `black_list`
				   WHERE    phone = '$num' AND phone != 'Anonymous' AND `comment` != ''
				   ORDER BY id DESC
				   LIMIT 1");

	$req                 = $db->getResultArray();
	$last_blocked_reason = $req[result][0];

	$db->setQuery("SELECT  `id`,
                           `name`,
						   `rand_name`,
                           `file_date`
				   FROM    `file`
				   WHERE   `incomming_call_id` = '$res[id]'");

	$increm = $db->getResultArray();

	$db->setQuery(" SELECT     IF(ISNULL(user_info.`name`),'ოპერატორი',user_info.`name`) AS `name`,
                               IFNULL(extention.extention,0) AS last_extension
                    FROM      `users`
                    LEFT JOIN  extention ON users.extension_id = extention.extention
                    LEFT JOIN `user_info` ON `user_info`.`user_id` = users.id
                    WHERE      users.`id` = '$_SESSION[USERID]'");

	$req     = $db->getResultArray();
	$getuser = $req[result][0];

	if ($number == '') {
		if ($res['chat_id'] != 0 && $res['chat_id'] != '' &&  $res['chat_id'] != null) {
			$imchat = 1;
		} else {
			$imchat = 0;
		}
	} else {
		$imchat = 0;
	}

	if ($res['asterisk_incomming_id'] != 'null' && $res['asterisk_incomming_id'] != null) {
		$chatcom = 'display:block;';
		$callcom = 'display:none;';
	} else {
		$chatcom = 'display:none;';
		$callcom = 'display:block;';
	}

	if ($_REQUEST[chat_id] == '' || $_REQUEST[site_chat_id] != '') {
		$mychat_id = 0;
	} else {
		$mychat_id = $_REQUEST[chat_id];
	}

	$transfer_resps = 0;
	if ($res['chat_id'] != '' || $res['chat_id'] != 0) {
		$mychat_id = $res['chat_id'];
		$transfer_resps = 0;
	}
	$transfer_resps = 0;
	$chat_source = $_REQUEST["source"];
	if ($res['fb_chat_id']) {
		$chat_source = 'fbm';
		$mychat_id = $res['fb_chat_id'];
		$transfer_resps = 0;
	}
	if ($res['fb_comment_id']) {
		$chat_source = 'fbc';
		$mychat_id = $res['fb_comment_id'];
		$transfer_resps = 0;
	}
	if ($res['mail_chat_id']) {
		$chat_source = 'mail';
		$mychat_id = $res['mail_chat_id'];
		$transfer_resps = 0;
	}
	if ($res['viber_chat_id']) {
		$chat_source = 'viber';
		$mychat_id = $res['viber_chat_id'];
		$transfer_resps = 0;
	}

	$sms = '';

	$chat_disable = '';
	$chat_select = '';
	$border = 'border-bottom: 2px solid #333 !important;';

	if ($chat_source == "fbc") {
		$db->setQuery("	SELECT	message, time
						FROM	fb_feeds 
						WHERE	id = (SELECT feeds_id FROM fb_comments WHERE id = '" . $mychat_id . "')");

		$post_arr = $db->getResultArray();

		$post = '<div class="chat_fb_comment_post"><div class="chat_fb_coment_post_title"><span style="font-size: 11px;color: #878787;text-align: right;">' . $post_arr[result][0][time] . '</span></div><div class="chat_fb_comment_post_inner"> ' . $post_arr[result][0][message] . '</div></div>';
	} else {
		$post = '';
	}


	
	if($res[chat_id] > 0){
        $db->setQuery(" SELECT  chat.`name`,
                                chat.`device`,
                                chat.`browser`,
                                chat.`ip`,
                                chat_details.message_client,
                                chat_details.message_operator,
                                chat_details.message_datetime,
                                IF(chat_details.operator_user_id > 0,'ოპერატორი',chat.name)AS `op_name`,
                                chat_details.id
                        FROM `chat`
                        LEFT JOIN `chat_details` ON chat.id = chat_details.chat_id AND chat_details.message_operator != 'daixura'
                        
                        WHERE `chat`.id = $res[chat_id]
                        ORDER BY chat_details.message_datetime");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        
        $db->setQuery("SELECT  `chat_details`.`id`
            FROM    `chat_details`
            WHERE   `chat_details`.`chat_id` = '$res[chat_id]' AND chat_details.operator_user_id != '$_SESSION[USERID]'
            ORDER BY chat_details.id DESC LIMIT 1");
        
        $req            = $db->getResultArray();
        $ge             = $req[result][0];
        $chat_detail_id = $ge[id];
        
        foreach($res1[result] AS $req){
            if (strpos($req[message_client], 'შემოუერთდა ჩატს.') === FALSE) {
                $name = $req[op_name];
            }else{
                $name = '';
            }
            if($req[message_client]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[op_name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">'.$req[message_operator].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                            <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">'.$name.'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">'.$req[message_client].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req[message_datetime].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
        }
        
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border = 'display:none;';
        }
        
        $db->setQuery(" SELECT	IF(who_rejected = 1, 'ოპერატორმა გათიშა', 'აბონენტმა გათიშა') AS rejecter
            FROM   `chat`
            WHERE  `answer_user_id` != 0 AND `status` > 2
            AND     id               = '$res[chat_id]'");
        
        $rejecter_res = $db->getResultArray();
        $rejecter     = $rejecter_res[result][0]['rejecter'];
        
        $sms .= '<tr style="height: 17px;"><td colspan=2 style="font-weight:bold;color:#2681DC; word-break: break-word;">'.$rejecter.'</td></tr>';
    }
    
    if($res[fb_chat_id] > 0){
        $db->setQuery("SELECT     fb_chat.last_user_id,
                                    fb_messages.datetime,
                                    fb_chat.sender_name AS `name`,
                                    fb_messages.text,
                                    (SELECT url from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media,
                                    fb_messages.id,
                                    IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_messages.user_id),'') as operator,
                                    (SELECT type from fb_attachments WHERE messages_id=fb_messages.id LIMIT 1) as media_type
                        FROM      `fb_chat`
                        LEFT JOIN `fb_messages` ON fb_chat.id = fb_messages.fb_chat_id# AND chat_details.message_operator != 'daixura'
                        WHERE     `fb_chat`.id = $res[fb_chat_id]
                        ORDER BY   fb_messages.datetime");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        
        foreach($res1[result] AS $req){
            if ($req[media_type] == 'fallback') {
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'file'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'video'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }else{
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
            }
            
            if($req[operator]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">'.$req[name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">'.$req[text].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[operator].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">'.$req[text].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
            $chat_detail_id=$req['id'];
        }
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border       = 'display:none;';
        }
    }
    
    
    if($res[fb_comment_id] > 0){
        $db->setQuery("SELECT     fb_comments.last_user_id,
                                    fb_comments.time,
                                    fb_comments.fb_user_name AS `name`,
                                    fb_comments.message,
                                    (SELECT url from fb_attachments WHERE messages_id=fb_comments.id LIMIT 1) as media,
                                    fb_comments.id,
                                    IFNULL((SELECT `name` from user_info WHERE user_info.user_id=fb_comments.user_id),'') as operator,
                                    (SELECT type from fb_attachments WHERE messages_id=fb_comments.id LIMIT 1) as media_type
                        FROM      `fb_comments`
                        WHERE     `fb_comments`.id = $res[fb_comment_id]
                        ORDER BY   fb_comments.time");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        
        foreach($res1[result] AS $req){
            if ($req[media_type] == 'fallback') {
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'file'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }elseif ($req[media_type] == 'video'){
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 30px;' src='../../media/images/icons/file_icons.png'></a>";
            }else{
                if(!empty($req['media'])) $req[text] = $req[text]. "<a href='$req[media]' target='blank'><img style='border-radius: 10%;max-height: 200px;' src='$req[media]'></a>";
            }
            
            if($req[operator]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold;font-size: 12px;">'.$req[name].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px;font-size: 12px;">'.$req[text].'</p>
                                    <div>
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[operator].'</p>
                                    <p style="padding-bottom: 7px; padding-top: 7px; line-height: 20px; font-size: 12px;">'.$req[text].'</p>
                                    <div style="text-align: right;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
            $chat_detail_id=$req['id'];
        }
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select  = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border       = 'display:none;';
        }
    }
    
    
    if($res[mail_chat_id] > 0){
        
        $db->setQuery(" SELECT 		mail.user_id,
                                    mail_detail.datetime,
                                    IF(mail.sender_name = '', mail.sender_address, mail.sender_name) AS `name`,
                                    mail_detail.body,
                                    mail_detail.id,
                                    IFNULL((SELECT `name` from user_info WHERE user_info.user_id = mail_detail.user_id),'') as operator,
                                    mail.subject
                                    
                        FROM      `mail`
                        LEFT JOIN `mail_detail` ON mail.id = mail_detail.mail_id
                        WHERE     `mail`.id = '$res[mail_chat_id]'
                        ORDER BY   mail_detail.datetime");
        
        $res1           = $db->getResultArray();
        $chat_detail_id = 0;
        foreach($res1[result] AS $req){
            
            $db->setQuery("SELECT `name` FROM `mail_attachment` WHERE mail_detail_id='$req[id]'");
            $fq          = $db->getResultArray();
            $req['text'] = preg_replace('/<base[^>]+href[^>]+>/', '', $req['text']);
            
            foreach($fq[result] AS $file){
                if ($file[check_mesage] == 1) {
                    $req['text']      = $req[text]. "<a href='$file[name]' target='_blank'>$file[name]</a><br>";
                }else{
                    $req['text'] = preg_replace('/(?<=cid:'.$file[name].').*?(?=>)/', $file[name].'"',  $req['text']);//preg_grep('/(?<=cid:).*?(?=@)/', array($req['4'],'123'));
                    $req['text'] = str_replace('cid:'.$file[name], 'https://crm.my.ge/', $req['text']);
                    $file[patch]    = str_replace('/var/www/html/','',$file[name]);
                    $req['text']     = $req['text']. "<a href='https://crm.my.ge/$file[name]'>$file[name]</a><br>";
                }
            }
            
            $subject = '';
            if($req[subject] != ''){
                $subject = 'თემა: '.$req[subject];
            }
            
            if($req[operator]==''){
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                           <div style="font-family: BPG_baner !important; float: left; background: #fff none repeat scroll 0 0; padding: 7px 11px; color: #212121; max-width: auto; max-width: 85%;  overflow-wrap: break-word; border: 1px solid #DEDEDE; border-radius: 5px;">
                    	            <p style="font-weight: bold; font-size: 12px;">'.$req[name].'</p>
                                    <p style="font-weight: bold; font-size: 12px;">'.$subject.'</p>
                                    <p style="padding-bottom: 7px; line-height: 20px;font-size: 12px;">'.$req[text].'</p>
                                    <div style="padding-top: 7px;">
                                        <p style="width: 100%; color: #878787;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }else{
                $sms .= '<tr style="height: 17px;">
            	            <td colspan="2" style=" word-break: break-word;">
                                <div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">
                    	            <p style="text-align: right; font-weight: bold; font-size: 12px;">'.$req[operator].'</p>
                                    <p style="padding-top: 7px; line-height: 20x; font-size: 12px;">'.$req[text].'</p>
                                    <div style="text-align: right; padding-top: 7px;">
                                        <p style="color: #878787; width: 100%;">'.$req['datetime'].'</p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:10px;"></tr>';
            }
            $chat_detail_id=$req['id'];
        }
        
        
        $db->setQuery("	SELECT     mail.sender_address AS `name`
            FROM      `mail`
            LEFT JOIN `mail_detail` ON mail.id = mail_detail.mail_id
            WHERE     `mail`.id = '$res[mail_chat_id]'");
        
        $mail_info = $db->getResultArray();
        
        if ($res[client_mail] == '') {
            $mail_name = $mail_info[result][0][name];
        }else{
            $mail_name = $res[client_mail];
        }
        
        if($imby != '1'){
            $chat_disable = 'disabled';
            $chat_select = 'border-bottom: 2px solid rgb(51, 51, 51) !important;';
            $border = 'display:none;';
        }
    }


	$display_standart_amount_action = "block";
	$display_in_out_amount_action   = "none";

	$data  .= '
	<div id="dialog-form">
		<div style="display: flex; flex-direction: row;">
		<div class="communication_chat_style" id="gare_div" style="position:inherit;margin:0!important;">
			<fieldset class="chat_top">
			<table id="blocklogo" >
					<tr>
						<td>
							<img id="chat_live" src="media/images/icons/lemons_chat.png" alt="ლაივი" title="მიმდინარე ჩატი" width="20px" style="filter: brightness(0.1);  border-bottom: 2px solid #333 !important;">
						</td>
					</tr>
				</table>
			</fieldset>
			<fieldset class="communication_chat">
			<!-- <span check_zoom="0" id="zomm_div" style="float: right; cursor: pointer;">>></span> -->
				<div style="float:left;height:650px;" id="chatcontent" class="l_chatcontent" >
					<div class="chat_scroll" id="chat_scroll" style="height:650px;width: 288px;">
					<table style="width: 100%;" id="log">
					'.$post.'
						'.$sms.'
					</table>
					<div style="display:flex;flex-direction: column;">
							<span id="span_seen" style="display:none;text-align: end; float:right; margin-right:5px;">&#10004; seen</span>
							<span id="chat_close_text" style="display:none; text-align: end; float:right; margin-right:5px;">მომხმარებელმა დატოვა ჩატი</span>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
    	<div class="dialog-style">
    		<input type="hidden" value="' . $imchat . '" id="imchat">
    		<input type="hidden" value="' . $res[fb_chat_id] . '" id="fb_chat">
            <input type="hidden" value="' . $res['mail_chat_id'] . '" id="mail_chat">
            <input type="hidden" value="' . $res['viber_chat_id'] . '" id="viber_chat">
            <input type="hidden" value="' . $res['source_id'] . '" id="source_id">
    	    <input type="hidden" value="' . $_SESSION[USERID] . '" id="chat_user_id" chat_user_name="' . $getuser[name] . '" ext="' . $getuser[last_extension] . '">
			

			<div class="communication_fieldset_style" style="float: left;">
				 <div id="side_menu" class="communication_side_menu" style="float: left;">

				 <span class="crm_info communication_side_menu_button" onclick="show_right_side(\'crm_info\')"><img class="communication_side_menu_img" style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/auto_dialer_file.png" alt="24 ICON" title="CRM ინფო"></span>


        			<span class="info communication_side_menu_button" onclick="show_right_side(\'info\')"><img class="communication_side_menu_img"  style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/lemons_info.png" alt="24 ICON" title="ინფო"></span>
              
                    <span class="client_history communication_side_menu_button"  onclick="show_right_side(\'client_history\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_client_history.png" alt="24 ICON"  title="ისტორია"></span>
                    				
                    <span class="task communication_side_menu_button"  onclick="show_right_side(\'task\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_task.png" alt="24 ICON"  title="დავალება"></span>

                    <span class="client_conversation communication_side_menu_button"  onclick="show_right_side(\'client_conversation\')"><img class="communication_side_menu_img" src="media/images/icons/client_conversation.png" alt="24 ICON"  title="დავალება"></span>

    				<span class="sms communication_side_menu_button" onclick="show_right_side(\'sms\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_sms.png" alt="24 ICON" title="ესემეს"></span>
    				
    				<span class="incomming_mail communication_side_menu_button" onclick="show_right_side(\'incomming_mail\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_mail.png" alt="24 ICON" title="ელ. ფოსტა"></span>
    
                    <span class="chat_question communication_side_menu_button" onclick="show_right_side(\'chat_question\')"><img class="communication_side_menu_img" src="media/images/icons/chat_question.png" alt="24 ICON" title="კომენტარები"></span>
    			    
                    <span style="display:none" class="newsnews communication_side_menu_button" onclick="show_right_side(\'newsnews\')"><img class="communication_side_menu_img" src="media/images/icons/newsnews.png" alt="24 ICON" title="სიახლეები"></span>
                    
                    <span class="record communication_side_menu_button" onclick="show_right_side(\'record\')"><img class="communication_side_menu_img" src="media/images/icons/inc_record.png" alt="24 ICON"  title="ჩანაწერები"></span>
                        				
                    <span class="incomming_file communication_side_menu_button" onclick="show_right_side(\'incomming_file\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_files.png" alt="24 ICON"  title="ფაილები"></span>
    				
                    <span class="user_logs communication_side_menu_button" onclick="show_right_side(\'user_logs\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_logs.png" alt="24 ICON"  title="ლოგები"></span>

                    <span style="float: right; margin-right: 10px;" class="call_resume communication_side_menu_button" onclick="show_right_side(\'call_resume\')"><img class="communication_side_menu_img" src="media/images/icons/inc_resume.png" alt="24 ICON"  title="ლოგები"></span>
                   
    			 </div>
			 </div>
			 <div class="communication_right_side" id="right_side" style="float: left;">

			 <ff class="communication_right_side_info" id="crm_info">
			 <table class="dialog-form-table" style="width: 96%;">
				 
				 <tr style="height:5px;"></tr>
				 <tr style="height:0px;">
					 <td>' . $res['task_former_user'] . '</td>
				 </tr>
				 <tr style="height:10px;"></tr>
				 <tr style="height:0px;">
					 <td><label for="task_id">მომართვის №</label></td>
					 <td><label for="task_date">შექმნის თარიღი</label></td>
				 </tr>
				 <tr style="height:0px;">
					 <td><input id="task_id" style="width:270px" type="text" value="' . $res['crm_id'] . '" readonly></td>
					 <td><input class="idle" style="width:270px" id="task_date" type="text" value="' . (($res['task_date'] == '') ? (($res["crm_date"] != '') ? $res["crm_date"] : date("Y-m-d H:i:s")) : $res['task_date']) . '" readonly></td>
				 </tr>
				 <tr style="height:10px;"></tr>
				 <tr style="height:0px;">
					 <td><label for="task_status">სტატუსი</label></td>
					 <td><label for="task_status">ტელეფონის ნომერი</label></td>
				 </tr>
				 <tr style="height:0px;">
					  <td>
					  	<select id="crm_status" style="width: 180px;" >
					  		' . get_crm_status($res['status_1']) . '
				 		 </select>
					  </td>
					 <td><input type="text" id="crm_phone" style="width:270px" value="' . $res['crm_phone'] . '" /></td>
				 </tr>
				 <tr style="height:5px;"></tr>
				 <tr style="height:0px;">
					 <td colspan="2"><label for="task_description">კომენტარი</label></td>
				 </tr>
				 <tr style="height:0px;">
					 <td colspan="2">
					 <textarea disabled style="width: 590px; padding: 3px 7px; height:69px" id="crm_descr">' . $res['comment'] . '</textarea></td>
				 </tr>
				 <tr style="height:5px;"></tr>
				 <tr style="height:0px;">
					 <td colspan="2"><label for="task_note">შედეგი</label></td>
				 </tr>
				 <tr style="height:0px;">
					 <td colspan="2"><textarea style="height:69px; padding: 3px 7px; width: 590px;" id="res_comment" >' . $res['res_comment'] . '</textarea></td>
				 </tr>
			 </table>
			 
			 </ff>

    			<ff class="communication_right_side_info" id="info">';

	$incomming_dialog_data = new automator();
	//$tt->getDialog($res[base_id],$res['phone'],'compaign_request_processing','base_id',1)
	$db->setQuery("	SELECT 	outgoing_campaign_request_base.id AS 'base_id'
								FROM 	incomming_call
								JOIN 	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = incomming_call.asterisk_incomming_id
								WHERE 	incomming_call.id = '$res[id]'");
	$base_id = $db->getResultArray();
	$base_id = $base_id['result'][0]['base_id'];
	if ($base_id != '') {
		$data .= $incomming_dialog_data->getDialog($base_id, '', 'compaign_request_processing', 'base_id', 1);
	} else {
		$data .= $incomming_dialog_data->getDialog($res['id'], '');
	}

	//die($res['id']);

	$data .= '
				
				</ff>
                 
                 <ff style="display:none; height: 656px;" id="newsnews">
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                        <table class="display" id="table_news" >
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width:15%;">დასაწყისი</th>
                        			<th style="width:15%;">დასასრული</th>
                        			<th style="width:15%;">დასა-<br>ხელება</th>
                                    <th style="width:15%;">საიტი(ები)</th>
                        			<th style="width:40%;">შინაარსი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                    	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                    	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="client_history">
                    <table style="margin-left: 7px;">
                        <tr style="height:0px;">
                            <td colspan="2" style="text-align: center;">პერიოდი</td>
                            <td><label>ტელეფონი</label></td>
                            <td></td>
                        </tr>
                        <tr style="height:0px;">
							<td style="width: 120px;">
								<input class="callapp_filter_body_span_input date_input" type="text" id="date_from" style="width: 110px;" value="' . date('Y-m-d', strtotime('-30 day')) . '">
							</td>
							<td style="width: 120px;">
								<input class="callapp_filter_body_span_input date_input" type="text" id="date_to" style="width: 110px;" value="' . date('Y-m-d') . '">
							</td>
							<td style="width: 140px;">
								<input class="callapp_filter_body_span_input" type="text" id="client_history_phone" style="width: 130px;" value="' . $k_phone . '" />
							</td>

                            <td><button id="search_client_history" style="margin-top: -6px;">ძებნა</button></td>
                        </tr>
                    </table>
                
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                        <table class="display" id="table_history" style="width: 100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 20%;">მომართვის ტიპი</th>
                					<th style="width: 20%">ტელეფონის ნომერი</th>
                					<th style="width: 20%;">ოპერატორი</th>
                					<th style="width: 20%;">კომენტარი</th>
                                    <th style="width: 20%;">სტატუსი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
									</th>
									<th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
									</th>
									<th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
        	                        <th>
                                        <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
              				</thead>
          				</table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="task">
                    <div id="task_button_area" class="margin_top_10" style="margin-left: 7px;"> 
                        <button id="add_task_button" style="display: none">ახალი დავალება</button>
                    </div>
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
        	            <table class="display" id="task_table" style="width: 100%;">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%">თარიღი</th>
                                    <th style="width: 20%;">დასაწყისი</th>
                                    <th style="width: 20%;">დასასრული</th>
                                    <th style="width: 20%;">სტატუსი</th>
                                    <th style="width: 20%;">პასუხისმგებელი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                        <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </ff>
                 <ff style="display:none; height: 656px;" id="incomming_comment">
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                        
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="incomming_mail">
                    
                    <div style="margin-left: 7px;" id="task_button_area">
                        <button id="add_mail">ახალი E-mail</button>
                    </div>
                     <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                        <table class="display" id="table_mail" >
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 25%;">ადრესატი</th>
                                    <th style="width: 35%;">გზავნილი</th>
                                    <th style="width: 20%;">სტატუსი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                    	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                    </th>
                                </tr>
                            </thead>
                        </table>
    	            </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="client_conversation">
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
        	           <table class="display" id="table_question" style="width:100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 20%;">თარიღი</th>
                                    <th style="width: 20%;">თანამშრომელი</th>
                                    <th style="width: 20%;">კითხვა</th>
                                    <th style="width: 40%;">პასუხი</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	    <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px; " id="chat_question">
                     <div style="margin-left: 7px;"><p>ახალი კომენატრი</p></div>
    	             <table>
                        <tr>
                            <td><textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; margin-left: 7px; width:510px; height:50px; display:inline-block;" rows="3"></textarea > </td>
                            <td><button style="height: 50px; margin-left: 9px;" id="add_comment">დამატება</button></td>
                        </tr>
                    </table>

                     <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; border: 1px solid #dbdbdb;">' . get_comment($hidde_inc_id) . '</div>
                     <input type=hidden id="comment_incomming_id" value="' . $res['id'] . '"/>
                 </ff>
                 <ff style="display:none; height: 656px;" id="user_logs">
                     <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                         <table class="display" id="user_log_table">
                            <thead>
                                <tr style="font-size: 11px;" id="datatable_header">
                                    <th>ID</th>
                                    <th style="width:20%">თარიღი</th>
                                    <th style="width:15%">ქმედება</th>
                                    <th style="width:12%">მომხმა<br>რებელი</th>
                                    <th style="width:15%">ველი</th>
                                    <th style="width:19%">ახალი მნიშვნელ���ბა</th>
                                    <th style="width:19%">ძველი მნიშვნელობა</th>
                                </tr>
                            </thead>
                            <thead >
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                        <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width:97%" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                 </ff>
                 <ff style="display:none; height: 656px;" id="call_resume">
                     <table style="margin-left: 7px;" width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ოპერატორი</label></td>
							<td style="width: 205px;"><label for="d_number">თარიღი</label></td>
                            <td style="width: 205px;"><label for="d_number">მომართვის ნომერი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="my_operator_name" class="idle" value="' . $res[operator] . '" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_datetime" class="idle" value="' . $res[call_date] . '" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="incomming_call_id" class="idle" value="' . $hidde_inc_id . '" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ლოდინის დრო</label></td>
							<td style="width: 205px;"><label for="d_number">საუბრის ხანგრძლივობა</label></td>
                            <td style="width: 205px;"><label for="d_number">დამუშავების ხანგრძლივობა</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="call_wait_time" class="idle" value="' . $res[wait_time] . '" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_duration" class="idle" value="' . $res[duration] . '" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id=call_done_date" class="idle" value="' . $res[processed_time] . '" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        <tr style="height:0px">
							<td colspan="3"><label for="d_number">საიტი</label></td>
						</tr>
						<tr style="height:0px">
                            <td colspan="3"><select style="height: 18px; width: 590px;" id="my_site_resume" class="idls object" disabled="disabled" multiple>' . web_site($res['id']) . '</select></td>
                        </tr>
                    </table>
                 </ff>
				 
				<ff style="display:none; height: 656px;" id="sms">
					
                    <div id="task_button_area" style="margin-left: 7px;">
						<button id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
					</div>
                    <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
    					<table class="display" id="table_sms" >
    						<thead>
    							<tr id="datatable_header">
    								<th>ID</th>
    								<th style="width: 20%;">თარიღი</th>
    								<th style="width: 15%;">ადრესატი</th>
    								<th style="width: 50%;">ტექსტი</th>
    								<th style="width: 15%;">სტატუსი</th>
    							</tr>
    						</thead>
    						<thead>
    							<tr class="search_header">
    								<th class="colum_hidden">
    								<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
    								</th>
    								<th>
    									<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
    								</th>
    							</tr>
    						</thead>
    					</table>
					</div>
				</ff>
                <ff style="display:none; height: 656px;" id="incomming_file">
                    <table style="float: right;text-align: center; border: 1px solid #ccc; width: 100%;">
    					<tr>
    						<td>
    							<div class="file-uploader">
    								<input id="choose_file1" type="file" name="choose_file1" class="input" style="display: none;">
    								<button id="choose_button" style="width: 100%;" class="center">აირჩიეთ ფაილი</button>
    								<input id="hidden_inc" type="text" value="" style="display: none;">
    							</div>
    						</td>
    					</tr>
    				</table>
    			     <table style="float: right;border: 1px solid #ccc;width: 100%;margin-top: 2px;text-align: center;">
    			          <tr>
    			           <td colspan="4">მიმაგრებული ფაილი</td>
    			          </tr>
    				</table>
    				<table id="file_div" style="float: right; border: 1px solid #ccc; width: 100%;margin-top: 2px; text-align: center;">';

	foreach ($increm[result] as $increm_row) {
		$data .= '
    						        <tr style="border-bottom: 1px solid #CCC;">
                                      <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[file_date] . '</td>
    						          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;word-wrap:break-word;">' . $increm_row[name] . '</td>
    						          <td style="border-right: 1px solid #CCC;height: 20px;vertical-align: middle;"><button type="button" value="media/uploads/file/' . $increm_row[rand_name] . '" style="cursor:pointer; border:none;height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="' . $increm_row[rand_name] . '"> </td>
    						          <td style="height: 20px;vertical-align: middle;"><button type="button" value="' . $increm_row[id] . '" style="cursor:pointer; border:none; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
    						        </tr>';
	}

	$data .= '
        	 		</table>
        		</ff>
                <ff style="display:none; height: 656px;" id="crm">
                    <div style="width:100%">
                       
                        <table>
                            <tr style="height: 0px;">
                                <td colspan="2" style="width: 145px; text-align: center;">პერიოდი</td>
                                <td><label>User Id</label></td>
                                <td>ტელეფონი</td>
                                <td></td>
                            </tr>
                            <tr style="height: 0px;">
                                <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="start_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="' . date('Y-m-d', strtotime('-7 day')) . '"></td>
                                <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="end_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="' . date('Y-m-d') . '"></td>
                                <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_pin" style="width: 111px; height: 20px; margin-top: 20px;" value="' . $person_pin . '"></td>
                                <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_phone" style="width: 111px; height: 20px; margin-top: 20px;" value="' . $res[phone] . '"></td>
                                <td><button id="search_ab_crm_pin" style="margin-left: 10px; margin-top: 4px;">ძებნა</button></td>
                            </tr>
                        <table>
                        <table class="display" id="table_crm" style="width: 100%">
                            <thead>
                                <tr id="datatable_header">
                                    <th>ID</th>
                                    <th style="width: 15%;">Upload<br>date</th>
                                    <th style="width: 14%;">Prom-<br>date</th>
                					<th style="width: 11%;">User Id</th>
                					<th style="width: 12%;">User<br>name</th>
                                    <th style="width: 13%;">Mobile</th>
                                    <th style="width: 13%;">PID</th>
                                    <th style="width: 10%;">Com-<br>ment</th>
                                    <th style="width: 12%;">status</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="search_header">
                                    <th class="colum_hidden">
                                	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
        	                        <th>
                                        <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                    <th>
                                        <input style="width: 96%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                    </th>
                                </tr>
              				</thead>
          				</table>
                    </div>
			    </ff>
                <ff style="display:none; height: 656px;" id="record" >
	                <div style="margin-top: 10px;">
                        <audio controls style="margin-left: 7px; width: 97%" id="auau">
                          <source src="" type="audio/wav">
                          Your browser does not support the audio element.
                        </audio>
					</div>
					<fieldset>
						<legend>შემომავალი</legend>
						<table style="margin: auto; width: 97%;">
                       <tr>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">თარიღი</td>
                    	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ხანგძლივობა</td>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ოპერატორი</td>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ვინ გათიშა</td>
                    	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">მოსმენა</td>
                        </tr>
                        ' . GetRecordingsSection($res) . '
                    </table>
					</fieldset>
					
					<fieldset>
						<legend>გამავალი</legend>
						<table style="margin: auto; width: 97%;">
                       <tr>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">თარიღი</td>
                    	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ხანგძლივობა</td>
                           <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ოპერატორი</td>
                    	   <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">მოსმენა</td>
                        </tr>
                        ' . GetOutGoingRecordingsSection($res['crm_phone'],$res['crm_date']) . '
                    </table>
					</fieldset>
    	            
    	      </ff>
            </div>
	 		<input id="hidden_user" type="text" value="' . $res['user_id'] . '" style="display: none;">
	 		<input id="note" type="text" value="' . $res['note'] . '" style="display: none;">
            <input id="ast_source" type="text" value="' . $res['ast_source'] . '" style="display: none;">
            <input id="hidde_incomming_call_id" type="text" value="' . $res['id'] . '" style="display: none;">
            <input id="hidden_incomming_call_id" type="hidden" value="' . $res['id'] . '">
            <input id="crm_id_hid" type="text" value="' . $res['crm_id'] . '" style="display: none;">
			<input id="chat_name" type="hidden" value="" />
			<input id="processing_start_date" type="text" value="' . $processing_start_date . '" style="display: none;">
		</div>
		</div>
	 </div>';

	return $data;
}

function GetRecordingsSection($res)
{
	global $db;

	$db->setQuery(" SELECT    FROM_UNIXTIME(asterisk_call_log.call_datetime) as `call_datetime`,
								  SEC_TO_TIME(asterisk_call_log.talk_time) AS `time`,
                				  IF(asterisk_call_log.call_type_id = 5,CONCAT('autodialer/',asterisk_call_record.name,'.',asterisk_call_record.format),CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format))  AS `userfield`,
                				  FROM_UNIXTIME(asterisk_call_log.call_datetime),
                				  user_info.`name`,
                				  IF(asterisk_call_log.call_status_id = 6, 'ოპერატორმა', 'მომხმარებელმა') AS `completed`
                        FROM 	  asterisk_call_log
                        LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
						LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
	                    WHERE	  asterisk_call_log.id ='$res[asterisk_incomming_id]'");

	$num_row = $db->getNumRow();
	$req     = $db->getResultArray();

	if ($num_row == 0) {
		$data .= '<tr>
                      <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
        	      </tr>';
	} else {
		foreach ($req[result] as $res2) {
			$src = $res2['userfield'];
			$data .= '
    		      <tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[call_datetime] . '</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[time] . '</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[name] . '</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[completed] . '</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\'' . $src . '\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
        	      </tr>';
		}
	}

	return $data;
}

function GetOutGoingRecordingsSection($phone,$crm_date)
{
	global $db;

	$db->setQuery(" SELECT SEC_TO_TIME(asterisk_call_log.talk_time) AS `time`,
					CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format) AS `userfield`,
					FROM_UNIXTIME(asterisk_call_log.call_datetime) AS `call_datetime`,
					user_info.`name`
				FROM asterisk_call_log
				LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
				JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
				WHERE asterisk_call_log.destination = '$phone' AND asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_datetime >= '$crm_date' ORDER BY asterisk_call_log.id DESC LIMIT 1");

	$num_row = $db->getNumRow();
	$req     = $db->getResultArray();

	if ($num_row == 0) {
		$data .= '<tr>
				  <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
			  </tr>';
	} else {
		foreach ($req[result] as $res2) {
			$src = $res2['userfield'];
			$data .= '
			  <tr>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[call_datetime] . '</td>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[time] . '</td>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">' . $res2[name] . '</td>
				<td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\'' . $src . '\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
			  </tr>';
		}
	}

	return $data;
}


function get_activitie_options()
{
	global $db;
	$options = '<li class="is-active" data-id="0" data-color="#bdc3c7">
					<span class="activitie-color-block" style="background: #bdc3c7;"></span>
					<span class="activitie-name">აირჩიეთ აქტივობა</span>
				</li>';
	$db->setQuery("SELECT id, name, color FROM work_activities WHERE actived = 1");
	$req = $db->getResultArray();
	foreach ($req[result] as $res) {

		$options .= '<li data-id="' . $res["id"] . '" data-color="' . $res["color"] . '">
						<span class="activitie-color-block" style="background:' . $res["color"] . ';"></span>
						<span class="activitie-name">' . $res["name"] . '</span>
					</li>';
	}

	return $options;
}

function get_dnd_status($userid)
{

	$query = mysql_query("SELECT dnd FROM crystal_users WHERE id = '$userid'");
	$res = mysql_fetch_assoc($query);

	return $res["dnd"];
}

function get_activitie_status_data($userid)
{
	global $db;
	$db->setQuery("SELECT work_activities_id
	               FROM   users
	               WHERE  id = '$userid'");

	$res = $db->getResultArray();
	$activitie_id = (int) $res[result][0]["work_activities_id"];
	$activitie_status = $activitie_id == 0 ? 'off' : 'on';

	return array(
		"id" => $activitie_id,
		"status" => $activitie_status
	);
}

function get_activitie_seconds_passed($userid, $activitieid)
{
	global $db;

	$db->setQuery("SELECT TIME_TO_SEC(TIMEDIFF(NOW(),start_datetime)) AS time_difference
				   FROM   work_activities_log
				   WHERE  user_id            = '$userid'
				   AND 	  work_activities_id = '$activitieid'
				   AND	  ISNULL(end_datetime) AND start_datetime > date(now())");

	$req = $db->getResultArray();
	$res = $req[result][0];



	$db->setQuery("SELECT    TIME_TO_SEC(DATE_FORMAT(work_shift.timeout,'%H:%i')) AS shift_timeout
					   FROM      work_graphic_rows
					   LEFT JOIN work_shift ON work_shift.id = work_graphic_rows.work_shift_id
					   WHERE     work_graphic_id =(SELECT id
        							               FROM   work_graphic
        						                   WHERE  project_id = '2' AND year = year(now()) 
                                                   AND    month = DATE_FORMAT(now(), '%m') 
                                                   AND    actived = 1 AND `operator_id` = '$userid')
					   AND `date` = CONCAT(DATE_FORMAT(now(), '%d'),'.',DATE_FORMAT(now(), '%m'),'.20',DATE_FORMAT(now(), '%y'))");

	$res_break_time = $db->getResultArray();
	$arr_break_time = $res_break_time[result][0];

	$db->setQuery("SELECT sum(TIME_TO_SEC(TIMEDIFF(end_datetime ,start_datetime)) ) AS time_difference
					   FROM   work_activities_log
					   WHERE  user_id = '$userid'
					   AND 	  work_activities_id IN (SELECT id FROM work_activities WHERE actived = 1)
					   AND    end_datetime > date(now()) AND start_datetime > date(now())");

	$res_used_time = $db->getResultArray();
	$arr_used_time = $res_used_time[result][0];

	// echo ">>>".$arr_break_time['shift_timeout'] - $arr_used_time["time_difference"];

	// if($arr_break_time['shift_timeout'] != 0){

	if ($res['time_difference'] > 0) {
		$result = $arr_break_time['shift_timeout'] - $arr_used_time["time_difference"] - $res["time_difference"];
	} else
		$result = $arr_break_time['shift_timeout'] - $arr_used_time["time_difference"];

	// }
	// else {
	// 	$result = "not_set";
	// }


	return $result;
}


function get_new_sms_dialog($type)
{

	// define adressee input structures
	$adressee_inputs = $type == 'phone' ? '<div class="new-sms-row">
												<label for="smsAddresseeByPhone">ტელეფონის ნომერი</label>
												<div class="new-sms-input-holder">
													<input type="text" id="smsAddresseeByPhone">
												</div>
											</div>'
		: '<div class="new-sms-row grid">
												<div class="nsrg-col">
													<label for="smsAddresseeByPIN">PIN კოდი</label>
													<div class="new-sms-input-holder">
														<input type="text" id="smsAddresseeByPIN">
													</div>
												</div>
												<div class="nsrg-col">
													<label for="smsAddresseeByIDNum">პირადი ნომერი</label>
													<div class="new-sms-input-holder">
														<input type="text" id="smsAddresseeByIDNum">
													</div>
												</div>
											</div>';

	//new sms send dialog content
	return '<fieldset style="display:block;" class="new-sms-send-container">

				<!-- label for fieldset -->
				<legend>SMS</legend>

				<!-- row unit -->
				<div class="new-sms-row">
					<button id="smsTemplate" data-button="jquery-ui-button">შაბლონი</button>
				</div>

				<!-- row unit -->
				' . $adressee_inputs . '

				<!-- row unit -->
				<div class="new-sms-row">
					<label for="smsText">ტექსტი</label>
					<div class="new-sms-input-holder">
						<textarea id="newSmsText"></textarea>
					</div>
				</div>

				<!-- row unit -->
				<div class="new-sms-row">
					<input type="text" id="smsCharCounter" data-limit="150" value="0/150">
					<button id="sendNewSms" data-button="jquery-ui-button">გაგზავნა</button>
				</div>

			</fieldset>';
}

function getShablon()
{
	global $db;
	$db->setQuery("SELECT 	sms.id,
        					sms.`name`,
        					sms.`message`
					FROM 	sms
					WHERE 	sms.actived=1 ");

	$data_count = $db->getNumRow();

	if ($data_count > 0) {

		$data = '<table id="box-table-b1">
            		<tr class="odd">
            			<th style="width: 26px;">#</th>
            			<th style="width: 160px;">დასახელება</th>
            			<th>ქმედება</th>
            		</tr> ';

		$req = $db->getResultArray();
		foreach ($req[result] as $res3) {
			$data .= '<tr class="odd">
            				<td>' . $res3[id] . '</td>
            				<td style="width: 30px !important;">' . $res3['name'] . '</td>
            				<td style="font-size: 10px !important;">
            					<button style="width: 45px;" class="download_shablon" sms_id="' . $res3['id'] . '" data-message="' . $res3['message'] . '">არჩევა</button>
            				</td>
        			   </tr>';
		}
		$data .= '</table>';
	} else {
		$data = '<div class="empty-sms-shablon">
					<p>შაბლონთა ჩამონათვალი ცარიელია</p>
				 </div>';
	}
	return $data;
}


function sms_template_dialog()
{

	return '
		<div class="sms-template-table-wrapper">
			<table class="display" id="sms_template_table" style="width: 100%;">
				<thead>
					<tr id="datatable_header">
						<th>ID</th>
						<th style="width: 10%;" >№</th>
						<th style="width:  70%;">დასახელება</th>
						<th style="width:  15%">ქმედება</th>
					</tr>
				</thead>
				<thead>
					<tr class="search_header">
						<th class="colum_hidden">
							<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
						</th>
						<th>
							<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
						<th>
							<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
			</table>
		<div>';
}

function Getquestion($id)
{
	$data = '
        <div style="margin-left: 7px;"><p>ახალი კომენატრი</p></div>
        <table>
            <tr>
                <td><textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; margin-left: 7px; width:510px; height:50px; display:inline-block;" rows="3"></textarea > </td>
                <td><button style="height: 50px; margin-left: 9px;" id="add_comment">დამატება</button></td>
            </tr>
        </table>
        <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; border: 1px solid #dbdbdb;">' . get_comment($id) . '</div>
        <input type=hidden id="comment_incomming_id" value="' . incomming_call_id . '"/>
    ';
	return $data;
}

function get_user($id)
{
	global $db;
	$data   = '';
	$db->setQuery("SELECT `name`
                   FROM   `user_info`
                   WHERE   user_id = $id");

	$res    = $db->getResultArray();
	$data   = $res[result][0]['name'];
	return $data;
}

function get_answers($id, $hidden_id)
{
	global $db;
	$data = '';
	$db->setQuery("SELECT `id`,
                          `user_id`,
                          `incomming_call_id`,
                          `comment`,
                          `datetime`
                    FROM  `chat_comment`
                    WHERE  parent_id = $id AND incomming_call_id = $hidden_id AND active = 1");

	$req = $db->getResultArray();

	foreach ($req[result] as $res) {
		$data .= '<div>
                        <span style="color: #369; font-weight: bold;">' . get_user($res['user_id']) . '</span> ' . $res['datetime'] . ' <br><br>
                        <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; line-height:32px; ">' . $res['comment'] . '</span>
                        <img id="edit_comment" my_id="' . $res['id'] . '" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                        <img id="delete_comment" my_id="' . $res['id'] . '" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
	}

	return $data;
}

function get_emoji()
{
	global $db;
	$db->setQuery("SELECT `emojis`.`code` FROM `emojis` WHERE `emojis`.actived = 1");

	$result  = $db->getResultArray();
	$emojis = "";
	foreach ($result[result] as $arr) {
		$emojis .= "<span code= '&#" . $arr[code] . "' >&#" . $arr[code] . "</span>";
	}
	$data = array("emojis" => $emojis);
	return $emojis;
}
