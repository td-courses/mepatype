

export function Theme() {

    this.DarkMode = () => {

        document.documentElement.style.setProperty('--dashboard-bg-color', '#1C323F');
        document.documentElement.style.setProperty('--block-bg-color', '#15222C');
        document.documentElement.style.setProperty('--text-color', '#929292');
        document.documentElement.style.setProperty('--white', '#15222C');
        document.documentElement.style.setProperty('--dark-sky', '#F7F6FF');
        document.documentElement.style.setProperty('--text-shadow', '0px 7px 0px rgb(10 10 10 / 68%)');
        document.documentElement.style.setProperty('--dark-blue', '#15222C');
        document.documentElement.style.setProperty('--menu-text', '#929292');
        

        localStorage.setItem("theme", "dark");

    }

    this.LightMode = () => {

        document.documentElement.style.setProperty('--dashboard-bg-color', '#F7F6FF');
        document.documentElement.style.setProperty('--block-bg-color', '#FFF');
        document.documentElement.style.setProperty('--text-color', '#404040');
        document.documentElement.style.setProperty('--white', '#FFF');
        document.documentElement.style.setProperty('--dark-sky', '#0d1340');
        document.documentElement.style.setProperty('--text-shadow', '3px 8px 0px rgb(0 0 0 / 28%)')
        document.documentElement.style.setProperty('--dark-blue', 'rgb(53, 79, 166)');
        document.documentElement.style.setProperty('--menu-text', '#FFFFFF');

        localStorage.setItem("theme", "light")

    }

    this.setTheme = () => {

        window.matchMedia('(prefers-color-scheme: dark)')
            .addEventListener('change', event => {
            if (event.matches) {
                this.DarkMode();
            } else {
                this.LightMode();
            }
        })

        var ThemeStorage = localStorage.getItem('theme');
    
        if(ThemeStorage == null){
            this.LightMode();
        }else if(ThemeStorage == 'dark'){
            this.DarkMode();
        }else{
            this.LightMode();
        }

    }


    this.onSwitchClick = () => {

        $(document).on('click', "switch[theme] input", () => {
            if($('switch[theme] input').is(":checked")){
                this.DarkMode();
            }else{
                this.LightMode();
            }
        })

    }


    this.getTheme = () => {

        return localStorage.getItem("theme");
    }


}