<head>

<style type="text/css">

/*Global Styles*/
button {
    cursor: pointer;
    border: none;
    background: none;
    outline: none;
}

.ui-corner-all ul li {
    float: none;
}

#button-filter{
  top:18px;
}

.ui-dialog .ui-dialog-content {
	padding: .5em 1em !important;
}

.empty-table-row {
  border: 1px solid black;
  margin-top: 10px;
}

.empty-table-row span {
  display: block;
  width: 100%; height: 15px;
  padding: 3px 0;
  text-align: center;
}

.hidden-data {
  display: none !important;
}

input[type="color"] {
	-webkit-appearance: none;
	border: none;
	width: 50px; height: 15px;
}
input[type="color"]::-webkit-color-swatch-wrapper {
	padding: 0;
  border: none;
}
input[type="color"]::-webkit-color-swatch {
	border: none;
}

.cursor-move {
  cursor: move !important;
}

#table_right_menu{
    top: 28px;
}
.ColVis, .dataTable_buttons{
	z-index: 100;
  display: none;
}
#table_holiday_length,
#table_week_length,
#table_lang_length,
#table_cikle_length,
#table_project_length,
#table_infosorce_length{
	position: absolute;
  left: 2px;
}

#table_holiday_length label select,
#table_week_length label select,
#table_lang_length label select,
#table_cikle_length label select,
#table_infosorce_length label select {
	width: 60px;
    font-size: 10px;
    padding: 0;
    height: 18px;
}
#table_holiday_info{
	width: 32%;
}
#table_holiday_paginate{
	margin-left: 0px;
}
#work_table tr{
	height: 19px;
}

#work_table tbody tr:last-child td {
	padding: 0;
}
.left_border_bold{
    border-left: 2px solid black !important;
}
.right_border_bold{
    border-right: 2px solid !important;
}
#table_cikle td:nth-child(3),
#table_cikle td:nth-child(4),
#table_cikle td:nth-child(5),
#table_week td:nth-child(5)  {
    text-align: right;
}

#table_project thead:first-child tr th {
    font-size: 11px;
    font-weight: bold;
    text-align: center;
    padding: 10px 0;
}

/*My Global Styles*/
.cursor-pointer {
    cursor: pointer;
}

.full-width {
    width: 100%;
}

.children-full-width > * {
    width: 100%;
}

.pad-bottom {
    padding-bottom: 10px;
}

[data-status="all-in"] {
  background: #026b3b;
  color: white;
}

[data-status="more-in"] {
  background: transparent;
  color: #026b3b;
}

[data-status="in-out"] {
  background: transparent;
  color: #c00000;
}

[data-status="more-out"] {
  background: #ef8a0a;
  color: white;
}

[data-status="all-out"] {
  background: #c00000;
  color: white;
}

input[type="color"] {
  border: none !important;
}

#workshift_24hours {
  width: 15px;
}

/*Directory Menu Wrapper*/
.directory-menu-wrapper {
			width: 100%; height: 30px;
			margin-bottom: 25px;
			border-bottom: 1px solid #A3D0E4;
		}

		.directory-menu-wrapper ul {
			width: 100%; height: 100%;
			list-style: none;
			padding: 0;
		}

		.directory-menu-wrapper ul li {
			display: flex;
			height: 100%;
			padding: 0 25px;
			align-items: center;
			justify-content: center;
			float: left;
			margin-top: -1px;
			font-family: pvn;
			font-weight: bold;
			letter-spacing: .5px;
			cursor: pointer;
			border: 1px solid transparent;
      opacity: .6;
			transition: color .1s ease, opacity .1s ease;
		}

		.directory-menu-wrapper ul li:not(.active):hover {
      opacity: 1;
		}

		.directory-menu-wrapper ul li.active {
			background: #E6F2F8;
			border: 1px solid #A3D0E4;
			border-bottom: none;
      opacity: 1;
      color: #2681DC;
		}

    .directory-menu-wrapper ul li.active i {
			color: #2681DC;
		}

    .directory-menu-wrapper ul li i {
      font-size: 1.1em;
    }

    .directory-menu-wrapper ul li span {
      margin-left: 5px;
    }

		/*Directory Receiver*/
    .directory-pages-wrapper {
      width: 100%; height: auto;
      position: relative;
    }

		#dirReceiver {
			width: 100%; height: auto;
			box-sizing: border-box;
		}

    .dpw-loading-container {
      display: none;
      width: 100%; height: 100%;
      min-height: 500px;
      background: white;
      align-items: center;
      justify-content: center;
      position: absolute;
      top: 0; left: 0;
    }

    .dpw-loading-container.active {
      display: flex;
    }

    .dpw-loading-container div {
      text-align: center;
    }

    .dpw-loading-container p {
      font-family: pvn;
      font-size: 16px;
      font-weight: bold;
      color: #2681DC;
      letter-spacing: .3px;
      margin-top: 5px;
    }

    .dpw-loading-container svg {
      font-size: 55px;
      color: #2681DC;
    }

    .dpw-loading-container.active svg {
      animation: loading 1s linear infinite;
    }

    @keyframes loading {      
      from {
        transform: rotate(0deg);
      }
      to {
        transform: rotate(360deg);
      }
    }

/*EDIT EXISTED PROJECT DIALOG*/
#edit-existed-project {
  display: none;
  position: fixed;
  top: 0px;
  z-index: 100;
  overflow: hidden;
  overflow-y: auto;
  background: white;
  box-sizing: border-box;
  padding: 5px;

}

#edit-existed-project.active {
  display: grid;
  grid-template-rows: 28px auto;
}

/*top bar*/
.project-details-topbar {
  width: 100%;
  background: #E6F2F8;
  border: 1px solid #A3D0E4;
  box-sizing: border-box; 
}

.project-details-topbar p {
  font-family: pvn;
  font-size: 13px;
  font-weight: bold;
  float: left;
  position: relative;
  margin: 0;
  top: 50%;
  transform: translateY(-50%);
  margin-left: 10px;
}

#editProjectNameReceiver {
  color: #2376C8;
}

.project-details-topbar button {
  display: flex;
  width: 18px; height: 18px;
  float: right;
  border: 1px solid #A3D0E4;
  align-items: center;
  justify-content: center;
  position: relative;
  top: 4px;
  margin-right: 4px;
}

.project-details-topbar button:hover {
  background: #DCDCDC;
  border: 1px solid #646464;
}

.project-details-topbar button svg {
  font-size: 12px;
}

/*content*/
.project-details-content {
  display: grid;
  width: 100%;
  grid-template-rows: 34px auto;
}

/*main menu*/
.project-details-menu {
  display: flex;
  width: 100%;
  border-bottom: 1px solid #bcbcbc;
  background: #E5E5E5;
}

.project-details-menu > div {
  width: 50%;
  box-sizing: border-box;
}

.project-details-menu ul {
  width: 100%; height: 100%;
  list-style: none;
  padding: 0;
  margin: 0;
}

.project-details-menu ul li {
  height: 34px;
  display: flex;
  color: #1D61A4;
  cursor: pointer;
  align-items: center;
  float: left;
  padding: 0 15px;
  user-select: none;
  transition: color .1s ease;
}

.project-details-menu ul li:not(.active):hover {
  color: #DD9D25;
}

.pdm-right ul li {
  float: right;
}

.project-details-menu ul svg {
  margin-right: 3px;
  font-size: 14px;
}

.project-details-menu ul span {
  font-family: pvn;
  font-size: 13px;
  font-weight: bold;
  letter-spacing: .4px;
}

.project-data-loader.active {
  background: white;
  border: 1px solid #bcbcbc;
  border-bottom: none;
  border-top: none;
  color: #313132;
  cursor: default;
}

#openEditProjectDetailsForm.active {
  color: #DD9D25;
}

/*data wrapper*/
.project-details-data-wrapper {
  position: relative;
}

/*details editor*/
.project-details-editor {
  display: grid;
  width: 400px; height: 100%;
  position: absolute;
  top: 0; left: -410px;
  background: #f5f6fa;
  z-index: 10;
  box-sizing: border-box;
  grid-template-rows: auto 68px;
  box-shadow: none;
  transition: left .2s ease-in;
}

.project-details-editor.active {
  left: 0;
  box-shadow: 2px 1px 5px 0 rgba(0, 0, 0, .3);
}

.project-details-editor-content {
  width: 100%;
  padding: 10px;
  box-sizing: border-box;
  overflow: hidden;
  overflow-y: auto;
}

.project-details-editor-footer {
  display: flex;
  width: 100%;
  justify-content: space-around;
  align-items: center;
  border-top: 1px solid #dcdde1;
}

.project-details-editor-footer button {
  width: 150px; height: 35px;
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, .3);
  border-radius: 3px;
  font-family: pvn;
  font-size: 13px;
  font-weight: bold;
  letter-spacing: .5px;
  transition: all .1s ease;
}

#confirmProjectEditing {
  background: #2376C8;
  color: #DCDCDC;
}

#confirmProjectEditing:hover {
  background: #03967D;
}

#cancelProjectEditing {
  background: #DCDCDC;
  color: #313132;
}

#cancelProjectEditing:hover {
  background: #BC2020;
  color: #F2F2F2;
}

.project-details-mane-container { /*mane container*/
    width: 100%; height: 100%;
    display: flex;
    flex-direction: column;
}

.new-project-form-container {
    width: 100%; height: auto;
    box-sizing: border-box;
}

/*Project details data receiver*/
.project-details-data-receiver {
  width: 100%; height: 100%;
  position: absolute;
  top: 0; left: 0;
  z-index: 1;
}

/*Project details day graphic container*/
.project-details-day-graphic-container {
  display: none;
}

.project-details-day-graphic-container.active {
  display: grid;
  width: 100%; height: 100%;
  position: absolute;
  top: 0; left: 0;
  z-index: 13;
  grid-template-rows: 50px auto 90px;
  background: white;
}

/*Project details loading container*/
.project-details-loading-container,
.project-details-loading-container.mini {
  display: none;
  width: 100%; height: 100%;
  position: absolute;
  top: 0; left: 0;
  z-index: 3;
  background: white;
}

.project-details-loading-container.active {
  display: flex;
  align-items: center;
  justify-content: center;
}

.project-details-loading-container.active div {
  text-align: center;
}

.project-details-loading-container p {
      font-family: pvn;
      font-size: 16px;
      font-weight: bold;
      color: #2681DC;
      letter-spacing: .3px;
      margin-top: 5px;
}

.project-details-loading-container.mini p {
    font-size: 13px;
}

.project-details-loading-container svg {
      font-size: 55px;
      color: #2681DC;
}

.project-details-loading-container.mini svg {
      font-size: 30px;
}

.project-details-loading-container.active svg {
      animation: loading 1s linear infinite;
    }

@keyframes loading {
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
}

/*slider wrapper*/
.new-project-slider-wrapper {
    width: 100%;
    flex-grow: 1;
    flex-shrink: 0;
    overflow: hidden;
    position: relative;
}

/*slider*/
.new-project-slider {
    width:100%; height: 100%;
    display: flex;
    position: absolute;
    top: 0; left: 0;
}

/*slider section*/
.nps-section {
    height: 100%;
    overflow: hidden;
    overflow-y: auto;
}

/*list table wrapper*/
.list-table-wrapper {
    width: 100%; height: auto;
    box-sizing: border-box;
    padding: 0 10px;
}

/*project details form row*/
.project-details-form-row {
  width: 100%; height: auto;
  padding: 10px 10px;
  box-sizing: border-box;
}

.project-details-form-row label {
  font-weight: bold;
  font-size: 11.3px;
  color: #313132;
  letter-spacing: .5px;
  margin-top: 5px;
}

/*copy project details button styles*/
#copyProjectDetails {
  width: 100%; height: 30px;
  background: #DCDCDC;
  font-family: pvn;
  font-size: 13px;
  font-weight: bold;
  color: #313132;
  letter-spacing: .5px;
  border-radius: 3px;
  box-shadow: 0 2px 3px 0 rgba(0,0,0, .2);
  transition: background .1s ease, color .1s ease;
}

.work-graphic-table-content .wgtc-body{
    height: 60vh;
    overflow: auto;
}

::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

::-webkit-scrollbar
{
	height: 10px;
  width:10px;
	background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
	background-color: #A3D0E4;	
	background-image: -webkit-linear-gradient(45deg,
	                                          rgba(255, 255, 255, .2) 25%,
											  transparent 25%,
											  transparent 50%,
											  rgba(255, 255, 255, .2) 50%,
											  rgba(255, 255, 255, .2) 75%,
											  transparent 75%,
											  transparent)
}
#copyProjectDetails:hover {
  background: #03967D;
  color: #DCDCDC;
}

/*new project name input*/
#newProjectName {
  width: 100%;
  box-sizing: border-box;
}

/*project details title holder*/
.project-details-title-holder {
  width: 100%; height: 30px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  box-sizing: border-box;
  padding-left: 5px;
  border-bottom: 1px solid #2681DC;
}

.project-details-title-holder h1 {
  font-family: pvn;
  font-weight: bold;
  font-size: 14px;
  color: #2681DC;
  letter-spacing: .5px;
}

/*project details content holder*/
.project-details-content-holder {
  width: 100%; height: auto;
  padding-top: 15px;
  box-sizing: border-box;
}

.project-details-ul {
  display: flex;
  width: 100%; height: auto;
  list-style: none;
  padding: 0;
}

.pdu-sl {
  margin-top: 13px;
}

.pdu-sl li {
  width: 31%;
}

.pdu-work-hours li {
  width: 50%;
}

.project-details-ul li span {
  font-size: 11.3px;
  position: relative;
  top: 3px;
}

.project-details-content-holder input {
  width: 60px;
  box-sizing: border-box;
}

.sl-partial-hidden {
  visibility: hidden;
}

/*jquery ui dialog styles*/
.ui-dialog, #add-new-project, #new-holiday-form {
    overflow: visible !important;
}

/*Existed Project List Holder Table Styles*/
.my-table {
    width: 100%;
    table-layout: fixed;
    border: 1px solid #1b6d85;
    border-collapse: collapse;
}

.my-table thead {
    background: #c7e6f9;
}

.my-table tbody tr:nth-child(even) {
    background: #D8DEE3;
}

.my-table thead th {
    padding: 6px 7px;
    border-right: 1px solid #1b6d85;
    font-family: pvn;
    font-size: 12px;
    color: #1b6d85;
    font-weight: bold;
}

.my-table thead th {
    text-align: left;
}

.my-table thead th:last-child {
    border-right: none;
}

.my-table tbody tr {
  border-top: 1px solid #1b6d85;
}

.my-table tbody td {
    padding: 3px 7px;
    border-right: 1px solid #1b6d85;
    font-size: 10.6px;
    color: #1b6d85;
}

.my-table tbody td span {
    display: flex;
    width: 100%; height: 100%;
    align-items: center;
}

.my-table tbody td button {
    width: 100%; height: 20px;
    border: none;
    border-radius: 2px;
    background: #1c5d79;
    color: whitesmoke;
    font-family: pvn;
    font-size: 11px;
    cursor: pointer;
}

.my-table .rmv-holiday-fl {
  background: #e74c3c;
}

.my-table .rmv-holiday-fl:hover {
  background: #c0392b;
}

.my-table tbody td button:hover {
    background: #2097b7;
}

.my-table tbody td:last-child {
    border-right: none;
}

.empty-table-td {
    border-top: 1px solid #1c5d79;
    padding: 6px 0;
    text-align: center;
}

/*Holiday Dialog Header Styles*/
.holidays-schema-wrapper {
  display: grid;
  grid-template-rows: 50px auto;
  width: 100%; height: 100%;
}

/*header*/ 
.holidays-schema-header {
  width: 100%;
  border-bottom: 1px solid #dcdde1;
  box-sizing: border-box;
  padding: 0 10px;
}

.holidays-schema-header button {
    display: flex;
    height: 26px;
    padding: 0 15px;
    background: #f0f0f0;
    font-family: pvn;
    font-size: 11px;
    font-weight: bold;
    color: #272727;
    letter-spacing: .5px;
    border: 1px solid #aaaaaa;
    border-radius: 4px;
    align-items: center;
    justify-content: center;
    white-space: nowrap;
    margin-top: 13px;
    margin-left: 10px;
    float: left;
    transition: background .1s ease;
}

.holidays-schema-header button:first-child {
  margin-left: 0;
}

.holidays-schema-header button:hover {
  background: #e8e7e7;
}

/*body*/
.holidays-schema-body {
  width: 100%;
  padding: 25px 10px;
  box-sizing: border-box;
  overflow: hidden;
  overflow-y: auto;
}

/*Hour Graphic Table Styles*/
.wfm-section {
    width: 100%;
    padding: 21px 0;
    overflow: hidden;
    border-top: 1px solid #e9eff3;
}

/*section title holder*/
.wfm-section-title-holder {
    width: 100%; height: auto;
}

.wfm-section-title-holder h1 {
    font-family: pvn;
    font-size: .95em;
    color: #247b9f;
    letter-spacing: .3px;
    padding-left: 10px;
    cursor: pointer;
}

.wfm-section-title-holder i {
  font-size: 1.3em;
  transform: rotate(0deg);
  margin-top: -1px;
  transition: transform .2s ease;
}

.wfm-section-open i {
  transform: rotate(45deg);
}

/*section controls holder*/
.wfm-section-controls-holder {
    width: 100%; height: 23px;
    box-sizing: border-box;
    margin-top: 7px;
    padding-left: 10px;
}

.wfm-section-controls-holder button {
    color: whitesmoke;
    transition: background .1s ease;
}

.wfm-scb-icon-holder {
    width: 20px; height: 30px;
    float: left;
    display: flex;
    align-items: center;
    justify-content: center;
}

.wfm-scb-icon-holder i {
    font-size: 1em;
}

.wfm-scb-text-holder {
    display: block;
    height: 23px;
    float: left;
    box-sizing: border-box;
    font-family: pvn;
    font-size: .80em;
    letter-spacing: .4px;
    padding: 1px 7px 0 0;
}

#addNewHours {
    background: #f39c12;
    margin-top: 0px;
}

#addNewHours:hover {
    background: #e67e22;
}

/*Work Table Wrapper*/
.wfm-work-table-wrapper {
    width: 100%; height: 250px;
    margin-top: 21px;
    padding-left: 10px;
    box-sizing: border-box;
    display: flex;
}

/*Work Table Side*/
.wfm-work-table-holder {
    flex-basis: 80%;
    padding-right: 5px;
    overflow: hidden;
}

.wfm-work-table-week {
    flex-basis: 20%;
}

.add-new-hours-dialog-class input[type="text"], input[type="search"] {
    font-size: 1rem!important;
    height: auto!important;
    line-height: 0px!important;
    min-width: auto!important;
}

.wfm-wt-body {
    width: 100%; height: 179px;
    position: relative;
}

/*Work Table Head*/
.wfm-work-table-head {
    width: 100%;
    border-collapse: collapse;
}

/*Work Table container*/
.wfm-work-table-container {
    width: 100%;
    overflow: hidden;
}

.wfm-work-table-head thead {
    border-bottom: 1px solid #247b9f;
}

.wfm-work-table-head thead th {
    position: relative;
    padding: 10px 0 6px 0;
}

.wfm-work-table-head thead th span {
    display: table;
    width: 100%; height: 100%;
    position: relative;
    font-family: pvn;
    font-size: .85em;
    color: #3c4748;
    letter-spacing: .3px;
    text-align: left;
}

.wfm-work-table-head thead tr th {
    width: 70px;
}

.wfm-work-table-head thead tr th:nth-child(1) {
    width: 100px;
}

.wfm-work-table-head thead tr th:nth-child(1) span {
    position: absolute;
    bottom: 3px;
}

.wfm-work-table-head thead tr th:not(:first-child) span {
    text-align: center;
}

.wfm-work-table-head tbody td {
    height: 19px;
    padding: 0;
    position: relative;
    border-bottom: 1px solid #bdc3c7;
}

.wfm-work-table-head tbody tr:last-child td {
  border-bottom: 1px solid #247b9f;
}

.op{
  text-decoration: underline;
    cursor: pointer;
}

.wfm-work-table-head tbody td span {
    display: flex;
    width: 100%; height: 100%;
    align-items: center;
    box-sizing: border-box;
    padding-left: 5px;
    font-family: pvn;
    font-size: .85em;
    color: #247b9f;
    font-weight: bold;
    letter-spacing: .3px;
    cursor: pointer;
}

.wfm-work-table-head tbody tr:last-child td span {
    padding-bottom: 1px;
}

.wfm-work-table-head tbody tr:last-child td span {
    text-decoration: none;
}

.wfm-work-table-head tbody td span:hover {
    color: #e67e22;
}

.wfm-work-table-head tbody td h6 {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%; height: 100%;
  text-align: center;
  font-size: 1em;
  color: #247b9f;
  box-sizing: border-box;
  margin: 0;
}

.wfm-work-table-head tfoot tr td {
    font-family: pvn;
    font-size: .85em;
    color: #2c3e50;
    text-align: center;
    font-weight: bold;
    padding-top: 5px;
}

/*Work Table*/
#work_table{
    width: 100%;
    position: absolute;
    top: 0; left: 0;
    border-collapse: collapse;
}

#work_table thead th, #work_table tbody td {
    border: 1px solid #bdc3c7;
}

#work_table thead tr:nth-child(2) th, #work_table tbody tr:last-child td{
    padding: 5px 4px;
    border-bottom: none;
}

#work_table tbody tr:first-child td {
    border-top: none;
}

#work_table thead, #work_table tbody {
    border-bottom: 1px solid #247b9f;
}

#work_table td,#work_table th{
    font-size: 11px;
    font-weight: normal;
    text-align: center;
}

/*Work Table Body*/
.wfm-wt-footer {
    width: 100%; height: 7px;
    box-sizing: border-box;
    margin-top: 18px;
    overflow: hidden;
    overflow-x: scroll;
    transform: translateY(-13px);
}

.wfm-wt-scroll-width {
    width: 2113px; height: 10px;
}

/*Planer Table styles*/
.wfm-planer-table-wrapper {
    width: 99.7%; height: auto;
    display: flex;
    padding-left: 10px;
    box-sizing: border-box;
    margin-top: 18px;
}

.wfm-planer-table-wrapper table:not(.wfm-planer-table-content) {
    table-layout: fixed;
    width: 100%;
}

/*operators list holder side*/
.wfm-pts-operators {
    flex-basis: 360px;
    flex-shrink: 0;
}

.thick-border-bottom {
    border-bottom: 2px solid #1b6d85;
}

/*operators list holder table*/
.wfm-planer-table-head tr {
    display: flex;
}

.wfm-planer-table-head tbody tr:not(:last-child) {
    border-bottom: 1px solid #bdc3c7;
}

.wfm-pth-col {
    height: 50px;
    position: relative;
}

.wfm-pth-col > span {
  display: block;
  width: 100%; height: auto;
  font-family: pvn;
  font-size: .80em;
  color: #3c4748;
  letter-spacing: .3px;
  text-align: center;
  position: absolute;
  bottom: 5px; left: 0;
}

.wfm-pth-col-min {
    height: 35px;
    position: relative;
}

.wfm-pth-col-xs {
    height: 15px;
    position: relative;
}

.wfm-pth-col-title {
    width: 290px;
}

.wfm-pth-col-title span {
    display: block;
    width: 100%; height: auto;
    font-family: pvn;
    font-size: .80em;
    color: #3c4748;
    letter-spacing: .3px;
    text-align: left;
    position: absolute;
    bottom: 8px; left: 0;
}

.wfm-pth-col-name {
    width: 107px;
    display: flex !important;
    align-items: center;
}

.wfm-pth-col-name span {
    display: block;
    width: 100%; height: auto;
    font-size: .97em;
    color: #1e2829;
    letter-spacing: .3px;
}

.wfm-pth-col-select {
    width: 180px;
}

.wfm-pth-col-select select {
    width: 95%;
    position: absolute;
    top: 50%; left: 50%;
    transform: translate(-50%, -50%);
    border: 1px solid #1b6d85;
    padding: 4px 0;
    border-radius: 5px;
    background: white;
    font-size: 1.1em;
}

.wfm-pth-col-button {
/*     width: 70px; */
}

.wfm-pth-col-button button {
    background-color: orange;
    font-size: 11px !important;
    color: #fff;
    font-family: 'BPG' !important
}

.wfm-pth-col-button #addWorkPlannerRow {
    
    
    height: 26px;
    bottom: 3px;
   
    position: absolute;
  
    padding: 0 7px;
}
.wfm-pth-col-button button {
    
  
   
}

.wfm-pth-col-title span {
    display: block;
    width: 100%;
    height: auto;
    font-family: pvn;
    font-size: .80em;
    color: #3c4748;
    letter-spacing: .3px;
    text-align: left;
    position: absolute;
    bottom: 40px;
    left: 10px;
}
.wfm-pth-col-select select {
    /* width: 95%;
    position: absolute;
    top: 20%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: 1px solid #1b6d85;
    padding: 4px 0;
    border-radius: 5px;
    background: white;
    font-size: 1.1em; */
}
.wfm-pth-col-button .delete-work-planner-row {
  background: #e74c3c;
    font-size: .90em;
    left: -228px;
    top: 6px;
}

.wfm-pth-col-button .clear-work-planner-row {
  background: #3498db;
    font-size: .80em;
    left: -199px;
    width: 30px;
    height: 26px;
    top: 5px;
}

.wfm-pth-col-button button svg {
  font-size: 15px;
}

.wfm-pth-col-button button:hover {
    background: #e67e22;
}

.wfm-pth-col-button .delete-work-planner-row:hover {
    background: #c0392b;
}

.wfm-pth-col-button .clear-work-planner-row:hover {
    background: #2980b9;
}

/*day list*/
.wfm-pts-table-holder {
    flex-grow: 1;
    box-sizing: border-box;
    border-right: 1px solid #bdc3c7;
}

.wfm-planer-table-container {
    width: 100%; height: 100%;
    box-sizing: border-box;
}

.wfm-pt-body {
    position: relative;
    width: 100%; height: 100%;
    overflow: hidden;
    box-sizing: border-box;
}

.wfm-planer-table-content {
    width: 2325px;
    position: absolute;
    top: 0; left: 0;
    padding-bottom: 3px;
    border-collapse: collapse;
}

.wfm-planer-hours-table {
    width: 2325px; height: 100px;
    position: absolute;
}

.wfm-planer-table-content thead, .wfm-pts-days-body {
    border-bottom: 2px solid #1b6d85;
}

.wfm-pts-days-body tr:last-child td {
    border-bottom: none;
}

.wfm-planer-table-content td, .wfm-planer-table-content th {
    border: 1px solid #bdc3c7;
}

.wfm-planer-table-content thead span {
    font-family: pvn;
}

.wfm-ptc-col-min {
    height: 15px;
}

.wfm-ptc-col {
    display: flex;
    height: 35px;
    cursor: pointer;
    box-sizing: border-box;
}

.wfm-ptc-col.footer-col {
    padding: 5px 2px;
}

.wfm-ptc-col-mane {
    position: relative;
}

.wfm-day-green {
    background: #1abc9c;
}

.wfm-day-blue {
    background: #3498db;
}

.wfm-day-pink {
    background: #D2527F;
}

.wfm-ptc-col-weekeand {
    background: #EB9532;
}

.wfm-ptc-col-mane span {
    display: block;
    width: 100%; height: 100%;
    box-sizing: border-box;
    padding-top: 2px;
    font-size: 1em;
}

.wfm-ptc-col-title {
    padding-top: 1px;
    width: 80px; height: 28px;
}

.wfm-ptc-col-title span {
    display: flex;
    width: 100%; height: 50%;
    text-align: center;
    font-size: .80em;
    font-weight: bolder;
    align-items: center;
    justify-content: center;
}

.wfm-ptc-col span.wfm-ptc-hours-sum {
  font-weight: bold;
  color: #4183da;
}

.wfm-ptc-col span.wfm-ptc-hours-sum.over {
  padding: 3px 0;
  background: #E51D2B;
  color: #F2F2F2;
  border-radius: 3px;
}

.wfm-planer-table-buttons tbody tr:not(:last-child) td {
  border-bottom: 1px solid #bdc3c7;
}

.wfm-pt-footer {
    width: 100%; height: 7px;
    margin-top: 6px;
    overflow: hidden;
    overflow-x: scroll;
    box-sizing: border-box;
    position: relative;
}

.wfm-pt-scroll-width {
    width: 2325px; height: 7px;
    position: absolute;
    top: 0; left: 0;
}

/*loop list*/
.wfm-pts-loop-buttons {
    flex-basis: 200px;
    flex-shrink: 0;
}

.wfm-pts-loop-buttons button {
    width: 97%; height: 100%;
    font-family: pvn;
    font-size: .87em;
    color: #ecf0f1;
    letter-spacing: .3px;
    background: #758687;
    margin-left: 2px;
    transition: background .1s ease;
}

.wfm-pts-loop-buttons button:not(.sasl-active):hover {
    background: #3c4748;
}

.wfm-pts-loop-buttons .sasl-active {
    background: #1E824C;
}

.wfm-planer-table-buttons tbody tr:not(:last-child) {
    border-bottom: 1px solid transparent;
}

/*operators list holder side*/
.wfm-pts-plan-hours {
    flex-basis: 360px;
    flex-shrink: 0;
}

.wfm-pth-hours-name {
    display: flex;
    width: 350px; height: 25px;
    align-items: center;
}

.wfm-pth-hours-name span {
    display: block;
    width: 100%;
    text-align: right;
    font-family: pvn;
    font-size: 1em;
    font-weight: bold;
}

.plane-hours-name {
    color: #4183D7;
}

.operation-hours-name {
    color: #3c4748;
}

.difference-hours-name {
    color: #4183D7;
}

.wfm-planer-table-holder {
    flex-grow: 1;
}

.wfm-pht-row {
    width: 100%; height: 25px;
    display: flex;
    border-bottom: 1px solid transparent;
}

.wfm-pht-row span {
    font-weight: bolder;
    font-family: Sylfaen;
    font-size: .95em;
    letter-spacing: .3px;
}

.wfm-pht-paln-row span {
    color: #4183D7;
}

.wfm-pht-operation-row span {
    color: #3c4748;
}

.wfm-pht-over-value span {
    color: #c0392b;
}

.wfm-pht-less-value span {
    background: #019875;
}

.wfm-pht-less-value span {
    color: #ecf0f1;
}

.wfm-pht-exact-value span {
    color: #019875;
}

.wfm-pht-col {
    width: 83px; height: 25px;
    padding: 0 3px;
}

.wfm-pht-col span {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
}

/*button area*/
.button-area {
    width: 75%;
    padding: 15px 0;
}

/*Tables In Dialog Styles*/
.dialog-form-table-holidays {
    width: 100%;
}

/*choose holiday from list for project button styles*/
.choose-holiday-for-project {
  width: 100%; height: 94%;
  border-radius: 2px;
  font-family: pvn;
  font-size: .90em;
  color: whitesmoke;
}

.chfp-add {
  background: #1abc9c;
}

.chfp-add:hover {
  background: #16a085;
}

.chfp-remove {
  background: #7f8c8d;
}

.chfp-remove:hover {
  background: #5d6262;
}

/*ADD NEW HOURS TABLE STYLES*/
.anh-header {
  width: 100%; height: 25px;
}

.anh-header-margin {
  margin-bottom: 10px;
}

.anh-header button {
  height: 100%;
  box-sizing: border-box;
  padding: 0 11px;
  background: #f39c12;
}

.anh-header button span {
  font-family: pvn;
  font-size: .80em;
  font-weight: bold;
  color: #ecf0f1;
  letter-spacing: .3px;
  position: relative;
  top: 2px;
}

.anh-header button:hover {
  background: #e67e22;
}

/*table*/
.add-new-hours-table {
    width: 100%;
    table-layout: fixed;
}

/*table header*/
.add-new-hours-table thead tr th:nth-child(1) {
    width: 100px;
}

.add-new-hours-table thead tr th:nth-child(2) {
    width: 50px;
    padding: 0 10px;
}

.add-new-hours-table thead tr th:nth-child(3) {
    width: 50px;
    padding: 0 10px;
}

.add-new-hours-table thead tr th:nth-child(4) {
    width: 50px;
}

.add-new-hours-table thead tr th:nth-child(5) {
    width: 70px;
}

.add-new-hours-table thead tr th:nth-child(6) {
    width: 70px;
}

.add-new-hours-table thead tr th:nth-child(7) {
    width: 50px;
}

.add-new-hours-table thead tr th:nth-child(8) {
    width: 50px;
}

.add-new-hours-table thead tr th:nth-child(9) {
    width: 50px;
}

.anh-col {
    width: 100%; height: 30px;
    padding-top: 17px;
    padding-bottom: 6px;
    display: flex;
    align-items: start;
    justify-content: center;
}

.anh-col-lefter {
    justify-content: flex-start;
}

.anh-col span {
    display: block;
    font-family: pvn;
    font-size: .83em;
    color: #7f8c8d;
    letter-spacing: .3px;
    align-self: flex-end;
}

.anh-all-time-sum-receiver {
  display: block;
  width: 100%; height: 15px;
  border-top: 1px solid #247b9f;
  text-align: center;
  padding-top: 3px;
  font-size: 1.2em;
  font-weight: bolder;
  color: #247b9f;
  letter-spacing: .3px;
}


/*input wrapper*/
.anh-input-wrapper {
    width: 70%; height: 100%;
    box-sizing: border-box;
    border: 1px solid #1b6d85;
    display: flex;
    position: relative;
    left: 50%;
    transform: translateX(-50%);
    margin-top: 2px;
    margin-right: 5px;
}

.anh-input-wrapper input {
    width: 50%;
    border: none;
    text-align: center;
    padding: 2px 0;
	min-width: 4px;
    font-size: 10px !important;
}

.anh-input-wrapper .anh-colen {
    width: 2px;
    background: none;
    font-weight: bolder;
}

.new-hours-input {
    width: 70%;
    box-sizing: border-box;
    border: 1px solid #1b6d85;
    text-align: center;
    padding: 2px 0;
    margin-top: 2px;
    position: relative;
    margin-left: 0;
    left: 50%;
    transform: translateX(-50%);
}

.new-hours-input::-webkit-inner-spin-button,
.new-hours-input::-webkit-inner-spin-button {
    -webkit-appearance: none;
}

/*day name holder*/
.anh-day-name {
    display: flex;
    width: 100%; height: 100%;
    align-items: center;
    justify-content: flex-start;
    padding: 4px 0;
    font-family: pvn;
    font-size: 1.1em;
    color: #247b9f;
    letter-spacing: .3px;
    font-weight: bold;
}

/*different time receiver*/
.anh-dif-time-receiver, .anh-sum-time-receiver {
    display: flex;
    width: 100%; height: 18px;
    align-items: center;
    justify-content: center;
    font-family: Sylfaen;
    font-size: 1.1em;
    color: #247b9f;
    letter-spacing: .3px;
    margin-top: 2px;
}

.anh-sum-time-receiver {
    font-size: 1.2em;
}

.new-hours-checkbox {
    width: 100%; height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 2px;
}

.focus-receiver {
  width: 0px; height: 0px;
  border: none;
  background: none;
  position: absolute;
  top: 0; left: 0;
  opacity: 0;
}

/*button*/
.new-hours-buttons button {
  width: 30px; height: 20px;
  padding: 2px;
  border-radius: 2px;
  transform: translateX(50%);
  transition: background .1s ease;
}

.new-hours-buttons button i {
  font-size: 1em;
  color: #ecf0f1;
}

.delete-hour-ind-day-row{
  background: #e74c3c;
}

.edit-hour-ind-day-row {
  background: #3498db;
}

.delete-hour-ind-day-row:hover {
  background: #c0392b;
}

.edit-hour-ind-day-row:hover {
  background: #2980b9;
}

/*planner shift control styles*/
.shift-controller-head {
  width: 100%; height: auto;
  padding: 10px 0;
  box-sizing: border-box;
}

.shift-controller-head select {
  width: 100%;
}

.planner-shift-controller-foot {
  width: 100%; height: auto;
}

.planner-shift-controller-foot h5 {
  font-family: pvn;
  font-size: 1em;
  color: #247b9f;
}

.planner-shift-controller-foot button {
  font-family: pvn;
  font-syze: .90em;
  padding: 6px 10px;
  background: #f39c12;
  color: #ecf0f1;
  margin-top: 7px;
  transition: background .1s ease;
}

.planner-shift-controller-foot button:hover {
  background: #e67e22;
}

/*planer ad shift table styles*/
#planerAddShiftTable {
  width: 100%; height: auto;
  table-layout: fixed;
}

#planerAddShiftTable td:nth-child(1) {
  width: 100px;
  white-space: nowrap;
  padding-top: 5px;
  padding-bottom: 7px;
}

#planerAddShiftTable td:nth-child(1) span {
  font-family: pvn;
  font-size: 1em;
  font-weight: bold;
  color: #7f8c8d;
  letter-spacing: .3px;
}

#planerAddShiftTable input,
#planerAddShiftTable select,
#planerAddShiftTable textarea {
  width: 100%;
  border: 1px solid #03839a;
}

#planerAddShiftTable textarea {
  box-sizing: border-box;
}

#planerAddShiftTable #planner_shift_start_timeout {
  width: 95%;
}

#planerAddShiftTable #planner_shift_end_timeout {
  width: 95%;
  float: right;
}

#plannerShiftAmply {
  float:right;
  text-decoration: underline;
  padding: 5px 0 0 0;
}

#plannerShiftAmply.open {
  color: #03839a;
}

#plannerShiftAmply.close {
  color: #95a5a6;
}

#plannerShiftAmply:hover {
  color: #005260;
}

#plannerShiftAddTableBody.psa-hidden {
  display: none;
}

#plannerCycleStartPosition {
  width: 100%;
  border: 1px solid #03839a;
  margin-top: 5px;
}

#divideShiftBreakTime {
  width: 28px; height: 16px;
  background: #e67e22;
  border-radius: 2px;
  float: right;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: background .1s ease;
}

#divideShiftBreakTime:hover {
  background: #d35400;
}

#divideShiftBreakTime i {
  color: #ecf0f1;
}

#shiftBreakRangeListHolder { /*shift break list holder*/
  width: 100%; height: auto;
  table-layout: fixed;
  border-bottom: 1px solid #dcdde1;
  margin-bottom: 8px;
}

#shiftBreakRangeListHolder th {
  font-family: pvn;
  font-size: 11px;
  font-weight: bold;
  color: #40739e;
  text-align: left;
}

#shiftBreakRangeListHolder th:nth-child(1) {
  width: 75px;
}

#shiftBreakRangeListHolder th:nth-child(2) {
  width: 85px;
}

#shiftBreakRangeListHolder th:nth-child(3) {
  width: 55px;
}

#shiftBreakRangeListHolder th:nth-child(4) {
  width: 71px;
}

#shiftBreakRangeListHolder td {
  box-sizing: border-box;
  padding-top: 0 !important;
}

#shiftBreakRangeListHolder td:not(:last-child) {
  padding-right: 10px;
}

.shift-break-activitie {
  width: 100%; height: 14px;
  border: 1px solid #03839a;
  cursor: pointer;
}

.delete-shift-break-time-row {
  width: 24px; height: 16px;
  border-radius: 2px;
  float: right;
  background: #e74c3c;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: background .1s ease;
}

.delete-shift-break-time-row i {
  color: #ecf0f1;
}

.delete-shift-break-time-row:hover {
  background: #c0392b;
}

/*-GRAPHIC STYLES-*/
.work-graphic-schema-wrapper {
  display: grid;
  width: 100%; height: 100%;
  box-sizing: border-box;
  grid-template-rows: 50px auto 70px;
}

.work-graphic-schema-header {
  width: 100%;
  border-bottom: 1px solid #dcdde1;
  box-sizing: border-box;
  padding-left: 10px;
}

.work-graphic-schema-header select {
  width: 150px; height: 26px;
  border: 1px solid #2681dc;
  border-radius: 5px;
  outline: none;
  position: relative;
  top: 50%;
  transform: translateY(-50%);
  font-size: .93em;
  box-sizing: border-box;
  padding-left: 6px;
  -webkit-appearance: none;
  background-image: url("media/images/select_bg.png");
  cursor: pointer;
}

.work-graphic-schema-body {
  display: grid;
  width: 100%; height: auto;
  grid-template-columns: 450px auto 140px;
  margin-top: 34px;
  overflow: hidden;
  overflow-y: auto;
  box-sizing: border-box;
  padding: 0 10px;
}

.wgsb-header {
  width: 100%; height: auto;
}

.wgtc-head {
  width: 100%; height: auto;
  border-bottom: 2px solid #2681dc;
}

.wgtc-head-row {
  display: flex;
  width: 100%;
}

.wgtc-head-row.height-1 {
/*   height: 20px; */
	font-size: 13px;
}

.wgtc-head-row.height-2 {
  height: 40px;
}

.wgtc-head-row.height-3 {
  height: 60px;
}

.wgtc-head-row.height-4 {
  height: 80px;
}

.wgtc-head-row.results {
  height: 24px;
}

.wgsb-header-col, .wgsb-footer-col {
  height: 100%;
  box-sizing: border-box;
  padding: 3px;
  padding-top: 0;
}

.wgsb-header-col.no {
  width: 50px;
  text-align: center;
}

.wgsb-header-col.select {
  width: 165px;
}

.wgsb-header-col.button {
/*   width: 70px; */
  display: flex;
}

.wgsb-footer-col {
  width: 50%;
}

.wgsb-header-col.full-width {
  width: 100%;
}

.wgsb-header-col.content-center {
  text-align: center;
}

.work-graphic-table-results span {
  display: flex;
  width: 100%; height: 100%;
  justify-content: flex-end;
  align-items: center;
  box-sizing: border-box;
  padding-right: 5px;
  font-size: .84em;
  font-weight: bold;
  letter-spacing: .3px;
}

.work-graphic-table-results span.planned {
  color: #0f6299;
}

.work-graphic-table-results span.operated {
  color: #808080;
}

.work-graphic-table-results span.differenc {
  color: #0f6299;
}

.wgsb-header .wgtc-head span {
  width: 100%; height: 100%;
  display: block;
  font-size: .83em;
  font-family: pvn;
  font-weight: bold;
  color: #2681dc;
  letter-spacing: .3px;
  box-sizing: border-box;
  padding-top: 6px;
}

.wgsb-header-col.button button {
  height: 100%;
  font: .77em pvn;
  color: #ecf0f1;
  letter-spacing: .5px;
  transition: background .1s ease;
}

.wgtc-head button {
  width: 100%;
  background: #f39c12;
}

.wgtc-head button:hover {
  background: #e67e22;
}

.wgtc-body button {
  display: flex;
  width: 49%;
  margin-right: 2px;
  border-radius: 2px;
  align-items: center;
  justify-content: center;
}

.wgtc-body button svg {
  font-size: 15px;
}

.wgtc-body .delete-work-graphic-row {
  background: #e74c3c;
}

.wgtc-body .delete-work-graphic-row:hover {
  background: #c0392b;
}

.wgtc-body .clear-work-graphic-row {
  background: #3498db;
}

.wgtc-body .clear-work-graphic-row:hover {
  background: #2980b9;
}

.wgtc-body select {
  width: 100%; height: 26px;
  border: 1px solid #2681dc;
  border-radius: 5px;
  outline: none;
  font-size: .93em;
  box-sizing: border-box;
  padding-left: 6px;
  -webkit-appearance: none;
  background-image: url(media/images/select_bg.png);
  background-size: cover;
  cursor: pointer;
  position: relative;
  top: -1px;
font-size: 11px !important;
}

.wgsb-content {
  position: relative;
  overflow: hidden;
  box-sizing: border-box;
  margin-top: 9px;
}

.wgtc-slider {
  height: auto;
  position: relative;
  left: 0;
}

.wgtc-month-year {
  border-bottom: 1px solid #808080;
}

.wgtc-month-year span {
  display: block;
  width: 100%; height: 100%;
  box-sizing: border-box;
  padding-top: 3px;
  letter-spacing: .5px;
  color: #272727;
  font-weight: bold;
  font-family: pvn;
  font-size: .90em;
}

.wgsb-content-col {
  width: 90px; height: 100%;
  border-right: 1px solid #808080;
  box-sizing: border-box;
  position: relative;
}

.wgsb-content-col:last-child {
  border-right:  none;
}

.wgsb-content-col .wgtc-head-row:not(:last-child) {
  border-bottom: 1px solid #808080;
}

.wgsb-content-col span, .wgsb-content-col a {
  display: flex;
  width: 100%; height: 100%;
  justify-content: center;
  align-items: center;
  letter-spacing: .3px;
}

.wgsb-content-col span {
  font-size: .85em;
}

.wgsb-content-col a {
  font-size: .78em;
}

.wgsb-content-col.work-shift {
  display: flex;
  cursor: pointer;
  padding: 2px;
}

.wgsb-content-col-unit {
  display: flex;
  flex: 1;
  position: relative;
}

.wgsb-content-col-unit > span {
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
}

.wgsb-content-col-unit.holiday-unit {
  background: #098987;
}

.wgsb-content-col-unit.holiday-unit span {
  color: #F2F2F2;
}

.wgsb-ccu-description {
  min-width: 50px; min-height: 25px;
  display: flex;
  border-radius: 3px;
  position: absolute;
  background: #313132;
  visibility: hidden;
  left: 50%;
  align-items: center;
  justify-content: center;
  transform: translate(-50%, -100%);
  color: #F2F2F2;
  padding: 5px 20px;
  text-align: center;
  z-index: 10;
  white-space: nowrap;
  box-shadow: 0px 2px 3px 0px rgba(0,0,0, .3);
}

.wgsb-content-col.work-shift:first-child .wgsb-content-col-unit:first-child .wgsb-ccu-description {
  transform: translate(-23%, -100%);
}

.wgsb-content-col.work-shift:last-child .wgsb-content-col-unit:last-child .wgsb-ccu-description {
  transform: translate(-77%, -100%);
}

.wgsb-content-col-unit:hover .wgsb-ccu-description {
  visibility: visible;
}

.wg-shift-list-container {
  width: 100%; height: auto;
  padding: 15px 0;
}

.wg-shift-list-container h1 {
  font-family: pvn;
  font-size: 14px;
  color: #2376C8;
  letter-spacing: .3px;
}

.wg-slc-ul {
  width: 100%; height: auto;
  list-style: none;
  padding: 0;
}

.wg-slc-ul li {
  display: grid;
  grid-template-columns: 100px auto 60px;
}

.wg-slc-header {
  padding: 10px 0;
  margin-top: 10px;
  background: #DCDCDC;
}

.wg-slc-header > div {
  box-sizing: border-box;
  padding-left: 10px;
  font-family: pvn;
  font-weight: bold;
  font-size: 12px;
  color: #313132;
}

.wg-slc-list-unit {
  height: 30px;
  padding: 2px 0;
  border-bottom: 1px solid #F4F4F4;
}

.wg-slc-list-unit:last-child {
  border-bottom: 1px solid #DCDCDC;
	height: 35px;
}

.wg-slc-name-holder {
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
}

.wg-slc-name-holder span {
  display: block;
  width: 100%;
  white-space: nowrap; 
  overflow: hidden;
  text-overflow: ellipsis;
  box-sizing: border-box;
  padding-left: 7px;
}

.remove-shift-from-graphic {
  color: #313132;
  border-radius: 3px;
  transition: background .1s ease, color .1s ease;
}

.remove-shift-from-graphic[disabled] {
  opacity: .3;
}

.remove-shift-from-graphic:not([disabled]):hover {
  background: #C12631;
  color: #F4F4F4;
}

.remove-shift-from-planner {
  color: #313132;
  border-radius: 3px;
  transition: background .1s ease, color .1s ease;
}

.remove-shift-from-planner[disabled] {
  opacity: .3;
}

.remove-shift-from-planner:not([disabled]):hover {
  background: #C12631;
  color: #F4F4F4;
}

.wg-shift-list-add {
  width: 100%; height: auto;
  padding: 10px 0;
}

#plannerAddShift {
  width: 100%; height: 35px;
  box-shadow: 0 2px 3px 0 rgba(0, 0, 0, .3);
  border-radius: 3px;
  font-family: pvn;
  font-weight: bold;
  background: #2376C8;
  color: #DCDCDC;
  margin-top: 10px;
  transition: all .1s ease;
}

#plannerAddShift:hover {
  background: #03967D;
}

.wg-shift-list-add h1 {
  font-family: pvn;
  font-size: 14px;
  color: #2376C8;
  letter-spacing: .3px;
  margin-bottom: 7px;
}

.wgsb-content .wgtc-body span {
  font-size: .9em;
  font-weight: bold;
}

.wgtc-body .col-edit-history-pointer {
  display: block;
  padding: 0;
  width: 15px; height: 15px;
  position: absolute;
  top: 0; right: -2px;
  z-index: 10;
  color: #F90442;
  border-radius: 0;
  overflow: hidden;
}

.wgtc-body .col-edit-history-pointer svg {
  font-size: 30px;
  transform: rotate(-45deg);
  position: absolute;
  top: -10px; right: 0;
  margin: 0;
  padding: 0;
}

.column-edit-history-shift {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: center;
}

.wgtc-head-percent-holder {
  width: 100%; height: 100%;
  box-sizing: border-box;
  padding: 2px;
  padding-bottom: 4px;
}

.wgtc-head-percent-value {
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  background: #bdc3c7;
  border-radius: 2px;
  position: relative;
}

.wgtc-head-percent-value .value {
  height: 100%;
  position: absolute;
  top: 0; left: 0;
  z-index: 1;
}

.wgtc-head-percent-value span {
  font-size: .78em;
  font-weight: bold;
  position: relative;
  z-index: 2;
}

.wgtc-head-weekend {
  background: #ef8a0a;
}

.wgtc-head-hollyday {
  background: #098987;
  color: #F2F2F2;
}

.wgt-ddo-hollyday {
  color: #098987;
}

.work-graphic-table-content {
  border-top: 1px solid transparent;
}

.wgsb-content .work-graphic-table-content {
  border: 1px solid #808080;
  border-bottom: none;
}

.wgsb-footer-col span {
  display: block;
  width: 100%;
  font-size: .88em;
  color: #808080;
  letter-spacing: .3px;
  line-height: 16px;
  text-align: center;
}

.wgtc-body-row {
  width: 100%; height: 38px;
  display: flex;
  box-sizing: border-box;
  border-bottom: 1px solid #808080;
}

.wgtc-body-row:last-child {
  border-bottom: 2px solid #2681dc;
}

.wgtc-body .wgsb-header-col {
  padding-top: 6px;
  padding-bottom: 6px;
}

.wgtc-body .wgsb-header-col.no span {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: center;
}

.wgsb-footer-col.hour-count {
  display: flex;
  align-items: center;
  justify-content: center;
}

.wgsb-footer-col.hour-count span {
  font-size: .87em;
  font-weight: bold;
  color: #1062ab;
}

.wgsb-footer-col.over span {
  padding: 3px 0;
  background: #E51D2B;
  color: #F2F2F2;
  border-radius: 3px;
}

.work-graphic-table-scroll {
  width: 100%; height: 15px;
  box-sizing: border-box;
  padding: 4px 0;
  border: none !important;
}

.wgts-wrapper {
  width: 100%; height: 100%;
  overflow: hidden;
  overflow-x: scroll;
}

.wgts-place-holder {
  width: 2520px; height: 7px;
}

.wgtc-results-row {
  width: 100%; height: 24px;
  display: flex;
  box-sizing: border-box;
}

.wgsb-results-col {
  width: 90px; height: 100%;
  box-sizing: border-box;
}

.wgsb-results-col span {
  display: flex;
  width: 100%; height: 100%;
  justify-content: center;
  align-items: center;
  letter-spacing: .3px;
}

.wgsb-results-col.planned span {
  color: #0f6299;
}

.wgsb-results-col.operated span {
  color: #808080;
}

.wgsb-results-col.exact span {
  color: #026b3b;
}

.wgsb-results-col.less span {
  color: #9b2c03;
}

.wgsb-results-col.over {
  padding: 0 1px;
}

.wgsb-results-col.over span {
  color: #ffffff;
  background: #026b3b;
}

.work-graphic-schema-footer {
    width: 100%;
    padding: 15px 10px;
    border-top: 1px solid #dcdde1;
    box-sizing: border-box;
}

/*DAY GRAPHIC STYLES*/
.wg-dg-header {     /*header*/
  display: grid;
  width: 100%;
  padding: 10px 0;
  grid-template-columns: 150px auto 90px 50px;
  border-bottom: 1px solid #dcdde1;
}

.wg-dg-date-holder {
  box-sizing: border-box;
  padding-left: 10px;
}

.wg-dg-date-holder span {
  font-family: pvn;
  font-size: .86em;
  font-weight: bold;
  color: #429ace;
  letter-spacing: .3px;
}

.wg-dg-date-holder .date-receiver {
  color: #272727;
  font-size: 1em;
}

.wg-dg-date-holder span:not(.date-receiver) {
  position: relative;
  top: 1px;
}

.wg-dg-controls-holder button {
  display: flex;
  width: 26px;
  height: 26px;
  background: #f0f0f0;
  border: 1px solid #aaaaaa;
  border-radius: 4px;
  align-items: center;
  justify-content: center;
  float: left;
  margin-top: 1.5px;
  margin-right: 4px;
  transition: background .1s ease;
}

.wg-dg-controls-holder button.active {
  opacity: .4;
}

.wg-dg-controls-holder button:not(.active):hover {
  background: #e8e7e7;
}

.wg-dg-controls-holder button i {
  font-size: 18px;
  font-weight: bold;
  color: #272727;
}

.wg-dg-controls-holder button:last-child {
  margin-right: 0;
}

.wg-dg-dialog-closer {
  box-sizing: border-box;
  padding-right: 10px;
}

.wg-dg-dialog-closer button {
  display: flex;
  width: 26px;
  height: 26px;
  align-items: center;
  justify-content: center;
  margin-top: 1.5px;
  float: right;
}

.wg-dg-dialog-closer button i {
  font-size: 18px;
  font-weight: bold;
  color: #272727;
  opacity: .7;
  transform: rotateY(180deg);
  transition: opacity .1s ease;
}

.wg-dg-dialog-closer button i:hover {
  opacity: 1;
}

.status-description-list,
.color-description-list {
  list-style: none;
  padding: 0;
  margin: 0;
}

.color-description-list {
  float: left;
  margin-right: 50px;
  margin-left: 10px;
}

.status-description-list label,
.color-description-list label {
  font-size: .86em;
  font-weight: bold;
  color: #272727;
  letter-spacing: .3px;
  margin-right: 9px;
}

.status-description-list li,
.color-description-list li {
  display: inline-block;
  margin-top: 4px;
  text-align: center;
  align-items: center;
  justify-content: center;
}

.color-description-list li:not(:last-child) {
  margin-right: 12px;
}

.status-description-list span {
  display: block;
  padding: 5px 10px 10px 10px;
}

.status-description-list span,
.color-description-list span {
  font-size: 11px;
}

.color-description-list span {
  position: relative;
  top: 5px;
}

.color-description-list span::before {
    content: "";
    display: inline-block;
    width: 13px; height: 13px;
    margin-right: 4px;
    position: relative;
    top: 2px;
}

.color-description-list span[data-type="work-hours"]::before {
  background-color: #a9d08e;
}

.color-description-list span[data-type="rush-hours"]::before {
  background-image: url("./media/images/rush_hour_patern.png");
}

.wg-dg-body {   /*body*/
  display: grid;
  width: 100%; height: auto;
  padding: 10px 10px;
  margin-top: 44px;
  grid-template-columns: 280px auto 230px;
  overflow: hidden;
  overflow-y: auto;
  box-sizing: border-box;
}

.wg-dg-body .value-table {
  box-sizing: border-box;
  overflow: hidden;
}

.wg-dg-body .content {  /* body - content */
  width: 100%; height: auto;
  position: relative;
  box-sizing: border-box;
}

.wg-dg-body .operators .content {
  border-right: 1px solid #808080;
}

.wg-dg-body .hours-summary .content {
  border-left: 1px solid #808080;
}

.wg-dg-body .content .head {
  display: flex;
  width: 100%; height: 45px;
  border-bottom: 2px solid #0f6299;
  box-sizing: border-box;
}

.wg-dg-body .value-table .content .head {
  border-top: 1px solid #808080;
}

.wg-dg-body .operators .content .col {
  box-sizing: border-box;
  text-align: center;
  padding: 0 5px;
}

.wg-dg-body .operators .content .col:first-child {
  width: 50px;
}

.wg-dg-body .operators .content .flex-width {
  flex: 1;
  text-align: left;
  padding: 0;
  padding-left: 5px;
}

.wg-dg-body .operators .content .head .col span {
  display: block;
  font-family: pvn;
  font-weight: bold;
  font-size: .83em;
  letter-spacing: .5px;
  color: #0f6299;
  margin-top: 25px;
}

.wg-dg-body .operators .content .head .col button {
  width: 100%; height: 20px;
  background: #f39c12;
  font-family: pvn;
  font-weight: bold;
  font-size: .75em;
  letter-spacing: .5px;
  color: #ecf0f1;
  margin-top: 16px;
  transition: background .1s ease;
}

.wg-dg-body .operators .body .row {
  display: flex;
  width: 100%; height: 25px;
  border-bottom: 1px solid #808080;
}

.wg-dg-body .operators .body .row:last-child {
  border-bottom: none;
}

.wg-dg-body .operators .body .row .col {
  display: flex;
  height: 100%;
  align-items: center;
  margin: 0;
}

.wg-dg-body .operators .body .row .content-center {
  justify-content: center;
}

.wg-dg-body .operators .content .head .col button:hover {
  background: #e67e22;
}

.wg-dg-body .value-table .content .col {
  width: 300px;
  text-align: center;
  border-right: 1px solid #808080;
}

.wg-dg-body .value-table .content .table-col {
  display: flex;
  align-items: center;
  justify-content: center;
}

.wg-dg-body .value-table .content .table-col span {
  font-family: pvn;
  letter-spacing: .5px;
}

.wg-dg-body .value-table .content .body .row {
  border-bottom: 1px solid #808080;
  position: relative;
}

.wg-dg-body .value-table .content .body .row:last-child {
  border-bottom: none;
}

.wg-dg-body .value-table .content .child-div-flex > div {
  height: 20px;
  box-sizing: border-box;
  display: flex;
}

.wg-dg-body .value-table .content .child-div-flex > div:first-child {
  height: 22px;
  align-items: center;
  justify-content: center;
  font-size: 11px;
  font-weight: bold;
  color: #565656;
  border-bottom: 1px solid #808080;
}

.wg-dg-body .value-table .content .minute-col {
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: 1px solid #bfbfbf;
  border-bottom: none;
  font-size: 10px;
  font-weight: bold;
  color: #949494;
}

.wg-dg-body .value-table .content .minute-col:last-child {
  border: none;
}

.wg-dg-body .value-table .content .activitie-col {
  width: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: 1px solid #bfbfbf;
  border-bottom: none;
  font-size: 11px;
  font-weight: bold;
  color: #272727;
}

.wg-dg-body .value-table .content .activitie-col:last-child {
  border: none;
}


.wg-dg-body .value-table .body .minute-col {
  background-color: #ffffff;
  border-color: #9a9a9a;
  position: relative;
}

.wg-dg-body .value-table .body .minute-col[data-status="work-time"] {
  background-color: #a9d08e;
}

.wg-dg-body .value-table .body .minute-col .selector {
  position: absolute;
  width: 24px; height: 25px;
  border: 1px solid #00d8ff;
  left: -1px; right: 0;
  background: rgba(0,216,255, .3);
  cursor: pointer;
}

.wg-dg-body .value-table .body .minute-col .selector[data-type="start"] {
  border-right: none;
}

.wg-dg-body .value-table .body .minute-col .selector[data-type="middle"] {
  width: 25px;
  border-right: none;
  border-left: none;
}

.wg-dg-body .value-table .body .minute-col .selector[data-type="end"] {
  border-left: none;
  left: 0;
}

.move-bh-selector {
  width: 6px; height: 6px;
  position: absolute;
  top: 50%; right: -3px;
  transform: translateY(-50%);
  background: rgb(0,216,255);
  visibility: hidden;
}

.wg-dg-body .hours-summary .content .row {
  display: flex;
  width: 100%; height: 25px;
  text-align: center;
  border-bottom: 1px solid #808080;
}

.wg-dg-body .hours-summary .content .row:last-child {
  border-bottom: none;
}

.wg-dg-body .hours-summary .content .col {
  width: 33%;
  text-align: center;
}

.wg-dg-body .hours-summary .content .row .col {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: center;
  font-size: 11px;
  font-weight: bold;
  color: #0f6299;
  letter-spacing: .3px;
}

.wg-dg-body .hours-summary .content .precent-wrapper {
  display: flex;
  width: 100%; height: 15px;
  box-sizing: border-box;
  padding: 0 5px;
  border-radius: 2px;
  background: #bdc3c7;
  align-items: center;
  justify-content: center;
  position: relative;
}

.wg-dg-body .hours-summary .content .precent-wrapper .value {
  width: 0%; height: 100%;
  top: 0; left: 0;
  position: absolute;
  z-index: 1;
  border-radius: 2px;
}

.wg-dg-body .hours-summary .content .precent-wrapper span {
  display: blocl;
  color: #272727;
  position: relative;
  z-index: 2;
}

.wg-dg-body .hours-summary .content .head .col span {
  display: block;
  font-family: pvn;
  font-weight: bold;
  font-size: .83em;
  letter-spacing: .5px;
  color: #808080;
  margin-top: 12px;
}

.wg-dg-body .content .body {
  width: 100%; min-height: 25px;
  border-bottom: 2px solid #0f6299;
}

div[data-status="very-weak"] .value {
  background: #FFD54F;
}

div[data-status="weak"] .value {
  background: #FFCA28;
}

div[data-status="over-weak"] .value {
  background: #FFC107;
}

div[data-status="half"] .value {
  background: #FFB300;
}

div[data-status="before-strong"] .value {
  background: #FFA000;
}

div[data-status="strong"] .value {
  background: #FF8F00;
}

div[data-status="over-strong"] .value {
  background: #FF6F00;
}

div[data-status="exact"] .value {
  background: #008237;
}

div[data-status="over"] .value {
  background: #c0392b;
}

div[data-status="exact"] span, div[data-status="over"] span {
  color: white !important;
}

.wg-dg-body .scroll {  /* body - scroll */
  width: 100%; height: 7px;
  overflow: hidden;
  overflow-x: auto;
  margin-top: 6px;
}

.wg-dg-body .scroll .maker {
  width: 7200px; height: 7px;
}

.wg-dg-body .footer {  /* body - footer */
  width: 100%; height: 50px;
  margin-top: 6px;
  position: relative;
}

.wg-dg-body .hidden {
  visibility: hidden;
}

.wg-dg-body .footer .row {
  width: 100%; height: 25px;
}

.wg-dg-body .footer .row.is-out {
  color: #0f629a;
}

.wg-dg-body .footer .row.on-place {
  color: #808080;
}

.wg-dg-body .footer .col {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: flex-end;
  box-sizing: border-box;
  padding-right: 8px;
}

.wg-dg-body .footer .col {
  font-size: 11px;
  font-weight: bold;
  letter-spacing: .3px;
}

.wg-dg-body .footer .col span:first-child {
  position: relative;
  left: -5px;
}

.wg-dg-body .slider { /* body - slider */
  width: 7200px; height: auto;
  position: relative;
  left: 0;
}

.wg-dg-body .slider .row {
  display: flex;
  width: 100%; height: 25px;
}

.wg-dg-footer {  /* footer */
  width: 100%;
  padding: 15px 0;
  border-top: 1px solid #dcdde1;
  box-sizing: border-box;
}

.wg-dg-footer .status-description-list {
  list-style: none;
  margin-left: 0;
  padding-left: 10px;
}

.wg-dg-footer .status-description-list label {
  font-size: .86em;
  font-weight: bold;
  color: #272727;
  letter-spacing: .3px;
  margin-right: 9px;
}

.wg-dg-footer .status-description-list li {
  display: inline-block;
  margin-top: 4px;
  text-align: center;
  align-items: center;
  justify-content: center;
}

.status-description-list .exact {
  color: #026b3b;
  font-weight: bold;
}

.status-description-list .less {
  color: #9b2c03;
  font-weight: bold;
}

.status-description-list .over {
  background: #026b3b;
  color: #ffffff;
}

.status-description-list span {
  display: block;
  font-size: 11px;
  padding: 5px 10px 10px 10px;
  letter-spacing: .3px;
}

/*RUSH HOURS GRAPHIC STYLES*/
.rush-hour-header {     /*header*/
  width: 100%; height: auto;
  padding: 10px;
  box-sizing: border-box;
}

.rush-hour-header .row {
  display: flex;
  width: 100%; height: auto;
  padding: 5px 0;
  margin-bottom: 4px;
  box-sizing: border-box;
  justify-content: flex-start;
  align-items: center;
}

.rush-hour-header .row:last-child {
  margin-bottom: 0;
}

.rush-hour-header .row .col {
  width: auto; height: 100%;
  margin-left: 10px;
}

.rush-hour-header .row .col:first-child {
  margin-left: 0;
}

.rush-hour-header .row .col label {
  font-size: .86em;
  font-weight: bold;
  color: #272727;
  letter-spacing: .3px;
  margin-bottom: 3px;
  display: block;
}

.rush-hour-header .row .col select {
  height: 26px;
}

#chooseRushHourYear {
  width: 110px;
}

#chooseRushHourWeek {
  width: 170px;
}

.rush-hour-header button {
  display: flex;
  width: 26px; height: 26px;
  background: #f0f0f0;
  font-family: pvn;
  font-size: 11px;
  font-weight: bold;
  color: #272727;
  letter-spacing: .5px;
  border: 1px solid #aaaaaa;
  border-radius: 4px;
  align-items: center;
  justify-content: center;
  margin-top: 18px;
  transition: background .1s ease;
}

.rush-hour-header button:hover {
  background: #e8e7e7;
}

.rush-hour-header button svg {
  font-size: 13px;
  font-weight: bold;
  color: #272727;
}

.rush-hour-body {   /*body*/
  display: grid;
  width: 100%; height: auto;
  padding: 10px;
  margin-top: 30px;
  grid-template-columns: 165px auto;
  box-sizing: border-box;
}

.rush-hour-body .value-table {
  box-sizing: border-box;
  overflow: hidden;
}
table {
	font-size: 1.3em
}
.rush-hour-body .content {  /* body - content */
  width: 100%; height: auto;
  position: relative;
  box-sizing: border-box;
}

.rush-hour-body .content .right-boder {
  width: 1px; height: 100%;
  position: absolute;
  top: 0; right: 0;
  z-index: 10;
  background: #808080;
}

.rush-hour-body .days .content {
  border-right: 1px solid #808080;
}

.rush-hour-body .content .head {
  display: flex;
  width: 100%; height: 45px;
  border-bottom: 2px solid #0f6299;
  box-sizing: border-box;
}

.rush-hour-body .value-table .content .head {
  border-top: 1px solid #808080;
}

.rush-hour-body .days .content .col {
  box-sizing: border-box;
  text-align: center;
  padding: 0 5px;
}

.rush-hour-body .days .content .col:first-child {
  width: 50px;
}

.rush-hour-body .days .content .flex-width {
  flex: 1;
  text-align: left;
  padding: 0;
  padding-left: 5px;
}

.rush-hour-body .days .content .head .col span {
  display: block;
  font-family: pvn;
  font-weight: bold;
  font-size: .83em;
  letter-spacing: .5px;
  color: #0f6299;
  margin-top: 25px;
}

.rush-hour-body .days .content .head .col button {
  width: 100%; height: 20px;
  background: #f39c12;
  font-family: pvn;
  font-weight: bold;
  font-size: .75em;
  letter-spacing: .5px;
  color: #ecf0f1;
  margin-top: 16px;
  transition: background .1s ease;
}

.rush-hour-body .days .body .row {
  display: flex;
  width: 100%; height: 25px;
  border-bottom: 1px solid #808080;
  align-items: center;
  padding-left: 5px;
  position: relative;
}

.rush-hour-body .days .body .row span {
  font-family: pvn;
  font-weight: bold;
  font-size: 10.7px;
  color: #272727;
  letter-spacing: .5px;
}

.rush-hour-body .days .body .row:last-child {
  border-bottom: none;
}

.rush-hour-body .value-table .content .col {
  width: 300px;
  text-align: center;
  border-right: 1px solid #808080;
}

.rush-hour-body .value-table .content .table-col {
  display: flex;
  align-items: center;
  justify-content: center;
}

.rush-hour-body .value-table .content .table-col span {
  font-family: pvn;
  letter-spacing: .5px;
}

.rush-hour-body .value-table .content .body .row {
  border-bottom: 1px solid #808080;
  position: relative;
}

.rush-hour-body .value-table .content .body .row:last-child {
  border-bottom: none;
}

.rush-hour-body .value-table .content .child-div-flex > div {
  height: 20px;
  box-sizing: border-box;
  display: flex;
}

.rush-hour-body .value-table .content .child-div-flex > div:first-child {
  height: 22px;
  align-items: center;
  justify-content: center;
  font-size: 11px;
  font-weight: bold;
  color: #565656;
  border-bottom: 1px solid #808080;
}

.rush-hour-body .value-table .content .minute-col {
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: 1px solid #bfbfbf;
  border-bottom: none;
  font-size: 10px;
  font-weight: bold;
  color: #949494;
}

.rush-hour-body .value-table .content .minute-col:last-child {
  border: none;
}

.rush-hour-body .value-table .content .activitie-col {
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: 1px solid #bfbfbf;
  border-bottom: none;
  font-size: 11px;
  font-weight: bold;
  color: #272727;
}

.rush-hour-body .value-table .content .activitie-col:last-child {
  border: none;
}


.rush-hour-body .value-table .body .minute-col {
  background-color: #ffffff;
  border-color: #9a9a9a;
  position: relative;
}

.rush-hour-body .value-table .body .minute-col[data-status="selected"] {
  background-color: #a9d08e;
}

.rush-hour-body .value-table .body .minute-col .selector {
  position: absolute;
  width: 24px; height: 25px;
  border: 1px solid #00d8ff;
  left: -1px; right: 0;
  background: rgba(0,216,255, .3);
  cursor: pointer;
}

.rush-hour-body .value-table .body .minute-col .selector[data-type="start"] {
  border-right: none;
}

.rush-hour-body .value-table .body .minute-col .selector[data-type="middle"] {
  width: 25px;
  border-right: none;
  border-left: none;
}

.rush-hour-body .value-table .body .minute-col .selector[data-type="end"] {
  border-left: none;
  left: 0;
}

.move-bh-selector {
  width: 6px; height: 6px;
  position: absolute;
  top: 50%; right: -3px;
  transform: translateY(-50%);
  background: rgb(0,216,255);
  visibility: hidden;
}

.rush-hour-body .hours-summary .content .row {
  display: flex;
  width: 100%; height: 25px;
  text-align: center;
  border-bottom: 1px solid #808080;
}

.rush-hour-body .hours-summary .content .row:last-child {
  border-bottom: none;
}

.rush-hour-body .hours-summary .content .col {
  width: 33%;
  text-align: center;
}

.rush-hour-body .hours-summary .content .row .col {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: center;
  font-size: 11px;
  font-weight: bold;
  color: #0f6299;
  letter-spacing: .3px;
}

.rush-hour-body .hours-summary .content .precent-wrapper {
  display: flex;
  width: 100%; height: 15px;
  box-sizing: border-box;
  padding: 0 5px;
  border-radius: 2px;
  background: #bdc3c7;
  align-items: center;
  justify-content: center;
  position: relative;
}

.rush-hour-body .hours-summary .content .precent-wrapper .value {
  width: 0%; height: 100%;
  top: 0; left: 0;
  position: absolute;
  z-index: 1;
  border-radius: 2px;
}

.rush-hour-body .hours-summary .content .precent-wrapper span {
  display: blocl;
  color: #272727;
  position: relative;
  z-index: 2;
}

.rush-hour-body .hours-summary .content .head .col span {
  display: block;
  font-family: pvn;
  font-weight: bold;
  font-size: .83em;
  letter-spacing: .5px;
  color: #808080;
  margin-top: 12px;
}

.rush-hour-body .content .body {
  width: 100%; min-height: 25px;
  border-bottom: 2px solid #0f6299;
}

.rush-hour-row-clearer {
  display: flex;
  width: 26px; height: 80%;
  position: absolute;
  right: 10px;
  background: #3498db;
  border-radius: 2px;
  align-items: center;
  justify-content: center;
  transition: background .1s ease;
}

.rush-hour-row-clearer:hover {
  background: #2980b9;
}

.rush-hour-row-clearer svg {
  font-size: 13px;
  color: #ecf0f1;
}

div[data-status="very-weak"] .value {
  background: #FFD54F;
}

div[data-status="weak"] .value {
  background: #FFCA28;
}

div[data-status="over-weak"] .value {
  background: #FFC107;
}

div[data-status="half"] .value {
  background: #FFB300;
}

div[data-status="before-strong"] .value {
  background: #FFA000;
}

div[data-status="strong"] .value {
  background: #FF8F00;
}

div[data-status="over-strong"] .value {
  background: #FF6F00;
}

div[data-status="exact"] .value {
  background: #008237;
}

div[data-status="over"] .value {
  background: #c0392b;
}

div[data-status="exact"] span, div[data-status="over"] span {
  color: white !important;
}

.rush-hour-body .scroll {  /* body - scroll */
  width: 100%; height: 7px;
  overflow: hidden;
  overflow-x: auto;
  margin-top: 6px;
}

.rush-hour-body .scroll .maker {
  width: 7200px; height: 7px;
}

.rush-hour-body .footer {  /* body - footer */
  width: 100%; height: 50px;
  margin-top: 6px;
  position: relative;
}

.rush-hour-body .hidden {
  visibility: hidden;
}

.rush-hour-body .footer .row {
  width: 100%; height: 25px;
}

.rush-hour-body .footer .row.is-out {
  color: #0f629a;
}

.rush-hour-body .footer .row.on-place {
  color: #808080;
}

.rush-hour-body .footer .col {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: flex-end;
  box-sizing: border-box;
  padding-right: 8px;
}

.rush-hour-body .footer .col {
  font-size: 11px;
  font-weight: bold;
  letter-spacing: .3px;
}

.rush-hour-body .footer .col span:first-child {
  position: relative;
  left: -5px;
}

.rush-hour-body .value-table .status .col[data-status="all-in"] {
  background: #026b3b;
  color: white;
}

.rush-hour-body .value-table .status .col[data-status="more-in"] {
  background: transparent;
  color: #026b3b;
}

.rush-hour-body .value-table .status .col[data-status="in-out"] {
  background: transparent;
  color: #c00000;
}

.rush-hour-body .value-table .status .col[data-status="more-out"] {
  background: #ef8a0a;
  color: white;
}

.rush-hour-body .value-table .status .col[data-status="all-out"] {
  background: #c00000;
  color: white;
}

.rush-hour-body .slider { /* body - slider */
  width: 7200px; height: auto;
  position: relative;
  left: 0;
}

.rush-hour-body .slider .row {
  display: flex;
  width: 100%; height: 25px;
}

#chooseRushHourWeek_chosen li {
  font-family: pvn;
  font-weight: bold;
  font-size: 11px;
  color: #272727;
  letter-spacing: .5px;
}

#chooseRushHourWeek_chosen li:not(:last-child) {
  border-bottom: 1px solid #dcdcdc;
}

.rush-hour-footer { /*footer*/
  width: 100%; height: 30px;
  padding: 0 10px;
  box-sizing: border-box;
  margin-top: 13px;
}

.rhs-button-holder {
  width: auto; height: 100%;
  float: right;
}

.rhs-button-holder button {
  height: 100%;
  padding: 0 11px;
  padding-top: 1px;
  background: #159f99;
  border-radius: 3px;
  font-family: pvn;
  font-weight: bold;
  font-size: 11px;
  color: #ecf0f1;
  letter-spacing: .5px;
  transition: background .1s ease;
}

.rhs-button-holder button:hover {
  background: #1a8580;
}

.rhs-button-holder button[disabled] {
  background: #dadddf;
  color: #95a5a6;
}

/*confirmer p*/
.confirm-p {
  width: 100%; height: auto;
  padding: 15px 0;
  text-align: center;
  line-height: 18px;
}

/*footer logo*/
#footerLogo {
  margin-left: 80px;
}

/*save button style*/
.save-button {
  background: #2376C8 !important;
  border: 1px solid #2376C8 !important;
  color: #F2F2F2 !important;
  transition: .1s ease;
}

.save-button:hover {
  background: #03967D !important;
  border: 1px solid #03967D !important;
}

#testerman {
  font-family: testuna;
}

/*REPORT STYLES*/
.wfm-report-button-area  { /*====================== button area =====================*/
  display: flex;
  min-height: 50px;
}

.wfm-report-button-area input {
  height: 25px;
  border: 1px solid #aaa;
  padding: 0 7px;
  font-size: 12px;
}

.wfm-report-filter-column {
  margin-left: 10px;
}

.wfm-report-filter-column:first-child {
  margin-left: 0;
}

.wfm-report-content-wrapper { /*====================== content wrapper =====================*/
  display: flex;
    width: 100%;
    min-height: 30px;
    margin-top: 22px;
    flex-direction: column;
}

/*table head*/
.wfm-report-table-head {
  display: grid;
  width: 70%; height: 40px;
  grid-template-columns: 20% repeat(7, 20%);
}

.operator-table-head {
  display: grid;
  width: 42%; height: 40px;
  grid-template-columns: 20% repeat(12, 20%);
}

.operator-report-row {
  display: grid;
  width: 42%; height: 26px;
  grid-template-columns: 20% repeat(12, 20%);
}

.wfm-report-table-head .wfm-report-col {
  background: #E6F2F8;
  border-top: 1px solid #A3D0E4;
}

.wfm-report-table-head span {
  font-family: pvn;
  font-size: 11px;
  font-weight: bold;
  letter-spacing: .3px;
}

/*table body*/
.wfm-report-table-body {
  width: 100%; height: auto;
}

.wfm-report-row {
  display: grid;
  width: 70%; height: 26px;
  grid-template-columns: 20% repeat(7, 20%);
}

.wfm-report-col {
  height: 100%;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  white-space: nowrap;
  text-overflow: ellipsis;
  border: 1px solid #A3D0E4;
  border-right: none;
  border-top: none;
}

.wfm-report-col:last-child {
  border-right: 1px solid #A3D0E4;
}

.wfm-rc-center {
  justify-content: center;
  text-align: center;
  border-color: #A3D0E4;
}

.wfm-rc-lefter {
  justify-content: flex-start;
  padding-left: 5px;
}

.wfm-rc-relative {
  position: relative;
}

.wfm-report-col span {
  font-size: 11.6px;
}

.wfm-report-activitie-wrapper {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
}

.wfm-report-activitie-color {
  width: 10px; height: 10px;
}

.wfm-report-activitie-name {
  margin-left: 3px;
}

.wfm-report-activitie-name span {
  position: relative;
  top: 1px;
}

.wfm-report-sort {
  display: flex;
  width: 20px; height: 100%;
  position: absolute;
  right: 0;
  align-items: center;
  justify-content: center;
}

.wfm-report-sort i {
  color: #A3D0E4;
}

.wfm-report-sort:not(.active):hover i {
  color: #71b1ce;
}

.wfm-report-sort.active i {
  color: #e67e22;
}

/*empty row*/
.wfm-report-empty-row {
  width: 100%; height: 26px;
  border: 1px solid #A3D0E4;
  border-top: none;
  box-sizing: border-box;
}

.wfm-report-empty-col {
  display: flex;
  width: 100%; height: 100%;
  align-items: center;
  justify-content: center;
}

.wfm-report-empty-col span {
  font-size: 11.6px;
}

/*diagram*/
.wfm-report-diagram {
  /* background: green; */
}
button{
  background: #fef400;
}


table td[class*=col-], table th[class*=col-] {
    position: relative;
	text-align: center
}

</style>
  
<!-- high charts -->
<script src="js/highcharts/highcharts.js"></script>

<script type="text/javascript">
var aJaxURL = "server-side/info/wfm.action.php";
var $G      = {
  projectPageIsOpen: ""
};
var projectDialogDetails = "";
var projectDetails = {
  originalId: 0,
  savedData: {
    name: "",
    slType: -1,
    slPercent: -1,
    slSecondIn: -1,
    slSecondFrom: -1,
    maxHourPerMonth: -1,
    workShiftMaxHours: -1
  }
}

let workDayGraphic = {};

/*NOTIFICATION STARTS*/
;(function(global) {

 // define notifications
  global.note = (msgText, callback) => {

    // define dialog buttons
    let buttons = {
        "save": {
            text: "დადასტურება",
            id: "confirm-confirmer-dialog",
            click: function () {

                // execute confirmed function
                callback();

                // close dialog
                $(this).dialog("close");
            }
        },
        "cancel": {
            text: "უარყოფა",
            id: "undo-confirmer-dialog",
            click: function () {

                // close dialog
                $(this).dialog("close");

              }
            }
        };

    // insert message text in dialog
    $("#confirm-dialog p").text(msgText);

    // open dialog
    GetDialog("confirm-dialog", 400, "auto", buttons, `center cneter-100`);

    // set dialog and dialog back holder
    $(`.confirm-dialog-class`).css({zIndex: "101"});
    $(`.ui-widget-overlay`).css({zIndex: "100"});

  };

}(window));
/*NOTIFICATION ENDS*/

/*HOURS CALCULATION METHODS STARTS*/
;(function(global) {

  // define hours mane class
  class Hours {

    // returns minutes in hours
    inSeconds(stringHours) {

      // hours data
      let _Hours = stringHours.split(":");
      let hour = isNaN(parseInt(_Hours[0])) ? 0 : parseInt(_Hours[0]);
      let minute = isNaN(parseInt(_Hours[1])) ? 0 : parseInt(_Hours[1]);
      let second = _Hours[2] ? isNaN(parseInt(_Hours[2])) ? 0 : parseInt(_Hours[2]) : 0;

      // convert hours and minutes to seconds
      second += hour * 3600;
      second += minute * 60;

      // return second summary
      return second;

    }

    // add two hours
    add(a, b) {

      /* methos expects that hour parameters will be strings...
      because of that, we need to split hours */

      // first hours data
      let hoursA = a.split(":");
      let aHour = isNaN(parseInt(hoursA[0])) ? 0 : parseInt(hoursA[0]);
      let aMinute = isNaN(parseInt(hoursA[1])) ? 0 : parseInt(hoursA[1]);
      let aSecond = hoursA[3] ? isNaN(parseInt(hoursA[3])) ? 0 : parseInt(hoursA[3]) : 0;

      // second hours data
      let hoursB = b.split(":");
      let bHour = isNaN(parseInt(hoursB[0])) ? 0 : parseInt(hoursB[0]);
      let bMinute = isNaN(parseInt(hoursB[1])) ? 0 : parseInt(hoursB[1]);
      let bSecond = hoursB[3] ? isNaN(parseInt(hoursA[3])) ? 0 : parseInt(hoursA[3]) : 0;

      // add two data
      let addHours = aHour + bHour;
      let addMinutes = aMinute + bMinute;
      let addSeconds = aSecond + bSecond;

      // checking for seconds
      if(addSeconds >= 60) {
        addMinutes += parseInt(addSeconds / 60);
        addSeconds %= 60;
      }

      // checking for minutes
      if(addMinutes >= 60) {
        addHours += parseInt(addMinutes / 60);
        addMinutes %= 60;
      }

      let hour = zeroFixer(addHours);
      let minute = zeroFixer(addMinutes);
      let second = zeroFixer(addSeconds);

      // return end result
      return `${hour}:${minute}${second !== "00" ? `:${second}` : ''}`;

    }

    // subtract two hours
    subtract(a, b) {

      /* methos expects that hour parameters will be strings...
      because of that, we need to split hours */

      // first hours data
      let aTimeInSeconds = this.inSeconds(a);

      // second hours data
      let bTimeInSeconds = this.inSeconds(b);

      // subtract seconds
      let subTimeInSeconds = aTimeInSeconds - bTimeInSeconds;

      // get hours minutes and seconds
      let resHour = Math.floor(subTimeInSeconds / 3600);
      let resMinut = Math.floor(subTimeInSeconds / 60);
      let resSecond = subTimeInSeconds - (resHour * 3600) + (resMinut * 60);

      // check for different cases
      if(resMinut >= 60 || resMinut <= -60) {
        resMinut %= 60;
      }

      if(resSecond >= 60 || resSecond <= -60) {
        resSecond %= 60;
      }

      if(resMinut < 0 && resSecond < 0) {
        resMinut += 1;
        resSecond = -resSecond;
      } else if(resSecond < 0) {
        resSecond = -resSecond;
      }

      if(resHour < 0 && resMinut < 0) {
        resHour += 1;
        resMinut = -resMinut;
      } else if(resMinut < 0) {
        resMinut = -resMinut;
      }

      let hour = zeroFixer(resHour);
      let minute = zeroFixer(resMinut);
      let second = zeroFixer(resSecond);

      // return end result
      return `${hour}:${minute}${second !== "00" ? `:${second}` : ''}`;

    }

    // checks difference between two hours
    isGreater(g, l) {

      /* methos expects that hour parameters will be strings...
      first parameter must be greater option, second - less */

      // define greater hour data
      let greaterHour = g.split(":");
      let gHour = minusFixer(parseInt(greaterHour[0]));
      let gMin = minusFixer(parseInt(greaterHour[1]));
      let gSec = minusFixer(greaterHour[2] ? parseInt(greaterHour[2]) : false);

      // define less hour data
      let lessHour = l.split(":");
      let lHour = minusFixer(parseInt(lessHour[0]));
      let lMin = minusFixer(parseInt(lessHour[1]));
      let lSec = minusFixer(lessHour[2] ? parseInt(lessHour[2]) : false);

      // compare
      if((gHour < lHour) || (gHour === lHour && gMin < lMin) || (gHour === lHour && gMin === lMin && (gSec && lSec && gSec < lSec))) {
        return false;
      } else {
        return true;
      }

    }

    // convert hours to num
    convertToNum(mindivide, time) {

      // process time
      let divider = 60 / mindivide;
      let splitedTime = time.split(":");
      let hour = parseInt(splitedTime[0]) * divider;
      let minute = (divider / 60) * parseInt(splitedTime[1]);

      // return result
      return hour + minute;

    }

    // convert num to hours
    convertFromNum(minmultiple, num) {

      // process num
      let sumMinute = minmultiple * num;
      let hours = zeroFixer(parseInt(sumMinute / 60));
      let minutes = zeroFixer(sumMinute % 60);

      // return result
      return `${hours}:${minutes}`;

    }

    // convert seconds to number
    secondsToNum(mindivide, seconds) {
      return (seconds / 60) / mindivide;
    }

    // convert number to seconds
    secondsFromNum(minmultiple, num) {
      return (minmultiple * 60) * num;
    }

  }

  // export class
  global.hours = new Hours();

}(window));
/*HOURS CALCULATION METHODS ENDS*/

/*========================================================================================= FUNCTIONS ========================================================================================*/

// color calculator
function colorCalculator(value) {

  // define red, green and blue color values
  let redBlue = 255;
  let green = 255;
  let multiplyValue = 15;
  let redBlueLimiter = parseInt(255 / multiplyValue);

  // checking for red and blue
  if(value <= redBlueLimiter) {
    redBlue -= value * multiplyValue;
  } else {
    redBlue = 0;
  }

  // checking for green
  if(value > redBlueLimiter) {

    // define subtract value
    let subtractValue = parseInt((value * multiplyValue) - 255);
    green -= subtractValue;

    if(green < 50) {
      green = 50;
    }

  }

  // return value
  return `rgb(${redBlue}, ${green}, ${redBlue})`;

}

// colorize hours table
function colorizeHoursTable() {

  // define loop body
  let loopBody = $("#work_table tbody");

  // loop throw rows
  loopBody.find("tr").each(function() {

    // loop throw value holder containers
    $(this).find("td").each(function() {

      // define value
      let value = $(this).text().trim() === '' ? 0 : parseInt($(this).text().trim());

      // define color
      let rgbColor = colorCalculator(value);
      $(this).css({background: rgbColor});

    });

  });

}

// check if all checkboxes are checked
function checkAllCheckBoxes(allcheckboxes, parentcheckbox) {

  // define status variable
  let allChecked = true;

  // all checkboxes must be selector name
  $(allcheckboxes).each(function() {

    // define each checkbox checked status
    let boxIsChecked = $(this).is(":checked");

    if(!boxIsChecked) {
      allChecked = false;
    }

  });

  // check for all checked status
  if(allChecked) {
    $(parentcheckbox).prop("checked", true);
  } else {
    $(parentcheckbox).prop("checked", false);
  }


}

function loadHoursAfterAdd() {

  // define project id
  let { originalId } = projectDetails;

	$('#work_table td').css('background', '#fff');
	$.ajax({
        url: aJaxURL,
        data: `act=get_wk_add&project_id=${originalId}`,
        success: function(data) {
    		if(typeof(data.error) != "undefined"){
    			if(data.error != ""){
    				alert(data.error);
    			}else{
    				$("td[check_clock]").html('');


    				for(g=0;g < $(data.work).size();g++){
    					for(i=(parseInt(data.work[g].starttime)+15);i <= parseInt(data.work[g].endtime);i+=15){

    						switch(i) {
    						case 60:
        		    	    	i+=40;
        		    	        break;
    						case 160:
        		    	    	i+=40;
        		    	        break;
    						case 260:
        		    	    	i+=40;
        		    	        break;
    						case 360:
        		    	    	i+=40;
        		    	        break;
    						case 460:
        		    	    	i+=40;
        		    	        break;
    						case 560:
        		    	    	i+=40;
        		    	        break;
    						case 660:
        		    	    	i+=40;
        		    	        break;
    						case 760:
        		    	    	i+=40;
        		    	        break;
    						case 860:
        		    	    	i+=40;
        		    	        break;
    						case 960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1360:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1460:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1560:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1660:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1760:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1860:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2360:
        		    	    	i+=40;
        		    	        break;
        		    	    }
    						if(i<9){
    							er='000'+i;
    						}else if(i<99){
        						er='00'+i;
        					}else if(i<999){
                				er='0'+i;
        					}else{
            					er=i;
            				}

                  // define value holder block
                  let valueHolder = $(`td[clock='${er}'][wday='${data.work[g].wday}']`);

                  // define integer value
      						intvar = parseInt(valueHolder.text(),10);

                  // check if value exists
      						if(!isNaN(intvar)){
     		    				   valueHolder.text(intvar + 1);
    		    			}else{
          						valueHolder.text(1);
    		    			}

    		    		}

    				}
    				for(o=0;o < $(data.break).size();o++){
    					for(i=parseInt(data.break[o].breakstarttime);i < parseInt(data.break[o].breakendtime);i+=5){
    						if(i<99){er='00'+i;}else{er=i;}
    						if(i<999){er='0'+i;}else{er=i;}
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").css('background','yellow');
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").html('');
    		    		}
    				}
    			}
    		}
        }
    }).done(function() {
    	
      hoursCalculator();
      colorizeHoursTable();

      // build planner table
      workPlanner.build();
     });

}

function loadHours() {

  // define project id
  let projectId = projectDetails.originalId;

	$('#work_table td').css('background', '#fff');
	$.ajax({
        url: aJaxURL,
        data: 'act=get_wk&project_id='+projectId,
        success: function(data) {
    		if(typeof(data.error) != "undefined"){
    			if(data.error != ""){
    				alert(data.error);
    			}else{
            
    				$("td[check_clock]").html('');
            

    				for(g=0;g < $(data.work).size();g++){
    					for(i=(parseInt(data.work[g].starttime)+15);i <= parseInt(data.work[g].endtime);i+=15){

    						switch(i) {
    						case 60:
        		    	    	i+=40;
        		    	        break;
    						case 160:
        		    	    	i+=40;
        		    	        break;
    						case 260:
        		    	    	i+=40;
        		    	        break;
    						case 360:
        		    	    	i+=40;
        		    	        break;
    						case 460:
        		    	    	i+=40;
        		    	        break;
    						case 560:
        		    	    	i+=40;
        		    	        break;
    						case 660:
        		    	    	i+=40;
        		    	        break;
    						case 760:
        		    	    	i+=40;
        		    	        break;
    						case 860:
        		    	    	i+=40;
        		    	        break;
    						case 960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1360:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1460:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1560:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1660:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1760:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1860:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2360:
        		    	    	i+=40;
        		    	        break;
        		    	    }
    						if(i<9){
    							er='000'+i;
    						}else if(i<99){
        						er='00'+i;
        					}else if(i<999){
                				er='0'+i;
        					}else{
            					er=i;
            				}

                    // define value holder block
                    let valueHolder = $(`td[clock='${er}'][wday='${data.work[g].wday}']`);

                    // define integer value
                    intvar = parseInt(valueHolder.text(),10);

                    // check if value exists
                    if(!isNaN(intvar)){
                         valueHolder.text(intvar + 1);
                    }else{
                        valueHolder.text(1);
                    }

    		    		}

    				}
            
    				for(o=0;o < $(data.break).size();o++){
    					for(i=parseInt(data.break[o].breakstarttime);i <= parseInt(data.break[o].breakendtime);i+=5){
    						if(i<99){er='00'+i;}else{er=i;}
    						if(i<999){er='0'+i;}else{er=i;}
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").css('background','yellow');
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").html('');
    		    		}
    				}
    			}
    		}
        }
    }).done(function() {

      hoursCalculator();
      colorizeHoursTable();

      // build planner table
      workPlanner.build();

        // scroll the hours table
        $(".wfm-wt-footer").scroll(function() {

        // define scroll position
        let leftPos = $(this).scrollLeft();

            if(leftPos > 10) {
                leftPos += 3;
            }

            // change position of the table
            $("#work_table").css({left: `-${leftPos}px`});

        });

     });

}

// build project data by type
function buildProjectDataByType(type) {

  // check for type
  if(type === "graphic") {

    // build work graphic
    workGraphic.build();

  } else if(type === "workHours") {

    // build work hours
    loadHours();

  } else if(type === "rushHours") {

    // build rush hours
    rushHours.build();

  } else if(type === "holidays") {

    // build holidays table
   holiDays.build();

  }

}

// add newproject dialog
function loadAddNewProjectDialog() {

  // send ajax request]'
  $.post(aJaxURL, {act: "get_add_project_form"}, responce => {

    // restruct responce
    const { error, dialog } = responce;
    
    // check for error
    if(error !== "") {
      alert(error);
    } else {

      // define dialog buttons
      let buttons = {
              "save": {
                  text: "შენახვა",
                  id: "save-dialog",
                  class: "save-button",
                  click: function () {

                      // define dialog
                      let self = $(this);

                      saveNewProject(result => {

                          // destruct responsed data
                          let {error, complete, exists} = result;

                          // checking error
                          if(error !== '') {
                              alert(error);
                          } else {

                              // check if olready exists project with same name
                              if(exists) {
                                  alert("მსგავსი დასახელების პროექტი უკვე არსებობს");
                              } else {

                                  // checking complete
                                  if(complete) {
                                      alert("პროექტი წარმატებით შეიქმნა");
                                  } else {
                                      alert("დაფიქსირდა გაურკვეველი შეცდომა");
                                  }

                                  // close dialog
                                  self.dialog("close");

                                  // load project table
                                  loadDirPage("projects");
                              }
                          }

                      });
                  }
              },
              "cancel": {
                  text: "დახურვა",
                  id: "cancel-dialog",
                  click: function () {

                    // close dialog
                    $(this).dialog("close");

                  }
              }
          };

          // insert responsed data in dialog container
          $("#add-new-project").html(dialog);

          // stylize elements
          $(".jquery-ui-chosen").chosen({
            search_contains: true
          }).each(function() {

            // get id and width property
            let id = $(this).attr("id");
            let width = $(this).data("width");

            // define chosen object
            let chosenObj = $(`#${id}_chosen`);

            // set width property
            chosenObj.css({width: "100%"});

          });
          $("#workHoursDuration").timepicker({
                hourMax: 23,
                hourMin: 0,
                hourGrid: 5,
                minuteMax: 55,
                minuteMin: 0,
                minuteGrid: 15,
                stepMinute: 15,
                timeFormat: 'HH:mm'
            });

          // open dialog
          GetDialog("add-new-project", 400, "auto", buttons, `center center`);

    }

  });

}

// load mine dialog
function loadEditProjectDialog() {

  // define project id
  let { originalId, name } = projectDetails;

  // snd ajax request to get dialog content
  $.post(aJaxURL, {act: "get_project_edit_form", projectId: originalId}, result => {

      // checking for responsed error
      if(result.error !== '') {
          alert(result.error);
      } else {

        // stylize and open the dialog
        const {workWidth, workHeight, fixedMenuWidth, dialogSliderWidth} = projectDialogDetails = getProjectDialogDetails();

        // insert responsed data in dialog container
        $("#edit-existed-project").addClass("active").html(result.dialog).css({
          width: `${workWidth-20}px`,
          height: `${workHeight}px`,
          left: `${fixedMenuWidth}px`
        });

        // insert project name in relevant receiver
        $("#editProjectNameReceiver").text(name);

        // stylize elements
        $(".jquery-ui-chosen").chosen({
          search_contains: true
        }).each(function() {

          // get id and width property
          let id = $(this).attr("id");

          // define chosen object
          let chosenObj = $(`#${id}_chosen`);

          // set width property
          chosenObj.css({width: "100%"});

        });
        $("#workHoursDuration").timepicker({
            hourMax: 23,
            hourMin: 0,
            hourGrid: 5,
            minuteMax: 55,
            minuteMin: 0,
            minuteGrid: 15,
            stepMinute: 15,
            timeFormat: 'HH:mm'
        });

        // start loading
        $(".project-details-loading-container").addClass("active");

        // build graphic table
        workGraphic.build();

        // set project details saved data
        setProjectDetailsSavedData();
      }

      $('.wgtc-body').on('scroll', function () {
        $('.wgtc-body').scrollTop($(this).scrollTop());
    });
    $(".choose-work-graphic-operator").chosen();
          $(".choose-work-graphic-cycle").chosen();
  });

}

// get project details data
function getProjectDetailsData() {

    return {
      name: $("#newProjectName").val(),
      slType: $("#newProjectSlTypeList").val(),
      slPercent: $("#npslCallPercent").val(),
      slSecondIn: $("#npslCallAnswered").val(),
      slSecondFrom: $("#npslCallUnanswered").val() || 0,
      maxHourPerMonth: $("#workHoursOperator").val(),
      workShiftMaxHours: $("#workHoursDuration").val()
    }

    }

    // set project details
    function setProjectDetailsSavedData() {

    projectDetails.savedData = {
    ...getProjectDetailsData()
    }

}

// check project details changing
function checkProjectDetailsChanging() {

    // define return status variable
    let status = false;

    // define current data
    const curData = {
    ...getProjectDetailsData()
    }

    // define saved data
    const savedData = {
    ...projectDetails.savedData
    }

    // loop throw current data
    for(let prop in curData) {

    // define current and saved data values
    let curDataValue = curData[prop];
    let savedDataValue = savedData[prop];

    if(curDataValue !== savedDataValue) {
    status = true;
    break;
    }

    }

    // return status value
    return status;

}

// save new project
function saveNewProject(callback) {

    // define project data
    let data = {
        act: "save_new_project",
        ...getProjectDetailsData()
    };

    // define ajax send allow variable
    let allowSend = true;

    // loop trow data object
    for(let prop in data) {
        if(prop === 'name' && (data[prop] === undefined || data[prop] === '')) {
            allowSend = false;
        }
    }

    // check if ajax sending is allowed
    if(allowSend) {

        // send ajax request
        $.post(aJaxURL, data, result => {
            callback(result);
        });

    } else {
        alert("გთხოვთ მიუთითოთ პროექტის დასახელება");
    }

}

// update existed project
function updateExistedProject(callback) {

    // define project data
    let data = {
        act: "update_existed_project",
        projectId: projectDetails.originalId,
        ...getProjectDetailsData()
    };

    // define ajax send allow variable
    let allowSend = true;

    // loop trow data object
    for(let prop in data) {
        if(prop === 'name' && (data[prop] === undefined || data[prop] === '')) {
            allowSend = false;
        }
    }

    // check if ajax sending is allowed
    if(allowSend) {

        // send ajax request
        $.post(aJaxURL, data, result => {
            callback(result);
        });

    } else {
        alert("გთხოვთ მიუთითოთ პროექტის დასახელება");
    }

}

// close edit project details dialog
function closeEditProjectDetailsDialog() {

    // define details change status
    const detailsAreChanged = checkProjectDetailsChanging();

    // check details change status
    if(detailsAreChanged) {

      // define note data
      let noteText = "თქვენ განახორციელეთ ცვლილებები პროექტის დეტალებში, რომელთა შენახვაც არ დაგიდასტურებიათ... დაიხუროს პროექტის ფანჯარა მაინც?";
      
      // execute note function
      note(noteText, () => $("#edit-existed-project").removeClass("active").html(""));

    } else {
      $("#edit-existed-project").removeClass("active").html("");
    }

}

// get working area
function getProjectDialogDetails() {

    // define variables
    const windowWidth = $(window).width();
    const windowHeight = $(window).height();
    const fixedMenuWidth = $("#sidemenu").width();
    const fixedFooterHeight = 40;
    const workWidth = windowWidth - fixedMenuWidth;
    const workHeight = windowHeight;
    const workTableWidth = $("#work_table").outerWidth();
    const dialogMenuLenght = $(".npfc-menu-container ul").find("li").length;
    const dialogSliderWidth = workWidth * dialogMenuLenght;

    // define return object
    return {
        workWidth,
        workHeight,
        fixedMenuWidth,
        fixedFooterHeight,
        workTableWidth,
        dialogSliderWidth
    }

}

// check table body if is empty
function tableBodyCheck(tableBody) {

  // check if list holder body is emptyRow
  if(!tableBody.children().length) {

    let emptyRow = `<tr>
                       <td class="empty-table-td" colspan="4">
                            ჩამონათვალი ცარიელია
                       </td>
                     </tr>`;

    tableBody.html(emptyRow);
  }

}

// load holidays dialog for project
function loadHolidaysDialogForProject(page) {

    // define dialog buttons
    let buttons = {
        "cancel": {
            text: "დახურვა",
            id: "cancel-dialog-holiday-for-project",
            click: function () {
                $(this).dialog("close");
            }
        }
    };

    // insert page data in dialog
    $("#add-edit-form-holidays").html(page);

    // load table
    const tableLoadData = `project_id="${projectDetails.originalId}"`;
    GetDataTable("holiday_for_project_table", aJaxURL, "get_holiday_list_inside_project", 5, tableLoadData, 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");

    setTimeout(() => {

      // remove redunant stuff
      $("#holiday_for_project_table_wrapper").css({marginTop:"20px"}).find(".dataTable_buttons, .ColVis").remove();

      // open dialog
      GetDialog("add-edit-form-holidays", 700, "auto", buttons, "center");

      // set dialog and dialog back holder z position
      $(`.ui-widget-overlay`).css({zIndex: "100"});
      $(`.ui-dialog`).css({zIndex: "101"});

    }, 50);

}

// responce handler
function responceErrorHandler(responce, callback) {

  // restruct error from responce
  let { error } = responce;

  // check for error
  if(error !== '') {
    alert(error);
  } else {
    if(callback) callback(responce);
  }

}

// reactivate between similar objects
const reactivateSimilars = (allObjects, activeObject, activeClass) => {
  $(allObjects).removeClass(activeClass);
  $(activeObject).addClass(activeClass);
};

// returns load table data depended on directory type
const getLoadTableData = pageType => {

  switch(pageType) {
    case 'projects':
      return {
        tableName: "table_project",
        action: "get_project_list",
        columnCount: 11,
        addButton: "add_button",
        deleteButton: "delete_button"
      };
    case 'holidays':
      return {
          tableName: "holiday_table",
          action: "get_holiday_list",
          columnCount: 4,
          addButton: "add_button_new_holiday",
          deleteButton: "delete_button_exist_holiday"
        };
    case 'cycles':
      return {
          tableName: "work_cycle_table",
          action: "get_cycle_list",
          columnCount: 6,
          addButton: "add_button_cycle",
          deleteButton: "delete_button_cycle"
        };
    case 'shifts':
      return {
        tableName: "work_Shift_table",
        action: "get_work_shift_list",
        columnCount: 9,
        addButton: "add_button_work_shift",
        deleteButton: "delete_button_work_shift"
      };
    case 'activities':
      return {
          tableName: "work_activities_table",
          action: "get_activities_table_list",
          columnCount: 5,
          addButton: "add_button_activities",
          deleteButton: "delete_button_activities"
        };
    case 'report':
      return {
          tableName: "report",
          action: "skipLoadTable",
          columnCount: 0,
          addButton: "",
          deleteButton: ""
        };
  }

};

// load directory page
const loadDirPage = pageType => {

  // define request data
  const data = {
    act: "get_dir_page",
    pageType
  }

  // send ajax request
  $.ajax({
    type: "POST",
    url: aJaxURL,
    cache: false,
    data,
    beforeSend() {
      
        // start loading
        $(".dpw-loading-container").addClass('active');

    },
    success(responce) {

      // restruct responce
      const { error, dirPage } = responce;

      // check for error
      if(error !== "") {
          alert(error);
      } else {
        
          // insert directory page structure in relevant receiver
          $("#dirReceiver").html(dirPage);

          // define table load data
          const { tableName, action, columnCount, addButton, deleteButton } = getLoadTableData(pageType);
          
          // check for table name
          if(action !== "skipLoadTable") {
            
            // define table load more data
            const url = "server-side/info/wfm.action.php";
            const changeColumnMain = "<'dataTable_buttons'T><'F'Cfipl>";

            // load and stylize table
            GetButtons(addButton, deleteButton);
            GetDataTable(tableName, url, action, columnCount, "", 0, "", 1, "desc", "", changeColumnMain);
          } else if(tableName === "report") {

            // build reports table
            report.customize().build();
            $("#button-filter").button();
          }
          
          // reload graphic data
          workGraphic.build();

      }

    },
    complete() {

          // end loading
          setTimeout(() => {
            $(".dpw-loading-container").removeClass('active');  
          }, 1000);
        
    }
  });

  };

$(document).ready(function () {
  loadDirPage("projects");

});

$(document).on("click", "#24st", function () {
	if ($(this).is(':checked')) {
		$('#start_time').val('00:00');
		$('#end_time').val('24:00');
		$("#start_time").prop('disabled', true);
		$("#end_time").prop('disabled', true);
	}else{
		$('#start_time').val('');
		$('#end_time').val('');
		$("#start_time").prop('disabled', false);
		$("#end_time").prop('disabled', false);
	}
});

// double click on mane table rows
$(document).on("dblclick", "#table_project tbody tr", function() {

  // check for empty datatable
  let emptyRow = $(this).find(".dataTables_empty");

  // execute double click logic if table is not empty
  if(!emptyRow.length) {

    // define opened project id and name
    projectDetails.originalId = parseInt($(this).find(".colum_hidden").text().trim());
    projectDetails.name = $(this).find("td:eq(2)").text().trim();

    // load mane dialog
    loadEditProjectDialog();

  }

});

// check for project table all checkboxes
$(document).on("change", ".project-list-check", () => checkAllCheckBoxes(".project-list-check", "#projectTableChecker"));

$(document).on("click", "#delete_week", function () {

    var data = $("#table_week .check:checked").map(function () { //Get Checked checkbox array
        return this.value;
    }).get();

    for (var i = 0; i < data.length; i++) {
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=delete_week&id=" + data[i],
            dataType: "json",
            success: function (data) {
                if (data.error != "") {
                    alert(data.error);
                } else {
                    $("#check-all-import-actived").attr("checked", false);
                    GetDataTable('table_week', aJaxURL, 'table_week', 9, "client_id="+client_id+"&project_id="+project_id+"&wday="+$("#wday").val()+ '&hidde_cycle=' + $('#hidde_cycle').val(), 0, "", 1, "asc", [5], "<'F'lip>");
                }
            }
        });
    }

    $.ajax({
        url: aJaxURL,
        data: 'act=get_wk&project_id='+$('#project_id').val(),
        success: function(data) {
    		if(typeof(data.error) != "undefined"){
    			if(data.error != ""){
    				alert(data.error);
    			}else{
    				$('#work_table td').css('background', '#fff');
    				$("td[check_clock]").html('');
    				for(g=0;g < $(data.work).size();g++){

    					for(i=(parseInt(data.work[g].starttime)+15);i < parseInt(data.work[g].endtime);i+=15){
    						switch(i) {
    						case 60:
        		    	    	i+=40;
        		    	        break;
    						case 160:
        		    	    	i+=40;
        		    	        break;
    						case 260:
        		    	    	i+=40;
        		    	        break;
    						case 360:
        		    	    	i+=40;
        		    	        break;
    						case 460:
        		    	    	i+=40;
        		    	        break;
    						case 560:
        		    	    	i+=40;
        		    	        break;
    						case 660:
        		    	    	i+=40;
        		    	        break;
    						case 760:
        		    	    	i+=40;
        		    	        break;
    						case 860:
        		    	    	i+=40;
        		    	        break;
    						case 960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1360:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1460:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1560:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1660:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1760:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1860:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2360:
        		    	    	i+=40;
        		    	        break;
        		    	    }
    						if(i<9){
    							er='000'+i;
    						}else if(i<99){
        						er='00'+i;
        					}else if(i<999){
                				er='0'+i;
        					}else{
            					er=i;
            				}
    						intvar = parseInt($("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(),10)
    						if($("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").css('background-color') == 'rgb(0, 128, 0)'){
   		    				   $("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(intvar + 1);
      		    			}else{
        						$("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(1);
        		    			$("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").css('background','green');
      		    			}

    		    		}

    				}
    				for(o=0;o < $(data.break).size();o++){
    					for(i=parseInt(data.break[o].breakstarttime);i < parseInt(data.break[o].breakendtime);i+=5){
    						if(i<99){er='00'+i;}else{er=i;}
    						if(i<999){er='0'+i;}else{er=i;}
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").css('background','yellow');
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").html('');
    		    		}
    				}
    			}
    		}
        }
    })

});


$(document).on("click", "#delete_weeks", function () {

    var data = $("#table_cikle .check:checked").map(function () { //Get Checked checkbox array
        return this.value;
    }).get();

    for (var i = 0; i < data.length; i++) {
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=delete_cikle&id=" + data[i],
            dataType: "json",
            success: function (data) {
                if (data.error != "") {
                    alert(data.error);
                } else {
                    $("#check-all-cikle").attr("checked", false);
                    GetDataTable('table_cikle', aJaxURL, 'table_cikle', 5, "client_id="+client_id+"&project_id="+project_id, 0, "", 1, "desc", [2,3,4], "<'F'lip>");
                }
            }
        });
    }


	$.ajax({
        url: aJaxURL,
        data: 'act=get_wk&project_id='+$('#project_id').val(),
        success: function(data) {
    		if(typeof(data.error) != "undefined"){
    			if(data.error != ""){
    				alert(data.error);
    			}else{
    				$('#work_table td').css('background', '#fff');
    				$("td[check_clock]").html('');
    				for(g=0;g < $(data.work).size();g++){
    					for(i=(parseInt(data.work[g].starttime)+15);i < parseInt(data.work[g].endtime);i+=15){
    						switch(i) {
    						case 60:
        		    	    	i+=40;
        		    	        break;
    						case 160:
        		    	    	i+=40;
        		    	        break;
    						case 260:
        		    	    	i+=40;
        		    	        break;
    						case 360:
        		    	    	i+=40;
        		    	        break;
    						case 460:
        		    	    	i+=40;
        		    	        break;
    						case 560:
        		    	    	i+=40;
        		    	        break;
    						case 660:
        		    	    	i+=40;
        		    	        break;
    						case 760:
        		    	    	i+=40;
        		    	        break;
    						case 860:
        		    	    	i+=40;
        		    	        break;
    						case 960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1360:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1460:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1560:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1660:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1760:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1860:
        		    	    	i+=40;
        		    	        break;
        		    	    case 1960:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2060:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2160:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2260:
        		    	    	i+=40;
        		    	        break;
        		    	    case 2360:
        		    	    	i+=40;
        		    	        break;
        		    	    }
    						if(i<9){
    							er='000'+i;
    						}else if(i<99){
        						er='00'+i;
        					}else if(i<999){
                				er='0'+i;
        					}else{
            					er=i;
            				}
    						intvar = parseInt($("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(),10)
    						if($("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").css('background-color') == 'rgb(0, 128, 0)'){
   		    				   $("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(intvar + 1);
      		    			}else{
        						$("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(1);
        		    			$("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").css('background','green');
      		    			}

    		    		}

    				}
    				for(o=0;o < $(data.break).size();o++){
    					for(i=parseInt(data.break[o].breakstarttime);i < parseInt(data.break[o].breakendtime);i+=5){
    						if(i<99){er='00'+i;}else{er=i;}
    						if(i<999){er='0'+i;}else{er=i;}
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").css('background','yellow');
    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").html('');
    		    		}
    				}
    			}
    		}
        }
    })

});

$(document).on("click", "#delete_lang", function () {

    var data = $("#table_lang .check:checked").map(function () { //Get Checked checkbox array
        return this.value;
    }).get();

    for (var i = 0; i < data.length; i++) {
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=delete_lang&id=" + data[i],
            dataType: "json",
            success: function (data) {
                if (data.error != "") {
                    alert(data.error);
                } else {
                    $("#check-all-lang").attr("checked", false);
                    week_day_graphic_id = $('#week_day_graphic_id').val();
                    GetDataTable('table_lang', aJaxURL, 'get_list_lang', 2, "week_day_graphic_id="+week_day_graphic_id, 0, "", 1, "desc", '', "<'F'lip>");
                    GetDataTable('table_week', aJaxURL, 'table_week', 9, "client_id="+client_id+"&project_id="+project_id+"&wday="+$("#wday").val(), 0, "", 1, "asc", [5], "<'F'lip>");
                }
            }
        });
    }

});

$(document).on("click", "#delete_infosorce", function () {

    var data = $("#table_infosorce .check:checked").map(function () { //Get Checked checkbox array
        return this.value;
    }).get();

    for (var i = 0; i < data.length; i++) {
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=delete_infosorce&id=" + data[i],
            dataType: "json",
            success: function (data) {
                if (data.error != "") {
                    alert(data.error);
                } else {
                    $("#check-all-infosorce").attr("checked", false);
                    week_day_graphic_id = $('#week_day_graphic_id').val();
                    GetDataTable('table_infosorce', aJaxURL, 'get_list_infosorce', 2, "week_day_graphic_id="+week_day_graphic_id, 0, "", 1, "desc", '', "<'F'lip>");
                    GetDataTable('table_week', aJaxURL, 'table_week', 9, "client_id="+client_id+"&project_id="+project_id+"&wday="+$("#wday").val(), 0, "", 1, "asc", [5], "<'F'lip>");
                }
            }
        });
    }

});


$(document).on("click", "#holi_creap", function () {
	start = parseInt($("#start_time").val());
	end = parseInt($("#end_time").val());
	hidde_cycle = $("#hidde_cycle").val();
	ch_id = 0;
	if(start < end){
	    ch_id = 1;
	}else{

			  alert('მიუთითეთ სწორი დრო!');
			  ch_id = 0;

	}
	if(ch_id == 1){
		if($('#start_time').val() != '' && $('#end_time').val() != '' && $('#ext_number').val() != '' && $("#week_day_id").val()!=''){
		$.ajax({
	        url: aJaxURL,
		    data: 'act=work_gr&project_id='+$('#project_id').val()+'&start_time='+$("#start_time").val()+'&end_time='+$("#end_time").val()+'&wday='+$("#week_day_id").val() + '&week_day_graphic_id=' + $("#week_day_graphic_id").val() + '&type=' + $("#type").val()+ '&hidde_cycle=' + $("#hidde_cycle").val(),
	        success: function(data) {
				if(typeof(data.error) != "undefined"){
					if(data.error != ""){
						alert(data.error);
					}else{
						CloseDialog("add-edit-form-weekADD");
						$("#hidde_cycle").val(data.new_cycle)
						GetDataTable('table_week', aJaxURL, 'table_week', 9, "client_id="+client_id+"&project_id="+project_id+"&wday="+$("#wday").val()+ '&hidde_cycle=' + data.new_cycle, 0, "", 1, "asc", [5], "<'F'lip>");
						$.ajax({
					        url: aJaxURL,
						    data: 'act=get_wk&project_id='+$('#project_id').val(),
					        success: function(data) {
								if(typeof(data.error) != "undefined"){
									if(data.error != ""){
										alert(data.error);
									}else{
										$('#work_table td').css('background', '#fff');
					    				$("td[check_clock]").html('');
					    				for(g=0;g < $(data.work).size();g++){
					    					for(i=(parseInt(data.work[g].starttime)+15);i < parseInt(data.work[g].endtime);i+=15){
					    						switch(i) {
					    						case 60:
					        		    	    	i+=40;
					        		    	        break;
					    						case 160:
					        		    	    	i+=40;
					        		    	        break;
					    						case 260:
					        		    	    	i+=40;
					        		    	        break;
					    						case 360:
					        		    	    	i+=40;
					        		    	        break;
					    						case 460:
					        		    	    	i+=40;
					        		    	        break;
					    						case 560:
					        		    	    	i+=40;
					        		    	        break;
					    						case 660:
					        		    	    	i+=40;
					        		    	        break;
					    						case 760:
					        		    	    	i+=40;
					        		    	        break;
					    						case 860:
					        		    	    	i+=40;
					        		    	        break;
					    						case 960:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1060:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1160:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1260:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1360:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1460:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1560:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1660:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1760:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1860:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 1960:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 2060:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 2160:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 2260:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    case 2360:
					        		    	    	i+=40;
					        		    	        break;
					        		    	    }
					    						if(i<9){
					    							er='000'+i;
					    						}else if(i<99){
					        						er='00'+i;
					        					}else if(i<999){
					                				er='0'+i;
					        					}else{
					            					er=i;
					            				}
					    						intvar = parseInt($("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(),10)
					    						if($("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").css('background-color') == 'rgb(0, 128, 0)'){
					   		    				   $("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(intvar + 1);
					      		    			}else{
					        						$("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").html(1);
					        		    			$("td[clock='"+er+"'][wday='"+data.work[g].wday+"']").css('background','green');
					      		    			}

					    		    		}

					    				}
					    				for(o=0;o < $(data.break).size();o++){
					    					for(i=parseInt(data.break[o].breakstarttime);i < parseInt(data.break[o].breakendtime);i+=5){
					    						if(i<99){er='00'+i;}else{er=i;}
					    						if(i<999){er='0'+i;}else{er=i;}
					    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").css('background','yellow');
					    						$("td[clock='"+er+"'][wday='"+data.break[o].wday+"']").html('');
					    		    		}
					    				}
									}
								}
						    }
					    });
					}
				}
		    }
	    });
	}else{
	    alert('შეავსეთ "სამუშაო დღე", "სამუშაო იწყება","სამუშაო მთავრდება","სადგურის რაოდენობა" !');
	}
	}
});

    // click on add new project button
    $(document).on("click", "#add_button", function() {

      // open add new project dialog
      loadAddNewProjectDialog();

    });

    // click on delete existed project button
    $(document).on("click", "#delete_button", function() {

      // set selected project collector
      let selectedProjects = "";

      // loop throw selected projects
      $("#table_project tbody").find(`input[type="checkbox"]:checked`).each(function() {

        // collect projects
        if(selectedProjects === "") {
          selectedProjects = $(this).val();
        } else {
          selectedProjects += `, ${$(this).val()}`;
        }

      });

      // define request data
      const data = {
        act: "delete_existed_project",
        selectedProjects
      }

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // destruct responce
        const { error, deleteStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check for delete status
          if(!deleteStatus) {
            alert("თქვენ მიერ მოტხოვნილი მოქმედება ვერ განხორციელდა... მიმართეთ დეველოპერს!");
          } else {

            // load project table
            loadDirPage("projects");
            
          }

        }

      });

    });

// change service level types
$(document).on("change", "#newProjectSlTypeList", function() {

    // define type and input
    let type = $(this).val();
    let input = $(".sl-partial");

    // checking type for sl partial visibility
    if(type !== "4") {

        if(!input.hasClass("sl-partial-hidden")) {
            input.addClass("sl-partial-hidden");
        }

    } else {
        input.removeClass("sl-partial-hidden");
    }

});

// click on copy project details
$(document).on("click", "#copyProjectDetails", function() {

    // send ajax request
    $.post(aJaxURL, {act: "get_existed_project_list"}, result => {

        // destruct responsed result
        let {error, list} = result;

        // check error
        if(error !== "") {
            alert(error);
        } else {
          
            // insert project list in receiver
            $("#copy-project").html(list);

            // define dialog buttons
            const buttons = {
                  "cancel": {
                      text: "დახურვა",
                      id: "cancel-dialog",
                      click: function () {
                          $(this).dialog("close");
                      }
                  }
            };

            // open dialog
            GetDialog("copy-project", 400, "auto", buttons, "center center");

          // set dialog and dialog back holder z position
          $(`.ui-widget-overlay`).css({zIndex: "100"});
          $(`.ui-dialog`).css({zIndex: "101"});

        }

    });


});

// click on choose existed project
$(document).on("click", ".choose-existed-project", function() {

    // define choosed data
    let projectId = $(this).data("id");
    let choosedData = $(this).data("project");

    // set copied project id
    projectDetails.copyId = projectId;

    // destruct data
    let { slMissed,
        slPercent,
        slSecond,
        slType,
        workShift,
        maxHour } = choosedData;

    // destribute data in inputs
    $("#npslCallUnanswered").val(slMissed);
    $("#npslCallPercent").val(slPercent);
    $("#npslCallAnswered").val(slSecond);
    $("#workHoursOperator").val(maxHour);
    $("#workHoursDuration").val(workShift);

    // activate relevant sl type
    let slTypeSelect = $("#newProjectSlTypeList");

    slTypeSelect.find("option[selected]")
        .removeAttr("selected");
    slTypeSelect.find(`option[value="${slType}"]`)
        .prop("selected", true);

    slTypeSelect.trigger("chosen:updated").trigger("change");
    CloseDialog("copy-project");

    // make copy of work hours
    let copyData = {
      act: "copy_wotk_hours",
      id: projectId
    }

    $.post(aJaxURL, copyData, result => {

      loadHoursAfterAdd();

    });

});

// click on close edit project details dialog button
$(document).on("click", "#closeEditProjectDialog", () => {
  closeEditProjectDetailsDialog();
});

// open editing project editor container
$(document).on("click", "#openEditProjectDetailsForm:not(.active)", () => {

  // activate elements
  $("#openEditProjectDetailsForm").addClass("active");
  $(".project-details-editor").addClass("active");

});

// close editing project editor container
$(document).on("click", "#cancelProjectEditing", () => {

  // deactivate elements
  $("#openEditProjectDetailsForm").removeClass("active");
  $(".project-details-editor").removeClass("active");

});

// update project details
$(document).on("click", "#confirmProjectEditing", () => {

  // update project
  updateExistedProject(result => {

    // destruct responsed data
    let {error, complete, exists} = result;

    // checking error
    if(error !== '') {
        alert(error);
    } else {

        // check if olready exists project with same name
        if(exists) {
            alert("მსგავსი დასახელების პროექტი უკვე არსებობს");
        } else {

            // checking complete
            if(complete) {

                // load project table
                loadDirPage("projects");

            } else {
                alert("დაფიქსირდა გაურკვეველი შეცდომა");
            }

            // set project details saved data
            setProjectDetailsSavedData();

            // updated opened page
            buildProjectDataByType($G.projectPageIsOpen);
        }
    }

  });

});

// add new holiday action
$(document).on("click", "#addNewHoliday", function() {

  // send ajax request
  $.post(aJaxURL, {act: "get_holiday_for_project_table"}, result => {

      // destruct result
      let {error, structure} = result;

      // checking received data
      if(error) {
          alert(error);
      } else {

          // load dialog
          loadHolidaysDialogForProject(structure);
      }

  });

});

// add all official holidays action
$(document).on("click", "#addAllOfficialHollidays", () => {

  // define request data
  const data = {
    act: "get_all_official_holidays",
    projectId: projectDetails.originalId
  }

  // send ajax request
  $.post(aJaxURL, data, responce => {

      // destruct result
      let {error, saveStatus} = responce;

      // checking received data
      if(error) {
          alert(error);
      } else {

          // check save status
          if(saveStatus === "allSelected") {
              alert("ყველა ოფიციალური დასვენების დღე უკვე არის მონიშნული");
          } else if(saveStatus) {

            // rebuild holidays table
            holiDays.build();

          } else {
              alert("გაურკვეველი მიზეზების გამო, თქვენ მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
          }
        
      }

  });

});

// click on choose holiday from list for project buttons
$(document).on("click", ".chfp-add", function() {

  // define parent row
  const parentRow = $(this).parents("tr");

  // define data
  const data = {
    act: "add_project_holiday",
    holidayId: $(this).data("id"),
    projectId: projectDetails.originalId
  };


  // send ajax request
  $.post(aJaxURL, data, responce => {

    // destruct responce
    const { error, saveStatus } = responce;

    // check for error
    if(error !== "") {
      alert(error);
    } else {

      // check save status
      if(saveStatus) {

        // rebuild holidays
        holiDays.removeRowFromChooseTable(parentRow).build();

      } else {
        alert("გაურკვეველი მიზეზების გამო, თქვენ მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
      }
      
    }

  });

});


// click on delete holiday from project list button
$(document).on("click", ".rmv-holiday-fl", function() {

    // define data
    const data = {
        act: "remove_project_holiday",
        holidayId: $(this).data("id"),
        projectId: projectDetails.originalId
      };

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // destruct responce
        const { error, removeStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check save status
          if(removeStatus) {

            // rebuild holidays
            holiDays.build();

          } else {
            alert("გაურკვეველი მიზეზების გამო, თქვენ მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
          }
          
        }

      });

});

// check table all rows
$(document).on("change", ".tbale-checker", function() {

  // define check status
  let checkStatus = $(this).is(":checked");

  // define target table
  let targetTable = $(this).data("table");

  // update table row check status
  $(`#${targetTable}`).find("input[type='checkbox']").prop("checked", checkStatus);

});


// structure requester function
function structureRequester(url, act, receiver, callback) {

    // send ajax request
    $.post(url, {act}, result => {

        // destruct responsed result
        let {error, structure} = result;

        // check error
        if(error !== "") {
            alert(error);
        } else {
            $(receiver).html(structure);
            if(callback) callback();
        }

    });

}

    /*click on dialog menu itme*/
    $(document).on("click", ".npfc-menu-container ul li:not(.npfc-menu-active)", function() {

        // define sequense of li and move position number
        let sequense = $(this).data("seq");
        let movePos =  getSliderMovePos(sequense);

        // reactivate menu item
        $(".npfc-menu-container ul li").removeClass("npfc-menu-active");
        $(this).addClass("npfc-menu-active");

        // move the slider
        $(".new-project-slider").css({left: `-${movePos}px`});

    });

    // get slider move position number
    function getSliderMovePos(num) {

        let { workWidth } = projectDialogDetails;
        return workWidth * num;

    }


/*=================================================================================== HOLIDAYS DYRECTORY ACTIONS STARTS ==========================================================================*/

      // define holidays class
      class Holidays {

        // define main constructor metyhod
        constructor() {
          this.projectId = projectDetails.originalId;
          this.rowControler = [];
        }

        // row structure
        rowStructure(data) {

          // destruct data
          const { categoryId, categoryName, date, id, name } = data;

          // return structure
          return `
          <tr data-id="${id}" data-category="${categoryId}">
              <td><span>${date}</span></td>
              <td><span>${name}</span></td>
              <td><span>${categoryName}</span></td>
              <td>
                <button class="rmv-holiday-fl" data-id="${id}">
                    წაშლა
                </button>
              </td>
          </tr>
          `;

        }

        // update rows
        updateRows() {

          // define structure collector
          let structureCollector = "";
          
          // check for row controller
          if(this.rowControler.length) {

            // loop throw rows
            this.rowControler.forEach(item => {
              structureCollector += this.rowStructure(item);
            });
            
          } else {
            structureCollector = `<tr>
                                    <td class="empty-table-td" colspan="4">
                                          ჩამონათვალი ცარიელია
                                    </td>
                                  </tr>`;
          }

          // insert rows structure in relevant receiver
          $("#projectHolidaysListReceiver").html(structureCollector);

          // return object itself
          return this;

        }

        // inner table checker
        innertableChecker() {

          setTimeout(() => {

              // execute outer checker loop
              $("#holiday_for_project_table tbody").find(".choose-holiday-for-project").each(function() {

                // define add button
                let addButton = $(this);
                let addButtonId = addButton.data("id");

                // inner loop for existing data fetching
                $("#holiday-list-for-project-wrapper tbody").find("tr").each(function() {

                  // define existing data
                  let dataId = $(this).data("id");

                  // check for data coincidence
                  if(addButtonId === dataId) {
                    addButton.removeClass("chfp-add").addClass("chfp-remove");
                    addButton.text("წაშლა");
                  } else {
                    addButton.addClass("chfp-add").removeClass("chfp-remove");
                    addButton.text("დამატება");
                  }

                });

              });
              
          }, 50);

        }

        // remove choose holiday table row
        removeRowFromChooseTable(row) {

          // define row count
          const rowCount = row.parents("tbody").find("tr").length;

          // remove row
          row.remove();

          // check for row count
          if(rowCount === 1) {

           $("#holiday_for_project_table tbody").html(`<tr class="odd" style="background: rgb(255, 255, 255);">
                                        <td valign="top" colspan="5" class="dataTables_empty">
                                          ჩანაწერი ვერ მოიძებნა
                                        </td>
                                      </tr>`);
          }

          // return object itself
          return this;

        }

        // fetch build data
        fetchBuildData(callback) {

          // define this pointer
          const self = this;

          // send post request
          $.ajax({
            type:"POST",
            url: aJaxURL,
            cache: false,
            data: {
              act: "fetch_holidays_table_build_data",
              projectId: projectDetails.originalId
            },
            beforeSend() {

              // check if loading already going on
              if(!$(".project-details-loading-container").hasClass("active")) {

                // start loading
                $(".project-details-loading-container").addClass("active");
              }

            },
            success(responce) {

              // // restruct received data
              let { rowControler } = responce;

              // set row controller
              self.rowControler = rowControler;

              // execute collback
              if(callback) callback();

            },
            complete() {

              // end loading
              setTimeout(() => {
                $(".project-details-loading-container").removeClass("active");
              }, 1000);
              
            }
          });

        }

        // build table
        build() {
          
          this.fetchBuildData(() => {
            
            this.updateRows()
                .innertableChecker();

          });

        }

        
      }

      // define main holidays object
      const holiDays = new Holidays();

      // load holiday table
      function loadHolidayTable() {

          GetButtons("add_button_new_holiday", "delete_button_exist_holiday");
          GetDataTable("holiday_table", aJaxURL, "get_holiday_list", 4, "", 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");
          $("#holiday_table .search_header").find(".tbale-checker").prop("checked", false);

      }

    // returns holidays data object
    function getHolidayData() {

        return {
            act: "save_holiday",
            id: $("#holidays_hidden_id").val(),
            category: $("#holidayCategory").val(),
            creeping: $("#holidayCreeping").is(":checked") ? 1 : 0,
            year: $("#holidaYear").val(),
            month: $("#holidayMonth").val(),
            date: $("#holidayDate").val(),
            name: $("#holidayName").val()
        };

    }

    // load holidays dialog
    function loadHolidaysDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-holiday",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-holiday",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // stylize dialog content
        $("#add-edit-form-holidays").html(page);
        $('[data-select="jquery-ui-select"]').chosen({search_contains: true}).next(".chosen-container").css({width:"100%"});
        $("#holidayCategory_chosen").css({width: "96.6%"});
        $("#add-edit-form-holidays").css({overflow: "visible"});

        // open dialog
        GetDialog("add-edit-form-holidays", 500, "auto", buttons, "center");

    }

    // get holidays delete object list
    function getHolidaysDeleteList() {

        // define return value
        let data = '';
        let checkedData = $("#holiday_table tbody").find("input[type='checkbox']:checked");

        checkedData.each(function() {

            if(data !== '') {
                data += ',';
            }

            data += Number($(this).val());

        });
        
        return data;

    }

    // click on adding new holiday button
    $(document).on("click", "#add_button_new_holiday", function() {

        // send ajax request
        $.post(aJaxURL, {act: "get_holiday_form"}, result => {

            // destruct result
            let {error, page} = result;

            // checking received data
            if(error) {
                alert(error);
            } else {

                // load dialog
                loadHolidaysDialog(page);
            }

        });

    });

    // toggle holiday years activity
    $(document).on("change", "#holidayCreeping", function() {

      // define check status and selec value
      let checkStatus = !$(this).is(":checked");
      let value = !checkStatus ? $("#holidaYear").val() : 0;

      // chenge select disabled property
      $("#holidaYear").val(value).prop("disabled", checkStatus).trigger("chosen:updated");

    });

    // save new holiday in directory
    $(document).on("click", "#save-dialog-holiday", function() {

        // collect save data
        let data = getHolidayData();

        // check data before sending with ajax
        if(data.creeping && data.year === "0") {
            alert("გთხოვთ აირჩიოთ წელი");
        } else {

            $.post(aJaxURL, data, result => {

                // restruct result data
                let {error, exists, complete, responseText} = result;

                if(error) {
                    alert(error);
                } else {

                  if(exists) {
                    alert("ასეთი დასვენების დღე უკვე ფიქსირდება ბაზაში");
                  } else if(complete) {

                    alert(`მონაცემები წარმატებით ${responseText}`);

                    //load table
                    loadDirPage("holidays");

                     //close dialog
                     CloseDialog("add-edit-form-holidays");
                  }                  

                }

            });
        }
    });

    // delete holiday from directory
    $(document).on("click", "#delete_button_exist_holiday", function() {

        // define ajax request data
        let data = {
            act: "disable_holiday_from_directory",
            id: getHolidaysDeleteList()
        };

        // send ajax request
        $.post(aJaxURL, data, result => {

            // restruct result
            let {error} = result;

            if(error !== "") {
                alert(error);
            } else {

                //load table
                loadDirPage("holidays");
            }

        });

    });

    // edit work shift data
    $(document).on("dblclick", "#holiday_table tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // collect save data
            let data = {
                act: "edit_holiday_form",
                id: $(this).find(".colum_hidden").text().trim()
            };

            // send ajax request
            $.post(aJaxURL, data, result => {

                // destruct result
                let {error, page} = result;

                // checking received data
                if(error) {
                    alert(error);
                } else {

                    // load dialog
                    loadHolidaysDialog(page);
                }

            });

        }

    });

    // change checkboxes
    $(document).on("change", ".holiday-list-check", () => checkAllCheckBoxes(".holiday-list-check", "#holidayTableChecker"));

/*=================================================================================== HOLIDAYS DYRECTORY ACTIONS ENDS ==========================================================================*/

/*=================================================================================== WORKSHIFT DYRECTORY ACTIONS STARTS ==========================================================================*/

    // returns work shift data object
    function getWorkShiftData() {

        return {
            act: "save_work_shift",
            id: $("#workshift_id").val(),
            start_date: $("#workshift_start_date").val(),
            end_date: $("#workshift_end_date").val(),
            name: $("#workshift_name").val(),
            color: $("#workshift_color").val(),
            type: $("#workshift_type").val(),
            comment: $("#workshift_comment").val(),
            pay: $("#workshift_pay").val(),
            timeout: $("#workshift_timeout").val()
        };

    }

    // load work shift dialog
    function loadWorkShiftDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-workshift",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-workshift",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // insert page in dialog
        $("#add-edit-form-workshift").html(page);

        // stylize dialog inputs
        $('#workshift_start_date,#workshift_end_date').timepicker({
            hourMax: 23,
            hourMin: 0,
            minuteMax: 59,
            minuteMin: 0,
            stepMinute: 1,
            minuteGrid: 15,
            hourGrid: 3,
            timeFormat: 'HH:mm'
        });

        $('#workshift_timeout,#workshift_start_timeout,#workshift_end_timeout').timepicker({
            hourMax: 24,
            hourMin: 0,
            minuteMax: 59,
            minuteMin: 0,
            stepMinute: 1,
            minuteGrid: 15,
            hourGrid: 5,
            timeFormat: 'HH:mm'
        });

        $("#workshift_start_date").trigger("change");

        // open dialog
        GetDialog("add-edit-form-workshift", 230, "auto", buttons, "center");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

    }

    // get work shift delete object list
    function getWorkShiftDeleteList() {

        // define return value
        let data = '';
        let checkedData = $("#work_Shift_table tbody").find("input[type='checkbox']:checked")

        checkedData.each(function() {

            if(data !== '') {
                data += ',';
            }

            data += Number($(this).val());

        });
        console.log(data);
        return data;

    }

    // click on adding new work shift button
    $(document).on("click", "#add_button_work_shift", function() {

        // send ajax request
        $.post(aJaxURL, {act: "get_workshift_form"}, result => {

            // destruct result
            let {error, page} = result;

            // checking received data
            if(error) {
                alert(error);
            } else {

                // load dialog
                loadWorkShiftDialog(page);
            }

        });

    });


    // save new work shift in directory
    $(document).on("click", "#save-dialog-workshift", function () {

        // collect save data
        let data = getWorkShiftData();

        if(data.name === "" || data.color === '' || data.type === 0 || data.pay === 0 || data.project_id === 0){
            alert("შეავსეთ ველი!");
        }else{
            $.ajax({
                url: aJaxURL,
                data,
                success: function(data) {
                    if(typeof(data.error) !== 'undefined'){
                        if(data.error !== ''){
                            alert(data.error);
                        }else{

                          const { saveStatus } = data;

                            if(saveStatus && saveStatus !== "exists") {

                              loadDirPage("shifts");
                              CloseDialog("add-edit-form-workshift");

                            } else if(saveStatus && saveStatus === "exists") {
                              alert("მსგავსი დასახელების მქონე ცვლა უკვე არსებობს ბაზაში");
                            } else {
                              alert("გაურკვეველი მიზეზების გამო, თქვენ მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
                            }
                            
                        }
                    }
                }
            });
        }
    });



    // delete work shift from directory
    $(document).on("click", "#delete_button_work_shift", function() {

        // define ajax request data
        let data = {
            act: "delete_work_shift_from_directory",
            id: getWorkShiftDeleteList()
        };

        // send ajax request
        $.post(aJaxURL, data, result => {

            // restruct result
            let {error} = result;

            if(error !== "") {
                alert(error);
            } else {

                //load table
                loadDirPage("shifts");

                // uncheck all checkbox checker checkbox
                $("#shiftTableChecker").prop("checked", false);
            }

        });

    });

    // edit work shift data
    $(document).on("dblclick", "#work_Shift_table tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // collect save data
            let data = {
                act: "edit_workshift_form",
                id: $(this).find(".colum_hidden").text().trim()
            };

            // send ajax request
            $.post(aJaxURL, data, result => {

                // destruct result
                let {error, page} = result;

                // checking received data
                if(error) {
                    alert(error);
                } else {

                    // load dialog
                    loadWorkShiftDialog(page);
                }

            });

        }

    });


    // set shift duration to 24 hours
    $(document).on("change", "#workshift_24hours", function() {

      // define if box is checked
      const isChecked = $(this).is(":checked");

      // define value receiver inputs
      const startDateInput = $("#workshift_start_date");
      const endDateInput = $("#workshift_end_date");

      // check checked status
      if(isChecked) {
        startDateInput.val("00:00");
        endDateInput.val("24:00");
      } else {
        startDateInput.val("");
        endDateInput.val("");
      }

      // triger change action on timeout imput
      $("#workshift_timeout").trigger("change");

    });

    // calculate shift full and work hours
    $(document).on("change", "#workshift_start_date, #workshift_end_date, #workshift_timeout", function() {

      // define calculate data
      let startTime = $("#workshift_start_date").val();
      let endTime = $("#workshift_end_date").val();
      let breakTime = $("#workshift_timeout").val();
      let inputType = $(this).attr("id");
      let shiftFullHours = "";

      // initilize data variables
      startTime = startTime === "" ? "00:00" : startTime;
      endTime = endTime === "" ? "00:00" : endTime;
      breakTime = breakTime === "" ? "00:00" : breakTime;

      //compare two times
      let startTimeIsGreater = compareTwoHours(startTime, endTime);

      // check if start time is greater
      if(startTime === "00:00" && endTime === "00:00" && breakTime === "00:00") {
        shiftFullHours = "00:00";
      } else if(startTimeIsGreater) {
        let conductor = hours.subtract("24:00", startTime);
        shiftFullHours = hours.add(conductor, endTime);
      } else {
        shiftFullHours = hours.subtract(endTime, startTime);
      }

      // calculate shift work hours
      let shiftWorkHours = hours.subtract(shiftFullHours, breakTime);

      // distribute result values
      $("#plannerNewShiftFullDur").val(shiftFullHours);
      $("#plannerNewShiftWorkDur").val(shiftWorkHours);

      // nullify 24 hours checker
      if(inputType !== "workshift_timeout") {
        $("#workshift_24hours").prop("checked", false);
      } 
      
      // check 24 hours checker
      if((startTime === "00:00" && endTime === "24:00") || (startTime !== "00:00" && endTime !== "00:00" && startTime === endTime)) {
        $("#workshift_24hours").prop("checked", true);
      }

    });

    // change work shift check boxes
    $(document).on("change", ".work-shift-check", () => checkAllCheckBoxes(".work-shift-check", "#shiftTableChecker"));

/*=================================================================================== WORKSHIFT DYRECTORY ACTIONS ENDS ==========================================================================*/

/*=================================================================================== WORKGRAPHIC DYRECTORY ACTIONS STARTS ==========================================================================*/

    // load work graphic table
    function loadWorkGraphicTable() {
        GetButtons("work_graphic_show_active", "work_graphic_show_rchive");
        GetDataTable("work_graphic_table", aJaxURL, "get_work_graphic_list", 4, "", 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");
    }

    // work graphic table actions (CLASS STARTS)
    class WorkGraphic {

      // define mane constructor method
      constructor() {
        this.currentYear = this.curDateData().year;
        this.currentMonth = this.curDateData().month;
        this.daysInMonth = this.curDateData().dayCount;
        this.dataRow = false;
        this.dataCol = false;
        this.cycleList = [];
        this.operatorList = [];
        this.holyDays = [];
        this.reservedCycleData = {};
        this.holidayColNameBg = "#8D2EA0";
        this.holidayColNameColor = "#F2F2F2";
        this.workHoursSum = "00:00";
        this.BreakHoursSum = "00:00";
        this.plannedHours = new Array(this.daysInMonth).fill("00:00");
        this.operatedHours = new Array(this.daysInMonth).fill("00:00");
        this.rowControler =  [ this.rowControllerData() ];
      }

      // get status of percent
      getPercentStatus(percent) {

        // check percents
        if(percent < 15) {
          return "very-weak";
        } else if(percent >= 15 && percent < 30) {
          return "weak";
        } else if(percent >= 30 && percent < 45) {
          return "over-weak";
        } else if(percent >= 45 && percent < 60) {
          return "half";
        } else if(percent >= 60 && percent < 75) {
          return "before-strong";
        } else if(percent >= 75 && percent < 90) {
          return "strong";
        } else if(percent >= 90 && percent < 100) {
          return "over-strong";
        } else if(percent === 100) {
          return "exact";
        } else if(percent > 100) {
          return "over";
        }

      }

      // get week day names
      getWeekDayNames(index) {

        // define week day names array
        let weekDayNames = [
          "კვირა",
          "ორშაბათი",
          "სამშაბათი",
          "ოთხშაბათი",
          "ხუთშაბათი",
          "პარასკევი",          
          "შაბათი"
        ];

        return weekDayNames[index];
      }

      // get month names
      getMonthNames(index) {

        // define week day names array
        let monthNames = [
          "იანვარი",
          "თებერვალი",
          "მარტი",
          "აპრილი",
          "მაისი",
          "ივნისი",
          "ივლისი",
          "აგვისტო",
          "სექტემბერი",
          "ოქტომბერი",
          "ნოემბერი",
          "დეკემბერი"
        ];

        return monthNames[index];
      }

      // returns current date data
      curDateData() {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let dayCount = this.dayCountInMonth(year, month);

        return {
          year,
          month,
          dayCount
        }
      }

      // returns row controller data
      rowControllerData() {
        return {
          id: 0,
          operatorId: 0,
          cycleId: 0,
          shiftList: []
        };
      }

      // returns shift save data
      setShiftSaveData(rowId, colId, shiftId) {

          return {
            projectId: projectDetails.originalId,
            year: this.currentYear,
            month: Number($("#choosWorkGraphicMonth").val()),
            rowId,
            colId,
            shiftId
          };

      }

      // update days count in month
      updateHoursRowLength() {

        // set day count in month
        this.daysInMonth = this.dayCountInMonth(this.currentYear, this.currentMonth);

        // update row length
        this.plannedHours = new Array(this.daysInMonth).fill("00:00");
        this.operatedHours = new Array(this.daysInMonth).fill("00:00");

        // return object itself
        return this;

      }

      // calculate work and break hours
      calcWorkAndBreakHours(data) {

        // resturct data
        const { curDayTimeStart, curDayTimeEnd, curDayTimeout, prevDayTimeEnd, prevDayTimeout } = data;
        
        // define required variables
        const prevDayTimeStart = "00:00";
        const curDayForeseenHours = hours.subtract(curDayTimeEnd, curDayTimeStart);
        const curDayWorkHours = hours.subtract(curDayForeseenHours, curDayTimeout);
        const prevDayForeseenHours = hours.subtract(prevDayTimeEnd, prevDayTimeStart);
        const prevDayWorkHours = hours.subtract(prevDayForeseenHours, prevDayTimeout);

        // define result
        return {
          fullHours: hours.add(curDayForeseenHours, prevDayForeseenHours),
          workHours: hours.add(curDayWorkHours, prevDayWorkHours),
          breakHours: hours.add(curDayTimeout, prevDayTimeout)
        };

      }

      // calculate shift full hours
      calcShiftFullHours(hour1, hour2) {
        return hours.add(hour1, hour2);
      }

      // update row controller data
      updateRowController(updateddata) {
        this.rowControler = updateddata;
      }

      // returns day count in specific month
      dayCountInMonth(year, month) {
        return new Date(year, month, 0).getDate();
      }

      // returns day count in previous month of specific month
      dayCountInPrevMonth() {

        // define month
        let year = this.currentMonth - 1 < 0 ? this.currentYear - 1 : this.currentYear;
        let month = this.currentMonth - 1 < 0 ? 12 : this.currentMonth - 1;

        return new Date(year, month, 0).getDate();

      }

      // returns operator list row structure
      dayNameCols(colid, dayname, date, percent, percentStatus, holidayData) {

        // define hollyday status
        const isHollyday = holidayData !== undefined;

        // define additional class variables
        let dayNameAdditionalClass = "";
        let dateLinkAdditionalClass = "";

        // check hollyday status
        if(isHollyday) {
          dayNameAdditionalClass = "wgtc-head-hollyday";
          dateLinkAdditionalClass = "wgt-ddo-hollyday";
        } else {
          dayNameAdditionalClass = dayname == "შაბათი" || dayname == "კვირა" ? "wgtc-head-weekend" : "";
        }

        // define percent denoting div width value
        const widthPercent = percent > 100 ? 100 : percent;

        // return structure
        return `<div class="wgsb-content-col" data-col="${colid}">
    										<div class="wgtc-head-row wgtc-head-day-name height-1 ${dayNameAdditionalClass}">
    											<span>${dayname}</span>
    										</div>
    										<div class="wgtc-head-row height-1">
    											<a class="wgt-day-dialog-open ${dateLinkAdditionalClass}" data-date="${date}" href="#">${date}</a>
    										</div>
    										<div class="wgtc-head-row height-1">
    											<div class="wgtc-head-percent-holder">
    												<div class="wgtc-head-percent-value" data-timeout="0" data-activitie="0">
                              <div class="value" style="width:${widthPercent}%;"></div>
    													<span>${percent}%</span>
    												</div>
    											</div>
    										</div>
    									</div>`;
      }

       // returns operator list row structure
       operatorRow(rowid) {

        // define build data variables
        const { id, operatorId, cycleId } = this.rowControler[rowid];
        var filtered = this.rowControler[rowid]["rowId"];
        // define return data
        return `<div class="wgtc-body-row">
                      <div class="wgsb-header-col no">
                        <span>${rowid + 1}</span>
                      </div>
                      <div class="wgsb-header-col select">
                        <select class="choose-work-graphic-operator" data-id="${id}" data-row="${filtered}">
                          ${this.makeOptionList(this.operatorList, operatorId, "აირჩიეთ ოპერატორი")}
                        </select>
                      </div>
                      <div class="wgsb-header-col select">
                        <select class="choose-work-graphic-cycle" data-id="${id}">
                          ${this.makeOptionList(this.cycleList, cycleId, "აირჩიეთ ციკლი")}
                        </select>
                      </div>
                      <div class="wgsb-header-col button">
                        ${rowid > 0 ? `<button class='delete-work-graphic-row' data-id="${id}" title="წაშლა">
                          <i class="fa fa-trash"></i>
                        </button>` : '<button></button>'}
                        <button class='clear-work-graphic-row' data-id="${id}" title="გასუფთავება">
                          <i class="fa fa-eraser"></i>
                        </button>
                      </div>
                    </div>`;
        }

      // returns shift list row structure
      shiftRow(rowid) {

        return `<div class="wgtc-body-row">
                        ${this.shiftCol(rowid)}
                      </div>`;
      }

      // returns shift list column structure graphiccol
      shiftCol(rowid) {

        // define main structure collector
        let mainStructure = "";

        for(let i = 0; i < this.daysInMonth; i++) {

          // define inner structure collector
          let innerStructure = "";

          // define timeout and activitie seconds collectors
          let timeoutSecondsCollecotr = 0;
          let activitieSecondsCollector = this.columnActivitieSecondsSummary(this.rowControler[rowid].activitieSeconds, i);

          // define shift list
          const shiftlist = this.rowControler[rowid].shiftList[i];
          
          // define edit status
          const isEdited = this.rowControler[rowid].editedColimns.some(item => Number(item) === i);

          // define holiday status
          const isHoliday = this.holyDays.some(item => Number(item.colId) === i);

          // check for holiday
          if(!isHoliday) {

            // check for shift list
            if(shiftlist && shiftlist.length) {

              // loop throw list
              shiftlist.map((item, dataIndex) => {
                
                // restruct shift list data
                const {colId, id, shiftColor, shiftId, shiftName, currentDayTimeout, editStatus} = item;

                // colloect timeout
                timeoutSecondsCollecotr += hours.inSeconds(currentDayTimeout);
                
                // get previews day day timeout if exists
                const prevDayData = this.rowControler[rowid].shiftList[i - 1];

                if(i > 0 && (prevDayData !== undefined && prevDayData !== null) && (prevDayData[dataIndex] !== undefined && prevDayData[dataIndex] !== null)) {

                  // define previews day timeout variable
                  let { nexDayTimeout } = prevDayData[dataIndex];

                  // collect previews day timeout
                  timeoutSecondsCollecotr += hours.inSeconds(nexDayTimeout);

                }
                
                // check for column id
                if(Number(colId) === i) {
                  innerStructure += `
                    <div class="wgsb-content-col-unit" style="background: ${shiftColor}">
                      ${shiftlist.length == 1 ? `<span>
                          ${item.shiftName}
                        </span>` : `<div class="wgsb-ccu-description">
                          ${item.shiftName}
                        </div>`}
                    </div>
                  `; 
                }
                
              });

            }

          } else {

            // define dollyday data
            const hollydayData = this.holyDays.find(item => Number(item.colId) === i);

            // inner structure
            innerStructure += `
                    <div class="wgsb-content-col-unit holiday-unit">
                      <span>
                        ${hollydayData.name}
                      </span>
                    </div>
                  `; 

          }

          // get saved timeout and activitie seconds
          let savedTimeoutSeconds = $(`.wgsb-content-col[data-col="${i}"]`).find(".wgtc-head-percent-value").attr("data-timeout");
          let savedActivitieSeconds = $(`.wgsb-content-col[data-col="${i}"]`).find(".wgtc-head-percent-value").attr("data-activitie");

          // increase timeout and activitie seconds
          let increasedTimeoutSeconds = Number(savedTimeoutSeconds) + Number(timeoutSecondsCollecotr);
          let increasedActivitieSeconds = Number(savedActivitieSeconds) + Number(activitieSecondsCollector);

          // set new value of timeout and activitie seconds
          $(`.wgsb-content-col[data-col="${i}"]`).find(".wgtc-head-percent-value").attr("data-timeout", increasedTimeoutSeconds);
          $(`.wgsb-content-col[data-col="${i}"]`).find(".wgtc-head-percent-value").attr("data-activitie", increasedActivitieSeconds);
                                                                                                                                        
          // collect structure                                                                                    ///   rowid                            
          mainStructure += `<div class="wgsb-content-col work-shift ${isHoliday ? 'is-holiday' : ''}" data-row="${this.rowControler[rowid].rowId}" data-col="${i}">
                              ${innerStructure}
                              ${isEdited ? this.getEditHistoryPointer({ rowId:this.rowControler[rowid].id, colId:i }) : ''}
                            </div>`;
        }

        return mainStructure;

      }

      // returns summary of column activitie seconds
      columnActivitieSecondsSummary(activitieSeconds, colId) {

        // define seconds collector
        let activitieSecondsCollector = 0;

        // collect activities
        activitieSeconds.map(item => {

          if(Number(item.colId) === colId) {
            activitieSecondsCollector += Number(item.seconds);
          }
          
        });

        // return collected seconds
        return activitieSecondsCollector;

      }

      // returns work and break hours summery row
      workBreakHoursRow(rowid) {

        // define time status
        const timeStatus = this.getWorkBreakHoursRowStatus(this.workHoursSum);

        // return structure
        return `<div class="wgtc-body-row">
										<div class="wgsb-footer-col hour-count work-houers ${timeStatus}" data-row="${rowid}">
											<span>${this.workHoursSum}</span>
										</div>
										<div class="wgsb-footer-col hour-count break-houers" data-row="${rowid}">
											<span>${this.BreakHoursSum}</span>
										</div>
									</div>`;
      }

      // returns pointer for get column edit history
      getEditHistoryPointer(data) {

        // destruct data
        const { rowId, colId } = data;

        // return structure
        return `<button class="col-edit-history-pointer" data-row="${rowId}" data-col="${colId}" >
          <i class="fa fa-caret-right"></i>
        </button>`;
      }
      
      // returns work and break hours summary row status
      getWorkBreakHoursRowStatus(workHours) {

        // define project max hours and splited work hours
        const projectMaxHoursInWeek = Number($("#workHoursOperator").val());
        const projectMaxHoursInMonth = projectMaxHoursInWeek * 4;
        const splitedWorkHours = workHours.split(":");

        // define time from splited work hours
        const parsedWorkHours = Number(splitedWorkHours[0]);
        const parsedWorkMinutes = Number(splitedWorkHours[1]);

        // compare project hours and work hours
        if((projectMaxHoursInMonth < parsedWorkHours) || (projectMaxHoursInMonth === parsedWorkHours && parsedWorkMinutes > 0)) {
          return "over";
        } else {
          return "normal";
        }

      }

      // returns shift column results rows by category
      makeResultRows() {

        // define object itself
        const self = this;

        // loop throw results rows
        $(".work-graphic-table-results .wgtc-body").find(".wgtc-results-row").each(function(row) {

          // define structure collector
          let structure = "";

          for(let i = 0; i < self.daysInMonth; i++) {

            // define category variable
            let category = self.getResultRowCategory(row);

            // collect structure
            structure += `<div class="wgsb-results-col ${category}" data-col="${i}">
        										<span data-difference="00:00" data-shiftid="0">00:00</span>
        									</div>`;
          }

          // insert columns in row
          $(this).html(structure);

        });

        // return object itself
        return self;

      }

      // returns result row col category
      getResultRowCategory(index) {
        switch(index) {
          case 0:
            return "planned";
          case 1:
            return "operated";
          case 2:
            return "difference";
        }
      }

      // make option list
      makeOptionList(array, activeid, endtext) {

        // define variables
        let structure = "";
        activeid = parseInt(activeid);

        // execute loop
        array.map(item => {

          // define item id
          let itemId = parseInt(item.id);
          let shiftCount = item.shiftCount ? item.shiftCount : null;

          structure += `<option value="${itemId}"  ${activeid === itemId ? "selected" : ""} ${shiftCount ? `data-shift-count="${shiftCount}"` : ''}>${item.name}</optin>`;
        });

        structure += `<option value="0" ${activeid === 0 ? "selected" : ""}>${endtext}</optin>`;

        return structure;

      } 

      // updates rows in table
      updateRows() {

        // define row collectors
        let operatorsRows = "";
        let shiftRows = "";
        let workBreakHoursRows = "";

        // define rou loop
        for(let i = 0; i < this.rowControler.length; i++) {
          
          // nullfying work and break hours summary
          this.workHoursSum = "00:00";
          this.BreakHoursSum = "00:00";

          this.summaryWorkAndBreakHours(i);
          
          // collecting structures
          operatorsRows += this.operatorRow(i);
          shiftRows += this.shiftRow(i);
          workBreakHoursRows += this.workBreakHoursRow(i);

        }

        // insert rows in relevant parents
        $(".wgsb-header").find(".wgtc-body").html(operatorsRows);
        $(".wgtc-work-shift-receiver").html(shiftRows);
        $(".wgsb-footer").find(".wgtc-body").html(workBreakHoursRows);

        // update result rows
        this.calcColActivitiePercent()
            .makeResultRows()
            .updatePlannedHours()
            .distributeOperatedHours();

        // return object itself
        return this;

      }

      // calculates column activitie percentence
      calcColActivitiePercent() {

        // loop throw day columns
        for(let i = 0; i < this.daysInMonth; i++) {

          // get receiver
          const receiver = $(`.wgsb-content-col[data-col="${i}"]`).find(".wgtc-head-percent-value");

          // get timeout and activitie seconds
          const timeoutSeconds = receiver.data("timeout");
          const activitieSeconds = receiver.data("activitie");

          // define percent variable
          let percent = 0, percentWidth = 0, precentText = "0%", status = "";
          
          // check for seconds
          if(timeoutSeconds !== 0 && activitieSeconds !== 0) {
            percent = parseInt((activitieSeconds / timeoutSeconds) * 100);
            percentWidth = percent <= 100 ? percent : 100;
            precentText = `${percent}%`;
            status = this.getPercentStatus(percent);
          } else if(timeoutSeconds === 0 && activitieSeconds !== 0) {
            precentText = "~";
            percentWidth = 100;
            status = "over";
          }

          // initilize receiving data
          receiver.attr("data-status", status);
          receiver.find("span").text(precentText);
          receiver.find(".value").css({
            width: `${percentWidth}%`
          });
          
        }

        // return object itself
        return this;

      }

      // update column activitie percentence by update data
      updateSingleColActivitiePercent(updateData) {

        // destruct update data
        const { colId, timeoutInSeconds, activitieInSeconds } = updateData;

        // // get receiver
        const receiver = $(`.wgsb-content-col[data-col="${colId}"]`).find(".wgtc-head-percent-value");

        // set updated values
        receiver.attr("data-timeout", timeoutInSeconds)
            .attr("data-activitie", activitieInSeconds)
            .data("timeout", timeoutInSeconds)
            .data("activitie", activitieInSeconds);

        // define percent variable
        let percent = 0, percentWidth = 0, percentText = "0%", status = "";

        // check for seconds
        if(timeoutInSeconds !== 0 && activitieInSeconds !== 0) {
          percent = parseInt((activitieInSeconds / timeoutInSeconds) * 100);
          percentWidth = percent <= 100 ? percent : 100;
          percentText = `${percent}%`;
          status = this.getPercentStatus(percent);
        } else if(timeoutInSeconds === 0 && activitieInSeconds !== 0) {
          percentText = "~";
          percentWidth = 100;
          status = "over";
        }

        // initilize receiving data
        receiver.attr("data-status", status);
        receiver.find("span").text(percentText);
        receiver.find(".value").css({
          width: `${percentWidth}%`
        });

        // return object itself
        return this;

      }

      // update planned hours
      updatePlannedHours() {

        // check for row controler data
        if(this.rowControler.length) {

          // loop throw all days
          for(let dayId = 0; dayId < this.daysInMonth; dayId++) {

            // define hollyday status
            const isHoliday = this.holyDays.some(item => Number(item.colId) === dayId);

            // check hollyday status
            if(!isHoliday) {

              // define  time collector
              let workHours = "00:00"; 
              
              // loop throw rows
              this.rowControler.forEach((row, index) => {

                // define shift list data
                let shiftData = row.shiftList[dayId];
                
                // check for data
                if(shiftData) {

                  // define previews shift data
                  let prevShiftData = row.shiftList[dayId - 1];           

                  // loop throw shift data
                  shiftData.forEach((item, index) => {
                    workHours = hours.add(workHours, item.currentDayWorkTime);
                  });

                  // check for previews data
                  if(prevShiftData) {

                    // loop throw previews shift data
                    prevShiftData.forEach((prevItem, index) => {
                        workHours = hours.add(workHours, prevItem.nexDayWorkTime);
                    });
                  }

                  // insert time in receiver
                  $(`.wgsb-results-col.planned[data-col="${dayId}"]`).find("span").attr("data-difference", workHours).text(workHours);

                  // calculate planned and operated hours difference
                  this.calcPlannedAndOperatedDifference(workHours, dayId);

                }

              });

            }

          }

        }

        // return object iself
        return this;

      }

      // calculate planned and operated hours difference
      calcPlannedAndOperatedDifference(plannedHour, colid) {

        // define receiver
        let receiver = $(`.wgsb-results-col.difference[data-col="${colid}"]`);

        // substract operated hour to planned hour
        let opHour = this.operatedHours[colid];
        let difference = hours.subtract(plannedHour, opHour);

        // define status of difference
        let diffStatus = "exact";

        if(difference !== "00:00") {
          diffStatus = difference.match(/^-/i) ? "less" : "over";
        }

        //insert difference hours into relevant column and set difference status
        receiver.attr("class", "wgsb-results-col difference")
                .addClass(diffStatus)
                .find("span").text(difference);

      }

      // distribute operated hours
      distributeOperatedHours() {

        // loop throw operated hours array
        this.operatedHours.map((item, i) => {
          $(`.wgsb-results-col.operated[data-col="${i}"]`).find("span").text(item);
        });

        // return object itself
        return this;

      }

      // summaryze work and break hours
      summaryWorkAndBreakHours(rowid) {

        // define shift list
        const shiftList = this.rowControler[rowid].shiftList;

        // loop throw shift list
        for(let item in shiftList) {

          // define holiday status
          const isHoliday = this.holyDays.some(holyday => holyday.colId === item);

          // check for hollyday
          if(!isHoliday) {

            // define shift data and previews shift data
            let shiftData = shiftList[item];
            let prevShiftData = shiftList[Number(item) - 1];

            // loop throw shift data
            shiftData.forEach(unit => {

              this.workHoursSum = hours.add(this.workHoursSum, unit.currentDayWorkTime);
              this.BreakHoursSum = hours.add(this.BreakHoursSum, unit.currentDayTimeout);

            });

            // check for prviews shift data
            if(prevShiftData) {
              
              // loop throw shift data
              prevShiftData.forEach(prevUnit => {

                  this.workHoursSum = hours.add(this.workHoursSum, prevUnit.nexDayWorkTime);
                  this.BreakHoursSum = hours.add(this.BreakHoursSum, prevUnit.nexDayTimeout);

              });
            }

          }
        
        }

      }

      // returns assimilated activitie percent
      assimilatedActivitiePercent(percentdata) {

        // define variables
        let breakHourCollector = "00:00";
        let activitiesCount = 0;

        // loop throw percent data
        percentdata.forEach(item => {

          breakHourCollector = hours.add(breakHourCollector, item.breakHour);
          activitiesCount += this.assimilatedActivitieCount(item.activities);

        });

        // convert break hours to count
        let breakHoursCount = hours.convertToNum(5, breakHourCollector);

        // calculate result value
        let result = (activitiesCount / breakHoursCount) * 100;

        // return data
        if(isFinite(result)) {
          return Math.round(result);
        } else {
          return 0;
        }

      }    

      // returns assimilated activitie count in numbers
      assimilatedActivitieCount(activities) {

        // define count collector
        let countCollector = 0;

        // loop throw activities array
        activities.forEach(item => {

          // define start and end
          let start = Number(item.start);
          let end = Number(item.end);

          // check for valid start and end
          if(start !== -1 && end !== -1) {

            // define difference
            let difference = end - start + 1;

            // collect assimilated activities count
            countCollector += difference;

          }

        });

        // return data
        return countCollector;

      }

      // summarize planned hours
      summaryPlannedHours(rowid, colid) {

        // define hour values
        let oldValue = this.plannedHours[colid];
        let newValue = this.calcWorkAndBreakHours(this.rowControler[rowid].shiftList[colid]).workHours;

        this.plannedHours[colid] = hours.add(oldValue, newValue);

      }

      // set slider width
      setSliderWidth() {

        // define width
        let width = this.daysInMonth * 90 + 2;

        $(".wgts-days-width").css({width: `${width}px`});

        // return object itself
        return this;
      }      

      // name day columns in work graphic table
      makeDayCols() {
        
        // set month and year
        let monthAndYear = `${this.getMonthNames(this.currentMonth - 1)} ${this.currentYear}`;

        // define structure collector and receiver
        let collector = "";
        let monthReceiver = $(".wgtc-month-year span");
        let dayReceiver = $("#dayNameReceiver");

        // temporrary
        let percent = 0, percentStatus = 0;

        // loop throw month days
        for(let i = 0; i < this.daysInMonth; i++) {

          // define holiday data
          const holidayData = this.holyDays.find(item => Number(item.colId) === i);

          // define day data
          let dayIndex = new Date(`${this.currentYear}-${this.currentMonth}-${i + 1}`).getDay();
          let dayName = this.getWeekDayNames(dayIndex);
          let date = `${zeroFixer(i + 1)}.${zeroFixer(this.currentMonth)}.${this.currentYear}`;

          collector += this.dayNameCols(i, dayName, date, percent, percentStatus, holidayData);

        }

        // insert collected html to receiver
        monthReceiver.text(monthAndYear);
        dayReceiver.html(collector);

        // return object itself
        return this;

      }

      // initilize work graphic table header
      initTableHeader() {

        // set width of slider and name the day columns
        this.setSliderWidth().makeDayCols();

        // activate month in month choose list
        $("#choosWorkGraphicMonth").find(`option[value="${this.currentMonth}"]`).prop("selected", true);

        // return object itself
        return this;

      }

      // filter operated hours
      processOperatedHours(operatedhours) {

        // define hours daTa collector
        let hoursDataCollector = [];

        // loop throw operated hours
        operatedhours.map(opHour => {
          
          // check if data already exists in collector array
          if(hoursDataCollector.some(datColItem => datColItem.id === opHour.id)) {

            // if data exists, increment it
            hoursDataCollector.map(mapItem => {
              if(mapItem.id === opHour.id) {
                mapItem.hours = hours.add(mapItem.hours, opHour.hours);
              }
            });

          } else {
            hoursDataCollector.push(opHour);
          }

        });

        // loop throw all month days for update operated hours array
        this.loopThrowAllDays((index, dayIndex) => {
          
          // check for day index as 0
          if(dayIndex === 0) {
              dayIndex = 7;
          }

          let data = hoursDataCollector.find(item => parseInt(item.id) === dayIndex);
          this.operatedHours[index] = data ? data.hours : "00:00";
 
        });

      }

      // loop thro individual day by date
      loopThrowAllDays(callback) {

        // loop throw month days
        for(let i = 0; i < this.daysInMonth; i++) {

          // define day data
          let dayIndex = new Date(`${this.currentYear}-${this.currentMonth}-${i + 1}`).getDay();
          callback(i, dayIndex);

        }

      }

      // initilize events
      initEvents() {

        // scroll the graphic table
        $(".wgts-wrapper").scroll(function() {

            // define scroll position
            let leftPos = $(this).scrollLeft();

            // change position of the table
            $(".wgtc-slider").css({left: `-${leftPos}px`});

        });

      }

      // fetch build data
      fetchBuildData(callback) {

        // define this pointer
        const self = this;

        // send post request graphicbla
        $.ajax({
          type:"POST",
          url: aJaxURL,
          cache: false,
          data: {
            act: "fetch_work_graphic_build_data",
            projectId: projectDetails.originalId,
            year: self.currentYear,
            month: self.currentMonth
          },
          beforeSend() {

            // check if loading already going on
            if(!$(".project-details-loading-container").hasClass("active")) {

              // start loading
              $(".project-details-loading-container").addClass("active");
            }

          },
          success(responce) {

            // // restruct received data
            let {operatorList, cycleList, operatedHours, rowControler, holyDays} = responce;

            // set operator list
            self.operatorList = operatorList;

            // set cycle list
            self.cycleList = cycleList;

            // set row controller data
            self.rowControler = rowControler;

            // set holidays data
            self.holyDays = holyDays;

            // process operated hours
            self.processOperatedHours(operatedHours);
            
            // execute collback
            if(callback) callback();

          },
          complete() {
			if($("#hidde_session_goup_id").val() == 34){
				$('.wgsb-content-col').prop('disabled',true);
				$('.choose-work-graphic-operator').prop('disabled',true);
				$('.choose-work-graphic-cycle').prop('disabled',true);
				$('.delete-work-graphic-row').prop('disabled',true);
				$('.clear-work-graphic-row').prop('disabled',true);
				$("#addWorkGraphicRow").css('display','none');
			}else if($("#hidde_session_goup_id").val() == 46){
				$('.choose-work-graphic-operator').prop('disabled',true);
				$('.choose-work-graphic-cycle').prop('disabled',true);
				$('.delete-work-graphic-row').prop('disabled',true);
				$('.clear-work-graphic-row').prop('disabled',true);
				$("#addWorkGraphicRow").css('display','none');
			}else{
				$('.wgsb-content-col').prop('disabled',false);
				$('.choose-work-graphic-operator').prop('disabled',false);
				$('.choose-work-graphic-cycle').prop('disabled',false);
				$('.delete-work-graphic-row').prop('disabled',false);
				$('.clear-work-graphic-row').prop('disabled',false);
				$("#addWorkGraphicRow").css('display','');
			}
            // end loading
            setTimeout(() => {
              $(".project-details-loading-container").removeClass("active");
            }, 1000);
            
          }
        });

      }

      // build the table
      build(callback) {

        this.fetchBuildData(() => {

            // initilize work graphic table
            this.initTableHeader()
                .updateRows()
                .initEvents();

            // execute callback if exists
            if(callback) callback();

        });

      }


    }

    const workGraphic = new WorkGraphic();
    // work graphic table actions (CLASS ENDS)

    // add row in table
    $(document).on("click", "#addWorkGraphicRow", function() {
      console.log(workGraphic.rowControler);
      var onlyRowIds = [];
      workGraphic.rowControler.forEach((e)=>{
          onlyRowIds.push(e.rowId);
      });
      var max = Math.max.apply(null,onlyRowIds);
      // define request data
      const data = {
        act: "add_graphic_row",
        projectId: projectDetails.originalId,
        year: workGraphic.currentYear,
        month: workGraphic.currentMonth,
        rowId: max + 1
      }

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // restruct responce
        const { error, addStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check for add status
          if(addStatus) {
            workGraphic.build();
          } else {
            alert("გაურკვეველი მიზეზების გამო თქვენს მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
          }

        }

      });

    });

    // delete row in table
    $(document).on("click", ".delete-work-graphic-row", function() {
      // define request data
      const data = {
        act: "delete_graphic_row",
        dataId: $(this).data("id")
      }

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // restruct responce
        const { error, deleteStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check for delete status
          if(deleteStatus) {
            workGraphic.build();
          } else {
            alert("გაურკვეველი მიზეზების გამო, თქვენს მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
          }

        }

      });

    });

    // delete row in table
    $(document).on("click", ".clear-work-graphic-row", function() {
      // define clear data
      const clearData = {
        act: "clear_graphic_row",
        dataId: $(this).data("id")
      }
      
      // send ajax request
      $.post(aJaxURL, clearData, responce => {

        // restruct responce
        const { error, clearStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check for clear status
          if(clearStatus) {
            workGraphic.build();
          } else {
            alert("გაურკვეველი მიზეზების გამო, თქვენს მიერ მოთხოვნიული მოქმედება ვერ განხორციელდა");
          }

        }

      });     

    });

    // change the operator
    $(document).on("change", ".choose-work-graphic-operator", function() {

      // define save data
      const saveData = {
        act: "save_graphic_operator",
        dataId: $(this).data("id"),
        operatorId: $(this).val()
      };

      // send ajax request
      $.post(aJaxURL, saveData, responce => {

        // restruct responce
        const { error, saveStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check for save status
          if(saveStatus) {
            workGraphic.build();
          } else {
            alert("გაურკვეველი მიზეზების გამო თქვენს მიერ მოთხოვნილი ოპერაცია ვერ განხორციელდა");
          }

        }

      });

    });

    // change the cycle
    $(document).on("change", ".choose-work-graphic-cycle", function() {

      // define this keyword sustitue
      const self = this;

      // define data main id
      const dataId = $(self).data("id");

      // define active cycle id
      const activeCycle = workGraphic.rowControler.find(item => Number(item.id) === dataId).cycleId;

      // define shift count data
      const shiftCount = $(this).find("option:selected").data("shift-count");

      // check for shift count
      if(shiftCount) {

        // define dialog structure request data
        const dialogData = {
          act: "get_workShift_cycle_start_pos_structure",
          daysInMonth: workGraphic.daysInMonth,
          shiftCount: shiftCount,
          user:$(`.choose-work-graphic-operator[data-id=${dataId}]`).val()
        }

        // send dialog structure request
        $.post(aJaxURL, dialogData, result => {

          // destruct result
          let {error, dialog} = result;

          // check for error
          if(error !== "") {
            alert(error);
          } else {

            // define dialog buttons
            let buttons = {
                "save": {
                    text: "შენახვა",
                    id: "save-workGraphic-cycle",
                    click: function () {

                      // define save data
                      const saveData = {
                        act: "save_graphic_cycle",
                        daysInMonth: workGraphic.daysInMonth,
                        cycleId: Number($(self).val()),
                        cycleStartDate: Number($("#wgCycleSTartFromDate").val()) - 1,
                        cycleStartShift: Number($("#wgCycleSTartFromQueu").val()) - 1,
                        continueFromPrevMonth: $("#continueFromPrevMonth").is(":checked"),
                        month: Number($("#choosWorkGraphicMonth").val()) - 1,
                        user: $("#hide_this_user_id").val(),
                        dataId,
                        shiftCount
                      }

                      // check save data
                      if((saveData.cycleStartDate === -1 || saveData.cycleStartShift === -1) && !saveData.continueFromPrevMonth) {
                        alert("გთხოვთ შემოწმოთ არის თუ არა მონიშნული ციკლის დაწყების თარიღი და საწყისი ცვლის რიგითობა");
                      } else {

                        // send aja request
                        $.post(aJaxURL, saveData, responce => {
                          
                          // restruct responce
                          const  { error, saveStatus } = responce;

                          // check for error
                          if(error !== "") {
                            alert(error);
                          } else {

                            // check for save status
                            if(saveStatus) {

                              // update build
                              workGraphic.build();
                            } else {
                              alert("გაურკვეველი მიზეზის გამო მოთხოვნილი მოქმედება ვერ განხორციელდა");
                            }
                            
                          }

                        });

                        // close dialog
                        $(this).dialog("close");

                      }                     

                    }
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-workGraphic-cycle",
                    click: function () {
                        $(this).dialog("close");

                        // reset cycle name
                        $(self).val(activeCycle);
                    }
                }
            };

            // insert page in dialog
            $("#workGraphic-cycle-start-pos").html(dialog);

            // stylize objects in dialog
            $("#workGraphic-cycle-start-pos").find("select").chosen({
              search_contains: true
            });
            $("#workGraphic-cycle-start-pos").css({overflow: "visible"});
            $("#wgCycleSTartFromDate_chosen, #wgCycleSTartFromQueu_chosen").css({width: "100%"});

            // open dialog
            GetDialog("workGraphic-cycle-start-pos", 250, "auto", buttons, "center center");

            // set dialog and dialog back holder z position
            $(`.ui-widget-overlay`).css({zIndex: "100"});
            $(`.ui-dialog`).css({zIndex: "101"});

          }

        });

      } else {
         // reset cycle name
         $(self).val(activeCycle);
      }

    });

    // change the shift
    $(document).on("click", ".wgsb-content-col.work-shift:not(.is-holiday)", function() {

      // define self as this object
      let self = this;

      // defie row and col and shift id
      let rowId = $(this).data("row");
      let colId = $(this).data("col");

      // define ajax request data
      const data = {
        act: "get_wg_shift_dialog",
        type: "graphic",
        projectId: projectDetails.originalId,
        year: workGraphic.currentYear,
        month: $("#choosWorkGraphicMonth").val(),
        rowId,
        colId
      }

      // send ajax request
      $.post(aJaxURL, data, result => {

        // destruct result
        let {error, dialog, projectMaxHours} = result;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

            // define dialog buttons
            const buttons = {
                  "save": {
                      text: "დამატება",
                      id: "save-graphic-shift-dialog",
                      click: function () {

                        // define choosed shift id
                        const shiftId = Number($("#wgShiftShoose").val());

                        // check for shift id
                        if(shiftId !== "" && shiftId !== undefined && shiftId !== null && shiftId !== 0) {

                          // define ajax request data
                          const data = {
                            act: "save_graphic_shift",
                            ... workGraphic.setShiftSaveData(rowId, colId, shiftId)
                          }

                          // send ajax request
                          $.post(aJaxURL, data, responce => {

                              // restruct responce
                              const { error, result, shiftList } = responce;
                              
                              // check for error
                              if(error !== "") {
                                alert(error);
                              } else {
                                
                                // restruct result
                                const { complete, denialReasone } = result;

                                // check if complete do not implemented
                                if(!complete) {

                                  // end loading
                                  $(".project-details-loading-container").removeClass("active");

                                  // check for denial reasone
                                  switch(denialReasone) {
                                    case 'id':
                                      alert("ერთ დღეზე შეუძლებელია დაემატოს ერთიდაიგივე ცვლა");
                                      break;
                                    case 'prevDayTime':
                                      alert("არცეული ცვლა ჰკვეთს წინა დღის ცვლ(ებ)ის დროს");
                                      break;
                                    case 'time':
                                      alert("არჩეული ცვლის დრო ჰკვეთს უკვე არსებული ცვლ(ებ)ის დროს");
                                      break;
                                    case 'nextDayTime':
                                      alert("არცეული ცვლა ჰკვეთს მომდევნო დღის ცვლ(ებ)ის დროს");
                                      break;
                                  }

                                } else {

                                  // build graphic
                                  workGraphic.build(() => {

                                     // reinsert shift list
                                    $(".wg-shift-list-container").remove();
                                    $(".wg-shift-list-add").before(shiftList);

                                  });
                                }

                              }

                          });

                        } else {
                          alert("გთხოვთ მიუთითოთ ცვლა");
                        }


                      }
                  },
                	"cancel": {
        	            text: "დახურვა",
        	            id: "cancel-dialog",
        	            click: function () {
        	            	$(this).dialog("close");
        	            }
        	        }
        	    };

            // insert received dialog structure in the dialog wrapper
            $("#planer-shift-controler").html(dialog);

            // stylize details
            $(".jquery-ui-chosen").chosen({
              search_contains: true
            });
            $("#wgShiftShoose_chosen").css({width: "100%"});
            $("#planer-shift-controler").css({overflow: "visible"});

            // load dialog (delivery)
            GetDialog("planer-shift-controler", 300, "auto", buttons, `center center`);

            // set dialog and dialog back holder z position
            $(`.ui-widget-overlay`).css({zIndex: "100"});
            $(`.ui-dialog`).css({zIndex: "101"});

        }

      });

    });

    // click on ediut history pointer
    $(document).on("click", ".col-edit-history-pointer", function(event) {

      // stop parent element events
      event.stopPropagation();

      // define row and column id
      const rowId = $(this).data("row");
      const colId = $(this).data("col");

      // define dialog request data
      const data = {
        act: "get_col_edit_history_dialog"
      }

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // destruct responce
        const { error, structure } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // define dialog buttons
          const buttons = {
              "cancel": {
                  text: "დახურვა",
                  id: "cancel-wg-eh-dialog",
                  click: function () {

                    // close dialog
                    $(this).dialog("close");

                  }
              }
          };

          // insert structure in dialog
          $("#graphic-edit-history").html(structure);

          // load history table
          const tableLoadData = `project_id="${projectDetails.originalId}"&rowId="${rowId}"&colId="${colId}"`;
          GetDataTable("work_graphic_edit_history_table", aJaxURL, "get_work_graphic_edit_history", 6, tableLoadData, 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");

          // open dialog
          GetDialog("graphic-edit-history", 960, "auto", buttons, "center center");

          // set dialog and dialog back holder z position
          $(`.ui-widget-overlay`).css({zIndex: "100"});
          $(`.ui-dialog`).css({zIndex: "101"});

        }

      });      

    });

    // remove shift from graphic column
    $(document).on("click", ".remove-shift-from-graphic", function() {

      // define request data
      const data = {
        act: "remove_shift_from_graphic_col",
        delElementId: $(this).data("id")
      }

      // define remove element
      const removeElement = $(".wg-slc-list-unit").length === 1 ? 
                            $(".wg-shift-list-container") : 
                            $(this).parents(".wg-slc-list-unit");

      // start loading
      $(".project-details-loading-container").addClass("active");

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // rstruct responce
        const { error, deleteStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check delete status
          if(deleteStatus) {

            // update graphic
            workGraphic.build(() => {

              // remove element from list
              removeElement.remove();

            });

          } else {
            alert("გაირკვეველი მიზეზების გამო მოქმედება ვერ განხორციელდა");
          }

        }

      });

    });

    // remove shift from planner column
    $(document).on("click", ".remove-shift-from-planner", function() {

      // define request data
      const data = {
        act: "remove_shift_from_planner_col",
        delElementId: $(this).data("id")
      }

      // define remove element
      const removeElement = $(".wg-slc-list-unit").length === 1 ? 
                            $(".wg-shift-list-container") : 
                            $(this).parents(".wg-slc-list-unit");

      // start loading
      $(".project-details-loading-container").addClass("active");

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // rstruct responce
        const { error, deleteStatus } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // check delete status
          if(deleteStatus) {

            // update graphic
            workPlanner.build(() => {

              // remove element from list
              removeElement.remove();

            });

          } else {
            alert("გაირკვეველი მიზეზების გამო მოქმედება ვერ განხორციელდა");
          }

        }

      });

    });

    // get shift edit history
    $(document).on("click", ".get-edit-history", function(event) {

      // stop parent event actions
      event.stopPropagation();

      // define row and col id
      const parent = $(this).parent(".work-shift");
      const rowId = parent.data("row");
      const colId = parent.data("col");

      // define request data
      const data = {
        act: "get_work_graphic_edit_history_dialog"
      }

      // send ajax request
      $.post(aJaxURL, data, responceData => {

        // restruct responce data
        const { structure } = responceData;

        // insert responced data in relavant pdialog
        $("#graphic-edit-history").html(structure);

        // define dialog buttons
        const buttons = {
            "cancel": {
                text: "დახურვა",
                id: "cancel-wg-eh-dialog",
                click: function () {

                  // close dialog
                  $(this).dialog("close");

                }
            }
        };

        // open dialog
        GetDialog("graphic-edit-history", 700, "auto", buttons, `center center-100`);

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

        // load history table
        const tableLoadData = `project_id="${projectDetails.originalId}"&row_num="${rowId}"&string_date="${$(".wgt-day-dialog-open").eq(colId).data("date")}"`;
        GetDataTable("work_graphic_edit_history_table", aJaxURL, "get_work_graphic_edit_history", 5, tableLoadData, 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");


      });

    });

    // when work graphic month list is changed
    $(document).on("change", "#choosWorkGraphicMonth", function() {

      // define value
      let value = parseInt($(this).val());

      // check value
      if(value !== -1) {

        // change work graphic month
        workGraphic.currentMonth = value;

        // recalculate days count in month and rebuild work graphic table
        workGraphic.updateHoursRowLength()
                   .build();

      }

    });

    //------------ DAY DIALOG ACTIONS ---------------

    // day graphic table actions (CLASS STARTS)
    class WorkDayGraphic {

      // mane constructor method
      constructor(stringdate = "") {

        // define this class params
        this.date = new Date();
        this.stringDate = stringdate;  
        this.details = this.processeStringDate();  
        this.sumOfBreakTimeToNum = 0;
        this.sumOfActivitiesTime = 0;
        this.selectStart = 0;
        this.selectStarted = false;
        this.selectedColVals = [];
        this.chosedActivitie = {};
        this.selectorResponce = false;
        this.moveSelector = false;
        this.graphicScrollPosition = 0;
        this.rowControler = [];
        this.layout = {
          type: "graphic",
          structure: "",
          activitiesCount: 0
        };
      }

      // processe string date
      processeStringDate() {

        // split string date
        const splitedDate = this.stringDate.split(".");

        // define date object format
        const formatedDate = `${splitedDate[2]}-${splitedDate[1]}-${splitedDate[0]}`;

        // return values object
        return {
          colId: Number(splitedDate[0]) - 1,
          month: Number(splitedDate[1]),
          year: Number(splitedDate[2]),
          dayIndex: new Date(formatedDate).getDay() - 1 < 0 ? 6 : new Date(formatedDate).getDay() - 1
        }

      }

      // update activities in work graphgic row controler
      updateWorkGraphicActivities(callback) {

        this.colData.forEach(item => {

          // define row controler index
          let rowIndex = item.num - 1;

          // insert activities in relevant row controler shift
          workGraphic.rowControler[rowIndex].shiftList[this.colId].activities = [...item.activities];

        });

        // execute callback if is set
        if(callback) callback();

      }

      // returns operator row structure
      operatorRowStructure(rowId, operatorData) {

        // restruct operator data
        let { num, operator } = operatorData;

        return `<div class="row" data-row="${rowId}">
                  <div class="col content-center">
                    ${num}
                  </div>
                  <div class="col">
                    ${operator}
                  </div>
                </div>`;

      }

      // returns value table row structure
      valueTableRowStructure(rowId = 0, rowNum, valuedata) {
        
        // define structure collector
        let structure = `<div class="row" data-row="${rowId}">`;

        // check for layout type
        if(this.layout.type === "graphic") {
          
          // restruct value data
          const { shiftData, activities, rushHours } = valuedata;
          
          // define variables
          let activitieId, backgroundColor, rushHourBg;

          // loop for 24 hours divided on 5 minutes
          for(let i = 0; i < 288; i++) {
            
            // define work time status
            let isWorkTime = shiftData.some(item => hours.convertToNum(5, item.shiftStartTime) <= i && hours.convertToNum(5, item.shiftEndTime) > i);
            let status = isWorkTime ? 'work-time' : '';

            // define activitie
            let activitie = activities.find(item => Number(item.activitieStart) <= i && Number(item.activitieEnd) >= i);

            // check for activities
            if(activitie === undefined || activitie === "") {              
              activitieId = -1;
              backgroundColor = '';
            } else {
              activitieId = activitie.activitieId;
              backgroundColor = activitie.activitieColor;
            }

            // check for rush hours
            if(rushHours && rushHours !== undefined && rushHours !== null) {

              // destruct rush hours
              const { dayIndex, start, end } = rushHours;
              
              // check for status
              if(status === 'work-time' && (i >= start && i <= end)) {
                rushHourBg = 'url(./media/images/rush_hour_patern.png)';
              } else {
                rushHourBg = 'none';
              }

            }            

            // collect structure
            structure += `<div class="minute-col" data-status="${status}" data-activitie="${activitieId}" data-row="${rowId}" data-col="${i}" style="background-color: ${backgroundColor}; background-image: ${rushHourBg};">

                          </div>`;

        	}

        } else if(this.layout.type === "table") {

          // restruct value data
          const { activities } = valuedata;
        
          // define variables
          let activitieId;

          // loop for 24 hours divided on 5 minutes
          for(let i = 0; i < this.layout.activitiesCount; i++) {

            // ndefine activitie id by columns
            let activitieId = $(".wg-dg-body .value-table .head").find(".table-col").eq(i).data("id");
            
            // define activitie start, end and full time collectors
            let activitieFullTime = 0;

            // get filtered activities array
            let filteredActivities = activities.filter(item => Number(item.activitieId) === Number(activitieId));
            
            // check filtered activities length
            if(filteredActivities.length) {

              // loop throw activities array
              filteredActivities.forEach(item => {
                activitieFullTime += Number(item.activitieEnd) - Number(item.activitieStart) + 1;

              });

            }

            // collect structure
        		structure += `<div class="activitie-col">
                            <span>
                              ${activitieFullTime ? hours.convertFromNum(5, activitieFullTime) : '00:00'}
                            </span>
        			            </div>`;
        	}

        }

        structure += `</div>`;


        return structure;

      }

      // returns hours summary row structure
      hoursSummaryRowStructure(rowId = 0, data) {
        
        // restruct data
        const { shiftData, activities } = data;

        // define time collectors
        let workTime = "00:00:00";
        let breakTime = "00:00:00";
        
        // sum work and break times
        shiftData.forEach(item => {

          workTime = hours.add(workTime, item.shiftWorkTime);
          breakTime = hours.add(breakTime, item.shiftTimeout);

        });

        // convert break string time to column numbers
        let breakTimeToNum = hours.convertToNum(5, breakTime);

        // summary break times
        this.sumOfBreakTimeToNum += breakTimeToNum;

        // define and colloect activities
        let activitieCollector = 0;

        // loop throw activities
        activities.forEach(item => {
          activitieCollector += Number(item.activitieEnd) - Number(item.activitieStart) + 1;
        });

        // summary activitieTimes times
        this.sumOfActivitiesTime += activitieCollector;
        
        // define status and percent variables
        let status, widthPrecent, percent = 0, percentText = "0%";

        // check for breakTimeToNum and activitieCollector
        if((breakTimeToNum !== undefined && activitieCollector !== undefined) && 
           (breakTimeToNum !== null && activitieCollector !== null) &&
           (activitieCollector !== 0 && breakTimeToNum !== 0)) {

            percent = parseInt(activitieCollector / breakTimeToNum * 100);
            widthPrecent = percent <= 100 ? percent : 100;
            percentText = `${percent}%`;
            status = this.getPercentStatus(percent);
        } else if(activitieCollector !== 0 && breakTimeToNum === 0) {
            widthPrecent = 100;
            percentText = "~";
            status = "over";
        }      

        return `<div class="row" data-row="${rowId}">
                  <div class="col">
                    <span>${workTime}</span>
                  </div>
                  <div class="col flex-width">
                    <span>${breakTime}</span>
                  </div>
                  <div class="col">
                    <div class="precent-wrapper" data-status="${status}">
                      <div class="value" style="width: ${widthPrecent}%;"></div>
                      <span>${percentText}</span>
                    </div>
                  </div>
                </div>`;

      }

      // set titling date
      setTitlingDate() {

        $(".wg-dg-header .date-receiver").text(this.stringDate);
        return this;
      }

      // updates rows in table
      updateRows() {

        // define row collectors
        let operatorsRows = "";
        let valueTableRows = "";
        let hoursSummaryRows = "";

        // execute row loop
        this.rowControler.forEach((item, i) => {
          
          // define operator data
          let operatorData = {
            num: item.num,
            operator: item.operator
          };

          // define value table data
          let valueTableData = {
            shiftData: item.shiftData,
            activities: item.activities,
            rushHours: item.rushHours.find(hourItem => Number(hourItem.dayIndex) === this.details.dayIndex)
          };

          // define hours summary datra
          let hoursSummaryData = {
            shiftData: item.shiftData,
            activities: item.activities
          };
          
          // collecting structures
          operatorsRows += this.operatorRowStructure(item.rowId, operatorData);
          valueTableRows += this.valueTableRowStructure(item.rowId, i, valueTableData);
          hoursSummaryRows += this.hoursSummaryRowStructure(item.rowId, hoursSummaryData);

        });

        // insert rows in relevant parents
        $(".wg-dg-body").find(".operators .content .body").html(operatorsRows);
        $(".wg-dg-body").find(".value-table .content .body").html(valueTableRows);
        $(".wg-dg-body").find(".hours-summary .content .body").html(hoursSummaryRows);

        // return object itself
        return this;

      }

      // select legal value columns
      selectBreakCols(data, callback) {

        // destruct data
        let {row, col} = data;

        // define variables mikimouse
        let rowIndex = $(row).data("row");
        let colBlock = $(row).find(`.minute-col[data-col="${col}"]`);
        let isSelectable = colBlock.attr("data-status") === "break-time";
        let lastIndex = this.selectedColVals.length - 1;

        // check if column is selectable and push the value in selected columns array
        if(isSelectable) {

          // define column data
          let colData = {
            rowIndex,
            colIndex: col
          }

          /*check for items in array... If item not exists, push it in array,
          and if col number exists in array and is not equivalent of last value in array, remove all values after selected value*/
          if(this.selectStarted) {
            this.selectedColVals.length = 1;
            this.selectedColVals[0] = colData;
          } else if(this.selectedColVals.every(item => item.colIndex !== colData.colIndex)) {
            this.selectedColVals.push(colData);
          } else if(this.selectedColVals.some(item => item.colIndex === colData.colIndex) && (this.selectedColVals.findIndex(item => item.colIndex === colData.colIndex) !== lastIndex)) {
            this.selectedColVals.length = this.selectedColVals.findIndex(item => item.colIndex === colData.colIndex) + 1;
          }

        }

        // execute callback
        callback(isSelectable);

      }

      // update column activitie percent
      upateColumnActivitiePercent() {

        // define update data
        const updateData = {
          colId: this.details.colId,
          timeoutInSeconds: hours.secondsFromNum(5, this.sumOfBreakTimeToNum),
          activitieInSeconds: hours.secondsFromNum(5, this.sumOfActivitiesTime)
        }

        // update work graphic column data
        workGraphic.updateSingleColActivitiePercent(updateData);

        // return object itself
        return this;

      }

      // dsitribute chosed activitie in selected columns
      distributeActivities() {

        // loop throw selected column values
        this.selectedColVals.forEach(item => {

          // define selected data
          let dividedActivitieLeft = {};
          let dividedActivitieRight = {};
          let select = {
            ...this.chosedActivitie,
            ...item
          }

          // copy activities array
          let activitiesArrayCopy = [...this.colData[item.row].activities];

          activitiesArrayCopy.map((activitie, index) => {

            // check for exact concurence
            if(item.start <= activitie.start && item.end >= activitie.end) {

              // remove activitie
              activitie.start = -1;
              activitie.end = -1;

            } else if(item.end >= activitie.start && item.start <= activitie.start) {

              // redefine activitie start
              activitie.start = item.end + 1;

            } else if(item.start <= activitie.end && item.end >= activitie.end) {

              // redefine activitie end
              activitie.end = item.start - 1;

            } else if(item.start > activitie.start && item.end < activitie.end) {

              // define divided activitie left side
              dividedActivitieLeft = {...activitie};
              dividedActivitieLeft.end = item.start;

              // define divided activitie right side
              dividedActivitieRight = {...activitie};
              dividedActivitieRight.start = item.end;

              // remove old activitie
              activitie.start = -1;
              activitie.end = -1;

            }

          });

          // push selected activitie in array after check for clearing activities
          if(select.id !== 0 && select.color !== "") {
            activitiesArrayCopy.push(select);
          }

          // check for divided activities
          if(dividedActivitieLeft.hasOwnProperty("start") && dividedActivitieRight.hasOwnProperty("start")) {
            activitiesArrayCopy.push(dividedActivitieLeft, dividedActivitieRight);
          }

          // update column data activities array
          this.colData[item.row].activities = activitiesArrayCopy.filter(item => item.start !== -1 && item.start !== -1);

        });

        // return object itself
        return this;

      }

      // select columns
      selectColumns() {

        // remove all selectors
        $(".wg-dg-body .minute-col").find(".selector").remove();

        // loop throw selected rows
        workDayGraphic.selectedColVals.forEach(item => {

          // loop throw selected columns
          $(".wg-dg-body .value-table .body").find(`.row[data-row="${item.row}"]`).find(".minute-col").each(function() {

            // define column num
            let dataCol = $(this).data("col");

            // make selection
            if(dataCol === item.start) {
               $(this).html(`<div class='selector' data-type='start'></div>`);
            } else if(dataCol > item.start && dataCol < item.end) {
              $(this).html(`<div class='selector' data-type='middle'></div>`);
            } else if(dataCol === item.end) {
              $(this).html(`<div class='selector' data-type='end'></div>`);
            }

          });

        });

        // return object itself
        return this;

      }

      // nullify selector mover propertyes
      clearSelector() {

        $(".wg-dg-body .minute-col").find(".selector").remove();
        workDayGraphic.selectedColVals = [];

        // return object itself
        return this;

      }

      // get shift activitie update data
      getShiftActivitieUpdateData() {

        // define activities update array
        return this.colData.filter(filtItem => filtItem.activities.length !== 0).map(mapItem => {

          return {
            projectId: projectDetails.originalId,
            rowId: mapItem.num - 1,
            date: this.stringDate,
            shiftId: mapItem.workShiftId,
            activities: mapItem.activities
          }
        });

      }

      // calculate operator presence
      calcOperatorPresence() {

        // execute loop throw individual column
        for(let i = 0; i < 288; i++) {

          // define calculators
          let onPlace = 0;
          let isOut = 0;

          // execute loop throw individual row
          $(".wg-dg-body .value-table .body").find(".row").each(function() {

            // define variables
            let col = $(this).find(`.minute-col[data-col="${i}"]`);
            let status = col.data("status");
            let activitie = col.data("activitie") ? col.data("activitie") : '';

            if(status === "work-time" && activitie > 0) {
              isOut++;
            } else if(status === "work-time" && activitie < 0) {
              onPlace++;
            }

          });

          // checking for presence status
          let presenceStatus = this.getPresenceStatus(isOut, onPlace);

          // insert data in relevant columns
          $(".wg-dg-body .value-table .footer .status").find(`.col[data-col="${i}"]`).attr("data-status", presenceStatus).text(isOut);
          $(".wg-dg-body .value-table .footer .on-place").find(`.col[data-col="${i}"]`).text(onPlace);

        }

        // return object itself
        return this;

      }

      // get percent status
      getPresenceStatus(isOut, onPlace) {
        if(isOut === 0 && onPlace !== 0) {
          return "all-in";
        } else if(isOut > 0 && isOut < onPlace) {
          return "more-in";
        } else if(isOut === onPlace && (isOut !== 0 && onPlace !== 0)) {
          return "in-out";
        } else if(isOut > onPlace && onPlace > 0) {
          return "more-out";
        } else if(isOut > onPlace && onPlace === 0) {
          return "all-out";
        }
      }

      // get status of percent
      getPercentStatus(percent) {

        // check percents
        if(percent < 15) {
          return "very-weak";
        } else if(percent >= 15 && percent < 30) {
          return "weak";
        } else if(percent >= 30 && percent < 45) {
          return "over-weak";
        } else if(percent >= 45 && percent < 60) {
          return "half";
        } else if(percent >= 60 && percent < 75) {
          return "before-strong";
        } else if(percent >= 75 && percent < 90) {
          return "strong";
        } else if(percent >= 90 && percent < 100) {
          return "over-strong";
        } else if(percent === 100) {
          return "exact";
        } else if(percent > 100) {
          return "over";
        }

      }

      // change layout
      changeLayout() {

        // restruct and define data
        const { type, structure, activitiesCount } = this.layout;
        let sliderWidth = type === "graphic" ? 7200 : (activitiesCount * 210);
        let frameWidth = $(".wg-dg-body .value-table").width();

        // check for slider width once more to avoid invalid strucutre
        if(sliderWidth < frameWidth) sliderWidth = frameWidth;

        // change slider width
        $(".wg-dg-body .scroll .maker").css({width: `${sliderWidth}px`});
        $(".wg-dg-body .slider").css({width: `${sliderWidth}px`});

        // insert received structure in relevant DOM
        $(".wg-dg-body .value-table .content .head").html(structure);

        // hide and show footer
        if(type === "table") {
          $(".wg-dg-body .footer").addClass("hidden");
        } else {
          $(".wg-dg-body .footer").removeClass("hidden");
        }

        // update rows
        this.updateRows();

      }

      // imitilize events
      initEvents() {

        // define object
        const self = this;

        // scroll the graphic table
        $(".wg-dg-body .value-table .scroll").scroll(function() {

            // define scroll position
            self.graphicScrollPosition = $(this).scrollLeft();

            // regulate left position
            if(self.graphicScrollPosition > 10) {
              self.graphicScrollPosition -= 1;
            }

            // change position of the table
            $(".wg-dg-body .slider").css({left: `-${self.graphicScrollPosition}px`});

        });

        // return object itself
        return this;

      }

      // nullify data
      nullifyData() {

        this.sumOfBreakTimeToNum = 0;
        this.sumOfActivitiesTime = 0;

      }

      // fetch build data
      fetchBuildData(callback) {
        
        // define this pointer
        const self = this;

        // define request data
        const data = {
          act: "get_project_day_graphic_build_data",
          projectId: projectDetails.originalId,
          ...self.processeStringDate()
        }
        
        // send post request
        $.ajax({
          type:"POST",
          url: aJaxURL,
          cache: false,
          data,
          beforeSend() {

            // check if loading already going on
            if(!$(".project-details-loading-container").hasClass("active")) {

              // start loading
              $(".project-details-loading-container").addClass("active");
            }

            // nullify data
            self.nullifyData();

          },
          success(responce) {

            // handle responce error
            responceErrorHandler(responce, data => {

              // restruct data
              const { tableStructure, rowControler } = data;
              
              if(rowControler.length) {

                // set row controller
                self.rowControler = [...rowControler];

                // insert day graphic structure
                $(".project-details-day-graphic-container").addClass("active").html(tableStructure);

                // set table scroll position
                $(".wg-dg-body .value-table .scroll").scrollLeft(self.graphicScrollPosition);

                // execute collback
                if(callback) callback();

              } else {
                alert("მონიშნულ თარიღში მონაცემები ვერ მოიძებნა");
              }
              

            });

          },
          complete() {
            
            // end loading
            setTimeout(() => {
                $(".project-details-loading-container").removeClass("active");
            }, 1000);

          }
        });

      }

      // build day graphic table
      build(callback) {
        
        // fetch build data
        this.fetchBuildData(() => {

            // init table
            this.setTitlingDate()
                .updateRows()
                .calcOperatorPresence()
                .initEvents();

            // check for callback
            if(callback) callback(this);

        });

      }

    }
    // day graphic table actions (CLASS ENDS)


    //open day dialog
    $(document).on("click", ".wgt-day-dialog-open:not(.is-holiday)", function() {

      // define date
      const date = $(this).data("date");

      // define and build work day graphic
      workDayGraphic = new WorkDayGraphic(date);
      workDayGraphic.build();

    });


    //CLICK ON SELECTOR
    $(document).on("mouseup", ".wg-dg-body .value-table .body .selector", function(event) {

      // define request data
      const data = {
        act: "get_activitie_dialog"
      }

      // send ajax request
      $.post(aJaxURL, data, result => {

        // restruct result
        let { error, dialog } = result;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // define dialog buttons mikimouse
          const buttons = {
              "save": {
                  text: "შენახვა",
                  id: "save-wg-cha-dialog",
                  click: function () {

                    // define save activitie data
                    const data = {
                      act: "save_day_graphic_activities",
                      activitieId: $("#chooseDayGraphicActivitie").val(),
                      colId: workDayGraphic.processeStringDate().colId,
                      rows: workDayGraphic.selectedColVals
                    }

                    // start loading
                    $(".project-details-loading-container").addClass("active");

                    // send ajax request
                    $.post(aJaxURL, data, responce => {

                      // restruct responce
                      const { error, saveStatus } = responce;

                      // check for error
                      if(error !== "") {
                        alert(error);
                      } else {

                        // check for save status
                        if(saveStatus) {

                          // update column activitie percent and rebuild work day graphic
                          workDayGraphic.build(wdg => {
                            wdg.upateColumnActivitiePercent();
                          });           

                        } else {

                          // alert error
                          alert("გაურვეველი მიზეზების გამო, თქვენ მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");

                          // end loading
                          $(".project-details-loading-container").removeClass("active");
                        }

                        // close dialog
                        $(this).dialog("close");
                      }
                    });

                  }
              },
              "cancel": {
                  text: "დახურვა",
                  id: "cancel-wg-cha-dialog",
                  click: function () {

                    // clear all selectors
                    workDayGraphic.clearSelector();

                    // close dialog
                    $(this).dialog("close");

                  }
              }
          };

          // insert structure in dialog
          $("#workGraphic-choose-activitie").html(dialog).css({overflow: "visible"});

          // stylize html elements
          $("#chooseDayGraphicActivitie").chosen();
          $("#chooseDayGraphicActivitie_chosen").css({width: "100%"});

          // nullify chosed activitie data
          workDayGraphic.chosedActivitie = {
            id: 0,
            color: ""
          };

          // open dialog
          GetDialog("workGraphic-choose-activitie", 300, "auto", buttons, "center center");

          // set dialog and dialog back holder z position
          $(`.ui-widget-overlay`).css({zIndex: "100"});
          $(`.ui-dialog`).css({zIndex: "101"});

        }

      });

    });

    // CHANGE ACTIVITIE CHOOSE SELECT
    $(document).on("change", "#chooseDayGraphicActivitie", function() {

      // define variables
      let activtieData = JSON.parse($(this).find("option:selected").attr("value"));

      // save chosed activitie in object propertie
      workDayGraphic.chosedActivitie = activtieData;

    });


    // MOUSE DOUN ON MINUTE COLUMNS
    $(document).on("mousedown", ".wg-dg-body .minute-col", function() {

      // check for selector child element
      let selectorChild = $(this).find(".selector");

      // clear all selectors
      workDayGraphic.clearSelector();

      // define column status
      let status = $(this).data("status");

      // check if culumn status equivalent to work time
      if(status === "work-time") {

        // define select data
        let select = {
          row: $(this).data("row"),
          start: $(this).data("col"),
          end: $(this).data("col")
        };

        // fix select start position
        workDayGraphic.selectStart = select.start;

        // save select data in relevant array
        workDayGraphic.selectedColVals.push(select);

        // select event target column
        workDayGraphic.selectColumns();

      }

    });

    // MOUSE OVER ON MINUTE COLUMNS
    $(document).on("mouseover", ".wg-dg-body .minute-col[data-status='work-time']", function(event) {

      // check if mouse button is clicked
      if(event.buttons === 1) {

        // define colun data
        let row = $(this).data("row");
        let col = $(this).data("col");
        let startCol = workDayGraphic.selectedColVals[0].start;

        workDayGraphic.selectedColVals.forEach((select, index) => {

          if(select.row === row) {

            if(col <= workDayGraphic.selectStart) {
              select.end = workDayGraphic.selectStart;
              select.start = col;
            } else {
              select.end = col
            }

          }

        });

        // select event target column
        workDayGraphic.selectColumns();

      }

    });

    // CHANGE WORK DAY GRAPHIC LAYOUT
    $(document).on("click", ".wg-dg-table-view-changer:not(.active)", function() {

      // deactivate all buttons
      $(".wg-dg-table-view-changer").removeClass("active");

      // activate clicked
      $(this).addClass("active");

      // define ajax request data
      const data = {
        act: "change_dg_layout",
        type: $(this).data("type")
      };

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // restruct responce
        let { error } = responce;

        // check for error
        if(error !== "") {
          alert(error);
        } else {

          // change day graphic layout
          workDayGraphic.layout = responce;
          workDayGraphic.changeLayout();

        }

      });

    });

    // CLOSE WORK DAY GRAPHIC DIALOG
    $(document).on("click", "#wgDgDialogCloser", () => {

      // remove work day graphic dialog container from dom
      $(".project-details-day-graphic-container").removeClass("active").html("");

    });


/*=================================================================================== WORKGRAPHIC DYRECTORY ACTIONS ENDS ==========================================================================*/


/*======================================================================================= RUSH HOURS ACTIONS STARTS ==============================================================================*/

  // rush hours table actions (CLASS STARTS) rushy
  class RushHours {

    // main constructor method
    constructor() {
      this.currentYear = new Date().getFullYear();
      this.changedYear = '';
      this.chooseStatus = 1;
      this.selectStart = 0;
      this.selectedColVals = {};
      this.selectedColumns = [];
      this.compareSelectedColumns = [];
    }

    // updates value table rows
    rowUpdate() {

      // loop throw week days
      for(let i = 0; i < 7; i++) {

        // define column structure
        let colStructure = this.valueTableColStruct(i);

        // insert structure in relevant row
        $(".rush-hour-body .value-table .body").find(".row").eq(i).html(colStructure);

      }

      // update save changes button status
      this.updateSvageButtonStatus();

      // return object itself
      return this;

    }

    // returns value table column structure
    valueTableColStruct(rowid) {

      // define structure collector
      let structure = '';

      // loop throw 24 hours / 5 minutes count
      for(let i = 0; i < 288; i++) {

        // define status
        let isSelected = this.selectedColumns.some(item => rowid === item.row && (i >= item.start && i <= item.end ));

        structure += `<div class="minute-col" data-status="${isSelected ? "selected" : ""}" data-activitie="0" data-row="${rowid}" data-col="${i}">
                      </div>`;
      }

      // return structure
      return structure;

    }

    // change year
    updateWeekList(callback) {

      // define ajax request data
      const data = {
        act: "update_week_nums_by_year",
        year: this.changedYear
      };

      // send ajax request
      $.post(aJaxURL, data, responce => {

        // checking for error
        responceErrorHandler(responce, data => {

          // restruct data
          let { weekList } = data;

          // insert list in relevant parent dom
          $("#chooseRushHourWeek").html(weekList).trigger("chosen:updated");

          // execute callback
          if(callback) callback();

        });

      });

    }

    // refresh year chose select to current year
    refreshYear() {

      // define data
      let chooseYear = $("#chooseRushHourYear");
      let allOptions = chooseYear.find("option");
      let currentYearOption = chooseYear.find("#rushHoursCurrentYear");

      // nullify all options
      allOptions.prop("selected", false);

      // select current year option
      currentYearOption.prop("selected", true);

      // refresh select
      chooseYear.trigger("chosen:updated");

      // return object itself
      return this;

    }

    // refresh year and week number... selects current data
    refreshDate() {

      // set changed year to currents year
      this.changedYear = this.currentYear;

      // refresh year and update week list
      this.refreshYear()
          .updateWeekList(() => this.build());
    }

    // nullify selector mover propertyes
    clearSelector() {

      $(".rush-hour-body .minute-col").find(".selector").remove();
      this.selectedColVals = {};

      // return object itself
      return this;

    }

    // select columns
    selectColumns() {

      // remove all selectors
      $(".rush-hour-body .minute-col").find(".selector").remove();

      // define selected column values
      let { row, start, end } = this.selectedColVals;

      // loop throw selected columns
      $(".rush-hour-body .value-table .body").find(`.row[data-row="${row}"]`).find(".minute-col").each(function() {

        // define column num
        let dataCol = $(this).data("col");

        // make selection
        if(dataCol === start) {
           $(this).html(`<div class='selector' data-type='start'></div>`);
        } else if(dataCol > start && dataCol < end) {
          $(this).html(`<div class='selector' data-type='middle'></div>`);
        } else if(dataCol === end) {
          $(this).html(`<div class='selector' data-type='end'></div>`);
        }

      });

      // return object itself
      return this;

    }

    // make selection
    makeSelection() {

      // define new selected column values
      let selection = {
        ...this.selectedColVals
      };

      // check selected column values
      this.selectedColumns.map((item, index) => {

        if(selection.row === item.row && selection.start < item.start && ((selection.end >= item.start || selection.end === item.start - 1) && selection.end <= item.end)) {
          selection.end = item.end;
          item.start = -1;
        } else if(selection.row === item.row && selection.end > item.end && (selection.start >= item.start && (selection.start <= item.end || selection.start === item.end + 1))) {
          selection.start = item.start;
          item.end = -1;
        } else if(selection.row === item.row && (selection.start >= item.start && selection.end <= item.end)) {
          selection.start = -1;
        } else if(selection.row === item.row && (selection.start < item.start && selection.end > item.end)) {
          item.start = -1;
        }

      });

      // push new selection in array
      this.selectedColumns.push(selection);

      // filter selection columns
      this.selectedColumns = this.selectedColumns.filter(item => item.start !== -1 && item.end !== -1);

    }

    // clear selection
    clearSelection() {

      // define new selected column values
      let selection = {
        ...this.selectedColVals
      };

      // check selected column values
      this.selectedColumns.map((item, index) => {

        if(selection.row === item.row && selection.start <= item.start && (selection.end >= item.start && selection.end < item.end)) {
          item.start = selection.end + 1;
        } else if(selection.row === item.row && selection.end >= item.end && (selection.start > item.start && selection.start <= item.end)) {
          item.end = selection.start - 1;
        } else if(selection.row === item.row && (selection.start > item.start && selection.end < item.end)) {

          // define selection start saver
          const newItem = {
            row: item.row,
            start: selection.end + 1,
            end: item.end
          };

          item.end = selection.start - 1;

          this.selectedColumns.push(newItem);

        } else if(selection.row === item.row && (selection.start <= item.start && selection.end >= item.end)) {
          item.start = -1;
        }

      });

      // filter selection columns
      this.selectedColumns = this.selectedColumns.filter(item => item.start !== -1 && item.end !== -1);

    }

    // checks if selected column values has been changed
    selectedValuesChanged() {

      // define original and compare array length
      const originalLength = this.selectedColumns.length;
      const compareLength = this.compareSelectedColumns.length;

      // check for length
      if(originalLength !== compareLength) return true;

      // if selected columns array and comapre array length equals, check
      // for value euqal
      for(let i = 0; i < originalLength; i++) {

        // define check item
        let checkItem = this.selectedColumns[i];

        // define compare status
        let isChanged = this.compareSelectedColumns.every(item => {
          return item.row !== checkItem.row || item.start !== checkItem.start || item.end !== checkItem.end
        });

        // return true if array was changed
        if(isChanged) {
          return true;
        }

      }

      // return fals anyway
      return false;

    }

    // update save changes button status
    updateSvageButtonStatus() {

      // check if selected columns array was changed
      if(this.selectedValuesChanged()) {
        $("#saveRushHours").prop("disabled", false);
      } else {
        $("#saveRushHours").prop("disabled", true);
      }

    }

    // makie selected columns copy
    makeSelColCopy() {

      // nullify compare array
      this.compareSelectedColumns = [];

      // check selected columns length
      if(this.selectedColumns.length) {

        // loop throw selected columns
        this.selectedColumns.forEach((item, index) => {

          // asign copied valye to compare array values
          this.compareSelectedColumns[index] = {
            ...item
          }
        });

      }

      // return object itself
      return this;

    }

    // copy previous week data
    copyPrevWeekData() {

      // define this ponter
      const self = this;

      // define request data
      const data = {
        act: "copy_prev_week_data",
        projectId: projectDetails.originalId,
        ...this.getPrevWeekNum()
      };

      // send axaj request
      $.post(aJaxURL, data, responce => {

        // handle responce error
        responceErrorHandler(responce, data => {

          // restruct data
          const { responceMessage } = data;

          // check for message
          if(responceMessage === "not-exists") {
            alert("დასაკოპირებელი მონაცემები ვერ მოიძებნა");
          } else {
            self.build();
          }

        });

      });

    }

    // get previous week number
    getPrevWeekNum() {

      // define data
      const curWeekNum = parseInt($("#chooseRushHourWeek").val());
      const year =  curWeekNum - 1 < 0 ? parseInt($("#chooseRushHourYear").val()) - 1 : parseInt($("#chooseRushHourYear").val());
      const prevWeekNum = curWeekNum - 1 < 0 ? 53 : curWeekNum - 1;

      // return data
      return {
        year,
        curWeekNum,
        prevWeekNum
      }

    }

    // clears one specific row
    clearRow(rowid) {

      // check if data exists
      if(this.selectedColumns.some(item => item.row === rowid)) {

        // define filtered array
        const filteredArray = this.selectedColumns.filter(item => item.row !== rowid);

        // update selected columns array
        this.selectedColumns = [...filteredArray];

        // update rows
        this.rowUpdate();

      }

      // return object itself
      return this;

    }

    // initilize events
    initEvents() {

      // scroll the rush hour
      $("#rushHoursTableScroll").scroll(function() {

          // define scroll position
          let leftPos = $(this).scrollLeft();

          if(leftPos > 10) {
              leftPos -= 1;
          }

          // change position of the table
          $("#rushHoursTableSlider").css({left: `-${leftPos}px`});

      });

    }

    // fetch build data
    fetchBuildData(callback) {

      // define this pointer
      const self = this;

      // send post request
      $.ajax({
        type:"POST",
        url: aJaxURL,
        cache: false,
        data: {
          act: "fetch_rush_hours_build_data",
          projectId: projectDetails.originalId,
          year: $("#chooseRushHourYear").val(),
          week: $("#chooseRushHourWeek").val()
        },
        beforeSend() {

          // check if loading already going on
          if(!$(".project-details-loading-container").hasClass("active")) {

            // start loading
            $(".project-details-loading-container").addClass("active");
          }

        },
        success(responce) {

          // handle responce error
          responceErrorHandler(responce, data => {

            // restruct data
            const { colData } = data;

            // set data
            self.selectedColumns = colData;

            // make data copy
            self.makeSelColCopy();

          });

          // execute collback
          if(callback) callback();

        },
        complete() {
          
          // end loading
          setTimeout(() => {
              $(".project-details-loading-container").removeClass("active");
          }, 1000);

        }
      });

    }

    // builds value table
    build(callback) {

      // get built data
      this.fetchBuildData(() => {

        // update rows
        this.rowUpdate();

        // init events
        this.initEvents();

        // execute callback
        if(callback) callback();

      });

      return this;

    }

  }

  // define rush hours object
  const rushHours = new RushHours();
  // rush hours table actions (CLASS ENDS)


  // CHNAGE YEAR AND UPDATE WEEK CHOOSE SELECT (depended on year)
  $(document).on("change", "#chooseRushHourYear", function() {

    // define chosed year
    rushHours.changedYear = $(this).val();

    // update week list
    rushHours.updateWeekList(() => rushHours.build());

  });

  // CHNAGE WEEK NUMBER
  $(document).on("change", "#chooseRushHourWeek", () => rushHours.build());


  // REFRESH DATE TO CURRENT DATE
  $(document).on("click", "#refreshRushHoursDate", () => rushHours.refreshDate());

  //CLICK ON SELECTOR
  $(document).on("mouseup", ".rush-hour-body .value-table .body .selector", function(event) {

    // reset choosed status
    rushHours.chooseStatus = 1;

    // define ajax request data
    const data = {
      act: "get_rush_hours_select_structure"
    }

    // send ajax request
    $.post(aJaxURL, data, responce => {

      // check for error
      responceErrorHandler(responce, data => {

        // restruct data
        let { structure } = data;

        // define dialog buttons mikimouse
        const buttons = {
            "save": {
                text: "შენახვა",
                id: "save-ruhu-dialog",
                click: function () {

                  // check choosed status
                  switch(rushHours.chooseStatus) {
                    case 0:
                      rushHours.clearSelection();
                      break;
                    case 1:
                      rushHours.makeSelection();
                      break;
                  }

                  // clear all selectors and close choose dialog
                  rushHours.rowUpdate()
                           .clearSelector();
                  $(this).dialog("close");

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-ruhu-dialog",
                click: function () {

                  // clear all selectors
                  rushHours.clearSelector();

                  // close dialog
                  $(this).dialog("close");

                }
            }
        };

        // insert structure in relevant parent dom
        $("#rush-hours-confirm").html(structure);

        // stylize select
        $(".jquery-ui-chosen").chosen();
        $("#choseRushHourStatus_chosen").css({
          width: "100%",
          paddingTop: "10px"
        }).find('.chosen-search').hide();
        $("#rush-hours-confirm").css({overflow: "visible"});

        // open dialog
        GetDialog("rush-hours-confirm", 300, "auto", buttons, "center center");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

      });

    });

  });

  // MOUSE DOUN ON MINUTE COLUMNS
  $(document).on("mousedown", ".rush-hour-body .minute-col", function() {

    // check for selector child element
    let selectorChild = $(this).find(".selector");

    // clear all selectors
    rushHours.clearSelector();

    // define column status
    let status = $(this).data("status");

    // define select data
    let select = {
      row: $(this).data("row"),
      start: $(this).data("col"),
      end: $(this).data("col")
    };

    // fix select start position
    rushHours.selectStart = select.start;

    // save select data in relevant array
    rushHours.selectedColVals = select;

    // select event target column
    rushHours.selectColumns();

  });

  // MOUSE OVER ON MINUTE COLUMNS
  $(document).on("mouseover", ".rush-hour-body .minute-col", function(event) {

    // check if mouse button is clicked
    if(event.buttons === 1) {

      // define colun data
      let row = $(this).data("row");
      let col = $(this).data("col");

      // check in which direction must select
      if(col <= rushHours.selectStart) {
        rushHours.selectedColVals.end = rushHours.selectStart;
        rushHours.selectedColVals.start = col;
      } else {
        rushHours.selectedColVals.end = col
      }

      // select event target column
      rushHours.selectColumns();

    }

  });

  // CHOOSE SELECT STATUS
  $(document).on("change", "#choseRushHourStatus", function() {
    rushHours.chooseStatus = Number($(this).val());
  });

  // CLICK ON SAVE DATA BUTTON
  $(document).on("click", "#saveRushHours", () => {

    // define save data
    const data = {
      act: "save_rush_hours",
      projectId: projectDetails.originalId,
      year: $("#chooseRushHourYear").val(),
      weekNum: $("#chooseRushHourWeek").val(),
      values: rushHours.selectedColumns
    }

    // send ajax request
    $.post(aJaxURL, data, responce => {

      // handle the responce error
      responceErrorHandler(responce, data => {

        // save selected columns for compare
        rushHours.makeSelColCopy()
                 .updateSvageButtonStatus();

      });
    });
  });

  // CLICK ON CLEARE ROW BUTTON
  $(document).on("click", ".rush-hour-row-clearer", function() {

    // define row id
    const row = $(this).data("row");

    // clear relevant data from rush hours array
    rushHours.clearRow(row);

  });

  // COPY PREVIOUS WEEK DATA
  $(document).on("click", "#copyRashHorsFromPrevWeek", () => {

    // check if data exists
    if(rushHours.selectedColumns.length) {

      // define note data
      let noteText = "დაკოპირების შემთხვევაში, არსებული მონაცემები გაუქმდება... გსურთ მოქმედების განხორციელება?";

      // execute note function
      note(noteText, () => rushHours.copyPrevWeekData());

    } else {
      rushHours.copyPrevWeekData();
    }

  });

/*======================================================================================= RUSH HOURS ACTIONS ENDS ================================================================================*/


/*=================================================================================== WORKCYCLE DYRECTORY ACTIONS STARTS ==========================================================================*/

    // load work cycle inner table
    function loadWorkCycleInnerTable() {

      var workCycleId		= $("#wcsh_cycle_id").val();

      GetButtons("add_button_inner_cycle", "delete_inner_cycle");
      GetDataTable('work_cyrcle_inner_table', aJaxURL, "get_cycle_inner_list&id="+workCycleId, 9, "", 0, "", 1, "asc", "", "");

    }

    // returns work shift data object
    function getWorkCycleData() {

        return {
            act: "save_work_cycle",
            cycle_id: $("#wcsh_cycle_id").val(),
            name: $("#wcsh_name").val()
        };

    }

    // returns work shift data object
    function getWorkCycleShiftData() {

        return {
            act: "save_work_cycle_shift",
            detail_id: $("#work_cycle_shift_detail_id").val(),
            cycle_id: $("#wcsh_cycle_id").val(),
            work_shift_id: $("#wcsh_work_shift_id").val(),
            num: $("#wcsh_num").val()
        };

    }

    // load work cycle dialog
    function loadWorkCycleEditDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-workcycle",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-workcycle",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // insert page in dialog
        $("#add-edit-form-workcycle").html(page);

        // open dialog
        GetDialog("add-edit-form-workcycle", 960, "auto", buttons, "center");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

        // load table inside of dialog
        loadWorkCycleInnerTable();

    }

    // load work cycle create dialog
    function loadWorkCycleCreateDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-workcycle",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-workcycle",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // insert page in dialog
        $("#add-edit-form-workcycle").html(page);

        // open dialog
        GetDialog("add-edit-form-workcycle", 250, "auto", buttons, "center");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});


    }

    // get work cycle delete object list
    function getWorkCycleDeleteList() {

        // define return value
        let data = '';
        let checkedData = $("#work_cycle_table").find(".check:checked");

        checkedData.each(function() {

            if(data !== '') {
                data += ',';
            }

            data += Number($(this).val());

        });

        return data;

    }

    // get work cycle shift delete object list
    function getWorkCycleShiftDeleteList() {

        // define return value
        let data = '';
        let checkedData = $("#work_cyrcle_inner_table").find(".check:checked");

        checkedData.each(function() {

            if(data !== '') {
                data += ',';
            }

            data += Number($(this).val());

        });

        return data;

    }

    // load work cycle shift dialog
    function loadWorkCycleShiftDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-cycle-workshift",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-cycle-workshift",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // insert page in dialog
        $("#add-edit-cycle-workshift").html(page);

        // open dialog
        GetDialog("add-edit-cycle-workshift", 220, "auto", buttons, "center");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

    }

    // click on adding new cycle button
    $(document).on("click", "#add_button_cycle", function() {

        // send ajax request
        $.post(aJaxURL, {act: "get_cycle_form_create"}, result => {

            // destruct result
            let {error, page} = result;

            // checking received data
            if(error) {
                alert(error);
            } else {

                // load dialog
                loadWorkCycleCreateDialog(page);
            }

        });

    });

    // click on cycle work shift add button
    $(document).on("click", "#add_button_inner_cycle", function() {

        // define ajax recuest data
        let data = {
            act: "get_cycle_workshift_form",
            nextProject: $("#wcsh_next_project").val()
        }

        // send ajax request
        $.post(aJaxURL, data, result => {

            // destruct result
            let {error, page} = result;

            // checking received data
            if(error) {
                alert(error);
            } else {

                // load dialog
                loadWorkCycleShiftDialog(page);
            }

        });

    });

    // save new work cycle in directory
    $(document).on("click", "#save-dialog-workcycle", function () {

        // collect save data
        let data = getWorkCycleData();

        if(data.name === ""){
            alert("შეავსეთ ველი!");
        }else{
            $.ajax({
                url: aJaxURL,
                data,
                success: function(data) {
                    if(typeof(data.error) !== 'undefined'){
                        if(data.error !== ''){
                            alert(data.error);
                        }else{

                          const { saveStatus } = data;

                          if(saveStatus && saveStatus !== "exists") {

                            loadDirPage("cycles");
                            CloseDialog("add-edit-form-workcycle");

                          } else if(saveStatus && saveStatus === "exists") {
                            alert("ასეთი დასახელების ცვლა უკვე არსებობს ბაზაში");
                          } else {
                            alert("გაურკვეველი მიზეზი გამო, თქვენ მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
                          }
                          
                        }
                    }
                }
            });
        }
    });

    // save new work cycle shift in directory
    $(document).on("click", "#save-dialog-cycle-workshift", function () {

        // collect save data
        let data = getWorkCycleShiftData();

        // check for num
        if(data.num === "") {
          data.num = $("#work_cyrcle_inner_table tbody tr").length + 1;
        }

        if(data.work_shift_id === "0"){
            alert("გთხოვთ მონიშნოთ ცვლა");
        }else{

          $.post(aJaxURL, data, responce => {

            // destruct responce
            const { error, saveStatus } = responce;

            // check for error
            if(error !== "") {
              alert(error);
            } else {

              // destruct save status
              const { complete, denialReasone } = saveStatus;

              // check for save status
              if(complete && !denialReasone) {

                // close add shift in cycle dialog
                $("#add-edit-cycle-workshift").dialog("close");

                // load tables
                loadDirPage("cycles");
                loadWorkCycleInnerTable();

              } else if(!complete && denialReasone) {

                // switch between denial reasones
                switch(denialReasone) {
                  case 'previewsDayEnding':
                    alert("წინა დღის ცვლის დასასრული დრო მეტია, ვიდრე არჩეული ცვლის საწყისი დრო");
                    break;
                  case 'sameId':
                    alert("ერთიდაიგივე ცვლის, ერთიდაიგივე ნუმერაციით დამატება შეუძლებელია");
                    break;
                  case 'sameDayTimeCross':
                    alert("არჩეული ცვლის დრო ჰკვეთს იგივე ნუმერაციის მჯქონე სხვა ცვლის დროს");
                    break;
                  case 'nextDayStarting':
                    alert("მომდევნო დღის ცვლის საწყისი დრო ნაკლებია, ვიდრე არჩეული ცვლის დასასრული დრო");
                    break;
                  case 'firstShiftStarting':
                    alert("პირველი და ბოლო ცვლების დრო ჰკვეთავს ერთმანეთს");
                    break;
                }

              }

            }

          });
        }

    });

    // delete work cycle from directory
    $(document).on("click", "#delete_button_cycle", function() {

        // define ajax request data
        let data = {
            act: "delete_work_cycle_from_directory",
            id: getWorkCycleDeleteList()
        };

        // send ajax request
        $.post(aJaxURL, data, result => {

            // restruct result
            let {error} = result;

            if(error !== "") {
                alert(error);
            } else {

                //load table
                loadDirPage("cycles");

                // uncheck all checkbox checker checkbox
                $("#cycleTableChecker").prop("checked", false);
            }

        });

    });

    // change cycle outer chable checkboxes
    $(document).on("change", ".cycle-general-check", () => checkAllCheckBoxes(".cycle-general-check", "#cycleTableChecker"));

    // delete work cycle shift from directory
    $(document).on("click", "#delete_inner_cycle", function() {

        // define ajax request data
        let data = {
            act: "delete_work_cycle_shift_from_directory",
            id: getWorkCycleShiftDeleteList()
        };

        // send ajax request
        $.post(aJaxURL, data, result => {

            // restruct result
            let {error} = result;

            if(error !== "") {
                alert(error);
            } else {

                //load table
                loadWorkCycleInnerTable();

                // unchck all checkbox checker checkbox
                $("#checkAllCycleShifts").prop("checked", false);
            }

        });

    });

    // edit work cycle details
    $(document).on("dblclick", "#work_cycle_table tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // collect save data
            let data = {
                act: "edit_cycle_form",
                id: $(this).find(".colum_hidden").text().trim()
            };

            // send ajax request
            $.post(aJaxURL, data, result => {

                // destruct result
                let {error, page} = result;

                // checking received data
                if(error) {
                    alert(error);
                } else {

                    // load dialog
                    loadWorkCycleEditDialog(page);
                }

            });

        }

    });

    // edit work cycle shift details
    $(document).on("dblclick", "#work_cyrcle_inner_table tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // collect save data
            let data = {
                act: "edit_cycle_workshift_form",
                id: $(this).find(".colum_hidden").text().trim()
            };

            // send ajax request
            $.post(aJaxURL, data, result => {

                // destruct result
                let {error, page} = result;

                // checking received data
                if(error) {
                    alert(error);
                } else {

                    // load dialog
                    loadWorkCycleShiftDialog(page);
                }

            });

        }

    });

    // select all cycle shift in list
    $(document).on("change", "#checkAllCycleShifts", function() {

      // define check status
      const checkStatus = $(this).is(":checked");

      // check check status
      if(checkStatus) {
        $(".cycle-shifts").prop("checked", true);
      } else {
        $(".cycle-shifts").prop("checked", false);
      }

    });

    // change cycle shift checked status
    $(document).on("change", ".cycle-shifts", () => checkAllCheckBoxes(".cycle-shifts", "#checkAllCycleShifts"));


/*=================================================================================== WORKCYCLE DYRECTORY ACTIONS ENDS ==========================================================================*/


/*=================================================================================== ACTIVITIES DYRECTORY ACTIONS STARTS ==========================================================================*/

    // returns work shift data object
    function getActivitiesData() {

        return {
            act: "save_activities",
            id: $("#activities_id").val(),
            name: $("#activities_name").val(),
            color: $("#activities_color").val(),
            comment: $("#activities_comment").val(),
            pay: $("#activities_pay").val(),
            dnd: $("#activities_dnd").is(":checked") ? 1 : 0
        };

    }

    // load activities dialog
    function loadActivitiesDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-activities",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-activities",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // insert page in dialog
        $("#add-edit-form-activities").html(page);

        // stylize inputs and buttons
        loadActivitiesRangeTable();
        $('#activities_add_limit').button();
        $('#activities_timer').timepicker({
            hourMax: 23,
            hourMin: 0,
            minuteMax: 59,
            minuteMin: 0,
            stepMinute: 1,
            minuteGrid: 15,
            hourGrid: 3,
            timeFormat: 'HH:mm'
        });

        // open dialog
        GetDialog("add-edit-form-activities", 250, "auto", buttons, "center center-70");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

    }

    // get activities delete object list
    function getActivitiesDeleteList() {

        // define return value
        let data = '';
        let checkedData = $("#work_activities_table tbody").find(".check:checked");

        checkedData.each(function() {

            if(data !== '') {
                data += ',';
            }

            data += Number($(this).val());

        });

        return data;

    }

    // click on adding new activitie button
    $(document).on("click", "#add_button_activities", function() {

        // send ajax request
        $.post(aJaxURL, {act: "get_activities_form"}, result => {

            // destruct result
            let {error, page} = result;

            // checking received data
            if(error) {
                alert(error);
            } else {

                // load dialog
                loadActivitiesDialog(page);
            }

        });

    });

    // save new activities in directory
    $(document).on("click", "#save-dialog-activities", function () {

        // collect save data
        let data = getActivitiesData();

        if(data.name === "" || data.color === '' || data.type === 0 || data.pay === 0 || data.project_id === 0){
            alert("შეავსეთ ველი!");
        }else{
            $.ajax({
                url: aJaxURL,
                data,
                success: function(data) {
                    if(typeof(data.error) !== 'undefined'){
                        if(data.error !== ''){
                            alert(data.error);
                        }else{
                          
                            const { saveStatus } = data;

                            if(saveStatus && saveStatus !== "exists") {
                              loadDirPage("activities");
                              CloseDialog("add-edit-form-activities");
                            } else if(saveStatus && saveStatus === "exists") {
                              alert("ასეთი დასახელების აქტივობა უკვე არსებობს ბაზაში");
                            } else {
                              alert("გაურკვეველი მიზეზების გამო, ტქვენ მიერ მოტხოვნილი მოქმედება ვერ განხორციელდა");
                            }
                            
                        }
                    }
                }
            });
        }
    });

    // delete activities from directory
    $(document).on("click", "#delete_button_activities", function() {

        // define ajax request data
        let data = {
            act: "delete_activities_from_directory",
            id: getActivitiesDeleteList()
        };

        // send ajax request
        $.post(aJaxURL, data, result => {

            // restruct result
            let {error} = result;

            if(error !== "") {
                alert(error);
            } else {

                //load table
                loadDirPage("activities");

                // uncheck all checkbox checher checkbox
                $("#activitiesTableChecker").prop("checked", false);
            }

        });

    });

    // edit activities data
    $(document).on("dblclick", "#work_activities_table tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // collect save data
            let data = {
                act: "edit_activities_form",
                id: $(this).find(".colum_hidden").text().trim()
            };

            // send ajax request
            $.post(aJaxURL, data, result => {

                // destruct result
                let {error, page} = result;

                // checking received data
                if(error) {
                    alert(error);
                } else {

                    // load dialog
                    loadActivitiesDialog(page);
                }

            });

        }

    });

    // change activities table checkbox
    $(document).on("change", ".activities-check", () => checkAllCheckBoxes(".activities-check", "#activitiesTableChecker"));


/*=================================================================================== ACTIVITIES DYRECTORY ACTIONS ENDS ==========================================================================*/


/*=================================================================================== ACTIVITIES RANGE DYRECTORY ACTIONS STARTS ==========================================================================*/


    // load activities range table
    function loadActivitiesRangeTable() {
        GetButtons("activities_add_button1", "activities_delete_button1");
        GetDataTable("activities_activiti", aJaxURL, "get_activities_range_list&work_activities_id="+$("#activities_id").val(), 3, "", 0, "", 1, "desc", "", "<'dataTable_buttons'T><'F'Cfipl>");

        $('#activities_activiti').css({width: '100%'});
        $('#activities_activiti_length').css({marginTop: '20px', marginLeft: '2px'});
        $('#activities_activiti_filter input').css({marginTop: '3px', marginLeft: '2px'});
        $('.dataTable_buttons, .ColVis').remove();
    }

    // load activities dialog
    function loadActivitiesRangeDialog(page) {

        // define dialog buttons
        let buttons = {
            "save": {
                text: "შენახვა",
                id: "save-dialog-activities-range",
                click: function () {

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-activities-range",
                click: function () {
                    $(this).dialog("close");
                }
            }
        };

        // insert page in dialog
        $("#add-edit-form-activities-range").html(page);

        // stylize inputs
        $('#sctivities_range_start,#sctivities_range_end').timepicker({
            hourMax: 23,
            hourMin: 0,
            minuteMax: 59,
            minuteMin: 0,
            stepMinute: 1,
            minuteGrid: 15,
            hourGrid: 3,
            timeFormat: 'HH:mm'
        });

        // open dialog
        GetDialog("add-edit-form-activities-range", 500, "auto", buttons, "center");

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

    }

    // returns activities range data object
    function getActivitiesRangeData() {

        return {
            act: "save_activities_range",
            id: $("#activities_range_id").val(),
            start: $("#sctivities_range_start").val(),
            end: $("#sctivities_range_end").val(),
            work_activities_id: $("#activities_id").val()
        };

    }

    // get activities range delete object list
    function getActivitiesRangeDeleteList() {

        // define return value
        let data = '';
        let checkedData = $("#activities_activiti").find("input[type='checkbox']:checked");

        checkedData.each(function() {

            if(data !== '') {
                data += ',';
            }

            data += Number($(this).val());

        });

        return data;

    }


    // click on adding new activitie range button
    $(document).on("click", "#activities_add_button1", function() {

        // send ajax request
        $.post(aJaxURL, {act: "get_activities_range_form"}, result => {

            // destruct result
            let {error, page} = result;

            // checking received data
            if(error) {
                alert(error);
            } else {

                // load dialog
                loadActivitiesRangeDialog(page);
            }

        });

    });

    // save new activities range in directory
    $(document).on("click", "#save-dialog-activities-range", function () {

        // collect save data
        let data = getActivitiesRangeData();

        if(data.start === "" || data.end === ''){
            alert("შეავსეთ ველი!");
        }else{
            $.ajax({
                url: aJaxURL,
                data,
                success: function(data) {
                    if(typeof(data.error) !== 'undefined'){
                        if(data.error !== ''){
                            alert(data.error);
                        }else{
                            loadActivitiesRangeTable();
                            CloseDialog("add-edit-form-activities-range");
                        }
                    }
                }
            });
        }
    });

    // delete activities from directory
    $(document).on("click", "#activities_delete_button1", function() {

        // define ajax request data
        let data = {
            act: "delete_activities_range_from_directory",
            id: getActivitiesRangeDeleteList()
        };

        // send ajax request
        $.post(aJaxURL, data, result => {

            // restruct result
            let {error} = result;

            if(error !== "") {
                alert(error);
            } else {

                //load table
                loadActivitiesRangeTable();
            }

        });

    });

    // check activities strat and end time row
    $(document).on("click", "#activities_activiti tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // define checkbox status
            let checkbox = $(this).find("input[type='checkbox']");
            let checkboxStat = checkbox.is(":checked");

            // change checkbox status
            checkbox.prop("checked", !checkboxStat);
        }

    });

    // edit activities strat and end time data
    $(document).on("dblclick", "#activities_activiti tbody tr", function () {

        // check for empty datatable
        let emptyRow = $(this).find(".dataTables_empty");

        // if data table is not empty
        if(!emptyRow.length) {

            // collect save data
            let data = {
                act: "edit_activities_range_form",
                id: $(this).find(".colum_hidden").text().trim()
            };

            // send ajax request
            $.post(aJaxURL, data, result => {

                // destruct result
                let {error, page} = result;

                // checking received data
                if(error) {
                    alert(error);
                } else {

                    // load dialog
                    loadActivitiesRangeDialog(page);
                }

            });

        }

    });

/*=================================================================================== ACTIVITIES DYRECTORY ACTIONS ENDS ==========================================================================*/

/*=================================================================================== ADD HOURS ACTIONS STARTS ==========================================================================*/

    // click on add new hours button
    $(document).on("click", "#addNewHours", function() {

        // send ajax request
        $.post(aJaxURL, {act: "get_add_hours_dialog"}, result => {

            // restruct result data
            let {error, structure} = result;

            // check fopr error
            if(error != '') {
                alert(error);
            } else {

                // define buttons
                var buttons = {
                    "save": {
                        text: "შენახვა",
                        id: "save-add-new-hours-dialog",
                        click: function () {


                        }
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "close-add-new-hours-dialog",
                        click: function () {
                            $(this).dialog("close");

                        }
                    }
                };

                $("#add-new-hours-dialog").html(structure);
                GetDialog("add-new-hours-dialog", 790, "auto", buttons, 'center center');

                // set dialog and dialog back holder z position
                $(`.ui-widget-overlay`).css({zIndex: "100"});
                $(`.ui-dialog`).css({zIndex: "101"});

            }

        });

    });

    // hour convertor
    function hourConvertor (number) {

      // define data
      let remainder = number % 1;
      let remainderTime = new Date(remainder * 3600 * 1000);
      let hours = Math.floor(number);
      let minutes = remainderTime.getMinutes();

      // fix the zores
      let outputHours = zeroFixer(hours);
      let outputMinutes = zeroFixer(minutes);

      // return value
      return `${outputHours}:${outputMinutes}`;

    };

    // days in week hours calculator
    function hoursCalculator() {

      // define action table body and value receiver
      let tableBody = $("#work_table tbody");
      let plannerReceiver = $(".wfm-pht-operation-row");

      // execute loop throw action table rows for calculate
      // hour row summary
      tableBody.find("tr").each(function(i) {

        // define data collectore
        let collector = 0;

        $(this).find("td").each(function() {

          collector += Number($(this).text().trim());

        });

        // divide collector value on 4 for get hours count
        collector /= 4;

        let weekCount = hourConvertor(collector);
        let monthCount = hourConvertor(collector * 4);

        // insert collector data in relevant receivers
        $(`.hours-row[data-id="${i}"]`).find("td:eq(1)").find("h6").data("hours", collector).text(weekCount);
        $(`.hours-row[data-id="${i}"]`).find("td:eq(2)").find("h6").data("hours", collector * 4).text(monthCount);

        // insert day hours count into planner operating hours count
        for(let p = 0; p < 4; p++) {

          let planDayColId = i + (p * 7);
          plannerReceiver.find(".wfm-pht-col").eq(planDayColId).find("span").text(weekCount);

        }

      });

      // execute loop for summarize week hours
      let hourRow = $(`.hours-row`);
      let weekHourCollector = 0;

      hourRow.each(function(i) {

        weekHourCollector += $(this).find("td:eq(1)").find("h6").data("hours");

      });

      $(".hours-per-week-sum-receiver").text(hourConvertor(weekHourCollector));


      // execute loop for summarize month hours
      let monthHourCollector = 0;

      hourRow.each(function(i) {

        monthHourCollector += $(this).find("td:eq(2)").find("h6").data("hours");

      });

      $(".hours-per-month-sum-receiver").text(hourConvertor(monthHourCollector));

    }


    // check for copy checked
    function disableCopy(parent) {

      // define checkbox
      let checkbox = parent.find(".anh-copy-above");

      // define check status
      let isChecked = checkbox.is(":checked");

      // chenge check status
      if(isChecked) {
        checkbox.prop("checked", false);
      }

    }

    // toggle add new hour inputs
    function toggleAddHourInputs(parent, status) {

      // define target objects
      let timeInputs = parent.find(".anh-input-wrapper input:not(.anh-colen)");
      let timeInputColen = parent.find(".anh-input-wrapper .anh-colen");
      let countInput = parent.find(".new-hours-count");
      let targetSpan = parent.find(".anh-dif-time-receiver, .anh-sum-time-receiver");

      // check status
      if(status) {
          timeInputs.val("").prop("disabled", true);
          timeInputColen.css({background:"#ebebe4"}).val("");
          countInput.val("").prop("disabled", true);
          targetSpan.text("");
      } else {
        timeInputs.val("00").prop("disabled", false);
        timeInputColen.css({background:"none"}).val(":");
        countInput.val("1").prop("disabled", false);
        targetSpan.text("00:00");
      }

    }

    // check add new hours, start hours, hour input
    $(document).on("keydown", ".anh-input-wrapper input", function(event) {

      // define proccesse data
      let actionKey = Number(event.key);

      // check for number characters
      if((event.key !== "Backspace" && event.key !== "Delete" && event.key !== "ArrowRight" && event.key !== "ArrowLeft") && isNaN(actionKey)) {
        return false;
      } else if(event.keyCode === 32) {
        return false;
      }

    });

    // calculate time on input key up
    $(document).on("keyup", ".anh-input-wrapper input", function(event) {

        // define data
        let parent = $(this).parents("tr");
        let value = $(this).val();
        let numVal = Number(value);
        let keyPresed = event.key;

        // checking for input
        if($(this).hasClass("nh-hr")) {

          // checking for value
          if(!$(this).hasClass("new-hours-start-hr") && numVal > 24) {
            $(this).val("24");
          } else if($(this).hasClass("new-hours-start-hr") && numVal >= 24) {
            $(this).val("00");
          } else if(numVal < 0) {
            $(this).val("00");
          }

          // checking for value length
          if(value.length === 2) {
            $(this).blur();
            $(this).parent().find(".nh-mn").focus();
          }

        } else if($(this).hasClass("nh-mn")) {

          // define previews input value
          let prevValue = Number($(this).parent().find(".nh-hr").val());

          // checking for value
          if(numVal >= 60 || prevValue >= 24 || numVal < 0) {
            $(this).val("00");
          }

        }

        // check for key pressed
        if(keyPresed === "Tab") {

          if($(this).hasClass("new-hours-start-hr")) {
            $(this).parent().find(".new-hours-start-mn").focus();
          } else if($(this).hasClass("new-hours-start-mn")) {
            parent.find(".new-hours-end-hr").focus();
          } else if($(this).hasClass("new-hours-end-hr")) {
            parent.find(".new-hours-end-mn").focus();
          } else if($(this).hasClass("new-hours-end-mn")) {
            parent.find(".new-hours-count").focus();
          }


        }

        // calculate hours in row
        calculateHours(parent);

    });

    // focus on new hours adding start and end hour inputs
    $(document).on("focus", ".anh-input-wrapper input", function() {

        // define input value
        let value = $(this).val();

        // checking the value
        if(value === "00" || value === "0") {
            $(this).val("");
        }

    });

    // blur from new hours adding start and end hour inputs
    $(document).on("blur", ".anh-input-wrapper input", function() {

        // define input value and type
        let parent = $(this).parents("tr");
        let value = $(this).val();
        let type = $(this).data("type");

        // checking the value
        if(!value) {
            $(this).val("00");
        } else if(value.length === 1) {
            $(this).val(`0${value}`);
        }

    });

    // calculate adding new hours data
    function calculateHours(parent) {

        // define global data
        let difTimeReceiver = parent.find(".anh-dif-time-receiver");
        let timeCount = Number(parent.find(".new-hours-count").val());
        let timeCounterReceiver = parent.find(".anh-sum-time-receiver");
        let fullTimeCheckbox = parent.find(".anh-check-full-day");
        let differenceTime = "00:00", countedTime = "00:00";

        // start date
        let startDateHour = Number(zeroFixer(parent.find(".new-hours-start-hr").val()));
        let startDateMin = Number(zeroFixer(parent.find(".new-hours-start-mn").val()));

        // end date
        let endDateHour = Number(zeroFixer(parent.find(".new-hours-end-hr").val()));
        let endDateMin = Number(zeroFixer(parent.find(".new-hours-end-mn").val()));

        // define hour difference data
        let hourDifference;
        let minDifference;

        minDifference = endDateMin - startDateMin;

        if(endDateHour >= startDateHour) {
            hourDifference = endDateHour - startDateHour;
        } else if(endDateHour < startDateHour) {
            hourDifference = endDateHour + (24 - startDateHour);
        } else {
          hourDifference = 0;
          minDifference = 0;
        }

        // check for minute difference for negative sign
        if(minDifference < 0) {
            hourDifference = hourDifference - 1 < 0 ? 23 : hourDifference - 1;
            minDifference = 60 + minDifference;
        }

        // check for minute count
        if(minDifference >= 60) {
            hourDifference += Math.floor(minDifference / 60);
            minDifference = minDifference % 60;
        }

        // define hour count data
        let hourCount = hourDifference * timeCount;
        let minCount = minDifference * timeCount;

        // check for minute count
        if(minCount >= 60) {
            hourCount += Math.floor(minCount / 60);
            minCount = minCount % 60;
        }

        // check for 24 hour differrence checkbox checking
        if(hourDifference === 24 && minDifference === 0) {
            fullTimeCheckbox.prop("checked", true);
        } else {
          fullTimeCheckbox.prop("checked", false);
        }

        // assign zero fixed value of difference time
        differenceTime = `${zeroFixer(hourDifference)}:${zeroFixer(minDifference)}`;

        // assign zero fixed value of counted time
        countedTime = `${zeroFixer(hourCount)}:${zeroFixer(minCount)}`;

        // insert difference and counted time
        difTimeReceiver.text(differenceTime);
        timeCounterReceiver.text(countedTime);

        // check for copy
        disableCopy(parent);

        // calculate time sum
        calcAllTimeSum();
    }

    // calculate all time summary
    function calcAllTimeSum() {

      // define objects
      let timeItem = $(".add-new-hours-table tbody").find(".anh-sum-time-receiver");
      let hourSum = "00:00";
      let hoursAdd = hours.add;

      // execute loop throw time items
      timeItem.each(function() {

        // define values
        let value = $(this).text().trim();
            value = value === "" ? "00:00" : value;

        hourSum = hoursAdd(hourSum, value);

      });

      // insert summary value in receiver
      $(".anh-all-time-sum-receiver").text(hourSum);

    }

    // zero fixer
    function zeroFixer(value) {

      // define value data
      let numValue = typeof value === "string" ? parseInt(value) : value;

      if(numValue < 10 && numValue > 0) {
        return `0${numValue}`;
      } else if(numValue === 0) {
        return `00`;
      } else if(numValue < 0 && numValue > -10) {
        return `-0${String(numValue).replace(/^-/i, "")}`;
      } else {
        return value;
      }

    }

    // save new hours
    function saveWeekHours(projectId, daysId, dialog) {

      // define save data
      let data = {
        act: "save_wk",
        id: projectId,
        days: []
      };

      // collect the data
      $(`.anht-${daysId} tbody tr:not([used])`).each(function() {

        let arrayData = {
          _id: daysId === "individual" ? $("#newHoursIndDayId").val() : Number($(this).data("day")) + 1,
          holiday: daysId !== "individual" && $(this).find(".anh-check-holiday").is(":checked")
        }

        // checking for holiday
        if(!arrayData.holiday) {

          // define start and end time
          let self = $(this);
          let { startTime, endTime } = hoursGetStartANdEnd(self);

          if(startTime === endTime) {
            startTime = "00:00:00";
            endTime = "00:00:00";
          }

          if((startTime !== "" && startTime !== "00:00:00") || (endTime !== "" && endTime !== "00:00:00")) {

            arrayData.startTime = startTime;
            arrayData.endTime = endTime;
            arrayData.count = parseInt($(this).find(".new-hours-count").val());

            data.days.push(arrayData);

          }

        } else {
          data.days.push(arrayData);
        }

      });

      // send ajax request
      if(data.days.length) {
        $.post(aJaxURL, data, result => {

          // destruct result
          let {error, complete} = result;

          // check for error
          if(error != "") {
            alert(error);
          } else {

              loadHoursAfterAdd();
              CloseDialog(dialog);
          }

        });
      } else {
        CloseDialog(dialog);
      }

    }

    // keydown and new hours checkboxes
    $(document).on("keydown", ".add-new-hours-table input[type='checkbox']", function(event) {
      return false;
    });

    // keydown and new hours count
    $(document).on("keydown", ".new-hours-count", function(event) {
      if(event.key === "Tab") return false;
    });

    // changing and keyup and new hours count
    $(document).on("change, keyup", ".new-hours-count", function(event) {

        // define parent and next row
        let parent = $(this).parents("tr");
        let nextRow = parent.next("tr");

        // check for event key
        if(event.key === "Tab") {

          // define search row
          let targetInput = nextRow.find(".new-hours-start-hr");

          if(!nextRow.length) {
            targetInput = $(".add-new-hours-table tbody").find("tr:eq(0)").find(".new-hours-start-hr");
          }

          targetInput.focus();

        }

        // calculate hours in row
        calculateHours(parent);

    });

    // blur new hours count
    $(document).on("blur", ".new-hours-count", function() {

        // define parent and value
        let parent = $(this).parents("tr");
        let value = parseInt($(this).val());

        // check for valid value
        if(!value || value < 0) {
          $(this).val(1);
        }

        // calculate hours in row
        calculateHours(parent);

    });

    // change full time hours checkbox
    $(document).on("change", ".anh-check-full-day", function() {

        // define parent
        let parent = $(this).parents("tr");

        // check for holidays
        let isHoliday = parent.find(".anh-check-holiday").is(":checked");

        // define if is checked or not
        let isNotChecked = $(this).is(":checked");

        if(!isHoliday) {

          parent.find(".new-hours-start-hr, .new-hours-start-mn, .new-hours-end-hr, .new-hours-end-mn").val("00");

          if(isNotChecked) {
            parent.find(".new-hours-end-hr").val("24");
          } else {
            parent.find(".new-hours-end-hr").val("00");
          }


          // calculate hours in row
          calculateHours(parent);

        }

    });

    // make add new hours row dissable
    $(document).on("change", ".anh-check-holiday", function() {

        // define parent
        let parent = $(this).parents("tr");

        // define check status
        let isChecked = $(this).is(":checked");

        // check for full time checkbox
        let fullTimeCheckbox = parent.find(".anh-check-full-day");

        if(fullTimeCheckbox.is(":checked")) {
          fullTimeCheckbox.prop("checked", false);
        }

        //toggle inputs
        toggleAddHourInputs(parent, isChecked);

        // calculate all time summary
        calcAllTimeSum();

        // check for copy
        disableCopy(parent);

    });


    // make add new hours row copy
    $(document).on("change", ".anh-copy-above", function() {

        // define parent and previews object
        let parent = $(this).parents("tr");

        // check for holidays
        let isHoliday = parent.find(".anh-check-holiday").is(":checked");

        if(!isHoliday) {

          // define previews object
          let prevObject = parent.prev("tr");

          // define check status
          let isChecked = $(this).is(":checked");

          // define value definer
          let valueDefiner = {
            startTime: ".new-hours-start-hr",
            startTimeMin: ".new-hours-start-mn",
            endTimeHour: ".new-hours-end-hr",
            endTimeMin: ".new-hours-end-mn",
            diferenceHours: ".anh-dif-time-receiver",
            countedHours: ".new-hours-count",
            sumHours: ".anh-sum-time-receiver",
            fullTime: ".anh-check-full-day",
            holiday: ".anh-check-holiday"
          }

          // check status
          if(isChecked) {

            // copy data from prev row
            for(let property in valueDefiner) {

              let targetObject = parent.find(`${valueDefiner[property]}`);
              let value = prevObject.find(`${valueDefiner[property]}`).val();

              // check for copied value receiver object type
              if(property === "diferenceHours" || property === "sumHours") {

                // copy text values
                value = prevObject.find(`${valueDefiner[property]}`).text().trim();
                targetObject.text(value);

              } else if(property === "fullTime" || property === "holiday") {

                // copy checkboxes
                value = prevObject.find(`${valueDefiner[property]}`).is(":checked");
                targetObject.prop("checked", value);

                // calculate all time summary
                calcAllTimeSum();

                // catch holidays for input toggeling
                if(property === "holiday" && value) {

                  //toggle inputs
                  toggleAddHourInputs(parent, true);

                }

              } else {
                targetObject.val(value);
              }

            }

          }

        } else {
          $(this).prop("checked", false);
        }

    });


  // save new hours
  $(document).on("click", "#save-add-new-hours-dialog", () => {

    // define project id
    let projectId = projectDetails.originalId;

    saveWeekHours(projectId, "week", "add-new-hours-dialog");

  });

  // load activities dialog
  function loadHoursIndDayDialog(page, day) {

      // define dialog buttons
      let buttons = {
          "save": {
              text: "შენახვა",
              id: "save-dialog-hour-ind-day",
              click: function () {

              }
          },
          "cancel": {
              text: "დახურვა",
              id: "cancel-dialog-hour-ind-day",
              click: function () {
                  $(this).dialog("close");
              }
          }
      };

      // insert page in dialog
      $("#add-edit-form-hours-ind-day").html(page);

      // open dialog
      GetDialog("add-edit-form-hours-ind-day", 700, "auto", buttons, "center center");

      // set dialog and dialog back holder z position
      $(`.ui-widget-overlay`).css({zIndex: "100"});
      $(`.ui-dialog`).css({zIndex: "101"});

      // set dialog title
      $("#add-edit-form-hours-ind-day").prev().find(".ui-dialog-title").html(`სამუშაო საათები - ${day}`);

      // calculate all time summary
      calcAllTimeSum();

  }

  // returns add new hours individual day delete button structure
  function newHourIndDayRow(dayName) {
    return `<tr>
                <td>
                    <span class='anh-day-name'>${dayName}</span>
                </td>
                <td>
                    <div class='anh-input-wrapper'>
                        <input type='text' pattern='\d*' maxlength='2' name='anh_start_date_hr' class='nh-hr new-hours-start-hr' value='00'>
                        <input type='text' value=':' class='anh-colen' disabled>
                        <input type='text' pattern='\d*' maxlength='2' name='anh_start_date_mn' class='nh-mn new-hours-start-mn' value='00'>
                    </div>
                </td>
                <td>
                    <div class='anh-input-wrapper'>
                        <input type='text' pattern='\d*' maxlength='2' name='anh_end_date_hr' class='nh-hr new-hours-end-hr' value='00'>
                        <input type='text' value=':' class='anh-colen' disabled>
                        <input type='text' pattern='\d*' maxlength='2' name='anh_end_date_mn' class='nh-mn new-hours-end-mn' value='00'>
                    </div>
                </td>
                <td>
                    <span class='anh-dif-time-receiver'>00:00</span>
                </td>
                <td><input type='number' min='1' name='anh_hours_count' class='new-hours-input new-hours-count' value='1'></td>
                <td>
                    <span class='anh-sum-time-receiver'>00:00</span>
                </td>
                <td>
                    <div class='new-hours-buttons'>
                        <button class="delete-hour-ind-day-row">
                            <i class="material-icons">close</i>
                        </button>
                    </div>
                </td>
            </tr>`;
  }

  // returns empty hours in day row structure
  function emptyHourInDayRow() {
    return `<tr class='empty-table-row'><td colspan='7'><span>ჩამონათვალი ცარიელია</span></td></tr>`;
  }

  // returns hours start and end time
  function hoursGetStartANdEnd(selector) {
    return {
      startTime: selector.find(".new-hours-start-hr").val().concat(":", selector.find(".new-hours-start-mn").val(), ":00"),
      endTime: selector.find(".new-hours-end-hr").val().concat(":", selector.find(".new-hours-end-mn").val(), ":00")
    }
  }

  // returns edit hours change status
  function getChangeHoursEditStatus(olddata, newdata) {

    if((olddata.startTime !== newdata.startTime || olddata.endTime !== newdata.endTime) && olddata.count !== newdata.count) {
      return "both";
    } else if((olddata.startTime !== newdata.startTime || olddata.endTime !== newdata.endTime) && olddata.count === newdata.count) {
      return "time";
    } else if((olddata.startTime === newdata.startTime && olddata.endTime === newdata.endTime) && olddata.count !== newdata.count) {
      return "count";
    }

  }

  // click on add hours individual day
  $(document).on("click", ".hours-individual-day", function() {

    // define day index and name
    let data = {
      act: "get_hours_ind_day_dialog",
      projectId: projectDetails.originalId,
      index: $(this).parents(".hours-row").data("id") + 1,
      name: $(this).text().trim()
    };

    // send ajax request
    $.post(aJaxURL, data, result => {

      // destruct result
      let {error, page} = result;

      // check for error
      if(error !== "") {
        alert(error);
      } else {
        loadHoursIndDayDialog(page, data.name);
      }

    });

  });

  // click on add new hours individual day row add button
  $(document).on("click", "#addNewIndDayHourRow", () => {

    // define action table
    let actionTable = $(".add-new-hours-table");

    // check if data is empty
    if(actionTable.find(".empty-table-row").length) {
      actionTable.find(".hidden-data").removeClass("hidden-data");
      actionTable.find(".empty-table-row").remove();
      $(".anh-header").removeClass("anh-header-margin");
    }

    // define day $namelet day name
    let dayName = $("#newHoursIndDayName").val();

    // appand new row
    $(".add-new-hours-table tbody").append(newHourIndDayRow(dayName));

  });

  // click on delete add new hour individual day row
  $(document).on("click", ".delete-hour-ind-day-row", function() {

    // define parent row adn action table
    let actionTable = $(".add-new-hours-table");
    let parent = $(this).parents("tr");
    let siblings = parent.siblings().length;

    // remove parent row
    parent.remove();

    // check for remaning data count
    if(!siblings) {

      actionTable.find(".anht-header, .anh-all-time-sum-receiver").addClass("hidden-data");
      actionTable.find("tbody").html(emptyHourInDayRow());
      $(".anh-header").addClass("anh-header-margin");

    }

    // calculate all time summary
    calcAllTimeSum();

    // get delete data
    let deleteData = {
      act: "delete_hour_from_directory",
      projectId: projectDetails.originalId,
      dayId: $("#newHoursIndDayId").val(),
      ...hoursGetStartANdEnd(parent)
    }

    $.post(aJaxURL, deleteData, result => {

      // destruct result
      let {error} = result;

      // check for error
      if(error !== "") {
          alert(error);
      } else {
        loadHoursAfterAdd();
      }

    });

  });

  // click on edit add new hour individual day row
  $(document).on("click", ".edit-hour-ind-day-row", function() {

    // define status
    const status = $(this).data("status");
    const parent = $(this).parents("tr");

    // toggle status
    if(status === "edit") {

      // button status and icon change
      $(this).data("status", "confirm");
      $(this).find("i").text("check");
      parent.find("input").prop("disabled", false);

      // get edit data
      projectDetails.hoursEditData = {
        act: "edit_hour_from_directory",
        projectId: projectDetails.originalId,
        dayId: $("#newHoursIndDayId").val(),
        count: Number(parent.find(".new-hours-count").val()),
        ...hoursGetStartANdEnd(parent)
      }

    } else if(status === "confirm") {

      // button status and icon change
      $(this).data("status", "edit");
      $(this).find("i").text("mode_edit");
      parent.find("input").prop("disabled", true);

      // define changed data
      let { startTime, endTime, count } = projectDetails.hoursEditData;
      const changed = {
        ...hoursGetStartANdEnd(parent),
        count: Number(parent.find(".new-hours-count").val())
      }

      // check for changes
      if(startTime !== changed.startTime || endTime !== changed.endTime || count !== changed.count) {

        // update edit data
        const editData = {
          ...projectDetails.hoursEditData,
          changedStart: changed.startTime,
          changedEnd: changed.endTime,
          changedCount: changed.count,
          status: getChangeHoursEditStatus(projectDetails.hoursEditData, changed)
        }

        // send edit request
        $.post(aJaxURL, editData, result => {

          // destruct result
          let {error} = result;

          // check for error
          if(error !== "") {
              alert(error);
          } else {
            loadHoursAfterAdd();
          }

        });

      }

    }

  });

  // save new hours individual by individual day
  $(document).on("click", "#save-dialog-hour-ind-day", function() {

    // define project id
    let projectId = projectDetails.originalId;

    saveWeekHours(projectId, "individual", "add-edit-form-hours-ind-day");

  });

/*=================================================================================== ADD HOURS ACTIONS ENDS ==========================================================================*/


/*=================================================================================== PLANNER ACTIONS STARTS ==========================================================================*/

  // define planner min class planibu
  class WorkPlanner {

    // define main constructor fuction
    constructor() {
        this.cycleList = [];
        this.reservedCycleData = {};
        this.choosedShift = {};
        this.workHoursSum = "00:00";
        this.newCycleShiftList = [];
        this.operatedHours = new Array(28).fill("00:00");
        this.rowControler =  [ this.rowControllerData() ];
    }

    // returns row controller data
    rowControllerData() {
        return {
          id: 0,
          cycleId: 0,
          shiftList: []
        };
    }

      // returns operator list row structure
      operatorRow(rowid) {

        // define data id
        const { id } = this.rowControler[rowid];

        // define row delete button
        const rowDeleteButtonStruct = () => {

            if(rowid > 0) {
                return `<button class="delete-work-planner-row" data-id="${id}">
                          <i class="fa fa-trash"></i>
                        </button>`
            } else {
                return ``;
            }
        };

        return `<tr>
                    <td class="wfm-pth-col-min wfm-pth-col-name">
                            <span>ოპერატორი ${rowid + 1}</span>
                    </td>
                    <td class="wfm-pth-col-min wfm-pth-col-select">
                            <select class="choose-work-planner-cycle" data-id="${id}">
                                ${this.makeOptionList(this.cycleList, this.rowControler[rowid].cycleId, "აირჩიეთ ციკლი")}
                            </select>
                    </td>
                    <td class="wfm-pth-col-min wfm-pth-col-button">
                        <button class="clear-work-planner-row" data-id="${id}">
                          <i class="fa fa-eraser"></i>
                        </button>
                        ${rowDeleteButtonStruct()}
                    </td>
                </tr>`;

      }

        // make option list
        makeOptionList(array, activeid, endtext) {

            // define variables
            let structure = "";
            activeid = parseInt(activeid);

            // execute loop
            array.map(item => {

                // define item id
                let itemId = parseInt(item.id);
                let shiftCount = item.shiftCount ? item.shiftCount : null;

                structure += `<option value="${itemId}"  ${activeid === itemId ? "selected" : ""} ${shiftCount ? `data-shift-count="${shiftCount}"` : ''}>${item.name}</optin>`;
            });

            structure += `<option value="0" ${activeid === 0 ? "selected" : ""}>${endtext}</optin>`;

            return structure;

        }

      // returns shift list row structure
      shiftRow(rowid) {

        return `<tr>
                        ${this.shiftCol(rowid)}
                </tr>`;
      }

      // returns shift list column structure plannercol
      shiftCol(rowid) {

        // define main structure collector
        let mainStructure = "";

        for(let i = 0; i < 28; i++) {

          // define inner structure collector
          let innerStructure = "";

          // define shift list
          const shiftlist = this.rowControler[rowid].shiftList[i];
          
          // check for shift list
          if(shiftlist && shiftlist.length) {

            // loop throw list
            shiftlist.map((item, dataIndex) => {
              
              // restruct shift list data
              const {colId, id, shiftColor, shiftId, shiftName, currentDayTimeout} = item;
              
              // check for column id
              if(Number(colId) === i) {
                innerStructure += `
                <div class="wgsb-content-col-unit" style="background: ${shiftColor}">
                    ${shiftlist.length == 1 ? `<span>
                        ${item.shiftName}
                      </span>` : `<div class="wgsb-ccu-description">
                        ${item.shiftName}
                      </div>`}
                  </div>`;
              }
              
            });

          }

          // collect structure
          mainStructure += `<td>
                              <div class="wfm-ptc-col" data-row="${rowid}" data-col="${i}">
                                ${innerStructure}
                              </div>
                            </td>`;
        }

        return mainStructure;

      }

      // returns work and break hours summery row
      weekMonthHoursRows(rowid) {

        // define collected row hours
        const { workHoursInWeek, workHoursInMonth } = this.summaryRowWorkHours(rowid);

        // define time status
        const { statusOfWorkHoursInWeek, statusOfWorkHoursInMonth } = this.getWorkBreakHoursRowStatus(workHoursInWeek, workHoursInMonth);

        // define return structure
        return `<tr>
                    <td>
                        <div class="wfm-ptc-col footer-col">
                                <span class="wfm-ptc-hours-sum week ${statusOfWorkHoursInWeek}" data-row="${rowid}">${workHoursInWeek}</span>
                        </div>
                    </td>
                    <td>
                        <div class="wfm-ptc-col footer-col">
                                <span class="wfm-ptc-hours-sum month ${statusOfWorkHoursInMonth}" data-row="${rowid}">${workHoursInMonth}</span>
                        </div>
                    </td>
                    <td>
                        <div class="wfm-ptc-col">
                                <button class="save-as-loop" data-row="${rowid}">
                                        შეინახე ციკლად
                                </button>
                        </div>
                    </td>
                </tr>`;
      }

      // returns work and break hours summary row status
      getWorkBreakHoursRowStatus(workHoursInWeek = "00:00", workHoursInMonth = "00:00") {

        // define project max hours and splited work hours
        const projectMaxHoursInWeek = Number($("#workHoursOperator").val());
        const projectMaxHoursInMonth = projectMaxHoursInWeek * 4;
        const splitedWorkHoursInWeek = workHoursInWeek.split(":");
        const splitedWorkHoursInMonth = workHoursInMonth.split(":");

        // define time from splited work hours
        const parsedWorkHoursInWeek = Number(splitedWorkHoursInWeek[0]);
        const parsedWorkMinutesInWeek = Number(splitedWorkHoursInWeek[1]);
        const parsedWorkHoursInMonth = Number(splitedWorkHoursInMonth[0]);
        const parsedWorkMinutesInMonth = Number(splitedWorkHoursInMonth[1]);

        // define status variables
        let statusOfWorkHoursInWeek = "normal";
        let statusOfWorkHoursInMonth = "normal";

        // compare project hours and work hours in week
        if((projectMaxHoursInWeek < parsedWorkHoursInWeek) || (projectMaxHoursInWeek === parsedWorkHoursInWeek && parsedWorkMinutesInWeek > 0)) {
          statusOfWorkHoursInWeek = "over";
        } else {
          statusOfWorkHoursInWeek = "normal";
        }

        // compare project hours and work hours in month
        if((projectMaxHoursInMonth < parsedWorkHoursInMonth) || (projectMaxHoursInMonth === parsedWorkHoursInMonth && parsedWorkMinutesInMonth > 0)) {
          statusOfWorkHoursInMonth = "over";
        } else {
          statusOfWorkHoursInMonth = "normal";
        }

        // return object of statuses
        return {
          statusOfWorkHoursInWeek,
          statusOfWorkHoursInMonth
        }

      }

        // summarize planned hours
        summaryPlannedHours(rowid, colid) {

            // define hour values
            let oldValue = this.plannedHours[colid];
            let newValue = this.calcWorkAndBreakHours(this.rowControler[rowid].shiftList[colid]).workHours;

            this.plannedHours[colid] = hours.add(oldValue, newValue);

        }

        // collect new cycle shift list
        collectNewCycleShiftList(rowId) {

          // define object itself
          const self = this;

          // nullify list collector array
          self.newCycleShiftList = [];

          // loop throw all column in specific row
          $(".wfm-pts-days-body").find(`.wfm-ptc-col[data-row="${rowId}"]`).each(function(num) {

            // loop throw shift items in column
            $(this).find(".wgsb-content-col-unit").each(function() {

              // define shift data
              const shiftData = {
                num: num + 1,
                shiftId: $(this).data("shift")
              }

              // push shift data in array
              self.newCycleShiftList.push(shiftData);

            });

          });

        }

        // summaryze row work hours
        summaryRowWorkHours(rowid) {

            // define work hours collectors
            let workHoursInWeek = "00:00";
            let workHoursInMonth = "00:00";

            // check for row controller rowid and shiftlist type is it array or object
            if(this.rowControler.length && Array.isArray(this.rowControler[rowid].shiftList)) {

              // loop throw shift list
              this.rowControler[rowid].shiftList.forEach((shiftArray, i) => {

                // check for shift array
                if(shiftArray.length) {

                  // loop throw shift aray
                  shiftArray.forEach(shiftItem => {

                    // define work time
                    const workTime = hours.add(shiftItem.currentDayWorkTime, shiftItem.nexDayWorkTime);

                    // collect hours in week
                    if(i < 7) {
                      workHoursInWeek = hours.add(workHoursInWeek, workTime);
                    }

                    // collect hours in month
                    workHoursInMonth = hours.add(workHoursInMonth, workTime);

                  });

                }

              });

            } else if(this.rowControler.length && this.rowControler[rowid].shiftList instanceof Object) {

              // loop throw shift object
              for(let prop in this.rowControler[rowid].shiftList) {
                
                  // define shift array and data index
                  const dataIndex = Number(prop);
                  const shiftArray = this.rowControler[rowid].shiftList[prop];

                  // check for shift array
                  if(shiftArray.length) {

                    // loop throw shift aray
                    shiftArray.forEach(shiftItem => {

                      // define work time
                      const workTime = hours.add(shiftItem.currentDayWorkTime, shiftItem.nexDayWorkTime);

                      // collect hours in week
                      if(dataIndex < 7) {
                        workHoursInWeek = hours.add(workHoursInWeek, workTime);
                      }

                      // collect hours in month
                      workHoursInMonth = hours.add(workHoursInMonth, workTime);

                  });

                }

              }
              
            }

            // return collected times
            return {
              workHoursInWeek,
              workHoursInMonth
            }

        }

        // calculate work and break hours
        calcWorkAndBreakHours(data) {

            // resturct data
            const { curDayTimeStart, curDayTimeEnd, curDayTimeout, prevDayTimeEnd, prevDayTimeout } = data;

            // define required variables
            const prevDayTimeStart = "00:00";
            const curDayForeseenHours = hours.subtract(curDayTimeEnd, curDayTimeStart);
            const curDayWorkHours = hours.subtract(curDayForeseenHours, curDayTimeout);
            const prevDayForeseenHours = hours.subtract(prevDayTimeEnd, prevDayTimeStart);
            const prevDayWorkHours = hours.subtract(prevDayForeseenHours, prevDayTimeout);

            // define result
            return {
                fullHours: hours.add(curDayForeseenHours, prevDayForeseenHours),
                workHours: hours.add(curDayWorkHours, prevDayWorkHours),
                breakHours: hours.add(curDayTimeout, prevDayTimeout)
            };

        }

        // updates rows in table
        updateRows() {

            // define row collectors
            let operatorsRows = "";
            let shiftRows = "";
            let weekMonthHoursRows = "";

            // define rou loop
            this.rowControler.forEach((item, i) => {

              // collecting structures
              operatorsRows += this.operatorRow(i);
              shiftRows += this.shiftRow(i);
              weekMonthHoursRows += this.weekMonthHoursRows(i);

              // insert rows in relevant parents
              $(".wfm-planer-table-head tbody").html(operatorsRows);
              $(".wfm-planer-table-content tbody").html(shiftRows);
              $(".wfm-planer-table-buttons tbody").html(weekMonthHoursRows);

            });

            // distribute operated hours
            this.distributeOperatedHours()
                .calculateColumnHours();

        }

        // distribute operated hours
        distributeOperatedHours() {
          
          // loop throw operated hours array
          this.operatedHours.map((item, i) => {
            $(`.wfm-pht-operation-row .wfm-pht-col`).eq(i).find("span").text(item);
          });

          // return class itself
          return this;

        }

        // choose shift
        chooseShift(shiftReceiverData) {

          // destruct shift receiver data
          let {rowId, colId} = shiftReceiverData;

          // destruct choosed shift data
          let {color, end, id, name, start, timeout, nextDayTimeEnd, nextDayTimeOut} = this.choosedShift;

          // copy controller for update shift data
          let { prevDayTimeEnd, prevDayTimeout } = this.rowControler[rowId].shiftList[colId];


          // define new data
          let newData = {
            id: 0,
            workShiftId: id,
            shiftName: name,
            shiftColor: color,
            curDayTimeStart: start,
            curDayTimeEnd: end,
            curDayTimeout: timeout,
            prevDayTimeEnd,
            prevDayTimeout,
            activities: []
          };

          // update shift column
          // this.rowControler[rowId].cycleId = 0;
          this.rowControler[rowId].shiftList[colId] = newData;

          // update next shift prevDayWorkHours property after checking it existence
          if(this.rowControler[rowId].shiftList[colId + 1] !== undefined) {

            let nextColData = {
              ...this.rowControler[rowId].shiftList[colId + 1],
              prevDayTimeEnd: nextDayTimeEnd,
              prevDayTimeout: nextDayTimeOut
            };
            this.rowControler[rowId].shiftList[colId + 1] = nextColData;

          }

          // update rows
          this.updateRows();

        }

        // returns difference hours status class
        getDifferenceHoursStatus(checkhours) {

          // define zero and check hours as seconds
          let zeroHourInSeconds = hours.inSeconds("00:00");
          let checkHoursInSeconds = hours.inSeconds(checkhours);

          if(zeroHourInSeconds === checkHoursInSeconds) {
            return 'wfm-pht-exact-value';
          } else if(zeroHourInSeconds < checkHoursInSeconds) {
            return 'wfm-pht-less-value';
          } else if(zeroHourInSeconds > checkHoursInSeconds) {
            return 'wfm-pht-over-value';
          }

        }

        // calculate column summary hours
        calculateColumnHours() {

          // nulify receiver statuses
          $(`.wfm-pht-difference-row`).find(`.wfm-pht-col`).removeClass("wfm-pht-exact-value").removeClass("wfm-pht-less-value").removeClass("wfm-pht-over-value");

          // loop throw columns
          for(let col = 0; col < 28; col++) {

            // define column hours summary variable
            let colHours = "00:00";

            // define column relevant operated hours
            const operatedHours = $(`.wfm-pht-operation-row`).find(`.wfm-pht-col`).eq(col).find("span").text();

            // check for row controller length
            if(this.rowControler.length) {

              // loop throw row controller
              this.rowControler.forEach((item, row) => {

                // get relevant column shift times if exists
                if(item.shiftList[col] && item.shiftList[col].length) {

                  // loop throw shift data
                  item.shiftList[col].forEach(shiftItem => {

                    // get column shift work hours
                    const { currentDayWorkTime } = shiftItem;

                    // collect column hours
                    colHours = hours.add(colHours, currentDayWorkTime);

                  });
                }
                
                // get previews column shift times if exists
                if(item.shiftList[col - 1] && item.shiftList[col - 1].length) {

                  // loop throw shift data
                  item.shiftList[col - 1].forEach(shiftItem => {

                    // get column shift work hours
                    const { nexDayWorkTime } = shiftItem;

                    // collect column hours
                    colHours = hours.add(colHours, nexDayWorkTime);

                  });
                }

              });

              // get planned hours and operated hours difference
              let differneceHours = hours.subtract(colHours, operatedHours);

              // insert summarized hours in relevant receiver
              $(`.wfm-pht-paln-row`).find(`.wfm-pht-col`).eq(col).find("span").text(colHours);
              $(`.wfm-pht-difference-row`).find(`.wfm-pht-col`).eq(col).find("span").text(differneceHours);

              // define difference hours status and add it to relavant object
              let differenceHoursStatus = this.getDifferenceHoursStatus(differneceHours);
              $(`.wfm-pht-difference-row`).find(`.wfm-pht-col`).eq(col).addClass(differenceHoursStatus);
              
            }

          }

        }

        // filter operated hours
        processOperatedHours(operatedhours) {

          // define hours daTa collector
          let hoursDataCollector = [];

          // loop throw operated hours
          operatedhours.map(opHour => {

            // check if data already exists in collector array
            if(hoursDataCollector.some(datColItem => datColItem.id === opHour.id)) {

              // if data exists, increment it
              hoursDataCollector.map(mapItem => {
                if(mapItem.id === opHour.id) {
                  mapItem.hours = hours.add(mapItem.hours, opHour.hours);
                }
              });

            } else {
              hoursDataCollector.push(opHour);
            }

          });

          // loop throw all month days for update operated hours array
          this.loopThrowAllDays((index, dayIndex) => {

            // check for day index as 0
            if(dayIndex === 0) {
              dayIndex = 7;
            }

            let data = hoursDataCollector.find(item => parseInt(item.id) === dayIndex);
            this.operatedHours[index] = data ? data.hours : "00:00";

          });

        }

        // loop thro individual day by date
        loopThrowAllDays(callback) {

          // loop throw month days
          for(let i = 0; i < 28; i++) {

            // define day data
            let dayIndex = (i + 1) % 7;
            callback(i, dayIndex);

          }

        }

        // returns true or false depended on shift data
        isHolyday(data) {

          let { curDayTimeStart, curDayTimeEnd, curDayTimeout } = data;

          if(curDayTimeStart === "00:00" && curDayTimeEnd === "00:00" && curDayTimeout === "00:00") {
            return true;
          } else {
            return false;
          }

        }

        // opens cycle start posytion dialog
        openPlannerCycleStartPos(data, tableRowId, cycleId) {

          // define object itself
          const self = this;

          // define action table and data value index
          let actionTable = $(".wfm-pts-days-body");
          let dataId = 0;

          const buttons = {
            "choose": {
                text: "არჩევა",
                id: "planner-cycle-start-position-choose",
                click: function () {

                  // define reserv hours
                  let { prevDayTimeEnd, prevDayTimeout } = self.reservedCycleData;

                  // define start position
                  let startPos = parseInt($("#plannerCycleStartPosition").val()) - 1;
                  let dataCounter = 0;

                  // loop throw 28 days from start position
                  for(let i = startPos; i < 28; i++) {

                    // define day index
                    let dayIndex = (i - startPos) % data.length;

                    // check day index for add reserved data
                    if(i !== startPos && dayIndex === 0) {
                      data[dayIndex].prevDayTimeEnd = prevDayTimeEnd;
                      data[dayIndex].prevDayTimeout = prevDayTimeout;
                    }

                    // insert shift values to row controller array
                    self.rowControler[tableRowId].shiftList[i] = {
                      ...data[dayIndex]
                    };

                  }

                  // set shift value row cycle id
                  self.rowControler[tableRowId].cycleId = cycleId;

                  // update rows
                  self.updateRows();

                  // close dialog
                  $(this).dialog("close");

                }
            },
            "cancel": {
                text: "დახურვა",
                id: "cancel-dialog-maxCycle-confirm",
                click: function () {

                  // close dialog
                  $(this).dialog("close");
                }
            }
          }

          // nullfying start position input value
          $("#plannerCycleStartPosition").val("1");

          // load dialog
          GetDialog("planer-cycle-start-pos", 230, "auto", buttons, 'center center');

          // set dialog and dialog back holder z position
          $(`.ui-widget-overlay`).css({zIndex: "100"});
          $(`.ui-dialog`).css({zIndex: "101"});

        }

        saveAsCycle(data, button) {

          // send ajax request
          $.post(aJaxURL, data, result => {

            // destruct result
            let {error, save} = result;

            // checking for error
            if(error !== "") {
              alert(error);
            } else {

              // check for save
              if(save) {
                button.addClass("sasl-active");
                alert("ციკლის შენახვა წარმატებით განხორციელდა");

                // empty cycle name input
                $("#plannerNewCycle").val("");

                // update cycle list in choose select
                $.post(aJaxURL, {act: "update_planner_cycle_choose"}, lastCycle => {

                  // destruct result
                  let {error, option} = lastCycle;

                  // check for error
                  if(error!= "") {
                    alert(error);
                  } else {

                    // insert cycle list in choose selects
                    $(".wfm-planer-table-head tbody").find(".choose-work-planner-cycle").find("option:last").before(option);

                  }

                });

              } else {
                alert("ციკლის შენახვა ვერ განხორციელდა");
              }

            }

          });

        }

        // initilize events
        initEvents() {

          // scroll the planer table
          $(".wfm-pt-footer").scroll(function() {

              // define scroll position
              let leftPos = $(this).scrollLeft();

              if(leftPos > 10) {
                  leftPos -= 1;
              }

              // change position of the table
              $("#plan_table, #planer_hours_table").css({left: `-${leftPos}px`});

          });

        }

        // fetch build data
        fetchBuildData(callback) {

          // define this pointer
          const self = this;

          // send post request plannerbla
          $.ajax({
            type:"POST",
            url: aJaxURL,
            cache: false,
            data: {
              act: "fetch_work_planner_build_data",
              projectId: projectDetails.originalId
            },
            beforeSend() {

              // check if loading already going on
              if(!$(".project-details-loading-container").hasClass("active")) {

                // start loading
                $(".project-details-loading-container").addClass("active");
              }

            },
            success(responce) {

              // restruct received data
              let {cycleList, operatedHours, rowControler} = responce;

              // check for cycle list
              if(cycleList.length) {
                self.cycleList = cycleList;
              }

              // process operated hours
              self.processOperatedHours(operatedHours);

              // set row controller data
              self.rowControler = rowControler;

              // execute collback
              if(callback) callback();

            },
            complete() {

              // end loading
              setTimeout(() => {
                  $(".project-details-loading-container").removeClass("active");
              }, 1000);

            }
          });

        }

        // builds full table
        build(callback) {

          this.fetchBuildData(() => {

            // update rows
            this.updateRows();

            // init events
            this.initEvents();

            // execute callback
            if(callback) callback();

          });

        }

  }

  const workPlanner = new WorkPlanner();
  // work planner table actions (CLASS ENDS)

  // add row in table
  $(document).on("click", "#addWorkPlannerRow", function() {
      
    // define request data
    const data = {
      act: "add_planner_row",
      projectId: projectDetails.originalId,
      rowId: workPlanner.rowControler.length
    }

    // send ajax request
    $.post(aJaxURL, data, responce => {

      // restruct responce
      const { error, addStatus } = responce;

      // check for error
      if(error !== "") {
        alert(error);
      } else {

        // check for add status
        if(addStatus) {
          workPlanner.build();
        } else {
          alert("გაურკვეველი მიზეზების გამო თქვენს მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
        }

      }

    });

  });

  // delete row in table
  $(document).on("click", ".clear-work-planner-row", function() {

    // define clear data
    const clearData = {
      act: "clear_planner_row",
      dataId: $(this).data("id")
    }

    // send ajax request
    $.post(aJaxURL, clearData, responce => {

      // restruct responce
      const { error, clearStatus } = responce;

      // check for error
      if(error !== "") {
        alert(error);
      } else {

        // check for clear status
        if(clearStatus) {
          workPlanner.build();
        } else {
          alert("გაურკვეველი მიზეზების გამო, თქვენს მიერ მოთხოვნიული მოქმედება ვერ განხორციელდა");
        }

      }

    });     

  });

  // delete row in table
  $(document).on("click", ".delete-work-planner-row", function() {

    // define request data
    const data = {
      act: "delete_planner_row",
      dataId: $(this).data("id")
    }

    // send ajax request
    $.post(aJaxURL, data, responce => {

      // restruct responce
      const { error, deleteStatus } = responce;

      // check for error
      if(error !== "") {
        alert(error);
      } else {

        // check for delete status
        if(deleteStatus) {
          workPlanner.build();
        } else {
          alert("გაურკვეველი მიზეზების გამო, თქვენს მიერ მოთხოვნილი მოქმედება ვერ განხორციელდა");
        }

      }

    });

  });

  // change the cycle
  $(document).on("change", ".choose-work-planner-cycle", function() {

      // define this keyword sustitue
      const self = this;

      // define data main id and shift count
      const dataId = $(self).data("id");
      const shiftCount = $(self).find("option:selected").data("shift-count");

      // define active cycle id
      const activeCycle = workPlanner.rowControler.find(item => Number(item.id) === dataId).cycleId;

      // define dialog buttons
      let buttons = {
          "save": {
              text: "შენახვა",
              id: "save-workPlanner-cycle",
              click: function () {

                // define save data
                const saveData = {
                  act: "save_planner_cycle",
                  cycleId: Number($(self).val()),
                  cycleStartPos: Number($("#plannerCycleStartPosition").val()) - 1,
                  dataId,
                  shiftCount
                }

                // send aja request
                $.post(aJaxURL, saveData, responce => {
                    
                  // restruct responce
                  const  { error, saveStatus } = responce;

                  // check for error
                  if(error !== "") {
                    alert(error);
                  } else {

                    // check for save status
                    if(saveStatus) {

                      // update build
                      workPlanner.build();

                      // close dialog
                      $(this).dialog("close");

                    } else {
                      alert("გაურკვეველი მიზეზის გამო მოთხოვნილი მოქმედება ვერ განხორციელდა");
                    }
                    
                  }

                });                    

              }
          },
          "cancel": {
              text: "დახურვა",
              id: "cancel-workPlanner-cycle",
              click: function () {
                  $(this).dialog("close");

                  // reset cycle name
                  $(self).val(activeCycle);
              }
          }
      };

      // nullfying start position input value
      $("#plannerCycleStartPosition").val("1");

      // load dialog
      GetDialog("planer-cycle-start-pos", 230, "auto", buttons, 'center center');

      // set dialog and dialog back holder z position
      $(`.ui-widget-overlay`).css({zIndex: "100"});
      $(`.ui-dialog`).css({zIndex: "101"});

  });

  // returns planner table operator structure
  function plannerValuesStructure() {

    let collector = "<tr>";

  	for(let i = 0; i < 28; i ++) {

  		collector += `<td class="">
  											<div class="wfm-ptc-col">
  													<span data-difference="00:00" data-shift-id="0"></span>
  											</div>
  									</td>`;

  	}

    collector += "</tr>";

  	return collector;

  }

  // calculate cycle full time
  function calcCycleWeekTime(data) {

    // define data index
    let dataId = 0;
    let weekHours = "00:00";
    let monthHours = "00:00";

    // loop for 7 days
    for(let w = 0; w < 7; w++) {

      // reindex data
      dataId = w % data.length;

      // define difference
      let difference = data[dataId].shift_difference;

      // check for minus sign
      if(difference.indexOf("-") !== -1) {
        difference = difference.replace(/^-/, "");
      }

      // collect hours
      weekHours = hours.add(weekHours, difference);

    }

    // loop for 28 days
    for(let m = 0; m < 28; m++) {

      // reindex data
      dataId = m % data.length;

      // define difference
      let difference = data[dataId].shift_difference;

      // check for minus sign
      if(difference.indexOf("-") !== -1) {
        difference = difference.replace(/^-/, "");
      }

      // collect hours
      monthHours = hours.add(monthHours, difference);

    }

    return {
      weekHours: parseInt(weekHours.split(":")[0]),
      monthHours: parseInt(monthHours.split(":")[0])
    }

  }

  // minus fixer function
  function minusFixer(value) {

    if(value < 0) {
      return -value;
    } else {
      return value;
    }

  }

  // loda holidays table
  function loadHolidaysTable() {

    // define request data
    const data = {
      act: "load_holidays_table",
      projectId: projectDetails.originalId
    }

    // send request data
    $.post(aJaxURL, data, responce => {

      // destruct responce
      const { error, structure } = responce;

      // check for error
      if(error !== "") {
        alert(error);
      } else {

        // insert received structure in receiver
        $(".project-details-data-receiver").html(structure);

      }

    });
    
  }

  // compare two hours
  function compareTwoHours(greater, less) {

    // define greater hour data
    let greaterHour = greater.split(":");
    let gHour = minusFixer(parseInt(greaterHour[0]));
    let gMin = minusFixer(parseInt(greaterHour[1]));
    let gSec = minusFixer(greaterHour[2] ? parseInt(greaterHour[2]) : false);

    // define less hour data
    let lessHour = less.split(":");
    let lHour = minusFixer(parseInt(lessHour[0]));
    let lMin = minusFixer(parseInt(lessHour[1]));
    let lSec = minusFixer(lessHour[2] ? parseInt(lessHour[2]) : false);

    // compare
    if((gHour < lHour) || (gHour === lHour && gMin < lMin) || (gHour === lHour && gMin === lMin && (gSec && lSec && gSec < lSec))) {
      return false;
    } else {
      return true;
    }

  }

  // click on planner shift unit
  $(document).on("click", ".wfm-pts-days-body .wfm-ptc-col", function() {

    // define self as this object
    let self = this;

    // defie row and col and shift id
    let rowId = $(this).data("row");
    let colId = $(this).data("col");

    // define ajax request data
    const data = {
      act: "get_wg_shift_dialog",
      type: "planner",
      projectId: projectDetails.originalId,
      rowId,
      colId
    }

    // send ajax request
    $.post(aJaxURL, data, result => {

      // destruct result
      let {error, dialog} = result;

      // check for error
      if(error !== "") {
        alert(error);
      } else {

          // define dialog buttons
          const buttons = {
                "save": {
                    text: "შენახვა",
                    id: "save-planer-shift-dialog",
                    click: function () {

                      // define choosed shift id
                      const shiftId = Number($("#plannerShiftShoose").val());

                      // check for shift id
                      if(shiftId !== "" && shiftId !== undefined && shiftId !== null && shiftId !== 0) {

                        // define ajax request data
                        const data = {
                          act: "save_planner_shift",
                          projectId: projectDetails.originalId,
                          rowId,
                          colId,
                          shiftId
                        }

                        // send ajax request
                        $.post(aJaxURL, data, responce => {

                            // restruct responce
                            const { error, result, shiftList } = responce;
                            
                            // check for error
                            if(error !== "") {
                              alert(error);
                            } else {
                              
                              // restruct result
                              const { complete, denialReasone } = result;

                              // check if complete do not implemented
                              if(!complete) {

                                // end loading
                                $(".project-details-loading-container").removeClass("active");

                                // check for denial reasone
                                switch(denialReasone) {
                                  case 'id':
                                    alert("ერთ დღეზე შეუძლებელია დაემატოს ერთიდაიგივე ცვლა");
                                    break;
                                  case 'time':
                                    alert("არჩეული ცვლის დრო ჰკვეთს უკვე არსებული ცვლ(ებ)ის დროს");
                                    break;
                                }

                              } else {

                                // build graphic
                                workPlanner.build(() => {

                                  // reinsert shift list
                                  $(".wg-shift-list-container").remove();
                                  $(".wg-shift-list-add").before(shiftList);

                                });
                              }

                            }

                        });

                      } else {
                        alert("გთხოვთ მიუთითოთ ცვლა");
                      }

                    }
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function () {
                      $(this).dialog("close");
                    }
                }
            };

          // insert received dialog structure in the dialog wrapper
          $("#planer-shift-controler").html(dialog);

          // stylize details
          $(".jquery-ui-chosen").chosen({
            search_contains: true
          });
          $("#plannerShiftShoose_chosen").css({width: "100%"});
          $("#planer-shift-controler").css({overflow: "visible"});

          // load dialog
          GetDialog("planer-shift-controler", 300, "auto", buttons, `center center`);

          // set dialog and dialog back holder z position
          $(`.ui-widget-overlay`).css({zIndex: "100"});
          $(`.ui-dialog`).css({zIndex: "101"});

      }

    });

  });

  // click on planner add shift button
  $(document).on("click", "#plannerAddShift", () => {

    // define ajax request data
    let data = {
      act: "get_planer_add_shift_dialog"
    }

    // send ajax request
    $.post(aJaxURL, data, result => {

      // destruct result data
      let {error, dialog} = result;

      // check for error
      if(error !== "") {
        alert(error);
      } else {

        // define dialog buttons
        let buttons = {
              "save": {
                  text: "შენახვა",
                  id: "save-dialog-planner-as",
                  click: function () {

                    // define new shift data
                    let data = {
                      act: "save_new_shift_from_planner",
                      name: $("#plannerNewShiftName").val(),
                      start: $("#plannerNewShiftStart").val(),
                      end: $("#plannerNewShiftEnd").val(),
                      break: $("#plannerNewShiftBreak").val(),
                      color: $("#plannerNewShiftColor").val(),
                      wfmDep: $("#plannerNewShiftProjectId").val(),
                      payment: $("#plannerNewShiftPayment").val(),
                      workType: $("#plannerNewShiftType").val(),
                      comment: $("#plannerNewShiftComment").val()
                    }

                    // check required data existence
                    if(data.name === "") {
                      alert("გთხოვთ მიუთითოთ დასახელება");
                    } else if(data.start === "") {
                      alert("გთხოვთ მიუთითოთ ცვლის დასაწყისი");
                    } else if(data.end === "") {
                      alert("გთხოვთ მიუთითოთ ცვლის დასასრული");
                    } else if(data.break === "") {
                      alert("გთხოვთ მიუთითოთ შესვენების ხანგრძლივობა");
                    } else {

                      // send ajax request
                      $.post(aJaxURL, data, result => {

                        // destruct result
                        let {error, save, lastInsertedId, shiftlist} = result;

                        // check for error
                        if(error !== "") {
                          alert(error);
                        } else {

                          // check for save status
                          if(save && save !== "exists") {

                            // inform user
                            alert("ცვლა წარმატებით დაემატა ბაზაში");

                            // insert shift list in relevant parent
                            $("#plannerShiftShoose").html(shiftlist);
                            $("#plannerShiftShoose").val(lastInsertedId).trigger("chosen:updated");

                            // close dialog
                            $(this).dialog("close");

                          } else if(save && save === "exists") {

                            // inform user
                            alert("მსგავსი დასახელების ცვლა უკვე არსებობს ბაზაში");
                          } else {

                            // inform user
                            alert("ცვლა ვერ დაემატა ბაზაში");
                          }

                        }

                      });

                    }

                  }
              },
            	"cancel": {
    	            text: "დახურვა",
    	            id: "cancel-dialog-planner-as",
    	            click: function () {
    	            	$(this).dialog("close");

    	            }
    	        }
      	    };

        // insert dialog structure in relevant parent
        $("#planer-add-shift").html(dialog);

        // stylize inputs
        $('.jquery-ui-timepicker').timepicker({
          hourMax: 23,
          hourMin: 0,
          minuteMax: 55,
          minuteMin: 0,
          stepMinute: 15,
          minuteGrid: 15,
          hourGrid: 3,
          timeFormat: 'HH:mm'
        });


        // load dialog
        GetDialog("planer-add-shift", 350, "auto", buttons, 'center center-50px');

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

      }

    });

  });

  // change break time
  $(document).on("change", "#plannerNewShiftBreak", function() {

    // define data
    let breakTime = $(this).val();
    let shiftFullTime = $("#plannerNewShiftFullDur").val();
    let workTime = hours.subtract(shiftFullTime, breakTime);

    // insert break time in relevant input
    $("#plannerNewShiftWorkDur").val(workTime);

  });

  // control break range
  $(document).on("change", "#plannerNewShiftBreakSt, #plannerNewShiftBreakEn", function() {
    shiftBreakRangeController(this);
  });

  // calculate shift full and work hours
  $(document).on("change", "#plannerNewShiftStart, #plannerNewShiftEnd, #plannerNewShiftBreak", () => {

    // define calculate data
    let startTime = $("#plannerNewShiftStart").val();
    let endTime = $("#plannerNewShiftEnd").val();
    let breakTime = $("#plannerNewShiftBreak").val();
    let shiftFullHours = "";

    // initilize data variables
    startTime = startTime === "" ? "00:00" : startTime;
    endTime = endTime === "" ? "00:00" : endTime;
    breakTime = breakTime === "" ? "00:00" : breakTime;

    //compare two times
    let startTimeIsGreater = compareTwoHours(startTime, endTime);

    // check if start time is greater
    if(startTimeIsGreater) {
      let conductor = hours.subtract("24:00", startTime);
      shiftFullHours = hours.add(conductor, endTime);
    } else {
      shiftFullHours = hours.subtract(endTime, startTime);
    }

    // calculate shift work hours
    let shiftWorkHours = hours.subtract(shiftFullHours, breakTime);

    // distribute result values
    $("#plannerNewShiftFullDur").val(shiftFullHours);
    $("#plannerNewShiftWorkDur").val(shiftWorkHours);

  });

  // toggle planner add shift amply data
  $(document).on("click", "#plannerShiftAmply", function() {

    // define toggle status
    let togStatus = $(this).hasClass("open") ? true : false;

    // check toggle status
    if(togStatus) {

      $(this).text("მოკლედ").removeClass("open").addClass("close");
      $("#plannerShiftAddTableBody").removeClass("psa-hidden");

    } else {

      $(this).text("ვრცლად").removeClass("close").addClass("open");
      $("#plannerShiftAddTableBody").addClass("psa-hidden");

    }

  });


  // planner cycle save click
  $(document).on("click", ".save-as-loop", function(event) {

    // check for active class
    if(!$(this).hasClass("sasl-active")) {

      // define data
      let self = $(this);
      let rowId = $(this).data("row");
      let cycleId = Number(workPlanner.rowControler[rowId].cycleId);

      // check if saving new cycle
      if(cycleId === 0) {

        // define dialog button
        let buttons = {
              "save": {
                  text: "შენახვა",
                  id: "save-dialog-planner-cycle",
                  click: function () {

                    // define save data
                    const saveData = {
                      act: "save_cycle_from_planner",
                      projectId: projectDetails.originalId,
                      cycleName: $("#plannerNewCycle").val(),
                      shiftList: workPlanner.newCycleShiftList
                    }

                    // check for new cycle save data
                    if(!saveData.cycleName && saveData.shiftList.length) {
                      alert("გთხოვთ მიუთითოთ ციკლის დასახელება");
                    } else if(!saveData.shiftList.length) {
                      alert("შესანახი მონაცემები ვერ მოიძებნა");
                      $(this).dialog("close");
                    } else {

                      // send ajax request
                      $.post(aJaxURL, saveData, responce => {

                        // destruct responce
                        const { error, save, cycleList } = responce;

                        // check for error
                        if(error !== "") {
                          alert(error);
                        } else {

                          // check for save
                          if(save && save !== "exists") {

                            // define updated cycle list
                            const cycleListStructure = workPlanner.makeOptionList(cycleList, 0, "აირჩიეთ ციკლი");

                            // insert list in receivers
                            $(".choose-work-planner-cycle").html(cycleListStructure);

                            // activate cycle make button
                            $(self).addClass("sasl-active");

                            // inform user
                            alert("ციკლი წარმატებით შეიქმნა");
                            
                            // lose dialog
                            $(this).dialog("close");

                          } else if(save && save === "exists") {

                            // inform user
                            alert("მსგავსი დასახელების ციკლი უკვე არსებობს ბაზაში");

                          } else {

                            // inform user
                            alert("გაურკვეველი მიზეზების გამო თქვენ მიერ მოტხოვნილი მოქმედება ვერ განხორციელდა");

                          }

                        }

                      });
                      
                    }

                  }
              },
            	"cancel": {
    	            text: "დახურვა",
    	            id: "cancel-dialog-planner-cycle",
    	            click: function () {
    	            	$(this).dialog("close");
    	            }
    	        }
      	    };

        // collect new cycle shift list
        workPlanner.collectNewCycleShiftList(rowId);

        //open new cycle name input dialog
        GetDialog("planer-add-cycle-name", 200, "auto", buttons, 'center center-50');

        // set dialog and dialog back holder z position
        $(`.ui-widget-overlay`).css({zIndex: "100"});
        $(`.ui-dialog`).css({zIndex: "101"});

        // clear the input
        $("#plannerNewCycle").val("");

      } else {
        alert("მიმდინარე სტრიქონში დაფიქსირებულია უკვე არსებული ციკლი");
      }

    }

  });

  // click on directory menu not active items
  $(document).on("click", ".directory-menu-wrapper ul li:not(.active)", function() {

    // define page type
    const page = $(this).data("page");

    // reactivate menu items
    reactivateSimilars(".directory-menu-wrapper ul li", this, "active");

    // load irectiry page
    loadDirPage(page);

  });


  // click on project data loader not active
  $(document).on("click", ".project-data-loader:not(.active)", function() {

    // define data type
    const type = $(this).data("type");

    // set data type
    $G.projectPageIsOpen = type;

    // reactivate menu item
    $(".project-data-loader").removeClass("active");
    $(this).addClass("active");

    // define load data
    const data = {
      act: `load_project_data_structure`,
      projectId: projectDetails.originalId,
      type
    }

    // send ajax request
    $.ajax({
      type:"POST",
      url: aJaxURL,
      cache: false,
      data,
      beforeSend() {

        // start loading
        $(".project-details-loading-container").addClass("active");

      },
      success(responce) {

          // restruct responce
          const { error, structure } = responce;

          // check for error
          if(error !== "") {
            aldert(error);
          } else {

            // insert received structure in receiver
            $(".project-details-data-receiver").html(structure);

            // build project data
            buildProjectDataByType(type);
            
          }

      }

    });  

  });

/*=================================================================================== PLANNER ACTIONS ENDS ==========================================================================*/

/*=================================================================================== REPORT ACTIONS STARTS ==========================================================================*/

  // define main class
  class Report {

// mane constructor function
constructor() {
  this.buildRequest = {};
  this.rowControler = [];
}

// returns build request data
getBuildRequestData() {
  return {
    act: "fetch_report_build_data",
    date: {
      start: $("#wfmReportFilterStartDate").val(),
      end: $("#wfmReportFilterEndDate").val()
    },
    operators: $("#wfmReportOperators").val() === null ? '' : $("#wfmReportOperators").val().join(","),
    activities: $("#wfmReportActivities").val() === null ? '' : $("#wfmReportActivities").val().join(","),
    sort: {
      type: $(".wfm-report-sort.active").data("sort"),
      dir: $(".wfm-report-sort.active").data("sort-dir")
    }
  }
}

// upates build request object
updateBuildRequest() {

  // reasign request data to relevant object
  this.buildRequest = {
    ...this.getBuildRequestData()
  };

  // return class itself
  return this;

}

// customize dom elements
customize() {

  // stylize filter inputs
  $(".jquery-chosen").chosen();
  $("#wfmReportActivities_chosen, #wfmReportOperators_chosen").css({width: "200px"});
  $(".jquery-time-picker").datetimepicker({
    format:'YYYY-MM-DD HH:mm:ss'
  });


  // return object itself
  return this;

}

makeHiCharts(chartsdata) {

  // define distinct operators array
  const categories = [... new Set(chartsdata.map(item => item.operator))];

  // define distinct activitie array
  const activities = [... new Set(chartsdata.map(item => item.activitieName))];

  // define values
  const series = activities.map(activitie => {

    // define properties
    const name = activitie;
    const color = chartsdata.find(data => data.activitieName === activitie).activitieColor;
    const data = categories.map(operator => {

      // get actually time
      let actuallyTime = chartsdata.find(data => data.activitieName === activitie && data.operator === operator);

      // if actually time is not defined it equals 0
      actuallyTime = actuallyTime !== undefined && actuallyTime.actuallyTime !== null ? actuallyTime.actuallyTime : "00:00:00";

      // return value in seconds
      return hours.inSeconds(actuallyTime);

    });

    // return value
    return {
        name,
        color,
        data
    };

  });

  // Create the chart
  Highcharts.chart('chartReportContainer', {
      chart: {
          type: 'bar'
      },
      title: {
          text: 'რეპორტი'
      },
      subtitle: {
          text: 'ფაქტიური ხანგრძლივობის მიხედვით'
      },
      xAxis: {
          categories,
          title: {
              text: null
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'ხანგრძლივობა (წამებში)',
              align: 'high'
          },
          labels: {
              overflow: 'justify'
          }
      },
      tooltip: {
          valueSuffix: ' წმ'
      },
      plotOptions: {
          bar: {
              dataLabels: {
                  enabled: true
              }
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          backgroundColor: '#FFFFFF',
          shadow: true
      },
      credits: {
          enabled: false
      },
      series
  });

}

// returns content receiver row structure
contentRow(item) {

  // restruct item
  const { user_id, operator, activitieName, activitieColor, plannedTime, real_work_time, work_time, work_time_diff, break_time, break_time_diff, actuallyTime } = item;

  // var time = plannedTime;
  // var minutes = Math.floor(time / 60);
  // var hours = Math.floor(minutes / 60);
  // var seconds = time - (minutes * 60);
  // minutes = minutes - hours * 60;
 
  // // define planned hours summary
  // const plannedHoursSum = `${hours}:${minutes}:${seconds}`;

  // secTotime();
  // var time = break_time_diff;
  // var minutes = Math.floor(time / 60);
  // var hours = Math.floor(minutes / 60);
  // var seconds = time - (minutes * 60);
  // minutes = minutes - hours * 60;

  // if (hours < 10) hours = `0${hours}`;
  // if (minutes < 10) minutes = `0${minutes}`;
  // if (seconds < 10) seconds = `0${seconds}`;

  // const break_time_diff_time = `${hours}:${minutes}:${seconds}`;

  // return structure
  return `<div class="wfm-report-row">
            <div class="wfm-report-col wfm-rc-lefter">
              <span class="op" user_id = "${user_id}">${operator}</span>
            </div>
            <div class="wfm-report-col wfm-rc-center">
              <span>${secTotime(work_time)}</span>
            </div><div class="wfm-report-col wfm-rc-center">
              <span>${secTotime(real_work_time)}</span>
            </div><div class="wfm-report-col wfm-rc-center">
              <span>${secTotime(work_time_diff)}</span>
            </div>
           
            <div class="wfm-report-col wfm-rc-center">
              <span>${secTotime(break_time)}</span>
            </div>
            <div class="wfm-report-col wfm-rc-center">
              <span>${secTotime(actuallyTime)}</span>
            </div>
            <div class="wfm-report-col wfm-rc-center">
              <span>${secTotime(break_time_diff)}</span>
            </div>
           
          </div>`;
}

// returns empty row structure
emptyRow() {

  return `<div class="wfm-report-empty-row">
            <div class="wfm-report-empty-col">
              <span>ჩამონათვალი ცარიელია</span>
            </div>
          </div>`;
}

// update rows
updateRows() {

  // define structure collector variable
  let structureCollector = '';

  // check rows length
  if(this.rowControler.length) {

    this.rowControler.forEach(item => {
      structureCollector += this.contentRow(item);
    });


  } else {
    structureCollector = this.emptyRow();
  }

  // insert structure in structure receiver
  $(".wfm-report-table-body").html(structureCollector);

}


// fetch build data
fetchBuildData(callback) {

  // define this pointer
  const self = this;

  // send post request
  $.ajax({
    type:"POST",
    url: aJaxURL,
    cache: false,
    data: self.buildRequest,
    beforeSend() {

    },
    success(responce) {

      // restruct received data
      let { rowControler } = responce;

      // spread out row controller data
      self.rowControler = [...rowControler];

      // make hichart
      self.makeHiCharts(self.rowControler);

      // execute collback
      if(callback) callback();

    },
    complete() {

    }
  });

}

// table builder
build() {

  // initilize data
  this.updateBuildRequest()
      .fetchBuildData(() => {
        this.updateRows();
      });

}

}

// define main class
const report = new Report();

// change report filters
// $(document).on("change", ".wfm-report-filter", () => report.build()); 
$(document).on("click", "#button-filter", () => report.build()); 

// change sorting
$(document).on("click", ".wfm-report-sort", function() {

// define if button is active
const isActive = $(this).hasClass("active");

// check if button is active, change order direction
if(isActive) {

  // define sort direction
  let sortDir = $(this).data("sort-dir");

  // change sort direction
  switch(sortDir) {
    case 'ASC':
      sortDir = "DESC";
      $(this).find("i").text("arrow_drop_down");
    break;
    case 'DESC':
      sortDir = "ASC";
      $(this).find("i").text("arrow_drop_up");
    break;
  }

  $(this).attr("data-sort-dir", sortDir)
         .data("sort-dir", sortDir);

} else {

  // diactivate all sort buttons and activate clicked one
  $(".wfm-report-sort").removeClass("active")
                       .attr("data-sort-dir", "DESC")
                       .data("sort-dir", "DESC")
                       .find("i").text("arrow_drop_down");
  $(this).addClass("active");

}

// build report table
report.build();

});


function secTotime(sec){

  if (sec == "გასულია" || sec == "პროგრამაშია"){
    return sec;
  }
  else{

  var time = Math.abs(sec);
  var minutes = Math.floor(time / 60);
  var hours = Math.floor(minutes / 60);
  var seconds = time - (minutes * 60);
  minutes = minutes - hours * 60;

  if (hours < 10) hours = `0${hours}`;
  if (minutes < 10) minutes = `0${minutes}`;
  if (seconds < 10) seconds = `0${seconds}`;

  const result = `${hours}:${minutes}:${seconds}`;

  return result;
  }
}

$(document).on("click",".op",function(){
  let buttons = {
    "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {

                              // close dialog
                              $(this).dialog("close");

                            }
                        }
            };

  var obj = new Object();
  var id = $(this).attr("user_id");
  obj.id = id;
  obj.start = $("#wfmReportFilterStartDate").val();
  obj.end = $("#wfmReportFilterEndDate").val();
  obj.act = "get_user_table";
  $.ajax({
      url:aJaxURL,
      data:obj,
      success:function(data){
            // define structure collector variable
            let structureCollector = '';
          // check rows length
          if(data.rowControler.length) {

            data.rowControler.forEach(item => {
              structureCollector += getRow(item);
            });


          } else {
            structureCollector = `<div class="wfm-report-empty-row">
            <div class="wfm-report-empty-col">
              <span>ჩამონათვალი ცარიელია</span>
            </div>
          </div>`;
          }

          // insert structure in structure receiver
          $(".operator-table-body").html(structureCollector);
      }
  })

  GetDialog("operator-dialog", 1400, "auto", buttons, `center cneter`);
})


function getRow(item) {

// restruct item
const { user_id, operator, activitieName, datetime, income, all_time, income_real, income_diff, out, out_real, out_diff, activitieColor, plannedTime, real_work_time, work_time, work_time_diff, break_time, break_time_diff, actuallyTime } = item;

// var time = break_time_diff;
//   var minutes = Math.floor(time / 60);
//   var hours = Math.floor(minutes / 60);
//   var seconds = time - (minutes * 60);
//   minutes = minutes - hours * 60;

  // if (hours < 10) hours = `0${hours}`;
  // if (minutes < 10) minutes = `0${minutes}`;
  // if (seconds < 10) seconds = `0${seconds}`;

  // const break_time_diff_time = `${hours}:${minutes}:${seconds}`;



// return structure
return `<div class="operator-report-row">
          <div class="wfm-report-col wfm-rc-lefter">
            <span>${datetime}</span>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(income)}</span>
          </div><div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(income_real)}</span>
          </div><div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(income_diff)}</span>
          </div>
          
          <div class="wfm-report-col wfm-rc-lefter">
            <div class="wfm-report-activitie-wrapper">
              <div class="wfm-report-activitie-color" style="background:${activitieColor};"></div>
              <div class="wfm-report-activitie-name">
                <span>${activitieName}</span>
              </div>
            </div>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(break_time)}</span>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(actuallyTime)}</span>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(break_time_diff)}</span>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(out)}</span>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(out_real)}</span>
          </div>
          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(out_diff)}</span>
          </div>

          <div class="wfm-report-col wfm-rc-center">
            <span>${secTotime(all_time)}</span>
          </div>
         
        </div>`;
}


/*=================================================================================== REPORT ACTIONS ENDS ==========================================================================*/

  //script end

</script>
</head>

<body>
<div id="tabs" style="width: 90%">
    <div class="callapp_head">WFM მონაცემები<hr class="callapp_head_hr"></div>
      
        <!-- directory menu -->
        <div class="directory-menu-wrapper">
          <ul>
            <li class="active" data-page="projects">
              <i class="fa fa-tasks"></i>
              <span>პროექტები</span>
            </li>
            <li data-page="holidays">
              <i class="fa fa-couch"></i>
              <span>დასვენების დღეები</span>
            </li>
            <li data-page="cycles">
              <i class="fa fa-recycle"></i>
              <span>ციკლები</span>
            </li>
            <li data-page="shifts">
              <i class="fab fa-nintendo-switch"></i>
              <span>ცვლები</span>
            </li>
            <li data-page="activities">
              <i class="fa fa-chart-line"></i>
              <span>აქტივობები</span>
            </li>
            <li data-page="report">
              <i class="fa fa-flag"></i>
              <span>რეპორტი</span>
            </li>
          </ul>
        </div>

        <!-- directory data receiver -->
        <div class="directory-pages-wrapper">
          <div id="dirReceiver" style="display: inline-block; position: absolute; width: 82%;">

          </div>
          <div class="dpw-loading-container">
              <div>
                <i class="fa fa-cog"></i>
                <p>მიმდინარეობს მონაცემთა დამუშავება</p>
              </div>
          </div>
        </div>
        

        
</div>

<!-- jQuery Dialog -->
    <div  id="add-new-project" class="form-dialog" title="პროექტის დამატება">
        <div id="project-data-wrapper">

        </div>
    </div>
    <div  id="copy-project" class="form-dialog" title="არსებული პროეცტები">
    </div>
    <div  id="holiday-list" class="form-dialog" title="არასამუშაო დღეები">
    </div>
    <div  id="add-edit-form" class="form-dialog" title="შემომავალი ზარი">
    </div>
	<div  id="add-edit-form-hour" class="form-dialog" title="წუთი">
	</div>
	<div  id="add-edit-form-weekADD" class="form-dialog" title="დამატება">
	</div>
	<div  id="add-edit-form-week" class="form-dialog" title="სადგური/ სამუშაო დრო/ კვირა">
	</div>
	<div  id="add-edit-form-cikle" class="form-dialog" title="სატელეფონო სადგური/სთ/ოპერატორი">
	</div>
	<div  id="add-edit-form-lang" class="form-dialog" title="სასაუბრო ენა">
	</div>
	<div  id="add-edit-form-infosorce" class="form-dialog" title="ინფორმაციის წყარო">
	</div>
    <div  id="new-holiday-form" class="form-dialog" title="დაამატეთ არასამუშაო დღე">
	</div>
    <div id="add-edit-form-holidays" class="form-dialog" title="დასვენების დღე">
    </div>
    <div id="add-edit-form-workshift" class="form-dialog" title="სამუშაო ცვლები">
    </div>
    <div id="add-edit-form-workcycle" class="form-dialog" title="სამუშაო ციკლები">
    </div>
    <div id="add-edit-form-activities" class="form-dialog" title="აქტივობები">
    </div>
    <div id="add-edit-form-activities-range" class="form-dialog" title="აქტივობები">
    </div>
    <div id="add-new-hours-dialog" class="form-dialog" title="სამუშაო საათები">
    </div>
    <div id="add-edit-form-hours-ind-day" class="form-dialog" title="სამუშაო საათები">
    </div>
    <div id="add-edit-cycle-workshift" class="form-dialog" title="ცვლები">
  	</div>
    <div id="add-edit-form-workGraphicSchema" class="form-dialog" title="გრაფიკი">
  	</div>
    <div id="planer-shift-controler" class="form-dialog" title="ცვლის დამატება">
  	</div>
    <div id="planer-add-shift" class="form-dialog" title="ცვლა">
  	</div>
    <div id="planer-add-cycle-name" class="form-dialog" title="ციკლი">
      <label for="plannerNewCycle">მიუთითეთ დასახელება</label>
      <input type="text" name="planner_new_cycle" id="plannerNewCycle">
  	</div>
    <div id="planer-cycle-start-pos" class="form-dialog" title="ციკლი">
      <label for="">აირჩიეთ ციკლის საწყისი პოზიცია</label>
      <input type="number" name="planner_cycle_start_position" id="plannerCycleStartPosition" min="1" max="28" value="1">
  	</div>

    <div id="workGraphic-cycle-start-pos" class="form-dialog" title="ციკლი დაიწყოს">

  	</div>
    <div id="workGraphic-day-graphic" class="form-dialog" title="დღის გრაფიკი">

  	</div>
    <div id="workGraphic-choose-activitie" class="form-dialog" title="აირჩიეთ აქტივობა">

  	</div>
    <div id="rush-hours-confirm" class="form-dialog" title="აირჩიეთ მოქმედება">

  	</div>
    <div id="graphic-edit-history" class="form-dialog" title="ცვლილებების ისტორია">

  	</div>
    <div id="confirm-dialog" class="form-dialog" title="გთხოვთ დაადასტუროთ">
      <p class="confirm-p"></p>
  	</div>

    <div id="operator-dialog" class="form-dialog" title="ოპერატორი">
            <div class="operator-table">
                <div class="operator-table-head">
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                        <span>თარიღი</span>
                        <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                          <i class="material-icons">arrow_drop_down</i>
                        </button>
					            </div>

					            <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                        <span>მოსვლა გეგმიური</span>
                        <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                          <i class="material-icons">arrow_drop_down</i>
                        </button>
					            </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>მოსვლა ფაქტიური</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                          <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>სხვაობა</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>აქტივობა</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>აქტივობა გეგმიური</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>აქტივობა ფაქტიური</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>სხვაობა</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>წასვლა გეგმიური</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>წასვლა ფაქტიური</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>სხვაობა</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
                      <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
                          <span>ხ-ბა</span>
                          <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
                            <i class="material-icons">arrow_drop_down</i>
                          </button>
                      </div>
					        </div>
					
                  <div class="operator-table-body">

                  </div>
             </div>

  	</div>

    <!-- not jquery dialog -->
    <div id="edit-existed-project" class="">

    </div>

</body>
