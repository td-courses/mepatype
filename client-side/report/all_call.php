<head>
<style type="text/css">
 
</style>
<script type="text/javascript">
    var aJaxURL           = "server-side/report/all_call.action.php";
    var tName             = "table_";
    var dialog            = "add-edit-form";
    var colum_number      = 9;
    var main_act          = "get_list";
    var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
     
    $(document).ready(function () {
    	GetDateTimes('start_date');
    	GetDateTimes('end_date');
    	LoadTable('index',colum_number,main_act,change_colum_main);
    	$('#operator_id,#tab_id').chosen({ search_contains: true });
    	$('.callapp_filter_body').css('display','none');
    	
       

        $.session.clear(); 
    });

    function LoadTable(tbl,col_num,act,change_colum){

        // var chars = [];
        // var columnNames = $("#"+ tName + tbl + " #datatable_header").each(function(){
        //     $(this).find("th").each(function(){
        //         chars.push($(this).text());
        //     })
        // });
        
    	GetDataTable(tName+tbl,
    	    	aJaxURL,
    	    	act,
    	    	col_num,
    	    	"start_date="+$('#start_date').val()+
                "&end_date="+$('#end_date').val()+
                "&filter_1="+$('#filter_1:checked').val()+
                "&filter_2="+$('#filter_2:checked').val()+
                "&filter_3="+$('#filter_3:checked').val()+
                "&filter_4="+$('#filter_4:checked').val()+
                "&filter_5="+$('#filter_5:checked').val()+
                "&filter_6="+$('#filter_6:checked').val(),
    	    	0,
    	    	"",
    	    	1,
    	    	"desc",
    	    	'',
                change_colum);
                
    	setTimeout(function(){
	    	$('.ColVis, .dataTable_buttons').css('display','none');
	    	}, 0);
    }


$(document).on("click", "#table_index tbody td:last-child p", function(){
    $("audio").css("display","block");
})

    $(document).on("click", ".callapp_refresh", function () {
    	LoadTable('index',colum_number,main_act,change_colum_main);
    });
    
    $(document).on("click", "#loadtable", function () {
            LoadTable('index',colum_number,main_act,change_colum_main);
            // $('.callapp_filter_body').css('display','none');
            // $('.callapp_filter_body').attr('myvar',0);
            // $("#shh").css('transform', 'rotate(180deg)');
        });

    // $(document).on("change", "#end_date", function () {
    // 	LoadTable('index',colum_number,main_act,change_colum_main);
    // });

    // $(document).on("change", "#start_date", function () {
    // 	LoadTable('index',colum_number,main_act,change_colum_main);
    // });

    $(document).on("click", "#callapp_show_filter_button", function () {
        if($('.callapp_filter_body').attr('myvar') == 0){
        	$('.callapp_filter_body').css('display','block');
        	$('.callapp_filter_body').attr('myvar',1);
            $("#shh").css('transform', 'rotate(0)');

        }else{
        	$('.callapp_filter_body').css('display','none');
        	$('.callapp_filter_body').attr('myvar',0);
            $("#shh").css('transform', 'rotate(180deg)');
        }        
    });
    $(document).on('click','.report_audio',function(){
            var operator = $(this).attr('data-audio');
            load_audio_modal(operator)
        });
        function load_audio_modal(operator){
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_audio_calls&call_log="+operator,
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#audio").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("audio",operator);
                           }
                       }
                   }
               }
           });
        }
        function LoadDialog(fName,operator=0,call_id=0){
            //alert(fName);
			switch(fName){
				
                case "audio":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("audio", 500, "auto", buttons);
                    //GetDataTable("info_table8", aJaxURL, "transfer",20, "operator="+operator, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "noanswer_missed", aJaxURL, "");			
					break;
			}
							
			
		}
</script>
<style type="text/css">
.callapp_tabs{
	margin-top: 5px;
	margin-bottom: 5px;
	float: right;
	width: 100%;
	height: auto;
}
.callapp_tabs span{
	color: #FFF;
    border-radius: 5px;
    padding: 5px;
	float: left;
	margin: 0 3px 0 3px;
	background: #2681DC;
	font-weight: bold;
	font-size: 11px;
    margin-bottom: 2px;
}

.callapp_tabs span close{
	cursor: pointer;
	margin-left: 5px;
}

.callapp_head{
	font-family: pvn;
	font-weight: bold;
	font-size: 20px;
	color: #2681DC;
}
.callapp_head_hr{
	border: 1px solid #2681DC;
}
.callapp_refresh{
    padding: 5px;
    border-radius:3px;
    color:#FFF;
    background: #9AAF24;
    float: right;
    font-size: 13px;
    cursor: pointer;
}

.callapp_filter_body{
	width: 100%;
	height: 30px;
	margin-bottom: 42px;
}
.callapp_filter_body span {
	float: left;
    margin-right: 10px;
   
    margin-bottom: 5px;
}
.callapp_filter_body span label {
	color: #555;
    font-weight: bold;
	/* margin-left: 20px; */
    font-family: BPG;
}

.callapp_filter_header{
	color: #2681DC;
	font-family: pvn;
	font-weight: bold;
}

.ColVis, .dataTable_buttons{
	z-index: 50;
}
/* #flesh_panel{
    height: 630px;
    width: 150px;
    position: absolute;
    top: 0;
    padding: 15px;
    right: 2px;
	z-index: 49;
	background: #FFF;
} */
#flesh_panel {
    font-size: 12px
}
#table_sms_length{
	position: inherit;
    width: 0px;
	float: left;
}
#table_sms_length label select{
	width: 60px;
    font-size: 10px;
    padding: 0;
    height: 18px;
}
#table_sms_paginate{
	margin: 0;
}
#table_mail_length{
	position: inherit;
    width: 0px;
	float: left;
}
#table_mail_length label select{
	width: 60px;
    font-size: 10px;
    padding: 0;
    height: 18px;
}
#table_mail_paginate{
	margin: 0;
}

#table_index tbody td:last-child {
  padding: 0;
  min-width: 132px !important;
  line-height: 4px;
}


audio {
    display: none;
}

</style>
</head>

<body>
<div id="tabs">
<div class="callapp_head">ზარები/ საუბრის ჩანაწერები<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">   განახლება</span><hr class="callapp_head_hr"></div>
<div class="callapp_tabs">
</div>
<div class="callapp_filter_show">
        <button id="callapp_show_filter_button" style="background: #fff; margin-bottom: 26px">ფილტრი
            <div id="shh" style="background: url('media/images/icons/lemons_filter.png') 0px 9px no-repeat; float: right; transform: rotate(180deg);"></div>
        </button>
        <div class="callapp_filter_body" myvar="0" style="margin-top: -19px;">
            <div style="float: left; width: 100%;">
                <table>
                    <tr>
                        <td class="l_filter_date" style="width: 386px; vertical-align: middle; padding: 21px 0; ">
                            <span>
                                <input class="callapp_filter_body_span_input" value="<?php echo date("Y-m-d 00:00:00");?>" type="text" id="start_date" style="width: 147px;">
                                <label for="start_date" style="padding: 4px 0;">-დან</label>
                            </span>
                            <span>
                                <input class="callapp_filter_body_span_input" value="<?php echo date("Y-m-d 23:00:00");?>" type="text" id="end_date" style="width: 147px;">
                                <label for="end_date" style="padding: 4px 0;">-მდე</label>
                            </span>
                        </td>
                        <td rowspan="4" style="width: 100px;">
                            <table>
                            <tr>
                                    <td>
                					 <span >
                                     <label style="width: 100px; font-weight: bold !important">გამავალი ზარი</label>
                                      
                                    </span>
                                    </td>
                                    <td>
                					 <span >
                                     <label style="width: 115px; font-weight: bold !important">შემომავალი ზარი</label>
                                      
                                    </span>
                                    </td>
                                    <td>
                					 <span >
                                     <label style="width: 115px; font-weight: bold !important">შიდა ზარი</label>
                                      
                                    </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                					 <span style="margin-left: 15px">
                                     <label for="filter_1">ნაპასუხები</label>
                                         <div class="callapp_checkbox" style="margin-top:-21px;margin-left: -15px;">
                                         <input class="callapp_filter_body_span_input" id="filter_1" type="checkbox" value="4"/>
                                         <label for="filter_1" style="background: #4CAE50;"></label>
                                         </div>
                                    </span>
                                    </td>
                                    <td>
                					 <span style="margin-left: 15px">
                                     <label for="filter_3">ნაპასუხები</label>
                                         <div class="callapp_checkbox" style="margin-top:-21px;margin-left: -15px;">
                                         <input class="callapp_filter_body_span_input" id="filter_3" type="checkbox" value="1"/>
                                         <label for="filter_3" style="background: #4CAE50;"></label>
                                         </div>
                                    </span>
                                    </td>
                                    <td>
                					 <span style="margin-left: 15px">
                                     <label for="filter_5">ნაპასუხები</label>
                                         <div class="callapp_checkbox" style="margin-top:-21px;margin-left: -15px;">
                                         <input class="callapp_filter_body_span_input" id="filter_5" type="checkbox" value="6"/>
                                         <label for="filter_5" style="background: #4CAE50;"></label>
                                         </div>
                                    </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                					  <span style="margin-left: 15px">
                                          <label for="filter_2" style="">უპასუხო</label>
                                          <div class="callapp_checkbox" style="margin-top:-21px;margin-left: -15px;">
                                          <input class="callapp_filter_body_span_input" id="filter_2" type="checkbox" value="5"/>
                                          <label for="filter_2" style="background: #9E9E9E;"></label>
                                          </div>
                                      </span>
                                    </td>
                                    <td>
                					  <span style="margin-left: 15px">
                                          <label for="filter_4" style="">უპასუხო</label>
                                          <div class="callapp_checkbox" style="margin-top:-21px;margin-left: -15px;">
                                          <input class="callapp_filter_body_span_input" id="filter_4" type="checkbox" value="2"/>
                                          <label for="filter_4" style="background: #9E9E9E;"></label>
                                          </div>
                                      </span>
                                    </td>
                                    <td>
                					  <span style="margin-left: 15px">
                                          <label for="filter_6" style="">უპასუხო</label>
                                          <div class="callapp_checkbox" style="margin-top:-21px;margin-left: -15px;">
                                          <input class="callapp_filter_body_span_input" id="filter_6" type="checkbox" value="7"/>
                                          <label for="filter_6" style="background: #9E9E9E;"></label>
                                          </div>
                                      </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td rowspan="4" class="l_filter_button" style="margin-top: 20px">
                            <button id="loadtable" style="margin-left: -30px;">ფილტრი</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    <table id="table_right_menu">
        <tr>
        	<td><img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
        	</td>
            <td><img alt="log" src="media/images/icons/log.png" height="14" width="14">
            </td>
            <td id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14">
            </td>
        </tr>
    </table>
	<table class="display" id="table_index" style="width: 100%;">
        <thead>
            <tr id="datatable_header">
                <th>ID</th>
                <th style="width: 150px;">თარიღი</th>
                <th style="width: 120px;">წყარო</th>
                <th style="width: 120px;">ადრესატი</th>
                <th style="width: 25%;">ოპერატორი</th>
                <th style="width: 25%;">ზარის ტიპი</th>
                <th style="width: 25%;">სტატუსი</th>
                <th style="width: 25%;">საუბრის ხან.</th>  
                <th style="width: 25%;">მოსმენა</th>
            </tr>
        </thead>
        <thead>
           <tr class="search_header">
                <th class="colum_hidden">
            	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                </th>
                <th>
                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                </th>    
                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                </th>                         
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                </th>
                <th>
                    <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                </th> 
                <th>
                    <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                </th>           
            </tr>
        </thead>
    </table>
</div>
<div id="play_audio" title="ჩანაწერი">
    <audio controls>
      <source src="horse.ogg" type="audio/ogg">
    </audio>
</div>

</body>