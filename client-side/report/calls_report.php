<script type="text/javascript">
		var aJaxURL		      = "server-side/report/all_comunication.action.php";
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var tName		      = "example";
        
        var tName2		      = "info_table";
		var tName1		      = "report_1";

		$(document).on('click','.report_audio',function(){
            var operator = $(this).attr('data-audio');
            load_audio_modal(operator)
        });
        $(document).on("click", "#loadtable", function () {
            LoadTable();
        });
		$(document).ready(function () {
            var currentdate = new Date(); 
            GetDateTimes('start_date');
            GetDateTimes('end_date');

            $("#start_date").val(currentdate.getFullYear()+'-'+(currentdate.getMonth()+1)+'-'+currentdate.getDate()+' 00:00:00');
            $("#end_date").val(currentdate.getFullYear()+'-'+(currentdate.getMonth()+1)+'-'+currentdate.getDate()+' 23:59:59');
			GetDateTimes("search_start_my");
			GetDateTimes("search_end_my");
			$("#fillter_in").button();
			LoadTable();  
            $('#report_type_selector').chosen({ search_contains: true });
		});
		$(document).on('click','.report_answer',function(){
            var operator = $(this).attr('data-operator');
            load_answered_modal(operator)
        });

        $(document).on('click','.report_noanswer',function(){
            var operator = $(this).attr('data-operator');
            load_noanswer_modal(operator)
        });
        $(document).on('click','.report_missed',function(){
            var operator = $(this).attr('data-operator');
            load_missed_modal(operator)
        });

        $(document).on('click','.report_noanswer_missed',function(){
            var operator = $(this).attr('data-id');
            load_noanswer_missed_modal(operator)
        });
        $(document).on('click','.nonwork',function(){
            load_nonwork_modal();
        });
        $(document).on('click','.transfer',function(){
            var operator = $(this).attr('data-operator');
            load_transfer_modal(operator);
        });

        $(document).on('dblclick','#info_table3 tbody tr',function(){    
            var operator = $('.report_noanswer_missed',this).attr('data-id');
            load_noanswer_missed_modal(operator)
        });
        $(document).on('dblclick','#info_table4 tbody tr',function(){    
            var operator = $('.report_report_missed_det',this).attr('data-operator');
            var call_id = $('.report_report_missed_det',this).attr('data-call-id');
            load_missed_det_modal(operator,call_id)
        });


        function load_audio_modal(operator){
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_audio_calls&call_log="+operator,
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#audio").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("audio",operator);
                           }
                       }
                   }
               }
           });
        }
        function load_transfer_modal(operator){
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_transfer_calls",
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#transfer").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("transfer",operator);
                           }
                       }
                   }
               }
           });
        }
        function load_nonwork_modal(){
           
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_nonwork_calls",
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#nonwork").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("nonwork");
                           }
                       }
                   }
               }
           });
        }
        function load_missed_det_modal(operator,call_id){
           
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_missed_det_calls",
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#missed_det").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("missed_det",operator,call_id);
                           }
                       }
                   }
               }
           });
        }
        function load_noanswer_missed_modal(operator){
           
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_noanswer_missed_calls",
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#noanswer_missed").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("noanswer_missed",operator);
                           }
                       }
                   }
               }
           });
        }
        function load_missed_modal(operator){
           
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_missed_calls",
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#add_edit_form").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("add_edit_form",operator);
                           }
                       }
                   }
               }
           });
        }
        function load_noanswer_modal(operator){
           
           $.ajax({
               url: aJaxURL,
               type: "POST",
               data: "act=get_noanswer_calls",
               dataType: "json",
               success: function (data) {
                   if (typeof (data.error) != "undefined") {
                       if (data.error != "") {
                           alert(data.error);
                       } else {
                           $("#add_edit_form1").html(data.page);
                           if ($.isFunction(window.LoadDialog)) {
                               //execute it
                               LoadDialog("add_edit_form1",operator);
                           }
                       }
                   }
               }
           });
        }

        function load_answered_modal(operator){
           
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=get_answered_calls",
                dataType: "json",
                success: function (data) {
                    if (typeof (data.error) != "undefined") {
                        if (data.error != "") {
                            alert(data.error);
                        } else {
                            $("#add_edit_form3").html(data.page);
                            if ($.isFunction(window.LoadDialog)) {
                                //execute it
                                LoadDialog("add_edit_form3",operator);
                            }
                        }
                    }
                }
            });
        }

		function LoadTable(){
			var start	= $("#start_date").val();
	    	var end		= $("#end_date").val();
			         /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
			GetDataTable(tName, aJaxURL, "get_list",10, "type=call&start="+start+"&end="+end, 0, "", 0, "asc", '', change_colum_main);
			setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
		}

        function LoadDialog(fName,operator=0,call_id=0){
            //alert(fName);
            var start	= $("#start_date").val();
	    	var end		= $("#end_date").val();
			switch(fName){
				
				case "add_edit_form3":
                    
					var buttons = {
			        	"cancel": {
				            text: "დახურვა",
				            id: "cancel-dialog",
				            click: function () {
				            	$(this).dialog("close");
				            }
				        }
				    };
					GetDialog("add_edit_form3", 1000, "auto", buttons);
                    GetDataTable("info_table2", aJaxURL, "get_answered_list",20, "operator="+operator+"&start="+start+"&end="+end, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table2", "add_edit_form3", aJaxURL, "");			
					break;

                case "add_edit_form1":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("add_edit_form1", 1000, "auto", buttons);
                    GetDataTable("info_table3", aJaxURL, "get_no_answered_list",20, "operator="+operator+"&start="+start+"&end="+end, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table3", "add_edit_form1", aJaxURL, "act_type=no_answered");			
					break;    
                case "add_edit_form":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("add_edit_form", 1000, "auto", buttons);
                    GetDataTable("info_table4", aJaxURL, "get_missed",20, "operator="+operator+"&start="+start+"&end="+end, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "add_edit_form", aJaxURL, "");			
					break;
                case "noanswer_missed":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("noanswer_missed", 800, "auto", buttons);
                    GetDataTable("info_table5", aJaxURL, "noanswer_missed",20, "operator="+operator, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "noanswer_missed", aJaxURL, "");			
                    break;
                case "missed_det":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("missed_det", 800, "auto", buttons);
                    GetDataTable("info_table6", aJaxURL, "missed_det",20, "operator="+operator+"&call_id="+call_id, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "noanswer_missed", aJaxURL, "");			
                    break;
                case "nonwork":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("nonwork", 800, "auto", buttons);
                    GetDataTable("info_table7", aJaxURL, "nonwork",20, "start="+start+"&end="+end, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "noanswer_missed", aJaxURL, "");			
					break;
                case "transfer":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("transfer", 900, "auto", buttons);
                    GetDataTable("info_table8", aJaxURL, "transfer",20, "operator="+operator+"&start="+start+"&end="+end, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "noanswer_missed", aJaxURL, "");			
					break;
                case "audio":
                    
                    var buttons = {
                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    };
                    GetDialog("audio", 500, "auto", buttons);
                    //GetDataTable("info_table8", aJaxURL, "transfer",20, "operator="+operator, 0, "", 1, "asc", '', "<'F'lip>");
                    
                    //SetEvents("", "", "", "info_table4", "noanswer_missed", aJaxURL, "");			
					break;
			}
							
			
		}

       
        
        

	</script>
    
<div class="callapp_head" style="padding: 13px 5px 0 13px;">მომართვები<span class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="14" width="14">   განახლება</span><hr class="callapp_head_hr"></div>
<button id="callapp_show_filter_button" style="background: #fff;">ფილტრი <div id="shh" style="background: url('media/images/icons/lemons_filter.png') no-repeat;float: right; transform: rotate(0); "></div></button>
        <div class="callapp_filter_body" myvar="1">
            <div style="width: 100%;">
                <table>
                    <tr>
                        <td class="l_filter_date" style="width: 300px;">
                		<span style="margin: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 100px;">
                            <label for="start_date">-დან</label>
                        </span>
                        <span style="margin: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 100px;">
                            <label for="end_date" >-მდე</label>
                        </span>
                        </td>
                        <td rowspan="4">
                            <button id="loadtable">ფილტრი</button>
                        </td>
                    </tr>
                  
                </table>
            </div>
        </div>
    <div id="tabs" ">
    <div class="l_responsible_fix">
        <select id="report_type_selector">
            <option vlaue="1" selected>ზარები</option>
            <option vlaue="2">ჩატი</option>
            <option vlaue="3">Messenger</option>
            <option vlaue="4">Mail</option>
            <!--<option vlaue="5">Viber</option>-->
        </select> 
		<table id="table_right_menu" >
            <tr>
<!--            	<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">-->
<!--            		<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">-->
<!--            	</td>-->
<!--                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">-->
<!--                	<img alt="log" src="media/images/icons/log.png" height="14" width="14">-->
<!--                </td>-->
                <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4; background-color: #0C6A57; color: white" id="show_copy_prit_exel" myvar="0">
                	Excel/Print
                </td>
            </tr>
        </table>

        	
            <table class="display report_all_call" id="example" style="width: 100%;">
                <thead style="width: 10px;">
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 10%;">ოპერატორი</th>
                            <th style="width: 6%;">შემოსული ზარები</th>
                            <th style="width: 6%;">ნაპასუხები ზარები</th>
                            <th style="width: 4%;">%</th>
                            <th style="width: 5%;">უპასუხო ზარები</th>
                            <th style="width: 4%;">%</th>
                            <th style="width: 5%;">გამოტოვებული ზარები</th>
                            <th style="width: 3%;">%</th>
                            <th style="width: 5%;">გადართული ზარები</th>
                            <th style="width: 3%;">%</th>
                            <th style="width: 5%;">უპასუხო ზარები/ აბონენტმა გათიშა</th>
                            <th style="width: 3%;">%</th>
                            <th style="width: 5%;">უპასუხო ზარები/ ოპერატორმა გათიშა</th>
                            <th style="width: 3%;">%</th>
                            <th style="width: 5%;">უპასუხო ზარები/ სისტემამ გათიშა</th>
                            <th style="width: 3%;">%</th>
                            <th style="width: 5%;">უპასუხო ზარები/ პროგრამული ხარვეზი</th>
                            <th style="width: 3%;">%</th>
                            <th style="width: 5%;">არასამუშაო დროს შემოსული ზარი
                            <th style="width: 5%;">საუბრის საშ. ხ-ბა</th>
                            <th style="width: 5%;">დამუშავაბის საშ.ხანგძლივობა </th>
                            
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>                            
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_phone" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                            </th>
                        </tr>
                    </thead>
                    
                </table>
</div>
          
        </div>
         <!-- jQuery Dialog -->
	<div id="audio_dialog" title="ჩანაწერი">
	</div>
    <div id="add_edit_form"  class="form-dialog"  title="გამოტოვებული ზარები"></div>
    <div id="add_edit_form1"  class="form-dialog"  title="უპასუხო ზარები"></div>

    <div id="add_edit_form3"  class="form-dialog"  title="ნაპასუხები ზარები"></div>
    <div id="noanswer_missed"  class="form-dialog"  title="უპასუხო ზარები/გამოტოვებულები"></div>
    <div id="missed_det"  class="form-dialog"  title="გამოტოვებული ზარები"></div>
    <div id="nonwork"  class="form-dialog"  title="არასამუშაო დროს შემ. ზარები"></div>
    <div id="transfer"  class="form-dialog"  title="გადართული ზარები"></div>
    <div id="audio" class="form-dialog" title="აუდიო ჩანაწერი"></div>
    <script>
        
    </script>