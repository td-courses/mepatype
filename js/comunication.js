var imported = document.createElement("script");
imported.src = "js/jquery.timeago.js";
document.head.appendChild(imported);

var imported1 = document.createElement("script");
imported1.src = "js/MyWebSocket.js?v=1";
document.head.appendChild(imported1);

var import_css = document.createElement("link");
import_css.href = "dist/css/comunications.css";
document.head.appendChild(import_css);

// TestMachine

var file_name = "";
var rand_file = "";
var tab = 0;
var fillter = "";
var fillter_all = "";
var micr_val = 0;
var tbName = "tabs";
var Comunications_URL = "server-side/call/incomming.action.php";
var chat_detail_id = "";
var all_chats = 0;
var aJaxURL_task = "server-side/call/incomming/incomming_task.action.php";
var fName = "add-edit-form-comunications";
var aJaxURLsport = "server-side/call/action/action.action4.php";
var aJaxURL_crm = "server-side/call/auto_dialer_documents.action.php";
var aJaxURL_crm_file = "server-side/call/auto_dialer_file.action.php";
var seoyURL = "server-side/seoy/seoy.action.php";
var upJaxURL = "server-side/upload/file.action.php";
var wsdl_url = "includes/search_client_service.php";
var wsdl_url_log = "includes/search_client_log.php";
var asterisk_url = "server-side/asterisk/asterisk.action.php";
var aJaxURL_mail = "server-side/call/Email_sender.action.php";
var aJaxURL_getmail = "includes/phpmailer/smtp.php";
var aJaxURL_answer = "server-side/call/chat_answer.action.php";
var aJaxURL_sms = "includes/sendsms.php";

$(document).ready(function () {
  // runAjax();
});

$(document).on("click", ".user_logs", function () {
  GetDataTable(
    "user_log_table",
    Comunications_URL,
    "get_user_log",
    7,
    "incomming_call_id=" + $("#incomming_call_id").val(),
    0,
    "",
    1,
    "desc",
    "",
    "<'F'lip>"
  );

  $("#user_log_table_length").css("top", "0");
  $("#user_log_table_length select").css("height", "16px");
  $("#user_log_table_length select").css("width", "56px");
  setTimeout(function () {
    $(".ColVis, .dataTable_buttons").css("display", "none");
  }, 160);
});
$(document).on("click", "#download", function () {
  var download_file = $(this).val();
  var download_name = $("#download_name").val();
  SaveToDisk(download_file, download_name);
});

$(document).on("change", "#source_id, #my_site", function () {
  var source_id = $("#source_id").val();
  $("#call_content").off();
  $("#call_content1").off();
  $("#send_message").off();

  $("#call_content").autocomplete(
    "server-side/seoy/seoy_textarea.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 300,
      multiple: true,
      matchContains: true
    }
  );

  $("#call_content1").autocomplete(
    "server-side/seoy/seoy_textarea1.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 300,
      multiple: true,
      matchContains: true
    }
  );
if(source_id == 4)
{
  $("#send_message").autocomplete(
    "server-side/seoy/chat_textarea.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 360,
      multiple: true,
      matchContains: true
    }
  );
}
if(source_id == 7)
{
  $("#send_message").autocomplete(
    "server-side/seoy/chat_textarea2.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 360,
      multiple: true,
      matchContains: true
    }
  );
}
  
});

function SaveToDisk(fileURL, fileName) {
  $("#downloader").attr("href", fileURL);
  $("#downloader")[0].click();
  var iframe = document.createElement("iframe");
  iframe.src = Comunications_URL;
  iframe.style.display = "none";
  document.body.appendChild(iframe);
  return false;
}

$(document).on("click", "#delete", function () {
  var delete_id = $(this).val();
  var r = confirm("გსურთ წაშალოთ?");
  if (r == true) {
    $.ajax({
      url: Comunications_URL,
      data: {
        act: "delete_file",
        delete_id: delete_id,
        edit_id: $("#incomming_call_id").val()
      },
      success: function (data) {
        $("#file_div").html(data.page);
      }
    });
  }
});

$(document).on("click", "#dialog_emojis", function () {
  var height = $("#emoji_wrapper").height();
  if (height > 0) {
    $("#emoji_wrapper").css("height", "0");
  } else {
    $("#emoji_wrapper").css("height", "100%");
  }
});

$(document).on("click", "#emoji_wrapper span", function () {
  var emoji = $(this).attr("code");
  if (emoji != "0") {
    $("#send_message").html($("#send_message").html() + " " + emoji);
    $("#send_message").trigger("focus");
    $("#emoji_wrapper").css("height", "0");
  } else {
    $("#send_message").trigger("focus");
    $("#emoji_wrapper").css("height", "0");
  }
});
$(document).on("click", "#choose_button", function () {
  $("#choose_file1").click();
});

$(document).on("click", ".save-incomming-main-dialog", function () {
  //$('.save-incomming-main-dialog').attr('disabled','disabled');
  //save_inc(1);
});

$(document).on("click", ".save-incomming-main-dialog2", function () {
  //$('.save-incomming-main-dialog').attr('disabled','disabled');
  save_inc2(1);
});

$(document).on("click", ".delete_upload_file", function () {
  $(this)
    .parent()
    .parent()
    .html("");

  var isemty = true;
  var divs = document.getElementById("email_attachment_files").children;
  for (i = 0; i < divs.length; i++) {
    if (divs[i].innerHTML != "") {
      isemty = false;
    }
  }
  if (isemty) {
    $("#email_attachment_files").css("height", "0");
  }
});


$(document).on("click", ".chat_message_edit_button .open_edit", function(){

  id = $(this).attr("data-id");

  

    if( $('.chat_message_edit_block#'+id).attr("data-status") == 0){
      $('.chat_message_edit_block').attr("data-status", 0);
      $('.chat_message_edit_block').css("display", "none");

      $('.chat_message_edit_block#'+id).css("display", "");
      $('.chat_message_edit_block#'+id).attr("data-status", 1)

    } else{

      $('.chat_message_edit_block#'+id).css("display", "none");
      $('.chat_message_edit_block#'+id).attr("data-status", 0)

    }

    



})

$(document).on("click", "#close_edit", function(){

  $("#send_message").html('');
  $("#send_chat_program").attr("data-edit", 0)
  $("#send_chat_program").attr("data-id", "")

  var messageObj = $(".chat_message_id");
  // console.log(messageObj)
  messageObj.map((i, x) => {
      return x.style.backgroundColor = "";
    })

$(this).css("display", "none")


})

$(document).on("click", ".sms_delete", function () {
  var smsID = $(this).attr("data-id");
 
  $('.chat_message_edit_block').css("display", "none");
  $('.chat_message_edit_block').attr("data-status", 0)


  params = new Object();

  params.act = "chat_action";
  params.chatAction = "delete";
  params.id = smsID;

  $.ajax({
    type: "POST",
    url: Comunications_URL,
    data: params,
    success: function (res) {
      var messageObj = $(".chat_message_id");
      console.log(messageObj)
      messageObj.map((i, x) => {
        if(x.dataset.id == smsID) {
          return x.remove();
        }
      })

       console.log("done", res);
    }
  });
});
$(document).on('change', '#signatures', function(){
  var sign_id = $('#signatures').val();
  
});
$(document).on("click", ".sms_edit", function () {
  var smsID = $(this).attr("data-id");
  params = new Object();

  $('.chat_message_edit_block').css("display", "none");
  $('.chat_message_edit_block').attr("data-status", 0)
  $("#close_edit").css("display", "")
  
  params.act = "chat_action";
  params.chatAction = "edit";
  params.id = smsID;

  // console.log("edit", smsID)

  var messageObj = $(".chat_message_id");
  // console.log(messageObj)
  messageObj.map((i, x) => {
          if(x.dataset.id == smsID) {
            $("#send_chat_program").attr("data-edit", 1)
            $("#send_chat_program").attr("data-id", smsID)

            x.style.backgroundColor = "#81ca3145";

            if($("#send_message").length == 0){
             
              return $("#send_message").append(x.dataset.text)
          }else{
            console.log(x)
              $("#send_message").html('');
              return $("#send_message").append(x.dataset.text)
            }
          }else{
            x.style.backgroundColor = "";

          }
        })
});
$(document).on('click','.mail_forward',function(){
  var message_id = $(this).attr('data-id');
  
  if( $('.chat_message_edit_block#'+message_id).attr("data-status") == 0){
      $('.chat_message_edit_block').attr("data-status", 0);
      $('.chat_message_edit_block').css("display", "none");

      $('.chat_message_edit_block#'+message_id).css("display", "");
      $('.chat_message_edit_block#'+message_id).attr("data-status", 1)

  } else{

      $('.chat_message_edit_block#'+message_id).css("display", "none");
      $('.chat_message_edit_block#'+message_id).attr("data-status", 0)
  }
  params = new Object();
  
  params.act = "forward_dialog";
  params.message_id = message_id;
  
  $.ajax({
      type: "POST",
      url: aJaxURL_mail,
      data: params,
      success: function (res) {
          $("#mail_forwarding").html(res.page);
          var buttons = {
              "send": {
                  text: "გაგზავნა",
                  id: "send-forward-mail",
                  click: function () {
                      var receivers  = $("#receivers").val();
                      var forward_text = $("#forward_message").val();
                      var inc_id = $("#hidden_incomming_call_id").val();
                      $.ajax({
                          type: "POST",
                          url: 'server.php',
                          data: "act=mail_forward&message_id="+message_id+"&receivers="+receivers+"&forward_text="+forward_text+"&inc_id="+inc_id,
                          success: function (data) {
                              console.log(data);
                              if(data.status == 'OK'){
                                  alert('მეილი წარმატებით გაიგზავნა!!!');
                                  $("#mail_forwarding").dialog('close');
                              }
                              else if(data.status == 'ERR-MAIL'){
                                  alert('შეყვანილი ადრესატი არასწორი ფორმატისაა');
                              }
                              else{
                                  alert('დაფიქსირდა ხარვეზი!! სცადეთ ხელახლა');
                              }
                              
                              GetDataTable("table_mail",Comunications_URL,"get_list_mail",5,"incomming_id=" + $("#incomming_call_id").val(),0,"",0,"desc","","<'F'lip>");

                            
                          }
                      });
                  }
              },
              "cancel": {
                  text: "დახურვა",
                  id: "cancel",
                  click: function () {
                      $(this).dialog('close');
                  }
              }
      
          };
          GetDialog("mail_forwarding", 500, "auto",buttons);
      }
  });
  
  
});
$(document).on("click", "#send_chat_program", function () {
  $(".delete_upload_file").html(""); // delete x buttons \\
  var message_value = $("#send_message").html();
  var message_value_edit = $("#send_message").html();
	if( $("#chat_source").val() == 'mail' )
	{
      var message_value = CKEDITOR.instances['send_message2'].getData();
      
	}
  var links = "";
  var file_name = "";
  var file_html = "";
  var f_html = "";
  var message_value_check = message_value;
  var array_links = [];
  var fb_obj = new Object();
  if ($("#source_id").val() != 7) {
    $("#email_attachment_files .my_mail_file").each(function () {
      links += $(this).attr("link") + ";";
      array_links.push($(this).attr("link"));
      file_name += $(this).attr("file_name") + ";";
      f_html += "<hr>" + $(this).html() + "<br><hr>";
      file_html += '<a "' + links + '">' + $(this).html() + "</a><hr>";
    });
    message_value = f_html + message_value;
    //.replace(/\s/g, '');

    if($(this).attr("data-edit") == 0){

    if (message_value.length != 0 || links != 0) {
      sms_text =
        '<tr style="height: 17px;">' +
        '<td colspan="2" style=" word-break: break-word;text-align: right;">' +
        // '<span class="chat_message_edit_button"><span class="open_edit">...</span><div class="chat_message_edit_block" data-status="0" style="display: none"><div>რედაქტირება</div><div>წაშლა</div></div></span>'+
        '<div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE; ">' +
        '<p style="text-align: left; font-weight: bold; font-size: 12px;">' +
        $("#chat_user_id").attr("chat_user_name") +
        "</p>" +
        // +'<p style="padding-top: 7px; line-height: 20x; font-size: 14px; text-align: right;">'+file_html+'</p>'
        '<p style="font-family: BPG_baner; overflow-x: auto; text-align: left; padding-top: 7px; line-height: 20x; font-size: 12px;">' +
        message_value +
        "</p>" +
        '<div style="text-align: right; padding-top: 7px;">' +
        '<p style="color: #878787; width: 100%;">' +
        getDateTime() +
        "</p>" +
        "</div>" +
        "</div>" +
        "</td>" +
        "</tr>" +
        '<tr style="height:10px;"></tr>';
      $("#log").append(sms_text);
      if ($("#source_id").val() == "6") {
        fb_obj.arr = array_links;
        fb_obj.text = $("#send_message").html();
        fb_obj.text_fb = document.querySelector("#send_message").textContent;
      }
      console.log(fb_obj);
      
      sendSms(message_value, fb_obj, links, file_name);
      jQuery("time.timeago").timeago();
      document.getElementById("chat_scroll").scrollTop = 25000000000;

      if ($("#source_id").val() == 4) {
        param = new Object();
        param.act = "focusout";
        param.id = $("#chat_original_id").val();

        $.ajax({
          url: Comunications_URL,
          data: param,
          success: function (data) {}
        });
      }
    }
  }else{
      if (message_value.length != 0 || links != 0) {



        smsID = $(this).attr("data-id");
        params = new Object();

        params.act = "chat_action";
        params.chatAction = "edit";
        params.id = smsID;
        params.text = message_value_edit;
      
        $.ajax({
          type: "POST",
          url: Comunications_URL,
          data: params,
          success: function (res) {
            var messageObj = $(".chat_message_id");
            $("#send_chat_program").attr("data-id", "");
            $("#send_chat_program").attr("data-edit", 0);
            $("p#"+smsID+".operator_message")[0].innerText = message_value_edit
      $("p#"+smsID+".edit_date")[0].innerHTML = '<img src="media/images/icons/l_edit.png" style=" height: 14px;margin-bottom: 1px;margin-right: 4px;">'+getDateTime();

            var messageObj = $(".chat_message_id");
            messageObj.map((i, x) => {
              if(x.dataset.id == smsID) {
                x.dataset.text = message_value_edit;
              }
               x.style.backgroundColor = "";
            })

          $("#close_edit").css("display", "none")

          }
        });
      }
    }
  } else {
    $("#email_attachment_files .my_mail_file").each(function () {
      links += $(this).attr("link") + ";";
      file_name += $(this).attr("file_name") + ";";
      f_html += $(this).html() + "<hr>";
      file_html += '<a "' + links + '">' + $(this).html() + "</a><hr>";
    });
    var txt = $("#send_message").html();
    var txt_html = "<p>" + txt + "</p><hr>";
    message_value += " <br><br><br> " + file_name + " ";
    message_value_check = message_value; //.replace(/\s/g, '');

    
    if (message_value_check.length != 0) {
			console.log(file_name + " ****** " + message_value + " /// " + links);
      sms_text =
        '<tr style="height: 17px;">' +
        '<td colspan="2" style=" word-break: break-word;">' +
        '<div style="font-family: BPG_baner !important; float: right!important; background: rgba(254, 244, 0, 0.35) none repeat scroll 0 0; border-radius: 5px;padding: 7px 11px;color: #212121;  min-width: auto; max-width: 85%; overflow-wrap: break-word; border: 1px solid #DEDEDE;">' +
        '<p style="text-align: right; font-weight: bold; font-size: 12px;">' +
        $("#chat_user_id").attr("chat_user_name") +
        "</p>" +
        '<p style="padding-top: 7px; line-height: 20x; font-size: 12px; text-align: right;">' +
        f_html +
        "</p>" +
        '<p style="font-family: BPG_baner; padding-top: 7px; line-height: 20x; font-size: 12px; text-align: right;">' +
        message_value +
        "</p>" +
        '<div style="text-align: right; padding-top: 7px;">' +
        '<p style="color: #878787; width: 100%;">' +
        getDateTime() +
        "</p>" +
        "</div>" +
        "</div>" +
        "</td>" +
        "</tr>" +
        '<tr style="height:10px;"></tr>';
      $("#log").append(sms_text);
      sendSms(message_value, f_html + txt_html, links, file_name);
      jQuery("time.timeago").timeago();
      document.getElementById("chat_scroll").scrollTop = 25000000000;

      if ($("#source_id").val() == 4) {
        param = new Object();
        param.act = "focusout";
        param.id = $("#chat_original_id").val();

        $.ajax({
          url: Comunications_URL,
          data: param,
          success: function (data) {}
        });
      }
    }
  }
  $("#send_message").html("");
  $("#email_attachment_files").html("");
	$("#email_attachment_files").css("height", "0");
	if( $("#chat_source").val() == 'mail' )
	{
			CKEDITOR.instances['send_message2'].setData('');
	}
  return false;
});

$(document).on("change", "#choose_file1", function () {
  var file = $(this).val();
  var files = this.files[0];
  var name = uniqid();
  var path = "../../media/uploads/file/";

  var ext = file
    .split(".")
    .pop()
    .toLowerCase();
  if (
    $.inArray(ext, [
      "pdf",
      "pptx",
      "png",
      "xls",
      "xlsx",
      "jpg",
      "docx",
      "doc",
      "csv"
    ]) == -1
  ) {
    //echeck file type
    alert("This is not an allowed file type.");
    this.value = "";
  } else {
    file_name = files.name;
    rand_file = name + "." + ext;
    $.ajaxFileUpload({
      url: upJaxURL,
      secureuri: false,
      fileElementId: "choose_file1",
      dataType: "json",
      data: {
        act: "upload_file",
        path: path,
        file_name: name,
        type: ext
      },
      success: function (data, status) {
        if (typeof data.error != "undefined") {
          $.ajax({
            url: Comunications_URL,
            data: {
              act: "up_now",
              rand_file: data["file_name"],
              file_name: file_name,
              edit_id: $("#incomming_call_id").val()
            },
            success: function (data) {
              $("#file_div").html(data.page);
              $("#choose_file1").val("");
            }
          });
        }
      },
      error: function (data, status, e) {
        // alert(e);
        alert("It was E");
      }
    });
  }
});
/* $(document).on("change","#choose_file",function(e){
  var file_data = $('#choose_file').prop('files')[0];
  var fileName = e.target.files[0].name;
  var fileNameN = Math.ceil(Math.random()*99999999999);
  var fileSize = e.target.files[0].size;
  var fileExt = $(this).val().split('.').pop().toLowerCase();
  var form_data = new FormData();

  form_data.append('file', file_data);
  form_data.append('ext', fileExt);
  form_data.append('original', fileName);
  form_data.append('newName', fileNameN);
  var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp','pdf','xls','xlsx','csv','doc','docx','txt','zip','rar','7z','mp3','mp4','avi'];
  var imgExtensions = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    alert("დაუშვებელი ფორმატი!!!  გამოიყენეთ მხოლოდ: "+fileExtension.join(', '));
    $(".choose_file").val('');
  }
  else {

    if(fileSize>20971520) {
      alert("შეცდომა! ფაილის ზომა 20MB-ზე მეტია!!!");
      $(".choose_file").val('');
    }
    else{
      $.ajax({
        url: 'http://172.16.0.85/file_upload.php', // point to server-side PHP script
        dataType: 'json',  // what to expect back from the PHP script, if anything
        crossDomain: true,
        contentType: false,
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (data) {
          console.log("data: "+data);
        }
      });
    }

  }
}); */
$(document).on("change", "#choose_file", function () {
  var file		= $(this).val();
  var files 		= this.files[0];
  var name		= uniqid();
  var path		= "../../media/uploads/file/";


  var ext = file.split('.').pop().toLowerCase();
  if($.inArray(ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp','pdf','xls','xlsx','csv','doc','docx','txt','zip','rar','7z','mp3','mp4','avi']) == -1) { //echeck file type
      alert('This is not an allowed file type.');
      this.value = '';
  }else{
      file_name = files.name;
      rand_file = name + "." + ext;
      $.ajaxFileUpload({
          url: "http://172.16.0.85/file_upload.php",
          secureuri: false,
          fileElementId: "choose_file",
          dataType: 'json',
          data:{
              act: "upload_file",
              path: path,
              file_name: name,
              type: ext,
              new_name: rand_file
          },

          success: function (data, status){
              $("#choose_file").val('');
              //console.log(data);
              file = '';
                  if($("#source_id").val() == 4){
                      ge = 'http://172.16.0.85/upload/users/'+data['file_name'];
                      ge1 = `<a href="http://172.16.0.85/upload/users/${data['file_name']}" target="_blank"><div class="hero-image" style="background-image: url('http://172.16.0.85/upload/users/${data['file_name']}');"></div></a>`;
                      file = '';
                  }else if($("#source_id").val() == 6){
                      ge = 'https://crm.my.ge/fbmyge/file/'+data['file_name'];
                      ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/fbmyge/file/${data['file_name']}');" ></div>`;
                      file = 'fbmyge/file';
                  }else if($("#source_id").val() == 7){
                      ge = 'http://172.16.0.85/upload/users/'+data['file_name'];
                      ge1 = `<div class="hero-image" style="background-image: url('http://172.16.0.85/upload/users/${data['file_name']}');"></div>`;
                      file = 'mailmyge/file';
                  }else if($("#source_id").val() == 9){
                      ge = 'https://crm.my.ge/site_message_myge/file/'+data['file_name'];
                      ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/site_message_myge/file/${data['file_name']}');"></div>`;
                      file = 'site_message_myge/file';
                  }
              
              alert(ge+' '+ge1);
              ge_type=ge.slice(ge.lastIndexOf('.') - ge.length+1);
              console.log(ge_type);
              switch (ge_type) {
                  case 'png' :  ge1 = ge1; break;
                  case 'jpg' :  ge1 = ge1; break;
                  case 'xlsx':  ge1 = '<a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/xlsx_icon.png" alt="pic"></a>';break;
                  case 'xls' :  ge1 = '<a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/xls_icon.png" alt="pic"></a>';break;
                  case 'pptx':  ge1 = '<a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/pptx_icon.png" alt="pic"></a>';break;
                  case 'docx':  ge1 = '<a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/word_icon.png" alt="pic"></a>';break;
                  case 'pdf' :  ge1 = '<a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/pdf_icon.png" alt="pic"></a>';break;

                  default : ge1 = '<a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/file_icons.png" alt="pic"></a>';
              }
              console.log(ge1);


              
              // if($("#source_id").val() == 7){
              a_teg = '<div><div class="my_mail_file" file_name="'+file_name+'" style="float:left;" link="'+ge+'" >'+ge1+'<div class="file_text" style="display: contents;float:right;margin-top:15px;"><span style="width: max-content;"><a href="http://172.16.0.85/upload/users/'+data['file_name']+'" target="_blank">'+file_name+'</a></span></div>'+'<span class="delete_upload_file">x</span></div></div> '; // link="'+ge+'"
              // if($("#source_id").val() == 6){
              // 	var val = $("#send_message").html();
              // 	 $("#send_message").html(` ${val} ${ge} `);
              // }
              // else {
              attach_html = $("#email_attachment_files").html()+a_teg;
              $("#email_attachment_files").html(attach_html);
              $("#email_attachment_files").css("height","45px");
              $("#email_attachment_files").css("display","flex");
              $("#email_attachment_files").css("overflow","auto hidden");
              // }
              // }else{
              // 	sendSms($text_message,'', ge, file_name);
              // }
              jQuery("time.timeago").timeago();
              document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
          },
          error: function (data, status, e){
            $("#choose_file").val('');
            //console.log(data);
            file = '';
                if($("#source_id").val() == 4){
                    ge = 'http://172.16.0.85/upload/users/'+rand_file;
                    ge1 = `<a href="http://172.16.0.85/upload/users/${rand_file}" target="_blank"><div class="hero-image" style="background-image: url('http://172.16.0.85/upload/users/${rand_file}');"></div></a>`;
                    file = '';
                }else if($("#source_id").val() == 6){
                    ge = 'https://crm.my.ge/fbmyge/file/'+data['file_name'];
                    ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/fbmyge/file/${data['file_name']}');" ></div>`;
                    file = 'fbmyge/file';
                }else if($("#source_id").val() == 7){
                    ge = 'http://172.16.0.85/upload/users/'+rand_file;
                    ge1 = `<div class="hero-image" style="background-image: url('http://172.16.0.85/upload/users/${rand_file}');"></div>`;
                    file = 'mailmyge/file';
                }else if($("#source_id").val() == 9){
                    ge = 'https://crm.my.ge/site_message_myge/file/'+rand_file;
                    ge1 = `<div class="hero-image" style="background-image: url('https://crm.my.ge/site_message_myge/file/${data['file_name']}');"></div>`;
                    file = 'site_message_myge/file';
                }
            
            alert(ge+' '+ge1);
            ge_type=ge.slice(ge.lastIndexOf('.') - ge.length+1);
            console.log(ge_type);
            switch (ge_type) {
                case 'png' :  ge1 = ge1; break;
                case 'jpg' :  ge1 = ge1; break;
                case 'xlsx':  ge1 = '<a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/xlsx_icon.png" alt="pic"></a>';break;
                case 'xls' :  ge1 = '<a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/xls_icon.png" alt="pic"></a>';break;
                case 'pptx':  ge1 = '<a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/pptx_icon.png" alt="pic"></a>';break;
                case 'docx':  ge1 = '<a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/word_icon.png" alt="pic"></a>';break;
                case 'pdf' :  ge1 = '<a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/pdf_icon.png" alt="pic"></a>';break;

                default : ge1 = '<a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank"><img style="width: 19px; float: left;"  src="http://172.16.0.85//media/images/icons/file_icons.png" alt="pic"></a>';
            }
            console.log(ge1);


            
            // if($("#source_id").val() == 7){
            a_teg = '<div><div class="my_mail_file" file_name="'+file_name+'" style="float:left;" link="'+ge+'" >'+ge1+'<div class="file_text" style="display: contents;float:right;margin-top:15px;"><span style="width: max-content;"><a href="http://172.16.0.85/upload/users/'+rand_file+'" target="_blank">'+file_name+'</a></span></div>'+'<span class="delete_upload_file">x</span></div></div> '; // link="'+ge+'"
            // if($("#source_id").val() == 6){
            // 	var val = $("#send_message").html();
            // 	 $("#send_message").html(` ${val} ${ge} `);
            // }
            // else {
            attach_html = $("#email_attachment_files").html()+a_teg;
            $("#email_attachment_files").html(attach_html);
            $("#email_attachment_files").css("height","45px");
            $("#email_attachment_files").css("display","flex");
            $("#email_attachment_files").css("overflow","auto hidden");
            // }
            // }else{
            // 	sendSms($text_message,'', ge, file_name);
            // }
            jQuery("time.timeago").timeago();
            document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
          }
      });
  }
});

$(document).on("click", "#choose_button5", function () {
  $("#choose_file").click();
});

$(document).on("click", ".incomming_mail", function () {
  GetDataTable("table_mail",Comunications_URL,"get_list_mail",5,"incomming_id=" + $("#incomming_call_id").val(),0,"",0,"desc","","<'F'lip>");
  GetButtons("add_mail", "");
  setTimeout(function () {$(".ColVis, .dataTable_buttons").css("display", "none");}, 160);
  $("#table_mail_length").css("top", "0px");
});


$(document).on("click", "#get_answer", function () {
  // get answer page

  param = new Object();
  param.act = "get_add_page";
  comment_id = $(this).attr("class");
  $.ajax({
    url: aJaxURL_answer,
    data: param,
    success: function (data) {
      $("#add-edit-form-answer").html(data.page);

      var buttons = {
        save: {
          text: "შენახვა",
          id: "save-dialog-answer"
        },
        cancel: {
          text: "დახურვა",
          id: "cancel-dialog-answer",
          click: function () {
            $(this).dialog("close");
            $(this).html("");
            //$('#yesnoclose').dialog("close");
          }
        }
      };
      GetDialog("add-edit-form-answer", 357, "auto", buttons, "top");
      $("#add_comment").button();
    }
  });
});
$(document).on("click", "#save-dialog-answer", function () {
  // save answer
  if ($("#comment_info_sorce_id").val() == "") {
    param = new Object();
    param.act = "save_answer";
    param.comment = $("#in_answer").val();
    param.comment_id = $("#hidden_comment_id_" + comment_id + "").val();
    param.incomming_call_id = $("#incomming_call_id").val();
    if (param.comment != "") {
      $.ajax({
        url: aJaxURL_answer,
        data: param,
        success: function (data) {
          param2 = new Object();
          param2.act = "get_add_question";
          param2.incomming_call_id = $("#incomming_call_id").val();
          $.ajax({
            url: Comunications_URL,
            data: param2,
            success: function (data) {
              $("#add-edit-form-answer").dialog("close");
              $("#chat_question").html(data.page);
              $("#add_comment").button();
            }
          });
        }
      });
    } else {
      alert("შეავსეთ ველი");
    }
  } else {
    param2 = new Object();
    param2.act = "update_comment";
    param2.id = edit_comment_id;
    param2.comment = $("#in_answer").val();
    $.ajax({
      url: Comunications_URL,
      data: param2,
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {
            alert(data.error);
          }
          else{
            param2 = new Object();
            param2.act = "get_add_question";
            param2.incomming_call_id = $("#incomming_call_id").val();
            $.ajax({
              url: Comunications_URL,
              data: param2,
              success: function (data) {
                $("#add-edit-form-answer").dialog("close");
                $("#chat_question").html(data.page);
                $("#add_comment").button();
              }
            });
          }
        }
        
      }
    });
  }
});

$(document).on("click", "#delete_comment", function () {
  // save answer
  param = new Object();
  param.act = "delete_comment";
  param.comment_id = $(this).attr("my_id");
  $.ajax({
    url: Comunications_URL,
    data: param,
    success: function (data) {
      if (typeof data.error != "undefined") {
        if (data.error != "") {
          alert(data.error);
        }
        else{
          param2 = new Object();
          param2.act = "get_add_question";
          param2.incomming_call_id = $("#incomming_call_id").val();
          $.ajax({
            url: Comunications_URL,
            data: param2,
            success: function (data) {
              $("#chat_question").html(data.page);
              $("#add_comment").button();
            }
          });
        }
      }
      
    }
  });
});
$(document).on("click", "#edit_comment", function () {
  // save answer
  param = new Object();
  param.act = "get_edit_page";
  edit_comment_id = $(this).attr("my_id");
  param.id = $(this).attr("my_id");
  $.ajax({
    url: aJaxURL_answer,
    data: param,
    success: function (data) {
      $("#add-edit-form-answer").html(data.page);
      var buttons = {
        save: {
          text: "შენახვა",
          id: "save-dialog-answer"
        },
        cancel: {
          text: "დახურვა",
          id: "cancel-dialog-answer",
          click: function () {
            $(this).dialog("close");
            $(this).html("");
            //$('#yesnoclose').dialog("close");
          }
        }
      };
      GetDialog("add-edit-form-answer", 357, "auto", buttons, "top");
    }
  });
});
$(document).on("click","#add_new_addr", function(){
  $("#addr_to_add").toggleClass("showed");
  
});
$(document).on("click", "#send_email", function () {
  param = new Object();
  param.from_mail_id = $("#from_mail_id").val();
  param.source_id = $("#source_id").val();
  param.address = $("#mail_address").val();
  param.cc_address = $("#mail_address1").val();
  param.bcc_address = $("#mail_address2").val();
  param.subject = $("#mail_text").val();
  param.send_mail_id = $("#send_email_hidde").val();
  param.incomming_call_id = $("#incomming_call_id").val();
  param.hidden_increment = $("#hidden_increment").val();
  param.body = $("#input").val();
  var attachments = [];
  var counter = 0;

  $(".attachment_address").each(function () {
    attachments[counter] = $(this).val();
    counter++;
  });

  param.attachments = attachments.join(",");

  $.ajax({
    url: aJaxURL_getmail,
    data: param,
    success: function (data) {
      if (data.status == 2) {
        GetDataTable("table_mail",Comunications_URL,"get_list_mail",5,"incomming_id=" + $("#incomming_call_id").val(),0,"",0,"desc","","<'F'lip>");
        GetButtons("add_mail", "");
        alert("შეტყობინება წარმატებით გაიგზავნა!");
        $("#mail_text").val("");
        $("iframe")
          .contents()
          .find("body")
          .html("");
        $("#file_div_mail").html("");
        $("#add-edit-form-mail").dialog("close");
        $("#add-edit-form-mail").html("");
        // GetDataTable(
        //   "table_mail",
        //   Comunications_URL,
        //   "get_list_mail",
        //   5,
        //   "incomming_id=" + $("#incomming_call_id").val(),
        //   0,
        //   "",
        //   0,
        //   "desc",
        //   "",
        //   "<'F'lip>"
        // );
        setTimeout(function () {
          $(".ColVis, .dataTable_buttons").css("display", "none");
        }, 160);
        $("#table_mail_length").css("top", "0px");
      } else {
        // alert('შეტყობინება არ გაიგზავნა!');
      }
    }
  });
});
$(document).on("click", "#email_shablob", function () {
  param = new Object();
  param.act = "send_mail_shablon";
  param.mail_id = $("#from_mail_id").val();
  $.ajax({
    url: aJaxURL_mail,
    data: param,
    success: function (data) {
      $("#add-edit-form-mail-shablon").html(data.page);
      GetDataTable(
        "example_shablon_table",
        aJaxURL_mail,
        "get_list_shablon",
        5,
        "mail_id=" + $("#from_mail_id").val(),
        0,
        "",
        2,
        "desc",
        "",
        "<'F'lip>"
      );
      $("#example_shablon_table_length").css("top", "0px");
    }
  });
  var buttons = {
    cancel: {
      text: "დახურვა",
      id: "cancel-dialog",
      click: function () {
        $(this).dialog("close");
        $(this).html("");
      }
    }
  };
  GetDialog("add-edit-form-mail-shablon", 1200, "auto", buttons, "left top");
});

function pase_body(id, head) {
  $("#mail_text").val(head);
  $("iframe")
    .contents()
    .find("body")
    .html($("#" + id).html());
  $("#add-edit-form-mail-shablon").dialog("close");
}
$(document).on('change','#signature',function(){
	var sign_id = $("#signature").val();
	if(sign_id != '---')
	{
		$.ajax({
			url: aJaxURL_mail,
			data: "act=get_sign&sign_id="+sign_id,
			success: function (data) {
				var sign = data.sign;
				$("iframe").contents().find("body").append('<img src="media/uploads/file/'+sign[0].file+'"><p>'+sign[0].sign+'</p>');
			}
		});
	}
	
});
$(document).on("click", "#add_mail", function () {
  param = new Object();
  param.act = "send_mail";
  param.call_type = "inc";
  param.outgoing_id = $("#outgoing_id").val();
  $.ajax({
    url: aJaxURL_mail,
    data: param,
    success: function (data) {
      $("#add-edit-form-mail").html(data.page);
      $("#smsPhoneDir").button();
      $("#email_shablob,#choose_button_mail,#send_email").button();
      $("#input").cleditor();
      $("#signature").chosen();
    }
  });
  var buttons = {
    cancel: {
      text: "დახურვა",
      id: "cancel-dialog",
      click: function () {
        $(this).dialog("close");
        $(this).html("");
      }
    }
  };
  GetDialog("add-edit-form-mail", 640, "auto", buttons, "center top");
  setTimeout(() => {
    $("#from_mail_id").chosen({
      search_contains: true
    });
    
  }, 150);
  $("#smsPhoneDir").button();
});

$(document).on("dialogopen", "#add-edit-form-mail", function () {
  $("#email_shablob,#choose_button_mail,#send_email,#smsPhoneDir").button();
  $("#signature").chosen({
    search_contains: true
  });
  setTimeout(() => {
    //     tinymce.init({
    //         selector: '#input',
    //         width: "580px",
    //         height: "200px",
    //         cssclass: 'te',
    //         controlclass: 'tecontrol',
    //         dividerclass: 'tedivider',
    //         controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
    //             'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
    //             'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
    //             'font', 'size', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
    //         footer: true,
    //         fonts: ['Verdana', 'Arial', 'Georgia', 'Trebuchet MS'],
    //         xhtml: true,
    //         bodyid: 'editor',
    //         footerclass: 'tefooter',
    //         resize: {cssclass: 'resize'}
    //     });
    
  }, 100);
});

$(document).on("dialogclose", "#add-edit-form-mail", function (event) {
  tinymce.remove("#input");
});

$(document).on("click", "#sendNewSms", function () {
  // define send type
  const sendType = $(this).data("type");
  // define request data
  const data = {
    act: `send_sms_${sendType}`,
    smsphone: $("#sms_phone").val(),
    smsId: $D.choosedSmsId,
    smsText: $("#newSmsText").val(),
    incommingId: $("#hidden_incomming_call_id").val()
  };

  // check type of request malibu

  data.phone = $("#smsAddresseeByPhone").val();

  // send ajax request
  $.post(aJaxURL_sms, data, result => {
    if (result) {
      alert("SMS წარმატებით გაიგზავნა");
    } else {
      alert("SMS გაგზავნა ვერ განხორციელდა");
    }
    $("#add-edit-form-sms").dialog("close");
    GetDataTable("table_sms",Comunications_URL,"get_list_sms",5,"&incomming_id=" + $("#hidden_incomming_call_id").val(),0,"",2,"desc","","<'F'lip>");
    $("#table_sms_length").css("top", "0px");
    $("#table_sms_length select").css("height", "16px");
    $("#table_sms_length select").css("width", "56px");
  });
  $("#smsAddresseeByPhone").val("");
  $("#newSmsText").val("");
});

$(document).on("click", ".sms", function () {
  GetDataTable("table_sms",Comunications_URL,"get_list_sms",5, "&incomming_id=" + $("#hidden_incomming_call_id").val(),0,"",2,"desc","","<'F'lip>");
  $("#table_sms_length").css("top", "0px");
  $("#table_sms_length select").css("height", "16px");
  $("#table_sms_length select").css("width", "56px");
  setTimeout(function () {$(".ColVis, .dataTable_buttons").css("display", "none");}, 90);
});
function check_beneficiary(pn){
	$.ajax({
		url: Comunications_URL,
		type: "POST",
		data: "act=check_beneficiary&pn="+pn,
		dataType: "json",
		success: function (data) {
			var answer = data.response;
			if(answer == 1){
				$("#fermeri-1--152--6").prop('checked',true);
			}
			else if(answer == 0){
				$("#fermeri-2--152--6").prop('checked',true);
			}
		}
	  });
}
$(document).on("click", "#add_task_button", () => {
  $.ajax({
    url: aJaxURL_task,
    type: "POST",
    data: "act=get_add_page",
    dataType: "json",
    success: function (data) {
      if (typeof data.error != "undefined") {
        if (data.error != "") {
          alert(data.error);
        } else {
          $("#add-edit-form-task").html(data.page);
          var buttons = {
            save: {
              text: "შენახვა",
              id: "save-dialog-task"
            },
            cancel: {
              text: "დახურვა",
              id: "cancel-dialog-task",
              click: function () {
                $(this).dialog("close");
                $(this).html("");
              }
            }
          };
          GetDialog("add-edit-form-task", 800, "auto", buttons, "center top");
          $("#add-edit-form-task, .add-edit-form-task-class").css('overflow','visible');
          GetDateTimes("task_end_date");
          GetDateTimes("task_start_date");
          $("[data-select='chosen']").chosen({
            search_contains: true
          });
        }
      }
    }
  });
});
function mepa_number_formater(number){
  var phone = number;
  var last_2 = phone.slice(-2);
  var middle = phone.slice(-4,-2);
  var first = phone.slice(-6,-4);
  var triple = phone.slice(-9,-6);

  var formated_phone = triple+'-'+first+'-'+middle+'-'+last_2;

  return(formated_phone);
}
function run(number, ipaddres, ab_pin, check_save_chat,rr,calltype) {
  if (check_save_chat == 1) {
    save_inc(0);
  }

  param = new Object();
  param.act = "get_add_page";
  param.number = number;
  param.ipaddres = ipaddres;
  param.ab_pin = ab_pin;
  param.calltype = calltype;
  $.ajax({
    url: Comunications_URL,
    data: param,
    success: function (data) {
      if (typeof data.error != "undefined") {
        if (data.error != "") {
          // alert(data.error);
          alert("Abc");
        } else {
          $("#add-edit-form-comunications").html(data.page);
          
          
          LoadDialog_comunications();
          all_chats = 1;
          $("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0,#region,#municip,#temi,#village").chosen();
        
          $("#telefoni___183--124--1").val(mepa_number_formater(number));
          var inc_call_id = $("#hidden_incomming_call_id").val();
          $.ajax({
            url: Comunications_URL,
            data: "act=get_user&phone="+number+"&inc_id="+inc_call_id,
            success: function(data) {
              $("#saxeli___899--127--1").val(data.firstname);
              $("#gvari___921--128--1").val(data.lastname);
              
              $("#informaciis_wyaro--139--1").val(data.call_log_number);
              $('#komunikaciis_arxi--138--8').val(1);
              $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
              if(calltype == 'outgoing'){
                $(".ui-dialog-title").html('გამავალი ზარი');
              }
              if ($("#saxeli___899--127--1").val() == "" && $("#gvari___921--128--1").val() == "") 
                get_name_lastname();
              
            }
          });
          $.ajax({
                url: Comunications_URL,
                data: {
                  'act'		: 'get_fieldsets',
                  'setting_id': 4
                },
                success: function(data) {
                  $(data.chosen_keys).chosen({ search_contains: true });

                  if(data.datetime_keys != ''){
                    var datetimes = data.datetime_keys;

                    datetimes.forEach(function(item,index){
                      GetDateTimes(item);
                    });
                  }

                  if(data.date_keys != ''){
                    var date = data.date_keys;

                    date.forEach(function(item,index){
                      GetDate(item);
                    });
                  }
                  if(data.multilevel_keys != ''){
                    var date = data.multilevel_keys;
                    var main = '';
                    var secondary = '';
                    var third = '';

                    date.forEach(function(item,index){
                      if(index == 0){
                        main = item;
                      }
                      else if(index == 1){
                        secondary = item;
                      }
                      else{
                        third = item;
                      }
                    });
                  }

                  $(document).on("change", "#"+main, function () {
                    param = new Object();
                    param.act = "cat_2";
                    param.selector_id = $("#"+main).attr('id');
                    param.cat_id = $("#"+main).val();
                    $.ajax({
                      url: Comunications_URL,
                      data: param,
                      success: function (data) {
                        $("#"+secondary).html(data.page);
                        $("#"+secondary).trigger("chosen:updated");

                        if ($("#"+secondary+" option:selected").val() == 0) {
                          param = new Object();
                          param.act = "cat_3";
                          param.cat_id = $("#"+secondary).val();
                          $.ajax({
                            url: Comunications_URL,
                            data: param,
                            success: function (data) {
                              $("#"+third).html(data.page);
                              $("#"+third).trigger("chosen:updated");
                            }
                          });
                        }
                      }
                    });
                  });
                  $(document).on("change", "#"+secondary, function () {
                    param = new Object();
                    param.act = "cat_3";
                    param.selector_id = $("#"+main).attr('id');
                    param.cat_id = $("#"+secondary).val();
                    $.ajax({
                      url: Comunications_URL,
                      data: param,
                      success: function (data) {
                      $("#"+third).html(data.page);
                      $("#"+third).trigger("chosen:updated");
                      }
                    });
                  });

                  $(".dep").chosen();
                  $(".dep_project").chosen();
                  $(".dep_sub_project").chosen();
                  $(".dep_sub_type").chosen();
                }
          });
          // runAjax();
        }
      }
    }
  });
}
function save_inc2(ii) {
  if ($("#id").val() == "") {
    $(".save-incomming-main-dialog").attr("disabled", "disabled");
  }

  values = "";

  param = new Object();

  param.act = "save_incomming";
  param.hidde_incomming_call_id = $("#hidde_incomming_call_id").val();
  param.crm_id = $("#crm_id_hid").val();
  param.id = $("#incomming_call_id").val();
  param.phone = $("#phone").val();
  param.ipaddres = $("#ipaddres").val();
  param.call_date = $("#call_date").val();
  param.hidden_user = $("#hidden_user").val();
  param.processing_start_date = $("#processing_start_date").val();
  param.chat_id = $("#chat_original_id").val();
  param.source = $("#chat_source").val();
  param.incomming_cat_1 = $("#incomming_cat_1").val();
  param.incomming_cat_1_1 = $("#incomming_cat_1_1").val();
  param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();
  
  param.incomming_status_1 = $("#incomming_status_1").val();
  param.incomming_status_1_1 = $("#incomming_status_1_1").val();

  param.chat_language = $("#chat_language").val();
  param.chat_site     = $("#chat_site_id").val();
  param.inc_status_a    = $("#inc_status_a").val();
  param.company    = $("#company").val();
  param.manager    = $("#manager").val();
  param.manager_comment    = $("#manager_comment").val();
  param.space_type    = $("#space_type").val();

  param.phone1 = $("#phone1").val();
  param.fb_link = $("#fb_link").val();
  param.viber_address = $("#viber_address").val();
  param.vizit_datetime = $("#vizit_datetime").val();
  param.vizit_location = $("#vizit_location").val();
  param.client_comment = $("#client_comment").val();
  param.out_comment     = $("#out_comment").val();
  param.s_u_mail = $("#client_mail").val();
  param.s_u_name = $("#client_name").val();
  param.lid = $("input[id=check_lid]:checked").val();

  param.site_id = values;
  param.s_u_user_id = "";

  param.s_u_pid = "";
  param.client_sex = "";
  param.client_birth_year = "";
  param.s_u_status = $("#s_u_status").val();
  param.source_id = $("#source_id").val();
  param.inc_status = "";
  param.call_content = $("#call_content1").is(":visible") ?
    $("#call_content1").val() :
    $("#call_content").val();

  param.lid_comment = $("#lid_comment").val();

  param.rand_file = rand_file;
  param.file_name = file_name;
  param.hidden_inc = $("#hidden_inc").val();
  param.imchat = $("#imchat").val();
  param.ii = ii;

  var allowSave = true;
  var link = GetAjaxData(param);
  if ($("#phone").val() == "" && $("#source_id").val() == 1 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ ტელეფონის ნომერი!");
  }

  if ($("#incomming_cat_1").val() == 0 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ კატეგორია!");
  } else if ($("#incomming_cat_1_1").val() == 0 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ კატეგორია1!");
  } else if ($("#incomming_cat_1_1_1").val() == 0 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ კატეგორია2!");
  }

 

  if (allowSave) {
    $.ajax({
      url: "server-side/call/waiters.action.php",
      data: link,
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {
            alert(data.error);
            $(".save-incomming-main-dialog").attr("disabled", false);
          } else {
            $(".save-incomming-main-dialog").attr("disabled", false);
            var btn = {
              cancel: {
                text: "დახურვა",
                id: "cancel-dialog",
                click: function () {
                  $(this).dialog("close");
                  $(this).html("");
                }
              }
            };
            GetDialog("show_hint", "auto", "auto", btn);
            if ($("#add-edit-form-comunications input").length != 0) {
              LoadTable();
              setTimeout(function () {
                $("#show_hint").dialog("close");
              }, 700);
              $("#right_side, #side_menu, #gare_div").css(
                "pointer-events",
                "none"
              );
              $("#right_side, #side_menu, #gare_div").css("opacity", "0.5");
              $("#right_side, #side_menu, #gare_div").css("background", "#ccc");
              $("#send_message").attr("disabled", "disabled");
            }
            if (ii == 1 && $("#imchat").val() == 1) {}
          }
        }
      }
    });
  }
}
function save_inc(ii) {
  if ($("#id").val() == "") {
    $(".save-incomming-main-dialog").attr("disabled", "disabled");
  }

  values = "";

  param = new Object();

  param.act = "save_incomming";
  param.hidde_incomming_call_id = $("#hidde_incomming_call_id").val();
  param.id = $("#incomming_call_id").val();
  param.phone = $("#phone").val();
  param.ipaddres = $("#ipaddres").val();
  param.call_date = $("#call_date").val();
  param.hidden_user = $("#hidden_user").val();
  param.processing_start_date = $("#processing_start_date").val();
  param.chat_id = $("#chat_original_id").val();
  param.source = $("#chat_source").val();
  param.incomming_cat_1 = $("#incomming_cat_1").val();
  param.incomming_cat_1_1 = $("#incomming_cat_1_1").val();
  param.incomming_cat_1_1_1 = $("#incomming_cat_1_1_1").val();
  
  param.incomming_status_1 = $("#incomming_status_1").val();
  param.incomming_status_1_1 = $("#incomming_status_1_1").val();

  param.chat_language = $("#chat_language").val();
  param.chat_site     = $("#chat_site_id").val();
  param.inc_status_a    = $("#inc_status_a").val();
  param.company    = $("#company").val();
  param.space_type    = $("#space_type").val();

  param.phone1 = $("#phone1").val();
  param.fb_link = $("#fb_link").val();
  param.viber_address = $("#viber_address").val();
  param.vizit_datetime = $("#vizit_datetime").val();
  param.vizit_location = $("#vizit_location").val();
  param.client_comment = $("#client_comment").val();
  param.out_comment     = $("#out_comment").val();
  param.s_u_mail = $("#client_mail").val();
  param.s_u_name = $("#client_name").val();
  param.lid = $("input[id=check_lid]:checked").val();

  param.site_id = values;
  param.s_u_user_id = "";

  param.s_u_pid = "";
  param.client_sex = "";
  param.client_birth_year = "";
  param.s_u_status = $("#s_u_status").val();
  param.source_id = $("#source_id").val();
  param.inc_status = "";
  param.call_content = $("#call_content1").is(":visible") ?
    $("#call_content1").val() :
    $("#call_content").val();

  param.lid_comment = $("#lid_comment").val();

  param.rand_file = rand_file;
  param.file_name = file_name;
  param.hidden_inc = $("#hidden_inc").val();
  param.imchat = $("#imchat").val();
  param.ii = ii;

  var allowSave = true;
  var link = GetAjaxData(param);
  if ($("#phone").val() == "" && $("#source_id").val() == 1 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ ტელეფონის ნომერი!");
  }

  if ($("#incomming_cat_1").val() == 0 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ კატეგორია!");
  } else if ($("#incomming_cat_1_1").val() == 0 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ კატეგორია1!");
  } else if ($("#incomming_cat_1_1_1").val() == 0 && ii == 1) {
    allowSave = false;
    alert("შეავსეთ კატეგორია2!");
  }

 

  if (allowSave) {
    $.ajax({
      url: Comunications_URL,
      data: link,
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {
            alert(data.error);
            $(".save-incomming-main-dialog").attr("disabled", false);
          } else {
            localStorage.setItem('dialog_opened', 0);
            localStorage.setItem('already_clicked', 0);
            //	$('#add-edit-form').dialog("close");
            /*
                        $.ajax({
                            url: "server-side/call/getchats.action.php",
                            data: "act=getchats",
                            dataType: "json",
                            success: function(data) {
                                if(data.count != 0 && $D.sessionStatus == 1){
                                    $('#chat_count').css('background','#FFCC00');
                                    setTimeout(function(){
                                        $('#chat_count').css('background','#F44336');
                                        }, 400);
                                    var audio = new Audio('sms.mp3');
                                    if(micr_val == 0){
                                        audio.play();
                                        }
                                }else{
                                    $('#chat_count').css('background','none');
                                }

                                $("#incoming_chat_tab_chat").html(data.page.chat);
                                $("#incoming_chat_tab_site").html(data.page.site);
                                $("#incoming_chat_tab_mail").html(data.page.mail);
                                $("#incoming_chat_tab_fbm").html(data.page.fbm);

                                $('#chat_count').attr('first_id', data.first_id);
                                //alert($('#inc_chats tr').length)
                                if($('#inc_chats tr').length < 1){
                                    CloseDialog();
                                    LoadTable();
                                }else{
                                    if(ii!=0){
                                        $('.clickmetostart[color="#69D2E7"]').click();
                                    }
                                }
                            }
                        });
*/
            var btn = {
              cancel: {
                text: "დახურვა",
                id: "cancel-dialog",
                click: function () {
                  $(this).dialog("close");
                  $(this).html("");
                }
              }
            };
            GetDialog("show_hint", "auto", "auto", btn);
            if ($("#add-edit-form-comunications input").length != 0) {
              LoadTable();
              setTimeout(function () {
                $("#show_hint").dialog("close");
              }, 700);
              $("#right_side, #side_menu, #gare_div").css(
                "pointer-events",
                "none"
              );
              $("#right_side, #side_menu, #gare_div").css("opacity", "0.5");
              $("#right_side, #side_menu, #gare_div").css("background", "#ccc");
              $("#send_message").attr("disabled", "disabled");
            }
            if (ii == 1 && $("#imchat").val() == 1) {}
          }
        }
      }
    });
  }
}

$(document).on("click", "#add_sms_phone", function () {
	  
  // define request data
  const data = {
    act: "open_new_sms_dialog",
    type: $(this).data("type")
  };

  $.ajax({
    url: Comunications_URL,
    data,
    success: function (data) {
      var buttons = {
        cancel: {
          text: "დახურვა",
          id: "cancel-dialog",
          click: function () {
            $(this).dialog("close");
            $(this).html("");
          }
        }
      };

      $("#add-edit-form-sms").html(data.page);
      $("[data-button='jquery-ui-button']").css({"font-size": "7px"}).button();

      GetDialog("add-edit-form-sms", 561, "auto", buttons, "center top");
      $("#sms_phone").val($("#telefoni___183--124--1").val());
      // check new sms dialog type
      if (data.type === "phone") {
        // copy adressee phone number
        const phoneNum = $("#phone").val();

        // insert phone number into adressee receiver input
        $("#smsAddresseeByPhone").val(phoneNum);
      }

      // set send sms button type
      $("#sendNewSms").data("type", data.type);
    }
  });
});

$(document).on("click", ".hide_said_menu", function () {
  $("#right_side ff").hide();
  $(".add-edit-form-class").css("width", "560");
  //$('#add-edit-form').dialog({ position: 'top' });
  hide_right_side();
});
$(document).on(
  "click",
  ".add-edit-form-class .ui-dialog-titlebar-close",
  function () {
    $("#add-edit-form-comunications").dialog("close");
    $(this).dialog("close");
    if ($("#status-place > a > span").html() == 'რეგისტრაცია') {
    }
    else {
      $.ajax({
          url: "AsteriskManager/dndAction.php",
          data: {
              action: 'dndoff',
              activity_id: 1
          },
          success: function(res) {
            var statusBG = $('#status-place > a > x');
            var statusName = $('#status-place > a > span');
            $('#aStatus li[data-id=1]').css("background-color", "");
            statusBG.css("background-color", "#2dc100");
            statusName.text('აქტიური');
        
            $('.icon_status_color').css("background-color", "#2dc100");
          }
      });
      $.ajax({
          url: 'includes/menu.action.php',
          data: {
              act: "activity_status",
              status_id: 1
          },
          success: function(res) {
  
          }
      });
      
    }
    
    localStorage.setItem('dialog_opened', 0);
  }
);
$(document).on("click", "#save-dialog-task", function () {
  // $("#loading").show();
  values = $("#my_site")
    .chosen()
    .val();

  param = new Object();
  param.act = "save_task";
  param.task_type = $("#task_type").val();
  param.task_status_id = $("#task_status_id").val();
  param.task_status_id_2 = $("#task_status_id_2").val();
  param.task_branch = $("#task_branch").val();
  param.task_recipient = $("#task_recipient").val();
  param.task_create_date = $("#task_create_date").val();
  param.task_start_date = $("#task_start_date").val();
  param.task_end_date = $("#task_end_date").val();
  param.task_description = $("#task_description").val();
  param.task_note = $("#task_note").val();
  param.incomming_call_id = $("#incomming_call_id").val();
  param.site = values;

  if ($("#task_type").val() == 0) {
    alert("აირჩიე დავალების ტიპი!!!");
  } else if ($("#task_status_id").val() == 0) {
    alert("აირჩიე სტატუსი!!!");
  } else if (
    $("#task_status_id").val() == "77" &&
    $("#task_status_id_2").val() == "0"
  ) {
    alert("შეავსეთ ქვე-სტატუსი !");
  } else if ($("#task_recipient").val() == 0) {
    alert("აირჩიე დავალების მიმღები!!!");
  } else if ($("#task_branch").val() == 0) {
    alert("აირჩიე განყოფილება!!!");
  } else if ($("#task_start_date").val() == "") {
    alert("შეავსე პერიოდის დასაწყისი!!!");
  } else if ($("#task_end_date").val() == "") {
    alert("შეავსე პერიოდის დასასრული!!!");
  } else if (param.task_description == "") {
    alert("შეავსეთ კომენტარი!!!");
  } else {
    var link = GetAjaxData(param);
    $.ajax({
      url: aJaxURL_task,
      data: link,
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {
            alert(data.error);
          } else {
            GetDataTable(
              "task_table",
              aJaxURL_task,
              "get_list",
              6,
              "incomming_call_id=" + $("#incomming_call_id").val(),
              0,
              "",
              2,
              "desc",
              "",
              "<'F'lip>"
            );

            $("#task_table_length select").css("height", "16px");
            $("#task_table_length select").css("width", "56px");
            $("#add-edit-form-task").dialog("close");

            //if(data.send_check>=1){
            //     param1                   = new Object();
            //     param1.task_id           = data.task_id;
            //     param1.task_type         = data.task_type;
            //     param1.status_name       = data.status_name;
            //     param1.site              = values;
            //     param1.send_type         = 1;
            //     param1.incomming_call_id = $("#incomming_call_id").val();
            //     param1.task_recipient    = $('#task_recipient').val();
            //     var link1                = GetAjaxData(param1);
            //
            //     $.ajax({
            //         url: aJaxURL_getmail,
            //         data: link1,
            // 		success: function (data) {
            // 			$("#loading").hide();
            //         }
            //     });
            //}
          }
        }
      }
    });
  }
});

$(document).on("click", ".task", function () {
  GetDataTable(
    "task_table",
    aJaxURL_task,
    "get_list",
    6,
    "incomming_call_id=" + $("#incomming_call_id").val(),
    0,
    "",
    2,
    "desc",
    "",
    "<'F'lip>"
  );

  $("#task_table_length select").css("height", "16px");
  $("#task_table_length select").css("width", "56px");
  $("#add_task_button").button();
});

$(document).on("click", ".comunication_bar", function () {

  if(this.getAttribute('disabled')){
    return false;
  }
  if($(this).hasClass("active_comunication")) {
    $(this).removeClass("active_comunication");
    $("#flesh_panel").addClass("panel_hidden");
  }else {
    $(".comunication_bar").removeClass("active_comunication");
    $(this).addClass("active_comunication");
    $("#flesh_panel").removeClass("panel_hidden");
  }
});

// $(document).on("click", "#chat_count_open, #fb_chat_count_open, #mail_chat_count_open, #site_chat_count_open, #video_chat_count_open", function () {

//     if($(this).html() != ''){
//         var check_chat_status = 0;

//         if($(this).data('source')=='chat'){
//             if($("input[id='web_chat_checkbox']:checked").val()==1){
//                 check_chat_status = 1;
//             }
//         }

//         if($(this).data('source')=='site'){
//             if($("input[id='site_chat_checkbox']:checked").val()==1){
//                 check_chat_status = 1;
//             }
//         }

//         if($(this).data('source')=='fbm'){
//             if($("input[id='messanger_checkbox']:checked").val()==1){
//                 check_chat_status = 1;
//             }
//         }

//         if($(this).data('source')=='mail'){
//             if($("input[id='mail_chat_checkbox']:checked").val()==1){
//                 check_chat_status = 1;
//             }
//         }

//         if($(this).data('source')=='video'){
//             if($("input[id='video_call_checkbox']:checked").val()==1){
//                 check_chat_status = 1;
//             }
//         }

//         if($(this).data('source')=='video'){
//             check_chat_status = 1;
//             call_id = $(this).data('call_id');
//             chat_id = $(this).attr('chat_id');
//             if(chat_id > 0){
//                 newwindow=window.open('https://ccapi.crystalbet.com/videocall/index.html#'+call_id,'name','height=500,width=700');
//                 if (window.focus) {newwindow.focus()}
//             }
//         };

//         // end check_chat_status //

//         param 			= new Object();
//         param.act		= "get_edit_page";
//         param.number	= '';
//         param.imby	    = '1';
//         param.chat_id	= $(this).attr('chat_id');
//         param.source	= $(this).data('source');
//         if(param.chat_id != 0){
//             $.ajax({
//                 url: Comunications_URL,
//                 data: param,
//                 success: function(data) {
//                     if(typeof(data.error) != 'undefined'){
//                         if(data.error != ''){
//                         }else{
//                             $("#add-edit-form-comunications"
//                             ).html(data.page);
//                             // runAjax();
//                             $.ajax({
//                                 url: Comunications_URL,
//                                 data: {
//                                     'act'		:'get_all_chat',
//                                     'chat_id'	:$('#chat_original_id').val(),
//                                     'source'	:$("#chat_source").val()
//                                 },
//                                 success: function(data) {
//                                     if(typeof(data.error) != 'undefined'){
//                                         if(data.error != ''){

//                                         }else{

//                                             $('#log').html('');
//                                             $('#log').html(data.sms);
//                                             chat_detail_id=data.chat_detail_id;

//                                             $('#send_message').prop('disabled', false);
//                                             $('#chat_flag,#chat_browser,#chat_device').css('display','block');
//                                             $('#chat_flag').attr('src','media/images/icons/georgia.png');
//                                             $('#chat_browser').attr('src','media/images/icons/'+data.bro+'.png');
//                                             $('#chat_device').attr('src','media/images/icons/'+data.os+'.png');
//                                             $('#user_block').attr('ip',data.ip);
//                                             $('#user_block').attr('pin',data.pin);
//                                             $("#chat_name").val(data.name);
//                                             if(data.chat_user_id =='' ){
//                                                 $("#start_chat_wrap").show();
//                                             }
//                                             else{
//                                                 $("#start_chat_wrap").hide();
//                                             }
//                                             document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
//                                             jQuery("time.timeago").timeago();
//                                             $('#chat_count_open').css('background','none');
//                                             all_chats = 1;
//                                             LoadDialog_comunications();
//                                         }
//                                     }
//                                 }
//                             });
//                         }
//                     }
//                 }
//             });
//         }

//     }
// });
// var ajaxtimer;
// function runAjax() {
//     // try {clearTimeout(ajaxtimer);}
//     // catch(err) {}

//     $.ajax({
//         async: true,
//         dataType: "JSON",
//         url: 'AsteriskManager/liveState.php',
//         data: {
//             'sesvar'	:	'hideloggedoff',
//             'value'		:	true,
//             'stst'		:	1,
//             'checkState'		:	$("#check_state").val(),
//             'chat_id'			:	$('#chat_original_id').val(),
//             'chat_detail_id'	:	chat_detail_id,
//             'whoami' 			: 	$('#whoami').val()
//         },
//         success: function(data) {
//             //////////////////////////--------start flash paneli-------------////////////////////////////

//             let comunication = $(".active_comunication").attr('source');
//             let html;
//             let arr = data.users;
//             let queue_mail = data.queue.mail;
//             let queue_chat = data.queue.chat;
//             let queue_fb = data.queue.fb;

//             $("#fb_chat_count_open .fabtext").html(queue_fb.length);
//             $("#mail_chat_count_open .fabtext").html(queue_mail.length);
//             $("#chat_count_open .fabtext").html(queue_chat.length);
//             switch(comunication){
//                 case "phone":
//                 // console.log(data.users['1'])

//                     // let users = JSON.parse(data.users);
//                     html = `<div class="flesh_tabs_wrap flesh_tabs_head">
//                     <div class="flesh_tabs" >რიგი</div>
//                     <div class="flesh_tabs" >შიდა ნომერი</div>
//                     <div class="flesh_tabs" >თანამშრომელი</div>
//                     <div class="flesh_tabs" >სტატუსი</div>
//                     <div class="flesh_tabs" >ნომერი</div>
//                     <div class="flesh_tabs" >დრო</div>
//                     <div class="flesh_tabs" >პინი</div>

//                 </div>`;
//                 html += `<div class="flesh_tabs_wrap flesh_tabs_body">`;

//                 for(let i=0; i<arr.length; i++){
//                     let phone = arr[i].phone;
//                     let chat = arr[i].chat;
//                     let fb = arr[i].fb;
//                     let mail = arr[i].mail;
//                     let status = `
//                         <div class="flex">
//                             <div class="not_in_use"></div>
//                             <div class="chat_icon">${chat.length > 0 ? `<div class="flesh_counters">${isNull(chat.length)}</div>` : ``}</div>
//                             <div class="fb_icon">${fb.length > 0 ? `<div class="flesh_counters">${isNull(fb.length)}</div>` : ``}</div>
//                             <div class="mail_icon">${mail.length > 0 ? `<div class="flesh_counters">${isNull(mail.length)}</div>` : ``}</div>
//                         </div>
//                     `;
//                     html += ` <div class="flesh_tabs" >${isNull(phone.queue)}</div>
//                                 <div class="flesh_tabs" >${isNull(phone.ext)}</div>
//                                 <div class="flesh_tabs" >${isNull(arr[i].username)}</div>
//                                 <div class="flesh_tabs" >${isNull(status)}</div>
//                                 <div class="flesh_tabs" >${isNull(phone.number)}</div>
//                                 <div class="flesh_tabs" >${isNull(phone.time)}</div>
//                                 <div class="flesh_tabs" >${isNull(phone.pin)}</div>`;

//                 }
//                 html += `</div>`;

//                 break;
//                 case "fb":

//                     // let users = JSON.parse(data.users);
//                     html = `<div class="flesh_tabs_wrap_fb flesh_tabs_head">

//                     <div class="flesh_tabs" >ფოტო</div>
//                     <div class="flesh_tabs" >მომ.ავტორი</div>
//                     <div class="flesh_tabs" >სტატუსი</div>
//                     <div class="flesh_tabs" >ოპერატორი</div>
//                     <div class="flesh_tabs" >ხ-ბა</div>
//                     <div class="flesh_tabs" >სულ ხ-ბა</div>

//                 </div>`;
//                 html += `<div class="flesh_tabs_wrap_fb flesh_tabs_body">`;

//                 for(let i=0; i<arr.length; i++){
//                     let phone = arr[i].phone;
//                     let chat = arr[i].chat;
//                     let fb = arr[i].fb;
//                     let mail = arr[i].mail;
//                     let status = `
//                         <div class="flex">
//                             <div class="not_in_use"></div>
//                             <div class="chat_icon">${chat.length > 0 ? `<div class="flesh_counters">${isNull(chat.length)}</div>` : ``}</div>
//                             <div class="fb_icon">${fb.length > 0 ? `<div class="flesh_counters">${isNull(fb.length)}</div>` : ``}</div>
//                             <div class="mail_icon">${mail.length > 0 ? `<div class="flesh_counters">${isNull(mail.length)}</div>` : ``}</div>
//                         </div>
//                     `;
//                     $("#fb_chat_count_open .fabtext").html(fb.length);
//                     for(f=0;f<fb.length;f++){
//                         let img = `<div class="fb_img" style="background-image:url('${fb[f].picture}')"></div>`

//                         html += `
//                         <div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${img}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(fb[f].author)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(status)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(arr[i].username)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(fb[f].duration)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="fbm" chat_id = "${fb[f].id}"  >${isNull(fb[f].all_duration)}</div>`;
//                     }

//                 }
//                 html += `</div>`;

//                 break;
//                 case "chat":

//                     html = `<div class="flesh_tabs_wrap_chat flesh_tabs_head">
//                     <div class="flesh_tabs" >ქვეყანა</div>
//                     <div class="flesh_tabs" >ბრაუზერი</div>
//                     <div class="flesh_tabs" >მომ.ავტორი</div>
//                     <div class="flesh_tabs" >სტატუსი</div>
//                     <div class="flesh_tabs" >ოპერატორი</div>
//                     <div class="flesh_tabs" >ხ-ბა</div>
//                     <div class="flesh_tabs" >სულ ხ-ბა</div>

//                 </div>`;
//                 html += `<div class="flesh_tabs_wrap_chat flesh_tabs_body">`;

//                 for(let i=0; i<arr.length; i++){
//                     let phone = arr[i].phone;
//                     let chat = arr[i].chat;
//                     let fb = arr[i].fb;
//                     let mail = arr[i].mail;
//                     let status = `
//                         <div class="flex">
//                             <div class="not_in_use"></div>
//                             <div class="chat_icon">${chat.length > 0 ? `<div class="flesh_counters">${isNull(chat.length)}</div>` : ``}</div>
//                             <div class="fb_icon">${fb.length > 0 ? `<div class="flesh_counters">${isNull(fb.length)}</div>` : ``}</div>
//                             <div class="mail_icon">${mail.length > 0 ? `<div class="flesh_counters">${isNull(mail.length)}</div>` : ``}</div>
//                         </div>
//                     `;

//                     for(c=0;c<chat.length;c++){
//                         let country = `<div class="country_icon" style="background-image:url('media/images/icons/${chat[c].country}')"></div>`
//                         let browser = `<div class="flex">
//                                     <div class="browser_icon" style="background-image:url('media/images/icons/${chat[c].browser}.png')"></div>
//                                     <div class="browser_icon" style="background-image:url('media/images/icons/${chat[c].os}.png')"></div>

//                         </div>`;
//                         let duration = `<div class="flex" style="justify-content: space-between;">
//                                         <div>${chat[c].duration}</div>
//                                         <div class="duration_arrow" style="background-image:url('media/images/icons/${chat[c].duration_arrow == "1" ? "out_chat.png" : "inc_chat.png"}')"></div>
//                         </div>`;
//                         html += `
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${country}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${browser}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}">${isNull(chat[c].author)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(status)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(arr[i].username)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(duration)}</div>
//                         <div class="flesh_tabs clickmetostart" data-source="chat" chat_id = "${chat[c].id}" >${isNull(chat[c].all_duration)}</div>`;
//                     }

//                 }
//                 html += `</div>`;

//                 break;
//                 case "email":
//                 html = `<div class="flesh_tabs_wrap_fb flesh_tabs_head">

//                     <div class="flesh_tabs" >ფოტო</div>
//                     <div class="flesh_tabs" >მომ.ავტორი</div>
//                     <div class="flesh_tabs" >სტატუსი</div>
//                     <div class="flesh_tabs" >ოპერატორი</div>
//                     <div class="flesh_tabs" >ხ-ბა</div>
//                     <div class="flesh_tabs" >სულ ხ-ბა</div>

//                 </div>`;

//                 html += `
//                 <div class="flesh_tabs clickmetostart" ></div>
//                 <div class="flesh_tabs clickmetostart" ></div>
//                 <div class="flesh_tabs clickmetostart" ></div>
//                 <div class="flesh_tabs clickmetostart" ></div>
//                 <div class="flesh_tabs clickmetostart" ></div>
//                 <div class="flesh_tabs clickmetostart" ></div>
//                 <div class="flesh_tabs clickmetostart" ></div>`;

//                 break;
//             }

//             $("#flesh_panel").html(html);

//             ///////////////////////----------end flesh paneli----------------//////////////////////////

//             if($("#gare_div").css("display") == "none"){
//                 $(".add-edit-form-comunications-class").css("width","880");
//             }

//             if(all_chats ==2 && !is_dialog_open())	{
//                 all_chats = 0;
//             }
//             if($('#add-edit-form-comunications input').length != 0 && all_chats){
//                 $.ajax({
//                     async: true,
//                     url: "server-side/call/getchats.action.php",
//                     data: {
//                         'act':'getchats',
//                         'chat_id'		:$('#chat_original_id').val(),
//                         'chat_detail_id':1,
//                         'source'		:$("#chat_source").val()
//                     },
//                     dataType: "json",
//                     success: function(data) {
//                         $('#chat_flag,#chat_browser,#chat_device').css('display','none');
//                         if(data.chat_seen == 1){
//                             $("#span_seen").css('display', '');
//                             // document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
//                         }else{
//                             $("#span_seen").css('display', 'none');
//                         }
//                         if(data.chat_typing == 1){
//                             $("#chat_typing").show();
//                         }else{
//                             $("#chat_typing").hide();
//                         }
//                         if(data.chat_detail_id && data.chat_detail_id != chat_detail_id){
//                             $('#log').append(data.sms);
//                             chat_detail_id=data.chat_detail_id;
//                             // document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
//                         }
//                         if(data.count != 0 && $D.sessionStatus == 1){
//                             $('#chat_count').css('background','#FFCC00');
//                             setTimeout(function(){
//                                 $('#chat_count').css('background','#F44336');
//                             }, 400);
//                             var audio = new Audio('sms.mp3');

//                         }else{
//                             $('#chat_count').css('background','none');
//                         }

//                         if(data.chat_status == "12"){
//                             $("#chat_close_text").show();
//                         }
//                         else{
//                             $("#chat_close_text").hide();
//                         }

//                         $("#incoming_chat_tab_chat1").html(data.page.chat);
//                         $("#incoming_chat_tab_chat_queue").html(data.page.chat_queue);

//                         if(data.page.chat_color_status == 1){
//                             $(".incoming_chat_tab_chat").css('background-color','#ffeb3b');
//                             setTimeout(function(){
//                                 $(".incoming_chat_tab_chat").css('background-color','#E6F2F8');
//                             }, 400);
//                         }else{
//                             $(".incoming_chat_tab_chat").css('background-color','#E6F2F8');
//                         }

//                         $("#incoming_chat_tab_site1").html(data.page.site);
//                         $("#incoming_chat_tab_site_queue").html(data.page.site_queue);

//                         if(data.page.site_color_status == 1){
//                             $(".incoming_chat_tab_site").css('background-color','#ffeb3b');
//                             setTimeout(function(){
//                                 $(".incoming_chat_tab_site").css('background-color','#E6F2F8');
//                             }, 400);
//                         }else{
//                             $(".incoming_chat_tab_site").css('background-color','#E6F2F8');
//                         }

//                         $("#incoming_chat_tab_mail1").html(data.page.mail);
//                         $("#incoming_chat_tab_mail_queue").html(data.page.mail_queue);

//                         if(data.page.mail_color_status == 1){
//                             $(".incoming_chat_tab_mail").css('background-color','#ffeb3b');
//                             setTimeout(function(){
//                                 $(".incoming_chat_tab_mail").css('background-color','#E6F2F8');
//                             }, 400);
//                         }else{
//                             $(".incoming_chat_tab_mail").css('background-color','#E6F2F8');
//                         }

//                         $("#incoming_chat_tab_fbm1").html(data.page.fbm);
//                         $("#incoming_chat_tab_fbm_queue").html(data.page.fbm_queue);

//                         if(data.page.fbm_color_status == 1){
//                             $(".incoming_chat_tab_fbm").css('background-color','#ffeb3b');
//                             setTimeout(function(){
//                                 $(".incoming_chat_tab_fbm").css('background-color','#E6F2F8');
//                             }, 400);
//                         }else{
//                             $(".incoming_chat_tab_fbm").css('background-color','#E6F2F8');
//                         }

//                         $("#incoming_chat_video1").html(data.page.video);
//                         $("#incoming_chat_video_queue").html(data.page.video_queue);

//                         if(all_chats==1){
//                             // LoadDialog_comunications();
//                             // $.ajax({
//                             //     url: Comunications_URL,
//                             //     data: {
//                             //         'act'		:'get_all_chat',
//                             //         'chat_id'	:$('#chat_original_id').val(),
//                             //         'source'	:$("#chat_source").val()
//                             //     },
//                             //     success: function(data) {
//                             //         if(typeof(data.error) != 'undefined'){
//                             //             if(data.error != ''){

//                             //             }else{

//                             //                 $('#log').html('');
//                             //                 $('#log').html(data.sms);
//                             //                 chat_detail_id=data.chat_detail_id;
//                             //                 $("#chat_name").val(data.name);
//                             //                 if(data.chat_user_id =='' ){
//                             //                     $("#start_chat_wrap").show();
//                             //                 }
//                             //                 else{
//                             //                     $("#start_chat_wrap").hide();
//                             //                 }
//                             //                 document.getElementById( 'chat_scroll' ).scrollTop = 25000000000;
//                             //                 jQuery("time.timeago").timeago();
//                             //                 $('#chat_hist').css('border-bottom','none');
//                             //                 $('#chat_live').css('border-bottom','2px solid #333');
//                             //                 all_chats = 1;

//                             //             }
//                             //         }
//                             //     }
//                             // });
//                             // var source = $('#chat_source').val() || 'chat';
//                             // var index = $('#incoming_chat_tabs a[href="#incoming_chat_tab_'+source).parent().index();
//                             $("#incoming_chat_tabs").tabs();
//                         }

//                         all_chats = 2;

//                         // if($('#chat_user_id').attr('ext') !='0'){
//                         // 	$.ajax({
//                         // 		async: true,
//                         // 		url: "AsteriskManager/checkme.php",
//                         // 		data: "ext="+$('#chat_user_id').attr('ext'),
//                         // 		dataType: "json",
//                         // 		success: function(data) {
//                         // 			//console.log(data.phone);
//                         // 			$('#chat_call_live').attr('src','media/images/icons/'+data.icon);
//                         // 			if(data.phone != '' && data.phone != null){
//                         // 				if(data.ipaddres == '2004'){
//                         // 					iporphone = data.ipaddres;
//                         // 				}else{
//                         // 					iporphone = data.phone;
//                         // 				}
//                         // 				$('#chat_call_queue').css('display','table-row');
//                         // 				$('#chat_call_number').html(iporphone);
//                         // 				$('#chat_call_duration').html(data.duration);
//                         // 				$('#chat_call_number').attr('extention',$('#chat_user_id').attr('ext'));
//                         // 				$('#chat_call_number').attr('number',data.phone);
//                         // 				$('#chat_call_number').attr('ipaddres',data.ipaddres);
//                         // 			}else{
//                         // 				$('#chat_call_queue').css('display','none');
//                         // 				$('#chat_call_number').html('');
//                         // 				$('#chat_call_duration').html('');
//                         // 			}
//                         // 			ajaxtimer = setTimeout(runAjax, 1000);
//                         // 			$("#loading").hide();
//                         // 		}
//                         // 	});
//                         // }else{
//                         $('#main_call_chat').css('display','none');
//                         $("#loading").hide();
//                         ajaxtimer = setTimeout(runAjax, 1000);
//                         // }

//                     }
//                 });

//             }else{
//                 ajaxtimer = setTimeout(runAjax, 1000);
//                 $("#loading").hide();
//             }

//             //$("#loading").hide()

//         }

//     }).done(function(data) {

//     });

// }

function isNull(a) {
  return a == null || a == "null" ? "" : a;
}

$(document).on("keydown", "#send_message", function (e) {
  if (e.keyCode == 13) {
    e.preventDefault();

    $("#send_chat_program").click();
    $(this).html("");
  }
});

$(document).on("click", "#open_my_chat", function () {
  var check_chat_status = 0;

  if ($(this).data("source") == "chat") {
    if ($("input[id='web_chat_checkbox']:checked").val() == 1) {
      check_chat_status = 1;
    }
  }

  if ($(this).data("source") == "site") {
    if ($("input[id='site_chat_checkbox']:checked").val() == 1) {
      check_chat_status = 1;
    }
  }

  if ($(this).data("source") == "fbm") {
    if ($("input[id='messanger_checkbox']:checked").val() == 1) {
      check_chat_status = 1;
    }
  }

  if ($(this).data("source") == "mail") {
    if ($("input[id='mail_chat_checkbox']:checked").val() == 1) {
      check_chat_status = 1;
    }
  }

  if ($(this).data("source") == "video") {
    if ($("input[id='video_call_checkbox']:checked").val() == 1) {
      check_chat_status = 1;
    }
  }

  if (check_chat_status == 1) {
    param = new Object();
    param.act = "get_edit_page";
    param.number = "";
    param.imby = "1";
    param.chat_id = $(this).attr("chat_id");
    param.source = $(this).data("source");

    $.ajax({
      url: Comunications_URL,
      data: param,
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {
            alert("What is losting");
          } else {
            $("#add-edit-form-comunications").html(data.page);
            // runAjax();
            $.ajax({
              url: Comunications_URL,
              data: {
                act: "get_all_chat",
                chat_id: $("#chat_original_id").val(),
                source: $("#chat_source").val()
              },
              success: function (data) {
                if (typeof data.error != "undefined") {
                  if (data.error != "") {} else {
                    alert("xsxw");

                    $("#log").html("");
                    $("#log").html(data.sms);
                    chat_detail_id = data.chat_detail_id;
                    $("#send_message").prop("disabled", false);
                    $("#chat_flag,#chat_browser,#chat_device").css(
                      "display",
                      "block"
                    );
                    $("#chat_flag").attr(
                      "src",
                      "media/images/icons/georgia.png"
                    );
                    $("#chat_browser").attr(
                      "src",
                      "media/images/icons/" + data.bro + ".png"
                    );
                    $("#chat_device").attr(
                      "src",
                      "media/images/icons/" + data.os + ".png"
                    );
                    $("#chat_name").val(data.name);
                    if (data.chat_user_id == "") {
                      $("#start_chat_wrap").show();
                    } else {
                      $("#start_chat_wrap").hide();
                    }
                    $("#user_block").attr("ip", data.ip);
                    $("#user_block").attr("pin", data.pin);
                    jQuery("time.timeago").timeago();
                    $("#chatcontent,#gare_div").css("display", "block");
                    $(".add-edit-form-comunications-class").css(
                      "width",
                      "1270"
                    );
                    document.getElementById(
                      "chat_scroll"
                    ).scrollTop = 25000000000;
                    $(
                      '.clickmetostart[chat_id="' +
                      $("#chat_original_id").val() +
                      '"][data-source="' +
                      $("#chat_source").val() +
                      '"]'
                    ).click();
                    all_chats = 1;
                    var source = $("#chat_source").val() || "phone";
                    var index = $(
                        '#incoming_chat_tabs a[href="#incoming_chat_tab_' + source
                      )
                      .parent()
                      .index();
                    $("#incoming_chat_tabs").tabs("option", "active", index);
                    LoadDialog_comunications();
                  }
                }
              }
            });
          }
        }
      }
    });
  } else {
    alert("ჩატის გასახსნელად გააქტიურეთ შესაბამისი კომუნიკაციის არხი!");
  }
});
$(document).on("change", "#incomming_cat_1", function () {
  param = new Object();
  param.act = "cat_2";
  param.cat_id = $("#incomming_cat_1").val();
  $.ajax({
    url: Comunications_URL,
    data: param,
    success: function (data) {
      $("#incomming_cat_1_1").html(data.page);
      $("#incomming_cat_1_1").trigger("chosen:updated");
      if ($("#incomming_cat_1_1 option:selected").val() == 999) {
        param = new Object();
        param.act = "cat_3";
        param.cat_id = $("#incomming_cat_1_1").val();
        $.ajax({
          url: Comunications_URL,
          data: param,
          success: function (data) {
            $("#incomming_cat_1_1_1").html(data.page);
            $("#incomming_cat_1_1_1").trigger("chosen:updated");
          }
        });
      }
    }
  });
});

$(document).on("change", "#incomming_cat_1_1", function () {
  param = new Object();
  param.act = "cat_3";
  param.cat_id = $("#incomming_cat_1_1").val();
  $.ajax({
    url: Comunications_URL,
    data: param,
    success: function (data) {
      $("#incomming_cat_1_1_1").html(data.page);
      $("#incomming_cat_1_1_1").trigger("chosen:updated");
    }
  });
});


$(document).on("change", "#incomming_status_1", function () {
  param = new Object();
  param.act = "status_2";
  param.status_id = $("#incomming_status_1").val();
  $.ajax({
    url: Comunications_URL,
    data: param,
    success: function (data) {
      $("#incomming_status_1_1").html(data.page);
      $("#incomming_status_1_1").trigger("chosen:updated");
    
    }
  });
});

$(document).on("change", "#incomming2_status_1", function () {
  param = new Object();
  param.act = "status_2";
  param.status_id = $("#incomming2_status_1").val();
  $.ajax({
    url: 'server-side/call/outgoing_company.action.php',
    data: param,
    success: function (data) {
      $("#incomming2_status_1_1").html(data.page);
      $("#incomming2_status_1_1").trigger("chosen:updated");
    
    }
  });
});

// OUTSIDE CAMPAIGN INSERT DATA


$(document).on('change', '#CampanySelect', function(){
  var campanyID = this.value;

  var Grid = $('#select_campaign').parent().parent()
  var GridName = Grid[0].id;

  var selectedPhones = [];
  var entityGrid = $("#"+GridName).data("kendoGrid");
  var rows = entityGrid.select();
  rows.each(function(index, row) {
      var selectedItem = entityGrid.dataItem(row);
      if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
      if(selectedItem.phone != ''){
          selectedPhones.push(selectedItem.phone);
      }
  });

  $("#notReleasedCall span").text(0)  
  $("#notReleasedCall span").css("color", "#000")
  $("#ReleasedCall span").text(selectedPhones.length)  
  
  $.ajax({
      url: "server-side/call/outgoing_company.action.php",
      type: "POST",
      data: "act=get_campaign_insert&numbers="+selectedPhones+"&campaign_id="+campanyID,
      dataType: "json",
      success: function (data) {

        $("#relocateUserCall").html(data.page);

        let numberCount   = selectedPhones.length;
        let cn            = selectedPhones.length;
        let usersLine     = $("#relocateTable .user-line");
        let userCount     = usersLine.length;

        if(campanyID > 0){
          $('#ReleasedCall').css('display','contents');
          $('#notReleasedCall').css('display','contents');
          $('#labelRelease').css('display', 'none');
        }else{
          $('#ReleasedCall').css('display','none');
          $('#notReleasedCall').css('display','none');
          $('#labelRelease').css('display', 'contents');
        }

     
            for(x = 0; x < numberCount; x++){
                usersLine.map((i, tr) => {
                    let percent   =   tr.childNodes[1];
                    let count     =   tr.childNodes[2];
                
                    if(cn != 0){
                      sum = parseInt(count.childNodes[0].value) + 1; // Plus Existed Number
                      count.childNodes[0].dataset.count = sum // Set Data attribute
                      count.childNodes[0].value = sum; // Set InnerText
                      cn -= 1 // Minus Counted
                    }

                });
              }

            usersLine.map((i, tr) => {
                let percent   =   tr.childNodes[1];
                let count     =   tr.childNodes[2];
                
                let p = parseInt(count.childNodes[0].value) / numberCount * 100
                percent.innerText = p.toFixed(2)+'%';

            });

          }

  });
  
})

$(document).on('keyup', '.release-input', function(){
  

  if($("#select_campaign").length != 0 ){

    var Grid = $('#select_campaign').parent().parent()
    var GridName = Grid[0].id;

    var selectedPhones = [];
    var entityGrid = $("#"+GridName).data("kendoGrid");
    var rows = entityGrid.select();
    rows.each(function(index, row) {
        var selectedItem = entityGrid.dataItem(row);
        if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
        if(selectedItem.phone != ''){
            selectedPhones.push(selectedItem.phone);
        }
    });

  }else{
    var selectedPhones = [12312312,12321234234];
  }



  let numberCount   = selectedPhones.length;
  let usersLine     = $("#relocateTable .user-line");


  var callSum = $('#ReleasedCall')[0].dataset.oldcount;
  let sum = 0;
  
  $("#relocateTable .user-line").map((i, tr) => {
      let count     =   tr.childNodes[2];
      let num = count.childNodes[0].value;

      if(!isNaN(parseInt(num))){
        sum += parseInt(num)
      }
  })

  if(sum > callSum){
    this.value = this.dataset.count
    alert("რაოდენობა შეზღუდულია")
    $("#notReleasedCall span").text(0);  
    $("#ReleasedCall span").text(callSum)
  }else{

    usersLine.map((i, tr) => {
      let percent   =   tr.childNodes[1];
      let count     =   tr.childNodes[2];

      count.childNodes[0].dataset.count = count.childNodes[0].value;

    });

    var notReleasedValue = callSum - sum;
    var ReleasedValue = callSum - notReleasedValue;
    $("#notReleasedCall span").text(notReleasedValue);
    $("#ReleasedCall span").text(ReleasedValue);

    usersLine.map((i, tr) => {
      let percent   =   tr.childNodes[1];
      let count     =   tr.childNodes[2];

      let c;
        
        if(count.childNodes[0].value == ''){
          c = 0;
        }else{
          c = count.childNodes[0].value
        }

        let p = parseInt(c) / numberCount * 100
        percent.innerText = p.toFixed(2)+'%';

    });
  }
  
  if($("#notReleasedCall span").text() > 0){
    $("#notReleasedCall span").css("color", "#ff0000")
  }else{
    $("#notReleasedCall span").css("color", "#000")
  }



  console.log(callSum, sum);

});

$(document).on('keypress', '.release-input', function(e){
  let allow_char = [48,49,50,51,52,53,54,55,56,57];
  if(allow_char.indexOf(e.which) === -1 ){
    return false;
    
  }
});
$(document).on('click', '#select_campaign', function(){

  var Grid = $('#select_campaign').parent().parent()
  var GridName = Grid[0].id;


  var selectedPhones = [];
  var entityGrid = $("#"+GridName).data("kendoGrid");
  var rows = entityGrid.select();
  rows.each(function(index, row) {
      var selectedItem = entityGrid.dataItem(row);
      if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
      if(selectedItem.phone != ''){
          selectedPhones.push(selectedItem.phone);
      }
  });
  
  if(selectedPhones.length === 0){
      alert('ნომრების სია ცარიელია. მონიშნეთ ნომრები');
  }else{
      $.ajax({
      url: "server-side/call/outgoing_company.action.php",
      type: "POST",
      data: "act=get_campaign_select&numbers="+selectedPhones,
      dataType: "json",
      success: function (data) {
          $("#campaign-select-form").html(data.page);
          var buttons = {
            save: {
              text: "შენახვა",
              id: "save-Released"
            },
            cancel: {
                  text: "დახურვა",
                  id: "cancel-dialog",
                  click: function () {
                      $(this).dialog("close");
                  }
              }
          };

          $("#CampanySelect").chosen();
          $(".chosen-drop").css("position", "inhrit")
          
          
          GetDialog("campaign-select-form", 700, "auto", buttons);

          $("#campaign-select-form, .campaign-select-form-class").css("overflow", "visible")
      }
  });
}
     
  });    


$(document).on("click", "#save-Released", function(){

  var Grid = $('#select_campaign').parent().parent()
  var GridName = Grid[0].id;


  var selectedPhones = [];
  var entityGrid = $("#"+GridName).data("kendoGrid");
  var rows = entityGrid.select();
  rows.each(function(index, row) {
      var selectedItem = entityGrid.dataItem(row);
      if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
      if(selectedItem.phone != ''){
          selectedPhones.push(selectedItem.phone);
      }
  });

  var CampanySelect = $("#CampanySelect").val();
  let usersLine     = $("#relocateTable .user-line");
  var operators = [];

  usersLine.map((i, tr) => {
    let count     =   tr.childNodes[2];
    
    operators.push( { id: tr.dataset.id, count: count.childNodes[0].dataset.count });

  });
  
  var param = new Object();
  param.act       = "insert_into_base";
  param.phones    = selectedPhones;
  param.operators = operators;
  param.campany   = CampanySelect;


  if(parseInt($('#notReleasedCall span').text()) != 0){

    alert("დარჩენილია "+$('#notReleasedCall span').text()+" გასანაწილებელი ნომერი")

  }else{

    $.ajax({
      url: "server-side/call/outgoing_company.action.php",
      type: "POST",
      data: param,
      success: function () {
        $("#campaign-select-form").dialog('close')
      }

    });
  }

})

function get_name_lastname(){
  param = new Object();
  param.act = "get_name_lastname";
  param.phone = $("#telefoni___183--124--1").val();

  $.ajax({
      url: aJaxURL,
      data: param,
    success: function (res) {
          var data = res.name.split(' ');
          $("#saxeli___899--127--1").val(data[0]);
          $("#gvari___921--128--1").val(data[1]);
      }
  });
}

function save_custom_incomming(){
    //var baseID = $("#ast_source").val();
    var base;

    $.ajax({
        url: Comunications_URL,
        data: {
            'act'		: 'get_inputs_ids',
            'setting_id': 4,
            'base_id': base
        },
        success: function(data) {
            var dialogtt = $("#dialogType").val();
            var idd = $("#hidden_incomming_call_id").val();
            if(dialogtt == 'autodialer'){
                idd = '';
            }
            var query = 'act=save-custom&setting_id=4&processing_id='+idd;
            var query2 = 'act=save-custom-quize&baseID='+base;
            query += "&region="+$("#region").val()+"&municip="+$("#municip").val()+"&temi="+$("#temi").val()+"&village="+$("#village").val();
            if(data.input_ids != ''){
                var input_ids   = data.input_ids;
                var checkboxes  = data.checkboxes;
                var campaign_ids = data.campaign_ids;
                var campaignCheckboxes = data.campaignCheckboxes;
                var ready_to_save = 0;

                campaign_ids.forEach(function(item,index){
                    if($("#"+item+"[data-nec='1']").val() == '' || $("#"+item+"[data-nec='1']").val() == '0')
                    {
                        $("#"+item).css('border','1px solid red');
                        var transformed = item.replace(/--/g, "__");
                        $("#"+transformed+"_chosen .chosen-single").css('border','1px solid #e81f1f');
                        //ready_to_save++;
                        //alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                    }
                    else{
                        if($("#"+item).val() != ''){
                            query2 += "&"+item+'='+$("#"+item).val();
                        }
                    }
                    
                });
                campaignCheckboxes.forEach(function(item,index){
                    $("input[name='"+item+"']:checked").each(function() {
                        var post_data = this.id;
                        var check = post_data.split("--");

                        var check_id = check[0].split("-");

                        query2 += "&"+this.id+"="+check_id[1];
                    });
                });
                input_ids.forEach(function(item,index){
                    if($("#"+item+"[data-nec='1']").val() == '' || $("#"+item+"[data-nec='1']").val() == '0')
                    {
                        $("#"+item).css('border','1px solid red');
                        var transformed = item.replace(/--/g, "__");
                        $("#"+transformed+"_chosen .chosen-single").css('border','1px solid #e81f1f');
                        //ready_to_save++;
                        //alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                    }
                    else{
                        if($("#"+item).val() != ''){
                            query += "&"+item+'='+$("#"+item).val();
                        }
                    }
                    
                });

                checkboxes.forEach(function(item,index){
                    $("input[name='"+item+"']:checked").each(function() {
                        var post_data = this.id;
                        var check = post_data.split("--");

                        var check_id = check[0].split("-");

                        query += "&"+this.id+"="+check_id[1];
                    });
                });
                
                //default deps//
                var dep = $("#dep_0").val();
                var dep_project = $("#dep_project_0").val();
                var dep_sub_project = $("#dep_sub_project_0").val();
                var dep_sub_type = $("#dep_sub_type_0").val();

                if(dep_sub_type == '' || dep_sub_type == '0'){
                    //ready_to_save++;
                    //alert("შეავსეთ მომართვის ტიპი");
                }

                query += "&dep_0="+dep;
                query += "&dep_project_0="+dep_project;
                query += "&dep_sub_project_0="+dep_sub_project;
                query += "&dep_sub_type_0="+dep_sub_type;
                
                ////////////////

                let dep_array = [];
                let dep_project_array = [];
                let dep_sub_project_array = [];
                let dep_sub_type_array = [];

                $(".dep").each(function(item){
                    var rand_number = $(this).attr('data-id');
                    dep_array.push("&dep_"+rand_number+"="+$(this).val());
                });
                $(".dep_project").each(function(item){
                    var rand_number = $(this).attr('data-id');
                    dep_project_array.push("&dep_project_"+rand_number+"="+$(this).val());
                });
                $(".dep_sub_project").each(function(item){
                    var rand_number = $(this).attr('data-id');
                    dep_sub_project_array.push("&dep_sub_project_"+rand_number+"="+$(this).val());
                });
                $(".dep_sub_type").each(function(item){
                    var rand_number = $(this).attr('data-id');
                    var vall = $(this).val();
                    if(vall == '' || vall == '0'){
                        //ready_to_save++;
                        //alert("შეავსეთ მომართვის ტიპი");
                        return false;
                    }
                    dep_sub_type_array.push("&dep_sub_type_"+rand_number+"="+$(this).val());
                });
               
                /* console.log(dep_array);
                console.log(dep_project_array);
                console.log(dep_sub_project_array);
                console.log(dep_sub_type_array); */

                dep_array.forEach(function(name, index){
                    query +=dep_array[index] + dep_project_array[index] + dep_sub_project_array[index] + dep_sub_type_array[index];
                });
                query += "&dialogtype="+$("#dialogType").val();
                if(ready_to_save == 0){
                    $.ajax({
                        url: CustomSaveURL,
                        type: "POST",
                        data: query2,
                        dataType: "json",
                        success: function (data) {
                        }
                    });
                    $.ajax({
                        url: CustomSaveURL,
                        type: "POST",
                        data: query,
                        dataType: "json",
                        success: function (data) {
                           // alert('ინფორმაცია შენახულია');
                            //$("#kendo_incomming_table").data("kendoGrid").dataSource.read();
                            var source_name = $("#chat_source").val();
                            /* if(source_name == 'chat'){
                                $.ajax({
                                    url: Comunications_URL,
                                    data: "act=chat_end&chat_source=" +$("#chat_source").val()+ "+&chat_id=" + $("#chat_original_id").val(),
                                    success: function (data) {
                                    if (typeof data.error != "undefined") {
                                        if (data.error != "") {
                                        // alert(data.error);
                                        //alert("Caucasus Jixv");
                                        } else {
                                        $("#chat_close_dialog").dialog("close");
                                        }
                                    }
                                    }
                                });
                            } */
                            
                        }
                    });
                }
                //console.log(query);
                console.log(query2);
            }
        }
    });

}
function openInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}
$(document).on ("click","#check-all-group", function () {
  $(".callapp_checkbox input[class='check1']").prop("checked", $("#check-all-group").is(":checked"));
});
$(document).on("click", ".clickmetostart", function () {
  $("#loading").show();

  save_custom_incomming();
  //$(".save-incomming-main-dialog").trigger('click');

  $("#chat_original_id").val($(this).attr("chat_id"));
  var color = $(this).attr("color");
  param = new Object();
  param.act = "get_edit_page";
  param.number = "";
  param.imby = "1";
  param.chat_id = $(this).attr("chat_id");
  param.source = $(this).data("source");
  var source = param.source;
  if(source == 'video'){
    var peer_id = $(this).attr('title');
    if(peer_id == null){
      peer_id = $(this).html();
    }

    var offerer_from_ref = '';

    $.ajax({
      url: Comunications_URL,
      data: {
        act: "get_peer_info",
        peer_id: peer_id
      },
      success: function(data){
        offerer_from_ref = data.offerer_from_ref
      }
    })

    var open_video_window = $(this).attr('data-open-video');
    if(open_video_window == 0){
      window.open('http://video.mepa.gov.ge/#'+peer_id, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }
    
  }
  
  $.ajax({
    async: true,
    url: Comunications_URL,
    data: param,
    success: function (data) {
      if (typeof data.error != "undefined") {
        if (data.error != "") {
          // alert(data.error);
          alert("Check original id");
        }
        else if(data.access == 'NO'){
          alert('ჩატი აყვანილია სხვა პერატორის მიერ');
        } else {
          $("#add-edit-form-comunications").html(data.page);
          if(source == 'chat'){
            $('#komunikaciis_arxi--138--8').val(4);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
            $("#start_chat").click();
          }
          else if(source == 'viber'){
            $('#komunikaciis_arxi--138--8').val(14);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
          }
          else if(source == 'mail'){
            $('#komunikaciis_arxi--138--8').val(7);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
            $("#new_addresses").css('display','block');
            $("#choose_docs").css('right','68px');
            $("#signatures").css('display','block');
          }
          else if(source == 'skype'){
            $('#komunikaciis_arxi--138--8').val(15);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
          }
          else if(source == 'video'){
            $('#komunikaciis_arxi--138--8').val(16);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");

            $("#informaciis_wyaro--139--1").val(offerer_from_ref)

          }
          else if(source == 'whatsapp'){
            $('#komunikaciis_arxi--138--8').val(13);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
          }
          else if(source == 'fbm'){
            $('#komunikaciis_arxi--138--8').val(6);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
          }
          else if(source == 'instagram'){
            $('#komunikaciis_arxi--138--8').val(11);
            $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
          }
          //$("#loading").show();
          LoadDialog_comunications();
          $.ajax({
            url: Comunications_URL,
            data: {
                'act'		: 'get_fieldsets',
                'setting_id': 4
            },
            success: function(data) {
                $(data.chosen_keys).chosen({ search_contains: true });

                if(data.datetime_keys != ''){
                    var datetimes = data.datetime_keys;

                    datetimes.forEach(function(item,index){
                        GetDateTimes(item);
                    });
                }

                if(data.date_keys != ''){
                    var date = data.date_keys;

                    date.forEach(function(item,index){
                        GetDate(item);
                    });
                }
                if(data.multilevel_keys != ''){
                    var date = data.multilevel_keys;
                    var main = '';
                    var secondary = '';
                    var third = '';

                    date.forEach(function(item,index){
                        if(index == 0){
                            main = item;
                        }
                        else if(index == 1){
                            secondary = item;
                        }
                        else{
                            third = item;
                        }
                    });
                }

                $(document).on("change", "#"+main, function () {
                    param = new Object();
                    param.act = "cat_2";
                    param.selector_id = $("#"+main).attr('id');
                    param.cat_id = $("#"+main).val();
                    $.ajax({
                        url: Comunications_URL,
                        data: param,
                        success: function (data) {
                            $("#"+secondary).html(data.page);
                            $("#"+secondary).trigger("chosen:updated");

                            if ($("#"+secondary+" option:selected").val() == 0) {
                                param = new Object();
                                param.act = "cat_3";
                                param.cat_id = $("#"+secondary).val();
                                $.ajax({
                                    url: Comunications_URL,
                                    data: param,
                                    success: function (data) {
                                        $("#"+third).html(data.page);
                                        $("#"+third).trigger("chosen:updated");
                                    }
                                });
                            }
                        }
                    });
                });
                $(document).on("change", "#"+secondary, function () {
                    param = new Object();
                    param.act = "cat_3";
                    param.selector_id = $("#"+main).attr('id');
                    param.cat_id = $("#"+secondary).val();
                    $.ajax({
                        url: Comunications_URL,
                        data: param,
                        success: function (data) {
                        $("#"+third).html(data.page);
                        $("#"+third).trigger("chosen:updated");
                        }
                    });
                });

                $(".dep").chosen();
                $(".dep_project").chosen();
                $(".dep_sub_project").chosen();
                $(".dep_sub_type").chosen();
                ///BLOCKING CONTRACT FIELDSET BEGIN
                $(".dialog-tab-20 input").each(function(index){  
                    $(this).prop('disabled',true);
                });
                $("#servisi___259--146--8").prop('disabled',true).trigger("chosen:updated");

                ///BLOCKING CONTRACT FIELDSET END

            }
        });

        $("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0,#region,#municip,#temi,#village").chosen();
          var hidden = '&hidden='+$('#hidde_incomming_call_id').val();
                /* LoadKendoTable__Info_Projects(hidden);

                setTimeout(function(){
                    var grid = $("#kendo_project_info_table").data("kendoGrid");
                    grid.hideColumn(0); 

                    $("#kendo_project_info_table").kendoTooltip({  
                        show: function(e){  
                            if(this.content.text().length > 30){  
                            this.content.parent().css("visibility", "visible");  
                            }  
                        },  
                        hide:function(e){  
                            this.content.parent().css("visibility", "hidden");  
                        },  
                        filter: "td", 
                        position: "right",  
                        content: function(e){  
                            // var dataItem = $("#kendo_project_info_table").data("kendoGrid").dataItem(e.target.closest("tr"));  
                            // console.log("bottom ", dataItem.info)
                            var content = e.target[0].innerText;  
                            return content;  
                        }  
                        }).data("kendoTooltip");  
                }, 3000) */
          if(source=='mail'){
              $("#send_message").css("display","none");
              $("#send_message").replaceWith('<textarea contenteditable="true" placeholder="შეიყვანეთ ტექსტი" id="send_message2" style="padding: 0 8px; border:0px;width:85%;height:120px;resize:none;overflow-y: auto;"></textarea>');
              $("#dialog_emojis").css("display","none");
              $("#choose_button5").css("right","41px");
              CKEDITOR.replace( 'send_message2' );
          }
          var kendo = new kendoUI();
          kendo.kendoMultiSelector('mail_addresats','server-side/call/incomming.action.php', 'get_mail_addresats', "TO");
          kendo.kendoMultiSelector('mail_cc','server-side/call/incomming.action.php', 'get_mail_addresats', "CC");
          kendo.kendoMultiSelector('mail_bcc','server-side/call/incomming.action.php', 'get_mail_addresats', "BCC");
          $.ajax({
            async: true,
            url: Comunications_URL,
            data: {
              act: "get_all_chat",
              chat_id: $("#chat_original_id").val(),
              source: $("#chat_source").val()
            },
            success: function (data) {
              if (typeof data.error != "undefined") {
                if (data.error != "") {} else {
                  $("#user_block").attr("ip", data.ip);
                  $("#user_block").attr("pin", data.pin);
                  $("#log").html("");
                  $("#log").html(data.sms);
                  chat_detail_id = data.chat_detail_id;
                  $("#chat_name").val(data.name);
                  if (data.chat_user_id == "") {
                    $("#start_chat_wrap").show();
                  } else {
                    $("#start_chat_wrap").hide();
                  }
                  var source = $("#chat_source").val();
                  if(source != 'video'){
                    $("#chatcontent,#gare_div").css("display", "block");
                    $(".add-edit-form-comunications-class").css("width", "1270");
                    $("#chatistema").css("width", "210px");
                    $(".add-edit-form-class").css("width", "1270px");
                    
                  }
                  else{
                    $("#telefoni___183--124--1").val(peer_id);
                  }
                  
                  

                  $(".add-edit-form-class .ui-widget-header").css(
                    "background",
                    color
                  );
                  jQuery("time.timeago").timeago();
                  $("#send_message").prop("disabled", false);
                  $("#blocklogo").css("display", "table");
                  document.getElementById(
                    "chat_scroll"
                  ).scrollTop = 25000000000;
                  all_chats = 1;
                  var source = $("#chat_source").val() || "phone";
                  var index = $(
                      '#incoming_chat_tabs a[href="#incoming_chat_tab_' + source
                    )
                    .parent()
                    .index();
                  $("#incoming_chat_tabs").tabs("option", "active", index);
                }
              }
            }
          });
          // runAjax();
        }
      }
    }
  });
});

$(document).on("click", "#chat_live", function () {
  $.ajax({
    url: Comunications_URL,
    data: {
      act: "get_all_chat",
      chat_id: $("#chat_original_id").val(),
      source: $("#chat_source").val()
    },
    success: function (data) {
      if (typeof data.error != "undefined") {
        if (data.error != "") {} else {
          $("#log").html("");
          $("#log").html(data.sms);
          chat_detail_id = data.chat_detail_id;
          $("#chat_name").val(data.name);
          if (data.chat_user_id == "") {
            $("#start_chat_wrap").show();
          } else {
            $("#start_chat_wrap").hide();
          }
          document.getElementById("chat_scroll").scrollTop = 25000000000;
          jQuery("time.timeago").timeago();
          $("#chat_hist").css("border-bottom", "none");
          $("#chat_live").css("border-bottom", "2px solid #333");
          all_chats = 1;
          var source = $("#chat_source").val() || "phone";
          var index = $(
              '#incoming_chat_tabs a[href="#incoming_chat_tab_' + source
            )
            .parent()
            .index();
          $("#incoming_chat_tabs").tabs("option", "active", index);
        }
      }
    }
  });
});
function LoadKendoTable__Info_Projects(hidden){

  $.ajax({
  url: 'server-side/call/incomming.action.php',
  data: {
    act: "get_status_items"
  },
  type: "GET",
  success: function(data){

    var options = data.options;

    //KendoUI CLASS CONFIGS BEGIN
    var aJaxURL	        =   "server-side/call/waiters.action.php";
    var gridName        = 	'kendo_project_info_table';
    var actions         = 	'<button id="add_project_info" style="margin-right: 10px"> დამატება</button><select id="project_info_item_status">' + options.map(o => { return '<option value="'+o.id+'">' + o.name + '</option>'}) + '</select><button data-select="info" id="change_project_status">სტატუსის შეცვლა</button><button id="remove_project_info_item">წაშლა</button>';
    var editType        =   "popup"; // Two types "popup" and "inline"
    var itemPerPage     = 	10;
    var columnsCount    =	4;
    var columnsSQL      = 	[
                  "id:string",
                  "status:date",
                  "project:string",
                  "info:string"
                ];
    var columnGeoNames  = 	[  
                  "ID", 
                  "სტატუსი",
                  "პროექტი",
                  "გაცემული ინფორმაცია"
                ];

    var showOperatorsByColumns  =   [0,0,0,0]; //IF NEED USE 1 ELSE USE 0
    var selectors               =   [0,0,0,0]; //IF NEED NOT USE 0

    var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
    //KendoUI CLASS CONFIGS END

      
    const kendo = new kendoUI();
    kendo.loadKendoUI(aJaxURL,'get_list_info_project',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);

    }
  })
}
function LoadDialog_comunications(fNm) {
  $("#loading").show();
  if (fNm == "add-edit-form-comunications") {
    var buttons = {
      // "ipaddres_block": {
      //     text: "IP დაბლოკვა",
      //     id: "ipaddres_block"
      // },
      // "num_block": {
      //     text: "ნომრის დაბლოკვა",
      //     id: "num_block"
      // },
      cancel: {
        text: "დახურვა",
        id: "cancel-dialog",
        click: function () {
          var buttons1 = {
            done: {
              text: "კი",
              id: "save-yes",
              click: function () {
                $("#add-edit-form-comunications").dialog("close");
                $(this).dialog("close");
                if ($("#status-place > a > span").html() == 'რეგისტრაცია') {
                }
                else {
                  $.ajax({
                      url: "AsteriskManager/dndAction.php",
                      data: {
                          action: 'dndoff',
                          activity_id: 1
                      },
                      success: function(res) {
                        var statusBG = $('#status-place > a > x');
                        var statusName = $('#status-place > a > span');
                        $('#aStatus li[data-id=1]').css("background-color", "");
                        statusBG.css("background-color", "#2dc100");
                        statusName.text('აქტიური');
                    
                        $('.icon_status_color').css("background-color", "#2dc100");
                      }
                  });
                  $.ajax({
                      url: 'includes/menu.action.php',
                      data: {
                          act: "activity_status",
                          status_id: 1
                      },
                      success: function(res) {
              
                      }
                  });
                  
                }
                
                localStorage.setItem('dialog_opened', 0);
              }
            },
            no: {
              text: "არა",
              id: "save-no",
              click: function () {
                $(this).dialog("close");
              }
            }
          };
          GetDialog("hint_close", 300, "auto", buttons1);
          $("#save-yes").css("float", "right");
          $("#save-no").css("float", "right");
        }
      },
      done: {
        text: "შენახვა",
        id: "save-dialog",
        class: "save-incomming-main-dialog"
      }
    };

    /* Dialog Form Selector Name, Buttons Array */
    var width = 1270;
    GetDialog("add-edit-form-comunications", width, "auto", buttons);
    $(".save-incomming-main-dialog").button();
    $(".add-edit-form-class .ui-dialog-titlebar-close").remove();
    $(".add-edit-form-class").css("left", "84.5px");
    $(".add-edit-form-class").css("top", "0px");
    $(".ui-dialog-buttonset").css("width", "100%");
    $("#cancel-dialog").css("float", "right");
    $("#save-dialog").css("float", "right");
    $("#search_ab_pin").button();
    $("#add_comment").button();
    $.ajax({
      url: Comunications_URL,
      data: {
        act: "get_all_chat",
        chat_id: $("#chat_original_id").val(),
        source: $("#chat_source").val()
      },
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {} else {
            console.log(">>>");
            $("#log").html("");
            $("#log").html(data.sms);
            chat_detail_id = data.chat_detail_id;
            $("#chat_name").val(data.name);
            if (data.chat_user_id == "") {
              $("#start_chat_wrap").show();
            } else {
              $("#start_chat_wrap").hide();
            }
            document.getElementById("chat_scroll").scrollTop = 25000000000;
            jQuery("time.timeago").timeago();
            $("#chat_hist").css("border-bottom", "none");
            $("#chat_live").css("border-bottom", "2px solid #333");
            all_chats = 1;
            var source = $("#chat_source").val() || "phone";
            var index = $(
                '#incoming_chat_tabs a[href="#incoming_chat_tab_' + source
              )
              .parent()
              .index();
            $("#incoming_chat_tabs").tabs("option", "active", index);
          }
        }
      }
    });
    $(
      "#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming_status_1, #incomming_status_1_1, #inc_status_a,#company, #space_type, #chat_language, #s_u_status"
    ).chosen({
      search_contains: true
    });
    var dLength = [
      [5, 10, 30, -1],
      [5, 10, 30, "ყველა"]
    ];
    $("#my_site_resume_chosen").css("width", "560px");
    GetDate("start_check");
    GetDate("end_check");
    //GetDataTable("table_history", Comunications_URL, "get_list_history", 7, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id=''", 0, dLength, 1, "desc", '', "<'F'lip>");

    $("#add_project_info").button();
    GetDataTable(
      "project_ifo_table",
      Comunications_URL,
      "get_list_info_project",
      4,
      "&id=" + $("#hidde_incomming_call_id").val(),
      0,
      dLength,
      1,
      "desc",
      "",
      "<'F'lip>"
    );
    $("#project_ifo_table_length").css("top", "0px");
    $("#project_ifo_table_length select").css("height", "16px");
    $("#project_ifo_table_length select").css("width", "56px");

    $("#table_history_length").css("top", "0px");
    $("#table_sms_length").css("top", "0px");

    $(".jquery-ui-button").button();

    $("#call_content").off();
    $("#call_content1").off();
    $("#send_message").off();

    $("#call_content").autocomplete(
      "server-side/seoy/seoy_textarea.action.php?source_id=" +
      $("#source_id").val() +
      "&site=" +
      $("#my_site")
      .chosen()
      .val(), {
        width: 300,
        multiple: true,
        matchContains: true
      }
    );
    $("#call_content1").autocomplete(
      "server-side/seoy/seoy_textarea1.action.php?source_id=" +
      $("#source_id").val() +
      "&site=" +
      $("#my_site")
      .chosen()
      .val(), {
        width: 300,
        multiple: true,
        matchContains: true
      }
    );
    var source_id = $("#source_id").val();
    if(source_id == 4)
{
  $("#send_message").autocomplete(
    "server-side/seoy/chat_textarea.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 360,
      multiple: true,
      matchContains: true
    }
  );
}
if(source_id == 7)
{
  $("#send_message").autocomplete(
    "server-side/seoy/chat_textarea2.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 360,
      multiple: true,
      matchContains: true
    }
  );
}

    //////////
    var id = $("#incomming_id").val();
    var cat_id = $("#category_parent_id").val();

    if (id != "" && cat_id == 407) {
      $("#additional").removeClass("hidden");
    }
    if ($("#ipaddres").val() == 0 || $("#ipaddres").val() == "") {
      $("#ipaddres_block").css("display", "none");
    }
    GetDateTimes("problem_date");
    GetDateTimes("transaction_date_time");

    if ($("#tab_id").val() == 5) {
      $("#num_block, .save-incomming-main-dialog").css("display", "none");
    } else {
      $("#num_block, .save-incomming-main-dialog").css("display", "");
    }

    if ($("#imchat").val() == 1 && $("#ast_source").val() == "") {
      $("#source_id").val(4);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else if ($("#fb_chat").val() > 0 && $("#ast_source").val() == "") {
      $("#source_id").val(6);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else if ($("#mail_chat").val() > 0 && $("#ast_source").val() == "") {
      $("#source_id").val(7);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else {
      //$('#source_id option:eq(1)').prop('selected', true);
      $("#chatcontent,#gare_div").css("display", "none");
      $("#chatistema").css("width", "210px");
      $(".add-edit-form-class").css("width", "879px");
      $("#cancel-chat").css("display", "none");
      $(".add-edit-form-class .ui-dialog-title").html("ზარის დამუშავება");
      $(".add-edit-form-class .ui-dialog-titlebar").css(
        "background",
        "#673AB7"
      );
      $(".add-edit-form-class .ui-dialog-title").css("color", "#fff");
    }

    GetDateTimes("vizit_datetime");

    $("#send_message").keyup(function (e) {
      console.log("click");
      if ($("#source_id").val() == 4) {
        if (e.key == 8) {
          param = new Object();
          param.act = "focusout";
          param.id = $("#chat_original_id").val();

          $.ajax({
            url: Comunications_URL,
            data: param,
            success: function (data) {}
          });
        } else {
          param = new Object();
          param.act = "focusin";
          param.id = $("#chat_original_id").val();

          $.ajax({
            url: Comunications_URL,
            data: param,
            success: function (data) {}
          });
        }
      }
    });

    jQuery("time.timeago").timeago();
    //getchats();
    $("#choose_button").button();

    all_chats = 1;
    $("#incoming_chat_tabs").tabs();

    ////////////////////
  } else if (fNm == "crm_dialog") {
    var buttons = {
      cancel: {
        text: "დახურვა",
        id: "cancel-dialog",
        click: function () {
          $(this).dialog("close");
          $(this).html("");
        }
      }
    };
    GetDialog("crm_dialog", 900, "auto", buttons, "top");
    GetDataTable(
      "example_all_file",
      Comunications_URL_crm,
      "get_list",
      3,
      "request_table_id=" + $("#request_table_id").val(),
      0,
      "",
      1,
      "desc",
      "",
      ""
    );
    SetEvents(
      "",
      "",
      "",
      "example_all_file",
      "crm_file_dialog",
      Comunications_URL_crm_file
    );
    setTimeout(function () {
      $(".ColVis, .dataTable_buttons").css("display", "none");
    }, 90);
  } else if (fNm == "crm_file_dialog") {
    var buttons = {
      cancel: {
        text: "დახურვა",
        id: "cancel-dialog",
        click: function () {
          $(this).dialog("close");
          $(this).html("");
        }
      }
    };
    GetDialog("crm_file_dialog", "auto", "auto", buttons, "top");
  } else {
    $("#incoming_chat_tabs").tabs();
    var buttons = {
      //
      // "ipaddres_block": {
      //     text: "IP დაბლოკვა",
      //     id: "ipaddres_block"
      // },
      // "num_block": {
      //     text: "ნომრის დაბლოკვა",
      //     id: "num_block"
      // },
      cancel: {
        text: "დახურვა",
        id: "cancel-dialog",
        click: function () {
          var buttons1 = {
            done: {
              text: "კი",
              id: "save-yes",
              click: function () {
                $("#add-edit-form-comunications").dialog("close");
                $(this).dialog("close");
                if ($("#status-place > a > span").html() == 'რეგისტრაცია' || $("#status-place > a > span").html() == 'ონლაინ ჩათი/ზარი' || $("#status-place > a > span").html() == 'ონლაინ ჩათი' || $("#status-place > a > span").html() == 'მონიტორინგი') {
                }
                else {
                  $.ajax({
                      url: "AsteriskManager/dndAction.php",
                      data: {
                          action: 'dndoff',
                          activity_id: 1
                      },
                      success: function(res) {
                        var statusBG = $('#status-place > a > x');
                        var statusName = $('#status-place > a > span');
                        $('#aStatus li[data-id=1]').css("background-color", "");
                        statusBG.css("background-color", "#2dc100");
                        statusName.text('აქტიური');
                    
                        $('.icon_status_color').css("background-color", "#2dc100");
                      }
                  });
                  $.ajax({
                      url: 'includes/menu.action.php',
                      data: {
                          act: "activity_status",
                          status_id: 1
                      },
                      success: function(res) {
              
                      }
                  });
                  
                }
                
                localStorage.setItem('dialog_opened', 0);
              }
            },
            no: {
              text: "არა",
              id: "save-no",
              click: function () {
                $(this).dialog("close");
              }
            }
          };
          GetDialog("hint_close", 300, "auto", buttons1);
          $("#save-yes").css("float", "right");
          $("#save-no").css("float", "right");
        }
      },
      done: {
        text: "შენახვა",
        id: "save-dialog",
        class: "save-incomming-main-dialog"
      }
    };

    /* Dialog Form Selector Name, Buttons Array */
    var width = 1270;
    GetDialog("add-edit-form-comunications", width, "auto", buttons);
    $(".save-incomming-main-dialog").button();
    $(".add-edit-form-class .ui-dialog-titlebar-close").remove();
    $(".add-edit-form-class").css("left", "84.5px");
    $(".add-edit-form-class").css("top", "0px");
    $(".ui-dialog-buttonset").css("width", "100%");
    $("#cancel-dialog").css("float", "right");
    $("#save-dialog").css("float", "right");
    $(
      "#incomming_cat_1, #incomming_cat_1_1, #incomming_status_1, #incomming_status_1_1,  #inc_status_a, #company, #space_type, #chat_language, #incomming_cat_1_1_1,   #s_u_status"
    ).chosen({
      search_contains: true
    });
    $("#search_ab_pin").button();
    $("#add_comment").button();
    $.ajax({
      url: Comunications_URL,
      data: {
        act: "get_all_chat",
        chat_id: $("#chat_original_id").val(),
        source: $("#chat_source").val()
      },
      success: function (data) {
        if (typeof data.error != "undefined") {
          if (data.error != "") {} else {
            // console.log("clickedme");

            // let ChatId = $("#dialog_active_chat").attr("aria-data");
            // console.log(ChatId);
            // if ($(".clickmetostart").attr("chat_id") == ChatId) {
            //   $(".clickmetostart").css("background-color", "#f44");
            // }
            // // if ($(".clickmetostart").attr("chat_id") != ChatId) {
            // //   $(".clickmetostart").css("box-shadow", "");
            // // }

            $("#log").html("");
            $("#log").html(data.sms);
            chat_detail_id = data.chat_detail_id;
            $("#chat_name").val(data.name);
            if (data.chat_user_id == "") {
              $("#start_chat_wrap").show();
            } else {
              $("#start_chat_wrap").hide();
            }

            document.getElementById("chat_scroll").scrollTop = 25000000000;
            jQuery("time.timeago").timeago();
            $("#chat_hist").css("border-bottom", "none");
            $("#chat_live").css("border-bottom", "2px solid #333");
            all_chats = 1;
            var source = $("#chat_source").val() || "phone";
            var index = $(
                '#incoming_chat_tabs a[href="#incoming_chat_tab_' + source
              )
              .parent()
              .index();
            $("#incoming_chat_tabs").tabs("option", "active", index);
          }
        }
      }
    });
    $("#my_site_resume_chosen").css("width", "560px");
    var dLength = [
      [5, 10, 30, -1],
      [5, 10, 30, "ყველა"]
    ];
    GetDate("start_check");
    GetDate("end_check");
    GetDateTimes("vizit_datetime");
    //GetDataTable("table_history", Comunications_URL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 1, "desc", '', "<'F'lip>");

    $("#add_project_info").button();
    GetDataTable(
      "project_ifo_table",
      Comunications_URL,
      "get_list_info_project",
      4,
      "&id=" + $("#hidde_incomming_call_id").val(),
      0,
      dLength,
      1,
      "desc",
      "",
      "<'F'lip>"
    );
    $("#project_ifo_table_length").css("top", "0px");
    $("#project_ifo_table_length select").css("height", "16px");
    $("#project_ifo_table_length select").css("width", "56px");

    $("#table_history_length").css("top", "0px");
    $(".jquery-ui-button").button();
    GetTabs("tabs_sport");

    $("#call_content").off();
    $("#call_content1").off();
    $("#send_message").off();

    $("#call_content").autocomplete(
      "server-side/seoy/seoy_textarea.action.php?source_id=" +
      $("#source_id").val() +
      "&site=" +
      $("#my_site")
      .chosen()
      .val(), {
        width: 300,
        multiple: true,
        matchContains: true
      }
    );

    $("#call_content1").autocomplete(
      "server-side/seoy/seoy_textarea1.action.php?source_id=" +
      $("#source_id").val() +
      "&site=" +
      $("#my_site")
      .chosen()
      .val(), {
        width: 300,
        multiple: true,
        matchContains: true
      }
    );
    var source_id = $("#source_id").val();
    if(source_id == 4)
{
  $("#send_message").autocomplete(
    "server-side/seoy/chat_textarea.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 360,
      multiple: true,
      matchContains: true
    }
  );
}
if(source_id == 7)
{
  $("#send_message").autocomplete(
    "server-side/seoy/chat_textarea2.action.php?source_id=" +
    $("#source_id").val() +
    "&site=" +
    $("#my_site")
    .chosen()
    .val(), {
      width: 360,
      multiple: true,
      matchContains: true
    }
  );
}

    var id = $("#incomming_id").val();

    if ($("#tab_id").val() == 5) {
      $("#num_block, .save-incomming-main-dialog").css("display", "none");
    } else {
      $("#num_block, .save-incomming-main-dialog").css("display", "");
    }

    if ($("#imchat").val() == 1 && $("#ast_source").val() == "") {
      $("#source_id").val(4);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else if ($("#site_chat").val() > 0 && $("#ast_source").val() == "") {
      $("#source_id").val(9);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else if ($("#fb_chat").val() > 0 && $("#ast_source").val() == "") {
      $("#source_id").val(6);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else if ($("#mail_chat").val() > 0 && $("#ast_source").val() == "") {
      $("#source_id").val(7);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("შემომავალი ჩატი");
      $("#num_block").css("display", "none");
    } else if ($("#video_chat_id").val() > 0 && $("#ast_source").val() == "") {
      $("#source_id").val(12);
      $("#source_id").trigger("chosen:updated");
      $(".add-edit-form-class .ui-dialog-title").html("ვიდეო ზარი");
      $("#num_block").css("display", "none");
      $("#chatistema").css("width", "210px");
      $(".add-edit-form-class").css("width", "879px");
      $("#cancel-chat").css("display", "none");
      $("#chatcontent,#gare_div").css("display", "none");
    } else {
      //$('#source_id option:eq(1)').prop('selected', true);
      $("#chatcontent,#gare_div").css("display", "none");
      $("#chatistema").css("width", "210px");
      $(".add-edit-form-class").css("width", "879px");
      $("#cancel-chat").css("display", "none");
      $(".add-edit-form-class .ui-dialog-title").html("ზარის დამუშავება");
      $(".add-edit-form-class .ui-dialog-titlebar").css(
        "background",
        "#673AB7"
      );
      $(".add-edit-form-class .ui-dialog-title").css("color", "#fff");
    }

    $("#send_message").keyup(function (e) {
      if ($("#source_id").val() == 4) {
        console.log("click");
        if (e.key == 8) {
          param = new Object();
          param.act = "focusout";
          param.id = $("#chat_original_id").val();

          $.ajax({
            url: Comunications_URL,
            data: param,
            success: function (data) {}
          });
        } else {
          param = new Object();
          param.act = "focusin";
          param.id = $("#chat_original_id").val();

          $.ajax({
            url: Comunications_URL,
            data: param,
            success: function (data) {}
          });
        }
      }
    });

    jQuery("time.timeago").timeago();
    //getchats();
    $("#choose_button").button();

    ///////////////////
    // ai dunia      //
    ///////////////////
    if ($("#add-edit-form-comunications input").length != 0) {

    }
  }
}

$(document).on("click", "#add_comment", function () { // save answer
    param                   = new Object();
    param.act               = 'save_comment';
    param.comment           = $("#add_ask").val();
    param.incomming_call_id = $("#incomming_call_id").val();
    if(param.comment!=""){
        $.ajax({
            url: aJaxURL,
            data: param,
            success: function(data) {
                param2                      = new Object();
                param2.act                  = 'get_add_question';
                param2.incomming_call_id    = $("#incomming_call_id").val();
                $.ajax({
                    url: aJaxURL,
                    data: param2,
                    success: function(data) {
                        $("#chat_question").html(data.page);
                        $("#add_comment").button();
                    }
                });
            }
        });
    }
});
$(document).on("click", "#chat_end", function () {
  if ($("#chat_original_id").val() != "0") {
    var buttons = {
      cancel: {
        text: "არა",
        id: "cancel-dialog",
        click: function () {
          $(this).dialog("close");
          $(this).html("");
        }
      },
      save: {
        text: "დიახ",
        id: "close_chat_dialog",
        click: function () {
          $.ajax({
            url: Comunications_URL,
            data: "act=chat_end&chat_source=" +$("#chat_source").val()+ "+&chat_id=" + $("#chat_original_id").val(),
            success: function (data) {
              if (typeof data.error != "undefined") {
                if (data.error != "") {
                  // alert(data.error);
                  //alert("Caucasus Jixv");
                } else {
                  $("#chat_close_dialog").dialog("close");
                }
              }
            }
          });
        }
      }
    };

    /* Dialog Form Selector Name, Buttons Array */
    GetDialog("chat_close_dialog", 400, "auto", buttons);
  }
});


/** 
 * CNOBARI SELECT
*/

// $(document).on("click", "#project_select", function(){

//   var selectedID = $(this).val()
// console.log(selectedID)
//   var getCnobari = $(".cnobari_projects").attr("data-id");

//   if(getCnobari == selectedID){
//     $(".cnobari_projects#"+selectedID).css("display", "")
//   }else{
//     $(".cnobari_projects").css("display", "none")
//   }

//   })


/** 
* FILTERS
*/
$(document).on("click", "#filter_multiselect span", function(e){
  e.preventDefault();

  if($("#filter_multiselect_items").attr("data-status") == 0){
    $("#filter_multiselect_items").attr("data-status", 1)
    $("#filter_multiselect_items").css("display", "block");
  }else{
    $("#filter_multiselect_items").attr("data-status", 0)
    $("#filter_multiselect_items").css("display", "none");
  }

})


$(document).on("click", "#filter_multiselect_items li", function(e){

  var Name = $(this).attr("data-name"); // გამოძახებული ფილტრის სახელი
  var index = $(this).attr("data-id");  // გამოძახებული ფილტრის ინდექსი
  var indexStatus = $(this).attr("data-status");  // გამოძახებული ფილტრის სტატუსი

  $(this).attr("data-status", 1);
  $(this).css("display", "none");
  $("#column_names li#"+index).attr("data-status", 1);
  $("#column_names li#"+index).click();

})


$(document).on('click', '#remove_project_info_item', function(){
  var param = new Object();
      param.act           = "remove_project_info_item";
      param.incomming_id  = $('#hidden_incomming_call_id').val();

      var IDs = []
      var entityGrid = $("#kendo_project_info_table").data("kendoGrid");
      var rows = entityGrid.select();
      rows.each(function(index, row) {
        var selectedItem = entityGrid.dataItem(row);
        IDs.push(selectedItem.id);
      });
      param.ids = IDs;

      if(IDs.length == 0){
        alert("მონიშნეთ პროექტ(ებ)ი");
      }else{
        $.ajax({
          url: 'server-side/call/incomming.action.php',
          data: param,
          beforeSend: function(){
            console.log('changing');
          },
          success: function(data){

            if(data.errors == 0){
              $("#kendo_project_info_table").data("kendoGrid").dataSource.read();
            }

          }
        })
      }
})

  $(document).on('click', '#change_project_status', function(){
    
    console.log(this.dataset.select);
   
    if(this.dataset.select == "info"){

      var param = new Object();
      param.act = "edit_project_info_status";
      param.value = $('#project_info_item_status').children("option:selected").val();
      
      var IDs = []
      var entityGrid = $("#kendo_project_info_table").data("kendoGrid");
      var rows = entityGrid.select();
      rows.each(function(index, row) {
        var selectedItem = entityGrid.dataItem(row);
        IDs.push(selectedItem.id);
      });
      param.ids = IDs;

      if(IDs.length == 0){
        alert("მონიშნეთ პროექტ(ებ)ი");
      }else{
        $.ajax({
          url: 'server-side/call/incomming.action.php',
          data: param,
          beforeSend: function(){
            console.log('changing');
          },
          success: function(data){

            if(data.errors == 0){
              $("#kendo_project_info_table").data("kendoGrid").dataSource.read();
              $('#project_info_item_status').val($('#project_item_status option:first').val());
            }else{
              $('#project_info_item_status').val($('#project_item_status option:first').val());
            }

          }
        })
      }
  }else{
      var param = new Object();
      param.act = "edit_project_status";
      param.value = $('#project_item_status').children("option:selected").val();
      
      var IDs = []
      var entityGrid = $("#project_info_view_table").data("kendoGrid");
      var rows = entityGrid.select();
      rows.each(function(index, row) {
        var selectedItem = entityGrid.dataItem(row);
        IDs.push(selectedItem.id);
      });
      param.ids = IDs;

      if(IDs.length == 0){
        alert("მონიშნეთ პროექტ(ებ)ი");
      }else{
        $.ajax({
          url: 'server-side/call/incomming.action.php',
          data: param,
          beforeSend: function(){
            console.log('changing');
          },
          success: function(data){

            if(data.errors == 0){
              $("#project_info_view_table").data("kendoGrid").dataSource.read();
              $('#project_item_status').val($('#project_item_status option:first').val());
            }else{
              $('#project_item_status').val($('#project_item_status option:first').val());
            }

          }
        })
      }
    }

  })

function LoadKendoTable__Projects(hidden){


  $.ajax({
    url: 'server-side/call/incomming.action.php',
    data: {
      act: "get_status_items"
    },
    type: "GET",
    success: function(data){

    var options = data.options;

    //KendoUI CLASS CONFIGS BEGIN
    var aJaxURL	        =   "server-side/call/incomming.action.php";
    var gridName        = 	'project_info_view_table';
    var actions         = 	'<select id="project_item_status">' + options.map(o => { return '<option value="'+o.id+'">' + o.name + '</option>'}) + '</select><button id="change_project_status">სტატუსის შეცვლა</button>';
    var editType        =   "popup"; // Two types "popup" and "inline"
    var itemPerPage     = 	10;
    var columnsCount    =	  20;
    var columnsSQL      = 	[
                              "id:string",
                              "status:date",
                              "project:string",
                              "category:string",
                              "view:string", 
                              "sector:string",
                              "floor:string",
                              "number:string",
                              "sleeping_room:string",
                              "sacx_far:string",
                              "saz_far:string",
                              "balcony:string",
                              "saerto_far:string",
                              "price:string",
                              "ganv_price:string",
                              "full_price:string",
                              "stand_winaswar:string",
                              "cadr_code:string",
                              "phone:string",
                              "address:string"
                            ];
    var columnGeoNames  = 	[  
                              "ID", 
                              "სტატუსი",
                              "პროექტი",
                              "კატეგორია",
                              "ხედი",
                              "სექტორი",
                              "სართული",
                              "ბინის ნომერი",
                              "საძინებელი",
                              "საცხოვრებელი (კვ.მ)",
                              "საზაფხულო (კვ.მ)",
                              "აივანი",
                              "საერთო ფართი (კვ.მ)",
                              "ფასი ერთიანის",
                              "ფასი განვადების",
                              "ფასი სულ",
                              "სტანდარტული წინასწარ",
                              "საკანდასტრო კოდი",
                              "ტელეფონი",
                              "მისამართი"
                            ];

    var showOperatorsByColumns  =   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
    var selectors               = 	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0

    var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
    //KendoUI CLASS CONFIGS END
  
        
        const kendo = new kendoUI();
    kendo.loadKendoUI(aJaxURL,'get_list_project',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden,1);
    
    setTimeout(function(){
      var grid = $("#project_info_view_table").data("kendoGrid");
      grid.hideColumn(0);

      $("#project_info_view_table").kendoTooltip({  
        show: function(e){  
            if(this.content.text().length > 5){  
            this.content.parent().css("visibility", "visible");  
            }  
        },  
        hide:function(e){  
            this.content.parent().css("visibility", "hidden");  
        },  
        filter: "td", 
        position: "right",  
        content: function(e){  
            // var dataItem = $("#kendo_project_info_table").data("kendoGrid").dataItem(e.target.closest("tr"));  
            // console.log("bottom ", dataItem.info)
            var content = e.target[0].innerText;  
            return content;  
        }  
        }).data("kendoTooltip"); 
    }, 500);
    
  }
    });
    
}

$(document).on("click", ".dialog_filter_checklist_item .callapp_filter_body_span_input", function(){

  var checkArr = $(".dialog_filter_checklist_item .callapp_filter_body_span_input:checkbox:checked");
  let FilterArr = new Array();

  var hidden = "&FilterArr="+JSON.stringify(FilterArr);
  
  // if(checkArr.length == 0){

  //   LoadKendoTable__Projects(hidden);
   
  // }else{
  //   checkArr.map((x, i) => {
  //     return FilterArr.push({ filterValue: i.dataset.name, columnName: i.dataset.id })
  //   })
        
  //       var hidden = "&FilterArr="+JSON.stringify(FilterArr);
  //       LoadKendoTable__Projects(hidden);
        
  // }

  var hidden = "&incomming_id="+$('#hidden_incomming_call_id').val();
  LoadKendoTable__Projects(hidden);

})
$(document).on('click','#chat_expand',function(){
    if($(this).attr('expanded') == 0)
    {
        $(this).attr('expanded','1');
        $(".communication_chat_style").css('width','951px');
        $(this).css('transform', 'rotate(180deg)');
    }
    else{
        $(this).attr('expanded','0');
        $(".communication_chat_style").css('width','329px');
        $(this).css('transform', 'rotate(0)');
    }
    
});
$(document).on("click", "#column_names li", function (e) { /** @note გასასწორებელია. დაკლიკვისას სანამ არ ჩაიტვირთება უნდა იყოს გათიშულ მდგომაროებაში */
    e.preventDefault();

  var Name = $(this).attr("data-name"); // გამოძახებული ფილტრის სახელი
  var index = $(this).attr("data-id");  // გამოძახებული ფილტრის ინდექსი
  var indexStatus = $(this).attr("data-status");  // გამოძახებული ფილტრის სტატუსი

  // გამოძახებული ფილტრის შემოწმება
  if($("#dialog_filters #"+index).length == 0 && $("#dialog_filters #"+index).length < 2 && !$("#column_names li#"+index).attr("disabled") ){
      // console.log($("#dialog_filters #"+index).length)
      
      $("#filter_multiselect_items li#"+index).css("display", "none"); // გამოძახებული ფილტრის ღილაკის ამოშლა სელექტორიდან
      $("#column_names li#"+index).attr("data-status", 1);
      $("#filter_multiselect_items li#"+index).attr("data-status", 1);

      var getSetFilters = localStorage.getItem("dialogFilter");
      var DialogFilters = JSON.parse(getSetFilters);

      let FilterObject;

      if(DialogFilters == null || DialogFilters == undefined){
        FilterObject = [{ index: index, indexStatus: 1 }]
      }else{
        var currentFilter = DialogFilters.findIndex(x => x.index === index)
        if(currentFilter === -1){
          FilterObject = [...DialogFilters, { index: index, indexStatus: 1 }]
        }else{
          FilterObject = [...DialogFilters]
        }
      }

      // LocalStorage_ში ჩაწერა
      localStorage.setItem("dialogFilter", JSON.stringify(FilterObject))

      // incomming.action.php GET - გამოძახებული ფილტრის ლისტის წამოღება
      $.ajax({
        url: Comunications_URL,
        data: {
          act: "get_filter_details",
          filterName: Name
        },
        type: "GET",
        beforeSend: function(xhr){
            $("#column_names li#"+index).addClass("loading_li") // კლიკის დროს ჩარტვითვის პროცესის ბეგრაუნდის შექმნა ღილაკისთვის
            $("#column_names li#"+index).attr("disabled", true);
        },
        success: function(data){
            var dataList = data.result;
            var filterID = data.filterID;
            console.log(dataList)

            $("#column_names li#"+index).removeAttr('disabled');
            
            // გამოძახებული ფილტრის ღილაკის მონიშვნა
            $("#column_names li#"+index).removeClass("loading_li");  // ჩატვირთვის პროცესის ამოშლა
            $("#column_names li#"+index).addClass("selected_li")  // ღილაკის ჩართული მდგომარეობის მინიჭება
         
            var counts = dataList.map((q, i) => {
              return JSON.parse(q.count);
            });

            var countSum = counts.reduce(function (acc, score) {
              return acc + score;
            }, 0);

            // გამოძახებული ფილტრის შექმნა.
            $("#dialog_filters").append('<td style="width: 230px" id="'+index+'" data-show="true"><div class="dialog_filter_title"> <span>' + Name + '</span><span>'+
            countSum
            +'</span></div><div class="dialog_filter_search"><input id="'+index+'" type="text" placeholder="ძებნა"></div><ul id="'+index+'" class="dialog_filter_checklist">'+


            dataList.map((q, i) => {
              if(q.name.length == 0){ // ცარიელი გრაფები
                    listItem = '<li class="dialog_filter_checklist_item" data-value="ცარიელი" id="'+index+'" ><label style="padding: 0;" for="f'+index+'_'+i+'">ცარიელი</label><div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;"><input class="callapp_filter_body_span_input" id="f'+index+'_'+i+'" data-id="'+filterID+'" data-name="" type="checkbox" value="e"><label for="f'+index+'_'+i+'" class="l_checkbox"></label></div><num class="l_dialog_filter_list_num">'+q.count+'</num></li>';
                    
                    return listItem
              } else{ // ყველა გარდა ცარიელი გრაფებისა
                    
                    listItem = '<li class="dialog_filter_checklist_item" data-value="'+q.name+'" id="'+index+'"><label style="padding: 0;" for="f'+index+'_'+i+'">'+q.name+'</label><div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;"><input class="callapp_filter_body_span_input" id="f'+index+'_'+i+'"  data-id="'+filterID+'" data-name="'+q.name+'" type="checkbox" value="'+q.name+'"><label for="f'+index+'_'+i+'" class="l_checkbox"></label></div><num class="l_dialog_filter_list_num">'+q.count+'</num></li>';
                    
                    return listItem;
                }
              }).join("")+'</ul></td>')
        }
      })
  }else{

    var getSetFilters = localStorage.getItem("dialogFilter");
    var DialogFilters = JSON.parse(getSetFilters);
    if(DialogFilters != null){
      var currentFilter = DialogFilters.findIndex(x => x.index === index)
      if (currentFilter > -1) {
        DialogFilters.splice(currentFilter, 1);
      }
      localStorage.removeItem("dialogFilter");
      
      localStorage.setItem("dialogFilter", JSON.stringify(DialogFilters))
    }

      // გამოძახებული ფილტრის წაშლა
      $(this).removeClass("selected_li"); // თავდაპირველ მდგომარეობაზე დაბრუნება
      $("#dialog_filters #"+index).remove();  // ფილტრის წაშლა
      $("#filter_multiselect_items li#"+index).css("display", ""); // სელექტორში ღილაკის დაბრუნება

      $("#column_names li#"+index).attr("data-status", 0); // ღილაკისთვის სტატუსის მინიჭება
      $("#filter_multiselect_items li#"+index).attr("data-status", 0); // სელექტორის ღილაკისთვის სტატუსის მინიჭება
    }
})

  $(document).on("click", "#add_project_info", function () {
    var buttons = {
      cancel: {
        text: "დახურვა",
        id: "cancel-dialog",
        click: function () {
          $(this).dialog("close");
        }
      },
      save: {
        text: "შენახვა",
        id: "save_info_project",
        click: function () {
          // var data = $(".check:checked")
          //   .map(function () {
          //     return this.value;
          //   })
          //   .get();


            var data = []
            var entityGrid = $("#project_info_view_table").data("kendoGrid");
            var rows = entityGrid.select();
            rows.each(function(index, row) {
              var selectedItem = entityGrid.dataItem(row);
              data.push(selectedItem.id);
            });
            
            if(data.length > 0){
              $.ajax({
                url: Comunications_URL,
                data: {
                  act: "get_project_address",
                  id: data.slice(-1)[0]
                },
                success: function(data){
                  $('#vizit_location').val(data.address);
                }
              });
            }

          var letters = [];

          for (var i = 0; i < data.length; i++) {
            letters.push(data[i]);
          }
          param = new Object();
          param.act = "add_project";
          param.lt = letters;
          if($("#hidden_incomming_call_id").length){
            param.id = $("#hidden_incomming_call_id").val();
          }else{
            param.id = $("#hidde_incomming_call_id").val();
          }
          var link = GetAjaxData(param);

          if (param.rp == "0") {
            alert("აირჩიეთ პასუხისმგებელი პირი!");
          } else {
            $.ajax({
              url: aJaxURL,
              type: "POST",
              data: link,
              dataType: "json",
              success: function (data) {
                let idd;

                if($("#hidden_incomming_call_id").length){
                  idd = $("#hidden_incomming_call_id").val();
                }else{
                  idd = $("#hidde_incomming_call_id").val();
                }

                $("#kendo_project_info_table").data("kendoGrid").dataSource.read();
                
                $("#info_project_dialog").dialog("close");

               
                // var dLength = [
                //   [5, 10, -1],
                //   [5, 10, "ყველა"]
                // ];

                // $("#info_project_dialog").dialog("close");
                // if($("#project_info_table").length){
                //   GetDataTable(
                //     "project_info_table",
                //     aJaxURL,
                //     "get_list_info_project",
                //     4,
                //     "&id=" + idd,
                //     0,
                //     dLength,
                //     1,
                //     "desc",
                //     "",
                //     "<'F'lip>"
                //   );
                // }else{
                //   GetDataTable(
                //     "project_ifo_table",
                //     Comunications_URL,
                //     "get_list_info_project",
                //     4,
                //     "&id=" + idd,
                //     0,
                //     dLength,
                //     1,
                //     "desc",
                //     "",
                //     "<'F'lip>"
                //   );
                // }
                
                // $("#project_ifo_table_length").css("top", "0px");
                // $("#project_ifo_table_length select").css("height", "16px");
                // $("#project_ifo_table_length select").css("width", "56px");
              }
            });
          }
        }
      }
    };


    // ცხრილის სათაურების ამოღება „project_ifo_view_table“-დან
    // // var projectColumnNames = getTableColumns("project_ifo_view_table"); 
    // var projectColumnNames = [  
    //                           "ID", 
    //                           "სტატუსი",
    //                           "პროექტი",
    //                           "კატეგორია",
    //                           "ხედი",
    //                           "სექტორი",
    //                           "სართული",
    //                           "ბინის ნომერი",
    //                           "საძინებელი",
    //                           "საცხოვრებელი (კვ.მ)",
    //                           "საზაფხულო (კვ.მ)",
    //                           "აივანი",
    //                           "საერთო ფართი (კვ.მ)",
    //                           "ფასი ერთიანის",
    //                           "ფასი განვადების",
    //                           "ფასი სულ",
    //                           "სტანდარტული წინასწარ",
    //                           "საკანდასტრო კოდი"
    //                         ];

    // // ცხრილის სათაურების მასივი
    // const sProjectColumnNames = projectColumnNames.map((j, i) => {
    //   if (j != "ID" && j != "N" && j != "#") {
    //     return  '<li id="'+i+'" data-id="'+i+'" data-status="0" data-name="' + j + '">' + j + '</li>';
    // }
    // })

    // სათაურების გამოტანა როგორც ფილტრის ღილაკები.
    // if($("#column_names li").length == 0){
    //   $("#column_names").append(sProjectColumnNames);
    // }

    // სათაურების გამოტანა სელექტორად.
    // if($("#filter_multiselect_items li").length == 0){
    //   $("#filter_multiselect_items").append(sProjectColumnNames);
    // }
    
    // if($("#filter_multiselect_items").attr("data-status") == 0){
    //   $("#filter_multiselect_items").css("display", "none");
    // }

    
    // if(!localStorage.getItem("dialogFilter")){
    //   var emtpyArr = [];

    //   localStorage.setItem("dialogFilter", JSON.stringify(emtpyArr)) 
    // }

    //   var getSetFilters = localStorage.getItem("dialogFilter");
    //   var DialogFilters = JSON.parse(getSetFilters);


    //     if(DialogFilters != null || DialogFilters.length != 0){
    //     DialogFilters.map(x => {
    //       index = x.index;
    //       if($("#column_names li#"+index).attr("data-status") == 0){
    //         $("#column_names li#"+index).click()
    //       }
    //       })
    //     }
      

    // $(document).on("keyup", "#multiselect_filter_search", function(){
    
    //   filter = this.value.toUpperCase();
    //   ul = document.getElementById("filter_multiselect_items");
    //   li = ul.getElementsByTagName("li");

    //   for (i = 0; i < li.length; i++) {
    //     a = li[i];
    //     txtValue = a.textContent || a.innerText;

    //     if (txtValue.toUpperCase().indexOf(filter) > -1) {
    //         li[i].style.display = "";
    //     } else {
    //         li[i].style.display = "none";
    //     }
    // }
    // })


    // $(document).on("keyup", ".dialog_filter_search input", function(){

    //   filter = this.value.toUpperCase();

    //   index = $(this).attr("id");
    //   checklist = $(".dialog_filter_checklist#"+index);
      
    //   item = $(checklist.selector+" li");

    //   // console.log(item);

    //     for (i = 0; i < item.length; i++) {
    //       a = item[i];
    //       txtValue = a.dataset.value

    //       if (txtValue.toUpperCase().indexOf(filter) > -1) {
    //         item[i].style.display = "";
    //       } else {
    //         item[i].style.display = "none";
    //       }
    //   }
    // })



    /* Dialog Form Selector Name, Buttons Array */
    GetDialog("info_project_dialog", '100%', "auto", buttons, "center top");
    // var dLength = [
    //   [5, 10, -1],
    //   [5, 10, "ყველა"]
    // ];
    // GetDataTable(
    //   "project_ifo_view_table",
    //   Comunications_URL,
    //   "get_list_project",
    //   18,
    //   "",
    //   0,
    //   dLength,
    //   1,
    //   "desc",
    //   "",
    //   "<'F'lip>"
    // );
    
    // $("#project_ifo_view_table_length").css("top", "0px");
    // $("#project_ifo_view_table_length select").css("height", "16px");
    // $("#project_ifo_view_table_length select").css("width", "56px");

    var hidden = "&incomming_id="+$('#hidden_incomming_call_id').val();

    LoadKendoTable__Projects(hidden);
    
    
  });

function is_dialog_open() {
  try {
    if ($("#add-edit-form-comunications").dialog("isOpen")) return true;
    else return false;
  } catch (error) {
    return false;
  }
  return false;
}

function show_right_side(id) {
  $("#right_side ff").hide();
  $("#" + id).show();
  //$(".add-edit-form-class").css("width", "1260");
  //$('#add-edit-form').dialog({ position: 'left top' });
  hide_right_side();

  var str = $("." + id).children("img").attr("src");
  str = str.substring(0, str.length - 4);
  $("#side_menu span").children("img").css("border-bottom", "2px solid transparent");
  $("." + id).children("img").css("filter", "brightness(0.1)");
  $("." + id).children("img").css("border-bottom", "2px solid #333");

  if(id == 'client_history'){
    var phone = $('#telefoni___183--124--1').val();
    var modified_phone = phone.replace(/-/g,'');
    $('#client_history_phone').val('995'+modified_phone);
  }
}

function hide_right_side() {
	
  $(".info").children("img").css("filter", "brightness(1.1)");
  $(".client_history").children("img").css("filter", "brightness(1.1)");
  $(".task").children("img").css("filter", "brightness(1.1)");
  $(".record").children("img").css("filter", "brightness(1.1)");
  $(".incomming_file").children("img").css("filter", "brightness(1.1)");
  $(".sms").children("img").css("filter", "brightness(1.1)");
  $(".incomming_mail").children("img").css("filter", "brightness(1.1)");
  $(".client_conversation").children("img").css("filter", "brightness(1.1)");
  $(".crm").children("img").css("filter", "brightness(1.1)");
  $(".chat_question").children("img").css("filter", "brightness(1.1)");
  $(".user_logs").children("img").css("filter", "brightness(1.1)");
  $(".call_resume").children("img").css("filter", "brightness(1.1)");
  $(".newsnews").children("img").css("filter", "brightness(1.1)");
  
  $("#record fieldset").show();
}

$(document).on("click", "#search_user_id", function () {
  param = new Object();
  param.act = "search_user_id";
  param.s_u_user_id = $("#s_u_user_id").val();
  $.ajax({
    url: wsdl_url,
    data: param,
    success: function (data) {
      if (data.count == 1) {
        if (data.phone != "" && data.phone != "null" && data.phone != null) {
          $("#phone").val(data.phone);
          $("#check_ab_phone").val(data.phone);
        }
        $("#s_u_user_id").val(data.user_id);
        $("#check_ab_user_id").val(data.user_id);
        $("#s_u_mail").val(data.email);
        $("#s_u_name").val(data.user_name + " " + data.user_surname);
        $("#s_u_pid").val(data.id_code);
        $("#client_sex").val(data.gender_id);
        $("#client_birth_year").val(data.birth_year);
        $("#client_sex").trigger("chosen:updated");
      } else if (data.count == 0) {
        alert("მონაცემები ვერ მოიძებნა");
      }
    }
  });
});

$(document).on("click", "#search_phone", function () {
  param = new Object();
  param.act = "search_phone";
  param.phone = $("#phone").val();
  $.ajax({
    url: wsdl_url,
    data: param,
    success: function (data) {
      if (data.count == 1) {
        if (data.phone != "" && data.phone != "null" && data.phone != null) {
          $("#phone").val(data.phone);
          $("#check_ab_phone").val(data.phone);
        }
        $("#s_u_user_id").val(data.user_id);
        $("#check_ab_user_id").val(data.user_id);
        $("#s_u_mail").val(data.email);
        $("#s_u_name").val(data.user_name + " " + data.user_surname);
        $("#s_u_pid").val(data.id_code);
        $("#client_sex").val(data.gender_id);
        $("#client_birth_year").val(data.birth_year);
        $("#client_sex").trigger("chosen:updated");
      } else if (data.count == 0) {
        alert("მონაცემები ვერ მოიძებნა");
      }
    }
  });
});

$(document).on("click", "#search_mail", function () {
  param = new Object();
  param.act = "search_mail";
  param.s_u_mail = $("#s_u_mail").val();
  $.ajax({
    url: wsdl_url,
    data: param,
    success: function (data) {
      if (data.count == 1) {
        if (data.phone != "" && data.phone != "null" && data.phone != null) {
          $("#phone").val(data.phone);
          $("#check_ab_phone").val(data.phone);
        }
        $("#s_u_user_id").val(data.user_id);
        $("#check_ab_user_id").val(data.user_id);
        $("#s_u_mail").val(data.email);
        $("#s_u_name").val(data.user_name + " " + data.user_surname);
        $("#s_u_pid").val(data.id_code);
        $("#client_sex").val(data.gender_id);
        $("#client_birth_year").val(data.birth_year);
        $("#client_sex").trigger("chosen:updated");
      } else if (data.count == 0) {
        alert("მონაცემები ვერ მოიძებნა");
      }
    }
  });
});

$(document).on("click", "#search_s_u_pid", function (event) {
  var check_lenght = $("#s_u_pid").val();

  param = new Object();
  param.act = "search_s_u_pid";
  param.s_u_pid = $("#s_u_pid").val();
  if (check_lenght.length != 9) {
    alert("ძებნა ხორციელდება მხოლოდ საიდენტიფიკაციო ნომრით");
  } else {
    $.ajax({
      url: wsdl_url,
      data: param,
      success: function (data) {
        if (data.count == 1) {
          if (data.phone != "" && data.phone != "null" && data.phone != null) {
            $("#phone").val(data.phone);
            $("#check_ab_phone").val(data.phone);
          }
          $("#s_u_user_id").val(data.user_id);
          $("#check_ab_user_id").val(data.user_id);
          $("#s_u_mail").val(data.email);
          $("#s_u_name").val(data.user_name + " " + data.user_surname);
          $("#s_u_pid").val(data.id_code);
          $("#client_sex").val(data.gender_id);
          $("#client_birth_year").val(data.birth_year);
          $("#client_sex").trigger("chosen:updated");
        } else if (data.count == 0) {
          alert("მონაცემები ვერ მოიძებნა");
        }
      }
    });
  }
});

$(document).on("click", ".clickmetostart", function () {
  var ChatId = $(this).attr("chat_id");
  var ChatType = $(this).attr("data-source");

  if (
    $(this).attr("chat_id") == ChatId &&
    $(this).attr("data-source") == ChatType
  ) {
    // $("#dialog_active_chat").attr("aria-data", ChatId);

    param = new Object();
    param.act = "setChatSelect";
    param.active_chat_id = ChatId;
    param.chat_type = ChatType;

    $.ajax({
      url: "server-side/call/setChat.action.php",
      data: param,
      success: function (data) {
        console.log(data);
      }
    });
  } else {
    console.log(false);
  }
});


// OUTSIDE CAMPAIGN INSERT DATA


$(document).on('change', '#CampanySelect', function(){
  var campanyID = this.value;

  var Grid = $('#select_campaign').parent().parent()
  var GridName = Grid[0].id;

  var selectedPhones = [];
  var entityGrid = $("#"+GridName).data("kendoGrid");
  var rows = entityGrid.select();
  rows.each(function(index, row) {
      var selectedItem = entityGrid.dataItem(row);
      if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
      if(selectedItem.phone != ''){
          selectedPhones.push(selectedItem.phone);
      }
  });

  $("#notReleasedCall span").text(0)  
  $("#notReleasedCall span").css("color", "#000")
  $("#ReleasedCall span").text(selectedPhones.length)  
  
  $.ajax({
      url: "server-side/call/outgoing_company.action.php",
      type: "POST",
      data: "act=get_campaign_insert&numbers="+selectedPhones+"&campaign_id="+campanyID,
      dataType: "json",
      success: function (data) {

        $("#relocateUserCall").html(data.page);

        let numberCount   = selectedPhones.length;
        let cn            = selectedPhones.length;
        let usersLine     = $("#relocateTable .user-line");
        let userCount     = usersLine.length;

        if(campanyID > 0){
          $('#ReleasedCall').css('display','contents');
          $('#notReleasedCall').css('display','contents');
          $('#labelRelease').css('display', 'none');
        }else{
          $('#ReleasedCall').css('display','none');
          $('#notReleasedCall').css('display','none');
          $('#labelRelease').css('display', 'contents');
        }

     
            for(x = 0; x < numberCount; x++){
                usersLine.map((i, tr) => {
                    let percent   =   tr.childNodes[1];
                    let count     =   tr.childNodes[2];
                
                    if(cn != 0){
                      sum = parseInt(count.childNodes[0].value) + 1; // Plus Existed Number
                      count.childNodes[0].dataset.count = sum // Set Data attribute
                      count.childNodes[0].value = sum; // Set InnerText
                      cn -= 1 // Minus Counted
                    }

                });
              }

            usersLine.map((i, tr) => {
                let percent   =   tr.childNodes[1];
                let count     =   tr.childNodes[2];
                
                let p = parseInt(count.childNodes[0].value) / numberCount * 100
                percent.innerText = p.toFixed(2)+'%';

            });

          }

  });
  
})

$(document).on('keyup', '.release-input', function(){
  

  if($("#select_campaign").length != 0 ){

    var Grid = $('#select_campaign').parent().parent()
    var GridName = Grid[0].id;

    var selectedPhones = [];
    var entityGrid = $("#"+GridName).data("kendoGrid");
    var rows = entityGrid.select();
    rows.each(function(index, row) {
        var selectedItem = entityGrid.dataItem(row);
        if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
        if(selectedItem.phone != ''){
            selectedPhones.push(selectedItem.phone);
        }
    });

  }else{
    var selectedPhones = [12312312,12321234234];
  }



  let numberCount   = selectedPhones.length;
  let usersLine     = $("#relocateTable .user-line");


  var callSum = $('#ReleasedCall')[0].dataset.oldcount;
  let sum = 0;
  
  $("#relocateTable .user-line").map((i, tr) => {
      let count     =   tr.childNodes[2];
      let num = count.childNodes[0].value;

      if(!isNaN(parseInt(num))){
        sum += parseInt(num)
      }
  })

  if(sum > callSum){
    this.value = this.dataset.count
    alert("რაოდენობა შეზღუდულია")
    $("#notReleasedCall span").text(0);  
    $("#ReleasedCall span").text(callSum)
  }else{

    usersLine.map((i, tr) => {
      let percent   =   tr.childNodes[1];
      let count     =   tr.childNodes[2];

      count.childNodes[0].dataset.count = count.childNodes[0].value;

    });

    var notReleasedValue = callSum - sum;
    var ReleasedValue = callSum - notReleasedValue;
    $("#notReleasedCall span").text(notReleasedValue);
    $("#ReleasedCall span").text(ReleasedValue);

    usersLine.map((i, tr) => {
      let percent   =   tr.childNodes[1];
      let count     =   tr.childNodes[2];

      let c;
        
        if(count.childNodes[0].value == ''){
          c = 0;
        }else{
          c = count.childNodes[0].value
        }

        let p = parseInt(c) / numberCount * 100
        percent.innerText = p.toFixed(2)+'%';

    });
  }
  
  if($("#notReleasedCall span").text() > 0){
    $("#notReleasedCall span").css("color", "#ff0000")
  }else{
    $("#notReleasedCall span").css("color", "#000")
  }



  console.log(callSum, sum);

});

$(document).on('keypress', '.release-input', function(e){
  let allow_char = [48,49,50,51,52,53,54,55,56,57];
  if(allow_char.indexOf(e.which) === -1 ){
    return false;
    
  }
});
$(document).on('click', '#select_campaign', function(){

  var Grid = $('#select_campaign').parent().parent()
  var GridName = Grid[0].id;


  var selectedPhones = [];
  var entityGrid = $("#"+GridName).data("kendoGrid");
  var rows = entityGrid.select();
  rows.each(function(index, row) {
      var selectedItem = entityGrid.dataItem(row);
      if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
      if(selectedItem.phone != ''){
          selectedPhones.push(selectedItem.phone);
      }
  });
  
  if(selectedPhones.length === 0){
      alert('ნომრების სია ცარიელია. მონიშნეთ ნომრები');
  }else{
      $.ajax({
      url: "server-side/call/outgoing_company.action.php",
      type: "POST",
      data: "act=get_campaign_select&numbers="+selectedPhones,
      dataType: "json",
      success: function (data) {
          $("#campaign-select-form").html(data.page);
          var buttons = {
            save: {
              text: "შენახვა",
              id: "save-Released"
            },
            cancel: {
                  text: "დახურვა",
                  id: "cancel-dialog",
                  click: function () {
                      $(this).dialog("close");
                  }
              }
          };

          $("#CampanySelect").chosen();
          $(".chosen-drop").css("position", "inhrit")
          
          
          GetDialog("campaign-select-form", 700, "auto", buttons);

          $("#campaign-select-form, .campaign-select-form-class").css("overflow", "visible")
      }
  });
}
     
  });    


$(document).on("click", "#save-Released", function(){

  var Grid = $('#select_campaign').parent().parent()
  var GridName = Grid[0].id;


  var selectedPhones = [];
  var entityGrid = $("#"+GridName).data("kendoGrid");
  var rows = entityGrid.select();
  rows.each(function(index, row) {
      var selectedItem = entityGrid.dataItem(row);
      if(isNaN(parseInt(Number(selectedItem.phone)))) selectedItem.phone = '';
      if(selectedItem.phone != ''){
          selectedPhones.push(selectedItem.phone);
      }
  });

  var CampanySelect = $("#CampanySelect").val();
  let usersLine     = $("#relocateTable .user-line");
  var operators = [];

  usersLine.map((i, tr) => {
    let count     =   tr.childNodes[2];
    
    operators.push( { id: tr.dataset.id, count: count.childNodes[0].dataset.count });

  });
  
  var param = new Object();
  param.act       = "insert_into_base";
  param.phones    = selectedPhones;
  param.operators = operators;
  param.campany   = CampanySelect;


  if(parseInt($('#notReleasedCall span').text()) != 0){

    alert("დარჩენილია "+$('#notReleasedCall span').text()+" გასანაწილებელი ნომერი")

  }else{

    $.ajax({
      url: "server-side/call/outgoing_company.action.php",
      type: "POST",
      data: param,
      success: function () {
        $("#campaign-select-form").dialog('close')
      }

    });
  }

})






// CRM DIALOG

$(document).on('change', '#statusi--136--8', function(){

  if($(this).val() == 1 || $(this).val() == 2){

    /* $.ajax({
      url: Comunications_URL,
      data: {
        act: "get_crm_form"
      },
      success: function(data){

        $('#crm-dialog').html(data.page);
        $('#crm-0').chosen();

        var buttons = {
          save: {
            text: "შენახვა",
            id: "add-crm"
          },
          cancel: {
            text: "დახურვა",
            id: "cancel-dialog-answer",
            click: function () {
              $(this).dialog("close");
              $(this).html("");
              $('#statusi--136--8').val(3).trigger("chosen:updated");
            }
          }
        };

        GetDialog("crm-dialog", 427, "auto", buttons, "center");
        $('#crm-dialog, .crm-dialog-class').css("overflow", "visible");

      }
    }) */

    var crm_status    = 261;
    var st_id         = $(this).val();
    if(st_id == 2){
      crm_status = 262;
    }
    var crm_comment   = '';
    var inc_hidden_id = $('#hidden_incomming_call_id').val();
    var crm_phone     = $('#crm_phone').val();
    var dialogType    = $("#dialogType").val();
    var phone         = $("#telefoni___183--124--1").val();
    $.ajax({
      url: Comunications_URL,
      data: {
        act:        'insert_in_crm',
        status:     crm_status,
        comment:    crm_comment,
        inc_id:     inc_hidden_id,
        phone:      crm_phone,
        dialogType: dialogType,
        phone:      phone
      },
      success: function(data){
        if(data.status == 1){
            alert(data.message);
            $('#crm-dialog').dialog('close');
        }else{
            alert(data.message);
            $('#crm-dialog').dialog('close');
        }

      }
    })
  }
})
// $(document).on('change', '#crm-0', function(){

//   var parent_id = $(this).val();

//   if(parent_id != 0){
//     $.ajax({
//       url: Comunications_URL,
//       data: {
//         act: 'get_crm_sub',
//         parent_id: parent_id
//       },
//       success: function(data){

//         $("#crm-1").html(data.result);
//         $("#crm-1").trigger("chosen:updated");

//       }
//     })
//   }

// })


$(document).on('click', '#add-crm', function(){

  var crm_status    = $('#crm-0').val();
  var crm_comment   = $('#crm-comment').val();
  var inc_hidden_id = $('#hidden_incomming_call_id').val();
  var crm_phone     = $('#crm_phone').val();
  var dialogType    = $("#dialogType").val();
  var phone         = $("#telefoni___183--124--1").val();
  $.ajax({
    url: Comunications_URL,
    data: {
      act:        'insert_in_crm',
      status:     crm_status,
      comment:    crm_comment,
      inc_id:     inc_hidden_id,
      phone:      crm_phone,
      dialogType: dialogType,
      phone:      phone
    },
    success: function(data){
       if(data.status == 1){
          alert(data.message);
          $('#crm-dialog').dialog('close');
       }else{
          alert(data.message);
          $('#crm-dialog').dialog('close');
       }

    }
  })

})
