<html>
<head>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<style>
		
		.fix-width {
			/* min-width: 974px; */
			overflow-x: auto;
		}
	
		table.display {
			table-layout: auto;
		}
		.tree, .tree ul {
            margin:0;
            padding:0;
            list-style:none
        }
        .tree ul {
            margin-left:1em;
            position:relative
        }
        .tree ul ul {
            margin-left:.5em
        }
        .tree ul:before {
            content:"";
            display:block;
            width:0;
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            border-left:1px solid
        }
        .tree li {
            margin:0;
            padding:0 1em;
            line-height:19px;
            color:#369;
            font-weight:700;
            font-size:13px;
            position:relative;
            float: none!important;
        }
        .tree ul li:before {
            content:"";
            display:block;
            width:10px;
            height:0;
            border-top:1px solid;
            margin-top:-1px;
            position:absolute;
            top:1em;
            left:0
        }
        .tree ul li:last-child:before {
            background:#fff;
            height:auto;
            top:1em;
            bottom:0
        }
        .indicator {
            margin-right:5px;
        }
        .tree li a {
            text-decoration: none;
            color:#369;
        }
        .tree li button, .tree li button:active, .tree li button:focus {
            text-decoration: none;
            color:#369;
            border:none;
            background:transparent;
            margin:0px 0px 0px 0px;
            padding:0px 0px 0px 0px;
            outline: 0;
        }
        .cat_img{
            width: 12px;
            margin-right: 7px;
            cursor: pointer;
            border: none!important;
            padding: unset!important;
        }
        .cat_img:hover {
            color: red;
        }
        i{
            cursor: pointer;
        }
	</style>
    <script type="text/javascript">
        var aJaxURL	= "server-side/view/mepa_projects.action.php";		//server side folder url
        var tName	= "example";													//table name
        var dialog  = "add-edit-form";
        var fName	= "add-edit-form";												//form name
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
		function LoadKendoTable(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var gridName = 				    'mepa_projects';
			var actions = 				    '';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    12;
			var columnsSQL = 				["id:string","department:string","project:string","subproject:string","sms_sent:string","sms_text:string","work_stat:string","user:string","operator:string","mail_sent:string","mail:string","mail_text:string"];
			var columnGeoNames = 		    ["ID","უწყება","პროექტი","ქვე-პროექტი","დავალების ფორმირება","პასუხისმგებელი პირი","ფორმირდება ოპერატორზე","sms გაგზავნა","sms ტექსტი","იგზავნება ელფოსტა","ელფოსტის მისამართი","ელფოსტის ტექსტი",];

			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
        }
        function LoadKendoMainDocs(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var gridName = 				    'main_docs';
			var actions = 				    '';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    7;
			var columnsSQL = 				["id:string","docs_project_id:string","docs:string","link:string","date:date","dep:string","project:string"];
			var columnGeoNames = 		    ["ID","PID","დასახელება","ლინკი","თარიღი","უწყება","პროექტი"];

			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_main_docs',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
        }
        function LoadKendoMainContact(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var gridName = 				    'main_contacts';
			var actions = 				    '';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    8;
			var columnsSQL = 				["id:string","docs_project_id:string","firstlast:string","phone:string","email:date","pos:string","dep:string","project:string"];
			var columnGeoNames = 		    ["ID","PID","სახელი/გვარი","ტელეფონი","ელფოსტა","თანამდებობა","უწყება","პროექტი"];

			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_main_contacts',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
		}
        function LoadKendoDocs(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var gridName = 				    'kendo_docs';
			var actions = 				    '<button class="outgoing_button add_doc" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button delete_doc" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    5;
			var columnsSQL = 				["id:string","name:string","date:date","link:string","act:string"];
			var columnGeoNames = 		    ["ID","დასახელება","თარიღი","ლინკი","ქმედება"];

			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_docs_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
		}
        function LoadKendoContacts(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var gridName = 				    'kendo_contacts';
			var actions = 				    '<button class="outgoing_button add_contact" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button delete_contact" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    5;
			var columnsSQL = 				["id:string","name:string","pos:string","phone:string","email:string"];
			var columnGeoNames = 		    ["ID","სახელი/გვარი","თანამდებობა","ტელეფონი","E-mail"];

			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_contacts_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
        }
        function LoadKendoInputs(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'fieldset_inputs';
        var actions = 				    '<button class="outgoing_button button_add_input" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_input" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    4;
        var columnsSQL = 				["id:string","input:string","type:string","position:number"];
        var columnGeoNames = 		    ["ID","ველი","ტიპი","პოზიცია"];

        var showOperatorsByColumns =    [0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_input_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        $(document).on("click",".button_add_input",function(){
            var cat_id = $("#sub_category_1_select").val();
            $.ajax({
                url: aJaxURL,
                type: "POST",
                data: "act=input_editor&cat_id="+cat_id,
                dataType: "json",
                success: function (data) {
                    $("#input_editor").html(data.page);
                    LoadDialog('input_editor');
                }
            });
        });
        function geo_to_latin (str) {
    
            var ru = {
                'ა': 'a', 'ბ': 'b', 'გ': 'g', 'დ': 'd', 'ე': 'e', 'ვ': 'v', 'ზ': 'z', 
                'თ': 't', 'ი': 'i', 'კ': 'k', 'ლ': 'l','მ': 'm', 'ნ': 'n', 'ო': 'o',
                'პ': 'p', 'რ': 'r', 'ს': 's', 'ტ': 't', 'უ': 'u', 'ფ': 'f', 'ქ': 'q', 
                'ღ': 'gh','ყ': 'y', 'შ': 'sh', 'ჩ': 'ch', 'ც': 'c', 'ძ': 'dz', 'წ': 'w',
                'ჭ': 'ch', 'ხ': 'x', 'ჯ': 'j', 'ჰ': 'h', 'ჟ': 'zsh', ' ': '_','.':'_','/':'_','*':'_','=':'_','+':'_'
            }, n_str = [];
            for ( var i = 0; i < str.length; ++i ) {
                n_str.push(
                        ru[ str[i] ]
                    || ru[ str[i].toLowerCase() ] == undefined && str[i]
                    || ru[ str[i].toLowerCase() ].replace(/^(.)/, function ( match ) { return match.toUpperCase() })
                );
            }
            return n_str.join('');
        }
        $(document).on("click",".add_contact",function(){
            var cat_id = $("#sub_category_1_select").val();
            $.ajax({
				url: aJaxURL,
				data: 'act=add_contact&id='+cat_id,
				success: function(data) {
                    $("#add_contacts").html(data.page);
                    LoadDialog("add_contacts");
				}
			});
            
        });
        $(document).on("click",".add_doc",function(){
            var cat_id = $("#sub_category_1_select").val();
            $.ajax({
				url: aJaxURL,
				data: 'act=add_docs&id='+cat_id,
				success: function(data) {
                    $("#add_docs").html(data.page);
                    LoadDialog("add_docs");
				}
			});
            
        });
        $(document).on("click",".delete_doc",function(){
            var grid = $("#kendo_docs").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());
            $.ajax({
				url: aJaxURL,
				data: 'act=disable&type=docs&id='+dItem.id,
				success: function(data) {
                    $("#kendo_docs").data("kendoGrid").dataSource.read();
				}
			});
            
        });
        $(document).on("click",".delete_contact",function(){
            var grid = $("#kendo_contacts").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());
            $.ajax({
				url: aJaxURL,
				data: 'act=disable&type=contacts&id='+dItem.id,
				success: function(data) {
                    $("#kendo_contacts").data("kendoGrid").dataSource.read();
				}
			});
            
        });
        $(document).on("click",".button_delete_input",function(){
            var grid = $("#fieldset_inputs").data("kendoGrid");
            var dItem = grid.dataItem(grid.select());
            $.ajax({
				url: aJaxURL,
				data: 'act=disable&type=inputs&id='+dItem.id,
				success: function(data) {
                    $("#fieldset_inputs").data("kendoGrid").dataSource.read();
				}
			});
            
        });
        $(document).on('keyup','#input_name',function(){
            $('#input_key').val(geo_to_latin($('#input_name').val()));
        });
        function LoadDialog(fName){
            if(fName == 'add-edit-form'){
                GetDialog(fName, 600, "auto", '', 'center top');
                $('#parent_id,#client_id').chosen({ search_contains: true });
                $('#add-edit-form, .add-edit-form-class').css('overflow','visible');
                // $("#add-edit-form select").chosen({ search_contains: true });
            }
            else if(fName == 'add_docs'){
                var buttons ={
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-fieldset",
                                click: function () {

                                    var param 	            = new Object();
                                    param.act		        = "save_document";
                                    param.name              = $("#doc_name").val();
                                    param.link              = $("#doc_link").val();
                                    param.file_original     = $("#file_orig").val();
                                    param.file_new          = $("#file").val();
                                    param.cat_id            = $("#sub_category_1_select").val();

                                    var good = 0;
                                    if(param.name == ''){
                                        $("#doc_name").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(param.link == ''){
                                        $("#doc_link").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(param.file_original == '' || param.file_new == ''){
                                        $("#doc_file").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(good == 0){
                                        $.ajax({
                                            url: aJaxURL,
                                            type: "POST",
                                            data: param,
                                            dataType: "json",
                                            success: function (data) {
                                                if(data.responce == 'suc'){
                                                    $("#add_docs").dialog("close");
                                                    $("#kendo_docs").data("kendoGrid").dataSource.read();
                                                }
                                            }
                                        });
                                    }
                                    
                                }
                            }

                        };
                GetDialog(fName, 600, "auto", buttons, 'center top');
                $('#add_docs, .add_docs-class').css('overflow','visible');
            }
            else if(fName == 'add_contacts'){
                var buttons ={
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-fieldset",
                                click: function () {

                                    var param 	         = new Object();
                                    param.act		     = "save_contact";
                                    param.firstname      = $("#firstname").val();
                                    param.lastname       = $("#lastname").val();
                                    param.contact_pos    = $("#contact_pos").val();
                                    param.phone          = $("#phone").val();
                                    param.contact_email  = $("#contact_email").val();
                                    param.cat_id         = $("#sub_category_1_select").val();
                                    param.contact_id     = $("#contact_id").val();


                                    var good = 0;
                                    if(param.firstname == ''){
                                        $("#firstname").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(param.lastname == ''){
                                        $("#lastname").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(param.contact_pos == ''){
                                        $("#contact_pos").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(param.phone == ''){
                                        $("#phone").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(param.contact_email == ''){
                                        $("#contact_email").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(!isEmail(param.contact_email)){
                                        alert('არასწორი მეილის ფორმატი');
                                        $("#contact_email").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(!$.isNumeric(param.phone)){
                                        alert('არასწორი ტელეფონის ფორმატი');
                                        $("#phone").css("border",'1px solid red');
                                        good++;
                                    }
                                    if(good == 0){
                                        $.ajax({
                                            url: aJaxURL,
                                            type: "POST",
                                            data: param,
                                            dataType: "json",
                                            success: function (data) {
                                                if(data.responce == 'suc'){
                                                    $("#add_contacts").dialog("close");
                                                    $("#kendo_contacts").data("kendoGrid").dataSource.read();
                                                }
                                            }
                                        });
                                    }
                                    
                                }
                            }

                        };
                GetDialog(fName, 600, "auto", buttons, 'center top');
                $('#add_docs, .add_docs-class').css('overflow','visible');
            }
            else if(fName=='input_editor'){
            var buttons =
                        {
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-input",
                                click: function () {
                                    var input_name  = $("#input_name").val();
                                    var input_type  = $("#input_type").val();
                                    var input_pos   = $("#input_position").val();
                                    var nec         = $("#necessary").prop("checked");

                                    var itsOK = 0;
                                    if(input_name == '' && input_type != 11 && input_type != 12){
                                        $("#input_name").css('border','1px solid red');
                                        itsOK++;
                                    }
                                    if(itsOK == 0){
                                        
                                        var param 	            = new Object();
                                        param.act		        = "save_input";
                                        param.input_id          = $("#input_id").val();
                                        param.input_name        = $("#input_name").val();
                                        param.input_type        = $("#input_type").val();
                                        param.input_pos         = $("#input_position").val();
                                        param.key               = $("#input_key").val();
                                        param.nec               = $("#necessary").prop("checked");
                                        param.cat_id            = $("#sub_category_1_select").val();

                                        $.ajax({
                                            url: aJaxURL,
                                            type: "POST",
                                            data: param,
                                            dataType: "json",
                                            success: function (data) {
                                                
                                                if(data.response == 'suc'){
                                                    alert('ველი წარმატებით შენახულია!');
                                                    $("#fieldset_inputs").data("kendoGrid").dataSource.read();
                                                    $("#input_editor").dialog("close");
                                                    
                                                }
                                                else if(data.response == 'not_uniq'){
                                                    alert("ველი მსგავსი სახელით უკვე არსებობს!!!")
                                                }
                                                else{
                                                    alert('დაფიქსირდა შეცდომა!');
                                                }
                                            }
                                        });
                                    }
                                    
                                }
                            }

                        };
            GetDialog(fName, '500', "auto",buttons,'center top');

            //$("#input_type,#input_tab").chosen();
        }
        else if(fName=='ask-delete-category'){
            GetDialog(fName, 600, "auto", '', 'center top');
            $('#parent_id,#client_id').chosen({ search_contains: true });
            $('#add-edit-form, .add-edit-form-class').css('overflow','visible');
        }
            
        }
        $(document).on('change','#doc_file',function(e){
            //submit the form here
            //var name = $(".fileupchat").val();
            var file_data = $('#doc_file').prop('files')[0];
            var fileName = e.target.files[0].name;
            var fileNameN = Math.ceil(Math.random()*99999999999);
            var fileSize = e.target.files[0].size;
            var fileExt = $(this).val().split('.').pop().toLowerCase();
            var form_data = new FormData();
            console.log(file_data)
            form_data.append('act', 'project_docs');
            form_data.append('file', file_data);
            form_data.append('ext', fileExt);
            form_data.append('original', fileName);
            form_data.append('newName', fileNameN);
            var fileExtension = ['xls','xlsx','csv','doc','docx','pdf'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("დაუშვებელი ფორმატი!!!  გამოიყენეთ მხოლოდ: "+fileExtension.join(', '));
            $("#doc_file").val('');
            }
            else {
                if(fileSize>20971520) {
                    alert("შეცდომა! ფაილის ზომა 20MB-ზე მეტია!!!");
                    $("#doc_file").val('');
                }
                else{
                    $.ajax({
                        url: 'up2.php', // point to server-side PHP script
                        dataType: 'text',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (data) {
                            if(data == 'uploaded'){
                                $("#file_orig").val(fileName);
                                $("#file").val(fileNameN+'.'+fileExt);
                            }
                            else{
                                alert('ფაილი ვერ აიტვირთა');
                                $("#doc_file").val('');
                            }
                            
                        }
                    });
                }
            }
            
        });
		$(document).ready(function(){
			LoadKendoMainDocs();
		});
        $(document).on("click", "#add_button", function () {
            $.ajax({
                url: aJaxURL,
                data: 'act=get_add_page',
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            LoadDialog(dialog);
                            $('#'+dialog).html(data.page);
                            $("[data-select='chosen']").chosen({ search_contains: true });
							if ($("#send_sms").is(':checked')) {
								$("#sms_text").css("opacity","1");
								$("#sms_text").css("height","100px");
							}
							else{
								$("#sms_text").css("height","0px");
								$("#sms_text").css("opacity","0");
							}
							if ($("#task_form").is(':checked')) {
								$("#group_div").css("opacity","1");
                                $("#group_div").css("height","145px");
                                $("#group_div").css("overflow","visible");
							}
							else{
								$("#group_div").css("height","0px");
								$("#group_div").css("opacity","0");
							}
							if ($("#send_email").is(':checked')) {
								$("#email_text_wrapper").css("opacity","1");
								$("#email_text_wrapper").css("height","120px");
							}
							else{
								$("#email_text_wrapper").css("height","0px");
								$("#email_text_wrapper").css("opacity","0");
							}
							
                        }
                    }
                }
            });

        });
        $(document).on('click','.dialog_input_tabs li',function(){
            ///SWITCHING TABS HERE///
            var tab_id   = $(this).attr('data-id');
            $(this).each(function(item){
                $(".dialog_input_tabs li").removeAttr('aria-selected');
            });
            $(this).attr('aria-selected','true');
            $(".fields_cat").css('display','none');

            $("#field_"+tab_id).css('display','');

            var cat_id = $("#sub_category_1_select").val();
            var hidden = '&id='+cat_id;
            if(tab_id == 3){
                LoadKendoDocs(hidden);
            }
            else if(tab_id == 4){
                LoadKendoContacts(hidden);
            }
            else if(tab_id == 2){
                LoadKendoInputs(hidden);
            }
        });

        $(document).on('click','.dialog_input_tabs2 li',function(){
            ///SWITCHING TABS HERE///
            var tab_id   = $(this).attr('data-id');
            $(this).each(function(item){
                $(".dialog_input_tabs2 li").removeAttr('aria-selected');
            });
            $(this).attr('aria-selected','true');
            $(".dep_tabss").css('display','none');

            $("#dep_tab_"+tab_id).css('display','');

            var cat_id = $("#sub_category_1_select").val();
            var hidden = '&id='+cat_id;
            if(tab_id == 1){
                LoadKendoMainDocs();
            }
            else if(tab_id == 2){
                LoadKendoMainContact();
            }
            else if(tab_id == 3){
                $.ajax({
                    url: aJaxURL,
                    data: 'act=get_list',
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                $('#tree1').html(data.page);
                                $('#tree1').treed();
                                var param = "&kendo_list=1"
                                LoadKendoTable(param);
                            }
                        }
                    }
                });
            }
        });
        $(document).on("click", ".cat_edit", function () {
            $.ajax({
                url: aJaxURL,
                data: 'act=get_edit_page&id='+$(this).attr('my_id'),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            LoadDialog(dialog);
                            $('#'+dialog).html(data.page);
                            $("[data-select='chosen']").chosen({ search_contains: true });
							if ($("#send_sms").is(':checked')) {
								$("#sms_text").css("opacity","1");
								$("#sms_text").css("height","100px");
							}
							else{
								$("#sms_text").css("height","0px");
								$("#sms_text").css("opacity","0");
							}
							if ($("#task_form").is(':checked')) {
								$("#group_div").css("opacity","1");
                                $("#group_div").css("height","145px");
                                $("#group_div").css("overflow","visible");
							}
							else{
								$("#group_div").css("height","0px");
								$("#group_div").css("opacity","0");
							}
                            if ($("#send_email").is(':checked')) {
								$("#email_text_wrapper").css("opacity","1");
								$("#email_text_wrapper").css("height","120px");
							}
							else{
								$("#email_text_wrapper").css("height","0px");
								$("#email_text_wrapper").css("opacity","0");
							}
                        }
                    }
                }
            });
        });

		$.fn.extend({
        treed: function (o) {

            var openedClass = 'glyphicon-minus-sign';
            var closedClass = 'glyphicon-plus-sign';

            if (typeof o != 'undefined'){
                if (typeof o.openedClass != 'undefined'){
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined'){
                    closedClass = o.closedClass;
                }
            };

            //initialize each of the top levels
            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                branch.addClass('branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " + closedClass);
                        $(this).children().children().toggle();
                    }
                })
                branch.children().children().toggle();
            });
            //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function(){
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });
    $(document).on("click", ".toggle_all_categories", function() {

        //define variables
        let action = $(this).data("action");

        //toggle category list items
        if(action === "open") {
            $(".branch").find("li").css({display: "list-item"});
            $(".branch > i").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        } else if(action === "close") {
            $(".branch").find("li").css({display: "none"});
            $(".branch > i").removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
        }

    });
    $(document).on("click", "#show_sub_category_2_value", function () {

    // define variables for getting main category
    // index and retrieve sub category list
    var categoryId = $("#category_select").val();
    var self = this;

    getSubCategoryList(categoryId, function(subCategoryList) {

        // insert subcategory list in relevant select
        $('#sub_category_1_select').html(subCategoryList);

        //define main category value
        var subCategory = $("#sub_category_1_value").val();

        // if main category in not selected, that means
        // it is new category adding proccess, but if
        // main category input has some value, it means
        // going on editing proccess
        // get main category id
        let subCatValue = subCategory !== "" ? $("#sub_category_1_value").val() : false;
        let subCatIndex = subCatValue ? getOptionIndexByVal('#sub_category_1_select', subCatValue) : 0;

        if ($(self).is(':checked')) {

            $('#show_sub_category_1_value').prop("disabled",true);
            $('#sub_category_1_value').css('display','none');
            $('#sub_category_1_value').val('');
            $('#sub_category_1_select,#second_tr').css('display','');
            $('#sub_category_2_value').css({display: "block"});
            $("#cat_id").val("");
            document.getElementById('sub_category_1_select').selectedIndex = subCatIndex;

        } else {
            $('#show_sub_category_1_value').prop("disabled",false);
            $('#sub_category_1_value').css('display','');
            $('#sub_category_1_select,#second_tr').css('display','none');
            document.getElementById('sub_category_1_select').selectedIndex = 0;
            $('#sub_category_2_value').val('');
        }

    });

    });

    function getOptionIndexByVal(select, value) {

        var returnValue = 0;

        $(select).find("option").each(function(i) {

            var optionValue = $(this).text();

            if(value === optionValue) {
                returnValue = i;
            }

        });

        return returnValue;

    }
    function getSubCategoryList(categoryid, callback) {
        $.post(aJaxURL, {act: "get_sub_cat", category_id: categoryid}, result => {
            callback(result.page);
        });
    }

    $(document).on("change", "#category_select", function () {
        if($(this).val() == 0){
            $('#show_sub_category_2_value').prop("disabled",true);
        }else{
            $('#show_sub_category_2_value').prop("disabled",false);
            if($("#multilevel_deep").val() == 2){
                $('#show_sub_category_2_value').prop("disabled",true);
            }
        }
        $.ajax({
            url: aJaxURL,
            data: 'act=get_sub_cat&category_id='+$(this).val(),
            success: function(data) {
                if(typeof(data.error) != 'undefined'){
                    if(data.error != ''){
                        alert(data.error);
                    }else{
                        $('#sub_category_1_select').html(data.page);
                    }
                }
            }
        });
    });
    $(document).on("click", "#show_sub_category_1_value", function () {

        //define main category value
        var mainCategory = $("#category_value").val();

        // if main category in not selected, that means
        // it is new category adding proccess, but if
        // main category input has some value, it means
        // going on editing proccess
        // get main category id
        let mainCatValue = mainCategory !== "" ? $("#category_value").val() : false;
        let mainCatIndex = mainCatValue ? getOptionIndexByVal('#category_select', mainCatValue) : 0;

        if ($(this).is(':checked')) {
            $('#show_sub_category_2_value').prop("disabled",true);
            $('#category_value').css('display','none').val('');
            $('#category_select,#first_tr').css('display','');
            $('#sub_category_1_value').css({display: "block"});
            $('#sub_category_1_select').val(0);
            $("#cat_id").val("");
            document.getElementById('category_select').selectedIndex = mainCatIndex;

            if(!mainCatIndex) {
                $('#sub_category_1_select').css({display: "none"});
            } else {
                $('#show_sub_category_2_value').prop("disabled",false);
            }

        } else {
            $('#category_value').css('display','');
            $('#category_select,#first_tr').css('display','none');
            $("#sub_category_1_value").css({display: "none"});
            $("#sub_category_1_select").css({display: "none"});
            document.getElementById('category_select').selectedIndex = 0;
        }

    });
    $(document).on("click", "#send_sms", function(){
        if ($(this).is(':checked')) {
            $("#sms_text").css("opacity","1");
            $("#sms_text").css("height","100px");
            $("#sms_text_par").css("display","flex");
        }
        else{
            $("#sms_text").css("height","0px");
            $("#sms_text").css("opacity","0");
            $("#sms_text_par").css("display","none");
        }
    })

    $(document).on("click", "#task_form", function(){
        $(".chosen-container").css("width","173px");
        if ($(this).is(':checked')) {
            $("#group_div").css("opacity","1");
            $("#group_div").css("height","145px");
            $("#group_div").css("overflow","visible");
        }
        else{
            $("#group_div").css("height","0px");
            $("#group_div").css("opacity","0");
        }
    });

    $(document).on("click", "#send_email", function(){

        if ($("#send_email").is(':checked')) {
            $("#email_text_wrapper").css("opacity","1");
            $("#email_text_wrapper").css("height","120px");
        }
        else{
            $("#email_text_wrapper").css("height","0px");
            $("#email_text_wrapper").css("opacity","0");
        }
    });
    $(document).on("dblclick", "#main_contacts tr.k-state-selected", function () {
        var grid = $("#main_contacts").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_edit_contacts&id="+dItem.docs_project_id,
            dataType: "json",
            success: function (data) {
                LoadDialog(dialog);
                $('#'+dialog).html(data.page);
                $("[data-select='chosen']").chosen({ search_contains: true });
                $(".dialog_input_tabs li[data-id='4']").click();
                if ($("#send_sms").is(':checked')) {
                    $("#sms_text").css("opacity","1");
                    $("#sms_text").css("height","100px");
                }
                else{
                    $("#sms_text").css("height","0px");
                    $("#sms_text").css("opacity","0");
                }
                if ($("#task_form").is(':checked')) {
                    $("#group_div").css("opacity","1");
                    $("#group_div").css("height","145px");
                    $("#group_div").css("overflow","visible");
                }
                else{
                    $("#group_div").css("height","0px");
                    $("#group_div").css("opacity","0");
                }
                if ($("#send_email").is(':checked')) {
                    $("#email_text_wrapper").css("opacity","1");
                    $("#email_text_wrapper").css("height","120px");
                }
                else{
                    $("#email_text_wrapper").css("height","0px");
                    $("#email_text_wrapper").css("opacity","0");
                }
            }
        });
    });
    $(document).on("dblclick", "#main_docs tr.k-state-selected", function () {
        var grid = $("#main_docs").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_edit_docs&id="+dItem.docs_project_id,
            dataType: "json",
            success: function (data) {
                LoadDialog(dialog);
                $('#'+dialog).html(data.page);
                $("[data-select='chosen']").chosen({ search_contains: true });
                $(".dialog_input_tabs li[data-id='3']").click();
                if ($("#send_sms").is(':checked')) {
                    $("#sms_text").css("opacity","1");
                    $("#sms_text").css("height","100px");
                }
                else{
                    $("#sms_text").css("height","0px");
                    $("#sms_text").css("opacity","0");
                }
                if ($("#task_form").is(':checked')) {
                    $("#group_div").css("opacity","1");
                    $("#group_div").css("height","145px");
                    $("#group_div").css("overflow","visible");
                }
                else{
                    $("#group_div").css("height","0px");
                    $("#group_div").css("opacity","0");
                }
                if ($("#send_email").is(':checked')) {
                    $("#email_text_wrapper").css("opacity","1");
                    $("#email_text_wrapper").css("height","120px");
                }
                else{
                    $("#email_text_wrapper").css("height","0px");
                    $("#email_text_wrapper").css("opacity","0");
                }
            }
        });
    });
    $(document).on("dblclick", "#mepa_projects tr.k-state-selected", function () {
        var grid = $("#mepa_projects").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_edit_page&id="+dItem.id,
            dataType: "json",
            success: function (data) {
                LoadDialog(dialog);
                $('#'+dialog).html(data.page);
                $("[data-select='chosen']").chosen({ search_contains: true });
                if ($("#send_sms").is(':checked')) {
                    $("#sms_text").css("opacity","1");
                    $("#sms_text").css("height","100px");
                }
                else{
                    $("#sms_text").css("height","0px");
                    $("#sms_text").css("opacity","0");
                }
                if ($("#task_form").is(':checked')) {
                    $("#group_div").css("opacity","1");
                    $("#group_div").css("height","145px");
                    $("#group_div").css("overflow","visible");
                }
                else{
                    $("#group_div").css("height","0px");
                    $("#group_div").css("opacity","0");
                }
                if ($("#send_email").is(':checked')) {
                    $("#email_text_wrapper").css("opacity","1");
                    $("#email_text_wrapper").css("height","120px");
                }
                else{
                    $("#email_text_wrapper").css("height","0px");
                    $("#email_text_wrapper").css("opacity","0");
                }
            }
        });
    });
    $(document).on("dblclick", "#fieldset_inputs tr.k-state-selected", function () {
        var grid = $("#fieldset_inputs").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=input_editor&input_id="+dItem.id+"&cat_id="+$("#sub_category_1_select").val(),
            dataType: "json",
            success: function (data) {
                $("#input_editor").html(data.page);
                LoadDialog('input_editor');
            }
        });
    });
    $(document).on("dblclick", "#kendo_contacts tr.k-state-selected", function () {
        var grid = $("#kendo_contacts").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_contact&id="+dItem.id,
            dataType: "json",
            success: function (data) {
                LoadDialog("add_contacts");
                $('#add_contacts').html(data.page);
            }
        });
    });
    $(document).on("click", "#save-dialog", function () {

        var subCatecoryIsChecked = $("#show_sub_category_1_value").is(":checked");
        var endCatecoryIsChecked = $("#show_sub_category_2_value").is(":checked");

        param 			           = new Object();

        param.act		           = "save_category";
        param.id		           = $("#cat_id").val();
        param.category_value	   = $("#category_value").val();
        param.category_select	   = $("#category_select").val();
        param.sub_category_1_value = $('#sub_category_1_value').val();
        param.sub_category_1_select= $('#sub_category_1_select').val();
        param.sub_category_2_value = $('#sub_category_2_value').val();

        ///
            param.send_sms 			= $("#send_sms:checked").val();
            param.sms_text 			= $("#sms_text").val();
            param.task_form 		= $("#task_form:checked").val();
            param.department 		= $("#department").val();
            param.user 				= $("#user").val();
            param.operator_task 	= $("#operator_task:checked").val();
            param.task_type         = $("#tasktype").val();
            param.send_email 	    = $("#send_email:checked").val();
            param.email_address     = $("#email_address").val();
            param.email_text        = $("#email_address").val();

        ///
        param.region_selector       = $("#region_selector").prop("checked");
        if(!subCatecoryIsChecked && (param.category_value === "" && param.category_select === "0")) {
            alert("გთხოვთ დააფიქსიროთ კატეგორია");
        } else if(subCatecoryIsChecked && (param.category_value === "" && param.category_select === "0")) { // && (param.sub_category_1_value === "" && param.sub_category_1_select ==="0")) {
            alert("გთხოვთ დააფიქსიროთ კატეგორია ");
        } else if(subCatecoryIsChecked && (param.sub_category_1_value === "" && param.sub_category_1_select === "0")) {
            alert("გთხოვთ დააფიქსიროთ ქვე კატეგორია");
        } else if(subCatecoryIsChecked && (param.sub_category_1_value === "" && param.sub_category_1_select === "0")) { // && (param.sub_category_1_value === "" && param.sub_category_1_select ==="0")) {
            alert("გთხოვთ დააფიქსიროთ ქვე კატეგორია ");
        } 
        // else if(endCatecoryIsChecked && (param.sub_category_2_value === "")) {
        //     alert("გთხოვთ დააფიქსიროთ საიტის ტიპი");
        // } 
        else if(param.task_form && !param.operator_task && param.department == '0' && param.user == '0') {
            alert("მიუთითეთ დავალების ადრესატი !");
        }
        else if(param.send_sms && param.sms_text == '') {
            alert("მიუთითეთ sms ტექსტი !");
        }
        else if(param.user != '0' && param.operator_task ) {
            alert("ორ მომხმარებელს ვერ დაუფორმირდება ერთი დავალება !");
        }
        else {

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            $.ajax({
                                url: aJaxURL,
                                data: 'act=get_list',
                                success: function(data) {
                                    if(typeof(data.error) != 'undefined'){
                                        if(data.error != ''){
                                            alert(data.error);
                                        }else{
                                            $('#tree1').html(data.page);
                                            $('#tree1').treed();
                                        }
                                    }
                                }
                            });
                            $("#mepa_projects").data("kendoGrid").dataSource.read();
                            CloseDialog(dialog);
                        }
                    }
                }
            });

        }
    });
    $(document).on("click", ".cat_delete", function () {

        var objId = $(this).attr("my_id");

        var buttons2 = {
            "save": {
                text: "კი",
                id: "confirm-delete-category",
                click: function () {

                    $('#ask-delete-category').dialog("close");
                    deleteCategory(objId);

                }
            },"reject-delete-category": {
                text: "არა",
                id: "no-cc",
                click: function () {
                    $('#ask-delete-category').dialog("close");
                }
            }
        };

        GetDialog("ask-delete-category","300","auto",buttons2);

    });
    function deleteCategory(objid) {

        $.ajax({
            url: aJaxURL,
            data: 'act=disable&id='+objid,
            success: function(data) {
                if(typeof(data.error) != 'undefined'){
                    if(data.error != ''){
                        alert(data.error);
                    }else{
                        $.ajax({
                            url: aJaxURL,
                            data: 'act=get_list',
                            success: function(data) {
                                if(typeof(data.error) != 'undefined'){
                                    if(data.error != ''){
                                        alert(data.error);
                                    }else{
                                        $('#tree1').html(data.page);
                                        $('#tree1').treed();
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

    }
    </script>
    <style type="text/css">
        #table_right_menu{
            position: relative;
            float: right;
            width: 70px;
            top: 42px;
            z-index: 99;
            border: 1px solid #E6E6E6;
            padding: 4px;
        }

        .ColVis, .dataTable_buttons{
            z-index: 100;
        }
        .callapp_head{
            font-family: pvn;
            font-weight: bold;
            font-size: 20px;
            color: #2681DC;
        }
        #example_log tbody .dataTables_empty{
            text-align: center !important;
        }
        #example_log tbody td:last-child{
            text-align: left !important;
        }
    </style>
</head>

<body>
<div id="tabs">
    <ul class="dialog_input_tabs2 dep_tabs" style="display: flex;">
        <li aria-selected="true" data-id="1">დოკუმენტაცია</li>
        <li data-id="2">კონტაქტები</li>
        <li style="right: 21px;;position: absolute;" data-id="3"><img style="margin-bottom: 3px;" src="media/images/icons/options.png"> პარამეტრები</li>
    </ul>
	
    <div class="row_data" style="display: flex;">
        <div class="dep_tabss" id="dep_tab_1">
            <div class="col-12">
                <div id="main_docs"></div>
            </div>
        </div>
        <div class="dep_tabss" id="dep_tab_2">
            <div class="col-12">
                <div id="main_contacts"></div>
            </div>
        </div>
        <div style="display:none;" class="dep_tabss" id="dep_tab_3">
            <div id="button_area">
                <button id="add_button" data-button="jquery-ui-button">დამატება</button>
                <button class="toggle_all_categories" data-action="open">ყველას გახსნა</button>
                <button class="toggle_all_categories" data-action="close">ყველას დახურვა</button>
            </div>
            <div class="col-10">
                <div id="mepa_projects"></div>
            </div>
            <div class="col-2">
                <ul id="tree1"></ul>
            </div>
        </div>
		
	</div>
	
	
</div>

<div id="add-edit-form" class="form-dialog" title="ზარების კატეგორიები">
    <!-- aJax -->
</div>
<div id="add_docs" class="form-dialog" title="დოკუმენტაცია">
    <!-- aJax -->
</div>
<div id="add_contacts" class="form-dialog" title="კონტაქტები">
    <!-- aJax -->
</div>
<div id="input_editor" class="form-dialog" title="ველები"></div>
<div id="ask-delete-category" class="form-dialog" title="&nbsp;">
    <span class="ask-delete-text">დარწმუნებული ბრძანდებით რომ გსურთ ამ კატეგორიის წაშლა?</span>
</div>
</body>
</html>


