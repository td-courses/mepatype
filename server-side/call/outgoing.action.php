<?php
include('../../includes/classes/class.Mysqli.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
global $db;
$db = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
    case 'get_add_page':
        $page		= GetPage();
        $data		= array('page'	=> $page);

    break;
    case 'get_columns':
    
        $columnCount = 		$_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] = 	$_REQUEST['names'];
        $operators[] = 		$_REQUEST['operators'];
        $selectors[] = 		$_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }
        
        
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
            {
                continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if($columns[$j] == 'id')
                {
                    $width = "5%";
                }
                else if($columns[$j] == 'shin_status'){
                    $width = "12%";
                }
                else{
                    $width = 'auto';
                }
                if($columns[$j] == 'inc_id')
                {
                    $hidden = true;
                }
                else if($columns[$j] == 'id' OR $columns[$j] == 'req_id'){
                    $hidden = true;
                }
                else{
                    $hidden = false;
                }
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        
        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
        
        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for($j=0;$j<$columnCount;$j++)
        {
            if($types[$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else if($types[$j] == 'number'){

                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        
        $filtArr = array('fields'=>$new_data);
        
        
        
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);

        
        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
        
        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';
        
    break;
    case 'get_list' :
       
        // FOR KENDO GRID
        $id          =      $_REQUEST['hidden'];
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'], true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        
        // FOR REQUESTS
        $out_type       = $_REQUEST['filter_all_calls'];
		$operator	    = $_REQUEST['filter_operator'];
		$start        	= $_REQUEST['start'] . ' 00:00:00';
        $end         	= $_REQUEST['end'] . ' 23:59:59';
        $parent         = $_REQUEST['parent'];
        $child          = $_REQUEST['child'];
		$user	      	= $_SESSION['USERID'];
        
        if($out_type != ''){
            $filt = " AND outgoing_campaign_type.id = '$out_type'";
        }else{
            $filt = '';
        }
        
        if($operator != ''){
            $filt1 = " AND user_info.user_id = '$operator'";
        }else{
            $filt1 = '';
        }

        if($parent != ''){
           $parent = " AND outgoing_campaign_request_base.tree_id='$parent'";
        }else{
            $parent = "";
        }
        if($child != ''){
            $child = "AND outgoing_campaign_request_base.sub_tree_id='$child'";
        }else{
            $child = "";
        }

            $db->setQuery(" SELECT      IF(asterisk_call_log.call_status_id IN( 9, 12 ), '', incomming_call.id) AS id,
                                        incomming_call.date,
                                        CASE
                                            WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
                                            WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.name
                                            WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN mail.sender_name
                                            WHEN NOT ISNULL(incomming_call.video_call_id)  THEN IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value))
                                        END AS `mom_auth`,
                                        IF(ISNULL(user_info2.name),user_info3.`name`,user_info2.name) AS `name`,
                                        uwyeba_dat.name,
                                        project_dat.name,
                                        CASE
                                            WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(IFNULL(IFNULL(user_info1.`name`,user_info.`name`), ''), '(',asterisk_extension.number, ')'),user_info1.`name`),user_info.name)
                                            WHEN NOT ISNULL(incomming_call.chat_id)  THEN user_info1.name
                                            WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN user_info1.name
                                            WHEN NOT ISNULL(incomming_call.video_call_id) THEN user_info1.name
                                        END AS `user_name`,
                                        
                                        CASE
                                            WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN incomming_call.phone
                                            WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.phone
                                            WHEN NOT ISNULL(incomming_call.fb_chat_id)  THEN fb_chat.sender_name
                                            WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN proc_phone.value
                                            WHEN NOT ISNULL(incomming_call.viber_chat_id)  THEN viber_chat.sender_name
                                            WHEN NOT ISNULL(incomming_call.video_call_id) THEN incomming_call.phone
                                        END AS `phone`,
                                        CONCAT(TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.talk_time),'%i:%s'), ' (', TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.wait_time),'%i:%s'), ')', 
									CASE
										WHEN `asterisk_call_log`.`call_status_id` IN (13) THEN
										IF
											(
												isnull(incomming_call.`user_id` ),
											
										CONCAT(
											'<span class=\'play_audio_complate\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span>',
											'<a target=_blank href=http://172.16.0.80:8000/',CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span></a>' ),
											IF
												(
													incomming_call.transfer = 1,

										CONCAT('<button class=\'download5\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '><span style=\"vertical-align: -1px; font-size: 11px\">გადართული  </span></button>' ),
												
										CONCAT('<span class=\'play_audio_complate\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span>',
											'<a target=_blank href=http://172.16.0.80:8000/',CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ),' download=\'FILE\'><span class=\'download_audio_complate\' str=', CONCAT( DATE_FORMAT( FROM_UNIXTIME( asterisk_call_log.call_datetime ), '%Y/%m/%d/' ), asterisk_call_record.NAME, '.', asterisk_call_record.format ), '>
											</span></a>' )

										))
                                        ELSE CONCAT(
											'<span style=\'filter: grayscale(100%);\' class=play_audio_complate_disabled>
											</span>',
											'<span style=\'filter: grayscale(100%);\' class=download_audio_complate_disabled>
											</span>' )
                                    END
									) AS `file`,
                                    CASE
									-- CHAT
                                        WHEN ISNULL(`asterisk_call_log`.`call_status_id`)
                                            THEN concat(_utf8 '<button style=\'background-color: #ff9800;\' class=\'download2\' ><img src=\"media/images/icons/comunication/ringing.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">საუბრობს</span></button>')
                                        WHEN `asterisk_call_log`.`call_status_id` = 12 OR ISNULL(`asterisk_call_log`.`call_status_id`)
                                            THEN concat(_utf8 '<button class=\'download2\' ><img src=\"media/images/icons/comunication/busy_out.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px\">უპასუხო</span></button>')
                                        WHEN `asterisk_call_log`.`call_status_id` = 13
                                            THEN concat(_utf8 '<button class=\'download\' ><img src=\"media/images/icons/comunication/phone.png\" height=\"20\" width=\"20\"><span style=\"font-family: BPG; vertical-align: -1px; font-size: 11px;\">ნაპასუხები</span></button>')
                                    END AS `status`,
                                    CASE 
                                            WHEN genderID.`int_value` = 1 THEN 'მდედრობითი'
                                            WHEN genderID.`int_value` = 2 THEN 'მამრობითი'
                                        END AS `gender`

                            FROM        incomming_call
                            LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                            LEFT JOIN   asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
                            LEFT JOIN   user_info ON user_info.user_id = asterisk_call_log.user_id
                            LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                            LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                            LEFT JOIN   info_category ON info_category.id = incomming_call.cat_1
                            LEFT JOIN   users ON users.id = asterisk_call_log.user_id
                            LEFT JOIN	asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                            LEFT JOIN   user_info AS user_info2 ON  user_info2.user_id = users.id

                            
                            LEFT JOIN   users AS users1 ON users1.id = incomming_call.user_id
                            LEFT JOIN   user_info AS user_info3 ON  user_info2.user_id = users1.id


                            LEFT JOIN   `chat` ON `chat`.id = incomming_call.chat_id
                            LEFT JOIN   fb_chat ON fb_chat.id = incomming_call.fb_chat_id
                            LEFT JOIN   mail ON mail.id = incomming_call.mail_chat_id
                            LEFT JOIN	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
                            LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = incomming_call.id AND proc_name.processing_setting_detail_id = 127
                            LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = incomming_call.id AND proc_lastname.processing_setting_detail_id = 128
                            LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = incomming_call.id AND proc_phone.processing_setting_detail_id = 124
                            LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
                            LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
                            LEFT JOIN   compaign_request_processing AS `proc_phone2` ON proc_phone2.base_id = outgoing_campaign_request_base.id AND proc_phone2.processing_setting_detail_id = 124
                            

                            LEFT JOIN   viber_chat ON viber_chat.id = incomming_call.viber_chat_id
                            LEFT JOIN   video_calls ON video_calls.id = incomming_call.video_call_id
                            LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = incomming_call.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
						    LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = incomming_call.id AND project.processing_setting_detail_id = 0 AND project.value = 2
                            LEFT JOIN 	info_category AS `uwyeba_dat` ON uwyeba_dat.id = uwyeba.int_value
						    LEFT JOIN 	info_category AS `project_dat` ON project_dat.id = project.int_value
                            LEFT JOIN incomming_request_processing AS genderID ON genderID.processing_setting_detail_id = 130 AND incomming_call.id = genderID.incomming_request_id

                            WHERE       asterisk_call_log.call_type_id = 2 AND asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start') AND UNIX_TIMESTAMP('$end') 
                            $filt1 
                            GROUP BY    incomming_call.id
                            ORDER BY    id DESC");
       
        $result = $db->getKendoList($columnCount,$cols);

        //$data = array('data'=>$arr,'total'=>$rowCount['result'][0]['cc']);
        //$data = var_dump($result);
        $data = $result;
	break;
    case 'get_compaign_types':
        $db->setQuery("	SELECT 	id,
								name
						FROM   	outgoing_campaign_type
						WHERE   `actived` = 1");
		$data = $db->getResultArray();
    break;
    case 'get_tabs':
        $db->setQuery(" SELECT  id,
                                name
                        FROM tree
                        WHERE parent_id = 0 AND actived = 1 AND `type_id` = 1");
        $res = $db->getResultArray();
        
        $page = get_tabs($res['result']);
        
        $data		= array('page'	=> $page);
        break;
    case 'get_tabs_numbers':

        $db->setQuery(" SELECT 	tree.id AS 'tree_id',
                                COUNT(*) AS 'count'

                        FROM tree
                        LEFT JOIN outgoing_campaign_request_base ON outgoing_campaign_request_base.sub_tree_id = tree.id OR outgoing_campaign_request_base.tree_id = tree.id

                        WHERE outgoing_campaign_request_base.actived = 1

                        GROUP BY tree.id");
        $result = $db->getResultArray();
        $data['numbers'] = $result['result'];
        

        break;
	case 'disable_project':

		$projectID = $_REQUEST['projectID']; 

		$db->setQuery("	UPDATE `directory__projects`
						SET    `actived` = 0
						WHERE  `id` IN($projectID)");
    	$db->execQuery();


		$data = array('error' => 0, 'message' => 'removed');
	break;
    default:
        $error = 'Action is Null';
}

echo json_encode($data);




/**
 * FUNCTIONS
 */

function get_tabs($res){
    $datafull="";
    $data2='';
    $data='<div id="tab_0" class="main_tab" name="0"><ul>';
    foreach ($res as $arr) {
        if($arr['parent_id']==0){
            $data   .=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" >
                            <a>'.$arr['name'].'</a>
                            <span id="input_'.$arr['id'].'"><x>0</x></span>
                        </li>';
            $data2  .='</ul>
                            </div><div id="tab_'.$arr['id'].'" class="tab_'.$arr['id'].'" name="'.$arr['id'].'">
                        <ul>';
            $child_req = get_child_status($arr['id']);
            foreach($child_req AS $child_res){
                $data2.=get_child_tabs($child_res,$child_res['parent_id']);
            }
        }
    }
    $data.='</ul></div>';
    $data2.='</ul></div>';
    $datafull .=  $data;
    $datafull .=  $data2;
    return $datafull;
}


function get_child_tabs($arr,$id){
    $data='';
    if($arr['parent_id'] == $id && $arr['parent_id'] != 0 )
        $data.=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" id_select="'.$id.'"  class="child">
                        <a>'.$arr['name'].'</a><span id="input_'.$arr['id'].'"><x>0</x></span>
                </li>';
        
    return $data;
}

function get_child_status($id){
    global $db;
    $db->setQuery(" SELECT  id,
                            name,
                            parent_id
                    FROM `tree`
                    WHERE actived=1 AND parent_id = '$id' AND type_id='1'");
    $res = $db->getResultArray();
    return $res['result'];
}




?>