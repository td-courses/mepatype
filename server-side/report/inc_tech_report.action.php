<?php

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

//----------------------------- ცვლადი

$agent    = $_REQUEST['agent'];
$queue    = $_REQUEST['queuet'];
$start  = $_REQUEST['start_time'];
$end     = $_REQUEST['end_time'];
$source = $_REQUEST['source'];
$day = (strtotime($end)) -  (strtotime($start));
$day_format = round(($day / (60 * 60 * 24)) + 1);
$start_date = $start . ' 00:00';
$end_date = $end . ' 23:59';

// ----------------------------------

if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "call_filter") {

    //------------------------------------------------QUERY FUNCTIONS---------------------------------------

    //get summary of all calls
    function get_sum_calls($start_date, $end_date, $queue, $agent, $source)
    {
        global $db;

        if ($source == 4) {
            $db->setQuery(" SELECT COUNT(*) AS all_calls
                            FROM   chat 
                            JOIN   incomming_call ON incomming_call.chat_id = chat.id
                            WHERE  chat.join_date BETWEEN '$start_date' AND '$end_date'
                            AND    chat.last_user_id IN($queue)");
        } elseif ($source == 7) {
            $db->setQuery(" SELECT COUNT(*) AS all_calls
                            FROM   mail 
                            JOIN   incomming_call ON incomming_call.mail_chat_id = mail.id
                            WHERE  mail.send_datetime BETWEEN '$start_date' AND '$end_date'
                            AND    mail.user_id IN($queue)");
        } elseif ($source == 6) {
            $db->setQuery(" SELECT COUNT(*) AS all_calls
                            FROM   fb_chat
                            JOIN   incomming_call ON incomming_call.fb_chat_id = fb_chat.id
                            WHERE  fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                            AND    fb_chat.last_user_id IN($queue)");
        } elseif ($source == 14) {
            $db->setQuery(" SELECT COUNT(*) AS all_calls
                            FROM   viber_chat
                            JOIN   incomming_call ON incomming_call.viber_chat_id = viber_chat.id
                            WHERE  viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                            AND    fb_chat.last_user_id IN($queue)");
        } else {
            $db->setQuery("SELECT COUNT(asterisk_call_log.id) AS all_calls
                           FROM   asterisk_call_log
                           JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                           WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                           AND    asterisk_queue.number IN ($queue)
                           AND    asterisk_call_log.call_type_id = 1
                           AND    asterisk_call_log.call_status_id IN(6,7,9)");
        }

        $result =  $db->getResultArray();
        return     $result[result][0]["all_calls"];
    }

    //get summary of all calls in some time
    function get_sum_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent, $source)
    {
        global $db;

        if ($source == 4) {
            $db->setQuery(" SELECT  COUNT(*) AS calls_in_time
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                		      chat.id
                        			FROM      chat
                        			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4
                                              AND chat.last_user_id != 1 AND dashboard_request_durations.duration > 0 
                        			JOIN      user_info ON chat.last_user_id = user_info.user_id
                        			WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                  
                    			    GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } elseif ($source == 7) {
            $db->setQuery(" SELECT   COUNT(*) AS calls_in_time
                			FROM   (SELECT SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                						   mail.id
                			        FROM   mail
                			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                					  AND mail.user_id != 1 AND dashboard_request_durations.duration > 0 
                			JOIN      user_info ON mail.user_id = user_info.user_id
                			WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                						
                			GROUP BY  mail.id) AS `chat_dur`
                			JOIN      mail ON mail.id = chat_dur.id
                			WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } elseif ($source == 6) {
            $db->setQuery(" SELECT  COUNT(*) AS calls_in_time
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                              fb_chat.id
                                    FROM      fb_chat
                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                    AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                    JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                    WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                    GROUP BY  mail.id) AS `chat_dur`
                            JOIN    fb_chat ON fb_chat.id = chat_dur.id
                            WHERE   fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } elseif ($source == 14) {
            $db->setQuery(" SELECT  COUNT(*) AS calls_in_time
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                              viber_chat.id
                                    FROM      viber_chat
                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                    AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                    JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                    WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                    GROUP BY  mail.id) AS `chat_dur`
                            JOIN    viber_chat ON viber_chat.id = chat_dur.id
                            WHERE   viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } else {
            $db->setQuery("SELECT COUNT(asterisk_call_log.id) AS calls_in_time
                           FROM   asterisk_call_log
                           JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                           WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                           AND    asterisk_queue.number IN ($queue)
                           AND    asterisk_call_log.wait_time < '$call_ans_in'
                           AND    asterisk_call_log.call_type_id = 1
                           AND    asterisk_call_log.call_status_id IN(6,7,9)");
        }

        $result = $db->getResultArray();
        return $result[result][0]["calls_in_time"];
    }

    //get summary of  partly all calls
    function get_sum_partly_calls($start_date, $end_date, $call_unans_after, $queue, $agent, $source)
    {
        return get_sum_unanswered_calls_after_time($start_date, $end_date, $call_unans_after, $queue, $agent) + get_sum_answered_calls($start_date, $end_date, $queue, $agent);
    }

    //get summary of answered calls
    function get_sum_answered_calls($start_date, $end_date, $queue, $agent, $source)
    {
        global $db;

        if ($source == 4) {
            $db->setQuery(" SELECT  COUNT(*) AS answered_calls
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                		      chat.id
                        			FROM      chat
                        			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4
                                              AND chat.last_user_id != 1 AND dashboard_request_durations.duration > 0 
                        			JOIN      user_info ON chat.last_user_id = user_info.user_id
                        			WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                  
                    			    GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)");
        } elseif ($source == 7) {
            $db->setQuery(" SELECT   COUNT(*) AS answered_calls
                			FROM   (SELECT SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                						   mail.id
                			        FROM   mail
                			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                					  AND mail.user_id != 1 AND dashboard_request_durations.duration > 0 
                			JOIN      user_info ON mail.user_id = user_info.user_id
                			WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                						
                			GROUP BY  mail.id) AS `chat_dur`
                			JOIN      mail ON mail.id = chat_dur.id
                			WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)");
        } elseif ($source == 6) {
            $db->setQuery(" SELECT  COUNT(*) AS answered_calls
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                              fb_chat.id
                                    FROM      fb_chat
                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                    AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                    JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                    WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                    
                                    GROUP BY  fb_chat.id) AS `chat_dur`
                            JOIN    fb_chat ON fb_chat.id = chat_dur.id
                            WHERE   fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)");
        } elseif ($source == 14) {
            $db->setQuery(" SELECT  COUNT(*) AS answered_calls
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                              viber_chat.id
                                    FROM      viber_chat
                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                    AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                    JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                    WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                    
                                    GROUP BY  viber_chat.id) AS `chat_dur`
                            JOIN    viber_chat ON viber_chat.id = chat_dur.id
                            WHERE   viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)");
        } else {
            $db->setQuery("SELECT COUNT(asterisk_call_log.id) AS answered_calls
                            FROM   asterisk_call_log
                            JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            JOIN   asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                            WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND    asterisk_queue.number IN ($queue)
                            AND    asterisk_extension.number IN ($agent)
                            AND    asterisk_call_log.call_type_id = 1
                            AND    asterisk_call_log.call_status_id IN(6,7)");
        }


        $result =  $db->getResultArray();
        return  $result[result][0]["answered_calls"];
    }

    //get summary of answered calls
    function get_sum_unanswered_calls($start_date, $end_date, $queue, $agent, $source)
    {
        global $db;

        if ($source == 4) {
            $db->setQuery(" SELECT 0 AS answered_calls");
        } elseif ($source == 7) {
            $db->setQuery(" SELECT 0 AS answered_calls");
        } elseif ($source == 6) {
            $db->setQuery(" SELECT 0 AS calls_in_time");
        } elseif ($source == 14) {
            $db->setQuery(" SELECT 0 AS calls_in_time");
        } else {
            $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS answered_calls
                            FROM   asterisk_call_log
                            JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND    asterisk_queue.number IN ($queue)
                            AND    asterisk_call_log.call_type_id = 1
                            AND    asterisk_call_log.call_status_id IN(9)");
        }


        $result = $db->getResultArray();
        return  $result[result][0]["answered_calls"];
    }

    //get summary of answered calls in specific time
    function get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent, $source)
    {
        global $db;

        if ($source == 4) {
            $db->setQuery(" SELECT  COUNT(*) AS calls_in_time
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                		      chat.id
                        			FROM      chat
                        			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4 
                                              AND chat.last_user_id != 1 AND dashboard_request_durations.duration > 0 
                        			JOIN      user_info ON chat.last_user_id = user_info.user_id
                        			WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                  
                    			    GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } elseif ($source == 7) {
            $db->setQuery(" SELECT   COUNT(*) AS answered_calls
                			FROM   (SELECT SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                						   mail.id
                			        FROM   mail
                			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                					  AND mail.user_id != 1 AND dashboard_request_durations.duration > 0 
                			JOIN      user_info ON mail.user_id = user_info.user_id
                			WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                						
                			GROUP BY  mail.id) AS `chat_dur`
                			JOIN      mail ON mail.id = chat_dur.id
                			WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                            AND       chat_dur.avg_duration < '$call_ans_in'");
        } elseif ($source == 6) {
            $db->setQuery(" SELECT  COUNT(*) AS answered_calls
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                              fb_chat.id
                                    FROM      fb_chat
                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                    AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                    JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                    WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                    
                                    GROUP BY  fb_chat.id) AS `chat_dur`
                            JOIN    fb_chat ON fb_chat.id = chat_dur.id
                            WHERE   fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } elseif ($source == 14) {
            $db->setQuery(" SELECT  COUNT(*) AS answered_calls
                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                              viber_chat.id
                                    FROM      viber_chat
                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 5
                                    AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                    JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                    WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                    
                                    GROUP BY  viber_chat.id) AS `chat_dur`
                            JOIN    viber_chat ON viber_chat.id = chat_dur.id
                            WHERE   viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                            AND     chat_dur.avg_duration < '$call_ans_in'");
        } else {
            $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS calls_in_time
                            FROM   asterisk_call_log
                            JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            JOIN   asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                            WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND    asterisk_queue.number IN ($queue)
                            AND    asterisk_extension.number IN ($agent)
                            AND    asterisk_call_log.wait_time < '$call_ans_in'
                            AND    asterisk_call_log.call_type_id = 1
                            AND    asterisk_call_log.call_status_id IN(6,7)");
        }

        $result = $db->getResultArray();
        return  $result[result][0]["calls_in_time"];
    }

    //get summary of unanswered calls ofter specific time
    function get_sum_unanswered_calls_after_time($start_date, $end_date, $call_unans_after, $queue, $agent, $source)
    {
        global $db;

        if ($source == 4) {
            $db->setQuery(" SELECT 0 AS calls_in_time");
        } elseif ($source == 7) {
            $db->setQuery(" SELECT 0 AS calls_in_time");
        } elseif ($source == 6) {
            $db->setQuery(" SELECT 0 AS calls_in_time");
        } elseif ($source == 14) {
            $db->setQuery(" SELECT 0 AS calls_in_time");
        } else {
            $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS calls_in_time
                            FROM   asterisk_call_log
                            JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND    asterisk_queue.number IN ($queue)
                            AND    asterisk_call_log.wait_time > '$call_unans_after'
                            AND    asterisk_call_log.call_type_id = 1
                            AND    asterisk_call_log.call_status_id IN(9)");
        }


        $result = $db->getResultArray();
        return  $result[result][0]["calls_in_time"];
    }

    //get requested data
    $queue          = $_REQUEST["queue"];
    $agent          = $_REQUEST["agent"];

    $start = $_REQUEST['startDate'];
    $end     = $_REQUEST['endDate'];
    $start_date = $start . ' 00:00';
    $end_date = $end . ' 23:59';
    $sl_type        = $_REQUEST["slType"];
    $call_ans_in    = $_REQUEST["callAnsIn"];
    $call_not_after = $_REQUEST["callNotAfter"];

    //SL answered calls / answered calls (1)
    if ($sl_type == 1) {

        //define variables
        $calls_in_time = get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent, $source);
        $call_answered = get_sum_answered_calls($start_date, $end_date, $queue, $agent, $source);

        //calculate sl percent
        $sl_result = round(($calls_in_time / $call_answered) * 100, 2);

        //set result array
        $result = array("allCalls" => $call_answered, "percentValues" => $sl_result . " / " . $call_ans_in);

        //SL answered calls / all calls (2)
    } else if ($sl_type == 2) {

        //define variables
        $all_calls = get_sum_calls($start_date, $end_date, $queue, $agent, $source);
        $calls_in_time = get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent, $source);

        //calculate sl percent
        $sl_result = round(($calls_in_time / $all_calls) * 100, 2);

        //set result array
        $result = array("allCalls" => $all_calls, "percentValues" => $sl_result . " / " . $call_ans_in);

        //SL all calls / all calls (3)
    } else if ($sl_type == 3) {

        //define variables
        $all_calls = get_sum_calls($start_date, $end_date, $queue, $agent, $source);
        $all_calls_in_time = get_sum_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent, $source);

        //calculate sl percent
        $sl_result = round(($all_calls_in_time / $all_calls) * 100, 2);

        //set result array
        $result = array("allCalls" => $all_calls, "percentValues" => $sl_result . " / " . $call_ans_in);

        //SL answered calls / partly all calls (4)
    } else if ($sl_type == 4) {

        //define variables
        $calls_in_time = get_sum_answered_calls_in_time($start_date, $end_date, $call_ans_in, $queue, $agent, $source);
        $calls_partly_all = get_sum_partly_calls($start_date, $end_date, $call_not_after, $queue, $agent, $source);

        //calculate sl percent
        $sl_result = round(($calls_in_time / $calls_partly_all) * 100, 2);

        //set result array
        $result = array("allCalls" => $calls_partly_all, "percentValues" => $sl_result . " / " . $call_ans_in);
    }

    //return data array
    echo json_encode($result);
}

$request = $_REQUEST["request"];
if ($_REQUEST['act'] == 'get_columns') {
    $request = "diagrams";
}

if (isset($request) && $request == "diagrams") {

    if ($_REQUEST['act'] == 'all_calls_dialog_table') {

        $data   = array('page' => array('answear_dialog' => ''));
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];

        if ($source == 4) {
            $db->setQuery("");
        } elseif ($source == 7) {
            $db->setQuery("");
        } elseif ($source == 6) {
            $db->setQuery("");
        } elseif ($source == 14) {
            $db->setQuery("");
        } else {
            $db->setQuery(" SELECT    FROM_UNIXTIME(call_datetime),
                    			      FROM_UNIXTIME(call_datetime),
                    			      asterisk_call_log.source,
                    			      asterisk_queue.number,
                    			      asterisk_extension.number,
                    			      SEC_TO_TIME(asterisk_call_log.talk_time),
                    			      CONCAT('<p style=\"cursor:pointer;\" onclick=play(', '\'',DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://172.16.0.80:8000/', DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                            FROM      asterisk_call_log
                            JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            LEFT JOIN asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                            LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                            WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND       asterisk_queue.number IN ($queue)
                            AND       asterisk_call_log.call_type_id = 1
                            AND       asterisk_call_log.call_status_id IN(6,7,9)");
        }

        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
    } else if ($_REQUEST['act'] == 'answear_dialog_table') {

        $data   = array('page' => array('answear_dialog' => ''));
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];

        if ($source == 4) {
            $db->setQuery("");
        } elseif ($source == 7) {
            $db->setQuery("");
        } elseif ($source == 6) {
            $db->setQuery("");
        } elseif ($source == 14) {
            $db->setQuery("");
        } else {
            $db->setQuery(" SELECT    FROM_UNIXTIME(call_datetime),
                    			      FROM_UNIXTIME(call_datetime),
                    			      asterisk_call_log.source,
                    			      asterisk_queue.number,
                    			      asterisk_extension.number,
                    			      SEC_TO_TIME(asterisk_call_log.talk_time),
                    			      CONCAT('<p style=\"cursor:pointer;\" onclick=play(', '\'',DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://172.16.0.80:8000/', DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                            FROM      asterisk_call_log
                            JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                            JOIN      asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                            WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND       asterisk_queue.number IN ($queue)
                            AND       asterisk_extension.number IN ($agent)
                            AND       asterisk_call_log.call_type_id = 1
                            AND       asterisk_call_log.call_status_id IN(6,7)");
        }

        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
    } elseif ($_REQUEST['act'] == 'done_dialog_table') {

        $data   = array('page' => array('answear_dialog' => ''));
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];

        if ($source == 4) {
            $db->setQuery("");
        } elseif ($source == 7) {
            $db->setQuery("");
        } elseif ($source == 6) {
            $db->setQuery("");
        } elseif ($source == 14) {
            $db->setQuery("");
        } else {
            $db->setQuery(" SELECT    FROM_UNIXTIME(call_datetime),
                    			      FROM_UNIXTIME(call_datetime),
                    			      asterisk_call_log.source,
                    			      asterisk_queue.number,
                    			      asterisk_extension.number,
                    			      SEC_TO_TIME(asterisk_call_log.talk_time),
                    			      CONCAT('<p style=\"cursor:pointer;\" onclick=play(', '\'',DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://172.16.0.80:8000/', DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                            FROM      asterisk_call_log
                            JOIN      incomming_call ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND incomming_call.user_id > 0
                            JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                            JOIN      asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                            WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND       asterisk_queue.number IN ($queue)
                            AND       asterisk_extension.number IN ($agent)
                            AND       asterisk_call_log.call_type_id = 1
                            AND       asterisk_call_log.call_status_id IN(6,7)");
        }
        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
    } elseif ($_REQUEST['act'] == 'undone_dialog_table') {
        $data   = array('page' => array('answear_dialog' => ''));
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];

        if ($source == 4) {
            $db->setQuery("");
        } elseif ($source == 7) {
            $db->setQuery("");
        } elseif ($source == 6) {
            $db->setQuery("");
        } elseif ($source == 14) {
            $db->setQuery("");
        } else {
            $db->setQuery("SELECT    FROM_UNIXTIME(call_datetime),
                    			      FROM_UNIXTIME(call_datetime),
                    			      asterisk_call_log.source,
                    			      asterisk_queue.number,
                    			      asterisk_extension.number,
                    			      SEC_TO_TIME(asterisk_call_log.talk_time),
                    			      CONCAT('<p style=\"cursor:pointer;\" style=\"cursor:pointer;\" onclick=play(', '\'',DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\'',  ')>მოსმენა</p>', '<a download=\"audio.wav\" href=\"http://172.16.0.80:8000/', DATE_FORMAT(DATE(FROM_UNIXTIME(call_datetime)),'%Y/%m/%d/'), CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format), '\" target=\"_blank\">ჩამოტვირთვა</a>') AS `file`
                            FROM      asterisk_call_log
                            JOIN      incomming_call ON asterisk_call_log.id = incomming_call.asterisk_incomming_id 
                            JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                            JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                            JOIN      asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                            WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                            AND       asterisk_queue.number IN ($queue)
                            AND       asterisk_extension.number IN ($agent)
                            AND       asterisk_call_log.call_type_id = 1
                            AND       asterisk_call_log.call_status_id IN(6,7)
                            AND       (incomming_call.user_id = 0 OR ISNULL(incomming_call.user_id) OR incomming_call.user_id = '')");
        }

        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
    } elseif ($_REQUEST['act'] == 'incoming_all_dialog') {
        $data['page']['answear_dialog'] = '<div id="example1"></div>';
    } elseif ($_REQUEST['act'] == 'undone_dialog') {
        $data['page']['answear_dialog'] = '<div id="example5"></div>';
    } elseif ($_REQUEST['act'] == 'done_dialog') {
        $data['page']['answear_dialog'] = '<div id="example4"></div><div id="example1"></div>';
    } elseif ($_REQUEST['act'] == 'get_columns') {
        $columnCount =         $_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] =     $_REQUEST['names'];
        $operators[] =         $_REQUEST['operators'];
        $selectors[] =         $_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f = 0;
        foreach ($cols[0] as $col) {
            $column = explode(':', $col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        foreach ($res as $item) {
            $columns[$i] = $item['Field'];
            $i++;
        }

        $dat = array();
        $a = 0;
        for ($j = 0; $j < $columnCount; $j++) {
            if (1 == 2) {
                continue;
            } else {

                if ($operators[0][$a] == 1) $op = true;
                else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                //$op = false;

                if ($res['data_type'][$j] == 'date') {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
                } else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
                } else {
                    if ($columns[$j] == "inc_status") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 153);
                    } elseif ($columns[$j] == "audio_file") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 150);
                    } elseif ($columns[$j] == "id") {
                        $g = array('field' => $columns[$j], 'hidden' => true, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 100);
                    } elseif ($columns[$j] == "inc_date") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 130);
                    } else {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
                    }
                }
                $a++;
            }
            array_push($dat, $g);
        }

        // array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for ($j = 0; $j < $columnCount; $j++) {
            if ($res['data_type'][$j] == 'date') {
                $new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
            } else {
                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
            }
        }

        $filtArr = array('fields' => $new_data);
        $kendoData = array('columnss' => $dat, 'modelss' => $filtArr);

        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';

    } else
        if ($_REQUEST['act'] == 'unanswear_dialog_table') {


        $data   = array('page' => array('answear_dialog' => ''));
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];

        if ($source == 4) {
            $db->setQuery("");
        } elseif ($source == 7) {
            $db->setQuery("");
        } elseif ($source == 6) {
            $db->setQuery("");
        } elseif ($source == 14) {
            $db->setQuery("");
        } else {
            $db->setQuery(" SELECT    FROM_UNIXTIME(call_datetime),
                        			      FROM_UNIXTIME(call_datetime),
                        			      asterisk_call_log.source,
                        			      asterisk_queue.number,
                        			      SEC_TO_TIME(asterisk_call_log.wait_time)
                                FROM      asterisk_call_log
                                JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                LEFT JOIN asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                                WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                                AND       asterisk_queue.number IN ($queue)
                                AND       asterisk_call_log.call_type_id = 1
                                AND       asterisk_call_log.call_status_id IN(9)");
        }

        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
    } else
            if ($_REQUEST['act'] == 'answear_dialog') {

        $data['page']['answear_dialog'] = '<div id="example2"></div>';
    } else if ($_REQUEST['act'] == 'unanswear_dialog') {

        $data['page']['answear_dialog'] = '<div id="example3"></div>';
    } else {
        $error = '';
        $data = array('page' => array(
            'answer_call' => '',
            'technik_info' => '',
            'report_info' => '',
            'answer_call_info' => '',
            'answer_call_by_queue' => '',
            'disconnection_cause' => '',
            'unanswer_call' => '',
            'disconnection_cause_unanswer' => '',
            'unanswered_calls_by_queue' => '',
            'totals' => '',
            'call_distribution_per_day' => '',
            'call_distribution_per_hour' => '',
            'call_distribution_per_day_of_week' => '',
            'service_level' => ''
        ));

        $data['error'] = $error;

        if ($source == 4) {
            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     chat 
                                        JOIN     incomming_call ON incomming_call.chat_id = chat.id
                                        WHERE    chat.`status` IN(1,2,12,13)
                                        AND      chat.join_date BETWEEN '$start_date' AND '$end_date'
                                        AND      chat.last_user_id IN($queue)");

            $row_all_calls = $db->getResultArray();

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     chat 
                                        JOIN     incomming_call ON incomming_call.chat_id = chat.id
                                        WHERE    chat.`status` IN(1,2,12,13)
                                        AND      chat.join_date BETWEEN '$start_date' AND '$end_date'
                                        AND      chat.last_user_id IN($queue)");

            $row_answer = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_abadon = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_transfer = $db->getResultArray();

            $db->setQuery(" SELECT   SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(last_request_datetime) - UNIX_TIMESTAMP(join_date))/COUNT(*))) AS avarage
                                        FROM     chat 
                                        JOIN     incomming_call ON incomming_call.chat_id = chat.id
                                        WHERE    chat.`status` IN(1,2,12,13)
                                        AND      chat.join_date BETWEEN '$start_date' AND '$end_date'
                                        AND      chat.last_user_id IN($queue)");

            $row_avarage = $db->getResultArray();

            $db->setQuery(" SELECT GROUP_CONCAT(user_info.`name`) AS `queue_info`
                                        FROM   user_info
                                        WHERE user_id IN($queue)");

            $req_info = $db->getResultArray();
            $queue_list = $req_info[result][0][queue_info];
        } elseif ($source == 7) {

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     mail
                                        JOIN     incomming_call ON incomming_call.mail_chat_id = mail.id
                                        WHERE    mail.`mail_status_id` IN(1,2,3)
                                        AND      mail.send_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      mail.user_id IN($queue)");

            $row_all_calls = $db->getResultArray();

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     mail
                                        JOIN     incomming_call ON incomming_call.mail_chat_id = mail.id
                                        WHERE    mail.`mail_status_id` IN(1,2,3)
                                        AND      mail.send_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      mail.user_id IN($queue)");

            $row_answer = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_abadon = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_transfer = $db->getResultArray();

            $db->setQuery(" SELECT   SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(last_datetime) - UNIX_TIMESTAMP(send_datetime))/COUNT(*))) AS avarage
                                        FROM     mail
                                        JOIN     incomming_call ON incomming_call.mail_chat_id = mail.id
                                        WHERE    mail.`mail_status_id` IN(1,2,3)
                                        AND      mail.send_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      mail.user_id IN($queue)");

            $row_avarage = $db->getResultArray();

            $db->setQuery("SELECT GROUP_CONCAT(user_info.`name`) AS `queue_info` 
                                       FROM   user_info 
                                       WHERE user_id IN($queue)");

            $req_info = $db->getResultArray();
            $queue_list = $req_info[result][0][queue_info];
        } elseif ($source == 6) {

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     fb_chat
                                        JOIN     incomming_call ON incomming_call.fb_chat_id = fb_chat.id
                                        WHERE    fb_chat.`status` IN(1,2,3)
                                        AND      fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      fb_chat.last_user_id IN($queue)");

            $row_all_calls = $db->getResultArray();

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     fb_chat
                                        JOIN     incomming_call ON incomming_call.fb_chat_id = fb_chat.id
                                        WHERE    fb_chat.`status` IN(1,2,3)
                                        AND      fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      fb_chat.last_user_id IN($queue)");

            $row_answer = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_abadon = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_transfer = $db->getResultArray();

            $db->setQuery(" SELECT   SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(last_datetime) - UNIX_TIMESTAMP(first_datetime))/COUNT(*))) AS avarage
                                        FROM     fb_chat
                                        JOIN     incomming_call ON incomming_call.fb_chat_id = fb_chat.id
                                        WHERE    fb_chat.`status` IN(1,2,3)
                                        AND      fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      fb_chat.last_user_id IN($queue)");

            $row_avarage = $db->getResultArray();

            $db->setQuery(" SELECT GROUP_CONCAT(user_info.`name`) AS `queue_info`
                                        FROM   user_info
                                        WHERE user_id IN($queue)");

            $req_info = $db->getResultArray();
            $queue_list = $req_info[result][0][queue_info];
        } elseif ($source == 14) {

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     viber_chat
                                        JOIN     incomming_call ON incomming_call.viber_chat_id = viber_chat.id
                                        WHERE    viber_chat.`status` IN(1,2,3)
                                        AND      viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      viber_chat.last_user_id IN($queue)");

            $row_all_calls = $db->getResultArray();

            $db->setQuery(" SELECT   COUNT(*) AS count
                                        FROM     viber_chat
                                        JOIN     incomming_call ON incomming_call.viber_chat_id = viber_chat.id
                                        WHERE    viber_chat.`status` IN(1,2,3)
                                        AND      viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      viber_chat.last_user_id IN($queue)");

            $row_answer = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_abadon = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_transfer = $db->getResultArray();

            $db->setQuery(" SELECT   SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(last_datetime) - UNIX_TIMESTAMP(first_datetime))/COUNT(*))) AS avarage
                                        FROM     viber_chat
                                        JOIN     incomming_call ON incomming_call.viber_chat_id = viber_chat.id
                                        WHERE    viber_chat.`status` IN(1,2,3)
                                        AND      viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                        AND      viber_chat.last_user_id IN($queue)");

            $row_avarage = $db->getResultArray();

            $db->setQuery(" SELECT GROUP_CONCAT(user_info.`name`) AS `queue_info`
                                        FROM   user_info
                                        WHERE user_id IN($queue)");

            $req_info = $db->getResultArray();
            $queue_list = $req_info[result][0][queue_info];
        } else {
            $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS count
                                        FROM   asterisk_call_log
                                        JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                        WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                                        AND    asterisk_queue.number IN ($queue)
                                        AND    asterisk_call_log.call_type_id = 1
                                        AND    asterisk_call_log.call_status_id IN(6,7,9)");

            $row_all_calls = $db->getResultArray();

            $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS count,
                                               SUM(IF(asterisk_call_log.call_status_id = 6,1,0)) AS `completeagent`,
                                               SUM(IF(asterisk_call_log.call_status_id = 7,1,0)) AS `completecaller`
                                        FROM   asterisk_call_log
                                        JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                        JOIN   asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                        WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                                        AND    asterisk_queue.number IN ($queue)
                                        AND    asterisk_extension.number IN ($agent)
                                        AND    asterisk_call_log.call_type_id = 1
                                        AND    asterisk_call_log.call_status_id IN(6,7)");

            $row_answer = $db->getResultArray();

            $db->setQuery(" SELECT COUNT(asterisk_call_log.id) AS count
                                        FROM   asterisk_call_log
                                        JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                        WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                                        AND    asterisk_queue.number IN ($queue)
                                        AND    asterisk_call_log.call_type_id = 1
                                        AND    asterisk_call_log.call_status_id IN(9)");

            $row_abadon = $db->getResultArray();

            $db->setQuery(" SELECT 0 AS count");

            $row_transfer = $db->getResultArray();

            $db->setQuery(" SELECT SEC_TO_TIME(ROUND(sum(talk_time)/COUNT(asterisk_call_log.id))) AS avarage
                                        FROM   asterisk_call_log
                                        JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                        WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date')
                                        AND    asterisk_queue.number IN ($queue)
                                        AND    asterisk_call_log.call_type_id = 1
                                        AND    asterisk_call_log.call_status_id IN(6,7,9)");

            $row_avarage = $db->getResultArray();

            $queue_list = $queue;
        }




        //---------------------------------------- რეპორტ ინფო

        $data['page']['report_info'] = '<tr>
                                    <td>რიგი:</td>
                                    <td>' . $queue_list . '</td>
                                </tr>
                                <tr>
                                    <td>საწყისი თარიღი:</td>
                                    <td>' . $start . '</td>
                                </tr>
                                <tr>
                                    <td>დასრულების თარიღი:</td>
                                    <td>' . $end . '</td>
                                </tr>
                                <tr>
                                    <td>პერიოდი:</td>
                                    <td>' . $day_format . ' დღე</td>
                                </tr>';

        //----------------------------------------------

        if ($_REQUEST['act'] == 'tab_0') {

            if ($source == 4) {
                $db->setQuery(" SELECT   COUNT(*) AS count
                                            FROM     chat
                                            JOIN     incomming_call ON incomming_call.chat_id = chat.id
                                            WHERE    chat.`status` IN(1,2,12,13)
                                            AND      chat.join_date BETWEEN '$start_date' AND '$end_date'
                                            AND      chat.last_user_id IN($queue)
                                            AND      incomming_call.user_id > 0");
            } elseif ($source == 7) {
                $db->setQuery(" SELECT   COUNT(*) AS count
                                            FROM     mail
                                            JOIN     incomming_call ON incomming_call.mail_chat_id = mail.id
                                            WHERE    mail.`mail_status_id` IN(1,2,3)
                                            AND      mail.send_datetime BETWEEN '$start_date' AND '$end_date'
                                            AND      mail.user_id IN($queue)
                                            AND      incomming_call.user_id > 0");
            } elseif ($source == 6) {
                $db->setQuery(" SELECT   COUNT(*) AS count
                                            FROM     fb_chat
                                            JOIN     incomming_call ON incomming_call.fb_chat_id = fb_chat.id
                                            WHERE    fb_chat.`status` IN(1,2,3)
                                            AND      fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                            AND      fb_chat.last_user_id IN($queue)
                                            AND      incomming_call.user_id > 0");
            } elseif ($source == 14) {
                $db->setQuery(" SELECT   COUNT(*) AS count
                                            FROM     viber_chat
                                            JOIN     incomming_call ON incomming_call.viber_chat_id = viber_chat.id
                                            WHERE    viber_chat.`status` IN(1,2,3)
                                            AND      viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                            AND      viber_chat.last_user_id IN($queue)
                                            AND      incomming_call.user_id > 0");
            } else {
                $db->setQuery("	SELECT COUNT(asterisk_call_log.id) AS count
                                            FROM   asterisk_call_log
                                            JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                            JOIN   asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                            JOIN incomming_call ON incomming_call.asterisk_incomming_id = asterisk_call_log.id AND incomming_call.user_id>0
                                            WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                            AND    asterisk_queue.number IN ($queue)
                                            AND    asterisk_extension.number IN ($agent)
                                            AND    asterisk_call_log.call_type_id = 1
                                            AND    asterisk_call_log.call_status_id IN(6,7)");
            }

            $row_done_blank = $db->getResultArray();
            //------------------------------- ტექნიკური ინფორმაცია


            $data['page']['technik_info'] = '<td>მომართვა</td>
                                                         <td>' . $row_all_calls[result][0][count] . '</td>
                                                         <td>' . $row_answer[result][0][count] . '</td>
                                                         <td>' . $row_abadon[result][0][count] . '</td>
                                                         <td>' . $row_transfer[result][0][count] . '</td>
                                                         <td>' . $row_done_blank[result][0][count] . '</td>
                                                         <td>' . ($row_answer[result][0][count] - $row_done_blank[result][0][count]) . '</td>
                                                         <td>' . $row_avarage[result][0]['avarage'] . '</td>
                                                         <td>' . round(((($row_answer[result][0][count]) / ($row_answer[result][0][count] + $row_abadon[result][0][count])) * 100), 2) . ' %</td>
                                                         <td>' . round(((($row_abadon[result][0][count]) / ($row_answer[result][0][count] + $row_abadon[result][0][count])) * 100), 2) . ' %</td>
                                                         <td>' . round(((($row_transfer[result][0][count]) / ($row_answer[result][0][count] + $row_transfer[result][0][count])) * 100), 2) . ' %</td>
                                                         <td>' . round(((($row_done_blank[result][0][count]) / ($row_answer[result][0][count])) * 100), 2) . ' %</td>
                                                         <td>' . round(((($row_answer[result][0][count] - $row_done_blank[result][0][count]) / ($row_answer[result][0][count])) * 100), 2) . ' %</td>';
            // -----------------------------------------------------

        }

        if ($_REQUEST['act'] == 'tab_1') {
            //------------------------------- ნაპასუხები მომართვები რიგის მიხედვით

            if ($source == 1) {
                $db->setQuery("SELECT   COUNT(asterisk_call_log.id) AS count,
                                                    asterisk_queue.number AS `dst_queue`
                                           FROM     asterisk_call_log
                                           JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                           JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                           WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                           AND      asterisk_queue.number IN ($queue)
                                           AND      asterisk_extension.number IN ($agent)
                                           AND      asterisk_call_log.call_type_id = 1
                                           AND      asterisk_call_log.call_status_id IN(6,7)
                                           GROUP BY asterisk_call_log.queue_id");

                $answer_que = $db->getResultArray();
                foreach ($answer_que[result] as $row_answer_que) {
                    $data['page']['answer_call'] .= '
    							<tr><td>' . $row_answer_que[dst_queue] . '</td><td>' . $row_answer_que[count] . ' მომართვა</td><td>' . round($row_answer_que[count] / $row_answer[result][0][count] * 100, 2) . ' %</td></tr>
    							';
                }

                //-------------------------------------------------------

                //------------------------------- მომსახურების დონე(Service Level)


                $db->setQuery("SELECT   asterisk_call_log.wait_time AS `wait_time`
                                       FROM     asterisk_call_log
                                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                       JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                       AND      asterisk_queue.number IN ($queue)
                                       AND      asterisk_extension.number IN ($agent)
                                       AND      asterisk_call_log.call_type_id = 1
                                       AND      asterisk_call_log.call_status_id IN(6,7)");
                $w15 = 0;
                $w30 = 0;
                $w45 = 0;
                $w60 = 0;
                $w75 = 0;
                $w90 = 0;
                $w91 = 0;

                $res_service_level = $db->getResultArray();
                foreach ($res_service_level[result] as $res_service_level_r) {

                    if ($res_service_level_r['wait_time'] < 15) {
                        $w15++;
                    }

                    if ($res_service_level_r['wait_time'] < 30) {
                        $w30++;
                    }

                    if ($res_service_level_r['wait_time'] < 45) {
                        $w45++;
                    }

                    if ($res_service_level_r['wait_time'] < 60) {
                        $w60++;
                    }

                    if ($res_service_level_r['wait_time'] < 75) {
                        $w75++;
                    }

                    if ($res_service_level_r['wait_time'] < 90) {
                        $w90++;
                    }

                    $w91++;
                }

                $d30 = $w30 - $w15;
                $d45 = $w45 - $w30;
                $d60 = $w60 - $w45;
                $d75 = $w75 - $w60;
                $d90 = $w90 - $w75;
                $d91 = $w91 - $w90;


                $p15 = round($w15 * 100 / $w91);
                $p30 = round($w30 * 100 / $w91);
                $p45 = round($w45 * 100 / $w91);
                $p60 = round($w60 * 100 / $w91);
                $p75 = round($w75 * 100 / $w91);
                $p90 = round($w90 * 100 / $w91);


                $data['page']['service_level'] = '
							
							<tr class="odd">
						 		<td>15 წამში</td>
					 			<td>' . $w15 . '</td>
					 			<td></td>
					 			<td>' . $p15 . '%</td>
					 		</tr>
				 			<tr>
						 		<td>30 წამში</td>
					 			<td>' . $w30 . '</td>
					 			<td>' . $d30 . '</td>
					 			<td>' . $p30 . '%</td>
					 		</tr>
				 			<tr class="odd">
						 		<td>45 წამში</td>
					 			<td>' . $w45 . '</td>
					 			<td>' . $d45 . '</td>
					 			<td>' . $p45 . '%</td>
					 		</tr>
				 			<tr>
						 		<td>60 წამში</td>
					 			<td>' . $w60 . '</td>
					 			<td>' . $d60 . '</td>
					 			<td>' . $p60 . '%</td>
					 		</tr>
				 			<tr class="odd">
						 		<td>75 წამში</td>
					 			<td>' . $w75 . '</td>
					 			<td>' . $d75 . '</td>
					 			<td>' . $p75 . '%</td>
					 		</tr>
					 		<tr>
						 		<td>90 წამში</td>
					 			<td>' . $w90 . '</td>
					 			<td>' . $d90 . '</td>
					 			<td>' . $p90 . '%</td>
					 		</tr>
					 		<tr class="odd">
						 		<td>90+ წამში</td>
					 			<td>' . $w91 . '</td>
					 			<td>' . $d91 . '</td>
					 			<td>100%</td>
					 		</tr>
							';
            }
            //-------------------------------------------------------

            // VVV
            //------------------------------------ ნაპასუხები მომართვები

            if ($source == 4) {
                $db->setQuery(" SELECT  SEC_TO_TIME(ROUND((SUM(UNIX_TIMESTAMP(chat.last_request_datetime) - UNIX_TIMESTAMP(chat.join_date)) / COUNT(*)))) AS `sec`,
                                    				SEC_TO_TIME(SUM(UNIX_TIMESTAMP(chat.last_request_datetime) - UNIX_TIMESTAMP(chat.join_date))) AS `min`,
                                    				SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration)/COUNT(*))) AS `hold`,
                                                    SUM(UNIX_TIMESTAMP(chat.last_request_datetime) - UNIX_TIMESTAMP(chat.join_date)) AS `all_talk_time`
                                            FROM   (SELECT    SUM(dashboard_request_durations.sum_wait_time)/COUNT(*) AS `avg_duration`,
                                                		      chat.id
                                        			FROM      chat
                                        			LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4 
                                                              AND chat.last_user_id != 1 AND dashboard_request_durations.sum_wait_time > 0 
                                        			JOIN      user_info ON chat.last_user_id = user_info.user_id
                                        			WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                                  
                                    			    GROUP BY  chat.id) AS `chat_dur`
                                            JOIN    chat ON chat.id = chat_dur.id
                                            WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)");
            } elseif ($source == 7) {
                $db->setQuery(" SELECT 	    SEC_TO_TIME(round(SUM(UNIX_TIMESTAMP(mail.fetch_datetime) - UNIX_TIMESTAMP(mail.send_datetime)) / COUNT(mail.id))) AS 'sec' ,
                                            SEC_TO_TIME(round(SUM(UNIX_TIMESTAMP(mail.fetch_datetime) - UNIX_TIMESTAMP(mail.send_datetime)))) AS 'min' ,
                                            SEC_TO_TIME(round(SUM(dashboard_request_durations.sum_wait_time ))) AS 'hold'
                                FROM      mail

                                JOIN      user_info ON mail.user_id = user_info.user_id
                                LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND source_id = 7
                                WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                ");
            } elseif ($source == 6) {
                $db->setQuery(" SELECT  SEC_TO_TIME(ROUND((SUM(UNIX_TIMESTAMP(fb_chat.last_datetime) - UNIX_TIMESTAMP(fb_chat.first_datetime)) / COUNT(*)))) AS `sec`,
                                                    SEC_TO_TIME(SUM(UNIX_TIMESTAMP(fb_chat.last_datetime) - UNIX_TIMESTAMP(fb_chat.first_datetime))) AS `min`,
                                                    SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration)/COUNT(*))) AS `hold`,
                                                    SUM(UNIX_TIMESTAMP(fb_chat.last_datetime) - UNIX_TIMESTAMP(fb_chat.first_datetime)) AS `all_talk_time`
                                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                              fb_chat.id
                                                    FROM      fb_chat
                                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                                    AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                                    JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                                    WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                            
                                                    GROUP BY  fb_chat.id) AS `chat_dur`
                                            JOIN    fb_chat ON fb_chat.id = chat_dur.id
                                            WHERE   fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)");
            } elseif ($source == 14) {
                $db->setQuery(" SELECT  SEC_TO_TIME(ROUND((SUM(UNIX_TIMESTAMP(viber_chat.last_datetime) - UNIX_TIMESTAMP(viber_chat.first_datetime)) / COUNT(*)))) AS `sec`,
                                                    SEC_TO_TIME(SUM(UNIX_TIMESTAMP(viber_chat.last_datetime) - UNIX_TIMESTAMP(viber_chat.first_datetime))) AS `min`,
                                                    SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration)/COUNT(*))) AS `hold`,
                                                    SUM(UNIX_TIMESTAMP(viber_chat.last_datetime) - UNIX_TIMESTAMP(viber_chat.first_datetime)) AS `all_talk_time`
                                            FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                              viber_chat.id
                                                    FROM      viber_chat
                                                    LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                                    AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                                    JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                                    WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                                    
                                                    GROUP BY  viber_chat.id) AS `chat_dur`
                                            JOIN    viber_chat ON viber_chat.id = chat_dur.id
                                            WHERE   viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)");
            } else {
                $db->setQuery("SELECT   SEC_TO_TIME(ROUND((SUM(wait_time) / COUNT(*)))) AS `hold`,
                                    				SEC_TO_TIME(ROUND((SUM(talk_time) / COUNT(*)))) AS `sec`,
                                    				SEC_TO_TIME(ROUND((SUM(talk_time) ))) AS `min`,
                                                    SUM(talk_time) AS `all_talk_time`
                                           FROM     asterisk_call_log
                                           JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                           JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                           WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                           AND      asterisk_queue.number IN ($queue)
                                           AND      asterisk_extension.number IN ($agent)
                                           AND      asterisk_call_log.call_type_id = 1
                                           AND      asterisk_call_log.call_status_id IN(6,7)");
            }

            $row_clock = $db->getResultArray();

            $data['page']['answer_call_info'] = '<tr>
                                            					<td>ნაპასუხები მომართვები</td>
                                            					<td>' . $row_answer[result][0][count] . '</td>
                                            					</tr><tr>
                                            					<td>საშ. ხანგძლივობა:</td>
                                            					<td>' . $row_clock[result][0][sec] . '</td>
                                            					</tr>
                                            					<tr>
                                            					<td>სულ საუბრის ხანგძლივობა:</td>
                                            					<td>' . $row_clock[result][0][min] . '</td>
                                            					</tr>
                                            					<tr>
                                            					<td>ლოდინის საშ. ხანგძლივობა:</td>
                                            					<td>' . $row_clock[result][0][hold] . '</td>
                                            				</tr>';

            //---------------------------------------------


            // VVV
            //--------------------------- ნაპასუხები მომართვები ოპერატორების მიხედვით
            $all_answer_count = $row_answer[result][0][count];
            $all_abandon_count = $row_abadon[result][0][count];
            $all_answer_completeagent = $row_answer[result][0][completeagent];
            $all_answer_completecaller = $row_answer[result][0][completecaller];

            $sum_duration     = $row_clock[result][0][all_talk_time];
            if ($all_abandon_count == '') {
                $all_abandon_count = 0;
            }
            if ($all_answer_completeagent == '') {
                $all_answer_completeagent = 0;
            }
            if ($all_answer_completecaller == '') {
                $all_answer_completecaller = 0;
            }

            if ($all_answer_count == '') {
                $all_answer_count = 0;
            }

            if ($sum_duration == '') {
                $sum_duration = 0;
            }
            if ($source == 4) {
                $db->setQuery("   SELECT    user_info.`name` AS `agent`,
                                              COUNT(*) AS `num`,
                                              ROUND(COUNT(*) / $all_answer_count * 100,2) AS `call_pr`,
                                              SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date)))) AS `call_time`,
                                              ROUND(SUM(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date)) / $sum_duration * 100,2) as `call_time_pr`,
                                              SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date)) / count(*))) AS `avg_call_time`,
                                              SEC_TO_TIME(ROUND(sum(chat_dur.avg_duration))) AS `hold_time`,
                                              SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold_time`
                                    FROM   (	SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                    										chat.id
                                    					FROM      chat
                                    					LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4
                                    					AND 			chat.last_user_id != 1 AND dashboard_request_durations.duration > 0 
                                    					JOIN      user_info ON chat.last_user_id = user_info.user_id
                                    					WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                    					GROUP BY  chat.id) AS `chat_dur`
                                    JOIN    chat ON chat.id = chat_dur.id
                                    JOIN    user_info ON user_info.user_id = chat.last_user_id
                                    WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                    GROUP BY chat.last_user_id");
            } elseif ($source == 7) {
                $db->setQuery("SELECT   		user_info.`name` AS `agent`,
                        						COUNT(*) AS `num`,
                        						ROUND(COUNT(*) / $all_answer_count * 100,2) AS `call_pr`,
                        						SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(mail.last_datetime)-UNIX_TIMESTAMP(mail.send_datetime)))) AS `call_time`,
                        						ROUND(SUM(UNIX_TIMESTAMP(mail.last_datetime)-UNIX_TIMESTAMP(mail.send_datetime)) / $sum_duration * 100,2) as `call_time_pr`,
                        						SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(mail.last_datetime)-UNIX_TIMESTAMP(mail.send_datetime)) / count(*))) AS `avg_call_time`,
                        						SEC_TO_TIME(ROUND(sum(chat_dur.avg_duration))) AS `hold_time`,
                        						SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold_time`
                                    FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                            					      mail.id
                            				FROM      mail
                            				LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                            				AND       mail.user_id != 1 AND dashboard_request_durations.duration > 0 
                            				JOIN      user_info ON mail.user_id = user_info.user_id
                            				WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                    
                                    				GROUP BY  mail.id) AS `chat_dur`
                                    JOIN      mail ON mail.id = chat_dur.id
                                    JOIN      user_info ON user_info.user_id = mail.user_id
                                    WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                    GROUP BY mail.user_id");
            } elseif ($source == 6) {
                $db->setQuery("SELECT   user_info.`name` AS `agent`,
                                          COUNT(*) AS `num`,
                                          ROUND(COUNT(*) / $all_answer_count * 100,2) AS `call_pr`,
                                          SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(fb_chat.last_datetime)-UNIX_TIMESTAMP(fb_chat.first_datetime)))) AS `call_time`,
                                          ROUND(SUM(UNIX_TIMESTAMP(fb_chat.last_datetime)-UNIX_TIMESTAMP(fb_chat.first_datetime)) / $sum_duration * 100,2) as `call_time_pr`,
                                          SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(fb_chat.last_datetime)-UNIX_TIMESTAMP(fb_chat.first_datetime)) / count(*))) AS `avg_call_time`,
                                          SEC_TO_TIME(ROUND(sum(chat_dur.avg_duration))) AS `hold_time`,
                                          SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold_time`
                                  FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                    fb_chat.id
                                          FROM      fb_chat
                                          LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                          AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                          JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                          WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                          
                                          GROUP BY  fb_chat.id) AS `chat_dur`
                                  JOIN    fb_chat ON fb_chat.id = chat_dur.id
                                  JOIN    user_info ON user_info.user_id = fb_chat.last_user_id
                                  WHERE   fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                  GROUP BY fb_chat.last_user_id");
            } elseif ($source == 14) {
                $db->setQuery("SELECT   user_info.`name` AS `agent`,
                                          COUNT(*) AS `num`,
                                          ROUND(COUNT(*) / $all_answer_count * 100,2) AS `call_pr`,
                                          SEC_TO_TIME(ROUND(SUM(UNIX_TIMESTAMP(viber_chat.last_datetime)-UNIX_TIMESTAMP(viber_chat.first_datetime)))) AS `call_time`,
                                          ROUND(SUM(UNIX_TIMESTAMP(viber_chat.last_datetime)-UNIX_TIMESTAMP(viber_chat.first_datetime)) / $sum_duration * 100,2) as `call_time_pr`,
                                          SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(viber_chat.last_datetime)-UNIX_TIMESTAMP(viber_chat.first_datetime)) / count(*))) AS `avg_call_time`,
                                          SEC_TO_TIME(ROUND(sum(chat_dur.avg_duration))) AS `hold_time`,
                                          SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold_time`
                                  FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                    viber_chat.id
                                          FROM      viber_chat
                                          LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                          AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                          JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                          WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                          
                                          GROUP BY  viber_chat.id) AS `chat_dur`
                                  JOIN    viber_chat ON viber_chat.id = chat_dur.id
                                  JOIN    user_info ON user_info.user_id = viber_chat.last_user_id
                                  WHERE   viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                  GROUP BY viber_chat.last_user_id");
            } else {
                $db->setQuery("SELECT   asterisk_extension.number AS `agent`,
                                          COUNT(*) AS `num`,
                        				  ROUND(COUNT(*) / $all_answer_count * 100,2) AS `call_pr`,
                                          SEC_TO_TIME(ROUND(SUM(talk_time))) AS `call_time`,
                                          ROUND(SUM(talk_time) / $sum_duration * 100,2) as `call_time_pr`,
                                          SEC_TO_TIME(ROUND(sum(talk_time) / count(*))) AS `avg_call_time`,
                            			  SEC_TO_TIME(sum(wait_time)) AS `hold_time`,
                            			  SEC_TO_TIME(ROUND(SUM(wait_time) / COUNT(*))) AS `avg_hold_time`
                                 FROM     asterisk_call_log
                                 JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                 JOIN     asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                 WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                 AND      asterisk_queue.number IN ($queue)
                                 AND      asterisk_extension.number IN ($agent)
                                 AND      asterisk_call_log.call_type_id = 1
                                 AND      asterisk_call_log.call_status_id IN(6,7)
                                 GROUP BY asterisk_call_log.extension_id");
            }
            $ress = $db->getResultArray();
            foreach ($ress[result] as $row) {
                $data['page']['answer_call_by_queue'] .= '

                   	<tr>
					<td>' . $row[agent] . '</td>
					<td>' . $row[num] . '</td>
					<td>' . $row[call_pr] . ' %</td>
					<td>' . $row[call_time] . ' </td>
					<td>' . $row[call_time_pr] . ' %</td>
					<td>' . $row[avg_call_time] . ' </td>
					<td>' . $row[hold_time] . ' </td>
					<td>' . $row[avg_hold_time] . ' </td>
					</tr>';
            }

            //----------------------------------------------------


            //--------------------------- კავშირის გაწყვეტის მიზეზეი

            $data['page']['disconnection_cause'] = '
                            <tr>
            					<td>ოპერატორმა გათიშა:</td>
            					<td>' . $all_answer_completeagent . ' მომართვა</td>
            					<td>' . ROUND($all_answer_completeagent / $all_answer_count * 100, 2) . ' %</td>
            				</tr>
            				<tr>
            					<td>აბონენტმა გათიშა:</td>
            					<td>' . $all_answer_completecaller . ' მომართვა</td>
            					<td>' . ROUND($all_answer_completecaller / $all_answer_count * 100, 2) . ' %</td>
        					</tr>';

            //-----------------------------------------------
        }

        if ($_REQUEST['act'] == 'tab_2') {
            //----------------------------------- უპასუხო მომართვები


            $data['page']['unanswer_call'] = '  <tr>
                                            					<td>უპასუხო მომართვების რაოდენობა:</td>
                                            					<td>' . $row_abadon[result][0][count] . ' მომართვა</td>
                                            				</tr>
                                            				<tr>
                                            					<td>ლოდინის საშ. დრო კავშირის გაწყვეტამდე:</td>
                                            					<td>' . $row_abadon[result][0][sec] . ' წამი</td>
                                            				</tr>
                                            				<tr>
                                            					<td>საშ. რიგში პოზიცია კავშირის გაწყვეტამდე:</td>
                                            					<td>1</td>
                                            				</tr>
                                            				<tr>
                                            					<td>საშ. საწყისი პოზიცია რიგში:</td>
                                            					<td>1</td>
                                        					</tr>';


            //--------------------------------------------


            //----------------------------------- კავშირის გაწყვეტის მიზეზი


            $data['page']['disconnection_cause_unanswer'] = '<tr> 
                                                                 <td>აბონენტმა გათიშა</td>
                                            			         <td>' . $row_abadon[result][0][count] . ' მომართვა</td>
                                            			         <td>' . round($row_abadon[result][0][count] / $row_abadon[result][0][count] * 100, 2) . ' %</td>
                                            		         </tr>
                                            			     <tr> 
                                                                  <td>დრო ამოიწურა</td>
                                                			      <td>0 მომართვა</td>
                                                			      <td>0 %</td>
                                            		         </tr>
                                            		         <tr> 
                                                                  <td>ტექ. ხარვეზი</td>
                                                			      <td>0 მომართვა</td>
                                                			      <td>0 %</td>
                                            		         </tr>

							';

            //--------------------------------------------

            //------------------------------ უპასუხო მომართვები რიგის მიხედვით

            $db->setQuery("	 SELECT   COUNT(*) AS `count`,
                                              asterisk_queue.number AS `queue`
                                     FROM     asterisk_call_log
                                     JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                     WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                     AND      asterisk_queue.number IN ($queue)
                                     AND      asterisk_call_log.call_type_id = 1
                                     AND      asterisk_call_log.call_status_id IN(9)
                                     GROUP BY asterisk_call_log.queue_id");

            $Unanswered_Calls_by_Queue_r = $db->getResultArray();
            foreach ($Unanswered_Calls_by_Queue_r[result] as $Unanswered_Calls_by_Queue) {
                $data['page']['unanswered_calls_by_queue'] .= '
                            <tr><td>' . $Unanswered_Calls_by_Queue[queue] . '</td><td>' . $Unanswered_Calls_by_Queue[count] . ' მომართვა</td><td>' . round((($Unanswered_Calls_by_Queue[count] / $Unanswered_Calls_by_Queue[count]) * 100), 2) . ' %</td></tr>

							';
            }

            //---------------------------------------------------
        }

        if ($_REQUEST['act'] == 'tab_3') {

            $all_answer_count          = $row_answer[result][0][count];
            $all_abandon_count         = $row_abadon[result][0][count];
            $all_answer_completeagent  = $row_answer[result][0][completeagent];
            $all_answer_completecaller = $row_answer[result][0][completecaller];

            if ($all_abandon_count == '') {
                $all_abandon_count = 0;
            }
            if ($all_answer_completeagent == '') {
                $all_answer_completeagent = 0;
            }
            if ($all_answer_completecaller == '') {
                $all_answer_completecaller = 0;
            }

            if ($all_answer_count == '') {
                $all_answer_count = 0;
            }


            $data['page']['totals'] = ' <tr> 
                                                          <td>ნაპასუხები მომართვების რაოდენობა:</td>
                                        		          <td>' . $row_answer[result][0][count] . ' მომართვა</td>
                                    	            </tr>
                                                    <tr>
                                                          <td>უპასუხო მომართვების რაოდენობა:</td>
                                                          <td>' . $row_abadon[result][0][count] . ' მომართვა</td>
                                                    </tr>
		        

							';

            //------------------------------------------------


            //-------------------------------- ზარის განაწილება დღეების მიხედვით

            if ($source == 4) {
                $db->setQuery("SELECT   DATE(chat.join_date) AS `datetime`,
                                              COUNT(*) AS `all_call`,
                                              COUNT(*) AS `answer_count`,
                                              100 AS `call_answer_pr`,
                                              0 AS `unanswer_call`,
                                              0 AS `call_unanswer_pr`,
                                              SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date)) / count(*))) AS `avg_durat`,
                                              SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                    FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                    										chat.id
                                    					FROM      chat
                                    					LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4
                                    					AND 			chat.last_user_id != 1 AND dashboard_request_durations.duration > 0 
                                    					JOIN      user_info ON chat.last_user_id = user_info.user_id
                                    					WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                    					GROUP BY  chat.id) AS `chat_dur`
                                    JOIN    chat ON chat.id = chat_dur.id
                                    JOIN    user_info ON user_info.user_id = chat.last_user_id
                                    WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                    GROUP BY DATE(chat.join_date)");
            } elseif ($source == 7) {
                $db->setQuery("SELECT   DATE(mail.send_datetime) AS `datetime`,
                                              COUNT(*) AS `all_call`,
                                              COUNT(*) AS `answer_count`,
                                              100 AS `call_answer_pr`,
                                              0 AS `unanswer_call`,
                                              0 AS `call_unanswer_pr`,
                                              SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(mail.last_datetime)-UNIX_TIMESTAMP(mail.send_datetime)) / count(*))) AS `avg_durat`,
                                              SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                      FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                      mail.id
                                      FROM      mail
                                      LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                                      AND       mail.user_id != 1 AND dashboard_request_durations.duration > 0
                                      JOIN      user_info ON mail.user_id = user_info.user_id
                                      WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                      
                                      GROUP BY  mail.id) AS `chat_dur`
                                      JOIN      mail ON mail.id = chat_dur.id
                          JOIN      user_info ON user_info.user_id = mail.user_id
                          WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                          GROUP BY DATE(mail.send_datetime)");
            } elseif ($source == 6) {
                $db->setQuery("SELECT   DATE(fb_chat.first_datetime) AS `datetime`,
                                              COUNT(*) AS `all_call`,
                                              COUNT(*) AS `answer_count`,
                                              100 AS `call_answer_pr`,
                                              0 AS `unanswer_call`,
                                              0 AS `call_unanswer_pr`,
                                              SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(fb_chat.last_datetime)-UNIX_TIMESTAMP(fb_chat.first_datetime)) / count(*))) AS `avg_durat`,
                                              SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                      FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                        fb_chat.id
                                              FROM      fb_chat
                                              LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                              AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                              JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                              WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                              
                                              GROUP BY  fb_chat.id) AS `chat_dur`
                                      JOIN      fb_chat ON fb_chat.id = chat_dur.id
                                      JOIN      user_info ON user_info.user_id = fb_chat.last_user_id
                                      WHERE     fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                      GROUP BY DATE(fb_chat.first_datetime)");
            } elseif ($source == 14) {
                $db->setQuery("SELECT   DATE(viber_chat.first_datetime) AS `datetime`,
                                              COUNT(*) AS `all_call`,
                                              COUNT(*) AS `answer_count`,
                                              100 AS `call_answer_pr`,
                                              0 AS `unanswer_call`,
                                              0 AS `call_unanswer_pr`,
                                              SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(viber_chat.last_datetime)-UNIX_TIMESTAMP(viber_chat.first_datetime)) / count(*))) AS `avg_durat`,
                                              SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                      FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                        viber_chat.id
                                              FROM      viber_chat
                                              LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                              AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                              JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                              WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                              
                                      GROUP BY  viber_chat.id) AS `chat_dur`
                                      JOIN      viber_chat ON viber_chat.id = chat_dur.id
                                      JOIN      user_info ON user_info.user_id = viber_chat.last_user_id
                                      WHERE     viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                      GROUP BY DATE(viber_chat.first_datetime)");
            } else {
                $db->setQuery("SELECT   DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `datetime`,
                                              COUNT(*) AS `all_call`,
                            				  SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)) AS `answer_count`,
                            				  ROUND(SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)) / $all_answer_count * 100,2) AS `call_answer_pr`,
                            				  SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)) AS `unanswer_call`,
                            				  ROUND(SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)) / $all_abandon_count * 100,2) AS `call_unanswer_pr`,
                            				  SEC_TO_TIME(ROUND(SUM(talk_time) / SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)))) AS `avg_durat`,
                            				  SEC_TO_TIME(ROUND(SUM(wait_time) / SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)))) AS `avg_hold`
                                     FROM     asterisk_call_log
                                     JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                     WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                     AND      asterisk_queue.number IN ($queue)
                                     AND      asterisk_call_log.call_type_id = 1
                                     AND      asterisk_call_log.call_status_id IN(6,7,9)
                                     GROUP BY DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
            }

            $res = $db->getResultArray();
            foreach ($res[result] as $row) {
                $data['page']['call_distribution_per_day'] .= '<tr class="odd">
                                                        					<td>' . $row[datetime] . ':00</td>
                                                    						<td>' . (($row[all_call] != '') ? $row[all_call] : "0") . '</td>
                                                    						<td>' . (($row[answer_count] != '') ? $row[answer_count] : "0") . '</td>
                                                    						<td>' . (($row[call_answer_pr] != '') ? $row[call_answer_pr] : "0") . ' %</td>
                                                    						<td>' . (($row[unanswer_call] != '') ? $row[unanswer_call] : "0") . '</td>
                                                    						<td>' . (($row[call_unanswer_pr] != '') ? $row[call_unanswer_pr] : "0") . '%</td>
                                                    						<td>' . (($row[avg_durat] != '') ? $row[avg_durat] : "0") . ' </td>
                                                    						<td>' . (($row[avg_hold] != '') ? $row[avg_hold] : "0") . ' </td>
                                                    				   </tr>';
            }

            //----------------------------------------------------


            //-------------------------------- ზარის განაწილება საათების მიხედვით

            if ($source == 4) {
                $db->setQuery("SELECT   HOUR(chat.join_date) AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                        chat.id
                                        FROM      chat
                                        LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4
                                        AND 			chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                        JOIN      user_info ON chat.last_user_id = user_info.user_id
                                        WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                        GROUP BY  chat.id) AS `chat_dur`
                                        JOIN    chat ON chat.id = chat_dur.id
                                        JOIN    user_info ON user_info.user_id = chat.last_user_id
                                        WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                        GROUP BY HOUR(chat.join_date)");
            } elseif ($source == 7) {
                $db->setQuery("SELECT   HOUR(mail.send_datetime) AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(mail.last_datetime)-UNIX_TIMESTAMP(mail.send_datetime)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                        mail.id
                                        FROM      mail
                                        LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                                        AND       mail.user_id != 1 AND dashboard_request_durations.duration > 0
                                        JOIN      user_info ON mail.user_id = user_info.user_id
                                        WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                        
                                        GROUP BY  mail.id) AS `chat_dur`
                                        JOIN      mail ON mail.id = chat_dur.id
                                        JOIN      user_info ON user_info.user_id = mail.user_id
                                        WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                        GROUP BY HOUR(mail.send_datetime)");
            } elseif ($source == 6) {
                $db->setQuery(" SELECT   HOUR(fb_chat.first_datetime) AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(fb_chat.last_datetime)-UNIX_TIMESTAMP(fb_chat.first_datetime)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                          fb_chat.id
                                                FROM      fb_chat
                                                LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                                AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                                JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                                WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                                
                                                GROUP BY  fb_chat.id) AS `chat_dur`
                                        JOIN      fb_chat ON fb_chat.id = chat_dur.id
                                        JOIN      user_info ON user_info.user_id = fb_chat.last_user_id
                                        WHERE     fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                        GROUP BY HOUR(fb_chat.first_datetime)");
            } elseif ($source == 14) {
                $db->setQuery(" SELECT   HOUR(viber_chat.first_datetime) AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(viber_chat.last_datetime)-UNIX_TIMESTAMP(viber_chat.first_datetime)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                          viber_chat.id
                                                FROM      viber_chat
                                                LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                                AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                                JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                                WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                                
                                                GROUP BY  viber_chat.id) AS `chat_dur`
                                        JOIN      viber_chat ON viber_chat.id = chat_dur.id
                                        JOIN      user_info ON user_info.user_id = viber_chat.last_user_id
                                        WHERE     viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                        GROUP BY HOUR(viber_chat.first_datetime)");
            } else {
                $db->setQuery("SELECT   HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `datetime`,
                                                COUNT(*) AS `all_call`,
                            				    SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)) AS `answer_count`,
                            				    ROUND(SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)) / $all_answer_count * 100,2) AS `call_answer_pr`,
                            				    SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)) AS `unanswer_call`,
                            				    ROUND(SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)) / $all_abandon_count * 100,2) AS `call_unanswer_pr`,
                            				    SEC_TO_TIME(ROUND(SUM(talk_time) / SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)))) AS `avg_durat`,
                            				    SEC_TO_TIME(ROUND(SUM(wait_time) / SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)))) AS `avg_hold`
                                       FROM     asterisk_call_log
                                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                       AND      asterisk_queue.number IN ($queue)
                                       AND      asterisk_call_log.call_type_id = 1
                                       AND      asterisk_call_log.call_status_id IN(6,7,9)
                                       GROUP BY HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
            }
            $res124 = $db->getResultArray();
            foreach ($res124[result] as $row) {
                $data['page']['call_distribution_per_hour'] .= '
                				<tr class="odd">
                						<td>' . $row[datetime] . ':00</td>
                						<td>' . (($row[all_call] != '') ? $row[all_call] : "0") . '</td>
                						<td>' . (($row[answer_count] != '') ? $row[answer_count] : "0") . '</td>
                						<td>' . (($row[call_answer_pr] != '') ? $row[call_answer_pr] : "0") . ' %</td>
                						<td>' . (($row[unanswer_call] != '') ? $row[unanswer_call] : "0") . '</td>
                						<td>' . (($row[call_unanswer_pr] != '') ? $row[call_unanswer_pr] : "0") . '%</td>
                						<td>' . (($row[avg_durat] != '') ? $row[avg_durat] : "0") . ' </td>
                						<td>' . (($row[avg_hold] != '') ? $row[avg_hold] : "0") . ' </td>
                                </tr>';
            }

            //-------------------------------------------------


            //------------------------------ ზარის განაწილება კვირების მიხედვით
            if ($source == 4) {
                $db->setQuery("SELECT   CASE
                    									WHEN DAYOFWEEK(chat.join_date) = 1 THEN 'კვირა'
                    									WHEN DAYOFWEEK(chat.join_date) = 2 THEN 'ორშაბათი'
                    									WHEN DAYOFWEEK(chat.join_date) = 3 THEN 'სამშაბათი'
                    									WHEN DAYOFWEEK(chat.join_date) = 4 THEN 'ოთხშაბათი'
                    									WHEN DAYOFWEEK(chat.join_date) = 5 THEN 'ხუთშაბათი'
                    									WHEN DAYOFWEEK(chat.join_date) = 6 THEN 'პარასკევი'
                    									WHEN DAYOFWEEK(chat.join_date) = 7 THEN 'შაბათი'
                    							END AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(chat.last_request_datetime)-UNIX_TIMESTAMP(chat.join_date)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                        chat.id
                                        FROM      chat
                                        LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = chat.id AND dashboard_request_durations.source_id = 4
                                        AND 			chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                        JOIN      user_info ON chat.last_user_id = user_info.user_id
                                        WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                        GROUP BY  chat.id) AS `chat_dur`
                                        JOIN    chat ON chat.id = chat_dur.id
                                        JOIN    user_info ON user_info.user_id = chat.last_user_id
                                        WHERE   chat.join_date BETWEEN '$start_date' AND '$end_date' AND chat.last_user_id IN($queue)
                                        GROUP BY DAYOFWEEK(chat.join_date)");
            } elseif ($source == 7) {
                $db->setQuery("SELECT   CASE
                    									WHEN DAYOFWEEK(mail.send_datetime) = 1 THEN 'კვირა'
                    									WHEN DAYOFWEEK(mail.send_datetime) = 2 THEN 'ორშაბათი'
                    									WHEN DAYOFWEEK(mail.send_datetime) = 3 THEN 'სამშაბათი'
                    									WHEN DAYOFWEEK(mail.send_datetime) = 4 THEN 'ოთხშაბათი'
                    									WHEN DAYOFWEEK(mail.send_datetime) = 5 THEN 'ხუთშაბათი'
                    									WHEN DAYOFWEEK(mail.send_datetime) = 6 THEN 'პარასკევი'
                    									WHEN DAYOFWEEK(mail.send_datetime) = 7 THEN 'შაბათი'
                    							END AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(mail.last_datetime)-UNIX_TIMESTAMP(mail.send_datetime)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                        mail.id
                                        FROM      mail
                                        LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = mail.id AND dashboard_request_durations.source_id = 7
                                        AND       mail.user_id != 1 AND dashboard_request_durations.duration > 0
                                        JOIN      user_info ON mail.user_id = user_info.user_id
                                        WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                        
                                        GROUP BY  mail.id) AS `chat_dur`
                                        JOIN      mail ON mail.id = chat_dur.id
                                        JOIN      user_info ON user_info.user_id = mail.user_id
                                        WHERE     mail.send_datetime BETWEEN '$start_date' AND '$end_date' AND mail.user_id IN($queue)
                                        GROUP BY  DAYOFWEEK(mail.send_datetime)");
            } elseif ($source == 6) {
                $db->setQuery("SELECT   CASE
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 1 THEN 'კვირა'
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 2 THEN 'ორშაბათი'
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 3 THEN 'სამშაბათი'
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 4 THEN 'ოთხშაბათი'
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 5 THEN 'ხუთშაბათი'
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 6 THEN 'პარასკევი'
                                                    WHEN DAYOFWEEK(fb_chat.first_datetime) = 7 THEN 'შაბათი'
                                                END AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(fb_chat.last_datetime)-UNIX_TIMESTAMP(fb_chat.first_datetime)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                          fb_chat.id
                                                FROM      fb_chat
                                                LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = fb_chat.id AND dashboard_request_durations.source_id = 6
                                                AND       fb_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                                JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                                WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                                
                                                GROUP BY  fb_chat.id) AS `chat_dur`
                                        JOIN      fb_chat ON fb_chat.id = chat_dur.id
                                        JOIN      user_info ON user_info.user_id = fb_chat.last_user_id
                                        WHERE     fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND fb_chat.last_user_id IN($queue)
                                        GROUP BY  DAYOFWEEK(fb_chat.first_datetime)");
            } elseif ($source == 14) {
                $db->setQuery("SELECT   CASE
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 1 THEN 'კვირა'
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 2 THEN 'ორშაბათი'
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 3 THEN 'სამშაბათი'
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 4 THEN 'ოთხშაბათი'
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 5 THEN 'ხუთშაბათი'
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 6 THEN 'პარასკევი'
                                                    WHEN DAYOFWEEK(viber_chat.first_datetime) = 7 THEN 'შაბათი'
                                                END AS `datetime`,
                                                COUNT(*) AS `all_call`,
                                                COUNT(*) AS `answer_count`,
                                                100 AS `call_answer_pr`,
                                                0 AS `unanswer_call`,
                                                0 AS `call_unanswer_pr`,
                                                SEC_TO_TIME(ROUND(sum(UNIX_TIMESTAMP(viber_chat.last_datetime)-UNIX_TIMESTAMP(viber_chat.first_datetime)) / count(*))) AS `avg_durat`,
                                                SEC_TO_TIME(ROUND(SUM(chat_dur.avg_duration) / COUNT(*))) AS `avg_hold`
                                        FROM   (SELECT    SUM(dashboard_request_durations.duration)/COUNT(*) AS `avg_duration`,
                                                          viber_chat.id
                                                FROM      viber_chat
                                                LEFT JOIN dashboard_request_durations ON dashboard_request_durations.request_id = viber_chat.id AND dashboard_request_durations.source_id = 14
                                                AND       viber_chat.last_user_id != 1 AND dashboard_request_durations.duration > 0
                                                JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                                WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                                
                                                GROUP BY  viber_chat.id) AS `chat_dur`
                                        JOIN      viber_chat ON viber_chat.id = chat_dur.id
                                        JOIN      user_info ON user_info.user_id = viber_chat.last_user_id
                                        WHERE     viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date' AND viber_chat.last_user_id IN($queue)
                                        GROUP BY  DAYOFWEEK(viber_chat.first_datetime)");
            } else {
                $db->setQuery("SELECT   CASE
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 1 THEN 'კვირა'
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 2 THEN 'ორშაბათი'
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 3 THEN 'სამშაბათი'
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 4 THEN 'ოთხშაბათი'
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 5 THEN 'ხუთშაბათი'
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 6 THEN 'პარასკევი'
                    									WHEN DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = 7 THEN 'შაბათი'
                    							END AS `datetime`,
                                                COUNT(*) AS `all_call`,
                            				    SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)) AS `answer_count`,
                            				    ROUND(SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)) / $all_answer_count * 100,2) AS `call_answer_pr`,
                            				    SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)) AS `unanswer_call`,
                            				    ROUND(SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)) / $all_abandon_count * 100,2) AS `call_unanswer_pr`,
                            				    SEC_TO_TIME(ROUND(SUM(talk_time) / SUM(IF(asterisk_call_log.call_status_id IN(6,7),1,0)))) AS `avg_durat`,
                            				    SEC_TO_TIME(ROUND(SUM(wait_time) / SUM(IF(asterisk_call_log.call_status_id IN(9),1,0)))) AS `avg_hold`
                                       FROM     asterisk_call_log
                                       JOIN     asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                       WHERE    asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                       AND      asterisk_queue.number IN ($queue)
                                       AND      asterisk_call_log.call_type_id = 1
                                       AND      asterisk_call_log.call_status_id IN(6,7,9)
                                       GROUP BY DAYOFWEEK(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
            }
            $res12 = $db->getResultArray();
            foreach ($res12[result] as $row) {
                $data['page']['call_distribution_per_day_of_week'] .= '

                               	<tr class="odd">
                					<td>' . $row[datetime] . '</td>
                					<td>' . ((($row[answer_count] != '') ? $row[answer_count] : "0") + (($row[unanswer_call] != '') ? $row[unanswer_call] : "0")) . '</td>
                					<td>' . (($row[answer_count] != '') ? $row[answer_count] : "0") . '</td>
                					<td>' . (($row[call_answer_pr] != '') ? $row[call_answer_pr] : "0") . ' %</td>
                					<td>' . (($row[unanswer_call] != '') ? $row[unanswer_call] : "0") . '</td>
                					<td>' . (($row[call_unanswer_pr] != '') ? $row[call_unanswer_pr] : "0") . '%</td>
                					<td>' . (($row[avg_durat] != '') ? $row[avg_durat] : "0") . ' </td>
                					<td>' . (($row[avg_hold] != '') ? $row[avg_hold] : "0") . ' </td>

					           </tr>';
            }

            //---------------------------------------------------
        }
    }

    echo json_encode($data);
}

//get charts data
if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "slDiagram") {

    $done         = $_REQUEST['done'];
    $start_time   = $_REQUEST['start'];
    $end_time     = $_REQUEST['end'];
    $procent      = $_REQUEST['procent'];
    $number       = $_REQUEST['number'];
    $day          = $_REQUEST['day'];
    $users        = $_REQUEST['users'];
    if ($users == 0) {
        $users_query = "";
    } else {
        $users_query = "AND asterisk_call_log.user_id = '$users'";
    }

    if ($done == 1) {

        $db->setQuery("SELECT 'SL-ფაქტიური' AS `status`,
                                DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                        FROM   asterisk_call_log
                        JOIN   asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                        JOIN   asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                        WHERE  asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                        AND    asterisk_queue.number IN ($queue)
                        AND    asterisk_extension.number IN ($agent)
                        AND    asterisk_call_log.call_type_id = 1
                        AND    asterisk_call_log.call_status_id IN(6,7) $users_query
                        GROUP BY  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");

        $result = $db->getResultArray();

        $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                                    ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(asterisk_call_log.id) AS `num`
                        FROM      asterisk_call_log
                        JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                        JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                        WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                        AND       asterisk_queue.number IN ($queue)
                        AND       asterisk_extension.number IN ($agent)
                        AND       asterisk_call_log.call_type_id = 1
                        AND       asterisk_call_log.call_status_id IN(6,7) $users_query
                        GROUP BY  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");

        $result1 = $db->getResultArray();

        $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                                    ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(asterisk_call_log.id) AS `num`
                        FROM      asterisk_call_log
                        JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                        WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                        AND       asterisk_queue.number IN ($queue)
                        AND       asterisk_call_log.call_type_id = 1
                        AND       asterisk_call_log.call_status_id IN(9)
                        GROUP BY  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");

        $result2 = $db->getResultArray();
        $all = mysql_fetch_assoc(mysql_query(" SELECT   'SL-ფაქტიური' AS `status`,
                                                            DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                                                            ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                                            COUNT(asterisk_call_log.id) AS `num`
                                                FROM      asterisk_call_log
                                                JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                                                JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
                                                WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_date') AND UNIX_TIMESTAMP('$end_date') 
                                                AND       asterisk_queue.number IN ($queue)
                                                AND       asterisk_extension.number IN ($agent)
                                                AND       asterisk_call_log.call_type_id = 1
                                                AND       asterisk_call_log.call_status_id IN(6,7) $users_query
                                                GROUP BY  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))"));

        $all_res = $db->getResultArray();
        $all = $all_res[result][0];

        foreach ($result[result] as $row) {
            $name_answer[]     = $row['status'];
            $percent_answer[] = (float)$row['percent'];
            $count_answer[] = (float)$row['num'];
            $date[] = $row['date'];
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
        }

        foreach ($result1[result] as $row) {
            $name_unanswer[]     = $row['status'];
            $percent_unanswer[] = (float)$row['percent'];
            $count_unanswer[] = (float)$row['num'];
            $date[] = $row['date'];
        }

        foreach ($result2[result] as $row) {
            $unanswer[] = (float)$row['num'];
            $date[] = $row['date'];
        }
    } elseif ($done == 2) {
        $db->setQuery(" SELECT  'SL-ფაქტიური' AS `status`,
                                    HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
                                    CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
                                    ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(asterisk_incomming.wait_time) as `total`
                            FROM     `asterisk_incomming`
                            WHERE    DATE(asterisk_incomming.call_datetime) = '$day' AND    asterisk_incomming.disconnect_cause NOT IN(1,0) AND asterisk_incomming.disconnect_cause != '2'  $users_query
                            GROUP BY HOUR(asterisk_incomming.call_datetime)");

        $result = $db->getResultArray();

        $db->setQuery(" SELECT  'SL-ფაქტიური' AS `status`,
                                    HOUR(asterisk_incomming.call_datetime) AS `hourCount`,
                                    CONCAT(HOUR(asterisk_incomming.call_datetime), ':00') AS `hour`,
                                    ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(asterisk_incomming.wait_time) as `total`
                            FROM     `asterisk_incomming`
                            WHERE    DATE(asterisk_incomming.call_datetime) = '$day'  AND asterisk_incomming.disconnect_cause = '2' 
                            GROUP BY HOUR(asterisk_incomming.call_datetime)");
        $result1 = $db->getResultArray();

        foreach ($result[result] as $res) {
            $myarray[$res[hourCount]] = $res[hour];
            $myarray1[$res[hourCount]] = $res[percent];
        }

        foreach ($result1[result] as $res1) {
            $myarray2[$res1[hourCount]] = $res1[hour];
            $myarray12[$res1[hourCount]] = $res1[percent];
        }

        for ($i = 0; $i <= 23; $i++) {
            if (array_key_exists($i, $myarray)) {
                if (strlen($i) == 1) {
                    $date[] = '0' . $i . ':00';
                } else {
                    $date[] = $i . ':00';
                }
                $percent_answer[] = (float)$myarray[$i];
                $count_answer[] = (float)$myarray1[$i];
            } else {
                if (strlen($i) == 1) {
                    $date[] = '0' . $i . ':00';
                } else {
                    $date[] = $i . ':00';
                }
                $percent_answer[] = 0;
                $count_answer[] = 0;
            }

            if (array_key_exists($i, $myarray2)) {
                $unhour[] = (float)$myarray12[$i];
            } else {
                $unhour[] = 0;
            }

            $name_answer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
        }
    }

    if ($done == 3) {
        $db->setQuery(" SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                    ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                    COUNT(asterisk_incomming.wait_time) as `total`
                            FROM    `asterisk_incomming`
                            WHERE    DATE(asterisk_incomming.call_datetime) = '$day' AND    asterisk_incomming.disconnect_cause NOT IN(1,0) AND asterisk_incomming.disconnect_cause != '2'  $users_query
                            GROUP BY time");
        $result = $db->getResultArray();

        $db->setQuery(" SELECT  CONCAT(HOUR(asterisk_incomming.call_datetime), IF(MINUTE(asterisk_incomming.call_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                    ROUND((SUM(IF(asterisk_incomming.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    FLOOR(UNIX_TIMESTAMP(asterisk_incomming.call_datetime) / (30 * 60)) AS time,
                                    COUNT(asterisk_incomming.wait_time) as `total`
                            FROM    `asterisk_incomming`
                            WHERE   DATE(asterisk_incomming.call_datetime) = '$day' AND asterisk_incomming.disconnect_cause = '2'  $users_query
                            GROUP BY  time");
        $result1 = $db->getResultArray();

        foreach ($result[result] as $res) {
            $my_array[$res[0]] = $res[1];
            $myarray1[$res[0]] = $res[3];
        }

        foreach ($result[result] as $res) {
            $my_array2[$res1[0]] = $res1[1];
            $myarray12[$res1[0]] = $res1[3];
        }

        for ($i = 0; $i <= 47; $i++) {
            if (array_key_exists($i, $my_array)) {
                if (strlen($i / 2) == 1) {
                    if ($i % 2 == 0) {
                        $date[] = (($i - 1) / 2 + 0.5) . ':00';
                    } else {
                        $date[] = (($i - 1) / 2) . ':30';
                    }
                } else {
                    if ($i % 2 == 0) {
                        $date[] = (($i) / 2) . ':00';
                    } else {
                        $date[] = (($i - 1) / 2) . ':30';
                    }
                }
                $percent_unanswer[] = (float)$my_array[$i];
                $count_answer[] = (float)$myarray1[$i];
            } else {
                if (strlen(round(($i / 2))) == 1) {
                    if ($i % 2 == 0) {
                        $date[] = '0' . (($i - 1) / 2 + 0.5) . ':00';
                    } else {
                        $date[] = '0' . (($i - 1) / 2) . ':30';
                    }
                } else {
                    if ($i % 2 == 0) {
                        $date[] = (($i) / 2) . ':00';
                    } else {
                        $date[] = (($i - 1) / 2) . ':30';
                    }
                }
                $percent_unanswer[] = 0;
                $count_answer[] = 0;
            }

            if (array_key_exists($i, $my_array2)) {
                $unmin[] = (float)$myarray12[$i];
            } else {
                $unmin[] = 0;
            }

            $name_unanswer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
        }
    }

    if ($done == 4) {
        //         mysql_query("SELECT   persons.`name`,
        //                               ext
        //                      FROM    `users`
        //                      JOIN     persons ON users.person_id = persons.id
        //                      WHERE    NOT ISNULL(ext)
        //                      ORDER BY ext ASC");
    }


    $unit     = " %";

    $serie1[] = array('count_unanswer' => $count_unanswer, 'count_answer' => $count_answer, 'name_answer' => $name_answer[0], 'name_unanswer' => $name_unanswer[0], 'percent_answer' => $percent_answer, 'percent_unanswer' => $percent_unanswer, 'unit' => $unit, 'date' => $date, 'limit_number' => $limit_number, 'limit_percent' => $limit_percent, 'unanswer' => $unanswer, 'unhour' => $unhour, 'unmin' => $unmin, 'all_answer' => $all[result][0][num], 'all_procent' => $all[result][0][percent]);


    echo json_encode($serie1);
}
