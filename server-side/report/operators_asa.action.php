<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action = $_REQUEST['act'];
$error	= '';
$data	= '';

switch ($action) {
	case 'get_list' :
		$count      = $_REQUEST['count'];
		$hidden     = $_REQUEST['hidden'];
		$date_start = $_REQUEST['date_start'];
		$date_end   = $_REQUEST['date_end'];
		
	  	$db->setQuery(" SELECT   asterisk_incomming.user_id,
                                 user_info.`name`,
                                 COUNT(*) AS call_caunt,
                                 SEC_TO_TIME(SUM(asterisk_incomming.duration)) AS sum_duration,
                        		 SEC_TO_TIME(ROUND(SUM(asterisk_incomming.duration)/COUNT(*))) AS avg_duration,
                                 SEC_TO_TIME(ROUND(SUM(asterisk_incomming.wait_time)/COUNT(*))) AS avg_wait_time
                        FROM     asterisk_incomming 
                        JOIN     user_info ON user_info.user_id = asterisk_incomming.user_id
                        WHERE    asterisk_incomming.disconnect_cause IN('3', '4') 
                        AND      asterisk_incomming.duration>0  
                        AND      DATE(asterisk_incomming.datetime) BETWEEN '$date_start' AND '$date_end'
                        GROUP BY asterisk_incomming.user_id");
	  
		$data = $db->getList($count, $hidden);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);
?>