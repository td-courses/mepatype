<?php
ini_set('display_errors', 'On');
error_reporting(E_ERROR);
// MySQL Connect Link
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

// Main Strings
$action                     = $_REQUEST['act'];
$error                      = '';
$data                       = '';
$user_id	                = $_SESSION['USERID'];

switch ($action) {
	case 'get_audio_calls':
		$id = $_REQUEST['call_log'];
		$page		= GetPageAudio(getAudioFile($id));
		$data		= array('page'	=> $page);
		break;
	case 'get_list':
		$count        = $_REQUEST['count'];
		$hidden       = $_REQUEST['hidden'];
		$start_date   = $_REQUEST['start_date'];
		$end_date     = $_REQUEST['end_date'];


		// გამავალი 
		$filter_1     = $_REQUEST['filter_1']; // ნაპასუხები
		$filter_2     = $_REQUEST['filter_2']; // უპასუხო

		// შემომავალი
		$filter_3     = $_REQUEST['filter_3']; // ნაპასუხები
		$filter_4     = $_REQUEST['filter_4']; // უპასუხო

		// შიდა
		$filter_5     = $_REQUEST['filter_5']; // ნაპასუხები
		$filter_6     = $_REQUEST['filter_6']; // უპასუხო
		$q = ' ';
		$INTYPE = '';
		$INSTATUS = '';
		if ($filter_1 == 4 or $filter_2 == 5) {
			$INTYPE .= "'2',";
		}
		if ($filter_3 == 1 or $filter_4 == 2) {
			$INTYPE .= "'1',";
		}
		if ($filter_5 == 6 or $filter_6 == 7) {
			$INTYPE .= "'3',";
		}
		if ($filter_1 == 4 or $filter_3 == 1 or $filter_5 == 6) {
			$INSTATUS .= "'6','7','8','13',";
		}
		if ($filter_2 == 5 or $filter_4 == 2 or $filter_6 == 7) {
			$INSTATUS .= "'9','12'";
		}
		$INTYPE = rtrim($INTYPE, ",");
		$INSTATUS = rtrim($INSTATUS, ",");

		if (!empty($INSTATUS)) {
			$call_status = ' AND asterisk_call_log.call_status_id IN (' . $INSTATUS . ')';
		}

		if ($filter_1 == 4 or $filter_2 == 5 or $filter_3 == 1 or $filter_4 == 2 or $filter_5 == 6 or $filter_6 == 7) {
			$call_type = ' AND asterisk_call_log.call_type_id IN (' . $INTYPE . ')';
		}


		$audio = 'class="report_audio"';
		$db->setQuery(" SELECT asterisk_call_log.id AS 'id',
								FROM_UNIXTIME(asterisk_call_log.call_datetime) AS 'datetime',
								asterisk_call_log.source AS 'source',
								(CASE WHEN asterisk_call_log.call_type_id = 1 THEN asterisk_call_log.did WHEN asterisk_call_log.call_type_id = 2 THEN asterisk_call_log.destination  WHEN asterisk_call_log.call_type_id = 3 THEN asterisk_call_log.destination END)AS 'adresati',
								IF(ISNULL(asterisk_call_log.user_id),CONCAT('(',asterisk_extension.number,')'),CONCAT(user_info.name,'(',asterisk_extension.number,')')) AS 'operator',
								(CASE
									WHEN asterisk_call_log.call_type_id = 1 THEN 'შემომავალი'
									WHEN asterisk_call_log.call_type_id = 2 THEN 'გამავალი'
									WHEN asterisk_call_log.call_type_id = 3 THEN 'შიდა'
									ELSE 'UNKNOWN'
								END) AS 'call_type',
								(CASE
									WHEN asterisk_call_log.call_status_id = 6 OR asterisk_call_log.call_status_id = 7 OR asterisk_call_log.call_status_id = 8 OR asterisk_call_log.call_status_id = 13 THEN 'ნაპასუხები'
									ELSE 'უპასუხო'
								END) AS 'call_status',
								SEC_TO_TIME(asterisk_call_log.talk_time) AS 'talk_time',
								IF(asterisk_call_log.call_type_id = 5,CONCAT('<p onclick=play(\"','autodialer/',asterisk_call_record.name,'.',asterisk_call_record.format,'\")>მოსმენა</p><a download=audio.wav href=http://172.16.0.80:8000/autodialer/',asterisk_call_record.name,'.',asterisk_call_record.format,' target=_blank>ჩამოტვირთვა</a>'),CONCAT('<p onclick=play(\"',DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format,'\")>მოსმენა</p><a download=audio.wav href=http://172.16.0.80:8000/',DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format,' target=_blank>ჩამოტვირთვა</a>')) AS 'audio'
								

						FROM asterisk_call_log
						LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
						LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
						LEFT JOIN asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
						WHERE FROM_UNIXTIME(asterisk_call_log.call_datetime) >= '$start_date' AND FROM_UNIXTIME(asterisk_call_log.call_datetime) <= '$end_date' $call_type $call_status
						ORDER BY asterisk_call_log.call_datetime DESC");

		$data = $db->getList($count, $hidden);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;
echo json_encode($data);
function getAudioFile($call_log_id)
{
	global $db;
	$db->setQuery("SELECT asterisk_call_record.name AS 'name',
                          asterisk_call_record.format AS 'format',
						  DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/') AS 'dat'
                   FROM asterisk_call_record
				   LEFT JOIN asterisk_call_log ON asterisk_call_log.id=asterisk_call_record.asterisk_call_log_id
                   WHERE asterisk_call_record.asterisk_call_log_id='$call_log_id'");
	$res2 = $db->getResultArray();
	return $res2;
}
function GetPageAudio($res = '')
{
	global $db;
	$data = '
	<div id="dialog-form">
    <audio controls="" autoplay="" style="width:500px; display:block;"><source src="http://92.241.76.198:8081/' . $res[result][0][dat] . $res[result][0][name] . '.' . $res[result][0][format] . '" type="audio/wav"> Your browser does not support the audio element.</audio>
		
    </div>
    ';

	return $data;
}
