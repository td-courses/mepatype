<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
require_once('../../includes/classes/class.Mysqli.php');

global $db;
$db = new dbClass();
$ip = $_SERVER['REMOTE_ADDR'];
$act = $_REQUEST['act'];
$array = array();
$error ='';

switch($act){
    case "getQueueExt":
          $db->setQuery("SELECT
                                    extension 
                                FROM
                                    extension 
                                ORDER BY
                                    CAST( extension AS UNSIGNED ) ASC");
          $ext_req=$db->getResultArray();
          $db->setQuery("SELECT `number`
                                    FROM `queue`
                                    where actived = 1
                                    ORDER BY
                                        CAST( `number` AS UNSIGNED ) ASC");
           $queue_req=$db->getResultArray();
            $ext_options = '';
            $query_options = '';
            foreach($ext_req['result'] as $rs ){
                $ext_options .= '<option value='.$rs['extension'].'>'.$rs['extension'].'</option>';
            }
            foreach($queue_req['result'] as $queue_res){
                $query_options .= '<option value='.$queue_res['number'].'>'.$queue_res['number'].'</option>';
            }
            $array['query_options'] = $query_options;
            $array['ext_options'] = $ext_options;
    break;
    default:
        $error = "action is null";
    break;
}


echo  json_encode($array);

?>