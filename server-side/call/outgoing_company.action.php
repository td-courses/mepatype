<?php

ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
require_once('../../includes/classes/dialog_automator.class.php');
include('../../includes/classes/wsdl.class.php');

$action = $_REQUEST['act'];
$error	= '';
$filterID = '';
$data	= array();
$user_id	             = $_SESSION['USERID'];
$tableName = 'outgoing_campaign_request';
//incomming
$incom_id			 = $_REQUEST['id'];
$call_date			 = $_REQUEST['call_date'];
$hidden_user		 = $_REQUEST['hidden_user'];
$hidden_inc			 = $_REQUEST['hidden_inc'];
$imchat			     = $_REQUEST['imchat'];
$chat_id             = $_REQUEST['chat_id'];
$ipaddres            = $_REQUEST['ipaddres'];
$ii			         = $_REQUEST['ii'];
$phone				 = $_REQUEST['phone'];

// file
$rand_file			= $_REQUEST['rand_file'];
$file				= $_REQUEST['file_name'];
$optData = array();

switch ($action) {
	case 'get_edit_page':
	    $id		= $_REQUEST['id'];
		$page		= GetPage(get_compaign_data($id));

        $data		= array('page'	=> $page);

        break;
    case 'get_tabs_numbers':
        $req_id		= $_REQUEST['req_id'];

        $db->setQuery(" SELECT 	tree.id AS 'tree_id',
                                COUNT(*) AS 'count'

                        FROM tree
                        LEFT JOIN outgoing_campaign_request_base ON outgoing_campaign_request_base.sub_tree_id = tree.id OR outgoing_campaign_request_base.tree_id = tree.id

                        WHERE outgoing_campaign_request_base.outgoing_campaign_request_id='$req_id' AND outgoing_campaign_request_base.actived=1

                        GROUP BY tree.id");
        $result = $db->getResultArray();
        $data['numbers'] = $result['result'];
        break;
    case 'save_input':
        $id            = $_REQUEST['input_id'];
        $field_id      = $_REQUEST['field_id'];
        $name          = $_REQUEST['input_name'];
        $type          = $_REQUEST['input_type'];
        $position      = $_REQUEST['input_pos'];
        $key           = $_REQUEST['key'];
        $depth         = $_REQUEST['multilevel_deep'];
        $name_1        = $_REQUEST['name_1'];
        $name_2        = $_REQUEST['name_2'];
        $name_3        = $_REQUEST['name_3'];
        $nec           = $_REQUEST['nec'];
        $input_tab     = $_REQUEST['input_tab'];
        $req_id		   = $_REQUEST['req_id'];
        if($nec == 'true'){
            $nec = 1;
        }
        else{
            $nec = 0;
        }
        if($id != ''){

            $db->setQuery(" SELECT COUNT(*) AS 'cc'
                            FROM outgoing_campaign_inputs
                            WHERE id != '$id' AND `name` = '$name' AND request_id = '$req_id' AND actived=1");
            $key_count = $db->getResultArray();
            if($key_count['result'][0]['cc'] == 0){
                $db->setQuery("UPDATE outgoing_campaign_inputs SET name = '$name', necessary_input='$nec', `field_type_id` = '$type', position='$position',`key`='$key' WHERE id='$id'");
                $res = $db->execQuery();
                if($res){
                    $data['response'] = 'suc';
                }
            }
            else{
                $data['response'] = 'not_uniq';
            }
            
        }
        else{
            if($type == 11 or $type == 12){
                $key = rand(100,9999).rand(1234,8975);
            }
            $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                            FROM    outgoing_campaign_inputs
                            WHERE   `key` = '$key' AND request_id = '$req_id' AND actived = 1");
            $key_count = $db->getResultArray();
            $db->setQuery(" SELECT  COUNT(*) AS 'cc'
                            FROM    outgoing_campaign_inputs
                            WHERE   request_id = '$req_id' AND position = '$position'");
            $count = $db->getResultArray();
            if($count['result'][0]['cc'] > 0){
                if($key_count['result'][0]['cc'] == 0){
                    $db->setQuery("UPDATE outgoing_campaign_inputs SET position = position + 1 WHERE request_id = '$req_id' AND position >= '$position' AND actived = 1 ORDER BY position DESC");
                    $db->execQuery();
                    $key = $key . '___' .rand(100,999);
                    $db->setQuery("INSERT INTO outgoing_campaign_inputs 
                                            (`user_id`,`datetime`,`name`,`request_id`,`field_type_id`,`position`,`key`,`necessary_input`) VALUES ('$user_id',NOW(),'$name','$req_id','$type','$position','$key','$nec')");
                    $db->execQuery();
    
                    $data['response'] = 'suc';
                }
                else{
                    $data['response'] = 'not_uniq';
                }
                
            }
            else{
                
                if($key_count['result'][0]['cc'] == 0){
                    $key = $key . '___' .rand(100,999);
                    $db->setQuery("INSERT INTO outgoing_campaign_inputs 
                                            (`user_id`,`datetime`,`name`,`request_id`,`field_type_id`,`position`,`key`,`necessary_input`) VALUES ('$user_id',NOW(),'$name','$req_id','$type','$position','$key','$nec')");
                    $db->execQuery();
                    $data['response'] = 'suc';
                }
                else{
                    $data['response'] = 'not_uniq';
                }
                
            }
        }

    break;
    case 'get_compaign_question_inputs':
        $columnCount = 		$_REQUEST['count'];
        $cols[]      =      $_REQUEST['cols'];
        $id          =      $_REQUEST['id'];
        $request_id  =      $_REQUEST['request_id'];
        $db->setQuery(" SELECT      outgoing_campaign_inputs.id,
                                    outgoing_campaign_inputs.name,
                                    processing_field_type.name,
                                    outgoing_campaign_inputs.position
                        FROM        outgoing_campaign_inputs
                        LEFT JOIN   processing_field_type ON processing_field_type.id = outgoing_campaign_inputs.field_type_id
                        WHERE       outgoing_campaign_inputs.actived = '1' AND outgoing_campaign_inputs.request_id = '$request_id'");
        $result = $db->getKendoList($columnCount,$cols);

        $data = $result;

    break;
    case 'input_editor':
        $id         = $_REQUEST['input_id'];
        $cat_id   	= $_REQUEST['req_id'];
		$page		= GetInputsEditor($id,$cat_id);
		$data		= array('page'	=> $page);

    break;
    case 'get_tabs':
        $db->setQuery(" SELECT  id,
                                name
                        FROM tree
                        WHERE parent_id=0 AND actived=1 AND type_id=1");
        $res = $db->getResultArray();
        $page = get_tabs($res['result']);
        $data		= array('page'	=> $page);
        break;
    case 'get_users':
        $db->setQuery(" SELECT  users.id,
                                user_info.name AS 'name'
                        FROM users
                        LEFT JOIN user_info ON user_info.user_id=users.id
                        WHERE users.actived=1");
        $result = $db->getResultArray();
        $data = $result['result'];
        break;
    case 'get_compaign_users':

        $req_id = $_REQUEST['req_id'];
        
        $db->setQuery(" SELECT  operator_id AS 'id'
                                
                        FROM outgoing_campaign_settings_group_schedule_details
                        WHERE actived=1 and outgoing_campaign_settings_group_schedule_id='$req_id'");
        $result = $db->getResultArray();
        $data['users'] = $result['result'];

        break;
    case 'get_filters':

        $db->setQuery(" SELECT  id,
                                name,
                                (SELECT COUNT(*) FROM outgoing_campaign_request WHERE actived = 1 AND NOT ISNULL(outgoing_campaign_source_id)) AS 'sum',
                                (SELECT COUNT(*) FROM outgoing_campaign_request WHERE outgoing_campaign_source_id=source.id AND actived = 1) AS 'cc'
                        FROM    source
                        WHERE   actived='1'
                        ORDER BY position ASC");
        $source_filter = $db->getResultArray();

        $db->setQuery(" SELECT  outgoing_campaign_category.id,
                                outgoing_campaign_category.name,
                                (SELECT COUNT(*) FROM outgoing_campaign_request WHERE actived = 1 AND NOT ISNULL(outgoing_campaign_category_id)) AS 'sum',
                                (SELECT COUNT(*) FROM outgoing_campaign_request WHERE outgoing_campaign_category_id=outgoing_campaign_category.id AND actived = 1) AS 'cc'
                        FROM    outgoing_campaign_category
                        ORDER BY id ASC");

        $category_filter = $db->getResultArray();


        $db->setQuery(" SELECT  id,
                                (CASE
                                     WHEN id = 1 THEN 'დაწყებული'
                                     WHEN id = 2 THEN 'დაპაუზებული'
                                     WHEN id = 3 THEN 'შეწყვეტილი'
                                END) AS 'status',
                                (SELECT COUNT(*) FROM outgoing_campaign_request WHERE actived = 1 AND outgoing_campaign_active_status_id!=5) AS 'sum',
				                (SELECT COUNT(*) FROM outgoing_campaign_request WHERE outgoing_campaign_active_status_id=outgoing_campaign_active_status.id AND actived = 1  AND outgoing_campaign_request.outgoing_campaign_active_status_id!=5) AS 'cc'
                        FROM    outgoing_campaign_active_status
                        ORDER BY id ASC
                        LIMIT 3");

        $status_filter = $db->getResultArray();

        $data = array(  "source"=>$source_filter['result'],
                        "category"=>$category_filter['result'],
                        "status"=>$status_filter['result'],
                        "status_sum"=>$status_filter['result'][0]['sum'],
                        "category_sum"=>$category_filter['result'][0]['sum'],
                        "source_sum"=>$source_filter['result'][0]['sum']);

        break;

    case 'get_setting_page':
        $per_id		= $_REQUEST['id'];
        $req_id     = $_REQUEST['req_id'];

        $page		= GetOptPage(get_campaign_settings_data($req_id));

        $data		= array('page'	=> $page);

        break;
    case 'start_compaign':
        $id		= $_REQUEST['id'];
        
        $db->setQuery(" SELECT  * 
                        FROM    `users` 
                        WHERE   `group_id` = 1 AND id = '$user_id'");
        
        $count = $db->getNumRow();

        $db->setQuery(" SELECT  default_compaign 
                        FROM    outgoing_campaign_request
                        WHERE   id = '$id'");

        $req = $db->getResultArray();
        
        if($req['result'][0]['default_compaign'] == 1){
            if($count > 0){
                $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id = 1 WHERE id IN($id) AND outgoing_campaign_active_status_id NOT IN(3,4)");
            
                $db->execQuery();
        
                $db->setQuery("UPDATE outgoing_campaign_request_base SET outgoing_campaign_request_status_id = 1 WHERE outgoing_campaign_request_id IN($id) AND outgoing_campaign_request_status_id NOT IN(3,4)");
        
                $db->execQuery();
            }
        }else{
            $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id = 1 WHERE id IN($id) AND outgoing_campaign_active_status_id NOT IN(3,4)");
            
            $db->execQuery();
    
            $db->setQuery("UPDATE outgoing_campaign_request_base SET outgoing_campaign_request_status_id = 1 WHERE outgoing_campaign_request_id IN($id) AND outgoing_campaign_request_status_id NOT IN(3,4)");
    
            $db->execQuery();
        }

        break;
    case 'pause_compaign':

        $id		= $_REQUEST['id'];
        
        $db->setQuery(" SELECT  * 
        FROM    `users` 
        WHERE   `group_id` = 1 AND id = '$user_id'");

        $count = $db->getNumRow();

        $db->setQuery(" SELECT  default_compaign 
                FROM    outgoing_campaign_request
                WHERE   id = '$id'");

        $req = $db->getResultArray();

        if($req['result'][0]['default_compaign'] == 1){
            if($count > 0){
                $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id=2 WHERE id IN($id) AND outgoing_campaign_active_status_id NOT IN(3,4)");
                $db->execQuery();
                $db->setQuery("UPDATE outgoing_campaign_request_base SET outgoing_campaign_request_status_id=2 WHERE outgoing_campaign_request_id IN($id) AND outgoing_campaign_request_status_id NOT IN(3,4)");
                $db->execQuery();
            }
        }else{
            $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id=2 WHERE id IN($id) AND outgoing_campaign_active_status_id NOT IN(3,4)");
            $db->execQuery();
            $db->setQuery("UPDATE outgoing_campaign_request_base SET outgoing_campaign_request_status_id=2 WHERE outgoing_campaign_request_id IN($id) AND outgoing_campaign_request_status_id NOT IN(3,4)");
            $db->execQuery();
        }
        break;   
    
    case 'stop_compaign':
        $id		= $_REQUEST['id'];
        

        $db->setQuery(" SELECT  * 
        FROM    `users` 
        WHERE   `group_id` = 1 AND id = '$user_id'");

        $count = $db->getNumRow();

        $db->setQuery(" SELECT  default_compaign 
                FROM    outgoing_campaign_request
                WHERE   id = '$id'");

        $req = $db->getResultArray();

        if($req['result'][0]['default_compaign'] == 1){
            if($count > 0){
                $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id=3 WHERE id IN($id) AND outgoing_campaign_active_status_id NOT IN(3,4)");
                $db->execQuery();
                $db->setQuery("UPDATE outgoing_campaign_request_base SET outgoing_campaign_request_status_id=3 WHERE outgoing_campaign_request_id IN($id) AND outgoing_campaign_request_status_id NOT IN(3,4)");
                $db->execQuery();
            }
        }else{
            $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id=3 WHERE id IN($id) AND outgoing_campaign_active_status_id NOT IN(3,4)");
            $db->execQuery();
            $db->setQuery("UPDATE outgoing_campaign_request_base SET outgoing_campaign_request_status_id=3 WHERE outgoing_campaign_request_id IN($id) AND outgoing_campaign_request_status_id NOT IN(3,4)");
            $db->execQuery();
        }

        break;
    case 'disable_input':
        $id = $_REQUEST['id'];
        $type = $_REQUEST['type'];

        if($type == 'docs'){
            //$db->setQuery("UPDATE mepa_project_docs SET actived = 0 WHERE id = '$id'");
        }
        else if($type == 'contacts'){
            //$db->setQuery("UPDATE mepa_project_contacts SET actived = 0 WHERE id = '$id'");
        }
        else if($type == 'input'){
            $db->setQuery("UPDATE outgoing_campaign_inputs SET actived = 0 WHERE id = '$id'");
        }
        else{
            //DisableCategory($id);
        }
        $db->execQuery();
    break;
    case 'disable_compaign':
        $id		= $_REQUEST['id'];

        $db->setQuery(" SELECT  * 
        FROM    `users` 
        WHERE   `group_id` = 1 AND id = '$user_id'");

        $count = $db->getNumRow();

        $db->setQuery(" SELECT  default_compaign 
                FROM    outgoing_campaign_request
                WHERE   id = '$id'");

        $req = $db->getResultArray();

        if($req['result'][0]['default_compaign'] == 1){
            if($count > 0){
                $db->setQuery("UPDATE outgoing_campaign_request SET actived=0 WHERE id IN($id)");
                $db->execQuery();
            }
        }else{
            $db->setQuery("UPDATE outgoing_campaign_request SET actived=0 WHERE id IN($id)");
            $db->execQuery();
        }

        

        break;
    case 'archive_compaign':
        $id		= $_REQUEST['id'];
        
        $db->setQuery("UPDATE outgoing_campaign_request SET outgoing_campaign_active_status_id=5 WHERE id IN($id)");
        $db->execQuery();
        
        break;
    case 'get_columns':
    
        $columnCount = 		$_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] = 	$_REQUEST['names'];
        $operators[] = 		$_REQUEST['operators'];
        $selectors[] = 		$_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f=0;
        foreach($cols[0] AS $col)
        {
            $column = explode(':',$col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        foreach($res AS $item)
        {
            $columns[$i] = $item['Field'];
            $i++;
        }
        
        
        $dat = array();
        $a = 0;
        for($j = 0;$j<$columnCount;$j++)
        {
            if(1==2)
			{
				continue;
            }
            else{
                
                if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                
                if($res['data_type'][$j] == 'date')
                {
                    $g = array('field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
                }
                else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
                }
                else
                {
                    if($columns[$j] == 'id'){
                        $g = array('hidden'=>true,'field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                    }
                    else{
                        $g = array('field'=>$columns[$j],'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
                    }
                    
                }
                $a++;
            }
            array_push($dat,$g);
            
        }
        
        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
        
        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for($j=0;$j<$columnCount;$j++)
        {
            if($res['data_type'][$j] == 'date')
            {
                $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
            }
            else
            {
                $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
            }
        }
        
        
        $filtArr = array('fields'=>$new_data);
        
        
        
        $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);

        
        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
        
        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';
        
        break;

    case 'get_list2' :
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $filters     =      $_REQUEST['filters'];
        
        $filters = explode(',',$filters);
        if(!empty($filters)){
            $categories = "";
            $sources = "";
            $statuses = "";
            foreach($filters AS $filter)
            {
                $filt = explode('_',$filter);
                if($filt[0] == 'source')
                {
                    $sources .= $filt[1].',';
                }
                else if($filt[0] == 'status')
                {
                    $statuses .= $filt[1].',';
                }
                else if($filt[0] == 'category')
                {
                    $categories .= $filt[1].',';
                }   
            }
            $categories = rtrim($categories, ",");
            $sources = rtrim($sources, ",");
            $statuses = rtrim($statuses, ",");
            if(!empty($categories)){
                $cat = " AND outgoing_campaign_request.outgoing_campaign_category_id IN ($categories)";
            }
            if(!empty($statuses)){
                $stat = " AND outgoing_campaign_request.outgoing_campaign_active_status_id IN ($statuses)";
            }
            if(!empty($sources)){
                $sour = " AND outgoing_campaign_request.outgoing_campaign_source_id IN ($sources)";
            }
            $star = '<img style="margin-bottom: 3px;" src="media/images/icons/star.png">';
            $sql = "SELECT 		outgoing_campaign_request.id AS 'id',
                                IF(outgoing_campaign_request.default_compaign=1,'$star',outgoing_campaign_request.id) AS 'id2',
                                outgoing_campaign_request.datetime AS 'create_date',
                                outgoing_campaign_request.name AS 'name',
                                source.name AS 'source',
                                outgoing_campaign_category.`name` AS 'cat_name',
                                outgoing_campaign_type.`name` AS 'type',
                                IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0) AS 'contact_count',
                                'გასარკვევია' AS 'con_source',
                                (CASE
														WHEN ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1) >= 0 AND ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1) < 80 THEN CONCAT('<p class=progress_grey>',ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1),'%</p>')
														WHEN ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1) >= 80 AND ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1) <= 99 THEN CONCAT('<p class=progress_yellow>',ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1),'%</p>')
														WHEN ROUND((SUM(IF(outgoing_campaign_request_base.outgoing_campaign_request_status_id IN (4,7),1,0))*100)/IF(NOT ISNULL(outgoing_campaign_request_base.id),SUM(1),0),1)=100 THEN CONCAT('<p class=progress_green>',100,'%</p>')
														ELSE '<p class=progress_grey>0.0%</p>'
						        END) AS 'progress',
                                (CASE
                                                WHEN outgoing_campaign_request.outgoing_campaign_active_status_id = 1 THEN '<p class=status_pink><img src=\"media/images/icons/play.png\"></p>'
                                                WHEN outgoing_campaign_request.outgoing_campaign_active_status_id = 2 THEN '<p class=status_lightgreen><img src=\"media/images/icons/pause.png\"></p>'
                                                WHEN outgoing_campaign_request.outgoing_campaign_active_status_id = 3 THEN '<p class=status_red><img src=\"media/images/icons/stop.png\"></p>'
                                                WHEN outgoing_campaign_request.outgoing_campaign_active_status_id = 4 THEN '<p class=status_done><img src=\"media/images/icons/icon_check2.png\"></p>'
                                END) AS 'status'
                                
                                
                                
                    FROM outgoing_campaign_request
                    LEFT JOIN outgoing_campaign_category ON outgoing_campaign_category.id = outgoing_campaign_request.outgoing_campaign_category_id
                    LEFT JOIN outgoing_campaign_type ON outgoing_campaign_type.id = outgoing_campaign_request.outgoing_campaign_type_id
                    LEFT JOIN source ON source.id = outgoing_campaign_request.outgoing_campaign_source_id
                    LEFT JOIN outgoing_campaign_request_base ON outgoing_campaign_request_base.outgoing_campaign_request_id = outgoing_campaign_request.id AND outgoing_campaign_request_base.actived=1
                    WHERE outgoing_campaign_request.actived = 1 AND outgoing_campaign_request.outgoing_campaign_active_status_id!=5 $cat $stat $sour
                    GROUP BY outgoing_campaign_request.id";
        }    

            $db->setQuery($sql);
            $result = $db->getKendoList($columnCount,$cols);

        //$data = array('data'=>$arr,'total'=>$rowCount['result'][0]['cc']);
        //$data = var_dump($result);
        $data = $result;
        break;


    case 'get_list3' :
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $filters     =      $_REQUEST['filters'];
        $id          =      $_REQUEST['req_id'];
        $parent      =      $_REQUEST['parent'];
        $child       =      $_REQUEST['child'];
        $sql = "SELECT	outgoing_campaign_request_base.id AS 'id',
                            outgoing_campaign_request_details.datetime AS 'datetime',
                            user_info.name AS 'operator',
                            outgoing_campaign_request_base.phone_number AS 'phone',
                            FROM_UNIXTIME(outgoing_calls.call_datetime) AS 'call_date',
                            TIME_TO_SEC(outgoing_calls.duration) AS 'talk_time',
                            (CASE
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 3 THEN '<p class=base_red_status>დაუმუშავებელი</p>'
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 7 THEN '<p class=base_red_status>მიღებული</p>'
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 8 THEN '<p class=base_red_status>პროგრესში</p>'
                                        
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 4 THEN '<p class=base_red_status>მიმდინარე</p>'
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 9 THEN '<p class=base_red_status>გასაგზავნი</p>'
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 10 THEN '<p class=base_red_status>გზაშია</p>'
                                        WHEN outgoing_campaign_request_base.sub_tree_id = 11 THEN '<p class=base_red_status>მიტანილია</p>'
                            END) AS 'shin_status',
                            IF(outgoing_campaign_request_base.retries < 3,(CASE
                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 1 THEN '<p class=status_pink><img src=\"media/images/icons/play.png\"></p>'
                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 2 THEN '<p class=status_lightgreen><img src=\"media/images/icons/pause.png\"></p>'
                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 3 THEN '<p class=status_red><img src=\"media/images/icons/stop.png\"></p>'
                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 4 THEN '<p class=status_done><img src=\"media/images/icons/icon_check2.png\"></p>'

                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 5 THEN 'არ მოუსმინა'
                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 6 THEN 'ნაწილობრივ მოუსმინა'
                                WHEN outgoing_campaign_request_base.outgoing_campaign_request_status_id = 7 THEN 'მოუსმინა'
                            END),'<p style=\"color:white;margin: 0; background-color:red;\">უპასუხო</p>') AS 'status'
                            

                FROM outgoing_campaign_request_base
                LEFT JOIN outgoing_campaign_request_details ON outgoing_campaign_request_details.id = outgoing_campaign_request_base.outgoing_campaign_request_details_id
                LEFT JOIN outgoing_calls ON outgoing_calls.request_details_id = outgoing_campaign_request_details.id
                LEFT JOIN user_info ON user_info.user_id = outgoing_campaign_request_base.operator_id
                WHERE outgoing_campaign_request_base.outgoing_campaign_request_id = $id AND outgoing_campaign_request_base.actived=1 AND outgoing_campaign_request_base.tree_id='$parent' AND outgoing_campaign_request_base.sub_tree_id='$child'
                ORDER BY outgoing_campaign_request_base.id ASC";  

            $db->setQuery($sql);
            $result = $db->getKendoList($columnCount,$cols);

        //$data = array('data'=>$arr,'total'=>$rowCount['result'][0]['cc']);
        //$data = var_dump($result);
        $data = $result;
        break;
        case 'set_calendar':
            
            $hidden_id          =                    $_REQUEST['hidden_id'];
            $quarters           =  json_decode($_REQUEST['quarters'], true);
            $removing           =  json_decode($_REQUEST['removing'], true);
            $saved              =     json_decode($_REQUEST['saved'], true);
            $schedule_name      =                $_REQUEST['schedule_name'];
            
            // if($hidden_id == 0){
            //     $db->setQuery("INSERT INTO outgoing_campaign_settings_calendar_schedules
            //                             (`name`)
            //                             VALUES 
            //                             ('')");
            //     $db->execQuery();
                
            //     $setting_id = $db->getLastId();

            //     $db->setQuery("DELETE FROM  `outgoing_campaign_settings_calendar_schedules`
            //                         WHERE   `id` = '$setting_id'");
            //     $db->execQuery();

            //     $data = array('ss' => "ss");

            // }else{
            //     $setting_id = $hidden_id;
            // }

            $db->setQuery(" SELECT  `id` 
                            FROM    outgoing_campaign_settings_calendar_schedules 
                            WHERE   `name` = '$schedule_name'");

            $db->execQuery();
            $check_name = $db->getResultArray();

            if(count($quarters) > 0 && $check_name['count'] == 0){

                $db->setQuery(" INSERT  INTO    outgoing_campaign_settings_calendar_schedules 
                                            (`user_id`, `datetime`, `name`)
                                        VALUES
                                            ('$user_id', NOW(), '$schedule_name')");

                $db->execQuery();
                $last_id = $db->getLastId();

              
                if(count($saved) > 0){

                    $saveInsert = '';

                    foreach ($saved as $q) {
                        $day = $q['day'];
                        $quarter = $q['id'];
                        $time = $q['time'];
                        $counter = $q['counter'];
                        $period = $q['period'];
                        $saveInsert .= "('$last_id', '$quarter', '$day', '$time', '$counter', '$period'),"; 
                    }
                    
                    $trimmedSaved = rtrim($saveInsert, ', ');

                    $query  = " INSERT  INTO outgoing_campaign_settings_calendar
                                            (`calendar_id`, `quarter`, `day`, `time`, `hammer`, `period`)
                                        VALUES
                                            $trimmedSaved";

                    $db->setQuery($query);
                    $db->execQuery();
                }

                $values = '';

                foreach ($quarters as $q) {
                    $day = $q['day'];
                    $quarter = $q['id'];
                    $time = $q['time'];
                    $counter = $q['counter'];
                    $period = $q['period'];
                    $values .= "('$last_id', '$quarter', '$day', '$time', '$counter', '$period'),"; 
                }
                
                $trimmed = rtrim($values, ', ');
                // print var_dump($trimmed);

                $query  = " INSERT  INTO outgoing_campaign_settings_calendar
                                        (`calendar_id`, `quarter`, `day`, `time`, `hammer`, `period`)
                                    VALUES
                                        $trimmed";

                $db->setQuery($query);
                $db->execQuery();

                $data = array('error' => 0, 'message' => 'დამატებულია', 'id' => $last_id);

            }elseif($check_name['count'] != 0 && count($quarters) > 0){
                $id = $check_name['result'][0]['id'];


                if(count($saved) > 0){

                    $db->setQuery("DELETE FROM  `outgoing_campaign_settings_calendar`
                                             WHERE   `calendar_id` = '$id'");
                    $db->execQuery();

                    $saveInsert = '';
                    foreach ($saved as $q) {
                        $day = $q['day'];
                        $quarter = $q['id'];
                        $time = $q['time'];
                        $counter = $q['counter'];
                        $period = $q['period'];
                        $saveInsert .= "('$id', '$quarter', '$day', '$time', '$counter', '$period'),"; 
                    }
                    
                    $trimmedSaved = rtrim($saveInsert, ', ');

                    $query  = " INSERT  INTO outgoing_campaign_settings_calendar
                                            (`calendar_id`, `quarter`, `day`, `time`, `hammer`, `period`)
                                        VALUES
                                            $trimmedSaved";

                    $db->setQuery($query);
                    $db->execQuery();
                }

                    foreach ($quarters as $q) {
                        $day = $q['day'];
                        $quarter = $q['id'];
                        $time = $q['time'];
                        $counter = $q['counter'];
                        $period = $q['period'];
                        $values .= "($id, '$quarter', '$day', '$time', '$counter', '$period'),"; 
                    }
                    
                    $trimmed = rtrim($values, ', ');

                    $query  = " INSERT  INTO outgoing_campaign_settings_calendar
                                            (`calendar_id`, `quarter`, `day`, `time`, `hammer`, `period`)
                                        VALUES
                                            $trimmed";

                    $db->setQuery($query);
                    $db->execQuery();

                    $data = array('error' => 0, 'message' => 'შესწორებულია', 'id' => $id);
            } else{

                $data = array('error' => 1, 'message' => 'შეცდომა', 'id' => $hidden_id);
            }
    break;
    case 'add_campaign':
            
            $id                     = $_REQUEST['campaign_id'];
            $campaign_name          = $_REQUEST['campaign_name'];
            $campaign_cat           = $_REQUEST['campaign_cat'];
            $campaign_com_channel   = $_REQUEST['campaign_com_channel'];
            $campaign_connect_type  = $_REQUEST['campaign_connect_type'];
            $campaign_start_date    = $_REQUEST['campaign_start_date'];
            $campaign_end_date      = $_REQUEST['campaign_end_date'];
            $campaign_comment       = $_REQUEST['campaign_comment'];
            $campaign_calendar_id   = $_REQUEST['campaign_calendar_id'];
            $campaign_parameter_id  = $_REQUEST['campaign_parameter_id'];
            $campaign_group_id      = $_REQUEST['campaign_group_id'];
            $check_id               = $_REQUEST['check_id'];
          
            if($check_id != ''){
                edit_campaign($id, $user_id, $campaign_name, $campaign_cat, $campaign_com_channel, $campaign_connect_type, $campaign_start_date, $campaign_end_date, $campaign_comment, $campaign_calendar_id, $campaign_parameter_id, $campaign_group_id);
            }else{
                add_campaign($id, $user_id, $campaign_name, $campaign_cat, $campaign_com_channel, $campaign_connect_type, $campaign_start_date, $campaign_end_date, $campaign_comment, $campaign_calendar_id, $campaign_parameter_id, $campaign_group_id);
            }


    break;
    case 'save_parameters':
        $first_call                 = $_REQUEST['first_call'];
        $wait_time_before_answer    = $_REQUEST['wait_time_before_answer'];
        $attempt_count              = $_REQUEST['attempt_count'];
        $attempt_period_time        = $_REQUEST['attempt_period_time'];
        $call_type                  = $_REQUEST['call_type'];
        $listening_status           = $_REQUEST['listening_status'];
        $missed_call_upload_time    = $_REQUEST['missed_call_upload_time'];

        $min_off                    = $_REQUEST['min_off'];
        $max_off                    = $_REQUEST['max_off'];
        $to_call_time               = $_REQUEST['to_call_time'];

        $parameter_schedule_name    = $_REQUEST['parameter_schedule_name'];
        $parameters_hidden_id       = $_REQUEST['parameters_hidden_id'];
        $settings_hidden_id         = $_REQUEST['settings_hidden_id'];
        
        $db->setQuery(" SELECT  `id` 
                        FROM    `outgoing_campaign_settings_parameters`
                        WHERE `name` = '$parameter_schedule_name'");
        $db->execQuery();
        $check_name = $db->getResultArray();

        if($check_name['count'] == 0){
            
            $db->setQuery("INSERT INTO outgoing_campaign_settings_parameters
                                        (`user_id`, `datetime`, `name`, `first_call`, `wait_time_before_answer`, `attempt_count`, `attempt_period_time`, `call_type_id`, `listening_status`, `min_off`, `max_off`, `to_call_time`, `missed_upload_time`)
                                    VALUES 
                                        ('$user_id', NOW(), '$parameter_schedule_name', '$first_call', '$wait_time_before_answer', '$attempt_count', '$attempt_period_time', '$call_type', '$listening_status', '$min_off', '$max_off', '$to_call_time', '$missed_call_upload_time')");
            
            $db->execQuery();
            $parameter_id = $db->getLastId();

            $db->setQuery(" UPDATE  `outgoing_campaign_request`
                            SET     `outgoing_campaign_settings_parameters_id` = '$parameter_id'
                            WHERE   `id` = '$settings_hidden_id'");
            $db->execQuery();


            $data = array('error' => 0, 'message' => 'შაბლონი წარმატებით შეიქმნა', 'parameter_id' => $parameter_id);

        } else {

            $db->setQuery(" UPDATE  `outgoing_campaign_settings_parameters`
                            SET     `user_id`                   = '$user_id', 
                                    `datetime`                  =  NOW(), 
                                    `first_call`                = '$first_call', 
                                    `wait_time_before_answer`   = '$wait_time_before_answer', 
                                    `attempt_count`             = '$attempt_count', 
                                    `attempt_period_time`       = '$attempt_period_time', 
                                    `call_type_id`              = '$call_type', 
                                    `listening_status`          = '$listening_status',
                                    `missed_upload_time`        = '$missed_call_upload_time',
                                    `min_off`                   = '$min_off',
                                    `max_off`                   = '$max_off',
                                    `to_call_time`              = '$to_call_time'   
                            WHERE   `id`                        = '$parameters_hidden_id'");

            $db->execQuery();

            $data = array('error' => 0, 'message' => 'ცვლილება წარმატებით განხორციელდა', 'parameter_id' => $parameters_hidden_id);
        }

    break;
    case 'get_selectors_data':
        $id          = $_REQUEST['input'];
        $cols[]      = $_REQUEST['cols'];
        $columnCount = $_REQUEST['count'];
        $db->setQuery(" SELECT  table_name
                        FROM    outgoing_campaign_inputs
                        WHERE   id = '$id'");
        $table = $db->getResultArray();
        $tableName = $table['result'][0]['table_name'];

        $db->setQuery(" SELECT  id,
                                name
                        FROM    $tableName
                        WHERE   actived = '1'");
        $result = $db->getKendoList($columnCount,$cols);

        $data = $result;

    break;
    case 'add_selector':
        $id         = $_REQUEST['input_id'];
        $selector   = $_REQUEST['selector_id'];
		$page		= GetSelectorEditor($id,$selector);
		$data		= array('page'	=> $page);

    break;
    case 'save_selector':
        $id         = $_REQUEST['input_id'];
        $selector   = $_REQUEST['selector_id'];
        $param_name = $_REQUEST['param_name'];
        if($id != ''){
            $db->setQuery(" SELECT  outgoing_campaign_inputs.table_name,
                                outgoing_campaign_inputs.key AS 'input_key'
                        FROM    outgoing_campaign_inputs
                        WHERE   outgoing_campaign_inputs.id = $id");
            $res = $db->getResultArray();
            $tableName = $res['result'][0]['table_name'];
            if($tableName == '')
            {
                $new_tableName = 'dir_campaign_'.$res['result'][0]['input_key'];

                //echo $new_tableName;

                $db->setQuery("CREATE TABLE `$new_tableName` (
                                            id INT(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
                                            user_id INT(4),
                                            datetime datetime,
                                            name VARCHAR(70),
                                            actived INT(2) DEFAULT 1
                                            )");
                $db->execQuery();
                $db->setQuery("INSERT INTO `$new_tableName` (`user_id`,`datetime`,`name`) VALUES('$user_id',NOW(),'$param_name')");
                $db->execQuery();
                $db->setQuery("UPDATE outgoing_campaign_inputs SET table_name='$new_tableName' WHERE id = '$id'");
                $db->execQuery();
                $data['response'] = 'suc';
            }
            else{
                if($selector != ''){
                    
                    $db->setQuery("UPDATE $tableName SET name = '$param_name' WHERE id = '$selector' ");
                    $answer = $db->execQuery();
                    if($answer){
                        $data['response'] = 'suc';
                    }
                }
                else{
                    $db->setQuery("INSERT INTO $tableName (`user_id`,`datetime`,`name`) VALUES('$user_id',NOW(),'$param_name')");
                    $answer = $db->execQuery();
                    
                    $data['response'] = 'suc';
                }
            }
            $data['input_id'] = '';
        }
        else{
            $db->setQuery("INSERT INTO outgoing_campaign_inputs 
                                            (`user_id`,`datetime`,`name`,`processing_fieldset_id`) VALUES ('$user_id',NOW(),'temporary_name_sys','$field_id')");
            $db->execQuery();

            $increment = $db->getIncrement('outgoing_campaign_inputs');
            $increment = $increment - 1;
            $new_tableName = 'dir_campaign_newinputID_'.$increment;



            $db->setQuery("UPDATE outgoing_campaign_inputs SET table_name='$new_tableName' WHERE id='$increment'");
            $db->execQuery();



            $db->setQuery("CREATE TABLE `$new_tableName` (
                                        id INT(11) AUTO_INCREMENT NOT NULL PRIMARY KEY,
                                        user_id INT(4),
                                        datetime datetime,
                                        name VARCHAR(70),
                                        actived INT(2) DEFAULT 1
                                        )");
            $db->execQuery();
            $db->setQuery("INSERT INTO `$new_tableName` (`user_id`,`datetime`,`name`) VALUES('$user_id',NOW(),'$param_name')");
            $answer = $db->execQuery();

            $data['response'] = 'suc';
            $data['input_id'] = $increment;
        }
        
            

    break;
    case 'create_group':

        $op_id                  = $_REQUEST['op_id'];
        $group_schedule_name    = $_REQUEST['group_schedule_name'];
        $group_hidden_id        = $_REQUEST['group_hidden_id'];
        $settings_hidden_id     = $_REQUEST['settings_hidden_id'];
        
        if(count($op_id) > 0){

                $db->setQuery(" SELECT  `id` 
                                FROM    `outgoing_campaign_settings_group_schedules`
                                WHERE   `name` = '$group_schedule_name'");

                $db->execQuery();
                $check_name = $db->getResultArray();

                if($check_name['count'] == 0){

                $db->setQuery("INSERT INTO outgoing_campaign_settings_group_schedules
                                        (`user_id`, `datetime`, `name`)
                                    VALUES 
                                        ('$user_id', NOW(), '$group_schedule_name')");

                $db->execQuery();
                $group_id = $db->getLastId();

                $values = '';
                foreach ($op_id as $op) {
                    $values .= "('$group_id','$op'),"; 
                }
                $trimmed = rtrim($values, ', ');
        
                $db->setQuery("INSERT INTO outgoing_campaign_settings_group_schedule_details
                                        (`outgoing_campaign_settings_group_schedule_id`, `operator_id`)
                                    VALUES 
                                        $trimmed");
                $db->execQuery();

                $db->setQuery("UPDATE  `outgoing_campaign_request`
                                SET    `outgoing_campaign_settings_group_schedule_id` = '$group_id'
                                WHERE  `id` = '$settings_hidden_id'");
                $db->execQuery();

                $data = array('error' => 0, 'message' => 'შაბლონი წარმატებით შეიქმნა', 'group_id' => $group_id);

            }else {
               
                $db->setQuery(" DELETE  
                                FROM    `outgoing_campaign_settings_group_schedule_details` 
                                WHERE   `outgoing_campaign_settings_group_schedule_id` = '$group_hidden_id'");
                $db->execQuery();

                $values = '';
                foreach ($op_id as $op) {
                    $values .= "('$group_hidden_id','$op'),"; 
                }
                $trimmed = rtrim($values, ', ');

                $db->setQuery("INSERT INTO outgoing_campaign_settings_group_schedule_details
                                        (`outgoing_campaign_settings_group_schedule_id`, `operator_id`)
                                    VALUES 
                                        $trimmed");

                $db->execQuery();
                $data = array('error' => 0, 'message' => 'ცვლილება წარმატებით განხორციელდა', 'group_id' => $group_hidden_id);
            }
        }else{

            $data = array('error' => 1, 'message' => 'მონიშნეთ ოპერატორი');
        }
    
    break;
    case 'select_group':
    
        $id         = $_REQUEST['id'];
        $req_id     = $_REQUEST['req_id'];


        $db->setQuery("UPDATE  `outgoing_campaign_request`
                        SET    `outgoing_campaign_settings_group_schedule_id` = '$id'
                        WHERE  `id` = '$req_id'");
        $db->execQuery();

        $db->setQuery(" SELECT
                            `details`.`operator_id` AS `id`
                        FROM
                            `outgoing_campaign_settings_group_schedules` AS `group`
                        LEFT JOIN `outgoing_campaign_settings_group_schedule_details` as `details` ON `details`.`outgoing_campaign_settings_group_schedule_id` = `group`.`id`
                        WHERE
                            `group`.`id` = '$id'");
        $db->execQuery();

                
        $users = $db->getResultArray();
        $data['users'] = $users['result'];

    break;
    case 'select_parameter': 

        
        $id         = $_REQUEST['id'];
        $req_id     = $_REQUEST['req_id'];


        $db->setQuery(" UPDATE  `outgoing_campaign_request`
                        SET     `outgoing_campaign_settings_parameters_id` = '$id'
                        WHERE   `id` = '$req_id'");
        $db->execQuery();

        $db->setQuery(" SELECT  `id`, 
                                `name`,
                                `first_call`,
                                `welcome_file_original`,
                                `wait_time_before_answer`,
                                `attempt_count`,
                                `attempt_period_time`,
                                `call_type_id`,
                                `listening_status`
                        FROM    `outgoing_campaign_settings_parameters`
                        WHERE   `id` = '$id' AND `actived` = 1");
                
        $db->execQuery();

        $data = $db->getResultArray();
        
    break;
    case 'select_calendar':

        $id = $_REQUEST['id'];
        $req_id     = $_REQUEST['req_id'];

        if($id != 0){

            $db->setQuery(" UPDATE  `outgoing_campaign_request`
                            SET     `outgoing_campaign_settings_calendar_schedule_id` = '$id'
                            WHERE   `id` = '$req_id'");
            
            $db->execQuery();
            
            $db->setQuery(" SELECT  `day`, `quarter`
                            FROM 
                                    `outgoing_campaign_settings_calendar` as `calendar`
                            LEFT JOIN `outgoing_campaign_request` as `req` ON `req`.`outgoing_campaign_settings_calendar_schedule_id` = `calendar`.`calendar_id`
                            WHERE `calendar`.`calendar_id` = '$id'");
            
            $result =  $db->getResultArray();
            $data = array("error" => 0, "message" => "", "result" => $result['result']);
        
        }else{
            $data = array("error" => 1, "message" => "დროის კალენდარი ცარიელია");
        }

    break;
    
    case 'delete_schedule':

        $id           = $_REQUEST['id'];
        $act_name     = $_REQUEST['name'];

        switch($act_name){
            case 'operators':
                
                $db->setQuery("UPDATE `outgoing_campaign_settings_group_schedules`
                                        SET `actived` = 0
                                WHERE   `id` = '$id' ");

                $db->execQuery();
            
                $data = array('act' => 'operators', 'message' => "შაბლონი წაიშალა", 'status' => 1, 'data' => get_group_schedules());

            break;
            case 'calendar':

                $db->setQuery("UPDATE   `outgoing_campaign_settings_calendar_schedules`
                                SET     `actived` = 0
                                WHERE   `id` = '$id' ");

                $db->execQuery();

                $data = array('act' => 'calendar', 'message' => "შაბლონი წაიშალა", 'status' => 1, 'data' => get_calendar_schedules());

            break;
            case 'parameter':
                $db->setQuery("UPDATE   `outgoing_campaign_settings_parameters`
                                SET     `actived` = 0
                                WHERE   `id` = '$id' ");

                $db->execQuery();

                $data = array('act' => 'parameter', 'message' => "შაბლონი წაიშალა", 'status' => 1, 'data' => get_parameter_schedules());

            break;
            default:

            $data = array('message' => "შეცდომა", 'status' => 0);

        }
    break;
   
    case 'get_parameters':
            $id     = $_REQUEST['id'];

            $db->setQuery(" SELECT
                                `parameters`.`name`,
                                `parameters`.`first_call`,
                                `parameters`.`wait_time_before_answer`,
                                `parameters`.`attempt_count`,
                                `parameters`.`attempt_period_time`,
                                `parameters`.`call_type_id`,
                                `parameters`.`listening_status`,
                                `parameters`.`min_off`,
                                `parameters`.`max_off`,
                                `parameters`.`to_call_time`,
                                `parameters`.`missed_upload_time`
                            FROM
                                `outgoing_campaign_settings_parameters` AS `parameters` 
                            WHERE
                                `id` = '$id'");
            $db->execQuery();
            
            
            $result = $db->getResultArray();

            $data = array('result' => $result, 'schedules' => get_parameter_schedules());
            
    break;
    case 'get_group_schedule':
        $id     = $_REQUEST['id'];

        $db->setQuery(" SELECT
                            `details`.`operator_id` AS `id`
                        FROM
                            `outgoing_campaign_settings_group_schedules` AS `group`
                        LEFT JOIN `outgoing_campaign_settings_group_schedule_details` as `details` ON `details`.`outgoing_campaign_settings_group_schedule_id` = `group`.`id`
                        WHERE
                            `group`.`id` = '$id'");

        $db->execQuery();
        $result = $db->getResultArray();

        $data['schedules'] = get_group_schedules();
        $data['users'] = $result['result'];

    break;
    case 'get_calendar_schedule': 
            $id = $_REQUEST['id'];

            if($id != 0){
            
            $db->setQuery(" SELECT  `day`, `quarter`
                            FROM 
                                    `outgoing_campaign_settings_calendar` as `calendar`
                            LEFT JOIN `outgoing_campaign_request` as `req` ON `req`.`outgoing_campaign_settings_calendar_schedule_id` = `calendar`.`calendar_id`
                            WHERE `calendar`.`calendar_id` = '$id'");
            
            $result =  $db->getResultArray();
            
           
            $data = array("error" => 0, "message" => "", "result" => $result['result'], 'schedules' => get_calendar_schedules());
        
        }else{
            $data = array("error" => 1, "message" => "დროის კალენდარი ცარიელია", 'schedules' => get_calendar_schedules());
        }


    break;
    case 'get_settings_voices':
        
        $id          =      $_REQUEST['id'];
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        

        $db->setQuery(" SELECT      `voice`.`id` AS `id`,
                                    `voice`.`datetime` AS `create_date`,
                                    `voice`.`name` AS `name` ,
                                    `type`.`name` AS `voice_type`,
                                    `voice`.`position` AS `position`,
                                    `voice`.`value` AS `value`
                        FROM        outgoing_campaign_request_voice as voice
                        LEFT JOIN   outgoing_campaign_field_type as type ON `type`.`id` = `voice`.`voice_type_id`
                        WHERE       request_id = '$id' AND actived = 1
                        ORDER BY `voice`.`position`");
        
        $db->execQuery();

      
        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'add_settings_voice':

        $id               = $_REQUEST['id'];
        $name             = $_REQUEST['name'];
        $add_voice_type   = $_REQUEST['add_voice_type'];
        $add_voice_pos    = $_REQUEST['add_voice_pos'];

        $db->setQuery(" INSERT  INTO outgoing_campaign_request_voice
                                    (`datetime`, `user_id`, `voice_type_id`, `request_id`, `name`, `position`)
                                VALUES
                                    (NOW(), '$user_id', '$add_voice_type', '$id', '$name', '$add_voice_pos')");

        $db->execQuery();

    break;
    case 'update_voice':

        $id               = $_REQUEST['id'];
        $name             = $_REQUEST['name'];
        $add_voice_type   = $_REQUEST['add_voice_type'];
        $add_voice_pos    = $_REQUEST['add_voice_pos'];

        $db->setQuery(" UPDATE  `outgoing_campaign_request_voice`
                        SET     `datetime`          =   NOW(),
                                `user_id`           =   '$user_id',
                                `voice_type_id`     =   '$add_voice_type',
                                `name`              =   '$name',
                                `position`          =   '$add_voice_pos'
                        WHERE   `id`                =   '$id'
                    ");

        $db->execQuery();
        $data = array('error' => 0, 'message' => 'updated');

    break;
    case 'disable_voice_item':

        $id		= $_REQUEST['id'];
        
        $db->setQuery(" UPDATE  `outgoing_campaign_request_voice` 
                        SET     `actived` = 0 
                        WHERE   `id` IN($id)");
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'removed');

    break;
    case 'disable_upload_item':  

        $id		= $_REQUEST['id'];

        $db->setQuery(" UPDATE  `outgoing_campaign_request_details` 
                        SET     `actived` = 0 
                        WHERE   `id` IN($id)");
        $db->execQuery();

        $db->setQuery(" UPDATE  `outgoing_campaign_request_base` 
                        SET     `actived` = 0 
                        WHERE   `outgoing_campaign_request_details_id` IN($id)");
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'removed');

    break;
    case 'get_campaign_select': 
        
        $numbers    = $_REQUEST['numbers'];

        $page		= GetCampaignSelectPage(getNumbersCount($numbers));
        $data		= array('page'	=> $page);

    break;
    case 'insert_into_base': 

        $phones = $_REQUEST['phones'];

        $operators = $_REQUEST['operators'];
        $campany    = $_REQUEST['campany'];

        $db->setQuery(" INSERT INTO     outgoing_campaign_request_details  
                                        (`outgoing_campaign_request_id`,
                                        `datetime`,
                                        `comment`)  
                            VALUES      ('$campany',
                                        NOW(),
                                        'ხელით ატვირთული')");

        $db->execQuery();
        $last_id = $db->getLastId();

            $j = 0;
        foreach($operators as $op){
            for($i = 0; $i < $op['count']; $i++){
                $db->setQuery("INSERT INTO `outgoing_campaign_request_base`
                            (`outgoing_campaign_request_id`, `outgoing_campaign_request_details_id`, `phone_number`, `operator_id`, `tree_id`, `sub_tree_id`, `outgoing_campaign_request_status_id` ) 
                                
                            VALUES ('$campany', '$last_id', '$phones[$j]', '$op[id]', 1, 3, 1)");

                $db->execQuery();
                $j++;
            }
            
        }



    break;
    case 'get_campaign_insert_percentage':
        
        $numbers        = $_REQUEST['numbers'];
        $campaign_id    = $_REQUEST['campaign_id'];
        $operators      = $_REQUEST['users'];

        $page		= GetCampaignPercentage(getUsersPercents($operators, $campaign_id));
       
        $data		= array('page'	=> $page);

    break;
    case 'get_campaign_insert':
        
        $numbers        = $_REQUEST['numbers'];
        $campaign_id    = $_REQUEST['campaign_id'];


		$page		= GetCampaignPage(getUsers($campaign_id));
        $data		= array('page'	=> $page);

    break;
    case 'add_in_campaign':
        $numbers                = $_REQUEST['numbers'];

        add_in_campaign($numbers);
    break;
    case 'create_campaign': 

            $campaign_name          = "CRM";
            $campaign_cat           = 1;
            $campaign_com_channel   = 1;
            $campaign_connect_type  = 2;
            $campaign_start_date    = '2020-05-14 07:00:00';
            $campaign_end_date      = '2020-05-14 08:00:00';
            $campaign_comment       = '';
            $campaign_calendar_id   = 16;
            $campaign_parameter_id  = 7;
            $campaign_group_id      = 7;
            $numbers                = $_REQUEST['numbers'];            
           
            create_campaign($user_id, $campaign_name, $campaign_cat, $campaign_com_channel, $campaign_connect_type, $campaign_start_date, $campaign_end_date, $campaign_comment, $campaign_calendar_id, $campaign_parameter_id, $campaign_group_id, $numbers);

    break;
    case 'get_settings_upload':
        
        $id          =      $_REQUEST['id'];
        $columnCount = 		$_REQUEST['count'];
        $json_data   = 		json_decode($_REQUEST['add'],true);
        $itemPerPage = 		$json_data['pageSize'];
        $skip        = 		$json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];

        $db->setQuery(" SELECT	    `details`.`id` AS `id`,
                                    `details`.`datetime` AS `datetime`,
                                    `details`.`comment` AS `comment`,
                                    ROUND(SUM(IF(`base`.`outgoing_campaign_request_status_id` = 4, 1, 0)) / COUNT(*) * 100, 2) AS `status`,
                                    COUNT(`base`.`id`) AS `count` 
                        FROM 		`outgoing_campaign_request_base` AS `base`
                        LEFT JOIN 	`outgoing_campaign_request_details` AS `details` ON `details`.`id` = `base`.`outgoing_campaign_request_details_id` 
                        WHERE 		`base`.`outgoing_campaign_request_id` = '$id'  AND `base`.`actived` = 1
                        GROUP BY 	`base`.`outgoing_campaign_request_details_id`   ");

        $db->execQuery();

        $result = $db->getKendoList($columnCount,$cols);
        $data = $result;

    break;
    case 'get_setting_voice_add':
        $id		= $_REQUEST['id'];
        $conn_type		= $_REQUEST['conn_type'];

        $page		= Get_voice_add_page(get_voice_data($id), $conn_type);
        
        $data		= array('page'	=> $page);

        break;
    case 'get_setting_upload_add':
        $id		= $_REQUEST['id'];

        $page		= Get_upload_add_page($id);

        $data		= array('page'	=> $page);

        break;
    case 'add_settings_upload':

        $id         = $_REQUEST['id'];
        $nameArr    = $_REQUEST['name'];

        $db->setQuery(" INSERT INTO     outgoing_campaign_request_details  
                                        (`outgoing_campaign_request_id`,
                                        `datetime`,
                                        `comment`)  
                            VALUES      ('$id',
                                        NOW(),
                                        'ხელით ატვირთული')");

        $db->execQuery();
        $last_id = $db->getLastId();

        $value = '(';

        foreach($nameArr as $key => $name){
            
            $value .= "'".$name['value']."',";
        }
        $trimmed_value = rtrim($value, ', ');

        $trimmed_value .= ', '.$id.', '.$last_id.', 1)';
       
        $count = count($nameArr);

        $into = '(`phone_number`, ';
        for($c = 1; $c < $count; $c++){
            $into .= '`number_';
            $into .= $c;
            $into .= "`, ";
        }

        $trimmed_into = rtrim($into, ', ');

        $trimmed_into .= ', `outgoing_campaign_request_id`, `outgoing_campaign_request_details_id`, `outgoing_campaign_request_status_id`)';


        // print var_dump($trimmed_value, $trimmed_into);

        $db->setQuery(" INSERT  INTO `outgoing_campaign_request_base`
                                    $trimmed_into
                                VALUES
                                    $trimmed_value");

        $db->execQuery();

        $data = array('error' => 0, 'message' => 'მოთხოვნა წარმატებით დასრულდა');


    break;
    case 'get_processing': 
        $id     = $_REQUEST['id'];
        $type   = $_REQUEST['type'];

        $dialog = new automator();
        if($type == 'outgoing'){
            $callT = 'outgoing';
            $page = $dialog->getDialog($id,$phone,'incomming_request_processing','incomming_request_id');
        }
        else{
            $callT = 'autodialer';
            $page = $dialog->getDialog($id,$phone,'compaign_request_processing','base_id');
        }
        $pre_page = '<div id="dialog-form">
        <div class="dialog-style">
        <div class="communication_fieldset_style">
            <div id="side_menu" class="communication_side_menu">
                <span class="info communication_side_menu_button" onclick="show_right_side(\'info\')"><img class="communication_side_menu_img"  style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/lemons_info.png" alt="24 ICON" title="ინფო"></span>
            
                

                <span class="sms communication_side_menu_button" onclick="show_right_side(\'sms\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_sms.png" alt="24 ICON" title="ესემეს"></span>
                
                <span class="incomming_mail communication_side_menu_button" onclick="show_right_side(\'incomming_mail\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_mail.png" alt="24 ICON" title="ელ. ფოსტა"></span>

                
                
                </div>
            </div>
        <div class="communication_right_side" style="float:left;" id="right_side">
        <ff class="communication_right_side_info" id="info">';
        $after_page = '</ff>
        <ff style="display:none; height: 656px;" id="sms">
					
            <div id="task_button_area" style="margin-left: 7px;">
                <button id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
            </div>
            <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                <table class="display" id="table_sms" >
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 20%;">თარიღი</th>
                            <th style="width: 15%;">ადრესატი</th>
                            <th style="width: 50%;">ტექსტი</th>
                            <th style="width: 15%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </ff>
        <ff style="display:none; height: 656px;" id="incomming_mail">
            <div style="margin-left: 7px;" id="task_button_area">
                <button id="add_mail">ახალი E-mail</button>
            </div>
            <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                <table class="display" id="table_mail" >
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 20%;">თარიღი</th>
                            <th style="width: 25%;">ადრესატი</th>
                            <th style="width: 35%;">გზავნილი</th>
                            <th style="width: 20%;">სტატუსი</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                            </th>
                            <th>
                                <input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </ff>
        </div></div></div></div><input type="hidden" value="'.$callT.'" id="dialogType"><input type="hidden" value="'.$id.'" id="hidden_incomming_call_id">';
        $data		= array('page'	=> $pre_page.$page.$after_page);

    break;
    case 'save_incomming':
        $id                     = $_REQUEST['id'];
        $inc_id                 = $_REQUEST['hidden_incomming_call_id'];
        $user		            = $_SESSION['USERID'];
        $c_date		            = date('Y-m-d H:i:s');
        
        $call_date			    = $_REQUEST['call_date'];
        $hidden_user		    = $_REQUEST['hidden_user'];
        $hidden_inc			    = $_REQUEST['hidden_inc'];

        $incomming_cat_1	    = $_REQUEST['incomming_cat_1'];
        $incomming_cat_1_1	    = $_REQUEST['incomming_cat_1_1'];
        $incomming_cat_1_1_1    = $_REQUEST['incomming_cat_1_1_1'];
        $incomming_status_1	    = $_REQUEST['incomming_status_1'];
        $incomming_status_1_1   = $_REQUEST['incomming_status_1_1'];

        $s_u_mail			    = $_REQUEST['s_u_mail'];
        
        $chat_language		    = $_REQUEST['chat_language'];
        $inc_status_a		    = $_REQUEST['inc_status_a'];
        $company			    = $_REQUEST['company'];
        $space_type			    = $_REQUEST['space_type'];
        
        $s_u_mail			    = $_REQUEST['s_u_mail'];
        $ab_person_name         = $_REQUEST['s_u_name'];
        $lid        		    = $_REQUEST['lid'];
        $fb_link        	    = $_REQUEST['fb_link'];
        $viber_address          = $_REQUEST['viber_address'];
        $vizit_datetime         = $_REQUEST['vizit_datetime'];
        $vizit_location         = $_REQUEST['vizit_location'];
        $client_comment         = $_REQUEST['client_comment'];
        $out_comment            = $_REQUEST['out_comment'];
        
        $client_sex			    = $_REQUEST['client_sex'];
        $client_birth_year	    = $_REQUEST['client_birth_year'];
        $s_u_status			    = $_REQUEST['s_u_status'];
        $call_content		    = htmlspecialchars($_REQUEST['call_content'], ENT_QUOTES);
        $lid_comment            = $_REQUEST['lid_comment'];
        $processing_start_date  = $_REQUEST['processing_start_date'];

        $phone                  = $_REQUEST['phone'];
        $phone1                 = $_REQUEST['phone1'];


        $db->setQuery(" SELECT  `incomming_call_id`
                        FROM    `outgoing_campaign_request_base`
                        WHERE  	`incomming_call_id` = '$inc_id' AND `incomming_call_id` IS NOT NULL");
        $check_id = $db->getResultArray();
        
        $incomming_id = $check_id['result'][0]['incomming_call_id'];

        if($incomming_id == ''){
            
            Addincomming($id, $inc_id, $user, $c_date, $call_date, $hidden_inc, $incomming_cat_1, $incomming_cat_1_1, $incomming_cat_1_1_1, $incomming_status_1, $incomming_status_1_1,  $chat_language, $inc_status_a, $company, $space_type, $s_u_mail, $ab_person_name, $lid, $fb_link, $viber_address, $vizit_datetime, $vizit_location, $client_comment, $out_comment, $client_sex, $client_birth_year, $s_u_status, $call_content, $lid_comment, $processing_start_date, $phone, $phone1);
        
        }else {

            Saveincomming($id, $inc_id, $user, $c_date, $call_date, $hidden_inc, $incomming_cat_1, $incomming_cat_1_1, $incomming_cat_1_1_1, $incomming_status_1, $incomming_status_1_1,  $chat_language, $inc_status_a, $company, $space_type, $s_u_mail, $ab_person_name, $lid, $fb_link, $viber_address, $vizit_datetime, $vizit_location, $client_comment, $out_comment, $client_sex, $client_birth_year, $s_u_status, $call_content, $lid_comment, $processsing_start_date, $phone, $phone1);
       
        }


        $data = array('error' => 0, 'message' => 'done');
    
    break;
    case 'status_2':
	    $page		= get_status_1_1($_REQUEST['status_id'],'');
	    $data		= array('page'	=> $page);
	    
    break;
    case 'get_list_info_project' :
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];
        
        $id           = $_REQUEST['id'];
        
        $db->setQuery("SELECT   incomming_call_info.id,
                                incomming_call_info.id,  
                                'პროქტები' AS `name`,
                                CONCAT('პროექტი: ', directory__projects.project, ' კატეგორია: ', directory__projects.category, ' ხედი: ', directory__projects.`view`, ' ნომერი: ', directory__projects.number, ' სართული: ', directory__projects.floor, ' სექტორი: ', directory__projects.sector, ' საკადასტრო კოდი: ', directory__projects.cadr_code, ' საზაფხულო ფართი: ', directory__projects.saz_far, ' საცხოვრებელი ფართი: ', directory__projects.sacx_far, ' ფასი: ', directory__projects.price, ' აივანი: ', directory__projects.balcony, ' საერთო ფართი: ', directory__projects.saerto_far, ' სრული ფასი: ', directory__projects.full_price, ' განვადების ფასი: ', directory__projects.ganv_price, ' სტანდარტული წინასწარ: ', directory__projects.stand_winaswar, ' სტატუსი: ', directory__projects.`status`) AS `info`
                        FROM   `incomming_call_info`
                        -- JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
                        JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
                        WHERE  `incomming_call_info`.`actived` = 1 AND `incomming_call_info`.`incomming_call_id` = $id");
        
        $data = $db->getList($count, $hidden);
        
    break;
    case 'get_list_history' :
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];

        $start_check  = $_REQUEST['start_check'];
        $end_check    = $_REQUEST['end_check'];
        $phone        = $_REQUEST['phone'];
        $s_u_user_id  = $_REQUEST['s_u_user_id'];

        $filt = '';
        if($phone == '' && $s_u_user_id == ''){
            $filt="AND inc.client_user_id = 'fddhgdhgd'";
        }

        if ($phone != '' && $s_u_user_id != '') {
            $filt="AND inc.client_user_id = '$s_u_user_id' OR inc.phone = '$phone'";
        }

        if ($phone != '' && $s_u_user_id == '') {
            $filt="AND inc.phone = '$phone'";
        }

        if ($phone == '' && $s_u_user_id != '') {
            $filt="AND inc.client_user_id = '$s_u_user_id'";
        }

        $db->setQuery(" SELECT    `inc`.`id` AS `task_id`,
                                    `inc`.`date` AS `date`,
                                    IF(`inc`.`phone` = '2004', `inc`.`ipaddres`, `inc`.`phone`) AS `phone`,
                                    user_info.`name`,
                                    `inc`.`call_content`,
                                    IF(inc.inc_status_id=0,'დაუმუშავებელი',inc_status.`name`) AS `status`
                        FROM      `incomming_call` AS `inc`
                        LEFT JOIN  user_info ON `inc`.`user_id` = user_info.user_id
                        LEFT JOIN  inc_status  ON inc.inc_status_id = inc_status.id
                        WHERE      DATE(inc.date) BETWEEN '$start_check' AND '$end_check' $filt");

        $data = $db->getList($count, $hidden);

        break;
    case 'get_list_quest' :
        $count = 		$_REQUEST['count'];
        $hidden = 		$_REQUEST['hidden'];
        
        
        
        $db->setQuery(" SELECT 	queries.id,
                                queries.datetime,
                                directory__projects.`project`,
                                queries.`quest`,
                                queries.`answer`
                        FROM 	queries
                        LEFT JOIN user_info ON user_info.user_id = queries.user_id
                        LEFT JOIN directory__projects ON directory__projects.id = queries.project_id
                        WHERE 	queries.actived=1 ");
        
        $data = $db->getList($count, $hidden);
    break;
    case 'get_list_news' :
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];
        
        $db->setQuery(" SELECT action.id,
                                action.start_date,
                                action.end_date,
                                action.`name`,
                                (SELECT GROUP_CONCAT(my_web_site.`name`) 
                                FROM  `action_site`
                                JOIN   my_web_site ON action_site.site_id = my_web_site.id
                                WHERE  action_site.action_id = action.id) AS `site`,
                                action.content
                        FROM   action
                        WHERE  action.actived=1 AND action.end_date >= DATE(CURDATE())");
        
        $data = $db->getList($count, $hidden,1);
    
    break;
    case 'get_list_sms':
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];
        $incomming_id = $_REQUEST['incomming_id'];
        
        if ($incomming_id != '') {
            $db->setQuery(" SELECT id,
                                   date,
                                   phone,
                                  `content`,
                                   if(`status`=1,'გასაგზავნია',IF(`status`=2,'გაგზავნილია',''))
                            FROM  `sent_sms`
                            WHERE  incomming_call_id = '$incomming_id'");
            
            $data = $db->getList($count, $hidden);
        }else{
            $data = '';
        }
        break;
    case 'add_project' :
        $id      = $_REQUEST['id'];
        //$letters = json_decode('[' . $_REQUEST['lt'] . ']');
        $letters = explode(",",$_REQUEST['lt']);
        
        $db->setQuery("DELETE FROM incomming_call_info WHERE incomming_call_id = $id");
        $db->execQuery();
        
        foreach ($letters AS $pr_id){
            $db->setQuery("INSERT INTO `incomming_call_info`
                                        (`user_id`, `datetime`, `incomming_call_id`, `directory_tables_id`, `directory_row_id`, `actived`) 
                                VALUES 
                                        (1, NOW(), $id, 3, '$pr_id', 1);");
            $db->execQuery();
        }
        
        break;
        case 'get_user_list':

            $users		= getUsersFromCampany(2);
            $data		= array('users'	=> $users);

        break;
        case 'modal_number_save':

            $operator   =   $_REQUEST['user'];
            $phone      =   $_REQUEST['number'];

            $db->setQuery(" SELECT `outgoing_campaign_request_details`.`datetime`
                            FROM        `outgoing_campaign_request_base`
                            LEFT JOIN   `outgoing_campaign_request_details` ON `outgoing_campaign_request_details`.`id` = `outgoing_campaign_request_base`.`outgoing_campaign_request_details_id`
                            WHERE `phone_number` = '$phone' AND DATE(`outgoing_campaign_request_details`.`datetime`) = CURDATE()");
            $count = $db->getNumRow();

if($count > 0){
    $data = array('message' => 'ნომერი უკვე დამატებულია!', 'status' => 0);
}else{
    $db->setQuery(" INSERT INTO outgoing_campaign_request_details  
                            (`outgoing_campaign_request_id`,
                            `datetime`,
                            `comment`)  
                        VALUES      (2,
                            NOW(),
                            'ხელით ატვირთული')");

    $db->execQuery();
    $last_id = $db->getLastId();

    $db->setQuery(" INSERT INTO `outgoing_campaign_request_base`
        (`outgoing_campaign_request_id`, `outgoing_campaign_request_details_id`, `phone_number`, `operator_id`, `tree_id`, `sub_tree_id`, `outgoing_campaign_request_status_id` ) 
    VALUES 
        (2, '$last_id', '$phone', '$operator', 1, 3, 1)");

    $db->execQuery();

    $data = array('message' => 'ნომერი წარმატებით დაემატა', 'status' => 1);
}
            

            
        break;
	default:
		$error = 'Action is Null';
}


// send array
echo json_encode($data);

//-----------------------------------------------------------------------------------
// FUNCTIONS 
//-----------------------------------------------------------------------------------
function get_tabs($res){
    $datafull="";
    $data2='';
    $data='<div id="tab_0" class="main_tab" name="0"><ul>';
    foreach ($res as $arr) {
        if($arr['parent_id']==0){
            $data   .=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" >
                            <a>'.$arr['name'].'</a>
                            <span id="input_'.$arr['id'].'"><x>0</x></span>
                        </li>';
            $data2  .='</ul>
                            </div><div id="tab_'.$arr['id'].'" class="tab_'.$arr['id'].'" name="'.$arr['id'].'">
                        <ul>';
            $child_req = get_child_status($arr['id']);
            foreach($child_req AS $child_res){
                $data2.=get_child_tabs($child_res,$child_res['parent_id']);
            }
        }
    }
    $data.='</ul></div>';
    $data2.='</ul></div>';
    $datafull .=  $data;
    $datafull .=  $data2;
    return $datafull;
}



function get_child_tabs($arr,$id){
    $data='';
    if($arr['parent_id']==$id && $arr['parent_id'] != 0 )
        $data.=' <li id="t_'.$arr['id'].'" name="'.$arr['id'].'" id_select="'.$id.'"  class="child">
                        <a>'.$arr['name'].'</a><span id="input_'.$arr['id'].'"><x>0</x></span>
                </li>';
        
    return $data;
}


function get_child_status($id){
    global $db;
    $db->setQuery(" SELECT  id,
                            name,
                            parent_id
                    FROM `tree`
                    WHERE actived=1 AND parent_id = '$id' AND type_id='1'");
    $res = $db->getResultArray();
    return $res['result'];
}


function addWhere($where, $add, $and = true) {
	if ($where) {
		if ($and) $where .= " AND $add";
		else $where .= " OR $add";
	}
	else $where = $add;
	return $where;
}


function getSelectors($table)
{
	GLOBAL $db;
	$query = "
				SELECT `name`,`id` 
				FROM   $table 
				ORDER BY id ASC
	";
	
	$db -> setQuery($query);
	$result = $db -> getResultArray();
	
	$selectorData = array();
	
	foreach($result['result'] AS $option)
	{
		array_push($selectorData,array('text'=>$option['name'],'value'=>$option['id']));
	}
	
	return $selectorData;
}

function get_compaign_data($id)
{
    GLOBAL $db;
    $db->setQuery(" SELECT  id,
                            name,
                            outgoing_campaign_active_status_id AS 'status',
                            default_compaign
                    FROM outgoing_campaign_request
                    WHERE actived = 1 AND id='$id'");
    $result = $db->getResultArray();

    return $result['result'][0];
}


function edit_campaign($id, $user_id, $campaign_name, $campaign_cat, $campaign_com_channel, $campaign_connect_type, $campaign_start_date, $campaign_end_date, $campaign_comment, $campaign_calendar_id, $campaign_parameter_id, $campaign_group_id){

    global $db;

    $group_id = $_SESSION['USERGR'];

    $db->setQuery(" SELECT  `default_compaign` 
                    FROM    `outgoing_campaign_request`
                    WHERE   `id` = '$id'");

    $check = $db->getResultArray();
    $default = $check['result'][0]['default_compaign'];

    if($default == 0){
        $name  =  "`name` = '$campaign_name',";
    }elseif($default == 1 AND $group_id == 1){
        $name  =  "`name` = '$campaign_name',";
    }else{
        $name  = "";
    }

    $db->setQuery(" UPDATE  `outgoing_campaign_request` 
                    SET     `datetime`                                           = NOW(),
                            `user_id`                                            = '$user_id',
                                $name
                            `outgoing_campaign_category_id`                      = '$campaign_cat', 
                            `outgoing_campaign_type_id`                          = '$campaign_connect_type',
                            `outgoing_campaign_source_id`                        = '$campaign_com_channel', 
                            `start_datetime`                                     = '$campaign_start_date', 
                            `end_datetime`                                       = '$campaign_end_date',
                            `comment`                                            = '$campaign_comment', 
                            `outgoing_campaign_settings_calendar_schedule_id`    = '$campaign_calendar_id',
                            `outgoing_campaign_settings_parameters_id`           = '$campaign_parameter_id',
                            `outgoing_campaign_settings_group_schedule_id`       = '$campaign_group_id'
                    WHERE   `id` = '$id'");

    $db->execQuery();

}



function add_in_campaign($numbers){
    global $db;

    $values = "";
    
    foreach($numbers as $key => $number){
        $values .= "($number, 7, 2),";
        
    }

    $trimmed = rtrim($values, ', ');    

    $db->setQuery("INSERT INTO outgoing_campaign_request_base 
                            (`phone_number`,`sub_tree_id`, `outgoing_campaign_request_id`)
                            VALUES
                            $trimmed");

    $db->execQuery();
}
function GetSelectorEditor($id,$selector){
    global $db;
	if($selector == ''){
        $val = '';
    }
    else{
        $db->setQuery(" SELECT  table_name
                        FROM    outgoing_campaign_inputs
                        WHERE   id = '$id'");
        $res = $db->getResultArray();
        $tableName = $res['result'][0]['table_name'];
        $db->setQuery(" SELECT  name
                        FROM    $tableName
                        WHERE   id = '$selector'");
        $res = $db->getResultArray();
        $val = $res['result'][0]['name'];
    }
    $data  .= '
    <div id="dialog-form">
        <fieldset>
            <legend>ინფორმაცია</legend>
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                <tr>
                    <td style="width: 170px;"><label for="CallType" style="margin-top: 8px;">პარამეტრის დასახელება</label></td>
                    <td style="width: 300px;">
                        <input style="width:220px;" type="text" id="param_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $val . '" />
                    </td>
                </tr>
                <tr style="height:5px;"></>
                
            </table>
            

        </fieldset>
		<!-- ID -->
        <input type="hidden" id="selector_id" value="' . $selector . '" />
    </div>';

	return $data;
}
function create_campaign($user_id, $campaign_name, $campaign_cat, $campaign_com_channel, $campaign_connect_type, $campaign_start_date, $campaign_end_date, $campaign_comment, $campaign_calendar_id, $campaign_parameter_id, $campaign_group_id, $numbers){

    global $db;


    $db->setQuery(" INSERT  INTO    `outgoing_campaign_request`
                                    (`datetime`,
                                    `user_id`,
                                    `name`, 
                                    `outgoing_campaign_category_id`, 
                                    `outgoing_campaign_type_id`, 
                                    `outgoing_campaign_source_id`, 
                                    `start_datetime`, 
                                    `end_datetime`,
                                    `comment`, 
                                    `outgoing_campaign_active_status_id`,
                                    `outgoing_campaign_settings_calendar_schedule_id`,
                                    `outgoing_campaign_settings_parameters_id`,
                                    `outgoing_campaign_settings_group_schedule_id`) 
                            VALUE
                                    (NOW(),
                                    '$user_id',
                                    '$campaign_name', 
                                    '$campaign_cat', 
                                    '$campaign_connect_type', 
                                    '$campaign_com_channel', 
                                    NOW(), 
                                    NOW(), 
                                    '$campaign_comment', 
                                    1,
                                    '$campaign_calendar_id',
                                    '$campaign_parameter_id',
                                    '$campaign_group_id')");

    $db->execQuery();
    $get_last = $db->getLastId();
    
    $values = "";
    
    foreach($numbers as $key => $number){
        $values .= "($number, '7', $get_last),";
        
    }
    $trimmed = rtrim($values, ', ');    

    $db->setQuery("INSERT INTO outgoing_campaign_request_base 
                            (`phone_number`,`sub_tree_id`, `outgoing_campaign_request_id`)
                            VALUES
                            $trimmed");

    $db->execQuery();

    // print var_dump();

}

function add_campaign($id, $user_id, $campaign_name, $campaign_cat, $campaign_com_channel, $campaign_connect_type, $campaign_start_date, $campaign_end_date, $campaign_comment, $campaign_calendar_id, $campaign_parameter_id, $campaign_group_id){

    global $db;

    $db->setQuery(" INSERT  INTO    `outgoing_campaign_request`
                                    (`id`,
                                    `datetime`,
                                    `user_id`,
                                    `name`, 
                                    `outgoing_campaign_category_id`, 
                                    `outgoing_campaign_type_id`, 
                                    `outgoing_campaign_source_id`, 
                                    `start_datetime`, 
                                    `end_datetime`,
                                    `comment`, 
                                    `outgoing_campaign_active_status_id`,
                                    `outgoing_campaign_settings_calendar_schedule_id`,
                                    `outgoing_campaign_settings_parameters_id`,
                                    `outgoing_campaign_settings_group_schedule_id`) 
                            VALUE
                                    ($id,
                                     NOW(),
                                    '$user_id',
                                    '$campaign_name', 
                                    '$campaign_cat', 
                                    '$campaign_connect_type', 
                                    '$campaign_com_channel', 
                                    '$campaign_start_date', 
                                    '$campaign_end_date', 
                                    '$campaign_comment', 
                                    1,
                                    '$campaign_calendar_id',
                                    '$campaign_parameter_id',
                                    '$campaign_group_id')");

    $db->execQuery();

}

/**
 * @param int $req_id
 */
function get_campaign_settings_data($id) {

    global $db;

    $db->setQuery(" SELECT  `req`.`id`,
                            `req`.`name`,
                            `req`.`outgoing_campaign_category_id` AS `category_id`,
                            `req`.`outgoing_campaign_type_id` AS `type_id`,
                            `req`.`outgoing_campaign_source_id` AS `source_id`,
                            `req`.`outgoing_campaign_settings_calendar_schedule_id` AS `calendar_schedule_id`,
                            `req`.`outgoing_campaign_settings_group_schedule_id` AS `group_schedule_id`,
                            `req`.`outgoing_campaign_settings_parameters_id` AS `parameters_id`,
                            `req`.`outgoing_campaign_settings_parameters_id` AS `parameters_id`,
                            `req`.`start_datetime`,
                            `req`.`end_datetime`,
                            `req`.`comment`,
                            `req`.`default_compaign`,
                            `group`.`name` AS `group_schedule_name`,
                            `calendar`.`name` AS `calendar_schedule_name`,
                            `parameters`.`name` AS `parameter_schedule_name`,
                            `parameters`.`welcome_file_original` AS `welcome_file_original`
                        FROM
                            `outgoing_campaign_request` AS `req`
                        LEFT JOIN `outgoing_campaign_settings_group_schedules` as `group` ON  `group`.`id` = `req`.`outgoing_campaign_settings_group_schedule_id`
                        LEFT JOIN `outgoing_campaign_settings_calendar_schedules` as `calendar` ON  `calendar`.`id` = `req`.`outgoing_campaign_settings_calendar_schedule_id`
                        LEFT JOIN `outgoing_campaign_settings_parameters` as `parameters` ON  `parameters`.`id` = `req`.`outgoing_campaign_settings_parameters_id`
                        WHERE
                            `req`.`actived` = 1 
                                AND 
                            `req`.`id` = '$id'");
    $result = $db->getResultArray();

    return $result['result'][0];
}

/**
 * @param int $id
 */
function get_campaign_cats($id){
    global $db;

    $db->setQuery(" SELECT `id`, `name`
                    FROM   `outgoing_campaign_category`
                    WHERE   actived = 1");

    $data = $db->getSelect($id);

    return $data;
}

/**
 * @param int $id
 */
function get_campaign_com_channels($id){
    global $db;

    $db->setQuery(" SELECT `id`, `name_ka` as `name`
                    FROM   `source`
                    WHERE   actived = 1");

    $data = $db->getSelect($id);

    return $data;
}
/**
 * @param int $id
 */
function get_campaign_com_types($id){
    global $db;

    $db->setQuery(" SELECT `id`, `name`
                    FROM   `outgoing_campaign_type`
                    WHERE   actived = 1");

    $data = $db->getSelect($id);

    return $data;
}


function get_voice_types($id, $conn_type){
    global $db;

    if($conn_type == 2){
        $where = " WHERE `id` = 5";
    }

    $db->setQuery(" SELECT `id`, `name`
                    FROM   `outgoing_campaign_field_type`
                    $where");

    $data = $db->getSelect($id);

    return $data;
}


/**
 * group schedules
 */
function get_group_schedules(){

    global $db; 

    $db->setQuery(' SELECT  `id`, 
                            `name`
                    FROM    `outgoing_campaign_settings_group_schedules`
                    WHERE   `actived` = 1');

    $db->execQuery();
    $result = $db->getResultArray();

    $data = '<li data-search=1 style="padding: 0;"><input type="text" class="schedule_search" data-name="group_selector" value /></li>';

    

    if($result['count'] > 0){
        foreach($result['result'] as $s){
            $data .= '<li data-id="'.$s['id'].'">'.$s['name'].'<span class="delete_schedule" deletex data-name="operators" data-id="'.$s['id'].'"></span></li>';
        }
    }else{
        $data .= "<li data-id='0' style='text-align: center'>სია ცარიელია</li>";
    }
   

    return $data;
}

function get_calendar_schedules(){

    global $db; 

    $db->setQuery(' SELECT  `id`, 
                            `name`
                    FROM    `outgoing_campaign_settings_calendar_schedules`
                    WHERE   actived = 1');

    $db->execQuery();
    $result = $db->getResultArray();

    $data = '<li data-search=1 style="padding: 0;"><input type="text" class="schedule_search" data-name="calendar_selector" value /></li>';

    if($result['count'] > 0){
        foreach($result['result'] as $s){
            $data .= '<li data-id="'.$s['id'].'">'.$s['name'].'<span  class="delete_schedule" deletex data-name="calendar" data-id="'.$s['id'].'"></span></li>';
        }
    }else{
        $data .= "<li data-id='0' style='text-align: center'>სია ცარიელია</li>";
    }

    return $data;
}

/**
 *  Parameter Schedules
 */
function get_parameter_schedules(){
    global $db; 

    $db->setQuery(' SELECT  `id`, 
                            `name`
                    FROM    `outgoing_campaign_settings_parameters`
                    WHERE   `actived` = 1');

    $db->execQuery();
    $result = $db->getResultArray();

    $data = '<li data-search=1 style="padding: 0;"><input type="text" class="schedule_search" data-name="parameter_selector" value /></li>';

    if($result['count'] > 0){
        foreach($result['result'] as $s){
            $data .= '<li data-id="'.$s['id'].'">'.$s['name'].'<span  class="delete_schedule" deletex data-name="parameter" data-id="'.$s['id'].'"></span></li>';
        }
    }else{
        $data .= "<li data-id='0' style='text-align: center'>სია ცარიელია</li>";
    }


    return $data;
}

/**
 * @param array $res return
 */
function GetPage($res = '')
{
	$data = '
	<div id="dialog-form">
	    <fieldset style="border:none; border-bottom: 1px solid black; border-radius:0;">
            <div class="left" style="display: flex;">
                <p style="margin-right:4px;margin-top: 11px;margin-bottom: 0;font-weight: 900;font-size: 13px;"><img style="margin-right:5px;margin-bottom: 5px;" src="media/images/icons/star.png" />'.$res['name'].'</p>
                <table style="margin-left: 40px;">
                    <tr>
                        <td class="l_filter_date" style="width: 300px;">
                		<span style="margin: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 100px;height: 18px;margin-top: 17px;">
                            <label for="start_date" style="margin-top: 15px;">-დან</label>
                        </span>
                        <span style="margin: 5px 10px 0 0">
                            <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 100px;height: 18px;margin-top: 17px;">
                            <label for="end_date" style="margin-top: 15px;">-მდე</label>
                        </span>
                        </td>

                    </tr>
                  
                </table>
            </div>
            <div class="right">
                <button class="outgoing_button button_plus" id="button_plus"><img style="margin-bottom: 3px;" src="media/images/icons/play.png"> დაწყება</button>
                <button class="outgoing_button button_pause" id="button_pause"><img style="margin-bottom: 3px;" src="media/images/icons/pause.png"> პაუზა</button>
                <button class="outgoing_button button_stop" id="button_stop"><img style="margin-bottom: 3px;" src="media/images/icons/stop.png"> შეწვეტა</button>
                <button class="outgoing_button button_trash" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>
                <button class="outgoing_button button_archive" id="button_archive"><img style="margin-bottom: 3px;" src="media/images/icons/archive.png"> დაარქივება</button>
                <div id="setting_done" data-percent="75"></div>
                <button class="outgoing_button" id="button_option"><img style="margin-bottom: 3px;" src="media/images/icons/options.png"> პარამეტრები</button>
            </div>
        </fieldset>

        <fieldset style="border:none; border-radius:0;">
            <div id="tabs1" class="l_inline-tabs" style="display: inline-grid; width: 98%"></div>
            <br><br>
            <div id="grid2"></div>
        </fieldset>
        <input type="hidden" id="request_id" value="'.$res['id'].'">
        <input type="hidden" id="default_compaign" value="'.$res['default_compaign'].'">
    </div>
    ';
	return $data;
}
function get_input_types($id){
    global $db;
    $data = '';
    $db->setQuery("SELECT 	`id`, `name`
					FROM 	`processing_field_type`
					WHERE 	actived=1 AND id NOT IN (3,4,5,8,9,10)");
    
    
    $data = $db->getSelect($id);
    return $data;
}
function GetInputsEditor($id,$cat_id){
    global $db;
    
    $db->setQuery("SELECT name,field_type_id,position,`key`,depth,multilevel_1,multilevel_2,multilevel_3,necessary_input FROM outgoing_campaign_inputs WHERE id='$id'");
    $result = $db->getResultArray();
    $vars = $result['result'][0];

    if($vars['field_type_id'] == 6 or $vars['field_type_id'] == 7 or $vars['field_type_id'] == 8 or $vars['field_type_id'] == 9){
        $displaySelectorsKendo = 'block;';
    }
    else{
        $displaySelectorsKendo = 'none;';
    }
    if($vars['necessary_input'] == 1){
        $necessary = 'checked';
    }

    if($vars['field_type_id'] == 10){
        $displayMultiselect = 'contents;';
        $displayLevels = 'block;';
    }
    else{
        $displayMultiselect = 'none;';
        $displayLevels = 'none;';
    }


    if($vars['depth'] == 2){
        $selected2 = 'selected';
    }
    else if($vars['depth'] == 3){
        $selected3 = 'selected';
    }

    if($selected2 == 'selected' OR $selected3 == 'selected'){
        $displayTree = "block";
    }

    
    if($vars['position'] == ''){
        $db->setQuery(" SELECT 	IFNULL(MAX(position)+1,1) AS 'position'
                        FROM 	outgoing_campaign_inputs
                        WHERE	actived = 1 AND request_id = '$cat_id'");
        $input_pos = $db->getResultArray();

        $input_pos = $input_pos['result'][0]['position'];
    }
    else{
        $input_pos = $vars['position'];
    }
    $data  .= '
    <div id="dialog-form">
        <fieldset>
            <legend>ველის პარამეტრები</legend>
            
            <table class="dialog-form-table" id="signature_dialog_form_main_table">
                
                <tr id="input_namer">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ველის დასახელება</label></td>
                    <td style="width: 300px;">
                        <input type="text" style="width:200px;" id="input_name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $vars['name'] . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">ველის ტიპი</label></td>
                    <td>
                        <select  id="input_type" style="width: 200px; height: 28px; ">'.get_input_types($vars['field_type_id']).'</select>
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">აუცილებელი ველი</label></td>
                    <td>
                        <input style="margin-top: 3px!important;" type="checkbox" id="necessary" '.$necessary.'>
                    </td>
                   
                </tr>
                <tr style="height:5px"></tr>
                <tr>
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">პოზიცია</label></td>
                    <td style="width: 300px;">
                        <input style="width:20px;" type="text" id="input_position" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $input_pos . '" />
                    </td>
                </tr>
                <tr style="height:5px"></tr>
                <tr style="display:none;">
                    <td style="width: 200px;"><label style="margin-top: 8px;" for="CallType">id-class</label></td>
                    <td style="width: 300px;">
                        <input type="text" id="input_key" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $vars['key'] . '" />
                    </td>
                </tr>
                
            </table>
            <!-- ID -->
            <input type="hidden" id="input_id" value="' . $id . '" />
            <input type="hidden" id="req_id" value="' . $cat_id . '" />
        </fieldset>
        <fieldset style="display:'.$displaySelectorsKendo.'" id="selectors_type_inputs">
            <legend>სელექტორი/CHECKBOX/RADIO</legend>
            <div id="selectors_type_kendo"></div>
        </fieldset>
		
    </div>';

	return $data;
}

/**
 * @param array $res return;
 * @return array $data
 */
function GetOptPage($res = '')
{

    

    if(!$res['id']){
        global $db; 
    
        $db->setQuery("INSERT INTO outgoing_campaign_request
                                     (`name`)
                                     VALUES 
                                     ('')");

        $db->execQuery();
        $new_id = $db->getLastId();

        $db->setQuery("DELETE   FROM `outgoing_campaign_request`
                                WHERE id = '$new_id'");
        $db->execQuery();

        $action_status = false;
        
    }else{
        $new_id = $res['id'];
        $action_status = true;
    }

    $data = '
    <div id="dialog-form">
    
        <section id="dialog__top_section" class="dialog__top_seciton">
        <table class="dialog__top_section_table">
        <tr>
            <td class="dialog__top_section_stats">
            <div class="dialog__top_section_chart" id="graph" data-percent="75"></div>
            </td>
            <td class="dialog__top_section_fields">
                <table>
                  
                    <tr>
                        <td>
                            <label>შეიყვანეთ კამპანიის დასახელება ან აირჩიეთ <a class="permalink">შაბლონიდან</a></label>
                            <input type="text" id="campaign_name" value="'.$res['name'].'" />
                        </td>
                        <td>
                            <label>კამპანიის კატეგორია</label>
                            <select id="campaign_cat" style="width: 120px">
                                '.get_campaign_cats($res['category_id']).'
                            </select>
                        </td>
                        <td>
                            <label>კომუნიკაციის არხი</label>
                            <select id="campaign_com_channel">
                                '.get_campaign_com_channels($res['source_id']).'
                            </select>
                        </td>
                        <td>
                            <label>დაკავშირების ტიპი</label>
                            <select id="campaign_connect_type">
                            '.get_campaign_com_types($res['type_id']).'
                            </select>
                        </td>
                    </tr>
                    <tr>
                    <td>
                        <label>დასაწყისი</label>
                        <input value="'.$res['start_datetime'].'" class="callapp_filter_body_span_input" type="text" id="campaign_start_date" >
                    </td>
                    <td>
                        <label>დასასრული</label>
                        <input value="'.$res['end_datetime'].'" class="callapp_filter_body_span_input" type="text" id="campaign_end_date" >
                    </td>
                   
                </tr>
                </table>
            </td>
        </tr>
        </table>
        </section>

        <section id="dialog__second_section" class="dialog__second_section">
            <table class="dialog__second_section_table">
                <tr>
                    <td class="dialog__second_section_tablebar">
                        <div class="dialog__second_section_item" id="second_operators" title="ოპერატორები" data-status="'.$action_status.'" data-name="get_group_schedule" data-id="'.$res['group_schedule_id'].'" aria-selected="true"></div>
                        <div class="dialog__second_section_item" id="second_settings" title="პარამეტრები" data-status="'.$action_status.'"  data-name="get_parameters" data-id="'.$res['parameters_id'].'"> </div>
                        <div class="dialog__second_section_item" id="second_calendar" title="კალენდარი" data-status="'.$action_status.'"  data-name="get_calendar_schedule" data-id="'.$res['calendar_schedule_id'].'"> </div>
                        <div class="dialog__second_section_item" id="second_mic" title="ჩანაწერები" data-status="'.$action_status.'"  data-name="get_settings_voices" data-id="'.$res['id'].'"> </div>
                        <div class="dialog__second_section_item" id="second_upload" title="ატვირთვა" data-status="'.$action_status.'"  data-name="get_settings_upload" data-id="'.$res['id'].'" > </div>
                        <div class="dialog__second_section_item second_edit2" id="second_edit" title="რედაქტირება" data-status="'.$action_status.'"  > </div>
                    </td>
                    <td class="dialog__second_section_tablecontent">

                        <fieldset class="dialog__second_section_content" id="second_operators" aria-selected="true">

                            <div id="create_group" class="schedule_create">
                                <span class="button_loading" />
                                შეინახე შაბლონად
                                <div id="group_button_down" class="button_down" data-status="0">
                                    <input type="text" id="group_schedule_name" value="'.$res['group_schedule_name'].'"  placeholder="შაბლონის სახელი">
                                    <button id="create_group_button">შენახვა</button>
                                </div>
                            </div>

                            <div id="select_group" class="schedule_select">
                                <span class="button_loading" />
                                აირჩიე შაბლონი

                                <ul id="group_selector" data-status="0" class="select_schedule">
                                    
                                    '.get_group_schedules().'
                                </ul>
                            </div>
                            <span class="block_settings_content"></span>
                            <div>
                                <select id="customers" style="width: 100%;float: left; display: none;" data-placeholder="აირჩიეთ მომხმარებლები"></select>
                            </div>
                            <message></message>
                           <div style="display: inline-block; margin-top:13px;">
                            <span style="font-size: 13px;font-family: BPG; display: none" id="ReleasedCall" data-oldcount="2" data-count="2">განაწილებული: <span>2</span></span>
                                <br />
                                <span style="font-size: 13px;font-family: BPG; display: none" id="notReleasedCall" data-oldcount="2" data-count="0">გასანაწილებელი: <span>0</span></span>

                                <div id="relocateUserCall"></div>
                            </div> 
                        </fieldset>


                        <fieldset class="dialog__second_section_content" id="second_settings">
                        
                            <div id="save_parameters" class="schedule_create">
                                <span class="button_loading" />
                                შეინახე შაბლონად
                                <div id="parameters_button_down" class="button_down" data-status="0">
                                    <input type="text" id="parameter_schedule_name" value="'.$res['parameter_schedule_name'].'"  placeholder="შაბლონის სახელი">
                                    <button id="save_parameters_button">შენახვა</button>
                                </div>
                            </div>

                            <div id="select_parameters" class="schedule_select">
                                <span class="button_loading" />
                                აირჩიე შაბლონი

                                <ul id="parameter_selector" data-status="0" class="select_schedule">
                                '.get_parameter_schedules().'
                            </ul>
                            </div>

                            <span class="block_settings_content"></span>
                            <table class="dialog__second_section_content_table">
                                <tr id="section_first_call">
                                    <td label >პირველი ზარი შევიდეს <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <div class="radio_inline">
                                            <div class="custom_radio">
                                                <input type="radio" id="parameter_operator" name="first_call" value="1">
                                                <span class="checkmark"></span>
                                                <label for="parameter_operator">ოპერატორი</label>
                                            </div>
                                            <div class="custom_radio">
                                                <input type="radio" id="parameter_client" name="first_call" value="2">   
                                                <span class="checkmark"></span> 
                                                <label for="parameter_client">აბონენტი</label>
                                                </div>
                                            </div>
                                        <div class="input__withlabel">
                                            <label>ატვირთეთ მისასალმებელი ხმოვანი ფაილი <span help-icon title="გამარჯობა მეგობარო" /></label>
                                            <input type="text" id="compaign_parameters_file_name" value="'.$res['welcome_file_original'].'"/>
                                            <input type="file" id="compaign_parameters_file" name="image_upload">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td label>ლოდინის ხანგრძლივობა პასუხამდე <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="wait_time_before_answer" max="45" style="width: 60px" /> წმ
                                    </td>
                                </tr>
                                <tr>
                                    <td label>მცდელობების რ-ბა <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="attempt_count" max="5" style="width: 60px" /> 
                                    </td>
                                </tr>

                                <tr class="only_default" style="display: none">
                                    <td label>ნომრების ატვირთვის პერიოდი <span help-icon title="" /></td>
                                    <td content>
                                        <input type="text" id="missed_call_upload_time" style="width: 73px; border: 1px solid #D1D1D1;" value="00:00:00" /> 
                                    </td>
                                </tr>

                                <tr class="only_default">
                                    <td label>მინიმუმ რამდენ წამს იცდიდა გათიშვამდე <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="min_off" max="5" style="width: 60px" /> 
                                    </td>
                                </tr>
                                <tr class="only_default">
                                    <td label>მაქსიმუმ რამდენ წამს იცდიდა გათიშვამდე <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="max_off" max="5" style="width: 60px" /> 
                                    </td>
                                </tr>
                                <tr class="only_default">
                                    <td label>რამდენ წუთში აიტვირთოს დასარეკთა ბაზაში <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="to_call_time" max="5" style="width: 60px" /> 
                                    </td>
                                </tr>

                                
                                
                                <tr>
                                    <td label>ზარების განხორციელების წესი <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                       <select id="call_type">
                                        <option value="1">მიყევი სიას, შემდეგ დაუბრუნდი მცდელობებს</option>
                                        <option value="2">ჯერ ამოწურე მცდელობები, მერე მიყევი სიას</option>
                                       </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td label>მცდელობებს შორის შუალედი <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="attempt_period_time" style="width: 60px" /> წმ
                                    </td>
                                </tr>
                                <tr id="section_listening_status">
                                    <td label>სტატუსი - „მოისმინა“ <span help-icon title="გამარჯობა მეგობარო" /></td>
                                    <td content>
                                        <input type="number" id="listening_status" style="width: 60px" /> წმ
                                </td>
                                </tr>
                            </table>
                        </fieldset>

                            
                        <fieldset class="dialog__second_section_content" id="second_calendar" >
                            <div class="timeTableHeader">
                                <span class="green">უკვე არსებული</span>
                                <span class="yellow">ახალი</span>
                                <span class="red">წასაშლელი</span>
                            </div>

                            <div id="save_calendar" class="schedule_create">
                                <span class="button_loading" />
                                შეინახე შაბლონად
                                <div id="button_down" class="button_down" data-status="0">
                                    <input type="text" id="calendar_schedule_name" value="'.$res['calendar_schedule_name'].'" placeholder="შაბლონის სახელი">
                                    <button id="save_calendar_button">შენახვა</button>
                                </div>
                            </div>

                            <div id="select_calendar" class="schedule_select">
                                <span class="button_loading" />
                                აირჩიე შაბლონი

                                <ul id="calendar_selector" data-status="0" class="select_schedule">
                                    '.get_calendar_schedules().'
                                </ul>
                            </div>

                            <span class="block_settings_content"></span>
                            <span class="block_timetable"></span>
                            
                            <table class="calendar_content"><tr>
                            <td class="weekDays">
                            <div>ორშაბათი</div>
                            <div>სამშაბათი</div>
                            <div>ოთხშაბათი</div>
                            <div>ხუთშაბათი</div>
                            <div>პარასკევი</div>
                            <div>შაბათი</div>
                            <div>კვირა</div>
                            </td>
                            <td>                           
                            <table class="timeTable">
                            <tr>
                                <td>09:00</td>
                                <td>10:00</td>
                                <td>11:00</td>
                                <td>12:00</td>
                                <td>13:00</td>
                                <td>14:00</td>
                                <td>15:00</td>
                                <td>16:00</td>
                                <td>17:00</td>
                                <td>18:00</td>
                                <td>19:00</td>
                                <td>20:00</td>
                                <td>21:00</td>
                                <td>22:00</td>
                                <td>23:00</td>
                                <td>00:00</td>
                                <td>01:00</td>
                                <td>02:00</td>
                                <td>03:00</td>
                                <td>04:00</td>
                                <td>05:00</td>
                                <td>06:00</td>
                                <td>07:00</td>
                                <td>08:00</td>
                            </tr>
                            </table>
                            <table id="timeTableSection">

                            </table>
                        </td>
                        </tr></table>

                    </fieldset>

                    <fieldset class="dialog__second_section_content" id="second_mic">
                        <div id="settings_voice"></div>
                    </fieldset>

                    <fieldset class="dialog__second_section_content" id="second_upload">
                    
                        <div id="settings_upload"></div>

                    </fieldset>

                    <fieldset class="dialog__second_section_content" id="second_edit">
                        <div id="fieldset_inputs"></div>  
                    </fieldset>
                    </td>
                </tr>
            </table>
        </section>

        <section id="dialog__comment_section" class="dialog__comment_section">
        <label>კომენტარი</label>
            <textarea id="campaign_comment"></textarea>
        </section>
    </div>
    <input type="hidden" value="'.$res['calendar_schedule_id'].'" id="outgoing_campaign_setting_calendar_hidden_id" />
    <input type="hidden" value="'.$res['parameters_id'].'" id="outgoing_campaign_setting_parameters_hidden_id" />
    <input type="hidden" value="'.$res['group_schedule_id'].'" id="outgoing_campaign_setting_group_hidden_id" />
    <input type="hidden" value="'.$new_id.'" id="settings_hidden_id" />
    <input type="hidden" value="'.$res['id'].'" id="new_settings_hidden_id" />
    <input type="hidden" value="'.$res['default_compaign'].'" id="default_compaign" />
    ';
    return $data;
}



function get_voice_data($id){

    global $db;

    $db->setQuery(" SELECT  `id`,
                            `voice_type_id`,
                            `name`,
                            `position`
                    FROM    `outgoing_campaign_request_voice`
                    WHERE   `id` = '$id'");
    $db->execQuery();

    $result = $db->getResultArray();

    return $result['result'][0];
}

function get_base_numbers($id){

    $numArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    $data = '';

    foreach($numArray as $num){
        if($num == $id){
            $data .=  '<option value="'.$num.'" selected>'.$num.'</option>';    
        }else{
            $data .= '<option value="'.$num.'">'.$num.'</option>';    
        }
        
    }

    return $data;

}

function Get_voice_add_page($res = '', $conn_type){

    $data = '
    <div id="dialog-form">
        <input type="text" id="voice_name" value="'.$res['name'].'" placeholder="დასახელება" style="width: 186px; margin: 20px auto; display: block" />

        <select id="add_voice_type" style="display: block; margin: 19px auto; width: 186px; padding: 4px;">
            '.get_voice_types($res['voice_type_id'], $conn_type).'
        </select>

        <select id="add_voice_pos" style="display: block; margin: 19px auto; width: 60px; padding: 4px;">
            '.get_base_numbers($res['position']).'
        </select>
    </div>
    <input type="hidden" value="'.$res['id'].'" id="voice_hidden_id" />
    ';

    return $data;
}


function get_upload_forms($id){
    global $db;

    $db->setQuery(" SELECT      `voice`.`id` AS `id`,
                                `voice`.`datetime` AS `create_date`,
                                `voice`.`name` AS `name` ,
                                `type`.`name` AS `voice_type`,
                                `voice`.`position` AS `position`,
                                `voice`.`value` AS `value`
                    FROM        outgoing_campaign_request_voice as voice
                    LEFT JOIN   outgoing_campaign_field_type as type ON `type`.`id` = `voice`.`voice_type_id`
                    WHERE       request_id = '$id' AND actived = 1
                    ORDER BY    `voice`.`position`");

    $db->execQuery();
    $columns = $db->getResultArray();

    $data = '';
    $c = 1;
    $data .= '<label style="margin: 10px 45px;width: 100%;display: block;">მობილურის ნომერი</label>
        
    <input type="text" placeholder="მობილურის ნომერი" id="0" style="display: block; margin: 10px auto; width: 77%" />';


    foreach($columns['result'] as $col){

        $data .= '<label style="margin: 10px 45px;width: 100%;display: block;">'.$col['name'].'</label>
        
                    <input type="text" placeholder="'.$col['name'].'" id="'.$c.'" style="display: block; margin: 10px auto; width: 77%" />';
    $c++;    
    }
    
   return $data; 
}

function Get_upload_add_page($id){

    $data = '
    <div id="dialog-form">
     
    <div id="upload_manual_voice_form">
            '.get_upload_forms($id).'
        </div>

    </div>
    ';

    return $data;
}







/**
 * ----------------------------------------------------------------------------------------------------------------------------
 * PROCESSING PAGE
 * ----------------------------------------------------------------------------------------------------------------------------
 */

 /**
  * @param int $id
  */
function get_cat_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

/**
 * @param int $id
 * @param int $child_id
 * 
 */
function get_cat_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `info_category`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req['result'] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}


/**
 * @param int $id
 * @param int $child_id
 */
function get_cat_1_1_1($id,$child_id){
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req['result'] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

/**
 * @param int $id
 */
function get_space_type($id){
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `space_type`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

/**
 * @param int $id
 */
function get_inc_status($id){
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `inc_status`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}


/**
 * @param int $id
 */
function get_company($id){
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `company`
					WHERE   `actived` = 1 AND `disabled` = 0");

	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

/**
 * @param int $id
 */
function get_lang($id){
	global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `user_language`
                   WHERE   actived = 1");
    
	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == 1){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

/**
 * @param int $id
 */
// function web_site($id){
//     global $db;
// 	$data = '';
	
// 	$db->setQuery("SELECT `id`, `name`
// 				   FROM   `my_web_site`
// 				   WHERE   actived = 1");
	
// 	$res = $db->getResultArray();
// 	foreach ($res['result'] AS $value){
// 	    $db->setQuery("SELECT id
// 	                   FROM  `incoming_call_site`
// 	                   WHERE `incoming_call_site.incomming_call_id` = '$id' AND site_id = '$value[id]'");
// 	    $check = $db->getNumRow();
	    
// 	    if($check > 0){
// 	        $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
// 	    } else {
// 	        $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
// 	    }
	    
// 	}
	
// 	return $data;
// }



function get_status_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `tree`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

function get_status_1_1($id, $child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `tree`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req['result'] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}


function Getincomming($incomming_id){

	global $db;

    if($incomming_id != ''){
	
        $db->setQuery("SELECT   `incomming_call`.`id` AS id,
                                `incomming_call`.`date` AS call_date,
                                `incomming_call`.`date` AS record_date,
                                IFNULL( asterisk_call_log.source, incomming_call.phone ) AS `phone`,
                                incomming_call.ipaddres,
                                incomming_call.asterisk_incomming_id,
                        IF
                            (
                            NOT ISNULL( incomming_call.asterisk_incomming_id ),
                        CASE
                            
                            WHEN FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime ) + wait_time + talk_time ) <= incomming_call.processing_start_date THEN
                            SEC_TO_TIME( UNIX_TIMESTAMP( processing_end_date ) - UNIX_TIMESTAMP( processing_start_date ) ) 
                            WHEN FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime ) + wait_time + talk_time ) > incomming_call.processing_start_date 
                            AND FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime ) + wait_time + talk_time ) >= incomming_call.processing_end_date THEN
                                '00:00:00' 
                                WHEN FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime ) + wait_time + talk_time ) > incomming_call.processing_start_date 
                                AND FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime ) + wait_time + talk_time ) < incomming_call.processing_end_date THEN
                                    SEC_TO_TIME( UNIX_TIMESTAMP( incomming_call.processing_end_date ) - ( UNIX_TIMESTAMP( asterisk_call_log.call_datetime ) + wait_time + talk_time ) ) 
                                END,
                                '00:00:00' 
                            ) AS `processed_time`,
                            user_info.`name` AS operator,
                        IF
                            ( ISNULL( asterisk_call_log.user_id ), chat_user_name.`name`, user_info.`name` ) AS operator,
                        IF
                            ( ISNULL( asterisk_call_log.user_id ), chat.answer_user_id, asterisk_call_log.user_id ) AS `user_id`,
                        /* incomming_call.inc_status_id AS `inc_status`, */
                            incomming_call.cat_1,
                            incomming_call.cat_1_1,
                            incomming_call.cat_1_1_1,
                            incomming_call.status_1,
                            incomming_call.status_1_1,
                            incomming_call.space_type,
                            incomming_call.chat_language,
                            incomming_call.inc_status,
                            incomming_call.company,
                            incomming_call.call_content AS call_content,
                            incomming_call.source_id,
                            incomming_call.client_user_id AS personal_pin,
                            incomming_call.client_pid AS personal_id,
                            incomming_call.web_site_id,
                            incomming_call.client_name,
                            incomming_call.client_comment,
                            incomming_call.out_comment,
                            incomming_call.vizit_location,
                            incomming_call.vizit_date,
                            incomming_call.fb_link,
                            incomming_call.viber_address,
                            incomming_call.phone1,
                            incomming_call.client_satus,
                            incomming_call.client_sex,
                            incomming_call.client_mail,
                            incomming_call.client_birth_year,
                            incomming_call.chat_id,
                            incomming_call.lid,
                            incomming_call.note_comm,
                            incomming_call.lid_comment,
                            asterisk_call_log.source AS `ast_source`,
                            SEC_TO_TIME( asterisk_call_log.talk_time ) AS `duration`,
                            SEC_TO_TIME( asterisk_call_log.wait_time ) AS `wait_time`
                        FROM
                            incomming_call
                            LEFT JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                            LEFT JOIN users ON users.id = asterisk_call_log.user_id
                            LEFT JOIN user_info ON users.id = user_info.user_id
                            LEFT JOIN chat ON incomming_call.chat_id = chat.id
                            LEFT JOIN users AS chat_user ON chat_user.id = chat.answer_user_id
                            LEFT JOIN user_info AS chat_user_name ON chat_user.id = chat_user_name.user_id
                            WHERE `incomming_call`.`id` = $incomming_id");
        
        $req = $db->getResultArray();
        $res = $req['result'][0];
        return $res;
    }
                           
}

function Addincomming($id, $inc_id, $user, $c_date, $call_date, $hidden_inc, $incomming_cat_1, $incomming_cat_1_1, $incomming_cat_1_1_1, $incomming_status_1, $incomming_status_1_1,  $chat_language, $inc_status_a, $company, $space_type, $s_u_mail, $ab_person_name, $lid, $fb_link, $viber_address, $vizit_datetime, $vizit_location, $client_comment, $out_comment, $client_sex, $client_birth_year, $s_u_status, $call_content, $lid_comment, $processing_start_date, $phone, $phone1){

    global $db;
    
    $db->setQuery(" INSERT INTO  `incomming_call`
                SET     `id`                    = '$inc_id',
                        `date`                  = NOW(),
                        `processing_start_date` = '$processing_start_date',
                        `processing_end_date`   = NOW(),
                        `user_id`               = '$user',
                        `phone`                 = '$phone',
                        `phone1`                = '$phone1',
                        `fb_link`               = '$fb_link',
                        `viber_address`         = '$viber_address',
                        `vizit_date`            = '$vizit_datetime',
                        `vizit_location`        = '$vizit_location',
                        `client_comment`        = '$client_comment',
                        `out_comment`	        = '$out_comment',
                        `client_name`           = '$ab_person_name',
                        `client_mail`           = '$s_u_mail',
                        `lid`                   = '$lid',
                        `cat_1`                 = '$incomming_cat_1',
                        `cat_1_1`               = '$incomming_cat_1_1',
                        `cat_1_1_1`             = '$incomming_cat_1_1_1',
                        `chat_language` 	    = '$chat_language',
                        `source_id` 	        = '1',
                        `inc_status`		    = '$inc_status_a',
                        `company`		        = '$company',
                        `space_type`		    = '$space_type',
                        `lid_comment`           = '$lid_comment',
                        `call_content`          = '$call_content',
                        `actived`               = 0");
    $db->execQuery();
    $last_id    = $db->getLastId();

    $db->setQuery(" UPDATE  `outgoing_campaign_request_base`
                    SET     `incomming_call_id` = '$last_id',
                            `tree_id`           = '$incomming_status_1',
                            `sub_tree_id`       = '$incomming_status_1_1'
                    WHERE   `id` = '$id'");

    $db->execQuery();

}

function Saveincomming($id, $inc_id, $user, $c_date, $call_date, $hidden_inc, $incomming_cat_1, $incomming_cat_1_1, $incomming_cat_1_1_1, $incomming_status_1, $incomming_status_1_1,  $chat_language, $inc_status_a, $company, $space_type, $s_u_mail, $ab_person_name, $lid, $fb_link, $viber_address, $vizit_datetime, $vizit_location, $client_comment, $out_comment, $client_sex, $client_birth_year, $s_u_status, $call_content, $lid_comment, $processsing_start_date, $phone, $phone1){
   
    global $db;

    $db->setQuery("UPDATE   `incomming_call`
                        SET `user_id`           = '$user',
                            `phone`             = '$phone',
                            `client_name`       = '$ab_person_name',
                            `client_mail`       = '$s_u_mail',
                            `phone1`            = '$phone1',
                            `fb_link`           = '$fb_link',
                            `viber_address`     = '$viber_address',
                            `vizit_date`        = '$vizit_datetime',
                            `vizit_location`    = '$vizit_location',
                            `client_comment`    = '$client_comment',
                            `out_comment`	    = '$out_comment',

                            `client_satus`      = '$s_u_status',
                            `client_sex`        = '$client_sex',
                            `client_birth_year` = '$client_birth_year',
                            `cat_1`             = '$incomming_cat_1',
                            `cat_1_1`           = '$incomming_cat_1_1',
                            `cat_1_1_1`         = '$incomming_cat_1_1_1',

                            `status_1`          = '$incomming_status_1',
                            `status_1_1`        = '$incomming_status_1_1',
                            `source_id` 	    = '1',
                            `chat_language`     = '$chat_language',
                            `inc_status`		= '$inc_status_a',
                            `company`			= '$company',
                            `space_type`		= '$space_type',
                            `lid`               = '$lid',
                            `lid_comment`       = '$lid_comment',
                            `call_content`      = '$call_content'
                    WHERE  `id`                = '$inc_id'");
			
			$db->execQuery();
            
            $db->setQuery("UPDATE `incomming_call`
			                  SET  processing_end_date = NOW()
			               WHERE  `id` = '$inc_id' AND NOT ISNULL(asterisk_incomming_id) AND ISNULL(processing_end_date)");
			$db->execQuery();
}


function get_processing_page($res = '', $hidden_incomming_id, $id,$req_id){


    global $db;
    if(strlen($id) != 9){
        $db->setQuery(" SELECT  phone_number 
                    FROM    outgoing_campaign_request_base
                    WHERE   id = '$id'");
        $db->execQuery();
        $base = $db->getResultArray();
        $db->setQuery(" SELECT  phone_number,
                                number_1,
                                number_2,
                                number_3,
                                number_4,
                                number_5,
                                number_6,
                                number_7,
                                number_8,
                                number_9,
                                number_10
                        FROM    outgoing_campaign_request_base
                        WHERE   id = '$id'");
        $db->execQuery();
        $addit_fields = $db->getResultArray();

        $db->setQuery(" SELECT  id,
                                name,
                                position
                        FROM    outgoing_campaign_request_voice
                        WHERE   request_id = '$req_id'
                        ORDER BY position");

        $inputs = $db->getResultArray();
    }
    else{
        $base['result'][0]['phone_number'] = $id;
        $db->setQuery(" SELECT  COUNT(*) AS 'cc',
                                outgoing_campaign_request_base.id,
                                outgoing_campaign_request_base.incomming_call_id
                        FROM    outgoing_campaign_request_base
                        WHERE   phone_number = '995$id'");
        $base_checker = $db->getResultArray();
        
        if($base_checker['result'][0]['cc'] == 0){
            die($data['base_error'] = 'base_error_not_found');
        }
        else{
            
            $ii = $base_checker['result'][0]['id'];
            if($base_checker['result'][0]['incomming_call_id'] == ''){
                $db->setQuery("INSERT INTO incomming_call
                                (`user_id`)
                            VALUES 
                                ('')");
                $db->execQuery();
                $hidden_incomming_id = $db->getLastId();
                $db->setQuery("UPDATE outgoing_campaign_request_base SET incomming_call_id='$hidden_incomming_id' WHERE id='$ii'");
                $db->execQuery();
            }
            
            $db->setQuery(" SELECT  phone_number,
                                    number_1,
                                    number_2,
                                    number_3,
                                    number_4,
                                    number_5,
                                    number_6,
                                    number_7,
                                    number_8,
                                    number_9,
                                    number_10,
                                    outgoing_campaign_request_id
                            FROM    outgoing_campaign_request_base
                            WHERE   id = '$ii'");
            $db->execQuery();
            $addit_fields = $db->getResultArray();
            $req_id = $addit_fields['result'][0]['outgoing_campaign_request_id'];
            $db->setQuery(" SELECT  id,
                                    name,
                                    position
                            FROM    outgoing_campaign_request_voice
                            WHERE   request_id = '$req_id'
                            ORDER BY position");

            $inputs = $db->getResultArray();
        }
    }
    

    

    $data  = '
    <div id="dialog-form">
    <div class="dialog-style">
 
        <div class="communication_fieldset_style">
            <div id="side_menu" class="communication_side_menu" style="width: 879px">
                <span class="info communication_side_menu_button" onclick="show_right_side(\'info\')"><img class="communication_side_menu_img"  style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/lemons_info.png" alt="24 ICON" title="ინფო"></span>

                <span class="client_history communication_side_menu_button"  onclick="show_right_side(\'client_history\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_client_history.png" alt="24 ICON"  title="ისტორია"></span>
                                
                <span class="task communication_side_menu_button"  onclick="show_right_side(\'task\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_task.png" alt="24 ICON"  title="დავალება"></span>

                <span class="client_conversation communication_side_menu_button"  onclick="show_right_side(\'client_conversation\')"><img class="communication_side_menu_img" src="media/images/icons/client_conversation.png" alt="24 ICON"  title="დავალება"></span>

                <span class="sms communication_side_menu_button" onclick="show_right_side(\'sms\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_sms.png" alt="24 ICON" title="ესემეს"></span>
                
                <span class="incomming_mail communication_side_menu_button" onclick="show_right_side(\'incomming_mail\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_mail.png" alt="24 ICON" title="ელ. ფოსტა"></span>

                <span class="chat_question communication_side_menu_button" onclick="show_right_side(\'chat_question\')"><img class="communication_side_menu_img" src="media/images/icons/chat_question.png" alt="24 ICON" title="კომენტარები"></span>
                
                <span class="newsnews communication_side_menu_button" onclick="show_right_side(\'newsnews\')"><img class="communication_side_menu_img" src="media/images/icons/newsnews.png" alt="24 ICON" title="სიახლეები"></span>
                
                <span class="record communication_side_menu_button" onclick="show_right_side(\'record\')"><img class="communication_side_menu_img" src="media/images/icons/inc_record.png" alt="24 ICON"  title="ჩანაწერები"></span>
                                    
                <span class="incomming_file communication_side_menu_button" onclick="show_right_side(\'incomming_file\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_files.png" alt="24 ICON"  title="ფაილები"></span>
                
                <span class="user_logs communication_side_menu_button" onclick="show_right_side(\'user_logs\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_logs.png" alt="24 ICON"  title="ლოგები"></span>

                <span style="float: right; margin-right: 10px;" class="call_resume communication_side_menu_button" onclick="show_right_side(\'call_resume\')"><img class="communication_side_menu_img" src="media/images/icons/inc_resume.png" alt="24 ICON"  title="ლოგები"></span>
            
            </div>
        </div>

    <div class="communication_right_side" id="right_side" style="width: 896px">
    <ff class="communication_right_side_info" id="info">
        <fieldset class="communication_right_side_info">
            <table width="100%" class="dialog-form-table">
                <legend>INFO</legend>
                <div class="col-4">
                    <label>ტელეფონი</label>
                    <input class="idle" disabled style="width:95%;" type="text" value="'.$base['result'][0]['phone_number'].'">
                </div>';
                foreach($inputs['result'] AS $input){
                    
                    if($addit_fields['result'][0]['number_'.$input['position']] != '')
                    {
                        $data .= '<div class="col-4"><label>'.$input['name'].'</label><input class="idle" disabled style="width:95%;" type="text" value="';
                        $data .= $addit_fields['result'][0]['number_'.$input['position']];
                        $data .= '"></div>';
                    }   
                }
                
            $data .= '</table>
        </fieldset>
        <fieldset class="communication_right_side_info">
            <table width="100%" class="dialog-form-table">
                <legend>მომრთვა</legend>
                <tr style="height:0px">
                    <td style="width: 50%"><label for="d_number">მომართვის კატეგორია</label></td>
                    <td style="width: 50%;"><label for="d_number">ქვე-კატეგორია 1</label></td>
                    <td style="width: 50%;"><label for="d_number">ქვე-კატეგორია 2</label></td>
                </tr>
                <tr style="height:0px" class="ff_till">
                    <td><select  id="incomming_cat_1" style="width: 63%; height: 18px;">'.get_cat_1($res['cat_1']).'</select></td>
                    <td><select  id="incomming_cat_1_1" style="width: 63%; height: 18px;">'.get_cat_1_1($res['cat_1'],$res['cat_1_1']).'</select></td>
                    <td style="padding-right: 11px"><select  id="incomming_cat_1_1_1" style="width: 63%; height: 18px;">'.get_cat_1_1_1($res['cat_1_1'],$res['cat_1_1_1']).'</select></td>
                </tr>
            <tr style="height:0px">
                <td style="width: 50%;"><label for="d_number">მომართვის სტატუსი</label></td>
                <td style="width: 50%;"><label for="d_number">მომართვის ქვე-სტატუსი</label></td>
                <td style="width: 50%;"><label for="d_number">ფართის ტიპი</label></td>
            </tr>
            <tr style="height:0px" class="ff_till">
                <td><select  id="incomming2_status_1" style="width: 63%; height: 18px; ">'.get_status_1($res['status_1']).'</select></td>
                <td><select  id="incomming2_status_1_1" style="width: 63%; height: 18px; ">'.get_status_1_1($res['status_1'],$res['status_1_1']).'</select></td>
                <td><select id="space_type" style="width: 63%; height: 18px; ">'.get_space_type($res['space_type']).'</select></td>
                </tr>
                <tr style="height:0px">
                    <td style="width: 50%;"><label for="d_number">რეაგირება</label></td>
                    <td style="width: 50%;"><label for="d_number">აქცია</label></td>
                </tr>
                <tr class="ff_till" style="height:0px">
                    <td><select id="inc_status_a" style="width: 63%; height: 18px; ">'.get_inc_status($res['inc_status']).'</select></td>
                    <td><select id="company" style="width: 63%; height: 18px; ">'.get_company($res['company']).'</select></td>
                </tr>
            </table>
        </fieldset>
        <fieldset class="communication_right_side_info">
            <table width="100%" class="dialog-form-table">
                <legend style="margin-top: -8px;">მომრთვის ავტორი</legend>
                <tr style="height:0px">
                    <td style="width: 30%;"><label for="d_number">სახელი და გვარი</label></td>
                    <td style="width: 30%;"><label for="d_number">ტელეფონი1</label></td>
                    <td style="width: 30%;"><label for="d_number">ტელეფონი2</label></td>
                </tr>
                <tr style="height:0px">
                    <td>
                        <input style="height: 18px; width: 95%;" type="text" id="client_name" class="idle" value="' . $res['client_name']. '"/>	
                    </td>
                    <td>
                        <input style="height: 18px; width: 95%;" type="text" id="phone" value="'.$base['result'][0]['phone_number'].'" class="idle" />	
                    </td>
                    <td>
                        <input style="height: 18px; width: 94%;" type="text" id="phone1" class="idle" value="' . $res['phone1']. '"/>	
                    </td>
                </tr>
                <tr style="height:5px;"></tr>
                <tr style="height:0px">
                    <td><label for="d_number">ელ-ფოსტა</label></td>
                    <td><label for="d_number">FB ლინკი</label></td>
                    <td><label for="d_number">ვაიბერი</label></td>
                </tr>
                <tr style="height:0px">
                    <td>
                        <input style="height: 18px; width: 95%;" type="text" id="client_mail" class="idle" value="' . $res['client_mail']. '"/>	
                    </td>
                    <td>
                        <input style="height: 18px; width: 95%;" type="text" id="fb_link" class="idle" value="' . $res['fb_link']. '"/>	
                    </td>
                    <td>
                        <input style="height: 18px; width: 94%;" type="text" id="viber_address" class="idle" value="' . $res['viber_address']. '"/>	
                    </td>
                </tr>
                <tr style="height: 11px;"></tr>
                <tr style="height:0px">
                    <td colspan="2">
                        <label style="font-size: 12px;display: initial;" for="d_number">კომენტარი მომართვის ავტორზე</label>
                    </td>
                    <td><label style="/* text-align: right; */margin-right: 21px;" for="d_number">ენა</label></td>
                </tr>
                <tr style="height:0px">
                    <td colspan="2">
                        <textarea  style="resize: vertical;width: 97%;height: 30px;" id="client_comment" class="idle" name="call_content">' . $res['client_comment'] . '</textarea>
                    </td>
                    <td><select id="chat_language" style="width: 96%; height: 18px; ">'.get_lang($res['chat_language']).'</select></td>
                </tr>
                <tr style="height:5px;"></>
            </table>
        </fieldset>
        <fieldset class="communication_right_side_info">
            <table width="100%" class="dialog-form-table">
                <legend style="margin-top: -8px;">გაცემული ინფო</legend>
                <tr>
                    <td style=" width: 100%">
                        <div id="kendo_project_info_table"></div>
                    </td>
                </tr>
            </table>
            <tr style="height:0px">
            <tr>
            <td style="padding-top: 12px;"><label style="text-align: right; margin-right: 26px; margin-top: 11px" for="out_comment">კომენტარი გაცემულ ინფორმაციაზე</label></td>
            </tr>
            <tr>
            <td>
            <textarea  style="resize: vertical;width: 97%;height: 30px;" id="out_comment" class="idle" name="call_content">' . $res['out_comment'] . '</textarea>
            </td>
            </tr>
        </fieldset>
        <fieldset>
        <legend>შედეგი</legend>
            <table width="100%" class="dialog-form-table">
            <tr style="height:0px">
            <td><label style="text-align: right; margin-right: 26px;" for="d_number">შეხვედრის თარიღი და დრო</label></td>
            <td colspan="2" style="width: 205px;"><label for="d_number">შეხვედრის ლოკაცია</label></td>
        </tr>
        <tr style="height:0px">
            <td colspan="2">
                <input style="margin-right:11px; width: 33%;" type="text" id="vizit_datetime" class="idle" value="'.$res['vizit_date'].'"/>	
                <input style="height: 18px; width: 62%;" type="text" id="vizit_location" class="idle" value="' . $res['vizit_location']. '"/>	
            </td>
        </tr>
                <tr style="height:0px">
                    <td style="padding-top: 12px;"><label style="text-align: right; margin-right: 26px;" for="call_content">კომენტარი</label></td>
                </tr>
                <tr style="height:0px">
                    <td colspan="2">
                        <textarea  style="resize: vertical;width: 98%; height: 60px;" id="call_content" class="idle" name="call_content">' . $res['call_content'] . '</textarea>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ff>
    
    <ff style="display:none; height: 656px;" id="newsnews">
        <div style="margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            <table class="display" id="table_news" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width:15%;">დასაწყისი</th>
                        <th style="width:15%;">დასასრული</th>
                        <th style="width:15%;">დასა-<br>ხელება</th>
                        <th style="width:15%;">საიტი(ები)</th>
                        <th style="width:40%;">შინაარსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="client_history">
        <table style="margin-left: 7px;">
            <tr style="height:0px;">
                <td colspan="2" style="text-align: center;">პერიოდი</td>
                <td><label>ტელეფონი</label></td>
                <td></td>
            </tr>
            <tr style="height:0px;">
                <td style="width: 120px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_from" style="width: 110px;" value="'.date('Y-m-d', strtotime('-7 day')).'"></td>
                <td style="width: 120px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_to" style="width: 110px;" value="'.date('Y-m-d').'"></td>
                <td style="width: 140px;"><input class="callapp_filter_body_span_input" type="text" id="client_history_user_id" style="width: 130px;" ></td>
                <td><button id="search_client_history" style="margin-top: -6px;">ძებნა</button></td>
            </tr>
        </table>

        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            <table class="display" id="table_history" style="width: 100%">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 20%">ტელეფონის ნომერი</th>
                        <th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 20%;">კომენტარი</th>
                        <th style="width: 20%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="task">
        <div id="task_button_area" class="margin_top_10" style="margin-left: 7px;"> 
            <button id="add_task_button">ახალი დავალება</button>
        </div>
        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            <table class="display" id="task_table" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%">თარიღი</th>
                        <th style="width: 20%;">დასაწყისი</th>
                        <th style="width: 20%;">დასასრული</th>
                        <th style="width: 20%;">სტატუსი</th>
                        <th style="width: 20%;">პასუხისმგებელი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="incomming_comment">
        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="incomming_mail">
        
        <div style="margin-left: 7px;" id="task_button_area">
            <button id="add_mail">ახალი E-mail</button>
        </div>
        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            <table class="display" id="table_mail" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 25%;">ადრესატი</th>
                        <th style="width: 35%;">გზავნილი</th>
                        <th style="width: 20%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="client_conversation">
        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
        <table class="display" id="table_question" style="width:100%">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 20%;">პროექტი</th>
                        <th style="width: 20%;">კითხვა</th>
                        <th style="width: 40%;">პასუხი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px; " id="chat_question">
        <div style="margin-left: 7px;"><p>ახალი კომენატრი</p></div>
        <table>
            <tr>
                <td><textarea id="add_ask" style="border: 1px solid #dbdbdb; resize:none; margin-left: 7px; width:510px; height:50px; display:inline-block;" rows="3"></textarea > </td>
                <td><button style="height: 50px; margin-left: 9px;" id="add_comment">დამატება</button></td>
            </tr>
        </table>

        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; border: 1px solid #dbdbdb;"></div>
        <input type=hidden id="comment_incomming_id" value="'.$res['id'].'"/>
    </ff>
    <ff style="display:none; height: 656px;" id="user_logs">
        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            <table class="display" id="user_log_table">
                <thead>
                    <tr style="font-size: 11px;" id="datatable_header">
                        <th>ID</th>
                        <th style="width:20%">თარიღი</th>
                        <th style="width:15%">ქმედება</th>
                        <th style="width:12%">მომხმა<br>რებელი</th>
                        <th style="width:15%">ველი</th>
                        <th style="width:19%">ახალი მნიშვნელობა</th>
                        <th style="width:19%">ძველი მნიშვნელობა</th>
                    </tr>
                </thead>
                <thead >
                    <tr class="search_header">
                        <th class="colum_hidden">
                            <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input style="width:97%" type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="call_resume">
        <table style="margin-left: 7px;" width="100%" class="dialog-form-table">
            <tr style="height:0px">
                <td style="width: 205px;"><label for="d_number">ოპერატორი</label></td>
                <td style="width: 205px;"><label for="d_number">თარიღი</label></td>
                <td style="width: 205px;"><label for="d_number">მომართვის ნომერი</label></td>
            </tr>
            <tr style="height:0px">
                <td>
                    <input style="height: 18px; width: 160px;" type="text" id="my_operator_name" class="idle" value="'.$res['operator'].'" disabled="disabled"/>
                </td>
                <td>
                    <input style="height: 18px; width: 160px;" type="text" id="call_datetime" class="idle" value="'.$res['call_date'].'" disabled="disabled"/>
                </td>
                <td>
                    <input style="height: 18px; width: 160px;" type="text" id="incomming_call_id" class="idle" disabled="disabled"/>
                </td>
            </tr>
            <tr style="height:15px;"></tr>
            <tr style="height:0px">
                <td style="width: 205px;"><label for="d_number">ლოდინის დრო</label></td>
                <td style="width: 205px;"><label for="d_number">საუბრის ხანგრძლივობა</label></td>
                <td style="width: 205px;"><label for="d_number">დამუშავების ხანგრძლივობა</label></td>
            </tr>
            <tr style="height:0px">
                <td>
                    <input style="height: 18px; width: 160px;" type="text" id="call_wait_time" class="idle" value="'.$res['wait_time'].'" disabled="disabled"/>
                </td>
                <td>
                    <input style="height: 18px; width: 160px;" type="text" id="call_duration" class="idle" value="'.$res['duration'].'" disabled="disabled"/>
                </td>
                <td>
                    <input style="height: 18px; width: 160px;" type="text" id=call_done_date" class="idle" value="'.$res['processed_time'].'" disabled="disabled"/>
                </td>
            </tr>
            <tr style="height:15px;"></tr>
            <tr style="height:0px">
                <td colspan="3"><label for="d_number">საიტი</label></td>
            </tr>
            <tr style="height:0px">
                <td colspan="3"><select style="height: 18px; width: 590px;" id="my_site_resume" class="idls object" disabled="disabled" multiple></select></td>
            </tr>
        </table>
    </ff>
    
    <ff style="display:none; height: 656px;" id="sms">
        
        <div id="task_button_area" style="margin-left: 7px;">
            <button id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
        </div>
        <div style=" margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            <table class="display" id="table_sms" >
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 15%;">ადრესატი</th>
                        <th style="width: 50%;">ტექსტი</th>
                        <th style="width: 15%;">სტატუსი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        <input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_number" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="incomming_file">
        <table style="float: right;text-align: center; border: 1px solid #ccc; width: 100%;">
            <tr>
                <td>
                    <div class="file-uploader">
                        <input id="choose_file1" type="file" name="choose_file1" class="input" style="display: none;">
                        <button id="choose_button" style="width: 100%;" class="center">აირჩიეთ ფაილი</button>
                        <input id="hidden_inc" type="text" value="" style="display: none;">
                    </div>
                </td>
            </tr>
        </table>
        <table style="float: right;border: 1px solid #ccc;width: 100%;margin-top: 2px;text-align: center;">
            <tr>
            <td colspan="4">მიმაგრებული ფაილი</td>
            </tr>
        </table>
        <table id="file_div" style="float: right; border: 1px solid #ccc; width: 100%;margin-top: 2px; text-align: center;">';

    $data .= '
        </table>
    </ff>
    <ff style="display:none; height: 656px;" id="crm">
        <div style="width:100%">
        
            <table>
                <tr style="height: 0px;">
                    <td colspan="2" style="width: 145px; text-align: center;">პერიოდი</td>
                    <td><label>User Id</label></td>
                    <td>ტელეფონი</td>
                    <td></td>
                </tr>
                <tr style="height: 0px;">
                    <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="start_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d', strtotime('-7 day')).'"></td>
                    <td style="width: 92px;"><input class="callapp_filter_body_span_input date_input" type="text" id="end_crm" style="width: 79px; height: 20px; margin-top: 20px;" value="'.date('Y-m-d').'"></td>
                    <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_pin" style="width: 111px; height: 20px; margin-top: 20px;" ></td>
                    <td><input class="callapp_filter_body_span_input" type="text" id="check_ab_crm_phone" style="width: 111px; height: 20px; margin-top: 20px;" value="'.$res['phone'].'"></td>
                    <td><button id="search_ab_crm_pin" style="margin-left: 10px; margin-top: 4px;">ძებნა</button></td>
                </tr>
            <table>
            <table class="display" id="table_crm" style="width: 100%">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 15%;">Upload<br>date</th>
                        <th style="width: 14%;">Prom-<br>date</th>
                        <th style="width: 11%;">User Id</th>
                        <th style="width: 12%;">User<br>name</th>
                        <th style="width: 13%;">Mobile</th>
                        <th style="width: 13%;">PID</th>
                        <th style="width: 10%;">Com-<br>ment</th>
                        <th style="width: 12%;">status</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                            <input style="width: 96%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </ff>
    <ff style="display:none; height: 656px;" id="record" >
        <div style="margin-top: 10px;">
            <audio controls style="margin-left: 7px; width: 97%" id="auau">
            <source src="" type="audio/wav">
            Your browser does not support the audio element.
            </audio>
        </div>
        <table style="margin: auto; width: 97%;">
        <tr>
            <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">თარიღი</td>
            <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ხანგძლივობა</td>
            <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ოპერატორი</td>
            <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">ვინ გათიშა</td>
            <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;">მოსმენა</td>
            </tr>
            '.GetRecordingsSection($res).'
        </table>
        
    </ff>
    </div>
    <input id="hidden_incomming_call_id" type="text" value="'. $hidden_incomming_id .'" style="display: none;">
    <input id="hidden_base_id" value="'.$id.'" type="text" style="display: none;">
    </div>
    </div>';

    return $data;

}



function GetRecordingsSection($res) {
    global $db;
    
	if ($res['phone']==''){

        $num_row = 0;
        
	}else{

    	$db->setQuery(" SELECT    SEC_TO_TIME(asterisk_call_log.talk_time) AS `time`,
                				  CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'),asterisk_call_record.name,'.',asterisk_call_record.format)  AS `userfield`,
                				  FROM_UNIXTIME(asterisk_call_log.call_datetime),
                				  user_info.`name`,
                				  IF(asterisk_call_log.call_status_id = 6, 'ოპერატორმა', 'მომხმარებელმა') AS `completed`
                        FROM 	  asterisk_call_log
                        LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
						LEFT JOIN asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
	                    WHERE	  asterisk_call_log.id ='$res[asterisk_incomming_id]'");
    	
    	$num_row = $db->getNumRow();
        $req     = $db->getResultArray();
        
	}

	if ($num_row == 0){
        $data = '';
		$data .= '<tr>
                      <td style="border: 1px solid #CCC; padding: 5px; text-align: center; vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
                  </tr>';
                  
	}else{

        foreach($req['result'] AS $res2){
            $src = $res2['userfield'];
            $data = '';
    		$data .= '
    		      <tr>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2['call_datetime'].'</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2['time'].'</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2['name'].'</td>
                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$res2['completed'].'</td>
            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$src.'\')"><span style="color: #2a8ef2;">მოსმენა</span></td>
        	      </tr>';
        }
        
	}
	
    return $data;
}

function getUsersFromCampany($campaign_id){
    global $db;
    

    $db->setQuery(" SELECT      details.operator_id AS id, user_info.name
                    FROM        outgoing_campaign_request AS `req`
                    LEFT JOIN   outgoing_campaign_settings_group_schedules AS `group` ON group.id = req.outgoing_campaign_settings_group_schedule_id
                    LEFT JOIN   outgoing_campaign_settings_group_schedule_details AS `details` ON details.outgoing_campaign_settings_group_schedule_id = group.id
                    LEFT JOIN   user_info ON user_info.user_id = details.operator_id
                    LEFT JOIN   users ON    users.id = user_info.user_id
                    WHERE       req.id = '$campaign_id' AND users.actived = 1");

    $req = $db->getResultArray();

    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req['result'] AS $res){
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
    }
    
    return $data;

}

function getUsers($campaign_id){

    global $db;

    $db->setQuery(" SELECT      details.operator_id AS id, user_info.name
                    FROM        outgoing_campaign_request AS `req`
                    LEFT JOIN   outgoing_campaign_settings_group_schedules AS `group` ON group.id = req.outgoing_campaign_settings_group_schedule_id
                    LEFT JOIN   outgoing_campaign_settings_group_schedule_details AS `details` ON details.outgoing_campaign_settings_group_schedule_id = group.id
                    LEFT JOIN   user_info ON user_info.user_id = details.operator_id
                    LEFT JOIN   users ON    users.id = user_info.user_id
                    WHERE       req.id = '$campaign_id' AND users.actived = 1");

    $res = $db->getResultArray();

    $data = '';

    if($res['count'] > 0){
        foreach($res['result'] AS $op){
            $data .= '<tr class="user-line" data-id="'.$op['id'].'"><td>'.$op['name'].'</td><td data-percent>0%</td><td data-count><input class="release-input" type="text" value="0" data-count /></td></tr>';
        }
    }else{
        $data .= '<tr><td colspan=3>სია ცარიელია</td></tr>';
    }

    return $data;

}
function getUsersPercents($operators, $campaign_id){

    global $db;

    $data = '';
    $a = $operators;
    $a = explode(',', $a);

    
    if(count($a) > 0){
        $db->setQuery(" SELECT  `user_info`.`name`, 
                                `user_info`.`user_id` 
                        FROM    `user_info` 
                        LEFT JOIN `users` ON `users`.id = `user_info`.`user_id` 
                        WHERE `user_info`.`user_id` IN ($operators) AND users.actived = 1");

        $res = $db->getResultArray();
    
        if($res['count'] > 0){
            foreach($res['result'] AS $op){
                $data .= '<tr class="user-line" data-id="'.$op['user_id'].'">
                            <td>'.$op['name'].'</td>
                            <td data-percent>0%</td>
                        </tr>';
            }
        }
    }else{
        $data .= '<tr><td colspan=2>სია ცარიელია</td></tr>';
    }
    

    return $data;

}


function GetCampaignPage($userList){

    $data = '
    
    <table id="relocateTable">
        <thead>
            <th>ოპერატორი</th>
            <th>პროცენტული მაჩვენებელი</th>
            <th>რაოდენობა</th>
        </thead>
        
            '.$userList.'
       
    </table>
    ';
    return $data;
}
function GetCampaignPercentage($userList){

    $data = '
    
    <table id="relocateTable">
        <thead>
            <th>ოპერატორი</th>
            <th>პროცენტული მაჩვენებელი</th>
        </thead>
            '.$userList.'
       
    </table>
    ';
    return $data;
}



function getCampanySelects(){

    global $db;
    
    $db->setQuery("	SELECT  `id`, `name`
                    FROM    `outgoing_campaign_request`
                    WHERE   `actived` = 1");

    $req = $db->getResultArray();

    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req['result'] AS $res){
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
    }
    
    return $data;
}

function getNumbersCount($numbers){
    $numArray = explode(',', $numbers);

    $data = count($numArray);
    return $data;
}

function GetCampaignSelectPage($count){

    $data = '

    <table style="width: 100%; margin: 20px 0;table-layout: fixed;">
        <tr>
            <td style="vertical-align: middle; text-align: center; font-size: 13px;">
                <label style="font-size: 13px;">კამპანია</label><br />
                <select id="CampanySelect">'.getCampanySelects().'</select>
            </td>
            <td style="text-align: right; padding:0 10px; vertical-align: middle;">
            <span style="font-size: 13px;font-family: BPG;" id="labelRelease">აირჩიეთ კამპანია</span>
                <span style="font-size: 13px;font-family: BPG; display: none" id="ReleasedCall" data-oldcount="'.$count.'" data-count="'.$count.'">განაწილებული: <span>'.$count.'</span></span>
<br />
                <span style="font-size: 13px;font-family: BPG; display: none" id="notReleasedCall" data-oldcount="'.$count.'" data-count="0">გასანაწილებელი: <span>0</span></span>
                
            </td>
        </tr>
        <tr>
            <td colspan=2>
                <div id="relocateUserCall"></div>
            </td>
        </tr>
    </table>
    
    ';

    return $data;

}