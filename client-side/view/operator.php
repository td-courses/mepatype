<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
.dialog-grid{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
}

.dialog-grid textarea, .dialog-grid input {
    border-radius: 5px;
    border-color:#518bc5!important;
}

.text {
    font-size: 15px;
    color: #03a2ef;
    font-weight: bold;
}

.tab_grid{
    display: grid;
    grid-auto-flow: column;
    width: 50%;
    margin: 45px;
    height: 30px;
}

.tab_grid div{
    padding:10px;
    border-bottom: 4px solid #AAA;
    cursor:pointer;
    
}

.tab_grid1{
    display: grid;
    grid-auto-flow: column;
    width: 50%;
    margin: 30px;
    height: 30px;
}

.tab_grid1 div{
    padding:10px;
    border-bottom: 4px solid #AAA;
    cursor:pointer;
    
}

.tab span {
    margin: 20px;
    background: #00b306a8;
    padding: 1px 6px;
    border-radius: 50%;
    color:#FFF;
}
[i_id='2'] span {
    background: red;
}

[i_id='0'] span{
    display:none;
}

.highcharts-button-symbol{
    display:none!important;
}
.active {
    border-bottom: 4px solid #00b306a8!important;
}

.active1 {
    border-bottom: 4px solid #00b306a8!important;
}

.dialog-grid{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
}

.dialog-grid textarea, .dialog-grid input, .dialog-grid select {
    border-radius: 5px;
    border-color:#518bc5!important;
    width:100%;
    resize:vertical;
}

.text {
    font-size: 15px;
    color: #03a2ef;
    font-weight: bold;
}

#add-edit-form .grid1{
    display: grid;
    grid-auto-flow: column;
}

#add-edit-form .grid2{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 50% 30% 20%;
    width:95%;
}
#add-edit-form .grid3{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 10px;
}
#add-edit-form .grid4{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 23% 25% 51%;
    width: 97%;
}
#add-edit-form .grid5{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 5px;
}
#add-edit-form input[disabled]{
    background-color: #46b2e638;
    color: #000;
    text-align:center;
}

#add-edit-form-att input[disabled]{
    background-color: #46b2e638;
    color: #000;
    text-align:center;
}
.tab{
    font-size: 17px;
    font-family: BPG arial!important;
}
#appeal_button{
    margin-right: 545px;
    width: 120px;
    background: orange;
    color: #FFF;
}
#appeal_button[disabled]{
    background:#999;
}
.hide{
    display:none;
}
.operator_pic{
  background-image: url("media/images/icons/operator.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 80px;
  width: 80px;
  border-radius: 50%;
  margin-left: 30px;
}
.operator_wrapper{
    margin-top: 50px;
}
.head_wrap {
    display: flex;
    flex-direction: row;
}

.tab1 {
    font-size: 17px;
    font-family: BPG arial!important;
}

.operator_content{
    width: 75vw;
    font-family:pvn!important;
    font-weight:bold;
	margin-top: 50px;
}
.highcharts-credits{
    display:none!important;
}
.k {
    display: flex;
}
.k_t{
    display: flex;
}

.k1 {
    font-size: 60px;
}

.k_t .k1 {
    font-size: 40px;
}

.k2 {
    margin-top: 35px;
    font-size: 25px;
}

.k_t .k2{
    margin-top: 20px;
    font-size: 20px;
}

.k3 {
    margin-top: 30px;
    font-size: 30px;
    margin-left: 15px;
}

.op-grid{
   display: grid;
    grid-template-columns: 11% 7% 12% 10%;
    font-size: 16px;
    grid-gap: 5px;
    margin-bottom: 16px;
}

.title{
    font-size: 25px;
    margin-bottom: 25px;

}
.pie-wrap{
    width: 450px;
    position: relative;
    margin-top:  -26px;
    padding: 20px;
}
.shadow {
  -webkit-box-shadow: 3px 3px 5px 6px #ccc;  /* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
  -moz-box-shadow:    3px 3px 5px 6px #ccc;  /* Firefox 3.5 - 3.6 */
  box-shadow:         3px 3px 5px 6px #ccc;  /* Opera 10.5, IE 9, Firefox 4+, Chrome 6+, iOS 5 */
}

.pie-counter{
    position: absolute;
    top: 150px;
    right: 50px;
    font-size: 40px;
}

.pie-table-grid{
    
    display: grid;
    grid-template-columns: auto auto auto;
    width: 100%;
    margin-left: 35px;
    font-size: 20px;
    grid-gap: 5px;

}

.highcharts-container {
    margin-left:-60px!important;
}

.relative{
    position:relative;
}

.counters {
    display: grid;
    position: absolute;
    right: 45px;
    top: 235px;
    grid-auto-flow: row;
    grid-gap: 105px;
}
.attendance{
    width: 500px;
    height: 150px;
}

.discipline{
    width: 500px;
    height: 150px;
    margin-top: -80px;
}

.quality{
    width: 500px;
    height: 200px;
    margin-top: -80px;
}
.filter_grid{
    display: grid;
    grid-auto-flow: column;
    width: 400px;
    position: absolute;
}
.counter_text{
    font-size: 20px;
    text-align: center;
    margin-top: 15px;
}

 .counter_number {
    font-size: 40px;
    margin-left: 36%;
    margin-top: 15px;
    color: #008000;
}

.counter_indicator {
    width: 90%;
    height: 5%;
    background: #BBB;
    margin: auto;
    margin-top: 2%;
}

.attendance .counter_indicator div {
    background:#008000;
    height:100%;
    width:98.12%;
}

.quality .counter_indicator div {
    background:#008000;
    height:100%;
    width:93.61%;
}

.discipline .counter_indicator div {
    background:#008000;
    height:100%;
    width:92.55%;
}

.grid {
    display:grid;
}


.quality .grid {
    grid-template-columns: auto auto auto;
    font-size: 15px;
    margin-top: 10px;
    margin-left: 70px;
    grid-gap: 10px;
}

.pie-table-grid div {display:flex;}

.call_icon {
    background-image: url("media/images/icons/comunication/phone.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}
.chat_icon{
    background-image: url("media/images/icons/comunication/Chat.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}
.messenger_icon{
    background-image: url("media/images/icons/comunication/CB.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}
.fb_icon{
    background-image: url("media/images/icons/comunication/Messenger.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}
.web_icon{
    background-image: url("media/images/icons/comunication/Video.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}
.mail_icon{
    background-image: url("media/images/icons/comunication/E-MAIL.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}

.viber_icon{
    background-image: url("media/images/icons/comunication/Viber_BUSY1.png"); /* The image used */
  background-color: #FFF; /* Used if the image is unavailable */
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  height: 20px;
  width: 20px;
  margin-right: 5px;
}

.status_gr, .status_re {
    color:#FFF;
}



#add-edit-form-att .grid1{
    display: grid;
    grid-auto-flow: column;
}

#add-edit-form-att .grid2{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 50% 30% 20%;
    width:95%;
}
#add-edit-form-att .grid3 {
    display: grid;
    grid-auto-flow: column;
    grid-gap: 10px;
}
#add-edit-form-att .grid4{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 50%;
}
#add-edit-form-att .grid4_in{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 15px;
}
#add-edit-form-att .grid4_in_right{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 15px;
}
#add-edit-form-att .grid5{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 5px;
}

.choose_file{
    background: #46b2e638;
    font-size: 20px;
    text-align: center;
    height: 25px;
    border-radius: 5px;
    width: 90%;
    margin-left: 5%;
    margin-top: 28px;
    color: #be00e0;
    font-weight: bold;
    cursor:pointer;
}

</style>
	<script type="text/javascript">
    	var aJaxURL	= "server-side/view/operator.action.php";		//server side folder url
    	var tName	= "example";													//table name
    	var fName	= "add-edit-form";												//form name
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var aJaxURL_dis	= "server-side/view/disorders.action.php";
        var aJaxURL_att = "server-side/view/attendance.action.php";
        var aJaxURL_monitoring = "server-side/call/inc_monitoring.action.php"; 
    	    	
    	$(document).ready(function () { 
              
            $("[data-select='chosen']").chosen({ search_contains: true });
            $("#filter").button();
            get_chart();
            get_operator();
            get_tabs();
            get_operator_k();
            LoadTable(tName);	
//             LoadTable(tName+"1");
//             LoadTable(tName+"2");
            LoadTable(tName+"3");
            LoadTable(tName+"_shemowmebuli",12);
            LoadTable(tName+"_attendance",10);
    		GetButtons("add_button", "delete_button");			
    		SetEvents("add_button", "delete_button", "", tName, fName, aJaxURL_dis);
            SetEvents("", "", "check-all", tName+"_attendance", "add-edit-form-att", aJaxURL_att);
            
    	});

        function get_operator_k(){
            $.ajax({
                url:aJaxURL,
                data:{
                    act:"op-grid"

                },
                success:function(data){
                    $(".k3").html(`${Number(data.k).toFixed(2)}%`);
                    $(".op-grid").html(data.html);
                }
            })
        }

        $(document).on("dblclick", " #example_shemowmebuli tr", function () {
	    	table_hidde_id = ($(this).find("td").eq(0).html());
	    	tab_id         = $("li[id^='t_'][aria-selected=true]").attr('name');
	    	
		    obj        = new Object;
	    	obj.act    = "get_edit_page";
            obj.id     = table_hidde_id;
	    	obj.tab_id = tab_id;
            $.ajax({
                url: aJaxURL_monitoring,
                data:obj,
                success:function(data){
                    $("#add-edit-form-monitoring").html(data.page);
                    var buttons = {
                        "appeal":{
                                        text:"გასაჩივრება",
                                        id:"gasachivreba"
                                        // click: function(){
                                        //     $.ajax({
                                        //             url: aJaxURL_dis,
                                        //             data:{
                                        //                 act:"get_appeal_page",
                                        //                 id:$("#add-edit-form #hidden_id").val()
                                        //             },
                                        //             success:function(data){
                                        //                 $("#appeal-dialog-form").html(data.page);
                                        //                 LoadDialog("appeal-dialog-form");
                                        //             }
                                        //         })
                                        // }
                                    },
    			        "cancel": {
    			            text: "გაუქმება",
    			            id: "cancel-dialog",
    			            click: function () {
    			                $(this).dialog("close");
    			            }
    			        }
    			    };
    			    
        			GetDialog("add-edit-form-monitoring", 1553, "auto", buttons, 'center top');
        			var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
        			GetDataTable("table_history", aJaxURL, "get_list_history", 9, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&check_ab_pin="+$("#check_ab_pin").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
        			$("#table_history_wrapper").css('width','650px');
        			$("#table_history_length").css('display', 'none');
        			$("#category_id, #category_parent_id, #call_type_id").chosen();
        			$("#call_type_id_chosen").css('width', '165px');
        			$("#search_ab_pin, #play_audio, #gasachivreba").button();
                    $("#add-edit-form-monitoring input").prop("disabled",true);
        			$(".additional-table").addClass('hidden');

					var cat_parent_id = $("#category_parent_id").val();

					if(cat_parent_id == 407 || cat_parent_id == 937) {

						$("#additional").removeClass('hidden');

						if($("#category_id").val() == 938 || $("#category_id").val() == 937 || $("#category_id").val() == 931 || $("#category_id").val() == 940 || $("#category_id").val() == 939){
							
							displayAmountStandartSubCat("none");
							displayAmountInOutSubCat("block");
							$("#info").css("height","840px");
							
						}else{
							
							if(this.value == 423 || this.value == 931) {
								$(".friend").removeClass('hidden');
							} else {
								$(".friend").addClass('hidden');
							}

							displayAmountStandartSubCat("block");
							displayAmountInOutSubCat("none");
							
						}
					}
        			
        		}
            });
	    });



        $(document).on("click", "#gasachivreba", function () {
	    	table_hidde_id = $("#monit_hidde_id").val();
	    	
	    	obj     = new Object;
	    	obj.act = "get_add_page_gasachivreba";
            obj.id  = table_hidde_id;
            $.ajax({
                url: aJaxURL_monitoring,
                data:obj,
                success:function(data){
                    $("#add-edit-form-gasachivreba-dialog").html(data.page);
                    var buttons = {
        					"save": {
        			            text: "შენახვა",
        			            id: "save-dialog-gasachivreba",
        			            click: function () {
        			            	obj     = new Object;
        			    	    	obj.act = "save-dialog-gasachivreba";
        			                obj.id  = $("#monit_hidde_id").val();

        			                obj.monitoring_appealing_operator = $("#monitoring_appealing_operator").attr('user_id');
        			                obj.operator_yes_no               = $("#operator_appealing_yes").val();
        			                obj.operator_appealing_comment    = $("#operator_appealing_comment").val();
        			                
        			                obj.monitoring_appealing_manager  = $("#monitoring_appealing_manager").attr('user_id');
        			                obj.manager_yes_no                = $("#manager_appealing_yes").val();
        			                obj.manager_appealing_comment     = $("#manager_appealing_comment").val();

        			                obj.monitoring_appealing_call_center_manager = $("#monitoring_appealing_call_center_manager").attr('user_id');
        			                obj.call_center_yes_no                       = $("#call_center_manager_appealing_yes").val();
        			                obj.call_center_manager_appealing_comment    = $("#call_center_manager_appealing_comment").val();
        			                
        			                
        			                $.ajax({
        			                    url: aJaxURL_monitoring,
        			                    data:obj,
        			                    success:function(data){
        			                    	$("#hidde_monitoring_id").val('');
        			                    	$("#add-edit-form-gasachivreba-dialog").dialog('close');
        			                    	$("#add-edit-form").dialog('close');
        									var tab = $("li[id^='t_'][aria-selected=true]").attr('name');
        									
        									if(tab == 2){
        							    		LoadTable('shesafasebeli', 13, "get_list_shesafasebeli", change_colum_main, obj);
        								    }else if(tab == 3){
        								    	LoadTable('shemowmebuli', 13, "get_list_shemowmebuli", change_colum_main, obj);
        								    }else if(tab == 4){
        								    	LoadTable('gasachivrebuli', 15, "get_list_gasachivrebuli", change_colum_main, obj);
        									}else if(tab == 5){
        										LoadTable('dasrulebuli', 15, "get_list_dasrulebuli", change_colum_main, obj);
        									}
        			            		}
        			                });
        			            }
        			        },
        			        "cancel": {
        			            text: "გაუქმება",
        			            id: "cancel-dialog",
        			            click: function () {
        			                $(this).dialog("close");
        			            }
        			        }
        			    };
        			
        			GetDialog("add-edit-form-gasachivreba-dialog", 1020, "auto", buttons, 'center top');
        			$("#operator_appealing_yes, #manager_appealing_yes, #call_center_manager_appealing_yes").chosen();
                }
            });
	    });



        function displayAmountStandartSubCat(status) {
			$("#card_type_id_holder").css({
				display: status
			});
			$("#for_card_type_id").css({
				display: status
			});
		}

	    function displayAmountInOutSubCat(status) {
			$("#for_transaction_date_time").css({
				display: status
			});
			$("#transaction_date_time").css({
				display: status
			});
			$("#for_card_16_number").css({
				display: status
			});
			$("#card_16_number_holder").css({
				display: status
			});
		}

      function get_chart(){

            $.ajax({
                url: aJaxURL,
                data:{
                    act:"get_chart"
                },
                success:function(data){
                    $(".pie-wrap").html(data.html);
                    let values = data.values;
                    console.log(Number(values.web));
                    Highcharts.chart('container', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        },

                        plotOptions: {
                            pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            // showInLegend: true
                            }
                        },
                        colors: ['#2dc100', '#f80aea', '#030202', '#2196f3', '#7d3daf', '#e81d42', '#009895', '#15e5b6'],
                        
                        series: [{
            
                            name: 'Brands',
                            colorByPoint: true,
                            data: [{
                            name: 'ზარი',
                            y: Number(values.call),
                            // sliced: true,
                            selected: true
                            }, {
                            name: 'ჩათი',
                            y: Number.isNaN(Number(values.chat)) ? 0 : Number(values.chat)
                            }, {
                            name: 'მესენჯერი',
                            y: Number.isNaN(Number(values.messenger)) ? 0 : Number(values.messenger)
                            }, {
                            name: 'FB მესენჯერი',
                            y: Number.isNaN(Number(values.fb)) ? 0 : Number(values.fb)
                            }, {
                            name: 'ვაიბერი',
                            y: Number.isNaN(Number(values.viber)) ? 0 : Number(values.viber)
                            },{
                            name: 'ვებ-ზარი',
                            y: Number.isNaN(Number(values.web)) ? 0 : Number(values.web)
                            }, {
                            name: 'ელ-ფოსტა',
                            y: Number.isNaN(Number(values.mail)) ? 0 : Number(values.mail)
                            }]
                        }]
                        });

                }
            })
        }
        
    	function LoadTable(tName, count = 8, hidden){
    		var aJaxURL	= "server-side/view/operator.action.php";
            var gridName        = 	tName;
            var actions         = 	'';
            var editType        =   "popup"; // Two types "popup" and "inline"
            var itemPerPage     = 	40;
            var columnsCount    =	count;

            
            if(tName == 'example_shemowmebuli'){
                var columnsSQL      = 	[
                                            "id:string",
                                            "type:string",
                                            "all:string",
                                            "type1:string",
                                            "type2:string",
                                            "type3:string",
                                            "type4:string",
                                            "type5:string",
                                            "type6:string",
                                            "type7:string",
                                            "type8:string",
                                            "type9:string"
                                        ];
                var columnGeoNames  = 	[
                                            "ID", 
                                            "ფორმირების თ-ღი",
                                            "მომართვის თ-ღი",
                                            "წყარო",
                                            "ტიპი",
                                            "ქმედება",
                                            "ხან-ბა",
											"ოპერატორი",
                                            "განაწილება",
                                            "სტატუსი",
                                            "შედეგი",
                                            "მონიტორინგი"
                                        ];
    
                var showOperatorsByColumns  =   [0,0,0,0,0,0,0,0,0,0,0,0]; 
                var selectors               =   [0,0,0,0,0,0,0,0,0,0,0,0]; 
            }else if(tName == 'example_attendance'){
            	var columnsSQL = [
                                    "id:string",
                                    "type:date",
                                    "all:string",
                                    "type1:string",
                                    "type2:string",
                                    "type3:string",
                                    "type4:string",
                                    "type5:string",
                                    "type6:string",
                                    "type7:string"
                                	];
                var columnGeoNames = [
                                    "ID", 
                                    "თარიღი",
                                    "თანამშრომელი",
                                    "თანამდებობა",
                                    "ფაქტ. ხანგრძ.",
                                    "ნაკლებობა",
                                    "მეტობა",
                                    "ტიპი",
                                    "სტატუსი",
                                    "კომენტარი"
                					];

                var showOperatorsByColumns  =   [0,0,0,0,0,0,0,0,0,0]; 
                var selectors               =   [0,0,0,0,0,0,0,0,0,0]; 
            }else if(tName == 'example' || tName == 'example3'){
            	
            	var columnsSQL = [
                                    "id:string",
                                    "type:date",
                                    "all:string",
                                    "type1:string",
                                    "type2:string",
                                    "type3:string",
                                    "type4:string",
                                    "type5:string"
                                	];
                var columnGeoNames = [
                                    "ID", 
                                    "ფორმირების თ-ღი",
                                    "გასაჩივრების დედლაინი",
                                    "სახელი და გვარი",
                                    "თანამდებობა",
                                    "დსცპ დარღვევა",
                                    "საჯარიმო ქულა",
                                    "მიმდ.ბალანსი"
                					];

                var showOperatorsByColumns  =   [0,0,0,0,0,0,0,0]; 
                var selectors               =   [0,0,0,0,0,0,0,0]; 
            }
            
            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END
             
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL,"get_list_"+tName,itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&month="+$("#month").val()+"&year="+$("#year").val()+"&tab_id="+$(".active").attr("i_id"), 0);

//     		GetDataTable(tName, aJaxURL, "get_list_"+tName, count, `month=${$("#month").val()}&year=${$("#year").val()}&tab_id=${$(".active").attr("i_id")}`, 0, "", 1, "desc", "", change_colum_main);
//     		setTimeout(function(){$('.ColVis, .dataTable_buttons').css('display','none');}, 90);
        }
        
        setInterval(() => {
            $(".status_gr").parent().css("background","#00b3aa");
            $(".status_re").parent().css("background","#a31205");
        }, 100);

        let get_tabs = () => {
            $.ajax({
                url:aJaxURL,
                data:{
                    act:"get_tabs",
                    selected : $(".active1").attr("select")
                },
                success:(data) => {
                    console.log(data.tabs);
                    $(".tab_grid1").html(data.tabs);
                    if($("[i_id='2'] span").html() === "0"){
                        $("[i_id='2'] span").css("background","#00b306a8");
                    }else{
                        $("[i_id='2'] span").css("background","red");
                    }

                    let tab = $(".active1").attr("select");

                    if(tab == "t-2"){
                        $(".tab").removeClass("active");
                        $('[i_id="0"]').addClass("active");
                        $("#kxrsx").css("display","block");
                        $("#monitor").css("display","none");
                    }else{
                        $("#kxrsx").css("display","none");
                        $("#monitor").css("display","block");
                    }
                    
                }
            })
        }
    	
    	function LoadDialog(dialog = fName){
            switch (dialog){
                case "add-edit-form" :
                        var id		= $("#department_id").val();

                        var buttons = {
                            "appeal":{
                                        text:"გასაჩივრება",
                                        id:"appeal_button",
                                        click: function(){
                                            $.ajax({
                                                    url: aJaxURL_dis,
                                                    data:{
                                                        act:"get_appeal_page",
                                                        id:$("#add-edit-form #hidden_id").val()
                                                    },
                                                    success:function(data){
                                                        $("#appeal-dialog-form").html(data.page);
                                                        LoadDialog("appeal-dialog-form");
                                                    }
                                                })
                                        }
                                    },
                                    // "save": {
                                    //     text: "შენახვა",
                                    //     id: "save-dialog"
                                    
                                    // },
                                    "cancel": {
                                        text: "გაუქმება",
                                        id: "cancel-dialog",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                    
                                    

                                };

                        GetDialog(fName, 770, "auto", buttons);
                        
                        $("#add-edit-form,.add-edit-form-class").css("overflow","visible");
                        $("#add-edit-form input ").not(".chosen-search input").prop("disabled",true);
                        $("#add-edit-form textarea ").prop("disabled",true);
                        $("[data-select='chosen']").chosen({ search_contains: true });
                        $("#add-edit-form .chosen-container").css("pointer-events","none");
                        var bool = ($("#add-edit-form #hidden_id").val() === ""? true : false);
                        $("#appeal_button").prop("disabled", bool);
                break;
            case "appeal-dialog-form" :
                    var buttons1 = {
                            
                                    "save": {
                                        text: "შენახვა",
                                        id: "save-dialog-appeal"
                                    
                                    },
                                    "cancel": {
                                        text: "გაუქმება",
                                        id: "cancel-dialog-appeal",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                    
                                    

                                };

                        GetDialog(dialog, 770, "auto", buttons1);
                        $("[data-select='chosen']").chosen({ search_contains: true });
                break;
                case "add-edit-form-att":
                    var id		= $("#department_id").val();

                    var buttons = {
                        "appeal":{
                                    text:"გასაჩივრება",
                                    id:"appeal_button",
                                    click: function(){
                                            $.ajax({
                                                    url: aJaxURL_dis,
                                                    data:{
                                                        act:"get_appeal_page",
                                                        id:$("#add-edit-form-att #hidden_id").val()
                                                    },
                                                    success:function(data){
                                                        $("#appeal-dialog-form").html(data.page);
                                                        LoadDialog("appeal-dialog-form");
                                                    }
                                                })
                                        }
                                },
                                "cancel": {
                                    text: "გაუქმება",
                                    id: "cancel-dialog",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                                
                                

                            };

                    GetDialog(fName+"-att", 770, "auto", buttons);
                    $("[data-select='chosen']").chosen({ search_contains: true });
                    $("#add-edit-form-att,.add-edit-form-att-class").css("overflow","visible");
                    $("#add-edit-form-att input, #add-edit-form-att textarea").not(".chosen-search input").prop("disabled",true);
                    $("#add-edit-form-att .chosen-container").css("pointer-events","none");
                    var bool = ($("#add-edit-form-att #hidden_id").val() === ""? true : false);
                    $("#appeal_button").prop("disabled", bool);
                break;
            }
    	}

    	$(document).on("click", "#show_copy_prit_exel", function () {
	        if($(this).attr('myvar') == 0){
	            $('.ColVis,.dataTable_buttons').css('display','block');
	            $(this).css('background','#2681DC');
	            $(this).children('img').attr('src','media/images/icons/select_w.png');
	            $(this).attr('myvar','1');
	        }else{
	        	$('.ColVis,.dataTable_buttons').css('display','none');
	        	$(this).css('background','#E6F2F8');
	            $(this).children('img').attr('src','media/images/icons/select.png');
	            $(this).attr('myvar','0');
	        }
	    });

        function get_operator() {
            $.ajax({
                url:aJaxURL,
                data:{
                    act:"get_operator"
                },
                success:function(data){
                    $("#op_name").html(data.user);
                    $("#op_position").html(data.position);
                }
            })
        }


        // $(document).on("click", "#appeal_button", function (){
            
        // })

        $(document).on("click", "#save-dialog-appeal", function(){
            let appeal = (Number($("#operator_appealing_yes").val()) == 2);
            let disorder_id = ($("#add-edit-form-att").css("display") == 'block' ? $("#add-edit-form-att #hidden_id").val() : $("#add-edit-form #hidden_id").val());
            $.ajax({
                url:aJaxURL_dis,
                data:{
                    act:"save_appeal",
                    id:$("#appeal_hidden_id").val(),
                    disorder_id:disorder_id,
                    date:$("#monitoring_appealing_date").val(),
                    operator:$("#monitoring_appealing_operator").attr("user_id"),
                    operator_agree:$("#operator_appealing_yes").val(),
                    operator_comment:$("#operator_appealing_comment").val(),
                    maneger:$("#monitoring_appealing_manager").attr("user_id"),
                    maneger_agree:$("#manager_appealing_yes").val(),
                    maneger_comment:$("#manager_appealing_comment").val(),
                    head_manager:$("#monitoring_appealing_call_center_manager").attr("user_id"),
                    head_manager_agree:$("#call_center_manager_appealing_yes").val(),
                    head_manager_comment:$("#call_center_manager_appealing_comment").val(),
                    appeal:appeal
                },
                success:function(data){
                    $("#appeal-dialog-form").dialog('close');
                    LoadTable(tName);
                    get_tabs();
                }
            })
        })

	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_disorder";
	    	param.id		= $("#hidden_id").val();
	    	param.disorder		= $("#disorder").val();
            param.penalty		= $("#penalty").val();
            param.appeal		= $("#appeal").val();
            param.comment		= $("#comment").val();
            param.group		    = $("#group").val();
            param.operator_id   = $("#operator_id").val();
            param.status        = $("#status").val();

	    	
			if(param.name == ""){
				alert("შეავსეთ ველი!");
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable(tName);
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});


        $(document).on("click",".tab", function(){
            $(".tab").removeClass("active");
            $(this).addClass("active");
            let id = $(this).attr("i_id");
            let tab = $(".active1").attr("select");

            switch(tab){
                case "t-1":

                    break;
                case "t-2":
                    LoadTable(tName+"_shemowmebuli",12);
                    if($(this).attr("i_id") == "0"){
                        $("#kxrsx").css("display","block");
                        $("#monitor").css("display","none");
                    }
                    else {
                        $("#kxrsx").css("display","none");
                        $("#monitor").css("display","block");
                    }
                    break;
                case "t-3":
                    LoadTable(tName+"_attendance",10);
                    break;
                case "t-4":
                    LoadTable(tName);
                    break;
            }
        })

        $(document).on("click",".tab1", function(){

            $(".tab1").removeClass("active1");
            $(this).addClass("active1");
            get_tabs();
            $(".operator_content").hide();
            let this_select_tab = $(this).attr("select");
            $(".operator_content[tab-select="+this_select_tab+"]").removeClass("hide").show()
            let tab = $(".active1").attr("select");
            $(".tab").removeClass("active");
            $('[i_id="1"]').addClass("active");
            switch(tab){
                case "t-1":

                    break;
                case "t-2":
                    LoadTable(tName+"_shemowmebuli",12);
                    break;
                case "t-3":
                    LoadTable(tName+"_attendance",10);
                    break;
                case "t-4":
                    LoadTable(tName);
                    break;
            }
        })

        $(document).on("change", "#disorder", function(){
            var penalty = $("#disorder option:selected").attr("penalty");
            var appeal = $("#disorder option:selected").attr("appeal");
            $("#penalty").val(penalty);
            $("#appeal").val(appeal);
            
        })

        $(document).on("change", "#operator_id", function(){
            var group_name = $("#operator_id option:selected").attr("group_name");
            var appeal = $("#operator_id option:selected").attr("appeal");
            var point = $("#operator_id option:selected").attr("point");
            $("#group").val(group_name);
            $("#appeal").val(appeal);
            $("#balance").val(point);
            
        })

        // $(document).on("click", "[i_id]", function (){
            

        // })
	   
    </script>
</head>

<body>
    <div id="tabs" style="display: inline-block; position: absolute; width: 80%">
        <div class="head_wrap" >
        	<div class="operator_wrapper">
                <div class="operator_pic shadow"></div>
                <div style="margin-top: 30px;margin-left: 30px;">
                    <label id="op_name" style="font-size: 17px;font-weight: bold;"></label>
                    <label id="op_position" style="font-size: 15px;font-weight: 100;"></label> 
                </div>
            </div>
            
            <div class="filter_grid">
                <div>
                    <label style="margin-top: 8px;">თვე</label>
                    <select id="month" data-select="chosen" style="width: 140px;">
                            <?php
                                
                                global $db;
                                $db = new dbClass();
                                $html = "";
                                $db->setQuery("SELECT * FROM `month`");
                                $res = $db->getResultArray();
                                $db->setQuery("SELECT MONTH(NOW()) AS `mo`");
                                $req = $db->getResultArray();
                                $now = $req[result][0];
                                foreach($res[result] AS $arr){
                                    if ($arr['id'] == $now['mo'])
                                    $html .= "<option value='".$arr['id']."' selected >".$arr['name']."</option>";
                                    else
                                    $html .= "<option value='".$arr['id']."'>".$arr['name']."</option>";
                                }
                                echo $html;
                            ?>   
                    
                    </select>
                </div>
                <div>
                    <label style="margin-top: 8px;">წელი</label>
                    <select id="year" data-select="chosen" style="width: 100px;">
                            <?php
                                global $db;
                                $db = new dbClass();
                                $structure = "";
                                $db->setQuery(" SELECT year(NOW()) - 1, year(NOW()) AS 'now', year(NOW()) + 1 ");
                                $year_array = $db->getResultArray();
                                foreach($year_array[result][0] as $name => $year) {
                                    $selected = "";
                                    if ($name == 'now') $selected = "selected";
                                    $structure .= "<option value='".$year."' $selected >".$year."</option>";
                                }
                            
                                echo $structure;
                            ?>
                    
                    </select>
                </div>
                <div>
                    <button id="filter" >ფილტრი</button>
                </div>
            </div>

            <div>
                <div class="tab_grid">
                    <div class="active1 tab1 t-1" select ="t-1">მთავარი</div>
                    <div class="tab1 t-2" select ="t-2">ხარისხი</div>
                    <div class="tab1 t-3" select ="t-3">დასწრება</div>
                    <div class="tab1 t-4" select ="t-4">დისციპლინა</div>
				</div>
                <div class="tab_grid1"></div>
            </div>

        </div>
        <div class="operator_content " tab-select = "t-1">
        
            <div class="pie-wrap shadow"></div>
            <div class="counters">
                <div class="attendance shadow relative">
                    <div class="counter_text">დასწრების კოეფიციენტი</div>
                    <div class="counter_number">98.12%</div>
                    <div class="counter_indicator">
                        <div></div>
                    </div>
                </div>
                <div class="discipline shadow relative">
                    <div class="counter_text">დისციპლინის კოეფიციენტი</div>
                    <div class="counter_number">92.55%</div>
                    <div class="counter_indicator">
                        <div></div>
                    </div>
                </div>
                <div class="quality shadow relative">
                    <div class="counter_text">ხარისხის კოეფიციენტი</div>
                    <div class="counter_number">93.61%</div>
                    <div class="counter_indicator">
                        <div></div>
                    </div>

                    <div class="grid">
                        <div>შესაფასებელი</div>  <div>შეფასებული</div> <div>%</div>
                        <div>1200</div>  <div>1180</div>               <div>90</div> 
                            
                    </div>

                </div>
            </div>
        </div>
        <div class="operator_content hide" tab-select = "t-2">
            <div id = "monitor">
                <hr>
                <div id="example_shemowmebuli"></div>
        		<hr>
            </div>
			<div id="kxrsx" style="display:none">
				<div class="k"> <div class="k1">k</div><div class="k2">ხრსხ =</div> <div class="k3">93.61%</div> </div>       
             	<div class="op-grid"></div>
				<div id="example3"></div>
			</div>
        </div>

        <div class="operator_content hide" tab-select = "t-3">
        	<div id="example_attendance"></div>
        </div>
		<div class="operator_content hide" tab-select = "t-4"> 
            <div id="example"></div>
        </div>
	</div>

           <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="განყოფილებები">
    	<!-- aJax -->
	</div>
         <!-- jQuery Dialog -->
    <div id="appeal-dialog-form" class="form-dialog" title="საჩივარი">
    	<!-- aJax -->
	</div>

    <div id="add-edit-form-att" class="form-dialog" title="საჩივარი">
    	<!-- aJax -->
	</div>

    <div id="add-edit-form-monitoring" class="form-dialog" title="მონიტორინგი">
    	<!-- aJax -->
	</div>
    
    <div id="add-edit-form-gasachivreba-dialog" class="form-dialog" title="მონიტორინგი">
    	<!-- aJax -->
	</div>
</body>
</html>


