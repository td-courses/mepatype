<script src="Assets/js/lib/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="Assets/js/lib/kendoUI/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="Assets/js/lib/kendoUI/kendo.all.min.js"></script>
<script type="text/javascript" language="javascript" src="Assets/js/lib/kendoUI/pako_deflate.min.js"></script>
<script type="text/javascript" language="javascript" src="Assets/js/helpers/kendo.main.class.js"></script>
<link rel="stylesheet" href="Assets/css/page/authorization/authorization.css">
<link rel="stylesheet" href="Assets/css/lib/kendoUI/kendo.default-v2.min.css" />
<link rel="stylesheet" href="Assets/css/utils/kendo.css">
<script type="module">
    import Authorization from "./Assets/js/pages/authorization/authorization.class.js"

    new Authorization();
</script>

<section Authorization>


</section>