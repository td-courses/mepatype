<?php 
/* ******************************
 *	Workers aJax actions
 * ******************************
 */ 
// include('../../core.php');
include('../../includes/classes/class.Mysqli.php');

$mysqli = new dbClass();
$action 	= $_REQUEST['act'];
$user_id	= $_SESSION['USERID'];
$error 		= '';
$data 		= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
	    $per_id		= $_REQUEST['id'];
		$page		= GetPage(GetWorker($per_id));

        $data		= array('page'	=> $page);

        break;
	case 'get_list':
	    $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        
        $data = '';
        $query = "SELECT 	`user_language`.`id`, `user_language`.`name`
                            FROM 	`user_language`
                            WHERE `actived` = 1";
                            $mysqli -> setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

		break;
		
		
    case 'save_pers':
		$language_id 		= $_REQUEST['id'];
		$language 			= $_REQUEST['position'];
		
		if(empty($language_id)){
            AddWorker($language);
		}else{
			SaveWorker($language_id, $language);
		}

        break;
    case 'disable':
		$per_id = $_REQUEST['id'];
		DisableWorker($per_id);

		break;
   
    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Workers Functions
 * ******************************
 */
 
function AddWorker($language)
{
  
	global $mysqli;
    
    $user_id = $_SESSION["USERID"];

    $query="INSERT	INTO `user_language`
                (`user_id`,`name`,`actived`)
                VALUES
                ('$user_id','$language', 1)";
                $mysqli -> setQuery($query);
                $mysqli -> execQuery(); 


}

function SaveWorker($language_id, $language){	
    global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	
	$query = "UPDATE `user_language` 
              SET `user_language`.`name` = '$language',
                  `user_language`.`user_id` = '$user_id'
			  WHERE   `user_language`.`id` = $language_id";
    $mysqli -> setQuery($query);
    $mysqli -> execQuery();
}

function DisableWorker($per_id){	
    global $mysqli;
	
    $query = "UPDATE `user_language` SET
                    `actived` = 0
                WHERE  `user_language`.`id` = '$per_id'";
    $mysqli -> setQuery($query);
    $mysqli -> execQuery(); 
		
}

function GetWorker($per_id)
{
	global $mysqli;
    $query = "	SELECT `id`,`name`
                FROM `user_language`
                WHERE `id` = $per_id";
											
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}

function GetPage($res = '')
{
  
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table" id="info_table">

				<tr>
					<td class="label_td">
						<label for="person_position">ენა</label>
					</td>
					<td>
                        <input type="text" id="person_position" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['language'] . '" /> 
					</td>
				</tr>
			
			</table>
        </fieldset>
 	
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		
    </div>
    ';
	return $data;
}




?>