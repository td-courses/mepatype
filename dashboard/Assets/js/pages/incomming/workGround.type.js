import Tdg from "../../tdg.class.js";
import Input from "../../components/input/input.class.js";
import Datetime from "../../components/datetime/datetime.class.js";
import { kendo_SelectedID, kendo_setIndex } from "../../helpers/kendo.helper.js";
import Selector from "../../components/selector/selector.class.js";

export default class WorkGround extends Tdg{

    constructor(prop){
        super();

        self.WorkGround = this;
        self.selector = Selector

        this.state = {
            interface: this.CreateElement({
                element: "interface",
                attributes: ["workground"],
                "grid-column": 2
            }),
            element: [],
            fieldset: [],
            tab: [],
            inputs: [],
            prop: prop
        }

    }


    /**
     * INITIALIZE RIG
     */
    init = async () => {

        await this.build()
        
        return this.state.interface

    }

    /**
     * WORKGROUND INTERFACE
     * @returns {element} Workground Interface
     */
     build = async () => {
        
        this.buildTab()
        
    }


    createTab = async () => {

        this.state.element.tab = this.CreateElement({
            element: "tab",
            attributes:["incomming"]
        })

        let data = [{
            key: "info",
            title: "ინფო"
        },
        {
            key: "history",
            title: "ისტორია"
        },
        {
            key: "task",
            title: "დავალება"
        },
        {
            key: "question",
            title: "შეკითხვა"
        },
        {
            key: "sms",
            title: "SMS"
        },
        {
            key: "mail",
            title: "Mail"
        },
        {
            key: "comment",
            title: "კომენტარი"
        },
        {
            key: "record",
            title: "ჩანაწერი"
        },
        {
            key: "file",
            title: "ფაილები"
        }
    
    
    ]

        typeof data == 'object' && data.length > 0 && data.forEach(x => this.append(this.state.element.tab, this.createTabItem(x)))

        return this.state.element.tab;

    }

    createTabItem =  (tab) => {

        this.state.element.tabItem = this.CreateElement({
            element: "item",
            attributes: ["svg"],
            key: tab.key,
            title: tab.title,
            children: `<svg viewBox="0 0 16 16" width="16" height="16">
                            <use xlink:href="Assets/images/icons/icons.svg#tab-${tab.key}" />
                        </svg>`,
            onclick: this.handleTabClick
        }, this.CreateElement({
            element: "span",
            text: tab.title,
            style: {
                display: "none",
                visibility: "hidden"
            }
        }))

if(tab.key == "info"){
    this.state.element.tabItem.click()
}

        return this.state.element.tabItem

    }

    buildTab = async () => {

        this.append(this.state.interface, await this.createTab());

    }


    // loading before get response

    handleTabClick = function() {

        Object.values(self.WorkGround.state.fieldset).forEach(x => x.remove())

        // self.Helper.setLoadingForm(self.WorkGround.state.interface)

        self.WorkGround.state.element.tab.childNodes.forEach(x => {
            x.removeAttribute("actived")
            x.children[1].style.display = "none"
            x.children[1].style.visibility = "hidden"

        })

        let key =  this.getAttribute("key");

        if(key == 'info'){
            self.WorkGround.getInfoPage().then(() => {
                // self.Helper.removeLoadingForm
            })
        }      

       this.children[1].style = {display: "inline-block", visibility: "visible"}

        this.setAttribute("actived", true)

       

    }


    getInfoPage = async () => {

        this.state.inputData = await this.getResponse({
            route: "Automator",
            act: "getGeneratedInputs",
            pageKey: "incommingRequest",
            id: this.state.prop.id
        })

        this.state.inputData.length > 0 && this.state.inputData.forEach((x, i) => this.buildInfoInputData(x, i));

    }

    /**
     * 
     * @param {Object} field 
     * @param {Number} i 
     */
    buildInfoInputData = (field, i) => {

        this.createFieldSet(field, i)

    }


    createFieldSet = (field, i) => {

        let fieldsetName =  this.geo_to_latin(field.fieldsetName)

        this.state.fieldset[fieldsetName] = this.CreateElement({
            element: "div",
            attributes: ["fieldset"],
            title: field.fieldsetName
        })

        this.append(this.state.interface, this.state.fieldset[fieldsetName])
        
        typeof field.inputs != "undefined" && field.inputs.length > 0 && field.inputs.forEach(x => this.buildInfoInput(x, this.state.fieldset[fieldsetName]))

        field.tabs.length > 0 && this.createInfoTab(field.tabs, this.state.fieldset[fieldsetName], i)

    }

    
    createInfoTab = (tab, fieldset, i) => {
        
        this.state.tab[i] = this.CreateElement({
            element: "tab",
            attributes: ["fieldset"],
            "grid-column": "max"
        })

        this.append(fieldset, this.state.tab[i])
        tab.forEach((x, ii) => this.buildInfoTab(x, ii, this.state.tab[i], fieldset))

    }

    buildInfoTab = async (data, i, tab, fieldset) => {

        let content = this.CreateElement({
            element: "tabcontent",
            attributes: ["hidden"],
            "grid-column": "max",
        })

        let tabItem = this.CreateElement({
            element: "item",
            text: data.tabName,
            onclick: (e) => this.handleInfoTabItem(e.target, content, tab, fieldset)
        })

        this.append(tab, tabItem);

        if(i == 0){
            tabItem.click()
        }

        this.append(fieldset, content);

        data.inputs.length > 0 && data.inputs.forEach(x =>  this.buildInfoInput(x, content))

    }
  
    handleInfoTabItem = (e, content, tab, fieldset) => {

        tab.childNodes.forEach(x => x.removeAttribute("actived"))

        fieldset.childNodes.forEach(x => {
            if(x.localName == "tab") return false;
            x.setAttribute("hidden", true)
        })

        e.setAttribute("actived", true);

        content.removeAttribute("hidden"); 

    }


    /**
     * 
     * @param {Object} input 
     */
    buildInfoInput = (input, fieldset) => {

        let element;
        let column;

        switch(input.input_type){
            case "input":
                if(input.input_size == 0) input.input_size = 2
                element = new Input({
                    type: "text",
                    placeholder: input.input_name,
                    column: input.input_size,
                    id: input.input_key,
                    value: input.input_value
                }).build()


                this.state.inputs.push(element.childNodes[0]);

            break;
            case "textarea":
                if(input.input_size == 0) input.input_size = "max"
                element = new Input({
                    type: "textarea",
                    placeholder: input.input_name,
                    column: input.input_size,
                    id: input.input_key
                }).build()
                element.childNodes[0].value = input.input_value;
                this.state.inputs.push(element.childNodes[0]);
            break;
            case "number":

            break;
            case "date":
                if(input.input_size == 0) {
                    column =  `1 / -1`
                }else{
                    column = `auto / span ${input.input_size}`
                }
                element = this.CreateElement({
                    element: "input",
                    type: "date",
                    style: {
                        "grid-column": column
                    }
                })

            break;
            case "datetime":
                if(input.input_size == 0) {
                    column =  `1 / -1`
                }else{
                    column = `auto / span ${input.input_size}`
                }
                element = this.CreateElement({
                    element: "input",
                    type: "datetime",
                    style: {
                        "grid-column": column
                    }
                })
            break;
            case "radio":

            break;
            case "checkbox":

            break;
            case "select":
                if(input.input_size == 0) {
                    column =  `1 / -1`
                }else{
                    column = `auto / span ${input.input_size}`
                }
                
                

                element = this.CreateElement({
                    element: "kendo",
                    type: "selector",
                    title: input.input_name,
                    custom: input.input_parameters,
                    id: input.input_key,
                    value: (typeof input.input_value != 'undefined' ? input.input_value : 0),
                    style: {
                        "grid-column": column
                    }
                })

                
                
                this.state.inputs.push(element);

            break;
            case "multiselect":

            break;
            case "multilevelselect":

                if(input.input_size == 0) {
                    column =  `1 / -1`
                }else{
                    column = `auto / span ${input.input_size}`
                }

                let level_1 = this.CreateElement({
                    element: "kendo",
                    type: "selector",
                    id: input.input_key + '--1',
                    title: input.level_1_name,
                    custom: input.level_1_parameters,
                    style: {
                        "grid-column": column
                    }
                })

                let level_2 = this.CreateElement({
                    element: "kendo",
                    type: "selector",
                    id: input.input_key + '--2',
                    title: input.level_2_name,
                    custom: input.level_2_parameters,
                    style: {
                        "grid-column": column
                    }
                })

                let level_3 = this.CreateElement({
                    element: "kendo",
                    type: "selector",
                    id: input.input_key + '--3',
                    title: input.level_3_name,
                    custom: input.level_3_parameters,
                    style: {
                        "grid-column": column
                    }
                })

                this.state.inputs.push(level_1);
                this.state.inputs.push(level_2);
                this.state.inputs.push(level_3);

                this.append(fieldset, level_1, false, (cb) => {
                   
                        let id = kendo_SelectedID(cb.sender.element[0])
                        new Selector().setDataSource({
                            element: level_2,
                            data: {
                                route: "Automator",
                                act: "getLevel",
                                index: 1,
                                id: id,
                                inputKey: input.input_key
                            }
                        })
                })

                this.append(fieldset, level_2, false, (cb) => {
                   
                        let id = kendo_SelectedID(cb.sender.element[0])
                        new Selector().setDataSource({
                            element: level_3,
                            data: {
                                route: "Automator",
                                act: "getLevel",
                                index: 1,
                                id: id,
                                inputKey: input.input_key
                            }
                        })
                })
                this.append(fieldset, level_3)

            break;
        }

        if(typeof element == "undefined") return false;

        this.append(fieldset, element)

    }



}