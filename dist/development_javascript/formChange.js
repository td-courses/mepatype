const addBtn = document.querySelector('.add_button')
const popup = document.querySelector('.popup-window')
const popupQuestions = document.querySelector('.second_popup__addQuestions')
const popup2 = document.querySelector('.popup-window__blacked-BG')
const secondPopUpBtn = document.querySelector('.second_popupform__button')
const popupQuestionsBg = document.querySelector('.second_popup__addQuestions__blacked-BG')
const formSelect = document.querySelector('#formSelect')


const addQuestionBtn = document.querySelector('.addPollQuestionsBtn')




const event_form = document.querySelector('.event_form')
const post_form = document.querySelector('.post_form')
const poll_form = document.querySelector('.poll_form')


const popupWrapper = document.querySelector(".popup-window__container__wrapper")

if (addBtn) {
    const popupFormPostBtn = document.querySelectorAll('.popupFormPostBtn')


    event_form.style.display = "none";
    poll_form.style.display = "none";

    addBtn.addEventListener('click', () => {
        getUsersDepartmentGroups()

        popup.style.display = "flex"
        let editBtn = document.querySelectorAll('.post_editBtn')
        editBtn.forEach(el => el.classList.remove('display-block'))
        popupFormPostBtn.forEach(el => el.classList.add('display-block'))

        var viewPreview = document.querySelector('.video_preview');
        var iframe = document.querySelector('.video_preview__video iframe');

        if (youtubeUrlInp.value == "") {
            viewPreview.style.display = 'none';
        } else {
            viewPreview.style.display = 'block';
        }
    })

    popup2.addEventListener('click', () => {
        popup.style.display = "none"
    })

    secondPopUpBtn.addEventListener('click', () => {
        popupQuestions.style.display = "none"
    })


    formSelect.addEventListener('change', () => {

        if (formSelect.value == 'event') {
            post_form.style.display = "none";
            event_form.style.display = "grid";
            poll_form.style.display = "none";
            document.querySelector('#youtube_url').style.bottom = "43%";
        }


        if (formSelect.value == 'post') {
            post_form.style.display = "grid";
            event_form.style.display = "none";
            poll_form.style.display = "none";
            document.querySelector('#youtube_url').style.bottom = "23%";
        }

        if (formSelect.value == 'poll') {
            post_form.style.display = "none";
            event_form.style.display = "none";
            poll_form.style.display = "grid";
            document.querySelector('#youtube_url').style.bottom = "43%";
        }
    })



    popupQuestionsBg.addEventListener('click', _ => {
        popupQuestions.style.display = "none"
    })


    addQuestionBtn.addEventListener('click', _ => {
        popupQuestions.style.display = "flex"
    })


}
