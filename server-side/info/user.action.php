<?php 
/* ******************************
 *	Workers aJax actions
 * ******************************
 */ 
// include('../../core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action 	= $_REQUEST['act'];
$user_id	= $_SESSION['USERID'];
$error 		= '';
$data 		= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
	    $per_id		= $_REQUEST['id'];
		$page		= GetPage(GetWorker($per_id));

        $data		= array('page'	=> $page);

		break;
	case 'activate_user':
		$per_id 	= $_REQUEST['id'];
		$page		= getActivatePage(GetPerson($per_id));

		$data		= array('page' => $page);
	break;
	case 'activate_person':
		$per_id 	= $_REQUEST['id'];

		$name 		= $_REQUEST['name'];
		$email 		= $_REQUEST['email'];
		$phone1 	= $_REQUEST['phone1'];
		$phone2 	= $_REQUEST['phone2'];
		$username 	= $_REQUEST['username'];
		$password 	= md5($_REQUEST['password']);
		$group 		= $_REQUEST['group'];


		if(CheckUser($username)){
			$mysqli->setQuery("INSERT INTO users(`arda_person_id`,`username`,`password`,`group_id`,`actived`) VALUES('$per_id','$username','$password','$group','1')");
			$mysqli->execQuery();

			$mysqli->setQuery("SELECT id FROM users WHERE username='$username' LIMIT 1");
			$user_id = $mysqli->getResultArray();
			$user_id = $user_id['result'][0]['id'];


			$mysqli->setQuery("INSERT INTO user_info(`user_id`,`name`,`mail`,`home_phone`,`mobile_phone`) VALUES('$user_id','$name','$email','$phone1','$phone2')");
			$mysqli->execQuery();

			$mysqli->setQuery("UPDATE arda_persons SET actived='0' WHERE id='$per_id'");
			$mysqli->execQuery();

			$data['response'] = 'OK';
		}
		else{
			$data['response'] = 'failed';
		}

	break;
	case 'get_to_activate_list':
		$columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];
        $page_id        = $_REQUEST['page_id'];
		$mysqli->setQuery("	SELECT 	id,
									name,
									phone1,
									phone2,
									corp_number,
									ExtNumber,
									EmailAddress,
									PositionNameGeo,
									DepartmentNameGeo,
									UnitNameGeo,
									SSIPId,
									SSIPName

							FROM 	arda_persons
							WHERE 	actived = 1");
        $result = $mysqli->getKendoList($columnCount,$cols);

        $data = $result;
	break;
	case 'get_persons_api':
		$Curl            = curl_init();
        $xml_post_string = "UserName=ARDA&Password=ARDA_P6zZ95xX.M_pa6";

        $url             = "https://api.mepa.gov.ge/arda/PersonsViewHotLine";

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml_post_string
        );

        curl_setopt_array( $Curl, $options );
        $Result = curl_exec($Curl);

		curl_close($Curl);

		$user_data = json_decode($Result,true);

		$query = "INSERT INTO arda_persons(`id`,`name`,`phone1`,`phone2`,`corp_number`,`ExtNumber`,`EmailAddress`,`PositionNameGeo`,`DepartmentNameGeo`,`UnitNameGeo`,`SSIPId`,`SSIPName`,`datetime`) VALUES ";

		foreach($user_data AS $item){
			$query .= "('$item[PersonId]','$item[DisplayName]','$item[MobilePhoneNumber]','$item[CorporateMobilePhone]','$item[CorporatePhoneNumber]','$item[ExtNumber]','$item[EmailAddress]','$item[PositionNameGeo]','$item[DepartmentNameGeo]','$item[UnitNameGeo]','$item[SSIPId]','$item[SSIPName]',NOW()),";
		}

		$query = rtrim($query,',');
		$query .= "ON DUPLICATE KEY UPDATE
					name=VALUES(name),
					phone1=VALUES(phone1),
					phone2=VALUES(phone2),
					corp_number=VALUES(corp_number),
					ExtNumber=VALUES(ExtNumber),
					EmailAddress=VALUES(EmailAddress),
					PositionNameGeo=VALUES(PositionNameGeo),
					DepartmentNameGeo=VALUES(DepartmentNameGeo),
					UnitNameGeo=VALUES(UnitNameGeo),
					SSIPId=VALUES(SSIPId),
					SSIPName=VALUES(SSIPName)";
		$mysqli->setQuery($query);
		$mysqli->execQuery();

		$data['response'] = 'OK';
	break;
	case 'get_list':
	    $count = $_REQUEST['count'];
	    $hidden = $_REQUEST['hidden'];
		$query = "	SELECT 	`users`.`id`,
							`user_info`.`name`,
							'',

							`user_info`.`mobile_phone`,
							`position`.`person_position`,
							`user_info`.`address`				
					FROM   	`user_info` 
					left JOIN `position` ON `user_info`.`position_id` = `position`.`id`
					JOIN 	`users` ON `user_info`.`user_id` = `users`.`id`
					left join	extention ON `users`.extension_id = extention.id
					WHERE  	`users`.`actived` = 1";

		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

		break;
		
		case 'get_list_archive':
	    $count = $_REQUEST['count'];
	    $hidden = $_REQUEST['hidden'];
		$query = "	SELECT 	`users`.`id`,
							`user_info`.`name`,
							'',
							`extention`.`extention`,
							`user_info`.`mobile_phone`,
							`position`.`person_position`,
							`user_info`.`address`				
					FROM   	`user_info` 
					left JOIN `position` ON `user_info`.`position_id` = `position`.`id`
					JOIN 	`users` ON `user_info`.`user_id` = `users`.`id`
					left join	extention ON `users`.extension_id = extention.id
					WHERE  	`users`.`actived` = 0";

		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);

        break;

    case 'save_pers':
		$persons_id 		= $_REQUEST['id'];
    	$name 				= htmlspecialchars($_REQUEST['n'], ENT_QUOTES);
		$tin 				= $_REQUEST['t'];
		$position 			= $_REQUEST['p'];
		$address 			= htmlspecialchars($_REQUEST['a'], ENT_QUOTES);
		$image				= $_REQUEST['img'];
		$password			= $_REQUEST['pas'];
		$home_number		= $_REQUEST['h_n'];
		$mobile_number		= $_REQUEST['m_n'];
		$manager_id			= $_REQUEST['manager_id'];
		$comment			= $_REQUEST['comm'];
		$user				= $_REQUEST['user'];
		$userpassword		= md5($_REQUEST['userp']);
		$group_permission	= $_REQUEST['gp'];
		$dep_id             = $_REQUEST['dep_id'];
		$service_center_id  = $_REQUEST['service_center_id'];
		$branch_id          = $_REQUEST['branch_id'];
		$language_id        = $_REQUEST['lang'];
		$extension_id       = $_REQUEST['ext'];
		$project_selects    = $_REQUEST['project_selects'];
		$chat_nick    		= $_REQUEST['chat_nick'];


		$CheckUser 			= CheckUser($user);


		if(empty($persons_id)){
			if($CheckUser){
				AddWorker($user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment,  $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id, $language_id, $extension_id, $project_selects,$chat_nick,$manager_id);
			}else{
				$error = "მომხმარებელი ასეთი სახელით  უკვე არსებობს\nაირჩიეთ სხვა მომხმარებლის სახელი";
			}
		}else{
			SaveWorker($persons_id, $user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment,  $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id, $language_id, $extension_id, $project_selects,$chat_nick,$manager_id);
		}


		break;
	case 'get_columns':

		$columnCount = 		$_REQUEST['count'];
		$cols[] =           $_REQUEST['cols'];
		$columnNames[] = 	$_REQUEST['names'];
		$operators[] = 		$_REQUEST['operators'];
		$selectors[] = 		$_REQUEST['selectors'];
		//$query = "SHOW COLUMNS FROM $tableName";
		//$db->setQuery($query,$tableName);
		//$res = $db->getResultArray();
		$f=0;
		foreach($cols[0] AS $col)
		{
			$column = explode(':',$col);

			$res[$f]['Field'] = $column[0];
			$res[$f]['type'] = $column[1];
			$f++;
		}
		$i = 0;
		$columns = array();
		$types = array();
		foreach($res AS $item)
		{
			$columns[$i] = $item['Field'];
			$types[$i] = $item['type'];
			$i++;
		}
		
		
		$dat = array();
		$a = 0;
		for($j = 0;$j<$columnCount;$j++)
		{
			if(1==2)
			{
				continue;
			}
			else{
				
				if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
				$op = false;
				if($columns[$j] == 'id')
				{
					$width = "5%";
				}
				else if($columns[$j] == 'position'){
					$width = "12%";
				}
				else{
					$width = 'auto';
				}
				if($columns[$j] == 'inc_id')
				{
					$hidden = true;
				}
				else{
					$hidden = false;
				}
				if($res['data_type'][$j] == 'date')
				{
					$g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
				}
				else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
				{
					$g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
				}
				else
				{
					$g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
				}
				$a++;
			}
			array_push($dat,$g);
			
		}
		
		//array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
		
		$new_data = array();
		//{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
		for($j=0;$j<$columnCount;$j++)
		{
			if($types[$j] == 'date')
			{
				$new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
			}
			else if($types[$j] == 'number'){

				$new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
			}
			else
			{
				$new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
			}
		}
		
		$filtArr = array('fields'=>$new_data);
		
		
		
		$kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);

		
		//$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
		
		$data = $kendoData;
		//$data = '[{"gg":"sd","ads":"213123"}]';
		
	break;
    case 'disable':
		$per_id = $_REQUEST['id'];
		DisableWorker($per_id);

		break;
		case 'enable':
		$per_id = $_REQUEST['id'];
		EnableWorker($per_id);

        break;
	case 'delete_image':
		$file_name		= $_REQUEST['file_name'];
		DeleteImage($file_name);

		break;
	case 'view_img':
	    $page		= GetIMG($_REQUEST[id]);
	    $data		= array('page'	=> $page);
	     
	    break;
    case 'GetServiceCenter':
        $page		= GetServiceCenter('',$_REQUEST[branch_id]);
        $data		= array('page'	=> $page);
    
        break;
	case 'clear':
		$file_list = $_REQUEST['file'];
		ClearProduct();
		if (!empty($file_list)) {
			$file_list = ClearFiles(json_decode($file_list));
		}
		$data = array('file_list' => json_encode($file_list));

    default:
       $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Workers Functions
 * ******************************
 */
 
function GetIMG($id){
	global $mysqli;
	$query = "SELECT `image` FROM `user_info` WHERE `user_id` = $id ";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];	

    if (empty($res['image'])) {
        $image = '0.jpg';
    }else{
        $image = $res['image'];
    }
    $data = '<div id="dialog-form">
	           <fieldset>
                <img style="margin: auto;display: block;" width="350" height="350"  src="media/uploads/file/'.$image.'">
               </fieldset>
             </div>
            ';

    return $data;
}
function CheckUser($user){
	global $mysqli;
	$query = "SELECT `username`
						FROM   `users`
						WHERE  `username` = '$user'
						AND actived = 1";

	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();						

	if($result['count'] > 0){
		return false;
	}

	return true;
}



function ClearProduct() { 
	global $mysqli;
	$query = "SELECT	`id`,
    							`name`
						FROM `persons`";
	
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$req = $result['result'];	

	foreach( $req as $res){
		$name = htmlspecialchars($res[name], ENT_QUOTES);

		GLOBAL $log;
		$log->setUpdateLogBefore('persons', $res[id]);

		$query = "	UPDATE
		`persons`
		SET
		`name`	= '$name'
		WHERE
		`id`	= '$res[id]'";

		$mysqli -> setQuery($query);
		$mysqli -> execQuery();	

		$log->setUpdateLogAfter('persons', $res[id]);
	}
}

function AddWorker($user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment,  $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id, $language_id, $extension_id, $project_selects,$chat_nick,$manager_id)
{
    $task_send_mail	= $_REQUEST['task_send_mail'];
    $user_mail		= $_REQUEST['user_mail'];
    $site    		= $_REQUEST['site'];
    
	global $mysqli;
    if($user != '' && $userpassword !='' && $group_permission !=''){
        $ext			= $_REQUEST['ext'];
        if(strlen($_REQUEST['userp']) == 32){
    
        }else{
            $query="INSERT	INTO `users`
                        (`username`,`password`,`group_id`,`extension_id`)
                        VALUES
						('$user','$userpassword','$group_permission','$extension_id')";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
        }
    }
    
	$persons_id = $mysqli->getLastId();
	
	$user_id = $_SESSION["USERID"];
	
	$query = "INSERT INTO `user_info`
					(`user_id`, `name`, `tin`, `position_id`, `address`, `image`, `home_phone`, `mobile_phone`, `comment`, `dep_id`, `service_center_id`,  `mail`, `send_mail`, `lang`,`chat_nick`,`manager_id`)
				VALUES
				    ($persons_id, '$name', '$tin', $position, '$address', '$image', '$home_number', '$mobile_number', '$comment', '$branch_id', '$service_center_id',  '$user_mail', '$task_send_mail','$language_id','$chat_nick','$manager_id')";

	$mysqli -> setQuery($query);
	$mysqli -> execQuery();
	
	$site_array = explode(",",$site);
	$mysqli->setQuery("DELETE FROM users_site WHERE user_id = '$persons_id'");
	$mysqli->execQuery();


	$projects = explode(",", $project_selects);  
	$sep = '';

	foreach($projects as $project_id){
		$values .= $sep."($project_id,$persons_id, NOW())";
		$sep = ',';
	}
	
	$query = "INSERT 
				INTO `user_projects`
					(`project_id`, `person_id`, `datetime`)
				VALUES
					$values";
	$mysqli->setQuery($query);
	$mysqli->execQuery();

	
//	foreach($site_array AS $site_id){
//	    $mysqli->setQuery("INSERT INTO users_site (user_id, site_id) values ('$persons_id', $site_id)");
//	    $mysqli->execQuery();
//	}
	

}

function SaveWorker($persons_id, $user_id, $name, $tin, $position, $address, $image, $password, $home_number, $mobile_number, $comment, $user, $userpassword, $group_permission, $dep_id, $service_center_id, $branch_id, $language_id, $extension_id, $project_selects,$chat_nick,$manager_id)
{	global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	
	$task_send_mail	= $_REQUEST['task_send_mail'];
	$user_mail		= $_REQUEST['user_mail'];
	$site    		= $_REQUEST['site'];
	
	$group_id 		= checkgroup($user_id);

	
	$query = "UPDATE `user_info` 

                 SET `user_id`			 = '$persons_id',
					 `name`				 = '$name',
					 `tin`				 = '$tin',
					 `position_id`		 = '$position',
					 `dep_id`	    	 = '$branch_id',
					 `address`			 = '$address',
					 `image`			 = '$image',
				 	 `home_phone`		 = '$home_number',
					 `mobile_phone`  	 = '$mobile_number',
					 `comment`			 = '$comment',
					 `service_center_id` = '$service_center_id',
                     `mail`              = '$user_mail',
                     `send_mail`         = '$task_send_mail',
					 `chat_nick`         = '$chat_nick',
					 `manager_id`		 = '$manager_id',
					 `lang`				 = '$language_id'

			 WHERE   `user_id`           = $persons_id";
			$mysqli -> setQuery($query);
			$mysqli -> execQuery();
				
			$site_array = explode(",",$site);
			$mysqli->setQuery("DELETE FROM users_site WHERE user_id = '$persons_id'");
			$mysqli->execQuery();
			
//			foreach($site_array AS $site_id){
//			    $mysqli->setQuery("INSERT INTO users_site (user_id, site_id) values ('$persons_id', $site_id)");
//			    $mysqli->execQuery();
//			}

	if( $user!= '' && $userpassword!='' && $group_permission!=''){
		$ext			= $_REQUEST['ext'];
		if(strlen($_REQUEST['userp']) == 32){
			$query = "	UPDATE	`users` SET
									`users`.`username` = '$user',
									`users`.`group_id` = '$group_permission',
									`users`.`extension_id` = '$extension_id'
							WHERE	`users`.`id` = '$persons_id' AND `users`.actived = 1";
							$mysqli -> setQuery($query);
							$mysqli -> execQuery(); 
		}else{
		$query = "	UPDATE	`users` SET
								`users`.`username` = '$user',
								`users`.`password` = '$userpassword',
								`users`.`group_id` = '$group_permission',
								`users`.`extension_id` = '$extension_id'
						WHERE	`users`.`id` = '$persons_id'	&& `users`.actived = 1";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
		}
	}else{
		$query = "	UPDATE	`users` SET
								`users`.`extension_id` = '$extension_id'
						WHERE	`users`.`id` = '$persons_id' AND `users`.actived = 1";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
	}
	$data['error']='';
	// $query = "SELECT * FROM `user_projects` WHERE `person_id` = $person_id";
	// $mysqli->setQuery($query);
	// $result = $mysqli->getResultArray();
	// $count = count($result[result]);
	
	// if($count > 0){
	// 	$query = "DELETE FROM `user_projects` WHERE `person_id` = $persons_id";
	// 	$mysqli -> setQuery($query);
	// 	$mysqli -> execQuery(); 
	// }

	// $projects = explode(",", $project_selects);  
	// $sep = '';

	// foreach($projects as $project_id){
	// 	$values .= $sep."($project_id,$persons_id, NOW())";
	// 	$sep = ',';
	// }
	
	// $query = "INSERT 
	// 			INTO `user_projects`
	// 				(`project_id`, `person_id`, `datetime`)
	// 			VALUES
	// 				$values";
	// $mysqli->setQuery($query);
	// $mysqli->execQuery();
	
	
}

function DisableWorker($per_id)
{	global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	$group_id 		= checkgroup($user_id);

	if ($user_id != 2) {
		$query = "UPDATE `users` SET
							`actived` = 0
					WHERE  `users`.`id` = '$per_id'";
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 

		$query = "SELECT id 
												FROM user_status_log
												WHERE `user_id` = '$user_id'";
												// echo ">>". $res['id'];
												$mysqli -> setQuery($query);
		$result = $mysqli -> getResultArray();

		$res = $result['result'][0];
		if($res['id']==null){
			
			$query = "INSERT INTO `user_status_log` (`user_id`,`disable_time`,`enable_time`)
							VALUES ('$user_id', NOW(),'')
								";
			$mysqli -> setQuery($query);
			$mysqli -> execQuery(); 						
		}
		
		else{
			$query = "UPDATE `user_status_log` SET
						`disable_time` = NOW()
						WHERE  `user_id` = '$user_id'";
						$mysqli -> setQuery($query);
						$mysqli -> execQuery(); 
		}
		
	} else {
		global $error;
		$error = 'თქვენ არ გაქვთ თანამშრომლის წაშლის უფლება';
	}
}

function EnableWorker($per_id)
{	global $mysqli;
	$user_id 		= $_SESSION['USERID'];
	$group_id 		= checkgroup($user_id);

	if ($user_id != 2) {
		$query = "UPDATE `users` SET
							`actived` = 1
					WHERE  `users`.`id` = '$per_id'";
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 

		$query = "SELECT id 
														FROM user_status_log
														WHERE `user_id` = '$user_id'";
														// echo ">>". $res['id'];
														$mysqli -> setQuery($query);
														$result = $mysqli -> getResultArray();

														$res = $result['result'][0];
				if($res['id']==null){
					
					$query = "INSERT INTO `user_status_log` (`user_id`,`disable_time`,`enable_time`)
									VALUES ('$user_id','', NOW())
										";	
					$mysqli -> setQuery($query);
					$mysqli -> execQuery(); 
				}
				
				else{
					$query = "UPDATE `user_status_log` SET
								`enable_time` = NOW()
								WHERE  `user_id` = '$user_id'";
								$mysqli -> setQuery($query);
								$mysqli -> execQuery(); 
				}

	} else {
		global $error;
		$error = 'თქვენ არ გაქვთ თანამშრომლის აღდგენის უფლება';
	}
}


function GetPosition($id)
{
	global $mysqli;
	$data = '';
    $query = "SELECT 	`id`,
    						   	`person_position`
						FROM 	`position`
						WHERE 	`actived` = '1'";
						$mysqli -> setQuery($query);
						$data = $mysqli->getSelect($id,"person_position");
	

	return $data;
}
function GetManager($id){
	global $mysqli;
	$data = '';
    $query = "	SELECT	users.id,
						user_info.name
				FROM 	users
				JOIN 	user_info ON user_info.user_id = users.id
				WHERE 	users.actived = 1";
	$mysqli -> setQuery($query);
	$data = $mysqli->getSelect($id,"name");
	

	return $data;
}
function GetDepart($id){
	global $mysqli;
    $data = '';
    $query = "SELECT 	`id`,
    						   	`name`
						FROM 	`department`
						WHERE 	`actived` = '1'";
    $mysqli -> setQuery($query);
	$data = $mysqli -> getSelect();
	
    return $data;
}

function GetPerson($per_id)
{
	global $mysqli;
	$query = "	SELECT	id,
						name,
						EmailAddress,
						phone1,
						phone2
				FROM 	arda_persons
				WHERE 	id='$per_id' AND actived='1'";
											
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}

function GetWorker($per_id)
{
	global $mysqli;
    $query = "	SELECT	`users`.`id` as `id`,
                                    				`user_info`.`name` as `name`,
                                    				`user_info`.`tin` as `tin`,
                                    				`user_info`.`position_id` as `position`,
                                                    `user_info`.`dep_id` as `dep_id`,
                                    				`user_info`.`address` as `address`,
                                                    `user_info`.branch_id,
													`user_info`.manager_id,
                                                    `user_info`.service_center_id,
                                    				`user_info`.`image`,
                                                    -- `file`.`id` as `image_id`,
                                    				`users`.`username` as `username`,
                                    				`users`.`password` as `user_password`,
                                    				`users`.`group_id` as `group_id`,
                                    				`users`.`extension_id` as `ext`,
                                    				`user_info`.`home_phone` as `home_number`,
                                    				`user_info`.`mobile_phone` as `mobile_number`,
                                    				`user_info`.`comment` as `comment`,
                                                    `user_info`.mail,
													`user_info`.chat_nick,
                                                    `user_info`.send_mail,
													group_concat(`user_projects`.`project_id`) as project_id
                                            FROM	`user_info`
                                            LEFT JOIN	`users` ON `users`.`id` = `user_info`.`user_id`
                                            -- LEFT JOIN	`file` ON `users`.`id` = `file`.`user_id`
											LEFT JOIN `user_projects` ON `users`.`id` = `user_projects`.`person_id`
											WHERE	`user_info`.`user_id` = '$per_id'
											GROUP BY `users`.`id`";
											
	$mysqli -> setQuery($query);
	$result = $mysqli -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}

function DeleteImage($file_name)
{
	global $mysqli;
	$query = "UPDATE
				`file`
				SET
				`actived`			= 0
				WHERE
				`rand_name`			= '$file_name' ";
				$mysqli -> setQuery($query);
				$mysqli -> execQuery();	
}

function GetGroupPermission( $group_id, $check){
	global $mysqli;
	$data  = '';
	$where = "actived = 1";
	
	if ($check != '') {
	    $where = "actived IN(1,0)";
	}
	$query = "SELECT	`group`.id as `id`,
								`group`.`name` as `name`
						FROM	`group`
						WHERE   actived = 1";
	
    $mysqli -> setQuery($query);
    $data = $mysqli -> getSelect($group_id);
						


	return $data;
}


function GetProjects($selects){
	global $mysqli;

	$projects = '';
	$data = '';
	$id = '';
	

// -----------------------------------------------

	if($selects != ''){

		$mysqli->setQuery("	SELECT 		`id`, 
										`project` as `name`
							FROM 		directory__projects 
							WHERE 		id IN ($selects)
							GROUP BY 	project");

		$res2 = $mysqli->getResultArray();

		$fx = '';
		$in = '';

		foreach($res2['result'] as $res){
			$data .= '<option value="' . $res['id'] . '" selected>' . $res['name'] . '</option>';
			$in .= $fx."'".$res['name']."'";
			$fx = ',';

		}

		$mysqli->setQuery("	SELECT 		`id`, `project` as `name`
						FROM 		directory__projects 
						WHERE project NOT IN ($in)
						GROUP BY 	project");

		$res = $mysqli->getResultArray();

		foreach($res['result'] as $res){
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
		
	}else{
		$mysqli->setQuery("	SELECT 		`id`, `project` as `name`
							FROM 		directory__projects 
							GROUP BY 	project");

		$res = $mysqli->getResultArray();

		foreach($res['result'] as $res){
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}

	}

	// ---------------------------------------------------
	
	return $data;
}


function GetServiceCenter( $id,$branch_id ){
	global $mysqli;
	$data = '';
	$query = "	SELECT	`id`,
						`name`
				FROM	`service_center`
				WHERE   `actived` = 1 AND 
						`parent_id` = 0 AND 
						`branch_id` = $branch_id";
	$mysqli -> setQuery($query);
	$data = $mysqli -> getSelect();

	return $data;
}

function GetBranch( $id ){
	global $mysqli;
	$data = '';
	$query = "SELECT	`id`,`name`
						FROM	 `department`
	                    WHERE   `actived` = 1";
	$mysqli -> setQuery($query);
	$data = $mysqli->getSelect($id);
	return $data;
}

function GetLang($id){
	global $mysqli;
	$data = '';
	$query = "	SELECT	`id`,`name`
				FROM	 `user_language`
				WHERE   `actived` = 1";
	$mysqli -> setQuery($query);
	$data = $mysqli->getSelect($id);
	
	return $data;
}

function GetExt($id){
	global $mysqli;
	$data = '';
	$query = "	SELECT	`id`,`extention` As `name`
				FROM	 `extention`
				WHERE   `actived` = 1";
	$mysqli -> setQuery($query);
	$data = $mysqli->getSelect($id);
	
	return $data;
}

function web_site($id){
    global $mysqli;
    $data = '';
    
    $mysqli->setQuery("SELECT `id`, 
                              `name`
				       FROM   `my_web_site`
				       WHERE   actived = 1");
    
    $res = $mysqli->getResultArray();
    foreach ($res[result] AS $value){
        $mysqli->setQuery("SELECT id
                           FROM  `users_site`
                           WHERE  users_site.user_id = '$id' AND site_id = '$value[id]'");
        $check = $mysqli->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        }else{
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function checkgroup($user){
	global $mysqli;
	$query = " SELECT users.group_id           
				FROM   users          
				WHERE  users.id = $user";
				
				$mysqli -> setQuery($query);
				$result = $mysqli -> getResultArray();
		
				$res = $result['result'][0];										   
	 return $res['group_id']; 
}
function getActivatePage($res = ''){
	global $mysqli;
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table" id="info_table">
				<tr>
					<td class="label_td">
						<label for="name">სახელი, გვარი</label>
					</td>
					<td>
						<input type="text" id="name_a" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['name'] . '" />
					</td>
				</tr>
				
				
				
				
                <tr>
					<td class="label_td">
						<label for="address">მეილი
					</label></td>
					<td>
						<input type="text" id="user_mail_a" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['EmailAddress'] . '" />
					</td>
				</tr>
                
				
				<tr>
					<td class="label_td">
						<label for="home_number">ტელეფონი: </label>
					</td>
					<td>
						<input type="text" id="phone1_a" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['phone1'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="mobile_number">კორპ.ტელეფონი: </label>
					</td>
					<td>
						<input type="text" id="phone2_a" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['phone2'] . '" />
					</td>
				</tr>
				
			</table>
			<!-- ID -->
			<div id="accordion">
			  <h3>მომხმარებელი</h3>
			  <div>
				<div>
					<div style="display: inline;"><label for="user" style="float:left;">მომხმარებელი:</label>
						<input type="text" id="user_a" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['username'] . '" style="display: inline; margin-left: 36px;"/>
					</div>
				</div>
				<div style=" margin-top: 10px; ">
					<div style="display: inline;"><label for="user_password" style="float:left;">პაროლი:</label>
						<input type="password" id="user_password_a" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['user_password'] . '" style="display: inline; margin-left: 72px;"/>
					</div>
				</div>
                <div style=" margin-top: 10px; ">
					<div style="display: inline;"><label for="user_password" style="float:left;">ჯგუფი:</label>
						<select style="display: inline; margin-left: 86px; width: 165px;" id="group_permission_a">' . GetGroupPermission( $res['group_id'], $res[id]) . '</select>
					</div>
				</div>
				<div>
				
			 </div>
			</div>
        </fieldset>
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		<input type="hidden" id="is_user" value="'; 
		 $query = "SELECT id+1 AS `id` FROM users ORDER BY id DESC LIMIT 1" ;
		 $mysqli->setQuery($query);
		$result = $mysqli -> getResultArray();

		$incUs = $result['result'][0];
		$data .= $incUs[0];
		$data .= '" />
	</div>';
	
	return $data;
}
function GetPage($res = '')
{
    global $mysqli;
    $image = $res['image'];
	if(empty($image)){
		$image = '0.jpg';
	}else{
		if($image != '0.jpg')
	    	$disable_img = 'disabled';
	}
	$query = "SELECT extention.id, extention.extention FROM extention";
	$mysqli -> setQuery($query);
	
	$checked = "";
	if ($res[send_mail] == 1) {
        $checked = "checked";
    }
	$data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table" id="info_table">
				<tr>
					<td class="label_td">
						<label for="name">სახელი, გვარი</label>
					</td>
					<td>
						<input type="text" id="name" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['name'] . '" />
					</td>
				</tr>
				
				<tr>
					<td class="label_td">
						<label for="tin">პირადი ნომერი</label>
					</td>
					<td>
						<input type="text" id="tin" class="idle user_id" onblur="this.className=\'idle user_id\'" onfocus="this.className=\'activeField user_id\'" value="' . $res['tin'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="position">თანამდებობა</label>
					</td>
					<td>
						<select id="position" class="idls">' . GetPosition($res['position']) . '</select>
					</td>
				</tr>
                <tr>
					<td class="label_td">
						<label for="position">მენეჯერი</label>
					</td>
					<td>
						<select id="manager_id" class="idls">' . GetManager($res['manager_id']) . '</select>
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="branch_id">განყოფილება</label>
					</td>
					<td>
						<select id="branch_id" class="idls" >' . GetBranch($res['dep_id']) . '</select>
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="language_id">ენა</label>
					</td>
					<td>
						<select id="language_id" class="idls" >' . GetLang($res['lang']) . '</select>
					</td>
				</tr>
				<tr style="display:none;">
					<td class="label_td">
						<label for="extension_id">ექსთენშენი</label>
					</td>
					<td>
						<select id="extension_id" class="idls" >' . GetExt($res['ext']) . '</select>
					</td>
				</tr>
               <!-- <tr>
					<td class="label_td">
						<label for="address">დავალებაზე გაიგზავნოს მეილი</label>
                    </td>
					<td>
						<input style="margin-left: -103px;" type="checkbox" name="language" id="task_send_mail" value="1" autocomplete="off" '.$checked.'>
					</td>
				</tr> -->
                <tr>
					<td class="label_td">
						<label for="address">მეილი
					</label></td>
					<td>
						<input type="text" id="user_mail" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['mail'] . '" />
					</td>
				</tr>
                
				<tr>
					<td class="label_td">
						<label for="address">მისამართი
					</label></td>
					<td>
						<input type="text" id="address" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['address'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="home_number">სახლის ტელ: </label>
					</td>
					<td>
						<input type="text" id="home_number" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['home_number'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="mobile_number">მობილური ტელ: </label>
					</td>
					<td>
						<input type="text" id="mobile_number" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['mobile_number'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="chat_nick">ჩატის ნიკნეიმი: </label>
					</td>
					<td>
						<input type="text" id="chat_nick" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['chat_nick'] . '" />
					</td>
				</tr>
				<tr>
					<td class="label_td">
						<label for="comment">შენიშვნა: </label>
					</td>
					<td valign="top">
						<textarea id="comment" class="idle"  style="width: 230px !important;resize: vertical;">' . $res['comment'] . '</textarea>
					</td>
				</tr>
				
					<td>
						<label for="type_selector">პროექტები</label>
					</td>
					<td>
					<select multiple id="type_selector" >
							'.GetProjects($res['project_id']).'
						</select>
					</td>
				</tr>
			</table>
			<!-- ID -->
			<div id="accordion">
			  <h3>მომხმარებელი</h3>
			  <div>
				<div>
					<div style="display: inline;"><label for="user" style="float:left;">მომხმარებელი:</label>
						<input type="text" id="user" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['username'] . '" style="display: inline; margin-left: 36px;"/>
					</div>
				</div>
				<div style=" margin-top: 10px; ">
					<div style="display: inline;"><label for="user_password" style="float:left;">პაროლი:</label>
						<input type="password" id="user_password" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['user_password'] . '" style="display: inline; margin-left: 72px;"/>
					</div>
				</div>
                <div style=" margin-top: 10px; ">
					<div style="display: inline;"><label for="user_password" style="float:left;">ჯგუფი:</label>
						<select style="display: inline; margin-left: 86px; width: 165px;" id="group_permission">' . GetGroupPermission($res['group_id'], '') . '</select>
					</div>
				</div>
				<div>
				
			 </div>
			</div>
        </fieldset>
 	    <fieldset>
	    	<legend>თანამშრომლის სურათი</legend>

	    	<table class="dialog-form-table" width="100%">
	    		<tr>
					<td id="img_colum">
						<img style="margin-left: 5px;" width="105" height="105" id="upload_img" img="'.$image.'" src="media/uploads/file/'.$image.'" />
					</td>
				</tr>
				<tr>
					<td style="padding-left: 30px;">
						<span>
							<a href="#" onclick="view_image('.$res[image_id].')" class="complate">View</a> | <a href="#" id="delete_image" image_id="'.$res[image_id].'" class="delete">Delete</a>
						</span>
					</td>
				</tr>
				</tr>
					<td style="padding-left: 5px;">
						<div style="margin-top:10px; width: 127px; margin-left: -5px;" class="file-uploader">
							<input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
							<button id="choose_button'.$disable_img.'" class="center" >აირჩიეთ ფაილი</button>
						</div>
					</td>
				</tr>
			</table>
        </fieldset>
		<input type="hidden" id="pers_id" value="' . $res['id'] . '" />
		<input type="hidden" id="is_user" value="'; 
		 $query = "SELECT id+1 AS `id` FROM users ORDER BY id DESC LIMIT 1" ;
		$result = $mysqli -> getResultArray();

		$incUs = $result['result'][0];
		$data .= $incUs[0];
		$data .= '" />
    </div>
    ';
	return $data;
}




?>