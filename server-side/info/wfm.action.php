<?php
// MySQL Connect Link
include('../../includes/classes/class.Mysqli.php');
$db = new dbClass();

// Main Strings
$action                     = $_REQUEST['act'];
$error                      = '';
$data                       = '';


// Incomming Call Dialog Strings
$hidden_id         = $_REQUEST['id'];
$project_name      = $_REQUEST['project_name'];
$project_type      = $_REQUEST['project_type'];
$project_add_date  = $_REQUEST['project_add_date'];

global  $error;

switch ($action) {
	case 'get_project_list':

				$count = 		$_REQUEST['count'];
				$hidden = 		0;
				$db->setQuery("SELECT 	`project`.`id`,
										`project`.`id`,
										`project`.`name`,
										'',
										`project`.`max_hour_per_month`,
										`project`.`work_shift_max_hour`,
										`project`.`sl_percent`,
										(SELECT count(*) FROM work_shift WHERE project_id = project.id AND work_shift.actived = 1) AS shift_count,
										(SELECT count(*) FROM work_cycle WHERE project_id = project.id AND work_cycle.actived = 1) AS cycle_count,
										IF(ISNULL(`holidays`.`id`), 'არა', 'კი') as project_holiday,
										'0%'
								FROM    `project`
								LEFT JOIN project_holiday ON project_holiday.project_id = project.id
								LEFT JOIN holidays ON holidays.id = project_holiday.holidays_id
								WHERE `project`.`actived` = 1
								GROUP BY `project`.`id`
								ORDER BY `project`.`id` DESC");

				$data = $db->getList($count,$hidden,1);
		break;
	case 'get_add_page':
		$page		= GetPage('',object($hidden_id));
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':

		$page		= GetPage(object($hidden_id));
		$data		= array('page'	=> $page);

		break;

	case 'get_project':
        $db->setQuery("SELECT  `id`,
                		            `name`
                    		FROM 	`project`
                    		WHERE 	`actived` = 1");
        $res = $db->getResultArray();
        $page = '<option value="0">-----</option>';
        foreach ($res[result] AS $req){
	       $page .= '<option value="'.$req[0].'">'.$req[1].'</option>';
	    }
		$data		= array('proj'	=> $page);

		break;
	case 'get_GetHoliday':

	    $page       = GetHoliday();
	    $data		= array('holiday'	=> $page);

	    break;
	case 'get_hour':
	    $page		= GetHour($_REQUEST[wday],$_REQUEST[clock],$_REQUEST[project_id]);
	    $data		= array('hour'	=> $page);

	    break;
    case 'get_week':
        $page		= GetDialogWeek($_REQUEST[cycle],$_REQUEST[project_id]);
        $data		= array('week'	=> $page);

        break;
    case 'get_cikle':
        $page		= GetDialogCikle($_REQUEST[project_id]);
        $data		= array('week'	=> $page);

        break;
    case 'get_weekADD':
        $page		= GetDialogWeekAdd($_REQUEST[week_id],$_REQUEST[project_id],$_REQUEST[get_weekADD_id]);
        $data		= array('weekADD'	=> $page);

        break;
    case 'get_langdialog':
        $page		= GetDialogLangAdd($_REQUEST[week_id],$_REQUEST[project_id]);
        $data		= array('lang'	=> $page);

        break;
    case 'get_infosorce':
        $page		= GetDialogInfoSorceAdd($_REQUEST[week_id],$_REQUEST[project_id]);
        $data		= array('infosorce'	=> $page);

        break;
	case 'delete_week':
		$db->setQuery("	UPDATE  `week_day_graphic` SET
                		        `actived` = 0
                		WHERE   `id`='$hidden_id'");
		$db->execQuery();

		break;
	case 'delete_all_holiday':
	    $project_id    = $_REQUEST['project_id'];
	    $db->setQuery("	UPDATE  `project_holiday` SET
        	                    `actived` = 0
            	        WHERE   `project_id`='$project_id'");
$db->execQuery();
	    break;
    case 'delete_cikle':
        $id = $_REQUEST['id'];
        
        $db->setQuery("UPDATE  `week_day_graphic` SET
                             `actived` = 0
                     WHERE   `week_day_graphic`.`cycle`='$id'");
$db->execQuery();
        break;
    case 'delete_holiday':

        $holiday_id = $_REQUEST["id"];
        $db->setQuery("	UPDATE  `project_holiday` SET
                                `actived` = 0
                            WHERE `id` IN($holiday_id)");
        $db->execQuery();
        break;
    case 'delete_lang':
        $id = $_REQUEST['id'];
        $db->setQuery("	UPDATE  `week_day_lang` SET
                                `actived` = 0
                            WHERE   `id`='$id'");
        $db->execQuery();
        break;
    case 'delete_infosorce':
        $id = $_REQUEST['id'];
        $db->setQuery("	UPDATE  `week_day_info_sorce` SET
                                `actived` = 0
                        WHERE   `id`='$id'");
        $db->execQuery();
        break;
    case 'check_weak':
        $project_id    = $_REQUEST['project_id'];
        $wday          = $_REQUEST['wday'];
        $req = mysql_fetch_array(mysql_query("  SELECT  TIME_FORMAT(start_time,'%H:%i'),
                                                        TIME_FORMAT(end_time,'%H:%i'),
                                                        ext_number
                                                FROM `week_day_graphic`
                                                WHERE project_id = '$project_id' AND week_day_id = $wday"));
        $data = array('start_time'=>$req[0],'end_time'=>$req[1],'ext_number'=>$req[2]);
        break;
    case 'get_holiday':
        $count = 		$_REQUEST['count'];
        $hidden = 		$_REQUEST['hidden'];
        $db->setQuery("SELECT 	`project_holiday`.`id`,
                                        `holidays`.`date`,
                                        `holidays`.`name`,
                                        `holidays_category`.`name`
                                FROM    `project_holiday`
                                JOIN    `holidays` ON project_holiday.holidays_id = holidays.id
                                JOIN    `holidays_category` ON `holidays`.holidays_category_id = `holidays_category`.`id`
                                WHERE   `project_holiday`.`actived` = 1 AND `project_holiday`.`project_id` = '$_REQUEST[project_id]'");

       $db->getList($count, $hidden,1);
        break;
    case 'table_week':
        $count = 		$_REQUEST['count'];
        $hidden = 		$_REQUEST['hidden'];

        $cycli = $_REQUEST[hidde_cycle];

        $db->setQuery("SELECT  week_day_graphic.id,
                                        week_day.id,
                                        week_day.`name`,
                                        week_day_graphic.start_time,
                                        week_day_graphic.end_time,
                                        TIME_FORMAT(  TIMEDIFF( `end_time`,`start_time` ),'%H') AS `TIME_COUNT`,
                                        IF(week_day_graphic.type=1,'სამუშაო საათი','არა სამუშაო საათი'),
                                        (SELECT  GROUP_CONCAT(`language`.`name`)
                                        FROM `week_day_graphic` AS te1
                                        LEFT JOIN week_day_lang ON te1.id = week_day_lang.week_day_graphic_id AND week_day_lang.actived = 1
                                        LEFT JOIN `language` ON week_day_lang.spoken_lang_id = `language`.id
                                        WHERE te1.id = week_day_graphic.id AND language.actived = 1) AS `lang`,
                                        (SELECT  GROUP_CONCAT(source.`name`)
                                        FROM `week_day_graphic` AS te2
                                        LEFT JOIN week_day_info_sorce ON te2.id = week_day_info_sorce.week_day_graphic_id AND week_day_info_sorce.actived = 1
                                        LEFT JOIN source ON week_day_info_sorce.information_source_id = source.id
                                        WHERE te2.id = week_day_graphic.id) AS `info_sorce`
                                FROM `week_day_graphic`
                                JOIN week_day ON week_day.id=week_day_graphic.week_day_id
                                WHERE week_day_graphic.project_id = $_REQUEST[project_id]  AND week_day_graphic.actived = 1 AND week_day_graphic.cycle=$cycli");

        $db->getList($count, $hidden,1);
        break;
  case 'table_cikle':
            $count = 		$_REQUEST['count'];
            $hidden = 		$_REQUEST['hidden'];


            $db->setQuery("SELECT  week_day_graphic.cycle,
                            				CONCAT(CONVERT('სადგური ' USING utf8),week_day_graphic.cycle),
                            				TIME_FORMAT( TIMEDIFF(  SUM( `end_time` ), SUM(  `start_time`  ) ),'%H') AS `TIME_COUNT`,
                            				TIME_FORMAT( SEC_TO_TIME(  TIME_TO_SEC( TIMEDIFF(  SUM( `end_time` ), SUM(  `start_time`  ) ) ) ),'%H') * 4 AS `TIME_COUNT_4`,
                            				ROUND((TIME_FORMAT( SEC_TO_TIME(  TIME_TO_SEC( TIMEDIFF(  SUM( `end_time` ), SUM(  `start_time`  ) ) ) ),'%H,%i') / 40),2) AS `OPER_NEED`
                                    FROM `week_day_graphic`
                                    WHERE week_day_graphic.project_id = $_REQUEST[project_id] AND week_day_graphic.actived = 1
                                    GROUP BY week_day_graphic.cycle");

            $db->getList($count, $hidden,1);
			break;
	case 'get_cycle_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];

		$db->setQuery("SELECT 	`work_cycle`.`id`,
		                                `work_cycle`.`id`,
                        				`work_cycle`.`name`,
										GROUP_CONCAT(work_shift.`name` ORDER BY work_cycle_detail.num),
										SEC_TO_TIME(SUM(IF(TIMEDIFF(t1.end_date,t1.`start_date`) > '00:00:00', 

											 TIME_TO_SEC(t1.end_date) - TIME_TO_SEC(t1.`start_date`)  ,

											TIME_TO_SEC(t1.end_date) - TIME_TO_SEC(t1.`start_date`) + TIME_TO_SEC('24:00:00')

											)) - SUM(TIME_TO_SEC(`t1`.`timeout`))),
											SEC_TO_TIME(SUM(TIME_TO_SEC(`t1`.`timeout`)))
                                FROM 	`work_cycle`
		                        LEFT JOIN    `work_cycle_detail` ON work_cycle.id = `work_cycle_detail`.`work_cycle_id` AND work_cycle_detail.actived = 1
								LEFT JOIN    `work_shift` ON work_cycle_detail.work_shift_id = `work_shift`.`id`
								LEFT JOIN    `work_shift` AS t1 ON work_cycle_detail.work_shift_id = t1.`id` AND t1.type=1 
                                WHERE 	`work_cycle`.`actived` = 1
                                GROUP BY `work_cycle`.`id`");

		$data = $db->getList($count, $hidden,1);
		break;
	case 'get_cycle_inner_list' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];

	    $db->setQuery("SELECT 	`work_cycle_detail`.`id`,
	                                    `work_cycle_detail`.`num`,
                                        `work_shift`.`name`,
                                        `work_shift`.`start_date`,
                                        `work_shift`.`end_date`,
                                        `work_type`.`name`,
                                        `work_pay`.`name`,
                                        `work_shift`.`comment`,
                                        CONCAT('<div style=\"height: 100%; weight: 100%; background: ',`work_shift`.`color`,'\"></div>') AS `color`
                                FROM 	`work_cycle`
								LEFT JOIN work_cycle_detail ON work_cycle.id = work_cycle_detail.work_cycle_id
                                LEFT JOIN    work_shift ON work_cycle_detail.work_shift_id = work_shift.id
									AND work_shift.actived = 1
                                LEFT JOIN    work_pay ON work_shift.pay = work_pay.id
                                LEFT JOIN    work_type ON work_shift.type = work_type.id
                                WHERE 	work_cycle.`id` = '$_REQUEST[id]' AND `work_cycle_detail`.`actived` = 1");

	    $data = $db->getList($count, $hidden,1);

	    break;
    case 'get_holiday_list' :
        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];

        $db->setQuery("SELECT 	holidays.id,
										CONCAT(IF(holidays.year = '', '', holidays.year),
										      IF(holidays.year = '', '', '-'),
										      IF((holidays.month + 1) < 10, CONCAT('0',(holidays.month + 1)), holidays.month + 1),'-',
										      IF(holidays.date < 10, CONCAT('0',holidays.date), holidays.date))
                                        AS `date`,
		                                holidays.name,
										holidays_category.name AS holiday_category
							    FROM 	holidays
							    LEFT JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
							    WHERE 	holidays.actived=1
							    ORDER BY holidays.name");

        $data = $db->getList($count, $hidden,1);

        break;
		case 'get_holiday_list_inside_project' :
        $count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
		$project_id = $_REQUEST['project_id'];

		$db->setQuery("SELECT 	holidays.id,
									CONCAT(IF(holidays.year = '', '', holidays.year),
												IF(holidays.year = '', '', '-'),
												IF((holidays.month + 1) < 10, CONCAT('0',(holidays.month + 1)), holidays.month + 1),'-',
												IF(holidays.date < 10, CONCAT('0',holidays.date), holidays.date))
									AS `date`,
									holidays.name,
									holidays_category.name AS holiday_category,
									CONCAT('<button class=\"choose-holiday-for-project chfp-add\" data-id=\"',holidays.id,'\" data-category=\"',holidays.holidays_category_id,'\">დამატება</button>') AS action_button
								FROM 	holidays
								LEFT JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
								WHERE 	holidays.actived=1
								ORDER BY holidays.name");

        $data = $db->getList($count, $hidden,1);

		break;
	case 'fetch_holidays_table_build_data':

		$project_id = $_REQUEST["projectId"];

		$data = array(
			"rowControler" => get_project_holidays_data($project_id)
		);

		break;
    case 'disable_holiday_from_directory':
        $holiday_id	= $_REQUEST['id'];

        $db->setQuery("	UPDATE `holidays`
					SET    `actived` = 0
					WHERE  `id` IN($holiday_id)");
		$db->execQuery();
        break;
    case 'get_work_shift_list':
        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];

        $db->setQuery("SELECT 	`work_shift`.`id`,
		                                `work_shift`.`name`,
                        				`work_shift`.`start_date`,
		                                `work_shift`.`end_date`,
		                                `work_shift`.`timeout`,
		                                `work_type`.`name`,
		                                `work_pay`.`name`,
		                                `work_shift`.`comment`,
		                                CONCAT('<div style=\"height: 100%; weight: 100%; background: ',`work_shift`.`color`,'\"></div>') AS `color`
                                FROM    `work_shift`
				                        LEFT JOIN work_pay ON work_shift.pay = work_pay.id
				                        LEFT JOIN work_type ON work_shift.type = work_type.id
		                            WHERE   `work_shift`.`actived` = 1");

        $data = $db->getList($count, $hidden,1);

        break;
		case 'get_work_graphic_list':
        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];

        $db->setQuery("SELECT 	`project`.`id`,
				                                `project`.`id`,
		                        						`project`.`name`,
				                                '0%'
                                FROM    `project`
                                WHERE   `project`.`actived` = 1");

        $db->getList($count, $hidden,1);

        break;
	case 'add_all_holiday':
	    $project_id    = $_REQUEST['project_id'];
	    $user          = $_SESSION['USERID'];

	    $res = mysql_query("SELECT id
                            FROM `holidays`
                            WHERE actived = 1");
	    while ($req = mysql_fetch_array($res)){
	        $ch_hl = mysql_num_rows(mysql_query("SELECT id FROM project_holiday WHERE project_id=$project_id AND holidays_id=$req[0] AND actived = 1"));
    	    if($ch_hl == 0){
    	    mysql_query("INSERT INTO `project_holiday`
                         (`user_id`, `project_id`, `holidays_id`)
                         VALUES
                         ('$user', '$project_id', '$req[0]')");
    	    }
	    }
	    break;
    case 'add_holiday':
        $project_id    = $_REQUEST['project_id'];
        $holiday_id    = $_REQUEST['holiday_id'];
        $user          = $_SESSION['USERID'];

        $req = mysql_num_rows(mysql_query("SELECT id
                                           FROM `project_holiday`
                                           WHERE project_id = $project_id AND holidays_id = $holiday_id AND actived = 1"));
        if($req == 0){
        mysql_query("INSERT INTO `project_holiday`
                     (`user_id`, `project_id`, `holidays_id`)
                     VALUES
                     ('$user', '$project_id', '$holiday_id')");
        }else{
            $error = 'ეს დასვენების დღე უკვე დამატებულია!';
        }

        break;
    case 'get_wk':
        $project_id    = $_REQUEST['project_id'];

		$db->setQuery("UPDATE week_day_graphic
								SET actived = 0
								WHERE project_id = 0");
		$db->execQuery();
		
        $db->setQuery("SELECT  week_day_id,
                                    TIME_FORMAT(start_time,'%H%i') AS tt1,
                                    TIME_FORMAT(end_time,'%H%i') AS tt2,
                                    ext_number
                            FROM `week_day_graphic`
                            WHERE project_id = '$project_id' AND actived = 1 AND type = 1");
		$res = $db->getResultArray();

        $db->setQuery("SELECT  week_day_id,
                                     TIME_FORMAT(start_time,'%H%i') AS tt1,
                                     TIME_FORMAT(end_time,'%H%i') AS tt2,
                                     ext_number
                             FROM `week_day_graphic`
							 WHERE project_id = '$project_id' AND actived = 1 AND type = 2");
		$res1 = $db->getResultArray();

        foreach($res['result'] AS $req){

					$start_time = $req['tt1'];
					$end_time = $req['tt2'];

					if($end_time >= $start_time) {
							$data['work'][] = array('wday'	=> $req['week_day_id'],'starttime' => $start_time,'endtime' => $end_time,'ext_number' => $req['ext_number']);
					} else {

							$data['work'][] = array('wday'	=> $req['week_day_id'],'starttime' => $start_time,'endtime' => '2400','ext_number' => $req['ext_number']);
							$data['work'][] = array('wday'	=> $req['week_day_id'],'starttime' => '0000','endtime' => $end_time,'ext_number' => $req['ext_number']);

					}

        }

        foreach($res1['result'] AS $req1){
            $data['break'][] = array('wday'=> $req1['week_day_id'],'breakstarttime'=> $req1['tt1'],'breakendtime'=> $req1['tt2']);
        }

        break;

		case 'get_wk_add':
        $project_id    = $_REQUEST['project_id'];

        $db->setQuery("SELECT  week_day_id,
                                    TIME_FORMAT(start_time,'%H%i') AS tt1,
                                    TIME_FORMAT(end_time,'%H%i') AS tt2,
                                    ext_number
                            FROM `week_day_graphic`
                            WHERE project_id = '$project_id' AND actived = 1 AND type = 1");
		$query = $db->getResultArray();
        $db->setQuery("SELECT  week_day_id,
                                     TIME_FORMAT(start_time,'%H%i') AS tt1,
                                     TIME_FORMAT(end_time,'%H%i') AS tt2,
                                     ext_number
                             FROM `week_day_graphic`
                             WHERE project_id = '$project_id' AND actived = 1 AND type = 2");
		$query1 = $db->getResultArray();
        foreach($query['result'] AS $req){

						$start_time = $req['tt1'];
						$end_time = $req['tt2'];

						if($end_time >= $start_time) {
								$data['work'][] = array('wday'	=> $req['week_day_id'],'starttime' => $start_time,'endtime' => $end_time,'ext_number' => $req['ext_number']);
						} else {

								$data['work'][] = array('wday'	=> $req['week_day_id'],'starttime' => $start_time,'endtime' => '2400','ext_number' => $req['ext_number']);
								$data['work'][] = array('wday'	=> $req['week_day_id'],'starttime' => '0000','endtime' => $end_time,'ext_number' => $req['ext_number']);

						}

        }

        foreach($query1['result'] AS $req1){
            $data['break'][] = array('wday'=> $req1['week_day_id'],'breakstarttime'=> $req1['tt1'],'breakendtime'=> $req1['tt2']);
        }

        break;

		case 'save_wk':

				$id = $_REQUEST["id"];
				$days = $_REQUEST["days"];
				$data = array();

				foreach($days as $index => $day) {

					$day_id = $day["_id"];
					$holiday = $day["holiday"];

					if($holiday == "true") {

						$db->setQuery("UPDATE week_day_graphic
												SET actived = 0
												WHERE project_id = '$id' AND week_day_id = '$day_id'");
						$db->execQuery();
					} else {

						$start_time = $day["startTime"];
						$end_time = $day["endTime"];
						$data_count = $day["count"];

						for($i = 0; $i < $data_count; $i++) {
							$db->setQuery("INSERT INTO week_day_graphic
													(`project_id`, `week_day_id`, `type`, `start_time`, `end_time`, `cycle`, `actived`) VALUES
													('$id', '$day_id', '1', '$start_time', '$end_time', '0', '1')");
							$db->execQuery();
						}

					}

				}

				break;
    case 'work_gr':
        $db->setQuery("SELECT id
                       FROM week_day_graphic
                       WHERE id = '$_REQUEST[week_day_graphic_id]'");

        $res = $db->getNumRow();
        $db->setQuery(" SELECT IF(ISNULL(MAX(cycle)),1,MAX(cycle)+1) AS cycle
                        FROM `week_day_graphic`
                        WHERE actived = 1 AND project_id = $_REQUEST[project_id]");
        
        $req = $db->getResultArray();
        
        $res_c = $req[result][0];

        $cycli = $_REQUEST[hidde_cycle];

        if ($cycli == 0 ){
            $cycli=$res_c[cycle];
        }


        $db->setQuery("SELECT week_day_id AS cycle
                       FROM `week_day_graphic`
                       WHERE actived=1 AND project_id=$_REQUEST[project_id] AND cycle=$cycli AND week_day_id=$_REQUEST[wday]");
        if($res==0){

            if ($db->getNumRow()==0) {
                $db->setQuery("INSERT INTO `week_day_graphic`
                            (`project_id`, `week_day_id`, `start_time`, `end_time`, `type`, `cycle`)
                            VALUES
                            ('$_REQUEST[project_id]', '$_REQUEST[wday]', '$_REQUEST[start_time]', '$_REQUEST[end_time]', '$_REQUEST[type]', '$cycli');");

             $new_cycle = $db->getLastId();

             $db->setQuery("SELECT cycle AS cycle
                            FROM  `week_day_graphic`
                            WHERE  actived=1 AND week_day_graphic.id=$new_cycle");
             
             $cycllll = $db->getResultArray();
             $cycl    = $cycllll[result][0];
            }else {

                $error = 'მოცემული დღე უკვე არის სიაში';
            }


        }else{
            $db->setQuery("UPDATE `week_day_graphic` SET
                                `project_id`='$_REQUEST[project_id]',
                                `week_day_id`='$_REQUEST[wday]',
                                `type`='$_REQUEST[type]',
                                `start_time`='$_REQUEST[start_time]',
                                `end_time`='$_REQUEST[end_time]'
                         WHERE  `id`='$_REQUEST[week_day_graphic_id]'");
            $db->execQuery();

           $db->setQuery("SELECT cycle AS cycle
                          FROM  `week_day_graphic`
                          WHERE  actived=1 AND week_day_graphic.id='$_REQUEST[week_day_graphic_id]'");
           $cycllll = $db->getResultArray();
           $cycl    = $cycllll[result][0];

        }
        $data = array("new_cycle"	=> $cycl[cycle]);
        break;
    case 'add_lang':

         $check_lang=mysql_query("SELECT spoken_lang_id
                                  FROM  `week_day_lang`
                                  WHERE  actived=1 AND week_day_graphic_id=$_REQUEST[week_day_graphic_id] AND spoken_lang_id=$_REQUEST[spoken_lang_id]");

         if (mysql_num_rows($check_lang)==0) {


             mysql_query("INSERT INTO `week_day_lang`
                                     (`user_id`, `week_day_graphic_id`, `spoken_lang_id`)
                               VALUES
                                     ('$_SESSION[USERID]', $_REQUEST[week_day_graphic_id], '$_REQUEST[spoken_lang_id]');");
             }else {
                 $error='მოცემული ენა უკვე არის სიაში';
             }
             $cycl=mysql_fetch_assoc(mysql_query("SELECT cycle AS cycle
                                                  FROM  `week_day_graphic`
                                                  WHERE  actived=1 AND week_day_graphic.id=$_REQUEST[week_day_graphic_id]"));

             $data = array("new_cycle"	=> $cycl[cycle]);

        break;
    case 'add_infosorce':

        $check1=mysql_query("SELECT information_source_id
                             FROM  `week_day_info_sorce`
                             WHERE  actived=1 AND week_day_graphic_id=$_REQUEST[week_day_graphic_id] AND information_source_id=$_REQUEST[information_source_id]");

            if (mysql_num_rows($check1)==0) {


            mysql_query("INSERT INTO `week_day_info_sorce`
                                    (`user_id`, `week_day_graphic_id`, `information_source_id`)
                              VALUES
                                    ('$_SESSION[USERID]', $_REQUEST[week_day_graphic_id], '$_REQUEST[information_source_id]');");
            }else {

                $error='მოცემული წყარო უკვე არის სიაში';
            }
            $cycl=mysql_fetch_assoc(mysql_query("SELECT cycle AS cycle
                                                FROM  `week_day_graphic`
                                                WHERE  actived=1 AND week_day_graphic.id=$_REQUEST[week_day_graphic_id]"));

            $data = array("new_cycle"	=> $cycl[cycle]);


        break;
	case 'save-project':
		$hidden_id		  = $_REQUEST['project_hidden_id'];
    	$hidden_client_id = $_REQUEST['hidden_client_id'];
    	$start_date_holi  = $_REQUEST['start_date_holi'];
    	$end_date_holi    = $_REQUEST['end_date_holi'];

    	if($hidden_id==''){
    		Addproject($hidden_client_id, $project_name, $project_type, $project_add_date, $start_date_holi, $end_date_holi);
    	}else{
    		Saveproject($hidden_id,$project_name, $project_type, $project_add_date, $start_date_holi, $end_date_holi);
    	}

    	break;
	case 'get_list_lang':
	    $count = 		$_REQUEST['count'];
	    $hidden = 		$_REQUEST['hidden'];
	    $rResult = mysql_query("SELECT 	week_day_lang.id,
                        				language.`name`
                                FROM    `week_day_lang`
                                JOIN language ON week_day_lang.spoken_lang_id = language.id
                                WHERE   week_day_graphic_id = $_REQUEST[week_day_graphic_id] AND week_day_lang.actived = 1");

	    $data = array(
	        "aaData"	=> array()
	    );

	    while ( $aRow = mysql_fetch_array( $rResult ) )
	    {
	        $row = array();
	        for ( $i = 0 ; $i < $count ; $i++ )
	        {
	            /* General output */
	            $row[] = $aRow[$i];
	            if($i == ($count - 1)){
	                $row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="callapp_checkbox_lang_'.$aRow[$hidden].'" name="check_'.$aRow[$hidden].'" value="'.$aRow[$hidden].'" class="check" />
                                  <label style="margin-top: 2px;" for="callapp_checkbox_lang_'.$aRow[$hidden].'"></label>
                              </div>';
	            }
	        }
	        $data['aaData'][] = $row;
	    }
	    break;
    case 'get_list_infosorce':
        $count = 		$_REQUEST['count'];
        $hidden = 		$_REQUEST['hidden'];
        $rResult = mysql_query("SELECT 	week_day_info_sorce.id,
                        				source.`name`
                                FROM    `week_day_info_sorce`
                                JOIN    source ON week_day_info_sorce.information_source_id = source.id
                                WHERE   week_day_graphic_id = $_REQUEST[week_day_graphic_id] AND week_day_info_sorce.actived = 1");

        $data = array(
            "aaData"	=> array()
        );

        while ( $aRow = mysql_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i = 0 ; $i < $count ; $i++ )
            {
                /* General output */
                $row[] = $aRow[$i];
                if($i == ($count - 1)){
                    $row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="callapp_checkbox_infosorce_'.$aRow[$hidden].'" name="check_'.$aRow[$hidden].'" value="'.$aRow[$hidden].'" class="check" />
                                  <label style="margin-top: 2px;" for="callapp_checkbox_infosorce_'.$aRow[$hidden].'"></label>
                              </div>';
                }
            }
            $data['aaData'][] = $row;
        }
		break;
	case 'get_add_project_form':

		$data = array("dialog" => new_project_container_structure());

		break;
    case "get_project_edit_form":

		$project_id = $_REQUEST['projectId'];
        $data = array("dialog" => new_project_edit_form($project_id));

        break;

    case "save_new_project":

        $name           		= $_REQUEST["name"];
        $sl_type        		= $_REQUEST["slType"];
        $sl_percent     		= $_REQUEST["slPercent"];
        $sl_second_in   		= $_REQUEST["slSecondIn"];
        $sl_second_from 		= $_REQUEST["slSecondFrom"];
        $max_hours_per_month	= $_REQUEST["maxHourPerMonth"];
        $work_shift_max_hours 	= $_REQUEST["workShiftMaxHours"];
		$user_id        		= $_SESSION["USERID"];

		$data           = array("exists" => false);
		
		$db->setQuery("SELECT id FROM project 
					   WHERE name = '$name' AND actived=1");

		
		$data_count = $db->getNumRow();

		if($data_count > 0) {
			$data["exists"] = true;
		} else {
			$db->setQuery("INSERT INTO project 
                                      (user_id, name, create_date, max_hour_per_month, work_shift_max_hour, sl_percent, sl_second, sl_missed_from_second, sl_type_id)
						        VALUES 
                                      ('$user_id', '$name', NOW(), '$max_hours_per_month', '$work_shift_max_hours', '$sl_percent', '$sl_second_in', '$sl_second_from', '$sl_type')");
			
			$db->execQuery();

			$data["complete"] = 1;
		}

		break;
	case 'delete_existed_project':

		$selected_projects = $_REQUEST["selectedProjects"];

		$db->setQuery("UPDATE project SET actived = 0 WHERE id IN($selected_projects)");
		$db->execQuery();
		$data = array("deleteStatus" => 1);

		break;
	case "update_existed_project":

		$project_id				= $_REQUEST["projectId"];
        $name           		= $_REQUEST["name"];
        $sl_type        		= $_REQUEST["slType"];
        $sl_percent     		= $_REQUEST["slPercent"];
        $sl_second_in   		= $_REQUEST["slSecondIn"];
        $sl_second_from 		= $_REQUEST["slSecondFrom"];
        $max_hours_per_month	= $_REQUEST["maxHourPerMonth"];
        $work_shift_max_hours 	= $_REQUEST["workShiftMaxHours"];
		$user_id        		= $_SESSION["USERID"];

		$data           = array("exists" => false);
		
		$db->setQuery("SELECT id FROM project 
									   WHERE name = '$name' AND id != '$project_id'");
		$data_count = $db->getResultArray();
		$data_count = $data_count['count'];

		if($data_count > 0) {
			$data["exists"] = true;
		} else {
			$db->setQuery("UPDATE project
												   SET 	name = '$name',
														max_hour_per_month = '$max_hours_per_month',
														work_shift_max_hour = '$work_shift_max_hours',
														sl_percent = '$sl_percent',
														sl_second = '$sl_second_in',
														sl_missed_from_second = '$sl_second_from',
														sl_type_id = '$sl_type'
													WHERE id = '$project_id'");
			$db->execQuery();									   		
			$data["complete"] = true;
		}

        break;
    case 'save_holiday':
		
        $holiday_id 	= $_REQUEST['id'];
        $user       	= $_SESSION["USERID"];
        $category   	= $_REQUEST["category"];
		$creeping		= $_REQUEST["creeping"];
        $year       	= $_REQUEST["year"];
        $month       	= $_REQUEST["month"];
        $date       	= $_REQUEST["date"];
        $name       	= $_REQUEST["name"];
        $data       	= array("exists" => false, "complete" => false, "responseText" => "");

        $db->setQuery("SELECT name FROM holidays WHERE name = '$name' AND actived = 1 AND id != '$holiday_id'");
        
		$data_count = $db->getNumRow();
		
		if($data_count > 0) {
			$data["exists"] = true;
		} else {

			if($holiday_id != "") {

				$db->setQuery("UPDATE holidays
										SET holidays_category_id = '$category',
												creeping = '$creeping',
												year = '$year',
												month = '$month',
												date = '$date',
												name = '$name'
										WHERE holidays.id = '$holiday_id'");
				$db->execQuery();
				$data["complete"] = true;
				$data["responseText"] = "განახლდა";

			} else {

				$add_holiday = $db->setQuery("INSERT INTO holidays (holidays_category_id, creeping, user_id, year, month, date, name, actived)
											VALUES ('$category', '$creeping', '$user', '$year', '$month', '$date', '$name', 1)");
				$db->execQuery();
				$data["complete"] = true;
				$data["responseText"] = "დაემატა ბაზაში";

			}

		}

        break;

    case "get_existed_project_list":

        $list = get_existed_project_list();
        $data = array("list" => $list);

        break;
    case "add_new_holiday":

        $user       = $_SESSION["USERID"];
        $category   = $_REQUEST["category"];
        $date       = $_REQUEST["date"];
        $name       = $_REQUEST["name"];
        $data       = array("exists" => false);

        $check_holiday = mysql_query("SELECT name FROM holidays WHERE name = '$name' AND actived = 1");
        $data_count = mysql_num_rows($check_holiday);

        if($data_count > 0) {
            $data["exists"] = true;
        } else {

            $add_holiday = mysql_query("INSERT INTO holidays (holidays_category_id, user_id, date, name, actived)
                                                      VALUES ('$category', '$user', '$date', '$name', 1)");
            $data["complete"] = $add_holiday;
        }

        break;
	case "get_holiday_for_project_table":

        $structure = get_holiday_for_project_table();
        $data = array("structure" => $structure);

		break;
	case "get_all_official_holidays":

			$user_id = $_SESSION["USERID"];
			$project_id = $_REQUEST["projectId"];

			$db->setQuery(" SELECT holidays.`id`
							FROM holidays
							JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
							WHERE holidays_category.name = 'ოფიციალური დასვენება'
							AND holidays.actived = 1");
			
			$query_official_holidays = $db->getResultArray();
			
			$official_holidays_count = $db->getNumRow();
			$project_holidays_count = project_official_holidays_count($project_id);

			if($official_holidays_count > $project_holidays_count) {

			    foreach ($query_official_holidays[result] AS $res_official_holidays) {

					$holiday_id = $res_official_holidays["id"];
					$project_needs_holiday = check_if_project_has_holiday($project_id, $holiday_id);
	
					if($project_needs_holiday) {
	
						$db->setQuery("INSERT INTO project_holiday (`user_id`, `project_id`, `holidays_id`, `actived`)
											VALUES ('$user_id', '$project_id', '$holiday_id', 1)");
						$db->execQuery();
	
					}
	
				}

			} else if($official_holidays_count == $project_holidays_count) {
				$save = "allSelected";
			}

			$data = array("saveStatus" => true);
	
			break;
	case 'get_activities_table_list' :
        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];

        $db->setQuery("SELECT 	`work_activities`.`id`,
		                                `work_activities`.`name`,
		                                `work_pay`.`name`,
		                                `work_activities`.`comment`,
		                                CONCAT('<div style=\"height: 100%; weight: 100%; background: ',`work_activities`.`color`,'\"></div>') AS `color`
                                FROM    `work_activities`
		                        		JOIN work_pay ON work_activities.pay = work_pay.id
                                WHERE   `work_activities`.`actived` = 1");

        $data = $db->getList($count,$hidden,1);

        break;
    case 'get_holiday_form':
        $page		= get_holiday_form();
        $data		= array('page'	=> $page);

        break;
    case 'edit_holiday_form':
        $holiday_id		= $_REQUEST['id'];
        $page		= get_holiday_form(GetHolidayData($holiday_id));
        $data		= array('page'	=> $page);

        break;
    case 'get_workshift_form':
        $page		= get_workshift_form();
        $data		= array('page'	=> $page);

        break;
		case 'get_cycle_form_create':
        $page		= get_cycle_form_edit_create();
        $data		= array('page'	=> $page);

        break;
		case 'edit_cycle_form':
				$cycle_id = $_REQUEST["id"];
        $page		= get_cycle_form_edit($cycle_id);
        $data		= array('page'	=> $page);

        break;
    case 'edit_workshift_form':
        $workshift_id		= $_REQUEST['id'];
        $page		= get_workshift_form(GetWorkShiftData($workshift_id));
        $data		= array('page'	=> $page);

        break;
	case 'save_work_shift':
		
		$user_id		= $_SESSION['USERID'];
        $workshift_id 	= $_REQUEST['id'];
        $start_date     = $_REQUEST['start_date'];
        $end_date       = $_REQUEST['end_date'];
        $color          = $_REQUEST['color'];
        $type           = $_REQUEST['type'];
        $name           = $_REQUEST['name'];
        $pay            = $_REQUEST['pay'];
        $comment        = $_REQUEST['comment'];
		$timeout        = $_REQUEST['timeout'];
		$data			= array("saveStatus" => "");

		$check_query 		= $db->setQuery("SELECT id FROM work_shift WHERE name = '$name' AND actived = 1 AND id != '$workshift_id'");
		$check_data_count 	= $db->getResultArray();
		$check_data_count 	= $check_data_count['count'];

		if($check_data_count > 0) {
			$data["saveStatus"] = "exists";
		} else {

			if ($workshift_id == '') {

				$db->setQuery("INSERT INTO `work_shift`
										(`user_id`, `start_date`, `end_date`, `color`, `type`, `name`, `pay`, `comment`, `timeout`)
							VALUES	 	('$user_id', '$start_date', '$end_date', '$color', '$type', '$name', '$pay', '$comment', '$timeout')");
				$db->execQuery();
				$data["saveStatus"] = true;
	
			} else {
	
				$db->setQuery("UPDATE `work_shift`
											SET     `start_date` = '$start_date',
													`end_date`   = '$end_date',
													`color`      = '$color',
													`type`       = '$type',
													`name`       = '$name',
													`pay`        = '$pay',
													`comment`    = '$comment',
													`timeout`    = '$timeout',
													`user_id`    = '$user_id'
											WHERE	`id` = $workshift_id");
				$db->execQuery();
				$data["saveStatus"] = true;
	
			}

		}

        break;
    case 'delete_work_shift_from_directory':
        $workshift_id	= $_REQUEST['id'];
        delete_work_shift($workshift_id);

        break;
    case 'get_activities_form':
        $page		= get_activities_form();
        $data		= array('page'	=> $page);

        break;
    case 'edit_activities_form':
        $activities_id		= $_REQUEST['id'];
        $page		= get_activities_form(GetActivitiesData($activities_id));
        $data		= array('page'	=> $page);

        break;
    case 'get_activities_range_list' :

        $count	= $_REQUEST['count'];
        $hidden	= $_REQUEST['hidden'];
        $work_activities_id = $_REQUEST['work_activities_id'];

        if($work_activities_id == ''){
            $work_activities_RES = mysql_fetch_array(mysql_query("SELECT MAX(id)+1 AS `id` FROM `work_activities`"));
            $work_activities_id = $work_activities_RES[0];
        }

        $rResult = mysql_query("SELECT  work_activities_detail.id,
                            		    work_activities_detail.`start`,
                            		    work_activities_detail.`end`
                                FROM `work_activities_detail`
                                WHERE work_activities_id = '$work_activities_id' AND work_activities_detail.actived = 1");

        $data = array(
            "aaData"	=> array()
        );

        while ( $aRow = mysql_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i = 0 ; $i < $count ; $i++ )
            {
                /* General output */
                $row[] = $aRow[$i];
                if($i == ($count - 1)){
                    $row[] = '<div class="callapp_checkbox">
                                  <input type="checkbox" id="callapp_checkbox_'.$aRow[$hidden].'" name="check_'.$aRow[$hidden].'" value="'.$aRow[$hidden].'" class="check" />
                                  <label for="callapp_checkbox_'.$aRow[$hidden].'"></label>
                              </div>';
                }

            }
            $data['aaData'][] = $row;
        }

        break;
	case 'save_activities':
	
		$user_id		= $_SESSION['USERID'];
        $activities_id 	= $_REQUEST['id'];
        $color          = $_REQUEST['color'];
        $name           = $_REQUEST['name'];
        $pay            = $_REQUEST['pay'];
		$comment        = $_REQUEST['comment'];
		$dnd            = $_REQUEST['dnd'];

		$data			= array("saveStatus" => "");
		
		$db->setQuery("SELECT id FROM work_activities WHERE name = '$name' AND actived = 1 AND id != '$activities_id'");
		$check_data_count = $db->getResultArray();
		$check_data_count = $check_data_count['count'];

		if($check_data_count > 0) {
			$data["saveStatus"] = "exists";
		} else {

			if ($activities_id == '') {

				$db->setQuery("INSERT INTO  `work_activities`
													(`user_id`, `color`, `name`, `pay`, `comment`, `dnd`)
											VALUES  ('$user_id', '$color', '$name', '$pay', '$comment', '$dnd')");
				$db->execQuery();
				$data["saveStatus"] = true;

			}else {

				$db->setQuery("	UPDATE `work_activities`
								SET     `color`      = '$color',
										`name`       = '$name',
										`pay`        = '$pay',
										`comment`    = '$comment',
										`dnd`		 = '$dnd',
										`user_id`    = '$user_id'
								WHERE	`id`         = $activities_id");
				$db->execQuery();
				$data["saveStatus"] = true;

			}

		}

        break;
    case 'delete_activities_from_directory':
        $activities_id	= $_REQUEST['id'];
        delete_activities($activities_id);

        break;
    case 'get_activities_range_form':
        $page		= get_activities_range_form();
        $data		= array('page'	=> $page);

        break;
		case 'edit_activities_range_form':
				$range_id = $_REQUEST["id"];
        $page		= get_activities_range_form(get_activities_range_data($range_id));
        $data		= array('page'	=> $page);

        break;
    case 'save_activities_range':
        $activ_range_id = $_REQUEST['id'];
        $start          = $_REQUEST['start'];
        $end            = $_REQUEST['end'];
        $work_activities_id  = $_REQUEST['work_activities_id'];
        if($work_activities_id == ''){
            $work_activities_RES = mysql_fetch_array(mysql_query("   SELECT  (`id` + 1) AS `id`
                                                    FROM    `work_activities`
                                                    ORDER BY id DESC
                                                    LIMIT 1"));
            $work_activities_id = $work_activities_RES[0];
        }

        if ($activ_range_id == '') {
            AddActivitiesRange( $work_activities_id, $start, $end);
        }else {
            SaveActivitiesRange( $activ_range_id, $start, $end);
        }

        break;
    case 'delete_activities_range_from_directory':
        $activities_range_id	= $_REQUEST['id'];
        delete_activities_range($activities_range_id);

        break;
    case "get_add_hours_dialog":

        $structure = get_add_hours_dialog();
        $data = array("structure" => $structure);

        break;
		case 'get_cycle_workshift_form':
				$next_project = $_REQUEST['nextProject'];
        $page		= get_cycle_workshift_form('', $next_project);
        $data		= array('page'	=> $page);

        break;
		case 'edit_cycle_workshift_form':
				$id		    = $_REQUEST['id'];
        $page		= get_cycle_workshift_form(work_cycle_shift_details($id));
        $data		= array('page'	=> $page);

        break;
		case 'save_work_cycle':

			$cycle_id	= $_REQUEST['cycle_id'];
			$name		= $_REQUEST['name'];
			$data		= array("saveStatus" => "");



			$check_query = $db->setQuery("SELECT id FROM work_cycle WHERE name = '$name' AND actived = 1 AND id != '$cycle_id'");
			$check_data_count = $db->getResultArray();
			$check_data_count = $check_data_count['count'];

			if($check_data_count > 0) {
				$data["saveStatus"] = "exists";
			} else {

				if($cycle_id == '') {
					$user_id = $_SESSION["USERID"];
					$save_status = $db->setQuery("INSERT INTO `work_cycle` (`user_id`, `name`) VALUES ('$user_id', '$name')");
					$db->execQuery();
					$data["saveStatus"] = true;
				} else {
					$save_status = $db->setQuery("UPDATE `work_cycle` SET `name`='$name' WHERE `id`='$cycle_id'");
					$db->execQuery();
					$data["saveStatus"] = true;
				}

			}

		break;
		case 'save_work_cycle_shift':

		    	$detail_id     = $_REQUEST['detail_id'];
				$cycle_id      = $_REQUEST['cycle_id'];
				$work_shift_id = $_REQUEST['work_shift_id'];
				$num           = $_REQUEST['num'];

				$data			= array("saveStatus" => array(
					"complete" => false,
					"denialReasone" => false
				));

				// blabla
				$data["saveStatus"]["denialReasone"] = check_work_shift_before_cycle_sibling_save($cycle_id, $work_shift_id, $num, $detail_id);

				if($data["saveStatus"]["denialReasone"] == false) {

					if($detail_id==''){
						$data["saveStatus"]["complete"] = swcs_AddDetail($cycle_id, $work_shift_id, $num);
					}else{
						$data["saveStatus"]["complete"] = swcs_UpDetail($detail_id, $cycle_id, $work_shift_id, $num);
					}

				}
				

			break;
	case 'delete_work_cycle_shift_from_directory':
      $work_cycle_shift_id	= $_REQUEST['id'];
      delete_work_cycle_shift($work_cycle_shift_id);

      break;
	case 'delete_work_cycle_from_directory':
      $work_cycle_id	= $_REQUEST['id'];
      delete_work_cycle($work_cycle_id);

      break;
	case 'get_hours_ind_day_dialog':

			$project_id = $_REQUEST["projectId"];
			$index 			= $_REQUEST["index"];
			$name 			= $_REQUEST["name"];

			$query = mysql_query("SELECT start_time,
																		end_time,
																		count(id) AS count
														FROM week_day_graphic
														WHERE project_id = '$project_id' AND
																	week_day_id = '$index' AND
																	type = 1 AND
																	actived = 1
														GROUP BY start_time, end_time");

			$page		= get_hours_ind_day_dialog($index, $name, $query);
			$data		= array('page'	=> $page);

			break;
	case 'copy_wotk_hours':

			$copy_id = $_REQUEST["id"];

			$db->setQuery("INSERT INTO week_day_graphic (project_id, week_day_id, type, start_time, end_time, cycle, ext_number, actived)
										SELECT 0, week_day_id, type, start_time, end_time, cycle, ext_number, actived
										  FROM week_day_graphic
										 WHERE project_id = '$copy_id' AND actived = 1");
			$db->execQuery();
			break;
	case 'delete_hour_from_directory':

			$project_id = $_REQUEST["projectId"];
			$day_id 		= $_REQUEST["dayId"];
			$start_time = $_REQUEST["startTime"];
			$end_time 	= $_REQUEST["endTime"];

			mysql_query("UPDATE week_day_graphic SET
													actived = 0
										 	WHERE start_time = '$start_time' AND end_time = '$end_time' AND project_id = '$project_id' AND week_day_id = '$day_id'");

			break;
	case 'edit_hour_from_directory':

			$project_id			= $_REQUEST["projectId"];
			$day_id 				= $_REQUEST["dayId"];
			$start_time 		= $_REQUEST["startTime"];
			$end_time 			= $_REQUEST["endTime"];
			$count 					= $_REQUEST["count"];
			$changed_start 	= $_REQUEST["changedStart"];
			$changed_end 		= $_REQUEST["changedEnd"];
			$changed_count 	= $_REQUEST["changedCount"];
			$status 				= $_REQUEST["status"];

			if($status == "both" || $status == "time") {

				mysql_query("UPDATE week_day_graphic SET
														start_time = '$changed_start',
														end_time = '$changed_end'
											 	WHERE start_time = '$start_time' AND end_time = '$end_time' AND project_id = '$project_id' AND week_day_id = '$day_id'");

			}

			if($status == "both" || $status == "count") {

				if($count > $changed_count) {

					$count_difference = $count - $changed_count;

					mysql_query("UPDATE week_day_graphic SET
															actived = 0
													WHERE start_time = '$changed_start' AND end_time = '$changed_end' AND project_id = '$project_id' AND week_day_id = '$day_id' AND actived = 1
													ORDER BY id DESC LIMIT $count_difference");

				} else if($count < $changed_count) {

					$count_difference = $changed_count - $count;

					$old_diactivated_query = mysql_query("SELECT id FROM week_day_graphic
																								WHERE start_time = '$changed_start' AND end_time = '$changed_end' AND project_id = '$project_id' AND week_day_id = '$day_id' AND actived = 0");
					$old_diactivated_count = mysql_num_rows($old_diactivated_query);

					mysql_query("UPDATE week_day_graphic SET
															actived = 1
													WHERE start_time = '$changed_start' AND end_time = '$changed_end' AND project_id = '$project_id' AND week_day_id = '$day_id' AND actived = 0
													ORDER BY id DESC LIMIT $count_difference");

					if($old_diactivated_count < $count_difference) {

						$need_more = $count_difference - $old_diactivated_query;

						for($i = 0; $i < $count_difference; $i++) {

							mysql_query("INSERT INTO week_day_graphic
													(`project_id`, `week_day_id`, `type`, `start_time`, `end_time`, `cycle`, `actived`) VALUES
													('$project_id', '$day_id', '1', '$changed_start', '$changed_end', '0', '1')");

						}

					}

				}

			}


			break;
	case 'get_planner_cycle_data':

			$project_id 			= $_REQUEST["projectId"];
			$cycle_id 	= $_REQUEST["cycleId"];
			$saved_for_next_day = false;
			$saved_for_next_timeout = false;
			$time_from_prev_day = "00:00";
			$cycle_hours			= array();

			$data = array("over" => array(
										"status" => false,
										"projectHours" => 0,
										"cycleHours" => 0,
										"data" => array()
									)
							);

			// get cycle data
			$q = mysql_query("SELECT work_cycle_detail.id,
																work_cycle_detail.work_shift_id AS shift_id,
																work_shift.name AS shift_name,
																work_shift.color AS shift_color,
																DATE_FORMAT(work_shift.start_date,'%H:%i') AS shift_start,
																DATE_FORMAT(work_shift.end_date,'%H:%i') AS shift_end,
																DATE_FORMAT(work_shift.timeout,'%H:%i') AS shift_timeout
												FROM work_cycle_detail
												LEFT JOIN work_shift ON work_shift.id = work_cycle_detail.work_shift_id
												WHERE work_cycle_id = '$cycle_id' AND work_cycle_detail.actived = 1");

			$data["data"] = process_shift_list($q, $project_id, "cycle");

			// get max work hours per month
			$q_project = mysql_query("SELECT max_hour_per_month FROM project
																WHERE project.id = '$project_id' AND project.actived = 1");

			$res_query = mysql_fetch_assoc($q_project);

			$project_hours_per_month = (int)$res_query["max_hour_per_month"];
			$cycle_hours_calculated = calculate_cycle_time($cycle_hours, $days_in_month);

			$data["over"]["status"] = $project_hours_per_month < $cycle_hours_calculated;
			$data["over"]["projectHours"] = $project_hours_per_month;
			$data["over"]["cycleHours"] = $cycle_hours_calculated;

			break;

	case 'get_wg_shift_dialog':

			$project_id = $_REQUEST["projectId"];
			$year		= $_REQUEST["year"];
			$month		= $_REQUEST["month"];
			$row_id 	= $_REQUEST["rowId"];
			$col_id		= $_REQUEST["colId"];
			$type 		= $_REQUEST["type"];

			switch($type) {
				case 'graphic':
					$dialog = get_wg_shift_dialog($project_id, $year, $month, $row_id, $col_id);
					break;
				case 'planner':
					$dialog = get_planer_shift_dialog($project_id, $row_id, $col_id);
					break;
			}

			$data = array("dialog" => $dialog);

			break;
	case 'remove_shift_from_graphic_col':

			$delete_element_id = $_REQUEST["delElementId"];

			$db->setQuery("UPDATE work_graphic_cols
									SET actived = 0
									WHERE id = '$delete_element_id'");
            $db->execQuery();
			save_work_graphic_cols_log($delete_element_id, 3);
								
			$data = array("deleteStatus" => true);

			break;
	case 'remove_shift_from_planner_col':
		
			$delete_element_id = $_REQUEST["delElementId"];

			$db->setQuery("UPDATE work_planner_cols
									SET actived = 0
									WHERE id = '$delete_element_id'");
			$db->execQuery();			
			$data = array("deleteStatus" => true);

			break;
	case 'get_planer_add_shift_dialog':

		$dialog = get_planer_add_shift_dialog();
		$data = array("dialog" => $dialog);

		break;

	case 'save_new_shift_from_planner':

		$user_id 		= $_SESSION["USERID"];

		$name 			= $_REQUEST["name"];
		$start 			= $_REQUEST["start"];
		$end 			= $_REQUEST["end"];
		$break 			= $_REQUEST["break"];
		$color 			= $_REQUEST["color"];
		$wfmDep 		= $_REQUEST["wfmDep"];
		$payment 		= $_REQUEST["payment"];
		$workType 		= $_REQUEST["workType"];
		$comment 		= $_REQUEST["comment"];

		$db->setQuery("SELECT id FROM work_shift WHERE name = '$name' AND actived = 1");
		$query = $db->getResultArray();
		$check_data_count = $query['count'];

		if($check_data_count == 0) {

			$db->setQuery("INSERT INTO work_shift (`user_id`, `name`, `color`, `type`, `pay`, `start_date`, `end_date`, `timeout`, `comment`, `actived`)
																	VALUES ('$user_id', '$name', '$color', '$workType', '$payment', '$start', '$end', '$break', '$comment', '1')");
			$db->execQuery();
			$last_inserted_id = $db->getLastId();
			$shift_list = get_shift_list();

		} else {

			$query = "exists";
			$last_inserted_id = 0;
			$shift_list = "";
		}
		
		$data = array("save" => $query, "lastInsertedId" => $last_inserted_id, "shiftlist" => $shift_list);

		break;
	case 'save_cycle_from_planner':

		$user_id 		= $_SESSION["USERID"];

		$project_id = $_REQUEST["projectId"];
		$cycle_name = $_REQUEST["cycleName"];
		$shift_list = $_REQUEST["shiftList"];
		$data = array();

		$check_query = mysql_query("SELECT id FROM work_cycle WHERE name = '$cycle_name' AND actived = 1");
		$check_data_count = mysql_num_rows($check_query);

		if($check_data_count == 0) {

			$query = mysql_query("INSERT INTO work_cycle (user_id, name, actived)
										values ('$user_id', '$cycle_name', 1)");

			$new_cycle_id = get_last_inserted_id("work_cycle");

			foreach($shift_list as $value) {

				$num = $value["num"];
				$shift_id = $value["shiftId"];

				$shift_query = mysql_query("INSERT INTO work_cycle_detail (user_id, work_cycle_id, work_shift_id, num, actived)
											values ('$user_id', '$new_cycle_id', $shift_id, $num, 1)");

			}

			if($query && $shift_query) {
				$data["save"] = true;
				$data["cycleList"] = get_work_cycle_json_data();
			} else {
				$data["save"] = false;
			}

		} else {
			$data["save"] = "exists";
		}

		break;
	case 'update_planner_cycle_choose':

		$option = pp_get_last_cycle();
		$data = array("option" => $option);

		break;
	case 'fetch_work_graphic_build_data':

			$project_id = $_REQUEST["projectId"];
			$year 		= $_REQUEST["year"];
			$month 		= $_REQUEST["month"];

			$data = array(
				"operatorList" => array(),
				"cycleList" => array(),
				"operatedHours" => array(),
				"rowControler" => array(),
				"holyDays" => array()
			);

			// check for project id
			if($project_id != 0 && $project_id != null && $project_id != "") {

				// get operator list
				$data["operatorList"] = get_work_operator_json_data();

				// get cycle list
				$data["cycleList"] = get_work_cycle_json_data();

				// get operated hours data
				$data["operatedHours"] = get_operated_hours_data($project_id);

				// get row controler data
				$data["rowControler"] = get_work_graphic_row_controller_data($project_id, $year, $month);

				// get holidays data
				$data["holyDays"] = get_work_graphic_holidays_data($project_id, $year, $month);

			}

			break;
		case 'fetch_work_planner_build_data':

				$project_id = $_REQUEST["projectId"];

				$data = array(
					"cycleList" => array(),
					"operatedHours" => array(),
					"rowControler" => array()
				);

				// check for project id
				if($project_id != 0 && $project_id != null && $project_id != "") {

					// get cycle list
					$data["cycleList"] = get_work_cycle_json_data();

					// get operated hours data
					$data["operatedHours"] = get_operated_hours_data($project_id);

					// get row controler data
					$data["rowControler"] = get_work_planner_row_controller_data($project_id);

				}

				break;
	case 'get_workShift_cycle':

			$cycle = get_workShift_cycle_json_data();
			$data = array("cycle" => $cycle);

			break;
	case 'get_workShift_cycle_start_pos_structure':

			$days_in_month = $_REQUEST["daysInMonth"];
			$shift_count   = $_REQUEST["shiftCount"];
			$user          = $_REQUEST["user"];

			$dialog = get_workShift_cycle_start_pos_structure($days_in_month, $shift_count, $user);
			$data = array("dialog" => $dialog);

			break;
	case 'save_holiday_for_project':

			$user_id = $_SESSION["USERID"];

			$act					=	$_REQUEST["act"];
			$project_id		=	$_REQUEST["projectId"];
			$holiday_list	= $_REQUEST["holidayList"];

			// delete all preveious records
			mysql_query("UPDATE project_holiday SET actived = 0 WHERE project_id='$project_id'");

			// insert new records
			if(count($holiday_list) > 0) {

				foreach($holiday_list as $holiday_id) {
					$saved_data = mysql_query("INSERT INTO project_holiday (user_id, project_id, holidays_id, actived)
											 VALUES ('$user_id', '$project_id', '$holiday_id', 1)");
				};

				if(!$saved_data) {
					$error = "დაფიქსირდა შეცდომა!";
				}

			}

			$data = array("hasHolidays" => has_holiday_status($project_id));

			break;
		case 'get_project_day_graphic_build_data':

			$project_id	= $_REQUEST["projectId"];
			$year		= $_REQUEST["year"];
			$month		= $_REQUEST["month"];
			$col_id		= $_REQUEST["colId"];

			$table_structure	= get_project_details_day_graphic_structure();
			$col_data 			= get_project_details_day_graphic_col_data($project_id, $year, $month, $col_id);
			$data 				= array("tableStructure" => $table_structure, "rowControler" => $col_data);

			break;
		case 'get_activitie_dialog':

			$dialog = get_activitie_dialog();
			$data = array("dialog" => $dialog);

			break;
		case 'save_day_graphic_activities':
			
			$user_id 		= $_SESSION["USERID"];
			$activitie_id 	= $_REQUEST["activitieId"];
			$col_id			= $_REQUEST["colId"];
			$save_rows		= $_REQUEST["rows"];

			// loop throw rows array
			foreach($save_rows as $row) {

				// define variables from array
				$row_id 	= $row["row"];
				$start		= $row["start"];
				$end		= $row["end"];
				
				// query existing data
				$db->setQuery("SELECT id,
									  activitie_id,
									  start,
									  end
							   FROM   work_graphic_activities
							   WHERE  row_id = '$row_id'
							   AND    col_id = '$col_id'
							   AND    actived = 1");

				// define existing data count
				$query_count = $db->getNumRow();
				$res = $db->getResultArray();
				// check existing data count
				if($query_count > 0) {

					// loop throw query data
				    foreach($res[result] AS $item) {

						// define item data
						$item_id 	= $item["id"];
						$item_activitie_id = $item["activitie_id"];
						$item_start = $item["start"];
						$item_end 	= $item["end"];

						// CHECK DIFFERENT CASES

						// if new data start and end time is exact or more then existed
						if($start <= $item_start && $end >= $item_end) {

							$db->setQuery("UPDATE work_graphic_activities SET actived = 0 WHERE id = '$item_id'");
                            $db->execQuery();
						// if new data start and end time is less then existed
						} else if($start > $item_start && $end < $item_end) {

							$update_start = $start - 1;
							$update_end = $end + 1;

							$db->setQuery("INSERT INTO work_graphic_activities (`user_id`, `row_id`, `col_id`, `activitie_id`, `start`, `end`)
										VALUES ('$user_id', '$row_id', '$col_id', '$item_activitie_id', '$update_end', '$item_end')");
							$db->execQuery();

							$db->setQuery("UPDATE work_graphic_activities SET `end` = '$update_start' WHERE id = '$item_id'");
							$db->execQuery();
						// if new data start time is inside existed time
						} else if($end >= $item_end && ($start <= $item_end && $start > $item_start)) {

							$update_start = $start - 1;
							$db->setQuery("UPDATE work_graphic_activities SET `end` = '$update_start' WHERE id = '$item_id'");
							$db->execQuery();

						// if new data end time is inside existed time
						} else if($start <= $item_start && ($end >= $item_start && $end < $item_end)) {

							$update_end = $end + 1;
							$db->setQuery("UPDATE work_graphic_activities SET `start` = '$update_end' WHERE id = '$item_id'");
                            $db->execQuery();
						}

					}

				} 
				
				// save new data
				$db->setQuery("INSERT INTO work_graphic_activities (`user_id`, `row_id`, `col_id`, `activitie_id`, `start`, `end`)
									VALUES ('$user_id', '$row_id', '$col_id', '$activitie_id', '$start', '$end')");
				$db->execQuery();

			}

			$data = array("saveStatus" => 1);

			break;
		case 'update_shift_activitie_data':

			$user_id = $_SESSION["USERID"];
			$rows = $_REQUEST["rows"];

			foreach($rows as $value) {

				$project_id = $value["projectId"];
				$row_id = $value["rowId"];
				$date =  $value["date"];
				$shift_id = $value["shiftId"];
				$activitie_details = $value["activities"];

				// delete old records
				$db->setQuery("UPDATE work_graphic_row_activities
										SET actived = 0
										WHERE `project_id` = '$project_id'
											AND `row_num` = '$row_id'
											AND `date` = '$date'
											AND `shift_id` = '$shift_id'");
                $db->execQuery();
				foreach($activitie_details as $act_value) {

					$activitie_id = $act_value["id"];
					$start = $act_value["start"];
					$end = $act_value["end"];

					if($start != -1 && $end != -1) {
						$db->setQuery("INSERT INTO work_graphic_row_activities (`user_id`, `project_id`, `activitie_id`, `row_num`, `date`, `shift_id`, `start`, `end`)
												 VALUES	('$user_id', '$project_id', '$activitie_id', '$row_id', '$date', '$shift_id', '$start', '$end')");
						$db->execQuery();
					}

				}

			}

			$data = array("dialog" => $dialog);

			break;
		case 'change_dg_layout':

			$type = $_REQUEST["type"];
			$data = array("type" => $type);

			switch($type) {
				case 'graphic':
					$data["structure"] = table_in_24_hour_head_structure();
					break;
				case 'table':

					$tableData = wg_dt_value_table_head_structure();
					$data["structure"] = $tableData["structure"];
					$data["activitiesCount"] = $tableData["count"];
					break;
			}

			break;
		case 'update_week_nums_by_year':

			$year = $_REQUEST["year"];
			$data = array("weekList" => full_year_week_list($year));

			break;
		case 'get_rush_hours_select_structure':

			$data = array("structure" => rush_hours_select_structure());

			break;

		case 'save_rush_hours':

			$user_id 		= $_SESSION["USERID"];
			$project_id = $_REQUEST["projectId"];
			$year 			= $_REQUEST["year"];
			$week_num		= $_REQUEST["weekNum"];
			$values 		= $_REQUEST["values"];
			$data 			= array();

			$db->setQuery("UPDATE wfm_rush_hours
									 SET actived = 0
									 WHERE year = '$year' AND week_num = '$week_num'");
			$db->execQuery();
			$db->setQuery("INSERT INTO wfm_rush_hours (`user_id`, `project_id`, `year`, `week_num`)
		 							VALUES ('$user_id', '$project_id', '$year', '$week_num')");
			$db->execQuery();
		 	$last_id = $db->getLastId();

		 foreach($values as $value) {

			$row 	= $value["row"];
			$start = $value["start"];
			$end 	= $value["end"];

			$db->setQuery("INSERT INTO wfm_rush_hours_rows (`wfm_rush_hours_id`, `row`, `start`, `end`)
									 VALUES ('$last_id', '$row', '$start', '$end')");
			$db->execQuery();
		 };

			break;
		case 'fetch_rush_hours_build_data':

			$project_id = $_REQUEST["projectId"];
			$year 			= $_REQUEST["year"];
			$week 			= $_REQUEST["week"];
			$data       = array("colData" => array());

			$db->setQuery("SELECT `row`,
				 											 `start`,
															 `end`
											  FROM wfm_rush_hours
												JOIN wfm_rush_hours_rows ON wfm_rush_hours.id = wfm_rush_hours_rows.wfm_rush_hours_id
												WHERE actived = 1 AND wfm_rush_hours.project_id = '$project_id'
																					AND wfm_rush_hours.year = '$year'
																					AND wfm_rush_hours.week_num = '$week'");
			$query = $db->getResultArray();
			foreach($query['result'] AS $res) {

				$data_collector = array(
					"row" => (int) $res["row"],
					"start" => (int) $res["start"],
					"end" => (int) $res["end"]
				);

				array_push($data["colData"], $data_collector);

			}

			break;
		case 'copy_prev_week_data':

			$user_id				= $_SESSION["USERID"];
			$project_id			= $_REQUEST["projectId"];
			$year 					= $_REQUEST["year"];
			$cur_week_num 	= $_REQUEST["curWeekNum"];
			$prev_week_num	= $_REQUEST["prevWeekNum"];
			$update_id 			= 0;
			$data 					= array(
				"message" => ""
			);


			$prev_q = mysql_query("SELECT id FROM wfm_rush_hours
									 					 WHERE actived = 1 AND year = '$year'
									 									 					 AND week_num = '$prev_week_num'");

			$prev_data_count = mysql_num_rows($prev_q);

			if($prev_data_count == 0) {
				$data["responceMessage"] = "not-exists";
			} else {

				$cur_q = mysql_query("SELECT id FROM wfm_rush_hours
										 			WHERE actived = 1 AND year = '$year'
										 									 			AND week_num = '$cur_week_num'");


				$cur_data_count = mysql_num_rows($cur_q);

				if($cur_data_count > 0) {

					mysql_query("UPDATE wfm_rush_hours
											 SET actived = 0
											 WHERE year = '$year' AND week_num = '$cur_week_num'");
				}


				mysql_query("INSERT INTO wfm_rush_hours (`user_id`, `project_id`, `year`, `week_num`)
										VALUES ('$user_id', '$project_id', '$year', '$cur_week_num')");

				$fetched_data = mysql_fetch_assoc($prev_q);
				$prev_data_id = $fetched_data["id"];
				$last_id = mysql_insert_id();

				$rows_q = mysql_query("INSERT INTO wfm_rush_hours_rows (`wfm_rush_hours_id`, `row`, `start`, `end`)
															 SELECT $last_id, `row`, `start`, `end`
															 FROM wfm_rush_hours_rows
										 					 WHERE wfm_rush_hours_id = '$prev_data_id'");


			}

			break;
		case 'save_graphic_shift':

			$user_id	= $_SESSION["USERID"];

			$project_id = $_REQUEST["projectId"];
			$year 		= $_REQUEST["year"];
			$month 		= $_REQUEST["month"];

			$row_id 	= $_REQUEST["rowId"];
			$col_id 	= $_REQUEST["colId"];
			$shift_id 	= $_REQUEST["shiftId"];
			$month      = $_REQUEST['month'];

			$comment    = $_REQUEST["comment"];

			$work_graphic_data = get_work_graphic_data_by_details($project_id, $year, $month);
			$work_graphic_id = $work_graphic_data["id"];

			$work_graphic_rows_data = get_work_graphic_rows_data_by_details($work_graphic_id, $row_id);
			$work_graphic_rows_id = $work_graphic_rows_data["id"];

			$save_data = save_work_graphic_cols_data($work_graphic_rows_id, $col_id, $shift_id, $comment, $month);

			$data = array("result" => $save_data, "shiftList" => get_wg_added_shift_list_structure($project_id, $year, $month, $row_id, $col_id));

			break;
		case 'save_graphic_cycle':

			$days_in_month 				= $_REQUEST["daysInMonth"];
			$cycle_id 					= $_REQUEST["cycleId"];
			$cycle_start_date 			= $_REQUEST["cycleStartDate"];
			$cycle_start_shift 			= $_REQUEST["cycleStartShift"];
			$continue_from_prev_month 	= $_REQUEST["continueFromPrevMonth"];
			$month						= $_REQUEST["month"];
			$data_id 					= $_REQUEST["dataId"];
			$shift_count 				= $_REQUEST["shiftCount"];
			$operator_id                = $_REQUEST['user'];
			$shift_array				= get_shift_processed_list($cycle_id);
			$values_collector 			= "";

			
			/* Save Cycle */
			$db->setQuery("UPDATE work_graphic_rows
						      SET cycle_id = '$cycle_id'
						   WHERE  id       = '$data_id'");		

			$db->execQuery();
			save_work_graphic_rows_log($data_id, 2);
			
			/* Save Ccolumns */

			// update work_graphic_cols actived status to 0 to nullify all previews records
			clear_work_graphic_rows_data($data_id);

			// check if need to continue from previews month
			if($continue_from_prev_month == "false") {
				$month ++;
				// loop in days of month
				for($col_id = $cycle_start_date; $col_id < $days_in_month; $col_id++) {

					// determine sift index
					$shift_index = (($col_id - $cycle_start_date) % $shift_count) + $cycle_start_shift;

					// regulate shift index
					if($shift_index >= $shift_count) {
						$shift_index = $shift_index - $shift_count;
					}
					
					// loop in shift arry items
					foreach($shift_array[$shift_index] as $shift_id) {
						
						// save data
						$db->setQuery("INSERT INTO work_graphic_cols (`work_graphic_rows_id`,`operator_id`, `col_id`, `work_shift_id`,`shift_index`, `cycle_id`, `month`, `actived`)
											VALUES ('$data_id', '$operator_id', '$col_id', '$shift_id', '$shift_index', '$cycle_id', '$month', '1')");

						$db->execQuery();
						$last_saved_col_id = $db->getLastId();
						save_work_graphic_cols_log($last_saved_col_id, 1);

					}
				}

				
			} else {

				$cur_year = date("Y");
				
				$day_count = cal_days_in_month(CAL_GREGORIAN, $month, $cur_year);

				$db->setQuery("SELECT  `shift_index` 
                               FROM    `work_graphic_cols`
                               JOIN     work_graphic_rows ON work_graphic_rows.id = work_graphic_cols.work_graphic_rows_id AND work_graphic_rows.actived = 1 AND work_graphic_rows.operator_id = '$operator_id'
							   WHERE    work_graphic_cols.`month` = '$month' AND work_graphic_cols.cycle_id = '$cycle_id'
							   ORDER BY work_graphic_cols.id DESC
							   LIMIT 1");
				
				$req = $db->getResultArray();
				$counter = $req[result][0];
				
				if($counter['shift_index'] != '') {
				   $cycle_start_shift = $counter['shift_index']+1;
				
						// echo ">>".$cycle_start_shift;
						$month ++ ;
						// loop in days of month
						for($col_id = 0; $col_id < $days_in_month; $col_id++) {

							// determine sift index
							$shift_index = (($col_id-$_REQUEST['cycleStartDate']+$_REQUEST['cycleStartShift']) % $shift_count) + $cycle_start_shift;
							// $shift_index = 2;
							// var_dump($shift_index);
							// regulate shift index
							if($shift_index >= $shift_count) {
								$shift_index = $shift_index - $shift_count;
							}
							
							// loop in shift arry items
							foreach($shift_array[$shift_index] as $shift_id) {
								
								// save data
								$db->setQuery("INSERT INTO work_graphic_cols (`work_graphic_rows_id`, `operator_id`, `col_id`, `work_shift_id`, `shift_index`, `cycle_id`, `month`, `actived`)
													VALUES ('$data_id','$operator_id', '$col_id', '$shift_id', '$shift_index','$cycle_id', '$month','1')");
                                $db->execQuery();
                                
								$last_saved_col_id = $db->getLastId(); 
								save_work_graphic_cols_log($last_saved_col_id, 1);
							}
						}
					}
				else  {
					$error = "წინა თვის მონაცემები არ მოიძებნა ! ";
				}

			}

			$data = array("saveStatus" => 1);

			break;
		case 'save_graphic_operator':

			$data_id = $_REQUEST["dataId"];
			$operator_id = $_REQUEST["operatorId"];

			$db->setQuery("UPDATE work_graphic_rows
								SET operator_id = '$operator_id'
								WHERE id = '$data_id'");

			$db->execQuery();
			save_work_graphic_rows_log($data_id, 2);

			$data = array("saveStatus" => true);

			break;
		case 'clear_graphic_row':

			$data_id = $_REQUEST["dataId"];

			$db->setQuery("UPDATE work_graphic_rows
									SET operator_id = 0,
										cycle_id = 0,
										actived = 0
									WHERE id = '$data_id'");
			$db->execQuery();
			$db->setQuery("INSERT INTO work_graphic_rows (work_graphic_id, row_id, operator_id, cycle_id, actived)
									SELECT work_graphic_id, row_id, operator_id, cycle_id, 1
									FROM work_graphic_rows
									WHERE id = '$data_id'");
			$db->execQuery();
			save_work_graphic_rows_log($data_id, 3);

			$data = array("clearStatus" => true);
			
			break;
		case 'add_graphic_row':

			$project_id 	= $_REQUEST["projectId"];
			$year 			= $_REQUEST["year"];
			$month 			= $_REQUEST["month"];
			$row_id 		= $_REQUEST["rowId"];

			$work_graphic_data 	= get_work_graphic_data_by_details($project_id, $year, $month);
			$work_graphic_id	= $work_graphic_data["id"];

			$db->setQuery("INSERT INTO work_graphic_rows (`work_graphic_id`, `row_id`, `operator_id`, `cycle_id`)
								VALUES ('$work_graphic_id', '$row_id', 0, 0)");
			$db->execQuery();
			$data = array("addStatus" => true);

			break;
		case 'delete_graphic_row':

			$data_id = $_REQUEST["dataId"];

			$db->setQuery("UPDATE work_graphic_rows
								SET actived = 0
								WHERE id = '$data_id'");
			$db->execQuery();
			save_work_graphic_rows_log($data_id, 3);

			$data = array("deleteStatus" => true);

			break;

		case 'make_work_graphic_edit_history':

			$user_id = $_SESSION["USERID"];
			$project_id = $_REQUEST["projectId"];
			$row_num = $_REQUEST["rowNum"];
			$string_date = $_REQUEST["stringDate"];
			$shift_id = $_REQUEST["shiftId"];
			$new_shift_id = $_REQUEST["newShift"];

			mysql_query("INSERT INTO work_graphic_edit_history (`user_id`, `project_id`, `row_num`, `string_date`, `work_shift_id`, `after_shift_id`, `date`)
													VALUES ('$user_id', '$project_id', '$row_num', '$string_date', '$shift_id', '$new_shift_id', NOW())");

			break;

		case 'get_work_graphic_edit_history_dialog':

			$structure = get_work_graphic_edit_history_structure();
			$data = array("structure" => $structure);

			break;

		case 'get_work_graphic_edit_history':

			$count	= $_REQUEST['count'];
			$hidden	= $_REQUEST['hidden'];

			$project_id = $_REQUEST["project_id"];
			$row_id = $_REQUEST["rowId"];
			$col_id = $_REQUEST["colId"];

			$data = array(
					"aaData"	=> array()
			);

			$db->setQuery("SELECT 	`work_graphic_cols`.`id`,
												`work_graphic_cols_log`.`action_time` AS action_time,
												`users`.`username` AS action_author,
												`work_graphic_log_actions`.`name` AS action_name,
												CONCAT('<div class=\"column-edit-history-shift\" style=\"background:',`work_shift`.`color` ,';\">',
																`work_shift`.`name`, '</div>') AS shift,
												IF(`work_graphic_cols`.`cycle_id` != 0, `work_cycle`.`name`, '') AS cycle_name,
												`work_graphic_cols`.`comment`,
                                                ''
									FROM `work_graphic_cols`
									JOIN `work_graphic_cols_log` ON `work_graphic_cols_log`.`work_graphic_cols_id` = `work_graphic_cols`.`id`
									JOIN `work_graphic_log_actions` ON `work_graphic_cols_log`.`log_action_id` = `work_graphic_log_actions`.`id`
									JOIN `users` ON `users`.`id` = `work_graphic_cols_log`.`user_id`
									JOIN `work_shift` ON `work_shift`.`id` = `work_graphic_cols`.`work_shift_id`
									LEFT JOIN `work_cycle` ON `work_cycle`.`id` = `work_graphic_cols`.`cycle_id`
									WHERE `work_graphic_cols`.`work_graphic_rows_id` = $row_id
										AND `work_graphic_cols`.`col_id` = $col_id
									ORDER BY `work_graphic_cols_log`.`action_time`, `work_graphic_cols_log`.`log_action_id` DESC");


			$data = $db->getList(7,$hidden);

			break;

		case 'add_planner_row':

			$project_id 	= $_REQUEST["projectId"];
			$row_id 		= $_REQUEST["rowId"];

			$work_planner_data 	= get_work_planner_data_by_project($project_id);
			$work_planner_id	= $work_planner_data["id"];

			$save_data = save_work_planner_rows_data($work_planner_id, $row_id, 0, 0);

			$add = $save_data["complete"];

			$data = array("addStatus" => $add);

			break;

		case 'clear_planner_row':

			$data_id = $_REQUEST["dataId"];

			$db->setQuery("UPDATE work_planner_rows
									SET cycle_id = 0,
										save_as_cycle_id = 0,
										actived = 0
									WHERE id = '$data_id'");
            $db->execQuery();
			$db->setQuery("INSERT INTO work_planner_rows (work_planner_id, row_id, cycle_id, save_as_cycle_id, actived)
									SELECT work_planner_id, row_id, cycle_id, save_as_cycle_id, 1
									FROM work_planner_rows
									WHERE id = '$data_id'");
            $db->execQuery();
			$data = array("clearStatus" => true);
			
			break;

		case 'delete_planner_row':

			$data_id = $_REQUEST["dataId"];

			$db->setQuery("UPDATE work_planner_rows
								SET actived = 0
								WHERE id = '$data_id'");
            $db->execQuery();
			$data = array("deleteStatus" => true);

			break;

		case 'save_planner_shift':

				$user_id	= $_SESSION["USERID"];
	
				$project_id = $_REQUEST["projectId"];
				$row_id 	= $_REQUEST["rowId"];
				$col_id 	= $_REQUEST["colId"];
				$shift_id 	= $_REQUEST["shiftId"];
	
				$work_planner_data = get_work_planner_data_by_project($project_id);
				$work_planner_id = $work_planner_data["id"];
	
				$work_planner_rows_data = get_work_planner_rows_data_by_details($work_planner_id, $row_id);
				$work_planner_rows_id = $work_planner_rows_data["id"];
	
				$save_data = save_work_planner_cols_data($work_planner_rows_id, $col_id, $shift_id);
	
				$data = array("result" => $save_data, "shiftList" => get_wp_added_shift_list_structure($project_id, $row_id, $col_id));
	
				break;

		case 'save_planner_cycle':

			$cycle_id 					= $_REQUEST["cycleId"];
			$cycle_start_pos 			= $_REQUEST["cycleStartPos"];
			$data_id 					= $_REQUEST["dataId"];
			$shift_count 				= $_REQUEST["shiftCount"];

			$shift_array				= get_shift_processed_list($cycle_id);
			$values_collector 			= "";

			/* Save Cycle */
			$db->setQuery("UPDATE work_planner_rows
						      SET cycle_id = '$cycle_id'
						   WHERE  id       = '$data_id'");
			$db->execQuery();
			
			/* Save Ccolumns */

			// update work_planner_cols actived status to 0 to nullify all previews records
			$db->setQuery("UPDATE work_planner_cols SET actived = 0 WHERE work_planner_rows_id = '$data_id'");
            $db->execQuery();
			// loop in days of month
			for($col_id = $cycle_start_pos; $col_id < 28; $col_id++) {

				// determine sift index
				$shift_index = ($col_id - $cycle_start_pos) % $shift_count;

				// regulate shift index
				if($shift_index >= $shift_count) {
					$shift_index = $shift_index - $shift_count;
				}
				
				// loop in shift arry items
				foreach($shift_array[$shift_index] as $shift_id) {
					
					// save data
					$db->setQuery("INSERT INTO work_planner_cols (`work_planner_rows_id`, `col_id`, `work_shift_id`, `cycle_id`, `actived`)
										VALUES ('$data_id', '$col_id', '$shift_id', '$cycle_id', '1')");
					$db->execQuery();

				}
			}

			$data = array("saveStatus" => true);

			break;
		
		case 'get_dir_page':

			$page_type 	= $_REQUEST["pageType"];
			$data		= array();

			switch($page_type) {
				case 'projects':
					$data["dirPage"] = get_projects_directory();
					break;
				case 'holidays':
					$data["dirPage"] = get_holidays_directory();
					break;
				case 'cycles':
					$data["dirPage"] = get_cycles_directory();
					break;
				case 'shifts':
					$data["dirPage"] = get_shifts_directory();
					break;
				case 'activities':
					$data["dirPage"] = get_activities_directory();
					break;
				case 'report':
					$data["dirPage"] = wfm_report_structure();
					break;
			}

			break;
		case 'load_project_data_structure':

			$project_id 	= $_REQUEST["projectId"];
			$type		 	= $_REQUEST["type"];

			$data			= array("structure" => "");

			switch($type) {
				case "graphic":
					$data["structure"] = get_graphic_schema_dialog($project_id);
					break;
				case "workHours":
					$data["structure"] = new_project_operating_hours_structure()." ".new_project_planner_structure($project_id);
					break;
				case "rushHours":
					$data["structure"] = rush_hour_structure();
					break;
				case "holidays":
					$data["structure"] = get_project_holidays_table_structure();
					break;
			}

			break;

		case 'add_project_holiday':

			$user_id	= $_SESSION["USERID"];
			$holiday_id = $_REQUEST["holidayId"];
			$project_id = $_REQUEST["projectId"];

			$db->setQuery("INSERT INTO project_holiday (`user_id`, `project_id`, `holidays_id`, `actived`)
								VALUES ('$user_id', '$project_id', '$holiday_id', 1)");
			$db->execQuery();
			$data = array("saveStatus" => true);

			break;
		case 'remove_project_holiday':

			$holiday_id = $_REQUEST["holidayId"];
			$project_id = $_REQUEST["projectId"];

			$db->setQuery("UPDATE project_holiday 
								SET actived = 0
								WHERE project_id = '$project_id'
									AND holidays_id = '$holiday_id'");
			$db->execQuery();
			$data = array("removeStatus" => true);

			break;
		case 'get_col_edit_history_dialog':

			$structure = get_work_graphic_edit_history_structure();
			$data = array("structure" => $structure);
			break;
			
		case "get_user_table" :
			$activities = $_REQUEST["activities"];
            $date       = $_REQUEST["date"];
            $operator  = $_REQUEST["id"];
            $sort       = $_REQUEST["sort"];

            $date_start = $_REQUEST["start"];
            $date_end   = $_REQUEST["end"];

            $sort_type  = $sort["type"];
            $sort_dir   = $sort["dir"];

            $data = array(
                "rowControler" => array()
            );

            if($date_start == "") {
                $date_start = date("d-m-Y 00:00");
            }

            if($date_end == "") {
                $date_end = date("d-m-Y 23:59");
            }

            if($operators == "") {
                $operators = get_all_ids("users");
            }

            if($activities == "") {
                $activities = get_all_ids("work_activities");
			}
			

			$date_start = date("Y-m-d H:i:s", strtotime($date_start));

			$date_end = date("Y-m-d H:i:s", strtotime($date_end));

			$query = mysql_query("SELECT 
			work_activities_log.start_datetime AS `date`,
			work_shift.start_date AS `income`,
			time(users_log.datatime) AS `income_real`,
			TIMEDIFF(work_shift.start_date,time(users_log.datatime)) AS `income_diff`,
			work_activities.name AS activitie_name,
			work_activities.color AS activitie_color,
			SEC_TO_TIME(  SUM(	(work_graphic_activities.END - work_graphic_activities.START ) * 300 )) AS `break`, -- აქტივობა გეგმიური
			SEC_TO_TIME(CASE	
								WHEN ISNULL( work_activities_log.end_datetime ) THEN  'გასულია' 
								ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
						END ) AS `break_time_real`, -- აქტივობა რეალური
			SEC_TO_TIME(	SUM(((work_graphic_activities.END - work_graphic_activities.START ) * 300) - IFNULL(
			TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ),0	
			) 		
			)) AS `break_diff`,
			
			IFNULL(
			SUM( TIME_TO_SEC( TIMEDIFF( IFNULL( users_log.logout, NOW( ) ), users_log.datatime ) ) ),
			TIME_TO_SEC( TIMEDIFF ( NOW( ), users_log.datatime ) ) 
			) -
			CASE

			WHEN ISNULL( work_activities_log.end_datetime ) THEN
			0 ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
			END AS `real_work_time`,
			-- 	ფაქტიური სამუშაო 
			work_shift.end_date AS `out`,
			IFNULL(time(users_log.logout),'პროგრამაშია') AS `out_real`,
									
							
			
			TIME_TO_SEC(work_shift.start_date) - TIME_TO_SEC(time(users_log.datatime))     AS `work_time`,
			IFNULL(TIMEDIFF(work_shift.end_date,time(users_log.logout)),'პროგრამაშია') AS `out_diff`,
			TIMEDIFF(time(IFNULL(users_log.logout,NOW())),time(users_log.datatime)) AS `all_time`
									
									
			
			FROM work_graphic_cols
			JOIN users_log ON users_log.user_id = work_graphic_cols.operator_id
			JOIN work_activities_log ON work_activities_log.user_id = users_log.user_id
			JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id 
			JOIN work_activities ON work_activities.id = work_activities_log.work_activities_id
			JOIN work_graphic_activities ON work_graphic_activities.row_id = work_graphic_cols.work_graphic_rows_id
			
			WHERE
			work_graphic_cols.operator_id = $operator
			AND work_graphic_cols.`month` = month(now()) 
			AND work_graphic_cols.col_id = day(NOW()) - 1
			
			AND users_log.datatime BETWEEN '$date_start' AND '$date_end'
			AND work_activities_log.start_datetime  BETWEEN '$date_start' AND '$date_end'
			
			AND work_graphic_cols.actived = 1
			
			group by work_graphic_cols.id , users_log.id, work_activities_log.id, work_shift.id, work_activities.id,
			work_graphic_activities.id
									");

            while($res = mysql_fetch_assoc($query)) {
				$times = get_times_all($res["user_id"], $res["datetime"],$res["activitie"]);
                $data_collector = array(
					"user_id"  => $res["user_id"],
					"operator" => $res["operator"],
					"datetime" => $res["date"], 
                    "activitieName" => $res["activitie_name"],
                    "activitieColor" => $res["activitie_color"],
					"actuallyTime" => $res["break_time_real"],
					"real_work_time" => $res["real_work_time"],
					"work_time"      => $res["work_time"],
					"work_time_diff" => $res["real_work_time"],
					"break_time"     => $res["break"],
					"break_time_diff"     => $res["break_diff"],
					"income"              => $res["income"],
					"income_real"         => $res["income_real"],
					"income_diff"         => $res["income_diff"] ,
					"out"              => $res["out"],
					"out_real"         => $res["out_real"],
					"out_diff"         => $res["out_diff"] ,
					"all_time"         => $res["all_time"]
                    // "plannedTime" => get_reposrt_planned_time($res["user_id"], $res["activitie_id"], $date_start, $date_end)
                );

                array_push($data["rowControler"], $data_collector);

            }

			break;

		case 'fetch_report_build_data':

            $activities = $_REQUEST["activities"];
            $date       = $_REQUEST["date"];
            $operators  = $_REQUEST["operators"];
            $sort       = $_REQUEST["sort"];

            $date_start = $date["start"];
            $date_end   = $date["end"];

            $sort_type  = $sort["type"];
            $sort_dir   = $sort["dir"];

            $data = array(
                "rowControler" => array()
            );

            if($date_start == "") {
                $date_start = date("d-m-Y 00:00");
            }

            if($date_end == "") {
                $date_end = date("d-m-Y 23:59");
            }

            if($operators == "") {
                $operators = get_all_ids("users");
            }

            if($activities == "") {
                $activities = get_all_ids("work_activities");
            }

			$date_start = date("Y-m-d H:i:s", strtotime($date_start));

			$date_end = date("Y-m-d H:i:s", strtotime($date_end));

            $db->setQuery("SELECT * FROM ( SELECT 		users.id , 
																work_activities_log.id AS `log_id`,
																work_activities_log.user_id,
																work_activities_log.work_activities_id AS activitie_id,
																user_info.`name` AS operator,
																work_activities.`name` AS activitie_name,
																work_activities.color AS activitie_color,
																
																-- --------------------------------------------------------------------------------

														IF(TIMEDIFF(work_shift.end_date,work_shift.start_date) > '00:00:00', 

															TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)  ,

															TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC('24:00:00')

														) - TIME_TO_SEC(`work_shift`.`timeout`) AS `work_time`,  -- გეგმიური სამუშაო
																		
																				
																			

													IFNULL(
															SUM(TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.datatime ) )),
															TIME_TO_SEC( TIMEDIFF (NOW(), users_log.datatime ) ))   -   CASE	
																															WHEN ISNULL( work_activities_log.end_datetime ) THEN  0 
																															ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
																														END 
															AS `real_work_time`, -- ფაქტიური სამუშაო

													TIME_TO_SEC(work_shift.timeout) AS `break_time`, -- გეგმიური შესვენება

													CASE	
															WHEN ISNULL( work_activities_log.end_datetime ) THEN  'გასულია' 
															ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
														END AS `real_break_time` -- ფაქტიური შესვენება


													FROM users
													LEFT JOIN work_graphic_cols ON users.id = work_graphic_cols.operator_id
													LEFT JOIN user_info ON user_info.user_id = users.id
													LEFT JOIN users_log ON users_log.user_id = users.id
													LEFT JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id

													LEFT JOIN work_activities_log ON work_activities_log.user_id = users.id
													LEFT JOIN work_activities ON work_activities.id = work_activities_log.work_activities_id										
													WHERE  users_log.datatime BETWEEN '$date_start' AND '$date_end'
													AND work_activities_log.start_datetime BETWEEN '$date_start' AND '$date_end'
													AND  users.id IN($operators)
													AND work_activities_log.work_activities_id IN($activities)
													AND work_shift.actived = 1 
													AND work_graphic_cols.actived = 1
														group by work_graphic_cols.id,users.id,work_activities_log.id, users_log.id,
														work_shift.id, work_activities.id


													UNION ALL 
						
													SELECT  

														users.`id`,
														'' AS  `log_id`,
														'' AS user_id,
														'' AS  activitie_id,
														user_info.name AS  operator,
														'' AS  activitie_name,
														'' AS  activitie_color,
														'' AS `work_time`,
														IFNULL(
															SUM(TIME_TO_SEC( TIMEDIFF(IFNULL(users_log.logout,NOW()),users_log.datatime ) )),
															TIME_TO_SEC( TIMEDIFF (NOW(), users_log.datatime ) ))    AS `real_work_time`, -- ფაქტიური სამუშაო
														'' AS  `break_time`,
														CASE	
															WHEN ISNULL( work_activities_log.end_datetime ) THEN  '' 
															ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
														END AS `real_break_time` -- ფაქტიური შესვენება
														-- '' AS `real_break_time`


														FROM users
															LEFT JOIN user_info ON user_info.user_id = users.id
															LEFT JOIN users_log ON users_log.user_id = users.id
															LEFT JOIN work_activities_log ON work_activities_log.user_id = users.id
															WHERE
																users_log.datatime  BETWEEN '$date_start' AND '$date_end'
																AND work_activities_log.start_datetime BETWEEN '$date_start' AND '$date_end'
																group by users.id,work_activities_log.id, users_log.id
												) AS x

										GROUP BY id
                                    ORDER BY $sort_type $sort_dir");

			
            $query = $db->getResultArray();
            foreach($query[result] AS $res) {
				// $times = get_times($res["user_id"], $date_start, $date_end);
                $data_collector = array(
					"user_id"  => $res["id"],
                    "operator" => $res["operator"],
                    "activitieName" => $res["activitie_name"],
                    "activitieColor" => $res["activitie_color"],
					"actuallyTime" => $res["real_break_time"],
					"real_work_time" => $res["real_work_time"],
					"work_time"      => $res["work_time"],
					"work_time_diff" => $res["work_time"] - $res["real_work_time"],
					"break_time"     => $res["break_time"],
					"break_time_diff"     => $res["real_break_time"] - $res["break_time"]
                    // "plannedTime" => get_reposrt_planned_time($res["user_id"], $res["activitie_id"], $date_start, $date_end)
                );

                array_push($data["rowControler"], $data_collector);

            }

            break;
/*end of case*/
  default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/

/* DIRECTORY STRUCTURES */

// projects
function get_projects_directory() {

	return '
	<div id="button_area">
		<button id="add_button">დამატება</button>
		<button id="delete_button">წაშლა</button>
	</div>

	<table class="table table-hover display  pb-30 dataTable" id="table_project" >
	<thead>
	<tr id="datatable_header">
		<th>ID</th>
		<th style="width: 4%;" class="check">No:</th>
		<th style="width: 8%;">დეპარტამენტი/ <br> პროექტი</th>
		<th style="width: 8%;;">ოპერატორების <br> რ-ბა.</th>
		<th style="width: 9%;">სამუშაო ზღვარი <br> სთ/კვირა</th>
		<th style="width: 8%;" class="check">ცვლის მაქს. <br> ხ-ბა(სთ)</th>
		<th style="width: 11%;" class="check">მომსახურების დონე <br> Service Level</th>
		<th style="width: 8%;" class="check">ცვლის <br> რ-ბა</th>
		<th style="width: 8%;" class="check">ციკლის <br> რ-ბა</th>
		<th style="width: 8%;" class="check">დასვენების <br> დღეები</th>
		<th style="width: 8%;" class="check">შევსებულია</th>
		<th style="width: 2%;" class="check"></th>
	</tr>
	</thead>
	<thead>
	<tr class="search_header">
		<th class="colum_hidden">
			<input type="text" name="search_id" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_number" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<input type="text" name="search_date" value="ფილტრი" class="search_init" />
		</th>
		<th>
			<div class="callapp_checkbox">
				<input class="tbale-checker" data-table="table_project" type="checkbox" id="projectTableChecker" name="project_table_checker" />
				<label style="margin-top: 3px;" for="projectTableChecker"></label>
			</div>
		</th>
	</tr>
	</thead>
	</table>
	';

}

// holidays
function get_holidays_directory() {

	return '
	<div class="button-area">
		<button id="add_button_new_holiday">დამატება</button>
		<button id="delete_button_exist_holiday">წაშლა</button>
	</div>
	<table class="display" id="holiday_table">
		<thead>
			<tr id="datatable_header">
				<th>ID</th>
				<th style="width: 10%;">თარიღი</th>
				<th style="width: 60%;">დასახელება</th>
				<th style="width: 30%;">კატეგორია</th>
				<th class="check">#</th>
			</tr>
		</thead>
		<thead>
			<tr class="search_header">
				<th class="colum_hidden">
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<div class="callapp_checkbox">
						<input class="tbale-checker" data-table="holiday_table" type="checkbox" id="holidayTableChecker" />
															<label for="holidayTableChecker"></label>
					</div>
				</th>
			</tr>
		</thead>
	</table>
	';

}

// cycles
function get_cycles_directory() {

	return '
	<div class="button-area">
		<button id="add_button_cycle">დამატება</button>
		<button id="delete_button_cycle">წაშლა</button>
	</div>
	<table class="display" id="work_cycle_table">
	<thead>
		<tr id="datatable_header">
			<th>ID</th>
			<th style="width: 5%;">#</th>
			<th style="width: 30%;">ციკლის დასახელება</th>
			<th style="width: 30%;">ცვლები</th>
			<th style="width: 17%;">სამუშაო დრო</th>
			<th style="width: 17%;">შესვენების დრო</th>
			<th class="check">#</th>
		</tr>
	</thead>
	<thead>
		<tr class="search_header">
			<th class="colum_hidden">
				<input type="text" name="search_category" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input type="text" name="search_category" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input type="text" name="search_category" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input type="text" name="search_category" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input type="text" name="search_category" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<input type="text" name="search_category" value="ფილტრი" class="search_init" />
			</th>
			<th>
				<div class="callapp_checkbox">
					<input class="tbale-checker" data-table="work_cycle_table" type="checkbox" id="cycleTableChecker" />
											<label for="cycleTableChecker"></label>
				</div>
			</th>
		</tr>
	</thead>
	</table>
	';
}

// shifts
function get_shifts_directory() {

	return '
	<div class="button-area">
		<button id="add_button_work_shift">დამატება</button>
		<button id="delete_button_work_shift">წაშლა</button>
	</div>
	<table class="display" id="work_Shift_table">
		<thead>
			<tr id="datatable_header">
				<th>ID</th>
				<th style="width: 13%;">დასახელება</th>
				<th style="width: 12%;">დასაწყისი</th>
				<th style="width: 12%;">დასასრული</th>
				<th style="width: 12%;">შესვენება</th>
				<th style="width: 15%;">სამუშაო ტიპი</th>
				<th style="width: 13%;">ანაზღაურების ტიპი</th>
				<th style="width: 15%;">კომენტარი</th>
				<th style="width: 5%;">ფერი</th>
				<th class="check">#</th>
			</tr>
		</thead>
		<thead>
			<tr class="search_header">
				<th class="colum_hidden">
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<div class="callapp_checkbox">
						<input class="tbale-checker" data-table="work_Shift_table" type="checkbox" id="shiftTableChecker" />
															<label for="shiftTableChecker"></label>
					</div>
				</th>
			</tr>
		</thead>
	</table>
	';

}

// activities
function get_activities_directory() {

	return '
	<div class="button-area">
		<button id="add_button_activities">დამატება</button>
		<button id="delete_button_activities">წაშლა</button>
	</div>
	<table class="display" id="work_activities_table">
		<thead>
			<tr id="datatable_header">
				<th>ID</th>
				<th style="width: 31%;">დასახელება</th>
				<th style="width: 31%;">ანაზღაურების ტიპი</th>
				<th style="width: 31%;">კომენტარი</th>
				<th style="width: 7%;">ფერი</th>
				<th class="check">#</th>
			</tr>
		</thead>
		<thead>
			<tr class="search_header">
				<th class="colum_hidden">
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<input type="text" name="search_category" value="ფილტრი" class="search_init" />
				</th>
				<th>
					<div class="callapp_checkbox">
						<input class="tbale-checker" data-table="work_activities_table" type="checkbox" id="activitiesTableChecker" />
															<label for="activitiesTableChecker"></label>
					</div>
				</th>
			</tr>
		</thead>
	</table>
	';

}

// return work force managment reports structure
function wfm_report_structure() {

    return '<div class="button-area wfm-report-button-area">
                    <div class="wfm-report-filter-column">
                      <label for="wfmReportFilterStartDate">თარიღი (დან)</label>
                      <input type="text" id="wfmReportFilterStartDate" class="jquery-time-picker wfm-report-filter" placeholder="აირჩიეთ თარიღი" />
                    </div>
                    <div class="wfm-report-filter-column">
                      <label for="wfmReportFilterEndDate">თარიღი (მდე)</label>
                      <input type="text" id="wfmReportFilterEndDate" class="jquery-time-picker wfm-report-filter" placeholder="აირჩიეთ თარიღი" />
                    </div>
                    <div class="wfm-report-filter-column">
                      <label for="wfmReportOperators">ოპერატორი</label>
                      <select id="wfmReportOperators" class="jquery-chosen wfm-report-filter" data-placeholder="აირჩიეთ ოპერატორი" multiple>
                        '.get_operators_list().'
                      </select>
                    </div>
                    <div class="wfm-report-filter-column">
                      <label for="wfmReportActivities">აქტივობა</label>
                      <select id="wfmReportActivities" class="jquery-chosen wfm-report-filter" data-placeholder="აირჩიეთ აქტივობა" multiple>
                        '.get_multy_activitie_list().'
                      </select>
                    </div>
					<div class="wfm-report-filter-column">
							<button id ="button-filter">ძებნა</button>
					</div>
                </div>

				<div class="wfm-report-content-wrapper">
				<div class="wfm-report-table">
				  <div class="wfm-report-table-head">
					<div class="wfm-report-col wfm-rc-center wfm-rc-relative">
					  <span>ოპერატორი</span>
					  <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
						<i class="material-icons">arrow_drop_down</i>
					  </buuton>
					</div>

					<div class="wfm-report-col wfm-rc-center wfm-rc-relative">
					  <span>გეგმიური სამუშაო</span>
					  <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
						<i class="material-icons">arrow_drop_down</i>
					  </buuton>
					</div>
					<div class="wfm-report-col wfm-rc-center wfm-rc-relative">
					<span>ფაქტიური სამუშაო</span>
					<button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
					  <i class="material-icons">arrow_drop_down</i>
					</buuton>
				  </div>
				  <div class="wfm-report-col wfm-rc-center wfm-rc-relative">
				  <span>სხვაობა</span>
				  <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
					<i class="material-icons">arrow_drop_down</i>
				  </buuton>
				</div>


					
					<div class="wfm-report-col wfm-rc-center wfm-rc-relative">
					  <span> შესვენების გეგმიური <br/> ხ-ბა</span>
					</div>
					<div class="wfm-report-col wfm-rc-center wfm-rc-relative">
					  <span> შესვენების ფაქტიური <br/> ხ-ბა</span>
					  <button class="wfm-report-sort" data-sort="actually_time" data-sort-dir="DESC">
						<i class="material-icons">arrow_drop_down</i>
					  </buuton>
					</div>
					<div class="wfm-report-col wfm-rc-center wfm-rc-relative">
				  <span>სხვაობა</span>
				  <button class="wfm-report-sort active" data-sort="operator" data-sort-dir="DESC">
					<i class="material-icons">arrow_drop_down</i>
				  </buuton>
				</div>
				  </div>
				  
				  <div class="wfm-report-table-body">

                    </div>
                  </div>
                  <div class="wfm-report-diagram">
                    <div id="chartReportContainer" style="width: 100%; height: 400px; margin: 0 auto"></div>
                  </div>
                </div>

                <div>
                </div>';

}

/* DIRECTORY STRUCTURES */

// returns all operators ids
function get_all_ids($table_name) {
    global $db;
    $db->setQuery("SELECT id FROM $table_name WHERE actived = 1");
    $id_collector = "";
    $query = $db->getResultArray();
    foreach($query[result] AS $res) {
        if($id_collector == "") {
            $id_collector = $res["id"];
        } else {
            $id_collector .= ",".$res["id"];
        }
    }

    return $id_collector;

}

/* WORK GRAPHIC FUNCTIONS */

// save work graphic data
function save_work_graphic_data($project_id, $year, $month) {
	GLOBAL $db;
	$user_id = $_SESSION["USERID"];

	$db->setQuery("INSERT INTO `work_graphic` (`user_id`, `project_id`, `year`, `month`)
							VALUES ('$user_id', '$project_id', '$year', '$month') ");
	$db->execQuery();
	// define return result variable
	$return_result = array(
		"complete" => true
	);

	return $return_result;
}

// save work graphic rows data
function save_work_graphic_rows_data($work_graphic_id, $row_id, $operator_id, $cycle_id) {
	GLOBAL $db;

	$db->setQuery("INSERT INTO `work_graphic_rows` (`work_graphic_id`, `row_id`, `operator_id`, `cycle_id`)
							VALUES ('$work_graphic_id', '$row_id', '$operator_id', '$cycle_id')");
	$db->execQuery();
	// get last inserted id
	$last_inserted_id = $db->getLastId();

	// save log
	$save_data = save_work_graphic_rows_log($last_inserted_id, 1);

	// define return result variable
	$return_result = array(
		"complete" => true
	);

	return $return_result;

}

// clear work graphic rows data
function clear_work_graphic_rows_data($row_id) {
    global $db;
	$db->setQuery("SELECT id FROM `work_graphic_cols` WHERE work_graphic_rows_id = '$row_id'");

	$log_query = $db->getResultArray();
	foreach($log_query[result] AS $log_res) {

		$col_id = $log_res["id"];
		save_work_graphic_cols_log($col_id, 3);

	}

	$db->setQuery("UPDATE work_graphic_cols SET actived = 0 WHERE work_graphic_rows_id = '$row_id'");
    $db->execQuery();
	return true;

}

// save work graphic rows log
function save_work_graphic_rows_log($row_id, $log_action_id) {
	GLOBAL $db;
	$user_id = $_SESSION["USERID"];

	$db->setQuery("INSERT INTO `work_graphic_rows_log` (`user_id`, `log_action_id`, `work_graphic_rows_id`)
							VALUES ('$user_id', '$log_action_id', '$row_id')");
	$db->execQuery();
	return true;
}

// validate work graphic col saving
function validate_work_graphic_saving($work_graphic_rows_id, $col_id, $shift_id) {
		global $db;
		// get save shift data
		$save_shift_data = get_work_shift_data_by_id($shift_id);
		$save_shift_in_one_day = $save_shift_data["start_date"] < $save_shift_data["end_date"];
			

		// PREVIEWS DAY
		$prev_col_id = $col_id - 1;
		$db->setQuery("SELECT work_shift_id,
											work_shift.start_date,
											work_shift.end_date
									FROM work_graphic_cols
									JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
									WHERE work_graphic_rows_id = '$work_graphic_rows_id'
										AND col_id = '$prev_col_id'
										AND work_graphic_cols.actived = 1");

		$query_prev = $db->getResultArray();
		foreach ($query_prev[result] AS $res_prev) {

			if($res_prev["start_date"] > $res_prev["end_date"] && $res_prev["end_date"] > $save_shift_data["start_date"]) {
				
				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "prevDayTime"
				);
		
				return $return_result;
				
			}

		}

		// SAME DAY
		$db->setQuery("SELECT work_shift_id,
									work_shift.start_date,
									work_shift.end_date
							FROM work_graphic_cols
							JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
							WHERE work_graphic_rows_id = '$work_graphic_rows_id'
								AND col_id = '$col_id'
								AND work_graphic_cols.actived = 1");
		$query = $db->getResultArray();
		foreach($query[result] AS $res) {

			if($save_shift_data["id"] == $res["work_shift_id"]) {
				
				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "id"
				);
		
				return $return_result;
				
			} else if($res["start_date"] == "00:00:00" && $res["end_date"] == "00:00:00") {
				
				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "dayoff"
				);
		
				return $return_result;
				
			} else if($save_shift_data["start_date"] == $res["start_date"] && $save_shift_data["end_date"] == $res["end_date"]) {
				
				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "time"
				);
		
				return $return_result;
				
			} else if($save_shift_data["start_date"] <= $res["start_date"] && $save_shift_data["end_date"] > $res["start_date"]) {
				
				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "time"
				);
		
				return $return_result;
				
			} else if($save_shift_data["end_date"] >= $res["end_date"] && $save_shift_data["start_date"] < $res["end_date"]) {

				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "time"
				);
		
				return $return_result;

			} else if($save_shift_data["start_date"] >= $res["end_date"] && ($save_shift_data["end_date"] >= $res["start_date"] && $save_shift_data["end_date"] <= $res["end_date"])) {

				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "time"
				);
		
				return $return_result;

			} else if($save_shift_data["end_date"] <= $res["start_date"] && ($save_shift_data["start_date"] <= $res["end_date"] && $save_shift_data["start_date"] >= $res["start_date"])) {

				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "time"
				);
		
				return $return_result;
			}

		}

		// NEXT DAY
		$next_col_id = $col_id + 1;
		$db->setQuery("SELECT work_shift_id,
											work_shift.start_date,
											work_shift.end_date
									FROM work_graphic_cols
									JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
									WHERE work_graphic_rows_id = '$work_graphic_rows_id'
										AND col_id = '$next_col_id'
										AND work_graphic_cols.actived = 1");

		$query_next = $db->getResultArray();
		foreach($query_next[result] AS $res_next) {

			if(!$save_shift_in_one_day && $save_shift_data["end_date"] > $res_next["start_date"]) {

				$return_result = array(
					"allow_save" => false,
					"denial_reasone" => "nextDayTime"
				);
		
				return $return_result;
			}

		}

		// if everything is ok
		$return_result = array(
			"allow_save" => true,
			"denial_reasone" => ""
		);

		return $return_result;

}

// save work graphic cols data
function save_work_graphic_cols_data($work_graphic_rows_id, $col_id, $shift_id, $comment, $month) {
    global $db;
	$user = $_SESSION['USERID'];
	// define allow save variable
	$validate_saving = validate_work_graphic_saving($work_graphic_rows_id, $col_id, $shift_id);

	// check allow saving
	if($validate_saving["allow_save"] == true) {

		$db->setQuery("INSERT INTO `work_graphic_cols` (`work_graphic_rows_id`, `col_id`, `work_shift_id`,`comment`,`month`,`operator_id`)
								  VALUES ('$work_graphic_rows_id', '$col_id', '$shift_id','$comment', '$month','$user')");
        $db->execQuery();
		// get last inserted id
		$last_inserted_id = $db->getLastId();

		// save log
		$save_data = save_work_graphic_cols_log($last_inserted_id, 1);

	} else {
		$save_data = false;
	}

	// define return result variable
	$return_result = array(
		"complete" => 1,
		"denialReasone" => $validate_saving["denial_reasone"]
	);

	return $return_result;

}

// save work graphic cols log
function save_work_graphic_cols_log($col_id, $log_action_id) {
    global $db;
	$user_id = $_SESSION["USERID"];

	$db->setQuery("INSERT INTO `work_graphic_cols_log` (`user_id`, `log_action_id`, `work_graphic_cols_id`)
							VALUES ('$user_id', '$log_action_id', '$col_id')");
	$db->execQuery();
	return true;
}

// get work graphic data by project id, year and month
function get_work_graphic_data_by_details($project_id, $year, $month) {
	GLOBAL $db;
	$db->setQuery("SELECT `id` 
						FROM `work_graphic` 
						WHERE 	`project_id` = '$project_id'
							AND `year` = '$year'
							AND `month` = '$month'");
	$query = $db->getResultArray();
	$count 	= $query['count'];

	$result = array("count" => $count, "id" => $query['result'][0]['id']);

	return $result;

}

// get work graphic rows data by graphic id and row id
function get_work_graphic_rows_data_by_details($graphic_id, $row_id) {
global $db;
	$query = $db->setQuery("SELECT `id` 
						FROM `work_graphic_rows` 
						WHERE 	`work_graphic_id` = '$graphic_id'
							AND `row_id` = '$row_id'
							AND `work_graphic_rows`.`actived` = 1");

	$count 	= $db->getNumRow();
	$id 	= $db->getResultArray();

	$result = array("count" => $count, "id" => $id[result][0][id]);

	return $result;

}

// get work shift data by id
function get_work_shift_data_by_id($work_shift_id) {
	GLOBAL $db;
	$db->setQuery("SELECT * FROM work_shift WHERE id = '$work_shift_id' AND actived = 1");
	$result = $db->getResultArray();

	return $result['result'][0];

}

// get work cycle first shift
function get_work_cycle_first_shift($cycle_id) {
	GLOBAL $db;
	$db->setQuery("SELECT work_cycle_detail.id,
							work_cycle_detail.work_shift_id,
							work_cycle_detail.num,
							work_shift.start_date,
							work_shift.end_date
						FROM work_cycle_detail
						JOIN 	work_shift ON work_shift.id = work_cycle_detail.work_shift_id
						WHERE work_cycle_id = '$cycle_id' 
						AND work_cycle_detail.actived = 1 
						ORDER BY work_cycle_detail.id 
						ASC 
						LIMIT 1");

	$result = $db->getResultArray();

	return $result['result'][0];

}

// get work cycle last shift
function get_work_cycle_last_shift($cycle_id) {
	GLOBAL $db;
	$db->setQuery("SELECT work_cycle_detail.id,
							work_cycle_detail.work_shift_id,
							work_cycle_detail.num,
							work_shift.start_date,
							work_shift.end_date
						FROM work_cycle_detail
						JOIN 	work_shift ON work_shift.id = work_cycle_detail.work_shift_id
						WHERE work_cycle_id = '$cycle_id' 
						AND work_cycle_detail.actived = 1 
						ORDER BY work_cycle_detail.id 
						DESC
						LIMIT 1");

	$result = $db->getResultArray();

	return $result['result'][0];

}

// work graphic col data query
function work_graphic_col_data_query($row_id, $col_id) {
    global $db;
	// query data
	$db->setQuery("SELECT work_graphic_cols.id AS data_id,
								work_shift.id AS shift_id,
								work_shift.start_date AS shift_start_time,
								work_shift.end_date AS shift_end_time,
								work_shift.timeout AS shift_timeout
							FROM `work_graphic_cols`
							JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
							WHERE work_graphic_rows_id = '$row_id'
							AND col_id = '$col_id'
							AND work_graphic_cols.actived = 1 AND work_shift.id not in (0,3,11,13)");
	$query = $db->getResultArray();
	// return query
	return $query;

}

// process work graphic col data
function process_work_graphic_col_data($query, $type) {

	// define data collector
	$data_collector = array();

	// loop trow data
	foreach($query[result] AS $res) {

		// define shift times
		$shift_times = array(
			"start_date" => $res["shift_start_time"],
			"end_date" => $res["shift_end_time"],
			"timeout" => $res["shift_timeout"]
		);

		// define processed shift times
		$processed_shift_times = process_shift_times($shift_times);

		// check type
		if($type == "current") {

			// define data
			$data = array(
				"id" => $res["data_id"],
				"shiftId" => $res["shift_id"],
				"shiftStartTime" => $processed_shift_times["cur_day_work_time_start"],
				"shiftEndTime" => $processed_shift_times["cur_day_work_time_end"],
				"shiftTimeout" => $processed_shift_times["current_day_timeout"],
				"shiftWorkTime" => $processed_shift_times["current_day_work_time"]
			);

		} else if($type == "previews") {

			// define data
			$data = array(
				"id" => $res["data_id"],
				"shiftId" => $res["shift_id"],
				"shiftStartTime" => $processed_shift_times["next_day_work_time_start"],
				"shiftEndTime" => $processed_shift_times["next_day_work_time_end"],
				"shiftTimeout" => $processed_shift_times["next_day_timeout"],
				"shiftWorkTime" => $processed_shift_times["next_day_work_time"]
			);

		}
		
		// push data in data collector
		array_push($data_collector, $data);
		
	}

	// return data collector
	return $data_collector;

}

/* WORK GRAPHIC FUNCTIONS */

// get work graphic col data
function get_work_graphic_col_data($row_id, $col_id) {
	
	// current day data query
	$cur_day_data_query = work_graphic_col_data_query($row_id, $col_id);

	// define current day data
	$cur_day_data = process_work_graphic_col_data($cur_day_data_query, "current");

	// previews day data query
	$prev_day_data_query = work_graphic_col_data_query($row_id, $col_id - 1);

	// define previews day data
	$prev_day_data = process_work_graphic_col_data($prev_day_data_query, "previews");

	// define data collector
	$data_collector = array();

	foreach($cur_day_data as $cur_day_value) {
		array_push($data_collector, $cur_day_value);
	}

	foreach($prev_day_data as $prev_day_value) {
		array_push($data_collector, $prev_day_value);
	}

	// return data collector
	return $data_collector;

}

// get work graphic day activities
function get_work_graphic_col_activities($row_id, $col_id) {
    global $db;
	// define data collector
	$data_collector = array();

	// query the data
	$db->setQuery("SELECT 	work_graphic_activities.id AS data_id,
								work_activities.id AS activitie_id,
								work_graphic_activities.start AS activitie_start,
								work_graphic_activities.end AS activitie_end,
								work_activities.name AS activitie_name,
								work_activities.color AS activitie_color
							FROM `work_graphic_activities`
							JOIN	work_activities ON work_activities.id = work_graphic_activities.activitie_id
							WHERE work_graphic_activities.actived = 1
								AND row_id = '$row_id'
								AND col_id = '$col_id'");

	// get count of data for check
	$data_count = $db->getResultArray();
	$query = $db->getResultArray();
	// check count of data, if exists
	if($data_count > 0) {

		// if data exists loop throw it
	    foreach ($query[result] AS $res) {

			// define data
			$data = array(
				"id" => $res["data_id"],
				"activitieId" => $res["activitie_id"],
				"activitieStart" => $res["activitie_start"],
				"activitieEnd" => $res["activitie_end"],
				"activitieName" => $res["activitie_name"],
				"activitieColor" => $res["activitie_color"]
			);

			// push data in data collector
			array_push($data_collector, $data);

		}

	}

	// return collected data
	return $data_collector;

}

// get work graphic day rush hours
function get_work_graphic_col_rush_hours($project_id, $year, $week_number) {
global $db;
	// define data collector
	$data_collector = array();

	//query data
	$db->setQuery("SELECT wfm_rush_hours_rows.row AS day_index,
								wfm_rush_hours_rows.start,
								wfm_rush_hours_rows.end
							FROM `wfm_rush_hours`
							JOIN wfm_rush_hours_rows ON wfm_rush_hours_rows.wfm_rush_hours_id = wfm_rush_hours.id
							WHERE actived = 1
							AND project_id = '$project_id'
							AND year = '$year'
							AND week_num = '$week_number'");
	$query = $db->getResultArray();
	// loop throw data
	foreach($query[result] AS $res) {

		// define data
		$data = array(
			"dayIndex" => $res["day_index"],
			"start" => $res["start"],
			"end" => $res["end"]
		);

		// push data in data collector
		array_push($data_collector, $data);

	}

	// return collected data
	return $data_collector;

}

// get work planner data by project id
function get_work_planner_data_by_project($project_id) {
	GLOBAL $db;


	$db->setQuery("	SELECT `id` 
					FROM `work_planner` 
					WHERE 	`project_id` = '$project_id'");
	$query = $db->getResultArray();

	$count 	= $db->getNumRow();
	$id 	= $query['result'][0]['id'];

	$result = array("count" => $count, "id" => $id);

	return $result;

}

// get work planner rows data by planner id and row id
function get_work_planner_rows_data_by_details($planner_id, $row_id) {
	GLOBAL $db;
	$db->setQuery("SELECT `id` 
						FROM `work_planner_rows` 
						WHERE 	`work_planner_id` = '$planner_id'
							AND `row_id` = '$row_id'
							AND `work_planner_rows`.`actived` = 1");
	$query = $db->getResultArray();
	$count 	= $query['count'];
	$id 	= $query['result'][0]['id'];

	$result = array("count" => $count, "id" => $id);

	return $result;

}

// save work planner data
function save_work_planner_data($project_id) {
    global $db;
	$user_id = $_SESSION["USERID"];

	$db->setQuery("INSERT INTO `work_planner` (`user_id`, `project_id`)
							VALUES ('$user_id', '$project_id') ");
    $db->execQuery();
	// define return result variable
	$return_result = array(
		"complete" => true
	);

	return $return_result;
}

// save work planner rows data
function save_work_planner_rows_data($work_planner_id, $row_id, $cycle_id, $save_as_cycle_id) {
	GLOBAL $db;
	$db->setQuery("INSERT INTO `work_planner_rows` (`work_planner_id`, `row_id`, `cycle_id`, `save_as_cycle_id`)
							VALUES ('$work_planner_id', '$row_id', '$cycle_id', '$save_as_cycle_id')");
	$db->execQuery();
	// define return result variable
	$return_result = array(
		"complete" => true
	);

	return $return_result;

}

// save work planner cols data
function save_work_planner_cols_data($work_planner_rows_id, $col_id, $shift_id) {
	GLOBAL $db;
	// define allow save variable
	$validate_saving = validate_work_planner_saving($work_planner_rows_id, $col_id, $shift_id);

	// check allow saving
	if($validate_saving["allow_save"] == true) {

		$db->setQuery("INSERT INTO `work_planner_cols` (`work_planner_rows_id`, `col_id`, `work_shift_id`)
								  VALUES ('$work_planner_rows_id', '$col_id', '$shift_id')");
		$db->execQuery();
		// get last inserted id
		//$last_inserted_id = get_last_inserted_id("work_planner_cols");

	} else {
		$save_data = false;
	}

	// define return result variable
	$return_result = array(
		"complete" => true,
		"denialReasone" => $validate_saving["denial_reasone"]
	);

	return $return_result;

}

// validate work planner col saving
function validate_work_planner_saving($work_planner_rows_id, $col_id, $shift_id) {
	GLOBAL $db;
	// get save shift data
	$save_shift_data = get_work_shift_data_by_id($shift_id);
		
	// define allow save variable
	$allow_save = true;

	// define denial reasone variable
	$denial_reasone = "";

	$db->setQuery("SELECT work_shift_id,
								work_shift.start_date,
								work_shift.end_date
						FROM work_planner_cols
						JOIN work_shift ON work_shift.id = work_planner_cols.work_shift_id
						WHERE work_planner_rows_id = '$work_planner_rows_id'
							AND col_id = '$col_id'
							AND work_planner_cols.actived = 1");
	$query = $db->getResultArray();
	foreach($query['result'] AS $res) {

		if($save_shift_data["id"] == $res["work_shift_id"]) {
			$allow_save = false;
			$denial_reasone = "id";
		} else if($save_shift_data["start_date"] == $res["start_date"] && $save_shift_data["end_date"] == $res["end_date"]) {
			$allow_save = false;
			$denial_reasone = "time";
		} else if($save_shift_data["start_date"] <= $res["start_date"] && $save_shift_data["end_date"] > $res["start_date"]) {
			$allow_save = false;
			$denial_reasone = "time";
		} else if($save_shift_data["end_date"] >= $res["end_date"] && $save_shift_data["start_date"] < $res["end_date"]) {
			$allow_save = false;
			$denial_reasone = "time";
		} else if($save_shift_data["start_date"] >= $res["end_date"] && ($save_shift_data["end_date"] >= $res["start_date"] && $save_shift_data["end_date"] <= $res["end_date"])) {
			$allow_save = false;
			$denial_reasone = "time";
		} else if($save_shift_data["end_date"] <= $res["start_date"] && ($save_shift_data["start_date"] <= $res["end_date"] && $save_shift_data["start_date"] >= $res["start_date"])) {
			$allow_save = false;
			$denial_reasone = "time";
		}

	}

	$return_result = array(
		"allow_save" => $allow_save,
		"denial_reasone" => $denial_reasone
	);

	return $return_result;

}



function sum_the_time($time1, $time2) {
  $times = array($time1, $time2);
  $seconds = 0;
  foreach ($times as $time) {
    list($hour,$minute) = explode(':', $time);
    $seconds += $hour*3600;
    $seconds += $minute*60;
  }

  $hours = floor($seconds/3600);
  $seconds -= $hours*3600;
  $minutes  = floor($seconds/60);
  $seconds -= $minutes*60;
  // return "{$hours}:{$minutes}:{$seconds}";
  return sprintf('%02d:%02d', $hours, $minutes);
}

function calculate_cycle_time($cycle_hours, $days_in_month) {

	$hour_collector = "00:00";

	for($i = 0; $i < $days_in_month; $i++) {

		$data_index = $i % count($cycle_hours);
		$hour_collector = sum_the_time($hour_collector, $cycle_hours[$data_index]);

	}

	list($hours, $minutes) = explode(":", $hour_collector);

	// return (int)$hours;
	return (int)$hours;

}

// converts time to seconds
function string_time_to_seconds($time) {

	list($h, $m, $s) = explode(":", $time);

	$h = $h * 3600;
	$m = $m * 60;
	$s += $h + $m;

	return $s;

}

// converts time from seconds
function seconds_to_string_time($seconds) {

	$h = floor($seconds / 3600);
	$seconds = $seconds - ($h * 3600);

	$m = floor($seconds / 60);
	$s = $seconds - ($m * 60);

	return zeroFixer($h).":".zeroFixer($m);

}

// converts time to mintes
function string_time_to_minutes($time) {

	list($h, $m) = explode(":", $time);

	$m += $h * 60;

	return $m;

}

// converts minutes to hours
function minutes_to_hours($minutes_sum) {

	$hours = floor($minutes_sum / 60);
	$minutes = $minutes_sum % 60;

	return zeroFixer($hours).':'.zeroFixer($minutes);

}

// adds minutes and returns time in 24 hour format
function add_hours($hour1, $hour2) {

	$hour1 = string_time_to_minutes($hour1);
	$hour2 = string_time_to_minutes($hour2);

	$minute_summary = $hour1 + $hour2;

	return minutes_to_hours($minute_summary);

}

// subtracts minutes and returns time in 24 hour format
function sub_hours($hour1, $hour2) {

	$hour1 = string_time_to_minutes($hour1);
	$hour2 = string_time_to_minutes($hour2);

	$minute_subtracted = $hour1 - $hour2;
	return minutes_to_hours($minute_subtracted);

}

// calculates how meny percent is second argument hour of first argument hour
function percent_of_hour($hour1, $hour2) {

	$hour1 = string_time_to_minutes($hour1);
	$hour2 = string_time_to_minutes($hour2);

	$percent = round(($hour2 / $hour1) * 100);
	$precent_divided = $percent % 10;
	$clear_precent = $percent - $precent_divided;

	return $clear_precent;

}

// calculates percent of hours
function percent_hours($hour, $percent) {

	$hour = string_time_to_minutes($hour);

	$percent_minutes = round(($hour / 100) * $percent);
	$precent_divided = $percent_minutes % 5;
	$clear_precent = $percent_minutes - $precent_divided;

	return minutes_to_hours($clear_precent);

}

function process_shift_timeout($cur_day_time_start, $shift_end, $shift_timeout) {

	$cur_day_foreseen_time = sub_hours("24:00", $cur_day_time_start);
	$shift_full_foreseen_time = add_hours($shift_end, $cur_day_foreseen_time);
	$cur_day_precent = percent_of_hour($shift_full_foreseen_time, $cur_day_foreseen_time);
	$cur_day_timeout = percent_hours($shift_timeout, $cur_day_precent);
	$next_day_timeout = sub_hours($shift_timeout, $cur_day_timeout);

	return array(
		"curDayTimeout" => $cur_day_timeout,
		"nextDayTimeout" => $next_day_timeout
	);

}

function calculate_shift_difference($start_time, $end_time, $timeout, $saved_for_next_day, $saved_for_next_timeout) {

	$start = string_time_to_minutes($start_time);
	$end =  string_time_to_minutes($end_time);
	$work_time = "00:00";
	$break_time = "00:00";
	$full_day = "24:00";
	$next_day_work_time = "00:00";
	$next_day_break_time = "00:00";
	$saved_from_prev_day = $saved_for_next_day;

	if($start >= $end && ($start != 0 && $end != 0)) {
		$current_day_difference = sub_hours($full_day, $start_time);
		$full_shift_hour = add_hours($current_day_difference, $end_time);
		$cur_day_precent = percent_of_hour($full_shift_hour, $current_day_difference);
		$cur_day_timeout = percent_hours($timeout, $cur_day_precent);
		$next_day_break_time = sub_hours($timeout, $cur_day_timeout);
		$cur_day_work_time = sub_hours($current_day_difference, $cur_day_timeout);
		$next_day_work_time = sub_hours($end_time, $next_day_break_time);
	} else if($start == 0 && $end == 0) {
		$cur_day_work_time = "00:00";
		$cur_day_timeout = $timeout;
		$saved_from_prev_day = "00:00";
		$next_day_work_time = $saved_for_next_day;
		$next_day_break_time = $saved_for_next_timeout;
	} else {
		$cur_day_work_time = sub_hours(sub_hours($end_time, $start_time), $timeout);
		$cur_day_timeout = $timeout;
	}

	$work_time = add_hours($cur_day_work_time, $saved_from_prev_day);
	$break_time = add_hours($cur_day_timeout, $saved_for_next_timeout);

	return array(
		"current_day" => $work_time,
		"next_day" => $next_day_work_time,
		"cur_timeout" => $break_time,
		"next_timeout" => $next_day_break_time
	);

}

function get_time_from_prev_day($start_time, $end_time, $time_from_prev_day) {

	if(date($start_time) > date($end_time)) {
		$time_from_prev_day = $end_time;
		$end_time = "24:00";
	} else if($start_time != "00:00" && $end_time != "00:00") {
		$time_from_prev_day = "00:00";
	}

	return array(
		"end_time" => $end_time,
		"time_from_prev_day" => $time_from_prev_day
	);

}



function get_project_edit_status($project_id, $row_num, $string_date) {

	$query = mysql_query("SELECT id
												FROM work_graphic_edit_history
												WHERE project_id = '$project_id'
													AND row_num = '$row_num'
													AND string_date = '$string_date'
													AND actived = 1");

	$edited = mysql_num_rows($query);

	return $edited > 0;

}

function Addproject($hidden_client_id, $project_name, $project_type, $project_add_date, $start_date_holi, $end_date_holi){

	$user = $_SESSION['USERID'];

	mysql_query("INSERT INTO `project`
						(`user_id`, `client_id`, `name`, `type_id`, `create_date`, `actived`, `start_date`, `end_date`)
					VALUES
						('$user', '$hidden_client_id', '$project_name', '$project_type', '$project_add_date', '1', '$start_date_holi', '$end_date_holi')");

}

function Saveproject($hidden_id,$project_name, $project_type, $project_add_date, $start_date_holi, $end_date_holi){

	$user = $_SESSION['USERID'];

	mysql_query("UPDATE  `project`
	 				SET  `user_id`='$user',
						 `name`='$project_name',
						 `type_id`='$project_type',
						 `create_date`='$project_add_date',
	                     `start_date`='$start_date_holi',
	                     `end_date`='$end_date_holi'
				WHERE `id`='$hidden_id'");

}

function swcs_AddDetail($cycle_id, $work_shift_id, $num)
{
	GLOBAL $db;
	$user_id	= $_SESSION['USERID'];
	$db->setQuery("INSERT INTO 	 `work_cycle_detail`
								(`user_id`, `work_cycle_id`, `work_shift_id`, `num`)
					VALUES 		('$user_id', '$cycle_id', '$work_shift_id', '$num')");
	$db->execQuery();
	return true;
}

function swcs_UpDetail($detail_id, $cycle_id, $work_shift_id, $num)
{
	GLOBAL $db;
    $user_id	= $_SESSION['USERID'];
    $db->setQuery("UPDATE `work_cycle_detail` SET
                        `work_shift_id`='$work_shift_id',
                        `num`='$num'
				 WHERE  `id`='$detail_id'");
				 
	$db->execQuery();
	return true;
}

function GetScenario($id){
    $data = '';
    $req = mysql_query("SELECT 	`id`,
                				`name`
                        FROM `scenario`
                        WHERE actived = 1");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){

        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    return $data;
}

function Get_type($count){
	$data = '';
	$req = mysql_query("SELECT id, `name`
						FROM `call_type`");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){

		if($res['id'] == $count){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		}else{
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}
	return $data;
}
function object($hidden_id){

	$res = mysql_fetch_assoc(mysql_query("SELECT  project.id,
												  project.`name`,
												  project.type_id,
												  project.create_date
											FROM `project`
											WHERE project.id='$hidden_id'"));
	return $res;
}

function work_cycle_shift_details($id){
	GLOBAL $db;
    $db->setQuery("	SELECT 	`work_cycle_detail`.`id`,
                                    				`work_cycle_detail`.`work_shift_id`,
                                                    `work_cycle_detail`.`num`
                                            FROM 	`work_cycle_detail`
                                            JOIN    `work_cycle` ON work_cycle_detail.work_cycle_id = work_cycle.id
                                            WHERE 	`work_cycle_detail`.`id` = $id" );
	$res = $db->getResultArray();
    return $res['result'][0];
}

function GetHoliday(){
    $data = '';
    $req = mysql_query("SELECT  `id`,
                                `name`
						FROM    `holidays`
                        WHERE   `actived` = 1");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
    }
    return $data;
}

function GetLang($id){
    $data = '';
    $req = mysql_query("SELECT  `id`,
								`name`
						FROM    `language`
                        WHERE   `actived` = 1");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){

        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    return $data;
}

function GetInfoSource($id){
    $data = '';
    $req = mysql_query("SELECT 	`id`,
                				`name`
                        FROM    `source`
                        WHERE   `actived` = 1");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){

        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    return $data;
}

function GetDay($day_id)
{
    $data = '';
    $req = mysql_query("SELECT id,
                              `name`
                        FROM `week_day` ");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $day_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}
function GetHour($wday,$clock,$project_id){
    if(strlen($clock)==1){
        $real_clock = '0'.$clock;
    }else{
        $real_clock = $clock;
    }

    $data = '
        <style>
	    #table_hour{

	    width: 100%;
	    margin-top:25px;
	    }
	    #table_hour td,#table_hour th{
	    border: 1px solid;
        font-size: 11px;
        font-weight: normal;
        text-align: center;
        height: 13px;
	    }
	    </style>
        <div id="dialog-form">
    	    <fieldset style="width: 175px;">
    	       <legend>ძირითადი ინფორმაცია</legend>
                <div style="width: 855px;">
                <table id="table_hour">
                    <tr>
                        <th style="width: ;"></th>';
                        for($i = 5;$i < 60;$i+=5){
                            if(strlen($i) == 1){
                                $data .= '<th style="width: ;"><span id="clock_hour">'.$real_clock.'</span>:0'.$i.'</th>';
                            }else{
                                $data .= '<th style="width: ;"><span id="clock_hour">'.$real_clock.'</span>:'.$i.'</th>';
                            }
                        }
                        $data .= '
                    </tr>
    	            <tr id="wday1">
                        <td class="wday">'.$req[0].'</td>';
                            for($i = 5;$i < 60;$i+=5){
                                    $data .= '<td clockid="'.$i.'"  check_clock=""></td>';
                            }

                            $req = mysql_fetch_array($res = mysql_query("SELECT  CASE
                                                            WHEN week_day_id = 1 THEN 'ორშ'
                                                            WHEN week_day_id = 2 THEN 'სამ'
                                                            WHEN week_day_id = 3 THEN 'ოთხ'
                                                            WHEN week_day_id = 4 THEN 'ხუთ'
                                                            WHEN week_day_id = 5 THEN 'პარ'
                                                            WHEN week_day_id = 6 THEN 'შაბ'
                                                            WHEN week_day_id = 7 THEN 'კვი'
                                                            END AS `week_day`,
                                                            TIME_FORMAT(start_time,'%H:%i') AS `start_time`,
                                                            TIME_FORMAT(end_time,'%H:%i') AS `end_time`,
                                                            TIME_FORMAT(start_time,'%i'),
                                                            TIME_FORMAT(end_time,'%i'),
                                                            TIME_FORMAT(start_time,'%H'),
                                                            TIME_FORMAT(end_time,'%H'),
                                                            ext_number
                                                FROM `week_day_graphic`
                                                WHERE TIME_FORMAT(start_time,'%H') <= '$real_clock' AND TIME_FORMAT(end_time,'%H') >= '$real_clock' AND  project_id = '$project_id' AND week_day_id = '$wday' AND actived = 1 AND type = 1"));


                                if($req[3] == '00' && $req[4] == '00'){
                                    $data .= '<script>
                                              $("td[clockid]").css("background","green");
                                              $("td[clockid]").html("'.$req[7].'");
                                              $(".wday").html("'.$req[0].'");
                                              </script>';
                                }

                                if($req[3] != '00' && $req[4] == '00'){
                                    $data .= '<script>
                                                        $("td[clockid]").css("background","green");
                                                        $("td[clockid]").html("'.$req[7].'");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                    for($i = 5;$i < 60;$i+=5){
                                        if($i < $req[3] && $real_clock == $req[5]){
                                            $data .= '<script>
                                                        $("td[clockid='.$i.']").css("background","");
                                                        $("td[clockid='.$i.']").html("");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                        }
                                        if($i > $req[4] && $real_clock == $req[6]){
                                            $data .= '<script>
                                                        $("td[clockid='.$i.']").css("background","");
                                                        $("td[clockid='.$i.']").html("");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                        }
                                    }
                                }
                                if($req[3] != '00' && $req[4] != '00'){
                                    $data .= '<script>
                                                        $("td[clockid]").css("background","green");
                                                        $("td[clockid]").html("'.$req[7].'");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                    for($i = 5;$i < 60;$i+=5){
                                        if($i < $req[3] && $real_clock == $req[5]){
                                            $data .= '<script>
                                                        $("td[clockid='.$i.']").css("background","");
                                                        $("td[clockid='.$i.']").html("");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                        }
                                        if($i > $req[4] && $real_clock == $req[6]){
                                            $data .= '<script>
                                                        $("td[clockid='.$i.']").css("background","");
                                                        $("td[clockid='.$i.']").html("");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                        }
                                    }
                                }
                                if($req[3] == '00' && $req[4] != '00'){
                                    $data .= '<script>
                                                        $("td[clockid]").css("background","green");
                                                        $("td[clockid]").html("'.$req[7].'");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                    for($i = 5;$i < 60;$i+=5){
                                        if($i < $req[3] && $real_clock == $req[5]){
                                            $data .= '<script>
                                                        $("td[clockid='.$i.']").css("background","");
                                                        $("td[clockid='.$i.']").html("");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                        }
                                        if($i > $req[4] && $real_clock == $req[6]){
                                            $data .= '<script>
                                                        $("td[clockid='.$i.']").css("background","");
                                                        $("td[clockid='.$i.']").html("");
                                                            $(".wday").html("'.$req[0].'");
                                                        </script>';
                                        }
                                    }
                                }


                            $g = mysql_query("  SELECT  TIME_FORMAT(start_time,'%H:%i') AS `start_time`,
                                        				TIME_FORMAT(end_time,'%H:%i') AS `end_time`,
                                                        TIME_FORMAT(start_time,'%i'),
                                                        TIME_FORMAT(end_time,'%i'),
                                                        TIME_FORMAT(start_time,'%H'),
                                                        TIME_FORMAT(end_time,'%H'),
                                                        CASE
                                    						WHEN week_day_id = 1 THEN 'ორშ'
                                    						WHEN week_day_id = 2 THEN 'სამ'
                                    						WHEN week_day_id = 3 THEN 'ოთხ'
                                    						WHEN week_day_id = 4 THEN 'ხუთ'
                                    						WHEN week_day_id = 5 THEN 'პარ'
                                    						WHEN week_day_id = 6 THEN 'შაბ'
                                    						WHEN week_day_id = 7 THEN 'კვი'
                                    				    END AS `week_day`
                                                FROM `week_day_graphic`
                                                WHERE project_id = '$project_id' AND week_day_id = '$wday' AND actived = 1 AND type = 2 ");
                            while ($gg = mysql_fetch_array($g)){
                                for($i = 5;$i < 60;$i+=5){
                                if($gg[4] == $real_clock && $gg[2]=='00' && $gg[3]=='00'){
                                    $data .= '<script>
                                                $("td[clockid='.$i.']").css("background","yellow");
                                                $(".wday").html("'.$gg[6].'")
                                                    $("td[clockid]").html("");
                                              </script>';
                                }elseif($gg[4] == $real_clock && $gg[2]!='00' && $gg[3]!='00' && $gg[2]< $i){
                                    $data .= '<script>
                                                $("td[clockid='.$i.']").css("background","yellow");
                                                $(".wday").html("'.$gg[6].'");
                                                    $("td[clockid]").html("");
                                              </script>';
                                }elseif($gg[5] == $real_clock && $gg[2]!='00' && $gg[3]!='00' && $gg[3]> $i){
                                        $data .= '<script>
                                                    $("td[clockid='.$i.']").css("background","yellow");
                                                    $(".wday").html("'.$gg[6].'");
                                                        $("td[clockid]").html("");
                                                  </script>';
                                    }
                                }
                            }

                        $data .= '

                    </tr>
                </table>
                </div>
            </fieldset>
        </div>';
    return $data;
}

function GetPage($res,$increment){
	if ($res[id]=='') {
		$incr_id=increment(project);
	}else{
		$incr_id=$res[id];
	}

	$data  .= '

	<div id="dialog-form">


                       <fieldset style="" id="holiday">
                <legend>სამუშაო დღე/სთ</legend>
	            <span class="hide_said_menu">x</span>
	    <style>
	    #work_table{

	    width: 100%;
	    margin-top:15px;
	    }
	    #work_table td,#work_table th{
	    border: 1px solid;
        font-size: 11px;
        font-weight: normal;
        text-align: center;
	    }
	    .im_border{
	    border:1px solid;
	    }
        #work_table td input{
        display:none;
        }
	    </style>

	            <table class="dialog-form-table" id="work_table">
                    <tr>
                        <th style="width: ;"></th>
                	    <th style="width: ;">00:00</th>
                	    <th style="width: ;">01:00</th>
                	    <th style="width: ;">02:00</th>
                	    <th style="width: ;">03:00</th>
                	    <th style="width: ;">04:00</th>
                	    <th style="width: ;">05:00</th>
                	    <th style="width: ;">06:00</th>
                	    <th style="width: ;">07:00</th>
                	    <th style="width: ;">08:00</th>
                	    <th style="width: ;">09:00</th>
                	    <th style="width: ;">10:00</th>
                	    <th style="width: ;">11:00</th>
                	    <th style="width: ;">12:00</th>
                	    <th style="width: ;">13:00</th>
                	    <th style="width: ;">14:00</th>
	                    <th style="width: ;">15:00</th>
                	    <th style="width: ;">16:00</th>
                	    <th style="width: ;">17:00</th>
                	    <th style="width: ;">18:00</th>
                	    <th style="width: ;">19:00</th>
	                    <th style="width: ;">20:00</th>
                	    <th style="width: ;">21:00</th>
                	    <th style="width: ;">22:00</th>
                	    <th style="width: ;">23:00</th>
                    </tr>
    	            <tr id="wday1">
                        <td onclick="OpenWeek(1)">ორშ</td>
                	    <td style="" clock="0"  check_clock="" wday="1" ></td>
                	    <td style="" clock="1"  check_clock="" wday="1" ></td>
                	    <td style="" clock="2"  check_clock="" wday="1" ></td>
                	    <td style="" clock="3"  check_clock="" wday="1" ></td>
                	    <td style="" clock="4"  check_clock="" wday="1" ></td>
                	    <td style="" clock="5"  check_clock="" wday="1" ></td>
                	    <td style="" clock="6"  check_clock="" wday="1" ></td>
                	    <td style="" clock="7"  check_clock="" wday="1" ></td>
                	    <td style="" clock="8"  check_clock="" wday="1" ></td>
                	    <td style="" clock="9"  check_clock="" wday="1" ></td>
                	    <td style="" clock="10"  check_clock="" wday="1" ></td>
                	    <td style="" clock="11"  check_clock="" wday="1" ></td>
                	    <td style="" clock="12"  check_clock="" wday="1" ></td>
                	    <td style="" clock="13"  check_clock="" wday="1" ></td>
                	    <td style="" clock="14"  check_clock="" wday="1" ></td>
	                    <td style="" clock="15"  check_clock="" wday="1" ></td>
                	    <td style="" clock="16"  check_clock="" wday="1" ></td>
                	    <td style="" clock="17"  check_clock="" wday="1" ></td>
                	    <td style="" clock="18"  check_clock="" wday="1" ></td>
                	    <td style="" clock="19"  check_clock="" wday="1" ></td>
	                    <td style="" clock="20"  check_clock="" wday="1" ></td>
                	    <td style="" clock="21"  check_clock="" wday="1" ></td>
                	    <td style="" clock="22"  check_clock="" wday="1" ></td>
                	    <td style="" clock="23"  check_clock="" wday="1" ></td>
                    </tr>
	                <tr id="wday2">
                        <td onclick="OpenWeek(2)">სამ</td>
                	    <td style="" clock="0"  check_clock="" wday="2" ></td>
                	    <td style="" clock="1"  check_clock="" wday="2" ></td>
                	    <td style="" clock="2"  check_clock="" wday="2" ></td>
                	    <td style="" clock="3"  check_clock="" wday="2" ></td>
                	    <td style="" clock="4"  check_clock="" wday="2" ></td>
                	    <td style="" clock="5"  check_clock="" wday="2" ></td>
                	    <td style="" clock="6"  check_clock="" wday="2" ></td>
                	    <td style="" clock="7"  check_clock="" wday="2" ></td>
                	    <td style="" clock="8"  check_clock="" wday="2" ></td>
                	    <td style="" clock="9"  check_clock="" wday="2" ></td>
                	    <td style="" clock="10"  check_clock="" wday="2" ></td>
                	    <td style="" clock="11"  check_clock="" wday="2" ></td>
                	    <td style="" clock="12"  check_clock="" wday="2" ></td>
                	    <td style="" clock="13"  check_clock="" wday="2" ></td>
                	    <td style="" clock="14"  check_clock="" wday="2" ></td>
	                    <td style="" clock="15"  check_clock="" wday="2" ></td>
                	    <td style="" clock="16"  check_clock="" wday="2" ></td>
                	    <td style="" clock="17"  check_clock="" wday="2" ></td>
                	    <td style="" clock="18"  check_clock="" wday="2" ></td>
                	    <td style="" clock="19"  check_clock="" wday="2" ></td>
	                    <td style="" clock="20"  check_clock="" wday="2" ></td>
                	    <td style="" clock="21"  check_clock="" wday="2" ></td>
                	    <td style="" clock="22"  check_clock="" wday="2" ></td>
                	    <td style="" clock="23"  check_clock="" wday="2" ></td>
                    </tr>
	                <tr id="wday3">
                        <td onclick="OpenWeek(3)">ოთხ</td>
                	    <td style="" clock="0"  check_clock="" wday="3" ></td>
                	    <td style="" clock="1"  check_clock="" wday="3" ></td>
                	    <td style="" clock="2"  check_clock="" wday="3" ></td>
                	    <td style="" clock="3"  check_clock="" wday="3" ></td>
                	    <td style="" clock="4"  check_clock="" wday="3" ></td>
                	    <td style="" clock="5"  check_clock="" wday="3" ></td>
                	    <td style="" clock="6"  check_clock="" wday="3" ></td>
                	    <td style="" clock="7"  check_clock="" wday="3" ></td>
                	    <td style="" clock="8"  check_clock="" wday="3" ></td>
                	    <td style="" clock="9"  check_clock="" wday="3" ></td>
                	    <td style="" clock="10"  check_clock="" wday="3" ></td>
                	    <td style="" clock="11"  check_clock="" wday="3" ></td>
                	    <td style="" clock="12"  check_clock="" wday="3" ></td>
                	    <td style="" clock="13"  check_clock="" wday="3" ></td>
                	    <td style="" clock="14"  check_clock="" wday="3" ></td>
	                    <td style="" clock="15"  check_clock="" wday="3" ></td>
                	    <td style="" clock="16"  check_clock="" wday="3" ></td>
                	    <td style="" clock="17"  check_clock="" wday="3" ></td>
                	    <td style="" clock="18"  check_clock="" wday="3" ></td>
                	    <td style="" clock="19"  check_clock="" wday="3" ></td>
	                    <td style="" clock="20"  check_clock="" wday="3" ></td>
                	    <td style="" clock="21"  check_clock="" wday="3" ></td>
                	    <td style="" clock="22"  check_clock="" wday="3" ></td>
                	    <td style="" clock="23"  check_clock="" wday="3" ></td>
                    </tr>
	                <tr id="wday4">
                        <td onclick="OpenWeek(4)">ხუთ</td>
                	    <td style="" clock="0"  check_clock="" wday="4" ></td>
                	    <td style="" clock="1"  check_clock="" wday="4" ></td>
                	    <td style="" clock="2"  check_clock="" wday="4" ></td>
                	    <td style="" clock="3"  check_clock="" wday="4" ></td>
                	    <td style="" clock="4"  check_clock="" wday="4" ></td>
                	    <td style="" clock="5"  check_clock="" wday="4" ></td>
                	    <td style="" clock="6"  check_clock="" wday="4" ></td>
                	    <td style="" clock="7"  check_clock="" wday="4" ></td>
                	    <td style="" clock="8"  check_clock="" wday="4" ></td>
                	    <td style="" clock="9"  check_clock="" wday="4" ></td>
                	    <td style="" clock="10"  check_clock="" wday="4" ></td>
                	    <td style="" clock="11"  check_clock="" wday="4" ></td>
                	    <td style="" clock="12"  check_clock="" wday="4" ></td>
                	    <td style="" clock="13"  check_clock="" wday="4" ></td>
                	    <td style="" clock="14"  check_clock="" wday="4" ></td>
	                    <td style="" clock="15"  check_clock="" wday="4" ></td>
                	    <td style="" clock="16"  check_clock="" wday="4" ></td>
                	    <td style="" clock="17"  check_clock="" wday="4" ></td>
                	    <td style="" clock="18"  check_clock="" wday="4" ></td>
                	    <td style="" clock="19"  check_clock="" wday="4" ></td>
	                    <td style="" clock="20"  check_clock="" wday="4" ></td>
                	    <td style="" clock="21"  check_clock="" wday="4" ></td>
                	    <td style="" clock="22"  check_clock="" wday="4" ></td>
                	    <td style="" clock="23"  check_clock="" wday="4" ></td>
                    </tr>
	                <tr id="wday5">
                        <td onclick="OpenWeek(5)">პარ</td>
                	    <td style="" clock="0"  check_clock="" wday="5" ></td>
                	    <td style="" clock="1"  check_clock="" wday="5" ></td>
                	    <td style="" clock="2"  check_clock="" wday="5" ></td>
                	    <td style="" clock="3"  check_clock="" wday="5" ></td>
                	    <td style="" clock="4"  check_clock="" wday="5" ></td>
                	    <td style="" clock="5"  check_clock="" wday="5" ></td>
                	    <td style="" clock="6"  check_clock="" wday="5" ></td>
                	    <td style="" clock="7"  check_clock="" wday="5" ></td>
                	    <td style="" clock="8"  check_clock="" wday="5" ></td>
                	    <td style="" clock="9"  check_clock="" wday="5" ></td>
                	    <td style="" clock="10"  check_clock="" wday="5" ></td>
                	    <td style="" clock="11"  check_clock="" wday="5" ></td>
                	    <td style="" clock="12"  check_clock="" wday="5" ></td>
                	    <td style="" clock="13"  check_clock="" wday="5" ></td>
                	    <td style="" clock="14"  check_clock="" wday="5" ></td>
	                    <td style="" clock="15"  check_clock="" wday="5" ></td>
                	    <td style="" clock="16"  check_clock="" wday="5" ></td>
                	    <td style="" clock="17"  check_clock="" wday="5" ></td>
                	    <td style="" clock="18"  check_clock="" wday="5" ></td>
                	    <td style="" clock="19"  check_clock="" wday="5" ></td>
	                    <td style="" clock="20"  check_clock="" wday="5" ></td>
                	    <td style="" clock="21"  check_clock="" wday="5" ></td>
                	    <td style="" clock="22"  check_clock="" wday="5" ></td>
                	    <td style="" clock="23"  check_clock="" wday="5" ></td>
                    </tr>
	                <tr id="wday6">
                        <td onclick="OpenWeek(6)">შაბ</td>
                	    <td style="" clock="0"  check_clock="" wday="6" ></td>
                	    <td style="" clock="1"  check_clock="" wday="6" ></td>
                	    <td style="" clock="2"  check_clock="" wday="6" ></td>
                	    <td style="" clock="3"  check_clock="" wday="6" ></td>
                	    <td style="" clock="4"  check_clock="" wday="6" ></td>
                	    <td style="" clock="5"  check_clock="" wday="6" ></td>
                	    <td style="" clock="6"  check_clock="" wday="6" ></td>
                	    <td style="" clock="7"  check_clock="" wday="6" ></td>
                	    <td style="" clock="8"  check_clock="" wday="6" ></td>
                	    <td style="" clock="9"  check_clock="" wday="6" ></td>
                	    <td style="" clock="10"  check_clock="" wday="6" ></td>
                	    <td style="" clock="11"  check_clock="" wday="6" ></td>
                	    <td style="" clock="12"  check_clock="" wday="6" ></td>
                	    <td style="" clock="13"  check_clock="" wday="6" ></td>
                	    <td style="" clock="14"  check_clock="" wday="6" ></td>
	                    <td style="" clock="15"  check_clock="" wday="6" ></td>
                	    <td style="" clock="16"  check_clock="" wday="6" ></td>
                	    <td style="" clock="17"  check_clock="" wday="6" ></td>
                	    <td style="" clock="18"  check_clock="" wday="6" ></td>
                	    <td style="" clock="19"  check_clock="" wday="6" ></td>
	                    <td style="" clock="20"  check_clock="" wday="6" ></td>
                	    <td style="" clock="21"  check_clock="" wday="6" ></td>
                	    <td style="" clock="22"  check_clock="" wday="6" ></td>
                	    <td style="" clock="23"  check_clock="" wday="6" ></td>
                    </tr>
	                <tr id="wday7">
                        <td onclick="OpenWeek(7)">კვი</td>
                	    <td style="" clock="0"  check_clock="" wday="7" ></td>
                	    <td style="" clock="1"  check_clock="" wday="7" ></td>
                	    <td style="" clock="2"  check_clock="" wday="7" ></td>
                	    <td style="" clock="3"  check_clock="" wday="7" ></td>
                	    <td style="" clock="4"  check_clock="" wday="7" ></td>
                	    <td style="" clock="5"  check_clock="" wday="7" ></td>
                	    <td style="" clock="6"  check_clock="" wday="7" ></td>
                	    <td style="" clock="7"  check_clock="" wday="7" ></td>
                	    <td style="" clock="8"  check_clock="" wday="7" ></td>
                	    <td style="" clock="9"  check_clock="" wday="7" ></td>
                	    <td style="" clock="10"  check_clock="" wday="7" ></td>
                	    <td style="" clock="11"  check_clock="" wday="7" ></td>
                	    <td style="" clock="12"  check_clock="" wday="7" ></td>
                	    <td style="" clock="13"  check_clock="" wday="7" ></td>
                	    <td style="" clock="14"  check_clock="" wday="7" ></td>
	                    <td style="" clock="15"  check_clock="" wday="7" ></td>
                	    <td style="" clock="16"  check_clock="" wday="7" ></td>
                	    <td style="" clock="17"  check_clock="" wday="7" ></td>
                	    <td style="" clock="18"  check_clock="" wday="7" ></td>
                	    <td style="" clock="19"  check_clock="" wday="7" ></td>
	                    <td style="" clock="20"  check_clock="" wday="7" ></td>
                	    <td style="" clock="21"  check_clock="" wday="7" ></td>
                	    <td style="" clock="22"  check_clock="" wday="7" ></td>
                	    <td style="" clock="23"  check_clock="" wday="7" ></td>
                    </tr>
	            </table>
	            <table class="dialog-form-table">
                    <tr>
                       <td style="width: 210px;"><label for="queue_scenar">საანგარიშო პერიოდი</label></td>
	                   <td></td>
                    </tr>
    	            <tr>
                       <td><input style="width: 150px; float: left;" id="start_date_holi" type="text"><span style="margin-top: 5px;float: left;">-დან</span></td>
	                   <td><input style="width: 150px; float: left;" id="end_date_holi" type="text"><span style="margin-top: 5px;float: left;">-მდე</span></td>
                    </tr>
	            </table>
	            <table class="dialog-form-table">
                    <tr>
	                   <td><input id="holiday_all" type="checkbox"></td>
                       <td style="width: ;"><label for="holiday_id">დღესასწაულები</label></td>
                	   <td style="width: ;"><select id="holiday_id" style="width:253px;">'.GetHoliday().'</select></td>
	                   <td style="width: ;"><button id="add_holiday">დამატება</button></td>
                	   <td style="width: ;"><button id="delete_holiday">წაშლა</button></td>
                    </tr>
	            </table>
                <table class="display" id="table_holiday" >
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 29%;">თარიღი</th>
                            <th style="width: 40%;;">სახელი</th>
                            <th style="width: 29%;">კატეგორია</th>
							<th style="width: 30px;" class="check">&nbsp;</th>
						</tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                        	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                            	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                            </th>
							<th>
				            	<div class="callapp_checkbox">
				                    <input type="checkbox" id="check-all-holiday" name="check-all" />
				                    <label style="margin-top: 3px;" for="check-all-holiday"></label>
				                </div>
				            </th>
						</tr>
                    </thead>
                </table>
            </fieldset>
    	</div>
	</div>
	</div>
	<input type="hidden" value="'.$res[id].'" id="project_hidden_id">
	<input type="hidden" value="'.$incr_id.'" id="hidden_project_id">';

	return $data;
}

function GetDialogWeek($cycle,$project_id){
    $data = '<div id="dialog-form">
        	    <fieldset>
        	       <legend>ძირითადი ინფორმაცია</legend>
                    <input id="hidde_cycle" type="hidden" value="'.$cycle.'">
                    <div id="button_area">
                        <button id="add_week">დამატება</button>
                        <button id="delete_week">წაშლა</button>
                    </div>
    				<table class="display" id="table_week" style="width: 100%;">
                        <thead>
                            <tr id="datatable_header">
                                <th>ID</th>
                                <th style="width: 6%;">№</th>
                                <th style="width: 12%;">სამუშაო დღე</th>
                                <th style="width: 12%;">დასაწყისი</th>
                                <th style="width: 12%;">დასასრული</th>
                                <th style="width: 12%;">სთ</th>
                                <th style="width: 14%;">სამუშაოს ტიპი</th>
                                <th style="width: 14%;">ენა</th>
                                <th style="width: 14%;">ინფ. წყარო</th>
    							<th style="width: 40px;" class="check">&nbsp;</th>
    						</tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                            	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input style="width: 69px;" type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input style="width: 69px;" type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input style="width: 69px;" type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input style="width: 69px;" type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input style="width: 69px;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input style="width: 69px;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input style="width: 69px;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                    <input style="width: 69px;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                </th>
    							<th>
    				            	<div class="callapp_checkbox">
    				                    <input type="checkbox" id="check-all-import-actived" name="check-all" />
    				                    <label style="margin-top: 3px;" for="check-all-import-actived"></label>
    				                </div>
    				            </th>
    						</tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th id="qnt">სულ:</th>
                            <th id="qnt">&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                    </table>
                </fieldset>
             </div>
            ';
    return $data;
}
function GetDialogCikle($project_id){
    $data = '
         <div id="dialog-form">
        	    <fieldset>
        	       <legend>ძირითადი ინფორმაცია</legend>
                    <div id="button_area">
                        <button id="add_weeks">დამატება</button>
                        <button id="delete_weeks">წაშლა</button>
                    </div>
    				<table class="display" id="table_cikle" style="width: 100%;">
                        <thead>
                            <tr id="datatable_header">
                                <th>ID</th>
                                <th style="width: 50%;">სატელეფონო სადგური</th>
                                <th style="width: 40%;">სთ/კვირა</th>
                                <th style="width: 40%;">სთ/თვე</th>
                                <th style="width: 40%;">საჭირო ოპ. თვე</th>
    							<th style="width: 10%;" class="check">&nbsp;</th>
    						</tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                            	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
    							<th>
    				            	<div class="callapp_checkbox">
    				                    <input type="checkbox" id="check-all-cikle" name="check-all" />
    				                    <label style="margin-top: 3px;" for="check-all-cikle"></label>
    				                </div>
    				            </th>
    						</tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th id="qnt">სულ:</th>
                            <th id="qnt">&nbsp;</th>
                            <th id="qnt">&nbsp;</th>
                            <th id="qnt">&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                    </table>
                </fieldset>
             </div>';
    return $data;
}
function GetDialogWeekAdd($week_id,$project_id,$get_weekADD_id){
    $start = '';
    $end = '';
    $ext = 1;
    $type = 1;
    $req = mysql_fetch_assoc(mysql_query("SELECT 	id+1 AS `inc`
                                          FROM 	week_day_graphic
                                            ORDER BY id DESC LIMIT 1"));
    $id = $req[inc];
    if($req[inc] == ''){
        $id = 1;
    }
    if($get_weekADD_id != 0){
        $res = mysql_fetch_assoc(mysql_query("  SELECT 	id,start_time,
                                        				end_time,
                                        				ext_number,
                                                        week_day_id,
                                        				type
                                                FROM 	week_day_graphic
                                                WHERE 	id = $get_weekADD_id"));
        $id      = $res[id];
    }
    $start   = $res[start_time];
    $end     = $res[end_time];
    $ext     = $res[ext_number];
    $w_d_id  = $res[week_day_id];
    $type    = $res[type];
    $select1 = '';
    $select2 = '';
    if($type == 1){
        $select1 = "selected";
    }elseif($type == 2){
        $select2 = "selected";
    }
    $data = '<div id="dialog-form">
        	    <fieldset>
        	       <legend>ძირითადი ინფორმაცია</legend>
                    <table class="dialog-form-table">
                        <tr>
                           <td ><button id="addlang">სასაუბრო ენა</button></td>
                           <td ><button id="addinfosorce">ინფორმაციის წყარო</button></td>
                        </tr>
	               </table>
                   <table class="dialog-form-table">
                    <tr>
                       <td style="width: 150px;">სამუშაო<br>დღე</td>
                       <td style="width: 99px;">სამუშაო<br>იწყება</td>
                       <td style="width: 99px;">სამუშაო<br>მთავრდება</td>
                       <td style="width: 150px;">სამუშაოს<br>ტიპი</td>
                       <td style="width: 99px;">24 სთ</td>
                    </tr>
                    <tr>
                        <td><select style="width: 130px;" id="week_day_id" class="idls object">'.GetDay($w_d_id).'</select></td>
                        <td><input id="start_time" type="text" style="width: 60px;" value="'.$start.'"></td>
                        <td><input id="end_time" type="text" style="width: 60px;" value="'.$end.'"></td>
                        <td><select id="type"><option value="1" '.$select1.'>სამუშაო სთ.</option><option value="2" '.$select2.'>არა სამუშაო სთ.</option></select></td>
                        <td><input type="checkbox" id="24st"></td>
                    </tr>
	              </table>
                            <input type="hidden" value="'.$id.'" id="week_day_graphic_id">
                </fieldset>
             </div>
            ';
    return $data;
}

function GetDialogLangAdd($week_id,$project_id){
    $data = '<div id="dialog-form">
        	    <fieldset>
        	       <legend>ძირითადი ინფორმაცია</legend>
                    <table class="dialog-form-table">
                        <tr>
                           <td>სასაუბრო ენა</td>
                        </tr>
                        <tr>
                           <td><select id="spoken_lang_id">'.GetLang().'</select></td>
                        </tr>
	               </table>
                    <div id="button_area">
                        <button id="add_lang">დამატება</button>
                        <button id="delete_lang">წაშლა</button>
                    </div>
    				<table class="display" id="table_lang" style="width: 100%;">
                        <thead>
                            <tr id="datatable_header">
                                <th>ID</th>
                                <th style="width: 90%;">სასაუბრო ენა</th>
    							<th style="width: 10%;" class="check">&nbsp;</th>
    						</tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                            	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
    							<th>
    				            	<div class="callapp_checkbox">
    				                    <input type="checkbox" id="check-all-lang" name="check-all" />
    				                    <label style="margin-top: 3px;" for="check-all-lang"></label>
    				                </div>
    				            </th>
    						</tr>
                        </thead>
                    </table>
                </fieldset>
             </div>
            ';
    return $data;
}

function GetDialogInfoSorceAdd($week_id,$project_id){
    $data = '<div id="dialog-form">
        	    <fieldset>
        	       <legend>ძირითადი ინფორმაცია</legend>
                    <table class="dialog-form-table">
                        <tr>
                           <td>ინფორმაციის წყარო</td>
                        </tr>
                        <tr>
                           <td><select id="information_source_id">'.GetInfoSource().'</select></td>
                        </tr>
	               </table>
                   <div id="button_area">
                        <button id="add_infosorce">დამატება</button>
                        <button id="delete_infosorce">წაშლა</button>
                   </div>
                   <table class="display" id="table_infosorce" style="width: 100%;">
                        <thead>
                            <tr id="datatable_header">
                                <th>ID</th>
                                <th style="width: 90%;">ინფორმაციის წყარო</th>
    							<th style="width: 10%;" class="check">&nbsp;</th>
    						</tr>
                        </thead>
                        <thead>
                            <tr class="search_header">
                                <th class="colum_hidden">
                            	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                </th>
                                <th>
                                	<input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                </th>
    							<th>
    				            	<div class="callapp_checkbox">
    				                    <input type="checkbox" id="check-all-infosorce" name="check-all" />
    				                    <label style="margin-top: 3px;" for="check-all-infosorce"></label>
    				                </div>
    				            </th>
    						</tr>
                        </thead>
                    </table>
                </fieldset>
             </div>
            ';
    return $data;
}

function increment($table){

    $result   		= mysql_query("SHOW TABLE STATUS LIKE '$table'");
    $row   			= mysql_fetch_array($result);
    $increment   	= $row['Auto_increment'];
    $next_increment = $increment+1;
    mysql_query("ALTER TABLE '$table' AUTO_INCREMENT=$next_increment");

    return $increment;
}

// get service level list
function get_sl_list($sl_id) {
	GLOBAL $db;
    $options = "";
    $db->setQuery("SELECT id, name FROM sl_type WHERE actived = 1");
	$query = $db->getResultArray();
    foreach($query['result'] AS $option) {
				if($option['id'] == $sl_id) {
					$options .= "<option value='".$option['id']."' selected>".$option['name']."</option>";
				} else {
					$options .= "<option value='".$option['id']."'>".$option['name']."</option>";
				}
    }

    return $options;

}

// adding new project dialog form
function new_project_edit_form($project_id) {
    $user_id = $_SESSION['USERID'];
    
    GLOBAL $db;
    
    $db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
    
    $check_group = $db->getResultArray();
    $group_id = $check_group[result][0][group_id];
    
	return  '
	<!-- top bar -->
    <input id="hidde_session_goup_id" type="hidden" value="'.$group_id.'">
	<div class="project-details-topbar">
		<p>პროექტი: <span id="editProjectNameReceiver"></span></p>
		<button id="closeEditProjectDialog">
			<i class="fa fa-times"></i>
		</buttons>
	</div>
	<!-- top bar -->

	<!-- content -->
	<div class="project-details-content">

		<!-- menu -->
		<div class="project-details-menu">
			<div class="pdm-left">
				<ul>
					<li id="openEditProjectDetailsForm" class="">
						<div>
							<i class="fa fa-info-circle"></i>
							<span>დეტალები</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="pdm-right">
				<ul>
					<li class="project-data-loader" data-type="holidays">
						<div>
							<i class="fa fa-coffee"></i>
							<span>დასვენების დღეები</span>
						</div>
					</li>
					<li class="project-data-loader" data-type="rushHours">
						<div>
							<i class="fa fa-hourglass-half"></i>
							<span>პიკის საათები</span>
						</div>
					</li>
					<li class="project-data-loader" data-type="workHours">
						<div>
							<i class="fa fa-clock"></i>
							<span>სამუშაო საათები</span>
						</div>
					<li class="project-data-loader active" data-type="graphic">
						<div>
							<i class="fa fa-calendar-alt"></i>
							<span>გრაფიკი</span>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<!-- data wrapper -->
		<div class="project-details-data-wrapper">
			<div class="project-details-editor">
				<div class="project-details-editor-content">
					'.new_project_container_structure(get_project_fast_data($project_id)).'
				</div>
				<div class="project-details-editor-footer">
					<button id="confirmProjectEditing">
						განახლება
					</button>
					<button style="float:right;" id="cancelProjectEditing">
						დახურვა
					</button>
				</div>
			</div>

			<div class="project-details-data-receiver">
				'.get_graphic_schema_dialog($project_id).'
			</div>

			<div class="project-details-loading-container">
				<div>
					<i class="fa fa-cog"></i>
					<p>მიმდინარეობს მონაცემთა დამუშავება</p>
				</div>
			</div>

		</div>
		<!-- data wrapper -->

	</div>
	<!-- content -->';

}

// get project details day graphic structure
function get_project_details_day_graphic_structure() {

	return '<div class="wg-dg-header">
				<div class="wg-dg-date-holder">
					<span>თარიღი:</span>
					<br/>
					<span class="date-receiver">01.01.2018</span>
				</div>
				<div></div>
				<div class="wg-dg-controls-holder">
					<button class="wg-dg-table-view-changer active" title="მონაცემთა ჩვენება გრაფიკის სახით" data-type="graphic">
						<i class="material-icons">query_builder</i>
					</button>
					<button class="wg-dg-table-view-changer" title="მონაცემთა ჩვენება ცხრილის სახით" data-type="table">
						<i class="material-icons">grid_on</i>
					</button>
				</div>
				<div class="wg-dg-dialog-closer">
					<button id="wgDgDialogCloser" title="დღის ჭრილიდან გასვლა">
						<i class="material-icons">backspace</i>
					</button>
				</div>
			</div>

			<div class="wg-dg-body">
				<div class="part operators">
					<div class="content">
						<div class="head">
							<div class="col">
								<span>No:</span>
							</div>
							<div class="col flex-width">
								<span>ოპერატორი</span>
							</div>
						</div>
						<div class="body">

						</div>
					</div>
					<div class="scroll">

					</div>
					<div class="footer">
						<div class="row is-out">
							<div class="col">
								<span>გასულია</span>
								<span id="wgDgIsOutHour"></span>
							</div>
						</div>
						<div class="row on-place">
							<div class="col">
								<span>ადგილზეა</span>
								<span id="wgDgOnPlaceHour"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="part value-table">
					<div class="content">
						<div class="slider">
							<div class="head">
									'.table_in_24_hour_head_structure().'
							</div>
							<div class="body">

							</div>
						</div>
					</div>
					<div class="scroll">
						<div class="maker"></div>
					</div>
					<div class="footer">
						<div class="slider">
							<div class="row status">
								'.wg_dg_value_table_footcol_structure("isOut").'
							</div>
							<div class="row on-place">
								'.wg_dg_value_table_footcol_structure("onPlace").'
							</div>
						</div>
					</div>
				</div>
				<div class="part hours-summary">
					<div class="content">
						<div class="head">
							<div class="col">
								<span>სამუშაო <br/> სთ</span>
							</div>
							<div class="col flex-width">
								<span>დასვენება <br/> სთ</span>
							</div>
							<div class="col">
								<span> <br/> სთ/თვე</span>
							</div>
						</div>
						<div class="body">

						</div>
					</div>
					<div class="scroll">

					</div>
					<div class="footer">
						<div class="row">

						</div>
						<div class="row">

						</div>
					</div>
				</div>
			</div>

			<div class="wg-dg-footer">
				
				<ul class="color-description-list">
					<label for="statusDescriber">გრაფიკის აღმნიშვნელი:</label>
					<li>
						<span data-type="work-hours">სამუშაო საათი</span>
					</li>
					<li>
						<span data-type="rush-hours">პიკის საათი</span>
					</li>
				</ul>

				<ul class="status-description-list" id="statusDescriber">
					<label for="statusDescriber">ადგილზე ყოფნის სტატუსი:</label>
					<li data-status="all-in">
						<span>ყველა ადგილზეა</span>
					</li>
					<li data-status="more-in">
						<span>ადგილზე უფრო მეტია</span>
					</li>
					<li data-status="in-out">
						<span>გასული და ადგილზე თანაბარია</span>
					</li>
					<li data-status="more-out">
						<span>გასულია უფრო მეტი</span>
					</li>
					<li data-status="all-out">
						<span>ყველა გასულია</span>
					</li>
				</ul>

			</div>';
}

// get project day graphic column data
function get_project_details_day_graphic_col_data($project_id, $year, $month, $col_id) {
    global $db;
	// define variables
	$data_collector = array();
	$row_counter = 1;

	// get work graphic data
	$work_graphic_data = get_work_graphic_data_by_details($project_id, $year, $month);
	
	// check graphic data count
	if($work_graphic_data["count"] > 0) {

		// define graphic id
		$work_graphic_id = $work_graphic_data["id"];

		// select graphic rows
		$db->setQuery("SELECT 	work_graphic_rows.id,
    								        if(ISNULL(user_info.name), 'მონიშნული არ არის', user_info.name) AS service_manager
    								FROM    `work_graphic_rows`
    								LEFT JOIN users ON users.id = work_graphic_rows.operator_id
    								LEFT JOIN user_info ON user_info.user_id = users.id
    								WHERE work_graphic_rows.work_graphic_id = '$work_graphic_id'
    								AND work_graphic_rows.actived = 1");
		$row_query = $db->getResultArray();
		// loop throw rows
		foreach ($row_query[result] AS $row_res) {

			// define row id
			$row_id = $row_res["id"];

			// define shift data
			$shift_data = get_work_graphic_col_data($row_id, $col_id);

			// define activities
			$activities = get_work_graphic_col_activities($row_id, $col_id);

			// define date for get week number
			$selected_date = $year."-".zeroFixer($month)."-".zeroFixer($col_id);
			$exploded_date = explode("-", $selected_date);
			$formated_date = mktime(0, 0, 0, $exploded_date[1], $exploded_date[2], $exploded_date[0]);
			$week_number   = (int)date('W', $formated_date);

			// define rush hours data
			$rush_hours = get_work_graphic_col_rush_hours($project_id, $year, $week_number - 1);
		
			// check for shift data
			if(count($shift_data) > 0) {

				// define row data
				$row_data = array(
					"rowId" => $row_id,
					"num" => $row_counter,
					"operator" => $row_res["service_manager"],
					"shiftData" => $shift_data,
					"activities" => $activities,
					"rushHours" => $rush_hours
				);

				// pushh row data in data collector array
				array_push($data_collector, $row_data);
			}

			// increase row counter
			$row_counter++;
		}

	}

	// return data collector
	return $data_collector;
	

}

// get project fast data
function get_project_fast_data($project_id) {
	GLOBAL $db;
	$db->setQuery("SELECT project.id,
															project.name,
															project.max_hour_per_month,
															DATE_FORMAT(project.work_shift_max_hour,'%H:%i') AS work_shift_max_hour,
		 													project.sl_type_id,
															project.sl_percent,
															project.sl_second,
															project.sl_missed_from_second
												FROM project
												WHERE project.id = '$project_id'");

	$res = $db->getResultArray();
	return $res['result'][0];

}

// get has holiday status
function has_holiday_status($project_id) {

	$query = mysql_query("SELECT id FROM project_holiday WHERE project_id = '$project_id' AND actived = 1");
	$data_count = mysql_num_rows($query);

	if($data_count > 0) {
		return true;
	} else {
		return false;
	}

}

// returns new project form container structure
function new_project_container_structure($data = false) {

	return ($data == false ? '<div class="project-details-form-row">
				<button id="copyProjectDetails">
					დააკოპირე მონაცემები
				</button>
			</div>' : '').
			'<div class="project-details-form-row">
				<label for="newProjectName">პროექტის დასახელება</label>
				<input type="text" id="newProjectName" value="'.$data['name'].'">
			</div>
			<div class="project-details-form-row">
				<div class="project-details-title-holder">
					<h1>SL (მომსახურების დონე)</h1>
				</div>
				<div class="project-details-content-holder">
					<label for="newProjectSlTypeList">აირჩიეთ SL ტიპი</label>
					<select class="jquery-ui-chosen" name="new_project_sl_type_list" id="newProjectSlTypeList">
						'.get_sl_list($data['sl_type_id']).'
					</select>
					<ul class="project-details-ul pdu-sl">
						<li>
							<label for="npslCallPercent">ზარების</label>
							<input type="text" id="npslCallPercent" value="'.$data['sl_percent'].'">
							<span>%</span>
						</li>
						<li>
							<label for="npslCallAnswered">ნაპასუხები</label>
							<input type="text" id="npslCallAnswered" value="'.$data['sl_second'].'">
							<span>წმ-ში</span>
						</li>
						<li class="sl-partial sl-partial-hidden">
							<label for="npslCallUnanswered">უპასუხო</label>
							<input type="text" id="npslCallUnanswered" value="'.$data['sl_missed_from_second'].'">
							<span>წმ-დან</span>
						</li>
					</ul>
				</div>
			</div>
			<div class="project-details-form-row">
				<div class="project-details-title-holder">
					<h1>სამუშაო საათები</h1>
				</div>
				<div class="project-details-content-holder">
				<ul class="project-details-ul pdu-work-hours">
						<li>
							<label for="workHoursOperator">სამუშაო საათები 1ოპ:</label>
							<input type="text" id="workHoursOperator" value="'.$data['max_hour_per_month'].'">
							<span>სთ/კვირა</span>
						</li>
						<li>
							<label for="workHoursDuration">1 ცვლის მაქსიმალ. ხ-ბა:</label>
							<input type="text" id="workHoursDuration" value="'.$data['work_shift_max_hour'].'">
							<span>სთ</span>
						</li>
					</ul>
				</div>
			</div>';

}

//returns operating hours table days structure
function op_hours_days_list() {

	$days_array = array("ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა");
	$structure = "";

	foreach ($days_array as $index => $day) {

		$structure .= '<tr class="hours-row" data-id="'.$index.'">
											<td><span class="hours-individual-day">'.$day.'</span></td>
											<td><h6 class"hours-per-week-receiver">0</h6></td>
											<td><h6 class"hours-per-month-receiver">0</h6></td>
									</tr>';
	}

	return $structure;

}


// return new project opereting hours structure
function new_project_operating_hours_structure() {

    return '<section class="wfm-section operating-hours">
                <div class="wfm-section-title-holder">
                    <h1>სამუშაო საათები</h1>
                </div>
                <div class="wfm-section-controls-holder">
                    <button id="addNewHours">
                        <div class="wfm-scb-icon-holder">
                            <i class="material-icons">add</i>
                        </div>
                        <span class="wfm-scb-text-holder">
                            დამატება
                        </span>
                    </button>
                </div>
                <div class="wfm-work-table-wrapper">
                    <div class="wfm-work-table-side wfm-work-table-week">
                        <table class="wfm-work-table-head">
                            <thead>
                                <tr>
                                    <th><span>დღე</span></th>
                                    <th><span>სთ <br> კვირა</span></th>
                                    <th><span>სთ <br>4 კვირა</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                '.op_hours_days_list().'
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td class="hours-per-week-sum-receiver" data-hours="0">0</td>
                                    <td class="hours-per-month-sum-receiver" data-hours="0">0</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="wfm-work-table-side wfm-work-table-holder">
                        <div class="wfm-work-table-container">
                            <div class="wfm-wt-body">
                                <table class="dialog-form-table" id="work_table">
                                    <thead>
                                        <tr>
                                            <th colspan="4">00:00</th>
                                            <th colspan="4">01:00</th>
                                            <th colspan="4">02:00</th>
                                            <th colspan="4">03:00</th>
                                            <th colspan="4">04:00</th>
                                            <th colspan="4">05:00</th>
                                            <th colspan="4">06:00</th>
                                            <th colspan="4">07:00</th>
                                            <th colspan="4">08:00</th>
                                            <th colspan="4">09:00</th>
                                            <th colspan="4">10:00</th>
                                            <th colspan="4">11:00</th>
                                            <th colspan="4">12:00</th>
                                            <th colspan="4">13:00</th>
                                            <th colspan="4">14:00</th>
                                            <th colspan="4">15:00</th>
                                            <th colspan="4">16:00</th>
                                            <th colspan="4">17:00</th>
                                            <th colspan="4">18:00</th>
                                            <th colspan="4">19:00</th>
                                            <th colspan="4">20:00</th>
                                            <th colspan="4">21:00</th>
                                            <th colspan="4">22:00</th>
                                            <th colspan="4">23:00</th>
                                        </tr>
                                        <tr>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                            <th>00</th>
                                            <th>15</th>
                                            <th>30</th>
                                            <th>45</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            '.work_schedule_loop(1).'
                                        </tr>
                                        <tr>
                                            '.work_schedule_loop(2).'
                                        </tr>
                                        <tr>
                                            '.work_schedule_loop(3).'
                                        </tr>
                                        <tr>
                                            '.work_schedule_loop(4).'
                                        </tr>
                                        <tr>
                                            '.work_schedule_loop(5).'
                                        </tr>
                                        <tr>
                                            '.work_schedule_loop(6).'
                                        </tr>
                                        <tr>
                                            '.work_schedule_loop(7).'
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="wfm-wt-footer">
                                <div class="wfm-wt-scroll-width"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>';

}

// project planner day names list
function pp_day_names_list() {

	$days_array = array("ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა");
	$struture_collector = "";

	for($i = 0; $i < 28; $i++) {

		$days_index = $i % count($days_array);
		$day = $days_array[$days_index];

		if($days_index == 5 || $days_index == 6) {
			$weekend = "wfm-ptc-col-weekeand";
		} else {
			$weekend = "";
		}

		$struture_collector .= '<th>
																<div class="wfm-ptc-col-title '.$weekend.'">
																		<span>'.$day.'</span>
																		<span>'.($i + 1).'</span>
																</div>
														</th>';

	}

	return $struture_collector;

}

// project planner operator list
function pp_operators_list($project_id, $op_count) {
	GLOBAL $db;
	// define structure collector
	$struture_collector = "";
	$op_count = $op_count == 0 ? 1 : $op_count;

	// loop throw operator rows
	for($i = 0; $i < $op_count; $i++) {

		$db->setQuery("	SELECT 	wfm.id,
								wfm.cycle_id
						FROM 	wfm_planner wfm
						WHERE 	wfm.project_id = '$project_id' AND wfm.operator_num = '$i' AND wfm.actived=1");

		$r_cycle = $db->getResultArray();
		$r_cycle = $r_cycle['result'][0]["cycle_id"];

		$struture_collector .= '<tr>
															<td class="wfm-pth-col-min wfm-pth-col-name">
																 <span>ოპერატორი '.($i + 1).'</span>
															</td>
															<td class="wfm-pth-col-min wfm-pth-col-select">
																 <select class="wfm-op-choose-cycle" data-row-id="'.$i.'">
																			'.pp_choose_cycle_list($r_cycle).'
																 </select>
															</td>
															<td class="wfm-pth-col-min wfm-pth-col-button">
															'.($i > 0 ? '<button class="delete-operator" data-id="'.$i.'">
																							წაშლა
																					</button>' : null).'
															</td>
															<td class="wfm-pth-col-min wfm-pth-col-button">
																<button class="clear-planner-row" data-id="'.$i.'">
																	გასუფთავება
																</button>
															</td>
													</tr>';

	}

	return $struture_collector;

}

// project planner cycle values list
function pp_cycle_values_list($project_id, $op_count) {
	GLOBAL $db;
	// check for record count
	if($op_count > 0) {

			// define data collector
			$struture_collector = "";
			$saved_for_next_day = false;
			$saved_for_next_timeout = false;

			// loop throw operator rows
			for($i = 0; $i < $op_count; $i++) {

				$db->setQuery("	SELECT wfm.id,
														ws.id AS shift_id,
														ws.name AS shift_name,
														ws.color AS shift_color,
														TIME_FORMAT(ws.start_date, '%H:%i') AS start_time,
														TIME_FORMAT(ws.end_date, '%H:%i') AS end_time,
														TIME_FORMAT(ws.timeout, '%H:%i') AS timeout
										FROM wfm_planner wfm
										RIGHT JOIN work_graphic_planned wgp ON wgp.wfm_planner_id = wfm.id
										LEFT JOIN work_shift ws ON ws.id = wgp.work_shift_id
										WHERE wfm.project_id = '$project_id' AND wfm.operator_num = '$i' AND wfm.actived=1");

				$struture_collector .= "<tr>";
				
				$query = $db->getResultArray();
				
				$q_row_count = $query['count'];
				$additional_cols = 28 - $q_row_count;

				foreach($query['result'] AS $q_col) {

					$shift_difference = calculate_shift_difference($q_col["start_time"], $q_col["end_time"], $q_col["timeout"], $saved_for_next_day, $saved_for_next_timeout);
					$work_time = $shift_difference["current_day"];
					$saved_for_next_day = $shift_difference["next_day"];
					$shift_timeout = $shift_difference["cur_timeout"];
					$saved_for_next_timeout = $shift_difference["next_timeout"];

					$struture_collector .= '<td style="background:'.$q_col["shift_color"].';">
																			<div class="wfm-ptc-col">
																					<span data-difference="'.$work_time.'" data-shift-id="'.$q_col["shift_id"].'">'.$q_col["shift_name"].'</span>
																			</div>
																	</td>';

				}

				if($additional_cols > 0) {

					for($i = 0; $i < $additional_cols; $i++) {

						$struture_collector .= '<td>
													<div class="wfm-ptc-col">
															<span data-difference="00:00" data-shift-id="0"></span>
													</div>
												</td>';

					}

				}

				$struture_collector .= "</tr>";

			}

	} else {

		$struture_collector = "<tr>";

		for($i = 0; $i < 28; $i++) {

			$struture_collector .= '<td>
																	<div class="wfm-ptc-col">
																			<span data-difference="00:00" data-shift-id="0"></span>
																	</div>
															</td>';

		}

		$struture_collector .= "</tr>";

	}

	return $struture_collector;

}

// project planner add as cycle button list
function pp_add_cycle_button_list($op_count) {

	// define structure collector
	$struture_collector = "";
	$op_count = $op_count == 0 ? 1 : $op_count;

	// loop throw operator rows
	for($i = 0; $i < $op_count; $i++) {

		$struture_collector .= '<tr>
															<td>
																	<div class="wfm-ptc-col">
																			<span class="wfm-ptc-hours-sum week">00:00</span>
																	</div>
															</td>
															<td>
																	<div class="wfm-ptc-col">
																			<span class="wfm-ptc-hours-sum month">00:00</span>
																	</div>
															</td>
															<td>
																	<div class="wfm-ptc-col">
																			<button class="save-as-loop">
																					შეინახე ციკლად
																			</button>
																	</div>
															</td>
													</tr>';

	}

	return $struture_collector;

}

// project planner cycle calculate list
function pp_calculate_list($type) {

	$output = "00:00";
	$struture_collector = "";
	$additional_class = "";

	for($i = 0; $i < 28; $i++) {

		if($type == "difference") {
			$additional_class = "wfm-pht-exact-value";
		}

		$struture_collector .= '<div class="wfm-pht-col '.$additional_class.'">
																<span>'.$output.'</span>
														</div>';

	}

	return $struture_collector;

}

// project planner choose cycle list
function pp_choose_cycle_list($cycle_id) {
		GLOBAL $db;
		$structure_collector = "";

		$db->setQuery("SELECT id, name FROM work_cycle WHERE actived = 1 GROUP BY name ORDER BY id ASC");
		$query = $db->getResultArray();
		foreach($query['result'] AS $res) {

			if($cycle_id == $res["id"]) {
				$structure_collector .= '<option value="'.$res["id"].'" selected>'.$res["name"].'</option>';
			} else {
				$structure_collector .= '<option value="'.$res["id"].'">'.$res["name"].'</option>';
			}


		}

		$structure_collector .= "<option value='0' ".($cycle_id == '' || $cycle_id == 0 ? 'selected' : '').">აირჩიეთ ციკლი</option>";

		return $structure_collector;

}

// returns last added cycle item from project planner table
function pp_get_last_cycle() {

		$q = mysql_query("SELECT id, name FROM work_cycle WHERE actived = 1 GROUP BY name ORDER BY id DESC LIMIT 1");

		while($res = mysql_fetch_assoc($q)) {
			$structure_collector = '<option value="'.$res["id"].'">'.$res["name"].'</option>';
		}

		return $structure_collector;

}

// get work operator json data
function get_work_operator_json_data() {
		GLOBAL $db;
		$data = array();

		// $q = mysql_query("SELECT ic_is.id, 
		// 						 IF(river_side.name != '' , CONCAT(ic_is.name, ' (', river_side.name, ')'), ic_is.name) AS name
		// 					FROM incomming_call_info_service_manager ic_is
		// 					LEFT JOIN river_side ON ic_is.river_side_id = river_side.id && river_side.actived = 1
		// 					WHERE ic_is.actived = 1");
		$db->setQuery("	SELECT users.id, `user_info`.`name` AS `username` FROM users
						LEFT JOIN user_info ON user_info.user_id = users.id
						WHERE users.actived = 1");
		$query = $db->getResultArray();
		foreach($query['result'] AS $res) {

			$unit = array("id" => $res["id"], "name" => $res["username"]);
			array_push($data,$unit);

		}

		return $data;

}

// get work cycle json data
function get_work_cycle_json_data() {
		GLOBAL $db;
		$data = array();

		$db->setQuery("SELECT work_cycle.id, 
									work_cycle.name
							FROM work_cycle
							WHERE work_cycle.actived = 1 
							GROUP BY work_cycle.name");

		$query = $db->getResultArray();
		foreach($query['result'] AS $res) {

			$cycle_id = $res["id"];

			$db->setQuery("SELECT id
							FROM work_cycle_detail
							WHERE work_cycle_id = '$cycle_id'
								AND actived = 1
							GROUP BY num");

			$shift_count = $db->getNumRow();

			$unit = array("id" => $res["id"], "name" => $res["name"], "shiftCount" => $shift_count);

			array_push($data,$unit);

		}

		return $data;

}

// get work graphic operated hours data
function get_operated_hours_data($project_id) {
	GLOBAl $db;
	$data_collector = array();

	$db->setQuery("	SELECT 		week_day.id AS day_id,
								week_day.name AS day_name,
								TIME_FORMAT(TIMEDIFF(end_time,start_time),'%H:%i') AS day_hours
					FROM 		week_day_graphic
					LEFT JOIN 	week_day ON week_day.id = week_day_graphic.week_day_id
					WHERE 		week_day_graphic.actived = 1 AND project_id = '$project_id'");
	$query = $db->getResultArray();
 foreach($query['result'] AS $res) {

	 $data = array(
		 "id" => $res["day_id"],
		 "name" => $res["day_name"],
		 "hours" => $res["day_hours"]
	 );

	 array_push($data_collector, $data);

 }

 return $data_collector;

}

// get work graphic row controller data
function get_work_graphic_row_controller_data($project_id, $year, $month) {
    $user_id = $_SESSION['USERID'];
    
    
	GLOBAL $db;
	
	$db->setQuery("SELECT group_id FROM users WHERE id = '$user_id'");
	
	$check_group = $db->getResultArray();
	$group_id = $check_group[result][0][group_id];
	$data_collector = array();

	// get work graphic data
	$work_graphic_data = get_work_graphic_data_by_details($project_id, $year, $month);

	// check for data count
	if($work_graphic_data["count"] == 0) {

		$save_data = save_work_graphic_data($project_id, $year, $month);

		// check for successfull save of work graphic
		if($save_data["complete"] == 1) {

			$work_graphic_data = get_work_graphic_data_by_details($project_id, $year, $month);
			$work_graphic_id = $work_graphic_data["id"];
			
			$save_data = save_work_graphic_rows_data($work_graphic_id, 0, 0, 0);

		}

	} else {
		$work_graphic_data = get_work_graphic_data_by_details($project_id, $year, $month);
		$work_graphic_id = $work_graphic_data["id"];
	}

	$fillt_operator = '';
	
	if ($group_id == 34) {
	    $fillt_operator = "AND operator_id = $user_id";
	}
	
	$db->setQuery("	SELECT 	id,
							row_id,
							operator_id,
							cycle_id
					FROM 	work_graphic_rows
					WHERE 	work_graphic_id = '$work_graphic_id' AND actived = 1 $fillt_operator 
					ORDER BY row_id ASC");
	$query = $db->getResultArray();
	foreach($query['result'] AS $res_rows) {

		$row_id = $res_rows["id"];
		$cycle_id = $res_rows["cycle_id"];

		$data = array(
			"id" => $res_rows["id"],
			"rowId" => $res_rows["row_id"],
			"operatorId" => $res_rows["operator_id"],
			"cycleId" => $res_rows["cycle_id"],		 
			"shiftList" => get_shift_list_by_row_id($row_id),
			"activitieSeconds" => get_work_graphic_day_col_activitie_seconds($row_id),
			"editedColimns" => get_shift_edit_status($row_id)
		);

		array_push($data_collector, $data);

	}

 return $data_collector;

}

// get work graphic holidays data
function get_work_graphic_holidays_data($project_id, $year, $month) {
	GLOBAL $db;
	$data_collector = array();

	$db->setQuery("SELECT holidays.date - 1 AS col_id,
								 holidays.name	
						FROM project_holiday
						JOIN holidays ON holidays.id = project_holiday.holidays_id
						WHERE project_holiday.project_id = '$project_id'
							AND holidays.`year` IN (0, $year)
							AND holidays.`month` = '$month'
							AND project_holiday.actived = 1");
	$query = $db->getResultArray();
	foreach($query['result'] AS $res) {

		$data = array(
			"colId" => $res["col_id"],
			"name" => $res["name"]
		);

		array_push($data_collector, $data);

	};

	return $data_collector;

}

// get work planner row controller data
function get_work_planner_row_controller_data($project_id) {
    global $db;
	$data_collector = array();
	
	// get work planner data
	$work_planner_data = get_work_planner_data_by_project($project_id);

	// check for data count
	if($work_planner_data["count"] == 0) {

		$save_data = save_work_planner_data($project_id);

		// check for successfull save of work graphic
		if($save_data["complete"] == 1) {

			// get work planner data
			$work_planner_data = get_work_planner_data_by_project($project_id);
			$work_planner_id = $work_planner_data["id"];
			
			$save_data = save_work_planner_rows_data($work_planner_id, 0, 0, 0);

		}

	} else {

		// get work planner data
		$work_planner_data = get_work_planner_data_by_project($project_id);
		$work_planner_id = $work_planner_data["id"];

	}

	$db->setQuery("SELECT 	id,
									row_id,
									cycle_id,
									save_as_cycle_id
								FROM work_planner_rows
								WHERE work_planner_id = '$work_planner_id'
									AND actived = 1
									ORDER BY row_id ASC");
	$query_rows = $db->getResultArray();
	foreach($query_rows[result] AS $res_rows) {

		$row_id = $res_rows["id"];

		$data = array(
			"id" => $res_rows["id"],
			"rowId" => $res_rows["row_id"],
			"cycleId" => $res_rows["cycle_id"],
			"savedCycleId" => $res_rows["save_as_cycle_id"],
			"shiftList" => get_palnner_shift_list_by_row_id($row_id)
		);

		array_push($data_collector, $data);

	}

 	return $data_collector;

}

// returns processed shift times
function process_shift_times($shift) {

	// define shift times
	$start_time = $shift["start_date"];
	$end_time 	= $shift["end_date"];
	$timeout	= $shift["timeout"];
	$full_day	= "24:00:00";
	$day_start	= "00:00:00";

	// define time in seconds
	$full_day_in_seconds			= string_time_to_seconds($full_day);
	$shift_start_time_in_seconds 	= string_time_to_seconds($start_time);
	$shift_end_time_in_seconds 		= string_time_to_seconds($end_time);
	$shift_timeout_in_seconds 		= string_time_to_seconds($timeout);
	$day_start_in_seconds	 		= string_time_to_seconds($day_start);

	// check for start time, if it is greater then end time
	if($shift_start_time_in_seconds > $shift_end_time_in_seconds) {

		// define shift full time and work time
		$shift_full_time = $full_day_in_seconds - ($shift_start_time_in_seconds - $shift_end_time_in_seconds);
		$current_day_work_time = $full_day_in_seconds - $shift_start_time_in_seconds;

		// define percente of current day work time and timeout
		$current_day_work_time_percent = round($shift_full_time / $current_day_work_time);		
		$current_day_timeout_percent = round($shift_timeout_in_seconds / $current_day_work_time_percent);

		// define preresult times
		$current_day_timeout 	= $current_day_timeout_percent;
		$current_day_work_time 	= $current_day_work_time - $current_day_timeout;

		// regulated times (300 = 5 * 60 seconds) for regulate under 5 minute
		$regulated_cur_day_timeout 			= floor($current_day_timeout / 300)  * 300;
		$regulated_current_day_work_time 	= floor($current_day_work_time / 300)  * 300;

		// define times saved for next day
		$timeout_saved_for_next_day 	= $current_day_timeout - $regulated_cur_day_timeout;
		$work_time_saved_for_next_day 	= $current_day_work_time - $regulated_current_day_work_time;

		// define nex day times
		$next_day_timeout 		= $shift_timeout_in_seconds - $current_day_timeout_percent;
		$next_day_work_time 	= $shift_end_time_in_seconds - $next_day_timeout;

		// define result times
		$next_day_timeout 		= $next_day_timeout + $timeout_saved_for_next_day;
		$next_day_work_time 	= $next_day_work_time + $work_time_saved_for_next_day;
		$current_day_timeout	= $regulated_cur_day_timeout;
		$current_day_work_time 	= $regulated_current_day_work_time;

		// define work time start and end
		$cur_day_work_time_end = $full_day_in_seconds;
		$next_day_work_time_start = $day_start_in_seconds;
		$next_day_work_time_end = $shift_end_time_in_seconds;

	} else {

		// define shift full time and work time
		$shift_full_time = $shift_end_time_in_seconds - $shift_start_time_in_seconds;
		$shift_work_time = $shift_full_time - $shift_timeout_in_seconds;		

		// define result times
		$current_day_work_time 	= $shift_work_time;
		$current_day_timeout 	= $shift_timeout_in_seconds;
		$next_day_work_time 	= "00:00:00";
		$next_day_timeout 		= "00:00:00";

		// define work time start and end
		$cur_day_work_time_end 		= $shift_end_time_in_seconds;
		$next_day_work_time_start 	= 0;
		$next_day_work_time_end 	= 0;

	}

	// define result
	$result = array(
		"cur_day_work_time_start" => seconds_to_string_time($shift_start_time_in_seconds),
		"cur_day_work_time_end" => seconds_to_string_time($cur_day_work_time_end),
		"current_day_work_time" => seconds_to_string_time($current_day_work_time),
		"current_day_timeout" => seconds_to_string_time($current_day_timeout),
		"next_day_work_time_start" => seconds_to_string_time($next_day_work_time_start),
		"next_day_work_time_end" => seconds_to_string_time($next_day_work_time_end),
		"next_day_work_time" => seconds_to_string_time($next_day_work_time),
		"next_day_timeout" => seconds_to_string_time($next_day_timeout)
	);
	
	// retur result
	return $result;

}

// returns work graphic day column activitie precent by row id
function get_work_graphic_day_col_activitie_seconds($row_id) {
	GLOBAL $db;
	// define data collector
	$data_collector = array();

	// query the data
	$db->setQuery("SELECT work_graphic_activities.col_id,
							((work_graphic_activities.end - work_graphic_activities.start) + 1) * (5 * 60) AS activitie_seconds
						FROM work_graphic_activities
						WHERE work_graphic_activities.row_id = '$row_id'
						AND work_graphic_activities.actived = 1
						AND work_graphic_activities.activitie_id != 0");
	$query = $db->getResultArray();
	// summary seconds
	foreach($query['result'] AS $res) {

		// define data
		$data = array(
			"colId" => $res["col_id"],
			"seconds" => $res["activitie_seconds"]
		);

		array_push($data_collector, $data);
		
	}

	// return activitie seconds
	return $data_collector;

}

// returns shift list depended on work graphic rows id
function get_shift_list_by_row_id($row_id) {
	GLOBAl $db;
	$shift_list = array();

	$db->setQuery("SELECT 	work_graphic_cols.id,
									col_id,
									work_shift.id AS shift_id,
									work_shift.name AS shift_name,
									work_shift.color AS shift_color,
									work_shift.type,
									work_shift.start_date AS shift_start_date,
									work_shift.end_date AS shift_end_date,
									work_shift.timeout AS shift_timeout,
									SEC_TO_TIME(IF(TIMEDIFF(t1.end_date,t1.`start_date`) > '00:00:00', 

											 TIME_TO_SEC(t1.end_date) - TIME_TO_SEC(t1.`start_date`)  ,

											TIME_TO_SEC(t1.end_date) - TIME_TO_SEC(t1.`start_date`) + TIME_TO_SEC('24:00:00')

											) - TIME_TO_SEC(`t1`.`timeout`)) AS `time`,
									`t1`.`timeout`
								FROM work_graphic_cols
								JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
								LEFT JOIN `work_shift` AS t1 ON work_graphic_cols.work_shift_id = t1.`id` AND t1.type=1 
								WHERE work_graphic_rows_id = '$row_id'
									AND	work_graphic_cols.actived = 1
								ORDER BY col_id");

	$query = $db->getResultArray();
	foreach($query['result'] AS $res) {

		$shift_time = array(
			"start_date" 	=> $res["shift_start_date"],
			"end_date" 		=> $res["shift_end_date"],
			"timeout" 		=> $res["shift_timeout"]
		);

		   $processed_shift_times = process_shift_times($shift_time);
		// var_dump($processed_shift_times);

		$data_key = $res["col_id"];
		$data = array(
			"id" => $res["id"],
			"colId" => $res["col_id"],
			"shiftId" => $res["shift_id"],
			"shiftName" => $res["shift_name"],
			"shiftColor" => $res["shift_color"],
			"currentDayWorkTime" => $res['time'] == null ? "00:00" : $res['time'] ,
			"currentDayTimeout" =>  $res['timeout'] == null ? "00:00" : $res['timeout'],
			"nexDayWorkTime" => "0",
			"nexDayTimeout" => "0"
		);

		if(array_key_exists($data_key, $shift_list)) {
			array_push($shift_list[$data_key], $data);
		} else {
			$shift_list[$data_key] = array();
			array_push($shift_list[$data_key], $data);
		}

	}

	return $shift_list;

}

// returns activitie seconds depended on work graphic rows id
function get_column_activitie_seconds($row_id) {

	$shift_list = array();

	$query = mysql_query("SELECT 	work_graphic_cols.id,
									col_id,
									work_shift.id AS shift_id,
									work_shift.name AS shift_name,
									work_shift.color AS shift_color,
									work_shift.start_date AS shift_start_date,
									work_shift.end_date AS shift_end_date,
									work_shift.timeout AS shift_timeout
								FROM work_graphic_cols
								JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
								WHERE work_graphic_rows_id = '$row_id'
									AND	work_graphic_cols.actived = 1
								ORDER BY col_id");


	while($res = mysql_fetch_assoc($query)) {

		$shift_time = array(
			"start_date" 	=> $res["shift_start_date"],
			"end_date" 		=> $res["shift_end_date"],
			"timeout" 		=> $res["shift_timeout"]
		);
		
		$processed_shift_times = process_shift_times($shift_time);

		$data_key = $res["col_id"];
		$data = array(
			"id" => $res["id"],
			"colId" => $res["col_id"],
			"shiftId" => $res["shift_id"],
			"shiftName" => $res["shift_name"],
			"shiftColor" => $res["shift_color"],
			"currentDayWorkTime" => $processed_shift_times["current_day_work_time"],
			"currentDayTimeout" => $processed_shift_times["current_day_timeout"],
			"nexDayWorkTime" => $processed_shift_times["next_day_work_time"],
			"nexDayTimeout" => $processed_shift_times["next_day_timeout"]
		);

		if(array_key_exists($data_key, $shift_list)) {
			array_push($shift_list[$data_key], $data);
		} else {
			$shift_list[$data_key] = array();
			array_push($shift_list[$data_key], $data);
		}

	}

	return $shift_list;

}

// returns shift list depended on work planner rows id
function get_palnner_shift_list_by_row_id($row_id) {
    global $db;
	$shift_list = array();

	$db->setQuery("SELECT 	work_planner_cols.id,
									col_id,
									work_shift.id AS shift_id,
									work_shift.name AS shift_name,
									work_shift.color AS shift_color,
									work_shift.start_date AS shift_start_date,
									work_shift.end_date AS shift_end_date,
									work_shift.timeout AS shift_timeout
								FROM work_planner_cols
								JOIN work_shift ON work_shift.id = work_planner_cols.work_shift_id
								WHERE work_planner_rows_id = '$row_id'
									AND	work_planner_cols.actived = 1
								ORDER BY col_id");

	$query = $db->getResultArray();
	foreach($query[result] AS $res) {

		$shift_time = array(
			"start_date" 	=> $res["shift_start_date"],
			"end_date" 		=> $res["shift_end_date"],
			"timeout" 		=> $res["shift_timeout"]
		);
		
		$processed_shift_times = process_shift_times($shift_time);

		$data_key = $res["col_id"];
		$data = array(
			"id" => $res["id"],
			"colId" => $res["col_id"],
			"shiftId" => $res["shift_id"],
			"shiftName" => $res["shift_name"],
			"shiftColor" => $res["shift_color"],
			"currentDayWorkTime" => $processed_shift_times["current_day_work_time"],
			"currentDayTimeout" => $processed_shift_times["current_day_timeout"],
			"nexDayWorkTime" => $processed_shift_times["next_day_work_time"],
			"nexDayTimeout" => $processed_shift_times["next_day_timeout"]
		);

		if(array_key_exists($data_key, $shift_list)) {
			array_push($shift_list[$data_key], $data);
		} else {
			$shift_list[$data_key] = array();
			array_push($shift_list[$data_key], $data);
		}

	}

	return $shift_list;

}

// returns shift list depended on cycle id
function get_shift_list_by_cycle_id($cycle_id = 0) {

	$shift_list = array();

	$query = mysql_query("SELECT 	work_cycle.id AS cycle_id,
									work_cycle_detail.num AS col_num,
									work_shift.id AS shift_id,
									work_shift.name AS shift_name,
									work_shift.color AS shift_color,
									work_shift.start_date AS shift_start_date,
									work_shift.end_date AS shift_end_date,
									work_shift.timeout AS shift_timeout,
									CASE
										WHEN TIME_TO_SEC(work_shift.end_date) < TIME_TO_SEC(work_shift.start_date)
												THEN SEC_TO_TIME((TIME_TO_SEC('24:00:00') - TIME_TO_SEC(work_shift.start_date) + TIME_TO_SEC(work_shift.end_date)) - TIME_TO_SEC(work_shift.timeout))
										WHEN TIME_TO_SEC(work_shift.end_date) = TIME_TO_SEC(work_shift.start_date) AND TIME_TO_SEC(work_shift.start_date) != 0 AND TIME_TO_SEC(work_shift.end_date) != 0
												THEN SEC_TO_TIME(TIME_TO_SEC('24:00:00') - TIME_TO_SEC(work_shift.timeout))
										WHEN TIME_TO_SEC(work_shift.end_date) = TIME_TO_SEC(work_shift.start_date) AND TIME_TO_SEC(work_shift.start_date) = 0 AND TIME_TO_SEC(work_shift.end_date) = 0
												THEN 'weekend'
										WHEN TIME_TO_SEC(work_shift.end_date) > TIME_TO_SEC(work_shift.start_date)
												THEN SEC_TO_TIME((TIME_TO_SEC(work_shift.end_date) - TIME_TO_SEC(work_shift.start_date)) - TIME_TO_SEC(work_shift.timeout))
									END AS shift_work_hours
								FROM work_cycle
								JOIN work_cycle_detail ON work_cycle_detail.work_cycle_id = work_cycle.id
								JOIN work_shift ON work_shift.id = work_cycle_detail.work_shift_id
								WHERE work_cycle.id = '$cycle_id'
									AND	work_cycle.actived = 1
								ORDER BY col_num");


	while($res = mysql_fetch_assoc($query)) {

		$data_key = $res["col_num"];
		$data = array(
			"cycleId" => $res["cycle_id"],
			"colNum" => $res["col_num"],
			"shiftId" => $res["shift_id"],
			"shiftName" => $res["shift_name"],
			"shiftColor" => $res["shift_color"],
			"shiftStartDate" => $res["shift_start_date"],
			"shiftEndDate" => $res["shift_end_date"],
			"shiftTimeout" => $res["shift_timeout"],
			"shiftWorkHours" => $res["shift_work_hours"]
		);

		if(array_key_exists($data_key, $shift_list)) {
			array_push($shift_list[$data_key], $data);
		} else {
			$shift_list[$data_key] = array();
			array_push($shift_list[$data_key], $data);
		}

	}

	return $shift_list;

}

// returns shift list depended on work graphic planner id
function get_shift_list_by_planner_id($planner_id) {


	$query = mysql_query("SELECT work_graphic_planned.id,
															 work_shift.id AS shift_id,
															 work_shift.name AS shift_name,
															 work_shift.color AS shift_color,
															 DATE_FORMAT(work_shift.start_date,'%H:%i') AS shift_start,
															 DATE_FORMAT(work_shift.end_date,'%H:%i') AS shift_end,
															 DATE_FORMAT(work_shift.timeout,'%H:%i') AS shift_timeout
												FROM work_graphic_planned
												LEFT JOIN work_shift ON work_shift.id = work_graphic_planned.work_shift_id
												WHERE wfm_planner_id = '$planner_id'");


	$list = process_shift_list($query, $project_id, "planner");

	return $list;

}

// returns activities list depended on work graphic row id
function get_activities_by_graphic_row_id($project_id, $row_num, $date, $shift_id) {

	$activities = array();

	$query = mysql_query("SELECT  work_graphic_row_activities.id,
																work_graphic_row_activities.activitie_id AS activitie_id,
																work_graphic_row_activities.start AS activitie_start,
																work_graphic_row_activities.end AS activitie_end,
																work_activities.color AS activitie_color
															FROM work_graphic_row_activities
															LEFT JOIN work_activities ON work_activities.id = work_graphic_row_activities.activitie_id
															WHERE work_graphic_row_activities.project_id = '$project_id'
																AND work_graphic_row_activities.shift_id = '$shift_id'
																AND work_graphic_row_activities.row_num = '$row_num'
																AND work_graphic_row_activities.date = '$date'
																AND work_graphic_row_activities.actived = 1");

	while($res = mysql_fetch_assoc($query)) {

		$data_id = $res["id"];
		$activitie_id = $res["activitie_id"] ? $res["activitie_id"] : 0;
		$activitie_color = $res["activitie_color"] ? $res["activitie_color"] : "#ffffff";
		$activitie_start = $res["activitie_start"] != "" && $res["activitie_start"] != null ? $res["activitie_start"] : -1;
		$activitie_end = $res["activitie_end"] != "" && $res["activitie_end"] != null ? $res["activitie_end"] : -1;

		$activitie_data = array(
			"color" => $activitie_color,
			"end" => $activitie_end,
			"id" => $activitie_id,
			"start" => $activitie_start
		);

		array_push($activities, $activitie_data);

	}

	return $activities;

}

// returns new project planner structure
function new_project_planner_structure($project_id) {
	GLOBAL $db;
	// define dynamic data
	// query for operator numbers
	$db->setQuery("	SELECT 	id
					FROM 	wfm_planner wfm
					WHERE 	wfm.project_id = '$project_id' AND wfm.actived = 1");
	$query = $db->getResultArray();

	// count operators
	$op_count = $query['count'];

    return '<section class="wfm-section wfm-planner">
                <div class="wfm-section-title-holder">
                    <h1 class="wfm-section-togler">
											დამგეგმავი
										</h1>
                </div>
								<div>
	                <div class="wfm-planer-table-wrapper">
	                    <div class="wfm-planer-table-side wfm-pts-operators">
	                        <table class="wfm-planer-table-head">
	                            <thead class="thick-border-bottom">
	                                <tr>
	                                    <th class="wfm-pth-col wfm-pth-col-title" colspan="2">
	                                        <span>ოპერატორი</span>
	                                    </th>
	                                    <th class="wfm-pth-col wfm-pth-col-button">

	                                    </th>
																			<th class="wfm-pth-col wfm-pth-col-button">
	                                       <button id="addWorkPlannerRow">
	                                            დამატება
	                                       </button>
	                                    </th>
	                                </tr>
	                            </thead>
	                            <tbody class="thick-border-bottom">
	                                '.pp_operators_list($project_id, $op_count).'
	                            </tbody>
	                        </table>
	                    </div>
	                    <div class="wfm-planer-table-side wfm-pts-table-holder">
	                        <div class="wfm-planer-table-container">
	                            <div class="wfm-pt-body">
	                                <table class="wfm-planer-table-content" id="plan_table">
	                                    <thead>
	                                        <tr>
	                                            <th class="wfm-ptc-col-min wfm-ptc-col-mane" colspan="7">
	                                                <span>1 კვირა</span>
	                                            </th>
	                                            <th class="wfm-ptc-col-min wfm-ptc-col-mane" colspan="7">
	                                                <span>2 კვირა</span>
	                                            </th>
																							<th class="wfm-ptc-col-min wfm-ptc-col-mane" colspan="7">
	                                                <span>3 კვირა</span>
	                                            </th>
	                                            <th class="wfm-ptc-col-min wfm-ptc-col-mane" colspan="7">
	                                                <span>4 კვირა</span>
	                                            </th>
	                                        </tr>
	                                        <tr>
	                                            '.pp_day_names_list().'
	                                        </tr>
	                                    </thead>
	                                    <tbody class="wfm-pts-days-body">
	                                        '.pp_cycle_values_list($project_id, $op_count).'
	                                    </tbody>
	                                </table>
	                            </div>
	                            <div class="wfm-pt-footer">
	                                <div class="wfm-pt-scroll-width"></div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="wfm-planer-table-side wfm-pts-loop-buttons">
	                        <table class="wfm-planer-table-buttons" id="planer_loopbutton_table">
	                            <thead class="thick-border-bottom">
	                                <tr>
																			<th class="wfm-pth-col">
																				<span>სთ <br> კვირა</span>
																			</th>
																			<th class="wfm-pth-col">
																				<span>სთ <br> 4 კვირა</span>
																			</th>
	                                    <th class="wfm-pth-col"></th>
	                                </tr>
	                            </thead>
	                            <tbody class="thick-border-bottom planer-week-and-month">
	                                '.pp_add_cycle_button_list($op_count).'
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	                <div class="wfm-planer-table-wrapper">
	                    <div class="wfm-planer-table-side wfm-pts-plan-hours">
	                        <table class="wfm-planer-hours-table-head">
	                            <tr>
	                                <td>
	                                    <div class="wfm-pth-hours-name">
	                                        <span class="plane-hours-name">სულ გეგმიური სთ/დღე</span>
	                                    </div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <div class="wfm-pth-hours-name">
	                                        <span class="operation-hours-name">სულ საოპერაციო სთ/დღე</span>
	                                    </div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <div class="wfm-pth-hours-name">
	                                        <span class="difference-hours-name">სხვაობა</span>
	                                    </div>
	                                </td>
	                            </tr>
	                        </table>
	                    </div>
	                    <div class="wfm-planer-table-side wfm-planer-table-holder">
	                        <div class="wfm-planer-table-container">
	                            <div class="wfm-pt-body">
	                                <div class="wfm-planer-hours-table" id="planer_hours_table">
	                                   <div class="wfm-pht-row wfm-pht-paln-row">
	                                        '.pp_calculate_list().'
	                                   </div>
	                                   <div class="wfm-pht-row wfm-pht-operation-row">
	                                        '.pp_calculate_list().'
	                                   </div>
	                                   <div class="wfm-pht-row wfm-pht-difference-row">
	                                        '.pp_calculate_list("operate").'
	                                   </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="wfm-planer-table-side wfm-pts-loop-buttons">

	                    </div>
	                </div>
								</div>
            </section>';

}


// return work grphic table structure
function work_grapic_table_structure() {

    return '
            <div class="list-table-wrapper">
                <div class="button-area">
                    <button id="work_graphic_show_active">აქტიური</button>
                    <button id="work_graphic_show_rchive">არქივი</button>
                </div>
                <table class="display" id="work_graphic_table">
                    <thead>
                        <tr id="datatable_header">
                            <th>ID</th>
                            <th style="width: 5%;">#</th>
                            <th style="width: 47%;">პროექტი</th>
                            <th style="width: 48%;">მიმდინარე თვე შევსებულია</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header">
                            <th class="colum_hidden">
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input style="width: 95%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>';

}


// returns next 10 years list
function get_next_10_years_list($current_year) {

	$start_year = 2018;
	$options = '';

	for($i = 0; $i < 30; $i++) {

		$loop_year = $start_year + $i;

		if($loop_year == $current_year) {
			$options .= '<option id="rushHoursCurrentYear" value="'.$loop_year.'" selected>'.$loop_year.'</option>';
		} else {
			$options .= '<option value="'.$loop_year.'">'.$loop_year.'</option>';
		}

	}

	return $options;

}

// convert number to roman number
function num_convert_to_roman($num) {

	$result = "";
	$roman_nums = array(
		"M" => 1000,
		"CM" => 900,
		"D" => 500,
		"CD" => 400,
		"C" => 100,
		"XC" => 90,
		"L" => 50,
		"XL" => 40,
		"X" => 10,
		"IX" => 9,
		"V" => 5,
		"IV" => 4,
		"I" => 1
	);

	foreach($roman_nums as $key => $value) {

		while($num >= $value) {
			$result .= $key;
			$num -= $value;
		}

	}

	return $result;

}

// returns full year week list
function full_year_week_list($year) {

	$curr_year = date("Y");
	$cur_week_num = 0;
	$selected = "";
	$structure = '<option value="0">-------------</option>';

	if($year == $curr_year) {
		$cur_week_num = date("W");
	}

	for($w = 0; $w < 53; $w++) {

		$date = get_week_start_end_date($year, $w);

		if($w == ($cur_week_num - 1)) {
			$selected = "selected";
		} else {
			$selected = "";
		}

		$structure .= '<option class="week-num-li" value="'.$w.'" '.$selected.'>
										<span>'.$date["start"].' - '.$date["end"].'</span>
										<span> / '.($w + 1).' კვირა</span>
									</option>';

	}

    return $structure;
}

// returns week start and end date depended on year and week number
function get_week_start_end_date($year, $week) {

	$week_length = 6 * 24 * 3600;

	$time = strtotime("1 January $year", time());
	$day = date('w', $time);

	$start_time = $time + ( (7 * $week ) + 1 - $day ) * 24 * 3600;
	$end_time = $start_time + $week_length;

	$start_date = zeroFixer(date('j', $start_time)).'.'.zeroFixer(date('n', $start_time));
	$end_date = zeroFixer(date('j', $end_time)).'.'.zeroFixer(date('n', $end_time));

	$data = array(
		"start" => $start_date,
		"end" => $end_date
	);

	return $data;

}

// returns rush hours day list structure
function get_rush_hours_day_list() {

	$days = array("ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა");
	$structure = "";

	foreach($days as $day_index => $day_name) {

		$structure .= '<div class="row" data-row="'.$day_index.'">
											<span>'.$day_name.'</span>
											<button class="rush-hour-row-clearer" data-row="'.$day_index.'" title="სტრიქონის გასუფთავება">
												<i class="fa fa-eraser"></i>
											</button>
										</div>';
	}

	return $structure;

}

// get rush hour data structure
function rush_hour_structure() {

	$current_year = date('Y');

	return '
		<div class="rush-hour-header">
			<div class="row">
				<div class="col">
					<label for="chooseRushHourYear">აირჩიეთ წელი:</label>
					<select class="jquery-ui-chosen" data-width="110px" id="chooseRushHourYear">
						'.get_next_10_years_list($current_year).'
					</select>
				</div>
				<div class="col">
					<label for="chooseRushHourWeek">აირჩიეთ კვირა:</label>
					<select class="jquery-ui-chosen" data-width="200px" id="chooseRushHourWeek">
						'.full_year_week_list($current_year).'
					</select>
				</div>
				<div class="col">
					<button id="refreshRushHoursDate" title="მიმდინარე თარიღზე დაბრუნება">
						<i class="fa fa-sync-alt"></i>
					</button>
				</div>
				<div class="col">
					<button id="copyRashHorsFromPrevWeek" title="დააკოპირე წინა კვირიდან">
						<i class="fa fa-copy"></i>
					</button>
				</div>
			</div>
		</div>

		<div class="rush-hour-body">
			<div class="part days">
				<div class="content">
					<div class="head">
						<div class="col flex-width">
							<span>კვირის დღეები</span>
						</div>
					</div>
					<div class="body">
						'.get_rush_hours_day_list().'
					</div>
				</div>
				<div class="scroll">

				</div>
			</div>
			<div class="part value-table">
				<div class="content">
					<div class="right-boder"></div>
					<div class="slider" id="rushHoursTableSlider">
						<div class="head">
								'.table_in_24_hour_head_structure().'
						</div>
						<div class="body">
							<div class="row" data-row="0">

							</div>
							<div class="row" data-row="1">

							</div>
							<div class="row" data-row="2">

							</div>
							<div class="row" data-row="3">

							</div>
							<div class="row" data-row="4">

							</div>
							<div class="row" data-row="5">

							</div>
							<div class="row" data-row="6">

							</div>
						</div>
					</div>
				</div>
				<div class="scroll" id="rushHoursTableScroll">
					<div class="maker"></div>
				</div>
			</div>
		</div>

		<div class="rush-hour-footer">
			<div class="rhs-button-holder">
				<button id="saveRushHours" disabled>
					ცვლილებების შენახვა
				</button>
			</div>
		</div>
	';

}

// work schedule loop
function work_schedule_loop($num) {

    $structure = "";

    for($i = 0;$i <= 23;$i++){
        if (strlen($i) != 2){
            $structure.= '<td style="" clock="0'.$i.'15"  check_clock="" wday="'.$num.'"></td>
                                  <td style="" clock="0'.$i.'30"  check_clock="" wday="'.$num.'" ></td>
                                  <td style="" clock="0'.$i.'45"  check_clock="" wday="'.$num.'" ></td>
                	              <td style="" clock="'.((($i+1)=='10')?'':"0").''.($i+1).'00"  check_clock="" wday="'.$num.'"></td>';
        }else{
            $structure.= '<td style="" clock="'.$i.'15"  check_clock="" wday="'.$num.'"></td>
                                      <td style="" clock="'.$i.'30"  check_clock="" wday="'.$num.'" ></td>
                                      <td style="" clock="'.$i.'45"  check_clock="" wday="'.$num.'" ></td>
                	                  <td style="" clock="'.($i+1).'00"  check_clock="" wday="'.$num.'"></td>';
        }
    }

    return $structure;
}

// getting existed projects list
function get_existed_project_list() {
	GLOBAL $db;
    $table = "<table class='my-table'>
                <thead>
                    <tr>
                        <th style='width: 75%;'>პროექტის დასახელება</th>
                        <th style='width: 25%;'>ქმედება</th>
                    </tr>
                </thead>
                <tbody>";

    $db->setQuery("SELECT id,
                                    name,
                                    max_hour_per_month,
                                    work_shift_max_hour,
                                    sl_type_id,
                                    sl_percent,
                                    sl_second,
                                    sl_missed_from_second
                                FROM project
                                WHERE actived = 1");
	$query = $db->getResultArray();
    foreach($query['result'] AS $r) {

        $data_project = array(
            "slType" => $r['sl_type_id'],
            "slPercent" => $r['sl_percent'],
            "slSecond" => $r['sl_second'],
            "slMissed" => $r['sl_missed_from_second'],
            "maxHour" => $r['max_hour_per_month'],
            "workShift" => $r['work_shift_max_hour']
        );

        $data_project = json_encode($data_project);

        $table .= "<tr>
                       <td> <span>".$r["name"]."</span> </td>
                       <td>
                            <button class='choose-existed-project' data-id='".$r["id"]."' data-project='".$data_project."'>
                                არჩევა
                            </button>
                       </td>
                   </tr>";
    }

    $table .= "</tbody> </table>";

    return $table;

}

// get holiday data
function GetHolidayData($holiday_id)
{
	GLOBAL $db;
    $db->setQuery("SELECT 	holidays.id,
										holidays.creeping,
										holidays.year,
										holidays.month,
										holidays.date,
		                holidays.name,
										holidays_category.name AS holiday_category,
										holidays.holidays_category_id AS category
							    FROM 	holidays
							    LEFT JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
							    WHERE 	holidays.actived=1 AND holidays.id = '$holiday_id'");
	$res = $db->getResultArray();
    return $res['result'][0];
}


// returns project holidays table structure
function get_project_holidays_table_structure() {

	return '<div class="holidays-schema-wrapper">

		<!-- header -->
		<div class="holidays-schema-header">
			<button id="addNewHoliday">
				დამატება
			</button>
			<button id="addAllOfficialHollidays">
				ოფიციალური დასვენებები
			</button>
		</div>
	
		<!-- body -->
		<div class="holidays-schema-body">
			<table class="my-table" id="holiday-list-for-project-wrapper">
				<thead>
					<tr>
						<th style="width: 17%;">თარიღი</th>
						<th style="width: 42%;">დასახელება</th>
						<th style="width: 37%;">კატეგორია</th>
						<th style="width: 14%;">მოქმედება</th>
					</tr>
				</thead>
				<tbody id="projectHolidaysListReceiver">
					<tr>
						<td class="empty-table-td" colspan="4">
							ჩამონათვალი ცარიელია
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	
	</div>';
}

// getting holiday list
function get_project_holidays_data($project_id) {
	GLOBAL $db;
	$data_collector = array();

    $db->setQuery("SELECT holidays.id,
							CONCAT(IF(holidays.year = 0, '', holidays.year),
										IF(holidays.year = 0, '', '-'),
										IF((holidays.month + 1) < 10, CONCAT('0', (holidays.month + 1)), (holidays.month + 1)),
										'-',
										IF(holidays.date < 10, CONCAT('0', holidays.date), holidays.date)) AS holiday_date,
						holidays.name AS holiday_name,
								holidays.holidays_category_id AS hol_cat_id,
						holidays_category.name AS holidays_category
						FROM project_holiday
						LEFT JOIN holidays ON holidays.id = project_holiday.holidays_id
						LEFT JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
						LEFT JOIN project ON project.id = project_holiday.project_id
						WHERE project_holiday.actived = 1 AND project_holiday.project_id = '$project_id'
						ORDER BY holiday_date");
	$query = $db->getResultArray();
    $data_length = $query['count'];

    if($data_length > 0) {

        foreach($query['result'] AS $r) {

            $data = array(
				"id" => $r["id"],
				"date" => $r["holiday_date"],
				"name" => $r["holiday_name"],
				"categoryId" => $r["hol_cat_id"],
				"categoryName" => $r["holidays_category"]
			);

			array_push($data_collector, $data);
        }

    }

    return $data_collector;

}

// getting just officcial holiday list
function get_just_official_holiday_list() {

    $list = "";

    $query = mysql_query("SELECT holidays.id,
																	CONCAT(IF(holidays.year = 0, '', holidays.year),
																				IF(holidays.year = 0, '', '-'),
																				IF((holidays.month + 1) < 10, CONCAT('0', (holidays.month + 1)), (holidays.month + 1)),
																				'-',
																				IF(holidays.date < 10, CONCAT('0', holidays.date), holidays.date)) AS holiday_date,
                                   holidays.name AS holiday_name,
																	 holidays.holidays_category_id AS hol_cat_id,
                                   holidays_category.name AS holidays_category
                                FROM holidays
                                JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
                                WHERE holidays.actived = 1 AND holidays.holidays_category_id = 1
                                ORDER BY holiday_date");

		while($r = mysql_fetch_assoc($query)) {

				$list .= "<tr data-id='".$r['id']."' data-category='".$r['hol_cat_id']."'>
											 <td><span>".$r["holiday_date"]."</span></td>
											 <td><span>".$r["holiday_name"]."</span></td>
											 <td><span>".$r["holidays_category"]."</span></td>
											 <td>
													<button class='rmv-holiday-fl' data-id='".$r['id']."'>
															წაშლა
													</button>
											 </td>
									 </tr>";
		}

    return $list;

}

// getting holiday list
function get_holiday_for_project_table() {

    return '<table class="display" id="holiday_for_project_table">
			        <thead>
			        <tr>
			            <th>ID</th>
			            <th style="width: 15%;" class="check">თარიღი</th>
			            <th style="width: 42%;">დასახელება</th>
			            <th style="width: 30%;">კატეგრორია</th>
									<th style="width: 50px;">მოქმედება</th>
			        </tr>
			        </thead>
			        <thead>
			        <tr class="search_header">
			            <th class="colum_hidden">
			                <input type="text" name="search_id" value="ფილტრი" class="search_init" />
			            </th>
			            <th>
			                <input type="text" name="search_number" value="ფილტრი" class="search_init" />
			            </th>
			            <th>
			                <input type="text" name="search_date" value="ფილტრი" class="search_init" />
			            </th>
			            <th>
			                <input type="text" name="search_date" value="ფილტრი" class="search_init" />
			            </th>
									<th>

			            </th>
			        </tr>
			        </thead>
			    </table>';

}

// get holiday category list
function get_holiday_category_list($category_id) {
	GLOBAL $db;
    $data = "";
    $db->setQuery("SELECT id, name FROM holidays_category");
	$query = $db->getResultArray();
    foreach($query['result'] AS $res) {
				if($res["id"] == $category_id) {
						$data .= "<option value='".$res["id"]."' selected>".$res["name"]."</option>";
				} else {
						$data .= "<option value='".$res["id"]."'>".$res["name"]."</option>";
				}

    }

    return $data;

}

function get_holiday_form($res = '')
{
    
    $data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

	    	<table class="dialog-form-table-holidays">
					<input type="hidden" id="holidays_hidden_id" value="'.$res["id"].'">
				<tr>
                    <td class="pad-bottom" colspan="2">
                        <label for="holidayCategory">კატეგორია</label>
                        <select name="holiday_category" id="holidayCategory" data-select="jquery-ui-select">'.get_holiday_category_list($res["category"]).'</select>
                    </td>
										<td>
                        <label for="holidayCreeping">მცოცავი</label>
                        <input type="checkbox" name="holiday_creeping" id="holidayCreeping" '.($res["creeping"] ? 'checked' : '').'/>
                    </td>
                </tr>
                <tr>
                    <td class="children-full-width pad-bottom" style="width: 33%; padding-right: 10px;">
                        <label for="holidaYear">წელი</label>
                        <select name="holiday_year" id="holidaYear" data-select="jquery-ui-select" '.($res["creeping"] ? '' : '').'>
                            '.get_simple_years_list($res["year"]).'
                        </select>
                    </td>
                    <td class="children-full-width pad-bottom" style="width: 33%; padding-right: 10px;">
                        <label for="holidayMonth">თვე</label>
                        <select name="holiday_month" id="holidayMonth" data-select="jquery-ui-select">
                            '.get_simple_month_list($res["month"]).'
                        </select>
                    </td>
                    <td class="children-full-width pad-bottom" style="width: 33%;">
                        <label for="holidayDate">რიცხვი</label>
                        <select name="holiday_date" id="holidayDate" data-select="jquery-ui-select">
                            '.get_simple_day_list($res["date"]).'
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="children-full-width pad-bottom" style="width: 100%;" colspan="3">
                        <label for="holidayName">დასახელება</label>
                        <input type="text" name="holiday_name" id="holidayName" value="'.$res["name"].'">
                    </td>
                </tr>
			</table>
			<!-- ID -->
			<input type="hidden" id="department_id" value="' . $res['id'] . '" />
        </fieldset>
    </div>
    ';
    return $data;
}

// returns simple year list
function get_simple_years_list($selected_year) {

    $list = "<option value='0'>-------------</option>";

    for($i = 17; $i <= 50; $i++) {

        $year = "20".$i;

        if($selected_year == $year) {
            $list .= "<option value='".$year."' selected>".$year."</option>";
        } else {
            $list .= "<option value='".$year."'>".$year."</option>";
        }

    }

    return $list;

}

// returns simple month list
function get_simple_month_list($selected_month) {

    $list = "";
    $month_array = array("იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი");

    for($i = 0; $i < 12; $i++) {

        if($selected_month == $i) {
            $list .= "<option value='".$i."' selected>".$month_array[$i]."</option>";
        } else {
            $list .= "<option value='".$i."'>".$month_array[$i]."</option>";
        }

    }

    return $list;

}

// returns simple day list
function get_simple_day_list($selected_day) {

    $list = "";
    $day = "";

    for($i = 1; $i <= 31; $i++) {

        if($i < 10) {
            $day = "0".$i;
        } else {
            $day = $i;
        }

        if($selected_day == $i) {
            $list .= "<option value='".$day."' selected>".$day."</option>";
        } else {
            $list .= "<option value='".$day."'>".$day."</option>";
        }

    }

    return $list;

}

// returns work shift dialog structure
function get_workshift_form($res = '')
{
    $data = '
	<style>
		.shift-edit-dialog-table td {
			padding-bottom: 6px;
		}
		.shift-edit-dialog-table input {
			width: 98%;
		}

		.shift-edit-dialog-table select {
			width: 98%;
		}

		.shift-edit-dialog-table textarea {
			width: 98%;
		}
	</style>
	<div id="dialog-form" class="shift-edit-dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>
            <!-- ID -->
			<input type="hidden" id="workshift_id" value="' . $res['id'] . '" />
	    	<table class="dialog-form-table shift-edit-dialog-table">
			    <tr>
						<td colspan="2">
							<label for="workshift_name">სახელი</label>
							<input type="text" id="workshift_name" value="' . $res['name'] . '" />
						</td>
				</tr>
				<tr>
						<td colspan="2">
							<label for="workshift_24hours">24 საათი</label>
							<input type="checkbox" id="workshift_24hours" value="" />
						</td>
				</tr>
        		<tr>
					<td colspan="2">
						<label for="workshift_start_date">დასაწყისი</label>
						<input type="text" id="workshift_start_date" value="' . $res['start_date'] . '" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="workshift_end_date">დასასრული</label>
						<input type="text" id="workshift_end_date" value="' . $res['end_date'] . '" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="workshift_timeout">შესვენების ხან-ბა</label>
						<input type="text" id="workshift_timeout" value="' . $res['timeout'] . '" />
					</td>
				</tr>
				<tr>
					<td>
						<span>ცვლის ხ ბა</span>
						<input type="text" id="plannerNewShiftFullDur" style="width: 75px;" readonly/>
					</td>
					<td>
						<div style="position: relative; left: 8px;">
							<span>სამ. ხ ბა</span>
							<input type="text" id="plannerNewShiftWorkDur" style="width: 75px;" readonly/>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="workshift_color">ფერი</label>
						<input type="color" id="workshift_color" value="' . $res['color'] . '" />
					</td>
				</tr>
			    <tr>
					<td colspan="2">
						<label for="workshift_pay">ანაზღაურების ტიპი</label>
						<select id="workshift_pay" style="width: 174px;">'.GetWorkshiftPay($res['pay']).'</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="workshift_type">სამუშაო ტიპი</label>
						<select id="workshift_type" style="width: 174px;">'.GetWorkShitType($res['type']).'</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="workshift_comment">კომენტარი</label>
						<textarea  id="workshift_comment" style="resize: vertical;">' . $res['comment'] . '</textarea>
					</td>
				</tr>
			</table>
        </fieldset>
    </div>
    ';
    return $data;
}

// returns work cycle dialog structure
function get_cycle_form_edit($lg)
{
	GLOBAL $db;
	$db->setQuery(" SELECT `name`
					FROM `work_cycle`
					WHERE actived = 1 AND id = $lg");
	$req1 = $db->getResultArray();
	$req1 = $req1['result'][0];
$data = '
<div id="dialog-form">
		<fieldset>
			<legend>ძირითადი ინფორმაცია</legend>
					<!-- ID -->
		<input type="hidden" id="wcsh_cycle_id" value="' . $lg . '" />
		<input type="hidden" id="wcsh_next_project" value="' . $req1[1] . '" />
			<table class="dialog-form-table" style="margin-left:unset;">
				<tr>
						<td style="width: 190px;"><label for="wcsh_name">ციკლის დასახელება</label></td>
			</tr>
				<tr>
				<td>
					<input type="text" id="wcsh_name" value="'.$req1[name].'" />
				</td>
					<td>
							<button id="add_button_inner_cycle">დამატება</button>
					</td>
					<td>
							<button id="delete_inner_cycle">წაშლა</button>
					</td>
			</tr>

		</table>
		<table class="display" id="work_cyrcle_inner_table" style="margin-top:20px;">
							<thead>
									<tr id="datatable_header">
											<th>ID</th>
					<th style="width: 11%;">რიგითობა</th>
											<th style="width: 14%;">დასახელება</th>
											<th style="width: 12%;">დასაწყისი</th>
											<th style="width: 12%;">დასასრული</th>
											<th style="width: 14%;">სამუშაო ტიპი</th>
											<th style="width: 13%;">გადახდახი/არაგადახდადი</th>
											<th style="width: 16%;">კომენტარი</th>
											<th style="width: 10%;">ფერი</th>
										<th class="check">#</th>
									</tr>
							</thead>
							<thead>
									<tr class="search_header">
											<th class="colum_hidden">
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
					<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
												<div class="callapp_checkbox">
															<input type="checkbox" id="checkAllCycleShifts" name="checkAllCycleShifts" />
															<label for="checkAllCycleShifts"></label>
													</div>
											</th>
									</tr>
							</thead>
					</table>
			</fieldset>
	</div>
	';
return $data;
}

// returns work cycle dialog structure
function get_cycle_form_edit_create()
{

$data = '
<div id="dialog-form">
		<fieldset>
				<legend>ძირითადი ინფორმაცია</legend>
				<table class="dialog-form-table" style="width: 100%;">
						<tr>
								<td>
									<label for="wcsh_name">ციკლის დასახელება</label>
								</td>
						</tr>
						<tr>
							<td>
								<input type="text" id="wcsh_name" value="" style="width: 98%;" />
							</td>
						</tr>
				</table>
			</fieldset>
	</div>
	';
return $data;
}

// get full year month list
function get_full_year_month_list() {

	$structure = "<option value='-1' selected> აირჩიე თვე </option>";
	$month_array = array("იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი" , "დეკემბერი");

	foreach($month_array as $index => $month) {
		$structure .= "<option value='".(++$index)."'>".$month."</option>";
	}

	return $structure;

}

// get full year 
function get_full_year_list() {	
	GLOBAL $db;
	$structure = "<option value='-1' selected> აირჩიე წელი </option>";
	$db->setQuery(" SELECT year(NOW()) - 1 , year(NOW()) AS 'now', year(NOW()) + 1");
	$yeart = $db->getResultArray();
	foreach($yeart['result'][0] as $name => $year) {
		$selected = "";
		if ($name == 'now') $selected = "selected";
		$structure .= "<option value='".$year."' $selected >".$year."</option>";
	}

	return $structure;

}

// return name of day by specific date
function getWeekday($date) {

	$days_array = array("ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა");
	$day_index = date('w', strtotime($date));

  return $days_array[$day_index];
}

// return name of month by month index
function getMonthName($month_index) {

	$month_array = array(
		"01" => "იანვარი",
		"02" => "თებერვალი",
		"03" => "მარტი",
		"04" => "აპრილი",
		"05" => "მაისი",
		"06" => "ივნისი",
		"07" => "ივლისი",
		"08" => "აგვისტო",
		"09" => "სექტემბერი",
		"10" => "ოქტომბერი",
		"11" => "ნოემბერი",
		"12" => "დეკემბერი"
	);

	return $month_array[$month_index];

}

// return work graphic days list
function work_graphic_day_list($month, $year, $days_in_month) {

	$structure = '';

	for($i = 0; $i < $days_in_month; $i++) {

		$date = zeroFixer($i + 1).'.'.$month.'.'.$year;

		$structure .= '<div class="wgsb-content-col" data-col="'.$i.'">
										<div class="wgtc-head-row wgtc-head-day-name height-1 '.(getWeekday($date) == "შაბათი" || getWeekday($date) == "კვირა" ? "wgtc-head-weekend" : "").'">
											<span>'.getWeekday($date).'</span>
										</div>
										<div class="wgtc-head-row height-1">
											<a href="#">'.$date.'</a>
										</div>
										<div class="wgtc-head-row height-1">
											<div class="wgtc-head-percent-holder">
												<div class="wgtc-head-percent-value" style="width:100%;">
													<span>100%</span>
												</div>
											</div>
										</div>
									</div>';

	}

	return $structure;

}

// return work graphic values list
function work_graphic_values_list($row, $days_in_month) {

	$structure = '';

	for($i = 0; $i < $days_in_month; $i++) {

		$structure .= '<div class="wgsb-content-col work-shift" data-row="'.$row.'" data-col="'.$i.'">
										<span></span>
									</div>';

	}

	return $structure;

}

// return work graphic values list
function work_graphic_results_list($type, $days_in_month) {

	$structure = '';

	for($i = 0; $i < $days_in_month; $i++) {

		$structure .= '<div class="wgsb-results-col '.$type.' '.($type == "exact" ? "difference" : "").'" data-col="'.$i.'">
										<span data-difference="00:00" data-shiftId="0">00:00</span>
									</div>';

	}

	return $structure;

}

// return work graphic table body row list
function get_wgsb_header_body_row($count = 1) {

	$structure = "";

	for($i = 0; $i < $count; $i++) {
		$structure .= '<div class="wgtc-body-row">
											<div class="wgsb-header-col no">
												<span>'.($i + 1).'</span>
											</div>
											<div class="wgsb-header-col select">
												<select class="choose-work-graphic-operator" data-row="'.$i.'">
												</select>
											</div>
											<div class="wgsb-header-col select">
											<select class="choose-work-graphic-cycle" data-row="'.$i.'">
											</select>
											</div>
											<div class="wgsb-header-col button">
												'.($i > 0 ? "<button id='deleteWorkGraphicRow' data-row='".$i."'>
													წაშლა
												</button>" : "").'
											</div>
										</div>';
	}

	return $structure;

}

// return work graphic table content row list
function get_wgsb_content_body_row($count = 1, $days_in_month) {

	$structure = "";

	for($i = 0; $i < $count; $i++) {
		$structure .= '<div class="wgtc-body-row">
										'.work_graphic_values_list($i, $days_in_month).'
									</div>';
	}

	return $structure;

}

// return work graphic table footer row list
function get_wgsb_footer_body_row($count = 1) {

	$structure = "";

	for($i = 0; $i < $count; $i++) {
		$structure .= '<div class="wgtc-body-row">
										<div class="wgsb-footer-col hour-count work-houers" data-row="'.$i.'">
											<span>00:00</span>
										</div>
										<div class="wgsb-footer-col hour-count break-houers" data-row="'.$i.'">
											<span>00:00</span>
										</div>
									</div>';
	}

	return $structure;

}

// returns work cycle dialog structure
function get_graphic_schema_dialog($project_id) {
	GLOBAL $db;
	// get project name
	$db->setQuery("SELECT name FROM project WHERE id = '$project_id'");
	$p_name_result = $db->getResultArray();
	$project_name = $p_name_result['result'][0]["name"];

	// get current year and month
	$cur_year = date("Y");
	$cur_month = date("m");
	$days_in_month = cal_days_in_month(CAL_GREGORIAN, $cur_month, $cur_year);
	$sider_width = ($days_in_month * 90) + 3;

	$data = '
			<div class="work-graphic-schema-wrapper">

				<!-- work graphic schema header -->
				<div class="work-graphic-schema-header">
					<select id="choosWorkGraphicMonth">
						'.get_full_year_month_list().'
					</select>
					<select id="choosWorkGraphicYear">
						'.get_full_year_list().'
					</select>
				</div>

				<!-- work graphic schema body -->
				<div class="work-graphic-schema-body">

					<!-- work graphic schema body header -->
					<div class="wgsb-header">
						<div class="work-graphic-table-content">
							<div class="wgtc-head">
								<div class="wgtc-head-row height-3"></div>
								<div class="wgtc-head-row height-1">
									<div class="wgsb-header-col no">
										<span>No:</span>
									</div>
									<div class="wgsb-header-col select">
										<span>ოპერატორი</span>
									</div>
									<div class="wgsb-header-col select">
										<span>ციკლი</span>
									</div>
									<div class="wgsb-header-col button">
										<button id="addWorkGraphicRow">
											დამატება
										</button>
									</div>
								</div>
							</div>
							<div class="wgtc-body">
								'.get_wgsb_header_body_row().'
							</div>
						</div>
						<div class="work-graphic-table-scroll">
						</div>
						<div class="work-graphic-table-results">
							<div class="wgtc-head-row results">
								<div class="wgsb-header-col full-width">
									<span class="planned">სულ გეგმიური სთ/დღე</span>
								</div>
							</div>
							<div class="wgtc-head-row results">
								<div class="wgsb-header-col full-width">
									<span class="operated">სულ საოპერაციო სთ/დღე</span>
								</div>
							</div>
							<div class="wgtc-head-row results">
								<div class="wgsb-header-col full-width">
									<span class="difference">სხვაობა</span>
								</div>
							</div>
						</div>
					</div>

					<!-- work graphic schema body content -->
					<div class="wgsb-content">
						<div class="work-graphic-table-content">
							<div class="wgtc-head-row height-1">
								<div class="wgsb-header-col full-width wgtc-month-year content-center">
									<span>'.getMonthName($cur_month).' '.$cur_year.'</span>
								</div>
							</div>
							<div class="wgtc-slider wgts-days-width" id="wgsbContentSlider" style="width:'.$sider_width.'px;">
								<div class="wgtc-head">
									<div id="dayNameReceiver" class="wgtc-head-row height-3">
										'.work_graphic_day_list($cur_month, $cur_year, $days_in_month).'
									</div>
								</div>
								<div class="wgtc-body wgtc-work-shift-receiver">
									'.get_wgsb_content_body_row(1, $days_in_month).'
								</div>
							</div>
						</div>
						<div class="work-graphic-table-scroll">
							<div class="wgts-wrapper">
								<div class="wgts-place-holder wgts-days-width" style="width:'.$sider_width.'px;"></div>
							</div>
						</div>
						<div class="work-graphic-table-results">
							<div class="wgtc-slider wgts-days-width" style="width:'.$sider_width.'px;">
								<div class="wgtc-body">
									<div class="wgtc-results-row">
										'.work_graphic_results_list("planned", $days_in_month).'
									</div>
									<div class="wgtc-results-row">
										'.work_graphic_results_list("operated", $days_in_month).'
									</div>
									<div class="wgtc-results-row">
										'.work_graphic_results_list("exact", $days_in_month).'
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- work graphic schema body footer -->
					<div class="wgsb-footer">
						<div class="work-graphic-table-content">
							<div class="wgtc-head">
								<div class="wgtc-head-row height-2"></div>
								<div class="wgtc-head-row height-2">
									<div class="wgsb-footer-col">
										<span>სამუშაო <br> სთ/თვე</span>
									</div>
									<div class="wgsb-footer-col">
										<span>დასვენება <br> სთ/თვე</span>
									</div>
								</div>
							</div>
							<div class="wgtc-body">
								'.get_wgsb_footer_body_row().'
							</div>
						</div>
						<div class="work-graphic-table-scroll">
						</div>
						<div class="work-graphic-table-results">
						</div>
					</div>

				</div>

				<!-- work graphic schema footer -->
				<div class="work-graphic-schema-footer">

					<ul class="status-description-list" id="statusDescriber">
						<label for="statusDescriber">ჯამური დროის სტატუსები:</label>
						<li class="exact">
							<span>ზუსტია</span>
						</li>
						<li class="less">
							<span>ნაკლებია</span>
						</li>
						<li class="over">
							<span>გადაჭარბებულია</span>
						</li>
					</ul>

				</div>

			</div>
			<div class="project-details-day-graphic-container">
			</div>
				';
return $data;
}

// returns work cycle shift dialog structure
function get_cycle_workshift_form($res,$project)
{
	if($res == ''){
	    $project_id = $project;
	}else{
	    $project_id = $res[project_id];
	}
	    $data = '
		<div id="dialog-form">
		    <fieldset>
		    	<legend>ძირითადი ინფორმაცია</legend>
	            <!-- ID -->
				<input type="hidden" id="work_cycle_shift_detail_id" value="' . $res[id] . '" />
		    	<table class="dialog-form-table">
				    <tr>
				        <td style="width: 190px;"><label for="wcsh_work_shift_id">ცვლები</label></td>
				    </tr>
				    <tr>
						<td>
							<select id="wcsh_work_shift_id" style="width: 174px;">'.GetWorkShift($res[work_shift_id]).'</select>
						</td>
					</tr>
				</table>
				<table class="dialog-form-table">
				    <tr>
				        <td style="width: 190px;"><label for="wcsh_num">რიგითობა</label></td>
					</tr>
					<tr>
						<td>
							<input id="wcsh_num" type="number" value="'.$res[num].'" min="1">
						</td>
					</tr>
				</table>
	        </fieldset>
	    </div>
	    ';
	    return $data;
}

function GetWorkShift($id,$pay){
	GLOBAL $db;
    $db->setQuery("    SELECT  `id`,
                                    `name`
                            FROM    `work_shift`
                            WHERE actived = 1");
	$query = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($query['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function GetWorkShiftData($workshift_id)
{
	GLOBAL $db;
    $db->setQuery("	SELECT  `id`,
													DATE_FORMAT(`start_date`, '%H:%i') AS start_date,
													DATE_FORMAT(`end_date`, '%H:%i') AS end_date,
													DATE_FORMAT(`timeout`, '%H:%i') AS timeout,
	                                                `type`,
	                                                `name`,
                                                  	`color`,
                                            	    `comment`,
                                            	    `pay`
											FROM    `work_shift`
											WHERE   `id` = $workshift_id" );
	$res = $db->getResultArray();
    return $res['result'][0];
}


// get work shift pay
function GetWorkshiftPay($pay){
	GLOBAL $db;
    $db->setQuery("    SELECT  `id`,
                                    `name`
                            FROM    `work_pay`");
	$query = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($query['result'] AS $res){
        if($res['id'] == $pay){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

// get work shift shit list
function GetWorkShitType($type){
	GLOBAL $db;
    $db->setQuery("    SELECT  `id`,
                                    `name`
                            FROM    `work_type`");
	$query = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($query['result'] AS $res){
        if($res['id'] == $type){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function delete_work_shift($workshift_id)
{
	GLOBAL $db;
    $db->setQuery("	UPDATE `work_shift`
					SET    `actived` = 0
					WHERE  `id` IN($workshift_id)");
	$db->execQuery();
}

function delete_work_cycle_shift($worcycle_kshift_id)
{
	GLOBAL $db;
    $db->setQuery("	UPDATE `work_cycle_detail`
					SET    `actived` = 0
					WHERE  `id` IN($worcycle_kshift_id)");
	$db->execQuery();
}

function delete_work_cycle($workcycle_id)
{
    global $db;
    $db->setQuery("	UPDATE `work_cycle`
					SET    `actived` = 0
					WHERE  `id` IN($workcycle_id)");
    $db->execQuery();
}

// returns activities dialog structure
function get_activities_form($res = '')
{
	GLOBAL $db;
    //$table = mysql_fetch_array(mysql_query("SELECT GROUP_CONCAT(`name`) FROM `work_activities` WHERE project_id = '$res[project_id]' AND actived = 1 AND all_limit = 1"));
    $data = '
		<style>
			#dialog-form input:not(type="checkbox") {
				width: 100%;
			}

			#dialog-form select {
				width: 100%;
			}

			#dialog-form textarea {
				width: 100%;
			}

			#activities_color {
			border: none !important;
			}

			.dialog-form-table td {
				padding-bottom: 6px;
			}
		</style>
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>
            <!-- ID -->
			<input type="hidden" id="activities_id" value="' . $res['id'] . '" />
	    	<table class="dialog-form-table">
			    <tr>
					<td>
						<label for="activities_name">სახელი</label>
						<input type="text" id="activities_name" value="' . $res['name'] . '" />
					</td>
				</tr>
			    <tr>
					<td>
						<label for="activities_pay">ანაზღაურების ტიპი</label>
						<select id="activities_pay" style="width: 174px;">'.GetActivitiesPay($res['pay']).'</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="activities_color">ფერი</label>
						<input type="color" id="activities_color" value="' . $res['color'] . '" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="activities_comment">კომენტარი</label>
						<textarea  id="activities_comment" style="resize: vertical;">' . $res['comment'] . '</textarea>
					</td>
				</tr>
				<tr>
                    <td>
                        <label for="activities_dnd">DND</label>
                        <input type="checkbox" id="activities_dnd" '.($res['dnd'] == 0 && $res['dnd'] != "" ? "" : "checked" ).'/>
                    </td>
                </tr>
			</table>
        </fieldset>
    </div>
    ';
    return $data;
}

function GetActivitiesData($ctivities_id)
{
	GLOBAL $db;
    $db->setQuery("	SELECT  `id`,
							`name`,
							`color`,
							`comment`,
							`pay`,
							`dnd`
					FROM    `work_activities`
					WHERE   `id` = $ctivities_id" );
	$res = $db->getResultArray();
    return $res['result'][0];
}

function GetActivitiesProject($project_id){
    $req = mysql_query("    SELECT  `id`,
                                    `name`
                            FROM    `project`
                            WHERE   `actived` = 1");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $project_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function GetActivitiesPay($pay){
	GLOBAL $db;

    $db->setQuery(" SELECT  `id`,
							`name`
					FROM    `work_pay`");
	$query = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach($query['result'] AS $res){
        if($res['id'] == $pay){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function GetActivitiesShitType($type){
    $req = mysql_query("    SELECT  `id`,
                                    `name`
                            FROM    `work_activities_cat`
                            WHERE   `actived` = 1");

    $data .= '<option value="0" selected="selected">----</option>';
    while( $res = mysql_fetch_assoc($req)){
        if($res['id'] == $type){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function delete_activities($activities_id)
{
	GLOBAL $db;
    $db->setQuery("	UPDATE `work_activities`
					SET    `actived` = 0
					WHERE  `id` IN($activities_id)");
	$db->execQuery();
}

// returns activities rnge dialog structure
function get_activities_range_form($res = '')
{
    $data = '
	<div id="dialog-form">
			<input type="text" class="focus-receiver">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>
            <!-- ID -->
			<input type="hidden" id="activities_range_id" value="' . $res['id'] . '" />
	    	<table class="dialog-form-table">
			    <tr>
					<td style="width: 170px;"><label for="sctivities_range_start">დასაწყისი</label></td>
					<td>
						<input type="text" id="sctivities_range_start" value="' . $res['start'] . '" />
					</td>
				</tr>
			    <tr>
					<td style="width: 170px;"><label for="sctivities_range_end">დასასრული</label></td>
					<td>
						<input type="text" id="sctivities_range_end" value="' . $res['end'] . '" />
					</td>
				</tr>

			</table>
        </fieldset>
    </div>
    ';
    return $data;
}

function get_activities_range_data($range_id) {

	$query = mysql_query("SELECT id, start, end FROM work_activities_detail WHERE id = '$range_id'");
	$res = mysql_fetch_assoc($query);

	return $res;

}

function AddActivitiesRange($activ_range_id, $start, $end) {

    $user_id	= $_SESSION['USERID'];
    mysql_query("INSERT INTO 	 `work_activities_detail`
				 (`user_id`, `start`, `end`, `work_activities_id`)
				 VALUES
	             ('$user_id', '$start', '$end', '$activ_range_id')");

}

function SaveActivitiesRange($activ_range_id, $start, $end)
{
    $user_id	= $_SESSION['USERID'];
    mysql_query("	UPDATE `work_activities_detail`
					SET     `start`     = '$start',
                    	    `end`       = '$end'
					WHERE	`id`        = '$activ_range_id'");
}

function delete_activities_range($activities_range_id)
{
    mysql_query("	UPDATE `work_activities_detail`
					SET    `actived` = 0
					WHERE  `id` IN($activities_range_id)");
}

function get_add_hours_dialog() {

    return '<div class="add-new-hours-table-wrapper">
            	<table class="add-new-hours-table anht-week">
                <thead>
                    <tr>
                        <th>
                            <div class="anh-col anh-col-lefter">
                                <span>დღე</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>დასაწყისი</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>დასასრული</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>სთ</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>რ ბა</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>სულ სთ</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>24 სთ</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>არა სამუშაო</span>
                            </div>
                        </th>
                        <th>
                            <div class="anh-col">
                                <span>კოპი ზედა</span>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    '.add_new_hours_days_loop().'
                </tbody>
								<tfoot>
										<tr>
												<td>

												</td>
												<td>

												</td>
												<td>

												</td>
												<td>

												</td>
												<td>

												</td>
												<td>
														<span class="anh-all-time-sum-receiver">00:00</span>
												</td>
												<td>

												</td>
												<td>

												</td>
												<td>

												</td>
										</tr>
								</tfoot>
            </table>
        </div>';

}

// add new hours days loop
function add_new_hours_days_loop() {

    $daysArray = array("ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა");
    $structure = "";

    foreach($daysArray as $key => $value) {
        $structure .= "<tr data-day='".$key."'>
                    <td>
                        <span class='anh-day-name'>".$value."</span>
                    </td>
                    <td>
                        <div class='anh-input-wrapper'>
                            <input type='text' pattern='\d*' maxlength='2' name='anh_start_date_hr' class='nh-hr new-hours-start-hr' value='00'>
                            <input type='text' value=':' class='anh-colen' disabled>
                            <input type='text' pattern='\d*' maxlength='2' name='anh_start_date_mn' class='nh-mn new-hours-start-mn' value='00'>
                        </div>
                    </td>
                    <td>
                        <div class='anh-input-wrapper'>
                            <input type='text' pattern='\d*' maxlength='2' name='anh_end_date_hr' class='nh-hr new-hours-end-hr' value='00'>
                            <input type='text' value=':' class='anh-colen' disabled>
                            <input type='text' pattern='\d*' maxlength='2' name='anh_end_date_mn' class='nh-mn new-hours-end-mn' value='00'>
                        </div>
                    </td>
                    <td>
                        <span class='anh-dif-time-receiver'>00:00</span>
                    </td>
                    <td><input type='number' min='1' name='anh_hours_count' class='new-hours-input new-hours-count' value='1'></td>
                    <td>
                        <span class='anh-sum-time-receiver'>00:00</span>
                    </td>
                    <td>
                        <div class='new-hours-checkbox'>
                            <input type='checkbox' name='anh_check_full_day' class='anh-check-full-day'>
                        </div>
                    </td>
                    <td>
                        <div class='new-hours-checkbox'>
                            <input type='checkbox' name='anh_check_holiday' class='anh-check-holiday'>
                        </div>
                    </td>
                    <td>
                        <div class='new-hours-checkbox'>
                            ".($key == 0 ? "" : "<input type='checkbox' name='anh_copy_above' class='anh-copy-above'>")."
                        </div>
                    </td>
                </tr>";
    }

    return $structure;

}

// zero fixer function
function zeroFixer($value) {

	$result = 0;

	if($value < 10) {
		$result = "0".$value;
	} else {
		$result = $value;
	}

	return $result;

}

// add new hours days loop
function add_new_hours_day_structure($day, $query) {

			$structure = "";

			$data_count = mysql_num_rows($query);

			if($data_count > 0) {

				while ($res = mysql_fetch_assoc($query)) {

					$splited_start = explode(":",$res["start_time"]);
					$start_hour = $splited_start[0];
					$start_min = $splited_start[1];

					$splited_end = explode(":",$res["end_time"]);
					$end_hour = $splited_end[0];
					$end_min = $splited_end[1];

					if($end_hour != $start_hour) {
						$difference_hours = $end_hour > $start_hour ? $end_hour - $start_hour : $end_hour + (24 - $start_hour);
					} else {
						$difference_hours = 0;
					}

					if($end_min != $start_min) {
						$difference_minutes = $end_min - $start_min;
					} else {
						$difference_minutes = 0;
					}


					if($difference_minutes < 0) {
	            $difference_hours = $difference_hours - 1 < 0 ? 23 : $difference_hours - 1;
	            $difference_minutes = 60 + $difference_minutes;
	        }

					if($difference_minutes >= 60) {
	            $difference_hours .= floor($difference_minutes / 60);
	            $difference_minutes = $difference_minutes % 60;
	        }

					$counted_hours = $difference_hours * $res["count"];
					$counted_mionutes = $difference_minutes * $res["count"];

					if($counted_mionutes >= 60) {
	            $counted_hours .= floor($counted_mionutes / 60);
	            $counted_mionutes = $counted_mionutes % 60;
	        }

					if($difference_hours > 0 || $difference_minutes > 0) {

							$difference_hours = zeroFixer($difference_hours);
							$difference_minutes = zeroFixer($difference_minutes);

							$counted_hours = zeroFixer($counted_hours);
							$counted_mionutes = zeroFixer($counted_mionutes);

							$structure .= "<tr used>
					                    <td>
					                        <span class='anh-day-name'>".$day."</span>
					                    </td>
					                    <td>
					                        <div class='anh-input-wrapper'>
					                            <input type='text' pattern='\d*' maxlength='2' name='anh_start_date_hr' class='nh-hr new-hours-start-hr' value='".$start_hour."' data-type='start' disabled>
					                            <input type='text' value=':' disabled>
					                            <input type='text' pattern='\d*' maxlength='2' name='anh_start_date_mn' class='nh-mn new-hours-start-mn' value='".$start_min."' data-type='start' disabled>
					                        </div>
					                    </td>
					                    <td>
					                        <div class='anh-input-wrapper'>
					                            <input type='text' pattern='\d*' maxlength='2' name='anh_end_date_hr' class='nh-hr new-hours-end-hr' value='".$end_hour."' data-type='end' disabled>
					                            <input type='text' value=':' disabled>
					                            <input type='text' pattern='\d*' maxlength='2' name='anh_end_date_mn' class='nh-mn new-hours-end-mn' value='".$end_min."' data-type='end' disabled>
					                        </div>
					                    </td>
					                    <td>
					                        <span class='anh-dif-time-receiver'>".$difference_hours.":".$difference_minutes."</span>
					                    </td>
					                    <td><input type='number' min='1' name='anh_hours_count' class='new-hours-input new-hours-count' value='".$res["count"]."' disabled></td>
					                    <td>
					                        <span class='anh-sum-time-receiver'>".$counted_hours.":".$counted_mionutes."</span>
					                    </td>
					                    <td>
					                        <div class='new-hours-buttons'>
																			<button class='delete-hour-ind-day-row'>
							                            <i class='material-icons'>close</i>
							                        </button>
																			<button class='edit-hour-ind-day-row' data-status='edit'>
							                            <i class='material-icons'>mode_edit</i>
							                        </button>
					                        </div>
					                    </td>
					                </tr>";
								}
						}

			} else {
				$structure = "<tr class='empty-table-row'><td colspan='7'><span>ჩამონათვალი ცარიელია</span></td></tr>";
			}

      return $structure;

}

// get add hours individual days dialog structure
function get_hours_ind_day_dialog($index, $name, $query) {

	$data_count = mysql_num_rows($query);

	return '<div class="anh-header '.($data_count > 0 ? '' : 'anh-header-margin').'">
						<button id="addNewIndDayHourRow">
							<span>დამატება</span>
						</button>
					</div>
					<div class="add-new-hours-table-wrapper">
						<table class="add-new-hours-table anht-individual">
							<input type="hidden" id="newHoursIndDayId" value="'.$index.'" />
							<input type="hidden" id="newHoursIndDayName" value="'.$name.'" />
							<thead>
									<tr class="anht-header '.($data_count > 0 ? '' : 'hidden-data').'">
											<th>
													<div class="anh-col anh-col-lefter">
															<span>დღე</span>
													</div>
											</th>
											<th>
													<div class="anh-col">
															<span>დასაწყისი</span>
													</div>
											</th>
											<th>
													<div class="anh-col">
															<span>დასასრული</span>
													</div>
											</th>
											<th>
													<div class="anh-col">
															<span>სთ</span>
													</div>
											</th>
											<th>
													<div class="anh-col">
															<span>რ ბა</span>
													</div>
											</th>
											<th>
													<div class="anh-col">
															<span>სულ სთ</span>
													</div>
											</th>
											<th>
													<div class="anh-col">

													</div>
											</th>
									</tr>
							</thead>
							<tbody>
									'.add_new_hours_day_structure($name, $query).'
							</tbody>
							<tfoot>
									<tr>
											<td>

											</td>
											<td>

											</td>
											<td>

											</td>
											<td>

											</td>
											<td>

											</td>
											<td>
													<span class="anh-all-time-sum-receiver '.($data_count > 0 ? '' : 'hidden-data').'">00:00</span>
											</td>
											<td>

											</td>
									</tr>
							</tfoot>
					</table>
			</div>';

}

// returns existed shift list
function get_shift_list() {
	GLOBAL $db;
	$optins = "<option value='0'>-------------</option>";

	$db->setQuery("SELECT id, name FROM work_shift WHERE actived = 1");
	$query = $db->getResultArray();
	foreach($query['result'] AS $res) {

		$optins .= "<option value='".$res["id"]."'>".$res["name"]."</option>";
	}

	return $optins;

}

// returns work graphic added shift list structure
function get_wg_added_shift_list_structure($project_id, $year, $month, $row_id, $col_id) {
	GLOBAL $db;
	$structure = "";

	$db->setQuery("SELECT id FROM work_graphic 
									WHERE project_id = '$project_id'
										AND year = '$year'
										AND month = '$month'");
										
	$project_res = $db->getResultArray();
	$graphic_id = $project_res['result'][0]['id'];
	
	$db->setQuery("SELECT id FROM work_graphic_rows 
							   WHERE work_graphic_id = '$graphic_id'
								   AND row_id = '$row_id'
								   AND work_graphic_rows.actived = 1
								   ORDER BY id DESC
								LIMIT 1");
	$rows_res = $db->getResultArray();
	$rows_id = $rows_res['result'][0]['id'];

	$db->setQuery("SELECT work_graphic_cols.id,
									work_graphic_cols.cycle_id AS cycle_id,
									work_shift.name AS shift_name,
									work_shift.color AS shift_color	
								FROM work_graphic_cols
								JOIN work_shift ON work_shift.id = work_graphic_cols.work_shift_id
								WHERE work_graphic_rows_id = '$rows_id'
								AND col_id = '$col_id'
								AND work_graphic_cols.actived = 1");
	$query = $db->getResultArray();
	$data_count = $db->getNumRow();

	foreach($query['result'] AS $cols_res) {

		$structure .= '
			<li class="wg-slc-list-unit">
				<div class="wg-slc-name-holder" style="background:'.$cols_res["shift_color"].';" title="'.$cols_res["shift_name"].'">
					<span>'.$cols_res["shift_name"].'</span>
				</div>
				<div></div>
				<div>
					<button class="remove-shift-from-graphic" data-id="'.$cols_res["id"].'">
						<i class="material-icons">remove</i>
					</button>
				</div>
			</li>
		';

	}

	if($data_count > 0) {

		return '
			<div class="wg-shift-list-container">
				<h1>არსებული ცვლები:</h1>
				<ul class="wg-slc-ul">
					<li class="wg-slc-header">
						<div>ცვლა</div>
						<div></div>
						<div>წაშლა</div>
					</li>
					'.$structure.'
				</ul>
			</div>
		';

	} else {
		return '';
	}
}

// returns work planner added shift list structure
function get_wp_added_shift_list_structure($project_id, $row_id, $col_id) {
	GLOBAL $db;
	$structure = "";

	$db->setQuery("SELECT id FROM work_planner 
									WHERE project_id = '$project_id'");
										
	$project_res = $db->getResultArray();
	$graphic_id = $project_res['result'][0]['id'];
	
	$db->setQuery("SELECT id FROM work_planner_rows 
							   WHERE work_planner_id = '$graphic_id'
								   AND row_id = '$row_id'
								   AND work_planner_rows.actived = 1");
	$rows_res = $db->getResultArray();
	$rows_id = $rows_res['result'][0]['id'];

	$db->setQuery("SELECT work_planner_cols.id,
									work_planner_cols.cycle_id AS cycle_id,
									work_shift.name AS shift_name,
									work_shift.color AS shift_color	
								FROM work_planner_cols
								JOIN work_shift ON work_shift.id = work_planner_cols.work_shift_id
								WHERE work_planner_rows_id = '$rows_id'
								AND col_id = '$col_id'
								AND work_planner_cols.actived = 1");
	$query = $db->getResultArray();
	$data_count = $query['count'];

	foreach($query['result'] AS $cols_res) {

		$structure .= '
			<li class="wg-slc-list-unit">
				<div class="wg-slc-name-holder" style="background:'.$cols_res["shift_color"].';" title="'.$cols_res["shift_name"].'">
					<span>'.$cols_res["shift_name"].'</span>
				</div>
				<div></div>
				<div>
					<button class="remove-shift-from-planner" data-id="'.$cols_res["id"].'">
						<i class="material-icons">remove</i>
					</button>
				</div>
			</li>
		';

	}

	if($data_count > 0) {

		return '
			<div class="wg-shift-list-container">
				<h1>არსებული ცვლები:</h1>
				<ul class="wg-slc-ul">
					<li class="wg-slc-header">
						<div>ცვლა</div>
						<div></div>
						<div>წაშლა</div>
					</li>
					'.$structure.'
				</ul>
			</div>
		';

	} else {
		return '';
	}
}

// returns existed activities list
function get_activities_list() {
    global $db;
	$options = "<option value='0'>გასუფთავება</option>";
	$db->setQuery("SELECT id,
															 name,
															 color
											 FROM work_activities
											 WHERE actived = 1
										 	 GROUP BY name");
	
	$req = $db->getResultArray();

 foreach ($req[result] AS $res) {
	 $options .= "<option value='".$res["id"]."'>".$res["name"]."</option>";
 }

 return $options;

}

// returns planner shift modify dialog
function get_planer_shift_dialog($project_id, $row_id, $col_id) {

	return '
		<button id="plannerAddShift">
			ცვლის შექმნა
		</button>
		'.get_wp_added_shift_list_structure($project_id, $row_id, $col_id).'
		<div class="wg-shift-list-add">
			<h1>დაამატეთ ცვლა:</h1>
			<select id="plannerShiftShoose" class="jquery-ui-chosen">
				'.get_shift_list().'
			</select>
		</div>
		<div class="project-details-loading-container mini">
			<div>
				<i class="fa fa-cog"></i>
				<p>მიმდინარეობს მონაცემთა დამუშავება</p>
			</div>
		</div>
	';

}

// returns work graphic shift choose dialog
function get_wg_shift_dialog($project_id, $year, $month, $row_id, $col_id) {

	return '
		'.get_wg_added_shift_list_structure($project_id, $year, $month, $row_id, $col_id).'
		<div class="wg-shift-list-add">
			<h1>დაამატეთ ცვლა:</h1>
			<select id="wgShiftShoose" class="jquery-ui-chosen">
				'.get_shift_list().'
			</select>
			<textarea style="width: 100%; margin-top: 7px;" id="wgShift_textarea" placeholder="კომენტარი"></textarea>
		</div>
		<div class="project-details-loading-container mini">
			<div>
				<i class="fa fa-cog"></i>
				<p>მიმდინარეობს მონაცემთა დამუშავება</p>
			</div>
		</div>
	';

}

// returns planner add shift dialog
function get_planer_add_shift_dialog() {

	return '
		<table id="planerAddShiftTable">
			<thead>
				<tr>
					<td>
						<span>დასახელება</span>
					</td>
					<td colspan="2">
						<input type="text" id="plannerNewShiftName" />
					</td>
				</tr>
				<tr>
					<td>
						<span>დასაწყისი</span>
					</td>
					<td colspan="2">
						<input type="text" id="plannerNewShiftStart" class="jquery-ui-timepicker" />
					</td>
				</tr>
				<tr>
					<td>
						<span>დასასრული</span>
					</td>
					<td colspan="2">
						<input type="text" id="plannerNewShiftEnd" class="jquery-ui-timepicker" />
					</td>
				</tr>
				<tr>
					<td>
						<span>შესვენების ხ ბა</span>
					</td>
					<td colspan="2">
						<input type="text" id="plannerNewShiftBreak" class="jquery-ui-timepicker" />
					</td>
				</tr>
				<tr>
					<td>
						<span>ცვლის ხ ბა</span>
					</td>
					<td>
						<input type="text" id="plannerNewShiftFullDur" style="width: 55px;" readonly/>
					</td>
					<td>
						<span style="font-family: pvn; font-weight: bold; color: #7f8c8d; margin-right: 6px; position: relative; top: 2px;">სამ. ხ ბა</span>
						<input type="text" id="plannerNewShiftWorkDur" style="width: 55px; float: right; position: relative; right: -2px;" readonly/>
					</td>
				</tr>
				<tr>
					<td>
						<span>ფერი:</span>
					</td>
					<td>
						<input type="color" id="plannerNewShiftColor"/>
					</td>
					<td>
						<button id="plannerShiftAmply" class="open">
							ვრცლად
						</button>
					</td>
				</tr>
			</thead>
			<tbody id="plannerShiftAddTableBody" class="psa-hidden">
				<tr>
				<td><span>ანაზღაურება</span></td>
				<td colspan="2">
					<select id="plannerNewShiftPayment">'.GetWorkshiftPay().'</select>
				</td>
			</tr>
			<tr>
				<td style="width: 170px;"><span>სამუშაო ტიპი</span></td>
				<td colspan="2">
					<select id="plannerNewShiftType">'.GetWorkShitType().'</select>
				</td>
			</tr>
			<tr>
				<td>
					<span>კომენტარი</span>
				</td>
				<td colspan="2">
					<textarea  id="plannerNewShiftComment" style="resize: vertical;"></textarea>
				</td>
			</tr>
			</tbody>
		</table>
	';

}

function get_workShift_cycle_start_pos_structure($days_in_month, $shift_count, $user) {

	// make simple days list
	$day_option = '<option value="0" selected>აირჩიე რიცხვი</option>';
	$queu_option = '<option value="0" selected>არჩიე ცვლის რიგითობა</option>';

	for($i = 0; $i < $days_in_month; $i++) {

		$output = $i + 1;
		$day_option .= '<option value="'.$output.'">'.zeroFixer($output).'</option>';
	}

	for($j = 0; $j < $shift_count; $j++) {

		$output = $j + 1;
		$queu_option .= '<option value="'.$output.'">'.$output.'</option>';
	}

	// define structure maker variable
	return '
			<div style="width: 100%; height: auto;">
				<select id="wgCycleSTartFromDate" style="width: 100%;">
					'.$day_option.'
				</select>
			</div>
			<div style="width: 100%; height: auto; margin-top: 7px;">
				<select id="wgCycleSTartFromQueu" style="width: 100%;">
					'.$queu_option.'
				</select>
			</div>
			<div style="width: 100%; height: auto; margin-top: 7px;">
				<input type="checkbox" id="continueFromPrevMonth" style="float: left;"/>
				<label for="continueFromPrevMonth" style="position:relative; top:2px;">გააგრძელე წინა თვიდან</label>
			</div>
        <input type="text" id="hide_this_user_id" style="float: left; display:none" value="'.$user.'"/>
	';

}

// returns 24 hour table header structure
function table_in_24_hour_head_structure() {

	$structure = '';
	$minutes = '';
	$hours = '';

	for($m = 0; $m < 60; $m += 5) {

		$minutes .= '
			<div class="minute-col">
				'.$m.'
			</div>
		';

	}

	for($i = 0; $i < 24; $i++) {

		$hours = zeroFixer($i).':00';

		$structure .= '
			<div class="col graphic-col child-div-flex">
				<div>'.$hours.'</div>
				<div>
					'.$minutes.'
				</div>
			</div>
		';

	}

	return $structure;


}

// returns day graphic value table header structure (table layout)
function wg_dt_value_table_head_structure() {
    global $db;
	$data = array(
		"structure" => "",
		"count" => 0
	);

	$query = $db->setQuery("SELECT id,
															 name,
															 color
									 			FROM work_activities
												GROUP BY name");

	$data["count"] = $db->getNumRow();
	$query = $db->getResultArray();
	foreach($query[result] AS $r) {

		$color_block_styles = 'width: 12px; height: 12px; margin-left: 9px; background: '.$r["color"];

		$data["structure"] .= '<div class="col table-col child-div-flex" data-id="'.$r["id"].'">
														<div>
															<span>'.$r["name"].'</span>
															<div style="'.$color_block_styles.'"></div>
														</div>
													</div>';

	}

	return $data;


}

// returns day graphic value table futter columns structure
function wg_dg_value_table_footcol_structure($type) {

	$structure = '';

	for($i = 0; $i < 288; $i++) {

		$hours = zeroFixer($i).':00';

		$structure .= '
			<div class="col" data-type="'.$type.'" data-col="'.$i.'" data-status="one">
				0
			</div>
		';

	}

	return $structure;


}

// returns rush hours data in array
function date_to_rush_hours($project_id, $date) {

	list($d, $m, $y) = explode(".", $date);

	$date_format = $y."-".$m."-".$d;
	$time = strtotime($date_format);

	$day_index = date('w', $time) - 1;
	$day_index = $day_index < 0 ? $day_index = 6 : $day_index;

	$week_index = date('W', $time) - 1;

	$rush_hours = array();

	$query = mysql_query("SELECT wfm_rush_hours_rows.start,
															 wfm_rush_hours_rows.end
												FROM wfm_rush_hours_rows
												JOIN wfm_rush_hours ON wfm_rush_hours.id = wfm_rush_hours_rows.wfm_rush_hours_id
												WHERE wfm_rush_hours.actived = 1 AND wfm_rush_hours.project_id = '$project_id'
																												 AND wfm_rush_hours.year = '$y'
																												 AND wfm_rush_hours.week_num = '$week_index'
																												 AND wfm_rush_hours_rows.row = '$day_index'");
 while($res = mysql_fetch_assoc($query)) {

	 $data_unit = array(
		 "start" => (int)$res["start"],
		 "end" => (int)$res["end"]
	 );

	 array_push($rush_hours, $data_unit);

 }

	return $rush_hours;

}

// returns activitie list dialog structure
function get_activitie_dialog() {

	return '
		<div class="shift-controller-head">
			<select id="chooseDayGraphicActivitie" class="jquery-ui-chosen">
				'.get_activities_list().'
			</select>
		</div>
	';

}

// returns rush hours status select structure
function rush_hours_select_structure() {

	return '<select id="choseRushHourStatus" class="jquery-ui-chosen">
						<option value="0">გასუფთავება</option>
						<option value="1" selected>მონიშვნა</option>
					</select>';

}

// returns work graphic edit history structure
function get_work_graphic_edit_history_structure() {

	return '
	<div style="height:20px;"></div>
	<table class="display" id="work_graphic_edit_history_table">
							<thead>
									<tr id="datatable_header">
											<th style="width: 1%;">ID</th>
											<th style="width: 27%;">მოქმედების დრო</th>
											<th style="width: 26%;">მოქმედების ავტორი</th>
											<th style="width: 26%;">მოქმედების დასახელება</th>
											<th style="width: 10%;"> ცვლა</th>
											<th style="width: 10%;"> ციკლი</th>
											<th style="width: 10%;"> კომენტარი</th>
									</tr>
							</thead>
							<thead>
									<tr class="search_header">
											<th style="display:none;">
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
											<th>
													<input type="text" name="search_category" value="ფილტრი" class="search_init" />
											</th>
									</tr>
							</thead>
					</table>';

}

function  get_shift_processed_list($cycle_id) {
    global $db;
	$shift_array = array();

	$db->setQuery(" SELECT work_shift.id,
							work_cycle_detail.num
					FROM work_cycle
					JOIN work_cycle_detail ON work_cycle_detail.work_cycle_id = work_cycle.id AND work_cycle_detail.actived = 1
					JOIN work_shift ON work_shift.id = work_cycle_detail.work_shift_id AND work_shift.actived = 1
					WHERE work_cycle.id = '$cycle_id'
					ORDER BY work_cycle_detail.num");

	$shift_array_count = $db->getNumRow();
	$saved_num = "";
	$counter = 0;
	$shift_array_query = $db->getResultArray();
	foreach ($shift_array_query[result] AS $res) {
		
		if($res["num"] != $saved_num) {
			$shift_array[$counter] = array();
			array_push($shift_array[$counter], $res["id"]);
			
			$saved_num = $res["num"];
			$counter++;
		} else {
			array_push($shift_array[$counter - 1], $res["id"]);
		}

	}

	return $shift_array;

}

// checks if project has specific holiday
function check_if_project_has_holiday($project_id, $holiday_id) {
    global $db;
	$db->setQuery(" SELECT id FROM project_holiday
					WHERE project_id = '$project_id'
						AND holidays_id = '$holiday_id'
						AND actived = 1");

	$count = $db->getNumRow();

	if($count > 0) {
		return false;
	} else {
		return true;
	}

}

// returns count of official holidays of query
function project_official_holidays_count($project_id) {
    global $db;
    $db->setQuery("SELECT project_holiday.`id`
    				FROM project_holiday
    				JOIN holidays ON holidays.id = project_holiday.holidays_id
    				JOIN holidays_category ON holidays_category.id = holidays.holidays_category_id
    				WHERE holidays_category.name = 'ოფიციალური დასვენება'
    					AND holidays.actived = 1
    					AND project_holiday.actived = 1
    					AND project_holiday.project_id = '$project_id'");

	$count = $db->getNumRow();

	return $count;

}


// returns shift edit status by row id and column id
function get_shift_edit_status($row_id) {
global $db;
	// define data collector
	$data_collector = array();

	// query the data
	$db->setQuery("SELECT col_id, COUNT(col_id) AS data_count 
							FROM (SELECT `work_graphic_cols`.`col_id`
								FROM `work_graphic_cols`
								JOIN `work_graphic_cols_log` ON `work_graphic_cols_log`.`work_graphic_cols_id` = `work_graphic_cols`.`id`
								JOIN `work_graphic_rows` ON `work_graphic_rows`.`id` = `work_graphic_cols`.`work_graphic_rows_id`
								WHERE `work_graphic_cols`.`work_graphic_rows_id` = '$row_id'
									AND `work_graphic_rows`.`actived` = 1
								GROUP BY `work_graphic_cols`.`col_id`, `work_graphic_cols_log`.`action_time`) AS all_data
							GROUP BY col_id");
	$req = $db->getResultArray();		
	// loop throw result data
	foreach($req[result] AS $res) {

		// collect the data if column edit count is more then 1
		if($res["data_count"] > 1) {
			array_push($data_collector, $res["col_id"]);
		}

	}
	
	// return collectesd data
	return $data_collector;
	
}

// returns last inserted id i9n specific table
function get_last_inserted_id($table_name) {
	GLOBAl $db;
	$db->setQuery("SELECT max(id) AS last_id FROM ".$table_name);

	$res = $db->getResultArray();
	$last_inserted_id = $res['result'][0]['last_id'];
	
	return $last_inserted_id;

}

// checks work shift before save in cycle list
function check_work_shift_before_cycle_sibling_save($cycle_id, $save_shift_id, $save_shift_num, $editing_id) {
	GLOBAL $db;
	// get save shift data
	$save_shift_data = get_work_shift_data_by_id($save_shift_id);
	$save_shift_start_time = $save_shift_data["start_date"];
	$save_shift_end_time = $save_shift_data["end_date"];
	$save_shift_in_one_day = $save_shift_start_time < $save_shift_end_time;

	// make query
	$db->setQuery("SELECT 	work_cycle_detail.id,
									work_cycle_detail.work_shift_id,
									work_cycle_detail.num,
									work_shift.start_date,
									work_shift.end_date
							FROM 	work_cycle_detail
							JOIN 	work_shift ON work_shift.id = work_cycle_detail.work_shift_id
							WHERE 	work_cycle_detail.work_cycle_id = '$cycle_id'
								AND work_cycle_detail.actived = 1
							ORDER BY work_cycle_detail.num");
	$query = $db->getResultArray();
	// loop throw query result
	foreach($query['result'] AS $res){

		// define existed shift data
		$eexisting_data_id = $res["id"];
		$existed_shift_id = $res["work_shift_id"];
		$existed_shift_num = $res["num"];
		$existed_shift_start_time = $res["start_date"];
		$existed_shift_end_time = $res["end_date"];
		$existed_shift_in_one_day = $existed_shift_start_time < $existed_shift_end_time;


		// check for previews day
		if($existed_shift_num == ($save_shift_num - 1)) {

			// check on existed shift end time
			if(!$existed_shift_in_one_day && ($existed_shift_end_time > $save_shift_start_time)) {
				return "previewsDayEnding";
			}
			
		}

		// check for same day
		if($existed_shift_num == $save_shift_num) {

			// check for same id
			if($save_shift_id == $existed_shift_id) {
				return "sameId";

			// check for existing and saving is not in same day
			} else if((!$save_shift_in_one_day && !$existed_shift_in_one_day && $eexisting_data_id != $editing_id)) {
				return "sameDayTimeCross";

			// check for same day
			} else if($save_shift_in_one_day && $existed_shift_in_one_day) {

				// check for same start and end time
				if(($save_shift_start_time >= $existed_shift_start_time && $save_shift_end_time <= $existed_shift_end_time) && $eexisting_data_id != $editing_id) {
					return "sameDayTimeCross";

				// check for existed time over
				} else if(($save_shift_start_time < $existed_shift_start_time && $save_shift_end_time > $existed_shift_end_time) && $eexisting_data_id != $editing_id) {
					return "sameDayTimeCross";

				// check for existed start time
				} else if((($save_shift_end_time < $existed_shift_end_time && $save_shift_end_time > $existed_shift_start_time) && $save_shift_start_time <= $existed_shift_start_time) && $eexisting_data_id != $editing_id) {
					return "sameDayTimeCross";

				// check for existed end time
				} else if((($save_shift_start_time > $existed_shift_start_time && $save_shift_start_time < $existed_shift_end_time) && $save_shift_end_time >= $existed_shift_end_time) && $eexisting_data_id != $editing_id) {
					return "sameDayTimeCross";
				}

			// check for existed is not in same day
			} else if($save_shift_in_one_day && !$existed_shift_in_one_day) {

				// check for existed start time
				if(($save_shift_end_time > $existed_shift_start_time) && $eexisting_data_id != $editing_id) {
					return "sameDayTimeCross";
				}

			// check for saving is not in same day
			} else if(!$save_shift_in_one_day && $existed_shift_in_one_day) {

				// check for existed end time
				if(($save_shift_start_time < $existed_shift_end_time) && $eexisting_data_id != $editing_id) {
					return "sameDayTimeCross";
				}

			}

		}

		// check for next day
		if($existed_shift_num == ($save_shift_num + 1)) {

			// check for saveing shift end time
			if(!$save_shift_in_one_day && ($save_shift_end_time > $existed_shift_start_time)) {
				return "nextDayStarting";
			}

		}

	}

	// get data count
	$data_count = $query['count'];

	// check for first and last shift
	if($data_count > 1) {

		// define first shift data
		$first_shift_data = get_work_cycle_first_shift($cycle_id);
		$first_shift_detail_id = $first_shift_data["id"];
		$first_shift_start_time = $first_shift_data["start_date"];


		// define last shift data
		$last_shift_data = get_work_cycle_last_shift($cycle_id);
		$last_shift_detail_id = $last_shift_data["id"];
		$last_shift_num = $last_shift_data["num"];


		// check for first shift
		if($editing_id == $first_shift_detail_id) {

			// last shift details
			$last_shift_start_time = $last_shift_data["start_date"];
			$last_shift_end_time = $last_shift_data["end_date"];
			$last_shift_in_one_day = $last_shift_start_time < $last_shift_end_time;

			// check if first and last shift times no cross eachoter
			if(!$last_shift_in_one_day && ($last_shift_end_time > $first_shift_start_time)) {
				return "firstShiftStarting";
			}

		// check for last shift
		} else if($editing_id == $last_shift_detail_id || $save_shift_num >= $last_shift_num) {

			// check if first and last shift times no cross eachoter
			if(!$save_shift_in_one_day && ($save_shift_end_time > $first_shift_start_time)) {
				return "firstShiftStarting";
			}

		}

	}

}

// returns simple operators list
function get_operators_list() {
	GLOBAL $db;
    $db->setQuery("	SELECT users.id, `user_info`.`name` AS `username` FROM users
					LEFT JOIN user_info ON user_info.user_id = users.id
					WHERE users.actived = 1");
	$options = '';
	$query = $db->getResultArray();
    foreach($query['result'] AS $res) {
        $options .= '<option id="rushHoursCurrentYear" value="'.$res["id"].'">'.$res["username"].'</option>';
    }

    return $options;

}

// returns simple activitie list
function get_multy_activitie_list() {
    global $db;
    $db->setQuery("SELECT id, name FROM work_activities WHERE actived = 1");
    $options = '';
    $query = $db->getResultArray();
    foreach($query[result] AS $res) {
      $options .= '<option value="'.$res["id"].'">'.$res["name"].'</option>';
    }

    return $options;
}

function date_to_string_date($datetime) {

    list($date, $time) = explode(" ", $datetime);
    list($d, $m, $y) = explode("-", $date);

    return $d.".".$m.".".$y;

}

function get_reposrt_planned_time($user_id, $activitie_id, $date_start, $date_end) {

    // $date_start = date_to_string_date($date_start);
    // $date_end = date_to_string_date($date_end);

    // $data_collector = 0;

    // $query = mysql_query("SELECT end - start AS planned_hours
    //                         FROM work_graphic
    //                         JOIN work_graphic_rows ON work_graphic.id = work_graphic_rows.work_graphic_id
    //                         JOIN work_graphic_row_activities ON work_graphic_rows.row_num = work_graphic_row_activities.row_num AND work_graphic_rows.date = work_graphic_row_activities.date
    //                         WHERE work_graphic.operator_id = '$user_id'
    //                             AND work_graphic_row_activities.activitie_id = '$activitie_id'
    //                             AND work_graphic_row_activities.date BETWEEN '$date_start' AND '$date_end'
    //                             AND work_graphic_row_activities.actived = 1
    //                         GROUP BY work_graphic_row_activities.id");


    // while($res = mysql_fetch_assoc($query)) {
    //     $data_collector += ($res["planned_hours"] + 1);
	// }
	
	$shift_id = get_shift_id($user_id);
    
    $query = "SELECT TIME_TO_SEC(work_shift.timeout) AS shift_timeout
    FROM work_shift
	WHERE work_shift.id = '$shift_id'";
	
	$res = mysql_fetch_assoc(mysql_query($query));
		// echo($query);
		$result = $res['shift_timeout'];
		if($result ==''){$result = 0;}
    return $result;

}

function get_shift_id($user_id) {
    $res = mysql_fetch_assoc(mysql_query("SELECT work_graphic_cols.work_shift_id AS `id`
                        FROM work_graphic_cols
                        LEFT JOIN work_graphic_rows ON work_graphic_cols.work_graphic_rows_id = work_graphic_rows.id
                        LEFT JOIN work_graphic ON work_graphic.id = work_graphic_rows.work_graphic_id
                        WHERE work_graphic_cols.actived = 1 AND work_graphic_rows.operator_id = '$user_id' AND work_graphic_cols.col_id = Day(NOW())-1 AND work_graphic.actived = 1 AND work_graphic_rows.actived = 1
       LIMIT 1 "));

        return $res['id'];
}



function get_times($operator, $date_start, $date_end) {
	$data = array();
	$res = mysql_query("SELECT SUM(`sec`) AS `sec` , 'work_time' AS `key` FROM (
																				SELECT ABS(TIME_TO_SEC(TIMEDIFF(  TIMEDIFF(work_shift.end_date,work_shift.start_date ) , work_shift.timeout  ) ))  AS `sec`, 'work_time' AS `key`
																				FROM work_shift 
																				JOIN work_graphic_cols ON work_graphic_cols.work_shift_id = work_shift.id AND work_graphic_cols.actived = 1
																				JOIN work_graphic_rows ON work_graphic_rows.id = work_graphic_cols.work_graphic_rows_id AND work_graphic_rows.actived = 1
																				JOIN work_activities_log ON work_activities_log.user_id = work_graphic_rows.operator_id
																				JOIN work_graphic ON work_graphic.id = work_graphic_rows.work_graphic_id
																				WHERE work_graphic_rows.operator_id = $operator 
																				AND `work_graphic_cols`.col_id = DAY ( '$date_start' ) - 1 
																				AND work_graphic.`month` = MONTH ( '$date_start' ) 
																				AND  work_activities_log.start_datetime BETWEEN '$date_start' 
																				AND '$date_end' 
																				GROUP BY DAY(work_activities_log.start_datetime)
																				) AS x
						
						UNION ALL
						
						SELECT IFNULL(SUM(TIME_TO_SEC( TIMEDIFF (users_log.logout,users_log.datatime )) ),
										TIME_TO_SEC( TIMEDIFF (NOW(), users_log.datatime ) ))    AS `sec`, 'real_work_time' AS `key`
						FROM users_log 
						WHERE users_log.user_id = $operator AND users_log.datatime BETWEEN '$date_start' 
						AND '$date_end' 

						UNION ALL

						SELECT	CASE	
								WHEN ISNULL( work_activities_log.end_datetime ) THEN  'გასულია' 
								ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
						END AS `sec`, 'break_time_real' `key`
						FROM work_activities_log
						WHERE `user_id` = $operator AND  work_activities_log.start_datetime BETWEEN '$date_start' 
						AND '$date_end' 

						UNION ALL

						SELECT SUM(`break_time`) AS `sec`, 'break_time' AS `key` FROM ( SELECT  ( work_graphic_activities.END - work_graphic_activities.START ) * 300  AS `break_time`
						FROM work_graphic_activities 
						JOIN work_graphic_rows ON work_graphic_rows.id = work_graphic_activities.row_id 
						JOIN `work_graphic_cols` ON `work_graphic_cols`.work_graphic_rows_id = work_graphic_rows.id
						WHERE work_graphic_rows.operator_id = $operator AND work_graphic_activities.actived = 1  AND `work_graphic_cols`.col_id = DAY(NOW()) - 1
						AND work_graphic_activities.activitie_id <> 0
						GROUP BY work_graphic_activities.activitie_id ) AS x

	");

	while($arr = mysql_fetch_assoc($res)){
		// var_dump($arr['key']);
		// switch($arr['key']){
		// 	case "real_work_time": $data["real_work_time"] = $arr['sec'];break;
		// 	case "work_time": $data["work_time"] = $arr['sec'];break;
		// }
		$data[$arr['key']] = $arr['sec'];
	}

	return $data;


}




function get_times_all($operator, $date,$activitie) {
	$data = array();
	$res = mysql_query("SELECT SUM(`sec`) AS `sec` , 'work_time' AS `key` FROM (
																				SELECT ABS(TIME_TO_SEC(TIMEDIFF(  TIMEDIFF(work_shift.end_date,work_shift.start_date ) , work_shift.timeout  ) ))  AS `sec`, 'work_time' AS `key`
																				FROM work_shift 
																				JOIN work_graphic_cols ON work_graphic_cols.work_shift_id = work_shift.id AND work_graphic_cols.actived = 1
																				JOIN work_graphic_rows ON work_graphic_rows.id = work_graphic_cols.work_graphic_rows_id AND work_graphic_rows.actived = 1
																				JOIN work_activities_log ON work_activities_log.user_id = work_graphic_rows.operator_id
																				JOIN work_graphic ON work_graphic.id = work_graphic_rows.work_graphic_id
																				WHERE work_graphic_rows.operator_id = $operator 
																				AND `work_graphic_cols`.col_id = DAY ( '$date' ) - 1 
																				AND work_graphic.`month` = MONTH ( '$date' ) 
																				AND  work_activities_log.start_datetime >= DATE('$date') 
																			
																				GROUP BY DAY(work_activities_log.start_datetime)
																				) AS x
						
						UNION ALL

						SELECT TIME_TO_SEC(`sec`) AS `sec` , 'income' AS `key` FROM (
																				SELECT work_shift.end_date   AS `sec`, 'income' AS `key`
																				FROM work_shift 
																				JOIN work_graphic_cols ON work_graphic_cols.work_shift_id = work_shift.id AND work_graphic_cols.actived = 1
																				JOIN work_graphic_rows ON work_graphic_rows.id = work_graphic_cols.work_graphic_rows_id AND work_graphic_rows.actived = 1
																				JOIN work_activities_log ON work_activities_log.user_id = work_graphic_rows.operator_id
																				JOIN work_graphic ON work_graphic.id = work_graphic_rows.work_graphic_id
																				WHERE work_graphic_rows.operator_id = $operator 
																				AND `work_graphic_cols`.col_id = DAY ( '$date' ) - 1 
																				AND work_graphic.`month` = MONTH ( '$date' ) 
																				AND  DATE(work_activities_log.start_datetime) = DATE('$date') 
																			
																				GROUP BY DAY(work_activities_log.start_datetime)
																				) AS x
						
						UNION ALL

						SELECT TIME_TO_SEC(`sec`) AS `sec` , 'out' AS `key` FROM (
																				SELECT work_shift.start_date   AS `sec`, 'out' AS `key`
																				FROM work_shift 
																				JOIN work_graphic_cols ON work_graphic_cols.work_shift_id = work_shift.id AND work_graphic_cols.actived = 1
																				JOIN work_graphic_rows ON work_graphic_rows.id = work_graphic_cols.work_graphic_rows_id AND work_graphic_rows.actived = 1
																				JOIN work_activities_log ON work_activities_log.user_id = work_graphic_rows.operator_id
																				JOIN work_graphic ON work_graphic.id = work_graphic_rows.work_graphic_id
																				WHERE work_graphic_rows.operator_id = $operator 
																				AND `work_graphic_cols`.col_id = DAY ( '$date' ) - 1 
																				AND work_graphic.`month` = MONTH ( '$date' ) 
																				AND  DATE(work_activities_log.start_datetime) = DATE('$date') 
																			
																				GROUP BY DAY(work_activities_log.start_datetime)
																				) AS x
						
						UNION ALL
						
						SELECT IFNULL(SUM(TIME_TO_SEC( TIMEDIFF (users_log.logout,users_log.datatime )) ),
										TIME_TO_SEC( TIMEDIFF (NOW(), users_log.datatime ) ))    AS `sec`, 'real_work_time' AS `key`
						FROM users_log 
						WHERE users_log.user_id = $operator AND users_log.datatime >= DATE('$date') 
						

						UNION ALL

						SELECT	CASE	
								WHEN ISNULL( work_activities_log.end_datetime ) THEN  'გასულია' 
								ELSE SUM( TIME_TO_SEC( TIMEDIFF( work_activities_log.end_datetime, work_activities_log.start_datetime ) ) ) 
						END AS `sec`, 'break_time_real' `key`
						FROM work_activities_log
						WHERE `user_id` = $operator AND  DATE(work_activities_log.start_datetime) = DATE('$date') 
						AND work_activities_log.work_activities_id = $activitie
						

						UNION ALL

						SELECT SUM(`break_time`) AS `sec`, 'break_time' AS `key` FROM ( SELECT  ( work_graphic_activities.END - work_graphic_activities.START ) * 300  AS `break_time`
						FROM work_graphic_activities 
						JOIN work_graphic_rows ON work_graphic_rows.id = work_graphic_activities.row_id 
						JOIN `work_graphic_cols` ON `work_graphic_cols`.work_graphic_rows_id = work_graphic_rows.id
						WHERE work_graphic_rows.operator_id = $operator AND work_graphic_activities.actived = 1  AND `work_graphic_cols`.col_id = DAY(NOW()) - 1
						AND work_graphic_activities.activitie_id = $activitie
						GROUP BY work_graphic_activities.activitie_id ) AS x


						UNION all

						SELECT TIME_TO_SEC(TIME(datatime)) AS `sec`, 'income_real' AS `key` FROM `users_log` where `user_id` = $operator
								AND DATE(datatime) = DATE('$date') 

						UNION ALL 

						SELECT IFNULL(TIME_TO_SEC(TIME(logout)), 'პროგრამაშია' ) AS `sec`, 'out_real' AS `key` FROM `users_log` where `user_id` = $operator
								AND DATE(datatime) = DATE('$date') 	
						
						UNION ALL
						
						SELECT IFNULL(TIME_TO_SEC(TIME(logout) - TIME(datatime)), 'პროგრამაშია' ) AS `sec`, 'all_time' AS `key` FROM `users_log` where `user_id` = $operator
								AND DATE(datatime) = DATE('$date') 															

	");

	while($arr = mysql_fetch_assoc($res)){
		// var_dump($arr['key']);
		// switch($arr['key']){
		// 	case "real_work_time": $data["real_work_time"] = $arr['sec'];break;
		// 	case "work_time": $data["work_time"] = $arr['sec'];break;
		// }
		$data[$arr['key']] = $arr['sec'];
	}

	return $data;


}


?>
