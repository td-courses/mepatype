<?php

/*
 *
 * Get css imports
 *
 */


function GetCss(){
	$data = '
	<!-- Main -->
	<link rel="stylesheet" href="media/css/flashPanel/flashpanel.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_forms.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_inner_tables.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_loading.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_pos.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_seoy.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_table.css" type="text/css" />
	<link rel="stylesheet" href="media/css/datatable_tree.css" type="text/css" />
	<link rel="stylesheet" href="media/css/chosen.min.css" type="text/css" />
	<link rel="stylesheet" href="media/css/jquery.autocomplete.css" type="text/css" />
	<link rel="stylesheet" href="js/CLeditor/jquery.cleditor.css" type="text/css" />

	<!-- Plugins -->
	<link rel="stylesheet" href="media/css/TableTools_JUI.css" type="text/css" />
	<link rel="stylesheet" href="media/css/jquery-ui-timepicker-addon.css" type="text/css" />
	<link rel="stylesheet" href="media/css/jNotify.jquery.css" type="text/css" />
	<link rel="stylesheet" href="media/css/tinyeditor.css" type="text/css" />
	
	<!-- jQuery UI -->
	<link rel="stylesheet" href="media/themes/jQuery-UI/smoothness/jquery-ui.min.css" type="text/css" />
	<link rel="stylesheet" href="media/themes/jQuery-UI/smoothness/jquery-ui.datatable.css" type="text/css" />
	<link rel="stylesheet" href="dist/css/production/main.css?v=1.0" type="text/css" />
	<link rel="stylesheet" href="dist/css/material-design-iconic-font.min.css" type="text/css" />
	<link rel="stylesheet" href="vendors/bootstrap/bootstrap.css">

	<!-- KENDO -->
	<link rel="stylesheet" href="media/css/kendoUI/kendo.common.min.css">
	<link rel="stylesheet" href="media/css/kendoUI/kendo.default.min.css">
	
	<!-- NEW Models -->
	<link rel="stylesheet" href="media/css/pages/styles.css">

	';
	return $data;
}

/*
 *
 * Get javascript imports
 *
 */
function GetJs(){
    $rand = rand();
	$data = '   <script type="text/javascript" language="javascript" src="js/jquery-1.9.1.min.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery.ui.datepicker-ka.min.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
            	<script type="text/javascript" language="javascript" src="js/TableTools.min.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery.dataTables.columnFilter.js"></script>
            	<script type="text/javascript" language="javascript" src="js/ColReorderWithResize.js"></script>
            	<script type="text/javascript" language="javascript" src="js/dataTables.action.js?v='.$rand.'"></script>
	            <script type="text/javascript" language="javascript" src="js/jquery.autocomplete.js?v='.$rand.'"></script>
	            <script type="text/javascript" language="javascript" src="js/jquery.form.min.js?v='.$rand.'"></script>
            	
            	<!-- Plugins -->
            	<script type="text/javascript" language="javascript" src="js/jquery-ui-timepicker-addon.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery-ui-timepicker-ka.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery-ui-sliderAccess.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery.cookie.js"></script>
            	<script type="text/javascript" language="javascript" src="js/jquery.hoverIntent.minified.js"></script>
            	
            	<script type="text/javascript" language="javascript" src="js/ajaxfileupload.js"></script>
                
				<script type="text/javascript" language="javascript" src="js/phpjs.js"></script>
				<script type="text/javascript" language="javascript" src="js/default.js?v='.$rand.'"></script>
				
				<script type="text/javascript" language="javascript" src="js/comunication.js?v='.$rand.'"></script>
            	
            	<script type="text/javascript" src="js/jNotify.jquery.min.js"></script>
                <script type="text/javascript" src="js/tinymce.min.js"></script>
                <script type="text/javascript" src="js/chosen.jquery.min.js"></script>
                <script type="text/javascript" src="js/dataTables.colVis.js"></script>	    
                <script type="text/javascript" src="js/jquery.session.js"></script>
				<script type="text/javascript" src="js/tinymce.min.js"></script>
				<script type="text/javascript" src="js/CLeditor/jquery.cleditor.js"></script>
				<script type="text/javascript" src="js/CLeditor/jquery.cleditor.min.js"></script>
				<script type="text/javascript" src="js/moment.js"></script>
				<script src="vendors/bootstrap/bootstrap.js"></script>

				<script src="js/index.min.js"></script>
				<script src="vendors/ckeditor/ckeditor.js"></script>

				<!-- KENDO -->

				<script type="text/javascript" language="javascript" src="js/kendoUI/jszip.min.js"></script>
				<script type="text/javascript" language="javascript" src="js/kendoUI/kendo.all.min.js"></script>
				<script type="text/javascript" language="javascript" src="js/kendoUI/kendo.main.class.js"></script>
				<script type="text/javascript" language="javascript" src="js/kendoUI/pako_deflate.min.js"></script>

                <!-- <script type="text/javascript" language="javascript" src="js/highcharts.js"></script> -->
                ';
	return $data;
}
