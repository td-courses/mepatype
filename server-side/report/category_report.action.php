<?php
ini_set('display_errors', 'On');
error_reporting(E_ERROR);
// MySQL Connect Link
require_once('../../includes/classes/class.Mysqli.php');

    //----------------------------- ცვლადი
    $start_time = $_REQUEST['start_time'];
    $end_time 	= $_REQUEST['end_time'];
    $day = (strtotime($end_time)) -  (strtotime($start_time));
    $day_format = round(($day / (60*60*24)) + 1);
    $act = $_REQUEST['act'];
    global $db;
    $db = new dbClass();

    // ----------------------------------
    switch($act){
        case 'get_edit_page':
            $id         = $_REQUEST['id'];
            $page		= GetPage($id);
            $data		= array('page'	=> $page, 'check' => $check);
            $data['error'] = '';
        break;
        case 'get_list_dialog':
            $count	= $_REQUEST['count'];
            $hidden	= $_REQUEST['hidden'];
            $id     = $_REQUEST['id'];
           $db->setQuery("SELECT client_vehicle_catalog.id, client_vehicle_catalog.model_make_display,
                                            client_vehicle_catalog.model_name, client_vehicle_catalog.model_trim, client_vehicle_catalog.model_year, client_vehicle_catalog.datetime,
                                            production.`ws_code`,production.`ws_name`,production.`ws_partnumber`,production.`ws_comment`,production.`ws_usr_column_502`,
                                            production.`ws_unit`,production.`ws_group`,production.`ws_usr_column_533`,production.`ws_usr_column_537`,
                                            CONCAT(production.`ws_price`,'.00'),
                                            client_vehicle_catalog_production.amount,
                                            IF(client_vehicle_catalog_production.production_state_status_id='4','არასდროს გვქონია',
                                            IF(client_vehicle_catalog_production.production_state_status_id ='3','იკითხა, არ გვქონდა',
                                            IF(client_vehicle_catalog_production.production_state_status_id ='2','იკითხა, გვქონდა', 'აირჩია'))) as `status` 
                                    from client_vehicle_catalog
                                    LEFT JOIN client_vehicle_catalog_production on client_vehicle_catalog_production.client_vehicle_catalog_id = client_vehicle_catalog.id
                                    left join production on production.id = client_vehicle_catalog_production.production_id
                                    LEFT JOIN incomming_call on client_vehicle_catalog.incomming_call_id = incomming_call.id
                                    WHERE 	client_vehicle_catalog.actived = 1 and client_vehicle_catalog.incomming_call_id = '$hidden';");
            $data = $db->getList($count, $hidden);
            break;

        case 'get_list':
            $count	= $_REQUEST['count'];
            $hidden	= $_REQUEST['hidden'];
            
            $db->setQuery("SELECT 
                                incomming_call.`id`,
                                incomming_call.`id`,
                                incomming_call.`date`,
                                incomming_call.phone,
                                level_1.`name`,
                                level_2.`name`,
                                level_3.`name`
                            FROM
                                incomming_call
                                LEFT JOIN info_category AS level_1 ON level_1.id = incomming_call.cat_1
                                LEFT JOIN info_category AS level_2 ON level_2.id = incomming_call.cat_1_1
                                LEFT JOIN info_category AS level_3 ON level_3.id = incomming_call.cat_1_1_1
                            WHERE
                                NOT ISNULL( incomming_call.cat_1 ) 
                                AND incomming_call.cat_1 != 0
                                AND DATE(incomming_call.`date`) >= '$start_time' 
                                AND DATE(incomming_call.`date`) <= '$end_time'
                                GROUP BY incomming_call.id;");
            $data = $db->getList($count, $hidden);
        break;
        case 'tab_0':
            $data		    = array('page' => array('technik_info' => ''));
            $data['error']  = $error;
                      $db->setQuery("SELECT   COUNT(*) AS `all`,
                                                        sum(wait_time)/count(*) as avg_wait_time,
                                                        max(wait_time) as max_wait_time
                                                        FROM 	`transfered_calls`
                                                        where DATE(datetime) >= '$start_time'
                                                        AND DATE(datetime) <= '$end_time'
            ");
            $query=$db->getResultArray();
            $rejected_query = $db->setQuery("SELECT  count(*) as all_rejected
                                                        FROM 	`transfered_calls`
                                                        where `status` = 'rejected'
                                                        AND DATE(datetime) >= '$start_time'
                                                        AND DATE(datetime) <= '$end_time'
            ");
            $rejected_query=$db->getResultArray();
            $data['page']['technik_info'] = '
                            <td>ზარი</td>
                            <td>'.$query['result'][0]['all'].'</td>
                            <td>'.$query['result'][0]['avg_wait_time'].'</td>
                            <td>'.$query['result'][0]['max_wait_time'].'</td>
                            <td>'.$rejected_query['result'][0]['all_rejected']/$query['result'][0]['all']*100 .'</td>
                            <td>'.$rejected_query['result'][0]['all_rejected'].'</td>';
            $data['error'] = '';
        break;
        case 'get_pie':
            $cat_level = $_REQUEST['cat_level'];
            $where;
            $select;
            switch($cat_level){
                case '1':

                    $db->setQuery("SELECT   COUNT(*) AS `count`,
                                            info_cat_1.`name` AS `cause`
                                FROM `incomming_call`
                                left join info_category as info_cat_1 on info_cat_1.id  = incomming_call.cat_1
                                WHERE DATE(date) >= '$start_time'
                                AND DATE(date) <= '$end_time'
                                AND not ISNULL(incomming_call.cat_1) 
                                AND incomming_call.cat_1 != 0
                                GROUP BY incomming_call.cat_1");
                    $result=$db->getResultArray();
                break;
                case '2':
                    $db->setQuery("SELECT COUNT(*) AS `count`,
                                            CONCAT(info_cat_1.`name`,', ',info_cat_2.`name`) AS `cause`
                                FROM `incomming_call`
                                left join info_category as info_cat_1 on info_cat_1.id  = incomming_call.cat_1 AND not ISNULL(incomming_call.cat_1) AND incomming_call.cat_1 != 0 
                                left join info_category as info_cat_2 on info_cat_2.id  = incomming_call.cat_1_1 
                                WHERE DATE(date) >= '$start_time'
                                AND DATE(date) <= '$end_time'
                                AND not ISNULL(incomming_call.cat_1_1) 
                                AND incomming_call.cat_1_1 != 0 
                                AND info_cat_2.id != 999
                                GROUP BY incomming_call.cat_1_1");
                    $result=$db->getResultArray();
                break;
                case '3':
                    $db->setQuery("SELECT COUNT(*) AS `count`,
                                            CONCAT(info_cat_1.`name`,', ',info_cat_2.`name`,', ',info_cat_3.`name`)
                                            AS `cause`
                                FROM `incomming_call`
                                left join info_category as info_cat_1 on info_cat_1.id  = incomming_call.cat_1 AND not ISNULL(incomming_call.cat_1) AND incomming_call.cat_1 != 0 
                                left join info_category as info_cat_2 on info_cat_2.id  = incomming_call.cat_1_1 AND not ISNULL(incomming_call.cat_1_1) AND incomming_call.cat_1_1 != 0 
                                left join info_category as info_cat_3 on info_cat_3.id  = incomming_call.cat_1_1_1 
                                WHERE DATE(date) >= '$start_time'
                                AND DATE(date) <= '$end_time'
                                AND not ISNULL(incomming_call.cat_1_1_1) AND incomming_call.cat_1_1_1 != 0 
                                AND info_cat_3.id != 999
                                AND info_cat_2.id != 999
                                GROUP BY incomming_call.cat_1_1_1");
                    $result=$db->getResultArray();
                break;

            }
            $row = array();
            $data = array();
            foreach($result['result'] as $r) {
                $row[0] = $r['cause'].': '.$r['count'];
                $row[1] = (float)$r['count'];
                array_push($data,$row);
            }
        break;
    }
    function getPage($id){
        $data =
        '<div id="dialog-form">
            <fieldset>
                <legend>ძირითადი ინფორმაცია</legend>
                <table class="display" id="example_car_park_product" style="table-layout: fixed; zoom:90%;">
                    <thead>
                        <tr id="datatable_header_car_park_product">
                            <th style="display:none">ID</th>
                            <th style="width: 6.6%;">მარკა</th>
                            <th style="width: 6.6%;">მოდელი</th>
                            <th style="width: 6.6%;">მოდიფიკაცია</th>
                            <th style="width: 6.6%;">წელი</th>
                            <th style="width: 6.6%;">დამატების თარიღი</th>
                            <th style="width: 9.9%;">კოდი</th>
                            <th style="width: 9.9%;">დასახელება</th>
                            <th style="width: 9.9%;">არტიკული</th>
                            <th style="width: 9.9%;">აღწერა</th>
                            <th style="width: 9.9%;">მოდელი</th>
                            <th style="width: 3.3%;">საზომი</th>
                            <th style="width: 3.3%;">ჯგუფი</th>
                            <th style="width: 3.3%;">სეზონი</th>
                            <th style="width: 3.3%;">დანიშ<br>ნულება</th>
                            <th style="width: 3.3%;">ფასი</th>
                            <th style="width: 3.3%;">რაოდ<br>ენობა</th>
                            <th style="width: 6.6%;">სტატუსი</th>
                            <th style="width: 0px"></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr class="search_header_product">
                            
                            <th style="display:none">
                                <input type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_code" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_name" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_partnumber" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_comment" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_usr_column_502" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_unit" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_group" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_usr_column_533" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_usr_column_537" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_price" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_TotalRest" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <input id="ps_ws_not_yet2" type="text" name="search_address" value="" class="search_init" />
                            </th>
                            <th>
                                <div class="callapp_checkbox">
                                    <input type="checkbox" id="check-all_product" name="check-all_product" />
                                    <label for="check-all_products"></label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                </table>
            </fieldset>
            <input id="hidden_incomming_call_id" type="hidden" value="'.$id.'">
        </div>';
        return $data;
    }
    echo json_encode($data);
