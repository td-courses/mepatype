<?php
require_once("AsteriskManager/config.php");
include("AsteriskManager/sesvars.php");

if (isset($_SESSION['QSTATS']['hideloggedoff'])) {
    $ocultar = $_SESSION['QSTATS']['hideloggedoff'];
} else {
    $ocultar = "false";
}

?>

<head>
    <link href="media/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
    <script type="text/javascript" src="js/MyWebSocket.js?v=1"></script>
    <script type="text/javascript" src="js/jquery.timeago.js"></script>



    <?PHP if ($_SESSION['chat_off_on'] == '') {
        $_SESSION['chat_off_on'] = 1;
    } ?>
    <script type="text/javascript">
        $(document).keydown(function(e) {
            if (event.which == 13) {
                $("#loadtable").click();
            }
        });
        var aJaxURL = "server-side/call/incomming.action.php"; //server side folder url
        var aJaxURL2 = "server-side/view/automator.action.php"; //server side folder url

        var tName = "table_index"; //table name
        var fName = "add-edit-form-comunications";
        var tbName = "tabs"; //form name
        var file_name = '';
        var rand_file = '';
        var tab = 0;
        var fillter = '';
        var fillter_all = '';
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var Comunications_URL = "server-side/call/incomming.action.php";
        var CustomSaveURL = "server-side/view/automator.action.php";
        var multiselector_keys;
        /////////////////



        function LoadKendoTable_incomming(hidden) {

            //KendoUI CLASS CONFIGS BEGIN
            var aJaxURL = "server-side/call/incomming.action.php";
            var gridName = 'kendo_incomming_table';
            var actions = '<button style="float:right;" id="communication_excel_export">EXCEL რეპორტი</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 40;
            var columnsCount = 12;
            var columnsSQL = [
                "id:string",
                "inc_date:date",
                "phone:string",
                "mom_auth:string",
                "mom_type:string",
                "uwyeba:string",
                "project:string",
                "user_name:string",
                "name:string",
                "audio_file:string",
                "inc_status:string",
                "gender:string"
                
            ];
            var columnGeoNames = [
                "ID",
                "თარიღი",
                "ტელეფონი",
                "მომართვის ავტორი",
                "მომართვის ტიპი",
                "უწყება",
                "პროექტი",
                "ოპერატორი",
                "მომართვის სტატუსი",
                "ჩანაწერი",
                "სტატუსი",
                "სქესი"
            ];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var selectors              = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END

            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 1, '', '', '', true, false, true,[4,11]);

        }
        $(document).on('click', '.add_addit_addr', function() {
            var addr_type = $(this).attr('data-type');

            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_new_addr_dialog",
                    type: addr_type
                },
                success: function(data) {
                    $("#new_mail_addr").html(data.page);
                    LoadDialog("new_mail_addr");
                }
            });
        });

        function addNew(widgetId, value) {
            var widget = $("#" + widgetId).getKendoMultiSelect();
            var widget2 = $("#mail_cc").getKendoMultiSelect();
            var widget3 = $("#mail_bcc").getKendoMultiSelect();
            var dataSource = widget.dataSource;
            var dataSource2 = widget2.dataSource;
            var dataSource3 = widget3.dataSource;

            if (confirm("დარწმუნებული ხართ?")) {
                $.ajax({
                    url: aJaxURL,
                    data: {
                        act: "check_mail",
                        value: value
                    },
                    success: function(data) {
                        if (data.status == 'ok') {
                            dataSource.add({
                                id: 0,
                                name: value
                            });
                            dataSource2.add({
                                id: 0,
                                name: value
                            });
                            dataSource3.add({
                                id: 0,
                                name: value
                            });

                            dataSource.sync();
                        } else {
                            alert('არასწორი მეილის ფორმატი');
                        }

                    }
                });

            }
        }
        $(document).on('click', '#communication_excel_export', function() {
//             var start_date = $("#start_date").val() + " 00:00:00";
//             var end_date = $("#end_date").val() + " 23:59:59";
//             var call_sources = [];
//             $('#call_sources option:selected').toArray().map(c => call_sources.push(c.value));
// 
//             var hidden = getFilterResult();
//             var win = window.open('includes/communication_export.php?act=export_campaign_data_excel_incomming' + hidden, '_blank');
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_incomming_table","შემომავალი",[0,9]);
                $("#loading1").hide();
            }, 200);

        });

        $(document).on("dblclick", "#kendo_incomming_table tr.k-state-selected", function() {


            var grid = $("#kendo_incomming_table").data("kendoGrid");
            var dItem = grid.dataItem($(this));

            if (dItem.id == '') {
                return false;
            }

            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_edit_page",
                    id: dItem.id
                },
                success: function(data) {
                    if(data.error != ''){
                        alert(data.error);
                    }
                    else{
                        if (data.access == 'NO') {
                            alert('ჩატი აყვანილია სხვა პერატორის მიერ');
                        } else {
                            $("#add-edit-form-comunications").html(data.page);
                            LoadDialog("add-edit-form-comunications");
                            if (data.peer_id != 0) {
                                $('#telefoni___183--124--1').val(data.peer_id);
                                $('#telefoni___823--126--1').val(data.peer_id);
                            }

                            var source_id = $("#source_id").val();
                            var source;
                            //alert(source_id);git 
                            var kendo = new kendoUI();
                            kendo.kendoMultiSelector('mail_addresats', 'server-side/call/incomming.action.php', 'get_mail_addresats', "TO");
                            kendo.kendoMultiSelector('mail_cc', 'server-side/call/incomming.action.php', 'get_mail_addresats', "CC");
                            kendo.kendoMultiSelector('mail_bcc', 'server-side/call/incomming.action.php', 'get_mail_addresats', "BCC");
                            if (source_id == 4) {
                                source = 'chat';
                            } else if (source_id == 7) {
                                source = 'mail';
                            } else if (source_id == 1) {
                                source = 'call';
                            }
                            if (source == 'chat') {
                                $('#komunikaciis_arxi--138--8').val(4);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'call') {
                                $('#komunikaciis_arxi--138--8').val(1);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'viber') {
                                $('#komunikaciis_arxi--138--8').val(14);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'mail') {
                                $('#komunikaciis_arxi--138--8').val(7);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'skype') {
                                $('#komunikaciis_arxi--138--8').val(15);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'video') {
                                $('#komunikaciis_arxi--138--8').val(16);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'whatsapp') {
                                $('#komunikaciis_arxi--138--8').val(13);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'fbm') {
                                $('#komunikaciis_arxi--138--8').val(6);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            } else if (source == 'instagram') {
                                $('#komunikaciis_arxi--138--8').val(11);
                                $('#komunikaciis_arxi--138--8').trigger("chosen:updated");
                            }
                            if(data.permission == 50){
                                $("#info :input").prop("disabled",true);
                                $("#dep_0").prop("disabled",true);
                                $('#dep_0').trigger("chosen:updated");
                                
                                $("#dep_project_0").prop("disabled",true);
                                $("#dep_project_0").trigger("chosen:updated");
                                $("#dep_sub_project_0").prop("disabled",true);
                                $("#dep_sub_project_0").trigger("chosen:updated");
                                $("#dep_sub_type_0").prop("disabled",true);
                                $("#dep_sub_type_0").trigger("chosen:updated");
                                $("#region").prop("disabled",true);
                                $("#region").trigger("chosen:updated");
                                $("#municip").prop("disabled",true);
                                $("#municip").trigger("chosen:updated");
                                $("#temi").prop("disabled",true);
                                $("#temi").trigger("chosen:updated");
                                $("#village").prop("disabled",true);
                                $("#village").trigger("chosen:updated");
                                $("#add_dep").removeAttr("id");
                                $("#add_task_button").removeAttr("id");
                                $("#add_sms_phone").removeAttr("id");
                                $("#add_mail").removeAttr("id");
                                $(".incomming_file").css('display','none');


                                //$("#side_menu > span").eq(6).hide();
                                // $("#newsnews :input").prop("disabled",true);
                                // $("#client_history :input").prop("disabled",true);
                                // $("#task :input").prop("disabled",true);
                                // $("#incomming_comment :input").prop("disabled",true);
                                // $("#incomming_mail :input").prop("disabled",true);
                                // $("#client_conversation :input").prop("disabled",true);
                                // $("#chat_question :input").prop("disabled",true);
                                // $("#user_logs :input").prop("disabled",true);
                                // $("#call_resume :input").prop("disabled",true);
                                // $("#sms :input").prop("disabled",true);
                                // $("#icomming_file :input").prop("disabled",true);
                                // $("#crm :input").prop("disabled",true);
                                // $("#record :input").prop("disabled",true);
                            }
                            
                            // შეავსოს კონტრაქტის ველები
                 
                        }
                        
                    }

                }
            })

        });


        $(document).on('click', '#add_comm', function() {
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_edit_page",
                    id: ''
                },
                success: function(data) {
                    if(data.page == null){
                        alert("თქვენ არ გაქვთ დამატების უფლება");
                    }
                    else{
                        $("#add-edit-form-comunications").html(data.page);
                        LoadDialog("add-edit-form-comunications");
                        localStorage.setItem('new_case_dialog_opened', 1);
                        localStorage.setItem('new_case_dialog_inc_id', $("#hidden_incomming_call_id").val());
                    }
                    
                }
            })
        });

        function loadDndData() {
            param = new Object();
            param.act = "check_user_dnd_on_local";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {} else {
                            if (data.status == 0) {
                                $("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_ON.png');
                            } else if (data.status == 1) {
                                $("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_INSIDE.png');
                            } else if (data.status == 2) {
                                $("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_OFF.png');
                            }

                            if (data.muted == 1) {
                                $('#micr').attr('src', 'media/images/icons/5.png');
                                $(this).attr('value', '1');
                                micr_val = 1;
                            } else {
                                $('#micr').attr('src', 'media/images/icons/4.png');
                                $(this).attr('value', '0');
                                micr_val = 0;
                            }

                            if (data.queries_unrid_count > 0) {
                                $("#client_conversation_count_hint").html(data.queries_unrid_count);
                                $("#client_conversation_count_hint").css('display', '');
                            } else {
                                $("#client_conversation_count_hint").html(data.queries_unrid_count);
                                $("#client_conversation_count_hint").css('display', 'none');
                            }
                            setTimeout(loadDndData, 1000);
                        }
                    }
                }
            });
        }
        //////////////////////////////
        $(document).ready(function() {
            // loadDndData();
            check_user_comunication();
            // GetButtons("add_button");
            GetDate('start_date');
            GetDate('end_date');
            $('#operator_id,#tab_id,#user_info_id').chosen({
                search_contains: true
            });
            $('.callapp_filter_body').css('display', 'block');
            $.session.clear();
            $.session.set("checker_st", "1");
            // runAjax();
            // LoadTable();

            if(localStorage.getItem('new_case_dialog_opened') == "1") {
                if(localStorage.getItem('new_case_dialog_inc_id') == "" || localStorage.getItem('new_case_dialog_inc_id') == 0) {
                    console.log("can't find hidden incomming_id in localstorage");
                }else{
                    var inc_id_new = localStorage.getItem('new_case_dialog_inc_id');
                    $.ajax({
                        url: Comunications_URL,
                        data: {
                            act: "cancel_dialog",
                            inc_id:  inc_id_new
                        },
                        success: function(res) {
                            console.log("new case incomming_id has been deleted because of cancelation");
                            localStorage.removeItem('new_case_dialog_opened');
                            localStorage.setItem('new_case_dialog_opened', 0);

                            localStorage.removeItem('new_case_dialog_inc_id');
                            localStorage.setItem('new_case_dialog_inc_id', "");
                        },
                        error: function(res) {
                            console.log("deleting temporary case failed");
                            console.log(res);
                        }
                    });
                }
            }

            // GetButtons("add_button", "delete_button","refresh");
            SetEvents("", "", "", "table_index", fName, aJaxURL);
            toggleChatStatus('#chat_status', <?PHP echo $_SESSION['chat_off_on']; ?>);

            // $(".checkbox_check").css('display','none');
            // $("#delete_button").css('display','none');

            // $('#flesh_panel').css('width', '300px');
            $("#show_station").css('background', '#ccc');
            // loadDndData1();

            setTimeout(() => {
                var hidden = getFilterResult();
                LoadKendoTable_incomming(hidden)
            }, 2000);


        });

        $(document).on("change", "#task_branch, #task_position", function() {
            var task_type = $('#task_type').val();
            param = new Object();
            param.act = "task_branch_changed";
            param.cat_id = $('#task_branch').val();
            param.position_id = $('#task_position').val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#task_recipient").html(data.page);
                    $('#task_recipient').trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#task_recipient", function() {
            var task_type = $('#task_type').val();
            param = new Object();
            param.act = "task_recipient_changed";
            param.recipient_id = $(this).val();
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    $("#task_branch").html(data.page);
                    $('#task_branch').trigger("chosen:updated");
                }
            });
        });

        function getFilterResult() {
            var operators = $("#operators").data("kendoDropDownList");
            var selectedIndex = operators.select();
            var operators_dItem = operators.dataItem(selectedIndex);

            var all_calls = $("#all_calls").data("kendoDropDownList");
            var selectedIndex = all_calls.select();
            var all_calls_dItem = all_calls.dataItem(selectedIndex);

            var call_type = [];
            $('#call_type option:selected').toArray().map(c => call_type.push(c.value));

            var call_status = [];
            $('#call_status option:selected').toArray().map(c => call_status.push(c.value));

            var call_sources = [];
            $('#call_sources option:selected').toArray().map(c => call_sources.push(c.value));

            if (operators_dItem == undefined) {
                operators_dItem = new Object();
                operators_dItem.id = '';
            }
            if (all_calls_dItem == undefined) {
                all_calls_dItem = new Object();
                all_calls_dItem.id = '';
            }

            var hidden = "&start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + "&filter_operator=" + operators_dItem.id + "&filter_all_calls=" + all_calls_dItem.id + "&filter_call_status=" + call_status + "&filter_call_sources=" + call_sources + "&filter_call_type=" + call_type;

            return hidden;
        }


        function loadDndData1() {

            // define request data
            const data = {
                act: "load_dnd_data",
                getOptions: $("#callapp_op_activitie_choose").find("li").length === 1
            }

            // send ajax request
            $.post(aJaxURL, data, responce => {

                // restruct responce
                const {
                    error,
                    options,
                    dndStatus
                } = responce;

                // check for error
                if (error !== "") {
                    // alert(error);
                    alert("We where in Munxern");
                } else {

                    // customize dnd button
                    if (options !== "") {
                        $("#callapp_op_activitie_choose").html(options);
                    }

                    // make check interval
                    setTimeout(loadDndData, 1000);

                }
            });
        }

        function toggleChatStatus(selector, status) {
            $D.sessionStatus = status === 2 ? 1 : 0;
            const dataStatus = status === 2 ? "on" : "off";
            $(selector).val(status).attr("data-status", dataStatus).data("status", dataStatus);
        }


        function displayAmountStandartSubCat(status) {
            $("#card_type_id_holder").css({
                display: status
            });
            $("#for_card_type_id").css({
                display: status
            });
        }

        function displayAmountInOutSubCat(status) {
            $("#for_transaction_date_time").css({
                display: status
            });
            $("#transaction_date_time").css({
                display: status
            });
            $("#for_card_16_number").css({
                display: status
            });
            $("#card_16_number_holder").css({
                display: status
            });
        }


        $(document).on("click", "#filter_dam_shem", function() {
            fillter = $("#filter_dam_shem").val();
            LoadTable();
        });
        $(document).on("click", "#filter_transf_shem", function() {
            fillter = $("#filter_transf_shem").val();
            LoadTable();
        });
        $(document).on("click", "#filter_dam_gam", function() {
            fillter = $("#filter_dam_gam").val();
            LoadTable();
        });
        $(document).on("click", "#filter_transf_gam", function() {
            fillter = $("#filter_transf_gam").val();
            LoadTable();
        });
        $(document).on("click", "#filter_unansver", function() {
            fillter = $("#filter_unansver").val();
            LoadTable();
        });
        $(document).on("click", "#filter_dau_gam", function() {
            fillter = $("#filter_dau_gam").val();
            LoadTable();
        });
        $(document).on("click", "#noansverdone", function() {
            fillter = $("#noansverdone").val();
            LoadTable();
        });
        $(document).on("click", "#filter_dau_shem", function() {
            fillter = $("#filter_dau_shem").val();
            LoadTable();
        });
        $(document).on("click", "#filter_chati1", function() {
            fillter = $("#filter_chati1").val();
            LoadTable();
        });
        $(document).on("click", "#filter_chati2", function() {
            fillter = $("#filter_chati2").val();
            LoadTable();
        });
        $(document).on("click", "#filter_chati3", function() {
            fillter = $("#filter_chati3").val();
            LoadTable();
        });
        $(document).on("click", "#filter_saubr", function() {
            fillter = $("#filter_saubr").val();
            LoadTable();
        });
        $(document).on("click", "#filter_all1", function() {
            fillter = $("#filter_all1").val();
            LoadTable();
            $('#fi_call,#fi_chat').css('display', 'table');
        });
        $(document).on("click", "#filter_all2", function() {
            fillter = $("#filter_all2").val();
            LoadTable();
            $('#fi_chat').css('display', 'none');
            $('#fi_call').css('display', 'table');
        });
        $(document).on("click", "#filter_all3", function() {
            fillter = $("#filter_all3").val();
            LoadTable();
            $('#fi_call').css('display', 'none');
            $('#fi_chat').css('display', 'table');
        });


        $(document).on("click", ".client_history", function() {
            var dLength = [
                [10, 30, 50, -1],
                [10, 30, 50, "ყველა"]
            ];
            GetDate("date_from");
            GetDate("date_to");
            GetDataTable("table_history", 'server-side/call/waiters.action.php', "get_list_history", 7, "&start_check=" + $('#date_from').val() + "&end_check=" + $('#date_to').val() + "&phone=" + $("#client_history_phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");

            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("click", "#control_tab_id_4_0", function() {
            var dLength = [
                [10, 30, 50, -1],
                [10, 30, 50, "ყველა"]
            ];
            GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_date=" + $("#client_history_filter_date").val() + "&client_history_filter_activie=" + $("#client_history_filter_activie").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
            $("#example4_0_length").css('top', '0px');
            $("#example4_0_wrapper").css('width', '1000px');
            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("click", "#search_client_orders", function() {
            var dLength = [
                [10, 30, 50, -1],
                [10, 30, 50, "ყველა"]
            ];
            GetDataTable("example4_0", AJ, "get_list_client_order", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_date=" + $("#client_history_filter_date").val() + "&client_history_filter_activie=" + $("#client_history_filter_activie").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
            $("#example4_0_length").css('top', '0px');
            $("#example4_0_wrapper").css('width', '1000px');
            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("click", "#control_tab_id_4_1", function() {
            var dLength = [
                [10, 30, 50, -1],
                [10, 30, 50, "ყველა"]
            ];
            GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_site=" + $("#client_history_filter_site").val() + "&client_history_filter_group=" + $("#client_history_filter_group").val() + "&client_history_filter_transaction_type=" + $("#client_history_filter_transaction_type").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
            $("#example4_1_length").css('top', '0px');
            $("#example4_1_wrapper").css('width', '790px');
            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("click", "#search_client_transaction", function() {
            var dLength = [
                [10, 30, 50, -1],
                [10, 30, 50, "ყველა"]
            ];
            GetDataTable("example4_1", AJ, "get_list_client_transaction", 10, "incomming_call_id=" + $("#incomming_call_id").val() + "&history_user_id=" + $("#client_history_user_id").val() + "&client_history_filter_site=" + $("#client_history_filter_site").val() + "&client_history_filter_group=" + $("#client_history_filter_group").val() + "&client_history_filter_transaction_type=" + $("#client_history_filter_transaction_type").val(), 0, dLength, 2, "desc", '', "<'F'lip>");
            $("#example4_1_length").css('top', '0px');
            $("#example4_1_wrapper").css('width', '790px');
            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("click", "#search_client_history", function() {
            var dLength = [
                [10, 30, 50, -1],
                [10, 30, 50, "ყველა"]
            ];

            GetDataTable("table_history", 'server-side/call/waiters.action.php', "get_list_history", 7, "&start_check=" + $('#date_from').val() + "&end_check=" + $('#date_to').val() + "&phone=" + $("#client_history_phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");

            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("change, focus", "[data-hendler='error']", function() {
            var inputId = $(this).attr("id");
            $(`label[for="${inputId}"]`).removeClass("red-color");
        });


        $(document).on("change", "#tab_id", function() {
            if ($(this).val() == 5) {
                $(".checkbox_check").css('display', '');
                $("#delete_button").css('display', '');
            } else {
                $(".checkbox_check").css('display', 'none');
                $("#delete_button").css('display', 'none');
            }
        });
        $(document).on("click", "#loadtable", function() {

            // $('.callapp_filter_body').css('display','none');
            // $('.callapp_filter_body').attr('myvar',0);
            // $("#shh").css('transform', 'rotate(180deg)');

            // var operators = $("#operators").data("kendoDropDownList");
            // var selectedIndex = operators.select();
            // var operators_dItem = operators.dataItem(selectedIndex);

            // var all_calls = $("#all_calls").data("kendoDropDownList");
            // var selectedIndex = all_calls.select();
            // var all_calls_dItem = all_calls.dataItem(selectedIndex);

            // var call_status = [];
            // $('#call_status option:selected').toArray().map(c => call_status.push(c.value));

            // var call_sources = [];
            // $('#call_sources option:selected').toArray().map(c => call_sources.push(c.value));

            // LoadTable(operators_dItem,all_calls_dItem, call_status, call_sources);

            var hidden = getFilterResult();

            LoadKendoTable_incomming(hidden)


            // console.log("++ ", "op: "+ operators_dItem.id, "ALL: "+ all_calls_dItem.id, "statuss: "+call_status, "source: "+ call_sources);
        });

        $(document).on("click", ".play_audio_complate", function() {
            var str = 1;
            var link = ($(this).attr("str"));
            var tt = $(this).attr('tt');
            if (tt == 'autodialer') {
                link = link;
            } else {
                link = 'http://172.16.0.80:8000/' + link;
            }
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    class: "cancel-audio",
                    click: function() {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog", "auto", "auto", btn);
            console.log(link)
            $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="' + link + '" type="audio/wav"> Your browser does not support the audio element.</audio>');
            // $(".download").css( "background","#2dc100" );
            // $(".download4").css( "background","#2dc100" );
            // $(this).css( "background","#FF5555" );
            var file_col = $(this).parent()
            $(file_col).css('background-color', "#ff5555")


        })

        $(document).on("click", ".callapp_refresh", function() {
            fillter = '';
            LoadTable();
        });
        $(document).on("click", ".comunication_button", function() {
            if ($("#comunication_table_check").val() == 0) {
                $("#comunication_table_check").val(1);
                $("#comucation_status").css('display', '');
            } else {
                $("#comunication_table_check").val(0);
                $("#comucation_status").css('display', 'none');
            }
        });

        function LoadKendoChatDocs(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'kendo_chat_docs';
            var actions = '';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 5;
            var columnsSQL = ["id:string", "name:string", "date:date", "link:string", "action:string"];
            var columnGeoNames = ["ID", "სახელი", "შექმნის თარიღი", "ლინკი", "არჩევა"];

            var showOperatorsByColumns = [0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'chat_docs_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);
        }
        $(document).on('click', ".select_chat_doc", function() {
            var link = $(this).attr('data-link');

            $("#send_message").append('<a target="_blank" href="' + link + '">' + link + '</a>');
        });
        $(document).on('click', '#choose_docs', function() {
            $.ajax({
                url: aJaxURL,
                data: "act=chat_docs",
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {

                        } else {
                            $("#chat-docs-dialog-form").html(data.page);
                            LoadDialog('chat_docs');
                            LoadKendoChatDocs();
                        }
                    }
                }
            });

        });

        function LoadTable(operators_dItem, all_calls_dItem, call_status, call_sources) {
            AJ = aJaxURL;
            columcaunt = 8;

            if (operators_dItem == undefined) {
                operators_dItem = new Object();
                operators_dItem.id = '';
            }
            if (all_calls_dItem == undefined) {
                all_calls_dItem = new Object();
                all_calls_dItem.id = '';
            }


            /* Table ID, aJaxURL, Action, Colum Number, Custom Request, Hidden Colum, Menu Array */
            GetDataTable("table_index", AJ, "get_list", columcaunt, "start=" + $('#start_date').val() + "&end=" + $('#end_date').val() + "&operator_id=" + $('#operator_id').val() + "&user_info_id=" + $("#user_info_id").val() + "&tab_id=" + $('#tab_id').val() + "&filter_operator=" + operators_dItem.id + "&filter_all_calls=" + all_calls_dItem.id + "&filter_call_status=" + call_status + "&filter_call_sources=" + call_sources, 0, "", 2, "desc", '', change_colum_main);


            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');

                // $('#table_index td:nth-child(10)').css('padding',0);
                // $('#table_index td:nth-child(11)').css('padding',0);

            }, 90);
        }


        ///////////////////////////////////---------------aqedan---------------------------/////////////////////////////






        $(document).on("click", "#chat_hist", function() {
            $.ajax({
                url: aJaxURL,
                data: {
                    'act': 'get_history',
                    'ip': $('#user_block').attr('ip'),
                    'chat_id': $('#chat_original_id').val(),
                    'source': $("#chat_source").val(),
                    'chat_name': $("#chat_name").val()

                },
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {

                        } else {
                            $('#log').html('');
                            $('#log').html(data.sms);
                            jQuery("time.timeago").timeago();
                            document.getElementById('chat_scroll').scrollTop = 25000000000;
                            $('#chat_live').css('border-bottom', '2px solid transparent');
                            $('#chat_hist').css('border-bottom', '2px solid #333');
                        }
                    }
                }
            });
        });
        /////////////////////////////////// aqamde//////////////////////////////////////
        $(document).on("click", "#user_block_save", function() {
            $.ajax({
                url: aJaxURL,
                data: "act=block_ip&ip=" + $("#user_block").attr('ip') + "&pin=" + $("#user_block").attr('pin') + "&chat_comment=" + $('#block_comment').val() + "&chat_id=" + $('#chat_original_id').val(),
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {

                        } else {
                            $('#log').html('');
                            $('#add_blocked_ip').dialog("close");

                        }
                    }
                }
            });
        });
        $(document).on("click", "#user_block_pin", function() {
            $.ajax({
                url: aJaxURL,
                data: "act=block_pin&ip=" + $("#user_block").attr('ip') + "&pin=" + $("#user_block").attr('pin') + "&chat_comment=" + $('#block_comment').val() + "&chat_id=" + $('#chat_original_id').val(),
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {

                        } else {
                            $('#log').html('');
                            $('#add_blocked_ip').dialog("close");

                        }
                    }
                }
            });
        });
        /////////////////////////////////////////////////////////////////////////////////
        $(document).on("change", "#transfer_resp_ext_id", function() {
            var value = $(this).val();
            $.ajax({
                url: aJaxURL,
                data: "act=get_resp_user&extention=" + value,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {

                        } else {
                            $('#transfer_resp_id').html(data.name);
                            $('#transfer_resp_id').attr('val', data.id);
                        }
                    }
                }
            });
        });
        /////////////////////////////////////////////////////////////////////////////////
        $(document).on("change", "#check_transfer", function() {
            var value = $('input[name=check_transfer]:checked').val();
            if (value == 1) {
                $("#transfer_resp_ext_id").css('display', '');
                $("#transfer_resp_id").css('display', '');
            } else {
                $("#transfer_resp_ext_id").css('display', 'none');
                $("#transfer_resp_id").css('display', 'none');
            }
        });
        ////////////////////////////////////////////////////////////////////////////////
        $(document).on("click", "#user_block", function() {
            if ($("#source_id").val() == 6 || $("#source_id").val() == 7 || $("#source_id").val() == 9 || $("#source_id").val() == 11 || $("#source_id").val() == 12) {
                var buttons = {
                    "chat_block_locked": {
                        text: "დაბლოკვა",
                        id: "chat_block_locked",
                    },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel",
                        click: function() {
                            $(this).dialog('close');
                        }
                    }

                };
                GetDialog("numlock_dialog", 280, "auto", buttons);
                $("#numlock_dialog").html('<span>კომენტარი</span><textarea style="width: 245px; height: 80px; resize: none;" id="chat_block_reason" class="idle"></textarea>');
            } else {
                if ($(this).attr('ip') != '') {
                    var buttons = {

                        "cancel": {
                            text: "დახურვა",
                            id: "cancel-dialog",
                            click: function() {
                                $(this).dialog("close");
                                $(this).html("");
                            }
                        },
                        "save": {
                            text: "IP_ით დაბლოკვა",
                            id: "user_block_save"
                        },

                        "save_pin": {
                            text: "PIN_ით დაბლოკვა",
                            id: "user_block_pin"
                        }

                    };
                    //////////////////////////////////////////////////////////////////////////

                    /* Dialog Form Selector Name, Buttons Array */
                    GetDialog("add_blocked_ip", 400, "auto", buttons);
                } else {
                    alert('ჯერ მონიშნეთ აქტიური ჩატი!');
                }
            }
        });
        //////////////////////////////////////////////////////////////////////////



        $(document).on("click", "#num_block", function LoadDialog2() {
            var buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                "num_block_locked": {
                    text: "დაბლოკვა",
                    id: "num_block_locked",
                }

            };
            GetDialog("numlock_dialog", 280, "auto", buttons);
            $("#numlock_dialog").html('<span>კომენტარი</span><textarea style="width: 245px; height: 80px; resize: none;" id="block_comment2" class="idle"></textarea>');
        });


        $(document).on("click", "#save-edit-form-sport", function() {

            param = new Object();
            param.act = "save-edit-form-sport";
            param.end_date = $("#add_sport_dialog #action_end_date").val();
            param.start_date = $("#add_sport_dialog #action_start_date").val();
            param.id = $("#add_sport_dialog #action_sport_id").val();
            param.act_sport_id = $("#add_sport_dialog #act_id").val();
            param.category_id = $("#add_sport_dialog #action_category_id").val();
            param.game_list = $("#add_sport_dialog #game_list").val();
            param.info = $("#add_sport_dialog #action_info").val();

            $.ajax({
                url: aJaxURLsport,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != "undefined") {
                        if (data.error != "") {
                            alert(data.error);
                        } else {
                            GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc", '', "<'F'lip>");
                            $("#add_sport_dialog").dialog('close');
                            $("#table_sport_length").css('top', '0px');
                        }
                    }
                }
            });
        });

        /*	function getchats(){
			clearTimeout(t);

            var t = setTimeout(function(){


	    			getchats();


	    		}, 1000000);
		}*/
        $(document).on("click", "#search_ab_pin", function() {

            GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check=" + $('#start_check').val() + "&end_check=" + $('#end_check').val() + "&phone=" + $("#check_ab_phone").val() + "&s_u_user_id=" + $("#check_ab_user_id").val(), 0, "", 2, "desc", '', "<'F'lip>");

            $("#table_history_length").css('top', '0px');
        });
        $(document).on("click", "#tab_sport0", function() {
            GetDataTable("table_sport", aJaxURLsport, "get_list", 7, "", 0, "", 0, "desc", '', "<'F'lip>");
            $("#table_sport_length").css('top', '0px');
        });

        $(document).on("click", "#tab_sport1", function() {
            GetDataTable("table_sport1", aJaxURLsport, "get_list_archive", 7, "", 0, "", 0, "desc", '', "<'F'lip>");
            $("#table_sport1_length").css('top', '0px');
        });

        // $(document).on("click", ".download6", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://pbx.my.ge/records/' + link;
        //     } else{
        //         link = 'http://pbx.my.ge/records/' + link;
        //     }
        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };
        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);

        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download1").css( "background","#F99B03" );
        //     $(this).css( "background","#33dd33" );

        // });


        // $(document).on("click", ".download2", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://pbx.my.ge/records/' + link;
        //     } else{
        //         link = 'http://pbx.my.ge/records/' + link;
        //     }
        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };
        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);
        //     //	alert('hfgj');
        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download").css( "background","#408c99" );
        //     $(this).css( "background","#FF5555" );

        // });

        // $(document).on("click", ".download4", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://172.16.0.80:8000/' + link;
        //     } else{
        //         link = 'http://172.16.0.80:8000/' + link;
        //     }
        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };
        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);
        //     //	alert('hfgj');
        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download").css( "background","#2dc100" );
        //     $(".download4").css( "background","#2dc100" );
        //     $(this).css( "background","#FF5555" );

        // });


        //ჩანაწერები
        // $(document).on("click", ".download", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://pbx.my.ge/records/' + link;
        //     } else{
        //         link = 'http://pbx.my.ge/records/' + link;
        //     }

        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };
        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);

        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download").css( "background","#2dc100" );
        //     $(".download4").css( "background","#2dc100" );
        //     $(this).css( "background","#FF5555" );

        // });

        // $(document).on("click", ".download1", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://pbx.my.ge/records/' + link;
        //     } else{
        //         link = 'http://pbx.my.ge/records/' + link;
        //     }
        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };
        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);

        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download1").css( "background","#F99B03" );
        //     $(".download3").css( "background","#ce8a14" );
        //     $(".download5").css( "background","#4980AF" );
        //     $(".download").css( "background","#2dc100" );
        //     $(this).css( "background","#5757FF" );

        // });

        // $(document).on("click", ".download3", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://pbx.my.ge/records/' + link;
        //     } else{
        //         link = 'http://pbx.my.ge/records/' + link;
        //     }
        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };
        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);

        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download3").css( "background","#ce8a14" );
        //     $(".download5").css( "background","#4980AF" );
        //     $(".download1").css( "background","#F99B03" );
        //     $(".download").css( "background","#2dc100" );
        //     $(this).css( "background","#5757FF" );

        // });

        // $(document).on("click", ".download5", function () {
        //     var str = 1;
        //     var link = ($(this).attr("str"));
        //     $call_date = new Date($(this).parent().parent().find(":nth-child(3)").html());
        //     $limit_date = new Date("2018-06-29 19:45:24");
        //     if($call_date > $limit_date){
        //         link = 'http://pbx.my.ge/records/' + link;
        //     } else{
        //         link = 'http://pbx.my.ge/records/' + link;
        //     }
        //     var btn = {
        //         "cancel": {
        //             text: "დახურვა",
        //             id: "cancel-dialog",
        //             click: function () {
        //                 $(this).dialog("close");
        //                 $(this).html("");
        //             }
        //         }
        //     };

        //     GetDialog_audio("audio_dialog", "auto", "auto",btn);

        //     $("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        //     $(".download5").css( "background","#4980AF" );
        //     $(".download3").css( "background","#ce8a14" );
        //     $(".download1").css( "background","#F99B03" );
        //     $(".download").css( "background","#2dc100" );
        //     $(this).css( "background","#5757FF" );

        // });
        // end record

        // 		 $(button).on("click", ".download", function (){
        //         	  $(this).css( "background","#FF5555" );
        //          });

        function CloseDialog() {
            $("#" + fName).dialog("close");
        }

        // Add - Save

        $(document).on("click", "#lid", function() {
            if ($('input[id=lid]:checked').val() == 1) {
                $("#lid_comment").css('display', '');
            } else {
                $("#lid_comment").css('display', 'none');
            }
        });



        // open send sms dialog


        $(document).on("click", ".client_conversation", function(event) {
            param = new Object();
            param.act = "get_client_conversation_count";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {} else {
                            $("#client_conversation_count_hint").html(0);
                            $("#client_conversation_count_hint").css('display', 'none');

                            GetDataTable("table_question", aJaxURL, "get_list_quest", 5, "source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), 0, "", 0, "desc", '', "<'F'lip>");
                            $("#table_question_length").css('top', '0px');
                        }
                    }
                }
            });

        });






        $(document).on("click", "#refresh", function() {
            fillter = '';
            LoadTable(fillter);
        });
        $(document).on("click", "#refresh_button", function() {
            fillter_all = '';
            LoadTable1();

        });

        $(document).on("click", "#refresh_button1", function() {

            param = new Object();
            param.act = "refresh";

            $.ajax({
                url: aJaxURL4,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            // alert(data.error);
                            alert("Serjio");
                        } else {
                            $("#search_end_my1").val(data.date);
                            LoadTable4();
                        }
                    }
                }
            });
        });


        $(document).on("click", "#refresh_button2", function() {

            param = new Object();
            param.act = "refresh";

            $.ajax({
                url: aJaxURL6,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            // alert(data.error);
                            alert("Console log");
                        } else {
                            $("#search_end_my2").val(data.date);
                            LoadTable6();
                        }
                    }
                }
            });
        });


        $(document).on("click", "#ipaddres_block", function() {
            var buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel",
                    click: function() {
                        $(this).dialog("close");
                    }
                },

                "ipaddr": {
                    text: "დაბლოკვა",
                    id: "ipaddr"
                }

            };


            /* Dialog Form Selector Name, Buttons Array */
            GetDialog("ipaddr_dialog", 400, "auto", buttons);
        });

        $(document).on("click", "#ipaddr", function() {
            parame = new Object();
            parame.ipaddres = $("#ipaddres").val();
            parame.ipaddr_block_comment = $("#ipaddr_block_comment").val();
            parame.act = "ipaddr_check"

            $.ajax({
                url: aJaxURL,
                data: parame,
                success: function(data) {
                    if (data.check == '0') {
                        alert('ესეთი IP უკვე დაბლოკილია!');
                    } else {
                        $("#ipaddr_dialog").dialog("close");
                    }
                }
            });
        });

        $(document).on("click", "#num_block_locked", function() {
            parame = new Object();
            parame.phone = $("#phone").val();
            parame.ipaddres = $("#ipaddres").val();

            parame.act = "num_check"
            $.ajax({
                url: aJaxURL,
                data: parame,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            // alert(data.error);
                            alert("Loading");
                        } else {
                            if (data.check == 1) {
                                param = new Object();
                                param.phone = $("#phone").val();
                                param.block_comment = $("#block_comment2").val();

                                param.act = "black_list_add";
                                if (param.phone == "") {
                                    alert("შეავსეთ ტელეფონის ნომერი!");
                                } else {
                                    $.ajax({
                                        url: asterisk_url,
                                        data: param,
                                        success: function(data) {
                                            if (typeof(data.error) != 'undefined') {
                                                if (data.error != '') {
                                                    // alert(data.error);
                                                    alert("Container");
                                                } else {
                                                    LoadTable();
                                                    $("#numlock_dialog").dialog("close");

                                                }
                                            }
                                        }
                                    });

                                    $.ajax({
                                        url: aJaxURL,
                                        data: param,
                                        success: function(data) {
                                            if (typeof(data.error) != 'undefined') {
                                                if (data.error != '') {
                                                    // alert(data.error);
                                                    alert("Hello there");
                                                } else {
                                                    LoadTable();
                                                    $("#numlock_dialog").dialog("close");

                                                }
                                            }
                                        }
                                    });
                                }
                            } else {
                                alert('ნომერი უკვე არის დაბლოკილთა სიაში');
                            }
                        }
                    }
                }
            });


        });





        $(document).on("click", "#chat_block_locked", function() {

            parame = new Object();
            parame.act = "black_list_add_chat"
            parame.source_id = $("#source_id").val();
            parame.id = $("#id").val();
            parame.chat_block_reason = $("#chat_block_reason").val();
            parame.site_chat_id = $("#site_chat_id").val();

            if (parame.chat_block_reason == '') {
                alert('შეავსეთ კომენტარი !!!');
            } else if (parame.id == '') {
                alert('აირჩიეთ შესაბამისი ჩატი !!!')
            } else {
                $.ajax({
                    url: aJaxURL,
                    data: parame,
                    success: function(data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                LoadTable();
                                $("#numlock_dialog").dialog("close");
                                alert('ჩატი წარმატებით დაიბლოკა');

                                param = new Object();
                                param.source = 'fbm_block';
                                param.chat_id = $('#chat_original_id').val();

                                if ($("#source_id").val() == '6') {
                                    $.ajax({
                                        url: "server.php",
                                        data: param,
                                        dataType: "json",
                                        success: function(data) {
                                            console.log('დაიბლოკა');
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
            }
        });





        $(document).on("change", "#check_state", function() {
            alert(1)
        });
        ////////////////////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////

        $(document).on("click", ".open_dialog", function() {
            var number = $(this).attr("number");
            var ipaddres = $(this).attr("ipaddres");
            var ab_pin = $(this).attr("ab_pin");
            var check_save_chat = $(this).attr("check_save_chat");
            var source = $(this).attr("data-source");
            var calltype = $(this).attr("data-calltype");
            if ($('#imchat').val() == 1) {

            }
            if (number != "") {
                run(number, ipaddres, ab_pin, check_save_chat, source, calltype);
            }
        });

        $(document).on("click", ".insert", function() {
            var phone = $(this).attr("number");

            if (phone != "") {
                $('#telefoni___183--124--1').val(phone);
            }
        });

        $(document).on("change", "#task_type_id", function() {
            var task_type = $("#task_type_id").val();

            if (task_type == 1) {
                $("#task_department_id").val(37);
            }

        });
        $(document).on("change", "#task_type", function() {
            param = new Object();
            param.act = "task_type_changed";
            param.cat_id = $('#task_type').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function(data) {
                    $("#task_status_id").html(data.page);
                    $('#task_status_id').trigger("chosen:updated");
                    $('#task_branch').trigger("chosen:updated");
                    $('#task_status_id').trigger("change");
                }
            });
        });

        $(document).on("change", "#task_status_id", function() {
            param = new Object();
            param.act = "get_task_status_2";
            param.cat_id = $('#task_status_id').val();
            $.ajax({
                url: aJaxURL_task,
                data: param,
                success: function(data) {
                    $("#task_status_id_2").html(data.page);
                    $('#task_status_id_2').trigger("chosen:updated");
                }
            });
        });


        $(document).on("click", "#send_message", function() {
            param = new Object();
            param.act = "seen";
            param.id = $("#chat_original_id").val();

            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {}
            });
        })

        $(document).on("keyup", "#send_message", function(e) {
            if ($("#source_id").val() == 4) {

                if (e.keyCode == 8 || e.keyCode == 13) {

                    param = new Object();
                    param.act = "focusout";
                    param.id = $("#chat_original_id").val();

                    $.ajax({
                        url: Comunications_URL,
                        data: param,
                        success: function(data) {}
                    });
                } else {
                    param = new Object();
                    param.act = "focusin";
                    param.id = $("#chat_original_id").val();

                    $.ajax({
                        url: Comunications_URL,
                        data: param,
                        success: function(data) {}
                    });
                }
            }
        });


        $(document).on("click", "#refresh-dialog", function() {
            param = new Object();
            param.act = "get_calls";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            // alert(data.error);
                            alert("Jimsher G");
                        } else {
                            $("#last_calls").html(data.calls);
                            $(".insert").button({
                                icons: {
                                    primary: "ui-icon-plus"
                                }
                            });
                        }
                    }
                }
            });

        });


        //
        function my_filter() {
            var myhtml = '';
            if ($.session.get("operator_id") == 'on') {
                myhtml += '<span>ოპერატორი<close cl="operator_id">X</close></span>';
            } else {
                $('#operator_id option:eq(0)').prop('selected', true);
            }
            if ($.session.get("tab_id") == 'on') {
                myhtml += '<span>ტაბი<close cl="tab_id">X</close></span>';
            } else {
                $('#tab_id option:eq(0)').prop('selected', true);
            }
            if ($.session.get("filter_1") == 'on') {
                myhtml += '<span>შე. დამუშავებული<close cl="filter_1">X</close></span>';
            }
            if ($.session.get("filter_2") == 'on') {
                myhtml += '<span>შე. დაუმუშავებელი<close cl="filter_2">X</close></span>';
            }
            if ($.session.get("filter_3") == 'on') {
                myhtml += '<span>შე. უპასუხო<close cl="filter_3">X</close></span>';
            }
            if ($.session.get("filter_4") == 'on') {
                myhtml += '<span>შეხვედრა<close cl="filter_4">X</close></span>';
            }
            if ($.session.get("filter_5") == 'on') {
                myhtml += '<span>ინტერნეტი<close cl="filter_5">X</close></span>';
            }
            if ($.session.get("filter_6") == 'on') {
                myhtml += '<span>ტელეფონი<close cl="filter_6">X</close></span>';
            }
            if ($.session.get("filter_7") == 'on') {
                myhtml += '<span>გამცხადებელი<close cl="filter_7">X</close></span>';
            }

            $('.callapp_tabs').html(myhtml);
            LoadTable('index', colum_number, main_act, change_colum_main);
            $('#operator_id, #tab_id').trigger("chosen:updated");
        }




        $(document).on("click", "#search_ab_crm_pin", function() {

            GetDataTable("table_crm", aJaxURL, "get_list_crm", 9, "start_crm=" + $("#start_crm").val() + "&end_crm=" + $("#end_crm").val() + "&pin=" + $("#check_ab_crm_pin").val() + "&phone=" + $("#check_ab_crm_phone").val(), 0, [
                [50, -1],
                [50, "ყველა"]
            ], 0, "desc", "", "<'F'lip>");
            SetEvents("", "", "", "table_crm", "crm_dialog", aJaxURL_crm);
            $("#table_crm_length").css('top', '0px');

            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("click", ".crm", function() {
            $("#check_ab_crm_pin").val($("#personal_pin").val());
            GetDataTable("table_crm", aJaxURL, "get_list_crm", 9, "start_crm=" + $("#start_crm").val() + "&end_crm=" + $("#end_crm").val() + "&pin=" + $("#check_ab_crm_pin").val() + "&phone=" + $("#check_ab_crm_phone").val(), 0, [
                [50, -1],
                [50, "ყველა"]
            ], 0, "desc", "", "<'F'lip>");
            SetEvents("", "", "", "table_crm", "crm_dialog", aJaxURL_crm);
            $("#table_crm_length").css('top', '0px');
            $("#search_ab_crm_pin").button();
            GetDate('start_crm');
            GetDate('start_crm');

            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });



        $(document).on("click", ".newsnews", function() {
            GetDataTable("table_news", aJaxURL, "get_list_news", 6, "", 0, "", 0, "desc", '', "<'F'lip>");
            $("#table_news_length").css('top', '0px');
            setTimeout(function() {
                $('.ColVis, .dataTable_buttons').css('display', 'none');
            }, 90);
        });

        $(document).on("change", "#chat_status", function() {

            toggleChatStatus('#chat_status', Number($(this).val()));

            $.ajax({
                url: aJaxURL,
                data: 'act=chat_on_off&value=' + $(this).val(),
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {
                            // alert(data.error);
                            alert("Funny moments");
                        } else {

                        }
                    }
                }
            });
        });
        // $(document).on("click", "#callapp_show_filter_button", function () {
        //     if($('.callapp_filter_body').attr('myvar') == 0){
        //         $('.callapp_filter_body').css('display','block');
        //         $('.callapp_filter_body').attr('myvar',1);

        //         $('#go_exel').css('top','264px');
        //         $("#shh").css('transform', 'rotate(0)');
        //     }else{
        //         $('.callapp_filter_body').css('display','none');
        //         $('.callapp_filter_body').attr('myvar',0);

        //         $('#go_exel').css('top','171px');
        //         $("#shh").css('transform', 'rotate(180deg)');

        //     }
        // });
        $(document).on("click", "#callapp_show_filter_button2", function() {
            if ($('.callapp_filter_body2').attr('myvar') == 0) {
                $('.callapp_filter_body2').css('display', 'block');
                $('.callapp_filter_body2').attr('myvar', 1);

                $("#shh2").css('transform', 'rotate(0)');
            } else {
                $('.callapp_filter_body2').css('display', 'none');
                $('.callapp_filter_body2').attr('myvar', 0);

                $("#shh2").css('transform', 'rotate(180deg)');

            }
        });


        $(document).on("click", "#show_copy_prit_exel", function() {
            if ($(this).attr('myvar') == 0) {
                $('.ColVis,.dataTable_buttons').css('display', 'block');
                $(this).css('background', '#2681DC');
                $(this).children('img').attr('src', 'media/images/icons/select_w.png');
                $(this).attr('myvar', '1');
            } else {
                $('.ColVis,.dataTable_buttons').css('display', 'none');
                $(this).css('background', '#E6F2F8');
                $(this).children('img').attr('src', 'media/images/icons/select.png');
                $(this).attr('myvar', '0');
            }
        });
        $(document).on("click", "#show_station", function() {
            $(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
            $(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
            $(this).css('background', '#ccc');
            $(this).css('color', '#000');
            $("#whoami").val(1)
            if ($("#check_state").val() == 0) {
                $('#pirveli').html('რიგი');
                $('#meore').html('შიდა ნომერი');
                $('#mesame').html('თანამშრომელი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ნომერი');
                $('#meeqvse').html('დრო');
                $('#meshvide').html('სახელი');

                $('#q_pirveli').html('რიგი');
                $('#q_meore').html('პოზიცია');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('სახელი');
                $('#q_mexute').html('ლოდინის დრო');
            } else {
                $('#mini_pirveli').html('შიდა ნომერი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');
                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('ნომერი');
            }
            // runAjax();
        });
        $(document).on("click", "#show_chat", function() {
            $(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
            $(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
            $(this).css('background', '#ccc');
            $(this).css('color', '#000');
            $("#whoami").val(0)
            if ($("#check_state").val() == 0) {
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('ბრაუ.');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('ქვეყანა');
                $('#q_mesame').html('ბრაუზერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            } else {
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სახელი');
            }
            // runAjax();
        });
        $(document).on("click", "#show_site", function() {

            $(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
            $(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
            $(this).css('background', '#ccc');
            $(this).css('color', '#000');
            $("#whoami").val(2);
            if ($("#check_state").val() == 0) {
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            } else {
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სურათი');
                $('#m_q_mesame').html('სახელი');
            }
            // runAjax();
        });

        $(document).on("click", "#show_mail", function() {

            $(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
            $(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
            $(this).css('background', '#ccc');
            $(this).css('color', '#000');
            $("#whoami").val(3);
            if ($("#check_state").val() == 0) {
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            } else {
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სურათი');
                $('#m_q_mesame').html('სახელი');
            }
            // runAjax();
        });
        $(document).on("click", "#show_fbm", function() {
            $(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
            $(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
            $(this).css('background', '#ccc');
            $(this).css('color', '#000');
            $("#whoami").val(4);
            if ($("#check_state").val() == 0) {
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            } else {
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სახელი');
            }
            // runAjax();
        });

        $(document).on("click", "#show_video", function() {
            $(".flash_menu_link").css('background', 'rgb(255, 255, 255)');
            $(".flash_menu_link").css('color', 'rgb(38, 129, 220)');
            $(this).css('background', '#ccc');
            $(this).css('color', '#000');
            $("#whoami").val(6);
            if ($("#check_state").val() == 0) {
                $('#pirveli').html('ქვეყანა');
                $('#meore').html('სურათი');
                $('#mesame').html('მომ. ავტორი');
                $('#meotxe').html('სტატუსი');
                $('#mexute').html('ოპერატორი');
                $('#meeqvse').html('ხ-ბა');
                $('#meshvide').html('სულ ხ-ბა');

                $('#q_pirveli').html('პოზიცია');
                $('#q_meore').html('სურათი');
                $('#q_mesame').html('ნომერი');
                $('#q_meotxe').html('მომართვის ავტორი');
                $('#q_mexute').html('ლოდინის დრო');
            } else {
                $('#mini_pirveli').html('ოპერატორი');
                $('#mini_meore').html('მომ. ავტორი');
                $('#mini_mesame').html('სტატუსი');

                $('#m_q_pirveli').html('პოზიცია');
                $('#m_q_meore').html('სახელი');
            }
            // runAjax();
        });


        function listen(file,autoDialer_check = "") {
            $('#auau').each(function() {
                this.pause(); // Stop playing
                this.currentTime = 0; // Reset time
            });
            console.log(autoDialer_check);
            if(autoDialer_check == 1){
            console.log("if");
                var url = 'http://172.16.0.80:8000/autodialer/' + file;
            }else{
            console.log("else");
                var url = 'http://172.16.0.80:8000/' + file;
            }
            console.log(url);
            $("#auau source").attr('src', url);
            $("#auau").load();
        }

        function listen2(file) {
            $('#auau').each(function() {
                this.pause(); // Stop playing
                this.currentTime = 0; // Reset time
            });
            var url = file;
            $("#auau source").attr('src', url);
            $("#auau").load();
        }



        $(document).on("click", "#micr", function() {

            if (micr_val == 1) {
                $('#micr').attr('src', 'media/images/icons/4.png');
                $(this).attr('value', '0');
                micr_val = 0;
            } else {
                $('#micr').attr('src', 'media/images/icons/5.png');
                $(this).attr('value', '1');
                micr_val = 1;
            }

            param = new Object();
            param.act = "save_micr";
            param.activeStatus = micr_val;

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {} else {

                        }
                    }
                }
            });
        });





        // toggle dnd button
        $(document).on("click", "#incallToggleDnd", function() {

            // define extention from php session
            const phpSesExten = '<?php echo $_SESSION['EXTENSION']; ?>';

            if (phpSesExten === 0 || phpSesExten === "0" || phpSesExten === undefined || phpSesExten === null) {
                alert("თქვენ არ გაქვთ არჩეული ექსთენშენი, ამიტომ მოქმედების განხორციელება შეუძლებელია");
            } else {
                var activeStatus = $("#dnd_on_local_status").val();
                if (activeStatus == 0) {
                    $("#dnd_on_local_status").val(1)
                    var action = 'local';
                    activeStatus = 1;
                } else if (activeStatus == 1) {
                    $("#dnd_on_local_status").val(2)
                    var action = 'dndon';
                    activeStatus = 2;
                } else if (activeStatus == 2) {
                    $("#dnd_on_local_status").val(0)
                    var action = 'dndoff';
                    activeStatus = 0;
                }

                param = new Object();
                param.act = "change_incall_dnd_status";
                param.activeStatus = $("#dnd_on_local_status").val();

                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {} else {
                                //     					$.post("AsteriskManager/dndAction.php", {
                                //     						action: action
                                //     					}, result => {
                                if (activeStatus == 0) {
                                    $("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_ON.png');
                                } else if (activeStatus == 1) {
                                    $("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_INSIDE.png');
                                } else if (activeStatus == 2) {
                                    $("#incallToggleDnd").attr('src', 'media/images/icons/comunication/Phone_OFF.png');
                                }
                                //});
                            }
                        }
                    }
                });
            }
        });

        // open activitie chooe container
        $(document).on("click", ".callapp-op-activitie-wrapper", function(event) {

            // stop propagation
            event.stopPropagation();

            // define open status
            const openStatus = $("#callapp_op_activitie_choose").data("active");

            // check active status
            if (openStatus) {
                $("#callapp_op_activitie_choose").data("active", false).removeClass("on");
            } else {
                $("#callapp_op_activitie_choose").data("active", true).addClass("on");
            }

        });

        // choose activitie
        $(document).on("click", "#callapp_op_activitie_choose li:not(.is-active)", function() {

            // reactivate activitie in list
            $("#callapp_op_activitie_choose li").removeClass("is-active");
            $(this).addClass("is-active");

            // define activitie data
            $D.chosedActivitieId = $(this).data("id");
            const activitieColor = $(this).data("color");
            const activitieName = $(this).find(".activitie-name").text();

            // define request data
            const data = {
                act: "change_incall_activitie_status",
                activitieId: $D.chosedActivitieId,
                type: "on"
            };

            // send ajax request
            $.post(aJaxURL, data, responce => {

                // restruct responce
                const {
                    error
                } = responce;

                // check for error
                if (error !== "") {
                    alert(error);
                } else {

                    // change color label
                    $(".callapp-op-activitie-color").css({
                        background: activitieColor
                    });

                    // fix selected activitie
                    $(".callapp-op-activitie-choose-receiver").text(activitieName);

                    // start time count
                    $D.activitieOnTimer();

                    // chat status off
                    toggleChatStatus('#chat_status', 1);

                    // on dnd
                    $.post("AsteriskManager/dndAction.php", {
                        action: "dndon"
                    });
                }

            });

        });

        // zero fixer
        function zeroFixer(a) {
            return a < 10 ? `0${a}` : a;
        }

        //open template dialog
        $(document).on("click", "#smsTemplate", function() {

            //request data
            let data = {
                act: "get_sms_template_dialog"
            };

            //buttons
            let buttons = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };

            //ajax request
            $.post("server-side/call/incomming.action.php", data, structure => {

                //open dialog malibu
                $("#sms-templates").html(structure.html);
                GetDialog("sms-templates", "530", "auto", buttons, "right+100 top");

                GetDataTable("sms_template_table", aJaxURL, "phone_directory", 4, "", 0, "", 2, "desc", '', "<'F'lip>");
                $("#sms_template_table_length").css('top', '0px');
            });

        });

        function LoadDialog(fNm) {
            $("#loading").show();
            if (fNm == 'add-edit-form-comunications') {

                var buttons = {
                    // "ipaddres_block": {
                    //     text: "IP დაბლოკვა",
                    //     id: "ipaddres_block"
                    // },
                    // "num_block": {
                    //     text: "ნომრის დაბლოკვა",
                    //     id: "num_block"
                    // },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            var buttons1 = {
                                "done": {
                                    text: "კი",
                                    id: "save-yes",
                                    click: function() {
                                        $("#add-edit-form-comunications").dialog("close");
                                        $(this).dialog("close");

                                        if(localStorage.getItem('new_case_dialog_opened') == "1") {
                                            if(localStorage.getItem('new_case_dialog_inc_id') == "" || localStorage.getItem('new_case_dialog_inc_id') == 0) {
                                                console.log("can't find hidden incomming_id in localstorage");
                                            }else{
                                                var inc_id_new = localStorage.getItem('new_case_dialog_inc_id');
                                                $.ajax({
                                                    url: Comunications_URL,
                                                    data: {
                                                        act: "cancel_dialog",
                                                        inc_id:  inc_id_new
                                                    },
                                                    success: function(res) {
                                                        console.log("new case incomming_id has been deleted because of cancelation");
                                                        localStorage.removeItem('new_case_dialog_opened');
                                                        localStorage.setItem('new_case_dialog_opened', 0);

                                                        localStorage.removeItem('new_case_dialog_inc_id');
                                                        localStorage.setItem('new_case_dialog_inc_id', "");
                                                    },
                                                    error: function(res) {
                                                        console.log("deleting temporary case failed");
                                                        console.log(res);
                                                    }
                                                });
                                            }
                                        }
                                        if ($("#status-place > a > span").html() == 'რეგისტრაცია' || $("#status-place > a > span").html() == 'ონლაინ ჩათი/ზარი' || $("#status-place > a > span").html() == 'ონლაინ ჩათი' || $("#status-place > a > span").html() == 'მონიტორინგი') {
                                        }
                                        else {
                                        $.ajax({
                                            url: "AsteriskManager/dndAction.php",
                                            data: {
                                                action: 'dndoff',
                                                activity_id: 1
                                            },
                                            success: function(res) {
                                                var statusBG = $('#status-place > a > x');
                                                var statusName = $('#status-place > a > span');
                                                $('#aStatus li[data-id=1]').css("background-color", "");
                                                statusBG.css("background-color", "#2dc100");
                                                statusName.text('აქტიური');
                                            
                                                $('.icon_status_color').css("background-color", "#2dc100");
                                            }
                                        });
                                        $.ajax({
                                            url: 'includes/menu.action.php',
                                            data: {
                                                act: "activity_status",
                                                status_id: 1
                                            },
                                            success: function(res) {
                                    
                                            }
                                        });
                                        
                                        }
                                        
                                        localStorage.setItem('dialog_opened', 0);
                                    }
                                },
                                "no": {
                                    text: "არა",
                                    id: "save-no",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            };
                            GetDialog("hint_close", 300, "auto", buttons1);
                            $("#save-yes").css('float', 'right');
                            $("#save-no").css('float', 'right');
                        }
                    },
                    "done": {
                        text: "შენახვა",
                        id: "save-automated-dialog"
                    }

                };


                /* Dialog Form Selector Name, Buttons Array */
                var width = 1270;
                GetDialog("add-edit-form-comunications", width, "auto", buttons);
                //localStorage.setItem('dialog_opened', 1);
                $(".save-incomming-main-dialog").button();
                $('.add-edit-form-class .ui-dialog-titlebar-close').remove();
                $('.add-edit-form-class').css('left', '84.5px');
                $('.add-edit-form-class').css('top', '0px');
                $('.ui-dialog-buttonset').css('width', '100%');
                $("#cancel-dialog").css('float', 'right');
                $("#save-dialog").css('float', 'right');
                $("#search_ab_pin").button();
                $('#add_comment').button();
                GetDateTimes("vizit_datetime");

                $.ajax({
                    url: Comunications_URL,
                    data: {
                        'act': 'get_fieldsets',
                        'setting_id': 4
                    },
                    success: function(data) {
                        $(data.chosen_keys).chosen({
                            search_contains: true
                        });

                        if (data.datetime_keys != '') {
                            var datetimes = data.datetime_keys;

                            datetimes.forEach(function(item, index) {
                                GetDateTimes(item);
                            });
                        }

                        if (data.date_keys != '') {
                            var date = data.date_keys;

                            date.forEach(function(item, index) {
                                GetDate(item);
                            });
                        }
                        if (data.multilevel_keys != '') {
                            var date = data.multilevel_keys;
                            var main = '';
                            var secondary = '';
                            var third = '';

                            date.forEach(function(item, index) {
                                if (index == 0) {
                                    main = item;
                                } else if (index == 1) {
                                    secondary = item;
                                } else {
                                    third = item;
                                }
                            });
                        }

                        $(document).on("change", "#" + main, function() {
                            param = new Object();
                            param.act = "cat_2";
                            param.selector_id = $("#" + main).attr('id');
                            param.cat_id = $("#" + main).val();
                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function(data) {
                                    $("#" + secondary).html(data.page);
                                    $("#" + secondary).trigger("chosen:updated");

                                    if ($("#" + secondary + " option:selected").val() == 0) {
                                        param = new Object();
                                        param.act = "cat_3";
                                        param.cat_id = $("#" + secondary).val();
                                        $.ajax({
                                            url: Comunications_URL,
                                            data: param,
                                            success: function(data) {
                                                $("#" + third).html(data.page);
                                                $("#" + third).trigger("chosen:updated");
                                            }
                                        });
                                    }
                                }
                            });
                        });
                        $(document).on("change", "#" + secondary, function() {
                            param = new Object();
                            param.act = "cat_3";
                            param.selector_id = $("#" + main).attr('id');
                            param.cat_id = $("#" + secondary).val();
                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function(data) {
                                    $("#" + third).html(data.page);
                                    $("#" + third).trigger("chosen:updated");
                                }
                            });
                        });

                        $(".dep").chosen();
                        $(".dep_project").chosen();
                        $(".dep_sub_project").chosen();
                        $(".dep_sub_type").chosen();
                        ///BLOCKING CONTRACT FIELDSET BEGIN
                        $(".dialog-tab-20 input").each(function(index) {
                            $(this).prop('disabled', true);
                        });
                        $("#servisi___259--146--8").prop('disabled', true).trigger("chosen:updated");

                        ///BLOCKING CONTRACT FIELDSET END

                    }
                });

                $("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0,#region,#municip,#temi,#village").chosen();

                $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming_status_1, #incomming_status_1_1, #inc_status_a, #company, #space_type, #chat_language, #s_u_status").chosen({
                    search_contains: true
                });
                var dLength = [
                    [5, 10, 30, -1],
                    [5, 10, 30, "ყველა"]
                ];

                $("#my_site_resume_chosen").css('width', '560px');
                GetDate('start_check');
                GetDate('end_check');
                //GetDataTable("table_history", Comunications_URL, "get_list_history", 7, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id=''", 0, dLength, 1, "desc", '', "<'F'lip>");

                $("#add_project_info").button();


                var hidden = '&hidden=' + $('#hidde_incomming_call_id').val();
                LoadKendoTable__Info_Projects(hidden);

                setTimeout(function() {
                    var grid = $("#kendo_project_info_table").data("kendoGrid");
                    grid.hideColumn(0);

                    $("#kendo_project_info_table").kendoTooltip({
                        show: function(e) {
                            if (this.content.text().length > 30) {
                                this.content.parent().css("visibility", "visible");
                            }
                        },
                        hide: function(e) {
                            this.content.parent().css("visibility", "hidden");
                        },
                        filter: "td",
                        position: "right",
                        content: function(e) {
                            // var dataItem = $("#kendo_project_info_table").data("kendoGrid").dataItem(e.target.closest("tr"));  
                            // console.log("bottom ", dataItem.info)
                            var content = e.target[0].innerText;
                            return content;
                        }
                    }).data("kendoTooltip");
                }, 3000)


                // GetDataTable("project_ifo_table", Comunications_URL, "get_list_info_project", 4, "&id="+$('#hidde_incomming_call_id').val(), 0, dLength, 1, "desc", '', "<'F'lip>");
                // $("#project_ifo_table_length").css('top','0px');
                // $("#project_ifo_table_length select").css('height','16px');
                // $("#project_ifo_table_length select").css('width','56px');
                // $("#table_history_length").css('top','0px');
                // $("#table_sms_length").css('top','0px');

                $(".jquery-ui-button").button();

                $("#call_content").off();
                $("#call_content1").off();
                $("#send_message").off();
                if ($("#chat_source").val() == 'mail') {
                    $("#send_message").css("display", "none");
                    $("#send_message").replaceWith('<textarea contenteditable="true" placeholder="შეიყვანეთ ტექსტი" id="send_message2" style="padding: 0 8px; border:0px;width:85%;height:120px;resize:none;overflow-y: auto;"></textarea>');

                    $("#dialog_emojis").css("display", "none");
                    $("#choose_button5").css("right", "41px");
                    CKEDITOR.replace('send_message2');
                    $("#choose_docs").css('right', '68px');
                    $("#signatures").css('display', 'block');
                }

                $.ajax({
                    url: Comunications_URL,
                    data: {
                        'act': 'get_all_chat',
                        'chat_id': $('#chat_original_id').val(),
                        'source': $("#chat_source").val()
                    },
                    success: function(data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {

                            } else {
                                console.log(data.sms);
                                $('#log').html('');
                                $('#log').html(data.sms);
                                chat_detail_id = data.chat_detail_id;
                                $("#chat_name").val(data.name);
                                if (data.chat_user_id == '') {
                                    $("#start_chat_wrap").show();
                                } else {
                                    $("#start_chat_wrap").hide();
                                }

                                document.getElementById('chat_scroll').scrollTop = 25000000000;
                                jQuery("time.timeago").timeago();
                                $('#chat_hist').css('border-bottom', '2px solid transparent');
                                $('#chat_live').css('border-bottom', '2px solid #333');
                                all_chats = 1;
                                var source = $('#chat_source').val() || 'phone';
                                var index = $('#incoming_chat_tabs a[href="#incoming_chat_tab_' + source).parent().index();
                                $("#incoming_chat_tabs").tabs("option", "active", index);

                            }
                        }
                    }
                });

                $("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });
                $("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });

                $("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
                    width: 360,
                    multiple: true,
                    matchContains: true
                });

                //////////
                var id = $("#incomming_id").val();
                var cat_id = $("#category_parent_id").val();

                if (id != '' && cat_id == 407) {
                    $("#additional").removeClass('hidden');
                }
                if ($('#ipaddres').val() == 0 || $('#ipaddres').val() == '') {
                    $('#ipaddres_block').css('display', 'none');
                }
                GetDateTimes("problem_date");
                GetDateTimes("transaction_date_time");


                if ($("#tab_id").val() == 5) {
                    $("#num_block, .save-incomming-main-dialog").css('display', 'none');
                } else {
                    $("#num_block, .save-incomming-main-dialog").css('display', '');
                }


                if (($('#imchat').val() == 1 && $('#ast_source').val() == '')) {
                    $('#source_id').val(4);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else if (($('#fb_chat').val() > 0 && $('#ast_source').val() == '')) {
                    $('#source_id').val(6);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else if (($('#mail_chat').val() > 0 && $('#ast_source').val() == '')) {
                    $('#source_id').val(7);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else if (($('#viber_chat').val() > 0 && $('#ast_source').val() == '')) {
                    $('#source_id').val(14);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else {
                    //$('#source_id option:eq(1)').prop('selected', true);
                    $('#chatcontent,#gare_div').css('display', 'none');
                    $('#chatistema').css('width', '210px');
                    $('.add-edit-form-class').css('width', '879px');
                    $("#cancel-chat").css('display', 'none');
                    $('.add-edit-form-class .ui-dialog-title').html('ზარის დამუშავება');
                    $('.add-edit-form-class .ui-dialog-titlebar').css('background', '#673AB7');
                    $('.add-edit-form-class .ui-dialog-title').css('color', '#fff');
                }



                jQuery("time.timeago").timeago();
                //getchats();
                $('#choose_button').button();


                ///////////////////
                // ai dunia      //
                ///////////////////
                // if($('#add-edit-form-comunications input').length != 0){

                /*
                $.ajax({
                    url: "server-side/call/getchats.action.php",
                    data: "act=getchats",
                    dataType: "json",
                    success: function(data) {
                        if(data.count != 0 && $D.sessionStatus == 1){
                            $('#chat_count').css('background','#FFCC00');
                            setTimeout(function(){
                                $('#chat_count').css('background','#F44336');
                                }, 400);
                            var audio = new Audio('sms.mp3');
                            if(micr_val == 0){
                                audio.play();
                                }
                        }else{
                            $('#chat_count').css('background','none');
                        }

                        $("#incoming_chat_tab_chat").html(data.page.chat);
                        $("#incoming_chat_tab_site").html(data.page.site);
                        $("#incoming_chat_tab_mail").html(data.page.mail);
                        $("#incoming_chat_tab_fbm").html(data.page.fbm);
                        $('#chat_count').attr('first_id', data.first_id);
                    }
                });
                */
                //     if($('#chat_user_id').attr('ext') !='0'){
                //         $.ajax({
                //             url: "AsteriskManager/checkme.php",
                //             data: "ext="+$('#chat_user_id').attr('ext'),
                //             dataType: "json",
                //             success: function(data) {
                //                 //console.log(data.phone);
                //                 $('#chat_call_live').attr('src','media/images/icons/'+data.icon);
                //                 if(data.phone != '' && data.phone != null){
                //                     if(data.ipaddres == '2004'){
                //                         iporphone = data.ipaddres;
                //                     }else{
                //                         iporphone = data.phone;
                //                     }
                //                     $('#chat_call_queue').css('display','table-row');
                //                     $('#chat_call_number').html(iporphone);
                //                     $('#chat_call_duration').html(data.duration);
                //                     $('#chat_call_number').attr('extention',$('#chat_user_id').attr('ext'));
                //                     $('#chat_call_number').attr('number',data.phone);
                //                     $('#chat_call_number').attr('ipaddres',data.ipaddres);
                //                 }else{
                //                     $('#chat_call_queue').css('display','none');
                //                     $('#chat_call_number').html('');
                //                     $('#chat_call_duration').html('');
                //                 }
                //             }
                //         });
                //     }else{
                //         $('#main_call_chat').css('display','none');
                //     }
                // }
                all_chats = 1;
                $("#incoming_chat_tabs").tabs();

                ////////////////////
            } else if (fNm == 'crm_dialog') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                };
                GetDialog("crm_dialog", 900, "auto", buttons, "top");
                GetDataTable("example_all_file", Comunications_URL_crm, "get_list", 3, "request_table_id=" + $("#request_table_id").val(), 0, "", 1, "desc", "", '');
                SetEvents("", "", "", "example_all_file", "crm_file_dialog", Comunications_URL_crm_file);
                setTimeout(function() {
                    $('.ColVis, .dataTable_buttons').css('display', 'none');
                }, 90);
            } else if (fNm == 'crm_file_dialog') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                };
                GetDialog("crm_file_dialog", "auto", "auto", buttons, "top");

            } else if (fNm == 'dep_additional') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    "save": {
                        text: "შენახვა",
                        id: "save-additional",
                        click: function() {
                            $.ajax({
                                url: Comunications_URL,
                                data: {
                                    'act': 'get_inputs_ids_addit',
                                    'cat_id': $("#addit_cat_id").val(),
                                    'dep_project_id': $("#dep_project_0").val()
                                },
                                success: function(data) {
                                    var query = 'act=save-custom-addit&info_board=' + $("#addit_info_board").val() + '&processing_id=' + $("#hidden_incomming_call_id").val();
                                    if (data.input_ids != '') {
                                        var input_ids = data.input_ids;

                                        input_ids.forEach(function(item, index) {
                                            if ($("#" + item + "[data-nec='1']").val() == '') {
                                                $("#" + item).css('border', '1px solid red');
                                                ready_to_save++;
                                            } else {
                                                if ($("#" + item).val() != '') {
                                                    query += "&" + item + '=' + $("#" + item).val();
                                                }

                                            }

                                        });

                                    }
                                    query += "&region=" + $("#region").val() + "&municip=" + $("#municip").val() + "&temi=" + $("#temi").val() + "&village=" + $("#village").val();
                                    $.ajax({
                                        url: CustomSaveURL,
                                        type: "POST",
                                        data: query,
                                        dataType: "json",
                                        success: function(data) {
                                            alert('ინფორმაცია შენახულია');
                                        }
                                    });
                                    console.log(query);
                                }
                            });

                        }
                    }
                };
                GetDialog("dep_additional", 700, "auto", buttons, "top");
                $("#dep_additional, .dep_additional-class").css('overflow', 'visible');
            } else if (fNm == 'kendo_docs') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                };
                GetDialog("kendo_docs", 600, "auto", buttons, "top");
            } else if (fNm == 'kendo_contact4') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                };
                GetDialog("kendo_contact4", 600, "auto", buttons, "top");
            } else if (fNm == 'new_mail_addr') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    "save": {
                        text: "შენახვა",
                        id: "save-new-mail-addr",
                        click: function() {
                            alert(324);
                        }
                    }
                };
                GetDialog("new_mail_addr", 450, "auto", buttons, "top");
            } else if (fNm == 'sms_docs') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                };
                GetDialog("add-edit-form-sms", 400, "auto", buttons, "top");
            } else if (fNm == 'email_docs') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                };
                GetDialog("add-edit-form-mail", 600, "auto", buttons, "top");
            } else if (fNm == 'chat_docs') {
                var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                };
                GetDialog("chat-docs-dialog-form", 600, "auto", buttons, "top");
            } else {
                $("#incoming_chat_tabs").tabs();
                var buttons = {
                    //
                    // "ipaddres_block": {
                    //     text: "IP დაბლოკვა",
                    //     id: "ipaddres_block"
                    // },
                    // "num_block": {
                    //     text: "ნომრის დაბლოკვა",
                    //     id: "num_block"
                    // },
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            var buttons1 = {
                                "done": {
                                    text: "კი",
                                    id: "save-yes",
                                    click: function() {
                                        $("#add-edit-form-comunications").dialog("close");
                                        $(this).dialog("close");

                                        

                                        if ($("#status-place > a > span").html() == 'რეგისტრაცია' || $("#status-place > a > span").html() == 'ონლაინ ჩათი/ზარი' || $("#status-place > a > span").html() == 'ონლაინ ჩათი' || $("#status-place > a > span").html() == 'მონიტორინგი') {
                                        }
                                        else {
                                        $.ajax({
                                            url: "AsteriskManager/dndAction.php",
                                            data: {
                                                action: 'dndoff',
                                                activity_id: 1
                                            },
                                            success: function(res) {
                                                var statusBG = $('#status-place > a > x');
                                                var statusName = $('#status-place > a > span');
                                                $('#aStatus li[data-id=1]').css("background-color", "");
                                                statusBG.css("background-color", "#2dc100");
                                                statusName.text('აქტიური');
                                            
                                                $('.icon_status_color').css("background-color", "#2dc100");
                                            }
                                        });
                                        $.ajax({
                                            url: 'includes/menu.action.php',
                                            data: {
                                                act: "activity_status",
                                                status_id: 1
                                            },
                                            success: function(res) {
                                    
                                            }
                                        });
                                        
                                        }
                                       
                                        
                                        
                                        localStorage.setItem('dialog_opened', 0);
                                    }
                                },
                                "no": {
                                    text: "არა",
                                    id: "save-no",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            };
                            GetDialog("hint_close", 300, "auto", buttons1);
                            $("#save-yes").css('float', 'right');
                            $("#save-no").css('float', 'right');
                        }
                    },
                    "done": {
                        text: "შენახვა",
                        id: "save-dialog",
                        class: "save-incomming-main-dialog"
                    }

                };


                /* Dialog Form Selector Name, Buttons Array */
                var width = 1270;
                GetDialog("add-edit-form-comunications", width, "auto", buttons);
                $(".save-incomming-main-dialog").button();
                $('.add-edit-form-class .ui-dialog-titlebar-close').remove();
                $('.add-edit-form-class').css('left', '84.5px');
                $('.add-edit-form-class').css('top', '0px');
                $('.ui-dialog-buttonset').css('width', '100%');
                $("#cancel-dialog").css('float', 'right');
                $("#save-dialog").css('float', 'right');
                $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming_status_1, #incomming_status_1_1, #inc_status_a, #company, #space_type, #chat_language,  #s_u_status").chosen({
                    search_contains: true
                });

                $("#search_ab_pin").button();
                $('#add_comment').button();
                $("#my_site_resume_chosen").css('width', '560px')
                var dLength = [
                    [5, 10, 30, -1],
                    [5, 10, 30, "ყველა"]
                ];
                GetDate('start_check');
                GetDate('end_check');
                //GetDataTable("table_history", Comunications_URL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
                $("#add_project_info").button();
                GetDataTable("project_ifo_table", Comunications_URL, "get_list_info_project", 4, "&id=" + $('#hidde_incomming_call_id').val(), 0, dLength, 1, "desc", '', "<'F'lip>");
                $("#project_ifo_table_length").css('top', '0px');
                $("#project_ifo_table_length select").css('height', '16px');
                $("#project_ifo_table_length select").css('width', '56px');
                $("#table_history_length").css('top', '0px');
                $(".jquery-ui-button").button();
                GetTabs('tabs_sport');

                $("#call_content").off();
                $("#call_content1").off();
                $("#send_message").off();

                $("#call_content").autocomplete("server-side/seoy/seoy_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });

                $("#call_content1").autocomplete("server-side/seoy/seoy_textarea1.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
                    width: 300,
                    multiple: true,
                    matchContains: true
                });

                $("#send_message").autocomplete("server-side/seoy/chat_textarea.action.php?source_id=" + $("#source_id").val() + "&site=" + $("#my_site").chosen().val(), {
                    width: 360,
                    multiple: true,
                    matchContains: true
                });

                var id = $("#incomming_id").val();

                if ($("#tab_id").val() == 5) {
                    $("#num_block, .save-incomming-main-dialog").css('display', 'none');
                } else {
                    $("#num_block, .save-incomming-main-dialog").css('display', '');
                }

                if (($('#imchat').val() == 1 && $('#ast_source').val() == '')) {
                    $('#source_id').val(4);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else if (($('#fb_chat').val() > 0 && $('#ast_source').val() == '')) {
                    $('#source_id').val(6);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else if (($('#mail_chat').val() > 0 && $('#ast_source').val() == '')) {
                    $('#source_id').val(7);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else if (($('#viber_chat').val() > 0 && $('#ast_source').val() == '')) {
                    $('#source_id').val(14);
                    $('#source_id').trigger("chosen:updated");
                    $('.add-edit-form-class .ui-dialog-title').html('შემომავალი ჩატი');
                    $('#num_block').css('display', 'none');
                } else {
                    //$('#source_id option:eq(1)').prop('selected', true);
                    $('#chatcontent,#gare_div').css('display', 'none');
                    $('#chatistema').css('width', '210px');
                    $('.add-edit-form-class').css('width', '879px');
                    $("#cancel-chat").css('display', 'none');
                    $('.add-edit-form-class .ui-dialog-title').html('ზარის დამუშავება');
                    $('.add-edit-form-class .ui-dialog-titlebar').css('background', '#673AB7');
                    $('.add-edit-form-class .ui-dialog-title').css('color', '#fff');
                }



                jQuery("time.timeago").timeago();
                //getchats();
                $('#choose_button').button();


                ///////////////////
                // ai dunia      //
                ///////////////////
                if ($('#add-edit-form-comunications input').length != 0) {

                    /*
                    $.ajax({
                        url: "server-side/call/getchats.action.php",
                        data: "act=getchats",
                        dataType: "json",
                        success: function(data) {
                            if(data.count != 0 && $D.sessionStatus == 1){
                                $('#chat_count').css('background','#FFCC00');
                                setTimeout(function(){
                                    $('#chat_count').css('background','#F44336');
                                    }, 400);
                                var audio = new Audio('sms.mp3');
                                if(micr_val == 0){
                                    audio.play();
                                    }
                            }else{
                                $('#chat_count').css('background','none');
                            }

                            $("#incoming_chat_tab_chat").html(data.page.chat);
                            $("#incoming_chat_tab_site").html(data.page.site);
                            $("#incoming_chat_tab_mail").html(data.page.mail);
                            $("#incoming_chat_tab_fbm").html(data.page.fbm);

                            $('#chat_count').attr('first_id', data.first_id);
                        }
                    });
                    if($('#chat_user_id').attr('ext') !='0'){
                        $.ajax({
                            url: "AsteriskManager/checkme.php",
                            data: "ext="+$('#chat_user_id').attr('ext'),
                            dataType: "json",
                            success: function(data) {
                                //console.log(data.phone);
                                $('#chat_call_live').attr('src','media/images/icons/'+data.icon);
                                if(data.phone != '' && data.phone != null){
                                    if(data.ipaddres == '2004'){
                                        iporphone = data.ipaddres;
                                    }else{
                                        iporphone = data.phone;
                                    }
                                    $('#chat_call_queue').css('display','table-row');
                                    $('#chat_call_number').html(iporphone);
                                    $('#chat_call_duration').html(data.duration);
                                    $('#chat_call_number').attr('extention',$('#chat_user_id').attr('ext'));
                                    $('#chat_call_number').attr('number',data.phone);
                                    $('#chat_call_number').attr('ipaddres',data.ipaddres);
                                }else{
                                    $('#chat_call_queue').css('display','none');
                                    $('#chat_call_number').html('');
                                    $('#chat_call_duration').html('');
                                }
                            }
                        });
                    }else{
                        $('#main_call_chat').css('display','none');
                    }
                    */
                }
            }

        }
        // download sms shablon
        $(document).on("click", ".download_shablon", function() {

            //define variables
            var message = $(this).data("message");
            var sms_id = $(this).attr("sms_id");

            //check length of message text
            if (message != "" && message.length <= 150) {

                $('#newSmsText').val(message);
                $('#smsCharCounter').val((message.length) + '/150');

                // set choosed sms id
                $D.choosedSmsId = sms_id;

            } else if (message.length > 150) {
                alert("შაბლონის ტექსტი შეიცავს დასაშვებზე (150) მეტ სიმბოლოს (" + message.length + ")");
            }

            //close sms template dialog
            $("#sms-templates").dialog("close");

        });

        function LoadKendoDocs(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'kendo_docs2';
            var actions = '';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 5;
            var columnsSQL = ["id:string", "name:string", "date:date", "link:string", "act:string"];
            var columnGeoNames = ["ID", "დასახელება", "თარიღი", "ლინკი", "ქმედება"];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_docs_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);
        }

        function LoadKendoContacts(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var gridName = 'kendo_contact2';
            var actions = '';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 10;
            var columnsCount = 5;
            var columnsSQL = ["id:string", "name:string", "pos:string", "phone:string", "email:string"];
            var columnGeoNames = ["ID", "სახელი/გვარი", "თანამდებობა", "ტელეფონი", "E-mail"];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; //IF NEED NOT USE 0


            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END


            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_contacts_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);
        }
        $(document).on('click', '#get_docs_project', function() {
            var project_id = $("#dep_project_0").val();
            if (project_id != 0) {
                $.ajax({
                    url: aJaxURL,
                    data: "act=get_docs_project&project_id=" + project_id + "&inc_id=" + $("#hidden_incomming_call_id").val() + "&info_board=0",
                    success: function(data) {
                        $("#kendo_docs").html(data.page);
                        var hid = "&id=" + project_id;
                        LoadKendoDocs(hid);
                        LoadDialog("kendo_docs");
                    }
                });
            }
        });
        $(document).on('click', '#get_contact_project', function() {
            var project_id = $("#dep_project_0").val();
            if (project_id != 0) {
                $.ajax({
                    url: aJaxURL,
                    data: "act=get_contact_project&project_id=" + project_id + "&inc_id=" + $("#hidden_incomming_call_id").val() + "&info_board=0",
                    success: function(data) {
                        $("#kendo_contact4").html(data.page);
                        var hid = "&id=" + project_id;
                        LoadKendoContacts(hid);
                        LoadDialog("kendo_contact4");
                    }
                });
            }
        });
        $(document).on('click', '#get_contact_project_addit', function() {
            var rand_number = $(this).attr('data-id');
            var project_id = $("#dep_project_" + rand_number).val();
            if (project_id != 0) {
                $.ajax({
                    url: aJaxURL,
                    data: "act=get_contact_project&project_id=" + project_id + "&inc_id=" + $("#hidden_incomming_call_id").val() + "&info_board=0",
                    success: function(data) {
                        $("#kendo_contact4").html(data.page);
                        var hid = "&id=" + project_id;
                        LoadKendoContacts(hid);
                        LoadDialog("kendo_contact4");
                    }
                });
            }
        });
        $(document).on("click", "#get_docs_project_addit", function() {
            var rand_number = $(this).attr('data-id');
            var project_id = $("#dep_project_" + rand_number).val();

            if (project_id != 0) {
                $.ajax({
                    url: aJaxURL,
                    data: "act=get_docs_project&project_id=" + project_id + "&inc_id=" + $("#hidden_incomming_call_id").val() + "&info_board=0",
                    success: function(data) {
                        $("#kendo_docs").html(data.page);
                        var hid = "&id=" + project_id;
                        LoadKendoDocs(hid);
                        LoadDialog("kendo_docs");
                    }
                });
            }
        });

        // send sms
        $(document).on("click", "#mepa_sms_docs", function() {
            var docsIDS = [];
            var entityGrid = $("#kendo_docs2").data("kendoGrid");
            var rows = entityGrid.select();
            rows.each(function(index, row) {
                var selectedItem = entityGrid.dataItem(row);
                // selectedItem has EntityVersionId and the rest of your model
                docsIDS.push(selectedItem.id);
            });

            if (docsIDS != '') {
                $.ajax({
                    url: aJaxURL,
                    data: "act=sms4mepa&type=docs&ids=" + docsIDS,
                    success: function(data) {
                        $("#add-edit-form-sms").html(data.page);
                        var tab = $("#dialog-tab-17 li[aria-selected='true']").attr('data-id');
                        if (tab == 10) {
                            var phone1 = $("#telefoni___183--124--1").val();
                            var phone2 = $("#telefoni_2___201--125--1").val();
                        } else if (tab == 11) {
                            var phone1 = $("#telefoni___823--126--1").val();
                            var phone2 = $("#telefoni_2___986--131--1").val();
                        }
                        if (phone2 != '') {
                            var phone2 = ',' + phone2;
                        }

                        $("#phone_to_send").val(phone1 + phone2);
                        LoadDialog("sms_docs");
                    }
                });
            }
        });

        $(document).on("click", "#mepa_email_docs", function() {
            var contIDS = [];
            var entityGrid = $("#kendo_docs2").data("kendoGrid");
            var rows = entityGrid.select();
            rows.each(function(index, row) {
                var selectedItem = entityGrid.dataItem(row);
                // selectedItem has EntityVersionId and the rest of your model
                contIDS.push(selectedItem.id);
            });

            if (contIDS != '') {
                $.ajax({
                    url: aJaxURL,
                    data: "act=mail4mepa&type=docs&ids=" + contIDS,
                    success: function(data) {
                        $("#add-edit-form-mail").html(data.page);
                        LoadDialog("email_docs");
                        $("#input").cleditor();
                    }
                });
            }
        });




        $(document).on("click", "#mepa_sms_contact", function() {
            var contIDS = [];
            var entityGrid = $("#kendo_contact2").data("kendoGrid");
            var rows = entityGrid.select();
            rows.each(function(index, row) {
                var selectedItem = entityGrid.dataItem(row);
                // selectedItem has EntityVersionId and the rest of your model
                contIDS.push(selectedItem.id);
            });

            if (contIDS != '') {
                $.ajax({
                    url: aJaxURL,
                    data: "act=sms4mepa&type=contact&ids=" + contIDS,
                    success: function(data) {
                        $("#add-edit-form-sms").html(data.page);
                        var tab = $("#dialog-tab-17 li[aria-selected='true']").attr('data-id');
                        if (tab == 10) {
                            var phone1 = $("#telefoni___183--124--1").val();
                            var phone2 = $("#telefoni_2___201--125--1").val();
                        } else if (tab == 11) {
                            var phone1 = $("#telefoni___823--126--1").val();
                            var phone2 = $("#telefoni_2___986--131--1").val();
                        }
                        if (phone2 != '') {
                            var phone2 = ',' + phone2;
                        }

                        $("#phone_to_send").val(phone1 + phone2);
                        LoadDialog("sms_docs");
                    }
                });
            }
        });

        $(document).on("click", "#mepa_email_contact", function() {
            var contIDS = [];
            var entityGrid = $("#kendo_contact2").data("kendoGrid");
            var rows = entityGrid.select();
            rows.each(function(index, row) {
                var selectedItem = entityGrid.dataItem(row);
                // selectedItem has EntityVersionId and the rest of your model
                contIDS.push(selectedItem.id);
            });

            if (contIDS != '') {
                $.ajax({
                    url: aJaxURL,
                    data: "act=mail4mepa&type=contact&ids=" + contIDS,
                    success: function(data) {
                        $("#add-edit-form-mail").html(data.page);
                        LoadDialog("email_docs");
                        $("#input").cleditor();
                    }
                });
            }
        });



        //new sms textarea key up
        $(document).on("keyup", "#newSmsText", function() {

            charCounter(this, "#smsCharCounter");

        });

        function charCounter(checkobj, updateobj) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(updateobj).data("limit");

            $(updateobj).val(`${textLength}/${charLimit}`);

            if (textLength === charLimit) {
                $(checkobj).blur();
            }

        }
        //new sms textarea focus
        $(document).on("keydown", "#newSmsText", function(e) {

            if (e.key !== "Backspace" && e.key !== "Delete") {
                textAreaFocusAllowed(this, "#smsCharCounter");
            }

        });
        //textarea character counter


        //text area focus allowed
        function textAreaFocusAllowed(checkobj, limitinput) {

            var textLength = $(checkobj).val().length;
            var charLimit = $(limitinput).data("limit");

            if (textLength >= charLimit) {
                $(checkobj).blur();
            }

        }

        $(document).on("click", "#web_chat_checkbox", function() {
            if ($("input[id='web_chat_checkbox']:checked").val() == 1) {
                $("#VebChatImg").attr('src', 'media/images/icons/comunication/Chat.png');
            } else {
                $("#VebChatImg").attr('src', 'media/images/icons/comunication/Chat_OFF.png');
            }
        });

        $(document).on("click", "#site_chat_checkbox", function() {
            if ($("input[id='site_chat_checkbox']:checked").val() == 1) {
                // $("#siteImg").attr('src','media/images/icons/comunication/my_site.ico');
            } else {
                // $("#siteImg").attr('src','media/images/icons/comunication/OFF-my.png');
            }
        });

        $(document).on("click", "#messanger_checkbox", function() {
            if ($("input[id='messanger_checkbox']:checked").val() == 1) {
                $("#MessengerImg").attr('src', 'media/images/icons/comunication/Messenger.png');
            } else {
                $("#MessengerImg").attr('src', 'media/images/icons/comunication/Messenger_OFF.png');
            }
        });

        $(document).on("click", "#mail_chat_checkbox", function() {
            if ($("input[id='mail_chat_checkbox']:checked").val() == 1) {
                $("#MailImg").attr('src', 'media/images/icons/comunication/E-MAIL.png');
            } else {
                $("#MailImg").attr('src', 'media/images/icons/comunication/E-MAIL_OFF.png');
            }
        });

        $(document).on("click", "#zomm_div", function() {
            if ($(this).attr('check_zoom') == 0) {
                $(this).attr('check_zoom', '1');
                $(this).html('<<');
                $("#right_side").css('z-index', '-1');
                $("#chat_scroll").css('width', '907px');
            } else {
                $(this).attr('check_zoom', '0');
                $(this).html('>>');
                $("#right_side").css('z-index', '0');
                $("#chat_scroll").css('width', '288px');
            }
        });

        $(document).on("click", "#add_dep", function() {
            $.ajax({
                url: aJaxURL,
                data: "act=add_dep",
                success: function(data) {
                    $("#dep_arrival").append(data.data);
                    var chosen = data.chosen_data;
                    $("#dep_" + chosen + ",#dep_project_" + chosen + ",#dep_sub_project_" + chosen + ",#dep_sub_type_" + chosen).chosen();
                }
            });
        });
        $(document).on("click", ".delete_dep", function() {
            var to_delete = $(this).attr('data-id');

            $("#dep_row_" + to_delete).remove();
        });
        $(document).on("click", "#get_dep_additional", function() {
            var project_id = $("#dep_project_0").val();
            if (project_id != 0) {
                $.ajax({
                    url: aJaxURL2,
                    data: "act=get_additional&project_id=" + project_id + "&inc_id=" + $("#hidden_incomming_call_id").val() + "&info_board=0",
                    success: function(data) {
                        $("#dep_additional").html(data.page);
                        LoadDialog("dep_additional");
                        $("#region,#municip,#temi,#village").chosen();
                    }
                });
            }
        });

        $(document).on("click", "#get_dep_additional_addit", function() {
            var rand_number = $(this).attr('data-id');
            var project_id = $("#dep_project_" + rand_number).val();

            if (project_id != 0) {
                $.ajax({
                    url: aJaxURL2,
                    data: "act=get_additional&project_id=" + project_id + "&inc_id=" + $("#hidden_incomming_call_id").val() + "&info_board=" + rand_number,
                    success: function(data) {
                        $("#dep_additional").html(data.page);
                        LoadDialog("dep_additional");
                        $("#region,#municip,#temi,#village").chosen();
                    }
                });
            }
        });
        $(document).on("click", ".comunication_checkbox", function() {
            param = new Object();
            param.act = "save_user_comunication";

            param.web_chat_checkbox = $("input[id='web_chat_checkbox']:checked").val();
            param.site_chat_checkbox = $("input[id='site_chat_checkbox']:checked").val();
            param.messanger_checkbox = $("input[id='messanger_checkbox']:checked").val();
            param.mail_chat_checkbox = $("input[id='mail_chat_checkbox']:checked").val();
            param.video_call_checkbox = $("input[id='video_call_checkbox']:checked").val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {} else {}
                    }
                }
            });
        });

        // $(document).on("change", "#from_mail_id", function() {
        // 	param 	  = new Object();
        // 	param.act = "get_signature";

        // 	param.site_id   = $(this).val();

        // 	$.ajax({
        //         url: aJaxURL,
        // 	    data: param,
        //         success: function(data) {
        // 			if(typeof(data.error) != 'undefined'){
        // 				if(data.error != ''){
        // 				}else{
        // 					old_text = $("#input").val().split("<br>");
        // 					text     = old_text[1];
        // 					$("#input").html(text+"<br><br><br><br>"+data.signature);
        // 					setTimeout(() => {
        // 						var activeEditor = tinyMCE.get('input');
        // 						var content = text+"<br><br><br><br>"+data.signature;
        // 						activeEditor.setContent(content);
        // 				    }, 100);
        // 				}
        // 			}
        // 	    }
        //     });
        // });

        function check_user_comunication() {
            param = new Object();
            param.act = "check_user_comunication";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if (typeof(data.error) != 'undefined') {
                        if (data.error != '') {} else {
                            if (data.web_chat_checkbox == 1) {
                                $('#web_chat_checkbox').attr('checked', 'checked');
                                $("#VebChatImg").attr('src', 'media/images/icons/comunication/Chat.png');
                            }

                            if (data.site_chat_checkbox == 1) {
                                $('#site_chat_checkbox').attr('checked', 'checked');
                                // $("#siteImg").attr('src','media/images/icons/comunication/my_site.ico');
                            }

                            if (data.messanger_checkbox == 1) {
                                $('#messanger_checkbox').attr('checked', 'checked');
                                $("#MessengerImg").attr('src', 'media/images/icons/comunication/Messenger.png');
                            }

                            if (data.mail_chat_checkbox == 1) {
                                $('#mail_chat_checkbox').attr('checked', 'checked');
                                $("#MailImg").attr('src', 'media/images/icons/comunication/E-MAIL.png');
                            }

                            if (data.video_call_checkbox == 1) {
                                $('#video_call_checkbox').attr('checked', 'checked');
                                $("#VideoImg").attr('src', 'media/images/icons/comunication/Video.png');
                            }
                        }
                    }
                }
            });
        }



        $(document).on("click", "#smsPhoneDir", () => {

            //define data
            let data = {
                act: "phone_dir_list",
                type: 2
            };

            //buttons
            let buttons = {
                "choose": {
                    text: "არჩევა",
                    id: "choose-abonents",
                    click: function() {

                        //execute function whitch collects checked items value
                        choosePhoneFromDir(this);

                    }
                },
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    click: function() {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };

            //send dialog opening request to server side
            $.post(aJaxURL, data, result => {

                //insert received html structure in relevant dialog
                $("#phone-directory-list").html(result.html);

                //open relevant dialog
                GetDialog("phone-directory-list", 960, "auto", buttons, 'center top');
                GetDataTable("phoneDirectoryList", aJaxURL, "get_phone_dir_list", 4, "&type=" + $("#view_type").val(), 0, "", 2, "desc", '', "");
            });

        });

        //get conclusion container data
        function getConclusionContainerData(input) {

            // define variables
            const inputWidth = $(input).outerWidth();
            const charLength = $(input).text().length;
            const nextInput = $(input).next("br").next(".input-div");
            const nextLine = nextInput.length;

            // return data
            return {
                nextInput,
                nextLine,
                inputWidth,
                charLength
            }

        }
        $(document).on("click", ".itm-unit:not(.itm-unit-active)", function() {
            //define variables
            let selector = $(this).data("selector");

            //deactivate all menu items and activate relevant one
            $(".itm-unit").removeClass("itm-unit-active");
            $(this).addClass("itm-unit-active");

            //deactivate info sections and activate menu relevant one
            $(".inc-info-section").removeClass("is-active");
            $(`.inc-info-section[data-selector='${selector}']`).addClass("is-active");

            //stylize chosen objects
            setTimeout(function() {
                $(".iiss-sc-unit").find(".chosen-container").css({
                    width: "100%"
                });
            }, 10);
        });

        function infoEmailButInit(category) {

            let modifiedCategory = category.charAt(0).toUpperCase() + category.slice(1);
            return `mailTo${modifiedCategory}`;

        }
        //function which chooses numbers which was selected in phone directory
        function choosePhoneFromDir(dialogobj) {

            //define variables
            let checkBoxCollector = [];
            let uncheckedCollector = [];
            var receiver = '';
            if ($("#view_type").val() == 1) {
                var receiver = $("#smsAddressee");
            } else {
                var receiver = $("#mail_address");
            }

            let receiverData = receiver.val();
            let returnData;

            //collect checkbox checked values
            $(".pdl-check").each(function(i) {

                //check if checkbox is checked
                if ($(this).is(":checked")) {

                    //get value of checked inut and reach to data collector
                    checkBoxCollector.push($(this).val().trim());
                }

            });

            //collect checkbox unchecked values
            $(".pdl-check").each(function(i) {

                //check if checkbox is checked
                if (!$(this).is(":checked")) {

                    //get value of checked inut and reach to data collector
                    uncheckedCollector.push($(this).val().trim());
                }

            });
            //check if there is already data in receiver input
            if (receiverData !== "") {

                checkBoxCollector = checkBoxCollector.concat(receiverData.split(",").filter(function(item) {
                    return checkBoxCollector.indexOf(item.trim()) < 0;
                }));

            }

            //filter data by unchecked box values
            checkBoxCollector = checkBoxCollector.filter(function(item) {
                return uncheckedCollector.indexOf(item) < 0;
            });

            //join array with comma
            returnData = checkBoxCollector.join();

            //send collected data to receiver
            receiver.val(returnData);

            //close dialog of phone directory list
            $(dialogobj).dialog("close");

        }

        $(document).on("click", "#choose_button_mail", function() {
            $("#choose_mail_file").click();
        });

        $(document).on("change", "#choose_mail_file", function() {
            var file_url = $(this).val();
            var file_name = this.files[0].name;
            var file_size = this.files[0].size;
            var file_type = file_url.split('.').pop().toLowerCase();
            var path = "mailmepa/file/";

            if ($.inArray(file_type, ['pdf', 'pptx', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv']) == -1) {
                alert("დაშვებულია მხოლოდ 'pdf', 'png', 'xls', 'xlsx', 'jpg', 'docx', 'doc', 'csv' გაფართოება");
            } else if (file_size > '15728639') {
                alert("ფაილის ზომა 15MB-ზე მეტია");
            } else {

                $.ajaxFileUpload({
                    url: "server-side/upload/file.action.php",
                    secureuri: false,
                    fileElementId: "choose_mail_file",
                    dataType: 'json',
                    data: {
                        act: "upload_file_mail",
                        button_id: "choose_mail_file",
                        table_name: 'mail',
                        file_name: Math.ceil(Math.random() * 99999999999),
                        file_name_original: file_name,
                        file_type: file_type,
                        file_size: file_size,
                        path: path,
                        table_id: $("#hidden_increment").val(),

                    },
                    success: function(data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                var tbody = '';
                                $("#choose_mail_file").val('');
                                for (i = 0; i <= data.page.length; i++) {
                                    tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                                    tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                                    tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                                    tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
                                    tbody += "<input type='hidden' class=\"attachment_address\" value='" + data.page[i].rand_name + "'>";
                                    $("#paste_files1").html(tbody);
                                }
                            }
                        }

                    }
                });
            }
        });

        function delete_file1(id) {
            $.ajax({
                url: "server-side/upload/file.action.php",
                data: "act=delete_file1&file_id=" + id + "&table_name=mail",
                success: function(data) {

                    var tbody = '';
                    if (data.page.length == 0) {
                        $("#paste_files1").html('');
                    } else {
                        for (i = 0; i <= data.page.length; i++) {
                            tbody += "<div id=\"first_div\">" + data.page[i].file_date + "</div>";
                            tbody += "<div id=\"two_div\">" + data.page[i].name + "</div>";
                            tbody += "<a id=\"tree_div\" download href=\'media/uploads/file/" + data.page[i].rand_name + "\'>ჩამოტვირთვა</a>";
                            tbody += "<div id=\"for_div\" onclick=\"delete_file1('" + data.page[i].id + "')\">-</div>";
                            tbody += "<input type='hidden' class=\"attachment_address\" value='" + data.page[i].rand_name + "'>";
                            $("#paste_files1").html(tbody);
                        }
                    }
                }
            });
        }





        //chat question functions




        $(document).on("click", "#start_chat", function() {
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "set_user",
                    name: $("#chat_name").val(),
                    id: $('#chat_original_id').val()
                },
                success: function(data) {
                    if (data.status == 'OK') {
                        if(data.chat_status == '1'){
                            setTimeout(function(){ 
                            var name = $("#chat_name").val();
                            $("#send_message").html(`გამარჯობა ${name}, რით შემიძლია დაგეხმაროთ ?`);
                            $("#send_chat_program").trigger("click");
                            $("#start_chat_wrap").hide();

                         }, 1800);
                        }
                        
                    } else if (data.status == 'max_reached') {
                        alert('თქვენ არ შეგიძლიათ 6 ჩატზე მეტის აყვანა');
                    } else {
                        alert("ჩატი უკვე წამოწყებულია სხვა ოპერატორის მიერ!");
                    }

                }
            })
        })

        function get_sub_tab4(id, control_tab) {
            document.getElementById('control_tab_4_0').style.display = "none";
            document.getElementById('control_tab_4_1').style.display = "none";
            document.getElementById(control_tab).style.display = "block";

            document.getElementById("control_tab_id_4_0").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_0").style.borderBottomColor = "gray";

            document.getElementById("control_tab_id_4_1").style.borderBottomWidth = "1px";
            document.getElementById("control_tab_id_4_1").style.borderBottomColor = "gray";

            document.getElementById(id).style.borderBottomWidth = "2px";
            document.getElementById(id).style.borderBottomColor = "#0b5c9a";
            document.getElementById(id).children[0].style.color = "black";


        }
        //chat question functions END
        $(document).on("change", ".dep", function() {
            var rand_number = $(this).attr('data-id');

            param = new Object();
            param.act = "dep_1";
            param.cat_id = $("#dep_" + rand_number).val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_project_" + rand_number).html(data.page);
                    $("#dep_project_" + rand_number).trigger("chosen:updated");
                    if ($("#dep_project_" + rand_number + " option:selected").val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $("#dep_project_" + rand_number).val();
                        $.ajax({
                            url: Comunications_URL,
                            data: param,
                            success: function(data) {
                                $("#dep_sub_project_" + rand_number).html(data.page);
                                $("#dep_sub_project_" + rand_number).trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });

        $(document).on("change", ".dep_project", function() {
            var rand_number = $(this).attr('data-id');

            param = new Object();
            param.act = "dep_3";
            param.cat_id = $("#dep_project_" + rand_number).val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_sub_project_" + rand_number).html(data.page.data2);
                    $("#dep_sub_project_" + rand_number).trigger("chosen:updated");

                    $("#dep_" + rand_number).html(data.page.data3);
                    $("#dep_" + rand_number).trigger("chosen:updated");
                }
            });

            var project_id = $("#dep_project_" + rand_number).val();
            $.ajax({
                url: Comunications_URL,
                data: "act=calculate_fields&project_id=" + project_id,
                success: function(data) {
                    $("#get_dep_additional_addit[data-id='" + rand_number + "'] .field_notifier").remove();
                    $("#get_dep_additional_addit[data-id='" + rand_number + "']").append('<div class="field_notifier"><p><span id="field_done">' + data.r_fields + '</span>/<span id="field_no_done">' + data.fields_cc + '</span></p></div>');
                    if (data.r_fields == data.fields_cc) {
                        $("#get_dep_additional_addit[data-id='" + rand_number + "'] .field_notifier").css('background-color', '#21ff00');
                    }
                }
            });
        });


        $(document).on("change", "#region", function() {
            param = new Object();
            param.act = "get_region";
            param.region_id = $("#region").val();
            if(param.region_id == 0){
                
                $("#municip > option:selected").removeAttr("selected");
                $("#municip > option[value=0]").attr("selected","selected");
                $("#municip").trigger("chosen:updated");

                $("#temi").html("");
                $("#temi").trigger("chosen:updated");
                
                $("#village").html("");
                $("#village").trigger("chosen:updated");
            }
            else{
                $.ajax({
                    url: CustomSaveURL,
                    data: param,
                    success: function(data) {
                        console.log(data.page);
                        $("#municip").html(data.page);
                        $("#municip").trigger("chosen:updated");
                        if ($("#municip option:selected").val() == 999) {
                            param = new Object();
                            param.act = "region_4";
                            param.cat_id = $("#municip").val();
                            $.ajax({
                                url: CustomSaveURL,
                                data: param,
                                success: function(data) {
                                    $("#temi").html(data.page);
                                    $("#temi").trigger("chosen:updated");
                                }
                            });
                        }
                    }
                });
            }
            
        });

        $(document).on("click", "#municip_chosen", function() {
            if($("#region option:selected").val() == 0 && $("#municip option:selected").val() == 0){
                param = new Object();
                param.act = "get_municip";
                param.id = $("#municip").val();
                $.ajax({
                    url: CustomSaveURL,
                    data: param,
                    success: function(data) {
                        console.log("kojo");
                        $("#municip").html(data.page);
                        $("#municip").trigger("chosen:updated");
                    }
                });
            }
        });

        $(document).on("change", "#municip", function() {
            param = new Object();
            param.act = "get_municipipalitet";
            param.municipipalitet_id = $("#municip").val();
            param.parent_id = $("#municip > option:selected").attr("data-parent");
            if($("#region option:selected").val() != $("#municip > option:selected").attr("data-parent")){
                $("#region  option:selected").removeAttr("selected");
                $("#region option[value='"+param.parent_id+"'").prop("selected",true);
                $("#region").trigger("chosen:updated");
            }
            if(param.municipipalitet_id == 0){
                $("#temi").html("");
                $("#temi").trigger("chosen:updated");
                $("#village").html("");
                $("#village").trigger("chosen:updated");
            }
            else{
                $.ajax({
                    url: CustomSaveURL,
                    data: param,
                    success: function(data) {
                        $("#temi").html(data.page);
                        $("#temi").trigger("chosen:updated");
                    }
                });            
            }
        });

        $(document).on("change", "#temi", function() {
            param = new Object();
            param.act = "region_4";
            param.cat_id = $("#temi").val();
            $.ajax({
                url: CustomSaveURL,
                data: param,
                success: function(data) {
                    $("#village").html(data.page);
                    $("#village").trigger("chosen:updated");
                }
            });
        });

        $(document).on("change", "#dep_0", function() {
            param = new Object();
            param.act = "dep_1";
            param.cat_id = $("#dep_0").val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_project_0").html(data.page);
                    $("#dep_project_0").trigger("chosen:updated");
                    if ($("#dep_project_0 option:selected").val() == 999) {
                        param = new Object();
                        param.act = "cat_3";
                        param.cat_id = $("#dep_project_0").val();
                        $.ajax({
                            url: Comunications_URL,
                            data: param,
                            success: function(data) {
                                $("#dep_sub_project_0").html(data.page);
                                $("#dep_sub_project_0").trigger("chosen:updated");
                            }
                        });
                    }
                }
            });
        });
        $(document).on('change', '#dep_sub_project_0', function() {
            param = new Object();
            param.act = "dep_4";
            param.cat_id = $("#dep_sub_project_0").val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_project_0").html(data.page.data);
                    $("#dep_project_0").trigger("chosen:updated");
                    $("#dep_0").html(data.page.data1);
                    $("#dep_0").trigger("chosen:updated");
                }
            });
        });
        $(document).on("change", "#dep_project_0", function() {
            param = new Object();
            param.act = "dep_3";
            param.cat_id = $("#dep_project_0").val();
            $.ajax({
                url: Comunications_URL,
                data: param,
                success: function(data) {
                    $("#dep_0").html(data.page.data3);
                    $("#dep_0").trigger("chosen:updated");

                    $("#dep_sub_project_0").html(data.page.data2);
                    $("#dep_sub_project_0").trigger("chosen:updated");
                }
            });
            var project_id = $("#dep_project_0").val();
            $.ajax({
                url: Comunications_URL,
                data: "act=calculate_fields&project_id=" + project_id,
                success: function(data) {
                    $("#get_dep_additional .field_notifier").remove();
                    $("#get_dep_additional").append('<div class="field_notifier"><p><span id="field_done">' + data.r_fields + '</span>/<span id="field_no_done">' + data.fields_cc + '</span></p></div>');
                    if (data.r_fields == data.fields_cc) {
                        $("#get_dep_additional .field_notifier").css('background-color', '#21ff00');
                    }
                }
            });
        });


        function LoadKendoTable__Info_Projects(hidden) {

            $.ajax({
                url: 'server-side/call/incomming.action.php',
                data: {
                    act: "get_status_items"
                },
                type: "GET",
                success: function(data) {

                    var options = data.options;

                    //KendoUI CLASS CONFIGS BEGIN
                    var aJaxURL = "server-side/call/waiters.action.php";
                    var gridName = 'kendo_project_info_table';
                    var actions = '<button id="add_project_info" style="margin-right: 10px"> დამატება</button><select id="project_info_item_status">' + options.map(o => {
                        return '<option value="' + o.id + '">' + o.name + '</option>'
                    }) + '</select><button data-select="info" id="change_project_status">სტატუსის შეცვლა</button><button id="remove_project_info_item" >წაშლა</button>';
                    var editType = "popup"; // Two types "popup" and "inline"
                    var itemPerPage = 10;
                    var columnsCount = 4;
                    var columnsSQL = [
                        "id:string",
                        "status:date",
                        "project:string",
                        "info:string"
                    ];
                    var columnGeoNames = [
                        "ID",
                        "სტატუსი",
                        "პროექტი",
                        "გაცემული ინფორმაცია"
                    ];

                    var showOperatorsByColumns = [0, 0, 0, 0]; //IF NEED USE 1 ELSE USE 0
                    var selectors = [0, 0, 0, 0]; //IF NEED NOT USE 0

                    var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
                    //KendoUI CLASS CONFIGS END


                    const kendo = new kendoUI();
                    kendo.loadKendoUI(aJaxURL, 'get_list_info_project', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);

                }
            })
        }
        $(document).on('blur', '#shim', function() {
            $(this).focus();
        })

        $(document).on('focusin', '#vizit_location', function() {
            var box = $('.address_box');
            var date = $("#vizit_datetime").val();
            if (date != '' && date != "0000-00-00 00:00:00") {
                box.css("display", "block")
                $.ajax({
                    url: aJaxURL,
                    data: {
                        act: 'get_project_addresses',
                        inc_id: $('#hidden_incomming_call_id').val()
                    },
                    beforeSend: function() {
                        $('.address_box ul').html("<span class='empty-list'>იტვირთება</span>");
                    },
                    success: function(data) {
                        var addressList = data.addresses;
                        $('.address_box ul').html(addressList);
                    }
                })
            }
        });
        $(document).on('focusout', '#vizit_location', function() {
            var box = $('.address_box');
            var date = $("#vizit_datetime").val();
            if (date != '' && date != "0000-00-00 00:00:00") {
                setTimeout(() => {
                    box.css("display", "none")
                }, 500);
            }
        });
        $(document).on('click', ".address_box ul li", function() {
            $('#vizit_location').val($(this).text())
        })


        $(document).ready(function() {
            var kendo = new kendoUI();
            kendo.kendoSelector('operators', 'server-side/call/incomming.action.php', 'get_operators', "ყველა ოპერატორი");

            kendo.kendoSelector('all_calls', 'server-side/call/incomming.action.php', 'get_all_calls', "ყველა ზარი");

            // kendo.kendoSelector('all_info_id','server-side/call/incomming.action.php', 'get_all_info_id', "ყველა ზარი");
            kendo.kendoMultiSelector('call_type', 'server-side/call/incomming.action.php', 'get_call_types', 'ზარის ტიპი');

            kendo.kendoMultiSelector('call_status', 'server-side/call/incomming.action.php', 'get_call_statuses', 'ზარის სტატუსი');

            kendo.kendoMultiSelector('call_sources', 'server-side/call/incomming.action.php', 'get_sources', 'წყარო');

        });


        $(document).on('click', '#save-automated-dialog, .save-incomming-main-dialog', function(e) {


            localStorage.removeItem('new_case_dialog_opened');
            localStorage.setItem('new_case_dialog_opened', 0);

            localStorage.removeItem('new_case_dialog_inc_id');
            localStorage.setItem('new_case_dialog_inc_id', "");

            var baseID = $("#ast_source").val();
            var base;
            baseID = baseID.split("-");

            if (baseID != '') {
                base = baseID[1];
            }
            $.ajax({
                url: Comunications_URL,
                data: {
                    'act': 'get_inputs_ids',
                    'setting_id': 4,
                    'base_id': base
                },
                success: function(data) {
                    
                    var dialogtt = $("#dialogType").val();
                    var idd = $("#hidden_incomming_call_id").val();
                    if (dialogtt == 'autodialer') {
                        idd = '';
                    }
                    var query = 'act=save-custom&setting_id=4&processing_id=' + idd + '&ast_source='+$("#ast_source").val();
                    var query2 = 'act=save-custom-quize&baseID=' + base;
                    query += "&region=" + $("#region").val() + "&municip=" + $("#municip").val() + "&temi=" + $("#temi").val() + "&village=" + $("#village").val();
                    if (data.input_ids != '') {
                        var input_ids = data.input_ids;
                        var checkboxes = data.checkboxes;
                        var campaign_ids = data.campaign_ids;
                        var campaignCheckboxes = data.campaignCheckboxes;
                        var ready_to_save = 0;

                        campaign_ids.forEach(function(item, index) {
                            if ($("#" + item + "[data-nec='1']").val() == '' || $("#" + item + "[data-nec='1']").val() == '0') {
                                $("#" + item).css('border', '1px solid red');
                                var transformed = item.replace(/--/g, "__");
                                $("#" + transformed + "_chosen .chosen-single").css('border', '1px solid #e81f1f');
                                ready_to_save++;
                                alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                            } else {
                                if ($("#" + item).val() != '') {
                                    query2 += "&" + item + '=' + $("#" + item).val();
                                }
                            }

                        });
                        campaignCheckboxes.forEach(function(item, index) {
                            $("input[name='" + item + "']:checked").each(function() {
                                var post_data = this.id;
                                var check = post_data.split("--");

                                var check_id = check[0].split("-");

                                query2 += "&" + this.id + "=" + check_id[1];
                            });
                        });
                        input_ids.forEach(function(item, index) {
                            if ($("#" + item + "[data-nec='1']").val() == '' || $("#" + item + "[data-nec='1']").val() == '0') {
                                $("#" + item).css('border', '1px solid red');
                                var transformed = item.replace(/--/g, "__");
                                $("#" + transformed + "_chosen .chosen-single").css('border', '1px solid #e81f1f');
                                ready_to_save++;
                                alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                            } else {
                                if ($("#" + item).val() != '') {
                                    query += "&" + item + '=' + $("#" + item).val();
                                }
                            }

                        });

                        checkboxes.forEach(function(item, index) {
                            $("input[name='" + item + "']:checked").each(function() {
                                var post_data = this.id;
                                var check = post_data.split("--");

                                var check_id = check[0].split("-");

                                query += "&" + this.id + "=" + check_id[1];
                            });
                        });

                        //default deps//
                        var dep = $("#dep_0").val();
                        var dep_project = $("#dep_project_0").val();
                        var dep_sub_project = $("#dep_sub_project_0").val();
                        var dep_sub_type = $("#dep_sub_type_0").val();

                        if (dep_sub_type == '' || dep_sub_type == '0') {
                            ready_to_save++;
                            alert("შეავსეთ მომართვის ტიპი");
                        }

                        query += "&dep_0=" + dep;
                        query += "&dep_project_0=" + dep_project;
                        query += "&dep_sub_project_0=" + dep_sub_project;
                        query += "&dep_sub_type_0=" + dep_sub_type;

                        ////////////////

                        let dep_array = [];
                        let dep_project_array = [];
                        let dep_sub_project_array = [];
                        let dep_sub_type_array = [];

                        $(".dep").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_array.push("&dep_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_project").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_project_array.push("&dep_project_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_sub_project").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            dep_sub_project_array.push("&dep_sub_project_" + rand_number + "=" + $(this).val());
                        });
                        $(".dep_sub_type").each(function(item) {
                            var rand_number = $(this).attr('data-id');
                            var vall = $(this).val();
                            if (vall == '' || vall == '0') {
                                ready_to_save++;
                                alert("შეავსეთ მომართვის ტიპი");
                                return false;
                            }
                            dep_sub_type_array.push("&dep_sub_type_" + rand_number + "=" + $(this).val());
                        });

                        /* console.log(dep_array);
                        console.log(dep_project_array);
                        console.log(dep_sub_project_array);
                        console.log(dep_sub_type_array); */

                        dep_array.forEach(function(name, index) {
                            query += dep_array[index] + dep_project_array[index] + dep_sub_project_array[index] + dep_sub_type_array[index];
                        });
                        query += "&dialogtype=" + $("#dialogType").val();
                        if (ready_to_save == 0) {
                            $.ajax({
                                url: CustomSaveURL,
                                type: "POST",
                                data: query2,
                                dataType: "json",
                                success: function(data) {}
                            });
                            $.ajax({
                                url: CustomSaveURL,
                                type: "POST",
                                data: query,
                                dataType: "json",
                                success: function(data) {
                                    alert('ინფორმაცია შენახულია');
                                    $("#kendo_incomming_table").data("kendoGrid").dataSource.read();
                                    var source_name = $("#chat_source").val();
                                    if (source_name == 'chat') {
                                        $.ajax({
                                            url: Comunications_URL,
                                            data: "act=chat_end&chat_source=" + $("#chat_source").val() + "+&chat_id=" + $("#chat_original_id").val(),
                                            success: function(data) {
                                                if (typeof data.error != "undefined") {
                                                    if (data.error != "") {
                                                        // alert(data.error);
                                                        //alert("Caucasus Jixv");
                                                    } else {
                                                        $("#chat_close_dialog").dialog("close");
                                                        
                                                    }
                                                }
                                            }
                                        });
                                    }

                                }
                            });
                        }
                        //console.log(query);
                        console.log(query2);
                    }
                }
            });
        });
        $(document).on('click', '.dialog_input_tabs li', function() {
            ///SWITCHING TABS HERE///
            var field_id = $(this).parent().attr('id');
            var tab_id = $(this).attr('data-id');
            $('#' + field_id + ' li').removeAttr('aria-selected');
            $(this).attr('aria-selected', 'true');
            /////////////////////////
            $("." + field_id).css('display', 'none');

            $("." + field_id + "[data-tab-id='" + tab_id + "']").css('display', 'block');

        });
        // $(document).on('click', '#service_ab', function() {
        //     check_beneficiary($("#abonentis_kodi___133--142--1").val());
        // });

        $(document).on("click", ".service_icon", function() {
            console.log($(this).attr("id"))
            switch ($(this).attr("id")) {
                case 'service_ab':
                    identificator = 1;
                    break;
                case 'service_pn':
                    identificator = 2;
                    break;
                case 'service_phone':
                    identificator = 3;
                    break;
            
                default:
                    break;
            }
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_list_abonent",
                    pn:  ($("#piradi_nomeri--129--1").val() != "") ? $("#piradi_nomeri--129--1").val() : $("#saidentifikacio_kodi___940--134--1").val(),
                    code: $("#abonentis_kodi___133--142--1").val(),
                    mobile: $("#telefoni___183--124--1").val(),
                    identify: identificator
                },
                success: function (res) {
                    console.log(res);
                    if(res.hasOwnProperty("error") && res.error != "" && res.error != null) {
                        alert(res.error);
                    }
                    $("#saxeli___899--127--1").val( (res.data.name == "" || res.data.name == null) ? $("#saxeli___899--127--1").val() : res.data.name );
                    $("#gvari___921--128--1").val( (res.data.lastname == "" || res.data.lastname == null) ? $("#gvari___921--128--1").val() : res.data.lastname );
                    $("#E-mail___593--143--1").val( (res.data.email == "" || res.data.email == null) ? $("#E-mail___593--143--1").val() : res.data.email );
                    $("#abonentis_kodi___133--142--1").val( (res.data.code == "" || res.data.code == null) ? $("#abonentis_kodi___133--142--1").val() : res.data.code );
                    $("#telefoni___183--124--1").val( (res.data.mobile == "" || res.data.mobile == null || res.data.mobile == "000-00-00-00") ? $("#telefoni___183--124--1").val() : res.data.mobile );
                    //$("#ragac").val( (res.data.ragac == "" || res.data.ragac == null) ? $("#ragac").val() : res.data.ragac );

                    if(res.data.sex == 1) {
                        $("#sqesi___949-2--130--6").prop("checked",false)
                        $("#sqesi___949-1--130--6").prop("checked",true)
                    }                    
                    else if(res.data.sex == 2) {
                        $("#sqesi___949-1--130--6").prop("checked",false)
                        $("#sqesi___949-2--130--6").prop("checked",true)
                    }                    
                   
                    if(res.data.activity == "სასოფლო-სამეურნეო") {
                        $("#fermeri-2--152--6").prop("checked",false)
                        $("#fermeri-1--152--6").prop("checked",true)
                    }                    
                    else if(alreadyKnown){
                        $("#fermeri-1--152--6").prop("checked",false)
                        $("#fermeri-2--152--6").prop("checked",true)
                    }
                    
                    
                }
            });
        });

        // ------------------------------ START CONTRACT SERVICE ------------------------------------ 
        $(document).on("click", ".for_service_contract", function() {
            if($("#piradi_nomeri--129--1").val() == "" && $("#saidentifikacio_kodi___940--134--1").val() == "")
                alert("გთხოვთ შეავსოთ პირადი ნომერი ან საიდენტიფიკაციო კოდი ინფორმაციის მოსაძებნად");
            else{
                //buttons
                let buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            $(this).dialog("close");
                            $(this).html("");
                        }
                    }
                };

                $("#contract_dialog").html("<div id='kendo_contract_list'></div>");
                GetDialog("contract_dialog", 1300, "auto", buttons, 'center top');

                var hidden = ($("#piradi_nomeri--129--1").val() != "") ? "&code=" +  $("#piradi_nomeri--129--1").val() : "&code=" + $("#saidentifikacio_kodi___940--134--1").val();
                console.log(hidden);
                LoadKendoTable_contracts(hidden);

            }
        });

        $(document).on("click","#service_pn_ferm",function(){
            $.ajax({
                url: "server-side/call/incomming.action.php",
                data: {
                    act: "get_list_abonent",
                    pn:  ($("#piradi_nomeri--129--1").val() != "") ? $("#piradi_nomeri--129--1").val() : $("#saidentifikacio_kodi___940--134--1").val(),
                    code: $("#abonentis_kodi___133--142--1").val(),
                    mobile: $("#telefoni___183--124--1").val(),
                    identify: 2
                },
                success: function (res) {
                    console.log(res);
                    
                    if(res.hasOwnProperty("error") && res.error != "" && res.error != null) {
                        alert(res.error);
                    }
                    $("#saxeli___899--127--1").val( (res.data.name == "" || res.data.name == null) ? $("#saxeli___899--127--1").val() : res.data.name );
                    $("#gvari___921--128--1").val( (res.data.lastname == "" || res.data.lastname == null) ? $("#gvari___921--128--1").val() : res.data.lastname );
                    $("#E-mail___593--143--1").val( (res.data.email == "" || res.data.email == null) ? $("#E-mail___593--143--1").val() : res.data.email );
                    $("#abonentis_kodi___133--142--1").val( (res.data.code == "" || res.data.code == null) ? $("#abonentis_kodi___133--142--1").val() : res.data.code );
                    $("#telefoni___183--124--1").val( (res.data.mobile == "" || res.data.mobile == null || res.data.mobile == "000-00-00-00") ? $("#telefoni___183--124--1").val() : res.data.mobile );
                    //$("#ragac").val( (res.data.ragac == "" || res.data.ragac == null) ? $("#ragac").val() : res.data.ragac );

                    if(res.data.sex == 1) {
                        $("#sqesi___949-2--130--6").prop("checked",false)
                        $("#sqesi___949-1--130--6").prop("checked",true)
                    }                    
                    else if(res.data.sex == 2) {
                        $("#sqesi___949-1--130--6").prop("checked",false)
                        $("#sqesi___949-2--130--6").prop("checked",true)
                    }                    
                   
                    if(res.data.activity == "სასოფლო-სამეურნეო") {
                        $("#fermeri-2--152--6").prop("checked",false)
                        $("#fermeri-1--152--6").prop("checked",true)
                    }                    
                    else if(alreadyKnown){
                        $("#fermeri-1--152--6").prop("checked",false)
                        $("#fermeri-2--152--6").prop("checked",true)
                    }
                    
                    
                }
            });
        });

        $(document).on("dblclick", "#kendo_contract_list tr.k-state-selected", function() {
            // get data from selected row of kendo table
            var grid = $("#kendo_contract_list").data("kendoGrid");
            var dItem = grid.dataItem($(this));
            var data = {
                contract_code:  dItem.contract_code,
                service:        dItem.service,
                price:          dItem.price,
                balance:        dItem.balance,
                land_area:      dItem.land_area,
                fill_date:      dItem.fill_date,
                service_center: dItem.service_center,
                service_id:     dItem.service_id
            }

            $("#contract_dialog").dialog("close");
            $("#contract_dialog").html("");
            console.log(data);

            // write into the fields of contract fieldset 
            $("#_saxelshekrulebo_fartobi___383--148--1").val(data.land_area);
            $("#xelshekrulebis_kodi___221--145--1").val(data.contract_code);
            $("#_xelsh__shevsebis_tarighi--149--5").val(data.fill_date);
            $("#servis_centri___314--151--1").val(data.service_center);
            $("#xelshekrulebis_tanxa___649--147--1").val(data.price);
            $("#balansi___781--150--1").val(data.balance);
            $("#servisi___259--146--8").val(data.service_id);
            $("#servisi___259--146--8").trigger("chosen:updated");
        });

        $(document).on("click", "#contracts_excel", function(){
            const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("kendo_contract_list","კონტრაქტების სია",[]);
                $("#loading1").hide();
            }, 200)
        });

        function LoadKendoTable_contracts(hidden) {
            //KendoUI CLASS CONFIGS BEGIN
            var aJaxURL = "server-side/call/incomming.action.php";
            var gridName = 'kendo_contract_list';
            var actions = '<button style="float:right;" id="contracts_excel">EXCEL რეპორტი</button>';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 20;
            var columnsCount = 8;
            var columnsSQL = [
                "contract_code:string",
                "service:string",
                "price:string",
                "balance:string",
                "land_area:string",
                "fill_date:string",
                "service_center:string",
                "service_id:string"
            ];
            var columnGeoNames = [
                "ხელშეკრულების კოდი",
                "სერვისი",
                "ხელშეკრულების თანხა",
                "ბალანსი",
                "სახელშეკრულებო ფართობი",
                "ხელშ. შევსების თარიღი",
                "სერვის ცენტრი",
                "service_id"
            ];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0];
            var selectors              = [0, 0, 0, 0, 0, 0, 0, 0];

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END

            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list_contracts', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0, '', '', '', true, false, true,[7]);

        }
        // ------------------------------ END CONTRACT SERVICE ------------------------------------











        // $(document).on('click', '.service_icon', function() {
        //     var dat;
        //     var sevice_form = $(this).attr('id');
        //     var phone = $("#telefoni___183--124--1").val();
        //     var code = $("#abonentis_kodi___133--142--1").val();
        //     var pn = $("#piradi_nomeri--129--1").val();

        //     if (sevice_form == 'service_phone') {
        //         dat = 'act=abonent_sevice&phone=' + phone;
        //     } else if (sevice_form == 'service_ab') {
        //         dat = 'act=abonent_sevice&ab_num=' + code;
        //     } else if (sevice_form == 'service_pn') {
        //         dat = 'act=abonent_sevice&pn=' + pn;
        //     }
        //     $.ajax({
        //         url: aJaxURL,
        //         data: dat,
        //         success: function(data) {
        //             var resp_code = data.code;
        //             if (resp_code == 2) {
        //                 var abonenti = data.abonent;
        //                 var ab_type = abonenti.type;

        //                 if (ab_type == 1) {
        //                     $("#piradi_nomeri--129--1").val(abonenti.legal_code);
        //                     $("#saxeli___899--127--1").val(abonenti.name);
        //                     $("#gvari___921--128--1").val(abonenti.lastname);
        //                     $("#E-mail___593--143--1").val(abonenti.email);
        //                     $("#abonentis_kodi___133--142--1").val(abonenti.code);

        //                     if (abonenti.sex == 1) {
        //                         $("#sqesi___949-2--130--6").prop('checked', true);
        //                     } else {
        //                         $("#sqesi___949-1--130--6").prop('checked', true);
        //                     }
        //                 } else {

        //                 }

        //             } else if (resp_code == 0) {
        //                 alert(data.message);
        //             } else if (resp_code == 1) {
        //                 alert('აბონენტი ვერ მოიძებნა');
        //             }
        //         }
        //     })
        // });
    </script>
    <style type="text/css">
        .chosen-container {
            width: 95% !important;
        }

        .chosen-results {
            width: 200px !important;
        }

        .chosen-drop {
            width: 200px !important;
        }

        .address_box {
            position: absolute;
            top: 25px;
            right: 15px;
            background-color: #ffffff;
            border: 1px solid #afafaf;
            border-radius: 4px;
            max-width: 355px;
            padding: 6px 0px;
            width: 100%;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            display: none;
        }

        .address_box ul {
            margin-top: 6px;
            max-height: 55px;
            overflow: auto;
            margin-bottom: 0;
        }

        .address_box ul>li {
            float: unset;
            background-color: #ffffff;
            padding: 3px 8px;
            margin-bottom: 2px;
            border-top: 1px solid #d4d4d4;
            border-bottom: 1px solid #d4d4d4;
        }

        .address_box ul>li:hover {
            background-color: #f5f5f5;
            cursor: pointer;
        }

        .callapp_tabs {
            margin-top: 5px;
            /* margin-bottom: 5px; */
            float: right;
            width: 100%;

        }

        .callapp_tabs span {
            color: #FFF;
            border-radius: 5px;
            padding: 5px;
            float: left;
            margin: 0 3px 0 3px;
            background: #2681DC;
            font-weight: bold;
            font-size: 11px;
            margin-bottom: 2px;
        }

        .callapp_tabs span close {
            cursor: pointer;
            margin-left: 5px;
        }

        .callapp_head {
            font-family: pvn;
            font-weight: bold;
            font-size: 20px;
            color: #2681DC;
        }

        .callapp_head select {
            position: relative;
            top: -2px;
        }

        #chatcontent hr {
            border-top-width: 0px;
        }

        .callapp_head_hr {
            border: 1px solid #2681DC;
        }

        .callapp_refresh {
            padding: 5px;
            border-radius: 8px;
            color: #FFF;
            background: #9AAF24;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }

        .comunication_button {
            padding: 5px;
            border-radius: 8px;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }

        .callapp_dnd_button {
            width: 40px;
            height: 40px;
            background: none;
            border: none;
            cursor: pointer;
            margin: 0 5px;
            position: relative;
            color: #7f8c8d;
            font-size: 21px;
        }

        .callapp_filter_body {
            width: 1030px;
            height: 50px;
            padding: 7px 0px 0px 0px;
            margin-bottom: 0px;
        }

        .callapp_filter_body span {
            float: left;
            margin-right: 10px;
            height: 22px;
        }

        .callapp_filter_body span label {
            color: #555;
            font-weight: bold;
            /* margin-left: 20px; */
        }

        .callapp_filter_header {
            color: #2681DC;
            font-family: pvn;
            font-weight: bold;
        }

        .ColVis,
        .dataTable_buttons {
            z-index: 50;
        }

        /* #flesh_panel{
            height: auto;
            width: 300px;
            position: absolute;
            top: 0;
            padding: 15px;
            right: 2px;
            z-index: 49;
            background: #FFF;
        } */


        #table_right_menu {
            top: 39px;
            z-index: 1;
        }

        /* #table_index_wrapper .ui-widget-header{
            height: 55px;
        } */

        /* -SMS DIALOG STYLES- */
        .new-sms-row {
            width: 100%;
            height: auto;
            margin-top: 11.3px;
            box-sizing: border-box;
        }

        .new-sms-row:last-child {
            margin-top: 4px;
        }

        .new-sms-row.grid {
            display: grid;
            grid-template-columns: repeat(2, 50%);
        }

        .nsrg-col {
            padding-right: 10px;
        }

        .nsrg-col:last-child {
            padding-right: 0;
        }

        .new-sms-input-holder {
            width: 100%;
            min-height: 27px;
            display: flex;
        }

        .new-sms-input-holder input {
            height: 18px;
            flex-grow: 1;
            font-size: 11.3px;
        }

        .new-sms-input-holder textarea {
            height: 75%;
            min-height: 20px;
            flex-grow: 1;
            font-size: 11.3px;
            resize: vertical;
        }

        #smsCopyNum {
            /*button: copy*/
            margin: 0 5px;
        }

        #smsTemplate {
            /*button: open template*/
            margin-right: 0;
        }

        #smsNewNum i {
            /*icon: add new number in send new sms child*/
            font-size: 14px;
        }

        #smsCharCounter {
            /*input: sms character counter*/
            width: 55px;
            height: 18px;
        }

        #smsCharCounter {
            /*input: send new sms character counter*/
            position: relative;
            top: 0;
            text-align: center;
        }

        #sendNewSms {
            /*button: send new message action inplement*/
            float: right;
        }

        .empty-sms-shablon {
            width: 100%;
            height: auto;
            padding: 10px 0;
            text-align: center;
            font-family: pvn;
            font-weight: bold;
        }

        #box-table-b1 {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            text-align: center;
            border-collapse: collapse;
            border-top: 7px solid #70A9D2;
            border-bottom: 7px solid #70A9D2;
            width: 300px;
        }

        #box-table-b th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;
            ;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #start_chat_wrap {
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
            background: #e6f6ec;
            opacity: 0.8;
            display: none;
        }

        #mail_data p {
            font-size: 10px;
        }

        #start_chat {
            margin-top: 60px;
            margin-left: 84px;
            padding: 0px 9px;
            border: none;
            border-radius: 7px;
            background: #009658;
            color: #FFF;
            cursor: pointer;
            position: absolute;
        }

        #start_chat:hover {
            background: #01c26e;
        }

        #box-table-b1 th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;
            ;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #box-table-b td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        #box-table-b1 td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        .download_shablon {

            width: 100%;
            height: 20px;
            background-color: #4997ab;
            border-radius: 2px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: arial;
            font-size: 14px;
            border: 0px;
            text-decoration: none;
            text-overflow: ellipsis;
        }

        #comucation_status {
            position: absolute;
            z-index: 25000000;
            background: #fff;
            width: 199px;
            display: block;
            box-shadow: 0px 0px 25px #888888;
            left: -55%;
        }

        .download_shablon:hover {
            background-color: #ffffff;
            color: #4997ab;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
        }


        .chosen-container#crm_0_chosen {
            width: 200px !important;
        }

        .ui-dialog-titlebar-close {
            display: none;
        }

        .imagespan {
            padding: 0px 0px 0px 10px;
        }

        .comunicationTr {
            border-top: 0.5px #c1cbd0 solid;
        }

        .comunication_name {
            font-family: pvn;
            font-weight: bold;
            font-size: 13px;
            color: #2681DC;
        }


        .download10 {
            background-color: #7d3daf;
            border-radius: 0px;
            display: inline-block;
            cursor: pointer;
            color: #ffffff !important;
            font-size: 12px;
            border: 0px !important;
            text-decoration: none;
            text-overflow: ellipsis;
            width: 100%;
            font-weight: normal !important;
        }


        #kendo_project_info_table tr th:nth-child(1),
        #kendo_project_info_table tr td:nth-child(1) {
            /* display: none; */
        }

        #kendo_project_info_table tr th:nth-child(2),
        #kendo_project_info_table tr td:nth-child(2),
        #kendo_project_info_table tr th:nth-child(3),
        #kendo_project_info_table tr td:nth-child(3) {
            text-align: center;
            width: 100px;
        }

        #kendo_project_info_table tr th:nth-child(3) {
            text-align: center;
        }

        #kendo_project_info_table tr td:nth-child(4) {
            white-space: nowrap;

        }

        #kendo_project_info_table {
            height: 300px !important;
        }

        .videoCallPhotos span img {
            width: 170px;
        }

        .videoCallPhotos span {
            margin: 5px;
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            width: min-content;
            font-family: system-ui;
        }

        .videoCallPhotos {
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
        }

        
    </style>
</head>

<body>
    <div id="tabs">
        <div class="callapp_head" style="width: calc(100% - 200px);">
            შემომავალი კომუნიკაცია
            <span class="comunication_button"><img alt="refresh" src="media/images/icons/comunication/comunication.png" height="30" width="30"></span>

            <span activ_status="0" style="float: right; cursor:pointer;"><img id="incallToggleDnd" alt="micr" src="media/images/icons/comunication/Phone_ON.png" height="40" width="40" value="0"></span>

            <span style="float: right; cursor: pointer;"><img id="micr" alt="micr" src="media/images/icons/4.png" height="35" width="30" value="0"></span>

            <div style="margin-right:15px; height:33px" class="callapp-op-activitie-wrapper">
                <div class="callapp-op-activitie-color-holder">
                    <div class="callapp-op-activitie-color"></div>
                </div>
                <div class="callapp-op-activitie-choose-wrapper">
                    <div class="callapp-op-activitie-choose-receiver">აირჩიეთ აქტივობა</div>
                    <ul id="callapp_op_activitie_choose" data-active="false">
                        <li data-value="0" data-color="#bdc3c7">აირჩიეთ აქტივობა</li>
                    </ul>
                </div>
                <div class="callapp-op-activitie-choose-togbutton">
                    <button id="openActivitieChoose">
                        <i class="fas fa-angle-down"></i>
                    </button>
                </div>
            </div>

            <span style="margin-right:20px;" class="callapp_refresh"><img alt="refresh" src="media/images/icons/refresh.png" height="25" width="28"></span>
            <hr class="callapp_head_hr">
        </div>
        <div class="callapp_tabs">

        </div>
        <div class="callapp_filter_show">
            <!-- <button id="callapp_show_filter_button" style="background: #fff;">ფილტრი <div id="shh" style="background: url('media/images/icons/lemons_filter.png') no-repeat;float: right; transform: rotate(0); "></div></button> -->
            <div class="callapp_filter_body" myvar="1">
                <div style="width: 100%;">
                    <table>
                        <tr>
                            <td style="padding: 5px 0 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="start_date" style="width: 84px;">
                            </td>
                            <td style="font-family: 'Courier New', Courier, monospace !important;padding: 8px 6px">-</td>
                            <td style="padding: 5px 10px 0 0">
                                <input value="" class="callapp_filter_body_span_input" type="text" id="end_date" style="width: 84px;">
                            </td>
                            <td>
                                <div class="k-content" style="margin-right: 10px;  width: 154px;">
                                    <input id="operators" style="width: 100%;" />
                                </div>
                            </td>
                            <td>
                                <div class="k-content" style="margin-right: 10px; width: 154px; ">
                                    <input id="all_calls" style="width: 100%; " />
                                </div>
                            </td>
                            <td style="padding: 0 10px;">
                                <div class="k-content">
                                    <select id="call_type" style="width: 180px !important; font-size: 12px;"></select>
                                </div>
                            </td>
                            <td>
                                <div class="k-content">
                                    <select id="call_status" style="width: 180px; font-size: 12px;"></select>
                                </div>
                            </td>
                            <td style="padding: 0 10px;">
                                <div class="k-content">
                                    <select id="call_sources" style="width: 180px !important; font-size: 12px;"></select>
                                </div>
                            </td>

                        </tr>
                    </table>
                    <div class="l_incomming_selects">

                        <!-- <select id="operator_id" style="width: 281px;">
                                <option value="0">ყველა ოპერატორი</option>
                                <?php
                                // $db->setQuery("SELECT users.`id`,
                                //                       user_info.`name`
                                //                FROM   users
                                //                JOIN   user_info ON users.id = user_info.user_id
                                //                WHERE `group_id` = 1 AND users.`actived` = 1");

                                // $res = $db->getResultArray();
                                // foreach($res[result] AS $req){
                                //     $data .= "<option value='$req[id]'>$req[name]</option>";
                                // }
                                // echo $data;
                                ?>
                            </select>
                            -->


                        <!--
                            <select id="tab_id" style="width: 281px;display: block">
                                <option value="0">ყველა ზარი</option>
                                <?php
                                // $db->setQuery("SELECT id,`name`
                                //                FROM   inc_status
                                //                WHERE  actived = 1");

                                // $res1 = $db->getResultArray();
                                // foreach($res1[result] AS $req1){
                                //     $data1 .= "<option value='$req1[id]'>$req1[name]</option>";
                                // }
                                // echo $data1;
                                ?>
                            </select>
                            -->

                        <!-- <div class="k-content">
                    <input id="all_info_id" style="width: 100%;" />
                </div> -->

                        <!--
                            <select id="user_info_id" style="width: 281px; display: block">
                            	<option value="4">ყველა</option>
                                <option value="1">ჩემი მომართვები</option>
                            </select> -->
                    </div>
                </div>
            </div>

            <div class="clear"></div>
            <div style="position:relative;">
                <button id="loadtable">ფილტრი</button>
                <button id="add_comm" style="margin-bottom:10px;">დამატება</button>
                <div id="kendo_incomming_table" />

                <!-- <button id="add_button" style="font-family: BPG arial !important;font-size: 11px;border: 1px solid #A3D0E4;background: #E6F2F8;margin-top: 2px;margin-right: 4px;margin-bottom: 10px;">დამატება</button>
            <button id="delete_button" style="font-family: BPG arial !important;font-size: 11px;border: 1px solid #A3D0E4;background: #E6F2F8;margin-top: 2px;margin-right: 4px;margin-bottom: 10px;">წაშლა</button>
            <table id="table_right_menu" style="position:absolute;right:0;">
                <tr>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0"><img alt="table" src="media/images/icons/table_w.png" height="14" width="14"></td>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0"><img alt="log" src="media/images/icons/log.png" height="14" width="14"></td>
                    <td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0"><img alt="link" src="media/images/icons/select.png" height="14" width="14"></td>
                </tr>
            </table> -->
                <!-- <table class="display" id="table_index" style="width: 100%;">
                <thead style="font-size: 10px;">
                <tr id="datatable_header">
                    <th>ID</th>
                    <th style="width: 8%;" >№</th>
                    <th style="width:  20%;">თარიღი</th>
                    <th style="width:  15%">ტელეფონი</th>
                    <th style="width:  21%">ოპერატორი</th>
                    <th style="width:  21%">კატეგორია</th>
                    <th style="width:  10%">დრო</th>
                    <th style="width:  21%">სტატუსი</th>
                </tr>
                </thead>
                <thead>
                <tr class="search_header">
                    <th class="colum_hidden">
                        <input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                    </th>
                    <th>
                        <input type="text" name="search_number" value="" class="search_init" style="width:100%;">
                    </th>
                    <th>
                        <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
                    </th>
                    <th>
                        <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;"/>
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
                    </th>
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
                    </th>
                    
                    <th>
                        <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
                    </th>
                </tr>
                </thead>
            </table> -->
            </div>
        </div>
        <style>
            /* #flesh_panel_table, #flesh_panel_table_mini{
                box-shadow: 0px 0px 7px #888888;
            }

            #flesh_panel_table td, #flesh_panel_table_mini td {
                height: 25px;
                vertical-align: middle;
                text-align: left;
                padding: 0 6px;
                background: #FFF;

            } */

                .k-multiselect-wrap {
                    max-height: 54px !important;
                    overflow: auto !important;
                    display: flex;
                }

                .tb_head td {
                    border-right: 1px solid #E6E6E6;
                }

                /* #show_flesh_panel,#show_flesh_panel_right{
                float: left;
                cursor: pointer;
            } */
            .td_center {
                text-align: center !important;
            }

            #loading1{
                z-index:999;
                top:45%;
                left:45%;
                position: absolute;
                display: none;
                padding-top: 15px;
            }
            /* The container checkbox*/
            .container {
                display: block;
                position: relative;
                padding-left: 35px;
                margin-bottom: 12px;
                cursor: pointer;
                font-size: 22px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            /* Hide the browser's default checkbox */
            .container input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
            }

            /* Create a custom checkbox checkmark*/
            .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #fff;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input~.checkmark {
                background-color: #fff;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked~.checkmark {
                background-color: #fff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked~.checkmark:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid #f80aea;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            /*------------------------------------------*/
            /* Create a custom checkbox checkmark1*/

            .checkmark1 {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #fff;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input~.checkmark1 {
                background-color: #fff;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked~.checkmark1 {
                background-color: #fff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark1:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked~.checkmark1:after {
                display: block;
            }

            #chat_typing {
                display: none;
                float: right;
                margin-right: 5px;
                position: absolute;
                bottom: 170px;
                left: 308px;
                font-size: 15px;
                /* font-weight: bold; */
            }

            /* Style the checkmark/indicator */
            .container .checkmark1:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid #f29d09;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            /*------------------------------------------*/
            /* Create a custom checkbox checkmark2*/

            .checkmark2 {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #fff;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input~.checkmark2 {
                background-color: #fff;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked~.checkmark2 {
                background-color: #fff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark2:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked~.checkmark2:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark2:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid #2196f3;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            /*------------------------------------------*/

            /* Create a custom checkbox checkmark3*/

            .checkmark3 {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #fff;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input~.checkmark3 {
                background-color: #fff;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked~.checkmark3 {
                background-color: #fff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark3:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked~.checkmark3:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark3:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid #e81d42;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            /*------------------------------------------*/

            /* Create a custom checkbox checkmark3*/

            .checkmark4 {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #fff;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input~.checkmark4 {
                background-color: #fff;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked~.checkmark4 {
                background-color: #fff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark4:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked~.checkmark4:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark4:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid #030202;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            /*------------------------------------------*/

            /* Create a custom checkbox checkmark3*/

            .checkmark5 {
                position: absolute;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #fff;
            }

            /* On mouse-over, add a grey background color */
            .container:hover input~.checkmark5 {
                background-color: #fff;
            }

            /* When the checkbox is checked, add a blue background */
            .container input:checked~.checkmark5 {
                background-color: #fff;
            }

            /* Create the checkmark/indicator (hidden when not checked) */
            .checkmark5:after {
                content: "";
                position: absolute;
                display: none;
            }

            /* Show the checkmark when checked */
            .container input:checked~.checkmark5:after {
                display: block;
            }

            /* Style the checkmark/indicator */
            .container .checkmark5:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid #009895;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            #incoming_chat_tabs .ui-dialog .ui-widget-header {
                border: none;
                background: #fff;
            }

            #incoming_chat_tabs .ui-tabs .ui-tabs-nav {
                margin: 0;
                padding: .0em .0em 0;
            }

            /*------------------------------------------*/

            #kendo_incomming_table > th[data-field="id"]{
                font-size:10vw;
            }
        </style>
        <!-- <div id="flesh_panel">
            <div class="callapp_head" style="text-align: right;"><img id="show_flesh_panel" title="პანელის გადიდება" alt="arrow" src="media/images/icons/arrow_left.png" height="18" width="18">ქოლ-ცენტრი<hr class="callapp_head_hr"></div>
            <table style="display:none;" id="comucation_status">
                <tr>
                    <td colspan="3"><div style="padding: 12px 0px 0px 10px; font-size:16px" class="callapp_head" style="text-align: left;">კომუნიკაციის არხები</div></td>
                </tr>
                <tr style="height: 6px"></tr>
                <tr class="comunicationTr">
                    <td style="width: 60px;"><span class="imagespan"><img id="VebChatImg" src="media/images/icons/comunication/Chat_OFF.png" height="35" width="35"></span></td>
                    <td style="padding: 10px 0px 0px 0px; width: 100px;"><span style="color: #f80aea;" class="comunication_name">ვებ-ჩათი</span></td>
                    <td style="padding: 5px 0 0 0px;"><label class="container">
                            <input class="comunication_checkbox" type="checkbox" id="web_chat_checkbox" value="1">
                            <span style="border: 1px #f80aea solid;" class="checkmark"></span>
                        </label></td>
                </tr> -->
            <!--    	<tr class="comunicationTr">-->
            <!--    		<td><span class="imagespan"><img id="siteImg" src="media/images/icons/comunication/OFF-my.png" height="35" width="35"></span></td>-->
            <!--    		<td style="padding: 10px 0px 0px 0px;"><span style="color:#f29d09;" class="comunication_name">my მესენენჯერი</span></td>-->
            <!--    		<td style="padding: 5px 0 0 0px;"><label class="container">-->
            <!--              <input class="comunication_checkbox" type="checkbox" id="site_chat_checkbox" value="1">-->
            <!--              <span style="border: 1px #f29d09 solid;" class="checkmark1"></span>-->
            <!--            </label></td>-->
            <!--    	</tr>-->
            <!-- <tr class="comunicationTr">
                    <td><span class="imagespan"><img id="MessengerImg" src="media/images/icons/comunication/Messenger_OFF.png" height="35" width="35"></span></td>
                    <td style="padding: 10px 0px 0px 0px;"><span style="color: #2196f3;" class="comunication_name">მესენჯერი</span></td>
                    <td style="padding: 5px 0 0 0px;"><label class="container">
                            <input class="comunication_checkbox" type="checkbox" id="messanger_checkbox" value="1">
                            <span style="border: 1px #2196f3 solid;" class="checkmark2"></span>
                        </label></td>
                </tr>
                <tr class="comunicationTr">
                    <td><span class="imagespan"><img id="MailImg" src="media/images/icons/comunication/E-MAIL_OFF.png" height="35" width="35"></span></td>
                    <td style="padding: 10px 0px 0px 0px;"><span style="color: #e81d42;" class="comunication_name">ელ-ფოსტა</span></td>
                    <td style="padding: 5px 0 0 0px;"><label class="container">
                            <input class="comunication_checkbox" type="checkbox" id="mail_chat_checkbox" value="1">
                            <span style="border: 1px #e81d42 solid;" class="checkmark3"></span>
                        </label></td>
                </tr> -->

            <!-- <tr class="comunicationTr" style="height: 40px; display:none;">
                <td><span class="imagespan"><img id="VideoImg" src="media/images/icons/comunication/Video_OFF.png" height="35" width="35"></span></td>
                <td style="padding: 10px 0px 0px 0px;"><span style="color: #009895;" class="comunication_name">ვიდეო ზარი</span></td>
                <td style="padding: 5px 0 0 0px;"><label class="container">
                <input class="comunication_checkbox" type="checkbox" id="video_call_checkbox" value="1">
                <span style="border: 1px #009895 solid;" class="checkmark5"></span>
                </label></td>
            </tr> 
            </table>
            <table id="flesh_panel_table" style="margin-bottom: 0px; width: 100%;">

                <thead>
                <tr>
                    <td colspan="4">
                        <table>
                            <tr>
                                <td id="show_station" class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;color:#2681DC;text-decoration: underline;"><img src="media/images/icons/comunication/phone.png" height="27" width="27"></td>
                                <td id="show_chat" 	  class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Chat.png" height="30" width="30"></td>
                                <td id="show_fbm"     class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Messenger.png" height="30" width="30"></td>
                                <td id="show_mail"    class="flash_menu_link" style="cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important; "><img src="media/images/icons/comunication/E-MAIL.png" height="30" width="30"></td>
                                <td id="show_video"    class="flash_menu_link" style="display:none; cursor:pointer;font-size:12px;font-weight:bold;font-family: pvn !important;"><img src="media/images/icons/comunication/Video.png" height="30" width="30"></td> 
                            </tr>
                        </table>
                    </td>


                </tr>
                <tr class="tb_head" style="border: 1px solid #E6E6E6;">
                    <td style="width:85px" id="mini_pirveli">შიდა ნომერი</td>
                    <td colspan = "2"  style="width: 120px;" id="mini_meore">მომ. ავტორი</td>
                    <td style="width: 90px;" id="mini_mesame">სტატუსი</td>
                </tr>
                </thead>
                <tbody id="mini_call_ext">
                </tbody>
                <thead>
                <tr>
                    <td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;"></td>
                </tr>
                <tr>
                    <td colspan="4" style="border-left: 1px solid #E6E6E6;border-right: 1px solid #E6E6E6;">რიგი</td>
                </tr>
                <tr class="tb_head" style="border: 1px solid #E6E6E6;">
                    <td id="m_q_pirveli">პოზიცია</td>
                    <td colspan="3" id="m_q_mesame">ნომერი</td>
                </tr>
                </thead>
                <tbody id="mini_call_queue">
                </tbody>

            </table>
        </div> -->
        <script id="noDataTemplate" type="text/x-kendo-tmpl">
            # var value = instance.input.val(); #
            # var id = instance.element[0].id; #
            <div>
                No data found. Do you want to add new item - '#: value #' ?
            </div>
            <br />
            <button class="k-button" onclick="addNew('#: id #', '#: value #')" ontouchend="addNew('#: id #', '#: value #')">ადრესატის დამატება</button>
        </script>
        <div id="loading" style="z-index: 999999;padding: 160px 0;height: 100%;width: 100%;position: fixed;background: #f9f9f9;top: 0;">
            <div class="loading-circle-1">
            </div>
            <div class="loading-circle-2">
            </div>
        </div>
        <input id="check_state" type="hidden" value="1" />
        <input id="whoami" type="hidden" value="1" />
        <!-- jQuery Dialog -->
        <div id="loading_search_my_client_history" style="display:none; z-index: 999999;padding: 155px 0;height: 100%;width: 75%;position: fixed;top: 0;">
            <div class="loading-circle-1">
            </div>
            <div class="loading-circle-2">
            </div>
        </div>
        <div id="get_processing-dialog" class="form-dialog" title="დამუშავება"></div>

        <div id="dialog_active_chat" aria-data="false"></div>
        <div id="dep_additional" class="form-dialog" title="დამატებითი ველები"></div>
        <div id="chat-docs-dialog-form" class="form-dialog" title="დოკუმენტაცია"></div>
        <div id="new_mail_addr" class="form-dialog" title="ადრესატი">

        </div>
        <div id="campaign-select-form" class="form-dialog" title="კამპანიის არჩევა">
            <!-- aJax -->
        </div>
        <div id="kendo_docs" class="form-dialog" title="დოკუმენტაცია"></div>
        <div id="kendo_contact4" class="form-dialog" title="კონტაქტები"></div>
        <div id="mail_forwarding" class="form-dialog" title="Mail Forwarding"></div>
        <div id="contract_dialog" class="form-dialog" title="კონტრაქტების სია"></div>
        <div id="loading1">
            <p><img src="media/images/loader.gif" /></p>
        </div>
</body>