<?php

namespace IO;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use IO\Request;

use IO\Routes\flashpanel\phoneTaken;


class Base extends Request implements MessageComponentInterface {
    
    protected $clients;
    protected $response;
    protected $request;
    protected $msg;
    protected $class;
    protected $method;

    protected $loop;

    public $phoneTaken;

    public function __construct($loop) {

        $this->clients = new \SplObjectStorage;

        $that = $this;
        $this->loop = $loop;

        $this->loop->addPeriodicTimer(5, function() use ($that) {

            $phoneTaken = new phoneTaken();
            $phoneTaken->RUN();
            $this->phoneTaken  = $phoneTaken->GET();

        });

    }

    public function onOpen(ConnectionInterface $conn) {

        $this->clients->attach($conn);
        
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        
        $this->msg = json_decode($msg);

        if(isset($this->msg->wsRoute)){
            $wsRoute = $this->msg->wsRoute;
            $from->send(json_encode($this->$wsRoute));
        }else{
            $from->send(json_encode($this->response));
        }
        
        // $this->class    = new Request($msg);
        // $this->request  = $this->class->sendRequest();
        // $this->response = $this->class->getResponse();


        // var_dump($from);
        // foreach ($this->clients as $client) {
        //     $client->send(json_encode($this->response));
        // }

    }

    public function onClose(ConnectionInterface $conn) {
    
        $this->clients->detach($conn);

    }

    public function onError(ConnectionInterface $conn, \Exception $e) {

        parent::sLog($e->getMessage(), "Base");

        $conn->close();
    }
    
}