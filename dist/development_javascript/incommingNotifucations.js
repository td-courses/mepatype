document.querySelector('.notification_bell').classList.add('notification-element__with_no_notification');


const getIncomingNotification = () => {
    let lastPost = document.querySelectorAll('.main__posts')
    
    // if (lastPost.length == 0) {

    if (lastPost.length != 0) {
        let lastPostId = lastPost[0].dataset.postid


        let parser = new DOMParser();

        let actAndLastPostId = {
            act: "notification",
            lastPostId: lastPostId
        }

        var AjaxURL = "server-side/call/action_action.php";
        $.ajax({
            url: AjaxURL,
            data: actAndLastPostId,
            dataType: 'json',
            success: function (result) {
                if (result) {
                    let notificationsTemplate;

                    const profileNotifications = document.querySelector('.profile__notifications')
                    const notificationsContainer = document.querySelector('.profile__notifications__container')

                    const allPosts = document.querySelectorAll('.main__posts');
                    const allPostsId = [];

                    allPosts.forEach(post => {
                        allPostsId.push(post.dataset.postid)
                    });

                    let notificationsArray = result.notifications;
                    notificationsContainer.innerHTML = ""


                    let notificationBellElement = document.querySelector('.notification_bell');

                    console.log(notificationsArray.length);
                    

                    if (notificationsArray.length <= 0) {
                        notificationBellElement.classList.add('notification-element__with_no_notification');
                    } else {
                        notificationBellElement.classList.remove('notification-element__with_no_notification');
                        notificationsArray.forEach(notification => {


                            if (allPostsId.includes(notification.id.toString())) {
                                notificationsTemplate = ""
                            } else {
                                notificationBellElement.dataset.notificationamount = notificationsArray.length - 1;
                                notificationsTemplate = parser.parseFromString(`
                                                                        <li class="links notification" data-notificationpostid=${notification.id} onclick="getAllPosts()" onmouseup="setNotificationsToZero()">
                                                                            <div class="notification__image" style="background: center / cover no-repeat url('media/uploads/file/${notification.poster_image_name}')"></div>
                                                                            <p class="notification__description"> <span> ${notification.poster_name} </span> ${notification.post_description}</p>
                                                                            <p class="notification__time_posted">${notification.post_time}</p>
                                                                        </li>`
                                    , "text/html")

                                notificationsContainer.append(notificationsTemplate.body.firstChild)


                            }

                        })
                    }
                }
            }
        })
    }


    // }
}

setInterval(() => {
    getIncomingNotification()
}, 4000);




const setNotificationsToZero = () => {
    notificationBellElement.dataset.notificationamount = 0;
    notificationBellElement.classList.add('notification-element__with_no_notification');
}
