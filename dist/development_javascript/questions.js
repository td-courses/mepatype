const addMoreAnswers = document.querySelector('.addMoreAnswers');
const answersContainer = document.querySelector('.answers ol');

let parser = new DOMParser();
let addedQuestion;

if (addMoreAnswers) {
    addMoreAnswers.addEventListener('click', _ =>{
        addedQuestion = parser.parseFromString(`
                    <li>
                        <input type="text">
                        <div class="delete_answer" onclick="deleteAnswer(event)">✖</div>
                    </li>`,
            "text/html")
        
        answersContainer.append(addedQuestion.body.firstChild)
    })
    
}






const deleteAnswer = (e) =>{
    let liTag = e.target.parentNode; 
    liTag.remove(); 
}
 