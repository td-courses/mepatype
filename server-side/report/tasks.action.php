﻿<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../includes/classes/core.php');
include('../../includes/classes/log.class.php');
$log 		= new log();
$action 	= $_REQUEST['act'];
$error		= '';
$data		= '';
$incom_id 	= $_REQUEST['id'];




switch ($action) {
	case 'get_add_page':
		$number		= $_REQUEST['number'];
		$page		= GetPage($res='', $number);
		$data		= array('page'	=> $page);

		break;
	
	case 'get_edit_page':
		
		$page		= GetPage(Getincomming($incom_id),0,$incom_id);
		

		$data		= array('page'	=> $page);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/


function Getcall_status($call_status_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `call_status`
						FROM 	`status`
						WHERE 	actived=1");
	

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		
		if($res['id'] == $call_status_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['call_status'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['call_status'] . '</option>';
		}
	}
	return $data;
}

function Getpay_type($pay_type_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`pay_type`
					WHERE 	actived=1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $pay_type_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getsource($object_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`source`
					WHERE 	actived=1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $object_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
function Get_bank($bank_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`bank`
					WHERE 	actived=1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $bank_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
	
function Getbank_object($bank_object_id)
{  
	$data = '';
	$req = mysql_query("SELECT  id,
						     	`name`
						FROM 	bank_object
						WHERE 	bank_object.bank_id=$bank_object_id && actived =1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $bank_object_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getbank_object_edit($bank_object_id)
{

	$data = '';
	$req = mysql_query("SELECT  id,
								`name`
						FROM 	bank_object
						WHERE 	bank_object.id=$bank_object_id && actived =1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $bank_object_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
	
function Getcard_type($card_type_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`card_type`
					WHERE 	actived=1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $card_type_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getcard_type1($card_type1_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`card_type`
					WHERE 	actived=1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $card_type1_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getpay_aparat($pay_aparat_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`pay_aparat`
					WHERE 	actived=1");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $pay_aparat_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getobject($object_id)
{
	$data = '';
	$req = mysql_query("SELECT 	`id`, `name`
					FROM 	`object`
					WHERE 	actived=1");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $object_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getcategory($category_id)

{ 			

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `category`
						WHERE actived=1 && parent_id=0 ");


	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $category_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getcategory1($category_id)
{

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `category`
						WHERE actived=1 && parent_id=$category_id");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $category_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
	
}

function Getcategory1_edit($category_id)
{

	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `category`
						WHERE actived=1 && id=$category_id");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $category_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;

}

function Getcall_type($call_type_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
					FROM `call_type`
					WHERE actived=1");
	
	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $call_type_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}


function Getdepartment($task_department_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
					    FROM `department`
					    WHERE actived=1 ");
	

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $task_department_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getpriority($priority_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `priority`
						WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $priority_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Gettemplate($template_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
						FROM `template`
						WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $template_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Gettask_type($task_type_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
					    FROM `task_type`
					    WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $task_type_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getpersons($persons_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
							FROM `crystal_users`
							WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $persons_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getpersons1($persons_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
							FROM `crystal_users`
							WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $persons_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}
function Get_control($persons_id)
{
	$data = '';
	$req = mysql_query("SELECT `id`, `name`
							FROM `crystal_users`
							WHERE actived=1 ");

	$data .= '<option value="0" selected="selected">----</option>';
	while( $res = mysql_fetch_assoc($req)){
		if($res['id'] == $persons_id){
			$data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
		} else {
			$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
		}
	}

	return $data;
}

function Getincomming($incom_id){
	
	
$res = mysql_fetch_assoc(mysql_query("	SELECT    	task.id AS id,
													task.user_id,
													crystal_users.name as reqvester,
													incomming_call.uniqueid as inc_uniqueid,
													task.uniqueid as task_uniqueid,
													if(ISNULL(task.incomming_call_id), DATE_FORMAT(task.`date`,'%d/%m/%y %H:%i:%s'), DATE_FORMAT(incomming_call.`date`,'%d/%m/%y %H:%i:%s')) AS call_date,
													DATE_FORMAT(incomming_call.`date`,'%Y-%m-%d %H') AS record_date,
													if(ISNULL(task.incomming_call_id), task.`phone`, incomming_call.`phone` ) AS phone,
													if(ISNULL(task.incomming_call_id), task.`call_type_id`, incomming_call.`call_type_id` )  AS call_type_id,
													if(ISNULL(task.incomming_call_id), task.`task_source_id`, incomming_call.`source_id` ) AS `source`,
													if(ISNULL(task.incomming_call_id), task.`category_id`, incomming_call.`call_subcategory_id` ) AS category_id,
													task.status AS call_status_id,
													if(ISNULL(task.incomming_call_id), task.`subcategory_id`, incomming_call.`call_category_id` ) AS category_parent_id,
													if(ISNULL(task.incomming_call_id), task.`problem_date`, incomming_call.`problem_date` ) AS problem_date,
													if(ISNULL(task.incomming_call_id), task.`pay_type_id`, incomming_call.`pay_type_id` ) AS pay_type_id,
													if(ISNULL(task.incomming_call_id), task.`bank_id`, incomming_call.`bank_id` ) AS bank_id,
													if(ISNULL(task.incomming_call_id), task.`bank_object_id`, incomming_call.`bank_object_id` ) AS bank_object_id,
													if(ISNULL(task.incomming_call_id), task.`card_type_id`, incomming_call.`card_type_id` ) AS card_type_id,
													if(ISNULL(task.incomming_call_id), task.`pay_aparat_id`, incomming_call.`pay_aparat_id` ) AS pay_aparat_id,
													if(ISNULL(task.incomming_call_id), task.`object_id`, incomming_call.`object_id` ) AS object_id,
													if(ISNULL(task.incomming_call_id), task.`comment`, incomming_call.`call_content` ) AS `comment`,
													task.control_user_id,
													incomming_call.pid AS personal_id,
													incomming_call.pin AS personal_pin,
												  	task.task_type_id AS task_type_id,
													CONCAT(DATEDIFF(task.`fact_end_date`, task.`planned_end_date`),' ','დღე და',' ',TIMEDIFF(DATE_FORMAT(task.`fact_end_date`,'%H:%i:%s'),DATE_FORMAT(task.`planned_end_date`,'%H:%i:%s')),' ', 'სთ') as duration,
													task.responsible_user_id AS persons_id,
													task.responsible_user_id1 AS coller,
													task.priority_id AS priority_id,
													task.department_id AS task_department_id,
													DATE_FORMAT(task.`planned_end_date`,'%d/%m/%y %H:%i:%s') as planned_end_date,
													DATE_FORMAT(task.fact_end_date,'%d/%m/%y %H:%i:%s') as fact_end_date,
													task.`problem_comment` AS `problem_comment`
																						
										FROM 	   	`task`
										LEFT JOIN incomming_call ON task.incomming_call_id=incomming_call.id
										LEFT JOIN crystal_users ON crystal_users.id=task.user_id
										WHERE      	task.id =$incom_id
			" ));


return $res;
	
}

function GetPage($res='', $number, $incom_id)
{	
	//print_r($res);
	
	$num = 0;
	if($res[phone]==""){
		$num=$number;
	}else{ 
		$num=$res[phone]; 
	}
	
	$tanxa = 'class="hidden dialog-form-table"';
	$disabled = '';
	$hidden_class = '';
	
	if($_REQUEST['id'] == ''){
		
		$hidden_class = 'class="hidden"';
	}else{
		//echo $_REQUEST['id'];
		if($res[category_id] == 407){
			//echo $res[category_id];
			$tanxa = 'class="dialog-form-table"';
		}else{
			$tanxa = 'class="hidden dialog-form-table"';
			
		}
		$disabled = 'disabled="disabled"';
	}
	
	$increm = mysql_query("	SELECT  `name`,
	 								`rand_name`,
									`id`
							FROM 	`file`
							WHERE   `task_id` = $res[id]
							  ");
	
	
	$data  .= '
	<!-- jQuery Dialog -->
    <div id="add-edit-goods-form" title="საქონელი">
    	<!-- aJax -->
	</div>
	<div id="dialog-form">
			<div style="float: left; width: 500px;">	
				<fieldset >
			    	<legend>ძირითადი ინფორმაცია</legend>
	                <table width="100%" class="dialog-form-table">
    					<tr>
                    	    <td style="width: 110px;"><label for="req_num">დამფორმირებელი: </label></td>
                    	    <td><label for="req_num">' .$res['reqvester'].'</label></td>
    					</tr>
					</table>
			    	<table width="100%" class="dialog-form-table">
						<tr>
							<td style="width: 215px;"><label for="req_num">დავალების №</label></td>
							<td style="width: 215px !important;"><label for="req_data">თარიღი</label></td>
							<td style="width: 215px;"><label for="req_data">ტელეფონი</label></td>
						</tr>						
						<tr>
							<td style="width: 215px;">
								<input  style="width: 180px; type="text" id="id" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField user_id\'" value="' . $res['id']. '" disabled="disabled" />
							</td>
							<td style="width: 215px;">
								<input style="width: 180px; type="text" id="c_date" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField date\'" value="' .  $res['call_date']. '" disabled="disabled" />
							</td>
							<td style="width: 215px;">
								<input style="width: 180px; type="text" id="phone" class="idle" onblur="this.className=\'idle\'" value="' .  $res['phone']. '" />
							</td>
							
						</tr>
					</table>';
	
									
		$data .= '<table id="additiona" class="dialog-form-table" width="100%">				
									
						<tr>
							<td style="width: 215px;"><label for="d_number">ზარის ტიპი</label></td>
							<td style="width: 215px;"><label for="d_number">კატეგორია</label></td>
							<td style="width: 215px;"><label for="d_number">ქვე-კატეგოტია</label></td>
							
						</tr>
						<tr>
							<td style="width: 215px;"><select style="width: 186px;" id="call_type_id" class="idls object"" >'. Getcall_type($res['call_type_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="category_parent_id" class="idls object"" >'. Getcategory($res['category_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="category_id" class="idls object"" >'. Getcategory1_edit($res['category_parent_id']).'</select></td>
							
						</tr>
						</table>
						<table id="additiona3" '.$tanxa.' width="100%">
						<tr id="aaaa">
							<td style="width: 215px;"><label id="pey_label1" for="d_number">ტრანზაქციის ფორმა</label></td>
							<td style="width: 215px;"><label for="d_number" id="bank_label">მომსახურე ბანკი</label></td>
							<td style="width: 215px;"><label id="pey_label"  for="d_number" >აპარატის ტიპი</label></td>
							
						</tr>
						<tr id="c">
							<td style="width: 215px;"><select style="width: 186px;" id="pay_type_id" class="idls object">'. Getpay_type($res['pay_type_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="bank_id" class="idls object">'. Get_bank($res['bank_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="pay_aparat_id" class="idls object">'. Getpay_aparat($res['pay_aparat_id']).'</select></td>

						</tr>
						<tr id="bbbb">
							<td style="width: 215px;"><label for="d_number" id="card_label">ბარათის ტიპი</label></td>
							<td style="width: 215px;"></td>
							<td style="width: 215px;"></td>
							
						</tr>
						<tr id="d">
							<td style="width: 215px;"><select style="width: 186px;" id="card_type_id" class="idls object">'. Getcard_type($res['card_type_id']).'</select></td>
							<td style="width: 215px;"></td>
							<td style="width: 215px;"></td>
						
						</tr>
					</table>
					<table id="additiona2" class="dialog-form-table" width="100%">	
						<tr>
							<td style="width: 215px;"><label for="req_num">პრობლემის თარიღი</label></td>
							<td style="width: 215px;"><label for="d_number">სტატუსი</label></td>
							<td style="width: 215px;"><label for="d_number">ობიექტი</label></td>			
						</tr>
						<tr>	
							<td style="width: 215px;">
								<input style="width: 180px;" type="text" id="problem_date" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res[problem_date] . '""  />
							</td>
							<td style="width: 215px;"><select style="width: 186px;" id="call_status_id" class="idls object">'. Getcall_status($res['call_status_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="object_id" class="idls object">'. Getobject($res['object_id']).'</select></td>	
								
						</tr>
						<tr>
							<td style="width: 215px;"><label for="task_type_id">დავალების ტიპი</label></td>
							<td style="width: 215px;"><label for="task_department_id">განყოფილება</label></td>
							<td style="width: 215px;"><label for="persons_id">პასუხისმგებელი პირი</label></td>
						</tr>
						<tr>
							<td style="width: 215px;"><select style="width: 186px;" id="task_type_id" class="idls object">'.Gettask_type($res['task_type_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="task_department_id" class="idls object">'. Getdepartment($res['task_department_id']).'</select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="persons_id" class="idls object">'.Getpersons($res['persons_id']).'</select></td>
						</tr>
						<tr>
							<td style="width: 215px;"><label for="task_type_id">ინფორმაციის წყარო</label></td>
							<td style="width: 215px;"><label id="pr" class="hidden" for="d_number">პრიორიტეტები</label></td>
							<td style="width: 215px;"><label for="d_number">მაკონტროლებელი</label></td>
						</tr>
						<tr>
							<td style="width: 215px;"><select style="width: 186px;" id="source" class="idls object">'.Getsource($res['source']).'</select></td>
							<td style="width: 215px;"><label class="hidden" id="priority"><select id="priority_id" class="idls" >'.Getpriority($res['priority_id']).'</label></select></td>
							<td style="width: 215px;"><select style="width: 186px;" id="control_id" class="idls object">'.Get_control($res['control_user_id']).'</select></td>
						</tr>
						<tr>
							<td style="width: 215px;"><label for="task_type_id">ზარის განმხორციელებელი</label></td>
							<td style="width: 215px;"><label for="task_type_id">სხვა ნომერი</label></td>
							<td style="width: 215px;"></td>
						</tr>
						<tr>
							<td style="width: 215px;"><select style="width: 186px;" id="coller" class="idls object"disabled="disabled">'.Getpersons1($res['coller']).'</select></td>
							<td style="width: 215px;">
								<input style="width: 180px; type="text" id="task_phone" class="idle" onblur="this.className=\'idle\'" value="' .  $res['task_phone']. '"disabled="disabled" />
							</td>
							<td style="width: 215px;"></td>
									
						</tr>
						</table>

									
						<table id="additiona1" class="hidden dialog-form-table" width="100%">
						<tr>
							<td style="width: 215px;"><label for="req_num">შესრ.პერ.დასაწყისი</label></td>
							<td style="width: 215px !important;"><label for="req_data">შესრ.პერ.დასასრული</label></td>
							<td style="width: 215px;"><label for="req_phone">შესრულების დრო</label></td>
						</tr>
						<tr>
							<td style="width: 215px;">
								<input type="text" id="planned_date" style="width: 180px;" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' .  $res['planned_end_date']. '" />
							</td>
							<td style="width: 215px;">
								<input type="text" id="fact_end_date" style="width: 180px;" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' .  $res['fact_end_date']. '"/>
							</td>
							<td style="width: 215px;">
								<input type="text" id="call_duration" style="width: 180px;" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['duration'] . '" />
							</td>
						</tr>
					</table>	
								<br>	
									
						<table>			
						<tr>
							<td style="width: 150px;"><label for="content">საუბრის შინაარსი</label></td>
							<td style="width: 150px;"><label for="content"></label></td>
							<td style="width: 150px;"><label for="content"></label></td>
						</tr>
						<tr>
							<td colspan="6">
								<textarea  '.$disabled.' style="width: 641px; resize: none;" id="comment" class="idle" name="content" cols="300" rows="4">' . $res['comment'] . '</textarea>
							</td>
						</tr>
						<tr>
							<td '.$hidden_class.' style="width: 215px;"><label for="content">პრობლემის გადაწყვეტა</label></td>
						</tr>
						<tr>
							
							<td colspan="5">	
								<textarea '.$hidden_class.' style="width: 641px; resize: none;" id="problem_comment" class="idle" name="call_content" cols="300" rows="4">' . $res['problem_comment'] . '</textarea>
							</td>
						</tr>	
					</table>';		
									
		////////////////////-----------------------------------------------------------------------------------
												
		$data  .= '
				</fieldset >
		   
				
			</div>
			<div>
				  </fieldset>
			</div>
			<div style="float: right;  width: 355px;">
				 <fieldset>
					<legend>მომართვის ავტორი</legend>
					<table style="height: 134px;">
						<tr>
							<td style="width: 215px;">PIN კოდი</td>
							<td style="width: 215px;"</td>
						</tr>
						<tr>
							<td style="width: 215px;"><input type="text" id="personal_pin" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['personal_pin']  . '" /></td>
							<td style="width: 215px;" class="hidden friend">
						</td>
						</tr>
						<tr>
							<td style="width: 215px;">პირადი ნომერი</td>
							<td style="width: 215px;"></td>
						</tr>
						<tr>
							<td style="width: 215px;">
								<input type="text" id="personal_id" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'" value="' . $res['personal_id'] . '" />
							</td>
				</table>
				</fieldset>
				<div id="additional_info">';

	$data .= '</div>';
	
	$data .= GetRecordingsSection1($res);
	$data .= GetRecordingsSection($res);
    $data .= '<fieldset>
	         	<legend>დამატებითი ინფორმაცია</legend> 
	         	<table id="additiona1" class="hidden dialog-form-table" style="float: left; border: 1px solid #85b1de; width: 180px; text-align: center;">
		        
			    
				<table id="file_div" style="float: right; border: 1px solid #85b1de; width: 100%; text-align: center;">';
					while($increm_row = mysql_fetch_assoc($increm))	{	
						$data .=' 
						        <tr style="border-bottom: 1px solid #85b1de;">
						          <td style="width:100%; display:block;word-wrap:break-word;">'.$increm_row[name].'</td>													 
						          <td style="width:30px;"><button type="button" value="media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
						          <td style="width:30px;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
						        </tr>';
					}
			         
		 $data .= '
		 		</table>
		        <table style="float: right; border: 1px solid #85b1de; width: 100%; text-align: center;">
					<tr>
						<td>
							<div class="file-uploader">
								<input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
								<button style="width:100%" id="choose_button">აირჩიეთ ფაილი</button>
								<input id="hidden_inc" type="text" value="'. increment('task') .'" style="display: none;">
							</div>
						</td>
					</tr>
				</table>
		        </fieldset>
							
			</div>
		<!-- ID -->
		<input type="hidden" id="req_id" value="' . $res['id'] . '" />
	</div>';
	return $data;
}
function GetRecordingsSection($res)
{
	if ($res[phone]==''){
		//$data .= '<td colspan="2" style="height: 20px; text-align: center;">ჩანაწერები ვერ მოიძებნა</td>';
	}else{
	$req = mysql_query("SELECT  	DATE_FORMAT(asterisk_incomming.call_datetime,'%d/%m/%y %H:%i:%s') AS `time`,
									asterisk_incomming.file_name AS `userfield`
						FROM 		asterisk_incomming
						WHERE		asterisk_incomming.uniqueid ='$res[inc_uniqueid]'
						");
	}
	$data .= '
        <fieldset>
            <legend>შემომავალი ზარი</legend>

            <table style="width: 100%; border: solid 1px #85b1de; margin:auto;">
                <tr style="border-bottom: solid 1px #85b1de; height: 20px;">
                    <th style="padding-left: 10px;">დრო</th>
                    <th style="border: solid 1px #85b1de; padding-left: 10px;">ჩანაწერი</th>
                </tr>';
	if (mysql_num_rows($req) == 0){
		$data .= '<td colspan="2" style="height: 20px; text-align: center;">ჩანაწერები ვერ მოიძებნა</td>';
	}
	
	while( $res2 = mysql_fetch_assoc($req)){
		$src = $res2['userfield'];
		$data .= '
                <tr style="border-bottom: solid 1px #85b1de; height: 20px;">
                    <td>' . $res2['time'] . '</td>
                    <td><button style="width: 100%;" class="download" str="' . $src . '">მოსმენა</button></td>
                </tr>';
	}

	$data .= '
            </table>
        </fieldset>';
	
	return $data;
}
function GetRecordingsSection1($res)
{
	if ($res[phone]==''){
		//$data .= '<td colspan="2" style="height: 20px; text-align: center;">ჩანაწერები ვერ მოიძებნა</td>';
	}else{
		$req = mysql_query("SELECT  DATE_FORMAT(asterisk_outgoing.call_datetime,'%d/%m/%y %H:%i:%s') AS `time`,
									asterisk_outgoing.file_name AS `userfield`
							FROM 	asterisk_outgoing
							WHERE 	asterisk_outgoing.uniqueid='$res[task_uniqueid]'");
	}
	$data .= '
        <fieldset>
            <legend>გამავალი ზარი</legend>

            <table style="width: 100%; border: solid 1px #85b1de; margin:auto;">
                <tr style="border-bottom: solid 1px #85b1de; height: 20px;">
                    <th style="padding-left: 10px;">დრო</th>
                    <th style="border: solid 1px #85b1de; padding-left: 10px;">ჩანაწერი</th>
                </tr>';
	if (mysql_num_rows($req) == 0){
		$data .= '<td colspan="2" style="height: 20px; text-align: center;">ჩანაწერები ვერ მოიძებნა</td>';
	}

	while( $res2 = mysql_fetch_assoc($req)){
		$src = $res2['userfield'];
		$data .= '
                <tr style="border-bottom: solid 1px #85b1de; height: 20px;">
                    <td>' . $res2['time'] . '</td>
                    <td><button class="download1" str="' . $src . '">მოსმენა</button></td>
                </tr>';
	}

	$data .= '
            </table>
        </fieldset>';

	return $data;
}
function increment($table){
	
	$result   		= mysql_query("SHOW TABLE STATUS LIKE '$table'");
	$row   			= mysql_fetch_array($result);
	$increment   	= $row['Auto_increment'];
	$next_increment = $increment+1;
	mysql_query("ALTER TABLE '$table' AUTO_INCREMENT=$next_increment");

	return $increment;
}
?>
