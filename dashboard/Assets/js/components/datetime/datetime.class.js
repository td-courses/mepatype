
import Helper from "../../helpers/helper.class.js";

export default class Datetime extends Helper {

    constructor(element){
        super();

        this.state = {
            dom: [],
            element: element
        }

    }

    init = (type) =>{



        switch(type){
            case "date":
                $(this.state.element).kendoDatePicker({
                    componentType: "modern",
                    format: "yyyy-MM-dd",
                    value: new Date()
                });
            break;
            case "time":
                $(this.state.element).kendoTimePicker({
                    componentType: "modern",
                    timeFormat: "HH:mm:ss",
                    value: new Date()
                });
            break;
            default:
                $(this.state.element).kendoDateTimePicker({
                    componentType: "modern",
                    format: "yyyy-MM-dd hh:mm:ss",
                    timeFormat: "HH:mm:ss",
                    value: new Date()
                });
        }
        

    }


}