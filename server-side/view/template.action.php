<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();

$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$source_id		= $_REQUEST['id'];
		$page		= GetPage(Getsource($source_id));
		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
			
		$db->setQuery(" SELECT 	source.id,
								source.`name`
					    FROM 	source
					    WHERE 	source.actived=1");
		
		
		
		$data = $db->getList($count,$hidden,1);

		break;
	case 'save_source':
		$source_id 		= $_REQUEST['id'];
		$source_name    = $_REQUEST['name'];
		
        if($source_name != ''){
			if(!ChecksourceExist($source_name, $source_id)){
				if ($source_id == '') {
					Addsource($source_name);
				}else {
					Savesource($source_id, $source_name);
				}
            }else{
				$error = '"' . $source_name . '" უკვე არის სიაში!';
            }
		}

		break;
	case 'disable':
		$source_id	= $_REQUEST['id'];
		Disablesource($source_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addsource($source_name){
    global $db;
	$db->setQuery("INSERT INTO 	`source`
							   (`name`, `actived`)
					     VALUES 		
							   ('$source_name', '1')");
	$db->execQuery();
}

function Savesource($source_id, $source_name){
    global $db;
	$user_id	= $_SESSION['USERID'];
	
	$db->setQuery("	UPDATE `source`
					   SET `name` = '$source_name'
					WHERE  `id`   = $source_id");
	
	$db->execQuery();
}

function Disablesource($source_id){
    global $db;
    
    $db->setQuery("	UPDATE `source`
					   SET `actived` = 0
					WHERE  `id`      = $source_id");
    
    $db->execQuery();
}

function ChecksourceExist($source_name){
    global $db;
	$db->setQuery("SELECT `id`
				   FROM   `source`
				   WHERE  `name` = '$source_name' && `actived` = 1");
	
	$res = $db->getResultArray();
	if($res[result][0]['id'] != ''){
		return true;
	}
	return false;
}


function Getsource($source_id){
    global $db;
    $db->setQuery("SELECT  `id`,
						   `name`
				   FROM    `source`
				   WHERE   `id` = $source_id" );

    $res = $db->getResultArray();
	return $res[result][0];
}

function GetPage($res = ''){
	$data = '
        	<div id="dialog-form">
        	    <fieldset>
        	    	<legend>ძირითადი ინფორმაცია</legend>
        
        	    	<table class="dialog-form-table">
        				<tr>
        					<td><label for="CallType">დასახელება</label></td>
        					<td>
        						<input type="text" id="name" class="idle address" onblur="this.className=\'idle address\'" onfocus="this.className=\'activeField address\'" value="' . $res['name'] . '" />
        					</td>
        				</tr>
        
        			</table>
        			<!-- ID -->
        			<input type="hidden" id="source_id" value="' . $res['id'] . '" />
                </fieldset>
            </div>';
	return $data;
}

?>

