<html>
<head>
    <!-- CSS -->
    <style>
        .k-dropdown-operator {
            display: none
        }
       
        #dialog__clients{
            overflow: visible;
        }
        .ui-dialog{
            overflow: visible;
        }

        #dialog-form fieldset{
            padding: 22px 0;
            margin: 6px 3px;
        }
        #dialog-form fieldset label {
            display: block;
        }
        .chosen-container{
            width: 150px !important
        }
    </style>

    <!-- JS -->
    <script type="text/javascript">
    var DialogName	    =   "dialog__clients";
    var aJaxURL	        =   "server-side/view/client_directory.action.php";
    let param;

    $(document).ready(function(){
            LoadKendoTable__Clients('');
            $('#content_tables').attr("data-action", "clients");
    })

    function LoadKendoTable__Clients(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var aJaxURL	        =   "server-side/view/client_directory.action.php";
        var gridName        = 	'content_tables';
        var actions         = 	'<button id="clients_add" class="kendoButton__add">დამატება</button><button id="clients_delete" class="kendoButton__trash">წაშლა</button>';
        var editType        =   "popup"; // Two types "popup" and "inline"
        var itemPerPage     = 	10;
        var columnsCount    =	2;
        var columnsSQL      = 	[
                                    "id:string",
                                    "name:string"
                                ];
        var columnGeoNames  = 	[  
                                    "ID", 
                                    "სახელი"
                                ];

        var showOperatorsByColumns  =   [0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors               =   [0,0]; //IF NEED NOT USE 0

        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END

        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL, 'get_clients', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);
    }

    function LoadKendoTable__Socials(hidden, action){
        //KendoUI CLASS CONFIGS BEGIN
        var aJaxURL	        =   "server-side/view/client_directory.action.php";
        var gridName        = 	'content_tables';
        if(action == 'mail'){
            var actions         = 	'<button id="mail_add" class="kendoButton__add">დამატება</button><button id="mail_delete" class="kendoButton__trash">წაშლა</button>';
        }else{
            var actions         = 	'';
        }
        var editType        =   "popup"; // Two types "popup" and "inline"
        var itemPerPage     = 	10;
        var columnsCount    =	3;
        var columnsSQL      = 	[
                                    "id:string",
                                    "name:string",
                                    "client:string"
                                ];
        var columnGeoNames  = 	[  
                                    "ID", 
                                    "სახელი",
                                    "კლიენტი"
                                ];

        var showOperatorsByColumns  =   [0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors               =   [0,0,0]; //IF NEED NOT USE 0

        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END

        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL, action, itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);
    }

    function LoadKendoTable__Numbers(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var aJaxURL	        =   "server-side/view/client_directory.action.php";
        var gridName        = 	'content_tables';
        var actions         = 	'';
        var editType        =   "popup"; // Two types "popup" and "inline"
        var itemPerPage     = 	10;
        var columnsCount    =	3;
        var columnsSQL      = 	[
                                    "id:string",
                                    "number:string",
                                    "client:string"
                                ];
        var columnGeoNames  = 	[  
                                    "ID", 
                                    "ნომერი",
                                    "კლიენტი"
                                ];

        var showOperatorsByColumns  =   [0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors               =   [0,0,0]; //IF NEED NOT USE 0

        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END

        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL, 'get_numbers', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden);
    }

    $(document).on('click', '.client_tabs li', function(){

        $('.client_tabs li').attr('aria-selected', false)

        if($(this).attr("aria-selected") == "false"){

            $(this).attr("aria-selected", true);

            var action = $(this).attr("data-action");
            if(action == 'Clients'){
                LoadKendoTable__Clients('');
                $('.client_subtabs').empty();
                $('#content_tables').attr("data-action", "clients");
            }else if(action == "Socials"){
                $.ajax({
                    url: aJaxURL,
                    data: {
                        act: "get_social_names"
                    },
                    success: function(data){
                        var sources = data.result;
                        $('.client_subtabs').empty();
                        sources.forEach((source) => {
                             $('.client_subtabs').append('<li aria-selected="false" data-action="'+source.key+'">'+source.name+'</li>')
                        })
                        $('.client_subtabs li:first-child').click();
                    }
                })
            }else if(action == "Numbers"){
                $('.client_subtabs').empty();
                LoadKendoTable__Numbers('');
                $('#content_tables').attr("data-action", "numbers");
            }
        }
    })

    $(document).on('click', '.client_subtabs li', function(){

        $('.client_subtabs li').attr('aria-selected', false)
        if($(this).attr("aria-selected") == "false"){

            $(this).attr("aria-selected", true);
            
            var action = $(this).attr("data-action");
            if(action == 'fbm'){
                LoadKendoTable__Socials('', action);
                $('#content_tables').attr("data-action", "fbm");
            }else if(action == 'mail'){
                LoadKendoTable__Socials('', action);
                $('#content_tables').attr("data-action", "mail");
            }else if(action == 'viber'){
                LoadKendoTable__Socials('', action);
                $('#content_tables').attr("data-action", "viber");
            }
        }
    })

    $(document).on("dblclick", "#content_tables tr.k-state-selected", function() {
		var grid    = $("#content_tables").data("kendoGrid");
        var dItem   = grid.dataItem($(this));
        var action  = $('#content_tables').attr("data-action");
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=get_edit_page&id=" + dItem.id+"&table="+action,
			dataType: "json",
			success: function (data) {

                $("#dialog__clients").html(data.page);
               
				var buttons = {
					cancel: {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function () {
							$(this).dialog("close");
						}
                    },
                    done: {
						text: "შენახვა",
						class: "save-dialog",
						id: 'save-dialog'
					}
                };
                
                $("#client_selector").chosen({ search_contains: true });
                if(data.error != 1){
				    GetDialog("dialog__clients", "400", "auto", buttons, 'ceneter center');
                }
			}
		});
	});

    $(document).on('click', '.save-dialog', function(){
        
        var table = $('#hidden_table_key').val();
       

        if(table == 'clients'){
            param           = new Object;
            param.act       = "save_client";
            param.id        = $('#hidden_client_id').val();
            param.name      = $('#client_name').val();


            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(){
                    CloseDialog('dialog__clients');
                    LoadKendoTable__Clients('');
                    $('#content_tables').attr("data-action", "clients");
                }

            })
        }else if(table == 'numbers'){
            param           = new Object;
            param.act       = "save_number";
            param.id        = $('#hidden_number_id').val();
            param.client    = $('#client_selector').val();

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(){
                    CloseDialog('dialog__clients');
                    LoadKendoTable__Numbers('');
                    $('#content_tables').attr("data-action", "numbers");
                }

            })
        }else if(table == 'mail'){
            param               = new Object;
            param.act           = "save_mail";
            param.id            = $('#hidden_mail_id').val();
            param.mail_name     = $('#mail_name').val();
            param.mail_mail     = $('#mail_mail').val();
            param.mail_url      = $('#mail_url').val();
            param.client        = $('#client_selector').val();
            param.mail_password = $('#mail_password').val();
            
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(){
                    CloseDialog('dialog__clients');
                    LoadKendoTable__Socials('', 'mail');
                    $('#content_tables').attr("data-action", "mail");
                }

            })
        }

    })


    $(document).on('click', '#clients_add', function(){

        var action  = $('#content_tables').attr("data-action");

        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_add_page&table="+action,
            dataType: "json",
            success: function (data) {

                $("#dialog__clients").html(data.page);
            
                var buttons = {
                    cancel: {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $(this).dialog("close");
                        }
                    },
                    done: {
                        text: "შენახვა",
                        class: "save-dialog",
                        id: 'save-dialog'
                    }
                };
                
                $("#client_selector").chosen({ search_contains: true });
                
                GetDialog("dialog__clients", "400", "auto", buttons, 'ceneter center');
            }
        });
    })


    $(document).on('click', '#clients_delete', function () {
    
        var grid    = $("#content_tables").data("kendoGrid");
        var dItem   = grid.dataItem($("#content_tables tr.k-state-selected"));

        $.ajax({
            url: aJaxURL,
            data: {
                act: 'delete_client',
                id: dItem.id
            },
            success: function(data){
                LoadKendoTable__Clients('');
                $('#content_tables').attr("data-action", "clients");
            }
        })
    });

    $(document).on('click', '#mail_add', function(){

        var action  = $('#content_tables').attr("data-action");

        $.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=get_add_page&table="+action,
			dataType: "json",
			success: function (data) {

                $("#dialog__clients").html(data.page);
               
				var buttons = {
					cancel: {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function () {
							$(this).dialog("close");
						}
                    },
                    done: {
						text: "შენახვა",
						class: "save-dialog",
						id: 'save-dialog'
					}
                };
                
                $("#client_selector").chosen({ search_contains: true });
                
                GetDialog("dialog__clients", "400", "auto", buttons, 'ceneter center');
			}
		});
    })

    $(document).on('click', '#mail_delete', function () {
        
        var grid    = $("#content_tables").data("kendoGrid");
        var dItem   = grid.dataItem($("#content_tables tr.k-state-selected"));

        $.ajax({
            url: aJaxURL,
            data: {
                act: 'delete_mail',
                id: dItem.id
            },
            success: function(data){
                LoadKendoTable__Socials('', 'mail');
                $('#content_tables').attr("data-action", "mail");
            }
        })

    });

    </script>
</head>
<body>
<div id="section__content">
    <div class="l_responsible_fix">
        <!-- TABS SECTION -->
        <ul id="content_tabs" class="client_tabs">
            <li id="clients" data-action="Clients" aria-selected="true">კლიენტები</li>
            <li id="social_channels" data-action="Socials" aria-selected="false">კომუნიკაციის არხები</li>
            <li id="client_numbers" data-action="Numbers" aria-selected="false">ნომრები</li>
        </ul>
        <ul id="content_tabs" class="client_subtabs"></ul>
        <!-- TABLES SECTION -->
        <div id="content_tables" data-action />
    </div>
</div>
    <!-- jQuery Dialog -->
    <div id="dialog__clients" class="form-dialog" title="რედაქტირება"></div>

</body>
</html>


