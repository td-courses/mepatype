import Tdg from "../../tdg.class.js"

export default class Button extends Tdg{


    constructor(prop){
        super()

        this.state = {
            element: [],
            text: prop.text,
            style: prop.style,
            onclick: prop.onclick,
            access: prop.access,
            class: prop.class,
            prop: prop
        }

        return this.build()
    }

    build = () => {

        this.state.element.button = this.CreateElement({
            element: "button",
            ...this.state.prop
        })

        return this.state.element.button
    }



} 