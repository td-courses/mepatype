<?php

include('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

$action    = $_REQUEST['act'];
$error    = '';
$data    = '';

switch ($action) {
    case "get_tabs":
        $tabs = get_tabs();
        $data = array("tabs" => $tabs);
        break;
    case "perm_add":
        $db->setQuery(" SELECT 	 group_id
                FROM 	 users
                WHERE    id = $_SESSION[USERID]");

        $result = $db->getResultArray()['result'][0];
        if ($result['group_id'] == 34) {
            $data = array("delete" => 1);
        } else {
            $data = array("delete" => 0);
        }

        break;
    case "check_points":
        $usrid = $_REQUEST['usr_id'];

        $db->setQuery(" SELECT 	 `point`
                    FROM 	 users_point
                    WHERE    users_id = $usrid");

        $result = $db->getResultArray()['result'][0];
        if (empty($result['point'])) {
            $db->setQuery(" INSERT INTO users_point(`users_id`,`year`,`month`,`point`)
                                            VALUES('$usrid','2021','12','100')");
            $db->execQuery();
            $data = array('result' => array("point" => '100'));
        } else {
            $db->setQuery(" SELECT 	 `point`
                    FROM 	 users_point
                    WHERE    users_id = $usrid");
            $data = $db->getResultArray();
        }

        break;
    case 'get_add_page':
        $result["group"]        =   $_REQUEST["department"];
        $result["operator_id"]  =   $_REQUEST["id"];
        $result["name"]         =   $_REQUEST["phormer"];
        $result["datetime"]     =   date('y-m-d h:i:s');
        $page   =   GetPage($result);
        $data   =   array('page' => $page);
        break;
    case 'get_edit_page':
        $departmetn_id        = $_REQUEST['id'];
        $page        = GetPage(Getdisorder($departmetn_id));
        $data        = array('page'    => $page);

        break;
    case 'get_list_operators':
        $columnCount    = $_REQUEST['count'];
        $cols[]         = $_REQUEST['cols'];

        $db->setQuery("SELECT 	 users.id,
                				 user_info.`name`,
                				 COUNT(*),
                				 SUM(IF(operator_disorder.disorder_status_id IN(2,4), 1, 0)),
                				 SUM(IF(operator_disorder.disorder_status_id IN(4), 1, 0)),
                				 SUM(IF(operator_disorder.disorder_status_id IN(1,3), 1, 0)),
                				 CONCAT(100-SUM(IF(operator_disorder.disorder_status_id IN(1,3), 1,0)),'%'),
                                 CONCAT('<button id=','play_audio',' src=', '\'', DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'), asterisk_call_record.name,'.',asterisk_call_record.format, '\'',  '>მოსმენა</button>')
                        FROM 	 operator_disorder
                        JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                        JOIN    `users` ON `users`.id = operator_disorder.operator_id
                        JOIN    `user_info` ON `users`.id = user_info.user_id
                        LEFT JOIN incomming_call ON operator_disorder.incomming_id = incomming_call.id
                        LEFT JOIN asterisk_call_record ON incomming_call.asterisk_incomming_id = asterisk_call_record.asterisk_call_log_id
                        LEFT JOIN asterisk_call_log ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                        WHERE 	 operator_disorder.actived=1  AND operator_disorder.operator_id > 0
                        GROUP BY operator_disorder.operator_id");

        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;
        break;
    case 'get_list':
        $columnCount = $_REQUEST['count'];
        $cols[]      = $_REQUEST['cols'];
        $tab_id      = $_REQUEST['tab_id'];
        $flt = '';

        $db->setQuery(" SELECT  id 
                        FROM users WHERE group_id IN(31,46,48)");

        $res = $db->getResultArray()['result'];
        $tmpids = "";
        if (!empty($_SESSION["USERID"])) {
            $user_id = $_SESSION["USERID"];
        } else {
            $user_id = $_SESSION["user_id"];
        }

        foreach ($res as $user) {
            $tmpids .= $user['id'] . " , ";
        }

        if (!preg_match("/\b{$user_id}\b/", $tmpids)) {
            $flt = " AND users.id = $user_id OR `user_info`.manager_id = $user_id ";
        }

        $db->setQuery("SELECT 	 operator_disorder.id,
                                 operator_disorder.`datetime`,
                                 operator_disorder.`appeal_date`,
                                `user_info`.`name`,
                                `group`.`name`,
                                 disorders.`disorder`,
                                 disorders.`penalty`,
                                `users_point`.`point` AS `balance`,
                                CONCAT('<button id=','play_audio',' src=', '\'', DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime),'%Y/%m/%d/'), asterisk_call_record.name,'.',asterisk_call_record.format, '\'',  '>მოსმენა</button>')

                        FROM 	 operator_disorder
                        LEFT JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                        JOIN    `users` ON `users`.id = operator_disorder.operator_id
                        LEFT JOIN    `user_info` ON `users`.id = user_info.user_id
                        LEFT JOIN    `group` ON `group`.id = users.group_id
                        LEFT JOIN    `users_point` ON  `users_point`.users_id = `users`.id
                        LEFT JOIN incomming_call ON operator_disorder.incomming_id = incomming_call.id
                        LEFT JOIN asterisk_call_record ON incomming_call.asterisk_incomming_id = asterisk_call_record.asterisk_call_log_id
                        LEFT JOIN asterisk_call_log ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                        WHERE 	 operator_disorder.actived=1 AND operator_disorder.disorder_status_id = '$tab_id'
                        $flt ");

        $result = $db->getKendoList($columnCount, $cols);

        $data = $result;

        break;
    case 'save_disorder':
        $disorder_id        = $_REQUEST['id'];
        $disorder           = $_REQUEST['disorder'];
        $operator_id        = $_REQUEST['operator_id'];
        $status             = $_REQUEST['status'];
        $penalty            = $_REQUEST['penalty'];
        $appeal             = $_REQUEST['appeal'];
        $comment            = $_REQUEST['comment'];
        $inc_id             = $_REQUEST['inc_id'];
        
        if ($disorder_id == '') {
            Adddisorder($disorder_id, $disorder, $operator_id, $status, $penalty, $appeal, $comment,$inc_id);
        } else {
            Savedisorder($disorder_id, $disorder, $operator_id, $status, $penalty, $appeal, $comment);
        }

        break;
    case 'get_columns':

        $columnCount   = $_REQUEST['count'];
        $cols[]        = $_REQUEST['cols'];
        $columnNames[] = $_REQUEST['names'];
        $operators[]   = $_REQUEST['operators'];
        $selectors[]   = $_REQUEST['selectors'];
        //$query = "SHOW COLUMNS FROM $tableName";
        //$db->setQuery($query,$tableName);
        //$res = $db->getResultArray();
        $f = 0;
        foreach ($cols[0] as $col) {
            $column = explode(':', $col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        $types = array();
        foreach ($res as $item) {
            $columns[$i] = $item['Field'];
            $types[$i] = $item['type'];
            $i++;
        }


        $dat = array();
        $a = 0;
        for ($j = 0; $j < $columnCount; $j++) {
            if (1 == 2) {
                continue;
            } else {

                if ($operators[0][$a] == 1) $op = true;
                else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                $op = false;
                if ($columns[$j] == 'id') {
                    $width = "5%";
                } else if ($columns[$j] == 'position') {
                    $width = "12%";
                } else {
                    $width = 'auto';
                }
                if ($columns[$j] == 'inc_id') {
                    $hidden = true;
                } else if ($columns[$j] == 'docs_project_id') {
                    $hidden = true;
                } else if ($columns[$j] == 'id') {
                    $hidden = true;
                } else {
                    $hidden = false;
                }
                if ($res['data_type'][$j] == 'date') {
                    $g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
                } else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
                } else {
                    $g = array('field' => $columns[$j], 'hidden' => $hidden, 'width' => $width, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
                }
                $a++;
            }
            array_push($dat, $g);
        }

        //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for ($j = 0; $j < $columnCount; $j++) {
            if ($types[$j] == 'date') {
                $new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
            } else if ($types[$j] == 'number') {

                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'number');
            } else {
                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
            }
        }

        $filtArr = array('fields' => $new_data);



        $kendoData = array('columnss' => $dat, 'modelss' => $filtArr);


        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';

        break;
    case "get_appeal_page":

        $page = appeal_dialog();

        $data = array("page" => $page);
        break;
    case 'disable':
        $department_id    = $_REQUEST['id'];
        Disabledepartment($department_id);

        break;

    case "save_appeal":
        $date   =  $_REQUEST['date'];
        $id     = $_REQUEST['id'];
        $operator   =  $_REQUEST['operator'];
        $disorder_id = $_REQUEST['disorder_id'];
        $operator_agree   =  $_REQUEST['operator_agree'];
        $operator_comment   =  $_REQUEST['operator_comment'];
        $maneger   =  $_REQUEST['maneger'];
        $maneger_agree   =  $_REQUEST['maneger_agree'];
        $maneger_comment   =  $_REQUEST['maneger_comment'];
        $head_manager   =  $_REQUEST['head_manager'];
        $head_manager_agree   =  $_REQUEST['head_manager_agree'];
        $head_manager_comment   =  $_REQUEST['head_manager_comment'];
        if (isset($_REQUEST['appeal'])) {
            $appeal = ($_REQUEST['appeal'] == "true");
        }
        if ($appeal) {
            $db->setQuery("UPDATE `operator_disorder`
                              SET `disorder_status_id` = 2
                           WHERE  `id` = '$disorder_id'");
            $db->execQuery();

            $db->setQuery("UPDATE `attendance`
                              SET `disorder_status` = 2
                            WHERE `id` = '$disorder_id'");
            $db->execQuery();
        }

        if ($id == '') {

            $db->setQuery("INSERT INTO  `disorder_appeal`
                                   SET  `datetime` = now(),
                                        `user_id`  = '$operator',
                                        `disorder_id` = '$disorder_id',
                                        `operator_agree` = '$operator_agree',
                                        `operator_comment` = '$operator_comment',
                                        `manager_id` = '$maneger',
                                        `manager_agree` = '$maneger_agree',
                                        `manager_comment` = '$maneger_comment',
                                        `call_center_manager_id` = '$head_manager',
                                        `call_center_manager_agree` = '$head_manager_agree',
                                        `call_center_manager_comment` = '$head_manager_comment'");
            $db->execQuery();
        } else {
            $db->setQuery("UPDATE  `disorder_appeal`
                              SET  `datetime`                    = now(),
                                   `user_id`                     = '$operator',
                                   `disorder_id`                 = '$disorder_id',
                                   `operator_agree`              = '$operator_agree',
                                   `operator_comment`            = '$operator_comment',
                                   `manager_id`                  = '$maneger',
                                   `manager_agree`               = '$maneger_agree',
                                   `manager_comment`             = '$maneger_comment',
                                   `call_center_manager_id`      = '$head_manager',
                                   `call_center_manager_agree`   = '$head_manager_agree',
                                   `call_center_manager_comment` = '$head_manager_comment'
                            WHERE  `id`                          = '$id'");
            $db->execQuery();
        }

        break;
    case 'get_charts':

        $data = array(
            'main' =>
            array(
                'chart' =>
                array(
                    'typeData'   => 'pie',
                    'seriesData' => array(),
                    'categoriesData' => array()
                ),
                'chart1' =>
                array(
                    'typeData'       => 'donut',
                    'seriesData'     => array(),
                    'categoriesData' => array()
                )
            )
        );

        $db->setQuery("SELECT 	 COUNT(*) AS `count`, 
                                 disorder_status.`name`
                        FROM 	 operator_disorder
                        JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                        JOIN    `crystal_users` ON `crystal_users`.id = operator_disorder.operator_id
                        JOIN     disorder_status ON disorder_status.id = operator_disorder.disorder_status_id
                        WHERE 	 operator_disorder.actived=1  AND operator_disorder.operator_id > 0
                        GROUP BY operator_disorder.disorder_status_id");
        $res = $db->getResultArray();

        foreach ($res[result] as $row) {
            array_push($data[main][chart1][seriesData], (int)$row[count]);
            array_push($data[main][chart1][categoriesData], $row[name]);
        }

        $db->setQuery(" SELECT   COUNT(*) AS `count`, disorders.`disorder` AS `name`
                        FROM 	  operator_disorder
                        JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                        JOIN    `crystal_users` ON `crystal_users`.id = operator_disorder.operator_id
                        WHERE 	  operator_disorder.actived=1  AND operator_disorder.operator_id > 0
                        GROUP BY operator_disorder.disorder_id");

        $req = $db->getResultArray();
        foreach ($req[result] as $row1) {
            array_push($data[main][chart][seriesData], (int)$row1[count]);
            array_push($data[main][chart][categoriesData], $row1[name]);
        }
        break;
    case "get_user_info":
        $user_id = $_REQUEST["user_id"];
        $phormer = $_REQUEST["phormer_id"];
        $db->setQuery(" SELECT 	group.name,
                                user_info.`name` AS phormer
                        FROM users
                        LEFT JOIN `group` ON group.id = users.group_id
                        LEFT JOIN users AS phormer ON phormer.id =  $phormer
                        LEFT JOIN user_info ON user_info.user_id =  phormer.id
                        WHERE users.id = $user_id 
                        GROUP BY users.id");

        $data["result"] = $db->getResultArray();
        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Adddisorder($disorder_id, $disorder, $operator_id, $status, $penalty, $appeal, $comment,$inc_id)
{

    global $db;

    $db->setQuery("  SELECT disorders.id,
                            sum(disorders.penalty) as penalty
                    FROM 	 operator_disorder
                    LEFT JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                    LEFT JOIN    `disorder_appeal` ON disorder_appeal.`disorder_id` = `operator_disorder`.`id`
                    JOIN    `users` ON `users`.id = operator_disorder.operator_id
                    WHERE `users`.id = $operator_id AND (disorder_appeal.operator_agree = 1 OR disorder_appeal.manager_agree  = 1 OR disorder_appeal.call_center_manager_agree = 1)");
    $db->execQuery();

    $penalty = $db->getResultArray()['result'][0]['penalty'];
    $num = "";
    if (empty($penalty)) {
        $num = 100;
    } else {
        $num = 100 - $penalty;
    }

    $db->setQuery(" UPDATE `users_point`
                        SET `point` = $num
                    WHERE   users_id = $operator_id");
    $db->execQuery();

    $user_id    = $_SESSION['USERID'];
    $db->setQuery("INSERT INTO  `operator_disorder`	 
                           SET  `datetime`           =  NOW(),
                                `appeal_date`        = '$appeal',
                                `disorder_id`        = '$disorder',
                                `operator_id`        = '$operator_id',
                                `incomming_id`       = '$inc_id',
                                `user_id`            = '$user_id',
                                `disorder_status_id` = '$status',
                                `comment`            = '$comment'");
    $db->execQuery();
}

function Savedisorder($disorder_id, $disorder, $operator_id, $status, $penalty, $appeal, $comment)
{
    global $db;

    $user_id = $_SESSION['USERID'];
    $db->setQuery("	UPDATE `operator_disorder`
					   SET `disorder_id`        = '$disorder',
                           `appeal_date`        = '$appeal',
                           `operator_id`        = '$operator_id',
                           `user_id`            = '$user_id',
                           `disorder_status_id` = '$status',
                           `comment`            = '$comment'
                     WHERE `id`                 = '$disorder_id'");
    $db->execQuery();

    $db->setQuery("	SELECT disorders.id,
                            sum(disorders.penalty) as penalty
                        FROM 	 operator_disorder
                        LEFT JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                        LEFT JOIN    `disorder_appeal` ON disorder_appeal.`disorder_id` = `operator_disorder`.`id`
                        JOIN    `users` ON `users`.id = operator_disorder.operator_id
                        WHERE `users`.id = $operator_id AND (disorder_appeal.operator_agree = 1 OR disorder_appeal.manager_agree  = 1 OR disorder_appeal.call_center_manager_agree = 1)");

    $penalty = $db->getResultArray()['result'][0]['penalty'];
    $num = "";
    if (empty($penalty)) {
        $num = 100;
    } else {
        $num = 100 - $penalty;
    }

    $db->setQuery(" UPDATE `users_point`
                        SET `point` = $num
                    WHERE   users_id = $operator_id");
    $db->execQuery();
}

function Disabledepartment($department_id)
{
    global $db;
    $db->setQuery("	UPDATE `operator_disorder`
					   SET `actived` = 0
					WHERE  `id` = $department_id");
    $db->execQuery();
}

function CheckdepartmentExist($department_name)
{
    global $db;
    $db->setQuery("	SELECT `id`
					FROM   `department`
					WHERE  `name` = '$department_name' && `actived` = 1");
    $res = $db->getResultArray();
    if ($res[result][0]['id'] != '') {
        return true;
    }
    return false;
}


function Getdisorder($department_id)
{
    global $db;
    $db->setQuery("	SELECT 	operator_disorder.id,
                                            operator_disorder.`datetime`,
                                            operator_disorder.`disorder_status_id` AS `status_id`,
                                            operator_disorder.`appeal_date` AS appeal,
                                            `us`.`name`,
                                            `users`.id AS `operator_id`,
                                            `group`.`name` AS `group`,
                                            disorders.id AS `disorder_id`,
                                            disorders.`disorder`,
                                            disorders.`penalty`,
                                            operator_disorder.comment,
                                            `users_point`.`point` AS `balance`,
                                            users.id as user_id_n
                                        FROM 	operator_disorder
                                        LEFT JOIN    `disorders` ON operator_disorder.`disorder_id` = `disorders`.`id`
                                        JOIN    `users` ON `users`.id = operator_disorder.operator_id
                                        LEFT JOIN    `user_info` AS us ON `us`.user_id = operator_disorder.user_id
                                        LEFT JOIN    `group` ON `group`.id = users.group_id
                                        LEFT JOIN    `users_point` ON  `users_point`.users_id = `users`.id
                                        WHERE 	operator_disorder.id = $department_id ");
    $res = $db->getResultArray();
    return $res[result][0];
}

function GetPage($res = '')
{
    $data = '
	<div id="dialog-form">
	    <fieldset>
	    	<legend>ძირითადი ინფორმაცია</legend>

            <div class="dialog-grid" style="margin-left:12px">
                <div class="grid1">
                    <div>
                        <span>დააფორმირა :</span><br><br>
                        <span class="text">' . $res['name'] . '</span>
                    </div>
                    <div>
                        <span>ფორმირების თარიღი :</span><br><br>
                        <span class="text">' . $res['datetime'] . '</span>
                    </div>
                </div>  
                <div class="grid2">
                    <div>
                        <span>სახელი და გვარი :</span><br><br>
                        <select id="operator_id" data-select="chosen">' . get_users($res['operator_id']) . '</select>
                    </div>
                    <div>
                        <span>თანამდებობა</span><br><br>
                        <input id="group" type="text" value="' . $res['group'] . '" />
                    </div>
                    <div>
                        <span>მიმდ.დსცპ.ბალანსი</span><br><br>
                        <input id="balance" type="number" value="' . $res['balance'] . '" />
                    </div>

                </div>
                <div class="grid3">
                    <span>დსცპ დარღვევა</span>

                    <select id="disorder" data-select="chosen">' . get_disorder($res['disorder_id']) . '</select>
                
                </div> 
                <div class="grid4">
                    <div>
                        <span>საჯარიმო ქულა</span><br><br>
                        <div style="display: flex;"><input id="penalty" type="number" value="' . $res['penalty'] . '" /><span style="font-size: 22px;">%</span></div>
                    </div>
                    <div>
                        <span>გასაჩივრების დედლაინი</span><br><br>
                        <input id="appeal" type="text" value="' . $res['appeal'] . '" />
                    </div>
                    <div>
                        <span>სტატუსი</span><br><br>
                        <select id="status" data-select="chosen">' . get_status($res['status_id']) . '</select>
                    </div>
                </div> 
                <div class="grid5">
                    <span>კომენტარი</span>
                    <textarea id="comment">' . $res['comment'] . '</textarea>
                </div>           
            
            </div>
			<!-- ID -->
			<input type="hidden" id="hidden_id" value="' . $res['id'] . '" />
            <input type="hidden" id="hidden_id_user" value="' . $res['user_id_n'] . '" />
        </fieldset>
    </div>
    ';
    return $data;
}

function get_users($id)
{
    global $db;
    $data = '';
    $db->setQuery(" SELECT `users`.`id`, `user_info`.`name`, `group`.`name` AS `group_name`, users_point.point 
				    FROM `users`
                    JOIN `user_info` ON `user_info`.user_id = `users`.id
                    LEFT JOIN `group` ON `group`.id = `users`.group_id
                    LEFT JOIN `users_point` ON `users`.id = users_point.users_id
				    WHERE users.actived=1 ");

    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] as $res) {

        if ($res['id'] == $id) {
            $data .= '<option value="' . $res['id'] . '" selected="selected" group_name="' . $res['group_name'] . '" point="' . $res['point'] . '">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '" group_name="' . $res['group_name'] . '" point="' . $res['point'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function get_disorder($id)
{
    global $db;
    $data = '';
    $db->setQuery(" SELECT `id`, `disorder` AS `name`, `penalty`, DATE_ADD(NOW(), INTERVAL `appeal` DAY) AS  `appeal`
				    FROM   `disorders`
				    WHERE   actived = 1 ");

    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] as $res) {
        if ($res['id'] == $id) {
            $data .= '<option value="' . $res['id'] . '" selected="selected" penalty="' . $res['penalty'] . '" appeal="' . $res['appeal'] . '">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '" penalty="' . $res['penalty'] . '" appeal="' . $res['appeal'] . '" >' . $res['name'] . '</option>';
        }
    }

    return $data;
}

function get_status($id)
{
    global $db;
    $data = '';
    $db->setQuery(" SELECT `id`, `name` 
				    FROM   `disorder_status`
				    WHERE   actived=1 ");
    $req = $db->getResultArray();

    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] as $res) {
        if ($res['id'] == $id) {
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }

    return $data;
}


function appeal_dialog($res)
{
    global $db;
    $id      = $_REQUEST['id'];
    $user_id = $_SESSION['USERID'];
    $user_id_n      = $_REQUEST['user_id_n'];


    $db->setQuery("SELECT id
                   FROM   disorder_appeal
                   WHERE  disorder_id = '$id'
                   AND    actived = 1");
    $num_row = $db->getNumRow();
    if ($num_row == 0) {
        $db->setQuery("SELECT    '' AS id,
                                 users.id AS op_id,
                        		 user_info.`name` AS `op_name`,
                        		 manager.id AS `manager_id`,
                        		 manager1.`name` AS `manager_name`,
                                '102' AS `call_center_manager_user_id`,
                        		'თათული ტოხვაძე' AS `call_center_manager_name`,
                                 NOW() AS `datetime`,
                                 '2' AS `operator_agree`
                       FROM      users
                       LEFT JOIN user_info  ON user_info.user_id = users.id
                       LEFT JOIN users AS `manager` ON manager.id = user_info.manager_id
					   LEFT JOIN user_info AS `manager1` ON manager.id = manager1.user_id
                       WHERE     users.id = '$user_id_n'");

        $req = $db->getResultArray();

        $res = $req[result][0];
    } else {
        $db->setQuery("SELECT     disorder_appeal.id,
                                  users.id AS op_id,
                                  user_info.`name` AS `op_name`,
                                  disorder_appeal.`datetime`,
                                  disorder_appeal.operator_agree,
                                  disorder_appeal.operator_comment,
                                  manager.id AS `manager_id`,
                                  manager1.`name` AS `manager_name`,
                                  disorder_appeal.manager_agree,
                                  disorder_appeal.manager_comment,
                                  '102' AS `call_center_manager_user_id`,
                        		  'თათული ტოხვაძე' AS `call_center_manager_name`,
                                  disorder_appeal.call_center_manager_agree,
                                  disorder_appeal.call_center_manager_comment
                        FROM      disorder_appeal
                        JOIN      users ON users.id = disorder_appeal.user_id
                        JOIN      user_info ON users.id = user_info.user_id
                        LEFT JOIN users AS `manager` ON manager.id = disorder_appeal.manager_id
                        LEFT JOIN user_info AS `manager1` ON manager.id = manager1.user_id
                        LEFT JOIN users AS `call_center_manager` ON call_center_manager.id = disorder_appeal.call_center_manager_id
                        LEFT JOIN user_info AS `call_center_manager1` ON call_center_manager1.user_id = call_center_manager.id
                        WHERE     disorder_appeal.disorder_id = '$id'");

        $req = $db->getResultArray();

        $res = $req[result][0];
    }



    $data = '<div id="dialog-form">
                <fieldset>
                    <table class="dialog-form-table">
                        <tr style="height:0px">
                            <td style="width: 30%;">გასაჩივრების თარიღი</td>
                            <td style="width: 20%;"></td>
                            <td style="width: 50%;"></td>
                        </tr>
                        <tr style="height:0px">
                            <td><input style="width: 90%;" type="text" id="monitoring_appealing_date" class="idle" value="' . $res['datetime'] . '" disabled="disabled"/></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="height:20px;"></tr>
                        <tr style="height:0px">
                            <td>ოპერატორი</td>
                            <td>ეთანხმება</td>
                            <td>კომენტარი</td>
                        </tr>
                        <tr style="height:0px">
                            <td><input style="width: 90%;" type="text" id="monitoring_appealing_operator" class="idle" user_id="' . $res[op_id] . '" value="' . $res[op_name] . '" disabled="disabled"/></td>
                            <td><select id="operator_appealing_yes" class="idle" style="width: 130px;" data-select="chosen">' . get_apperling_status($res[operator_agree]) . '</select></td>
                            <td><textarea  style="width: 100%; resize: vertical;" id="operator_appealing_comment" class="idle" name="call_content" cols="300" rows="8">' . $res['operator_comment'] . '</textarea></td>
                        </tr>
                        <tr style="height:5px;"></tr>
                        <tr style="height:0px">
                            <td>მენეჯერი</td>
                            <td>ეთანხმება</td>
                            <td>კომენტარი</td>
                        </tr>
                        <tr style="height:0px">
                            <td><input style="width: 90%;" type="text" id="monitoring_appealing_manager" class="idle" user_id="' . $res[manager_id] . '" value="' . $res[manager_name] . '" disabled="disabled"/></td>
                            <td><select id="manager_appealing_yes" class="idle" style="width: 130px;" data-select="chosen">' . get_apperling_status($res[manager_agree]) . '</select></td>
                            <td><textarea  style="width: 100%; resize: vertical;" id="manager_appealing_comment" class="idle" name="call_content" cols="300" rows="8">' . $res['manager_comment'] . '</textarea></td>
                        </tr>
                        <tr style="height:5px;"></tr>
                        <tr style="height:0px">
                            <td>ქოლ-ცენტრის ხელმძღვანელი</td>
                            <td>ეთანხმება</td>
                            <td>კომენტარი</td>
                        </tr>
                        <tr style="height:0px">
                            <td><input style="width: 90%;" type="text" id="monitoring_appealing_call_center_manager" class="idle" user_id="' . $res[call_center_manager_user_id] . '" value="' . $res[call_center_manager_name] . '" disabled="disabled"/></td>
                            <td><select id="call_center_manager_appealing_yes" class="idle" style="width: 130px;" data-select="chosen">' . get_apperling_status($res[call_center_manager_agree]) . '</select></td>
                            <td><textarea  style="width: 100%; resize: vertical;" id="call_center_manager_appealing_comment" class="idle" name="call_content" cols="300" rows="8">' . $res['call_center_manager_comment'] . '</textarea></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <input type="hidden" id="appeal_hidden_id" value="' . $res['id'] . '" />
            
            ';

    return $data;
}


function get_apperling_status($id)
{

    $data .= '<option value="0" selected="selected">------</option>';


    if ($id == 1) {
        $data .= '<option value="1" selected="selected">კი</option>
                  <option value="2">არა</option>';
    } else if ($id == 2) {
        $data .= '<option value="1">კი</option>
                  <option value="2" selected="selected">არა</option>
                  ';
    } else {
        $data .= '<option value="1">კი</option>
                  <option value="2">არა</option>';
    }

    return $data;
}


function get_tabs()
{
    global $db;
    $data = "";
    $flt = '';

    $db->setQuery(" SELECT  id 
    FROM users WHERE group_id IN(31,46,48)");

    $res = $db->getResultArray()['result'];
    $tmpids = "";
    if (!empty($_SESSION["USERID"])) {
        $user_id = $_SESSION["USERID"];
    } else {
        $user_id = $_SESSION["user_id"];
    }

    foreach ($res as $user) {
        $tmpids .= $user['id'] . " , ";
    }

    if (!preg_match("/\b{$user_id}\b/", $tmpids)) {
        $flt = " AND users.id = $user_id OR `user_info`.manager_id = $user_id ";
    }

    $db->setQuery(" SELECT * 
                    FROM (SELECT    disorder_status.`id`, 
                                    disorder_status.`name`,
                                    COUNT(operator_disorder.id) AS `count`
                          FROM     `disorder_status` 
                          LEFT JOIN operator_disorder ON operator_disorder.disorder_status_id = disorder_status.id
                          JOIN    `users` ON `users`.id = operator_disorder.operator_id
                          JOIN    `user_info` ON `user_info`.user_id = operator_disorder.operator_id
                          WHERE    `disorder_status`.`actived` = 1 AND operator_disorder.actived = 1 $flt
                          GROUP BY  disorder_status.`id` 
                    UNION ALL
                    SELECT   disorder_status.`id`, 
                            `name`, '0' AS `count` 
                    FROM    `disorder_status` ) AS x
                    GROUP BY id");

    $res = $db->getResultArray();
    $all_count = 0;
    foreach ($res[result] as $arr) {
        $all_count += $arr['count'];
        if ($arr['id'] == 1) $active = 'active';
        else $active = "";
        $data .= '<div class="tab ' . $active . '" i_id = "' . $arr['id'] . '" >' . $arr['name'] . '<span>' . $arr['count'] . '</span></div>';
    }

    $data .= '<div class="tab" i_id = "5" >სულ<span>' . $all_count . '</span></div>';
    return $data;
}
