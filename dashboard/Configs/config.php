<?php

/**
 * CONFIG FILE
 */



ini_set("display_errors", true);
error_reporting(E_ERROR);

include "sql.config.php";

session_start();

spl_autoload_register(function($class) {
    require_once str_replace('\\', '/', $class) . '.class.php';
  });

?>