<?php
    include('../../includes/classes/class.Mysqli.php');
    global $db;
    $db = new dbClass();
?>
<html>
<head>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script type="text/javascript">
    var aJaxURL = "server-side/view/automator.action.php";
    var page_id = '';
    $(document).on('change','#input_type', function(){
        var type = $("#input_type").val();
        var deep = $("#multilevel_deep").val();
        if(type == 6 || type == 7 || type == 8 || type == 9){
            $("#input_namer").css("display","");
            $("#selectors_type_inputs").css("display","block");
            $(".multilevelpart_1").css('display','none');
            $("#multilevelpart_2").css('display','none');
        }
        else if(type == 10){
            $("#input_namer").css("display","");
            $("#selectors_type_inputs").css("display","none");
            $(".multilevelpart_1").css('display','contents');
            $("#multilevelpart_2").css('display','block');
            
        }
        else if(type == 11 || type == 12){
            $("#input_namer").css("display","none");
        }
        else{
            $("#input_namer").css("display","");
            $("#selectors_type_inputs").css("display","none");
            $(".multilevelpart_1").css('display','none');
            $("#multilevelpart_2").css('display','none');
        }
    });
    $(document).on('change','#multilevel_deep', function(){
        var deep = $("#multilevel_deep").val();

        if(deep != 0){
            if(deep == 2){
                $("#multi_level_1").css('display','block');
                $("#multi_level_2").css('display','block');
                $("#signature_dialog_form_main_table2").css('display','block');
            }
            else if(deep == 3){
                $("#multi_level_1").css('display','block');
                $("#multi_level_2").css('display','block');
                $("#multi_level_3").css('display','block');
                $("#signature_dialog_form_main_table2").css('display','block');
            }
        }
        else{
            $("#multi_level_1").css('display','none');
            $("#multi_level_2").css('display','none');
            $("#multi_level_3").css('display','none');
            $("#signature_dialog_form_main_table2").css('display','none');
        }
    });
    $(document).ready(function () {
        LoadKendoTable();
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_pages",
            dataType: "json",
            success: function (data) {
                var tabs = data.tabs;
                tabs.forEach(function(tab){
                    $("#status_tabs").append('<li id="'+tab.id+'" aria-selected="false">'+tab.name+'</li>');
                });
                $('.status_tabs li:first-child').click(); //Activates first tab
                var post_data = "&page_id="+$('.status_tabs li:first-child').attr('id');
            }
        });
    });

    $(document).on('click', '.status_tabs li', function(){
            $(".status_tabs li").each(function() {
                $(this).attr('aria-selected','false');
            });
            $(this).attr('aria-selected','true')
            var sub_status = $(this).attr('id');
            page_id = sub_status;
            var post_data = "&page_id="+sub_status;
            LoadKendoTable(post_data)
    });
    function LoadKendoTable(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'processing_page';
        var actions = 				    '<button class="outgoing_button button_add_setting_by_page" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_setting_by_page" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    4;
        var columnsSQL = 				["id:string","date:date","sourc:string","fieldsets:string"];
        var columnGeoNames = 		    ["ID","შექმნის თარიღი","წყარო","Fieldsets"];

        var showOperatorsByColumns =    [0,0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function LoadKendo_fieldsets(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'fieldsets_table';
        var actions = 				    '<button class="outgoing_button button_add_fieldset" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_fieldset" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    3;
        var columnsSQL = 				["id:string","fieldset:string","inputs:string"];
        var columnGeoNames = 		    ["ID","Fieldset","ველები"];

        var showOperatorsByColumns =    [0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_fieldset_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function LoadKendo_inputs(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'fieldset_inputs';
        var actions = 				    '<button class="outgoing_button button_add_input" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_input" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    5;
        var columnsSQL = 				["id:string","input:string","type:string","tab:string","position:number"];
        var columnGeoNames = 		    ["ID","ველი","ტიპი","ტაბი","პოზიცია"];

        var showOperatorsByColumns =    [0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_fieldset_input_list',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function LoadKendo_selectors(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'selectors_type_kendo';
        var actions = 				    '<button class="outgoing_button button_add_selector" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_selector" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    2;
        var columnsSQL = 				["id:string","input:string"];
        var columnGeoNames = 		    ["ID","პარამეტრი"];

        var showOperatorsByColumns =    [0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_selectors_data',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function LoadKendo_level_1(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'multi_level_1';
        var actions = 				    '<button class="outgoing_button" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    2;
        var columnsSQL = 				["id:string","input:string"];
        var columnGeoNames = 		    ["ID","პარამეტრი"];

        var showOperatorsByColumns =    [0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_multilevel_1',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function LoadKendo_level_2(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'multi_level_2';
        var actions = 				    '<button class="outgoing_button" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    3;
        var columnsSQL = 				["id:string","input:string","cat_param:string"];
        var columnGeoNames = 		    ["ID","პარამეტრი","კატ.პარამეტრი"];

        var showOperatorsByColumns =    [0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_multilevel_2',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function LoadKendo_level_3(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'multi_level_3';
        var actions = 				    '<button class="outgoing_button" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    3;
        var columnsSQL = 				["id:string","input:string","subcat_param:string"];
        var columnGeoNames = 		    ["ID","პარამეტრი","ქვე.კატ.პარამეტრი"];

        var showOperatorsByColumns =    [0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_multilevel_3',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
    function geo_to_latin (str) {
    
        var ru = {
            'ა': 'a', 'ბ': 'b', 'გ': 'g', 'დ': 'd', 'ე': 'e', 'ვ': 'v', 'ზ': 'z', 
            'თ': 't', 'ი': 'i', 'კ': 'k', 'ლ': 'l','მ': 'm', 'ნ': 'n', 'ო': 'o',
            'პ': 'p', 'რ': 'r', 'ს': 's', 'ტ': 't', 'უ': 'u', 'ფ': 'f', 'ქ': 'q', 
            'ღ': 'gh','ყ': 'y', 'შ': 'sh', 'ჩ': 'ch', 'ც': 'c', 'ძ': 'dz', 'წ': 'w',
            'ჭ': 'ch', 'ხ': 'x', 'ჯ': 'j', 'ჰ': 'h', 'ჟ': 'zsh', ' ': '_','.':'_','/':'_','*':'_','=':'_','+':'_'
        }, n_str = [];
        for ( var i = 0; i < str.length; ++i ) {
            n_str.push(
                    ru[ str[i] ]
                || ru[ str[i].toLowerCase() ] == undefined && str[i]
                || ru[ str[i].toLowerCase() ].replace(/^(.)/, function ( match ) { return match.toUpperCase() })
            );
        }
        return n_str.join('');
    }
    $(document).on('click','.button_add_setting_by_page',function(){
        var page_id = $("#status_tabs li[aria-selected='true']").attr('id');

        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_setting&page_id="+page_id,
            dataType: "json",
            success: function (data) {
                $("#get_fieldsets").html(data.page);
                var optional = $("#optional").kendoMultiSelect({
                    autoClose: false
                }).data("kendoMultiSelect");
                LoadDialog('get_fieldsets');
            }
        });
    });
    $(document).on('click','.button_delete_fieldset',function(){
       
        var grid = $("#fieldsets_table").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        //alert(dItem.id);
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=disable&type=fieldset&id="+dItem.id,
            dataType: "json",
            success: function (data) {
                $("#fieldsets_table").data("kendoGrid").dataSource.read();
            }
        });
    });
    $(document).on('click','.button_delete_input',function(){
       
       var grid = $("#fieldset_inputs").data("kendoGrid");
       var dItem = grid.dataItem(grid.select());
       //alert(dItem.id);
       $.ajax({
           url: aJaxURL,
           type: "POST",
           data: "act=disable&type=input&id="+dItem.id,
           dataType: "json",
           success: function (data) {
               $("#fieldset_inputs").data("kendoGrid").dataSource.read();
               $.ajax({
                    url: aJaxURL,
                    data: {
                        'act'		: 'get_demo_view',
                        'fieldset_id': $("#fieldset_id").val()
                    },
                    success: function(data) {
                        $("#demo_view").empty();
                        $("#demo_view").html(data.page);
                        $.ajax({
                            url: aJaxURL,
                            data: {
                                'act'		: 'get_fieldsets',
                                'fieldset_id': $("#fieldset_id").val()
                            },
                            success: function(data) {
                                $(data.chosen_keys).chosen({ search_contains: true });
                            }
                        });
                    }
                });
           }
       });
   });


    $(document).on('keyup','#input_name',function(){
        $('#input_key').val(geo_to_latin($('#input_name').val()));
    });
    $(document).on("dblclick", "#processing_page tr.k-state-selected", function () {
        var grid = $("#processing_page").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_edit_page&id="+dItem.id+"&page_id="+page_id,
            dataType: "json",
            success: function (data) {
                $("#get_fieldsets").html(data.page);
                
                LoadDialog('get_fieldsets');
                var dialog_id = "&id="+dItem.id+"&page_id="+page_id;
                LoadKendo_fieldsets(dialog_id);
                var optional = $("#optional").kendoMultiSelect({
                    autoClose: false
                }).data("kendoMultiSelect");
            }
        });
    });
    $(document).on('click','.dialog_input_tabs li',function(){
            ///SWITCHING TABS HERE///
            var field_id = $(this).parent().attr('id');
            var tab_id   = $(this).attr('data-id');
            $('#'+field_id+' li').removeAttr('aria-selected');
            $(this).attr('aria-selected','true');
            /////////////////////////
            $("."+field_id).css('display','none');

            $("."+field_id+"[data-tab-id='"+tab_id+"']").css('display','block');

        });
    $(document).on("dblclick", "#fieldsets_table tr.k-state-selected", function () {
        var grid = $("#fieldsets_table").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=fieldset_editor&fieldset_id="+dItem.id,
            dataType: "json",
            success: function (data) {
                $("#fieldsets_editor").html(data.page);
                
                LoadDialog('fieldsets_editor');
                var fieldset_id = "&fieldset_id="+dItem.id;
                LoadKendo_inputs(fieldset_id)

                $.ajax({
                    url: aJaxURL,
                    data: {
                        'act'		: 'get_fieldsets',
                        'fieldset_id': dItem.id
                    },
                    success: function(data) {
                        $(data.chosen_keys).chosen({ search_contains: true });
                    }
                });
                
            }
        });
    });
    $(document).on("dblclick", "#fieldset_inputs tr.k-state-selected", function () {
        var grid = $("#fieldset_inputs").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=input_editor&input_id="+dItem.id+"&fieldset_id="+$("#fieldset_id").val(),
            dataType: "json",
            success: function (data) {
                $("#input_editor").html(data.page);
                LoadDialog('input_editor');
                var input = "&input="+dItem.id;
                LoadKendo_selectors(input);
                GetButtons("add_button","delete_button");
                $("[data-button='jquery-ui-button']").button();

                $.ajax({
                    url: aJaxURL,
                    data: 'act=get_multilevel_list&input_id='+dItem.id+'&field_id='+$("#fieldset_id").val(),
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                $('#tree1').html(data.page);
                                $('#tree1').treed();
                            }
                        }
                    }
                });
            }
        });
    });
    $(document).on('click','#add_tab', function(){
        var field_id = $("#field_id").val();

        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_tab&field_id="+field_id,
            dataType: "json",
            success: function (data) {
                $("#add_tab_page").html(data.page);
                LoadDialog('add_tab_page');
            }
        });
    });
    $(document).on("dblclick", "#selectors_type_kendo tr.k-state-selected", function () {
        var grid = $("#selectors_type_kendo").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_selector&selector_id="+dItem.id+"&input_id="+$("#input_id").val(),
            dataType: "json",
            success: function (data) {
                $("#add_selector").html(data.page);
                LoadDialog('add_selector');
            }
        });
    });
    function LoadDialog(fNm){
        $("#loading").show();
        if(fNm=='get_fieldsets'){

            var buttons =
                {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            var buttons1 = {
                                "done": {
                                    text: "კი",
                                    id: "save-yes",
                                    click: function () {
                                        $("#get_fieldsets").dialog("close");
                                        $(this).dialog("close");
                                    }
                                },
                                "no": {
                                    text: "არა",
                                    id: "save-no",
                                    click: function () {
                                        $(this).dialog("close");
                                    }
                                }
                            };
                            GetDialog("hint_close", 300, "auto", buttons1);
                            $("#save-yes").css('float', 'right');
                            $("#save-no").css('float', 'right');
                        }
                    },
                    "done": {
                        text: "შენახვა",
                        id: "save-setting",
                        click: function () {
                            var multiSelectedValues = $("#optional").data("kendoMultiSelect").value();

                            var param 	          = new Object();
                            param.act		      = "save_setting";
                            param.setting_id      = $("#setting_id").val();
                            param.source_values   = multiSelectedValues;
                            param.page_id         = page_id;
                            $.ajax({
                                url: aJaxURL,
                                type: "POST",
                                data: param,
                                dataType: "json",
                                success: function (data) {
                                    if(data.response == 'suc'){
                                        alert("ინფორმაცია წარმატებით განახლდა!!!");
                                        var post_data = "&page_id="+page_id;
                                        LoadKendoTable(post_data)
                                    }
                                }
                            }); 
                        }
                    }

                };


            /* Dialog Form Selector Name, Buttons Array */
            GetDialog("get_fieldsets", '1200', "auto",buttons,'center top');
        }
        else if(fNm=='fieldsets_editor'){
            var buttons =
                        {
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    var buttons1 = {
                                        "done": {
                                            text: "კი",
                                            id: "save-yes",
                                            click: function () {
                                                $("#fieldsets_editor").dialog("close");
                                                $(this).dialog("close");
                                            }
                                        },
                                        "no": {
                                            text: "არა",
                                            id: "save-no",
                                            click: function () {
                                                $(this).dialog("close");
                                            }
                                        }
                                    };
                                    GetDialog("hint_close", 300, "auto", buttons1);
                                    $("#save-yes").css('float', 'right');
                                    $("#save-no").css('float', 'right');
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-fieldset",
                                click: function () {
                                    var param 	        = new Object();
                                    param.act		    = "save_fieldset";
                                    param.fieldset_id   = $("#fieldset_id").val();
                                    param.field_name    = $("#field_name").val();
                                    param.setting_id    = $("#setting_id").val();
                                    param.field_key     = geo_to_latin($("#field_name").val())
                                    $.ajax({
                                        url: aJaxURL,
                                        type: "POST",
                                        data: param,
                                        dataType: "json",
                                        success: function (data) {
                                            
                                            if(data.response == 'suc'){
                                                alert('ინფორმაცია წარმატებით განახლდა!!!')
                                                $("#processing_page").data("kendoGrid").dataSource.read();
                                                $("#fieldsets_table").data("kendoGrid").dataSource.read();

                                                $("#fieldsets_editor").dialog("close");
                                            }
                                            else{
                                                alert('დაფიქსირდა შეცდომა!');
                                            }
                                        }
                                    }); 
                                }
                            }

                        };
            GetDialog("fieldsets_editor", '1300', "auto",buttons,'center top');
        }
        else if(fNm=='input_editor'){
            var buttons =
                        {
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    var buttons1 = {
                                        "done": {
                                            text: "კი",
                                            id: "save-yes",
                                            click: function () {
                                                $("#input_editor").dialog("close");
                                                $(this).dialog("close");
                                            }
                                        },
                                        "no": {
                                            text: "არა",
                                            id: "save-no",
                                            click: function () {
                                                $(this).dialog("close");
                                            }
                                        }
                                    };
                                    GetDialog("hint_close", 300, "auto", buttons1);
                                    $("#save-yes").css('float', 'right');
                                    $("#save-no").css('float', 'right');
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-input",
                                click: function () {
                                    var input_name  = $("#input_name").val();
                                    var input_type  = $("#input_type").val();
                                    var input_pos   = $("#input_position").val();
                                    var deep        = $("#multilevel_deep").val();
                                    var nec         = $("#necessary").prop("checked");

                                    var itsOK = 0;
                                    if(input_name == '' && input_type != 11 && input_type != 12){
                                        $("#input_name").css('border','1px solid red');
                                        itsOK++;
                                    }
                                    if(itsOK == 0){
                                        var param 	            = new Object();
                                        param.act		        = "save_input";
                                        param.input_id          = $("#input_id").val();
                                        param.input_name        = $("#input_name").val();
                                        param.input_type        = $("#input_type").val();
                                        param.input_pos         = $("#input_position").val();
                                        param.field_id          = $("#field_id").val();
                                        param.key               = $("#input_key").val();
                                        param.multilevel_deep   = $("#multilevel_deep").val();
                                        param.nec               = $("#necessary").prop("checked");
                                        param.input_tab         = $("#input_tab").val();

                                        param.name_1      = $("#level_name_1").val();
                                        param.name_2      = $("#level_name_2").val();
                                        param.name_3      = $("#level_name_3").val();

                                        $.ajax({
                                            url: aJaxURL,
                                            type: "POST",
                                            data: param,
                                            dataType: "json",
                                            success: function (data) {
                                                
                                                if(data.response == 'suc'){
                                                    alert('ველი წარმატებით შენახულია!');
                                                    $("#fieldset_inputs").data("kendoGrid").dataSource.read();
                                                    $("#fieldsets_table").data("kendoGrid").dataSource.read();
                                                    $("#input_editor").dialog("close");
                                                    $.ajax({
                                                        url: aJaxURL,
                                                        data: {
                                                            'act'		: 'get_demo_view',
                                                            'fieldset_id': $("#field_id").val()
                                                        },
                                                        success: function(data) {
                                                            $("#demo_view").empty();
                                                            $("#demo_view").html(data.page);
                                                            $.ajax({
                                                                url: aJaxURL,
                                                                data: {
                                                                    'act'		: 'get_fieldsets',
                                                                    'fieldset_id': $("#field_id").val()
                                                                },
                                                                success: function(data) {
                                                                    $(data.chosen_keys).chosen({ search_contains: true });
                                                                }
                                                            });
                                                        }
                                                    });
                                                    

                                                    
                                                }
                                                else if(data.response == 'not_uniq'){
                                                    alert("ველი მსგავსი სახელით უკვე არსებობს!!!")
                                                }
                                                else{
                                                    alert('დაფიქსირდა შეცდომა!');
                                                }
                                            }
                                        });
                                    }
                                    
                                }
                            }

                        };
            GetDialog("input_editor", '500', "auto",buttons,'center top');

            //$("#input_type,#input_tab").chosen();
        }
        else if(fNm=='add_selector'){
            var buttons =
                        {
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-selector",
                                click: function () {
                                    var param 	        = new Object();
                                    param.act		    = "save_selector";
                                    param.input_id      = $("#input_id").val();
                                    param.selector_id   = $("#selector_id").val();
                                    param.field_id      = $("#field_id").val();
                                    param.param_name    = $("#param_name").val();
                                    $.ajax({
                                        url: aJaxURL,
                                        type: "POST",
                                        data: param,
                                        dataType: "json",
                                        success: function (data) {
                                            
                                            if(data.response == 'suc'){
                                                alert('ველი წარმატებით შენახულია!');
                                                if(data.input_id == '')
                                                {
                                                    $("#selectors_type_kendo").data("kendoGrid").dataSource.read();
                                                }
                                                else{
                                                    var input = "&input="+data.input_id;
                                                    LoadKendo_selectors(input);

                                                    $("#input_id").val(data.input_id);
                                                }
                                                
                                                $("#add_selector").dialog("close");
                                                
                                            }
                                            else{
                                                alert('დაფიქსირდა შეცდომა!');
                                            }
                                        }
                                    }); 
                                }
                            }

                        };
            GetDialog("add_selector", '350', "auto",buttons,'center top');
        }
        else if(fNm=='multilevel_editor'){
            var buttons =
                {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $("#multilevel_editor").dialog("close");
                        }
                    },
                    "done": {
                        text: "შენახვა",
                        id: "save-multilevel"
                    }

                };
            GetDialog(fNm, 600, "auto", buttons, 'center top');
            $('#parent_id,#client_id').chosen({ search_contains: true });
            $('#add-edit-form, .add-edit-form-class').css('overflow','visible');
        }
        else if(fNm=='add_tab_page'){
            var buttons =
                {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function () {
                            $("#add_tab_page").dialog("close");
                        }
                    },
                    "done": {
                        text: "შენახვა",
                        id: "save-tab-input",
                        click: function () {
                            var tab_name = $("#tab_name").val();

                            if(tab_name == ''){
                                $("#tab_name").css('border','1px solid red');
                            }
                            else{
                                $("#tab_name").css('border','1px solid #000');
                                var param 	        = new Object();
                                    param.act		    = "save_input_tab";
                                    param.tab_name      = tab_name;
                                    param.field_id      = $('#field_id').val();
                                    $.ajax({
                                        url: aJaxURL,
                                        type: "POST",
                                        data: param,
                                        dataType: "json",
                                        success: function (data) {
                                            if(data.err == 'exist_name'){
                                                alert('ტაბი ამ სახელით უკვე არსებობს');
                                            }
                                            else if(data.response == 'suc'){
                                                var param 	        = new Object();
                                                    param.act		    = "get_input_tabs";
                                                    param.field_id      = $('#field_id').val();
                                                    $.ajax({
                                                        url: aJaxURL,
                                                        type: "POST",
                                                        data: param,
                                                        dataType: "json",
                                                        success: function (data) {
                                                            $("#input_tab").html(data.page);
                                                            $("#input_tab").trigger("chosen:updated");
                                                            $("#add_tab_page").dialog("close");
                                                        }
                                                    });
                                            }
                                        }
                                    }); 
                            }
                        }
                    }

                };
            GetDialog(fNm, 600, "auto", buttons, 'center top');
        }
    }
    $(document).on('change','#input_tab',function(){
        var tab_id      = $("#input_tab").val();
        var field_id    = $("#field_id").val();
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=get_position_increment_by_tab&tab_id="+tab_id+"&field_id="+field_id,
            dataType: "json",
            success: function (data) {
                $("#input_position").val(data.position);
            }
        });
        
    });
    $(document).on('click','.button_add_input', function(){
        var field_id = $("#fieldset_id").val();
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=input_editor&fieldset_id="+field_id,
            dataType: "json",
            success: function (data) {
                $("#input_editor").html(data.page);
                LoadKendo_selectors();
                LoadDialog('input_editor');
                GetButtons("add_button","delete_button");
                $("[data-button='jquery-ui-button']").button();

                $.ajax({
                    url: aJaxURL,
                    data: 'act=get_multilevel_list&field_id='+field_id,
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                $('#tree1').html(data.page);
                                $('#tree1').treed();
                                // $("#tree1 li").click();
                            }
                        }
                    }
                });

            }
        });
    });
    $(document).on('click','.button_delete_setting_by_page',function(){
        var grid = $("#processing_page").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        //alert(dItem.id);
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=disable&type=setting&id="+dItem.id,
            dataType: "json",
            success: function (data) {
                $("#processing_page").data("kendoGrid").dataSource.read();
            }
        });
    });
    $(document).on('click','.button_add_selector', function(){
        var input_id = $("#input_id").val();
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_selector&input_id="+input_id,
            dataType: "json",
            success: function (data) {
                $("#add_selector").html(data.page);
                
                LoadDialog('add_selector');
            }
        });
    });
    $(document).on('click','.button_add_fieldset', function(){
        var input_id = $("#input_id").val();
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=fieldset_editor",
            dataType: "json",
            success: function (data) {
                $("#fieldsets_editor").html(data.page);
                
                LoadDialog('fieldsets_editor');
            }
        });
    });


    /* TREE AREA */
    $.fn.extend({
        treed: function (o) {

            var openedClass = 'glyphicon-minus-sign';
            var closedClass = 'glyphicon-plus-sign';

            if (typeof o != 'undefined'){
                if (typeof o.openedClass != 'undefined'){
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined'){
                    closedClass = o.closedClass;
                }
            };

            //initialize each of the top levels
            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function () {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                branch.addClass('branch');
                branch.on('click', function (e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " + closedClass);
                        $(this).children().children().toggle();
                    }
                })
                branch.children().children().toggle();
            });
            //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function(){
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
            //fire event to open branch if the li contains an anchor instead of text
            tree.find('.branch>a').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
            //fire event to open branch if the li contains a button instead of text
            tree.find('.branch>button').each(function () {
                $(this).on('click', function (e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });
    $(document).on("click", ".toggle_all_categories", function() {

        //define variables
        let action = $(this).data("action");

        //toggle category list items
        if(action === "open") {
            $(".branch").find("li").css({display: "list-item"});
            $(".branch > i").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
        } else if(action === "close") {
            $(".branch").find("li").css({display: "none"});
            $(".branch > i").removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
        }

    });
    $(document).on("click", "#show_sub_category_2_value", function () {

    // define variables for getting main category
    // index and retrieve sub category list
    var categoryId = $("#category_select").val();
    var self = this;

    getSubCategoryList(categoryId, function(subCategoryList) {

        // insert subcategory list in relevant select
        $('#sub_category_1_select').html(subCategoryList);

        //define main category value
        var subCategory = $("#sub_category_1_value").val();

        // if main category in not selected, that means
        // it is new category adding proccess, but if
        // main category input has some value, it means
        // going on editing proccess
        // get main category id
        let subCatValue = subCategory !== "" ? $("#sub_category_1_value").val() : false;
        let subCatIndex = subCatValue ? getOptionIndexByVal('#sub_category_1_select', subCatValue) : 0;

        if ($(self).is(':checked')) {

            $('#show_sub_category_1_value').prop("disabled",true);
            $('#sub_category_1_value').css('display','none');
            $('#sub_category_1_value').val('');
            $('#sub_category_1_select,#second_tr').css('display','');
            $('#sub_category_2_value').css({display: "block"});
            $("#cat_id").val("");
            document.getElementById('sub_category_1_select').selectedIndex = subCatIndex;

        } else {
            $('#show_sub_category_1_value').prop("disabled",false);
            $('#sub_category_1_value').css('display','');
            $('#sub_category_1_select,#second_tr').css('display','none');
            document.getElementById('sub_category_1_select').selectedIndex = 0;
            $('#sub_category_2_value').val('');
        }

    });

    });

    function getOptionIndexByVal(select, value) {

        var returnValue = 0;

        $(select).find("option").each(function(i) {

            var optionValue = $(this).text();

            if(value === optionValue) {
                returnValue = i;
            }

        });

        return returnValue;

    }
    function getSubCategoryList(categoryid, callback) {
        $.post(aJaxURL, {act: "get_sub_cat", category_id: categoryid}, result => {
            callback(result.page);
        });
    }

    $(document).on("change", "#category_select", function () {
        if($(this).val() == 0){
            $('#show_sub_category_2_value').prop("disabled",true);
        }else{
            $('#show_sub_category_2_value').prop("disabled",false);
            if($("#multilevel_deep").val() == 2){
                $('#show_sub_category_2_value').prop("disabled",true);
            }
        }
        $.ajax({
            url: aJaxURL,
            data: 'act=get_sub_cat&category_id='+$(this).val(),
            success: function(data) {
                if(typeof(data.error) != 'undefined'){
                    if(data.error != ''){
                        alert(data.error);
                    }else{
                        $('#sub_category_1_select').html(data.page);
                    }
                }
            }
        });
    });
    $(document).on("click", "#show_sub_category_1_value", function () {

        //define main category value
        var mainCategory = $("#category_value").val();

        // if main category in not selected, that means
        // it is new category adding proccess, but if
        // main category input has some value, it means
        // going on editing proccess
        // get main category id
        let mainCatValue = mainCategory !== "" ? $("#category_value").val() : false;
        let mainCatIndex = mainCatValue ? getOptionIndexByVal('#category_select', mainCatValue) : 0;

        if ($(this).is(':checked')) {
            $('#show_sub_category_2_value').prop("disabled",true);
            $('#category_value').css('display','none').val('');
            $('#category_select,#first_tr').css('display','');
            $('#sub_category_1_value').css({display: "block"});
            $('#sub_category_1_select').val(0);
            $("#cat_id").val("");
            document.getElementById('category_select').selectedIndex = mainCatIndex;

            if(!mainCatIndex) {
                $('#sub_category_1_select').css({display: "none"});
            } else {
                $('#show_sub_category_2_value').prop("disabled",false);
            }

        } else {
            $('#category_value').css('display','');
            $('#category_select,#first_tr').css('display','none');
            $("#sub_category_1_value").css({display: "none"});
            $("#sub_category_1_select").css({display: "none"});
            document.getElementById('category_select').selectedIndex = 0;
        }

    });
    $(document).on("click", "#add_button", function () {
        $.ajax({
            url: aJaxURL,
            data: 'act=add_multilevel&field_id='+$("#field_id").val()+"&input_id="+$("#input_id").val(),
            success: function(data) {
                if(typeof(data.error) != 'undefined'){
                    if(data.error != ''){
                        alert(data.error);
                    }else{
                        LoadDialog("multilevel_editor");
                        $('#multilevel_editor').html(data.page);
                    }
                }
            }
        });

    });
    $(document).on("click", ".cat_edit", function () {
            $.ajax({
                url: aJaxURL,
                data: 'act=multilevel_edit&id='+$(this).attr('my_id')+"&input_id="+$("#input_id").val(),
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            LoadDialog("multilevel_editor");
                            $('#multilevel_editor').html(data.page);
                            if($("#multilevel_deep").val() == 2){
                                $("#show_sub_category_2_value").prop('disabled',true);
                            }
                            
                        }
                    }
                }
            });
        });
    $(document).on("click", "#save-multilevel", function () {

        var subCatecoryIsChecked = $("#show_sub_category_1_value").is(":checked");
        var endCatecoryIsChecked = $("#show_sub_category_2_value").is(":checked");

        param 			           = new Object();

        param.act		           = "save_category";
        param.id		           = $("#cat_id").val();
        param.category_value	   = $("#category_value").val();
        param.category_select	   = $("#category_select").val();
        param.sub_category_1_value = $('#sub_category_1_value').val();
        param.sub_category_1_select= $('#sub_category_1_select').val();
        param.sub_category_2_value = $('#sub_category_2_value').val();

        param.input_id = $('#input_id').val();
        param.field_id = $('#field_id').val();

        if(!subCatecoryIsChecked && (param.category_value === "" && param.category_select === "0")) {
            alert("გთხოვთ დააფიქსიროთ კატეგორია");
        } else if(subCatecoryIsChecked && (param.category_value === "" && param.category_select === "0") && (param.sub_category_1_value === "" && param.sub_category_1_select ==="0")) {
            alert("გთხოვთ დააფიქსიროთ ქვე კატეგორია 1");
        } else if(endCatecoryIsChecked && (param.sub_category_2_value === "")) {
            alert("გთხოვთ დააფიქსიროთ ქვე კატეგორია 2");
        } else {

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    if(typeof(data.error) != 'undefined'){
                        if(data.error != ''){
                            alert(data.error);
                        }else{
                            if(data.new_input_id){
                                $("#input_id").val(data.new_input_id);
                            }
                            
                            $.ajax({
                                url: aJaxURL,
                                data: 'act=get_multilevel_list&input_id='+$('#input_id').val()+'&field_id='+$('#field_id').val(),
                                success: function(data) {
                                    if(typeof(data.error) != 'undefined'){
                                        if(data.error != ''){
                                            alert(data.error);
                                        }else{
                                            $('#tree1').html(data.page);
                                            $('#tree1').treed();
                                            $("#fieldset_inputs").data("kendoGrid").dataSource.read();
                                            $("#fieldsets_table").data("kendoGrid").dataSource.read();
                                        }
                                    }
                                }
                            });
                            CloseDialog("multilevel_editor");
                        }
                    }
                }
            });

        }

    });
    $(document).on("click", ".cat_delete", function () {
        var objId = $(this).attr("my_id");
        var buttons2 = {
            "save": {
                text: "კი",
                id: "confirm-delete-category",
                click: function () {

                    $('#ask-delete-category').dialog("close");
                    deleteCategory(objId);

                }
            },"reject-delete-category": {
                text: "არა",
                id: "no-cc",
                click: function () {
                    $('#ask-delete-category').dialog("close");
                }
            }
        };

        GetDialog("ask-delete-category","300","auto",buttons2);

    });
    function deleteCategory(objid) {

        $.ajax({
            url: aJaxURL,
            data: 'act=disable&id='+objid+'&input_id='+$("#input_id").val(),
            success: function(data) {
                if(typeof(data.error) != 'undefined'){
                    if(data.error != ''){
                        alert(data.error);
                    }else{
                        $.ajax({
                            url: aJaxURL,
                            data: 'act=get_multilevel_list&input_id='+$("#input_id").val()+'&field_id='+$("#fieldset_id").val(),
                            success: function(data) {
                                if(typeof(data.error) != 'undefined'){
                                    if(data.error != ''){
                                        alert(data.error);
                                    }else{
                                        $('#tree1').html(data.page);
                                        $('#tree1').treed();
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

    }
</script>
<style type="text/css">
.tree, .tree ul {
            margin:0;
            padding:0;
            list-style:none
        }
        .tree ul {
            margin-left:1em;
            position:relative
        }
        .tree ul ul {
            margin-left:.5em
        }
        .tree ul:before {
            content:"";
            display:block;
            width:0;
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            border-left:1px solid
        }
        .tree li {
            margin:0;
            padding:0 1em;
            line-height:19px;
            color:#369;
            font-weight:700;
            font-size:13px;
            position:relative;
            float: none!important;
        }
        .tree ul li:before {
            content:"";
            display:block;
            width:10px;
            height:0;
            border-top:1px solid;
            margin-top:-1px;
            position:absolute;
            top:1em;
            left:0
        }
        .tree ul li:last-child:before {
            background:#fff;
            height:auto;
            top:1em;
            bottom:0
        }
        .indicator {
            margin-right:5px;
        }
        .tree li a {
            text-decoration: none;
            color:#369;
        }
        .tree li button, .tree li button:active, .tree li button:focus {
            text-decoration: none;
            color:#369;
            border:none;
            background:transparent;
            margin:0px 0px 0px 0px;
            padding:0px 0px 0px 0px;
            outline: 0;
        }
        .cat_img{
            width: 12px;
            margin-right: 7px;
            cursor: pointer;
            border: none!important;
            padding: unset!important;
        }
        .cat_img:hover {
            color: red;
        }
        i{
            cursor: pointer;
        }

     

        /*ask delete category text styles*/
        .ask-delete-text {
            font-size: 12px;
        }
        .callapp_tabs{
            margin-top: 5px;
            /* margin-bottom: 5px; */
            float: right;
            width: 100%;

        }
        .callapp_tabs span{
            color: #FFF;
            border-radius: 5px;
            padding: 5px;
            float: left;
            margin: 0 3px 0 3px;
            background: #2681DC;
            font-weight: bold;
            font-size: 11px;
            margin-bottom: 2px;
        }

        .callapp_tabs span close{
            cursor: pointer;
            margin-left: 5px;
        }

        .callapp_head{
            font-family: pvn;
            font-weight: bold;
            font-size: 20px;
            color: #2681DC;
        }
        .callapp_head select {
            position: relative;
            top: -2px;
        }

        #chatcontent hr {
            border-top-width: 0px;
        }

        .callapp_head_hr{
            border: 1px solid #2681DC;
        }
        .callapp_refresh{
            padding: 5px;
            border-radius:8px;
            color:#FFF;
            background: #9AAF24;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }
        .comunication_button{
            padding: 5px;
            border-radius:8px;
            float: right;
            font-size: 13px;
            cursor: pointer;
        }
        .callapp_dnd_button {
            width: 40px; height: 40px;
            background: none;
            border: none;
            cursor: pointer;
            margin: 0 5px;
            position: relative;
            color: #7f8c8d;
            font-size: 21px;
        }
        
        .callapp_filter_body{
            width: 1030px;
            height: 187px;
            padding: 8px 0px 0px 0px;
            margin-bottom: 0px;
        }
        .callapp_filter_body span {
            float: left;
            margin-right: 10px;
            height: 22px;
        }
        .callapp_filter_body span label {
            color: #555;
            font-weight: bold;
            /* margin-left: 20px; */
        }
     
        .callapp_filter_header{
            color: #2681DC;
            font-family: pvn;
            font-weight: bold;
        }

        .ColVis, .dataTable_buttons{
            z-index: 50;
        }
        /* #flesh_panel{
            height: auto;
            width: 300px;
            position: absolute;
            top: 0;
            padding: 15px;
            right: 2px;
            z-index: 49;
            background: #FFF;
        } */
       
       
        #table_right_menu{
            top: 39px;
            z-index: 1;
        }
      
        /* #table_index_wrapper .ui-widget-header{
            height: 55px;
        } */

        /* -SMS DIALOG STYLES- */
        .new-sms-row {
            width: 100%; height: auto;
            margin-top: 11.3px;
            box-sizing: border-box;
        }

        .new-sms-row:last-child {
            margin-top: 4px;
        }

        .new-sms-row.grid {
            display: grid;
            grid-template-columns: repeat(2, 50%);
        }

        .nsrg-col {
            padding-right: 10px;
        }

        .nsrg-col:last-child {
            padding-right: 0;
        }

        .new-sms-input-holder {
            width: 100%; min-height: 27px;
            display: flex;
        }

        .new-sms-input-holder input {
            height: 18px;
            flex-grow: 1;
            font-size: 11.3px;
        }

        .new-sms-input-holder textarea {
            height: 75%;
            min-height: 20px;
            flex-grow: 1;
            font-size: 11.3px;
            resize: vertical;
        }

        #smsCopyNum { /*button: copy*/
            margin: 0 5px;
        }

        #smsTemplate { /*button: open template*/
            margin-right: 0;
        }

        #smsNewNum i{ /*icon: add new number in send new sms child*/
            font-size: 14px;
        }

        #smsCharCounter { /*input: sms character counter*/
            width: 55px; height: 18px;
        }

        #smsCharCounter { /*input: send new sms character counter*/
            position: relative;
            top: 0;
            text-align: center;
        }

        #sendNewSms { /*button: send new message action inplement*/
            float: right;
        }

        .empty-sms-shablon {
            width: 100%; height: auto;
            padding: 10px 0;
            text-align: center;
            font-family: pvn;
            font-weight: bold;
        }

        #box-table-b1{
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            text-align: center;
            border-collapse: collapse;
            border-top: 7px solid #70A9D2;
            border-bottom: 7px solid #70A9D2;
            width: 300px;
        }

        #box-table-b th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #start_chat_wrap{
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
            background: #e6f6ec;
            opacity: 0.8;
            display: none;
        }
        #start_chat{
            margin-top: 60px;
            margin-left: 84px;
            padding: 0px 9px;
            border: none;
            border-radius: 7px;
            background: #009658;
            color: #FFF;
            cursor: pointer;
            position: absolute;
        }
        #start_chat:hover{
            background: #01c26e;
        }

        #box-table-b1 th {
            font-size: 13px;
            font-weight: normal;
            padding: 8px;
            background: #E8F3FC;;
            border-right: 1px solid #9baff1;
            border-left: 1px solid #9baff1;
            color: #4496D5;
        }

        #box-table-b td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        #box-table-b1 td {
            padding: 8px;
            background: #e8edff;
            border-right: 1px solid #aabcfe;
            border-left: 1px solid #aabcfe;
            color: #669;
        }

        .download_shablon {

            width: 100%;
            height: 20px;
            background-color:#4997ab;
            border-radius:2px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:14px;
            border:0px;
            text-decoration:none;
            text-overflow: ellipsis;
        }
        #comucation_status{
            position: absolute;
            z-index: 25000000;
            background: #fff;
            width: 199px;
            display: block;
            box-shadow: 0px 0px 25px #888888;
            left:-55%;
        }

        .download_shablon:hover {
            background-color:#ffffff;
            color:#4997ab;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .2);
        }

        .ui-dialog-titlebar-close {
            display: none;
        }

        .imagespan{
            padding: 0px 0px 0px 10px;
        }

        .comunicationTr{
            border-top: 0.5px #c1cbd0 solid;
        }

        .comunication_name{
            font-family: pvn;
            font-weight: bold;
            font-size: 13px;
            color: #2681DC;
        }

        
        .download10{
        	background-color:#7d3daf;
        	border-radius:0px;
        	display:inline-block;
        	cursor:pointer;
        	color:#ffffff !important;
        	font-size:12px;
        	border:0px !important;
        	text-decoration:none;
        	text-overflow: ellipsis;
        	width: 100%;
        	font-weight: normal !important;
        }
        th [role="listbox"]{
            display: none;
        }
        .status_tabs{
            display:-webkit-inline-box;
            text-align: center;
        }
        .status_tabs li{
            padding: 10px;
            margin-right:10px;
            border-radius: 15px;
            cursor: pointer;
        }
        .status_tabs li[aria-selected="true"]{
            background-color:yellow;
            padding: 10px;
            margin-right:10px;
            border-radius: 15px;
            cursor: pointer;
        }

        #kendo_project_info_table tr th:nth-child(1),
        #kendo_project_info_table tr td:nth-child(1){
            /* display: none; */
        }

        #kendo_project_info_table tr th:nth-child(2), 
        #kendo_project_info_table tr td:nth-child(2), 
        #kendo_project_info_table tr th:nth-child(3),
        #kendo_project_info_table tr td:nth-child(3){
            text-align: center;
            width: 100px;
        }

        #kendo_project_info_table tr th:nth-child(3){
            text-align: center;
        }

        #kendo_project_info_table tr td:nth-child(4){
            white-space: nowrap;
            
        }
        .chosen-container{
            width:194px;
        }
</style>
</head>

<body>
<div id="tabs"  >
    

    <div class="callapp_filter_show" style="">
        <ul class="status_tabs" id="status_tabs"></ul>


        
        <div class="clear"></div>
        <div style="position:relative;">
            
            <div id="processing_page"></div>
        </div>
    </div>
</div>

<div id="get_fieldsets" class="form-dialog" title="Fieldsets"></div>
<div id="fieldsets_editor" class="form-dialog" title="Fieldsets რედაქტირება"></div>
<div id="input_editor" class="form-dialog" title="ველები"></div>
<div id="add_selector" class="form-dialog" title="სელექტორი/CHECKBOX/RADIO პარამეტრი"></div>
<div id="add_tab_page" class="form-dialog" title="ტაბის დამატება">ადსდ</div>

<div id="multilevel_editor" class="form-dialog" title="მულტიდონიანი სელექტორი"></div>
<div id="ask-delete-category" class="form-dialog" title="&nbsp;">
    <span class="ask-delete-text">დარწმუნებული ბრძანდებით რომ გსურთ ამ კატეგორიის წაშლა?</span>
</div>
</body>
</html>


