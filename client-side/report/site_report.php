<html>
<head>
<style type="text/css">
#in_form,.in_form-class{
	overflow: visible;
}
.green
	{
	background-color: rgba(255, 0, 29, 0.51) !important;
	color: #0D233A !important;
	font-size: 14px !important;
		
	}
    .ui-dropdownchecklist-selector{
		background: #FFF;
		padding: 9px 0;
	}
	.ui-dropdownchecklist-text{
		display: inline;
	}
	.ui-dropdownchecklist-item{
		background: #FFF !important;
	}
    #report td:nth-child(3){
	   text-align:right !important;
    }
    #report td:nth-child(4){
	   text-align:right !important;
    }
.download {
	background-color:#2dc100;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download1 {
	background-color:#ce8a14;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:14px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download2 {
	background-color:#d35454;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download4 {
	background-color:#2dc100;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	text-decoration:none;
	border:0px !important;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download3 {
	background-color:#ce8a14;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download5 {
	background-color:#4980AF;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download6 {
	background-color:#CE5F14;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 98%;
	font-weight: normal !important;
}
.download7{
	background-color:#f80aea;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download8{
	background-color:#f80aea;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.download9{
	background-color:#D1EC38;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download10{
	background-color:#7d3daf;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download11{
	background-color:#2196f3;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download12{
	background-color:#48a7f3;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download13{
	background-color:#030202;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download14{
	background-color:#089b98;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}

.download15{
	background-color:#e81d42;
	border-radius:0px;
	display:inline-block;
	cursor:pointer;
	color:#ffffff !important;
	font-size:12px;
	border:0px !important;
	text-decoration:none;
	text-overflow: ellipsis;
	width: 100%;
	font-weight: normal !important;
}
.search_button {
    padding: 2px;
    border-radius: 0px;
    background: #009688;
    cursor: pointer;
	float: left;
}
</style>
<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/exporting.js"></script>
<script type="text/javascript">
var title 	= '0';
var i 		= 0;
var done 	= ['','','','','','',''];
var aJaxURL	= "server-side/report/site_report.action.php";
var aJaxURL_inc	= "server-side/report/operator_call_chat_out.action.php";//server side folder url
var tName   = "report";
var start	= $("#search_start").val();
var end		= $("#search_end").val();
$(document).ready(function() {
	$('#show_filter').button();
	GetDate("search_start");
	GetDate("search_end");
	$("#dropdown2").dropdownchecklist({ firstItemChecksAll: true, explicitClose: 'დახურვა',icon: {}, width: 200, zIndex: 111});
	drawFirstLevel();
	$("#back").button({ disabled: true });
	$("#back").button({ icons: { primary: "ui-icon-arrowthick-1-w" }});
    $('#back').click(function(){
	    i--;
	    drawFirstLevel();
    	if(i==3)$("#back").button({ disabled: true });
    });
});

$(document).on("click", "#show_filter", function () 	{drawFirstLevel();});

function drawFirstLevel(){
		 var options = {
	                chart: {
	                    renderTo: 'chart_container',
	                    plotBackgroundColor: null,
	                    plotBorderWidth: null,
	                    plotShadow: false
	                },
	                title: {
	                    text: title
	                },
	                tooltip: {
	                    formatter: function() {
	                        return '<b>'+ this.point.name +'-'+this.point.y+' მომართვა :  '+this.percentage.toFixed(2) +' %</b>';

	                    }
	                },
	                plotOptions: {
	                	pie: {
	                        allowPointSelect: true,
	                        cursor: 'pointer',
	                        dataLabels: {
	                            enabled: true,
	                            color: '#000000',
	                            connectorColor: '#000000',
	                            formatter: function() {
	                            	return '<b>'+ this.point.name +'-'+this.point.y+' მომართვა :  '+this.percentage.toFixed(2) +' %</b>';
	                            }
	                        },
	                        point: {
	                            events: {
	                                click: function() {
	                                	$("#back").button({ disabled: false });
		                        		done[i]=this.name;
		                        		if(i==3) i=0;
		                        		else i++;
		                        		drawFirstLevel();
	                                }
	                            }
	                        }
	                    }
	                },
	                series: [{
	                    type: 'pie',
	                    name: 'კატეგორიები',
	                    data: []
	                }]
	            }
		var start	    = $("#search_start").val();
		var end		    = $("#search_end").val();
		var source_info = $("#dropdown2").val();
		var d_url       = "&start="+start+"&end="+end+"&done="+i+"&departament="+done[0]+"&type="+done[1]+"&category="+done[2]+"&sub_category="+done[3]+"&source_info="+source_info;
		var url         = aJaxURL+"?act=get_category"+d_url;
		GetDataTable(tName, aJaxURL, "get_list", 4, d_url, 0, "",'','',[2,3], "<'dataTable_buttons'T><'F'Cfipl>");
		$("#report tbody").on("click", "tr", function () {
            if(i==3){
                d_url1   ="&start="+start+"&end="+end+"&done="+i+"&type="+done[0]+"&category="+done[1]+"&sub_category="+done[2]+"&source_info="+source_info;
                var nTds = $("td", this);
                var rID = $(nTds[1]).text();
                GetDataTable("report_1", aJaxURL, "get_in_page", 12, d_url1+"&rid="+rID, 0, "", 1, "asc", '', "<'dataTable_buttons'T><'F'Cfipl>");
                GetDialog("in_form", "90%", "auto","","left+43 top");
                SetEvents("", "", "", "report_1", "add-edit-form", aJaxURL_inc);
                $('.ui-dialog-buttonset').hide();
            }
		    setTimeout(function(){
	    		$('#in_form  .ColVis,#in_form .dataTable_buttons').css('display','none');
	    		$("#show_copy_prit_exel1").css('background','#E6F2F8');
	            $("#show_copy_prit_exel1").children('img').attr('src','media/images/icons/select.png');
	            $("#show_copy_prit_exel1").attr('myvar','0');
	    	}, 190);
	     });
        $.getJSON(url, function(json) {
                    options.series[0].data = json.data;
                    options.title['text']=json.text;
                    chart = new Highcharts.Chart(options);
                    $("#total_quantity").html("იტვირთება....")
                    setTimeout(function(){ $("#total_quantity").html($("#qnt").html().split(">")[1]);}, 500);
                    if(i==3) $("#report td").addClass("green");
            		else     $(".green").removeClass("green");
        });

        setTimeout(function(){
    		$('.ColVis, .dataTable_buttons').css('display','none');
    		$("#show_copy_prit_exel").css('background','#E6F2F8');
            $("#show_copy_prit_exel").children('img').attr('src','media/images/icons/select.png');
            $("#show_copy_prit_exel").attr('myvar','0');
    	}, 190);
}

$(document).on("click", ".download", function () {
	var str = 1;
	var link = ($(this).attr("str"));
	link = 'http://pbx.my.ge/records/' + link;
	var btn = {
	        "cancel": {
	            text: "დახურვა",
	            id: "cancel-dialog",
	            click: function () {
	                $(this).dialog("close");
	            }
	        }
	    };
	GetDialog_audio("audio_dialog", "auto", "auto",btn );
	$("#audio_dialog").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
    $(".download").css( "background","#408c99" );
    $(this).css( "background","#FF5555" );
   
});
function LoadDialog(fname){
	 var button_modal = {

         "cancel": {
             text: "დახურვა",
             id: "cancel-dialog",
             click: function () {
                 $(this).dialog("close");
             }
         }
     }
	var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
    GetDialog(fname, 1050, "auto", button_modal, "center top");
    $("#search_ab_pin").button();
    var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
    GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");

    $("#table_history_length").css('top','0px');
    $("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #my_site, #source_id, #inc_status, #s_u_status, #client_sex").chosen({ search_contains: true });


};

$(document).on("click", "#search_ab_pin", function () {
	var dLength = [[5, 10, 30, -1], [5, 10, 30, "ყველა"]];
    GetDataTable("table_history", aJaxURL, "get_list_history", 8, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val()+"&s_u_user_id="+$("#s_u_user_id").val(), 0, dLength, 2, "desc", '', "<'F'lip>");

    $("#table_history_length").css('top','0px');
});

function show_right_side(id){
    $("#right_side fieldset").hide();
    $("#" + id).show();
    //$(".add-edit-form-class").css("width", "1260");
    //$('#add-edit-form').dialog({ position: 'left top' });
    hide_right_side();

    var str = $("."+id).children('img').attr('src');
	str = str.substring(0, str.length - 4);
	$("#side_menu span").children('img').css('border-bottom','2px solid transparent');
    $("."+id).children('img').css('filter','brightness(0.1)');
    $("."+id).children('img').css('border-bottom','2px solid #333');
}

function hide_right_side(){
	$(".info").children('img').css('filter','brightness(1.1)');
	$(".record").children('img').css('filter','brightness(1.1)');
    $(".call_resume").children('img').css('filter','brightness(1.1)');
    $("#record fieldset").show();
}

function listen(file){
	$('#auau').each(function(){
	    this.pause(); // Stop playing
	    this.currentTime = 0; // Reset time
	});
    var url = 'http://pbx.my.ge/records/' + file;
    $("#auau source").attr('src',url);
    $("#auau").load();
}
$(document).on("click", "#show_copy_prit_exel", function () {
    
    if($(this).attr('myvar') == 0){
        $('.ColVis,.dataTable_buttons,#table_right_menu_content').css('display','block');
        $(this).css('background','#2681DC');
        $(this).children('img').attr('src','media/images/icons/select_w.png');
        $(this).attr('myvar','1');
    }else{
    	$('.ColVis,.dataTable_buttons,#table_right_menu_content').css('display','none');
    	$(this).css('background','#E6F2F8');
        $(this).children('img').attr('src','media/images/icons/select.png');
        $(this).attr('myvar','0');
    }
});
$(document).on("click", "#show_copy_prit_exel1", function () {
    
    if($(this).attr('myvar') == 0){
        $('#in_form .ColVis,#in_form .dataTable_buttons,#in_form #table_right_menu_content').css('display','block');
        $(this).css('background','#2681DC');
        $(this).children('img').attr('src','media/images/icons/select_w.png');
        $(this).attr('myvar','1');
    }else{
    	$('#in_form .ColVis,#in_form .dataTable_buttons,#in_form #table_right_menu_content').css('display','none');
    	$(this).css('background','#E6F2F8');
        $(this).children('img').attr('src','media/images/icons/select.png');
        $(this).attr('myvar','0');
    }
});

</script>
</head>
<body>
	<div id="tabs" style="width: 90%; height: 850px;">
		<div class="callapp_head">საიტების მიხედვით<hr class="callapp_head_hr"></div>
         <div id="button_area" style="margin: 3% 0 0 0">
         <button id="back" style="margin-top:0px">უკან</button>
		</div>
       <div id="button_area" style="margin: 3% 0 0 0">
         <div class="left" style="width: 175px;">
           <input style="height: 15px;" type="text" name="search_start" id="search_start"  class="inpt right"/>
             </div>
            	<label for="search_start" class="left" style="margin:5px 0 0 3px">-დან</label>
             <div class="left" style="width: 185px;">
	            <input style="height: 15px;" type="text" name="search_end" id="search_end"  class="inpt right" />
             </div>
            	<label for="search_end" class="left" style="margin:5px 0 0 3px">–მდე</label>
            	
                 <div class="left" style="margin-left: 20px;">
	            <button id="show_filter">ფილტრი</button>
             </div>
           <label class="left" style="margin:5px 0 0 40px">მომართვების  ჯამური რაოდენობა: </label> <label id="total_quantity" class="left" style="margin:5px 0 0 2px; font-weight: bold;">5</label>
       		<br /><br /><br />
        </div>
		<div id="chart_container" style="width: 100%; height: 480px; margin-top:-30px;"></div>
		<input type="text" id="hidden_name" value="" style="display: none;" />
		<br><br><br><br><br><br>
		<table style="margin-top: 10px;" id="table_right_menu">
				<tr>
					<td>
						<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
					</td>
					<td>
						<img alt="log" src="media/images/icons/log.png" height="14" width="14">
					</td>
					<td id="show_copy_prit_exel" myvar="0">
						<img alt="link" src="media/images/icons/select.png" height="14" width="14">
					</td>
				</tr>
			</table>
		 <table class="display" id="report" style="width: 100%">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width:70%">დასახელება</th>
                        <th style="width:15%">რაოდენობა</th>
                        <th style="width:15%">პროცენტი</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" />
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init">
                        </th>
                        <th>
                        	<input type="text" name="search_object" value="ფილტრი" class="search_init">
                        </th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                        </th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th id="qnt">&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
            </table>

<div id="audio_dialog" class="form-dialog" title="ჩანაწერი"></div>
<div id="in_form"  class="form-dialog">
<table style="margin-top: 10px;" id="table_right_menu">
	<tr>
		<td>
			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
		</td>
		<td>
			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
		</td>
		<td id="show_copy_prit_exel1" myvar="0">
			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
		</td>
	</tr>
</table>
<table class="display" id="report_1" style="width: 100%;">
	<thead style="font-size: 10px;">
        <tr id="datatable_header">
            <th>ID</th>
            <th style="width: 6%;" >№</th>
            <th style="width:  13%;">თარიღი</th>
            <th style="width:  10%">სახელი/გვარი</th>
            <th style="width:  6%">User Id</th>
            <th style="width:  10%">ტელეფონი</th>
            <th style="width:  11%">ელ ფოსტა</th>
            <th style="width:  8%">საიტი</th>
            <th style="width:  11%">ოპერატორი</th>
            <th style="width:  10%">რეაგირება</th>
            <th style="width:  5%">ს.დრო</th>
            <th style="width:  12%">ქმედება</th>
        </tr>
    </thead>
    <thead>
        <tr class="search_header">
            <th class="colum_hidden">
            	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
            </th>
            <th>
            	<input type="text" name="search_number" value="" class="search_init" style="width:100%;"></th>
            <th>
                <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width:100%;"/>
            </th>
            <th>
                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;"/>
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
            </th>
            <th>
                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;"/>
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:100%;" />
            </th>
            <th>
                <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 100%;"/>
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
            </th>
             <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width:100%;" />
            </th>
            <th>
                <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 100%;" />
            </th>
        </tr>
    </thead>
</table>
</div>
 <div  id="add-edit-form" class="form-dialog" title="შემომავალი კომუნიკაცია">	</div>
</body>
</html>

