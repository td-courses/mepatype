<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
require_once('../../includes/classes/class.Mysqli.php');

    //----------------------------- ცვლადი
    global $db;
    $db = new dbClass();
    $count        = $_REQUEST['count'];
    $hidden       = $_REQUEST['hidden'];
    $agent	    = $_REQUEST['agent'];
    $queue	    = $_REQUEST['queuet'];
    $start_time = $_REQUEST['start_time'];
    $end_time 	= $_REQUEST['end_time'];
    $day        = (strtotime($end_time)) -  (strtotime($start_time));
    $day_format = round(($day / (60*60*24)) + 1);


    //----------------------------------
    $act = $_REQUEST['act'];
    switch($act){
        case 'get_list':
            $type = $_REQUEST['type'];
            $count	= $_REQUEST['count'];
            $hidden	= $_REQUEST['hidden'];

            $select = "work_activities_log.id,
            user_info.name AS operator,
            CASE
                WHEN ISNULL(work_activities_log.end_datetime) THEN 'გასულია'
                ELSE SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(work_activities_log.end_datetime, work_activities_log.start_datetime))))
            END AS actually_time";

            switch($type){
                case 'main' : $where = ' AND work_activities_log.work_activities_id != 0 AND work_activities_log.work_activities_id <> 5
                GROUP BY work_activities_log.id';
                                $select = " work_activities_log.id,
                                work_activities_log.id,
                                user_info.name AS operator,
                                CONCAT('<div class=\"wfm-report-activitie-color\" style=\"background:',work_activities.color,';\"></div>',work_activities.name) as work_activitie_name,
                                work_activities_log.start_datetime,
                                work_activities_log.end_datetime,
                                CASE
                                    WHEN ISNULL(work_activities_log.end_datetime) THEN 'გასულია'
                                    ELSE SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(work_activities_log.end_datetime, work_activities_log.start_datetime))))
                                END AS actually_time";
                break;

                case 'other' : $where = ' AND work_activities_log.work_activities_id != 0 AND work_activities_log.work_activities_id = 1
                GROUP BY work_activities_log.user_id';break;

                case 'break' : $where = ' AND work_activities_log.work_activities_id != 0 AND work_activities_log.work_activities_id = 3
                GROUP BY work_activities_log.user_id';break;

                case 'busy' : $where = ' AND work_activities_log.work_activities_id != 0 AND work_activities_log.work_activities_id = 2
                GROUP BY work_activities_log.user_id';break;

                case 'training' : $where = ' AND work_activities_log.work_activities_id != 0 AND work_activities_log.work_activities_id = 4
                GROUP BY work_activities_log.user_id';break;

                default : $where = 'Error';
            }

             $db->setQuery("SELECT 
                                        $select
                                    FROM work_activities_log
                                    LEFT JOIN user_info ON work_activities_log.user_id = user_info.user_id
                                    LEFT JOIN work_activities ON work_activities_log.work_activities_id = work_activities.id
                                    WHERE DATE(work_activities_log.start_datetime) >= '$start_time' 
                                    AND DATE(work_activities_log.start_datetime) <= '$end_time'
                                    $where
                                    ORDER BY work_activities_log.id DESC ");
            $data = $db->getList($count, $hidden);

        break;
        case 'tab_0':
            $data['error'] = $error;

                 $db->setQuery("SELECT  COUNT(asterisk_incomming.id) AS `count`
                                                            FROM 	`asterisk_incomming`
                                                            WHERE asterisk_incomming.call_datetime >= '$start_time'
                                                            AND asterisk_incomming.call_datetime <= '$end_time'
                                                            AND asterisk_incomming.dst_extension IN ($agent)
                                                            AND asterisk_incomming.dst_queue in ($queue)");
                $all_call=$db->getResultArray();
                $db->setQuery("SELECT  count(repeated_call.id) as `count`
                                                                    FROM repeated_call
                                                                    JOIN incomming_call on incomming_call.id = repeated_call.incomming_call_id
                                                                    JOIN asterisk_incomming on incomming_call.asterisk_incomming_id = asterisk_incomming.id
                                                                    JOIN extension on extension.extension = asterisk_incomming.dst_extension
                                                                    WHERE asterisk_incomming.`datetime` > '$start_time' 
                                                                        AND asterisk_incomming.`datetime` < '$end_time'
                                                                        AND extension.extension in ($agent)
                                                                        AND dst_queue in ($queue)
                                                                        AND repeated_call.actived ='1';");
                $repeated_calls=$db->getResultArray();
                $db->setQuery("SELECT  count(DISTINCT(repeated_call.phone)) as `count`
                                                                    FROM repeated_call
                                                                    JOIN incomming_call on incomming_call.id = repeated_call.incomming_call_id
                                                                    JOIN asterisk_incomming on incomming_call.asterisk_incomming_id = asterisk_incomming.id
                                                                    JOIN extension on extension.extension = asterisk_incomming.dst_extension
                                                                    WHERE asterisk_incomming.`datetime` > '$start_time' 
                                                                        AND asterisk_incomming.`datetime` < '$end_time'
                                                                        AND extension.extension in ($agent)
                                                                        AND dst_queue in ($queue)
                                                                        AND repeated_call.actived ='1';");
                $unique_repeated_calls = $db->getResultArray();
        
                $data['page']['technik_info'] = '
                                <td>ზარი</td>
                                <td>'.$all_call['result'][0]['count'].'</td>
                                <td>'.$repeated_calls['result'][0]['count'].'</td>
                                <td>'.round($repeated_calls['result'][0]['count']/$all_call['result'][0]['count']*100,2) .'%</td>
                                <td>'.$unique_repeated_calls['result'][0]['count'] .'</td>';
            break;
    }
        
    echo json_encode($data);

?>