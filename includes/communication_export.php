<?php

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="communication-' . rand(10000, 99999) . '.xlsx"');
header('Cache-Control: max-age=0');
header("Pragma: no-cache");
header("Expires: 0");
ob_end_clean();
require_once('excel/PHPExcel/IOFactory.php');
require_once('classes/class.Mysqli.php');
error_reporting(0);

global $db;
$db = new dbClass();
$act            = $_REQUEST['act'];
$start_date     = $_REQUEST['start_date'];
$end_date       = $_REQUEST['end_date'];
$department     = $_REQUEST['department'];
$project        = $_REQUEST['project'];
$region         = $_REQUEST['region'];
$municip        = $_REQUEST['municip'];
$status         = $_REQUEST['status'];
$operator       = $_REQUEST['operator'];
$call_type      = $_REQUEST['call_type'];

if ($act == "sex_report_tab_1") {
    $filter = "";
    $startDate  = $_REQUEST['start_date'];
    $endDate  = $_REQUEST['end_date'];

    if ($startDate != "" && $endDate != "") {
        $filter .= " AND DATE(incomming_call.date) >= '$start_date' ";
        $filter .= " AND DATE(incomming_call.date) <= '$end_date' ";
    }

    $query = "SELECT 	incomming_call.date AS id ,
                            MONTHNAME(incomming_call.date) as monthname,
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female
                FROM 		incomming_call
                    JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN 		asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 1 
                WHERE asterisk_incomming_id != '' $filter
                GROUP BY MONTH(incomming_call.date) 
                    UNION
                SELECT  incomming_call.id,
                        'ჯამი' as monthname,
                                
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0)))  as male,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0)))  as female
                FROM 		incomming_call
                    JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN 		asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 1
                WHERE       asterisk_incomming_id != '' $filter";


    $db->setQuery($query);
    $data = $db->getResultArray();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'პერიოდი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'შემოსული ზარი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მამრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'მდედრობითი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['monthname']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['callcount']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['male']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $item['female']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
    ob_end_clean();
}
if ($act == "sex_report_tab_2") {
    $filter = "";
    $startDate  = $_REQUEST['start_date'];
    $endDate  = $_REQUEST['end_date'];

    if ($startDate != "" && $endDate != "") {
        $filter .= " AND DATE(incomming_call.date) >= '$start_date' ";
        $filter .= " AND DATE(incomming_call.date) <= '$end_date' ";
    }

    $query = "SELECT 	incomming_call.date AS id ,
                            MONTHNAME(incomming_call.date) as monthname,
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male
                FROM 		incomming_call
                    JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN 		asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 2 
                WHERE asterisk_incomming_id != '' $filter
                GROUP BY MONTH(incomming_call.date) 
                    UNION
                SELECT  incomming_call.id,
                        'ჯამი' as monthname,
                                
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0)))  as male,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0)))  as female
                FROM 		incomming_call
                    JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN 		asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 2
                WHERE       asterisk_incomming_id != '' $filter";

    $db->setQuery($query);
    $data = $db->getResultArray();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'პერიოდი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'გამავალი ზარი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მამრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'მდედრობითი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['monthname']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['callcount']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['male']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $item['female']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
if ($act == "sex_report_tab_3") {
    $filter = "";
    $startDate  = $_REQUEST['start_date'];
    $endDate  = $_REQUEST['end_date'];

    $kendoFilters = json_decode($_REQUEST["kendoFilters"]);
    $kendoFiltering = "";


    // foreach ($kendoFilters as $filter) {
    //     $kendoFiltering .= " AND " . $filter["field"] . " = " . $filter["value"];
    // }


    if (!empty($startDate)) {
        $filter .= " WHERE DATE(incomming_call.date) >= '$start_date'";
    }
    if (!empty($endDate)) {
        $filter .= " AND DATE(incomming_call.date) <= '$end_date '";
    }

    $query = "SELECT incomming_call.id,
                        MONTHNAME(incomming_call.date) as monthname,
                        SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
                        SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
                        SUM(if((incomming_call.source_id = 1),(1),(0))) as phone,
                        SUM(if((incomming_call.source_id = 7),(1),(0))) as email,
                        SUM(if((incomming_call.source_id = 4),(1),(0))) as livechat,
                        SUM(if((incomming_call.source_id = 10),(1),(0))) as webcall
                FROM incomming_call
                    JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN source on source.id = incomming_call.source_id
                $filter 
                GROUP BY MONTH(incomming_call.date)
                UNION
                    SELECT incomming_call.date AS id,
                        'ჯამი' as monthname,
                        SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
                        SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
                        SUM(if((incomming_call.source_id = 1),(1),(0))) as phone,
                        SUM(if((incomming_call.source_id = 7),(1),(0))) as email,
                        SUM(if((incomming_call.source_id = 4),(1),(0))) as livechat,
                        SUM(if((incomming_call.source_id = 10),(1),(0))) as webcall
                FROM incomming_call
                    JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN source on source.id = incomming_call.source_id
                 $filter
                 
                 ";


    $db->setQuery($query);
    $data = $db->getResultArray();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'პერიოდი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'მამრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მდედრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ტელეფონი');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ელ. ფოსტა');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'საიტის ჩათი');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ვიდეო ზარი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['monthname']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['female']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['male']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $item['phone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $item['livechat']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $item['email']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $item['webcall']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
if ($act == "sex_report_tab_4") {
    $filter = "";
    $startDate  = $start_date . " 00:00";
    $endDate  = $end_date . " 23:59";

    $callsType = $_REQUEST["callsType"];
    $type_filter = '';
    if ($callsType != "" && $callsType != 'null') {
        $type_filter = " JOIN incomming_call ON incomming_call.id = uwyeba.inc_req_id
                             WHERE incomming_call.source_id IN('$callsType') ";
    }



    $query = "  SELECT
                        info_category.`name` as `uwyeba`,
                        SUM(IF( uwyeba.gender_id = 1, 1, 0 )) AS `females`,
                        SUM(IF( uwyeba.gender_id = 2, 1, 0 )) AS `males` 
                    FROM (
                            SELECT gender.int_value AS `gender_id`,
                                   incomming_request_processing.int_value AS `cat_id`,
                                   gender.inc_req_id
                        FROM (
                            SELECT incomming_request_processing.incomming_request_id AS inc_req_id,
                                        incomming_request_processing.int_value 
                            FROM incomming_request_processing 
                            WHERE datetime BETWEEN '$startDate' AND '$endDate'
                              AND incomming_request_processing.processing_setting_detail_id IN ( 130 ) 
                              AND incomming_request_processing.int_value > 0 
                            GROUP BY incomming_request_processing.incomming_request_id ) AS `gender`
                            JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = gender.inc_req_id 
                            AND incomming_request_processing.processing_setting_detail_id = 0 
                            AND incomming_request_processing.`value` = 1 
                        GROUP BY gender.inc_req_id,incomming_request_processing.additional_deps ) AS `uwyeba`
                        JOIN info_category ON info_category.id = uwyeba.cat_id 
                        $type_filter
                    GROUP BY uwyeba.cat_id";


    $db->setQuery($query);
    $data = $db->getResultArray();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'უწყება');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'მდედრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მამრობითი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['uwyeba']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['females']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['males']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
if ($act == "sex_report_tab_5") {
    $filter = "";
    $startDate  = $start_date . " 00:00";
    $endDate  = $end_date . " 23:59";


    $callsType = $_REQUEST["callsType"];

    $type_filter = '';
    if ($callsType != "" && $callsType != 'null') {
        $type_filter = " JOIN incomming_call ON incomming_call.id = uwyeba.inc_req_id
                             WHERE incomming_call.source_id IN('$callsType') ";
    }

    $query = "  SELECT
                        info_category.`name` as `project`,
                        SUM(IF( uwyeba.gender_id = 1, 1, 0 )) AS `females`,
                        SUM(IF( uwyeba.gender_id = 2, 1, 0 )) AS `males` 
                    FROM (
                            SELECT gender.int_value AS `gender_id`,
                                   incomming_request_processing.int_value AS `cat_id` ,
                                   gender.inc_req_id
                        FROM (
                            SELECT incomming_request_processing.incomming_request_id AS inc_req_id,
                                        incomming_request_processing.int_value 
                            FROM incomming_request_processing 
                            WHERE datetime BETWEEN '$startDate' AND '$endDate'
                              AND incomming_request_processing.processing_setting_detail_id IN ( 130 ) 
                              AND incomming_request_processing.int_value > 0 
                            GROUP BY incomming_request_processing.incomming_request_id ) AS `gender`
                            JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = gender.inc_req_id 
                            AND incomming_request_processing.processing_setting_detail_id = 0 
                            AND incomming_request_processing.`value` = 2 
                        GROUP BY gender.inc_req_id,incomming_request_processing.additional_deps ) AS `uwyeba`
                        JOIN info_category ON info_category.id = uwyeba.cat_id 
                        $type_filter
                    GROUP BY uwyeba.cat_id";

    $db->setQuery($query);
    $data = $db->getResultArray();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'პროექტი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'მდედრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მამრობითი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['project']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['females']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['males']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
if ($act == "sex_report_tab_6") {
    $filter = "";

    $startDate  = $start_date . " 00:00";
    $endDate  = $end_date . " 23:59";


    $callsType = $_REQUEST["callsType"];

    $type_filter = '';
    if ($callsType != "" && $callsType != 'null') {
        $type_filter = " JOIN incomming_call ON incomming_call.id = uwyeba.inc_req_id
                             WHERE incomming_call.source_id IN('$callsType') ";
    }

    $query = "  SELECT
                        mepa_project_types.`name` as `momartva`,
                        SUM(IF( uwyeba.gender_id = 1, 1, 0 )) AS `females`,
                        SUM(IF( uwyeba.gender_id = 2, 1, 0 )) AS `males` 
                    FROM (
                            SELECT gender.int_value AS `gender_id`,
                                   incomming_request_processing.int_value AS `cat_id`,
                                   gender.inc_req_id
                        FROM (
                            SELECT incomming_request_processing.incomming_request_id AS inc_req_id,
                                        incomming_request_processing.int_value 
                            FROM incomming_request_processing 
                            WHERE datetime BETWEEN '$startDate' AND '$endDate'
                              AND incomming_request_processing.processing_setting_detail_id IN ( 130 ) 
                              AND incomming_request_processing.int_value > 0 
                            GROUP BY incomming_request_processing.incomming_request_id ) AS `gender`
                            JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = gender.inc_req_id 
                            AND incomming_request_processing.processing_setting_detail_id = 0 
                            AND incomming_request_processing.`value` = 2 
                        GROUP BY gender.inc_req_id,incomming_request_processing.additional_deps ) AS `uwyeba`
                        JOIN mepa_project_types ON mepa_project_types.id = uwyeba.cat_id 

                        $type_filter

                    GROUP BY uwyeba.cat_id";

    $db->setQuery($query);
    $data = $db->getResultArray();

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'მომართვა');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'მდედრობითი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მამრობითი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['momartva']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['females']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['males']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
// END OF SEX REPORTS 



if ($act == 'export_campaign_data_excel_incomming') {

    $all_calls      = $_REQUEST['filter_all_calls'];
    $operator_id    = $_REQUEST['filter_operator'];
    $start            = $_REQUEST['start'];
    $end             = $_REQUEST['end'];
    $user_info_id     = $_REQUEST['user_info_id'];
    $user              = $_SESSION['USERID'];
    $user_filt        = '';

    if ($call_type != '') {
        $call_type_filter = " AND incomming_call.source_id IN($call_type)";
    }
    if ($user_info_id == 1) {
        $user_filt = "AND incomming_call.user_id = $user";
    }

    if ($all_calls > 0) {
        $filt = " AND incomming_request_processing.int_value = '$all_calls'";
    } else {
        $filt = '';
    }

    if ($operator_id > 0) {
        $filt1 = "AND IF(ISNULL(incomming_call.asterisk_incomming_id),incomming_call.user_id,asterisk_call_log.user_id) = '$operator_id'";
    } else {
        $filt1 = '';
    }

    $filter_call_status     = $_REQUEST['filter_call_status'];

    $filt_status = '';

    if ($filter_call_status != 'undefined' && $filter_call_status != '') {
        $filt_status = "AND (CASE
            							WHEN ISNULL(incomming_call.`user_id`) OR incomming_call.`user_id` = 0 AND IFNULL(`asterisk_call_log`.`call_status_id`,100) NOT IN(2,9,3) THEN 2
            							WHEN (NOT ISNULL(incomming_call.`user_id`) OR incomming_call.`user_id` > 0) AND IFNULL(`asterisk_call_log`.`call_status_id`,100) NOT IN(2,9,3) THEN 1
            							WHEN asterisk_call_log.call_status_id = 9 THEN 3
            							WHEN asterisk_call_log.call_status_id = 3 THEN 4
            							WHEN asterisk_call_log.call_status_id = 2 THEN 5
            					  END) IN ($filter_call_status)";
    }

    // echo $filt_status;

    $filter_call_sources = $_REQUEST['filter_call_sources'];

    if ($filter_call_sources != 'undefined' && $filter_call_sources != '') {
        $filter_source = "AND IFNULL(incomming_call.source_id, 11) IN($filter_call_sources)";
    }
    $filter_call_type = $_REQUEST['filter_call_type'];
    if ($filter_call_type != 'undefined' && $filter_call_type != '') {
        $filter_source = "AND asterisk_call_log.call_type_id IN($filter_call_type)";
    }


    $db->setQuery("SELECT      IF(asterisk_call_log.call_status_id = 9, '', incomming_call.id),
                                incomming_call.date AS `date`,
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN incomming_call.phone
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.phone
                                    WHEN NOT ISNULL(incomming_call.fb_chat_id)  THEN fb_chat.sender_name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN proc_phone.value
                                    WHEN NOT ISNULL(incomming_call.viber_chat_id)  THEN viber_chat.sender_name
                                    WHEN NOT ISNULL(incomming_call.video_call_id) THEN incomming_call.phone
                                END AS `phone`,
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN mail.sender_name
                                    WHEN NOT ISNULL(incomming_call.video_call_id)  THEN IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value))
                                END AS `mom_auth`,
                                
                                -- KOJO
                                momartvaa.`name` AS `mom_type`,


                                IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba_dat.name,uwyeba_dat2.name) as 'uwyeba',
                                IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),project_dat.name,project_dat2.name)as 'project',
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(IFNULL(IFNULL(user_info1.`name`,user_info.`name`), ''), '(',asterisk_extension.number, ')'),user_info1.`name`),user_info.name)
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN user_info1.name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN user_info1.name
                                    WHEN NOT ISNULL(incomming_call.video_call_id) THEN user_info1.name
                                END AS `user_name`,
                                CASE
                                    WHEN incomming_request_processing.int_value = 1 OR compaign_request_processing.int_value = 1 THEN 'გადაცემულია გასარკვევად'
                                    WHEN incomming_request_processing.int_value = 2 OR compaign_request_processing.int_value = 2 THEN 'გარკვევის პროცესშია'
                                    WHEN incomming_request_processing.int_value = 3 OR compaign_request_processing.int_value = 3 THEN 'დასრულებულია'
                                END AS `status`,



                                CASE
                                -- CHAT
                                    WHEN (NOT ISNULL(incomming_call.`chat_id`) AND NOT ISNULL(incomming_call.`user_id`) AND chat.status != 5)
                                        THEN 'დამუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`chat_id`) AND ISNULL(incomming_call.`user_id`) AND chat.status != 5)
                                        THEN 'დაუმუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`chat_id`) AND chat.status = 5)
                                        THEN 'არასამუშ.ჩატი'
                                    -- MAIL
                                    WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND ISNULL(incomming_call.`user_id`))
                                        THEN 'დაუმუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`mail_chat_id`) AND NOT ISNULL(incomming_call.`user_id`))
                                        THEN 'დამუშავებელი'

                                    -- FB CHAT 
                                    WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                        THEN 'დაუმუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`fb_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                        THEN 'დამუშავებელი'

                                    -- FB COMMENT 
                                    WHEN (NOT ISNULL(incomming_call.`fb_comment_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                        THEN 'დაუმუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`fb_comment_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                        THEN 'დამუშავებელი'
                                    -- VIBER
                                    WHEN (NOT ISNULL(incomming_call.`viber_chat_id`) AND (ISNULL(incomming_call.`cat_1`) OR ISNULL(incomming_call.`cat_1_1`)))
                                        THEN 'დაუმუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`viber_chat_id`) AND NOT ISNULL(incomming_call.`cat_1`) AND NOT ISNULL(incomming_call.`cat_1_1`))
                                        THEN 'დამუშავებელი'
                                    
                                    -- VIDEO CALL
                                    WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND ISNULL(incomming_call.`user_id`))
                                        THEN 'დაუმუშავებელი'
                                    WHEN (NOT ISNULL(incomming_call.`video_call_id`) AND NOT ISNULL(incomming_call.`user_id`))
                                            THEN 'დამუშავებელი'
                                    
                                    WHEN `asterisk_call_log`.`call_status_id` = 9 THEN 'უპასუხო'
                                    WHEN `asterisk_call_log`.`call_status_id` = 2 THEN 'რიგშია'
                                    WHEN `asterisk_call_log`.`call_status_id` = 3 THEN 'საუბრობს'
                                    WHEN `asterisk_call_log`.`call_status_id` = 11
                                        THEN 'გადამისამართება'
                                    WHEN `asterisk_call_log`.`call_status_id` = 12
                                        THEN 'უპ.Autodialer'
                                    WHEN `asterisk_call_log`.`call_status_id` = 13
                                        THEN 'ნაპ.Autodialer'
                                    WHEN `asterisk_call_log`.`call_status_id` IN(6,7,8)
                                        THEN IF (isnull(`incomming_call`.`user_id`),
                                        'დაუმუშავებელი',
                                        IF(`incomming_call`.transfer=1, 
                                        'გადართული',
                                        'დამუშავებული'))
                                    ELSE ''
                                END AS `status1`,
                                CASE 
                                    WHEN genderID.`int_value` = 1 THEN 'მდედრობითი'
                                    WHEN genderID.`int_value` = 2 THEN 'მამრობითი'
                                END AS `gender`

                FROM        incomming_call					
                LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                LEFT JOIN	asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                LEFT JOIN	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
                LEFT JOIN   asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
                LEFT JOIN   user_info ON user_info.user_id = asterisk_call_log.user_id
                LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                LEFT JOIN   info_category ON info_category.id = incomming_call.cat_1
                LEFT JOIN   `chat` ON `chat`.id = incomming_call.chat_id
                LEFT JOIN   fb_chat ON fb_chat.id = incomming_call.fb_chat_id
                LEFT JOIN   mail ON mail.id = incomming_call.mail_chat_id
                LEFT JOIN   viber_chat ON viber_chat.id = incomming_call.viber_chat_id
                LEFT JOIN   video_calls ON video_calls.id = incomming_call.video_call_id
                LEFT JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 136
                LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = incomming_call.id AND proc_name.processing_setting_detail_id = 127
                LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = incomming_call.id AND proc_lastname.processing_setting_detail_id = 128
                LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = incomming_call.id AND proc_phone.processing_setting_detail_id = 124
                LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = incomming_call.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
                LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = incomming_call.id AND project.processing_setting_detail_id = 0 AND project.value = 2
                LEFT JOIN 	incomming_request_processing AS `momartvebi` ON momartvebi.incomming_request_id = incomming_call.id AND momartvebi.processing_setting_detail_id = 0 AND momartvebi.value = 4

                LEFT JOIN   compaign_request_processing ON compaign_request_processing.base_id = outgoing_campaign_request_base.id AND compaign_request_processing.processing_setting_detail_id = 136
                LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
                LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
                LEFT JOIN   compaign_request_processing AS `proc_phone2` ON proc_phone2.base_id = outgoing_campaign_request_base.id AND proc_phone2.processing_setting_detail_id = 124
                LEFT JOIN 	compaign_request_processing AS `uwyeba2` ON uwyeba2.base_id = outgoing_campaign_request_base.id AND uwyeba2.processing_setting_detail_id = 0 AND uwyeba2.value = 1
                LEFT JOIN 	compaign_request_processing AS `project2` ON project2.base_id = outgoing_campaign_request_base.id AND project2.processing_setting_detail_id = 0 AND project2.value = 2
                LEFT JOIN 	info_category AS `uwyeba_dat` ON uwyeba_dat.id = uwyeba.int_value
                LEFT JOIN 	info_category AS `project_dat` ON project_dat.id = project.int_value

                LEFT JOIN 	info_category AS `uwyeba_dat2` ON uwyeba_dat2.id = uwyeba2.int_value
                LEFT JOIN 	info_category AS `project_dat2` ON project_dat2.id = project2.int_value
                LEFT JOIN incomming_request_processing AS genderID ON genderID.processing_setting_detail_id = 130 AND incomming_call.id = genderID.incomming_request_id
                
                LEFT JOIN mepa_project_types AS momartvaa ON momartvaa.id = momartvebi.int_value 
                    WHERE (asterisk_call_log.call_type_id = 1 OR asterisk_call_log.call_type_id = 5 OR ISNULL(asterisk_call_log.call_type_id)) AND NOT ISNULL(incomming_call.source_id) AND (incomming_call.noted!=2 OR incomming_call.noted IS NULL ) AND DATE(incomming_call.date) BETWEEN '$start' AND '$end' $filt $filt1 $filt_status $filter_source $user_filt 
                    GROUP BY  	`incomming_call`.`id`
                    ORDER BY 	`incomming_call`.`id` DESC");
    $data = $db->getResultArray();


    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'თარიღი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ტელეფონი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მომართვის ავტორი');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'მომართვის ტიპი');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'უწყება');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'პროექტი');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ოპერატორი');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'მომართვის სტატუსი');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'სტატუსი');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'სქესი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['phone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['mom_auth']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $item['mom_type']);

        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $item['uwyeba']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $item['project']);

        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $item['user_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $item['status']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $item['status1']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $item['gender']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
} else if ($act == 'export_outgoing') {

    $all_calls      = $_REQUEST['filter_all_calls'];
    $operator_id    = $_REQUEST['filter_operator'];
    $start            = $_REQUEST['start_date'];
    $end             = $_REQUEST['end_date'];
    $user_info_id     = $_REQUEST['user_info_id'];
    $user              = $_SESSION['USERID'];
    $user_filt        = '';

    if ($call_type != '') {
        $call_type_filter = " AND incomming_call.source_id IN($call_type)";
    }
    if ($user_info_id == 1) {
        $user_filt = "AND incomming_call.user_id = $user";
    }

    if ($all_calls > 0) {
        $filt = " AND incomming_request_processing.int_value = '$all_calls'";
    } else {
        $filt = '';
    }

    if ($operator_id > 0) {
        $filt1 = "AND IF(ISNULL(incomming_call.asterisk_incomming_id),incomming_call.user_id,asterisk_call_log.user_id) = '$operator_id'";
    } else {
        $filt1 = '';
    }

    $filter_call_status     = $_REQUEST['filter_call_status'];

    $filt_status = '';

    if ($filter_call_status != 'undefined' && $filter_call_status != '') {
        $filt_status = "AND (CASE
            							WHEN ISNULL(incomming_call.`user_id`) OR incomming_call.`user_id` = 0 AND IFNULL(`asterisk_call_log`.`call_status_id`,100) NOT IN(2,9,3) THEN 2
            							WHEN (NOT ISNULL(incomming_call.`user_id`) OR incomming_call.`user_id` > 0) AND IFNULL(`asterisk_call_log`.`call_status_id`,100) NOT IN(2,9,3) THEN 1
            							WHEN asterisk_call_log.call_status_id = 9 THEN 3
            							WHEN asterisk_call_log.call_status_id = 3 THEN 4
            							WHEN asterisk_call_log.call_status_id = 2 THEN 5
            					  END) IN ($filter_call_status)";
    }

    // echo $filt_status;

    $filter_call_sources = $_REQUEST['filter_call_sources'];

    if ($filter_call_sources != 'undefined' && $filter_call_sources != '') {
        $filter_source = "AND IFNULL(incomming_call.source_id, 11) IN($filter_call_sources)";
    }
    $filter_call_type = $_REQUEST['filter_call_type'];
    if ($filter_call_type != 'undefined' && $filter_call_type != '') {
        $filter_source = "AND asterisk_call_log.call_type_id IN($filter_call_type)";
    }


    $db->setQuery("SELECT      IF(asterisk_call_log.call_status_id = 9, '', incomming_call.id),
                                incomming_call.date AS `date`,
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN incomming_call.phone
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.phone
                                    WHEN NOT ISNULL(incomming_call.fb_chat_id)  THEN fb_chat.sender_name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN proc_phone.value
                                    WHEN NOT ISNULL(incomming_call.viber_chat_id)  THEN viber_chat.sender_name
                                    WHEN NOT ISNULL(incomming_call.video_call_id) THEN incomming_call.phone
                                END AS `phone`,
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN mail.sender_name
                                    WHEN NOT ISNULL(incomming_call.video_call_id)  THEN IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value))
                                END AS `mom_auth`,
                                IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba_dat.name,uwyeba_dat2.name) as 'uwyeba',
                                IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),project_dat.name,project_dat2.name)as 'project',
                                CASE
                                    WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(IFNULL(IFNULL(user_info1.`name`,user_info.`name`), ''), '(',asterisk_extension.number, ')'),user_info1.`name`),user_info.name)
                                    WHEN NOT ISNULL(incomming_call.chat_id)  THEN user_info1.name
                                    WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN user_info1.name
                                    WHEN NOT ISNULL(incomming_call.video_call_id) THEN user_info1.name
                                END AS `user_name`,
                                CASE
                                    WHEN incomming_request_processing.int_value = 1 OR compaign_request_processing.int_value = 1 THEN 'გადაცემულია გასარკვევად'
                                    WHEN incomming_request_processing.int_value = 2 OR compaign_request_processing.int_value = 2 THEN 'გარკვევის პროცესშია'
                                    WHEN incomming_request_processing.int_value = 3 OR compaign_request_processing.int_value = 3 THEN 'დასრულებულია'
                                END AS `status`,



                                CASE
									-- CHAT
                                        
                                        WHEN `asterisk_call_log`.`call_status_id` = 12 OR ISNULL(`asterisk_call_log`.`call_status_id`)
                                            THEN 'უპასუხო'
                                        WHEN `asterisk_call_log`.`call_status_id` = 13
                                            THEN 'ნაპასუხები'
                                    END AS `status1`,
                                CASE 
                                    WHEN genderID.`int_value` = 1 THEN 'მდედრობითი'
                                    WHEN genderID.`int_value` = 2 THEN 'მამრობითი'
                                END AS `gender`

                FROM        incomming_call					
                LEFT JOIN   asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                LEFT JOIN	asterisk_call_record ON asterisk_call_record.asterisk_call_log_id=asterisk_call_log.id
                LEFT JOIN	outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
                LEFT JOIN   asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
                LEFT JOIN   user_info ON user_info.user_id = asterisk_call_log.user_id
                LEFT JOIN   user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                LEFT JOIN   inc_status ON inc_status.id = incomming_call.inc_status_id
                LEFT JOIN   info_category ON info_category.id = incomming_call.cat_1
                LEFT JOIN   `chat` ON `chat`.id = incomming_call.chat_id
                LEFT JOIN   fb_chat ON fb_chat.id = incomming_call.fb_chat_id
                LEFT JOIN   mail ON mail.id = incomming_call.mail_chat_id
                LEFT JOIN   viber_chat ON viber_chat.id = incomming_call.viber_chat_id
                LEFT JOIN   video_calls ON video_calls.id = incomming_call.video_call_id
                LEFT JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 136
                LEFT JOIN   incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = incomming_call.id AND proc_name.processing_setting_detail_id = 127
                LEFT JOIN   incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = incomming_call.id AND proc_lastname.processing_setting_detail_id = 128
                LEFT JOIN   incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = incomming_call.id AND proc_phone.processing_setting_detail_id = 124
                LEFT JOIN 	incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = incomming_call.id AND uwyeba.processing_setting_detail_id = 0 AND uwyeba.value = 1
                LEFT JOIN 	incomming_request_processing AS `project` ON project.incomming_request_id = incomming_call.id AND project.processing_setting_detail_id = 0 AND project.value = 2

                LEFT JOIN   compaign_request_processing ON compaign_request_processing.base_id = outgoing_campaign_request_base.id AND compaign_request_processing.processing_setting_detail_id = 136
                LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
                LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
                LEFT JOIN   compaign_request_processing AS `proc_phone2` ON proc_phone2.base_id = outgoing_campaign_request_base.id AND proc_phone2.processing_setting_detail_id = 124
                LEFT JOIN 	compaign_request_processing AS `uwyeba2` ON uwyeba2.base_id = outgoing_campaign_request_base.id AND uwyeba2.processing_setting_detail_id = 0 AND uwyeba2.value = 1
                LEFT JOIN 	compaign_request_processing AS `project2` ON project2.base_id = outgoing_campaign_request_base.id AND project2.processing_setting_detail_id = 0 AND project2.value = 2
                LEFT JOIN 	info_category AS `uwyeba_dat` ON uwyeba_dat.id = uwyeba.int_value
                LEFT JOIN 	info_category AS `project_dat` ON project_dat.id = project.int_value

                LEFT JOIN 	info_category AS `uwyeba_dat2` ON uwyeba_dat2.id = uwyeba2.int_value
                LEFT JOIN 	info_category AS `project_dat2` ON project_dat2.id = project2.int_value
                LEFT JOIN incomming_request_processing AS genderID ON genderID.processing_setting_detail_id = 130 AND incomming_call.id = genderID.incomming_request_id
                    WHERE asterisk_call_log.call_type_id = 2  AND DATE(incomming_call.date) BETWEEN '$start' AND '$end'
                    GROUP BY  	`incomming_call`.`id`
                    ORDER BY 	`incomming_call`.`id` DESC");
    $data = $db->getResultArray();


    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'თარიღი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ტელეფონი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მომართვის ავტორი');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'უწყება');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'პროექტი');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ოპერატორი');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'მომართვის სტატუსი');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'სტატუსი');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'სქესი');

    foreach ($data['result'] as $item) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['phone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['mom_auth']);

        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $item['uwyeba']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $item['project']);

        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $item['user_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $item['status']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $item['status1']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $item['gender']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
} else {
    if ($department != '') {
        $department_filter = " AND saboloo.uwyeba2 IN($department)";
    }
    if ($project != '') {
        $project_filter = " AND saboloo.project2 IN($project)";
    }
    if ($region != '') {
        $region_filter = " AND saboloo.region2 IN($region)";
    }
    if ($municip != '') {
        $municip_filter = " AND saboloo.municip2 IN($municip)";
    }
    if ($status != '') {
        $status_filter = " AND incomming_request_processing.int_value IN($status) OR compaign_request_processing.int_value IN($status)";
    }
    if ($operator != '') {
        $operator_filter = " AND incomming_call.user_id IN($operator)";
    }
    $db->setQuery(" SELECT * FROM (SELECT 	id,
                jansugi.date AS `date`,
                jansugi.phone,
                jansugi.mom_auth,
                MAX(uwyeba) AS `uwyeba`, 
                MAX(project) AS `project`, 
    
                MAX(sub_project) AS `sub_project`, 
                MAX(region) AS `region`,
                MAX(municip) AS `municip`,
                MAX(temi) AS `temi`,
                MAX(village) AS `village`,
    
                MAX(request_type) AS `mom_type`,
                jansugi.status,
                jansugi.comm_source,
                jansugi.did,
    
                MAX(uwyeba2) AS `uwyeba2`,
                MAX(project2) AS `project2`,
                MAX(region2) AS `region2`,
                MAX(municip2) AS `municip2`
                FROM (SELECT 
                incomming_call.id,
                incomming_call.date,
                CASE
                        WHEN incomming_request_processing.int_value = 1 OR compaign_request_processing.int_value = 1 THEN 'გადაცემულია გასარკვევად'
                        WHEN incomming_request_processing.int_value = 2 OR compaign_request_processing.int_value = 2 THEN 'გარკვევის პროცესშია'
                        WHEN incomming_request_processing.int_value = 3 OR compaign_request_processing.int_value = 3 THEN 'დასრულებულია'
                END AS `status`,
                source.name_ka AS `comm_source`,
                asterisk_call_log.did,
                CASE
                        WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN incomming_call.phone
                        WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.phone
                        WHEN NOT ISNULL(incomming_call.fb_chat_id)  THEN fb_chat.sender_name
                        WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN proc_phone.value
                        WHEN NOT ISNULL(incomming_call.viber_chat_id)  THEN viber_chat.sender_name
                END AS `phone`,
                CASE
                        WHEN NOT ISNULL(incomming_call.asterisk_incomming_id) THEN IF(asterisk_call_log.call_type_id != 5,IFNULL(CONCAT(proc_name.value,' ',proc_lastname.value),IFNULL(proc_name.value,proc_lastname.value)),IFNULL(CONCAT(proc_name2.value,' ',proc_lastname2.value),IFNULL(proc_name2.value,proc_lastname2.value)))
                        WHEN NOT ISNULL(incomming_call.chat_id)  THEN chat.name
                        WHEN NOT ISNULL(incomming_call.mail_chat_id)  THEN mail.sender_name
                END AS `mom_auth`,
                uwyeba.additional_deps,
                uwyeba.int_value,
                CASE
							
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba.`value`,uwyeba2.`value`) = 1 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),info_category.`name`,info_category2.`name`) ELSE '' 
                    END AS `uwyeba`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba.`value`,uwyeba2.`value`) = 2 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),info_category.`name`,info_category2.`name`) ELSE ''
                    END AS `project`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba.`value`,uwyeba2.`value`) = 3 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),info_category.`name`,info_category2.`name`) ELSE '' 
                    END AS `sub_project`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),uwyeba.`value`,uwyeba2.`value`) = 4 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),( SELECT NAME FROM mepa_project_types WHERE id = uwyeba.`int_value` LIMIT 1 ),( SELECT NAME FROM mepa_project_types WHERE id = uwyeba2.`int_value` LIMIT 1 )) ELSE '' 
                    END AS `request_type`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),region.`value`,region2.`value`) = 1111 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),regions.`name`,regions2.`name`) ELSE '' 
                    END AS `region`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),region.`value`,region2.`value`) = 2222 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),regions.`name`,regions2.`name`) ELSE '' 
                    END AS `municip`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),region.`value`,region2.`value`) = 3333 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),regions.`name`,regions2.`name`) ELSE '' 
                    END AS `temi`,
                CASE
                        
                        WHEN IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),region.`value`,region2.`value`) = 4444 THEN
                        IF(asterisk_call_log.call_type_id IN (1,2) OR ISNULL(asterisk_call_log.call_type_id),regions.`name`,regions2.`name`) ELSE '' 
                    END AS `village`,
    
                CASE
                    WHEN uwyeba.`value` = 1 THEN info_category.`id`
                    ELSE 0
                END AS `uwyeba2`,
                CASE
                    WHEN uwyeba.`value` = 2 THEN info_category.`id`
                    ELSE 0
                END AS `project2`,
                CASE
                    
                    WHEN uwyeba.`value` = 3 THEN info_category.`id`
                    ELSE 0
                END AS `sub_project2`,
                CASE
                    WHEN uwyeba.`value` = 4 THEN info_category.`id`
                    ELSE 0
                END AS `request_type2`,
    
                CASE
                    WHEN region.`value` = 1111 THEN regions.`id`
                    ELSE 0
                END AS `region2`,
                CASE
                    WHEN region.`value` = 2222 THEN regions.`id`
                    ELSE 0
                END AS `municip2`,
                CASE
                    WHEN region.`value` = 3333 THEN regions.`id`
                    ELSE 0
                END AS `temi2`,
                CASE
                    WHEN region.`value` = 4444 THEN regions.`id`
                    ELSE 0
                END AS `village2`
                FROM   incomming_call
                LEFT JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id
                LEFT JOIN outgoing_campaign_request_base ON outgoing_campaign_request_base.call_log_id = asterisk_call_log.id
                LEFT JOIN asterisk_extension ON asterisk_extension.id = asterisk_call_log.extension_id
                LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                LEFT JOIN user_info AS `user_info1` ON user_info1.user_id = incomming_call.user_id
                LEFT JOIN `chat` ON `chat`.id = incomming_call.chat_id
                LEFT JOIN fb_chat ON fb_chat.id = incomming_call.fb_chat_id
                LEFT JOIN mail ON mail.id = incomming_call.mail_chat_id
                LEFT JOIN viber_chat ON viber_chat.id = incomming_call.viber_chat_id
                LEFT JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id 
                AND incomming_request_processing.processing_setting_detail_id = 136
                LEFT JOIN incomming_request_processing AS `proc_name` ON proc_name.incomming_request_id = incomming_call.id 
                AND proc_name.processing_setting_detail_id = 127
                LEFT JOIN incomming_request_processing AS `proc_lastname` ON proc_lastname.incomming_request_id = incomming_call.id 
                AND proc_lastname.processing_setting_detail_id = 128
                LEFT JOIN incomming_request_processing AS `proc_phone` ON proc_phone.incomming_request_id = incomming_call.id 
                AND proc_phone.processing_setting_detail_id = 124
                LEFT JOIN incomming_request_processing AS `trunk` ON trunk.incomming_request_id = incomming_call.id 
                AND trunk.processing_setting_detail_id = 139
                LEFT JOIN incomming_request_processing AS `uwyeba` ON uwyeba.incomming_request_id = incomming_call.id 
                AND uwyeba.processing_setting_detail_id =0 AND uwyeba.`value` IN(1,2,3,4)
                LEFT JOIN incomming_request_processing AS `region` ON region.incomming_request_id = incomming_call.id 
                AND region.processing_setting_detail_id = 999999999 AND region.`value` IN(1111,2222,3333,4444)
                LEFT JOIN source ON source.id = incomming_call.source_id 
                LEFT JOIN info_category ON info_category.id = uwyeba.int_value
                LEFT JOIN regions ON regions.id = region.int_value

                LEFT JOIN   compaign_request_processing ON compaign_request_processing.base_id = outgoing_campaign_request_base.id AND compaign_request_processing.processing_setting_detail_id = 136
                LEFT JOIN   compaign_request_processing AS `proc_name2` ON proc_name2.base_id = outgoing_campaign_request_base.id AND proc_name2.processing_setting_detail_id = 127
                LEFT JOIN   compaign_request_processing AS `proc_lastname2` ON proc_lastname2.base_id = outgoing_campaign_request_base.id AND proc_lastname2.processing_setting_detail_id = 128
                LEFT JOIN   compaign_request_processing AS `proc_phone2` ON proc_phone2.base_id = outgoing_campaign_request_base.id AND proc_phone2.processing_setting_detail_id = 124

                LEFT JOIN compaign_request_processing AS `uwyeba2` ON uwyeba2.base_id = outgoing_campaign_request_base.id AND uwyeba2.processing_setting_detail_id = 0 AND uwyeba2.`value` IN (1,2,3,4)
                LEFT JOIN info_category AS info_category2 ON info_category2.id = uwyeba2.int_value
                
                LEFT JOIN compaign_request_processing AS `region2` ON region2.base_id = outgoing_campaign_request_base.id AND region2.processing_setting_detail_id = 999999999 AND region2.`value` IN ( 1111, 2222, 3333, 4444 )
                LEFT JOIN regions AS regions2 ON regions2.id = region2.int_value
                WHERE asterisk_call_log.call_type_id IN(2, 5, NULL) $status_filter $operator_filter
                AND FROM_UNIXTIME(asterisk_call_log.call_datetime) BETWEEN '$start_date' AND '$end_date') AS `jansugi`
    
                GROUP BY jansugi.id, jansugi.additional_deps) AS `saboloo`
                WHERE 1=1 $department_filter $project_filter $region_filter $municip_filter");
    $data = $db->getResultArray();

    /* die(var_dump($deps_array)); */
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0);
    $rowCount = 2;
    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'თარიღი');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'ტელეფონი');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'მომართვის ავტორი');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'უწყება');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'პროექტი');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'ქვე-პროექტი');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'რაიონი');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'მუნიციპალიტეტი');

    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'თემი');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'სოფელი');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'მომართვის ტიპი');
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'სტატუსი');
    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'კომუნიკაციის არხი');
    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'ინფორმაციის წყარო');
    foreach ($data['result'] as $item) {

        //GETTING DEPARTMENTS BEGIN

        $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $item['date']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $item['phone']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $item['mom_auth']);

        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $item['uwyeba']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $item['project']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $item['sub_project']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $item['mom_type']);

        //GETTING DEPARTMENTS END

        //GETTING REGIONS BEGIN
        $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $item['region']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $item['municip']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $item['temi']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $item['village']);
        //GETTING REGIONS END


        $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $item['status']);
        $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $item['comm_source']);
        $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $item['did']);

        $rowCount++;
    }
    $objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold(true);
    $writerObject = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $writerObject->save('php://output');
}
