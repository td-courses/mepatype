<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();

//----------------------------- ცვლადი

$agent        = $_REQUEST['agent'];
$name        = $_REQUEST['name'];
$queue        = $_REQUEST['queuet'];
$start_time = $_REQUEST['start_time'] . " 00:00";
$end_time     = $_REQUEST['end_time'] . " 23:59";
$all_call = 0;



if ($_REQUEST['act'] == 'check') {

    $id          =      $_REQUEST['hidden'];

    $columnCount = 		$_REQUEST['count'];
    $json_data   = 		json_decode($_REQUEST['add'], true);
    $itemPerPage = 		$json_data['pageSize'];
    $skip        = 		$json_data['skip'];
    $cols[]      =      $_REQUEST['cols'];

    //--------------------------- ნაპასუხები ზარები ოპერატორების მიხედვით

    $db->setQuery(" SELECT COUNT(*) AS all_count 
                    FROM asterisk_call_log
                    JOIN user_info ON asterisk_call_log.user_id = user_info.user_id
                    JOIN incomming_call ON incomming_call.asterisk_incomming_id = asterisk_call_log.id 
                    WHERE FROM_UNIXTIME(asterisk_call_log.`call_datetime`) BETWEEN '$start_time' AND '$end_time' 
                        AND user_info.`name` IN ( $agent ) 
                        AND NOT ISNULL(asterisk_call_log.user_id)");
    $result = $db->getResultArray();
    global $all_call;
    $all_call = $result[result][0][all_count];

    $db->setQuery(" SELECT  
                            user_info.name AS `operator`,
                            COUNT(*)       AS `calls`,
                            SUM(IF( NOT ISNULL( incomming_call.`user_id` ), 1, 0 )) AS `proccessed`,
                            SUM(IF( ISNULL( incomming_call.`user_id` ), 1, 0 ))     AS `unproccessed`,
                            round((COUNT(*) / $all_call) * 100, 2) AS `answered`,
                            IF (NOT ISNULL( incomming_call.asterisk_incomming_id ),
                                SUM(CASE 
                                    WHEN FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime )+ wait_time + talk_time ) <= incomming_call.processing_start_date 
                                    THEN SEC_TO_TIME(UNIX_TIMESTAMP( processing_end_date )- UNIX_TIMESTAMP( processing_start_date )) 
                                                        
                                    WHEN FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime )+ wait_time + talk_time ) > incomming_call.processing_start_date 
                                            AND FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime )+ wait_time + talk_time ) >= incomming_call.processing_end_date 
                                    THEN '00:00:00' 
                                    
                                    WHEN FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime )+ wait_time + talk_time ) > incomming_call.processing_start_date 
                                            AND FROM_UNIXTIME( UNIX_TIMESTAMP( asterisk_call_log.call_datetime )+ wait_time + talk_time ) < incomming_call.processing_end_date 
                                    THEN  
                                    SEC_TO_TIME(UNIX_TIMESTAMP( incomming_call.processing_end_date ) - ( UNIX_TIMESTAMP( asterisk_call_log.call_datetime )+ wait_time + 	talk_time)) 
                                    END ) / COUNT( user_info.id ), 
                                '00:00:00' 
                                ) AS `proccessAVGduration`,

                            (   SELECT COUNT(*) FROM asterisk_call_missed AS amc 
                                WHERE amc.user_id = asterisk_call_log.user_id AND DATE( amc.datetime ) BETWEEN '$start_time' AND '$end_time' ) AS `missedCalls`,
                            SEC_TO_TIME(SUM( asterisk_call_log.talk_time )) AS `callTime`,
                            SEC_TO_TIME(ROUND(( SUM( asterisk_call_log.talk_time )/ COUNT(*)), 0 )) AS `AVGduration`,
                            SEC_TO_TIME(MAX( asterisk_call_log.talk_time )) AS `callMAXtime`,
                            SEC_TO_TIME(MIN( asterisk_call_log.talk_time )) AS `callMINtime`,
                            SUM(IF( asterisk_call_log.call_type_id = 2, 1, 0 )) AS `outgoingCall`,
                            SUM(if( asterisk_call_log.call_type_id = 2 AND asterisk_call_log.user_id > 0,1,0)) AS `outgoingProcessed`,
                            SUM(IF( asterisk_call_log.call_type_id = 2, 1, 0 )) - SUM(if( asterisk_call_log.call_type_id = 2 AND asterisk_call_log.user_id > 0,1,0)) AS `outgoingUnprocessed`
    
    
                           
                    
                    FROM user_info
                    JOIN asterisk_call_log ON asterisk_call_log.user_id = user_info.user_id AND asterisk_call_log.call_type_id IN (1,2) AND asterisk_call_log.call_status_id IN(13,6,7,8)
                    JOIN incomming_call ON incomming_call.asterisk_incomming_id = asterisk_call_log.id 
                    WHERE user_info.`name` IN ($agent) 
                        AND FROM_UNIXTIME(`asterisk_call_log`.`call_datetime` ) BETWEEN '$start_time' AND '$end_time'
                    GROUP BY asterisk_call_log.user_id");

    $data = $db->getKendoList($columnCount,$cols);
    
    // $ress = $db->getResultArray();
    // foreach ($ress[result] as $row) {
    //     global $all_call;

    //     $percent                 = round(($row['zarebi'] / $all_call) * 100, 2);
    //     $coeficienti             = round($row['damushavebuli'] / $row['zarebi'], 2);
    //     $gamavali_daumushavebeli = $row['out_call'] - $row['gamavali_damushavebuli'];
    //     $user_id                 = $row['user_id'];
    //     // echo " percent " . $row['zarebi'] . " / " . $result[result][0][all_count] . " * 100 ->" . $percent . " <br> gamavali_damushavebuli -> " . $gamavali_daumushavebeli ;
    //     $data['page']['answer_call_by_queue'] .= '
    //                     <tr>
    //     					<td style="cursor:pointer;" id="name">' . $row['NAME'] . '</td>
    //     					<td id="answear_dialog" style="cursor: pointer; text-decoration: underline;" user="' . $row['name'] . '">' . $row['zarebi'] . ' ზარი</td>
    //     					<td>' . $row['damushavebuli'] . ' ზარი</td>
    //     					<td id="undone_dialog" style="cursor: pointer; text-decoration: underline;" user1="' . $row['name'] . '">' . $row['daumushavebuli'] . ' ზარი</td>
    //     					<td>' . $percent . '%</td>
    //     					<td>' . $row['processed_time'] . '</td>
    //     					<td>' . $row['miss'] . '</td>
    //     					<td>' . $row['saubris_dro'] . '</td>
    //     					<td>' . $row['sashualo'] . '</td>
    //     					<td>' . $row['max_time'] . '</td>
    //     					<td>' . $row['min_time'] . '</td>
    //     					<td id="answear_dialog1" style="cursor: pointer; text-decoration: underline;" user2="' . $row['name'] . '">' . $row['out_call'] . '  ზარი</td>
    //     					<td>' . $row['gamavali_damushavebuli'] . 'ზარი</td>
    //     					<td id="undone_dialog1" style="cursor: pointer;" user3="' . $row['name'] . '">' . $gamavali_daumushavebeli . '  ზარი</td>
    // 					</tr>';
    // }

    //------------------------------//
} else if ($_REQUEST['act'] == 'answear_dialog_table') {
    $data    = array('page' => array('answear_dialog' => ''));
    $count  = $_REQUEST['count'];
    $hidden = $_REQUEST['hidden'];

    $db->setQuery(" SELECT asterisk_incomming.id,
                    	   asterisk_incomming.call_datetime,
                    	   asterisk_incomming.source,
                    	   asterisk_incomming.dst_queue,
                    	   user_info.`name`,
                    	   SEC_TO_TIME(asterisk_incomming.duration),
                           concat('<button class=\"download\" str=',`asterisk_incomming`.`file_name`,'>შემომავალი</button>')
                    FROM   asterisk_incomming
                    JOIN   user_info ON asterisk_incomming.user_id = user_info.user_id
                    JOIN   incomming_call ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
                    WHERE  DATE(`asterisk_incomming`.`call_datetime`) BETWEEN '$start_time' AND '$end_time'
                    AND    user_info.`name` IN('$name')
                    AND    asterisk_incomming.dst_queue = $queue
                    AND    asterisk_incomming.disconnect_cause IN(3,4)
                    AND    NOT ISNULL(asterisk_incomming.user_id)");

    $data = $db->getList($count, $hidden);
} elseif ($_REQUEST['act'] == 'undone_dialog_table') {
    $data    = array('page' => array('answear_dialog' => ''));
    $count  = $_REQUEST['count'];
    $hidden = $_REQUEST['hidden'];

    $db->setQuery("SELECT  asterisk_incomming.id,
                    	   asterisk_incomming.call_datetime,
                    	   asterisk_incomming.source,
                    	   asterisk_incomming.dst_queue,
                    	   user_info.`name`,
                    	   SEC_TO_TIME(asterisk_incomming.duration),
                           concat('<button class=\"download4\" str=',`asterisk_incomming`.`file_name`,'>დაუმ.შემომავ...</button>')
                    FROM   asterisk_incomming
                    JOIN   user_info ON asterisk_incomming.user_id = user_info.user_id
                    JOIN   incomming_call ON incomming_call.asterisk_incomming_id = asterisk_incomming.id
                    WHERE  DATE(`asterisk_incomming`.`call_datetime`) BETWEEN '$start_time' AND '$end_time'
                    AND    user_info.`name` IN('$name')
                    AND    asterisk_incomming.dst_queue = $queue
                    AND    asterisk_incomming.disconnect_cause IN(3,4)
                    AND    ISNULL(incomming_call.`user_id`)
                    AND    NOT ISNULL(asterisk_incomming.user_id)");

    $data = $db->getList($count, $hidden);
} else if ($_REQUEST['act'] == 'answear_dialog_table1') {

    $data    = array('page' => array('answear_dialog' => ''));
    $count  = $_REQUEST['count'];
    $hidden = $_REQUEST['hidden'];

    $db->setQuery("");
    $data = $db->getList($count, $hidden);

    //------------------------------//

} elseif ($_REQUEST['act'] == 'undone_dialog') {
    $data['page']['answear_dialog'] = '
            <table class="display" id="example2" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 16%;">თარიღი</th>
                        <th style="width: 16%;">ადრესატი</th>
            			<th style="width: 16%;">ექსთენშენი</th>
            			<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 16%;">დრო</th>
                        <th style="width: 16%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
            			</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
            			<th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
            		</tr>
                </thead>
            </table>';
} else if ($_REQUEST['act'] == 'answear_dialog') {

    $data['page']['answear_dialog'] = '
			<table class="display" id="example" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 16%;">თარიღი</th>
                        <th style="width: 16%;">წყარო</th>
                        <th style="width: 16%;">ადრესატი</th>
						<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 16%;">დრო</th>
                        <th style="width: 16%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
						</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
						<th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
					</tr>
                </thead>
            </table>';
} elseif ($_REQUEST['act'] == 'undone_dialog1') {
    $data['page']['answear_dialog'] = '
			<table class="display" id="example2" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 20%;">ადრესატი</th>
    					<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 20%;">დრო</th>
                        <th style="width: 20%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init" style=""/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
    					</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;" />
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;" />
                        </th>
    				</tr>
                </thead>
            </table>';
} else if ($_REQUEST['act'] == 'answear_dialog1') {

    $data['page']['answear_dialog'] = '
			<table class="display" id="example2_1" style="width: 100%;">
                <thead>
                    <tr id="datatable_header">
                        <th>ID</th>
                        <th style="width: 20%;">თარიღი</th>
                        <th style="width: 20%;">ადრესატი</th>
						<th style="width: 20%;">ოპერატორი</th>
                        <th style="width: 20%;">დრო</th>
                        <th style="width: 20%;">ქმედება</th>
                    </tr>
                </thead>
                <thead>
                    <tr class="search_header">
                        <th class="colum_hidden">
                        	<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
                        </th>
                        <th>
                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 80%;">
						</th>
                        <th>
                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>                            
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
                        <th>
                            <input type="text" name="search_category" value="ფილტრი" class="search_init" style="width: 80%;"/>
                        </th>
					</tr>
                </thead>
            </table>';
}

if($_REQUEST['act'] == 'get_columns'){
    
    $columnCount = 		$_REQUEST['count'];
    $cols[] =           $_REQUEST['cols'];
    $columnNames[] = 	$_REQUEST['names'];
    $operators[] = 		$_REQUEST['operators'];
    $selectors[] = 		$_REQUEST['selectors'];
    //$query = "SHOW COLUMNS FROM $tableName";
    //$db->setQuery($query,$tableName);
    //$res = $db->getResultArray();
    $f=0;
    foreach($cols[0] AS $col)
    {
        $column = explode(':',$col);

        $res[$f]['Field'] = $column[0];
        $res[$f]['type'] = $column[1];
        $f++;
    }
    $i = 0;
    $columns = array();
    $types = array();
    foreach($res AS $item)
    {
        $columns[$i] = $item['Field'];
        $types[$i] = $item['type'];
        $i++;
    }
    
    
    $dat = array();
    $a = 0;
    for($j = 0;$j<$columnCount;$j++)
    {
        if(1==2)
        {
            continue;
        }
        else{
            
            if($operators[0][$a] == 1) $op = true; else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
            $op = false;
            if($columns[$j] == 'id')
            {
                $width = "5%";
            }
            else if($columns[$j] == 'shin_status'){
                $width = "12%";
            }
            else{
                $width = 'auto';
            }
            if($columns[$j] == 'inc_id')
            {
                $hidden = true;
            }
            else if($columns[$j] == 'id' OR $columns[$j] == 'req_id'){
                $hidden = true;
            }
            else{
                $hidden = false;
            }
            if($res['data_type'][$j] == 'date')
            {
                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'format'=>"{0:yyyy-MM-dd hh:mm:ss}",'parseFormats' =>["MM/dd/yyyy h:mm:ss"]);
            }
            else if($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
            {
                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'values'=>getSelectors($selectors[0][$a]));
            }
            else
            {
                $g = array('field'=>$columns[$j],'hidden'=>$hidden,'width'=>$width,'encoded'=>false,'title'=>$columnNames[0][$a],'filterable'=>array('multi'=>true,'search' => true));
            }
            $a++;
        }
        array_push($dat,$g);
        
    }
    
    //array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));
    
    $new_data = array();
    //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
    for($j=0;$j<$columnCount;$j++)
    {
        if($types[$j] == 'date')
        {
            $new_data[$columns[$j]] = array('editable'=>false,'type'=>'string');
        }
        else if($types[$j] == 'number'){

            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'number');
        }
        else
        {
            $new_data[$columns[$j]] = array('editable'=>true,'type'=>'string');
        }
    }
    
    $filtArr = array('fields'=>$new_data);
    
    
    
    $kendoData = array('columnss'=>$dat,'modelss'=>$filtArr);

    
    //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');
    
    $data = $kendoData;
    //$data = '[{"gg":"sd","ads":"213123"}]';
    
}

echo json_encode($data);
