<?php
// MySQL Connect Link
require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db = new  dbClass();
ini_set('display_errors', 1); 
error_reporting(E_ERROR);
date_default_timezone_set('Asia/Tbilisi');

// Main Strings
$action                     = $_REQUEST['act'];
$error                      = '';
$data                       = array();
$user_id	                = $_SESSION['USERID'];
$open_number                = $_REQUEST['open_number'];
$queue                      = $_REQUEST['queue'];
$status                     = $_REQUEST['status'];
$task_status                = $_REQUEST['task_status'];
 
// Incomming Call Dialog Strings
$hidden_id                  = $_REQUEST['id'];
$incomming_id               = $_REQUEST['incomming_id'];


$ws_customer_id             = $_REQUEST['ws_customer_id'];
$ws_loan_id                 = $_REQUEST['ws_loan_id'];
$incomming_cat_1            = $_REQUEST['incomming_cat_1'];
$incomming_cat_1_1          = $_REQUEST['incomming_cat_1_1'];
$incomming_cat_1_1_1        = $_REQUEST['incomming_cat_1_1_1'];
$incomming_comment          = htmlspecialchars($_REQUEST['incomming_comment'], ENT_QUOTES);
$info_comment               = htmlspecialchars($_REQUEST['info_comment'], ENT_QUOTES);
$info_requested_info        = $_REQUEST['info_requested_info'];
$info_given_info            = $_REQUEST['info_given_info'];

//ambition
$incomming_call_ambition_id = $_REQUEST['incomming_call_ambition_id'];
$save_ambition              = $_REQUEST['save_ambition'];
if($save_ambition == 'true'){
    $ambition_number               = $_REQUEST["ambition_number"];
    $ambition_registration_date    = $_REQUEST["ambition_registration_date"];
    $ambition_loan_number          = $_REQUEST["ambition_loan_number"];
    $ambition_pid                  = $_REQUEST["ambition_pid"];
    $ambition_mobile               = $_REQUEST["ambition_mobile"];
    $ambition_product              = $_REQUEST["ambition_product"];
    $ambition_answer_need          = $_REQUEST["ambition_answer_need"];
    $ambition_answer_source        = $_REQUEST["ambition_answer_source"];
    $ambition_ambition             = $_REQUEST["ambition_ambition"];
    $ambition_date                 = $_REQUEST["ambition_date"];
    $ambition_author               = $_REQUEST["ambition_author"];
    $ambition_cause                = $_REQUEST["ambition_cause"];
    $ambition_owner                = $_REQUEST["ambition_owner"];
    $ambition_receiver             = $_REQUEST["ambition_receiver"];
    $ambition_client_email         = $_REQUEST["ambition_client_email"];
    $ambition_client_address       = $_REQUEST["ambition_client_address"];
    $ambition_description          = $_REQUEST["ambition_description"];
    $ambition_name                 = $_REQUEST["ambition_name"];
    $ambition_mail_sent_status     = $_REQUEST["ambition_mail_sent_status"];
}
//personal and contact info 
    $info_pid              = $_REQUEST["info_pid"];
    $info_client_number    = $_REQUEST["info_client_number"];
    $info_doc_number       = $_REQUEST["info_doc_number"];
    $info_born_date        = $_REQUEST["info_born_date"];
    $info_age              = $_REQUEST["info_age"];
    $info_name             = $_REQUEST["info_name"];
    $info_mobile           = $_REQUEST["info_mobile"];
    $info_other_mobile     = $_REQUEST["info_other_mobile"];
    $info_juristic_address = $_REQUEST["info_juristic_address"];
    $info_address          = $_REQUEST["info_address"];
    $info_email            = $_REQUEST["info_email"];


//arrow
    $by_arrow       = "false";
    $by_arrow 		= $_REQUEST['by_arrow'];
    $confirm_update = $_REQUEST['confirm_update'];
    $confirm_click  = $_REQUEST['confirm_click'];
$type_id = $_REQUEST['type_id'];

switch ($action) {
	case 'get_add_page':
		$page		= GetPage('','');
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$page		= GetPage(Getincomming($hidden_id));
		$data		= array('page'	=> $page);

		break;
	case 'get_list':
        $count          = $_REQUEST['count'];
		$hidden         = $_REQUEST['hidden'];
        $operator       = $_REQUEST['operator'];
        
		if($operator == 0 || $operator == ''){
		    $operator_fillter = '';
		}else{
		    $operator_fillter = "AND `outgoing_campaign_detail`.`responsible_person_id` = $operator";
        }
        
        $db->setQuery(" SELECT      `outgoing_campaign_detail`.`id`,
                                    `outgoing_campaign_detail`.`id`,
                                    `outgoing_campaign`.`create_date`,
                                    `phone_base_detail`.`phone1`,
                                    `phone_base_detail`.`phone2`,
                                    CONCAT(phone_base_detail.`firstname`,' ',phone_base_detail.`lastname`),
                                    phone_base_detail.`pid`,
                                    scenario.`name`,
                                    user_info.`name`
                        FROM        `outgoing_campaign`
                        JOIN        outgoing_campaign_detail ON outgoing_campaign.id = outgoing_campaign_detail.outgoing_campaign_id
                        JOIN        phone_base_detail ON outgoing_campaign_detail.phone_base_detail_id = phone_base_detail.id
                        JOIN        scenario ON outgoing_campaign.scenario_id = scenario.id
                        LEFT JOIN   users ON outgoing_campaign_detail.responsible_person_id = users.id
                        LEFT JOIN   user_info ON users.id = user_info.user_id
                        WHERE       outgoing_campaign_detail.actived = 1 AND  outgoing_campaign_detail.`status` = $status $operator_fillter");
            
            $db->execQuery();

            $data = $db->getResultArray();
		
	  	// $rResult = mysql_query("SELECT  `outgoing_campaign_detail`.`id`,
	  	//                                 `outgoing_campaign_detail`.`id`,
	  	//                                 `outgoing_campaign`.`create_date`,
	  	//                                 `phone_base_detail`.`phone1`,
        //                 				`phone_base_detail`.`phone2`,
        //                 				CONCAT(phone_base_detail.`firstname`,' ',phone_base_detail.`lastname`),
        //                 				phone_base_detail.`pid`,
	  	//                                 scenario.`name`,
	  	//                                 user_info.`name`
        //                         FROM `outgoing_campaign`
        //                         JOIN outgoing_campaign_detail ON outgoing_campaign.id = outgoing_campaign_detail.outgoing_campaign_id
        //                         JOIN phone_base_detail ON outgoing_campaign_detail.phone_base_detail_id = phone_base_detail.id
	  	//                         JOIN scenario ON outgoing_campaign.scenario_id = scenario.id
	  	//                         LEFT JOIN users ON outgoing_campaign_detail.responsible_person_id = users.id
        //                         LEFT JOIN user_info ON users.id = user_info.user_id
        //                         WHERE outgoing_campaign_detail.actived = 1 AND  outgoing_campaign_detail.`status` = $status $operator_fillter");
		// $data = array(
		// 		"aaData"	=> array()
		// );

		// while ( $aRow = mysql_fetch_array( $rResult ) )
		// {
		// 	$row = array();
		// 	for ( $i = 0 ; $i < $count ; $i++ )
		// 	{
		// 		/* General output */
		// 		$row[] = $aRow[$i];
		// 		if($i == ($count - 1)){
		// 		    $row[] = '<div class="callapp_checkbox">
        //                           <input type="checkbox" id="callapp_checkbox_'.$aRow[$hidden].'" name="check_'.$aRow[$hidden].'" value="'.$aRow[$hidden].'" class="check" />
        //                           <label for="callapp_checkbox_'.$aRow[$hidden].'"></label>
        //                       </div>';
		// 		}
		// 	}
		// 	$data['aaData'][] = $row;
		// }
	
	    break;
    case 'save_incomming':
        $id			               = $_REQUEST['id'];
        $task_id			       = $_REQUEST['task_id'];
        $incomming_call_id         = $_REQUEST['incomming_call_id'];
        $task_status               = $_REQUEST['task_status'];
        $task_status_2             = $_REQUEST['task_status_2'];
        $task_branch               = $_REQUEST['task_branch'];
        $task_date                 = $_REQUEST['task_date'];
        $task_start_date           = $_REQUEST['task_start_date'];
        $task_end_date             = $_REQUEST['task_end_date'];
        $task_description          = $_REQUEST['task_description'];
        $task_position             = $_REQUEST['task_position'];
        $task_recipient            = $_REQUEST['task_recipient'];
        $task_note                 = $_REQUEST['task_note'];
        $site                      = $_REQUEST['site'];
        $task_phone                = $_REQUEST['task_phone'];
        $finis_colum               = '';
        $finis_date                = '';
        $finish                    = '';

        
        if ($task_status == 74 || $task_status == 77) {
            $finis_colum = 'finish_date,';
            $finis_date = 'NOW(),';
            
            $finish = 'finish_date=NOW(),';
        }
        if($id == ''){
            //insert task
            $db->setQuery("INSERT INTO `task`
            		        (`id`,
                            `user_id`,
                            `incomming_call_id`,
                            `task_recipient_id`,
                            `task_date`,
                            `task_start_date`,
                            `task_end_date`,
                            $finis_colum
                            `task_description`,
                            `task_type_id`,
                            `task_note`,
                            `task_status_id`,
                            `task_status_id_2`,
                            `phone`,
                            `task_branch_id`,
                            `cat_1`,
                            `cat_1_1`,
                            `cat_1_1_1`,
                            `info_comment`)
            		    VALUES
                            ('$task_id',
                            '$user_id',
                            '$incomming_call_id',
                            '$task_recipient',
                            NOW(),
                            $finis_date
                            '$task_start_date',
                            '$task_end_date',
                            '$task_description',
                            '1',
                            '$task_note',
                            '$task_status',
                            '$task_status_2',
                            '$task_phone',
                            '$task_branch',
                            '$incomming_cat_1',
                            '$incomming_cat_1_1',
                            '$incomming_cat_1_1_1',
                            '$info_comment');");
            $db->execQuery();
            
            $site_array = explode(",",$site);
            $db->setQuery("DELETE FROM task_site WHERE task_id = '$task_id'");
            $db->execQuery();
            
//            foreach ($site_array AS $site_id){
//                $db->setQuery("INSERT INTO task_site (task_id, site_id) values ('$task_id', $site_id)");
//                $db->execQuery();
//            }
            
            
            
            $db->setQuery("SELECT task_status.`name`
                           FROM   task_status
                           WHERE  actived = 1 AND id = '$task_status'");
            
            $res_status  = $db->getResultArray();
            $status_name = $res_status[result][0][name];
            
            $db->setQuery("SELECT NOW() AS `time`;");
            $res = $db->getResultArray();

        $data = array('task_id' => $task_id, 'task_type' => 1, 'status_name' => get_task_status($task_status), 'time' => $res[result][0]['time']);

		} else {
            $db->setQuery("SELECT NOW() AS `time`;");
            $res = $db->getResultArray();
            $data = array('task_id' => $task_id, 'task_type' => 2, 'status_name' => get_task_status($task_status), 'time' => $res[result][0]['time']);
            //update task
            $db->setQuery("SELECT task_recipient_id 
                             FROM task 
                            WHERE id = '$id'");
            
            $old_recipient_req = $db->getResultArray();
            
            $old_recipient = $old_recipient_req[result][0]['task_recipient_id'];
            
            // if($old_recipient != $task_recipient){
                
            //     mysql_query("UPDATE `task_history`
			// 		            SET `actived` = 0
			// 		         WHERE  `task_id` = '$id'");
                
                
            // }
            $db->setQuery("SELECT `task_status_id_2` FROM `task` WHERE `id` = $id ");
            $res = $db->getResultArray();
            if ($res[result][0]['task_status_id_2'] == 34 && $task_status_2 !=34){
                $process_time = "`process_time` = NOW(),";
            }else{
                $process_time = "";
            }
            
            $db->setQuery(" SELECT user_id,
                                   task_start_date,
                    			   task_end_date,
                    			   task_status_id,
                    			   task_status_id_2,
                    			   task_branch_id,
                    			   task_recipient_id,
                    			   task_description,
                    			   info_comment
                            FROM  `task`
                            WHERE  id = $id AND actived = 1");
            
            $task_info   = $db->getResultArray();
            $tsk         = $task_info[result][0];
            $former_wiev = '';
            
            if ($tsk[task_start_date] != $task_start_date || $tsk[task_end_date] != $task_end_date || $tsk[task_status_id] != $task_status || $tsk[task_status_id_2] != $task_status_2 || $tsk[task_branch_id] != $task_branch || $tsk[task_recipient_id] != $task_recipient || $tsk[task_description] != $task_description || $tsk[info_comment] != $info_comment) {
                if ($tsk[user_id] == $user_id) {
                    $former_wiev = "`former_wiev` = 0,";
                }else{
                    $former_wiev = "`former_wiev` = 1,";
                }
                
            }
            
            $db->setQuery("SELECT task_recipient_id, user_info.`name`
                            FROM task 
                            left join user_info ON user_info.user_id = task_recipient_id
                            WHERE task.id = $id");

            $req = $db->getResultArray();
	


            $db->setQuery("UPDATE `task` 
                              SET  task_recipient_id = '$task_recipient', 
                                   task_start_date   = '$task_start_date',
                                   task_end_date     = '$task_end_date',
                                   $finish
                                   task_description  = '$task_description',
                                   task_note         = '$task_note',
                                   task_status_id    = '$task_status',
                                   task_status_id_2  = '$task_status_2',
                                   phone             = '$task_phone',
                                   task_position_id  = '$task_position',
                                   task_branch_id    = '$task_branch',
                                  `cat_1`            = '$incomming_cat_1',
                                  `cat_1_1`          = '$incomming_cat_1_1',
                                  `cat_1_1_1`        = '$incomming_cat_1_1_1',
                                   $process_time
                                   $former_wiev
                                  `info_comment`     = '$info_comment'
                            WHERE  id                = '$id'");
            $db->execQuery();
            
            $site_array = explode(",",$site);
            $db->setQuery("DELETE FROM task_site WHERE task_id = '$id'");
            $db->execQuery();
            
//            foreach ($site_array AS $site_id){
//                $db->setQuery("INSERT INTO task_site (task_id, site_id) values ('$id', $site_id)");
//                $db->execQuery();
//            }
        }
        break;
	case 'task_status_cat':
        $page		= getStatusTask_2($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);

        break;
    case 'task_branch_changed':
        $page		= get_task_recipient($_REQUEST['cat_id']);
        $data		= array('page'	=> $page);
        break;
    case 'task_recipient_changed':
        $page		= get_task_branch($_REQUEST['recipient_id']);
        $data		= array('page'	=> $page);
        break;
    case 'disable':
        $id      = $_REQUEST['id'];
        $user_id = $_SESSION['USERID'];
        $db->setQuery("UPDATE task set actived = 0 where `id` = '$id' AND task_recipient_id != '$user_id'");
        $db->execQuery();
        $db->setQuery("SELECT *
                       FROM   task
                       WHERE  id = $id AND task_recipient_id = '$user_id'");
        $check = $db->getNumRow();
        if ($check == 0) {
            $db->setQuery("UPDATE `task_history`
                            SET `actived` = 0
                         WHERE  `task_id` = '$id'");
            $db->execQuery();
        }
        
        break;
    case 'view':
	    $user 						= $_SESSION['USERID'];
	    $id   						= $_REQUEST['id'];
	    $check						= mysql_num_rows(mysql_query("SELECT * FROM task_history WHERE task_id = $id AND `user_id` = $user"));
	    if($check == 0){
	        mysql_query("INSERT INTO `task_history` 
                            (`user_id`, `datetime`, `task_id`, `status`, `actived`) 
                        VALUES
                            ('$user', NOW(), '$id', 1, 1)");
	    } else {
	        global $error;
	        $error = 'მოცემულ სიახლეს უკვე გაეცანით';
	    }
        break;
    case 'cat_2':
        $page		= get_cat_1_1($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);
    
        break;
    case 'cat_3':
        $page		= get_cat_1_1_1($_REQUEST['cat_id'],'');
        $data		= array('page'	=> $page);

    break;
    //chat comment cases
    case 'save_comment'  :
        $comment            = $_REQUEST['comment'];
        $incomming_call_id  = $_REQUEST['incomming_call_id'];
        $user_id	        = $_SESSION['USERID'];
        $db->setQuery(" INSERT INTO `chat_comment`
                                   (`user_id`, `incomming_call_id`, `parent_id`, `comment`, `datetime`, `active`)
                             VALUES
                                   ('$user_id', '$incomming_call_id', '0', '$comment', NOW(),'1');");
        $db->execQuery();

        $data = array('error' => 0, 'message' => 'added');

        break;
    case 'update_comment' :
        $comment    = $_REQUEST['comment'];
        $id         = $_REQUEST['id'];
        
        $db->setQuery("UPDATE `chat_comment`
                          SET `comment`    = '$comment'
                       WHERE  `id`        = $id");
        $db->execQuery();
        break;
    case 'delete_comment'  :
           $comment_id=$_REQUEST['comment_id'];
            
           $db->setQuery("UPDATE `chat_comment`
                             SET `active` = 0
                          WHERE  `id` = $comment_id");
           $db->execQuery();
        break;
    case 'get_add_question':
        $incomming_id   = $_REQUEST['incomming_call_id'];
        $task_id        = $_REQUEST['task_id'];
        $page		= Getquestion($incomming_id,$task_id);
        $data		= array('page'	=> $page);

        break;
    case 'get_list_info_project' :
        $count        = $_REQUEST['count'];
        $hidden       = $_REQUEST['hidden'];
        
        $id           = $_REQUEST['id'];
        
        $db->setQuery("SELECT   incomming_call_info.id,
                                incomming_call_info.id,  
                                'პროქტები' AS `name`,
                                CONCAT('პროექტი: ', directory__projects.project, ' კატეგორია: ', directory__projects.category, ' ხედი: ', directory__projects.`view`, ' ნომერი: ', directory__projects.number, ' სართული: ', directory__projects.floor, ' სექტორი: ', directory__projects.sector, ' საკადასტრო კოდი: ', directory__projects.cadr_code, ' საზაფხულო ფართი: ', directory__projects.saz_far, ' საცხოვრებელი ფართი: ', directory__projects.sacx_far, ' ფასი: ', directory__projects.price, ' აივანი: ', directory__projects.balcony, ' საერთო ფართი: ', directory__projects.saerto_far, ' სრული ფასი: ', directory__projects.full_price, ' განვადების ფასი: ', directory__projects.ganv_price, ' სტანდარტული წინასწარ: ', directory__projects.stand_winaswar, ' სტატუსი: ', directory__projects.`status`) AS `info`
                        FROM   `incomming_call_info`
                        -- JOIN   `incomming_call` ON incomming_call.id = `incomming_call_info`.incomming_call_id
                        JOIN   `directory__projects` ON `directory__projects`.`id` = `incomming_call_info`.`directory_row_id`
                        WHERE  `incomming_call_info`.`actived` = 1 AND `incomming_call_info`.`incomming_call_id` = $id");
        
        $data = $db->getList($count, $hidden);
            
    break;
    //chat comment cases end
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/
function get_task_branch($id){
    global $db;
    $data .= '';
    
    $db->setQuery("SELECT dep_id FROM user_info WHERE user_id = $id");
    $dep_id = $db->getResultArray();
    $db->setQuery(" SELECT `id`,
                           `name`
                    FROM    department
                    WHERE   actived = 1");
    
    $req = $db->getResultArray();
    $data .= '<option value="0">-------</option>';
    foreach($req[result] AS $res){
        if ($res[id] == $dep_id[result][0][dep_id]) {
            $data .= '<option selected="selected" value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function get_task_recipient($id){
    global $db;
	$where = '';
	if ($id != 0){
		$where ="AND user_info.dep_id = '$id'";
    }
	
	$data .= '';
	
	$db->setQuery(" SELECT users.`id`,
						   user_info.`name`
					FROM   user_info
					JOIN   users ON users.id = user_info.user_id
					WHERE  users.actived = 1
					$where");
	
	$req = $db->getResultArray();
	
	$data .= '<option value="0">-------</option>';
	
	foreach($req[result] AS $res){
		$data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
	}

	return $data;
}

function get_cat_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

function get_cat_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function get_cat_1_1_1($id,$child_id){
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}

function web_site($id, $check_site){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    $i = 0;
    foreach ($res[result] AS $value){
        if ($check_site == 2){
            $db->setQuery("SELECT id
                           FROM  `outgoing_call_site`
                           WHERE  outgoing_call_site.outgoing_call_id = '$id' AND site_id = '$value[id]'");
        }elseif ($check_site ==3){
            $db->setQuery("SELECT id
                           FROM  `incoming_call_site`
                           WHERE  incoming_call_site.incomming_call_id = '$id' AND site_id = '$value[id]'");
        }else{
            $db->setQuery("SELECT id
                           FROM  `task_site`
                           WHERE  task_site.task_id = '$id' AND site_id = '$value[id]'");
        }
        
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getclientstatus($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">აქტიური</option>
                  <option value="2">პასიური</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2" selected="selected">პასიური</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">აქტიური</option>
                  <option value="2">პასიური</option>';
    }
    
    return $data;
}

function get_s_u_sex($id){
    
    if($id == 1){
        $data .= '<option value="0">----</option>
                  <option value="1" selected="selected">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }elseif($id ==2){
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2" selected="selected">მამრობითი</option>';
    }else{
        $data .= '<option value="0">----</option>
                  <option value="1">მდედრობითი</option>
                  <option value="2">მამრობითი</option>';
    }
    
    return $data;
}

function getStatusTask($id){
    global $db;
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `task_status`
                   WHERE   `actived` = 1 AND id != 0 and parent_id = 0");
    
    $data = $db->getSelect($id);
    return $data;
}

function getStatusTask_2($id, $child_id){
    global $db;
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `task_status`
                   WHERE   `actived` = 1 AND id != 0 and parent_id != 0 and `parent_id` = '$id'");
    
    $data = $db->getSelect($child_id);
    return $data;
}

function get_user_select($id){
    global $db;
    $db->setQuery("SELECT  users.`id`,
						   user_info.`name`
				   FROM    user_info
				   JOIN    users on users.id = user_info.user_id");

    $data = $data = $db->getSelect($id);
	return $data;
}

function getUsers($id){
    global $db;
    $db->setQuery("SELECT `users`.`id`,
                          `user_info`.`name`
                   FROM   `users`
                   JOIN   `user_info` ON `users`.`id` = `user_info`.`user_id`
                   WHERE  `users`.`actived` = 1");

    $data = $db->getSelect($id);
    return $data;
}


//chat question functions
function get_user($id){
    $data   ='';
    global $db;
    $db->setQuery(" SELECT`username`
                    FROM `users`
                    WHERE id = $id AND actived = 1");
    
    $res    = $db->getResultArray();
    $data   = $res[result][0]['username'];
    return $data;
}

function get_comment($incomming_id, $task_id, $outgoing_id){
    global $db;
    $where ='';
    
    if($incomming_id != '' && $incomming_id != 0){
        $where = " AND incomming_call_id = '$incomming_id'";
    }elseif ($outgoing_id != '' && $outgoing_id == 0){
        $where = " AND outgoing_id = '$outgoing_id'";
    }else {
        $where = " AND task_id = '$task_id'";
    }
    $data="";
    $db->setQuery(" SELECT   `id`,
                             `user_id`,
                             `incomming_call_id`,
                             `comment`,
                             `datetime`
                    FROM     `chat_comment`
                    WHERE     parent_id = 0  AND active = 1  $where
                    ORDER BY `datetime` DESC");
    $req = $db->getResultArray();
    foreach($req['result'] AS $res){
            
        $data .='<div style="margin-top: 30px;">
                    <input type=hidden id="hidden_comment_id_'.$res['id'].'" value="'.$res['id'].'"/>
                    <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                    <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; "> '.$res['comment'].'</span>
                    <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                    <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg">
                    <br/><br/><span id="get_answer" class="'. $res['id'] .'"  style="color: #369; font-size:15px;     font-size: 11px;
                    margin-left: 10px;  ">პასუხის გაცემა</span>
                </div>
        <div style="margin-left: 50px; margin-top: 10px;">
            '.get_answers($res['id']).'
        </div>';
    }


    return $data;
}
function Getquestion($incomming_id,$task_id){
    $data = '
        <p>ახალი კომენატრი</p>
        <textarea id="add_ask" style="resize:none; width:480px; height:50px; margin-top:5px; display:inline-block;" rows="3"></textarea > <button style="top: -20px; height: 50px;" id="add_comment">დამატება</button>
            '.get_comment($incomming_id,$task_id, '').'
    ';
    return $data;
}
function get_answers($id){
    global $db;
    $data= '';
    
    $db->setQuery("SELECT `id`,
                          `user_id`,
                          `comment`,
                          `datetime`
                   FROM   `chat_comment`
                   WHERE   parent_id = '$id' AND active = 1");
    $req = $db->getResultArray();
    
    foreach($req[result] AS $res){
       $data .='<div>
                    <span style="color: #369; font-weight: bold;">'. get_user($res['user_id']) .'</span> '. $res['datetime'] .' <br><br>
                    <span style="border: 1px #D7DBDD solid; padding: 7px; border-radius:50px; background-color:#D7DBDD; ">'.$res['comment'].'</span>
                    <img id="edit_comment" my_id="'. $res['id'] .'" style="cursor:pointer; margin-left: 10px;margin-right: 10px;" class="cat_img cat_edit" alt="img" src="media/images/icons/edit.svg">
                    <img id="delete_comment" my_id="'. $res['id'] .'" style="cursor:pointer" class="cat_img cat_delete" alt="img" src="media/images/icons/delete.svg"><br/><br/>
                </div>';
    }
    return $data;
}
function GetPosition($id){
    global $db;
	$data = '';
    $db->setQuery(" SELECT `id`,
						   `person_position`
					FROM   `position`
					WHERE  `actived` = '1'");

	$data = $db->getSelect($id);

	return $data;
}
function get_select($id,$table,$row='name',$where='actived = 1'){
    global $db;
    $data = '<option value="0" >-------------</option>';
    
    $db->setQuery("SELECT `id`,
                          `$row`
                   FROM   `$table`
                   WHERE   $where");
    
    $data = $db->getSelect($id);
    
    return $data;
}
function GetUserGroup($user_id){
    global $db;
    $db->setQuery("SELECT `group_id`
                   FROM   `users`
                   WHERE  `id` = $user_id");
    
    $req = $db->getResultArray();
    
    return $req[result][0][group_id];
}

function Getincstatus($object_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT `id`,
                          `name`
				   FROM   `inc_status`
				   WHERE  `actived` = 1");
    
    $data = $db->getSelect($object_id);
    
    return $data;
}

function Getsource($object_id){
    global $db;
    $data = '';
    $db->setQuery("SELECT 	`id`,
                            `name`
				    FROM 	`source`
				    WHERE 	 actived=1");
    
    
    $data = $db->getSelect($object_id);
    
    return $data;
}

// FUNCTIONS

function get_status_1($id){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `tree`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}

function get_status_1_1($id, $child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
                           `name`
                   FROM    `tree`
                   WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    $data = '';
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req['result'] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}



/**
 * @param int $id
 */
function get_space_type($id){
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `space_type`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}


/**
 * @param int $id
 */
function get_inc_status($id){
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `inc_status`
					WHERE   `actived` = 1");

	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}


/**
 * @param int $id
 */
function get_company($id){
	global $db;

	$db->setQuery("SELECT 	`id`,
							`name`
					FROM    `company`
					WHERE   `actived` = 1 AND `disabled` = 0");

	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}


/**
 * @param int $id
 */
function get_lang($id){
	global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `user_language`
                   WHERE   actived = 1");
    
	$req = $db->getResultArray();
    $data = '';
	$data .= '<option value="0" selected="selected">----</option>';
	
    foreach ($req['result'] AS $res){
        if($res['id'] == 1){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
}

function Getincomming($hidden_id){
    global $db;
    //     $db->setQuery(" SELECT IFNULL(task.incomming_call_id,0) AS `incomming_call_id`,
    //                            IFNULL(task.outgoing_id,0) AS `outgoing_id`
    //                     from   task
    //                     where `task`.`actived` = 1
    //         AND   `task`.`id` =  '$hidden_id'");
    
    
    //     $res = $db->getResultArray();
    //     $phone_table ='task';
    //     $colum_list  = "";
    //     if($res[result][0]['incomming_call_id'] != 0 && $res[result][0]['incomming_call_id'] != ''){
    //         $phone_table = 'incomming_call';
        
    //         $colum_list  = "$phone_table.source_id,
    //                 	    $phone_table.client_user_id AS personal_pin,
    //                 	    $phone_table.client_pid AS personal_id,
    //                 	    $phone_table.web_site_id,
    //                 	    $phone_table.client_name,
    //                 	    $phone_table.client_satus,
    //                 	    $phone_table.client_sex,
    //                 	    $phone_table.client_mail,";
    
    //     }elseif ($res[result][0]['outgoing_id'] != 0 && $res[result][0]['outgoing_id'] != ''){
    //         $phone_table = 'outgoing_call';
    
    //         $colum_list  = "$phone_table.source_id,
    //                         $phone_table.client_user_id AS personal_pin,
    //                         $phone_table.client_pid AS personal_id,
    //                         $phone_table.web_site_id,
    //                         $phone_table.client_name,
    //                         $phone_table.client_satus,
    //                         $phone_table.client_sex,
    //                         $phone_table.client_mail,";
    //     }
    
    $db->setQuery("SELECT 	    `task`.`id`,
                            former_user.`name` as task_former_user,
                            CASE
                            WHEN NOT ISNULL(`task`.`outgoing_id`) THEN task.outgoing_id
                            WHEN NOT ISNULL(`task`.`incomming_call_id`) && task.incomming_call_id!=0 THEN task.incomming_call_id
                            WHEN (ISNULL(task.incomming_call_id) || task.incomming_call_id=0) && ISNULL(task.outgoing_id) THEN task.id
                            END AS from_site,
                            CASE
                            WHEN NOT ISNULL(task.outgoing_id) THEN 2
                            WHEN NOT ISNULL(task.incomming_call_id) && task.incomming_call_id!=0 THEN 3
                            WHEN (ISNULL(task.incomming_call_id) || task.incomming_call_id=0) && ISNULL(task.outgoing_id) THEN 1
                            END AS from_site_check,
                            task.incomming_call_id,
                            task.task_recipient_id,
                            task.task_date,
                            task.task_start_date,
                            task.task_end_date,
                            task.task_branch_id,
                            task.task_description,
                            task.task_note,
                            task.task_position_id,
                            task.task_status_id,
                            task.task_status_id_2,
                            IF(task.incomming_call_id=0, task.phone, incomming_call.phone) AS `task_phone`,
                            task.outgoing_id,
                            task.`user_id` AS task_user_id,
                            
                            incomming_call.id AS incomming_call_id,
                            incomming_call.`date` AS call_date,
                            incomming_call.`date` AS record_date,
                            IFNULL(asterisk_call_log.source,incomming_call.phone) AS `phone`,
                            incomming_call.ipaddres,
                            incomming_call.asterisk_incomming_id,
                            IF(NOT ISNULL(incomming_call.asterisk_incomming_id),
                            CASE
                            WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) <= incomming_call.processing_start_date
                            THEN SEC_TO_TIME(UNIX_TIMESTAMP(processing_end_date)-UNIX_TIMESTAMP(processing_start_date))
                            WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) > incomming_call.processing_start_date
                            AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) >= incomming_call.processing_end_date
                            THEN '00:00:00'
                            WHEN FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) > incomming_call.processing_start_date
                            AND FROM_UNIXTIME(UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time) < incomming_call.processing_end_date
                            THEN SEC_TO_TIME(UNIX_TIMESTAMP(incomming_call.processing_end_date) - (UNIX_TIMESTAMP(asterisk_call_log.call_datetime)+wait_time+talk_time))
                            END,
                            '00:00:00') AS `processed_time`,
                            user_info.`name` AS operator,
                            IF(ISNULL(asterisk_call_log.user_id),chat_user_name.`name`,user_info.`name`) AS operator,
                            IF(ISNULL(asterisk_call_log.user_id),chat.answer_user_id,asterisk_call_log.user_id ) as `user_id`,
                            incomming_call.cat_1,
                            incomming_call.cat_1_1,
                            incomming_call.cat_1_1_1,
                            incomming_call.status_1,
                            incomming_call.status_1_1,
                            incomming_call.space_type,
                            incomming_call.chat_language,
                            incomming_call.inc_status,
                            incomming_call.company,
                            incomming_call.call_content AS call_content,
                            incomming_call.source_id,
                            incomming_call.client_user_id AS personal_pin,
                            incomming_call.client_pid AS personal_id,
                            incomming_call.web_site_id,
                            incomming_call.client_name,
                            
                            incomming_call.client_comment,
                            incomming_call.out_comment,
                            incomming_call.vizit_location,
                            incomming_call.vizit_date,
                            incomming_call.fb_link,
                            incomming_call.viber_address,
                            incomming_call.phone1,
                            
                            incomming_call.client_satus,
                            incomming_call.client_sex,
                            incomming_call.client_mail,
                            incomming_call.client_birth_year,
                            incomming_call.chat_id,
                            incomming_call.lid,
                            incomming_call.note_comm,
                            incomming_call.lid_comment,
                            asterisk_call_log.source AS `ast_source`,
                            SEC_TO_TIME(asterisk_call_log.talk_time) AS `duration`,
                            SEC_TO_TIME(asterisk_call_log.wait_time) AS `wait_time`
                            
                    FROM 	 `task`
                    LEFT JOIN incomming_call on incomming_call.id = task.incomming_call_id
                    
                    LEFT JOIN   asterisk_call_log on asterisk_call_log.id = incomming_call.asterisk_incomming_id
                    LEFT JOIN   users ON users.id=asterisk_call_log.user_id
                    LEFT JOIN	asterisk_queue ON	asterisk_call_log.queue_id = asterisk_queue.id
                    LEFT JOIN   user_info ON users.id = user_info.user_id
                    LEFT JOIN   chat on incomming_call.chat_id = chat.id
                    LEFT JOIN   users AS chat_user ON chat_user.id=chat.answer_user_id
                    LEFT JOIN   user_info AS chat_user_name ON chat_user.id=chat_user_name.user_id
                    
                    
                    LEFT JOIN outgoing_call on task.outgoing_id = outgoing_call.id
                    LEFT JOIN user_info as former_user on former_user.`user_id` = task.`user_id`
                    WHERE 	 `task`.`actived` = 1 AND task.task_type_id = 1 AND `task`.`id` =  $hidden_id");
    
    $res = $db->getResultArray();
    return $res[result][0];
}
function GetPage($res){
    global $db;
    $user_id    = $_SESSION['USERID'];
    
    if ($res[task_user_id] == $user_id && $res[id] != '') {
        $db->setQuery("UPDATE task SET former_wiev = 0 WHERE id = '$res[id]'");
        $db->execQuery();
    }
    $user_gr    = GetUserGroup($user_id);
    $dis        = '';
    if($res == ''){
        $db->setQuery("INSERT INTO task
                               SET user_id = ''");
        $db->execQuery();
        $increment_id = $db->getLastId();
        $increment = $increment_id;
        $db->setQuery("DELETE FROM task WHERE id = $increment_id");
        $db->execQuery();
        
    } else {
        $increment = $res['id'];
    }
    
    $user_id	                = $_SESSION['USERID'];
    if($res == ''){
        $data .= "<script>
                    setTimeout(() => {
                        $('#view').hide();
                    }, 50);
                </script>";
        $wiev_status=0;
    } else if($res['task_recipient_id'] != $user_id && $user_id!=2){
        $data .= "<script>
                    setTimeout(() => {
                        $('#view').hide();
                    }, 50);
                </script>";
        $wiev_status=0;
    }else if($res['task_recipient_id'] == $user_id){
        $wiev_status=1;
        $dis='disabled="disabled"';
    }
    if ($res[from_site_check] == 2 || $res[from_site_check] == 3) {
        $disable_site = 'disabled="disabled"';
    }else{
        $disable_site = '';
    }
    if ($res[lid] == 1) {$lid_check='checked';}
	$data  .= '
         <div id="dialog-form">
            <div class="dialog-style">
        		<input type="hidden" value="'.$res['incomming_call_id'].'" id="incomming_call_id">
                <input type="hidden" value="'.$res['incomming_call_id'].'" id="hidden_incomming_call_id">
                <input type="hidden" value="'.$res['outgoing_id'].'" id="outgoing_id">
                <input type="hidden" value="'.$res['incomming_call_id'].'" id="incomming_id">
                <input type="hidden" value="'.$res['user_id'].'" id="hidden_user_id">
                <input type="hidden" value="'.$res['id'].'" id="hidden_id">

    			<div class="communication_fieldset_style">
                     <div id="side_menu" class="communication_side_menu">
                        <span class="task communication_side_menu_button"  onclick="show_right_side(\'task\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_task.png" alt="24 ICON"  title="დავალება"></span>
            			
                        <span class="info communication_side_menu_button" onclick="show_right_side(\'info\')"><img class="communication_side_menu_img"  style="filter: brightness(0.1); border-bottom: 2px solid rgb(51, 51, 51);" src="media/images/icons/lemons_info.png" alt="24 ICON" title="ინფო"></span>
                  
                        <span class="client_history communication_side_menu_button"  onclick="show_right_side(\'client_history\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_client_history.png" alt="24 ICON"  title="ისტორია"></span>
                        				
                        <span class="client_conversation communication_side_menu_button"  onclick="show_right_side(\'client_conversation\')"><img class="communication_side_menu_img" src="media/images/icons/client_conversation.png" alt="24 ICON"  title="ხშირად დასმული კითხვები"></span>
    
        				<span class="sms communication_side_menu_button" onclick="show_right_side(\'sms\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_sms.png" alt="24 ICON" title="ესემეს"></span>
        				
        				<span class="incomming_mail communication_side_menu_button" onclick="show_right_side(\'incomming_mail\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_mail.png" alt="24 ICON" title="ელ. ფოსტა"></span>
        
                        <span class="chat_question communication_side_menu_button" onclick="show_right_side(\'chat_question\')"><img class="communication_side_menu_img" src="media/images/icons/chat_question.png" alt="24 ICON" title="კომენტარები"></span>
        			    
                        <span style="display:none" class="newsnews communication_side_menu_button" onclick="show_right_side(\'newsnews\')"><img class="communication_side_menu_img" src="media/images/icons/newsnews.png" alt="24 ICON" title="სიახლეები"></span>
                        
                        <span class="record communication_side_menu_button" onclick="show_right_side(\'record\')"><img class="communication_side_menu_img" src="media/images/icons/inc_record.png" alt="24 ICON"  title="ჩანაწერები"></span>
                            				
                        <span class="incomming_file communication_side_menu_button" onclick="show_right_side(\'incomming_file\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_files.png" alt="24 ICON"  title="ფაილები"></span>
        				
                        <span class="user_logs communication_side_menu_button" onclick="show_right_side(\'user_logs\')"><img class="communication_side_menu_img" src="media/images/icons/lemons_logs.png" alt="24 ICON"  title="ლოგები"></span>
    
                        <span style="float: right; margin-right: 10px;" class="call_resume communication_side_menu_button" onclick="show_right_side(\'call_resume\')"><img class="communication_side_menu_img" src="media/images/icons/inc_resume.png" alt="24 ICON"  title="ლოგები"></span>
                       
        			 </div>
    			 </div>
    			 <div class="communication_right_side" id="right_side">
                    <ff style="height: 656px;" id="task">
                       <table class="dialog-form-table" style="width: 96%;">
                           <tr style="height:0px;">
                               <td>დამფორმირებელი</td>
                               <td>ტელეფონის ნომერი</td>
                               
                           </tr
                           <tr style="height:5px;"></tr>
                           <tr style="height:0px;">
                               <td>'.$res['task_former_user'].'</td>
                               <td><input class="idle" id="task_phone"   style="display: inline; width:270px" type="text" value="'.$res['task_phone'].'" '.$dis.'></td>
                           </tr>
                           <tr style="height:10px;"></tr>
                	       <tr style="height:0px;">
                               <td><label for="task_id">მომართვის №</label></td>
                               <td><label for="task_date">შექმნის თარიღი</label></td>
                		   </tr>
                           <tr style="height:0px;">
                               <td><input id="task_id" style="width:270px" type="text" value="'.$increment.'" readonly></td>
                               <td><input class="idle" style="width:270px" id="task_date" type="text" value="'.(($res['task_date']=='')?date("Y-m-d H:i:s"):$res['task_date']).'" readonly></td>
                		   </tr>
                           <tr style="height:5px;"></tr>
                		   <tr style="height:0px;">
                                <td><label>პერიოდი</label></td>
                		   </tr>
                           <tr style="height:0px;">
                                <td><input class="idle" id="task_start_date" style="display: inline; width:240px" type="text" value="'.$res['task_start_date'].'" '.$dis.'><span style="vertical-align: unset;"> -დან</span></td>
                                <td><input class="idle" id="task_end_date"   style="display: inline; width:240px" type="text" value="'.$res['task_end_date'].'" '.$dis.'><span style="vertical-align: unset;"> -მდე</span></td>
                		   </tr>
                           <tr style="height:5px;"></tr>
                           <tr style="height:0px;">
                               <td><label for="task_status">სტატუსი</label></td>
                               <td><label for="task_status_2">სტატუსი 2</label></td>
                		   </tr>
                           <tr style="height:0px;">
                                <td><select id="task_status" style=" width:270px" data-select="chosen">'.getStatusTask($res['task_status_id']).'</select></td>
                                <td><select id="task_status_2" style=" width:270px" data-select="chosen">'.getStatusTask_2($res['task_status_id'],$res['task_status_id_2']).'</select></td>
                		   </tr>
                           <tr style="height:5px;"></tr>
                           <tr style="height:0px;">
                                <td><label for="task_branch">განყოფილება</label></td>
                                <td><label for="task_recipient">დავალების მიმღები</label></td>
                		   </tr>
                           <tr style="height:0px;">
                                <td><select id="task_branch" style="width:270px" data-select="chosen">'.get_select($res['task_branch_id'],'department').'</select></td>
                                <td><select id="task_recipient" style=" width:270px" data-select="chosen">'.get_user_select($res['task_recipient_id']).'</select></td>
                           </tr>
                           <tr style="height:5px;"></tr>
                	       <tr style="height:0px;">
                               <td colspan="2"><label for="task_description">კომენტარი</label></td>
                	       </tr>
                           <tr style="height:0px;">
                               <td colspan="2"><textarea '.($user_gr == 1 || $res[task_user_id] = $user_id ? '' : 'readonly').' style="width: 590px; padding: 3px 7px; height:69px;" id="task_description" >'.$res['task_description'].'</textarea></td>
                           </tr>
                           <tr style="height:5px;"></tr>
                	       <tr style="height:0px;">
                               <td colspan="2"><label for="task_note">შედეგი</label></td>
                	       </tr>
                           <tr style="height:0px;">
                               <td colspan="2"><textarea style="height:69px; padding: 3px 7px; width: 590px;" id="task_note" >'.$res['task_note'].'</textarea></td>
                           </tr>
                       </table>
                    </ff>
    				<ff style="display:none;" class="communication_right_side_info" id="info">';
    
    					
    					$db->setQuery("	SELECT id,name
    									FROM processing_fieldset
    									WHERE actived = '1' AND id IN (SELECT fieldset_id FROM processing_fieldset_by_setting WHERE processing_setting_id='4')");
    					$fieldsets = $db->getResultArray();
    					$db->setQuery("	SELECT 		incomming_request_processing.id,
    											incomming_request_processing.processing_setting_detail_id AS 'input_id',
    											incomming_request_processing.`value`,
    											incomming_request_processing.int_value,
    											processing_setting_detail.processing_field_type_id AS 'type'
    									
    									FROM 		incomming_request_processing
    									LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id
    					    WHERE 		incomming_request_processing.incomming_request_id='$res[incomming_call_id]' AND incomming_request_processing.processing_setting_detail_id != 0
    									
    									GROUP BY incomming_request_processing.processing_setting_detail_id");
    					$values = $db->getResultArray();
    					$saved_values = $values['result'];
    
    					$db->setQuery("	SELECT 		incomming_request_processing.id,
    												incomming_request_processing.processing_setting_detail_id AS 'input_id',
    												incomming_request_processing.`value`,
    												incomming_request_processing.int_value,
    												processing_setting_detail.processing_field_type_id AS 'type',
    												incomming_request_processing.additional_deps
    										
    									FROM 		incomming_request_processing
    									LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id
    					    WHERE 		incomming_request_processing.incomming_request_id='$res[incomming_call_id]' AND incomming_request_processing.processing_setting_detail_id = 0 AND incomming_request_processing.additional_deps = 0");
    					$deps = $db->getResultArray();
    
    					$dep_val = 0;
    					$dep_project_val = 0;
    					$dep_sub_project_val = 0;
    					$dep_project_type_val = 0;
    					$dep_rand_n = 0;
    					foreach($deps['result'] AS $item){
    						if($item['value'] == 1){
    							$dep_val = $item['int_value'];
    						}
    						if($item['value'] == 2){
    							$dep_project_val = $item['int_value'];
    						}
    						if($item['value'] == 3){
    							$dep_sub_project_val = $item['int_value'];
    						}
    						if($item['value'] == 4){
    							$dep_project_type_val = $item['int_value'];
    						}
    						
    					}
    					$counter = 1;
    
    
    
    
    					///field notification block
    					$db->setQuery("	SELECT 	COUNT(*) AS 'cc'
    									FROM 	mepa_project_inputs
    									WHERE 	cat_id = '$dep_project_val' AND actived = 1");
    					$field_count = $db->getResultArray();
    
    					$db->setQuery("	SELECT 	region_selector
    									FROM 	info_category
    									WHERE 	id = '$dep_project_val'");
    					$regions_on = $db->getResultArray();
    					$regions_on = $regions_on['result'][0]['region_selector'];
    
    					if($regions_on == 1){
    						$regions_on = 4;
    					}
    
    					$db->setQuery("	SELECT
    										incomming_request_processing.id,
    										incomming_request_processing.processing_setting_detail_id AS 'input_id',
    										incomming_request_processing.`value`,
    										incomming_request_processing.int_value,
    										processing_setting_detail.processing_field_type_id AS 'type' 
    									FROM
    										incomming_request_processing
    										LEFT JOIN processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id 
    									WHERE
    					    incomming_request_processing.incomming_request_id = '$res[incomming_call_id]' 
    										AND incomming_request_processing.additional_dialog_id = '0'
    										AND incomming_request_processing.processing_setting_detail_id != 0
    										AND incomming_request_processing.processing_setting_detail_id != 999999999");
    					$ready_fields = $db->getResultArray();
    
    					$db->setQuery("	SELECT
    										incomming_request_processing.id,
    										incomming_request_processing.processing_setting_detail_id AS 'input_id',
    										incomming_request_processing.`value`,
    										incomming_request_processing.int_value,
    										processing_setting_detail.processing_field_type_id AS 'type' 
    									FROM
    										incomming_request_processing
    										LEFT JOIN processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id 
    									WHERE
    					    incomming_request_processing.incomming_request_id = '$res[incomming_call_id]' 
    										AND incomming_request_processing.additional_dialog_id = '0'
    										AND incomming_request_processing.processing_setting_detail_id != 0
    										AND incomming_request_processing.processing_setting_detail_id = 999999999
    										AND incomming_request_processing.int_value != 0");
    					$regions_cc = $db->getResultArray();
    					$fields_cc = $field_count['result'][0]['cc'] + $regions_on;
    
    					$r_fields = $ready_fields['count'] + $regions_cc['count'];
    					////////////////////////////
    					foreach($fieldsets['result'] AS $fieldset){
    						if($counter == 2){
    							$data .= '
    							<fieldset id="dep_arrival">
    								<legend>მომართვა</legend>
    								<div class="fieldset_row">
    									<div class="col-3-half">
    										<label>უწყება</label>
    										<select id="dep_0">
    										'.get_dep_1($dep_val).'
    										</select>
    									</div>
    									<div class="col-3-half">
    										<label>პროექტი</label>
    										<select id="dep_project_0">
    										'.get_dep_1_1($dep_val,$dep_project_val).'
    										</select>
    									</div>
    									<div class="col-3-half">
    										<label>ქვე პროექტი</label>
    										<select id="dep_sub_project_0">
    										'.get_dep_1_1_1($dep_project_val,$dep_sub_project_val).'
    										</select>
    									</div>
    									<div class="col-3-half">
    										<label>მომართვის ტიპი</label>
    										<select id="dep_sub_type_0">
    										'.get_project_type($dep_project_type_val).'
    										</select>
    									</div>
    									<div class="col-3-half" style="display: flex;">
    										<img style="margin-right:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/add2.png" id="add_dep">
    										<div id="get_dep_additional" style="position: relative;">
    											<div class="field_notifier">
    												<p><span id="field_done">'.$r_fields.'</span>/<span id="field_no_done">'.$fields_cc.'</span></p>
    											</div>
    											<img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/additional_inputs.png">
    										</div>
    										<img id="get_docs_project" style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/documents.png">
    										<img id="get_contact_project" style="margin-left:1px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/contacts.png">
    									</div>
    								</div>';
    							$db->setQuery("	SELECT 			*
    												
    											FROM 				incomming_request_processing
    											LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id
    							    WHERE 			incomming_request_processing.incomming_request_id='$res[incomming_call_id]' AND incomming_request_processing.processing_setting_detail_id = 0 AND incomming_request_processing.additional_deps != 0
    											
    											GROUP BY incomming_request_processing.additional_deps");
    
    							$peps = $db->getResultArray();
    							$db->setQuery("	SELECT 			*
    												
    											FROM 				incomming_request_processing
    											LEFT JOIN 	processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id
    							    WHERE 			incomming_request_processing.incomming_request_id='$res[incomming_call_id]' AND incomming_request_processing.processing_setting_detail_id = 0 AND incomming_request_processing.additional_deps != 0");
    
    							$peps2 = $db->getResultArray();
    							for($o=0;$o<$peps['count'];$o++){
    								$dep_val = 0;
    								$dep_project_val = 0;
    								$dep_sub_project_val = 0;
    								$dep_project_type_val = 0;
    								$dep_rand_n = 0;
    								foreach($peps2['result'] AS $item){
    									if($item['value'] == 1){
    										$dep_val = $item['int_value'];
    									}
    									if($item['value'] == 2){
    										$dep_project_val = $item['int_value'];
    									}
    									if($item['value'] == 3){
    										$dep_sub_project_val = $item['int_value'];
    									}
    									if($item['value'] == 4){
    										$dep_project_type_val = $item['int_value'];
    									}
    									$dep_rand_n = $item['additional_deps'];
    								}
    
    								///field notification block
    								$db->setQuery("	SELECT COUNT(*) AS 'cc'
    												FROM mepa_project_inputs
    												WHERE cat_id = '$dep_project_val' AND actived = 1");
    								$field_count = $db->getResultArray();
    
    								$db->setQuery("	SELECT 	region_selector
    												FROM 	info_category
    												WHERE 	id = '$dep_project_val'");
    								$regions_on = $db->getResultArray();
    								$regions_on = $regions_on['result'][0]['region_selector'];
    
    								if($regions_on == 1){
    									$regions_on = 4;
    								}
    
    								$db->setQuery("	SELECT
    													incomming_request_processing.id,
    													incomming_request_processing.processing_setting_detail_id AS 'input_id',
    													incomming_request_processing.`value`,
    													incomming_request_processing.int_value,
    													processing_setting_detail.processing_field_type_id AS 'type' 
    												FROM
    													incomming_request_processing
    													LEFT JOIN processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id 
    												WHERE
    								    incomming_request_processing.incomming_request_id = '$res[incomming_call_id]' 
    													AND incomming_request_processing.additional_dialog_id = '$dep_rand_n'
    													AND incomming_request_processing.processing_setting_detail_id != 0
    													AND incomming_request_processing.processing_setting_detail_id != 999999999");
    								$ready_fields = $db->getResultArray();
    
    								$db->setQuery("	SELECT
    													incomming_request_processing.id,
    													incomming_request_processing.processing_setting_detail_id AS 'input_id',
    													incomming_request_processing.`value`,
    													incomming_request_processing.int_value,
    													processing_setting_detail.processing_field_type_id AS 'type' 
    												FROM
    													incomming_request_processing
    													LEFT JOIN processing_setting_detail ON processing_setting_detail.id = incomming_request_processing.processing_setting_detail_id 
    												WHERE
    								    incomming_request_processing.incomming_request_id = '$res[incomming_call_id]' 
    													AND incomming_request_processing.additional_dialog_id = '$dep_rand_n'
    													AND incomming_request_processing.processing_setting_detail_id != 0
    													AND incomming_request_processing.processing_setting_detail_id = 999999999
    													AND incomming_request_processing.int_value != 0");
    								$regions_cc = $db->getResultArray();
    								$fields_cc = $field_count['result'][0]['cc'] + $regions_on;
    
    								$r_fields = $ready_fields['count'] + $regions_cc['count'];
    								////////////////////////////
    
    								$data .= '<div class="fieldset_row" id="dep_row_'.$dep_rand_n.'">
    								<div class="col-3-half">
    									<label>უწყება</label>
    									<select style="width: 100px;" id="dep_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep">
    									'.get_dep_1($dep_val).'
    									</select>
    								</div>
    								<div class="col-3-half">
    									<label>პროექტი</label>
    									<select style="width: 100px;" id="dep_project_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep_project">
    									'.get_dep_1_1($dep_val,$dep_project_val).'
    									</select>
    								</div>
    								<div class="col-3-half">
    									<label>ქვე პროექტი</label>
    									<select style="width: 100px;" id="dep_sub_project_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep_sub_project">
    									'.get_dep_1_1_1($dep_project_val,$dep_sub_project_val).'
    									</select>
    								</div>
    								<div class="col-3-half">
    									<label>მომართვის ტიპი</label>
    									<select style="width: 100px;" id="dep_sub_type_'.$dep_rand_n.'" data-id="'.$dep_rand_n.'" class="dep_sub_type">
    									'.get_project_type($dep_project_type_val).'
    									</select>
    								</div>
    								<div class="col-3-half" style="display: flex;">
    								<div class="delete_dep" data-id="'.$dep_rand_n.'"><i style="font-size:28px; margin-top:20px; margin-left:0px;cursor:pointer;" class="fa fa-minus-circle" aria-hidden="true"></i></div>
    									
    									<div id="get_dep_additional_addit" style="position: relative;" data-id="'.$dep_rand_n.'">
    										<img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/additional_inputs.png">
    										<div class="field_notifier">
    											<p><span id="field_done">'.$r_fields.'</span>/<span id="field_no_done">'.$fields_cc.'</span></p>
    										</div>
    									</div>
    									<img style="margin-left:3px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/documents.png">
    									<img style="margin-left:1px;border: none;margin-top: 21px; height:25px; cursor:pointer;" src="media/images/icons/contacts.png">
    									
    									
    								</div>
    							</div>';
    							}
    							$data .= '</fieldset>';
    						}
    						$inputs_by_tabs = array();
    						$db->setQuery("	SELECT	id,name
    										FROM 	processing_input_tabs
    										WHERE 	actived = 1 AND field_id = '$fieldset[id]'");
    						$tabs = $db->getResultArray();
    						$data .='	<fieldset class="communication_right_side_info">
    										<legend>'.$fieldset[name].'</legend>';
    										if($tabs['count'] > 1){
    											$data .= '<ul class="dialog_input_tabs" id="dialog-tab-'.$fieldset[id].'">';
    											$tab_count = 0;
    											foreach($tabs['result'] AS $tab){
    												if($tab_count == 0){
    													$data .= '<li aria-selected="true" data-id="'.$tab['id'].'">'.$tab['name'].'</li>';
    												}
    												else{
    													$data .= '<li data-id="'.$tab['id'].'">'.$tab['name'].'</li>';
    												}
    												$tab_count++;
    												$inputs_by_tabs[] = $tab['id'];
    											}
    											$data .= '</ul>';
    										}
    										else{
    											$inputs_by_tabs[] = 1;
    										}
    											
    											$data .= '<div class="fieldset_row">
    												'.get_fieldset_inputs($fieldset[id],$saved_values,$inputs_by_tabs,$res[phone]).'
    											</div>
    									</fieldset>';
    						$counter++;
    					}
        				
                     $data .='</ff>
                     
                     
                     <ff style="display:none; height: 656px;" id="client_history">
                        <table style="margin-left: 7px;">
                            <tr style="height:0px;">
                                <td colspan="2" style="text-align: center;">პერიოდი</td>
                                <td><label>ტელეფონი</label></td>
                                <td></td>
                            </tr>
                            <tr style="height:0px;">
                                <td style="width: 120px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_from" style="width: 110px;" value="'.date('Y-m-d', strtotime('-7 day')).'"></td>
                                <td style="width: 120px;"><input class="callapp_filter_body_span_input date_input" type="text" id="date_to" style="width: 110px;" value="'.date('Y-m-d').'"></td>
                                <td style="width: 140px;"><input class="callapp_filter_body_span_input" type="text" id="client_history_user_id" style="width: 130px;" value="'.$person_pin.'"></td>
                                <td><button id="search_client_history" style="margin-top: -6px;">ძებნა</button></td>
                            </tr>
                        </table>
                    
                        <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                            <table class="display" id="table_history" style="width: 100%">
                                <thead>
                                    <tr id="datatable_header">
                                        <th>ID</th>
                                        <th style="width: 20%;">თარიღი</th>
                    					<th style="width: 20%">ტელეფონის ნომერი</th>
                    					<th style="width: 20%;">ოპერატორი</th>
                                        <th style="width: 20%;">კომენტარი</th>
                                        <th style="width: 20%;">სტატუსი</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr class="search_header">
                                        <th class="colum_hidden">
                                    	   <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_phone" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_category" value="ფილტრი" class="search_init" />
                                        </th>
            	                        <th>
                                            <input style="width: 97%;" type="text" name="search_category" value="ფილტრი" class="search_init" />
                                        </th>
                                    </tr>
                  				</thead>
              				</table>
                        </div>
                     </ff>
                     <ff style="display:none; height: 656px;" id="call_resume">
                     <table style="margin-left: 7px;" width="100%" class="dialog-form-table">
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ოპერატორი</label></td>
							<td style="width: 205px;"><label for="d_number">თარიღი</label></td>
                            <td style="width: 205px;"><label for="d_number">მომართვის ნომერი</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="my_operator_name" class="idle" value="'.$res[operator].'" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_datetime" class="idle" value="'.$res[call_date].'" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="incomming_call_id" class="idle" value="'.$res['incomming_call_id'].'" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        <tr style="height:0px">
							<td style="width: 205px;"><label for="d_number">ლოდინის დრო</label></td>
							<td style="width: 205px;"><label for="d_number">საუბრის ხანგრძლივობა</label></td>
                            <td style="width: 205px;"><label for="d_number">დამუშავების ხანგრძლივობა</label></td>
						</tr>
						<tr style="height:0px">
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id="call_wait_time" class="idle" value="'.$res[wait_time].'" disabled="disabled"/>
                            </td>
							<td>
								<input style="height: 18px; width: 160px;" type="text" id="call_duration" class="idle" value="'.$res[duration].'" disabled="disabled"/>
                            </td>
                            <td>
								<input style="height: 18px; width: 160px;" type="text" id=call_done_date" class="idle" value="'.$res[processed_time].'" disabled="disabled"/>
                            </td>
                        </tr>
                        <tr style="height:15px;"></tr>
                        
                    </table>
                 </ff>
                     <ff style="display:none; height: 656px;" id="incomming_comment">
                        <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                            
                        </div>
                     </ff>
                     <ff style="display:none; height: 656px;" id="incomming_mail">
                        
                        <div style="margin-left: 7px;" id="task_button_area">
                            <button style="display:none;" id="add_mail">ახალი E-mail</button>
                        </div>
                         <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                            <table class="display" id="table_mail" >
                                <thead>
                                    <tr id="datatable_header">
                                        <th>ID</th>
                                        <th style="width: 20%;">თარიღი</th>
                                        <th style="width: 25%;">ადრესატი</th>
                                        <th style="width: 35%;">გზავნილი</th>
                                        <th style="width: 20%;">სტატუსი</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr class="search_header">
                                        <th class="colum_hidden">
                                    	    <input type="text" name="search_id" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                        	<input type="text" name="search_number" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 95%;"/>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
        	            </div>
                     </ff>
                     <ff style="display:none; height: 656px;" id="client_conversation">
                        <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
            	           <table class="display" id="table_question" style="width:100%">
                                <thead>
                                    <tr id="datatable_header">
                                        <th>ID</th>
                                        <th style="width: 20%;">თარიღი</th>
                                        <th style="width: 20%;">პროექტი</th>
                                        <th style="width: 20%;">კითხვა</th>
                                        <th style="width: 40%;">პასუხი</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr class="search_header">
                                        <th class="colum_hidden">
                                    	    <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input style="width: 99%;" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                     </ff>
                     <ff style="display:none; height: 424px;  overflow-y: auto;" id="chat_question">
                        <fieldset>
                            <legend>ახალი კომენატრი</legend>
                            <textarea id="add_ask" style="resize:none; width:480px; height:50px; margin-top:5px; display:inline-block;" rows="3">
                            </textarea> 
                            <button style="height: 50px;top: -22px;display: inline-block;position: relative;margin-left: 12px;" id="add_comment">დამატება</button>
                                '.get_comment($res['incomming_call_id'],$res['id'], $res['outgoing_id']).'
                            <input type=hidden id="comment_incomming_id" value="'.(($res['id']=='')?$increment:$res['id']).'"/>
                           
                        </fieldset>
                     </ff>
                     <ff style="display:none; height: 656px;" id="user_logs">
                         <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
                             <table class="display" id="user_log_table">
                                <thead>
                                    <tr style="font-size: 11px;" id="datatable_header">
                                        <th>ID</th>
                                        <th style="width:20%">თარიღი</th>
                                        <th style="width:15%">ქმედება</th>
                                        <th style="width:12%">მომხმა<br>რებელი</th>
                                        <th style="width:15%">ველი</th>
                                        <th style="width:19%">ახალი მნიშვნელობა</th>
                                        <th style="width:19%">ძველი მნიშვნელობა</th>
                                    </tr>
                                </thead>
                                <thead >
                                    <tr class="search_header">
                                        <th class="colum_hidden">
                                            <input type="text" name="search_id" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_number" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                        <th>
                                            <input style="width:97%" type="text" name="search_date" value="ფილტრი" class="search_init" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                     </ff>
                     
    				 
    				<ff style="display:none; height: 656px;" id="sms">
    					
                        <div id="task_button_area" style="margin-left: 7px;">
    						<button style="display:none;" id="add_sms_phone" class="jquery-ui-button new-sms-button" data-type="phone">ტელეფონის ნომრით</button>
    					</div>
                        <div style="width: 593px; margin-left: 7px; overflow-x: scroll; height: 465px; overflow-x: auto;">
        					<table class="display" id="table_sms" >
        						<thead>
        							<tr id="datatable_header">
        								<th>ID</th>
        								<th style="width: 20%;">თარიღი</th>
        								<th style="width: 15%;">ადრესატი</th>
        								<th style="width: 50%;">ტექსტი</th>
        								<th style="width: 15%;">სტატუსი</th>
        							</tr>
        						</thead>
        						<thead>
        							<tr class="search_header">
        								<th class="colum_hidden">
        								<input type="text" name="search_id" value="ფილტრი" class="search_init"/>
        								</th>
        								<th>
        									<input type="text" name="search_number" value="ფილტრი" class="search_init"/>
        								</th>
        								<th>
        									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
        								</th>
        								<th>
        									<input type="text" name="search_date" value="ფილტრი" class="search_init"/>
        								</th>
        								<th>
        									<input type="text" name="search_date" value="ფილტრი" class="search_init" style="width: 97%;"/>
        								</th>
        							</tr>
        						</thead>
        					</table>
    					</div>
    				</ff>
                    <ff style="display:none; height: 656px;" id="incomming_file">
                        <fieldset >
                            <legend>ფაილი</legend>
                                '.show_file($res).'
                        </fieldset>
                    </ff>
                    
                    <ff style="display:none;" id="record">
                        <fieldset >
                            <legend>ჩანაწერები</legend>
                                '.show_record($res).'
                        </fieldset>
                    </ff>
                </div>
	 		</div>
	 </div>';
            
    return $data;
}

function GetSmsSendPage() {
    $data = '
        <div id="dialog-form">
            <fieldset style="width: 299px;">
					<legend>SMS</legend>
			    	<table class="dialog-form-table">
						<tr>
							<td><label for="d_number">ადრესატი</label></td>
						</tr>
			    		<tr>
							<td>
								<span id="errmsg" style="color: red; display: none;">მხოლოდ რიცხვი</span>
								<input type="text" id="sms_phone"  value="">
							</td>
							<td>
								<button id="copy_phone">Copy</button>
							</td>
							<td>
								<button id="sms_shablon">შაბლონი</button>
							</td>
						</tr>
						<tr>
							<td><label for="content">ტექსტი</label></td>
						</tr>
					
						<tr>
							
							<td colspan="6">	
								<textarea maxlength="150" style="width: 298px; resize: vertical;" id="sms_text" name="call_content" cols="300" rows="4"></textarea>
							</td>
						</tr>
						<tr>
							<td>
								<input style="width: 50px;" type="text" id="simbol_caunt" value="0/150">
							</td>
							<td>
								
							</td>
							
							<td>
								<button id="send_sms">გაგზავნა</button>
							</td>
						</tr>	
					</table>
		        </fieldset>
        </div>';
    return $data;
}

function GetMailSendPage(){
    $data = '
            <div id="dialog-form">
        	    <fieldset style="height: auto;">
        	    	<table class="dialog-form-table">
        				
        				<tr>
        					<td style="width: 90px; "><label for="d_number">ადრესატი:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;"id="mail_address" value="" />
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 90px;"><label for="d_number">CC:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;" id="mail_address1" value="" />
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 90px;"><label for="d_number">Bcc:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;" id="mail_address2" value="" />
        					</td>
        				</tr>
        				<tr>
        					<td style="width: 90px;"><label for="d_number">სათაური:</label></td>
        					<td>
        						<input type="text" style="width: 490px !important;" id="mail_text" value="" />
        					</td>
        				</tr>
        			</table>
        			<table class="dialog-form-table">
        				<tr>
        					<td>	
        						<textarea id="input" style="width:551px; height:200px"></textarea>
        					</td>
        			   </tr>
        			</table>
			    </fieldset>
		    </div>';
    return $data;
}

function show_record($res){
    global $db;
    $db->setQuery(" SELECT   FROM_UNIXTIME(asterisk_call_log.call_datetime) AS `datetime`,
                              TIME_FORMAT(SEC_TO_TIME(asterisk_call_log.talk_time),'%i:%s') AS `duration`,
                              CONCAT(DATE_FORMAT(FROM_UNIXTIME(asterisk_call_log.call_datetime), '%Y/%m/%d/'),CONCAT(asterisk_call_record.`name`,'.',asterisk_call_record.format)) AS file_name,
                              user_info.`name`,
                              if(asterisk_call_log.call_status_id = '6', 'ოპერატორმა', 'აბონენტმა') as completed
                    FROM     `asterisk_call_log`
                    JOIN      asterisk_call_record ON asterisk_call_record.asterisk_call_log_id = asterisk_call_log.id
                    LEFT JOIN user_info ON user_info.user_id = asterisk_call_log.user_id
                    WHERE     asterisk_call_log.id = '$res[asterisk_incomming_id]'");
    
    $record_incomming = $db->getResultArray();
    
    foreach ($record_incomming[result] AS $record_res_incomming) {
        $str_record_incomming .= '<tr>
                                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['datetime'].'</td>
                            	      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['duration'].'</td>
                                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['name'].'</td>
                                      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_incomming['completed'].'</td>
                            	      <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$record_res_incomming['file_name'].'\')"><span>მოსმენა</span></td>
                            	  </tr>';
    }
    
    $phone = $res['phone'];
    if(strlen($res['phone']) > 1 && substr($phone,0,1) == '0'){
        $phone = substr($phone,1);
    }
    
    if(strlen($phone) > 1){
        $ph = "CAST(`phone` AS SIGNED) LIKE '%$phone%'";
    }else{
        $ph = "CAST(`phone` AS SIGNED) LIKE '%sdfgsdfge%'";
    }
    $db->setQuery(" SELECT   `call_datetime`,
                              TIME_FORMAT(SEC_TO_TIME(duration),'%i:%s') AS `duration`,
                              CONCAT(DATE_FORMAT(asterisk_outgoing.call_datetime, '%Y/%m/%d/'),`file_name`) AS file_name,
                              user_info.name
                    FROM     `asterisk_outgoing`
                    LEFT JOIN user_info ON user_info.user_id = asterisk_outgoing.user_id
                    WHERE   $ph");
   
    $record_outgoing = $db->getResultArray();
    foreach($record_outgoing[result] AS $record_res_outgoing) {
        $str_record_outgoing .= '<tr>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_outgoing[call_datetime].'</td>
                            	    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_outgoing[duration].'</td>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">'.$record_res_outgoing['name'].'</td>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;" onclick="listen(\''.$record_res_outgoing[file_name].'\')"><span>მოსმენა</span></td>
                                  </tr>';
    }
    
    if($str_record_outgoing == ''){
        $str_record_outgoing = '<tr>
                                    <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=4>ჩანაწერი არ მოიძებნა</td>
                        	      </tr>';
    }
    if($str_record_incomming == ''){
        $str_record_incomming = '<tr>
                                   <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;" colspan=5>ჩანაწერი არ მოიძებნა</td>
                        	      </tr>';
    }
    
    $data = '  <div style="margin-top: 10px;">
                    <audio controls style="margin-left: 165px;">
                      <source src="" type="audio/wav">
                      Your browser does not support the audio element.
                    </audio>
               </div>
               <fieldset style="display:block !important; margin-top: 10px;" class="nothide">
                    <legend>შემომავალი ზარი</legend>
                    <div style="overflow-y:scroll;height:130px;">
                        <table style="margin: auto; width: 96%;">
                            <tr>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ოპერატორი</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ვინ გათიშა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
                             </tr>
                            '.$str_record_incomming.'
                        </table>
                    </div>
                </fieldset>
	            <fieldset style="display:block !important; margin-top: 10px;" class="nothide">
                    <legend>გამავალი ზარი</legend>
                    <div style="overflow-y:scroll;height:117px;">
                        <table style="margin: auto; width: 96%;">
                            <tr>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">თარიღი</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ხანგძლივობა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">ვინ გათიშა</td>
                                <td style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;">მოსმენა</td>
                            </tr>
                            '.$str_record_outgoing.'
                        </table>
                    </div>
	            </fieldset>';
    return $data;
}
function show_file($res){
    global $db;
    if ($res['incomming_call_id'] != '' && $res['incomming_call_id'] != 0) {
        $where = "AND `incomming_call_id` = '$res[incomming_call_id]'";
    }elseif ($res['outgoing_id'] != '' && $res['outgoing_id'] != 0){
        $where = "AND `outgoing_id` = '$res[outgoing_id]'";
    }else{
        $where = "AND `task_id` = '$res[id]'";
    }
    
    $db->setQuery("SELECT `name`,
                          `rand_name`,
                          `file_date`,
                          `id`
                   FROM   `file`
                   WHERE  `actived` = 1 $where");
    
    $file_incomming = $db->getResultArray();
    foreach($file_incomming[result] AS $file_res_incomming) {
        $str_file_incomming .= '<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 30%;float:left;">'.$file_res_incomming[file_date].'</div>
                            	<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 30%;float:left;">'.$file_res_incomming[name].'</div>
                            	<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;width: 30%;float:left;"><a   download href="media/uploads/file/'.$file_res_incomming['rand_name'].'">ჩამოტვირთვა</a></div>
                            	<div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;cursor: pointer;width: 10%;float:left;" onclick="delete_file(\''.$file_res_incomming[id].'\')">-</div>';
    }
    $data = '<div style="margin-top: 15px;">
                    <div style="width: 100%; border:1px solid #CCC;float: left;">    	            
    	                   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 30%;float:left;">თარიღი</div>
                    	   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 30%;float:left;">დასახელება</div>
                    	   <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 30%;float:left;">ჩამოტვირთვა</div>
                           <div style="border: 1px solid #CCC;padding: 5px;text-align: center;vertical-align: middle;width: 10%;float:left;">-</div>
    	                   <div style="text-align: center;vertical-align: middle;float: left;width: 100%;"><button id="upload_file" style="cursor: pointer;background: none;border: none;width: 100%;height: 25px;padding: 0;margin: 0;">აირჩიეთ ფაილი</button><input style="display:none;" type="file" name="file_name" id="file_name"></div>
                           <div id="paste_files">
                           '.$str_file_incomming.'
                           </div>
            	    </div>
	            </div>';
    return $data;
}
function get_task_status($id){
    global $db;
    $db->setQuery("SELECT `name` FROM `task_status` WHERE id = '$id' ");
    $res = $db->getResultArray();

    return $res[result][0]['name'];

}
function get_fieldset_inputs($field_id,$saved_values,$inputs_by_tabs,$pphone){
    GLOBAL $db;
    
    $db->setQuery("	SELECT 	id,
        `key`,
        name,
        multilevel_table,
        table_name,
        processing_field_type_id AS 'type',
        position,
        multilevel_1,
        multilevel_2,
        multilevel_3,
        depth,
        tab_id,
        necessary_input AS 'nec',
        '' AS 'class'
        
        FROM 	processing_setting_detail
        WHERE 	processing_fieldset_id = '$field_id' AND actived = '1'
        ORDER BY position");
    $inputs = $db->getResultArray();
    
    $tab_count = count($inputs_by_tabs);
    $tabs_count = 0;
    foreach($inputs_by_tabs AS $tabs){
        $display = 'none';
        if($tabs_count == 0){
            $display = 'block';
            $tabs_count++;
        }
        $output .= '<div class="inp_tabs dialog-tab-'.$field_id.'" data-tab-id="'.$tabs.'"  style="display:'.$display.';">';
        $divider = 1;
        for($j=0;$j<$inputs['count'];$j++){
            if($tabs == $inputs['result'][$j]['tab_id']){
                if($inputs['result'][$j]['type'] == 11){
                    if($divider%3 == 0){
                        $inputs['result'][$j-1]['class'] = 'col-6';
                        $inputs['result'][$j-2]['class'] = 'col-6';
                    }
                    else{
                        $inputs['result'][$j-1]['class'] = 'col-12';
                    }
                }
                $divider++;
            }
            
        }
        for($i=0;$i<$inputs['count'];$i++){
            if($tabs == $inputs['result'][$i]['tab_id']){
                $nec = '';
                $nec_is = 0;
                $default = 'col-4';
                if($inputs['result'][$i]['nec'] == 1){
                    $nec = '<span style="color:red;"> *</span>';
                    $nec_is = 1;
                }
                
                if($inputs['result'][$i]['class'] != ''){
                    $default = $inputs['result'][$i]['class'];
                }
                
                if($inputs['result'][$i]['type'] == 1 OR $inputs['result'][$i]['type'] == 4 OR $inputs['result'][$i]['type'] == 5)
                {
                    $show = 0;
                    $input_name = $inputs['result'][$i]['name'];
                    
                    
                    $service_updater = '';
                    if($input_name == 'ტელეფონი'){
                        $service_updater = '<img class="service_icon" id="service_phone" src="media/images/icons/inner_call_2.png">';
                    }
                    else if($input_name == 'პირადი ნომერი'){
                        $service_updater = '<img class="service_icon" id="service_pn" src="media/images/icons/inner_call_2.png">';
                    }
                    else if($input_name == 'აბონენტის კოდი'){
                        $service_updater = '<img class="service_icon" id="service_ab" src="media/images/icons/inner_call_2.png">';
                    }
                    
                    foreach($saved_values AS $saved){
                        $val = $saved[value];
                        if($input_name == 'ტელეფონი'){
                            $number = $pphone;
                            if($pphone != ''){
                                $last = substr($number,-2);
                                $middle = substr($number,-4,-2);
                                $first = substr($number,-6,-4);
                                $triple = substr($number,-9,-6);
                                
                                $formated_phone = $triple.'-'.$first.'-'.$middle.'-'.$last;
                                
                                $val = $formated_phone;
                            }
                            
                        }
                        
                        
                        if($saved['input_id'] == $inputs['result'][$i]['id']){
                            $show++;
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$val.'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" />'.$service_updater.'</div>';
                        }
                    }
                    if($show == 0){
                        $number = $pphone;
                        $val = '';
                        if($pphone != '' and $input_name == 'ტელეფონი'){
                            $last = substr($number,-2);
                            $middle = substr($number,-4,-2);
                            $first = substr($number,-6,-4);
                            $triple = substr($number,-9,-6);
                            
                            $formated_phone = $triple.'-'.$first.'-'.$middle.'-'.$last;
                            
                            $val = $formated_phone;
                        }
                        $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><input value="'.$val.'" data-nec="'.$nec_is.'" style="height: 18px; width: 95%;" type="text" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" />'.$service_updater.'</div>';
                    }
                }
                else if($inputs['result'][$i]['type'] == 8)
                {
                    $cnobari_table = $inputs['result'][$i]['table_name'];
                    $show = 0;
                    foreach($saved_values AS $saved){
                        if($saved['input_id'] == $inputs['result'][$i]['id']){
                            $show++;
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 182px; height: 18px; ">'.get_cnobari_selector_options($cnobari_table,'selector','','',$saved['int_value']).'</select></div>';
                        }
                    }
                    if($show == 0){
                        $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><select data-nec="'.$nec_is.'" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cnobari_selector_options($cnobari_table,'selector','','',0).'</select></div>';
                    }
                    
                }
                else if($inputs['result'][$i]['type'] == 6)
                {
                    $cnobari_table = $inputs['result'][$i]['table_name'];
                    
                    $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.get_cnobari_selector_options($cnobari_table,'radio',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
                }
                else if($inputs['result'][$i]['type'] == 7)
                {
                    $cnobari_table = $inputs['result'][$i]['table_name'];
                    
                    $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label>'.get_cnobari_selector_options($cnobari_table,'checkbox',$inputs['result'][$i]['key'],$inputs['result'][$i]['id'],'',$saved_values).'</div>';
                }
                else if($inputs['result'][$i]['type'] == 2)
                {
                    $show = 0;
                    foreach($saved_values AS $saved){
                        if($saved['input_id'] == $inputs['result'][$i]['id']){
                            $show++;
                            $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content">'.$saved[value].'</textarea></div>';
                        }
                    }
                    if($show == 0){
                        $output .= '<div class="'.$default.'"><label>'.$inputs['result'][$i]['name'].$nec.'</label><textarea data-nec="'.$nec_is.'" style="resize: vertical;width: 97%;height: 30px;" id="'.$inputs['result'][$i]['key'].'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" class="idle" name="call_content"></textarea></div>';
                    }
                }
                else if($inputs['result'][$i]['type'] == 12)
                {
                    $output .= '<div class="col-4"></div>';
                }
                else if($inputs['result'][$i]['type'] == 10){
                    $depth = $inputs['result'][$i]['depth'];
                    $level_1_id = 0;
                    $level_2_id = 0;
                    $level_3_id = 0;
                    for($k=1;$k<=$depth;$k++){
                        
                        if($k == 1){
                            $show = 0;
                            foreach($saved_values AS $saved){
                                if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 1){
                                    $show++;
                                    $level_1_id = $saved['int_value'];
                                    $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1($saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                }
                            }
                            if($show == 0){
                                $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1(0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                            }
                            
                        }
                        else if($k == 2){
                            $show = 0;
                            foreach($saved_values AS $saved){
                                if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 2){
                                    $show++;
                                    $level_2_id = $saved['int_value'];
                                    $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_1_id,$saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                }
                            }
                            if($show == 0){
                                $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_1_id,0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                            }
                        }
                        else{
                            $show = 0;
                            foreach($saved_values AS $saved){
                                if($saved['input_id'] == $inputs['result'][$i]['id'] AND $saved['value'] == 3){
                                    $show++;
                                    $level_3_id = $saved['int_value'];
                                    $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_2_id,$saved['int_value'],$inputs['result'][$i]['multilevel_table']).'</select></div>';
                                }
                            }
                            if($show == 0){
                                $output .= '<div class="col-4"><label>'.$inputs['result'][$i]['multilevel_'.$k].'</label><select  id="'.$inputs['result'][$i]['key'].'_'.$k.'--'.$inputs['result'][$i]['id'].'--'.$inputs['result'][$i]['type'].'" style="width: 98%; height: 18px; ">'.get_cat_1_1($level_2_id,0,$inputs['result'][$i]['multilevel_table']).'</select></div>';
                            }
                        }
                    }
                }
                $divider++;
            }
            
        }
        $output .= '</div>';
        
    }
    return $output;
}

function get_cnobari_selector_options($table,$selector_type='selector',$key='',$input_id='',$selected=0,$saved_values=''){
    GLOBAL $db;
    if($table != ''){
        $db->setQuery("	SELECT 	id,
            name
            FROM 	$table
            WHERE 	actived = '1'");
        if($selector_type == 'selector'){
            
            return $db->getSelect($selected);
        }
        else if($selector_type == 'radio'){
            $output = '';
            $radio = $db->getResultArray();
            $output .= '<div class="checkbox_container">';
            foreach($radio['result'] AS $item){
                
            }
            foreach($radio['result'] AS $item){
                $show = 0;
                foreach($saved_values AS $saved){
                    
                    if($saved['input_id'] == $input_id AND $saved['int_value'] == $item[id]){
                        $show++;
                        $output .= '<div class="lonely-checkbox"><input checked type="radio" id="'.$key.'-'.$item[id].'--'.$input_id.'--6" name="'.$key.'" name="'.$key.'" value="'.$item[name].'">
							<label style="margin-top: 6px;margin-right: 5px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                    }
                }
                if($show == 0){
                    $output .= '<div class="lonely-checkbox"><input type="radio" id="'.$key.'-'.$item[id].'--'.$input_id.'--6" name="'.$key.'" name="'.$key.'" value="'.$item[name].'">
							<label style="margin-top: 6px;margin-right: 5px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                }
                
            }
            $output .= '</div>';
            return $output;
        }
        else if($selector_type == 'checkbox'){
            //var_dump($saved_values);
            $output = '';
            $radio = $db->getResultArray();
            $output .= '<div class="checkbox_container">';
            foreach($radio['result'] AS $item){
                $show = 0;
                foreach($saved_values AS $saved){
                    
                    if($saved['input_id'] == $input_id AND $saved['int_value'] == $item[id]){
                        $show++;
                        $output .= '<div class="lonely-checkbox"><input checked style="height:12px!important;" type="checkbox" id="'.$key.'-'.$item[id].'--'.$input_id.'--7" name="'.$key.'" value="'.$item[name].'">
							<label style="margin-right: 7px;margin-left: 3px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                    }
                }
                if($show == 0){
                    $output .= '<div class="lonely-checkbox"><input style="height:12px!important;" type="checkbox" id="'.$key.'-'.$item[id].'--'.$input_id.'--7" name="'.$key.'" value="'.$item[name].'">
							<label style="margin-right: 7px;margin-left: 3px;" for="'.$key.'-'.$item[id].'">'.$item[name].'</label></div>';
                }
                
            }
            
            $output .= '</div>';
            return $output;
        }
        
    }
    
}

function get_dep_1($id = 0){
    global $db;
    
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `info_category`
                   WHERE   actived = 1 AND `parent_id` = 0");
    
    $req = $db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    foreach ($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        }else{
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
    }
    
    return $data;
    
}
function get_dep_11($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
        `name`
        FROM    `info_category`
        WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}
function get_dep_1_1($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  		`cat1`.id,
								`cat1`.`name`
					FROM 		`info_category`
					LEFT JOIN 	`info_category` AS `cat1` ON `cat1`.`parent_id` = `info_category`.`id`
        
					WHERE 	 	`info_category`.`parent_id` = 0 AND `info_category`.actived=1");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}
function get_dep_1_1D($id,$child_id){
    global $db;
    
    $db->setQuery("SELECT  `id`,
        `name`
        FROM    `info_category`
        WHERE    actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}
function get_project_type($id){
    global $db;
    $db->setQuery("SELECT `id`,
                          `name`
                   FROM   `mepa_project_types`
                   WHERE   actived = 1");
    
    $req = $db->getResultArray();
    
    $data .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req[result] AS $res){
        if($res['id'] == $id){
            $data .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    return $data;
}
function get_dep_1_1_1($id,$child_id){
    global $db;
    $db->setQuery("SELECT `id`,
        `name`
        FROM   `info_category`
        WHERE   actived = 1 AND `parent_id` = '$id' AND `parent_id` != 0");
    
    $req = $db->getResultArray();
    
    $data2 .= '<option value="0" selected="selected">----</option>';
    $i = 0;
    
    foreach($req[result] AS $res){
        if($res['id'] == $child_id){
            $data2 .= '<option value="' . $res['id'] . '" selected="selected">' . $res['name'] . '</option>';
        } else {
            $data2 .= '<option value="' . $res['id'] . '">' . $res['name'] . '</option>';
        }
        $i = 1;
    }
    
    if($i == 0 && $id > 0){
        $data2 .= '<option value="999" selected="selected">არ აქვს კატეგორია</option>';
    }
    
    $db->setQuery("	SELECT id,name
        
        FROM info_category
        WHERE id = (SELECT parent_id
        FROM info_category
        WHERE info_category.id = '$id')");
    $main = $db->getResultArray();
    
    $data3 .= '<option value="'.$main['result'][0]['id'].'" selected="selected">'.$main['result'][0]['name'].'</option>';
    
    if($child_id == ''){
        if($id == 0){
            
            $data = array('data2' => $data2, 'data3' => get_dep_1());
        }
        else{
            $data = array('data2' => $data2, 'data3' => $data3);
        }
    }
    else{
        $data = $data2;
    }
    
    
    return $data;
}
?>