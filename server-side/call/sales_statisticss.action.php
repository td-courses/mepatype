<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();

header('Content-Type: application/json');
$name[]     = 'რაოდენობა';


			//------------------------------- მომსახურების დონე(Service Level)
			
			
			
			$db->setQuery("	SELECT queue_stats.info1
							FROM   queue_stats
							WHERE  DATE(queue_stats.datetime) = CURDATE() AND queue_stats.qevent = 10");
			$w15 = 0;
			$w30 = 0;
			$w45 = 0;
			$w60 = 0;
			$w75 = 0;
			$w90 = 0;
			$w91 = 0;
			
			$res_service_level = $db->getResultArray();
			foreach($res_service_level[result] AS $res_service_level_r) {
			
    			if ($res_service_level_r['info1'] < 15) {
    			    $w15++;
    			}
    			
    			if ($res_service_level_r['info1'] < 30){
    			    $w30++;
    			}
    			
    		    if ($res_service_level_r['info1'] < 45){
    				$w45++;
    			}
    			
    			if ($res_service_level_r['info1'] < 60){
    			    $w60++;
    			}
    			
    			if ($res_service_level_r['info1'] < 75){
    			    $w75++;
    			}
    			
    			if ($res_service_level_r['info1'] < 90){
    			    $w90++;
    			}
    			
    			$w91++;
			}

			$d30 = $w30 - $w15;
			$d45 = $w45 - $w30;
			$d60 = $w60 - $w45;
			$d75 = $w75 - $w60;
			$d90 = $w90 - $w75;
			$d91 = $w91 - $w90;
			
			
$mas = array($w15,$d30,$d45,$d60,$d75,$d90,$d91);
$call_second=array('15 წამში','30 წამში','45წამში','60 წამში','75 წამში','90 წამში','90+წამში');			
							
$unit[]="ზარი";
$series[] = array('name' => $name, 'unit' => $unit, 'mas' => $mas, 'call_second' => $call_second);

echo json_encode($series);

?>