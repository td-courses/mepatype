<?php

use function PHPSTORM_META\elementType;

include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action    = $_REQUEST['act'];
$error    = '';
$data    = array();
$user   = $_SESSION['USERID'];
date_default_timezone_set("Asia/Tbilisi");

$region_id = $_REQUEST['region_id'];
$raion_id  = $_REQUEST['raion_id'];
$sofel_id  = $_REQUEST['sofel_id'];
$region_filt = '';
$raion_filt  = '';
$sofel_filt  = '';

if ($region_id > 0) {
    $region_filt = " AND region.int_value = $region_id";
}

if ($raion_id > 0) {
    $raion_filt = " AND raion.int_value = $raion_id";
}

if ($sofel_id > 0) {
    $sofel_filt = " AND sofeli.int_value = $sofel_id";
}

switch ($action) {
    case 'get_list':
        $filter = "";

        $id          =      $_REQUEST['hidden'];
        $columnCount =         $_REQUEST['count'];
        $json_data   =         json_decode($_REQUEST['add'], true);
        $itemPerPage =         $json_data['pageSize'];
        $skip        =         $json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $startDate  = $_REQUEST['start_date'];
        $endDate  = $_REQUEST['end_date'];

        if ($startDate != "" && $endDate != "") {
            $filter .= " AND DATE(incomming_call.date) >= '$startDate' ";
            $filter .= " AND DATE(incomming_call.date) <= '$endDate' ";
        }

        $query = "SELECT 	incomming_call.date AS id ,
                            MONTHNAME(incomming_call.date) as monthname,
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female
                  FROM 		incomming_call
                  JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                  JOIN 		asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 1 
                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                  WHERE     asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt 
                  GROUP BY  MONTH(incomming_call.date) 
                  UNION
                  SELECT    incomming_call.id,
                           '<b>ჯამი<b>' as monthname,
                                
                            concat('<b>',COUNT(incomming_call.id),'<b>') as callcount,
                            concat('<b>',SUM(if((incomming_request_processing.int_value = 2),(1),(0))),'<b>')  as male,
                            concat('<b>',SUM(if((incomming_request_processing.int_value = 1),(1),(0))),'<b>')  as female
                  FROM 	    incomming_call
                  JOIN 	    incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                  JOIN 	    asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 1
                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
        		  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                  WHERE     asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt";

        $mysqli->setQuery($query);
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;
    case 'get_list_for_chart':
        $filter = "";
        $id          =      $_REQUEST['hidden'];
        $startDate  = $_REQUEST['start_date'];
        $endDate  = $_REQUEST['end_date'];

        if (!empty($startDate)) {
            $filter .= "AND DATE(incomming_call.date) >= '$startDate'";
        }
        if (!empty($endDate)) {
            $filter .= "AND DATE(incomming_call.date) <= '$endDate '";
        }

        $query = "SELECT    MONTH(incomming_call.date) as month,
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female
                    FROM incomming_call
                    JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                    JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 1
                    LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			   LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
                   LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                    WHERE asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt
                    GROUP BY month
                    ORDER BY month ASC";

        $mysqli->setQuery($query);
        $data = $mysqli->getResultArray();

        break;
    case 'get_list_for_chart2':
        $filter = "";
        $id          =      $_REQUEST['hidden'];
        $startDate  = $_REQUEST['start_date'];
        $endDate  = $_REQUEST['end_date'];

        if (!empty($startDate)) {
            $filter .= " AND DATE(incomming_call.date) >= '$startDate' ";
        }
        if (!empty($endDate)) {
            $filter .= " AND DATE(incomming_call.date) <= '$endDate' ";
        }

        $query = "SELECT    MONTH(incomming_call.date) as month,
                                COUNT(incomming_call.id) as callcount,
                                SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
                                SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male
                        FROM incomming_call
                        JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                        JOIN asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 2
                        LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			        LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
                        LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                        WHERE asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt
                        GROUP BY month
                        ORDER BY month ASC";

        $mysqli->setQuery($query);
        $data = $mysqli->getResultArray();

        break;
    case 'get_list_2':
        $filter = "";

        $id          =      $_REQUEST['hidden'];
        $columnCount =         $_REQUEST['count'];
        $json_data   =         json_decode($_REQUEST['add'], true);
        $itemPerPage =         $json_data['pageSize'];
        $skip        =         $json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $startDate  = $_REQUEST['start_date'];
        $endDate  = $_REQUEST['end_date'];

        if ($startDate != "" && $endDate != "") {
            $filter .= " AND DATE(incomming_call.date) >= '$startDate' ";
            $filter .= " AND DATE(incomming_call.date) <= '$endDate' ";
        }

        $query = "SELECT 	incomming_call.date AS id ,
                            MONTHNAME(incomming_call.date) as monthname,
                            COUNT(incomming_call.id) as callcount,
                            SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
                            SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male
                  FROM 		incomming_call
                  JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                  JOIN 	    asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 2
                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0 
                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                  WHERE     asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt
                  GROUP BY  MONTH(incomming_call.date) 
                  UNION
                  SELECT    incomming_call.id,
                           '<b>ჯამი<b>' as monthname,
                            concat('<b>',COUNT(incomming_call.id),'<b>') as callcount,
                            concat('<b>',SUM(if((incomming_request_processing.int_value = 2),(1),(0))),'<b>')  as male,
                            concat('<b>',SUM(if((incomming_request_processing.int_value = 1),(1),(0))),'<b>')  as female
                  FROM 		incomming_call
                  JOIN 		incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                  JOIN 		asterisk_call_log ON asterisk_call_log.id = incomming_call.asterisk_incomming_id AND asterisk_call_log.call_type_id = 2
                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0 
                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
    			  WHERE     asterisk_incomming_id != '' $filter $raion_filt $region_filt $sofel_filt";
        $mysqli->setQuery($query);
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;
    case 'get_list_3':
        $filter = "";

        $id          = $_REQUEST['hidden'];
        $columnCount = $_REQUEST['count'];
        $json_data   = json_decode($_REQUEST['add'], true);
        $itemPerPage = $json_data['pageSize'];
        $skip        = $json_data['skip'];
        $cols[]      = $_REQUEST['cols'];
        $startDate   = $_REQUEST['start_date'];
        $endDate     = $_REQUEST['end_date'];

        $query = "SELECT   incomming_call.id,
                           MONTHNAME(incomming_call.date) as monthname,
                           SUM(if((incomming_request_processing.int_value = 1),(1),(0))) as female,
                           SUM(if((incomming_request_processing.int_value = 2),(1),(0))) as male,
                           SUM(if((incomming_call.source_id = 1),(1),(0))) as phone,
                           SUM(if((incomming_call.source_id = 7),(1),(0))) as email,
                           SUM(if((incomming_call.source_id = 4),(1),(0))) as livechat,
                           SUM(if((incomming_call.source_id = 10),(1),(0))) as webcall
                  FROM     incomming_call
                  JOIN     incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                  JOIN     source on source.id = incomming_call.source_id
                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                  WHERE    DATE(incomming_call.date) >= '$startDate' AND DATE(incomming_call.date) <= '$endDate' $raion_filt $region_filt $sofel_filt
                  GROUP BY MONTH(incomming_call.date)
                  UNION
                  SELECT incomming_call.date,
                        '<b>ჯამი<b>' as monthname,
                         concat('<b>',SUM(if((incomming_request_processing.int_value = 1),(1),(0))),'<b>') as female,
                         concat('<b>',SUM(if((incomming_request_processing.int_value = 2),(1),(0))),'<b>') as male,
                         concat('<b>',SUM(if((incomming_call.source_id = 1),(1),(0))),'<b>') as phone,
                         concat('<b>',SUM(if((incomming_call.source_id = 7),(1),(0))),'<b>') as email,
                         concat('<b>',SUM(if((incomming_call.source_id = 4),(1),(0))),'<b>') as livechat,
                         concat('<b>',SUM(if((incomming_call.source_id = 10),(1),(0))),'<b>') as webcall
                  FROM   incomming_call
                  JOIN   incomming_request_processing ON incomming_request_processing.incomming_request_id = incomming_call.id AND incomming_request_processing.processing_setting_detail_id = 130
                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = incomming_call.id AND region.`value` = 1111 AND region.int_value > 0
    			  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = incomming_call.id AND raion.`value` = 2222 AND raion.int_value > 0
                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = incomming_call.id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                  JOIN   source on source.id = incomming_call.source_id
                  WHERE  DATE(incomming_call.date) >= '$startDate' AND DATE(incomming_call.date) <= '$endDate' $raion_filt $region_filt $sofel_filt";


        $mysqli->setQuery($query);
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;



    case 'get_list_4':

        $id          = $_REQUEST['hidden'];
        $columnCount = $_REQUEST['count'];
        $json_data   = json_decode($_REQUEST['add'], true);
        $itemPerPage = $json_data['pageSize'];
        $skip        = $json_data['skip'];
        $cols[]      = $_REQUEST['cols'];
        $startDate   = $_REQUEST['start_date'] . ' 00:00';
        $endDate     = $_REQUEST['end_date'] . ' 23:59';
        $callsType   = $_REQUEST["callsType"];

        $type_filter = '';
        if ($callsType != "" && $callsType != 'null') {
            $type_filter = " JOIN incomming_call ON incomming_call.id = uwyeba.inc_req_id
                             WHERE incomming_call.source_id IN('$callsType') ";
        }


        $mysqli->setQuery(" SELECT info_category.`name` as `uwyeba`,
                                   SUM(IF(uwyeba.gender_id = 1, 1, 0)) AS `females`,
                                   SUM(IF(uwyeba.gender_id = 2, 1, 0)) AS `males` 
                            FROM (SELECT gender.int_value AS `gender_id`,
                                         incomming_request_processing.int_value AS `cat_id`,
                                         gender.inc_req_id
                                  FROM ( SELECT    incomming_request_processing.incomming_request_id AS inc_req_id,
                                                   incomming_request_processing.int_value 
                                         FROM      incomming_request_processing 
                                         WHERE     datetime BETWEEN '$startDate' AND '$endDate'
                                         AND       incomming_request_processing.processing_setting_detail_id IN ( 130 ) 
                                         AND       incomming_request_processing.int_value > 0 
                                         GROUP BY  incomming_request_processing.incomming_request_id) AS `gender`
                                  JOIN      incomming_request_processing ON incomming_request_processing.incomming_request_id = gender.inc_req_id AND incomming_request_processing.processing_setting_detail_id = 0 AND incomming_request_processing.`value` = 1
                                  LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = gender.inc_req_id AND region.`value` = 1111 AND region.int_value > 0
		                          LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = gender.inc_req_id AND raion.`value` = 2222 AND raion.int_value > 0
                                  LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = gender.inc_req_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                                  WHERE     gender.inc_req_id > 0 $raion_filt $region_filt $sofel_filt
                                  GROUP BY gender.inc_req_id,incomming_request_processing.additional_deps ) AS `uwyeba`
                            JOIN   info_category ON info_category.id = uwyeba.cat_id 
                            $type_filter
                            GROUP BY uwyeba.cat_id");
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;

    case 'get_list_5':

        $id          =      $_REQUEST['hidden'];
        $columnCount =         $_REQUEST['count'];
        $json_data   =         json_decode($_REQUEST['add'], true);
        $itemPerPage =         $json_data['pageSize'];
        $skip        =         $json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $startDate  = $_REQUEST['start_date'] . ' 00:00';
        $endDate  = $_REQUEST['end_date'] . ' 23:59';

        $callsType = $_REQUEST["callsType"];

        $type_filter = '';
        if ($callsType != "" && $callsType != 'null') {
            $type_filter = " JOIN incomming_call ON incomming_call.id = uwyeba.inc_req_id
                             WHERE incomming_call.source_id IN('$callsType') ";
        }

        $query = "  SELECT info_category.`name` as `project`,
                           SUM(IF( uwyeba.gender_id = 1, 1, 0 )) AS `females`,
                           SUM(IF( uwyeba.gender_id = 2, 1, 0 )) AS `males` 
                    FROM ( SELECT gender.int_value AS `gender_id`,
                                  incomming_request_processing.int_value AS `cat_id` ,
                                  gender.inc_req_id
                           FROM (SELECT incomming_request_processing.incomming_request_id AS inc_req_id,
                                        incomming_request_processing.int_value 
                                 FROM   incomming_request_processing 
                                 WHERE  datetime BETWEEN '$startDate' AND '$endDate' AND incomming_request_processing.processing_setting_detail_id IN ( 130 ) AND incomming_request_processing.int_value > 0 
                                 GROUP BY incomming_request_processing.incomming_request_id ) AS `gender`
                          JOIN      incomming_request_processing ON incomming_request_processing.incomming_request_id = gender.inc_req_id AND incomming_request_processing.processing_setting_detail_id = 0 AND incomming_request_processing.`value` = 2
                          LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = gender.inc_req_id AND region.`value` = 1111 AND region.int_value > 0
		                  LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = gender.inc_req_id AND raion.`value` = 2222 AND raion.int_value > 0 
                          LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = gender.inc_req_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                          WHERE     gender.inc_req_id > 0 $raion_filt $region_filt $sofel_filt
                          GROUP BY gender.inc_req_id,incomming_request_processing.additional_deps ) AS `uwyeba`
                    JOIN info_category ON info_category.id = uwyeba.cat_id 
                    $type_filter
                    GROUP BY uwyeba.cat_id";

        $mysqli->setQuery($query);
        $data = $mysqli->getKendoList($columnCount, $cols);

        break;

    case 'get_list_6':

        $id          =      $_REQUEST['hidden'];
        $columnCount =         $_REQUEST['count'];
        $json_data   =         json_decode($_REQUEST['add'], true);
        $itemPerPage =         $json_data['pageSize'];
        $skip        =         $json_data['skip'];
        $cols[]      =      $_REQUEST['cols'];
        $startDate  = $_REQUEST['start_date'] . ' 00:00';
        $endDate  = $_REQUEST['end_date'] . ' 23:59';

        $callsType = $_REQUEST["callsType"];

        $type_filter = '';
        if ($callsType != "" && $callsType != 'null') {
            $type_filter = " JOIN incomming_call ON incomming_call.id = uwyeba.inc_req_id
                             WHERE incomming_call.source_id IN('$callsType') ";
        }

        $query = "  SELECT  mepa_project_types.`name` as `momartva`,
                            SUM(IF( uwyeba.gender_id = 1, 1, 0 )) AS `females`,
                            SUM(IF( uwyeba.gender_id = 2, 1, 0 )) AS `males` 
                    FROM (  SELECT gender.int_value AS `gender_id`,
                                   incomming_request_processing.int_value AS `cat_id`,
                                   gender.inc_req_id
                            FROM (SELECT   incomming_request_processing.incomming_request_id AS inc_req_id,
                                           incomming_request_processing.int_value 
                                  FROM     incomming_request_processing 
                                  WHERE    datetime BETWEEN '$startDate' AND '$endDate' AND incomming_request_processing.processing_setting_detail_id IN ( 130 ) AND incomming_request_processing.int_value > 0 
                                  GROUP BY incomming_request_processing.incomming_request_id ) AS `gender`
                            JOIN incomming_request_processing ON incomming_request_processing.incomming_request_id = gender.inc_req_id AND incomming_request_processing.processing_setting_detail_id = 0 AND incomming_request_processing.`value` = 4
                            LEFT JOIN incomming_request_processing AS region ON region.incomming_request_id = gender.inc_req_id AND region.`value` = 1111 AND region.int_value > 0
                            LEFT JOIN incomming_request_processing  AS raion ON raion.incomming_request_id = gender.inc_req_id AND raion.`value` = 2222 AND raion.int_value > 0  
                            LEFT JOIN incomming_request_processing  AS sofeli ON sofeli.incomming_request_id = gender.inc_req_id AND sofeli.`value` = 3333 AND sofeli.int_value > 0
                    WHERE     gender.inc_req_id > 0 $raion_filt $region_filt $sofel_filt
                    GROUP BY gender.inc_req_id,incomming_request_processing.additional_deps ) AS `uwyeba`
                    JOIN mepa_project_types ON mepa_project_types.id = uwyeba.cat_id 
                    $type_filter
                    GROUP BY uwyeba.cat_id";
        $mysqli->setQuery($query);
        $data = $mysqli->getKendoList($columnCount, $cols);
        break;
    case 'get_columns':

        $columnCount =         $_REQUEST['count'];
        $cols[] =           $_REQUEST['cols'];
        $columnNames[] =     $_REQUEST['names'];
        $operators[] =         $_REQUEST['operators'];
        $selectors[] =         $_REQUEST['selectors'];
        $f = 0;
        foreach ($cols[0] as $col) {
            $column = explode(':', $col);

            $res[$f]['Field'] = $column[0];
            $res[$f]['type'] = $column[1];
            $f++;
        }
        $i = 0;
        $columns = array();
        foreach ($res as $item) {
            $columns[$i] = $item['Field'];
            $i++;
        }

        $dat = array();
        $a = 0;
        for ($j = 0; $j < $columnCount; $j++) {
            if (1 == 2) {
                continue;
            } else {

                if ($operators[0][$a] == 1) $op = true;
                else $op = false; //  TRANSFORMS 0 OR 1 TO True or False FOR OPERATORS
                //$op = false;

                if ($res['data_type'][$j] == 'date') {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'format' => "{0:yyyy-MM-dd hh:mm:ss}", 'parseFormats' => ["MM/dd/yyyy h:mm:ss"]);
                } else if ($selectors[0][$a] != '0') // GETTING SELECTORS WHERE VALUES ARE TABLE NAMES
                {
                    $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'values' => getSelectors($selectors[0][$a]));
                } else {
                    if ($columns[$j] == "inc_status") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 153);
                    } elseif ($columns[$j] == "audio_file") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 150);
                    } elseif ($columns[$j] == "id" || $columns[$j] == 'protocol_hidde_id') {
                        $g = array('field' => $columns[$j], 'hidden' => true, 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 100);
                    } elseif ($columns[$j] == "inc_date") {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true), 'width' => 130);
                    } else {
                        $g = array('field' => $columns[$j], 'encoded' => false, 'title' => $columnNames[0][$a], 'filterable' => array('multi' => true, 'search' => true));
                    }
                }
                $a++;
            }
            array_push($dat, $g);
        }

        // array_push($dat,array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px'));

        $new_data = array();
        //{"id":"id","fields":[{"id":{"editable":true,"type":"number"}},{"reg_date":{"editable":true,"type":"number"}},{"name":{"editable":true,"type":"number"}},{"surname":{"editable":true,"type":"number"}},{"age":{"editable":true,"type":"number"}}]}
        for ($j = 0; $j < $columnCount; $j++) {
            if ($res['data_type'][$j] == 'date') {
                $new_data[$columns[$j]] = array('editable' => false, 'type' => 'string');
            } else {
                $new_data[$columns[$j]] = array('editable' => true, 'type' => 'string');
            }
        }

        $filtArr = array('fields' => $new_data);
        $kendoData = array('columnss' => $dat, 'modelss' => $filtArr);

        //$dat = array('command'=>["edit","destroy"],'title'=>'&nbsp;','width'=>'250px');

        $data = $kendoData;
        //$data = '[{"gg":"sd","ads":"213123"}]';

        break;
    case "get_chart":

        $divs = '';
        $count = 0;
        $db->setQuery("SELECT CASE
                                        WHEN source_id = 1 THEN 'ზარი'
                                        WHEN source_id = 11 THEN 'ჩათი'
                                        WHEN source_id = 6 THEN 'FB მესენჯერი'
                                        WHEN source_id = 10 THEN 'ვებ-ზარი'
                                        WHEN source_id = 7 THEN 'ელ-ფოსტა'
                                        WHEN source_id = 9 THEN 'ვაიბერი'
                                        ELSE 'სხვა'
                                   END AS `name`,
                                   CASE
                                        WHEN source_id = 1 THEN  'call'
                                        WHEN source_id = 11 THEN 'chat'
                                        WHEN source_id = 6 THEN  'fb'
                                        WHEN source_id = 10 THEN 'web'
                                        WHEN source_id = 7 THEN  'mail'
                                        WHEN source_id = 9 THEN  'viber'
                                        ELSE 'other'                       
                                   END AS `icon`,
                                   ROUND((COUNT(*) * 100 )/(SELECT COUNT(*) FROM incomming_call WHERE user_id = $user),2) AS `percent`,
                                   COUNT(*) AS `count`
                          FROM     incomming_call 
                          WHERE    user_id = $user
                          GROUP BY source_id");

        $res = $db->getResultArray();
        $values = array();
        foreach ($res[result] as $arr) {
            $divs .= '<div><div class="' . $arr['icon'] . '_icon"></div>' . $arr['name'] . '</div><div> ' . $arr['percent'] . ' % </div><div>' . $arr['count'] . '</div> ';
            $count += $arr['count'];
            $values[$arr['icon']] = $arr['count'];
        }


        $page = '<div id="container" style="min-width: 210px; height: 300px; max-width: 400px; margin: 0"></div>
                     <div class="pie-counter">' . $count . '</div>
                     <div class="pie-table-grid">' . $divs . '</div>';

        $data = array("html" => $page, 'values' => $values);
        break;
    default:
        $error = 'Action is Null';
}

$data['error'] = $error;
$data["filterID"] = $filterID;

echo $error;

echo json_encode($data);

function getSelectors($table)
{
    global $db;
    $db->setQuery("	SELECT		`directory__projects`.`status` as `name`,
									`directory__projects`.`status` as `id`
						FROM
									`directory__projects` 
						WHERE  		`status` <> ''
						GROUP BY 	`status`");
    $result = $db->getResultArray();

    $selectorData = array();

    foreach ($result['result'] as $option) {
        array_push($selectorData, array('text' => $option['name'], 'value' => $option['id']));
    }

    return $selectorData;
}
