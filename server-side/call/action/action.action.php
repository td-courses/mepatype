<?php

/* ******************************
 *	Request aJax actions
* ******************************
*/

require_once('../../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action = $_REQUEST['act'];
$error	= '';
$data	= array();

//action
$action_id			= $_REQUEST['id'];
$act_id  			= $_REQUEST['act_id'];
$action_name		= $_REQUEST['action_name'];
$start_date			= $_REQUEST['start_date'];
$end_date			= $_REQUEST['end_date'];
$action_content	    = $_REQUEST['action_content'];

//task
$task_type_id			= $_REQUEST['task_type_id'];
$priority_id			= $_REQUEST['persons_id'];
$comment 	        	= $_REQUEST['comment'];
$task_department_id 	= $_REQUEST['task_department_id'];
$hidden_inc				= $_REQUEST['hidden_inc'];
$edit_id				= $_REQUEST['edit_id'];
$delete_id				= $_REQUEST['delete_id'];

// file
$rand_file				= $_REQUEST['rand_file'];
$file					= $_REQUEST['file_name'];

switch ($action) {
	case 'get_add_page':
		$number		= $_REQUEST['number'];
		$page		= GetPage($res='', $number);
		$data		= array('page'	=> $page);

		break;
	case 'disable':
		$db->setQuery("UPDATE `action`
					      SET `actived` = 0
					   WHERE  `id`= $action_id ");
		$db->execQuery();
		
		$db->setQuery("UPDATE `action_history`
					      SET `actived` = 0
				       WHERE  `action_id`= $action_id ");
		$db->execQuery();
		
		break;
	case 'get_edit_page':
		$page		= GetPage(Getaction($action_id),'');

		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count  = $_REQUEST['count'];
		$hidden = $_REQUEST['hidden'];
		$user	= $_SESSION['USERID'];
		$group	= checkgroup($user);
		
		$db->setQuery("SELECT 	  action.id,
								  action.start_date,
								  action.end_date,
							      action.`name`,
                                 (SELECT GROUP_CONCAT(my_web_site.`name`) 
                                  FROM `action_site`
                                  JOIN  my_web_site ON action_site.site_id = my_web_site.id
                                  WHERE action_site.action_id = action.id) AS `site`,
							      action.content,
							      user_info.`name`,
                                  IF(NOT ISNULL(action_history.id), '<div class=\"action-status read-action\">წაკითხული</div>', '<div class=\"action-status unread-action\">წაუკითხავი</div>') AS read_status
						FROM 	  action
						JOIN      user_info ON action.user_id = user_info.user_id
						LEFT JOIN action_history ON action_history.action_id = action.id AND action_history.user_id = '$user' AND action_history.actived = '1'
						WHERE 	  action.actived=1 AND DATE(action.end_date) >= CURDATE()");
	  
		$data = $db->getList($count, $hidden,1);

		break;
	case 'save_action':
		if($action_id == ''){
			Addaction($act_id,$action_name,  $start_date, $end_date, $action_content, $file, $rand_file, $edit_id);
		}else{
			saveaction($act_id,$action_id,  $action_name,  $start_date, $end_date, $action_content);
		}
		break;
		
	case 'delete_file':
	    
		$db->setQuery("DELETE FROM file 
                       WHERE id = $delete_id");
		$db->execQuery();
		
		$db->setQuery("SELECT  `name`,
					           `rand_name`,
					           `id`
					   FROM    `file`
					   WHERE   `action_id` = $edit_id");
		
		$increm = $db->getResultArray();
		$data1  = '';
	
		foreach($increm[result] AS $increm_row){
			$data1 .='<tr style="border-bottom: 1px solid #85b1de;">
    			          <td style="width:390px; display:block;word-wrap:break-word;">'.$increm_row[name].'</td>
    			          <td ><button type="button" value="media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
					      <td ><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
				      </tr>';
		}
	
		$data = array('page' => $data1);
		
		break;
		
	case 'up_now':
		$user = $_SESSION['USERID'];
		if($rand_file != ''){
		    
			$db->setQuery("INSERT INTO 	`file` 
                                       (`user_id`, `action_id`, `name`, `rand_name`)
			                    VALUES
			                           ('$user','$edit_id','$file','$rand_file');");
			
			$db->execQuery();
		}
	
		$db->setQuery("	SELECT  `name`,
                    			`rand_name`,
            					`id`
    					FROM 	`file`
    					WHERE   `action_id` = $edit_id");
	
		$increm = $db->getResultArray();
		
		$data1  = '';
	
		foreach($increm[result] AS $increm_row)	{
			$data1 .='<tr style="border-bottom: 1px solid #85b1de;">
				          <td style="width:390px; display:block;word-wrap:break-word;">'.$increm_row[name].'</td>
						  <td ><button type="button" value="media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
						  <td ><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
					  </tr>';
	    }
		
		$data = array('page' => $data1);
		
		break;
	case 'view_action':
	    $user = $_SESSION['USERID'];
	    $id   = $_REQUEST['id'];
	    $db->setQuery("SELECT * FROM action_history WHERE action_id = $id AND user_id = $user");
	    $check = $db->getNumRow();
	    if($check == 0){
	        $db->setQuery("INSERT INTO `action_history` 
                                      (`user_id`, `datetime`, `action_id`, `status`, `actived`) 
                                VALUES 
                                      ('$user', NOW(), '$id', 1, 1)");
	        $db->execQuery();
	    }else{
	        global $error;
	        $error = 'მოცემულ სიახლეს უკვე გაეცანით';
	    }
	    break;
		
	
	case 'get_unread_action_count':

		$user = $_SESSION['USERID'];

		$db->setQuery("SELECT COUNT(id) AS all_count FROM `action` WHERE actived = 1");
		$all_action_count_query  = $db->getResultArray();
		$all_action_count		 = $all_action_count_query[result][0]["all_count"];
		
		$db->setQuery("SELECT COUNT(DISTINCT action_id) AS read_count FROM `action_history` WHERE user_id = '$user' AND actived = 1");
		$read_action_count_query = $db->getResultArray();
		$read_action_count		 = $read_action_count_query[result][0]["read_count"];
		
		$unread_action_count 	 = $all_action_count  - $read_action_count;

		$data = array("count" => $unread_action_count);

		break;	
	
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Request Functions
* ******************************
*/
function checkgroup($user){
    global $db;
    $db->setQuery("SELECT users.group_id
                   FROM   users
                   WHERE  users.id = $user");
    
    $res = $db->getResultArray();
    
    return $res[result][0]['group_id'];
    
}

function Addaction($act_id, $action_name,  $start_date, $end_date, $action_content, $file, $rand_file, $edit_id){
    global $db;
	$user    = $_SESSION['USERID'];
	$site_id = $_REQUEST['site_id'];
	$db->setQuery("INSERT INTO `action` 
							  (`id`, `user_id`, `name`, `start_date`, `end_date`, `content`, `actived`) 
						VALUES
							  ($act_id, '$user', '$action_name', '$start_date', '$end_date', '$action_content', '1');");
	$db->execQuery();
	$last_id = $db->getLastId();
	
	$site_array = explode(",",$site_id);
//
//	foreach ($site_array AS $site){
//	    $db->setQuery("INSERT INTO action_site (action_id, site_id) values ('$act_id', $site)");
//	    $db->execQuery();
//	}
	
	if($rand_file != ''){
		$db->setQuery("INSERT INTO `file` 
                                  (`user_id`, `action_id`, `name`, `rand_name`)
                             VALUES
                                  ('$user', '$edit_id', '$file', '$rand_file');");
		$db->execQuery();
	}
	
	$db->setQuery("INSERT INTO `action_history` 
							  (`user_id`, `datetime`, `action_id`, `status`, `actived`) 
						VALUES
							  ('$user', NOW(), '$last_id', 1, 1)");
	
	$db->execQuery(); 
}



				
function saveaction($act_id, $action_id,  $action_name,  $start_date, $end_date, $action_content){
	global $db;
	
	$user		  = $_SESSION['USERID'];
	$h_start_date = $_REQUEST['h_start_date'];
	$h_end_date	  = $_REQUEST['h_end_date'];
	$site_id	  = $_REQUEST['site_id'];
	
	$db->setQuery(" UPDATE `action` 
                       SET `user_id`    = '$user',
    					   `name`       = '$action_name',
    					   `start_date` = '$start_date', 
    					   `end_date`   = '$end_date', 
    					   `content`    = '$action_content', 
    					   `actived`    = '1' 
    				 WHERE `id`         = '$action_id'");
	
	$db->execQuery();
	
	$site_array = explode(",",$site_id);
	$db->setQuery("DELETE FROM action_site WHERE action_id = '$act_id'");
	$db->execQuery();
	
//	foreach ($site_array AS $site){
//	    $db->setQuery("INSERT INTO action_site (action_id, site_id) values ('$act_id', $site)");
//	    $db->execQuery();
//	}
}     

function web_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery("SELECT id
                       FROM  `action_site`
                       WHERE  action_site.action_id = '$id' AND site_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getaction($action_id){
    global $db;	
    $db->setQuery("	SELECT 	action.id,
							action.`name` AS action_name,
							action.start_date as start_date,
							action.end_date as end_date,
							action.content AS action_content
							
					FROM 	action
					WHERE 	action.id = '$action_id'");
	
    $res = $db->getResultArray();
	return $res[result][0];
}

function GetPage($res='', $number){
	global $db;
	if ($res[id] == '') {
	    $db->setQuery("INSERT INTO action (user_id) value ('1') ");
	    $db->execQuery();
	    $hidde_id = $db->getLastId();
	    $db->setQuery("DELETE FROM action WHERE id = '$hidde_id'");
	    $db->execQuery();
	}else{
	    $hidde_id = $res[id];
	}
	$db->setQuery(" SELECT  `name`,
			                `rand_name`,
			                 id
			        FROM 	`file`
			        WHERE   `action_id` = '$res[id]'");
	
    $increm = $db->getResultArray();
	
	$data  .= '
	<!-- jQuery Dialog -->
    <div id="add-edit-goods-form" title="აქცია">
    	<!-- aJax -->
	</div>
	<div id="dialog-form">
		<div style="float: left; width: 524px;">	
			<fieldset >
		    	<legend>ინფო</legend>
				<table width="100%">
					<tr>
						<td style="width: 100px;"><label style="padding: 3px 0 3px 1px;">დასახელება</label></td>
						<td colspan="3">
							<input style="width: 368px;" type="text" id="action_name" class="idle" onblur="this.className=\'idle\'"  value="' . $res['action_name']. '"  />
						</td>
					</tr>
                    <tr style="height:10px"></tr>
                   
                    <tr style="height:10px"></tr>
                    <tr>
						<td><label for="d_number"><label style="padding: 3px 0 3px 1px;">პერიოდი</label></td>
						<td>
							<input type="text" id="start_date" class="idle" onblur="this.className=\'idle\'" value="' . $res['start_date']. '" />
						</td>
						<td style="width: 40px;"><label style="padding: 3px 0 3px 1px;" for="d_number">- დან</label></td>
						<td>
							<input type="text" id="end_date" class="idle" onblur="this.className=\'idle\'"  value="' . $res['end_date']. '"  />
						</td>
						<td style="width: 150px;"><label style="padding: 3px 0 3px 1px;" for="d_number">- მდე</label></td>
					</tr>
				</table>
			    <table width="100%" class="dialog-form-table">
                    <tr style="height:10px"></tr>
                    <tr style="height:0px">
						<td><label>აღწერა</label></td>
					</tr>
					<tr style="height:0px">
						<td>
							<textarea  style="width: 99%; height: 100px; resize: none;" id="action_content" class="idle" name="content" cols="100" rows="2">'.$res['action_content'].'</textarea>
						</td>
					</tr>		
				</table>
			</fieldset>	
		</div>
        </fieldset>
		<div style="float: right;  width: 360px;">
			
									
			<fieldset style="float: right;  width: 440px;">
				<legend>განყოფილებები</legend>
									
		
	            	<div id="button_area">
	            		<button id="add_button_pp">დამატება</button>
        			</div>
                    <table class="display" id="example4" style="width: 100%;">
	                    <thead>
							<tr  id="datatable_header">
									
	                           <th style="display:none">ID</th>
								<th style="width:50%; word-break:break-all;">ფილიალი</th>
								<th style="width:50%; word-break:break-all;">მისამართი</th>
							</tr>
						</thead>
						<thead>
							<tr class="search_header">
								<th class="colum_hidden">
                        			<input type="text" name="search_id" value="" class="search_init"/>
                        		</th>
								<th>
									<input style="width:100%;" type="text" name="search_overhead" value="ფილტრი" class="search_init" />
								</th>
								<th>
									<input style="width:98%;" type="text" name="search_partner" value="ფილტრი" class="search_init" />
								</th>
							</tr>
						</thead>
	                </table>
	        </fieldset>
			
			<fieldset style="float: right;  width: 440px;">
			<legend>აქციის პროდუქტები</legend>
					<div id="button_area">
	            		<button id="add_button_p">დამატება</button>
        			</div>
	                <table class="display" id="example3" style="width: 100%;">
	                    <thead>
							<tr id="datatable_header">
								<th style="display:none">ID</th>
								<th style="width:30%;">თარიღი</th>
								<th style="width:55%;">პროდუქტი</th>
								<th style="width:15%;">თანხა</th>
							</tr>
						</thead>
						<thead>
							<tr class="search_header">
								<th class="colum_hidden">
                        			<input type="text" name="search_id" value="" class="search_init"/>
                        		</th>
								<th>
									<input style="width:100%;" type="text" name="search_overhead" value="ფილტრი" class="search_init"/>
								</th>
								<th>
									<input style="width:100%;" type="text" name="search_partner" value="ფილტრი" class="search_init"/>
								</th>
								<th>
									<input style="width:94%;" type="text" name="search_overhead" value="ფილტრი" class="search_init"/>
								</th>
							</tr>
						</thead>
	                </table>
		    </fieldset>
			<fieldset style="width: 440px; float: right;">
			    <legend>მიმაგრებული ფაილები</legend>				
			    <table style="float: right; border: none; width: 100%; text-align: center;">
					<tr>
						<td >
							<div class="file-uploader">
								<input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
								<button style="width:100%;" id="choose_button" class="center">აირჩიეთ ფაილი</button>
								<input id="hidden_inc" type="text" value="'. $hidde_id .'" style="display: none;">
							</div>
						</td>
					</tr>
				</table>
                <table id="file_div" style="float: left; border: 1px solid #85b1de; width: 438px; text-align: center;margin-left:1px;" >';
				    foreach ($increm[result] AS $increm_row)	{	
						$data .=' 
						        <tr style="border-bottom: 1px solid #85b1de;">
						          <td style="width:100%; display:block;word-wrap:break-word; padding:5px 0;">'.$increm_row[name].'</td>													 
						          <td style="width:30px;"><button type="button" value="media/uploads/file/'.$increm_row[rand_name].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none;background-image:url(\'media/images/get.png\');" id="download" ></button><input type="text" style="display:none;" id="download_name" value="'.$increm_row[rand_name].'"> </td>
						          <td style="width:30px;"><button type="button" value="'.$increm_row[id].'" style="cursor:pointer; border:none; margin-top:25%; display:block; height:16px; width:16px; background:none; background-image:url(\'media/images/x.png\');" id="delete"></button></td>
						        </tr>';
					}
			         
	   $data .= '</table>
  			</fieldset>		
		</div>
		<input type="hidden" id="actionn_id" value="'.$res['id'].'"/>
		<input type="hidden" id="act_id" value="'.$hidde_id.'"/>
		<input type="hidden" id="h_start_date" value="'.$res['start_date'].'"/>
		<input type="hidden" id="h_end_date" value="'.$res['end_date'].'"/>
    </div>';

	return $data;
}
?>