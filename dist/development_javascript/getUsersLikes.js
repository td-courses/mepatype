
const getUserHowLiked = (postSmile) => {
    const postId = postSmile.parentElement.parentElement.dataset.postid;
    document.querySelectorAll('[data-typeemoji]').forEach(el => el.classList.remove('active-emoji'))
    document.querySelector('[data-typeemoji="happy"]').classList.add('active-emoji');


    const likesPopElementUsersContainer = document.querySelector('.likes-popup-window__container__users').querySelector('ul');
    const likesPopElement = document.querySelector('.likes-popup-window');
    likesPopElement.classList.add('display-flex');

    document.querySelector('.likes-popup-window__blacked-BG').addEventListener('click', _ => {
        likesPopElement.classList.remove('display-flex');
    })




    let parser = new DOMParser();

    let actAndPostId = {
        act: "get_emoji",
        post_id: postId
    }

    var AjaxURL = "server-side/call/action_action.php";

    $.ajax({
        url: AjaxURL,
        data: actAndPostId,
        dataType: 'json',
        success: function (result) {
            const likesPopElement = document.querySelector('.likes-popup-window');
            
            likesPopElement.querySelectorAll('span').forEach(span => span.innerHTML = 0)

            let happy = result.happy;
            let love = result.love;
            let amazed = result.amazed;
            let neutral = result.neutral;
            let sad = result.sad;





            let happyUsers = [];
            let loveUsers = [];
            let amazedUsers = [];
            let neutralUsers = [];
            let sadUsers = [];

            if (happy) {
                const happyEmoji = likesPopElement.querySelector('.happy').parentElement.querySelector('span').innerHTML = happy.amount;
                happyUsers = [...happy.users];
            }
            if (love) {
                const loveEmoji = likesPopElement.querySelector('.love').parentElement.querySelector('span').innerHTML = love.amount;
                loveUsers = [...love.users];
            }
            if (amazed) {
                const amazedEmoji = likesPopElement.querySelector('.amazed').parentElement.querySelector('span').innerHTML = amazed.amount;
                amazedUsers = [...amazed.users];
            }
            if (neutral) {
                const neutralEmoji = likesPopElement.querySelector('.neutral').parentElement.querySelector('span').innerHTML = neutral.amount;
                neutralUsers = [...neutral.users];
            }
            if (sad) {
                const sadEmoji = likesPopElement.querySelector('.sad').parentElement.querySelector('span').innerHTML = sad.amount;
                sadUsers = [...sad.users];
            }

            likesPopElementUsersContainer.innerHTML = "";

            happyUsers.forEach(user => {
                let userElement = parser.parseFromString(`
                                    <li class="whoLiked" data-userliketype='happy'>
                                        <p>${user}</p>
                                    </li>`, "text/html")
                likesPopElementUsersContainer.append(userElement.body.firstChild)
            })
            loveUsers.forEach(user => {
                let userElement = parser.parseFromString(`
                                    <li class="whoLiked" data-userliketype='love'>
                                        <p>${user}</p>
                                    </li>`, "text/html")
                likesPopElementUsersContainer.append(userElement.body.firstChild)
            })
            amazedUsers.forEach(user => {
                let userElement = parser.parseFromString(`
                                    <li class="whoLiked" data-userliketype='amazed'>
                                        <p>${user}</p>
                                    </li>`, "text/html")
                likesPopElementUsersContainer.append(userElement.body.firstChild)
            })
            neutralUsers.forEach(user => {
                let userElement = parser.parseFromString(`
                                    <li class="whoLiked" data-userliketype='neutral'>
                                        <p>${user}</p>
                                    </li>`, "text/html")
                likesPopElementUsersContainer.append(userElement.body.firstChild)
            })
            sadUsers.forEach(user => {
                let userElement = parser.parseFromString(`
                                    <li class="whoLiked" data-userliketype='sad'>
                                        <p>${user}</p>
                                    </li>`, "text/html")
                likesPopElementUsersContainer.append(userElement.body.firstChild)
            })


            
            const toShowUsers = document.querySelectorAll(`[data-userliketype="happy"]`);
            toShowUsers.forEach(users => users.classList.add('display-block'))

        }
    })

}




const showReactionUsers = (emoji) => {
    emoji.classList.add('active-emoji');

    const emojiElements = document.querySelector('.likes-popup-window__container__emojis').querySelectorAll('li');
    const toShowUsers = document.querySelectorAll(`[data-userliketype = ${emoji.dataset.typeemoji}]`);
    const allUsers = document.querySelectorAll(`.whoLiked`);



    const otherElementsArray = [...emojiElements];
    const otherElements = otherElementsArray.filter(elem => elem != emoji);


    otherElements.forEach(emoji => emoji.classList.remove('active-emoji'))

    allUsers.forEach(users => users.classList.remove('display-block'))
    toShowUsers.forEach(users => users.classList.add('display-block'))



} 