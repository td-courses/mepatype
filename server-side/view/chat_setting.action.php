<?php

require_once('../../includes/classes/class.Mysqli.php');
global $db;

$db     = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_setting':
		$db->setQuery("SELECT   `ip_count`,
                                `welcome_on_off`,
                                `welcome_text`,
                                `input_on_off`,
                                `input_name`,
                                `input_mail`,
                                `input_phone`,
                                `sound_on_off`,
                                `color`,
                                `text_color`,
                                `send_file`,
                                `send_mail`,
                                `send_sound`,
                                `send_close`,
                                `close_time`,
                                `news_time`,
                                `start_work`,
                                `end_work`,
                                `taimer`
                        FROM    `chat_setting`;");
		
		$res = $db->getResultArray();
		
		$data = array(  'ip_count'	     => $res[result][0][ip_count],
                        'welcome_on_off' => $res[result][0][welcome_on_off],
                        'welcome_text'	 => $res[result][0][welcome_text],
                        'input_on_off'	 => $res[result][0][input_on_off],
                        'input_name'	 => $res[result][0][input_name],
                        'input_mail'	 => $res[result][0][input_mail],
                        'input_phone'	 => $res[result][0][input_phone],
                        'sound_on_off'	 => $res[result][0][sound_on_off],
                        'color_id'       => $res[result][0][color],
                        'text_color'     => $res[result][0][text_color],
                        'send_file'      => $res[result][0][send_file],
                        'send_mail'      => $res[result][0][send_mail],
                        'send_sound'     => $res[result][0][send_sound],
                        'send_close'     => $res[result][0][send_close],
                        'close_time'     => $res[result][0][close_time],
                        'news_time'      => $res[result][0][news_time],
                        'start_work'      => $res[result][0][start_work],
                        'end_work'      => $res[result][0][end_work],
                        'taimer'         => $res[result][0][taimer]);
		
        break;
	case 'save_setting':
                
                $db->setQuery("UPDATE `chat_setting` 
                                SET `ip_count`       = '$_REQUEST[ip_count]',
                                `welcome_on_off` = '$_REQUEST[welcome_on_off]',
                                `input_on_off`   = '$_REQUEST[input_on_off]',
                                `input_name`     = '$_REQUEST[input_name]',
                                `input_mail`     = '$_REQUEST[input_mail]', 
                                `input_phone`    = '$_REQUEST[input_phone]',
                                `sound_on_off`   = '$_REQUEST[sound_on_off]',
                                `color`          = '$_REQUEST[color_id]',
                                `text_color`     = '$_REQUEST[text_color]',
                                `send_file`      = '$_REQUEST[send_file]',
                                `send_mail`      = '$_REQUEST[send_mail]',
                                `send_sound`     = '$_REQUEST[send_sound]',
                                `send_close`     = '$_REQUEST[send_close]',
                                `close_time`     = '$_REQUEST[close_time]',
                                `news_time`      = '$_REQUEST[news_time]',
                                `start_work`      = '$_REQUEST[start_work]',
                                `end_work`      = '$_REQUEST[end_work]',
                                `taimer`         = '$_REQUEST[taimer]'");
                
                $db->execQuery();


                $groups = explode(',', $_REQUEST['groups_perms']);

                $db->setQuery("DELETE FROM allowed_groups_to_view_chat");
                $db->execQuery();
                
                foreach($groups AS $item){
                        $db->setQuery("INSERT INTO allowed_groups_to_view_chat SET group_id = '$item'");
                        $db->execQuery();
                }
        break;
        case "get_allowed_groups":
                $db->setQuery(" SELECT  GROUP_CONCAT(group_id) AS groups
                                FROM    `allowed_groups_to_view_chat`");
                $groups = $db->getResultArray();
		$groups = $groups['result'][0]['groups'];
		if($groups == ''){
			$groups = 0;
		}
		$data['groups'] = $groups;
        break;
        case "get_groups":
                $db->setQuery(" SELECT  id, name
                                FROM    `group`
                                WHERE   actived = 1");
                $data = $db->getResultArray();

        break;
        case "get_list_translations":
                $count = $_REQUEST['count'];
                $hidden = $_REQUEST['hidden'];
                    $query = " SELECT `translation`.`key`,     
                                        if(`lang_id` = 1,`translation`,'') AS `geo`,
                                        (SELECT `translation` FROM `translation` AS `eng` WHERE `key` = `translation`.`key` AND `lang_id` = 2 ) AS `eng`,
                                        (SELECT `translation` FROM `translation` AS `eng` WHERE `key` = `translation`.`key` AND `lang_id` = 3 )  AS `ru`
                                FROM `translation` 
                                WHERE `actived` = 1 group by `key`
 ";
    
                $db->setQuery($query);
                $data = $db->getList($count,$hidden,0);

        break;
        case 'get_welcome_list':
                $count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
                $query = "SELECT chat_welcome_text.id,
                                             
                                             chat_welcome_text.welcome_text,
                                             IF(ISNULL(chat_setting.welcome_text),CONCAT('<button data-id=\"',chat_welcome_text.id,'\" style=\"height: 22px; background-color: #2dc100;\" class=\"download15\"><span style=\"vertical-align: 1px; font-size: 13px;\">გააქტიურება</span></button>'),CONCAT('<button data-id=\"',chat_welcome_text.id,'\" class=\"download2\">აქტიური</button>')) 
                                      FROM chat_welcome_text
                                      LEFT JOIN chat_setting ON chat_setting.welcome_text=chat_welcome_text.id
                                      WHERE actived='1'";
                $db->setQuery($query);
                $data = $db->getList(3,$hidden,1);
                
        break;
        case 'update_welcome':
                $welcome_id = $_REQUEST['wlc_id'];
                $db->setQuery("UPDATE chat_setting SET welcome_text='$welcome_id' WHERE id='1'");
                $db->execQuery();
                
        break;
        case "get_add_page": // news
                $page = get_news_page();
                $data = array("page" => $page);
        break;
        case "get_edit_page": // news
                $id = $_REQUEST['id'];
                $page =  get_news_page(GetWelcome($id));
                $data = array("page" => $page);
        break;
        case "get_list_news":
                $count = $_REQUEST['count'];
                $hidden = $_REQUEST['hidden'];
                    $query = "SELECT `key`,     if(`lang_id` = 1,`news`,'') AS `geo`,
                    (SELECT `news` FROM `chat_news` AS `eng` WHERE `key` = `chat_news`.`key` AND `lang_id` = 2 ) AS `eng`,
                    (SELECT `news` FROM `chat_news` AS `ru` WHERE `key` = `chat_news`.`key` AND `lang_id` = 3 ) AS `ru`
                                                                   FROM `chat_news`
                                                                   
                                                                   WHERE `actived` = 1 group by `key`";
    
                $db->setQuery($query);
                $data = $db->getList($count,$hidden,1);

        break;
        case "save_news":
                $id = $_REQUEST['id'];
                $text = $_REQUEST['text'];
                if($id != ''){
                        $db->setQuery("UPDATE chat_welcome_text SET welcome_text='$text' WHERE id='$id'");
                        $db->execQuery();
                }
                else{
                        $db->setQuery("INSERT INTO chat_welcome_text(`welcome_text`) VALUES('$text')");
                        $db->execQuery();
                }
                
        break;
        case "save_translation":
                $key = $_REQUEST["key"];
		$geo = $db->escp($_REQUEST["geo"]);
		$eng = $db->escp($_REQUEST["eng"]);
                $ru =  $db->escp($_REQUEST["ru"]);

                
                $db->setQuery("UPDATE translation SET translation = '$geo' WHERE `key` = '$key' AND `lang_id` = 1");
                $db->execQuery();

                $db->setQuery("SELECT id FROM translation WHERE `key` = '$key' AND `lang_id` = 2");
                $num = $db->getNumRow();

                if($num > 0){
                        $db->setQuery("UPDATE translation SET translation = '$eng' WHERE `key` = '$key' AND `lang_id` = 2");
                        $db->execQuery();
                }
                else{
                        $db->setQuery("INSERT INTO translation SET translation = '$eng', `key` = '$key', `lang_id` = 2");
                        $db->execQuery();   
                }

                        
                $db->setQuery("SELECT id FROM translation WHERE `key` = '$key' AND `lang_id` = 3");
                $num = $db->getNumRow();

                if($num > 0){
                        $db->setQuery("UPDATE translation SET translation = '$ru' WHERE `key` = '$key' AND `lang_id` = 3");
                        $db->execQuery();
                }
                else{
                        $db->setQuery("INSERT INTO translation SET translation = '$ru', `key` = '$key', `lang_id` = 3");
                        $db->execQuery();   
                }

        break;
        case "get_dialog_translation":
                $id = $_REQUEST['id'];
                $page =  get_translation_page(get_translation($id));
                $data = array("page" => $page);
        break;
        case "disable":
                $id = $_REQUEST['id'];
                delete($id);
        break;
        
	default:
		$error = 'Action is Null';
}


function get_translation($id){
        global $db;
    $query = "	 SELECT `translation`.`key`,     
                if(`lang_id` = 1,`translation`,'') AS `geo`,
                (SELECT `translation` FROM `translation` AS `eng` WHERE `key` = `translation`.`key` AND `lang_id` = 2 ) AS `eng`,
                (SELECT `translation` FROM `translation` AS `eng` WHERE `key` = `translation`.`key` AND `lang_id` = 3 )  AS `ru`
                FROM `translation` 
                WHERE `actived` = 1  AND `translation`.`key` = '$id'  GROUP BY `key`";
											
         $db -> setQuery($query);
	$result = $db -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}
function GetWelcome($wlc_id)
{
        global $db;
        $db->setQuery(" SELECT  `id`,
                                `welcome_text`
                        FROM    `chat_welcome_text`
                        WHERE   `id` = $wlc_id");
        $result = $db->getResultArray();
        $res = $result['result'][0];

        return $res;
}
function get_news($id){
        global $db;
    $query = "	 SELECT `key`,     if(`lang_id` = 1,`news`,'') AS `geo`,
     (SELECT `news` FROM `chat_news` AS `eng` WHERE `key` = `chat_news`.`key` AND `lang_id` = 2 ) AS `eng`,
     (SELECT `news` FROM `chat_news` AS `ru` WHERE `key` = `chat_news`.`key` AND `lang_id` = 3 ) AS `ru`
                                                    FROM `chat_news`
                                                    
                                                    WHERE `actived` = 1 AND `key` = '$id' group by `key`";
											
         $db -> setQuery($query);
	$result = $db -> getResultArray();

	$res = $result['result'][0];
											
	return $res;
}

function get_translation_page($res){
        return '

                <div>
                        <div>
                                <label>ქართული</label>
                                <textarea class="translate_textarea" id="input_geo">'.$res['geo'].'</textarea>
                        </div>
                        <div>
                                <label>ინგლისური</label>
                                <textarea class="translate_textarea" id="input_eng">'.$res['eng'].'</textarea>
                        </div>
                        <div>
                                <label>რუსული</label>
                                <textarea class="translate_textarea" id="input_ru">'.$res['ru'].'</textarea>
                        </div>

                </div>
                <input type="hidden" id="key_hidden" value="'.$res['key'].'" />

        ';
}

function get_news_page($res){
        return '

        <div id="dialog-form">
                <fieldset>
                        <legend>ძირითადი ინფორმაცია</legend>

                        <table class="dialog-form-table">
                                <tr>
                                        <td style="width: 70px;"><label for="welcome_text">დასახელება</label></td>
                                        <td>
                                                
                                                <textarea id="welcome_text" rows="10" style="width:350px; resize: vertical;height: 70px;">' . $res['welcome_text'] . '</textarea>
                                        </td>
                                </tr>
                        </table>
                        <!-- ID -->
                        <input type="hidden" id="wlc" value="' . $res['id'] . '" />
        </fieldset>
        </div>


        ';
}

function delete($id){
        global $db;
        $db->setQuery("UPDATE chat_welcome_text SET actived='0' WHERE `id` = '$id' ");
	    
	$db->execQuery();
}

$data['error'] = $error;

echo json_encode($data);
?>
