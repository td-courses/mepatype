import Button from "../../components/button/button.class.js";
import Tdg from "../../tdg.class.js"
import IncommingModal from "./incomming.modal.js";
import Flashpanel from "../../layouts/flashpanel.class.js";
import Input from "../../components/input/input.class.js";
import { kendo_SelectedID } from "../../helpers/kendo.helper.js";

export default class Incomming extends Tdg{

    constructor(){
        super();

        self.Incomming = this;
        self.Tdg = this;
        

        this.state = {
            sectionName: document.querySelector("section[incomming]"),
            element: [],
            flashpanel: new Flashpanel().init(),
            savedInputValues: []
        }

        document.title = "Incomming";
        
        this.removeLoading()

        this.init();

    }

    init = () => {
        

        this.append(this.state.sectionName, this.Filterblock());
        this.append(this.state.element.filterBlock, this.selectorStatus())
        this.append(this.state.element.filterBlock, this.multiSelector())

        this.append(this.state.sectionName, this.createTable())

        this.append(this.state.element.filterBlock, this.datetime())

        this.append(this.state.element.filterBlock, new Input({
            type: "text"
        }).build())

        
        this.append(this.state.element.filterBlock, this.newButton())

        

    }

    datetime = () => {

        this.state.element.datetime = this.CreateElement({
            element: "input",
            type: "datetime"
        })

      
        return this.state.element.datetime
    }

    newButton = () => {

        this.state.element.button = new Button({
            text: "ფილტრი",
            onclick: () =>{
                console.log("FILTER")
            }
        })

        return this.state.element.button
    }


    selectorStatus = () => {

        this.state.element.status = this.CreateElement({
            element: "kendo",
            type: "selector",
            title: "ექსტენშენი",
            data: {
                route: "Authorization",
                act: "getExt"
            }
        })

        return this.state.element.status
    }

    multiSelector = () => {
        this.state.element.multi = this.CreateElement({
            element: "kendo",
            type: "multiselector",
            title: "ექსტენშენი",
            data: {
                route: "Authorization",
                act: "getExt"
            }
        })

        return this.state.element.multi
    }


    Filterblock = () => {

        this.state.element.filterBlock = this.CreateElement({
            element: "filterblock"
        })

        return this.state.element.filterBlock;

    } 

    
    createTable = () => {

        this.state.element.table = this.CreateElement({
            element: "kendo",
            type: "table",
            column: [
                {
                    field: "id",
                    hidden: false
                },
                {
                    field: "თარიღი"
                },
                {
                    field: "ოპერატორი"
                },
                {
                    field: "ტელეფონი"
                },
                {
                    field: "აბონენტი"
                },
                {
                    field: "აბონენტის ნომერი"
                },
                {
                    field: "მ/ცენტრი"
                },
                {
                    field: "კატეგორია"
                },
                {
                    field: "რეაგირება"
                }
            ],
            data: {
                route: "Incomming",
                act: "getList"
            },
            
            option: {
                header: true,
                numeric: false,
                export: {
                    excel: true,
                    pdf: true
                },
                footer: true
            },
            ondblclick: (cb) => {
               
                this.buildModal(null,  {
                    width: '100vw',
                    height: '100vh',
                    content: new IncommingModal({
                        ...cb
                    }).init(),
                    buttons: {
                        save: {
                            name: "შენახვა",
                            onclick: function(modal){
                                self.WorkGround.state.inputs.forEach(x => self.Incomming.getInputDatas(x));

                                self.Tdg.getResponse({
                                    route: "Automator",
                                    act: "saveModalData",
                                    pageKey: "incommingRequest",
                                    id: cb.id,
                                    ...self.Incomming.state.savedInputValues
                                }).then(alert('შენახულია'))
                            },
                            access: "view"
                        }
                    },
                    confirmOnCancel: true
                })

            },
            callback: (cb) => {
                // console.log(cb)
            }
        })

        return this.state.element.table;

    }

    getInputDatas = (input) => {
        

        switch(input.getAttribute("type")){
            case "text":
                self.Incomming.state.savedInputValues[input.getAttribute("id")] = input.value;
            break;
            case "textarea":
                self.Incomming.state.savedInputValues[input.getAttribute("id")] = input.value;
            break;
            case "date":
                self.Incomming.state.savedInputValues[input.getAttribute("id")] = input.value;
            break;
            case "datetime":
                self.Incomming.state.savedInputValues[input.getAttribute("id")] = input.value;
            break;
            case "radio":

            break;
            case "checkbox":

            break;
            case "selector":
                self.Incomming.state.savedInputValues[input.getAttribute("id")] = kendo_SelectedID(input);
            break;
            case "multiselect":

            break;
        }
    }


}