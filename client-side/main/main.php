<html>
<link rel="shortcut icon" href="favicon.ico">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Morris Charts CSS -->
<link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css" />

<!-- Data table CSS -->
<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="dist/css/style.css" rel="stylesheet" type="text/css">

<head>


    <style type="text/css">
        div.row:nth-child(11) {
            padding: 4vw 0 0 25rem !important;
        }

        .row {
            margin-right: 5px !important;
            margin-left: 5px !important;
            font-family: noto-bold, lexend-regular;
        }

        .svg_transform {
            margin-top: 40%;
        }

        .card-view.panel .panel-body {
            padding: 5px 0 10px;
        }

        table#MainTable td {
            padding: 10px 10px 10px 0px;
            margin: 10px 10px 10px 10px;

        }

        .label-chatrs {
            height: 35px;
        }

        .style_for_position {
            position: unset !important;
        }

        * {
            box-sizing: border-box;
        }

        #flesh_panel {
            font-size: 11px;
            right: 43px;
        }

        /* Create two equal columns that floats next to each other */
        .drop-down-chart {
            cursor: pointer;
            position: absolute;
            margin-top: -5px;
            color: red;
            font-weight: bold;
        }

        div.panel-heading {
            padding: 10px 15px !important;
        }


        /*        fdsfsad*/



        /* Create two equal columns that floats next to each other */
        .column {
            margin-top: 14px;
            float: left;
            width: 397px;
            /* Should be removed. Only for demonstration */
        }

        .column1 {
            margin-top: 14px;
            float: left;
            min-width: 410px;
            max-width: 100%;

        }

        
        .sectionforiframe{
            width: 86%;
            margin-left: 236px;
        }
        .divforiframe{
            width: 100%;
            display: flex;
        }

        .iframework{
            margin: 78px auto;
            width: 100%;
            height: 1700px;
        }

        /* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
    </style>
    <script src="includes/angular.min.js"></script>
    <script src="includes/apexchart.js"></script>
    <script src="angular_ctrl.js"></script>


</head>

<body>

<section class="sectionforiframe">
            <div class="divforiframe">
                <iframe class="iframework" src="dashboard/" frameborder="0"></iframe>
            </div>
        </section>





    <!-- Progressbar Animation JavaScript -->
    <script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
    <script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

    <!-- Owl JavaScript -->
    <script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>


</body>

</html>