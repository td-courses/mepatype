

var aJaxURL = "server-side/main.action.php";

var tab = 'main';

var app = angular.module("myApp", []);
app.controller("myCtrl", function ($scope) {

    $.ajax({
        url:"person_image.php",
        dataType: "json",
        async: false,
        success:function (data) {

            $scope.profile_img=data;
        }
    });

    param = new Object();
    param.act = tab;

    param = new Object();
    param.act = tab;

    $scope.inc_block="block";
    $scope.zmdi_caret_down="ti-angle-double-down";
    $scope.vs_block="block";
    $scope.vs_zmdi_caret_down="ti-angle-double-down";
    $scope.sl_block="block";
    $scope.sl_zmdi_caret_down="ti-angle-double-down";
    $scope.asa_block="block";
    $scope.asa_zmdi_caret_down="ti-angle-double-down";

    $scope.Changeinc = function() {
        if ($scope.zmdi_caret_down=="ti-angle-double-up") {
            $scope.zmdi_caret_down="ti-angle-double-down";
        }else {
            $scope.zmdi_caret_down="ti-angle-double-up";
        }

        if($scope.inc_block=="none"){
            $scope.inc_block="block"
        }else {
            $scope.inc_block="none"
        }

    };

    $scope.Changevs = function() {
        if ($scope.vs_zmdi_caret_down=="ti-angle-double-up") {
            $scope.vs_zmdi_caret_down="ti-angle-double-down";
        }else {
            $scope.vs_zmdi_caret_down="ti-angle-double-up";
        }

        if($scope.vs_block=="none"){
            $scope.vs_block="block"
        }else {
            $scope.vs_block="none"
        }

    };

    $scope.Changesl = function() {
        if ($scope.sl_zmdi_caret_down=="ti-angle-double-up") {
            $scope.sl_zmdi_caret_down="ti-angle-double-down";
        }else {
            $scope.sl_zmdi_caret_down="ti-angle-double-up";
        }

        if($scope.sl_block=="none"){
            $scope.sl_block="block"
        }else {
            $scope.sl_block="none"
        }

    };

    $scope.Changeasa = function() {
        if ($scope.asa_zmdi_caret_down=="ti-angle-double-up") {
            $scope.asa_zmdi_caret_down="ti-angle-double-down";
        }else {
            $scope.asa_zmdi_caret_down="ti-angle-double-up";
        }

        if($scope.asa_block=="none"){
            $scope.asa_block="block"
        }else {
            $scope.asa_block="none"
        }

    };

    $.ajax({
        url: aJaxURL,
        data: param,
        async: false,
        success: function (data) {

            var datachart1=data['datachart1'];
            var vsdata = data['vs'];
            var sldata = data['sl'];
            var asadata = data['asa'];
            var sl_color = data['sl_color'];



            $scope.real_call_inc=datachart1.call_percent;
            $scope.real_chat_inc=datachart1.chat_percent;
            $scope.real_cb_inc=datachart1.messenger_percent;
            $scope.real_fb_inc=datachart1.fb_percent;
            $scope.real_viber_inc=datachart1.viber_percent;
            $scope.real_mail_inc=datachart1.mail_percent;
            $scope.real_video_call_inc=datachart1.video_percent;
            $scope.real_web_call_inc=datachart1.webcall_percent;

            $scope.real_call_color=datachart1.call_color;
            $scope.real_chat_color=datachart1.chat_color;
            $scope.real_cb_color=datachart1.messenger_color;
            $scope.real_fb_color=datachart1.fb_color;
            $scope.real_viber_color=datachart1.viber_color;
            $scope.real_mail_color=datachart1.mail_color;
            $scope.real_video_call_color=datachart1.video_color;
            $scope.real_web_call_color=datachart1.webcall_color;

            $scope.call_count_color=datachart1.call_color;
            $scope.chat_count_color=datachart1.chat_color;
            $scope.messenger_count_color=datachart1.messenger_color;
            $scope.fb_count_color=datachart1.fb_color;
            $scope.viber_count_color=datachart1.viber_color;
            $scope.mail_count_color=datachart1.mail_color;
            $scope.video_count_color=datachart1.video_color;
            $scope.webcall_count_color=datachart1.webcall_color;

            $scope.call_transform=datachart1.call_transform;
            $scope.chat_transform=datachart1.chat_transform;
            $scope.cb_transform=datachart1.messenger_transform;
            $scope.fb_transform=datachart1.fb_transform;
            $scope.viber_transform=datachart1.viber_transform;
            $scope.mail_transform=datachart1.mail_transform;
            $scope.video_call_transform=datachart1.video_transform;
            $scope.web_call_transform=datachart1.webcall_transform;

            $scope.sl_call_color = sl_color[0];
            $scope.sl_chat_color = sl_color[1];
            $scope.sl_messenger_color = sl_color[2];
            $scope.sl_fb_color = sl_color[3];
            $scope.sl_viber_color = sl_color[4];
            $scope.sl_mail_color = sl_color[5];
            $scope.sl_video_color = sl_color[6];
            $scope.sl_webcall_color = sl_color[7];

            $scope.sl_call_transform = sl_color[8];
            $scope.sl_chat_transform = sl_color[9];
            $scope.sl_messenger_transform = sl_color[10];
            $scope.sl_fb_transform = sl_color[11];
            $scope.sl_viber_transform = sl_color[12];
            $scope.sl_mail_transform = sl_color[13];
            $scope.sl_video_transform = sl_color[14];
            $scope.sl_webcall_transform = sl_color[15];

            $scope.sl_real = sl_color[16];
            $scope.sl_real_color = sl_color[17];

            $scope.vs_plan = vsdata.call_vs_avg;
            $scope.vs_plan_call = vsdata.call_vs;
            $scope.vs_plan_chat = vsdata.chat_vs;
            $scope.vs_plan_cb = vsdata.chat_cb_vs;
            $scope.vs_plan_fb = vsdata.chat_fb_vs;
            $scope.vs_plan_viber = vsdata.viber_vs;
            $scope.vs_plan_mail = vsdata.chat_mail_vs;
            $scope.vs_plan_video_call = vsdata.video_call_vs;
            $scope.vs_plan_web_call = vsdata.web_call_vs;

            $scope.sl_plan = sldata.call_sl_avg;
            $scope.sl_plan_call = sldata.call_sl;
            $scope.sl_plan_chat = sldata.chat_sl;
            $scope.sl_plan_cb = sldata.chat_cb_sl;
            $scope.sl_plan_fb = sldata.chat_fb_sl;
            $scope.sl_plan_viber = sldata.viber_sl;
            $scope.sl_plan_mail = sldata.chat_mail_sl;
            $scope.sl_plan_video_call = sldata.video_call_sl;
            $scope.sl_plan_web_call = sldata.web_call_sl;

            $scope.sl_call = sldata.sl_call;
            $scope.sl_chat = sldata.sl_chat;
            $scope.sl_messenger = sldata.sl_messenger;
            $scope.sl_fb = sldata.sl_fb;
            $scope.sl_viber = sldata.sl_viber;
            $scope.sl_video = sldata.sl_video;
            $scope.sl_mail = sldata.sl_mail;
            $scope.sl_webcall = sldata.sl_webcall;

            $scope.sl_call_diff = sldata.sl_call_diff;
            $scope.sl_chat_diff = sldata.sl_chat_diff;
            $scope.sl_messenger_diff = sldata.sl_messenger_diff;
            $scope.sl_fb_diff = sldata.sl_fb_diff;
            $scope.sl_viber_diff = sldata.sl_viber_diff;
            $scope.sl_video_diff = sldata.sl_video_diff;
            $scope.sl_mail_diff = sldata.sl_mail_diff;
            $scope.sl_webcall_diff = sldata.sl_webcall_diff;

            $scope.sl_plan_percent = sldata.call_sl_avg_percent;
            $scope.sl_plan_call_precent = sldata.call_sl_percent;
            $scope.sl_plan_chat_precent = sldata.chat_sl_percent;
            $scope.sl_plan_cb_precent = sldata.chat_cb_sl_percent;
            $scope.sl_plan_fb_precent = sldata.chat_fb_sl_percent;
            $scope.sl_plan_viber_precent = sldata.viber_sl_percent;
            $scope.sl_plan_mail_precent = sldata.chat_mail_sl_percent;
            $scope.sl_plan_video_call_precent = sldata.video_call_sl_percent;
            $scope.sl_plan_web_call_precent = sldata.web_call_sl_percent;

            $scope.asa_plan = asadata.call_asa_avg;
            $scope.asa_plan_call = asadata.call_asa;
            $scope.asa_plan_chat = asadata.chat_asa;
            $scope.asa_plan_cb = asadata.chat_cb_asa;
            $scope.asa_plan_fb = asadata.chat_fb_asa;
            $scope.asa_plan_viber = asadata.viber_asa;
            $scope.asa_plan_mail = asadata.chat_mail_asa;
            $scope.asa_plan_video_call = asadata.video_call_asa;
            $scope.asa_plan_web_call = asadata.web_call_asa;

            $scope.asa_call = asadata.asa_call;
            $scope.asa_chat = asadata.asa_chat;
            $scope.asa_messenger = asadata.asa_messenger;
            $scope.asa_fb = asadata.asa_fb;
            $scope.asa_viber = asadata.asa_viber;
            $scope.asa_video = asadata.asa_video;
            $scope.asa_mail = asadata.asa_mail;
            $scope.asa_webcall = asadata.asa_webcall;

            $scope.asa_call_diff = asadata.asa_call_diff;
            $scope.asa_chat_diff = asadata.asa_chat_diff;
            $scope.asa_messenger_diff = asadata.asa_messenger_diff;
            $scope.asa_fb_diff = asadata.asa_fb_diff;
            $scope.asa_viber_diff = asadata.asa_viber_diff;
            $scope.asa_video_diff = asadata.asa_video_diff;
            $scope.asa_mail_diff = asadata.asa_mail_diff;
            $scope.asa_webcall_diff = asadata.asa_webcall_diff;

            $scope.asa_call_color = asadata.asa_call_color;
            $scope.asa_chat_color = asadata.asa_chat_color;
            $scope.asa_messenger_color = asadata.asa_messenger_color;
            $scope.asa_fb_color = asadata.asa_fb_color;
            $scope.asa_viber_color = asadata.asa_viber_color;
            $scope.asa_videocall_color = asadata.asa_video_color;
            $scope.asa_mail_color = asadata.asa_mail_color;
            $scope.asa_webcall_color = asadata.asa_webcall_color;

            $scope.asa_call_transform = asadata.asa_call_transform;
            $scope.asa_chat_transform = asadata.asa_chat_transform;
            $scope.asa_messenger_transform = asadata.asa_messenger_transform;
            $scope.asa_fb_transform = asadata.asa_fb_transform;
            $scope.asa_viber_transform = asadata.asa_viber_transform;
            $scope.asa_videocall_transform = asadata.asa_video_transform;
            $scope.asa_mail_transform = asadata.asa_mail_transform;
            $scope.asa_webcall_transform = asadata.asa_webcall_transform;

            $scope.asa_real = asadata.asa_real;
            $scope.asa_real_color = asadata.asa_real_color;


            for (var chartID in data[tab]) {


                var chartData = data[tab][chartID];
                var typeData = chartData.typeData;
                var colorsData = chartData.colorsData;
                var titleText = chartData.titleText;
                var seriesName = chartData.seriesName;
                var seriesData = chartData.seriesData;
                var categoriesData = chartData.categoriesData;
                var widthChart = chartData.widthChart;
                var heightChart = chartData.heightChart;
                var dataLabels_enable = chartData.dataLabels_enable;
                var yaxis_enable = chartData.yaxis_enable;
                var xaxis_labels = chartData.xaxis_labels;
                var sparkline = chartData.sparkline;
                var vs_percent = chartData.vs_percent;
                var xshow= chartData.xshow;

                if (chartData.name == 'chart3') {


                    $scope.vs_real = chartData.vs_percent[0];
                    $scope.vs_real_color = chartData.vs_percent[1];
                    $scope.vs_real_call = chartData.vs_percent[2];
                    $scope.vs_real_web_call = chartData.vs_percent[3];
                    $scope.vs_real_call_color = chartData.vs_percent[4];
                    $scope.vs_real_web_call_color = chartData.vs_percent[5];
                    $scope.vs_call_transform = chartData.vs_percent[6];
                    $scope.vs_web_call_transform = chartData.vs_percent[7];
                    $scope.vs_real_call_inc = chartData.vs_percent[8];
                    $scope.vs_real_web_call_inc = chartData.vs_percent[9];
                    $scope.vs_real_call_color_diff = chartData.vs_percent[10];
                    $scope.vs_real_web_call_color_diff = chartData.vs_percent[11];
                }


                if (chartData.name == 'chart1') {

                    $scope.sum_chart1 = chartData.sum_data;

                    $scope.call_count = seriesData[0];
                    $scope.chat = seriesData[1];
                    $scope.site_chat = seriesData[2];
                    $scope.fb_chat = seriesData[3];
                    $scope.viber_chat = seriesData[4];
                    $scope.mail_count = seriesData[5];
                    $scope.video_call = seriesData[6];
                    $scope.web_call_count = seriesData[7];

                    $scope.call_count_percent = (seriesData[0] / chartData.sum_data * 100).toFixed(2);
                    $scope.chat_percent = (seriesData[1] / chartData.sum_data * 100).toFixed(2);
                    $scope.site_chat_percent = (seriesData[2] / chartData.sum_data * 100).toFixed(2);
                    $scope.fb_chat_percent = (seriesData[3] / chartData.sum_data * 100).toFixed(2);
                    $scope.viber_chat_percent = (seriesData[4] / chartData.sum_data * 100).toFixed(2);
                    $scope.mail_count_percent = (seriesData[5] / chartData.sum_data * 100).toFixed(2);
                    $scope.video_call_percent = (seriesData[6] / chartData.sum_data * 100).toFixed(2);
                    $scope.web_call_count_percent = (seriesData[7] / chartData.sum_data * 100).toFixed(2);

                }


                var options = {
                    chart: {

                        dropShadow: {
                            show: false
                        },
                        sparkline: {
                            enabled: sparkline,
                        },
                        width: widthChart,
                        height: heightChart,
                        type: typeData,
                        zoom: {
                            enabled: false
                        },
                        toolbar: {
                            show: false,
                            tools: {
                                show: false
                            }
                        }
                    },
                    labels: categoriesData,
                    colors: colorsData,
                    plotOptions: {

                        bar: {
                            horizontal: false,
                            // endingShape: 'flat',
                            columnWidth: '70%',
                            barHeight: '70%',
                            distributed: true,
                            colors: {
                                ranges: [{
                                    from: 0,
                                    to: 0,
                                    color: undefined
                                }],
                                backgroundBarColors: [],
                                backgroundBarOpacity: 1,
                            },
                            dataLabels: {
                                position: 'top',
                                hideOverflowingLabels: true,

                            }
                        },
                        pie: {
                            size: undefined,
                            customScale: 1,
                            offsetX: 0,
                            offsetY: 0,
                            expandOnClick: true,
                            dataLabels: {
                                offset: 0,
                                minAngleToShowLabel: 10
                            },
                            donut: {
                                size: 50
                            }
                        }
                    },
                    markers: {
                        size: 0
                    },
                    dataLabels: {
                        enabled: dataLabels_enable,
                        offsetY: -30,
                        style: {
                            fontSize: '12px',
                        }
                    },
                    title: {
                        text: titleText,
                        align: 'left',
                        style: {
                            fontSize: '20px',
                            color: '#FFF'
                        },
                    },
                    series: seriesData,
                    legend: {

                        "show": false,
                        "position": "bottom",
                        "itemMargin": {
                            "horizontal": 5,
                            "vertical": 5
                        }
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                                return val
                            }
                        },
                        x: {
                            show: xshow
                        }
                    },
                    yaxis: {
                        labels: {
                            show: yaxis_enable,
                            style: {
                                color: '#333333'
                            }
                        }
                    },
                    stroke: {
                        curve: "straight",
                        width: 1
                    },
                    markers: {

                        size: 1,
                        strokeColor: "#000000",
                        strokeWidth: 1,
                        strokeOpacity: 1,
                        fillOpacity: 1,
                        hover: {
                            size: 0
                        }
                    },
                    xaxis: {
                        categories: categoriesData,
                        axisBorder: {
                            show: false
                        },

                        axisTicks: {
                            show: false
                        },
                        crosshairs: {
                            fill: {
                                type: 'gradient',
                                gradient: {
                                    colorFrom: '#D8E3F0',
                                    colorTo: '#BED1E6',
                                    stops: [0, 100],
                                    opacityFrom: 0.4,
                                    opacityTo: 0.5,
                                }
                            }
                        },
                        labels: {
                            categories: categoriesData,
                            show: xaxis_labels,
                            rotate: -45,
                            rotateAlways: false,
                            hideOverlappingLabels: true,
                            showDuplicates: true,
                            trim: true,
                            style: {
                                colors: colorsData,
                                fontSize: '14px'
                            }
                        },

                        responsive: [{
                            breakpoint: undefined,
                            options: {},
                        }]

                    }


                }

                var chart = new ApexCharts(
                    document.querySelector("#" + chartID),
                    options
                );

                chart.render();

            }

        }

    });
    $(".row").css({display:'block'});

});

