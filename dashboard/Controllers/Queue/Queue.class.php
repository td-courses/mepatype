<?php

use Routers\dbClass;

class Queue extends dbClass{


    public function getQueue(){

        parent::setQuery("  SELECT		IF(chat.chat_status_id = 1, 'waiter', 'taken') AS type,
                                        chat.sender_name AS name,
                                        (SELECT message FROM chat_details WHERE chat_id = chat.id ORDER BY id DESC LIMIT 1) AS lastMessage,
                                        (SELECT DATE_FORMAT(datetime, '%H:%i') FROM chat_details WHERE chat_id = chat.id ORDER BY id DESC LIMIT 1) AS lastDateTime,
                                        chat.first_datetime AS startDatetime,
                                        IFNULL(chat.sender_avatar,'Assets/images/no-image.png') AS imgUrl,
                                        IF((SELECT user_id FROM chat_details WHERE chat_id = chat.id ORDER BY id DESC LIMIT 1) = 0, 'false', 'true') AS newMessage,
                                        source.key AS sourceKey,
                                        chat.id AS id,
                                        incomming_call.id AS incommingId
                                    
                                

                            FROM 		chat
                            JOIN		chat_details ON chat_details.chat_id = chat.id
                            JOIN		source ON source.id = chat.source_id
                            JOIN		incomming_call ON incomming_call.chat_id = chat.id
                            WHERE 		chat.chat_status_id IN (1,2)
                            GROUP BY 	chat.id
                            ORDER BY 	chat.last_datetime DESC");


        $queueList = parent::getResultArray();

        return $queueList['result'][0];
    }

    public function flashPanel(){
        parent::setQuery("  SELECT 		asterisk_extension.id,
                                        user_info.name AS operator,
                                        queueMembers.name AS used_ext,
                                        GROUP_CONCAT(queueMembers.queue) AS queues,
                                        IFNULL(chan.exten,queueMembers.queue) AS `queue_now`,
                                        IFNULL(main_chan.connectedLineNum, IF(chan_out.channelStateDesc = 'Up',chan_out.connectedLineNum,'')) AS `phone`,
                                        CASE
                                            WHEN `main_chan`.`context` = 'macro-dial-one' OR `main_chan`.`context` = 'macro-dial' THEN 'in'
                                            WHEN `chan_out`.`context` = 'macro-dialout-trunk' THEN 'out'
                                            WHEN (SELECT context FROM asterisk.channels WHERE bridgeID = `main_chan`.`bridgeID` AND application='Dial' LIMIT 1) = 'autodialer' THEN 'in-autodialer'
                                        END  AS `type`,
                                        TIME_FORMAT(IFNULL(main_chan.duration, chan_out.duration), '%i:%s') AS `time`,
                                        IF(queueMembers.paused = 1 OR (UNIX_TIMESTAMP(NOW()) - `queueMembers`.`lastCall`) < `queues`.`wrapUpTime`, 'paused', queueMemberStatus.`icon`) AS `status`
                                

                            FROM 		asterisk.queueMembers
                            LEFT JOIN   asterisk.`queues` ON `queues`.`queue` = `queueMembers`.`queue`
                            JOIN        asterisk.queueMemberStatus ON queueMembers.`status` = queueMemberStatus.id
                            JOIN        CallappCoreNEW.asterisk_extension ON asterisk_extension.number = queueMembers.`name`
                            LEFT JOIN   CallappCoreNEW.users ON users.extension_id = asterisk_extension.id AND users.logged = 1
                            LEFT JOIN   CallappCoreNEW.user_info ON user_info.user_id = users.id AND `queueMembers`.status != 'unavailable'


                            LEFT JOIN   asterisk.channels AS main_chan ON main_chan.callerIDNum = queueMembers.`name` AND REPLACE(SUBSTRING_INDEX(queueMembers.location,'@',1),'Local','SIP') = SUBSTRING_INDEX(main_chan.channel,'-',1)
                            LEFT JOIN   asterisk.channels AS chan ON chan.linkedid = main_chan.linkedid AND chan.context = 'from-queue'
                            LEFT JOIN   asterisk.channels AS `chan_out` ON REPLACE(SUBSTRING_INDEX(queueMembers.location,'@',1),'Local','SIP') = SUBSTRING_INDEX(chan_out.channel,'-',1) AND chan_out.context = 'macro-dialout-trunk'
                            WHERE 		queueMembers.queue IN(SELECT CallappCoreNEW.asterisk_queue.number FROM asterisk_queue WHERE actived = 1)
                            GROUP BY    queueMembers.`name`");

        $panelData = parent::getResultArray();
        $panelArray = array();
        foreach($panelData['result'] AS $data){

            $queues = explode(',', $data['queues']);
            $queuesArray = array();
            foreach($queues AS $queue){
                array_push($queuesArray, array("text" => $queue, "background" => "#000", "foreground" => "#FFF"));
            }

            array_push($panelArray, array(  "id" => $data['id'],
                                            "operator" => array("text" => $data['operator'], "avatar" => false, "status" => array("key" => "active", "title" => "აქტიური", "background" => "red"), "extension" => array("text" => $data['used_ext'], "background" => "#B2E9A9", "foreground" => "")),
                                            "accList" => $queuesArray,
                                            "account" => array("text" => $data['queue_now'], "background" => "#FFF7CF", "foreground" => ""),
                                            "sourceKey" => "phone",
                                            "author" => array("text" => "", "avatar" => false, "number" => array("text" => $data['phone'], "background" => "", "foreground" => "")),
                                            "callType" => $data['type'],
                                            "duration" => $data['time'],
                                            "totalDuration" => $data['time']));
        }

        return $panelArray;
        
    }
}

?>