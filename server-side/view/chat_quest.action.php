<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;

$db     = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$id		= $_REQUEST['id'];
		$page		= GetPage(Getproblem($id));
		$data		= array('page'	=> $page);

		break;
	case 'get_list' :
		$count	= $_REQUEST['count'];
		$hidden	= $_REQUEST['hidden'];
			
		$db->setQuery(" SELECT 	  live_chat_question.id,
                                  live_chat_question.`quest`,
								  live_chat_question.`answer`,
								  GROUP_CONCAT(my_web_site.`name`),
                                  live_chat_question.position,
								  SEC_TO_TIME(time)
					    FROM 	  live_chat_question
						LEFT JOIN live_chat_quest_site ON live_chat_quest_site.quest_id = live_chat_question.id
						LEFT JOIN my_web_site ON my_web_site.id = live_chat_quest_site.site_id AND my_web_site.actived = 1
                        WHERE 	  live_chat_question.actived=1
                        GROUP BY live_chat_question.id");
		
		$data = $db->getList($count,$hidden,1);

		break;
	case 'save_problem':
		$id 	= $_REQUEST['id'];
		$quest  = htmlspecialchars($_REQUEST['quest'], ENT_QUOTES);
		$answer = htmlspecialchars($_REQUEST['answer'], ENT_QUOTES);
		
        if ($id == '') {
			Addproblem( $id, $quest, $answer);
		}else {
			Saveproblem($id, $quest, $answer);
		}

        break;
	case 'disable':
		$problem_id	= $_REQUEST['id'];
		Disableproblem($problem_id);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/

function Addproblem($id, $quest, $answer){
	global $db;
	$answer_time = $_REQUEST['answer_time'];
	$my_site     = $_REQUEST['my_site'];
	$position    = $_REQUEST['position'];
	
	$db->setQuery("INSERT INTO 	`live_chat_question`
							   (`quest`,`answer`, `position`, `time`)
					    VALUES 		
                               ('$quest','$answer', '$position', '$answer_time')");
	$db->execQuery();
	$insert_id = $db->getLastId();
	
	$site_array = explode(",",$my_site);
	
	$db->setQuery("DELETE FROM live_chat_quest_site WHERE quest_id = '$insert_id'");
	$db->execQuery();
	
	foreach($site_array AS $site_id){
	    $db->setQuery("INSERT INTO live_chat_quest_site (quest_id, site_id) values ('$insert_id', $site_id)");
	    $db->execQuery();
	}
}

function Saveproblem($id, $quest, $answer){
    global $db;
    $answer_time = $_REQUEST['answer_time'];
    $my_site     = $_REQUEST['my_site'];
    $position    = $_REQUEST['position'];
    
    $db->setQuery("	UPDATE `live_chat_question`
					SET     `quest`    = '$quest',
							`answer`   = '$answer',
                            `position` = $position,
                            `time`     = '$answer_time'
					WHERE	`id`       =  $id");
    $db->execQuery();
    
    $site_array = explode(",",$my_site);
    
    $db->setQuery("DELETE FROM live_chat_quest_site WHERE quest_id = '$id'");
    $db->execQuery();
    
    foreach($site_array AS $site_id){
        $db->setQuery("INSERT INTO live_chat_quest_site (quest_id, site_id) values ('$id', $site_id)");
        $db->execQuery();
    }
}

function Disableproblem($id){
    global $db;
    $db->setQuery("	UPDATE `live_chat_question`
					SET    `actived` = 0
					WHERE  `id` = $id");
    $db->execQuery();
}

function web_site($id){
    global $db;
    $data = '';
    
    $db->setQuery("SELECT `id`, 
                          `name`
				   FROM   `my_web_site`
				   WHERE   actived = 1");
    
    $res = $db->getResultArray();
    foreach ($res[result] AS $value){
        $db->setQuery(" SELECT id
                        FROM  `live_chat_quest_site`
                        WHERE `live_chat_quest_site`.quest_id = '$id' AND site_id = '$value[id]'");
        $check = $db->getNumRow();
        
        if($check>0){
            $data .= '<option value="' . $value['id'] . '" selected="selected">' . $value['name'] . '</option>';
        } else {
            $data .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
        }
        
    }
    
    return $data;
}

function Getproblem($id){
    global $db;
    $db->setQuery("	SELECT  `id`,
                            `quest`,
                            `answer`,
                            `time`,
                            `position`
                    FROM    `live_chat_question`
                    WHERE   `id` = $id" );
    
    $res = $db->getResultArray();
    
    return $res[result][0];
}
function GetPage($res = ''){
    
	$data = '
        	<div id="dialog-form">
        	    <fieldset>
        	    	<table class="dialog-form-table">
                       <tr style="height:0px;">
        					<td><label for="CallType">შეკითხვა</label></td>
        			   </tr>
        	           <tr style="height:0px;">
        					<td>
        						<textarea type="text" style="height:50px; width: 355px; resize:vertical;" id="quest" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'">'.$res['quest'].'</textarea> 
        					</td>
        				</tr>
                        <tr style="height:7px;"></tr>
                        <tr style="height:0px;">
        					<td><label for="CallType">პასუხი</label></td>
        				</tr>
        				<tr style="height:0px;">
        					<td>
        						<textarea type="text" style="height:50px; width: 355px; resize:vertical;" id="answer" class="idle" onblur="this.className=\'idle\'" onfocus="this.className=\'activeField\'">'.$res['answer'].'</textarea> 
        					</td>
        				</tr>
                        <tr style="height:7px;"></tr>
                        <tr style="height:0px;">
        					<td><label for="CallType">საიტი</label></td>
        				</tr>
        				<tr style="height:0px;">
        					<td>
        						<select style="height: 18px; width: 360px;" id="my_site" class="idls object" multiple>'.web_site($res['id']).'</select>
        					</td>
        				</tr>
                        <tr style="height:7px;"></tr>
                        <tr style="height:0px; display:none;">
        					<td><label for="CallType">პასუხის დრო</label></td>
        				</tr>
        				<tr style="height:0px; display:none;">
        					<td>
        						<span><input style="height: 18px; width: 60px;" type="number" min="0" id="answer_time" class="idle" value="'.$res['time'].'"/><a style="margin-top: -17px; margin-left: 70px; float: left;">წამი</a></span>
        					</td>
        				</tr>
                        <tr style="height:7px;"></tr>
                        <tr style="height:0px;">
        					<td><label for="CallType">პოზიცია</label></td>
        				</tr>
        				<tr style="height:0px;">
        					<td>
        						<input style="height: 18px; width: 60px;" type="number" min="0" id="quest_position" class="idle" value="'.$res['position'].'"/>
        					</td>
        				</tr>
                    </table>
        			<!-- ID -->
        			<input type="hidden" id="id" value="' . $res['id'] . '" />
                </fieldset>
            </div>';
	return $data;
}

?>
