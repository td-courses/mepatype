<?php
require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db = new dbClass();
$done         = $_REQUEST['done'];
$start_time   = $_REQUEST['start'].' 00:00';
$end_time     = $_REQUEST['end'].' 23:59';
$procent      = $_REQUEST['procent'];
$number       = $_REQUEST['number'];
$day          = $_REQUEST['day'];
$users        = $_REQUEST['users'];
$source       = $_REQUEST['source'];
if($users == 0){
    $users_query = "";
}else{
    if ($source == 4) {
        $users_query = "AND chat.last_user_id = '$users'";
    }elseif ($source == 7){
        $users_query = "AND mail.user_id = '$users'";
    }elseif ($source == 6){
        $users_query = "AND fb_chat.last_user_id = '$users'";
    }else{
        $users_query = "AND asterisk_call_log.user_id = '$users'";
    }
}
//------------------------------------------------query-------------------------------------------

    if($done == 1){
        if ($source == 4) {
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(chat.join_date) AS `date`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(*) AS `num`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              chat.id
                                    FROM      chat
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                                    AND 	  chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON chat.last_user_id = user_info.user_id
                                    WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            JOIN    user_info ON user_info.user_id = chat.last_user_id
                            WHERE   chat.join_date BETWEEN '$start_time' AND '$end_time' $users_query
                            GROUP BY DATE(chat.join_date)");
            
            $result = $db->getResultArray();
            
            $result1 = $result;
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `date`,
                                    0 AS `percent`,
                                    0 AS `num`");
            $result2 = $db->getResultArray();
            
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(chat.join_date) AS `date`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(*) AS `num`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              chat.id
                                    FROM      chat
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                                    AND 	  chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON chat.last_user_id = user_info.user_id
                                    WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            JOIN    user_info ON user_info.user_id = chat.last_user_id
                            WHERE   chat.join_date BETWEEN '$start_time' AND '$end_time' $users_query");
            $all = $db->getResultArray();
        }elseif ($source == 7){
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    DATE(mail.send_datetime) AS `date`,
                                    ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(*) AS `num`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              mail.id
                                    FROM      mail
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                                    AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON mail.user_id = user_info.user_id
                                    WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  mail.id) AS `chat_dur`
                            JOIN     mail ON mail.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = mail.user_id
                            WHERE    mail.send_datetime BETWEEN '$start_time' AND '$end_time' $users_query
                            GROUP BY DATE(mail.send_datetime)");
            
            $result = $db->getResultArray();
            
            $result1 = $result;
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `date`,
                                    0 AS `percent`,
                                    0 AS `num`");
            $result2 = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    DATE(mail.send_datetime) AS `date`,
                                    ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(*) AS `num`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              mail.id
                                    FROM      mail
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                                    AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON mail.user_id = user_info.user_id
                                    WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_date' AND '$end_date'
                                    GROUP BY  mail.id) AS `chat_dur`
                            JOIN     mail ON mail.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = mail.user_id
                            WHERE    mail.send_datetime BETWEEN '$start_time' AND '$end_time' $users_query");
            $all = $db->getResultArray();
        }elseif ($source == 6){
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(fb_chat.first_datetime) AS `date`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(*) AS `num`
                            FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              fb_chat.id
                                     FROM      fb_chat
                                     LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                     AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                     JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                     WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                                     GROUP BY  fb_chat.id) AS `chat_dur`
                            JOIN     fb_chat ON fb_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                            WHERE    fb_chat.first_datetime BETWEEN '$start_time' AND '$end_time' $users_query
                            GROUP BY DATE(fb_chat.first_datetime)");
            
            $result = $db->getResultArray();
            
            $result1 = $result;
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `date`,
                                    0 AS `percent`,
                                    0 AS `num`");
            $result2 = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(fb_chat.first_datetime) AS `date`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(*) AS `num`
                            FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                               fb_chat.id
                                     FROM      fb_chat
                                     LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                     AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                     JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                     WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                                     GROUP BY  fb_chat.id) AS `chat_dur`
                            JOIN     fb_chat ON fb_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                            WHERE    fb_chat.first_datetime BETWEEN '$start_time' AND '$end_time' $users_query");
            $all = $db->getResultArray();
        }elseif ($source == 14){
            $db->setQuery(" SELECT   'SL-ფაქტიური' AS `status`,
                                        DATE(viber_chat.first_datetime) AS `date`,
                                        ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                        COUNT(*) AS `num`
                                        FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                        viber_chat.id
                            FROM      viber_chat
                            LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                            AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                            JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                            WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                            GROUP BY  viber_chat.id) AS `chat_dur`
                            JOIN     viber_chat ON viber_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                            WHERE    viber_chat.first_datetime BETWEEN '$start_time' AND '$end_time' $users_query
                            GROUP BY DATE(viber_chat.first_datetime)");
            
            $result = $db->getResultArray();
            
            $result1 = $result;
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `date`,
                                    0 AS `percent`,
                                    0 AS `num`");
            $result2 = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(viber_chat.first_datetime) AS `date`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(*) AS `num`
                                     FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                     viber_chat.id
                            FROM      viber_chat
                            LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                            AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                            JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                            WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_date' AND '$end_date'
                            GROUP BY  viber_chat.id) AS `chat_dur`
                            JOIN     viber_chat ON viber_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                            WHERE    viber_chat.first_datetime BETWEEN '$start_time' AND '$end_time' $users_query");
            $all = $db->getResultArray();
        }else{
        	$db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(asterisk_call_log.id) AS `num`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                           JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
        	               WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_time') AND UNIX_TIMESTAMP('$end_time') 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(6,7) $users_query
                           GROUP BY  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
        	
        	$result = $db->getResultArray();
        	
        	$result1 = $result;
        	
        	$db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(asterisk_call_log.id) AS `num`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
        	               WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_time') AND UNIX_TIMESTAMP('$end_time') 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(9)
                           GROUP BY  DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
        	$result2 = $db->getResultArray();
        	
        	
        	$db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `date`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(asterisk_call_log.id) AS `num`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                           JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
        	               WHERE     asterisk_call_log.call_datetime BETWEEN UNIX_TIMESTAMP('$start_time') AND UNIX_TIMESTAMP('$end_time') 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(6,7) $users_query");
        	$all = $db->getResultArray();
        }
        foreach($result[result] AS $row) {
    	    $name_answer[]     = $row['status'];
    	    $percent_answer[] = (float)$row['percent'];
    	    $count_answer[] = (float)$row['num'];
    	    $date[] = $row['date'];
    	    $limit_number[] = (float)$number;
    	    $limit_percent[] = (float)$procent;
    	}
	
    	foreach($result1[result] AS $row) {
    	    $name_unanswer[]     = $row['status'];
    	    $percent_unanswer[] = (float)$row['percent'];
    	    $count_unanswer[] = (float)$row['num'];
    	    $date[] = $row['date'];
    	}
	
    	foreach($result2[result] AS $row){
    	    $unanswer[] = (float)$row['num'];
    	    $date[] = $row['date'];
    	}
    
    
    }elseif($done == 2){
        if ($source == 4) {
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     HOUR(chat.join_date) AS `hourCount`,
                                     CONCAT(HOUR(chat.join_date), ':00') AS `hour`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(*) AS `total`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                            chat.id
                            FROM      chat
                            LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                            AND 	  chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                            JOIN      user_info ON chat.last_user_id = user_info.user_id
                            WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_time' AND '$end_time'
                            GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            JOIN    user_info ON user_info.user_id = chat.last_user_id
                            WHERE   DATE(chat.join_date) = '$day'
                            GROUP BY HOUR(chat.join_date)");
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `hourCount`,
                                    '' AS `hour`,
                                    0 AS `percent`,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }elseif ($source==7){
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    HOUR(mail.send_datetime) AS `hourCount`,
                                    CONCAT(HOUR(mail.send_datetime), ':00') AS `hour`,
                                    ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    COUNT(*) AS `total`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              mail.id
                                    FROM      mail
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                                    AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON mail.user_id = user_info.user_id
                                    WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  mail.id) AS `chat_dur`
                            JOIN     mail ON mail.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = mail.user_id
                            WHERE    DATE(mail.send_datetime) = '$day'
                            GROUP BY HOUR(mail.send_datetime)");
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `hourCount`,
                                    '' AS `hour`,
                                    0 AS `percent`,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }elseif ($source==6){
            $db->setQuery(" SELECT   'SL-ფაქტიური' AS `status`,
                                      HOUR(fb_chat.first_datetime) AS `hourCount`,
                                      CONCAT(HOUR(fb_chat.first_datetime), ':00') AS `hour`,
                                      ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                      COUNT(*) AS `total`
                            FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                               fb_chat.id
                                     FROM      fb_chat
                                     LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                     AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                     JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                     WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                                     GROUP BY  fb_chat.id) AS `chat_dur`
                            JOIN     fb_chat ON fb_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                            WHERE    DATE(fb_chat.first_datetime) = '$day'
                            GROUP BY HOUR(fb_chat.first_datetime)");
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `hourCount`,
                                    '' AS `hour`,
                                    0 AS `percent`,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }elseif ($source==14){
            $db->setQuery(" SELECT   'SL-ფაქტიური' AS `status`,
                                      HOUR(viber_chat.first_datetime) AS `hourCount`,
                                      CONCAT(HOUR(viber_chat.first_datetime), ':00') AS `hour`,
                                      ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                      COUNT(*) AS `total`
                            FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                               viber_chat.id
                                    FROM      viber_chat
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                                    AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                    WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  viber_chat.id) AS `chat_dur`
                            JOIN     viber_chat ON viber_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                            WHERE    DATE(viber_chat.first_datetime) = '$day'
                            GROUP BY HOUR(viber_chat.first_datetime)");
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                    '' AS `hourCount`,
                                    '' AS `hour`,
                                    0 AS `percent`,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }else{
            $db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `hourCount`,
                                     CONCAT(HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)), ':00') AS `hour`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(asterisk_call_log.id) AS `total`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                           JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
        	               WHERE     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = '$day' 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(6,7) $users_query
                           GROUP BY  HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
        	$result = $db->getResultArray();
        	$db->setQuery("SELECT   'SL-ფაქტიური' AS `status`,
                                     HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)) AS `hourCount`,
                                     CONCAT(HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)), ':00') AS `hour`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     COUNT(asterisk_call_log.id) AS `total`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
        	               WHERE     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = '$day' 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(9)
                           GROUP BY  HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime))");
        	$result1 = $db->getResultArray();
        }
    	foreach($result[result] AS $res) {
    	    $myarray[$res[hourCount]] = $res[percent];
    	    $myarray1[$res[hourCount]] = $res[total];
    	}
    	
    	foreach($result1[result] AS $res1) {
    	    $myarray2[$res1[hourCount]] = $res1[percent];
    	    $myarray12[$res1[hourCount]] = $res1[total];
    	}
    	
    	for($i = 0; $i <= 23; $i++) {
    	    if(array_key_exists($i,$myarray)){
    	        if(strlen($i) == 1){
    	           $date[] = '0'.$i.':00';
    	        }else{
    	           $date[] = $i.':00';
    	        }
    	        $percent_answer[] = (float)$myarray[$i];
    	        $count_answer[] = (float)$myarray1[$i];
    	    }else{
    	        if(strlen($i) == 1){
    	           $date[] = '0'.$i.':00';
    	        }else{
    	           $date[] = $i.':00';
    	        }
    	        $percent_answer[] = 0;
    	        $count_answer[] = 0;
    	    }
    	    
    	    if(array_key_exists($i,$myarray2)){
    	        $unhour[] = (float)$myarray12[$i];
    	    }else{
    	        $unhour[] = 0;
    	    }
    	    
    	    $name_answer[]     = 'SL-ფაქტიური';
    	    $limit_number[] = (float)$number;
    	    $limit_percent[] = (float)$procent;
    	    
    	}
	
    }
    
    if($done == 3){
        if ($source == 4) {
            $db->setQuery("SELECT   CONCAT(HOUR(chat.join_date), IF(MINUTE(chat.join_date) >= 30, '.5', '')) *2 AS `hour`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     FLOOR(UNIX_TIMESTAMP(chat.join_date) / (30 * 60)) AS time,
                                     COUNT(*) AS `total`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                            chat.id
                            FROM      chat
                            LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = chat.id AND dashboard_chat_durations.source_id = 2
                            AND 	  chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                            JOIN      user_info ON chat.last_user_id = user_info.user_id
                            WHERE     chat.last_user_id > 0 AND chat.join_date BETWEEN '$start_time' AND '$end_time'
                            GROUP BY  chat.id) AS `chat_dur`
                            JOIN    chat ON chat.id = chat_dur.id
                            JOIN    user_info ON user_info.user_id = chat.last_user_id
                            WHERE   DATE(chat.join_date) = '$day' $users_query
                            GROUP BY time");
            
            $result = $db->getResultArray();
            $db->setQuery("SELECT    '' AS `hour`,
                                     0 AS `percent`,
                                    '' AS time,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }elseif ($source==7){
            $db->setQuery("SELECT   CONCAT(HOUR(mail.send_datetime), IF(MINUTE(mail.send_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                    ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                    FLOOR(UNIX_TIMESTAMP(mail.send_datetime) / (30 * 60)) AS time,
                                    COUNT(*) AS `total`
                            FROM   (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              mail.id
                                    FROM      mail
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = mail.id AND dashboard_chat_durations.source_id = 6
                                    AND       mail.user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON mail.user_id = user_info.user_id
                                    WHERE     mail.user_id > 0 AND mail.send_datetime BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  mail.id) AS `chat_dur`
                            JOIN     mail ON mail.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = mail.user_id
                            WHERE    DATE(mail.send_datetime) = '$day' $users_query
                            GROUP BY time");
            
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT    '' AS `hour`,
                                     0 AS `percent`,
                                    '' AS time,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }elseif ($source==6){
            $db->setQuery("SELECT    CONCAT(HOUR(fb_chat.first_datetime), IF(MINUTE(fb_chat.first_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     FLOOR(UNIX_TIMESTAMP(fb_chat.first_datetime) / (30 * 60)) AS time,
                                     COUNT(*) AS `total`
                            FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                              fb_chat.id
                                     FROM      fb_chat
                                     LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = fb_chat.id AND dashboard_chat_durations.source_id = 4
                                     AND       fb_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                     JOIN      user_info ON fb_chat.last_user_id = user_info.user_id
                                     WHERE     fb_chat.last_user_id > 0 AND fb_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                                     GROUP BY  fb_chat.id) AS `chat_dur`
                            JOIN     fb_chat ON fb_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = fb_chat.last_user_id
                            WHERE    DATE(fb_chat.first_datetime) = '$day' $users_query
                            GROUP BY time");
            
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT    '' AS `hour`,
                                     0 AS `percent`,
                                    '' AS time,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }elseif ($source==14){
            $db->setQuery("SELECT    CONCAT(HOUR(viber_chat.first_datetime), IF(MINUTE(viber_chat.first_datetime) >= 30, '.5', '')) *2 AS `hour`,
                                     ROUND((SUM(IF(chat_dur.avg_duration<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     FLOOR(UNIX_TIMESTAMP(viber_chat.first_datetime) / (30 * 60)) AS time,
                                     COUNT(*) AS `total`
                            FROM    (SELECT    SUM(dashboard_chat_durations.duration)/COUNT(*) AS `avg_duration`,
                                               viber_chat.id
                                    FROM      viber_chat
                                    LEFT JOIN dashboard_chat_durations ON dashboard_chat_durations.chat_id = viber_chat.id AND dashboard_chat_durations.source_id = 5
                                    AND       viber_chat.last_user_id != 1 AND dashboard_chat_durations.duration > 0
                                    JOIN      user_info ON viber_chat.last_user_id = user_info.user_id
                                    WHERE     viber_chat.last_user_id > 0 AND viber_chat.first_datetime BETWEEN '$start_time' AND '$end_time'
                                    GROUP BY  viber_chat.id) AS `chat_dur`
                            JOIN     viber_chat ON viber_chat.id = chat_dur.id
                            JOIN     user_info ON user_info.user_id = viber_chat.last_user_id
                            WHERE    DATE(viber_chat.first_datetime) = '$day' $users_query
                            GROUP BY time");
            
            $result = $db->getResultArray();
            
            $db->setQuery("SELECT    '' AS `hour`,
                                     0 AS `percent`,
                                    '' AS time,
                                    0 AS `total`");
            $result1 = $db->getResultArray();
        }else{
            $db->setQuery("SELECT    CONCAT(HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)), IF(MINUTE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 30, '.5', '')) *2 AS `hour`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     FLOOR(asterisk_call_log.call_datetime / (30 * 60)) AS time,
                                     COUNT(asterisk_call_log.id) AS `total`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
                           JOIN      asterisk_extension ON asterisk_call_log.extension_id = asterisk_extension.id
        	               WHERE     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = '$day' 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(6,7) $users_query
                           GROUP BY  time");
            $result = $db->getResultArray();
            $db->setQuery("SELECT    CONCAT(HOUR(FROM_UNIXTIME(asterisk_call_log.call_datetime)), IF(MINUTE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) >= 30, '.5', '')) *2 AS `hour`,
                                     ROUND((SUM(IF(asterisk_call_log.wait_time<$number, 1, 0)) / COUNT(*) ) * 100, 2) AS `percent`,
                                     FLOOR(asterisk_call_log.call_datetime / (30 * 60)) AS time,
                                     COUNT(asterisk_call_log.id) AS `total`
                           FROM      asterisk_call_log
                           JOIN      asterisk_queue ON asterisk_queue.id = asterisk_call_log.queue_id
        	               WHERE     DATE(FROM_UNIXTIME(asterisk_call_log.call_datetime)) = '$day' 
                           AND       asterisk_call_log.call_type_id = 1
                           AND       asterisk_call_log.call_status_id IN(9) $users_query
                           GROUP BY  time");
            $result1 = $db->getResultArray();
        }
        foreach($result[result] AS $res) {
            $my_array[$res[hour]] = $res[percent];
            $myarray1[$res[hour]] = $res[total];
        }
        
        foreach($result1[result] AS $res1) {
            $my_array2[$res1[hour]] = $res1[percent];
            $myarray12[$res1[hour]] = $res1[total];
        }
        
        for($i = 0; $i <= 47; $i++) {
            if(array_key_exists($i,$my_array)){
                if(strlen($i/2) == 1){
                    if($i % 2 == 0) {
                        $date[] = (($i-1)/2+0.5).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                    }
                }
                $percent_unanswer[] = (float)$my_array[$i];
                $count_answer[] = (float)$myarray1[$i];
            }else{
                if(strlen(round(($i/2))) == 1){
                    if($i % 2 == 0) { 
                        $date[] = '0'.(($i-1)/2+0.5).':00';
                    }else{
                        $date[] = '0'.(($i-1)/2).':30';
                    }
                }else{
                    if($i % 2 == 0) {
                        $date[] = (($i)/2).':00';
                    }else{
                        $date[] = (($i-1)/2).':30';
                        
                    }
                }
                $percent_unanswer[] = 0;
                $count_answer[] = 0;
            }
            
            if(array_key_exists($i,$my_array2)){
                $unmin[] = (float)$myarray12[$i];
            }else{
                $unmin[] = 0;
            }
             
            $name_unanswer[]     = 'SL-ფაქტიური';
            $limit_number[] = (float)$number;
            $limit_percent[] = (float)$procent;
             
        }
    }
    if($done == 4){
//         mysql_query("   SELECT  persons.`name`,
//                                 ext
//                         FROM    `users`
//                         JOIN    persons ON users.person_id = persons.id
//                         WHERE NOT ISNULL(ext)
//                         ORDER BY ext ASC");
        
    }
 	

    $unit     = " %";
    
    $serie1[] = array('count_unanswer' => $count_unanswer, 'count_answer' => $count_answer, 'name_answer' => $name_answer[0], 'name_unanswer' => $name_unanswer[0], 'percent_answer' => $percent_answer, 'percent_unanswer' => $percent_unanswer, 'unit' => $unit, 'date' => $date, 'limit_number' => $limit_number, 'limit_percent' => $limit_percent,'unanswer' => $unanswer, 'unhour' => $unhour, 'unmin' => $unmin, 'all_answer' => $all[num], 'all_procent' => $all[percent] );
    
    
    echo json_encode($serie1);

?>