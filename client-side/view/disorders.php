<html>

<head>
    <style type="text/css">
        .high {
            height: 0px;
        }

        .dialog-grid {
            display: grid;
            grid-auto-flow: row;
            grid-gap: 20px;
        }

        .dialog-grid textarea,
        .dialog-grid input {
            border-radius: 5px;
            border-color: #518bc5 !important;
        }

        .text {
            font-size: 15px;
            color: #03a2ef;
            font-weight: bold;
        }

        .tab_grid {
            display: grid;
            grid-auto-flow: column;
            margin: 30px;

        }

        .tab_grid div {
            padding: 10px;
            border-bottom: 4px solid #AAA;
            cursor: pointer;
        }

        .active {
            border-bottom: 4px solid #00b306a8 !important;
        }

        .dialog-grid {
            display: grid;
            grid-auto-flow: row;
            grid-gap: 20px;
        }

        .dialog-grid textarea,
        .dialog-grid input,
        .dialog-grid select {
            border-radius: 5px;
            border-color: #518bc5 !important;
            width: 100%;
            resize: vertical;
        }

        .text {
            font-size: 15px;
            color: #03a2ef;
            font-weight: bold;
        }

        .grid1 {
            display: grid;
            grid-auto-flow: column;
        }

        .grid2 {
            display: grid;
            grid-auto-flow: column;
            grid-gap: 15px;
            grid-auto-columns: 50% 30% 20%;
            width: 95%;
        }

        .grid3 {
            display: grid;
            grid-auto-flow: row;
            grid-gap: 10px;
        }

        .grid4 {
            display: grid;
            grid-auto-flow: column;
            grid-gap: 15px;
            grid-auto-columns: 23% 25% 51%;
            width: 97%;
        }

        .grid5 {
            display: grid;
            grid-auto-flow: row;
            grid-gap: 5px;
        }

        #add-edit-form input[disabled] {
            background-color: #46b2e638;
            color: #000;
            text-align: center;
        }

        .tab {
            font-size: 17px;
            font-family: BPG arial !important;
        }

        #appeal_button {
            margin-right: 472px;
            width: 120px;
            background: orange;
            color: #FFF;
        }

        #appeal_button[disabled] {
            background: #999;
        }

        .tab span {
            margin: 20px;
            background: #00b306a8;
            padding: 1px 6px;
            border-radius: 50%;
            color: #FFF;
        }

        [i_id='2'] span {
            background: red;
        }

        .ColVis,
        .dataTable_buttons {
            display: none !important;
        }


        .r_row {
            border-color: #3242a8 !important;
            color: #3242a8 !important;
            margin: -5px;
            padding: 25px !important;
        }

        .monitor-grid {
            display: grid;
            grid-auto-flow: column;
            width: max-content;
            grid-gap: 20px;
            position: absolute;
            margin-top: 500px;
            text-align: center;
        }

        .big_card {
            padding: 15px;
            background: white;
            border-radius: 10px;
        }

        .big_card * {
            font-family: sans-serif;
            margin: 0;
            padding: 0;
        }

        .big_card .big_card__left {
            grid-column: 1/2;
            display: flex;
            flex-direction: column;
        }

        .big_card .big_card__left__title {
            color: #808080;
            font-family: pvn;
            font-weight: bold;
            font-size: 15px;
        }

        .big_card .big_card__left__amount {
            font-size: 46px;
            margin-top: 20px;
        }

        .big_card .big_card__right {
            grid-column: 2/3;
            text-align: right;
        }

        .big_card .big_card__right .big_card__total_container {
            margin: 12px;
        }

        .big_card .big_card__right .big_card__total_container__text {
            font-size: 15px !important;
        }

        .big_card .big_card__right .big_card__total_container__amount {
            margin-top: 4px;
            font-size: 42px;
        }

        .big_card .big_card__right .values_container ul {
            list-style: none;
        }

        .big_card .big_card__right .values_container ul li .value_name * {
            display: inline-block;
        }

        .big_card .big_card__right .values_container ul li .value_name .circle {
            width: 10px;
            height: 10px;
            background: red;
            border-radius: 100px;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script type="text/javascript">
        var aJaxURL = "server-side/view/disorders.action.php"; //server side folder url
        var tName = "example"; //table name
        var fName = "add-edit-form"; //form name
        var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

        $(document).ready(function() {
            $("#all_tab_div").css('display', 'none');
            get_tabs();
            //LoadTable();
            GetButtons("add_button", "");
            SetEvents("add_button", "", "", tName, fName, aJaxURL);



            $.ajax({
                url: aJaxURL,
                data: {
                    act: "perm_add"
                },
                success: (data) => {
                    if (data.delete == 1) {
                        $("#add_button").remove();
                    }
                }
            })
        });

        function LoadTable(hidden) {
            var aJaxURL = "server-side/view/disorders.action.php";
            var gridName = 'example';
            var actions = '';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 40;
            var columnsCount = 9;
            var columnsSQL = [
                "id:string",
                "name:date",
                "text:string",
                "text1:string",
                "text2:string",
                "text3:string",
                "text4:string",
                "text5:string",
                "file:string"
            ];
            var columnGeoNames = [
                "ID",
                "ფორმირების თ-ღი",
                "გასაჩივრების დედლაინი",
                "სახელი და გვარი",
                "თანამდებობა",
                "დსცპ დარღვევა",
                "საჯარიმო ქულა",
                "მიმდ.ბალანსი",
                "ჩანაწერი"
            ];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0, 0];

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END

            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, "&tab_id=" + $(".active").attr("i_id"), 0);
        }

        function LoadTable1(hidden) {
            var aJaxURL = "server-side/view/disorders.action.php";
            var gridName = 'example_operators';
            var actions = '';
            var editType = "popup"; // Two types "popup" and "inline"
            var itemPerPage = 40;
            var columnsCount = 8;
            var columnsSQL = [
                "id:string",
                "name:string",
                "text:string",
                "text1:string",
                "text2:string",
                "text3:string",
                "text4:string",
                "file:string"
            ];
            var columnGeoNames = [
                "ID",
                "ოპერატორი",
                "შემთხვევა სულ",
                "გასაჩივრდა",
                "დაკმაყოფილდა",
                "სულ დარღვევა",
                "Kდცპლ",
                "ჩანაწერი"
            ];

            var showOperatorsByColumns = [0, 0, 0, 0, 0, 0, 0, 0];
            var selectors = [0, 0, 0, 0, 0, 0, 0, 0];

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END

            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL, 'get_list_operators', itemPerPage, columnsCount, columnsSQL, gridName, actions, editType, columnGeoNames, filtersCustomOperators, showOperatorsByColumns, selectors, hidden, 0);
        }

        let get_tabs = () => {
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_tabs"
                },
                success: (data) => {
                    $(".tab_grid").html(data.tabs);
                    if ($("[i_id='2'] span").html() === "0") {
                        $("[i_id='2'] span").css("background", "#00b306a8");
                    } else {
                        $("[i_id='2'] span").css("background", "red");
                    }
                    LoadTable();
                }
            })
        }

        function LoadDialog(dialog = fName) {
            switch (dialog) {
                case "add-edit-form":
                    var id = $("#department_id").val();

                    var buttons = {
                        "appeal": {
                            text: "საჩივარი",
                            id: "appeal_button"
                        },
                        "save": {
                            text: "შენახვა",
                            id: "save-dialog"

                        },
                        "cancel": {
                            text: "გაუქმება",
                            id: "cancel-dialog",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }



                    };

                    GetDialog(fName, 792, "auto", buttons);
                    $("[data-select='chosen']").chosen({
                        search_contains: true
                    });
                    $("#add-edit-form,.add-edit-form-class").css("overflow", "visible");
                    $("#add-edit-form input ").not(".chosen-search input").prop("disabled", true);
                    var bool = ($("#hidden_id").val() === "" ? true : false);
                    $("#appeal_button").prop("disabled", bool);

                    break;
                case "appeal-dialog-form":
                    var buttons1 = {

                        "save": {
                            text: "შენახვა",
                            id: "save-dialog-appeal"

                        },
                        "cancel": {
                            text: "გაუქმება",
                            id: "cancel-dialog-appeal",
                            click: function() {
                                $(this).dialog("close");
                            }
                        }



                    };

                    GetDialog(dialog, 770, "auto", buttons1);
                    $("[data-select='chosen']").chosen({
                        search_contains: true
                    });
                    break;

            }
        }


        $(document).on("click", "#save-dialog-appeal", function() {
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "save_appeal",
                    id: $("#appeal_hidden_id").val(),
                    disorder_id: $("#hidden_id").val(),
                    date: $("#monitoring_appealing_date").val(),
                    operator: $("#monitoring_appealing_operator").attr("user_id"),
                    operator_agree: $("#operator_appealing_yes").val(),
                    operator_comment: $("#operator_appealing_comment").val(),
                    maneger: $("#monitoring_appealing_manager").attr("user_id"),
                    maneger_agree: $("#manager_appealing_yes").val(),
                    maneger_comment: $("#manager_appealing_comment").val(),
                    head_manager: $("#monitoring_appealing_call_center_manager").attr("user_id"),
                    head_manager_agree: $("#call_center_manager_appealing_yes").val(),
                    head_manager_comment: $("#call_center_manager_appealing_comment").val()
                },
                success: function(data) {
                    $("#appeal-dialog-form").dialog('close');

                }
            })
        })

        $(document).on("click", "#show_copy_prit_exel", function() {
            if ($(this).attr('myvar') == 0) {
                $('.ColVis,.dataTable_buttons').css('display', 'block');
                $(this).css('background', '#2681DC');
                $(this).children('img').attr('src', 'media/images/icons/select_w.png');
                $(this).attr('myvar', '1');
            } else {
                $('.ColVis,.dataTable_buttons').css('display', 'none');
                $(this).css('background', '#E6F2F8');
                $(this).children('img').attr('src', 'media/images/icons/select.png');
                $(this).attr('myvar', '0');
            }
        });
        // Add - Save
        $(document).on("click", "#save-dialog", function() {
            param = new Object();

            param.act = "save_disorder";
            param.id = $("#hidden_id").val();
            param.disorder = $("#disorder").val();
            param.penalty = $("#penalty").val();
            param.appeal = $("#appeal").val();
            param.comment = $("#comment").val();
            param.group = $("#group").val();
            param.operator_id = $("#operator_id").val();
            param.status = $("#status").val();


            if ($("#status").val() == "0") {
                alert("შეავსეთ სტატუსი!");
                $("#status_chosen").css({
                    "border": "2px solid red",
                    "border-radius": "5px"
                });
            } else if ($("#operator_id").val() == "0") {
                alert("შეავსეთ სახელი და გვარი!");
                $("#operator_id_chosen").css({
                    "border": "2px solid red",
                    "border-radius": "5px"
                });
            } else if ($("#disorder").val() == "0") {
                alert("შეავსეთ დარღვევა!");
                $("#disorder_chosen").css({
                    "border": "2px solid red",
                    "border-radius": "5px"
                });
            } else {
                $.ajax({
                    url: aJaxURL,
                    data: param,
                    success: function(data) {
                        if (typeof(data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {
                                //LoadTable();
                                CloseDialog(fName);
                                get_tabs();
                            }
                        }
                    }
                });
            }
        });

        $(document).on("dblclick", "#example tr.k-state-selected", function() {
            var grid = $("#example").data("kendoGrid");
            var dItem = grid.dataItem($(this));

            if (dItem.id == '') {
                return false;
            }

            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_edit_page",
                    id: dItem.id
                },
                success: function(data) {
                    $("#add-edit-form").html(data.page);
                    LoadDialog(fName);
                }
            })

        });

        $(document).on("change", "#status", function() {
            let val = Number($(this).val());
            if (val) {
                $("#status_chosen").css({
                    "border": "none"
                })
            }
        })
        $(document).on("change", "#operator_id", function() {
            let val = Number($(this).val());
            if (val) {
                $("#operator_id_chosen").css({
                    "border": "none"
                })
            }
        })
        $(document).on("change", "#disorder", function() {
            let val = Number($(this).val());
            if (val) {
                $("#disorder_chosen").css({
                    "border": "none"
                })
            }
        })

        $(document).on("click", ".tab", function() {
            $(".tab").removeClass("active");
            $(this).addClass("active");

            if ($(this).attr('i_id') == 5) {
                $("#dinamiuri").css('display', 'none');
                $("#all_tab_div").css('display', '');
                getRenderAPEX();
                LoadTable1();
            } else {
                $("#dinamiuri").css('display', '');
                $("#all_tab_div").css('display', 'none');
                LoadTable();
            }
        })

        $(document).on("change", "#disorder", function() {
            var penalty = $("#disorder option:selected").attr("penalty");
            var appeal = $("#disorder option:selected").attr("appeal");
            $("#penalty").val(penalty);
            $("#appeal").val(appeal);

        })

        $(document).on("change", "#operator_id", function() {
            var group_name = $("#operator_id option:selected").attr("group_name");
            var appeal = $("#operator_id option:selected").attr("appeal");
            var point = $("#operator_id option:selected").attr("point");
            var user_id = $(this).val();

            $("#group").val(group_name);
            $("#appeal").val(appeal);
            $("#balance").val(point);

            if (point == '') {
                $.ajax({
                    url: aJaxURL,
                    data: {
                        act: "check_points",
                        usr_id: user_id
                    },
                    success: function(data) {
                        var user_point = data.result.point;
                        $(this).attr("point", user_point);
                        $("#balance").val(user_point);
                    }
                })
            }
        })

        $(document).on("click", "#appeal_button", function() {
            $.ajax({
                url: aJaxURL,
                data: {
                    act: "get_appeal_page",
                    id: $("#hidden_id").val(),
                    user_id_n: $("#hidden_id_user").val()
                },
                success: function(data) {
                    $("#appeal-dialog-form").html(data.page);
                    LoadDialog("appeal-dialog-form");
                }
            })
        })

        $(document).on("click", "#play_audio", function () {
            var str = 1;
            var link = ($(this).attr("src"));
            link = 'http://172.16.0.80:8000/' + link;
            var btn = {
                "cancel": {
                    text: "დახურვა",
                    id: "cancel-dialog",
                    class: "cancel-audio",
                    click: function () {
                        $(this).dialog("close");
                        $(this).html("");
                    }
                }
            };
            GetDialog_audio("audio_dialog2", "auto", "auto",btn);
            console.log(link)
            $("#audio_dialog2").html('<audio controls autoplay style="width:500px;"><source src="'+link+'" type="audio/wav"> Your browser does not support the audio element.</audio>');
        })


        function getRenderAPEX(tab = 'main') {
            //Main Dash
            param = new Object();
            param.act = "get_charts";

            $.ajax({
                url: aJaxURL,
                data: param,
                success: function(data) {
                    for (var chartID in data[tab]) {

                        var chartData = data[tab][chartID];
                        var typeData = chartData.typeData;
                        var seriesData = chartData.seriesData;
                        var categoriesData = chartData.categoriesData;

                        var options = {
                            series: seriesData,
                            chart: {
                                width: "70%",
                                type: typeData,
                            },
                            labels: categoriesData,
                            responsive: [{
                                breakpoint: "70%",
                                options: {
                                    chart: {
                                        width: "70%"
                                    },
                                    legend: {
                                        position: 'bottom'
                                    }
                                }
                            }]
                        };

                        $("#" + chartID).html('');
                        var chart = new ApexCharts(document.querySelector("#" + chartID), options);
                        chart.render();
                    }
                }
            });
        }
    </script>
</head>

<body>
    <div id="audio_dialog2" title="ჩანაწერი"></div>
    <div id="tabs" style="width: 80%; display: inline-block; position: absolute;">
        <div class="tab_grid">

        </div>
        <div id="dinamiuri" style="width: 100%;">
            <div id="button_area">
                <button id="add_button">დამატება</button>
                <!-- <button id="delete_button">წაშლა</button> -->
            </div>
            <div id="example"></div>
        </div>
        <div id="all_tab_div" style="width: 100%;">
            <table style="width: 100%;">
                <tr style="width: 100%;">
                    <td style="width: 50%;">
                        <div id="chart" style="width: 100%; border-radius: 5px; "></div>
                    </td>
                    <td style="width: 50%">
                        <div id="chart1" style="width: 100%; border-radius: 5px; "></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 100%;">
                        <div id="example_operators"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="განყოფილებები">
        <!-- aJax -->
    </div>

    <!-- jQuery Dialog -->
    <div id="appeal-dialog-form" class="form-dialog" title="საჩივარი">
        <!-- aJax -->
    </div>
</body>

</html>