<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
date_default_timezone_set('Etc/UTC');
header('Content-Type: text/html; charset=utf-8');

require_once('../../includes/classes/class.Mysqli.php');

require '../../phpmailer2/Exception.php';
    require '../../phpmailer2/PHPMailer.php';
    require '../../phpmailer2/SMTP.php';
$user_id	             = $_SESSION['USERID'];
global $db;
$db = new dbClass();
//error_reporting(E_ALL);
$source_id  	 	= $_REQUEST['source_id'];
$incomming_call_id	= $_REQUEST['incomming_call_id'];
$hidden_increment	= $_REQUEST['hidden_increment'];

$address 			= $_REQUEST['address'];
$cc_address 		= $_REQUEST['cc_address'];
$bcc_address 		= $_REQUEST['bcc_address'];
$subject 	 		= $_REQUEST['subject'];
$body 	 			= $_REQUEST['body'];
if($body == ''){
    $body = ' ';
}
$attachmet 	 		= $_REQUEST['attachments'];
$address = explode(",", $_REQUEST['address']);

$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = false;
$mail->SMTPAuth = false; 
$mail->CharSet = 'UTF-8';
$mail->SMTPSecure = false;
$mail->SMTPAutoTLS = false;
$mail->Debugoutput = 'html';
$mail->Host = 'mail.mepa.gov.ge';
$mail->Port = 5003;

$mail->setFrom('info@rda.gov.ge');
//preg_match("/<a ?.*>(.*)<\/a>/", $esc_message, $matches);
//$attachmet = $matches[1];
//Set the SMTP port number - likely to be 25, 465 or 587
// $mail->AuthType = 'PLAIN';

if ($cc_address != '') {
    $mail->AddCC($cc_address);
}
if ($bcc_address != '') {
    $mail->AddBCC($bcc_address);
}


$all_address = '';
for($i = 0; $i < count($address); $i++) {
    $add = $address[$i];
    
    $mail->addAddress($add);
    
    if ($all_address == '') {
        $all_address.=$add;
    }else{
        $all_address.=','.$add;
    }
    
}

$mail->Subject = $subject;
//$mail->addAttachment('http://localhost/TDG/mepacallapp/media/uploads/documents/'.$attachmet);
$mail->msgHTML($body);

//send the message, check for errors
if (!$mail->send()) {
    $status = '0';
    $data 	 = array("status" => $status);
}else{
    $status	= '2';
    $data 	= array("status" => $status);
    $user	= $_SESSION['USERID'];
    $c_date	= date('Y-m-d H:i:s');
    /* $db->setQuery("INSERT INTO  mail_live
                                (`user_id`,`send_datetime`,`sender_name`,`sender_address`,`to`,`cc`,`bcc`,`subject`,`mail_type_id`)
                        VALUES
                                ('$user_id',NOW(),'INFO RDA','info@rda.gov.ge','$all_address','$cc_address','$bcc_address','$subject','1')");
    $db->execQuery();
    $db->setQuery("
        insert mail_live_detail
        SET
            `datetime`      = NOW(),
            `mail_id` = '". $_REQUEST['chat_id']."',
            `user_id`       = '". $_REQUEST['user_id']."',
            `to`       = '". $all_address."',
            `cc`       = '". $cc_address."',
            `bcc`       = '". $bcc_address."',
            `mail_type_id`       = '1',
            `body`          = '".$body."'
                    ");
        $db->execQuery(); */
    $db->setQuery("INSERT INTO `sent_mail`
                              (`incomming_call_id`,`user_id`, `date`, `address`, `cc_address`, `bcc_address`, `subject`, `body`, `attachment`, `type`, `status`, `actived`)
                        VALUES
                              ('$incomming_call_id','$user', now(), '$all_address', '$cc_address', '$bcc_address', '$subject', '$body', '$attachmet', '$type', '2', '1')");

    $db->execQuery();
}

/* if ($status	== 'true') {
    if ($source_id == 1) {
        $type=1;
    }elseif ($source_id == 2){
        $type=2;
    }else {
        $type = 0;
    }
    $db->setQuery("INSERT INTO `sent_mail`
                              (`id`,`user_id`, `date`, `address`, `cc_address`, `bcc_address`, `subject`, `body`, `attachment`, `type`, `status`, `actived`)
                        VALUES
                              ($hidden_increment,'$user', now(), '$all_address', '$cc_address', '$bcc_address', '$subject', '$body', '$attachmet', '$type', '1', '1')");

    $db->execQuery();
} */

echo json_encode($data);