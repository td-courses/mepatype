<html>
<head>
<style type="text/css">
.high {
    height: 0px;
}
.dialog-grid{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
}

.dialog-grid textarea, .dialog-grid input {
    border-radius: 5px;
    border-color:#518bc5!important;
}

.text {
    font-size: 15px;
    color: #03a2ef;
    font-weight: bold;
}

.tab_grid{
    display: grid;
    grid-auto-flow: column;
    width: 50%;
    margin: 30px;
    
}

.tab_grid div{
    padding:10px;
    border-bottom: 4px solid #AAA;
    cursor:pointer;
}
.active {
    border-bottom: 4px solid #00b306a8!important;
}

.dialog-grid{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 20px;
}

.dialog-grid textarea, .dialog-grid input, .dialog-grid select {
    border-radius: 5px;
    border-color:#518bc5!important;
    width:100%;
    resize:vertical;
}

.text {
    font-size: 15px;
    color: #03a2ef;
    font-weight: bold;
}

.grid1{
    display: grid;
    grid-auto-flow: column;
}

.grid2{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 50% 30% 20%;
    width:95%;
}
.grid3 {
    display: grid;
    grid-auto-flow: column;
    grid-gap: 10px;
}
.grid4{
    display: grid;
    grid-auto-flow: column;
    grid-gap: 15px;
    grid-auto-columns: 50%;
}
.grid4_in{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 15px;
}
.grid4_in_right{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 15px;
}
.grid5{
    display: grid;
    grid-auto-flow: row;
    grid-gap: 5px;
}
#add-edit-form input[disabled]{
    background-color: #46b2e638;
    color: #000;
    text-align:center;
}
.tab{
    font-size: 17px;
    font-family: BPG arial!important;
}
#appeal_button{
    margin-right: 435px;
    width: 120px;
    background: orange;
    color: #FFF;
}
#appeal_button[disabled]{
    background:#999;
}
.fa-paperclip{
    color:#008000;
    font-size:20px;
}
.chosen-disabled{
    opacity:1!important;
}
.ch{
    font-size:14px;
}
.ch:hover{
    font-size:18px;
}

.choose_file{
    background: #46b2e638;
    font-size: 20px;
    text-align: center;
    height: 25px;
    border-radius: 5px;
    width: 90%;
    margin-left: 5%;
    margin-top: 28px;
    color: #be00e0;
    font-weight: bold;
    cursor:pointer;
}
.status_gr, .status_re {
    color:#FFF;
}
#more{
    border-color: #00b3aa!important;
}

#less{
    border-color: #a31205!important;
}

.file {
    background: #FFF;
    font-size: 14px;
    border: 1px solid #46b2e691;
    height: 25px;
    border-radius: 5px;
    padding: 8px 0px 1px 10px;
    width: 90%;
    margin-left: 5%;
    margin-bottom: 5px;
}

.file svg{
    float: right;
    margin-right: 17px;
    color: #cc0101;
    font-size: 15px;
    cursor:pointer;
}
.filter_grid{
    display: grid;
    grid-auto-flow: column;
    width: 400px;
    margin: 15px;
}

.file_list {
    overflow: auto;
    height: 140px;
}


.file_list::-webkit-scrollbar-track
		{
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #F5F5F5;
		}

        .file_list::-webkit-scrollbar
		{
			height: 0px;
			background-color: #F5F5F5;
            width:5px;
		}

		.file_list::-webkit-scrollbar-thumb
		{
			background-color: #85b1de;	
			background-image: -webkit-linear-gradient(45deg,
													rgba(255, 255, 255, .2) 25%,
													transparent 25%,
													transparent 50%,
													rgba(255, 255, 255, .2) 50%,
													rgba(255, 255, 255, .2) 75%,
													transparent 75%,
													transparent)
		}

        .ColVis, .dataTable_buttons{
            display:none!important;
        }


</style>
	<script type="text/javascript">
    	var aJaxURL	= "server-side/view/attendance.action.php";		//server side folder url
        var upJaxURL	= "server-side/upload/file.action.php";
    	var tName	= "example";													//table name
    	var fName	= "add-edit-form";												//form name
    	var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";
        var aJaxURL_dis	= "server-side/view/disorders.action.php";
    	    	
    	$(document).ready(function () {        	
    		LoadTable();	
    		GetButtons("add_button", "delete_button");			
    		SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);
            $("[data-select='chosen']").chosen({ search_contains: true });
            $("#filter").button();
            setInterval(() => {
                $('[title="-838:59:59"]').html("NaN")
                $('[title="838:59:59"]').html("NaN")
            }, 1000);
    	});
        $(document).on("dblclick","#example > div > table[data-role='selectable'] > tbody > tr",function(){
            var buttons = {
                    "cancel": {
                        text: "დახურვა",
                        id: "cancel-dialog",
                        click: function() {
                            var buttons1 = {
                                "done": {
                                    text: "კი",
                                    id: "save-yes",
                                    click: function() {
                                        $("#add-edit-form").dialog("close");
                                        $(this).dialog("close");
                                    }
                                },
                                "no": {
                                    text: "არა",
                                    id: "save-no",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                }
                            };
                            GetDialog("hint_close", 300, "auto", buttons1);
                            $("#save-yes").css('float', 'right');
                            $("#save-no").css('float', 'right');
                        }
                    }
                };
            param = new Object();
            param.act = "get_edit_page";
            param.id = "5027";
            $.ajax({
                url: aJaxURL,
                data: param,
                success: function (data) {
                    $("#add-edit-form").html(data.page);
                    GetDialog("add-edit-form",1270,"auto",buttons)
                }
            });
        });
    	function LoadTable(hidden){
    		var aJaxURL	= "server-side/view/attendance.action.php";
            var gridName        = 	'example';
            var actions         = 	'';
            var editType        =   "popup"; // Two types "popup" and "inline"
            var itemPerPage     = 	40;
            var columnsCount    =	12;
            var columnsSQL      = 	[
                                        "id:string",
                                        "date:string",
                                        "text:string",
                                        "text1:string",
                                        "text2:string",
                                        "text3:string",
                                        "text4:string",
                                        "text5:string",
                                        "text6:string",
                                        "text7:string",
                                        "text8:string",
                                        "text9:string"
                                    ];
            var columnGeoNames  = 	[
                                        "ID", 
                                        "თარიღი",
                                        "თანამშრომელი",
                                        "თანამდებობა",
                                        "ფაქტ. ხანგრძ.",
                                        "გეგმ. ხანგრძ.",
                                        "ნაკლებობა",
                                        "მეტობა",
                                        "ტიპი",
                                        "სტატუსი",
                                        "კომენტარი",
                                        "<i class='fas fa-paperclip'></i>"
                                    ];

            var showOperatorsByColumns  =   [0,0,0,0,0,0,0,0,0,0,0,0]; 
            var selectors               =   [0,0,0,0,0,0,0,0,0,0,0,0]; 

            var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
            //KendoUI CLASS CONFIGS END
                
            const kendo = new kendoUI();
            kendo.loadKendoUI(aJaxURL,"get_list",itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,"&year="+$("#year").val()+"&month="+$("#month").val(), 0);
		}

        $(document).on("click","#filter",function(){
            LoadTable();
        })
    	
    	function LoadDialog(dialog = fName){
            switch (dialog){
                case "add-edit-form" :
                        var id		= $("#department_id").val();

                        var buttons = {
                            "appeal":{
                                        text:"საჩივრის ნახვა",
                                        id:"appeal_button"
                                    },
                                    "save": {
                                        text: "შენახვა",
                                        id: "save-dialog"
                                    
                                    },
                                    "cancel": {
                                        text: "გაუქმება",
                                        id: "cancel-dialog",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                    
                                    

                                };

                        GetDialog(fName, 770, "auto", buttons);
                        $("[data-select='chosen']").chosen({ search_contains: true });
                        $("#add-edit-form,.add-edit-form-class").css("overflow","visible");
                        $("#add-edit-form input ").not(".chosen-search input").prop("disabled",true);
                        var bool = ($("#hidden_id").val() === ""? true : false);
                        $("#appeal_button").prop("disabled", bool);
                    break;
            case "appeal-dialog-form" :
                    var buttons1 = {
                            
                                    "save": {
                                        text: "შენახვა",
                                        id: "save-dialog-appeal"
                                    
                                    },
                                    "cancel": {
                                        text: "გაუქმება",
                                        id: "cancel-dialog-appeal",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                    
                                    

                                };

                        GetDialog(dialog, 770, "auto", buttons1);
                        $("[data-select='chosen']").chosen({ search_contains: true });
                break;
        }
    	}
        $(document).on("click", "#appeal_button", function (){
            $.ajax({
                url: aJaxURL_dis,
                data:{
                    act:"get_appeal_page",
                    id:$("#hidden_id").val()
                },
                success:function(data){
                    $("#appeal-dialog-form").html(data.page);
                    LoadDialog("appeal-dialog-form");
                }
            })
        })

        $(document).on("click", "#save-dialog-appeal", function(){
            let appeal = (Number($("#operator_appealing_yes").val()) == 2);
            $.ajax({
                url:aJaxURL_dis,
                data:{
                    act:"save_appeal",
                    id:$("#appeal_hidden_id").val(),
                    disorder_id:$("#hidden_id").val(),
                    date:$("#monitoring_appealing_date").val(),
                    operator:$("#monitoring_appealing_operator").attr("user_id"),
                    operator_agree:$("#operator_appealing_yes").val(),
                    operator_comment:$("#operator_appealing_comment").val(),
                    maneger:$("#monitoring_appealing_manager").attr("user_id"),
                    maneger_agree:$("#manager_appealing_yes").val(),
                    maneger_comment:$("#manager_appealing_comment").val(),
                    head_manager:$("#monitoring_appealing_call_center_manager").attr("user_id"),
                    head_manager_agree:$("#call_center_manager_appealing_yes").val(),
                    head_manager_comment:$("#call_center_manager_appealing_comment").val(),
                    appeal:appeal
                },
                success:function(data){
                    $("#appeal-dialog-form").dialog('close');
                    LoadTable(tName);
                    get_tabs();
                }
            })
        })
    	
	    // Add - Save
	    $(document).on("click", "#save-dialog", function () {
		    param 			= new Object();

		    param.act		="save_disorder";
	    	param.id		=   $("#hidden_id").val();
	    	param.disorder		= $("#disorder").val();
            param.penalty		= $("#penalty").val();
            param.appeal		= $("#appeal").val();
            param.comment		= $("#comment").val();
            param.group		    = $("#group").val();
            param.operator_id   = $("#operator_id").val();
            param.status        = $("#status").val();
            param.disorder_status = $("#disorder_status").val();

	    	
			if($("#disorder_status").val() == "0"){
				alert("შეავსეთ სტატუსი!");
                $("#disorder_status_chosen").css({"border":"1px solid red", "border-radius":"5px"});
            }
			else if($("#status").val() == "0"){
				alert("შეავსეთ ტიპი!");
                $("#status_chosen").css({"border":"2px solid red", "border-radius":"5px"});
			}
            else if($("#operator_id").val() == "0"){
				alert("შეავსეთ სახელი და გვარი!");
                $("#operator_id_chosen").css({"border":"2px solid red", "border-radius":"5px"});
			}else {
			    $.ajax({
			        url: aJaxURL,
				    data: param,
			        success: function(data) {			        
						if(typeof(data.error) != 'undefined'){
							if(data.error != ''){
								alert(data.error);
							}else{
								LoadTable();
				        		CloseDialog(fName);
							}
						}
				    }
			    });
			}
		});

        setInterval(() => {
            $(".status_gr").parent().css("background","#00b3aa");
            $(".status_re").parent().css("background","#a31205");
        }, 100);

        $(document).on("change", "#disorder_status", function(){
            let val = Number($(this).val());
            if(val){
                $("#disorder_status_chosen").css({"border":"none"})
            }
        })
        $(document).on("change", "#status", function(){
            let val = Number($(this).val());
            if(val){
                $("#status_chosen").css({"border":"none"})
            }
        })
        $(document).on("change", "#operator_id", function(){
            let val = Number($(this).val());
            if(val){
                $("#operator_id_chosen").css({"border":"none"})
            }
        })

        $(document).on("click",".tab", function(){
            $(".tab").removeClass("active");
            $(this).addClass("active");
            LoadTable();
        })

        $(document).on("change", "#disorder", function(){
            var penalty = $("#disorder option:selected").attr("penalty");
            var appeal = $("#disorder option:selected").attr("appeal");
            $("#penalty").val(penalty);
            $("#appeal").val(appeal);
            
        })

        $(document).on("change", "#operator_id", function(){
            var group_name = $("#operator_id option:selected").attr("group_name");
            var appeal = $("#operator_id option:selected").attr("appeal");
            $("#group").val(group_name);
            $("#appeal").val(appeal);
            
        })


        $(document).on("click",".choose_file",function(){
            $("#choose_file").prop("disabled",false);
            $("#choose_file").click();
        })

        $(document).on("change", "#choose_file", function () {
	    	var file		= $(this).val();
	    	var files 		= this.files[0];
		    var name		= uniqid();
		    var path		= "../../media/uploads/file/";

		    var ext = file.split('.').pop().toLowerCase();
	        if($.inArray(ext, ['pdf','png','xls','xlsx','jpg', 'docx']) == -1) { //echeck file type
	        	alert('This is not an allowed file type.');
                this.value = '';
	        }else{
	        	file_name = files.name;
	        	rand_file = name + "." + ext;
	        	$.ajaxFileUpload({
	    			url: upJaxURL,
	    			secureuri: false,
	    			fileElementId: "choose_file",
	    			dataType: 'json',
	    			data:{
						act: "upload_file",
						path: path,
						file_name: name,
						type: ext
					},
	    			success: function (data, status){
	    				if(typeof(data.error) != 'undefined'){
    						if(data.error != ''){
    							// alert(data.error);
								alert("It is not problem");
    						}
    					}

	    				$.ajax({
					        url: aJaxURL,
						    data: {
								act: "up_now",
								rand_file: rand_file,
					    		file_name: file_name,
								log_id: $("#hidden_id").val(),

							},
					        success: function(data) {
						        $(".file_list").html(data.page);
                                LoadTable();
						    }
					    });
    				},
    				error: function (data, status, e)
    				{
    					// alert(e);
						alert("It was E");
    				}
    			});
	        }
		});

        $(document).on("click",".file svg", function(){
            let id = $(this).attr("file_id");
            $.ajax({
                url:aJaxURL,
                data:{
                    act:"delete_file",
                    id:id,
                    log_id: $("#hidden_id").val()
                },
                success: function(data) {
                    $(".file_list").html(data.page);
                    LoadTable();
                }
            })
        })
	   
    </script>
</head>

<body>
    <div id="tabs" style="display: inline-block; position: absolute; width: 80%;">
    	<div class="callapp_head">დასწრება<hr class="callapp_head_hr"></div>
        <div class="filter_grid" style="margin: 0 0px 15px 0; width: 440px;">
            <div>
                <label style="margin-top: 5px;">თვე</label>
                <select id="month" data-select="chosen" style="width: 140px;">
                         <?php
                            $html = "";
                            global $db;
                            $db = new dbClass();
                            $db->setQuery("SELECT * FROM `month`");
                            $res = $db->getResultArray();
                            $db->setQuery("SELECT MONTH(NOW()) AS `mo`");
                            $now = $db->getResultArray();
                            
                            foreach($res[result] AS $arr){
                                if ($arr['id'] == $now[result][0]['mo'])
                                    $html .= "<option value='".$arr['id']."' selected >".$arr['name']."</option>";
                                else
                                    $html .= "<option value='".$arr['id']."'>".$arr['name']."</option>";
                            }
                            echo $html;
                         ?>   
                
                </select>
            </div>
            <div>
                <label style="margin-top: 5px;">წელი</label>
                <select id="year" data-select="chosen" style="width: 100px;">
                         <?php
                            global $db;
                            $db = new dbClass();
                        	$structure = "";
                            $db->setQuery("SELECT year(NOW())-1, year(NOW()) AS 'now', year(NOW()) + 1");
                            $year_array = $db->getResultArray();
                            foreach($year_array[result][0] as $name => $year) {
                                $selected = "";
                                if ($name == 'now') $selected = "selected";
                                $structure .= "<option value='".$year."' $selected >".$year."</option>";
                            }
                        
                            echo $structure;
                         ?>
                
                </select>
            </div>
            <div>
                <button id="filter">ფილტრი</button>
            </div>
        </div>
		<div id="example"></div>
    </div>   
    
    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="განყოფილებები">
    	<!-- aJax -->
	</div>
    <div id="appeal-dialog-form" class="form-dialog" title="საჩივარი">
    	<!-- aJax -->
	</div>
</body>
</html>


