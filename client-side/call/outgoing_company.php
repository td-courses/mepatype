<?php
$db->setQuery("SELECT * FROM source WHERE actived='1' ORDER BY position ASC");
$source_charts = $db->getResultArray();
?>
<html>
<head>
<link rel="stylesheet" href="vendors/apex-charts/apexcharts.css">
<script src="vendors/apex-charts/apex.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>

		<script>
			kendo.pdf.defineFont({
				"DejaVu Sans"             : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans.ttf",
				"DejaVu Sans|Bold"        : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Bold.ttf",
				"DejaVu Sans|Bold|Italic" : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
				"DejaVu Sans|Italic"      : "https://kendo.cdn.telerik.com/2016.2.607/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf",
				"WebComponentsIcons"      : "https://kendo.cdn.telerik.com/2017.1.223/styles/fonts/glyphs/WebComponentsIcons.ttf"
			});
		</script>
		
	<script>
	
	var Comunications_URL = 'server-side/call/incomming.action.php';
	var CustomSaveURL       = "server-side/view/automator.action.php";
	function LoadKendoTable__Info_Projects(hidden){

		$.ajax({
		url: 'server-side/call/incomming.action.php',
		data: {
			act: "get_status_items"
		},
		type: "GET",
		success: function(data){

			var options = data.options;

			//KendoUI CLASS CONFIGS BEGIN
			var aJaxURL	        =   "server-side/call/waiters.action.php";
			var gridName        = 	'kendo_project_info_table';
			var actions         = 	'<button id="add_project_info" style="margin-right: 10px"> დამატება</button><select id="project_info_item_status">' + options.map(o => { return '<option value="'+o.id+'">' + o.name + '</option>'}) + '</select><button data-select="info" id="change_project_status">სტატუსის შეცვლა</button><button id="remove_project_info_item">წაშლა</button>';
			var editType        =   "popup"; // Two types "popup" and "inline"
			var itemPerPage     = 	10;
			var columnsCount    =	4;
			var columnsSQL      = 	[
										"id:string",
										"status:date",
										"project:string",
										"info:string"
									];
			var columnGeoNames  = 	[  
										"ID", 
										"სტატუსი",
										"პროექტი",
										"გაცემული ინფორმაცია"
									];

			var showOperatorsByColumns  =   [0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors               =   [0,0,0,0]; //IF NEED NOT USE 0

			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END

				
			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_list_info_project',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);

			}
		})
	}
	function campaign_inputs(hidden){
        //KendoUI CLASS CONFIGS BEGIN
		var aJaxURL	= 				    "server-side/call/outgoing_company.action.php";
        var gridName = 				    'fieldset_inputs';
        var actions = 				    '<button class="outgoing_button button_add_input" id="button_add_input"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_input" id="button_delete_input"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    4;
        var columnsSQL = 				["id:string","input:string","type:string","position:number"];
        var columnGeoNames = 		    ["ID","ველი","ტიპი","პოზიცია"];

        var showOperatorsByColumns =    [0,0,0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0,0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_compaign_question_inputs',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
	function LoadKendo_selectors(hidden){
        //KendoUI CLASS CONFIGS BEGIN
        var gridName = 				    'selectors_type_kendo';
        var actions = 				    '<button class="outgoing_button button_add_selector" id="button_add_selector"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button button_delete_selector" id="button_delete_selector"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>';
        var editType = 		 		    "popup"; // Two types "popup" and "inline"
        var itemPerPage = 	 		    10;
        var columnsCount =			    2;
        var columnsSQL = 				["id:string","input:string"];
        var columnGeoNames = 		    ["ID","პარამეტრი"];

        var showOperatorsByColumns =    [0,0]; //IF NEED USE 1 ELSE USE 0
        var selectors = 			    [0,0]; //IF NEED NOT USE 0


        var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
        //KendoUI CLASS CONFIGS END


        const kendo = new kendoUI();
        kendo.loadKendoUI(aJaxURL,'get_selectors_data',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
    }
	$(document).on('click','.second_edit2',function(){
		var request_id = "&request_id="+$("#settings_hidden_id").val();
		campaign_inputs(request_id);
	});
	$(document).on("click",".button_add_input",function(){
		var cat_id = $("#settings_hidden_id").val();
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=input_editor&req_id="+cat_id,
			dataType: "json",
			success: function (data) {
				$("#input_editor").html(data.page);
				LoadDialog2('input_editor');
				LoadKendo_selectors();
			}
		});
	});
	$(document).on("click","#communication_excel_export",function(){
		const kendo = new kendoUI();
            $("#loading1").show();
            setTimeout(() => {
                kendo.saveExcelManual("grid","კამპანიები",[1]);
                $("#loading1").hide();
            }, 200);
	});
	function geo_to_latin (str) {
    
		var ru = {
			'ა': 'a', 'ბ': 'b', 'გ': 'g', 'დ': 'd', 'ე': 'e', 'ვ': 'v', 'ზ': 'z', 
			'თ': 't', 'ი': 'i', 'კ': 'k', 'ლ': 'l','მ': 'm', 'ნ': 'n', 'ო': 'o',
			'პ': 'p', 'რ': 'r', 'ს': 's', 'ტ': 't', 'უ': 'u', 'ფ': 'f', 'ქ': 'q', 
			'ღ': 'gh','ყ': 'y', 'შ': 'sh', 'ჩ': 'ch', 'ც': 'c', 'ძ': 'dz', 'წ': 'w',
			'ჭ': 'ch', 'ხ': 'x', 'ჯ': 'j', 'ჰ': 'h', 'ჟ': 'zsh', ' ': '_','.':'_','/':'_','*':'_','=':'_','+':'_'
		}, n_str = [];
		for ( var i = 0; i < str.length; ++i ) {
			n_str.push(
					ru[ str[i] ]
				|| ru[ str[i].toLowerCase() ] == undefined && str[i]
				|| ru[ str[i].toLowerCase() ].replace(/^(.)/, function ( match ) { return match.toUpperCase() })
			);
		}
		return n_str.join('');
	}
	$(document).on('change','#input_type', function(){
        var type = $("#input_type").val();
        if(type == 6 || type == 7 || type == 8 || type == 9){
            $("#input_namer").css("display","");
            $("#selectors_type_inputs").css("display","block");
            $(".multilevelpart_1").css('display','none');
            $("#multilevelpart_2").css('display','none');
        }
        else if(type == 10){
            $("#input_namer").css("display","");
            $("#selectors_type_inputs").css("display","none");
            $(".multilevelpart_1").css('display','contents');
            $("#multilevelpart_2").css('display','block');
            
        }
        else if(type == 11 || type == 12){
            $("#input_namer").css("display","none");
        }
        else{
            $("#input_namer").css("display","");
            $("#selectors_type_inputs").css("display","none");
            $(".multilevelpart_1").css('display','none');
            $("#multilevelpart_2").css('display','none');
        }
    });
	$(document).on('click','.button_add_selector', function(){
        var input_id = $("#input_id").val();
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_selector&input_id="+input_id,
            dataType: "json",
            success: function (data) {
                $("#add_selector").html(data.page);
                
                LoadDialog2('add_selector');
            }
        });
    });
	$(document).on('keyup','#input_name',function(){
        $('#input_key').val(geo_to_latin($('#input_name').val()));
    });
	$(document).on("dblclick", "#fieldset_inputs tr.k-state-selected", function () {
        var grid = $("#fieldset_inputs").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=input_editor&input_id="+dItem.id+"&req_id="+$("#settings_hidden_id").val(),
            dataType: "json",
            success: function (data) {
                $("#input_editor").html(data.page);
                LoadDialog2('input_editor');
                var input = "&input="+dItem.id;
                LoadKendo_selectors(input);
                GetButtons("add_button","delete_button");
                $("[data-button='jquery-ui-button']").button();

                $.ajax({
                    url: aJaxURL,
                    data: 'act=get_multilevel_list&input_id='+dItem.id+'&field_id='+$("#fieldset_id").val(),
                    success: function(data) {
                        if(typeof(data.error) != 'undefined'){
                            if(data.error != ''){
                                alert(data.error);
                            }else{
                                $('#tree1').html(data.page);
                                $('#tree1').treed();
                            }
                        }
                    }
                });
            }
        });
    });
	$(document).on("dblclick", "#selectors_type_kendo tr.k-state-selected", function () {
        var grid = $("#selectors_type_kendo").data("kendoGrid");
        var dItem = grid.dataItem(grid.select());
        
        $.ajax({
            url: aJaxURL,
            type: "POST",
            data: "act=add_selector&selector_id="+dItem.id+"&input_id="+$("#input_id").val(),
            dataType: "json",
            success: function (data) {
                $("#add_selector").html(data.page);
                LoadDialog2('add_selector');
            }
        });
    });
	$(document).on('click','#button_delete_input',function(){
       
       var grid = $("#fieldset_inputs").data("kendoGrid");
       var dItem = grid.dataItem(grid.select());
       //alert(dItem.id);
       $.ajax({
           url: aJaxURL,
           type: "POST",
           data: "act=disable_input&type=input&id="+dItem.id,
           dataType: "json",
           success: function (data) {
               $("#fieldset_inputs").data("kendoGrid").dataSource.read();
               $.ajax({
                    url: aJaxURL,
                    data: {
                        'act'		: 'get_demo_view',
                        'fieldset_id': $("#fieldset_id").val()
                    },
                    success: function(data) {
                        $("#demo_view").empty();
                        $("#demo_view").html(data.page);
                        $.ajax({
                            url: aJaxURL,
                            data: {
                                'act'		: 'get_fieldsets',
                                'fieldset_id': $("#fieldset_id").val()
                            },
                            success: function(data) {
                                $(data.chosen_keys).chosen({ search_contains: true });
                            }
                        });
                    }
                });
           }
       });
   });
	function LoadKendoTable__settings_voice(hidden){

		//KendoUI CLASS CONFIGS BEGIN
		var aJaxURL	= 				    "server-side/call/outgoing_company.action.php";
		var gridName = 				    'settings_voice';
		var actions = 				    '<button class="outgoing_button" id="button_add_voice"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button" id="button_trash_voice"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button><button class="outgoing_button" id="button_export">შაბლონის ჩამოტვირთვა</button>';
		var editType = 		 		    "popup"; // Two types "popup" and "inline"
		var itemPerPage = 	 		    10;
		var columnsCount =			    6;
		var columnsSQL = 				["id:string","create_date:date","name:string","voice_type:string","position:string", "value:string"];
		var columnGeoNames = 		    ["ID", "თარიღი","დასახელება","ტიპი","პოზიცია","ფაილი"];

		var showOperatorsByColumns =    [0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
		var selectors = 			    [0,0,0,0,0,0]; //IF NEED NOT USE 0


		var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
		//KendoUI CLASS CONFIGS END


		const kendo = new kendoUI();
		kendo.loadKendoUI(aJaxURL,'get_settings_voices',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);
	}
	$(document).on('blur','#shim',function(){
		$(this).focus();
	});
	$(document).on('click','#campaign_excel_export', function(){
		var campaign_id = $("#request_id").val();
		var win = window.open('includes/campaign_export.php?act=export_campaign_data_excel&campaign_id='+campaign_id, '_blank');
	});
	function LoadKendoTable__settings_upload(hidden){
		//KendoUI CLASS CONFIGS BEGIN
		var aJaxURL	= 				    "server-side/call/outgoing_company.action.php";
		var gridName = 				    'settings_upload';
		var actions = 				    '<button class="outgoing_button" id="button_add_upload"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button" id="button_trash_upload"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button>'+
		
		'<form id="ExcelUploadForm" action="includes/readxlsx.php?act=upload_excel" method="POST" enctype="multipart/form-data" style="float: right">\
				<input type="file" name="excel" id="ExcelFile" style="display: none">\
				<label for="ExcelFile" id="ExcelImport" class="excelImport">EXCEL იმპორტი</label>\
				<button  name="import_button" id="import_button" style="display: none;">იმპორტი</button>\
		</form>';
		var editType = 		 		    "popup"; // Two types "popup" and "inline"
		var itemPerPage = 	 		    10;
		var columnsCount =			    5;
		var columnsSQL = 				["id:string",	"datetime:date",		"comment:string","status:string","count:string"];
		var columnGeoNames = 		    ["ID", 			"თარიღი",				"კომენტარი",	"სტატუსი",		"რაოდენობა"];

		var showOperatorsByColumns =    [0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
		var selectors = 			    [0,0,0,0,0]; //IF NEED NOT USE 0

		var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
		//KendoUI CLASS CONFIGS END

		const kendo = new kendoUI();
		kendo.loadKendoUI(aJaxURL,'get_settings_upload',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden);

		setTimeout(function(){
			excel_upload()
		}, 5000);

	}


		function LoadKendoTable(hidden){
			//KendoUI CLASS CONFIGS BEGIN
			var aJaxURL	= 				    "server-side/call/outgoing_company.action.php";
			var gridName = 				    'grid';
			var actions = 				    '<button class="outgoing_button" id="button_add"><img style="margin-bottom: 3px;" src="media/images/icons/plus.png"> დამატება</button><button class="outgoing_button" id="button_plus"><img style="margin-bottom: 3px;" src="media/images/icons/play.png"> დაწყება</button><button class="outgoing_button" id="button_pause"><img style="margin-bottom: 3px;" src="media/images/icons/pause.png"> პაუზა</button><button class="outgoing_button" id="button_stop"><img style="margin-bottom: 3px;" src="media/images/icons/stop.png"> შეწვეტა</button><button class="outgoing_button" id="button_trash"><img style="margin-bottom: 3px;" src="media/images/icons/trash.png"> წაშლა</button><button class="outgoing_button" id="button_archive"><img style="margin-bottom: 3px;" src="media/images/icons/archive.png"> დაარქივება</button><button id="communication_excel_export">EXCEL რეპორტი</button>';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    11;
			var columnsSQL = 				["id:string","id2:string","create_date:date","name:string","com_source:string","category:string","type:string","contact_count:string","contact_source:string","progress:string","status:string"];
			var columnGeoNames = 		    ["ID","ID","თარიღი","კამპანიის დასახელება","კომუნიკაციის ფორმა","კატეგორია","ტიპი","კონტაქტების რ-ბა","კონტაქტების წყარო","პროგრესი","სტატუსი"];

			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_list2',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden,'','','',1, false, false);
		}
		function LoadKendoTable2(hidden){
			var aJaxURL	= 				    "server-side/call/outgoing_company.action.php";
			var gridName = 				    'grid2';
			var actions = 				    '<button id="campaign_excel_export">EXCEL რეპორტი</button>';
			var editType = 		 		    "popup"; // Two types "popup" and "inline"
			var itemPerPage = 	 		    10;
			var columnsCount =			    8;
			var columnsSQL = 				["id:string","create_date:date","operator:string","phone:string","call_date:date","talk_time:string","shin_status:string","status:string"];
			var columnGeoNames = 		    ["ID","შექმნის თარიღი","ოპერატორი","ტელეფონი","საუბრის ხან-ბა","დარეკვის თარიღი","შინაარსობრივი სტატუსი","სტატუსი",];
			
			var showOperatorsByColumns =    [0,0,0,0,0,0,0,0]; //IF NEED USE 1 ELSE USE 0
			var selectors = 			    [0,0,0,0,0,0,0,0]; //IF NEED NOT USE 0


			var filtersCustomOperators = '{"date":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}, "number":{"start":"-დან","ends":"-მდე","eq":"ზუსტი"}}';
			//KendoUI CLASS CONFIGS END


			const kendo = new kendoUI();
			kendo.loadKendoUI(aJaxURL,'get_list3',itemPerPage,columnsCount,columnsSQL,gridName,actions,editType,columnGeoNames,filtersCustomOperators,showOperatorsByColumns,selectors,hidden,1);
		}
		function get_tabs_count(id){
			$.ajax({
				url: aJaxURL,
				type: "POST",
				data: "act=get_tabs_numbers&req_id="+id,
				dataType: "json",
				success: function (data) {
					var numbers = data.numbers
					if(numbers != ''){
						numbers.forEach(function(num){
							$('#input_'+num.tree_id +" x").text(num.count);
						});
					}
				}
			});
		}
		function getFilters()
		{
			$("#filt_source").html('');
			$("#filt_category").html('');
			$("#filt_status").html('');

			$.ajax({
				url: aJaxURL,
				type: "POST",
				data: "act=get_filters",
				dataType: "json",
				success: function (data) {
					var source = data.source;
					var category = data.category;
					var status = data.status;
					$("#status_sum").html(data.status_sum);
					$("#category_sum").html(data.category_sum);
					$("#source_sum").html(data.source_sum);
					$("#active_campaign").html(data.status_sum);
					source.forEach(function(item){
						$("#filt_source").append('<li class="dialog_filter_checklist_item2" data-value="'
						+item.name+
						'"><label style="padding: 0;" for="source_'+item.id+'">'
						+item.name+
						'</label><div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;"><input class="callapp_filter_body_span_input2" data-get="source_'+item.id+'" id="source_'+item.id+'" data-id="view" data-name="" type="checkbox" value="'+item.id+'"><label for="source_'+item.id+'" class="l_checkbox"></label></div><num class="l_dialog_filter_list_num">'+item.cc+'</num></li>');
					});

					category.forEach(function(item){
						$("#filt_category").append('<li class="dialog_filter_checklist_item2" data-value="'
						+item.name+
						'"><label style="padding: 0;" for="category_'+item.id+'">'
						+item.name+
						'</label><div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;"><input class="callapp_filter_body_span_input2" data-get="category_'+item.id+'" id="category_'+item.id+'" data-id="view" data-name="" type="checkbox" value="'+item.id+'"><label for="category_'+item.id+'" class="l_checkbox"></label></div><num class="l_dialog_filter_list_num">'+item.cc+'</num></li>');
					});

					status.forEach(function(item){
						$("#filt_status").append('<li class="dialog_filter_checklist_item2" data-value="'
						+item.status+
						'"><label style="padding: 0;" for="status_'+item.id+'">'
						+item.status+
						'</label><div class="callapp_checkbox" style="margin-top: -16px;margin-left: -15px;"><input class="callapp_filter_body_span_input2" data-get="status_'+item.id+'" id="status_'+item.id+'" data-id="view" data-name="" type="checkbox" value="'+item.id+'"><label for="status_'+item.id+'" class="l_checkbox"></label></div><num class="l_dialog_filter_list_num">'+item.cc+'</num></li>');
					});
					
				}
			});
		}
	
		$(document).ready(function () {
			getFilters();
			LoadKendoTable();
			$('.callapp_filter_body2').attr('myvar',0);
		});

		var checkedboxes = [];
		$(document).on("click", ".callapp_filter_body_span_input2", function(){
			var checkbox = $(this).attr('id');
			if(checkedboxes.indexOf(checkbox) == -1){
				checkedboxes.push(checkbox);
			}
			else{
				checkedboxes.splice(checkedboxes.indexOf(checkbox), 1)
			}
			var hidden_data = "&filters="+checkedboxes;
			LoadKendoTable(hidden_data);
		});
		function LoadDialog2(fName){
            if(fName == 'add-edit-form'){
                GetDialog(fName, 600, "auto", '', 'center top');
                $('#parent_id,#client_id').chosen({ search_contains: true });
                $('#add-edit-form, .add-edit-form-class').css('overflow','visible');
                // $("#add-edit-form select").chosen({ search_contains: true });
            }
            else if(fName=='input_editor'){
            var buttons =
                        {
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-input",
                                click: function () {
                                    var input_name  = $("#input_name").val();
                                    var input_type  = $("#input_type").val();
                                    var input_pos   = $("#input_position").val();
                                    var nec         = $("#necessary").prop("checked");

                                    var itsOK = 0;
                                    if(input_name == '' && input_type != 11 && input_type != 12){
                                        $("#input_name").css('border','1px solid red');
                                        itsOK++;
                                    }
                                    if(itsOK == 0){
                                        
                                        var param 	            = new Object();
                                        param.act		        = "save_input";
                                        param.input_id          = $("#input_id").val();
                                        param.input_name        = $("#input_name").val();
                                        param.input_type        = $("#input_type").val();
                                        param.input_pos         = $("#input_position").val();
                                        param.key               = $("#input_key").val();
                                        param.nec               = $("#necessary").prop("checked");
                                        param.req_id            = $("#req_id").val();

                                        $.ajax({
                                            url: "server-side/call/outgoing_company.action.php",
                                            type: "POST",
                                            data: param,
                                            dataType: "json",
                                            success: function (data) {
                                                
                                                if(data.response == 'suc'){
                                                    alert('ველი წარმატებით შენახულია!');
                                                    $("#fieldset_inputs").data("kendoGrid").dataSource.read();
                                                    $("#input_editor").dialog("close");
                                                    
                                                }
                                                else if(data.response == 'not_uniq'){
                                                    alert("ველი მსგავსი სახელით უკვე არსებობს!!!")
                                                }
                                                else{
                                                    alert('დაფიქსირდა შეცდომა!');
                                                }
                                            }
                                        });
                                    }
                                    
                                }
                            }

                        };
            GetDialog(fName, '500', "auto",buttons,'center top');

            //$("#input_type,#input_tab").chosen();
        }
        else if(fName=='add_selector'){
            var buttons =
                        {
                            "cancel": {
                                text: "დახურვა",
                                id: "cancel-dialog",
                                click: function () {
                                    $(this).dialog("close");
                                }
                            },
                            "done": {
                                text: "შენახვა",
                                id: "save-selector",
                                click: function () {
                                    var param 	        = new Object();
                                    param.act		    = "save_selector";
                                    param.input_id      = $("#input_id").val();
                                    param.selector_id   = $("#selector_id").val();
                                    param.field_id      = $("#field_id").val();
                                    param.param_name    = $("#param_name").val();
                                    $.ajax({
                                        url: "server-side/call/outgoing_company.action.php",
                                        type: "POST",
                                        data: param,
                                        dataType: "json",
                                        success: function (data) {
                                            
                                            if(data.response == 'suc'){
                                                alert('ველი წარმატებით შენახულია!');
                                                if(data.input_id == '')
                                                {
                                                    $("#selectors_type_kendo").data("kendoGrid").dataSource.read();
                                                }
                                                else{
                                                    var input = "&input="+data.input_id;
                                                    LoadKendo_selectors(input);

                                                    $("#input_id").val(data.input_id);
                                                }
                                                
                                                $("#add_selector").dialog("close");
                                                
                                            }
                                            else{
                                                alert('დაფიქსირდა შეცდომა!');
                                            }
                                        }
                                    }); 
                                }
                            }

                        };
            GetDialog("add_selector", '350', "auto",buttons,'center top');
        }
            
        }
	</script>	

	<script type="text/javascript">
		var aJaxURL	     = "server-side/call/outgoing_company.action.php";		//server side folder url
		var upJaxURL     = "server-side/upload/file.action.php";				//server side folder url
		var tName	     = "example";											//table name
		var fName	     = "add-edit-form";										//form name
		var img_name	 = "0.jpg";
		var colum_number = 7;
	    var main_act     = "get_list";
	    
		var change_colum_main = "<'dataTable_buttons'T><'F'Cfipl>";

			
	$(document).ready(function () {
		//LoadTable();
		/* Add Button ID, Delete Button ID */
		//GetButtons("add_button", "delete_button");
		//SetEvents("add_button", "delete_button", "check-all", tName, fName, aJaxURL);

		$("#restore_button").button();
		GetTabs("tabs");
		
		var options = {
		series: [{
		name: "",
		data: [
			45,
			98,
			100,
			100,
			80,
			92,
			100
			]
		}],
		chart: {
		type: 'area',
		height: 60,
		width: 130,
		zoom: {
			enabled: false
		},
		toolbar: {
			show:false
		}
		},
		dataLabels: {
		enabled: false
		},
		stroke: {
			curve: 'straight',
			width: 1,
			colors:['#131414', '#858585', '#7c797d']
		},
		labels: [
		"13 Nov 2017",
		"14 Nov 2017",
		"15 Nov 2017",
		"16 Nov 2017",
		"17 Nov 2017",
		"18 Nov 2017",
		"19 Nov 2017"
		
		],
		xaxis: {
			type: 'datetime',
			labels: {
				show:false
			}
		},
		yaxis: {
			show:false,
			max: 100,
			min: 0
		},
		legend: {
		horizontalAlign: 'left'
		},
		markers: {
			size: 3,
			colors:['#131414', '#858585', '#7c797d']
		},
		fill: {
			colors: ['#a6a6a6', '#E91E63', '#9C27B0']
		}
		};
		<?php
			foreach($source_charts['result'] AS $chart){
				echo '	var chart = new ApexCharts(document.querySelector("#chart'.$chart['id'].'"), options);
						chart.render();';
			}
		?>
		
		
	// 			$("#current").button();
	// 			$("#archive").button();


	});
		
	function active(id){
		$(id).attr( "aria-selected", "true");
		
		$(id).css("cursor", "grabbing");
	}
	function deactive(id){
		$(id).attr( "aria-selected", "false");
		
		$(id).css("cursor", "pointer");
	}
	function loadtabs(activep="", activech=""){
		
		obj = new Object;
		obj.act="get_tabs";
		$.ajax({
			url: aJaxURL,
			data:obj,
			success: function (data)
			{
				$("#tabs1").html(data.page);
				GetTabs("tab_0");
				if(activep==""){
					$("[id^=t_]").first().click();
					$("[id^=t_3]").click();
				} else{
					$(activep).click();
					if(activech!=""){
						$(activech).click();
					}
				}
				//quantity_ajax();
			}
		})
	}

$(document).on("click", "#tabs1 li", function() {
	if(!$(this).hasClass("child")){ // parent
		deactive("li[id^='t_']");
		$('div[id^="tab_"]').css("display","inline-block");
		$('div[id^="tab_"]').css("position","relative");
		GetTabs("tab_"+$(this).attr("name"));
		active("#t_"+$(this).attr("name") );
		$('div[id^="tab_"]').not(".main_tab").not('.tab_'+$(this).attr("name")).css("display","none");
		active("#tab_"+$(this).attr("name")+" ul li:first-child");
		var obj = new Object;
		obj.parent=$(this).attr("name");
		obj.child=$("#tab_"+$(this).attr("name")+" ul li:first-child").attr("name");
		obj.start_date  = $('#start_date').val();
		obj.end_date    = $('#end_date').val();
		//quantity_ajax();
		$("#activeparent").val(""+obj.parent);
		$("#activechild").val(""+obj.child);
		//LoadTable('index',colum_number,main_act,change_colum_main,obj);
	} else { // child   
		deactive("li[id^='t_']");
		active("#"+$(this).attr("id") );
		active("#t_"+$(this).attr("id_select") );
		var obj = new Object;
		obj.parent  =$(this).attr("id_select");
		obj.child   =$(this).attr("name");
		obj.start_date  = $('#start_date').val();
		obj.end_date    = $('#end_date').val();
		//quantity_ajax();
		$("#activeparent").val(""+obj.parent);
		$("#activechild").val(""+obj.child);

		var req_id = $("#request_id").val();
		var pos = "&req_id="+req_id+"&parent="+obj.parent+"&child="+obj.child;
		LoadKendoTable2(pos);
		
	}
});
		

		// $(document).on('click', '#create_campaign', function(){
		// 		// ACTION
		// 		param = new Object();
		// 		param.act 	= "create_campaign";
		// 		param.numbers = ['995599570279','995574988080','995599758552','995598877807','995595401737','995555246784','995555350203','995591912147'];
				
		// 		$.ajax({
		// 			url: 'server-side/call/outgoing_company.action.php',
		// 			data: param,
		// 			success: function(data){
		// 			}
		// 		})
		// })



					
	function excel_upload() {

		document.getElementById("ExcelUploadForm").onchange = function() {
			$("#import_button").trigger("click");
		};

		let request_id = $('#settings_hidden_id').val();
		var hidden_id = '&id='+$('#settings_hidden_id').val();

		$('#ExcelUploadForm').ajaxForm({
			data: {
				request_id: request_id,
				request_table_id: request_id
			},
			beforeSend: function(){
				console.log('uploading');
			},
			success: function(data) {
				LoadKendoTable__settings_upload(hidden_id);
				console.log('Uploaded ', data);
			},
			error: function() {
				LoadKendoTable__settings_upload(hidden_id);
				console.log('Upload cant be done');
			}
		});
	}



	$(document).on('click', '#button_export', function(){
			
			var param 			= new Object();
			param.act 			= 'create_excel';
			param.request_id 	= $('#settings_hidden_id').val();


			var xhr = new XMLHttpRequest();
			xhr.open("POST", "includes/writexlsx.php", true);
			xhr.responseType = "arraybuffer";
			xhr.onload = function () {
			if (this.status === 200) {
				var filename = "";
				var disposition = xhr.getResponseHeader("Content-Disposition");
				if (disposition && disposition.indexOf("attachment") !== -1) {
				var filenameRegex = /filename[^;=\n]*=((['"']).*?\2|[^;\n]*)/;
				var matches = filenameRegex.exec(disposition);
				if (matches != null && matches[1])
					filename = matches[1].replace(/['""']/g, "");
				}
				var type = xhr.getResponseHeader("Content-Type");

				var blob = new Blob([this.response], {
				type: type
				});
				if (typeof window.navigator.msSaveBlob !== "undefined") {
				window.navigator.msSaveBlob(blob, filename);
				} else {
				// var URL = window.URL || window.webkitURL;
				var downloadUrl;

				if(window.URL){
					downloadUrl = window.URL.createObjectURL(blob);	
				}else{
					downloadUrl = window.webkitURL.createObjectURL(blob);
				}
				// var downloadUrl = URL.createObjectURL(blob);

				if (filename) {
					// use HTML5 a[download] attribute to specify filename
					var a = document.createElement("a");
					// safari doesn't support this yet
					if (typeof a.download === "undefined") {
					window.location = downloadUrl;
					} else {
					a.href = downloadUrl;
					a.download = filename;
					document.body.appendChild(a);
					a.click();
					}
				} else {
					window.location = downloadUrl;
				}

				setTimeout(function () {
					URL.revokeObjectURL(downloadUrl);
				}, 100); // cleanup
				}
			}
			};

			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.send(
			"act=" +
			param.act +
			"&request_id=" +
			param.request_id
			);
		
	})

		$(document).on('click','#button_add_voice', function(){
			let conn_type = $("#campaign_connect_type").val()
			$("#campaign_connect_type").on("change", function(){
				conn_type  = this.value;
			});
			$.ajax({
                    url: aJaxURL,
                    type: "POST",
                    data: "act=get_setting_voice_add&conn_type="+conn_type,
                    dataType: "json",
                    success: function (data) {
						$("#setting_voice_add-dialog").html(data.page);
						
						var buttons = {
							
							done: {
								text: "შენახვა",
								id: "add-voice"
							}
						};

					
						
						GetDialog("setting_voice_add-dialog", "320", "auto", buttons, "ceneter center");
					
						
					}	
			});
		});

		$(document).on('click', '#add-voice', function(){
			var param = new Object();

			param.act 				= 'add_settings_voice';
			param.id 				= $('#settings_hidden_id').val();
			param.name 				= $('#voice_name').val();
			param.add_voice_type 	= $('#add_voice_type').val()
			param.add_voice_pos		= $('#add_voice_pos').val();

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data){
					$("#setting_voice_add-dialog").dialog("close");
					let id =  '&id='+$('#settings_hidden_id').val();
					LoadKendoTable__settings_voice(id)
				}
			})
		})

		$(document).on('click', '#button_trash_voice', function(){

			let act = "disable_voice_item";

			var compaignIDS = [];
			var entityGrid = $("#settings_voice").data("kendoGrid");
			var rows = entityGrid.select();
			rows.each(function(index, row) {
				var selectedItem = entityGrid.dataItem(row);
				compaignIDS.push(selectedItem.id);
			});

			$.ajax({
				url: aJaxURL,
				type: "POST",
				data: "act="+act+"&id=" + compaignIDS,
				dataType: "json",
				success: function (data) {
					let id =  '&id='+$('#settings_hidden_id').val();
					LoadKendoTable__settings_voice(id)
				}
			});
	
		})

		$(document).on('click', '#button_add_upload', function(){
				let id = $('#settings_hidden_id').val();
			$.ajax({
				url: aJaxURL,
				type: "POST",
				data: "act=get_setting_upload_add&id="+id,
				dataType: "json",
				success: function (data) {
					$("#setting_upload_add-dialog").html(data.page);
					
					var buttons = {
						
						done: {
							text: "შენახვა",
							id: "add-upload-manual"
						}
					};
					GetDialog("setting_upload_add-dialog", "400", "auto", buttons, "ceneter center");
				}	
			});

		})


		$(document).on('click', '#add-upload-manual', function(){
			let param = new Object();
			var manualForms = $('#upload_manual_voice_form input');
			let manualFormArr = [];

			manualForms.map((i, f) => {
				return manualFormArr.push({name: f.placeholder, value: f.value})
			})

			param.act 				= 'add_settings_upload';
			param.id 				= $('#settings_hidden_id').val();
			param.name 				= manualFormArr;

			$.ajax({
				url: aJaxURL,
				data: param,
				success: function(data){
					$("#setting_upload_add-dialog").dialog("close");

					let id =  '&id='+$('#settings_hidden_id').val();
					LoadKendoTable__settings_upload(id);
					// var settings_upload = $("#settings_upload").data("kendoGrid");
					// settings_upload.refresh();
				}
			})
		})

		$(document).on('click', '#button_trash_upload', function(){

			let act = "disable_upload_item";

			var compaignIDS = [];
			var entityGrid = $("#settings_upload").data("kendoGrid");
			var rows = entityGrid.select();
			rows.each(function(index, row) {
				var selectedItem = entityGrid.dataItem(row);
				compaignIDS.push(selectedItem.id);
			});

			$.ajax({
				url: aJaxURL,
				type: "POST",
				data: "act="+act+"&id=" + compaignIDS,
				dataType: "json",
				success: function (data) {
					let id =  '&id='+$('#settings_hidden_id').val();
					LoadKendoTable__settings_upload(id);
					// $("#settings_upload").data("kendoGrid").refresh();
				}
			});

		})


		$(document).on('click', '.delete_schedule', function(){

			var act 	= 	"delete_schedule";
			var id 		= 	$(this).attr("data-id");
			var name 	= 	$(this).attr('data-name');

			$.ajax({
				url: aJaxURL,
				data: {
					act: act,
					id: id,
					name: name
				},
				beforeSend: function(){
					$('#select_group > .button_loading').css('display', 'initial');
					$('#select_calendar > .button_loading').css('display', 'initial');
					$('#select_parameters > .button_loading').css('display', 'initial');
				},
				success: function(data){
					$('#select_group > .button_loading').css('display', 'none');
					$('#select_calendar > .button_loading').css('display', 'none');
					$('#select_parameters > .button_loading').css('display', 'none');
				
					if(data.status == 1){
						if(data.act == "calendar"){
							$('#calendar_selector').html(data.data)
						}
						if(data.act == "operators"){
							$('#group_selector').html(data.data)
						}
						if(data.act == "parameter"){
							$('#parameter_selector').html(data.data)
						}
					}
				} 
			})

		})

	$(document).on('click', '#group_selector li, .delete_schedule', function(){

		if($(this).attr('class') == 'delete_schedule'){
			return false;
		}
		if($(this).attr('data-search') == 1){
			return false;
		}
		let param = new Object();
		param.id = $(this).attr('data-id');
		param.req_id = $('#settings_hidden_id').val();
		param.act = 'select_group';
		let name = $(this).text();

		if(param.id == 0){
			$('#group_selector').css({display: 'none', opacity: 0})
			$('#group_selector').attr('data-status', '0')
			return false;
		}

		$.ajax({
			url: aJaxURL,
			data: param,
			beforeSend: function(){
				$('#group_selector').css({display: 'none', opacity: 0})
				$('#group_selector').attr('data-status', '0')
				$('#select_group > .button_loading').css('display', 'initial');
			},
			success: function(data){
				$('#select_group > .button_loading').css('display', 'none');
				$('#second_operators').attr('data-id', param.id)
				$('#outgoing_campaign_setting_group_hidden_id').val(param.id);
				$('#group_schedule_name').val(name);
				$('#second_operators').click();
				// var customers = $("#customers").data("kendoMultiSelect");
				// customers.value(data.users);

			}
		})
	})


	$(document).on('click', '#parameter_selector li, .delete_schedule', function(){

		if($(this).attr('class') == 'delete_schedule'){
			return false;
		}
		if($(this).attr('data-search') == 1){
			return false;
		}

		let param = new Object();
		param.id = $(this).attr('data-id');
		param.req_id = $('#settings_hidden_id').val();
		param.act = 'select_parameter';
		let name = $(this).text();


		$.ajax({
			url: aJaxURL,
			data: param,
			beforeSend: function(){
				$('#parameter_selector').css({display: 'none', opacity: 0})
				$('#parameter_selector').attr('data-status', '0')
				$('#select_parameters > .button_loading').css('display', 'initial');
			},
			success: function(data){
				$('#select_parameters > .button_loading').css('display', 'none');
				$('#outgoing_campaign_setting_parameters_hidden_id').val(param.id);
				$('#parameter_schedule_name').val(name);

				var data = data.result[0];
				
				if(data.first_call == 1){
					$('#parameter_operator').prop("checked", true);
				}else if(data.first_call == 2){
					$('#parameter_client').prop("checked", true);
				}else{
					$('#parameter_client').prop("checked", false);
					$('#parameter_operator').prop("checked", false);
				}

				$('#wait_time_before_answer').val(data.wait_time_before_answer);
				$('#attempt_count').val(data.attempt_count);
				$('#attempt_period_time').val(data.attempt_period_time);
				$('#call_type').val(data.call_type_id);
				$('#listening_status').val(data.listening_status);

				$('#second_settings').attr('data-id', param.id)

			}
		})
	})

	$(document).on('click', '#calendar_selector li, .delete_schedule', function(){

		if($(this).attr('class') == 'delete_schedule'){
			return false;
		}
		if($(this).attr('data-search') == 1){
			return false;
		}

		let param = new Object();
		param.id = $(this).attr('data-id');
		param.req_id = $('#settings_hidden_id').val();
		param.act = 'select_calendar';
		let name = $(this).text();

		$.ajax({
			url: aJaxURL,
			data: param,
			beforeSend: function(){
				$('#calendar_selector').css({display: 'none', opacity: 0})
				$('#calendar_selector').attr('data-status', '0')
				$('#select_calendar > .button_loading').css('display', 'initial');
			},
			success: function(data){
				$('#select_calendar > .button_loading').css('display', 'none');
				$('#outgoing_campaign_setting_calendar_hidden_id').val(param.id);
				$('#second_calendar').attr('data-id', param.id)
				$('#calendar_schedule_name').val(name);

				set_calendar(data.result)

			}
		})
	})

	$(document).on('click', '#save_parameters_button', function(){

		var param = new Object();
		param.act 							= "save_parameters";
		param.first_call 					= $("input[name='first_call']:checked").val();;
		param.wait_time_before_answer 		= $('#wait_time_before_answer').val();
		param.attempt_count 				= $('#attempt_count').val();
		param.attempt_period_time			= $('#attempt_period_time').val();
		param.call_type 					= $('#call_type').val();
		param.listening_status 				= $('#listening_status').val();
		param.missed_call_upload_time		= $('#missed_call_upload_time').val();

		param.min_off 						= $('#min_off').val();
		param.max_off 						= $('#max_off').val();
		param.to_call_time 					= $('#to_call_time').val();
		
		param.parameter_schedule_name		= $('#parameter_schedule_name').val();
		param.parameters_hidden_id			= $('#outgoing_campaign_setting_parameters_hidden_id').val();
		param.settings_hidden_id			= $('#settings_hidden_id').val();

		$.ajax({
			url: aJaxURL,
			data: param,
			beforeSend: function(){
				$('#parameters_button_down').css({display: 'none', opacity: 0})
				$('#parameters_button_down').attr('data-status', '0')
				$('#save_parameters > .button_loading').css('display', 'initial')
			},
			success: function(data){

				if(data.error == 0){
						$('<div class="res_message success-color">' + data.message + '</div>')
						.appendTo('.dialog__second_section_content#second_settings')
						.promise().done(function(){
							$(this).css('opacity', 1);
							$this = $(this);
							setTimeout(function(){
								$this.css('opacity', 0).promise().done(function(){
								$this.remove();
							});
							}, 2000)
							
						})
						
					}
				$('#outgoing_campaign_setting_parameters_hidden_id').val(data.parameter_id);
				$('#save_parameters > .button_loading').css('display', 'none')

				$('#second_settings').click()
			}
		})

	})

	$(document).on('click', '#create_group_button', function(){

		var customers = [];
		$('#customers option:selected').toArray().map(c => customers.push(c.value));

		var param = new Object();
		param.act 							= "create_group";
		param.op_id							= customers;
		param.group_schedule_name			= $('#group_schedule_name').val();
		param.group_hidden_id				= $('#outgoing_campaign_setting_group_hidden_id').val();
		param.settings_hidden_id			= $('#settings_hidden_id').val();

		if(param.group_schedule_name == ''){
			alert('შეავსეთ შაბლონის სახელი');
		}else{
			$.ajax({
				url: aJaxURL,
				data: param,
				beforeSend: function(){

					$('#group_button_down').css({display: 'none', opacity: 0});
					$('#group_button_down').attr('data-status', '0');
					$('#create_group > .button_loading').css('display', 'initial');

				},
				success: function(data){
					if(data.error == 0){
						$('<div class="res_message success-color">' + data.message + '</div>')
						.appendTo('.dialog__second_section_content#second_operators message')
						.promise().done(function(){
							$(this).css('opacity', 1);
							$this = $(this);
							setTimeout(function(){
								$this.css('opacity', 0).promise().done(function(){
								$this.remove();
							});
							}, 2000)
						})
					}
					$('#second_operators').attr('data-id', data.group_id)
					$('#outgoing_campaign_setting_group_hidden_id').val(data.group_id);
					$('#create_group > .button_loading').css('display', 'none');

					$('#second_operators').click();

				}
			})
		}
	})

	$(document).on('click', '#save_group', function(){

		var customers = [];
		$('#customers option:selected').toArray().map(c => customers.push(c.value));

		var param = new Object();
		param.act 							= "save_group";
		param.op_id							= customers;
		param.group_hidden_id				= $('#outgoing_campaign_setting_group_hidden_id').val();
		
		$.ajax({
			url: aJaxURL,
			data: param,
			beforeSend: function(){
				$('#save_group > .button_loading').css('display', 'initial');
			},
			success: function(data){
				$('#outgoing_campaign_setting_group_hidden_id').val(data.group_id);
				$('#save_group > .button_loading').css('display', 'none');
			}
		})
	})





	$(document).on("click", "#choose_button", function () {
		$("#choose_file").click();
	});

	$(document).on("click", "#choose_buttondisabled", function () {
		alert('თუ გსურთ ახალი სურათის ატვირთვა, წაშალეთ მიმდინარე სურათი!');
	});
		
	$(document).on("click", "#callapp_show_filter_button2", function () {
		
		if($('.callapp_filter_body2').attr('myvar') == 0){
			$('.callapp_filter_body2').css('max-height','200px');
			$('.callapp_filter_body2').attr('myvar',1);
			
			$("#filterC").css('transform', 'rotate(180deg)');
			//$("#compaign_statistics").css('top','297px');
		}else{
			$('.callapp_filter_body2').css('max-height','0');
			$('.callapp_filter_body2').attr('myvar',0);
			//$("#compaign_statistics").css('top','129px')
			$("#filterC").css('transform', 'rotate(0)');
			
		}
	});
			
	$(document).on("change", "#choose_file", function () {
		var file_url  = $(this).val();
		var file_name = this.files[0].name;
		var file_size = this.files[0].size;
		var file_type = file_url.split('.').pop().toLowerCase();
		var path	  = "../../media/uploads/file/";

		if($.inArray(file_type, ['png','jpg']) == -1){
			alert("დაშვებულია მხოლოდ 'png', 'jpg'  გაფართოება");
		}else if(file_size > '15728639'){
			alert("ფაილის ზომა 15MB-ზე მეტია");
		}else{
			if($("#pers_id").val() == ''){
				users_id = $("#is_user").val();
			}else{
				users_id = $("#pers_id").val()
			}
			$.ajaxFileUpload({
				url: "server-side/upload/file.action.php",
				secureuri: false,
				fileElementId: "choose_file",
				dataType: 'json',
				data: {
					act: "file_upload",
					button_id: "choose_file",
					table_name: 'users',
					file_name: Math.ceil(Math.random()*99999999999),
					file_name_original: file_name,
					file_type: file_type,
					file_size: file_size,
					path: path,
					table_id: users_id,

				},
				success: function(data) {			        
					if(typeof(data.error) != 'undefined'){
						if(data.error != ''){
							alert(data.error);
						}else{
							$("#upload_img").attr('src','media/uploads/file/'+data.page[0].rand_name);
							$("#upload_img").attr('img',data.page[0].rand_name);
							$('#choose_button').attr('id','choose_buttondisabled');
							$("#delete_image").attr('image_id',data.page[0].id);
							$(".complate").attr('onclick','view_image('+ data.page[0].id + ')');
						}						
					}					
				}
			});
		}
	});

	$(document).on("click", "#delete_image", function () {
		$.ajax({
			url: aJaxURL,
			data: "act=delete_image&file_name="+$("#upload_img").attr('img'),
			success: function(data) {
				$('#upload_img').attr('src','media/uploads/file/0.jpg');
				$('#upload_img').attr('img','0.jpg');               
				$("#choose_button").button();
				$('#choose_buttondisabled').attr('id','choose_button')
			}
		});
	});

	function view_image(id){
		param = new Object();

		//Action
		param.act	= "view_img";
		param.id    = $("#pers_id").val();
		
		$.ajax({
			url: aJaxURL,
			data: param,
			success: function(data) {
				if(typeof(data.error) != "undefined"){
					if(data.error != ""){
						alert(data.error);
					}else{
						var buttons = {
								"cancel": {
									text: "დახურვა",
									id: "cancel-dialog",
									click: function () {
										$(this).dialog("close");
									}
								}
							};
						GetDialog("add-edit-form-img", 401, "auto", buttons, 'center top');
						$("#add-edit-form-img").html(data.page);
					}
				}
			}
		});
	}


	/* Enable Event */
	$(document).on("click", "#restore_button", function () {
			
		var data = $(".check:checked").map(function () {
			return this.value;
		}).get();
		
		for (var i = 0; i < data.length; i++) {
			$.ajax({
				url: aJaxURL,
				type: "POST",
				data: "act=enable&id=" + data[i] ,
				dataType: "json",
				success: function (data) {
						if (data.error != "") {
							alert(data.error);
						} else {
							LoadTable('get_list_archive');
							$(".check:checked").attr("checked", false);
						}
				}
			});
		}

	});


	$(document).on("click","#current",function(){
		$("#restore_button").css("display","none");
		$("#add_button").css("display","inline-block");
		$("#delete_button").css("display","inline-block");
		LoadTable();
	})

	$(document).on("click","#archive", function(){
		$("#add_button").css("display","none");
		$("#delete_button").css("display","none");
		$("#restore_button").css("display","block");
		LoadTable('get_list_archive');
	})

	function change_btn_color(id) {
		document.getElementById("current").setAttribute("aria-selected", "false");
		document.getElementById("archive").setAttribute("aria-selected", "false");
		document.getElementById("draft").setAttribute("aria-selected", "false");
		document.getElementById(id).setAttribute("aria-selected", "true");
		loadtabs(activep = "", activech = "");

	}
	$(document).on("dblclick", "#grid tr.k-state-selected, #grid tr.customHeaderRowStyles", function () {

		var dItem = $(this).find(":first-child").html();
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=get_edit_page&id=" + dItem,
			dataType: "json",
			success: function (data) {
				$("#add-edit-form").html(data.page);
				var buttons = {
					"cancel": {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function () {
							$(this).dialog("close");
						}
					}
				};
				GetDialog("add-edit-form","1280","auto",buttons,'ceneter top');
				// create_dialog_graph('setting_done',62,4,27);
				$("#setting_done span").remove();
				loadtabs(activep = "", activech = "");
				var pos = '&req_id='+dItem;
				//alert(dItem);
				LoadKendoTable2(pos);
				get_tabs_count(dItem);
			}
		});
	});


	$(document).on("dblclick", "#settings_voice tr.k-state-selected", function () {
		var grid = $("#settings_voice").data("kendoGrid");
		var dItem = grid.dataItem($(this));
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=get_setting_voice_add&id=" + dItem.id,
			dataType: "json",
			success: function (data) {
				$("#setting_voice_add-dialog").html(data.page);
				
				var buttons = {
					done: {
						text: "შენახვა",
						class: "edit-voice",
						id: 'save-dialog'
					}
				};
				GetDialog("setting_voice_add-dialog","320","auto",buttons,'ceneter center');
			}
		});
	});

	$(document).on('click', ".edit-voice", function(){

		var param 					= new Object();
		param.act					= "update_voice";
		param.id 					= $('#voice_hidden_id').val();
		param.name 					= $('#voice_name').val();
		param.add_voice_type 		= $('#add_voice_type').val();
		param.add_voice_pos 		= $('#add_voice_pos').val();

		$.ajax({
			url: aJaxURL,
			data: param,
			success: function(data){
				$('#setting_voice_add-dialog').dialog("close")
				var settings_voice = $("#settings_voice").data("kendoGrid");
				settings_voice.refresh();
			}
		})

	})
	$(document).on('click','#save-dialog-out-automated', function(e){
        $.ajax({
            url: Comunications_URL,
            data: {
                'act'		: 'get_inputs_ids',
                'setting_id': 4
            },
            success: function(data) {
                var query = 'act=save-custom&setting_id=4&processing_id='+$("#hidden_incomming_call_id").val();
                query += "&region="+$("#region").val()+"&municip="+$("#municip").val()+"&temi="+$("#temi").val()+"&village="+$("#village").val();
                if(data.input_ids != ''){
                    var input_ids   = data.input_ids;
                    var checkboxes  = data.checkboxes;

                    var ready_to_save = 0;

                    input_ids.forEach(function(item,index){
                        if($("#"+item+"[data-nec='1']").val() == '' || $("#"+item+"[data-nec='1']").val() == '0')
                        {
                            $("#"+item).css('border','1px solid red');
                            var transformed = item.replace(/--/g, "__");
                            $("#"+transformed+"_chosen .chosen-single").css('border','1px solid #e81f1f');
                            ready_to_save++;
                            alert('გთხოვთ შეავსოთ ყველა აუცილებელი ველი (გაწითლებული გრაფები)');
                        }
                        else{
                            if($("#"+item).val() != ''){
                                query += "&"+item+'='+$("#"+item).val();
                            }
                            
                        }
                        
                    });

                    checkboxes.forEach(function(item,index){
                        $("input[name='"+item+"']:checked").each(function() {
                            var post_data = this.id;
                            var check = post_data.split("--");

                            var check_id = check[0].split("-");

                            query += "&"+this.id+"="+check_id[1];
                        });
                    });
                    
                    //default deps//
                    var dep = $("#dep_0").val();
                    var dep_project = $("#dep_project_0").val();
                    var dep_sub_project = $("#dep_sub_project_0").val();
                    var dep_sub_type = $("#dep_sub_type_0").val();

                    query += "&dep_0="+dep;
                    query += "&dep_project_0="+dep_project;
                    query += "&dep_sub_project_0="+dep_sub_project;
                    query += "&dep_sub_type_0="+dep_sub_type;
                    
                    ////////////////

                    $(".dep").each(function(item){
                        var rand_number = $(this).attr('data-id');
                        query += "&dep_"+rand_number+"="+$(this).val();
                    });
                    $(".dep_project").each(function(item){
                        var rand_number = $(this).attr('data-id');
                        query += "&dep_project_"+rand_number+"="+$(this).val();
                    });
                    $(".dep_sub_project").each(function(item){
                        var rand_number = $(this).attr('data-id');
                        query += "&dep_sub_project_"+rand_number+"="+$(this).val();
                    });
                    $(".dep_sub_type").each(function(item){
                        var rand_number = $(this).attr('data-id');
                        query += "&dep_sub_type_"+rand_number+"="+$(this).val();
                    });
                        
                    


                    query += "&dialogtype="+$("#dialogType").val();
                    if(ready_to_save == 0){
                        $.ajax({
                            url: CustomSaveURL,
                            type: "POST",
                            data: query,
                            dataType: "json",
                            success: function (data) {
                                alert('ინფორმაცია შენახულია');
                                $("#kendo_incomming_table").data("kendoGrid").dataSource.read();
                                $.ajax({
                                    url: Comunications_URL,
                                    data: "act=chat_end&chat_source=" +$("#chat_source").val()+ "+&chat_id=" + $("#chat_original_id").val(),
                                    success: function (data) {
                                    if (typeof data.error != "undefined") {
                                        if (data.error != "") {
                                        // alert(data.error);
                                        //alert("Caucasus Jixv");
                                        } else {
                                        $("#chat_close_dialog").dialog("close");
                                        }
                                    }
                                    }
                                });
                            }
                        });
                    }
                    console.log(query);
                }
            }
        });
    });

	$(document).on("dblclick", "#grid2 tr.k-state-selected", function () {
		var grid = $("#grid2").data("kendoGrid");
		var dItem = grid.dataItem($(this));
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=get_processing&id=" + dItem.id,
			dataType: "json",
			success: function (data) {
				$("#get_processing-dialog").html(data.page);
				
				var buttons = {
					
					done: {
						text: "შენახვა",
						class: "save-processing",
						id: 'save-dialog-out-automated'
					},
					cancel: {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function () {
							$(this).dialog("close");
						}
					}
				};

				GetDialog("get_processing-dialog","625","auto",buttons,'ceneter center');

				
				$.ajax({
                    url: Comunications_URL,
                    data: {
                        'act'		: 'get_fieldsets',
                        'setting_id': 4
                    },
                    success: function(data) {
                        $(data.chosen_keys).chosen({ search_contains: true });

                        if(data.datetime_keys != ''){
                            var datetimes = data.datetime_keys;

                            datetimes.forEach(function(item,index){
                                GetDateTimes(item);
                            });
                        }

                        if(data.date_keys != ''){
                            var date = data.date_keys;

                            date.forEach(function(item,index){
                                GetDate(item);
                            });
                        }
                        if(data.multilevel_keys != ''){
                            var date = data.multilevel_keys;
                            var main = '';
                            var secondary = '';
                            var third = '';

                            date.forEach(function(item,index){
                                if(index == 0){
                                    main = item;
                                }
                                else if(index == 1){
                                    secondary = item;
                                }
                                else{
                                    third = item;
                                }
                            });
                        }

                        $(document).on("change", "#"+main, function () {
                            param = new Object();
                            param.act = "cat_2";
                            param.selector_id = $("#"+main).attr('id');
                            param.cat_id = $("#"+main).val();
                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function (data) {
                                    $("#"+secondary).html(data.page);
                                    $("#"+secondary).trigger("chosen:updated");

                                    if ($("#"+secondary+" option:selected").val() == 0) {
                                        param = new Object();
                                        param.act = "cat_3";
                                        param.cat_id = $("#"+secondary).val();
                                        $.ajax({
                                            url: Comunications_URL,
                                            data: param,
                                            success: function (data) {
                                                $("#"+third).html(data.page);
                                                $("#"+third).trigger("chosen:updated");
                                            }
                                        });
                                    }
                                }
                            });
                        });
                        $(document).on("change", "#"+secondary, function () {
                            param = new Object();
                            param.act = "cat_3";
                            param.selector_id = $("#"+main).attr('id');
                            param.cat_id = $("#"+secondary).val();
                            $.ajax({
                                url: Comunications_URL,
                                data: param,
                                success: function (data) {
                                $("#"+third).html(data.page);
                                $("#"+third).trigger("chosen:updated");
                                }
                            });
                        });

                        $(".dep").chosen();
                        $(".dep_project").chosen();
                        $(".dep_sub_project").chosen();
                        $(".dep_sub_type").chosen();
                        ///BLOCKING CONTRACT FIELDSET BEGIN
                        $(".dialog-tab-20 input").each(function(index){  
                            $(this).prop('disabled',true);
                        });
                        $("#servisi___259--146--8").prop('disabled',true).trigger("chosen:updated");

                        ///BLOCKING CONTRACT FIELDSET END

                    }
                });

                $("#dep_0,#dep_project_0,#dep_sub_project_0,#dep_sub_type_0,#region,#municip,#temi,#village").chosen();
				$("#incomming_cat_1, #incomming_cat_1_1, #incomming_cat_1_1_1, #incomming2_status_1, #incomming2_status_1_1, #inc_status_a, #company, #space_type, #chat_language, #s_u_status").chosen({ search_contains: true });
				// $('#hidden_base_id').val(data.id)
				
				// console.log(data.hisdden_incomming_id)
				// $('#hidden_incomming_call_id').val(data.hidden_incomming_id);

				// var dLength = [[10, 30, 50, -1], [10, 30, 50, "ყველა"]];
				// GetDataTable("project_info_table", 'server-side/call/incomming.action.php', "get_list_info_project", 4, "&id="+$('#hidden_incomming_call_id').val(), 0, dLength, 1, "desc", '', "<'F'lip>");
			
				
				var hidden = '&hidden='+$('#hidden_incomming_call_id').val();
                LoadKendoTable__Info_Projects(hidden);

                setTimeout(function(){
                    var grid = $("#kendo_project_info_table").data("kendoGrid");
                    grid.hideColumn(0); 

                    $("#kendo_project_info_table").kendoTooltip({  
                        show: function(e){  
                            if(this.content.text().length > 30){  
                            this.content.parent().css("visibility", "visible");  
                            }  
                        },  
                        hide:function(e){  
                            this.content.parent().css("visibility", "hidden");  
                        },  
                        filter: "td", 
                        position: "right",  
                        content: function(e){  
                            // var dataItem = $("#kendo_project_info_table").data("kendoGrid").dataItem(e.target.closest("tr"));  
                            // console.log("bottom ", dataItem.info)
                            var str = e.target[0].innerHTML;  
                            var content = str.replace('</span>', '</span><br />');
                            console.log(content);
                            return content;  
                        }  
                        }).data("kendoTooltip");  
                }, 3000)

				$(document).on("click", ".client_history", function () {
					$("#client_history_user_id").val($("#phone").val());
					GetDate("date_from");
					GetDate("date_to");
					$("#search_client_history").button();
					GetDataTable("table_history", aJaxURL, "get_list_history", 6, "&start_check="+$('#start_check').val()+"&end_check="+$('#end_check').val()+"&phone="+$("#phone").val(), 0, dLength, 1, "desc", '', "<'F'lip>");
				
					setTimeout(function(){
						$('.ColVis, .dataTable_buttons').css('display','none');
					}, 90);
				});

				GetDataTable("table_question", aJaxURL, "get_list_quest", 5, "source_id=1&site="+$("#my_site").chosen().val(), 0, "", 0, "desc",'',"<'F'lip>");
				$("#table_question_length").css('top','0px');
				

				$(document).on("click", ".newsnews", function () {

					GetDataTable("table_news", aJaxURL, "get_list_news", 6, "", 0, "", 0, "desc",'',"<'F'lip>");

					$("#table_news_length").css('top','0px');

					setTimeout(function () {$('.ColVis, .dataTable_buttons').css('display', 'none');}, 90);

				});
				
				GetDataTable("table_sms",aJaxURL,"get_list_sms",5,"&incomming_id=" + $("#hidden_incomming_call_id").val(),0,"",2,"desc","","<'F'lip>");



				
			}
		});
	});




	$(document).on('click','#button_plus,#button_pause,#button_stop,#button_trash,#button_archive', function(){
		var act;
		if($(this).attr('id') == 'button_plus'){
			act = 'start_compaign';
		}
		else if($(this).attr('id') == 'button_pause'){
			act = 'pause_compaign';
		}
		else if($(this).attr('id') == 'button_stop'){
			act = 'stop_compaign';
		}
		else if($(this).attr('id') == 'button_trash'){
			act = 'disable_compaign';
		}
		else if($(this).attr('id') == 'button_archive'){
			act = 'archive_compaign';
		}
		var compaignIDS = [];
		var entityGrid = $("#grid").data("kendoGrid");
		var rows = entityGrid.select();
		rows.each(function(index, row) {
			var selectedItem = entityGrid.dataItem(row);
			// selectedItem has EntityVersionId and the rest of your model
			compaignIDS.push(selectedItem.id);
		});
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act="+act+"&id=" + compaignIDS,
			dataType: "json",
			success: function (data) {
				LoadKendoTable();
				getFilters()
			}
		});
		
	});
		
	$(document).on('click','.button_plus,.button_pause,.button_stop,.button_trash', function(){
		var act;
		if($(this).attr('id') == 'button_plus'){
			act = 'start_compaign';
		}
		else if($(this).attr('id') == 'button_pause'){
			act = 'pause_compaign';
		}
		else if($(this).attr('id') == 'button_stop'){
			act = 'stop_compaign';
		}
		else if($(this).attr('id') == 'button_trash'){
			act = 'disable_compaign';
		}
		var request_id = $("#request_id").val();
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act="+act+"&id=" + request_id,
			dataType: "json",
			success: function (data) {
				LoadKendoTable();
				getFilters()
			}
		});
		
	});


	$(document).on('click','#button_option', function(){
		$.ajax({
			url: aJaxURL,
			type: "POST",
			data: "act=get_setting_page&req_id="+$("#request_id").val(),
			dataType: "json",
			success: function (data) {
				$("#setting-dialog").html(data.page);
				var default_compaign = $("#default_compaign").val();
				if(default_compaign == 1){
					if($("#default_compaign").val() == 1){
						<?php 
						if($_SESSION['USERGR'] != 1){
							print "$('#campaign_name').attr('disabled', true);";
						}
					?>
					}
					
					
					$("#campaign_start_date").val('');
					$("#campaign_start_date").attr('disabled',true);
					
					$("#campaign_end_date").val('');
					$("#campaign_end_date").attr('disabled',true);

					//$("#second_upload").css('display','none');

					$(".only_default").removeClass('only_default');
					$("#campaign_connect_type option[value='1']").remove();
				}
				var buttons = {
					done: {
						text: "შენახვა",
						id: "save-dialog",
						class: 'save-dialog'
					},
					cancel: {
						text: "დახურვა",
						id: "cancel-dialog",
						click: function () {
							$(this).dialog("close");
						}
					},
					schedule: {
						text: "შაბლონად შენახვა",
						id: "save-schedule"
					},
				};
				GetDialog("setting-dialog","1280","auto", buttons, 'ceneter top');
				get_users()
				$("#campaign_com_channel, #campaign_cat, #campaign_connect_type, #call_type").chosen({ search_contains: true });

				$("#campaign_com_channel, #campaign_cat, #campaign_connect_type, #call_type").chosen({ search_contains: true });

				let conn_type = $("#campaign_connect_type").val();
				if(conn_type == 3){
					$('#section_first_call').css("display","none");
					$('#parameter_operator').prop("checked", false);
					$('#parameter_client').prop("checked", false);
				}else{
					$('#section_first_call').css("display","contents");
				}
				if(conn_type == 2){
					$('#section_listening_status').css('display', 'none');
					$('#listening_status').val('');
					$('#parameter_client').prop("checked", true);
					$('#parameter_operator').attr("disabled", true);

					$("label[for='parameter_operator']").css("color",'#bbbbbb');
				}else{
					$('#section_listening_status').css("display","contents");
					$("label[for='parameter_operator']").css("color",'#333');
					$('#parameter_operator').attr("disabled", false);
				}

				create_dialog_graph('graph',62,10,110);

				GetDateTimes('campaign_start_date');
				GetDateTimes('campaign_end_date');
				
				if($('#settings_hidden_id').val() == 1){
					$('#missed_call_upload_time').closest('tr').css('display', 'contents');
					GetTimes('missed_call_upload_time');
				}
				$('#second_operators').click()
			}
		});
	});

	function create_dialog_graph(ident, percent, line_width,size){
		var el = document.getElementById(ident); // get canvas
		var options = {
			percent:  percent || 25,
			size: size || 110,
			lineWidth: line_width || 10,
			rotate: el.getAttribute('data-rotate') || 0
		}

		var canvas = document.createElement('canvas');
		var span = document.createElement('span');
		span.textContent = options.percent + '%';
			
		if (typeof(G_vmlCanvasManager) !== 'undefined') {
			G_vmlCanvasManager.initElement(canvas);
		}

		var ctx = canvas.getContext('2d');
		canvas.width = canvas.height = options.size;

		el.appendChild(span);
		el.appendChild(canvas);

		ctx.translate(options.size / 2, options.size / 2); // change center
		ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI); // rotate -90 deg

		//imd = ctx.getImageData(0, 0, 240, 240);
		var radius = (options.size - options.lineWidth) / 2;

		var drawCircle = function(color, lineWidth, percent) {
				percent = Math.min(Math.max(0, percent || 1), 1);
				ctx.beginPath();
				ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, false);
				ctx.strokeStyle = color;
				ctx.lineCap = 'round'; // butt, round or square
				ctx.lineWidth = lineWidth
				ctx.stroke();
		};

		drawCircle('#efefef', options.lineWidth, 100 / 100);
		drawCircle('#10A442', options.lineWidth, options.percent / 100);
	}

	/**
	* ---------------------------------------------------------------------------------------------------------------------
	* 	SETTING DIALOG 
	*/
	function get_users(){

		var req_id = $("#outgoing_campaign_setting_group_hidden_id").val();
		
		$.ajax({
			url: aJaxURL,
			data: "act=get_group_schedule&id="+req_id,
			success: function(data){
				$("#customers").kendoMultiSelect({
					dataTextField: "name",
					dataValueField: "id",
					headerTemplate: '<div class="dropdown-header k-widget k-header">' +
							'<span></span>' +
							'<span></span>' +
						'</div>',
					footerTemplate: 'სულ #: instance.dataSource.total() #',
					itemTemplate: '<span class="k-state-default" style="font-size: 13px">#: data.name #</span>',
					tagTemplate:  '<span>#:data.name#</span>',
					dataSource: {
						transport: {
							read: {
								dataType: "json",
								url: aJaxURL+"?act=get_users",
							}
						}
					},
					
					height: 400,
					value: data.users,
					change: function(e) {
								var operators = this.value();

									// operators.map(x => {
									// 	// GAAGRDZELE AQ
									// })


								// var form = '<thead><th>ოპერატორი</th><th>პროცენტული მაჩვენებელი</th></thead>';
								getOperatorTable(operators, '')

							}
				});

				var customers = $("#customers").data("kendoMultiSelect");
				// customers.wrapper.attr("id", "customers-wrapper");
			}
		});
	}

	function set_calendar(data){

		$("#timeTableSection tr").each(function() {

			var arrayOfThisRow = [];
			var tableData = $(this).find('td');


			if (tableData.length > 0 ) {
				tableData.each(function() { 
					if($(this).hasClass('selected')){
						$(this).removeClass('selected');
					}
					if($(this).hasClass('selecting')){
						$(this).removeClass('selecting');
					}
					if($(this).hasClass('unselecting')){
						$(this).removeClass('unselecting');
					}
					data.map(e => {
						if(this.dataset.id == e.quarter && this.dataset.day == e.day){
							$(this).addClass('selected')
						}
					})

				})
			}
		})
	}
		

$(document).on('input', ".schedule_search", function(){
	
	var filter, ul, li, a, i, txtValue, name;

		filter = this.value.toUpperCase();
		name = $(this).attr("data-name");
		ul = document.getElementById(name);
		li = ul.getElementsByTagName("li");

		for (i = 0; i < li.length; i++) {
		a = li[i];
		txtValue = a.textContent || a.innerText;

		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			li[i].style.display = "";
		} else {
			if(li[i].getAttribute('data-search') == 1){
				li[i].style.display = "";
			}else{
				li[i].style.display = "none";
			}
		}
	}

})
	

	function getOperatorTable(i = '', users = ''){

		var campanyID = $("#settings_hidden_id").val();
		var operators = [];

		if(users.length > 0){
			users.map(c => operators.push(c.id));
			// $('#customers option:selected').toArray().map(c => operators.push(c.value));
		}

		if(users == ''){
			i.map(c => operators.push(c));
		}
		
		$.ajax({
			url: "server-side/call/outgoing_company.action.php",
			type: "POST",
			data: "act=get_campaign_insert_percentage&campaign_id="+campanyID+"&users="+operators,
			dataType: "json",
			success: function (data) {

				$("#relocateUserCall").html(data.page);

				let usersLine     = $("#relocateTable .user-line");
				let userCount     = usersLine.length;

					usersLine.map((i, tr) => {
						let percent   =   tr.childNodes[3];
						let p = 100 / userCount;
						percent.innerText = p.toFixed(2)+'%';

					});
				}
		});
	}

	// DIALOG TAB BAR
	$(document).on('click', '.dialog__second_section_item', function(){

		var content = $('.dialog__second_section_content').attr('id');
		var tab = $(this).attr('id');
		var data_id = $(this).attr('data-id');
		var data_name = $(this).attr('data-name');
		var data_status = $(this).attr('data-status');

		$('.dialog__second_section_item').removeAttr('aria-selected');
		$('.dialog__second_section_content').removeAttr('aria-selected');
		$('#'+tab).attr('aria-selected', true)
		$('.dialog__second_section_content#'+tab).attr('aria-selected', true);

			var tab_param = new Object();
			var hidden_id = '&id='+$('#settings_hidden_id').val();

			tab_param.act 	= data_name;
			tab_param.id 	= data_id;

			if(data_status == 1){
			$.ajax({
				url: aJaxURL,
				data: tab_param,
				beforeSend: function(){
					$('.dialog__second_section_content#' + tab + ' .block_settings_content').css('display', 'initial');
					$('.button_down').css({display: 'none', opacity: 0})
					$('.button_down').attr('data-status', '0');
				},
				success: function(data){
					$('.dialog__second_section_content#' + tab + ' .block_settings_content').css('display', 'none');

													
					if(data_name == 'get_settings_voices'){
						LoadKendoTable__settings_voice(hidden_id);
					}

					if(data_name == 'get_settings_upload'){
						LoadKendoTable__settings_upload(hidden_id);
					}

					if(data_name == 'get_group_schedule'){
						var customers = $("#customers").data("kendoMultiSelect");
						customers.value(data.users);
						$('#group_selector').html(data.schedules);
						getOperatorTable('', data.users)
						
					}

					
						if(data_name == 'get_calendar_schedule'){
							
							$('#calendar_selector').html(data.schedules);
							
							if(data.error == 0){

								if($('#timeTableSection tr').length > 0){
									$('#timeTableSection tr').remove();
									
								}

								createTimeTable('timeTableSection');

								set_calendar(data.result)
								
							}else{

								if($('#timeTableSection tr').length > 0){
									$('#timeTableSection tr').remove();
									
								}

								createTimeTable('timeTableSection');
								
							}
						}

					if(data.result){
						if(data_name == 'get_parameters'){

							$('#parameter_selector').html(data.schedules);

							var data = data.result.result[0];
							
							// პირველი ზარი შევიდეს
							// if(data.first_call == 1){
							// 	$('#parameter_operator').prop("checked", true);
							// }else 
							if(data.first_call == 2){
								$('#parameter_client').prop("checked", true);
							}else{
								$('#parameter_client').prop("checked", false);
								$('#parameter_operator').prop("checked", false);
							}
							
							// ლოდინის ხანგრძლივობა პასუხამდე
							$('#wait_time_before_answer').val(data.wait_time_before_answer);
							// მცდელობების რ-ბა
							$('#attempt_count').val(data.attempt_count);
							// მცდელობებს შორის შუალედი
							$('#attempt_period_time').val(data.attempt_period_time);
							// ზარების განხორციელების წესი
							$('#call_type').val(data.call_type_id);
							// სტატუსი - „მოისმინა“
							$('#listening_status').val(data.listening_status);

							$('#min_off').val(data.min_off);
							$('#max_off').val(data.max_off);
							$('#to_call_time').val(data.to_call_time);

							if($('#settings_hidden_id').val() == 1){
								$('#missed_call_upload_time').val(data.missed_upload_time)
							}
						}
					}
				}
			})
		}
	});

	// GENERATE TIME TABLE
	function createTimeTable(TableName){

		var tbl = document.getElementById(TableName);

		
		let date = 1;

		for (let i = 1; i < 8; i++) {
			let row = document.createElement("tr");

			var time = 0;
			var hour = 9;
			let period;

			for (let j = 0; j < 96; j++) {

				if(time == 60) time = 0;
				if(hour == 24) hour = 0;

				time += 15;
				if(time == 60) hour += 1;

				if(hour == 9 && time === 60 ){
					period = 1;
				}else if(hour == 24 && time === 60){
					period = 1;
				}else if(hour > 0 && hour > 8) {
					period = 0
				}else{
					period = 1;
				}

				(hour == 24 ) ? hours = "00" : hours = hour;
				if(hour >= 0 && hour < 10) hours = '0'+hour;
				(time == 60 || time == 0) ? minutes = "00" : minutes = time; 
				
				if (date > 672) {
					break;
				}
				else {
					cell = document.createElement("td");
					cell.setAttribute('data-id', j);
					cell.setAttribute('data-day', i)
					cell.setAttribute('class', 'time-quarter');
					cell.setAttribute('data-time', hours+":"+minutes);
					cell.setAttribute('data-counter', '0');
					cell.setAttribute('data-period', period);
					row.setAttribute('data-time', i)
					row.appendChild(cell);
					date++;
				}
			}

			tbl.appendChild(row);
		}

	}

	$(document).on("change", "#campaign_connect_type", function(){
		if(this.value == 3){
			$('#section_first_call').css("display","none");
			$('#parameter_operator').prop("checked", false);
			$('#parameter_client').prop("checked", false);
		}else{
			$('#section_first_call').css("display","contents");
		}
		if(this.value == 2){
			$('#section_listening_status').css('display', 'none');
			$('#listening_status').val('');
			$('#parameter_client').prop("checked", true);
			$('#parameter_operator').attr("disabled", true);
			$("label[for='parameter_operator']").css("color",'#bbbbbb');
		}else{
			$('#section_listening_status').css("display","contents");
			$("label[for='parameter_operator']").css("color",'#333');
			$('#parameter_operator').attr("disabled", false);
		}

	});

	$(document).on('change', '#attempt_count', function(){
		if(this.value > 5){
			this.value = 5;
			var p = $("#attempt_count").parent()
			if($("#attept_msg").length > 0){
				$('#attept_msg').fadeIn();
			}else{
				p.append("<span style='line-height: 19px;font-size: 12px;font-family: BPG_baner;border-radius: 19px;background-color: #d85e5e;padding: 0 14px;height: 22px;margin-top: 3px;color: #fff;' id='attept_msg'>დაშვებულია მაქსიმუმ 5 მცდელობა</span>");
			}
		
			setTimeout(() => {
				$('#attept_msg').fadeOut();
				$('#attept_msg').remove();
			}, 3000);
		}
		
	})

	$(document).on('change', '#wait_time_before_answer', function(){
		if(this.value > 45){
			this.value = 45;
			var p = $("#wait_time_before_answer").parent()
			if($("#attept_msg").length > 0){
				$('#attept_msg').fadeIn();
			}else{
				p.append("<span style='line-height: 19px;font-size: 12px;font-family: BPG_baner;border-radius: 19px;background-color: #d85e5e;padding: 0 14px;height: 22px;margin-left: 10px; margin-top: 3px;color: #fff;' id='attept_msg'>დაშვებულია მაქსიმუმ 45 წამი</span>");
			}
			setTimeout(() => {
				$('#attept_msg').fadeOut();
				$('#attept_msg').remove();
			}, 3000);
		}
	})

	$(document).on('click', "#save-schedule", function(){
		
	});

	function TimeMarker(TableName){

		var start;
		var start_day;
		var end;
		var counter = 0;

		$(document).on('mousedown', '#'+TableName, function(e){
			e.preventDefault();

			if(!e.target.classList.contains('time-quarter')) return false;

			$(e.target).addClass('startSelect')

			start = e.target.dataset.id;
			start_day = e.target.dataset.day;

		})

		$(document).mouseup(function(e){
			if(!e.target.classList.contains('time-quarter')){

				if($("#"+TableName+" tr").length > 0){
					$("#"+TableName+" tr").each(function() {
						
						var tableData = $(this).find('td');
						
						if (tableData.length > 0 ) {
							tableData.each(function() { 
								if($(this).hasClass('startSelect')){
									$(this).removeClass('startSelect')
								};
							})
						}
					})
				}

			}else{

				end = e.target.dataset.id
				let cacheNumber;
				console.log(start, end)
				if(Number(end) > Number(start)){
					cacheNumber = _.range(start, end)
					cacheNumber.push(Number(end));
				}else if(!Number(end) && Number(end) != 0){
					cacheNumber = [];
				}else {
					cacheNumber = _.range(end, start);
					cacheNumber.push(Number(start))
				}

				console.log(cacheNumber);

				$("#"+TableName+" tr").each(function() {
					
					var tableData = $(this).find('td');
					
					if (tableData.length > 0 ) {
						tableData.each(function() { 
							if(this.dataset.day == start_day ){
								cacheNumber.map(a => {
									if(this.dataset.id == a){
										
										$(this).removeClass('startSelect');

										if($(this).hasClass('selecting')){
											$(this).removeClass('selecting');
										}else{
											$(this).addClass('selecting');
										}

										if($(this).hasClass('selected')){
											$(this).removeClass('selected');
											$(this).addClass('unselecting');
											$(this).removeClass('selecting');
										}else if($(this).hasClass('unselecting')){
											$(this).removeClass('unselecting');
											$(this).addClass('selected');
											$(this).removeClass('selecting');
										}
										
									}
								})
							}
						});
					}
				});

				var selecting = $('.selecting');
				counter += 1;
				selecting.map((i,q) => {
					if(q.dataset.counter == 0){
						return $(q).attr('data-counter', counter)
					}
				})
			
			}

		})
	}
	TimeMarker('timeTableSection')

	$(document).on('click', '#save_calendar_button', function(){
		var selecting = $('.selecting');
		var selected = $('.selected');
		var unselecting = $('.unselecting');
		var quarters = [];
		var removing = [];
		var saved = []; 

		unselecting.map((i,q) => {
			return removing.push({
				day: q.dataset.day, 
				id: q.dataset.id, 
				time: q.dataset.time, 
				counter: q.dataset.counter,
				period: q.dataset.period
			});
		})

		selecting.map((i,q) => {
			return quarters.push({
				day: q.dataset.day, 
				id: q.dataset.id, 
				time: q.dataset.time, 
				counter: q.dataset.counter,
				period: q.dataset.period
			});
		})

		selected.map((i,q) => {
			return saved.push({
				day: q.dataset.day, 
				id: q.dataset.id, 
				time: q.dataset.time, 
				counter: q.dataset.counter,
				period: q.dataset.period
			});
		})

		

		let p 						= new Object;
		p.act 						= 'set_calendar';
		p.quarters 					= JSON.stringify(quarters);
		p.removing 					= JSON.stringify(removing);
		p.saved 					= JSON.stringify(saved);
		p.hidden_id 				= $('#outgoing_campaign_setting_calendar_hidden_id').val();
		p.schedule_name 			= $('#calendar_schedule_name').val();
		
		$.ajax({
		url: aJaxURL,
		data: p,
		beforeSend: function(xhr){
			$('#button_down').css({display: 'none', opacity: 0})
			$('#button_down').attr('data-status', '0')
			$('#save_calendar > .button_loading').css('display', 'initial')
			$('.block_timetable').css('display', 'block')
		},
		success: function(data){
			$('#outgoing_campaign_setting_calendar_hidden_id').val(data.id);

			if(data.error == 0){
				$('.selecting').removeClass('selecting').addClass('selected');
			}else{
				$('.selecting').removeClass('selecting');
				$('.unselecting').addClass('selected');
			}

			$('#save_calendar > .button_loading').css('display', 'none');
			$('.block_timetable').css('display', 'none');


			$('#second_calendar').click();
		}


		})

	})



		$(document).on('click','#button_add', function(){
			$.ajax({
                    url: aJaxURL,
                    type: "POST",
                    data: "act=get_setting_page",
                    dataType: "json",
                    success: function (data) {
						$("#setting-dialog").html(data.page);
						
						var buttons = {
							done: {
								text: "შენახვა",
								id: "save-dialog",
								class: 'save-dialog'
							},
							cancel: {
								text: "დახურვა",
								id: "cancel-dialog",
								click: function () {
									$(this).dialog("close");
								}
							}
						};

						$("#campaign_com_channel, #campaign_cat, #campaign_connect_type, #call_type").chosen({ search_contains: true });

						GetDialog("setting-dialog","1280","auto", buttons, 'ceneter top');

						GetDateTimes('campaign_start_date');
						GetDateTimes('campaign_end_date');

						LoadKendoTable__settings_voice('');
						LoadKendoTable__settings_upload('');
				
						get_users()
						create_dialog_graph();
						// GENERATE TIME TABLE

						if($('#timeTableSection tr').length > 0){
							$('#timeTableSection tr').remove();
						}

						createTimeTable('timeTableSection')

					}	
			});
		});
		
		$(document).on('click', '#save_calendar', function(){
			$(document).on('click', '#button_down', function(){
				return false
			})
			if($('#button_down').attr('data-status') == 0){
				$('#button_down').css({display: 'block', opacity: 1})
				$('#button_down').attr('data-status', '1')
				return
			}else{
				$('#button_down').css({display: 'none', opacity: 0})
				$('#button_down').attr('data-status', '0')
				return
			}
		})

		$(document).on('click', '#save_parameters', function(){
			$(document).on('click', '#parameters_button_down', function(){
				return false
			})
			
			
			if($('#parameters_button_down').attr('data-status') == 0){
				$('#parameters_button_down').css({display: 'block', opacity: 1})
				$('#parameters_button_down').attr('data-status', '1')
				return
			}else{
				$('#parameters_button_down').css({display: 'none', opacity: 0})
				$('#parameters_button_down').attr('data-status', '0')
				return
			}
		})

		$(document).on('click', '#select_group', function(){
			$(document).on('click', '#group_selector', function(){
				return false
			})

			if($('#group_selector').attr('data-status') == 0){
				$('#group_selector').css({display: 'block', opacity: 1})
				$('#group_selector').attr('data-status', '1')
				return
			}else{
				$('#group_selector').css({display: 'none', opacity: 0})
				$('#group_selector').attr('data-status', '0')
				return
			}
		})

		
		$(document).on('click', '#select_parameters', function(){
			$(document).on('click', '#parameter_selector', function(){
				return false
			})

			if($('#parameter_selector').attr('data-status') == 0){
				$('#parameter_selector').css({display: 'block', opacity: 1})
				$('#parameter_selector').attr('data-status', '1')
				return
			}else{
				$('#parameter_selector').css({display: 'none', opacity: 0})
				$('#parameter_selector').attr('data-status', '0')
				return
			}
		})

		$(document).on('click', '#select_calendar', function(){
			$(document).on('click', '#calendar_selector', function(){
				return false
			})

			if($('#calendar_selector').attr('data-status') == 0){
				$('#calendar_selector').css({display: 'block', opacity: 1})
				$('#calendar_selector').attr('data-status', '1')
				return
			}else{
				$('#calendar_selector').css({display: 'none', opacity: 0})
				$('#calendar_selector').attr('data-status', '0')
				return
			}

		})

		
		$(document).on('click', '#create_group', function(){
			$(document).on('click', '#group_button_down', function(){
				return false
			})
			if($('#group_button_down').attr('data-status') == 0){
				$('#group_button_down').css({display: 'block', opacity: 1})
				$('#group_button_down').attr('data-status', '1')
				return
			}else{
				$('#group_button_down').css({display: 'none', opacity: 0})
				$('#group_button_down').attr('data-status', '0')
				return
			}
		})
		

				

		// Add - Save
		$(document).on("click", ".save-dialog", function () {

		// ACTION
		param = new Object();
		param.act 									= "add_campaign";
		param.campaign_name 						= $('#campaign_name').val();
		param.campaign_cat						 	= $('#campaign_cat').val();
		param.campaign_com_channel 					= $('#campaign_com_channel').val();
		param.campaign_connect_type 				= $('#campaign_connect_type').val();
		param.campaign_start_date 					= $('#campaign_start_date').val();
		param.campaign_end_date 					= $('#campaign_end_date').val();
		param.campaign_comment 						= $('#campaign_comment').val();
		param.campaign_calendar_id					= $('#outgoing_campaign_setting_calendar_hidden_id').val();
		param.campaign_parameter_id					= $('#outgoing_campaign_setting_parameters_hidden_id').val();
		param.campaign_group_id						= $('#outgoing_campaign_setting_group_hidden_id').val();
		param.campaign_id 							= $('#settings_hidden_id').val();
		param.check_id 								= $('#new_settings_hidden_id').val();

		if(param.campaign_name == ''){
			alert('შეავსეთ კომპანიის დასახელება ან აირჩიეთ შაბლონიდან')
		}else{
			$.ajax({
				url: aJaxURL,
				data: param,
				beforeSend: function(){
				
				},
				success: function(data){
					$("#setting-dialog").dialog("close");
					LoadKendoTable();
				}
			})
		}

		});
		$(document).on('click','#compaign_parameters_file_name', function(e){
			e.preventDefault();
			e.stopPropagation();
			$("#compaign_parameters_file").click();
		});
		$(document).on('change','#compaign_parameters_file', function(e){

			//submit the form here
			//var name = $(".fileupchat").val();
			var file_data = $('#compaign_parameters_file').prop('files')[0];
			var fileName = e.target.files[0].name;
			var fileNameN = Math.ceil(Math.random()*99999999999);
			var fileSize = e.target.files[0].size;
			var fileExt = $(this).val().split('.').pop().toLowerCase();
			var form_data = new FormData();
			var parameter_id = $(".dialog__second_section_item[aria-selected='true']").attr('data-id');
			form_data.append('act', 'upload_auto_dialer');
			form_data.append('file', file_data);
			form_data.append('ext', fileExt);
			form_data.append('original', fileName);
			form_data.append('newName', fileNameN);
			form_data.append('parameter_id', parameter_id);

			var fileExtension = ['wav', 'gsm'];
			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				alert("დაუშვებელი ფორმატი!!!  გამოიყენეთ მხოლოდ: "+fileExtension.join(', '));
				$("#compaign_parameters_file").val('');
			}
			else {

				if(fileSize>20971520) {
					alert("შეცდომა! ფაილის ზომა 20MB-ზე მეტია!!!");
					$(".compaign_parameters_file").val('');
				}
				else{
					$.ajax({
					url: 'up2.php', // point to server-side PHP script
					dataType: 'text',  // what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					success: function (data) {
						$("#compaign_parameters_file_name").val(data);

					}
					});
				}

			}
		});
	</script>
	<style>

	.schedule_search {
		width: 100%;
		border: 0;
		border-radius: 0;
		text-align: center;
	}

		.l_inline-tabs div {
			margin: 3px 7px;
		}
		
		th [role="listbox"]{
		display: none;
		}
		.ui-tabs .ui-tabs-panel {
        display: block!important;
        border-width: 0;
        padding: 0.2em 0em;
        background: none;
        
    }

    .ui.tabs li[aria-selected="true"] input {
        background-color: green !important
    }


    #tabs1 div {
        border:none;
    }

    /* // #tabs1 li {
    //     border: none;
    //     background: none;
    //     outline: none;
    //     border-bottom: 1px solid #222422;
    //     cursor: pointer;
    //     margin-right: 11px;
    //     font-family: BPG;
    //     font-size: 11px;
    //     font-weight: bold;
    //     color: #222422;
    // } */

  
    #tabs1{
        height:76px!important;
		margin-bottom: 11px;
    }
	.label_td{
		width:170px;
	}
	#info_table input{
		width: 230px !important;
	}
	#info_table select{
		width: 236px !important;
	}
	[aria-describedby="example_info"]{
		width: 100%
	}

    .l_tablist li{
        text-align:center;
    }
    .progress_grey{
        margin: 0;
        background-color: #c5c5c5;
        padding:1px;
        border-radius: 10px;
        font-weight: 700;
    }
    .progress_yellow{
        margin: 0;
        background-color: #fef400;
        padding:1px;
        border-radius: 10px;
        font-weight: 700;
    }
    .progress_green{
        margin: 0;
        background-color: #60f3a8;
        padding:1px;
        border-radius: 10px;
        font-weight: 700;
    }
    .status_pink{
        margin: 0;
        background-color: #e67ef6;
        border-radius: 10px;
        font-weight: 700;
    }
    .status_lightgreen{
        margin: 0;
        background-color: #b8bc06;
        border-radius: 10px;
        font-weight: 700;
    }
    .status_red{
        margin: 0;
        background-color: #cd0d75;
        border-radius: 10px;
        font-weight: 700;
    }
	.status_done{
        margin: 0;
        background-color: #00c76c;
        border-radius: 10px;
        font-weight: 700;
    }
	.chosen-container{
		width: 95% !important;
	}
	.chosen-container#call_type_chosen {
		width: 370px !important;
	}

	#save-schedule{
		float: left;
    background-color: #614701;
    color: #fff;
    border-radius: 3px;
    margin-right: 806px;
	}		
	#loading1{
		z-index:999;
		top:45%;
		left:45%;
		position: absolute;
		display: none;
		padding-top: 15px;
	}
	</style>
</head>

<body>
<div id="loading1">
    <p><img src="media/images/loader.gif" /></p>
</div>
<div id="tabs" style=" border: 0px solid #aaaaaa;">
	
    <div class="l_responsible_fix" style="width:80%"> 
	
    <table id="table_right_menu" style="top: 80px; left: -1px;">
    	<tr>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;background:#2681DC;" id="show_table" myvar="0">
    			<img alt="table" src="media/images/icons/table_w.png" height="14" width="14">
    		</td>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_log" myvar="0">
    			<img alt="log" src="media/images/icons/log.png" height="14" width="14">
    		</td>
    		<td style="cursor: pointer;padding: 4px;border-right: 1px solid #A3D0E4;" id="show_copy_prit_exel" myvar="0">
    			<img alt="link" src="media/images/icons/select.png" height="14" width="14">
    		</td>
    	</tr>
    </table>
    <ul class="l_tablist">
        <li  onclick="change_btn_color(this.id)" id="current" aria-selected="true" >
        	მიმდინარე<p id="active_campaign" style="margin: 0;">0</p>
        </li>
        <li  onclick="change_btn_color(this.id)" id="draft">
        	დრაფთი <p style="margin: 0;">0</p>
        </li>
        <li  onclick="change_btn_color(this.id)" id="archive">
        	არქივი <p style="margin: 0;">0</p>
        </li>
	</ul>
	
    <div id="button_area" style="height:0;">
		
	</div>
    <!-- <div class="callapp_dialog_filters" style="padding: 5px 10px;">
        <button id="callapp_show_filter_button2" style="background: #fff;">ფილტრი <img id="filterC" class="filterIcon" src="media/images/icons/lemons_filter.png"></button>
        <div class="callapp_filter_body2" style="width: 100%; max-height: 0px; transition:max-height 0.3s ease;">
            <div style="width:100%;">
                <div style="overflow: auto;width: 100%;">
                    <table class="filter_multiselect_table2">
                        <tr id="dialog_filters">
                            <td style="width: 230px" data-show="true">
								<div class="dialog_filter_title"> 
									<span>კომ. არხი</span>
									<span id="source_sum">0</span>
								</div>
								<ul class="dialog_filter_checklist" id="filt_source">
									
								</ul>
							</td>
                            <td style="width: 230px" data-show="true">
								<div class="dialog_filter_title"> 
									<span>სტატუსი</span>
									<span id="status_sum">0</span>
								</div>
								<ul class="dialog_filter_checklist" id="filt_status">
									
									
								</ul>
							</td>
							<td style="width: 230px" data-show="true">
								<div class="dialog_filter_title"> 
									<span>კატეგორია</span>
									<span id="category_sum">0</span>
								</div>
								<ul class="dialog_filter_checklist" id="filt_category">
									
									
								</ul>
							</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div> -->

	<!-- <button id="create_campaign">BUTTON</button> -->
	<div id="grid"></div>


</div>
<div class="column col-lg-3 col-md-6 col-sm-6 col-xs-12" id="compaign_statistics" style="position: absolute;right: 0;width: 410px; transition: 0.3s ease; display: none">
	<div class="panel panel-default card-view panel-refresh">
		<div class="refresh-container" style="padding-right: 15px; padding-left: 15px; display: none;">
			<div class="la-anim-1"></div>
		</div>
		<div class="panel-heading">
			<div class="pull-left">
				<h2 style="font-weight: 900;" class="panel-title txt-dark">მიმდინარე კამპანიები</h2>
			</div>
			<div class="pull-right">
				<a href="#" class="pull-left inline-block refresh mr-15">
					<i class="zmdi zmdi-replay"></i>
				</a>
				<div class="pull-left inline-block dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
					<ul class="dropdown-menu bullet dropdown-menu-right" role="menu">
						<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Devices</a>
						</li>
						<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>General</a>
						</li>
						<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>Referral</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-wrapper collapse in" style="border-bottom:1px solid grey;">
			<div class="panel-body" style="position: relative;">
				<div id="chart3" style="margin-top: -16px; margin-bottom: -21px; margin-left: -30px; min-height: 240px;"><div id="apexchartsy5zl0z69k" class="apexcharts-canvas apexchartsy5zl0z69k light" style="width: 240px; height: 240px;"><svg id="SvgjsSvg2298" width="240" height="240" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG2300" class="apexcharts-inner apexcharts-graphical" transform="translate(29.5, 10)"><defs id="SvgjsDefs2299"><clipPath id="gridRectMasky5zl0z69k"><rect id="SvgjsRect2302" width="184" height="196" x="-0.5" y="-0.5" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><clipPath id="gridRectMarkerMasky5zl0z69k"><rect id="SvgjsRect2303" width="223" height="235" x="-20" y="-20" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath></defs><g id="SvgjsG2305" class="apexcharts-pie" data:innerTranslateX="0" data:innerTranslateY="-25"><g id="SvgjsG2306" transform="translate(0, -5) scale(1)"><circle id="SvgjsCircle2307" r="42.13414634146342" cx="91.5" cy="91.5" fill="transparent"></circle><g id="SvgjsG2308"><g id="apexcharts-series-0" class="apexcharts-series apexcharts-pie-series უპასუხო" rel="1"><path id="apexcharts-donut-slice-0" d="M 91.5 7.231707317073159 A 84.26829268292684 84.26829268292684 0 0 1 135.28445237493716 19.499535543704425 L 113.39222618746858 55.49976777185221 A 42.13414634146342 42.13414634146342 0 0 0 91.5 49.36585365853658 L 91.5 7.231707317073159 z" fill="rgba(193,51,121,1)" fill-opacity="1" stroke="#ffffff" stroke-opacity="1" stroke-linecap="butt" stroke-width="1" stroke-dasharray="0" class="apexcharts-pie-area" index="0" j="0" data:angle="31.304347826086957" data:startAngle="0" data:strokeWidth="1" data:value="2" data:pathOrig="M 91.5 7.231707317073159 A 84.26829268292684 84.26829268292684 0 0 1 135.28445237493716 19.499535543704425 L 113.39222618746858 55.49976777185221 A 42.13414634146342 42.13414634146342 0 0 0 91.5 49.36585365853658 L 91.5 7.231707317073159 z" data:pieClicked="false"></path></g><g id="apexcharts-series-1" class="apexcharts-series apexcharts-pie-series ნაპასუხები" rel="2"><path id="apexcharts-donut-slice-1" d="M 135.28445237493716 19.499535543704425 A 84.26829268292684 84.26829268292684 0 1 1 91.48529240845114 7.2317086005526505 L 91.49264620422557 49.365854300276325 A 42.13414634146342 42.13414634146342 0 1 0 113.39222618746858 55.49976777185221 L 135.28445237493716 19.499535543704425 z" fill="rgba(58,181,145,1)" fill-opacity="1" stroke="#ffffff" stroke-opacity="1" stroke-linecap="butt" stroke-width="1" stroke-dasharray="0" class="apexcharts-pie-area" index="0" j="1" data:angle="328.69565217391306" data:startAngle="31.304347826086957" data:strokeWidth="1" data:value="21" data:pathOrig="M 135.28445237493716 19.499535543704425 A 84.26829268292684 84.26829268292684 0 1 1 91.48529240845114 7.2317086005526505 L 91.49264620422557 49.365854300276325 A 42.13414634146342 42.13414634146342 0 1 0 113.39222618746858 55.49976777185221 L 135.28445237493716 19.499535543704425 z" selected="false" data:pieClicked="false"></path></g></g></g></g><line id="SvgjsLine2313" x1="0" y1="0" x2="183" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine2314" x1="0" y1="0" x2="183" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line></g><text id="SvgjsText2301" font-family="Helvetica, Arial, sans-serif" x="10" y="22" text-anchor="start" dominant-baseline="auto" font-size="20px" fill="#ffffff" class="apexcharts-title-text" style="font-family: Helvetica, Arial, sans-serif; opacity: 1;"></text></svg><div class="apexcharts-legend"></div></div></div>

				<span style="right:5px;margin-left: 85%; font-weight: 900 !important;  position: absolute;  margin-top: -165px;  font-size: 16px;">დაგეგმილი</span>
				<span style="right:5px;margin-left: 70%; font-weight: 900 !important; font-size: 30px; position: absolute;  margin-top: -150px;color: #3ab591" class="ng-binding">900</span>
				<span style="right:5px;margin-left: 87%; font-weight: 900 !important;  position: absolute;  margin-top: -113px;">შესრულებულია</span>
				<span style="right:5px; margin-left: 87%; font-weight: 900 !important; position: absolute;margin-top: -95px;color: black;font-size: 16px; " class="ng-binding">50%</span>

			</div>
		</div>
		
		<?php
			foreach($source_charts['result'] AS $chart){
				echo '  <div class="apex_source_div">
							<div class="apex_source_column non-chart" style="width: 30%;">
								<img src="media/images/icons/comunication/'.$chart['background_image_name'].'" height="18" width="18">
								<p>'.$chart['name'].'</p>
							</div>
							<div class="apex_source_column non-chart" style="width: 20%;">
								<span>1200</span>
							</div>
							<div class="apex_source_column non-chart" style="width: 20%; color: grey;">
								<span>67%</span>
							</div>
							<div class="apex_source_column" style="width: 30%;">
								<div id="chart'.$chart['id'].'"></div>
							</div>
						</div>';
			}
		?>
	</div>
</div>

    <!-- jQuery Dialog -->
    <div id="add-edit-form" class="form-dialog" title="კამპანია"></div>

	<div id="setting-dialog" class="form-dialog" title="პარამეტრები"></div>
	<div id="setting_voice_add-dialog" class="form-dialog" title="დამატება"></div>
	<div id="setting_upload_add-dialog" class="form-dialog" title="დამატება"></div>
	<div id="get_processing-dialog" class="form-dialog" title="დამუშავება"></div>

	
    <!-- jQuery Dialog -->
    <div id="image-form" class="form-dialog" title="თანამშრომლის სურათი">
    	<img id="view_img" src="media/uploads/images/worker/0.jpg">
	</div>
	 <!-- jQuery Dialog -->
    <div id="add-group-form" class="form-dialog" title="ჯგუფი">
	</div>
	<div id="add-edit-form-img" class="form-dialog" title="თანამშრომლის სურათი">
	</div>
	<div id="input_editor" class="form-dialog" title="ველები"></div>
	<div id="add_selector" class="form-dialog" title="სელექტორი/CHECKBOX/RADIO პარამეტრი"></div>
</body>
<style>
.container {
  display: flex;
  align-items: center;
  justify-content:space-between;
}

</style>
</html>