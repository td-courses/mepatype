<?php

require_once('../../includes/classes/class.Mysqli.php');
global $db;
$db     = new dbClass();
$action = $_REQUEST['act'];
$error = '';
$week_error = '';
$data = array();
$code_not_finde='';
$excel_data = array();


switch ($action) {
    case 'get_add_page':
        if (isset($_REQUEST['voice_id'])) {
            if (empty($_SESSION['last_id'])) {
                $page = GetPage_for_add_voice($_REQUEST['voice_id']);
                $data = array('page' => $page);
            } else {
                $page = GetPage_for_add_voice($_SESSION['last_id']);
                $data = array('page' => $page);
            }

        } else if (isset($_REQUEST['week_id'])) {

            $page = GetPage_for_add_week();
            $data = array('page' => $page);


        } else if (isset($_REQUEST['position_id'])) {

            $page = GetPage_for_add_position();
            $data = array('page' => $page);


        }else if (isset($_REQUEST['base_id'])) {
            if (empty($_SESSION['last_id'])){
                $page = GetPage_for_base($_REQUEST['base_id']);
                $data = array('page' => $page);

            }else{
                $page = GetPage_for_base($_SESSION['last_id']);
                $data = array('page' => $page);

            }


        } else if (isset($_REQUEST['add_base_id'])) {

            $page = GetPage_for_add_call();
            $data = array('page' => $page);


        } else {

            $page = GetPage_for_add();
            $data = array('page' => $page);
        }


        break;

    case 'get_edit_page':

        if (isset($_REQUEST['add_base_id'])) {
            $request_id = $_REQUEST['id'];
            $details_id=$_REQUEST['details_id'];
            $page = GetPage_for_edit_call(Getproduct($details_id, "autocall_request_details"),Getproduct($_REQUEST['id'], "autocall_request_base"));
            $data = array('page' => $page);

        } else if (isset($_REQUEST['voice_id'])) {
            $request_id = $_REQUEST['id'];
            $page = GetPage_for_edit_voice(Getproduct($request_id, "autocall_request_voice"));
            $data = array('page' => $page);

        } else if (isset($_REQUEST['week_id'])) {
            $request_id = $_REQUEST['id'];
            $page = GetPage_for_edit_week(Getproduct($request_id, "autocall_request_dates"));
            $data = array('page' => $page);

        }else if (isset($_REQUEST['position_id'])) {
            $request_id = $_REQUEST['id'];
            $page = GetPage_for_edit_position(Getproduct($request_id, "autocall_request_positions"));
            $data = array('page' => $page);

        }else if (isset($_REQUEST['base_id'])) {
            $request_id = $_REQUEST['base_id'];
            $id=$_REQUEST['id'];
            $page = GetPage_for_edit_base(Getproduct($id, "autocall_request_details"));
            $data = array('page' => $page);

        }else if (isset($_REQUEST['file_id'])) {
            $request_id = $_REQUEST['file_id'];
            $id=$_REQUEST['id'];
            $page = GetPage_for_file(Getproduct($id, "autocall_request_file"));
            $data = array('page' => $page);

        }else {
            $_SESSION['last_id'] = '';
            $request_id = $_REQUEST['id'];
            $page = GetPage_for_edit(Getproduct($request_id, "autocall_request"));
            $data = array('page' => $page);
        }


        break;

    case 'save_position':

        $position = $_REQUEST['position'];

        $request_position_id = $_REQUEST['request_position_id'];

        $request_table_id = $_REQUEST['request_table_id'];
        $name = $_REQUEST['name'];
        $call_type_id = $_REQUEST['call_type_id'];
        $start_datetime = $_REQUEST['start_datetime'];
        $end_datetime = $_REQUEST['end_datetime'];
        $retry_time = $_REQUEST['retry_time'];
        $answer_wait_time = $_REQUEST['answer_wait_time'];
        $duration = $_REQUEST['duration'];
        $max_retries = $_REQUEST['max_retries'];
        $max_sum_listen_time = $_REQUEST['max_sum_listen_time'];
        $comment_main = $_REQUEST['comment_main'];
        $note = $_REQUEST['note'];
        $code = $_REQUEST['code'];
        $done_listen_time = $_REQUEST['done_listen_time'];
        $channel_quantity = $_REQUEST['channel_quantity'];


        if (!$request_table_id) {

            if (empty($_SESSION["last_id"])) {
                Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                $id = Get_Last_Id("autocall_request");
                $_SESSION["last_id"] = $id;

                if ($request_position_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Position($request_position_id, $id, $position);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Position($id, $position);
                }

            } else {
                $id = $_SESSION["last_id"];
                if ($request_position_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Position($request_position_id, $id, $position);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Position($id, $position);
                }

            }


        } else {
            $_SESSION["last_id"] = '';
            if ($request_position_id) {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Save_Position($request_position_id, $request_table_id, $position);
            } else {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Add_Position($request_table_id, $position);
            }


        }

        break;
    case 'save_week_time':
        $week_day_id = $_REQUEST['week_day_id'];
        $start_time = $_REQUEST['start_time'];
        $end_time = $_REQUEST['end_time'];
        $request_week_id = $_REQUEST['request_week_id'];

        $request_table_id = $_REQUEST['request_table_id'];
        $name = $_REQUEST['name'];
        $call_type_id = $_REQUEST['call_type_id'];
        $start_datetime = $_REQUEST['start_datetime'];
        $end_datetime = $_REQUEST['end_datetime'];
        $retry_time = $_REQUEST['retry_time'];
        $answer_wait_time = $_REQUEST['answer_wait_time'];
        $duration = $_REQUEST['duration'];
        $max_retries = $_REQUEST['max_retries'];
        $max_sum_listen_time = $_REQUEST['max_sum_listen_time'];
        $comment_main = $_REQUEST['comment_main'];
        $note = $_REQUEST['note'];
        $code=$_REQUEST['code'];
        $done_listen_time = $_REQUEST['done_listen_time'];
        $channel_quantity = $_REQUEST['channel_quantity'];


        if (!$request_table_id) {

            if (empty($_SESSION["last_id"])) {
                Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                $id = Get_Last_Id("autocall_request");
                $_SESSION["last_id"] = $id;

                if ($request_week_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Week($request_week_id, $id, $start_time, $end_time, $week_day_id, $week_error);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Week($id, $start_time, $end_time, $week_day_id, $week_error);
                }

            } else {
                $id = $_SESSION["last_id"];
                if ($request_week_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Week($request_week_id, $id, $start_time, $end_time, $week_day_id, $week_error);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Week($id, $start_time, $end_time, $week_day_id, $week_error);
                }

            }


        } else {
            $_SESSION["last_id"] = '';
            if ($request_week_id) {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Save_Week($request_week_id, $request_table_id, $start_time, $end_time, $week_day_id, $week_error);
            } else {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Add_Week($request_table_id, $start_time, $end_time, $week_day_id, $week_error);
            }


        }


        break;
    case 'save_voice':
        //   request table
        $file_name = $_REQUEST['file_name'];
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $call_type_id = $_REQUEST['call_type_id'];
        $start_datetime = $_REQUEST['start_datetime'];
        $end_datetime = $_REQUEST['end_datetime'];


        $retry_time = $_REQUEST['retry_time'];
        $answer_wait_time = $_REQUEST['answer_wait_time'];
        $duration = $_REQUEST['duration'];
        $max_retries = $_REQUEST['max_retries'];
        $max_sum_listen_time = $_REQUEST['max_sum_listen_time'];
        $comment_main = $_REQUEST['comment_main'];
        $note = $_REQUEST['note'];
        $code=$_REQUEST['code'];
        $done_listen_time = $_REQUEST['done_listen_time'];
        $channel_quantity = $_REQUEST['channel_quantity'];
//      voice table
        $voice_id = $_REQUEST['voice_id'];
        $voice_type_id = $_REQUEST['voice_type_id'];
        $request_table_id = $_REQUEST['request_table_id'];
        $request_voice_id = $_REQUEST['request_voice_id'];
        $voice_type_id = $_REQUEST['voice_type_id'];
//        $value = $_REQUEST['value'];
        $position = $_REQUEST['position'];
        $voice_name = $_REQUEST['voice_name'];


        if (!$request_table_id) {

            if (empty($_SESSION["last_id"])) {
                Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                $id = Get_Last_Id("autocall_request");
                $_SESSION["last_id"] = $id;

                if ($request_voice_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Voice($request_voice_id, $id, $voice_type_id, $position, $voice_name, $file_name);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Voice($id, $voice_type_id, $position, $voice_name, $file_name);
                }

            } else {
                $id = $_SESSION["last_id"];
                if ($request_voice_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Voice($request_voice_id, $id, $voice_type_id, $position, $voice_name, $file_name);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Voice($id, $voice_type_id, $position, $voice_name, $file_name);
                }

            }


        } else {
            $_SESSION["last_id"] = '';
            if ($request_voice_id) {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Save_Voice($request_voice_id, $request_table_id, $voice_type_id, $position, $voice_name, $file_name);
            } else {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Add_Voice($request_table_id, $voice_type_id, $position, $voice_name, $file_name);
            }


        }

        break;

    case 'save_call':
//   request table

        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $call_type_id = $_REQUEST['call_type_id'];
        $start_datetime = $_REQUEST['start_datetime'];
        $end_datetime = $_REQUEST['end_datetime'];


        $retry_time = $_REQUEST['retry_time'];
        $answer_wait_time = $_REQUEST['answer_wait_time'];
        $duration = $_REQUEST['duration'];
        $max_retries = $_REQUEST['max_retries'];
        $max_sum_listen_time = $_REQUEST['max_sum_listen_time'];
        $comment_main = $_REQUEST['comment_main'];
        $note = $_REQUEST['note'];
        $code=$_REQUEST['code'];
        $done_listen_time = $_REQUEST['done_listen_time'];
        $channel_quantity = $_REQUEST['channel_quantity'];
//      call table
        $details_id=$_REQUEST['details_id'];
        $call_id = $_REQUEST['call_id'];
        $phone_number = $_REQUEST['phone_number'];
        $request_table_id = $_REQUEST['request_table_id'];
        $request_details_id = $_REQUEST['request_details_id'];
        $comment=$_REQUEST['comment'];
        $number_1 = $_REQUEST['number_1'];
        $number_2 = $_REQUEST['number_2'];
        $number_3 = $_REQUEST['number_3'];
        $number_4 = $_REQUEST['number_4'];
        $number_5 = $_REQUEST['number_5'];
        $number_6 = $_REQUEST['number_6'];
        $number_7 = $_REQUEST['number_7'];
        $number_8 = $_REQUEST['number_8'];
        $number_9 = $_REQUEST['number_9'];
        $number_10 = $_REQUEST['number_10'];




        if (!$request_table_id) {

            if (empty($_SESSION["last_id"])) {
                Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                $id = Get_Last_Id("autocall_request");
                $_SESSION["last_id"] = $id;

                if ($request_details_id) {
                    Save_Details($details_id,$id,$comment);
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Call($request_details_id, $details_id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10);
                } else {

                    Add_Details($id,$comment,$details_id);
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Call($details_id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10);
                }

            } else {
                $id = $_SESSION["last_id"];

                if ($request_details_id) {
                    Save_Details($details_id,$id,$comment);
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Save_Call($request_details_id, $details_id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10);
                } else {
                    Add_Details($id,$comment,$details_id);
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    Add_Call($details_id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10);
                }

            }


        } else {


            if ($request_details_id) {
                Save_Details($details_id,$request_table_id,$comment);
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Save_Call($request_details_id, $details_id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10);
            } else {
                Add_Details($request_table_id,$comment,$details_id);
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                Add_Call($details_id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10);

            }


        }

        break;
    case 'save_modal':
        $request_table_id = $_REQUEST['request_table_id'];
        $name = $_REQUEST['name'];
        $call_type_id = $_REQUEST['call_type_id'];
        $start_datetime = $_REQUEST['start_datetime'];
        $end_datetime = $_REQUEST['end_datetime'];


        $retry_time = $_REQUEST['retry_time'];
        $answer_wait_time = $_REQUEST['answer_wait_time'];
        $duration = $_REQUEST['duration'];
        $max_retries = $_REQUEST['max_retries'];
        $max_sum_listen_time = $_REQUEST['max_sum_listen_time'];
        $comment_main = $_REQUEST['comment_main'];
        $note = $_REQUEST['note'];
        $code=$_REQUEST['code'];
        $done_listen_time = $_REQUEST['done_listen_time'];
        $channel_quantity = $_REQUEST['channel_quantity'];

        if (!$request_table_id&&empty($_SESSION['last_id'])) {

            Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);


        } else {

            Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);

        }


        break;

    case  'get_list_position':

        $id = $_REQUEST["request_table_id"];
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];

        if (empty($_SESSION["last_id"])) {
            $db->setQuery("
                 SELECT id, position
                  FROM autocall_request_positions 
                  WHERE actived=1 and request_id='$id'
        ");

        } else {

            $db->setQuery("
                          SELECT id, position
                          FROM autocall_request_positions 
                          WHERE actived=1 and request_id='" . $_SESSION["last_id"] . "'
                 

        ");
        }
        $data = $db->getList($count,$hidden,1);


        break;

    case 'get_list_week_time':

        $id = $_REQUEST["request_table_id"];
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];

        if (empty($_SESSION["last_id"])) {
            $db->setQuery("
                 SELECT 
                     ard.id,
                     arw.week_day,
                     ard.start_time,
                     ard.end_time
         
                 FROM 
                       autocall_request_dates ard
                 LEFT JOIN 
                      autocall_request_weekdays arw
                 ON   
                      ard.week_day_id=arw.id
                                
                 WHERE 
                     ard.request_id = $id
                     and ard.actived = 1
        ");

        } else {

            $db->setQuery("
                         SELECT 
                                 ard.id,
                                 arw.week_day,
                                 ard.start_time,
                                 ard.end_time
                                 
                         FROM 
                               autocall_request_dates ard
                         LEFT JOIN 
                              autocall_request_weekdays arw
                         ON   
                              ard.week_day_id=arw.id
                                        
                         WHERE 
                                 ard.request_id = '" . $_SESSION["last_id"] . "'
                                 and ard.actived = 1

        ");
        }
        $data = $db->getList($count,$hidden,1);

        break;

    case 'get_list_voice':
        $id = $_REQUEST["request_table_id"];
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];

        if (empty($_SESSION["last_id"])) {
            $db->setQuery("
                SELECT
                       arv.id,
                       arv.name, 
                       avt.name,
                       arv.position
                FROM 
                       autocall_request_voice arv
				LEFT JOIN
                       autocall_voice_type avt
                ON 
                      arv.voice_type_id=avt.id
                WHERE 
                      arv.request_id = $id
                      and actived = 1
        ");

        } else {

            $db->setQuery("
                SELECT
                       arv.id,
                       arv.name, 
                       avt.name,
                       arv.position
                FROM 
                       autocall_request_voice arv
				LEFT JOIN
                       autocall_voice_type avt
                ON 
                      arv.voice_type_id=avt.id
                WHERE 
                      arv.request_id = '" . $_SESSION["last_id"] . "'
                      and actived = 1
  
        ");
        }
        $data = $db->getList($count,$hidden,1);

        break;
    case 'call_table':

        $request_table_id = $_REQUEST["request_table_id"];
        $data_call_table1 = '';
        $data_call_table2 = '';
        if (empty($_SESSION["last_id"])) {
            $data_call_table1 .= get_call_table1($request_table_id);
            $data_call_table2 .= get_call_table2($request_table_id);
        } else {
            $data_call_table1 .= get_call_table1($_SESSION["last_id"]);
            $data_call_table2 .= get_call_table2($_SESSION["last_id"]);
        }
        break;
    case 'check_code':
        $code_check=$_REQUEST['code'];
        $db->setQuery("SELECT code from autocall_request WHERE `code`='$code_check' and actived=1 
            ");
        $res=$db->getResultArray();
        if($res[result]>0){
            $error=$code_check;
        }

        break;
    case 'get_list_call':


        $id = $_REQUEST["request_table_id"];
        $details_id=$_REQUEST['details_id'];
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];

        if (empty($_SESSION["last_id"]))

        {


            $db->setQuery("
                    SELECT 
                        id, 
                        phone_number,
                        number_1,
                        number_2,
                        number_3,
                        number_4,
                        number_5,
                        number_6,
                        number_7,
                        number_8,
                        number_9,
                        number_10
                    FROM
                       autocall_request_base
                    WHERE actived = 1 
                    and request_id='$details_id'   
            ");

        } else {

            $db->setQuery("
                    SELECT 
                    id, 
                    phone_number,
                    number_1,
                    number_2,
                    number_3,
                    number_4,
                    number_5,
                    number_6,
                    number_7,
                    number_8,
                    number_9,
                    number_10
                    FROM
                      autocall_request_base
                    WHERE 
                     actived = 1 
                     and request_id='".Get_Last_Id_Details(Get_Last_Id_Request("autocall_request_voice"))."'
                
             ");
        }


        $data = $db->getList($count,$hidden,1);

        break;
    case 'get_list_all_file':
        $id = $_REQUEST["request_table_id"];
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];

        if(isset($_REQUEST['request_base_id'])){
            $base_id=$_REQUEST['request_base_id'];
            $db->setQuery("SELECT  request_id FROM autocall_request_base WHERE actived=1 and id='$base_id'");
            $res_r_b_i=$db->getResultArray();
            $request_base_id= $res_r_b_i[result][0];
            $details_request_id=$request_base_id['request_id'];
            $db->setQuery("SELECT request_id FROM autocall_request_details WHERE actived=1 and id='$details_request_id'");
            $res_m_i=$db->getResultArray();
            $request_main_id=$res_m_i[result][0];
            $id=$request_main_id['request_id'];
            $db->setQuery("
               SELECT id,file_name,file_type FROM autocall_request_file WHERE request_id='$id'AND actived=1;
 
        ");

        }
        else if (empty($_SESSION["last_id"])) {
            $db->setQuery("
               SELECT id,file_name,file_type FROM autocall_request_file WHERE request_id='$id'AND actived=1;
 
        ");

        } else {

            $db->setQuery("
   SELECT id,file_name,file_type FROM autocall_request_file WHERE actived=1 AND request_id='".$_SESSION["last_id"]."'  ;
                
        ");
        }


        $data = $db->getList($count,$hidden,1);

        break;
    case 'get_list_details':


        $id = $_REQUEST["request_table_id"];
        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];

        if (empty($_SESSION["last_id"])) {
            $db->setQuery("
               SELECT
ard.id,
ard.set_time,
ard.comment,
 CONCAT('<div style=\"text-align: center;\">',ROUND(SUM(case when arb.status = 6 then 1 else 0 end)/COUNT(arb.id)*100),'%</div> <div class=\"container\"> <div class=\"skills\" style=\"width: ',
  ROUND(SUM(case when arb.status = 6 then 1 else 0 end)/COUNT(arb.id)*100),
  '%; background-color:',
	case when ROUND(SUM(case when arb.status = 6 then 1 else 0 end)/COUNT(arb.id)*100)= 100 then 'green' else 'red' end,
  ';\">',
	
	'</div></div>'	
         ) as bar,
SUM(case when arb.actived=1 THEN 1 ELSE 0 end) as quantity,
ard.actived,

ard.request_id

 

FROM 
 autocall_request_details ard
 left JOIN  autocall_request_base arb ON ard.id = arb.request_id

GROUP BY ard.id
HAVING ard.actived=1  and ard.request_id='$id' 
        ");

        } else {

            $db->setQuery("
SELECT
ard.id,
ard.set_time,
ard.comment,
 CONCAT('<div style=\"text-align: center;\">',ROUND(SUM(case when arb.status = 6 then 1 else 0 end)/COUNT(arb.id)*100),'%</div> <div class=\"container\"> <div class=\"skills\" style=\"width: ',
  ROUND(SUM(case when arb.status = 6 then 1 else 0 end)/COUNT(arb.id)*100),
  '%; background-color:',
	case when ROUND(SUM(case when arb.status = 6 then 1 else 0 end)/COUNT(arb.id)*100)= 100 then 'green' else 'red' end,
  ';\">',
	
	'</div></div>'	
         ) as bar,
SUM(case when arb.actived=1 THEN 1 ELSE 0 end) as quantity,
ard.actived,

ard.request_id

 

FROM 
 autocall_request_details ard
 left JOIN  autocall_request_base arb ON ard.id = arb.request_id

GROUP BY ard.id
HAVING ard.actived=1  and ard.request_id='" . $_SESSION["last_id"] . "' 

                
        ");
        }


        $data = $db->getList($count,$hidden,1);

        break;
    case 'get_list':

        $count = $_REQUEST['count'];
        $hidden = $_REQUEST['hidden'];
        $user = $_SESSION['CCUSERID'];
//        $group	= checkgroup($user);

        $db-> setQuery("SELECT
 ar.id,
 ar.start_datetime,
 ar.end_datetime,
 ar.`name`, 
 ar.code,
 'ფაილის იმპორტი' source, 
 act.`name`, 
 SUM(case when ard.actived=1 THEN 1 ELSE 0 end) as contact_count,
 ar.actived
  

FROM 
 autocall_request ar 
 left JOIN  autocall_request_details ard ON ar.id = ard.request_id
 left JOIN  autocall_call_type act ON ar.call_type_id = act.id
 left JOIN  autocall_request_status ars ON ar.request_status_id = ars.id
GROUP BY ar.id
HAVING ar.actived=1
");

        $data=$db->getList($count,$hidden,1);

        break;
    case 'upload_excel_code':

        $file_name = $_REQUEST['file_name'];
        $excel_data = array();

        $file = fopen("../../media/upload_auto_dialer/" . $file_name, "r");
        $count = 0;
        while (!feof($file)) {
            $excel_data[$count] = fgetcsv($file, "1000", chr(9));
            $count++;
        }
        fclose($file);
        $code_not_finde=get_request_id_with_code($excel_data);

        break;

    case  'upload_all_file':

        $request_id=$_REQUEST['request_id'];
        $file_name = $_REQUEST['file_name'];
        $file_type=$_REQUEST['file_type'];


        $request_position_id = $_REQUEST['request_position_id'];

        $request_table_id = $_REQUEST['request_table_id'];
        $name = $_REQUEST['name'];
        $call_type_id = $_REQUEST['call_type_id'];
        $start_datetime = $_REQUEST['start_datetime'];
        $end_datetime = $_REQUEST['end_datetime'];
        $retry_time = $_REQUEST['retry_time'];
        $answer_wait_time = $_REQUEST['answer_wait_time'];
        $duration = $_REQUEST['duration'];
        $max_retries = $_REQUEST['max_retries'];
        $max_sum_listen_time = $_REQUEST['max_sum_listen_time'];
        $comment_main = $_REQUEST['comment_main'];
        $note = $_REQUEST['note'];
        $code = $_REQUEST['code'];
        $done_listen_time = $_REQUEST['done_listen_time'];
        $channel_quantity = $_REQUEST['channel_quantity'];


        if (!$request_table_id) {

            if (empty($_SESSION["last_id"])) {
                Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                $id = Get_Last_Id("autocall_request");
                $_SESSION["last_id"] = $id;

                if ($request_position_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    upload_all_file($request_id,$file_name,$file_type);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    upload_all_file(Get_Last_Id("autocall_request"),$file_name,$file_type);
                }

            } else {
                $id = $_SESSION["last_id"];
                if ($request_position_id) {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    upload_all_file($id,$file_name,$file_type);
                } else {
                    Saveproduct($id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                    upload_all_file($id,$file_name,$file_type);
                }

            }


        } else {
            $_SESSION["last_id"] = '';
            if ($request_id) {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                upload_all_file($request_id,$file_name,$file_type);
            } else {
                Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity);
                upload_all_file($request_table_id,$file_name,$file_type);
            }


        }




        break;

    case 'upload_excel':
        $request_id = $_REQUEST['request_id'];
        $file_name = $_REQUEST['file_name'];
        $excel_data = array();

        $file = fopen("../../media/upload_auto_dialer/" . $file_name, "r");
        $count = 0;
        while (!feof($file)) {
            $excel_data[$count] = fgetcsv($file, "1000", chr(9));
            $count++;
        }


        fclose($file);
        if (empty($_SESSION['last_id'])) {
            write_to_db_from_excel($request_id, $excel_data);
        } else {
            write_to_db_from_excel(Get_Last_Id('autocall_request_details'), $excel_data);
        }


        break;
    case 'create_excel':
        $request_id = $_REQUEST['request_id'];
        create_excel($request_id);

        break;

    case 'create_excel_code':

        create_excel_code();

        break;
    case  'disable':

        $id = $_REQUEST['id'];
        $tb_name = $_REQUEST["tb_name"];
        if ($tb_name == "autocall_request") {
            disable_main_table($id, $tb_name);
        } else {
            disable($id, $tb_name);
        }

        break;

    case 'paus':
        $id = $_REQUEST['id'];
        set_paus($id);
        break;
    case 'stop':
        $id = $_REQUEST['id'];
        set_stop($id);
        break;

    case 'play':
        $id = $_REQUEST['id'];
        set_play($id);

        break;

    default:
        $error = 'Action is Null';
}

$data["data_call_table1"] = $data_call_table1;
$data["data_call_table2"] = $data_call_table2;
$data['ex'] = $excel_data;
$data['error'] = $error;
$data["week_error"] = $week_error;
$data["code_not_finde"] = $code_not_finde;

echo json_encode($data);

function  upload_all_file($request_id,$file_name,$file_type){
    global $db;
    $db->setQuery(" INSERT INTO
                        autocall_request_file  (
                        request_id,
                        file_name,
                        file_type
                       
                        )  
                        VALUES (
                        '$request_id',
                        '$file_name',
                        '$file_type'
                        
                        )");
    $db->execQuery();
}

function set_play($id)
{
    global $db;
    $db->setQuery(" 
                   UPDATE autocall_request
                   SET    request_status_id = 4
                   WHERE  id = '$id' and (request_status_id =2||request_status_id =0 )");
    if ($db->execQuery()) {
        global $error;
        $error = 'ოპერაცია წარმატებით დასრულდა!';
    } else {
        global $error;
        $error = 'ოპერაცია წარუმატებლად დასრულდა!';
    }
}

function set_stop($id)
{
    global $db;
    $db->setQuery(" 
                   UPDATE autocall_request
                   SET    request_status_id = 3
                   WHERE  id = '$id' and request_status_id<>1");
    if ($db->execQuery()) {
        global $error;
        $error = 'ოპერაცია წარმატებით დასრულდა!';
    } else {
        global $error;
        $error = 'ოპერაცია წარუმატებლად დასრულდა!';
    }
}

function set_paus($id)
{
    global $db;
    $db->setQuery(" 
                   UPDATE autocall_request
                   SET    request_status_id = 2
                   WHERE  id = '$id' and request_status_id =4 ");
    if ($db->execQuery()) {
        global $error;
        $error = 'ოპერაცია წარმატებით დასრულდა!';
    } else {
        global $error;
        $error = 'ოპერაცია წარუმატებლად დასრულდა!';
    }


}
function get_request_id_with_code($excel_data){
    global $db;
    $count=0;
    $db->setQuery("SELECT code,id FROM autocall_request WHERE actived=1;");
    $qer=$db->getResultArray();
    $res=$qer[result];
    $array_ = Array();
    $ar= array();
    $code_not_finde1='';
    $ec=0;
    $c=0;
    while (sizeof($res)>$c) {
        $id='';
        foreach ($excel_data as $x){
            if($x[0]==$res[$c]['code']){
                $id=$res[$c]['id'];
                array_push($array_,
                    array(
                        $res[$c]['code'],
                        $res[$c]['id'],
                        $x[1],
                        $x[2],
                        $x[3],
                        $x[4],
                        $x[5],
                        $x[6],
                        $x[7]
                    )
                );
            }
        }
        if($id!=''){
            $ar[$count]=$id;
            $count++;
        }
        $c++;
    }

    $voice_queery="INSERT INTO
                        autocall_request_voice  (
                        request_id,
                        name
                         )  
                        VALUES 
                        ";
    $check_voice=0;


    foreach ($ar as $a){
        $check_voice++;
        $db->setQuery("SELECT id FROM autocall_request_voice WHERE voice_type_id=0 and actived=1 and request_id=$a");
        $res_check_voice=$db->getResultArray();
        if($res_check_voice[result]>0){


        }else{

            $voice_queery .= "(
                        '$a',
                         '".$excel_data[0][2]."'
                          ),
                          (
                        '$a',
                        '".$excel_data[0][3]."'
                          ),
                          (
                        '$a',
                        '".$excel_data[0][4]."'
                          ),
                          (
                        '$a',
                        '".$excel_data[0][5]."'
                          ),
                          (
                        '$a',
                        '".$excel_data[0][6]."'
                          ),(
                        '$a',
                        '".$excel_data[0][7]."'
                          ),";


        }



    }
    $voice_queery=substr($voice_queery, 0, -1);
    $voice_queery.=";";

    $db->setQuery($voice_queery);
    $db->execQuery();

//    foreach ($array_ as $a){
//        $c='';
//        foreach ($excel_data as $ex){
//
//            if($ex[0]!=$a[0]){
//                $c=$ex[0];
//            }
//        }
//        if($c!=''&&$ec==0){
//            $code_not_finde1.="არ დაემატა კოდები:";
//            $code_not_finde1.=$c.",";
//            $ec++;
//        }else if ($c!=''&& $ec>0){
//            $code_not_finde1.=$c.",";
//        }
//    }

    $date=date("Y-m-d H:i:s");
    $query="INSERT INTO
                        autocall_request_details  (
                        request_id,
                        set_time,
                        comment
                        )  
                        VALUES";

    $check=0;


    foreach ($ar as $a){
        $check++;

        if($check==count($ar)){
            $query .= "(
                        '$a',
                        NOW(),
                        'ექსელიდან ატვირთული'
                          );";

        }else{
            $query .= "(
                        '$a',
                        NOW(),
                        'ექსელიდან ატვირთული'
                          ),";

        }

    }


    $db->setQuery($query);
    $db->execQuery();

    $db->setQuery("select id,request_id from autocall_request_details order by id desc limit $check");
    $base_querry=$db->getResultArray();
    $res_base=$base_querry[result];
    if($check>0){

        $querry="INSERT INTO
                        autocall_request_base  (
                        request_id,
                        phone_number,
                        number_1,
                        number_2,
                        number_3,
                        number_4,
                        number_5,
                        number_6
                       
                        )  
                        VALUES";
        $count=0;
        while (sizeof($res_base)>$count){

            foreach ($array_ as $r){

                if($res_base[$count]['request_id']==$r[1]){


                    $querry .= "(
                        '".$res_base[$count]['id']."',
                        '$r[2]',
                        '$r[3]',
                        '$r[4]',
                        '$r[5]',
                        '$r[6]',
                        '$r[7]',
                        '$r[8]'
                          ),";


                }

            }
          $count++;

        }

        $querry=substr($querry, 0, -1);
        $querry.=";";

        $db->setQuery($querry);
        $db->execQuery();
    }


    return  $code_not_finde1;
}


function write_to_db_from_excel($request_id, $excel_data)
{
    global $db;
    $count = 0;
    $query = "INSERT INTO
                        autocall_request_base  (
                        request_id,
                        phone_number,
                        number_1,
                        number_2,
                        number_3,
                        number_4,
                        number_5,
                        number_6,
                        number_7,
                        number_8,
                        number_9,
                        number_10
                        
                        )  
                        VALUES";
    foreach ($excel_data as $r) {

        $count++;
        if ($count == 1 || $count == count($excel_data)) {

        } else if ($count == count($excel_data) - 1) {
            $query .= "(
                        '$request_id',
                        '$r[0]',
                        '$r[1]',
                        '$r[2]',
                        '$r[3]',
                        '$r[4]',
                        '$r[5]',
                        '$r[6]',
                        '$r[7]',
                        '$r[8]',
                        '$r[9]',
                        '$r[10]'
                          );";

        } else {
            $query .= "(
                        '$request_id',
                        '$r[0]',
                        '$r[1]',
                        '$r[2]',
                        '$r[3]',
                        '$r[4]',
                        '$r[5]',
                        '$r[6]',
                        '$r[7]',
                        '$r[8]',
                        '$r[9]',
                        '$r[10]'
                          ),";
        }

    }

    $db->setQuery($query);
    $db->execQuery();


}
function create_excel_code()
{


    $row_excel = array();
    $row_excel[0] = "CampaignCode";
    $row_excel[1] = "Mobile";
    $row_excel[2] = "PinCode";
    $row_excel[3] = "CustomerID";
    $row_excel[4] = "UserName";
    $row_excel[5] = "FullName";
    $row_excel[6] = "PID";
    $row_excel[7] = "Language";


    $file = fopen("outgoing_voice_code.xls", "w");


    fputs($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));

    fputcsv($file, $row_excel,chr(0xEF) . chr(0xBB) . chr(0xBF));
    fclose($file);


}


function create_excel($request_id)
{
    global $db;
    if (empty($_SESSION['last_id'])) {
        $db->setQuery("SELECT name from autocall_request_voice where request_id='$request_id' and actived='1' and voice_type_id <> '1'");

    } else {

        $db->setQuery("SELECT name
            from 
            autocall_request_voice
            where request_id= (SELECT MAX(request_id) FROM autocall_request_voice)
            and actived='1' 
            and voice_type_id <> '1'
         ");

    }
    $req=$db->getResultArray();
    $res=$req[result];

    $row_excel = array();
    $row_excel[0] = "ნომერი";
    $count = 1;
    while (sizeof($res)>=$count) {
        $row_excel[$count] = $res[$count-1]['name'];
        $count++;
    }

    $file = fopen("outgoing_voice.csv", "w");


    fputs($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));

    fputcsv($file, $row_excel);
    fclose($file);

}

function Get_Last_Id_Details($id)
{
    global  $db;
    $db->setQuery("SELECT MAX(id) id FROM autocall_request_details WHERE request_id=$id");
    $res=$db->getResultArray();
    $id=$res[result][0]['id'];
    return $id;
}
function Get_Last_Id_Request($tn)
{
    global  $db;
    $db->setQuery("SELECT MAX(request_id) id  FROM  " . $tn);
    $res=$db->getResultArray();
    $id = $res[result][0]['id'];
    return $id;
}
function Get_Last_Id($tn)
{
    global $db;
    $db->setQuery("SELECT MAX(id) id  FROM  " . $tn);
    $res=$db->getResultArray();
    $id=$res[result][0]['id'];
    return $id;
}

function disable_main_table($id, $tb_name)
{
    global $db;
    $db->setQuery(" UPDATE $tb_name	SET    actived = 0 WHERE  id = '$id' and request_status_id=0");

    if ( $db->execQuery()) {
        global $error;
        $error = 'ოპერაცია წარმატებით დასრულდა!';
    } else {
        global $error;
        $error = 'ოპერაცია წარუმატებლად დასრულდა!';
    }

}

function disable($id, $tb_name)
{
    global $db;
    $db->setQuery(" 
                        UPDATE $tb_name
					SET    actived = 0
					WHERE  id = '$id'");
    $db->execQuery();
}

function  Save_Position($request_position_id, $id, $position){
    global $db;
   $db->setQuery(" SELECT id, position
                        FROM autocall_request_positions
                        WHERE actived=1 and request_id='$id' and position='$position' and id <> '$request_position_id'
 
 
                      ");
   $res=$db->getResultArray();

    if (sizeof($res[result])>0) {
        global $error;
        $error = 'შეიყვანეთ უნიკალური რიგის ნომერი!';
    } else {
        $db->setQuery(" UPDATE 
                               autocall_request_positions
                             SET 
                               request_id= '$id',
                               position= '$position'
                             where 
                                id= $request_position_id
    
                      ");
        $db->execQuery();

    }





}

function Save_Week($request_week_id, $id, $start_time, $end_time, $week_day_id, &$week_error)
{
    global $db;

    $week_days_dates_start = array();
    $week_days_dates_start = week_days_dates_start($week_day_id, $id);

    $week_days_dates_end = array();
    $week_days_dates_end = week_days_dates_end($week_day_id, $id);

    $check = 1;

    for ($i = 0; $i < sizeof($week_days_dates_end); $i++) {
        if (($start_time < $week_days_dates_start[$i] && $end_time > $week_days_dates_start[$i]) || ($start_time >= $end_time) || ($start_time > $week_days_dates_start[$i] && $start_time < $week_days_dates_end[$i]) || ($end_time > $week_days_dates_start[$i] && $end_time < $week_days_dates_end[$i])) {

            $check = 0;
        }
    }

    if ($start_time >= $end_time) {
        $check = 0;
    }
    if ($check == 1) {

        $db->setQuery(" UPDATE 
                               autocall_request_dates
                             SET 
                               request_id= '$id',
                               start_time= '$start_time',
                               end_time= '$end_time',
                               week_day_id='$week_day_id'
                             where 
                                id= $request_week_id
    
                      ");
         $db->execQuery();
    } else {


        $week_error = 'შეცვალეთ ზარის პერიოდი!';


    }


}

function Save_Voice($request_voice_id, $request_table_id, $voice_type_id, $position, $voice_name, $file_name)
{
    global $db;
    $db->setQuery(" UPDATE 
                          autocall_request_voice
                         SET 
                           request_id= '$request_table_id',
                           voice_type_id= '$voice_type_id',
                           position= '$position',
                           name= '$voice_name',
                           value='$file_name'
                         where 
                            id= $request_voice_id
    
                      ");
    $db->execQuery();
}
function Save_Details($details_id,$id,$commen){

    global $db;
    $db->setQuery(" UPDATE 
                          autocall_request_details
                         SET 
                           request_id= '$id',
                           set_time=NOW(),
                          
                           comment= '$commen'
                         where 
                            id= $details_id
    
                      ");
    $db->execQuery();
}


function Save_Call($call_id, $id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10)
{
    global $db;
    $db->setQuery(" UPDATE 
                          autocall_request_base
                         SET 
                           request_id= '$id',
                           phone_number= '$phone_number',
                           number_1= '$number_1',
                           number_2= '$number_2',
                           number_3= '$number_3',
                           number_4= '$number_4',
                           number_5= '$number_5',
                           number_6= '$number_6',
                           number_7= '$number_7',
                           number_8= '$number_8',
                           number_9= '$number_9',
                           number_10= '$number_10'
                         where 
                            id= $call_id
    
                      ");
    $db->execQuery();
}

function Saveproduct($request_table_id, $name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity)
{
    global $db;



    $db->setQuery("	UPDATE 
	                        autocall_request
	                    SET 
	                        name ='$name',
	                        call_type_id='$call_type_id',
	                        start_datetime='$start_datetime',
	                        end_datetime='$end_datetime',
	                        
	                     
	                        retry_time='$retry_time',
	                        answer_wait_time='$answer_wait_time',
	                        duration='$duration',
	                        max_retries= '$max_retries',
	                        max_sum_listen_time='$max_sum_listen_time',
	                        comment_main='$comment_main',
	                        note='$note',
	                        code='$code',
	                        done_listen_time='$done_listen_time',
	                        channel_quantity='$channel_quantity'
	                    WHERE  
	                        id= $request_table_id
	
	");
    $db->execQuery();
    $db->setQuery("UPDATE autocall_request ar 
JOIN autocall_request_details ard on ard.request_id=ar.id
JOIN autocall_request_base arb on arb.request_id=ard.id
SET arb.actived_status=1
WHERE  arb.actived=1 and ard.actived=1 and ar.actived=1 and arb.number_8 + INTERVAL ar.duration DAY > NOW()");
$db->execQuery();
}

function week_days_dates_start($week_day_id, $id)
{
    global  $db;
    $week_data_start = array();

     $db->setQuery("SELECT start_time, end_time
                             FROM autocall_request_dates 
                             WHERE week_day_id='$week_day_id' and actived=1 and request_id='$id'");
    $res=$db->getResultArray();
    $count = 0;
    while (sizeof($res[result])>$count) {
        $week_data_start[$count] = $res[result][$count]['start_time'];

        $count++;
    }
    return $week_data_start;

}

function week_days_dates_end($week_day_id, $id)
{
    global  $db;
    $week_data_end = array();
    $db->setQuery("SELECT start_time, end_time
                             FROM autocall_request_dates 
                             WHERE week_day_id='$week_day_id' and actived=1 and request_id='$id'");
    $res=$db->getResultArray();
    $count = 0;
    while (sizeof($res[result])>$count) {

        $week_data_end[$count] = $res[result][$count]['end_time'];
        $count++;
    }
    return $week_data_end;
}

function Add_Position($id, $position){
    global $db;
    $db->setQuery(" SELECT id, position
                        FROM autocall_request_positions
                        WHERE actived=1 and request_id='$id' and position='$position'
 
 
                      ");
     $res=$db->getResultArray();
    if (sizeof($res[result]) > 0) {
        global $error;
        $error = 'შეიყვანეთ უნიკალური რიგის ნომერი!';
    }else{
        $db->setQuery(" 
                        INSERT INTO
                        autocall_request_positions  (
                        request_id,
                        position
                       
                        )  
                        VALUES (
                        '$id',
                        '$position'
                        
                        )
    ");
        $db->execQuery();

    }


}

function Add_Week($id, $start_time, $end_time, $week_day_id, &$week_error)
{
    global  $db;
    $week_days_dates_start = array();
    $week_days_dates_start = week_days_dates_start($week_day_id, $id);

    $week_days_dates_end = array();
    $week_days_dates_end = week_days_dates_end($week_day_id, $id);

    $check = 1;

    for ($i = 0; $i < sizeof($week_days_dates_end); $i++) {
        if (($start_time < $week_days_dates_start[$i] && $end_time > $week_days_dates_start[$i]) || ($start_time >= $end_time) || ($start_time > $week_days_dates_start[$i] && $start_time < $week_days_dates_end[$i]) || ($end_time > $week_days_dates_start[$i] && $end_time < $week_days_dates_end[$i])) {

            $check = 0;
        }
    }
    if ($start_time >= $end_time) {
        $check = 0;
    }


    if ($check == 1) {
        $db->setQuery(" 
                        INSERT INTO
                        autocall_request_dates  (
                        request_id,
                        start_time,
                        end_time,
                        week_day_id
                       
                        )  
                        VALUES (
                        '$id',
                        '$start_time',
                        '$end_time',
                        '$week_day_id'
                        )
    ");
        $db->execQuery();

    } else {

        $week_error = 'არასწორი ზარის პერიოდი!';

    }


}

function Add_Voice($request_table_id, $voice_type_id, $position, $voice_name, $file_name)
{
    global  $db;

    $db->setQuery(" 
                        INSERT INTO
                        autocall_request_voice  (
                        request_id,
                        voice_type_id,
                        position,
                        name,
                        value
                        )  
                        VALUES (
                        '$request_table_id',
                        '$voice_type_id',
                        '$position',
                        '$voice_name',
                        '$file_name'
                        )
    ");
    $db->execQuery();
}

function Add_Details($id,$comment,$details_id){
    global  $db;
    $date=date("Y-m-d H:i:s");

    $db->setQuery("
    SELECT id FROM autocall_request_details WHERE id='$details_id';
    ");
    $res=$db->getResultArray();
    if(sizeof($res[result])>0){
        $db->setQuery(" UPDATE 
                          autocall_request_details
                         SET 
                           request_id= '$id',
                           set_time=NOW(),
                          
                           comment= '$comment'
                         where 
                            id= $details_id
    
                      ");
        $db->execQuery();
    }else{

        $db->setQuery(" 
                        INSERT INTO
                        autocall_request_details  (
                        request_id,
                        set_time,
                        comment
                        )  
                        VALUES (
                        '$id',
                         NOW(),
                        '$comment'
                       
                        )
    ");
        $db->execQuery();
    }




}

function Add_Call($id, $phone_number, $number_1, $number_2, $number_3, $number_4, $number_5, $number_6, $number_7, $number_8, $number_9, $number_10)
{
    global  $db;
    $db->setQuery(" 
                        INSERT INTO
                        autocall_request_base  (
                        request_id,
                        phone_number,
                        number_1,
                        number_2,
                        number_3,
                        number_4,
                        number_5,
                        number_6,
                        number_7,
                        number_8,
                        number_9,
                        number_10
                        )  
                        VALUES (
                        '$id',
                        '$phone_number',
                        '$number_1',
                        '$number_2',
                        '$number_3',
                        '$number_4',
                        '$number_5',
                        '$number_6',
                        '$number_7',
                        '$number_8',
                        '$number_9',
                        '$number_10'
                        )
    ");
    $db->execQuery();
}

function Addproduct($name, $call_type_id, $start_datetime, $end_datetime, $retry_time, $answer_wait_time,$duration, $max_retries,$comment_main,$note, $max_sum_listen_time,$code, $done_listen_time, $channel_quantity)
{
    global  $db;
    $db->setQuery("INSERT INTO  
                        autocall_request (
                                          name, 
                                          call_type_id, 
                                          start_datetime,
                                           end_datetime,
                                           
                                            
                                             retry_time, 
                                             answer_wait_time, 
                                             duration, 
                                             max_retries, 
                                             max_sum_listen_time,
                                             comment_main,
                                             note,
                                             code,
                                              done_listen_time, 
                                              channel_quantity, 
                                              actived
                                              ) 
                                              VALUES  (
                                              '$name', 
                                              '$call_type_id',
                                               '$start_datetime',
                                                '$end_datetime', 
                                                
                                                
                                                 '$retry_time', 
                                                 '$answer_wait_time', 
                                                 '$duration', 
                                                 '$max_retries',
                                                  '$max_sum_listen_time',
                                                  '$comment_main',
                                                  '$note',
                                                  '$code',
                                                   '$done_listen_time',
                                                   '$channel_quantity',
                                                   '1')");
    $db->execQuery();
}

function get_week_days($week_day_id)
{
    global  $db;

    $data = '';

    $db->setQuery("SELECT id, week_day 
                               from autocall_request_weekdays ");
     $res=$db->getResultArray();
    $data .= '<option value="0" selected="selected">----</option>';
    $count=0;
    while (sizeof($res[result])>$count) {
        if ($res[result][$count]['id'] == $week_day_id) {
            $data .= '<option value="' . $res[result][$count]['id'] . '" selected="selected">' . $res[result][$count]['week_day'] . '</option>';
        } else {
            $data .= '<option value="' . $res[result][$count]['id'] . '">' . $res[result][$count]['week_day'] . '</option>';
        }
        $count++;
    }

    return $data;

}

function get_nick($nick_id)
{
    global  $db;
    $data = '';

     $db->setQuery("SELECT id, name 
                               from autocall_call_type ");
     $res=$db->getResultArray();
     $count=0;
    $data .= '<option value="0" selected="selected">----</option>';
    while (sizeof($res[result])>$count) {
        if ($res[result][$count]['id'] == $nick_id) {
            $data .= '<option value="' . $res[result][$count]['id'] . '" selected="selected">' . $res[result][$count]['name'] . '</option>';
        } else if($nick_id!=4) {
            $data .= '<option value="' . $res[result][$count]['id'] . '">' . $res[result][$count]['name'] . '</option>';
        }
        $count++;
    }

    return $data;
}

function get_voice_type_name($voice_type_id)
{
     global $db;
    $data = '';

    $db->setQuery("SELECT id, name 
                               from autocall_voice_type ");
    $res=$db->getResultArray();
    $count=0;

    $data .= '<option value="0" selected="selected">----</option>';
    while (sizeof($res[result])>$count) {
        if ($res['id'] == $voice_type_id) {
            $data .= '<option value="' . $res[result][$count]['id'] . '" selected="selected">' . $res[result][$count]['name'] . '</option>';
        } else {
            $data .= '<option value="' . $res[result][$count]['id'] . '">' . $res[result][$count]['name'] . '</option>';
        }
        $count++;
    }

    return $data;
}

function get_num_sum($request_id)
{
    global  $db;
    $db->setQuery("   SELECT  name

                                 from autocall_request_voice
                                                                                        
                                 WHERE request_id=$request_id
                                                                                        
                                 and actived=1
                                 and voice_type_id <> 1
                                 ORDER BY position
                                                    ");
     $req=$db->getResultArray();

     return $req;
}

function get_call_table1($request_id)
{
    $data = '';
    $res = get_num_sum($request_id);
    $data1 = '';
    $count=0;

    while (sizeof($res[result])>$count) {

//        $data1 .= '<th>' . $res['name'] . '</th>';
        $data1 .= ' <th>
                    	' . $res[result][$count]['name'] . '
                    </th>';
    $count++;
    }

//    $data .= '
//                <th ></th>
//                <th>ნომრები</th>
//                ' . $data1 . '
//                <th class="check">#</th>';


    $data .= '<th class="colum_hidden sorting_disabled ui-state-default" rowspan="1" colspan="1" aria-label="">
                	
                </th>
<th class="sorting ui-state-default sorting_desc" tabindex="0" aria-controls="example_call" rowspan="1" colspan="1" aria-label="ნომრები: activate to sort column ascending" aria-sort="descending">
	ნომერი
		
</th>
 ' . $data1 . '
<th class="check">
	
</th>
              
    ';


    return $data;
}

function get_call_table2($request_id)
{
    $data = '';
    $req = get_num_sum($request_id);
    $data2 = '';
    $count=0;
    while (sizeof($req[result])>$count) {

        $data2 .= '<th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>';
        $count++;
    }


    $data .= '
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               ' . $data2 . '
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-call" name="check-all-call"/>
                        <label for="check-all-call"></label>
                    </div>
                </th>
            ';


    return $data;
}

function get_numbers($id, $voice_req)
{
    global $db;
    $data = '';
    $db->setQuery("SELECT 
                                  number_1,
                                  number_2,
                                  number_3,
                                  number_4,
                                  number_5,
                                  number_6,
                                  number_7,
                                  number_8,
                                  number_9,
                                  number_10
                              from 
                                 autocall_request_base
                              where 
                                  id='$id'   
                                  ");

    $res = $db->getResultArray();

    $count = 1;
    while (sizeof($voice_req[result])>=$count) {


        $data .= ' 
               <tr>
                
                 <td>' . $voice_req[result][$count-1]['name'] . '</td>
                   
              </tr>
                <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="number_' . $count . '"   value="' . $res[result][$count-1]['number_' . $count] . '" onchange="input_number_type_check(this.value,this.id)" />
					</td>
					
				  </tr>
				  
				  ';
        $count++;

    }


    return $data;


}

function Getproduct($product_id, $tn)
{
   global  $db;

    $db->setQuery("   SELECT  * 
                                                    from $tn
                                                    where id=$product_id 
                                                    
                                                    ");
    $res=$db->getResultArray();

    return $res[result][0];
}

function GetPage_for_edit_position($res){

    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    <table class="dialog-form-table" style="width: 100%;">
    
               
                <tr>
                
                    <td> რიგის ნომერი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 100%;" id="position_position"   value="'.$res['position'] .'" onchange="input_type_check(this.value,this.id)" />
					</td>
										
				</tr>
			
				
				
	
   </table>	
   </fieldset>
   <input type="hidden" id="request_position_id" value=' . $res['id'] . ' />
   
        </div>
        <script>
        var  position_position =document.getElementById("position_position").value;
        
       </script>
        
        
        ';
    return $data;

}


function GetPage_for_edit_week($res)
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    
    <table class="dialog-form-table" style="width: 100%;">
               
                <tr>
                   
                   <td>
               კვირის დღე
                   </td>
               
                </tr>
                <tr>
                    <td>
                     <select id="week_day_id" class="idle" style="width: 100%"  readonly>' . get_week_days($res['week_day_id']) . '</select>
                    </td>
                </tr>
                
			    <tr>
                
                    <td> ზარის პერიოდის დაწყების დრო </td>
                   
                </tr>
                <tr>
					<td>
						<input class="idle" type="time" style="width: 100%;" id="start_time"   value="' . $res['start_time'] . '"  />
					</td>
					
				</tr>
                <tr>
                
                    <td> ზარის პერიოდ დასრულების დრო</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="time" style="width: 100%;" id="end_time"   value="' . $res['end_time'] . '"  />
					</td>
										
				</tr>
				
				
	
   </table>	
   </fieldset>
    <input type="hidden" id="request_week_id" value=' . $res['id'] . ' />	
       <script>
   
   $(document).ready(function(){
    $("#start_time").timepicker({ timeFormat: "HH:mm:ss" });
    $("#end_time").timepicker({ timeFormat: "HH:mm:ss" });
});
    
</script>
      
        </div>';
    return $data;
}

function GetPage_for_edit_voice($res)
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    <table class="dialog-form-table" style="width: 100%;">
                <tr>
                
                    <td> დასახელება </td>
                   
                </tr>
                <tr>
					<td>
						<input class="idle" type="text" style="width: 100%;" id="voice_name"   value="' . $res['name'] . '"  />
					</td>
					
				</tr>
                <tr>
                
                    <td> ტიპი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<select onchange="change_input()" id="voice_type_id" class="idle" style="width: 100%"  readonly>' . get_voice_type_name($res['voice_type_id']) . '</select>
					</td>
										
				</tr>
				<tr>
                
                    <td> პოზიცია </td>
                   
                </tr>
                <tr>
					<td>
						<input class="idle" type="text" style="width: 100%;" id="position"   value="' . $res['position'] . '"  onchange="position_check(this.value)" />
					</td>
					
				</tr>
				
               
				<tr id="tr_voice_file">
                
                    <td> ფაილი</td>
                   
                </tr>
                <tr id="tr_voice_file_drop_box">
                   <td>
                      
                      <div id="voice_audio" style=" width: 100%; border: #42B4E6; border-width: 1px; border-style: solid; padding: 5px"> 
                        <div><div  style="padding-bottom: 5px;" ><span id="file_drop">' . $res['value'] . ' </span><button style="float: right;  text-align: left; cursor: pointer;" id="delete_file"><span class="ui-icon ui-icon-close"></span></button>
                         </div>
                        
                        <audio id="myAudio" controls>
                          <source src="media/upload_voice/' . $res['value'] . '" type="audio/ogg">
                          <source src="media/upload_voice/' . $res['value'] . '" type="audio/mpeg">
                           Your browser does not support the audio element.
                        </audio>
                        
                       </div>
                      
                   </td>
                </tr>
				<tr id="tr_voice_file_input">
				     <td>
					   <button id="voice_file_up" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" style="width: 100%" > 
					   <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
					   <span class="ui-button-text">ფაილის ატვირთვა</span>
					   </button>
                     </td>
                </tr>
				
				
	
   </table>	
   </fieldset>
    <input type="hidden" id="request_voice_id" value=' . $res['id'] . ' />	
        <script>
        



        var first_position=  "' . $res['position'] . '";
        
        var x = document.getElementById("myAudio");
        function enableControls() { 
          x.controls = true;
          x.load();
        } 
        
         var c=document.getElementById("call_type_id").value;
                   var x=document.getElementById("voice_type_id");
                   
            
                    if(c==3){
                       
                        x.remove(4);
                        x.remove(3);
                        x.remove(2);
                        x.remove(1);
                        
                    }else {
                      
                       
                      
                        x.remove(5);
                        
                      
            
                    }
        
   function position_check(val){
       var a = parseInt(val)
       if(!Number.isInteger(a)){
           alert("შეიტანეთ მხოლოდ რიცხვი!");
           document.getElementById("position").value=first_position;
       } else  if(first_position>val){
           alert(`"შეტანილი პოზიცია უკვე არსებობს ! შეიტანეთ  ${first_position-1}-ზე მეტი რიცხვი "`);
           document.getElementById("position").value=first_position;
       }
       
   }
        var x = document.getElementById("voice_type_id").value;
        if(x=="1"){
                 document.getElementById("tr_voice_file_drop_box").style.visibility="visible";
                 document.getElementById("tr_voice_file_input").style.visibility = "visible";
                 document.getElementById("tr_voice_file").style.visibility = "visible";
              }else{
                  document.getElementById("tr_voice_file_drop_box").style.visibility="hidden";
                 document.getElementById("tr_voice_file_input").style.visibility = "hidden";
                 document.getElementById("tr_voice_file").style.visibility = "hidden";
              }
            function change_input() {
              var x = document.getElementById("voice_type_id").value;
              if(x=="1"){
                 document.getElementById("tr_voice_file_drop_box").style.visibility="visible";
                 document.getElementById("tr_voice_file_input").style.visibility = "visible";
                 document.getElementById("tr_voice_file").style.visibility = "visible";
              }else{
                  document.getElementById("tr_voice_file_drop_box").style.visibility="hidden";
                 document.getElementById("tr_voice_file_input").style.visibility = "hidden";
                 document.getElementById("tr_voice_file").style.visibility = "hidden";
              }
            }
       </script>
        </div>';
    return $data;
}
function get_phone_number($id){
    global  $db;
    $db->setQuery("
    SELECT phone_number FROM autocall_request_base WHERE id=$id
    
    ");
    $res=$db->getResultArray();
    $phone_number = $res[result][0]['phone_number'];

    return $phone_number;

}

function GetPage_for_edit_call($res,$res_base)
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px" >
    <table class="dialog-form-table" style="width: 100%;">
                <tr>
                
                    <td> ნომერი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="phone_number"   value="' .get_phone_number($_REQUEST['id']). '" onchange="input_type_check(this.value,this.id)" />
					</td>
					
				</tr>';

    $data .= get_numbers($_REQUEST['id'], get_num_sum($res['request_id']));

    if($_REQUEST['details_id']>0){
        $data.='
        <tr>
                
                    <td> კომენტარი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="comment"   value="'.get_comment($_REQUEST['details_id']).'"  />
					</td>
					
				</tr>
				
        ';
    }else{
        $data.='
        <tr>
                
                    <td> კომენტარი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="comment"   value=""  />
					</td>
					
				</tr>
					
        ';


    }


    $data .= '</table>	
   </fieldset>
   <input type="hidden" id="request_details_id" value=' . $res_base['id'] . ' />
        </div>
        <script>
        
        var phone_number=document.getElementById("phone_number").value;
</script>
        
        ';
    return $data;
}

function get_position($request_id)
{
    global $db;
    $db->setQuery("SELECT case WHEN MAX(position) THEN MAX(position) ELSE 0 END id FROM autocall_request_voice where request_id=$request_id and actived=1");
    $res=$db->getResultArray();
    $id = $res[result][0]['id'];
    return $id + 1;
}

function GetPage_for_file($res){
    $data = '';

    if($res['file_type']=='ვიდეო'){
        $data .= '
  <div id="dialog-form">

<div id="" >
<fieldset style="height: 250px">

<legend>ვიდეო ფაილი</legend>
<video width="350" controls>
  <source src="media/auto_dialer_all_file/'.$res['file_name'].'" type="video/mp4">
  <source src="media/auto_dialer_all_file/'.$res['file_name'].'" type="video/ogg">
  Your browser does not support HTML5 video.
</video>
   
</fieldset>
  <input type="hidden" id="file_id" value="'.$res['id'].'" />	
</div>

</div>
        ';
    }else if($res['file_type']=='აუდიო'){
        $data .= '
  <div id="dialog-form">

<div id="" >
<fieldset>

<legend>აუდიო ფაილი</legend>

<audio controls>
  <source src="media/auto_dialer_all_file/'.$res['file_name'].'" type="audio/ogg">
  <source src="media/auto_dialer_all_file/'.$res['file_name'].'" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
   
</fieldset>
  <input type="hidden" id="file_id" value="'.$res['id'].'" />	
</div>

</div>
        ';
    }else if($res['file_type']=='სურათი'){
        $data .= '
  <div id="dialog-form">

<div id="img" >
<fieldset style="text-align: center">

<legend>სურათი</legend>

<img style="text-align: center" src="media/auto_dialer_all_file/'.$res['file_name'].'" alt="'.$res['file_name'].'" width="" height="">
   
</fieldset>
  <input type="hidden" id="file_id" value="'.$res['id'].'" />	
</div>

</div>
        ';
    } else{
        $data .= '
<style>
.container1 {
  position: relative;
 
  margin-left: 80px;
  margin-right: 80px;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0.2;
  transition: .5s ease;
  background-color: black;
}

.container1:hover .overlay {
  opacity: 0.6;
}

.text {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}
</style>

  <div id="dialog-form">

<div id="" >
<fieldset style="text-align: center">

<legend>ფაილი</legend>
<div class="container1">
    <a href="media/auto_dialer_all_file/'.$res['file_name'].'" download>
    
      <img src="media/images/icons/folder.png" alt="'.$res['file_name'].'" width="104" height="142">
      <div class="overlay">
      <div class="text"><img src="media/images/icons/file.png" alt="'.$res['file_name'].'" width="50" height="54"></div>
      </div>
    
    </a>
  </div>
    

</fieldset>
  <input type="hidden" id="file_id" value="'.$res['id'].'" />	
</div>

</div>
        ';

    }



    return $data;
}

function GetPage_for_edit_base($res){
    $data = '';
    $data .= '
  <div id="dialog-form">

<div id="" >
<fieldset style="height: 300px">

<legend>ბაზის ატვირთვა</legend>
   <div class="input_fild">
            <div id="button_area" style=" margin-top: 10px">
                <button id="add_button_base">დამატება</button>
                
                <button id="delete_button_base">წაშლა</button>
                <button id="exp_button_call" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
                 <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-s"> </span>
                <span class="ui-button-text">შაბლონის ჩამოტვირთვა</span>
                </button>
               <input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
                 <button id="up_button_call" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
				<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
				<span class="ui-button-text">ფაილის ატვირთვა</span>
				</button>
				
				
            </div>
         <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_call" style="margin-right: 10px; width: 100%;">
            <thead >
               <tr id="datatable_header_call1">';

    $data .= get_call_table1($res['request_id']);

    $data .= ' </tr>
            </thead>
            <thead >
            <tr id="datatable_header_call2" class="search_header">';

    $data .= get_call_table2($res['request_id']);

    $data .= ' </tr>
            </thead>
                
        </table>
        </div>
 
</div>
</fieldset>
  <input type="hidden" id="details_id" value="'.$res['id'].'" />	
</div>

</div>
        ';
    return $data;
}


function GetPage_for_base($id){
    $data = '';
    $data .= '
  <div id="dialog-form">

<div id="" >
<fieldset style="height: 300px">

<legend>ბაზის ატვირთვა</legend>
   <div class="input_fild">
            <div id="button_area" style=" margin-top: 10px">
                <button id="add_button_base">დამატება</button>
                
                <button id="delete_button_base">წაშლა</button>
                <button id="exp_button_call" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
                 <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-s"> </span>
                <span class="ui-button-text">შაბლონის ჩამოტვირთვა</span>
                </button>
               <input id="choose_file" type="file" name="choose_file" class="input" style="display: none;">
                 <button id="up_button_call" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
				<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
				<span class="ui-button-text">ფაილის ატვირთვა</span>
				</button>
				
				
            </div>
         <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_call" style="margin-right: 10px; width: 100%;">
            <thead >
               <tr id="datatable_header_call1">';

    $data .= get_call_table1($id);

    $data .= ' </tr>
            </thead>
            <thead >
            <tr id="datatable_header_call2" class="search_header">';

    $data .= get_call_table2($id);

    $last_id=Get_Last_Id("autocall_request_details")+1;

    $data .= ' </tr>
            </thead>
                
        </table>
        </div>
 
</div>
</fieldset>
<input type="hidden" id="details_id" value="'.$last_id.'" />
</div>

</div>
        ';
    return $data;
}

function GetPage_for_add_position()
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    <table class="dialog-form-table" style="width: 100%;">
    
               
                <tr>
                
                    <td> რიგის ნომერი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 100%;" id="position_position"   value="" onchange="input_type_check(this.value,this.id)"  />
					</td>
										
				</tr>
			
				
				
	
   </table>	
   </fieldset>
   
        </div>
        
        <script>
        
         var position_position=document.getElementById("position_position").value;
</script>
        
        ';
    return $data;

}


function GetPage_for_add_week()
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    <table class="dialog-form-table" style="width: 100%;">
    
                <tr>
                   
                   <td>
               კვირის დღე
                   </td>
               
                </tr>
                <tr>
                    <td>
                     <select id="week_day_id" class="idle" style="width: 100%"  readonly>' . get_week_days("") . '</select>
                    </td>
                </tr>
                <tr>
                
                    <td> ზარის პერიოდის დაწყების დრო </td>
                   
                </tr>
                <tr>
					<td>
						<input class="idle" type="time" style="width: 100%;" id="start_time"   value=""  />
					</td>
					
				</tr>
                <tr>
                
                    <td> ზარის პერიოდ დასრულების დრო</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="time" style="width: 100%;" id="end_time"   value=""  />
					</td>
										
				</tr>
			
				
				
	
   </table>	
   </fieldset>
   <script>
   
   $(document).ready(function(){
    $("#start_time").timepicker({ timeFormat: "HH:mm:ss" });
    $("#end_time").timepicker({ timeFormat: "HH:mm:ss" });
});
    
</script>
   
        </div>';
    return $data;

}

function GetPage_for_add_voice($request_id)
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    <table class="dialog-form-table" style="width: 100%;">
                <tr>
                
                    <td> დასახელება </td>
                   
                </tr>
                <tr>
					<td>
						<input class="idle" type="text" style="width: 100%;" id="voice_name"   value=""  />
					</td>
					
				</tr>
                <tr>
                
                    <td> ტიპი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<select onchange="change_input()" id="voice_type_id" class="idle" style="width: 100%"  readonly>' . get_voice_type_name("") . '</select>
					</td>
										
				</tr>
				<tr>
                
                    <td> პოზიცია </td>
                   
                </tr>
                <tr>
					<td>
						<input class="idle" type="text" style="width: 100%;" id="position"    value="' . get_position($request_id) . '"  onchange="position_check(this.value)" />
					</td>
					
				</tr>
				
				<tr id="tr_voice_file">
                
                    <td> ფაილი</td>
                   
                </tr>
                <tr id="tr_voice_file_drop_box">
                   <td>
                      
                      <div id="voice_audio" style=" width: 100%; border: #42B4E6; border-width: 1px; border-style: solid; padding: 5px"> 
                        <div><div  style="padding-bottom: 5px;" ><span id="file_drop">ფაილი არ არის არჩეული! </span><button style="float: right; text-align: left; cursor: pointer;" id="delete_file"><span class="ui-icon ui-icon-close"></span></button>
                         </div>
                        
                        <audio id="myAudio" controls>
                          <source src="media/upload_voice/" type="audio/ogg">
                          <source src="media/upload_voice/" type="audio/mpeg">
                           Your browser does not support the audio element.
                        </audio>
                        
                       </div>
                      
                   </td>
                </tr>
				<tr id="tr_voice_file_input">
				     <td>
					   <button id="voice_file_up" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" style="width: 100%" > 
					   <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
					   <span class="ui-button-text">ფაილის ატვირთვა</span>
					   </button>
                     </td>
                </tr>
				
				
	
   </table>	
   </fieldset>
   <script>
   var first_position=  "' . get_position($request_id) . '";
   
    var c=document.getElementById("call_type_id").value;
                   var x=document.getElementById("voice_type_id");
                   
            
                    if(c==3){
                       
                        x.remove(4);
                        x.remove(3);
                        x.remove(2);
                        x.remove(1);
                        
                    }else {
                      
                       
                      
                        x.remove(5);
                        
                      
            
                    }
   
   function position_check(val){
       var a = parseInt(val)
       if(!Number.isInteger(a)){
           alert("შეიტანეთ მხოლოდ რიცხვი!");
           document.getElementById("position").value=first_position;
       } else  if(first_position>val){
           alert(`"შეტანილი პოზიცია უკვე არსებობს ! შეიტანეთ  ${first_position-1}-ზე მეტი რიცხვი "`);
           document.getElementById("position").value=first_position;
       }
       
   }
        var x = document.getElementById("voice_type_id").value;
        if(x=="1"){
                 document.getElementById("tr_voice_file_drop_box").style.visibility="visible";
                 document.getElementById("tr_voice_file_input").style.visibility = "visible";
                 document.getElementById("tr_voice_file").style.visibility = "visible";
              }else{
                  document.getElementById("tr_voice_file_drop_box").style.visibility="hidden";
                 document.getElementById("tr_voice_file_input").style.visibility = "hidden";
                 document.getElementById("tr_voice_file").style.visibility = "hidden";
              }
            function change_input() {
              var x = document.getElementById("voice_type_id").value;
              if(x=="1"){
                document.getElementById("tr_voice_file_drop_box").style.visibility="visible";
                 document.getElementById("tr_voice_file_input").style.visibility = "visible";
                 document.getElementById("tr_voice_file").style.visibility = "visible";
              }else{
                 document.getElementById("tr_voice_file_drop_box").style.visibility="hidden";
                 document.getElementById("tr_voice_file_input").style.visibility = "hidden";
                 document.getElementById("tr_voice_file").style.visibility = "hidden";
              }
            }
       </script>
        </div>';
    return $data;
}

function get_comment($id){
    global $db;
    $db->setQuery("
    SELECT comment FROM autocall_request_details WHERE id='$id';
    ");
    $res=$db->getResultArray();
    $details_id = $res[result][0]['comment'];
    return $details_id;
}

function GetPage_for_add_call()
{
    $data = '';
    $data .= '
  <div id="dialog-form">
    <fieldset style="border:none; margin-right: 1px">
    <table class="dialog-form-table" style="width: 100%;">
    
                <tr>
                
                    <td> ნომერი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="phone_number"   value="" onchange="input_type_check(this.value,this.id)" />
					</td>
					
				</tr>';
    if (!empty($_REQUEST['add_base_id'])) {
        $data .= get_numbers("", get_num_sum($_REQUEST['add_base_id']));
    } else {
        $data .= get_numbers("", get_num_sum(Get_Last_Id('autocall_request')));
    }
    if($_REQUEST['details_id']>0){
        $data.='
        <tr>
                
                    <td> კომენტარი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="comment"   value="'.get_comment($_REQUEST['details_id']).'"  />
					</td>
					
				</tr>
				
        ';
    }else{
        $data.='
        <tr>
                
                    <td> კომენტარი</td>
                   
                </tr>
			
			    <tr>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="comment"   value=""  />
					</td>
					
				</tr>
					
        ';


    }

    $data .= '</table>	
   </fieldset>
   
        </div>
        <script>
        var phone_number=document.getElementById("phone_number").value;
        
</script>
        ';
    return $data;
}


function GetPage_for_add()
{
    $_SESSION['last_id'] = '';
    $data = '';

    $data .= '
<style>

.tab {
    float: left;
    background-color: #f1f1f1;
    /* height: 379px; */
    margin-top: 38px;
    position: absolute;
    border: 1px solid #000;
    border-left: 0;
}

.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 10px 15px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  font-size: 17px;
  height: auto;
}


.tab button:hover {
  background-color: #ddd;
}


.tab button.active {
    background-color: #ffff;
    border-bottom: 1px solid #000;
    border-top: 1px solid #000;
    margin-left: 1px;
}

.tabcontent {

    margin-left: 51px;
 
  height: 450px;
 
}
.input_fild {

}


.tooltip {
  position: relative;
  display: inline-block;
  opacity: 1
  
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 150px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;

  left: 50%;
  margin-left: 16px;
  opacity: 1;
  transition: opacity 0.3s;
}



.tooltip:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}
.tooltiptext{
position: fixed;
}

#add-edit-form fieldset {
    padding: 0px 3px 3px 12px;
}

</style>

';

    $data .= '
	
	    
			<div class="tab">
  <button class="tablinks tooltip" style="border-top: 0" onclick="openCity(event, \'request\')" id="defaultOpen"><img src="media/images/icons/auto_dialer_home.png" height="20px" width="15px"><span class="tooltiptext">ძირითადი ინფორმაცია</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'week_time\')" id="week_bt"><img src="media/images/icons/auto_dialer_periods.png" height="20px" width="20px"><span class="tooltiptext">ზარის პერიოდები</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'voice\')" id="voice_bt"><img src="media/images/icons/auto_dialer_record.png" height="20px" width="20px"><span class="tooltiptext">ჩანაწერები</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'detail\')" id="detail_bt"><img src="media/images/icons/auto_dialer_file.png" height="20px" width="20px"><span class="tooltiptext">ბაზის ატვირთვა</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'positions\')" id="position_bt"><img src="media/images/icons/auto_dialer_operator.png" height="20px" width="20px"><span class="tooltiptext">ოპერატორების ჯგუფი</span></button>
  <button class="tablinks tooltip" style="border-bottom: 0" onclick="openCity(event, \'all_file\')" id="file_bt"><img src="media/images/icons/auto_dialer_all_file.png" height="20px" width="20px"><span class="tooltiptext">ფაილები</span></button>
</div>


<div id="all_file" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 450px">

<legend>ფაილები</legend>
   <div class="input_fild">
            <div id="button_area" style=" margin-top: 10px">
            <table>
            <tbody>
            <tr>
            
            <td><button id="delete_button_all_file">წაშლა</button></td>
            <td><button id="all_file_up" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" style="width: 100%" > 
					   <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
					   <span class="ui-button-text">ფაილის ატვირთვა</span>
			    </button>
			</td>
			<td> <input id="choose_all_file" type="file" name="choose_all_file" class="input" style="display: none;"></td>
            
            </tr>
            
            </tbody>
            
            </table>
               
                
                 
               
                
            </div>
         <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_all_file" style="margin-right: 10px; width: 100%">
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 60%">ფაილის სახელი</th>
                <th style="width: 40%">ტიპი</th>
               
                
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-file" name="check-all-file"/>
                        <label for="check-all-file"></label>
                    </div>
                </th>
            </tr>
            </thead>
           
        </table>
        </div>
 
</div>
</fieldset>
</div>


</div>


<div id="request" style="margin-left: 50px;" class="tabcontent">

<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 450px">
<legend>ძირითადი ინფორმაცია</legend>
  <table class="dialog-form-table" style="width: 100%;">
                <tr>
                
                <td>ტიპი</td>
                    <td> დასახელება</td>
                     <td>კოდი</td>
                    
                   
                   
                
               </tr>
			
			    <tr>
			    <td>
						<select onchange="change_input_tab(this.value)"  id="call_type_id" class="idle" style="width: 95%"  readonly>' . get_nick("") . '</select>
					</td>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="name" class="idle"  value=""  />
					</td>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="code" class="idle"  value="" onchange="input_type_check(this.value,this.id)" />
				    </td>
					
					
					
				</tr>
				
				<tr>
				    <td>დაწყების თარიღი</td>
                     <td>დასრულების თარიღი</td>
                    <td>ხანგრძლივობა</td>
                    
                    
                
               </tr>
                <tr id="tr_1">
                    <td>
						<input type="text" class="idle" style="width: 95%;" id="start_datetime" class="idle"  value=""  />
					</td>
                    <td>
						<input type="text" class="idle" style="width: 95%;" id="end_datetime" class="idle"  value=""  />
					</td>
					 <td>
						<input type="text" class="idle" style="width: 95%;" id="duration" class="idle"  value=""  />
					</td>
					
                    
					
					
				</tr>
				
				<tr id="tr_2">
				    <td id="td_2"> ლოდინის დრო (წამი)</td>
                    <td id="td_3">მაქსიმალური მცდელობების რაოდენობა</td>
                    <td id="td_4">მაქსიმალური ჯამური ხანგძლივობა (წამი)</td>
                    
                    
                </tr>
                
                <tr id="tr_3">
                       <td>
						<input class="idle" type="text" style="width: 95%;" id="max_retries" class="idle"  value=""  onchange="input_type_check(this.value,this.id)" />
				       </td>
                       <td>
						<input class="idle" type="text" style="width: 95%;" id="retry_time" class="idle"  value="" onchange="input_type_check(this.value,this.id)"  />
					   </td>
					   
					   <td>
						<input class="idle" type="text" style="width: 95%;" id="done_listen_time" class="idle"  value="" onchange="input_type_check(this.value,this.id)" />
					   </td>
					   
                
                </tr>
                <tr id="tr_4">
                    <td id="td_1"> განმეორებითი ზარის შუალედი (წამი)</td>
				    <td id="td_5">სრულად მოსმენის დრო (წამი)</td>
                    <td id="td_6">ოპერატორების/არხების რ-ბა</td>
                    
				
				
                </tr>
                
                <tr id="tr_5">
                    <td>
						<input class="idle" type="text" style="width: 95%;" id="answer_wait_time" class="idle"  value=""  onchange="input_type_check(this.value,this.id)" />
				    </td>
                      <td>
						<input class="idle" type="text" style="width: 95%;" id="channel_quantity" class="idle"  value=""  onchange="input_type_check(this.value,this.id)" />
					   </td>
					   <td>
						<input class="idle" type="text" style="width: 95%;" id="max_sum_listen_time" class="idle"  value="" onchange="input_type_check(this.value,this.id)" />
					   </td>
					   
					  
                
                </tr>
                <tr>
                       <td>კომენტარი</td>                             
                </tr>
                <tr>
                  <td colspan="3">
                  <textarea id="comment_main" style=" resize: vertical;width: 98.5%; height: 50px;"> </textarea>
                  </td>
                </tr>
                <tr>
                       <td>შენიშვნა</td>                             
                </tr>
                <tr>
                  <td colspan="3">
                  <textarea id="note" style=" resize: vertical;width: 98.5%; height: 50px;"> </textarea>
                  </td>
                </tr>
               

			</table>
			
		
		
		
	</fieldset>
</div>
</div>


<div id="positions" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 100%">
<legend>ოპერატორების ჯგუფი</legend>
  <div class="input_fild">
  
  <div id="button_area" style="margin-top: 10px">
                <button id="add_button_position">დამატება</button>
                <button id="delete_button_position">წაშლა</button>
            
            </div>
        <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_position" style="margin-right: 10px" >
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 100%">რიგის ნომერი</th>
                
      
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-position" name="check-all-position"/>
                        <label for="check-all-position"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
      </div>
    </div>
</fieldset>
</div>
</div>


<div id="week_time" class="tabcontent" style="margin-left: 51px;">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 100%">
<legend>ზარის პერიოდები</legend>
  <div class="input_fild">
  
  <div id="button_area" style="margin-top: 10px">
                <button id="add_button_week_time">დამატება</button>
                <button id="delete_button_week_time">წაშლა</button>
            
            </div>
        <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_week_time" style="margin-right: 10px" >
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 33%">კვირის დღე</th>
                <th style="width: 33%">ზარის პერიოდის დაწყების დრო</th>
                <th style="width: 33%">ზარის პერიოდის დასრულების დრო</th>
      
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                 <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-week-time" name="check-all-week-time"/>
                        <label for="check-all-week-time"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
        </div>
    </div>
</fieldset>
</div>
</div>


<div id="detail" style="margin-left:51px" class="tabcontent" >
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 450px">

<legend>ბაზის ატვირთვა</legend>
   <div class="input_fild">
            <div id="button_area" style=" margin-top: 10px">
                <button id="get_button_base">დამატება</button>
                <button id="delete_button_details">წაშლა</button>
                
				
				
              
                
            </div>
      <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_details" style="margin-right: 10px; width: 100%">
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 20%">დამატების თარიღი</th>
                <th style="width: 40%">კომენტარი</th>
                <th style="width: 20%">პროგრესი</th>
                <th style="width: 20%">რაოდენობა</th>
                
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-details" name="check-all-details"/>
                        <label for="check-all-details"></label>
                    </div>
                </th>
            </tr>
            </thead>
           
        </table>
        </div>
 
</div>
</fieldset>
</div>

</div>

<div id="voice" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 100%">
<legend>ჩანაწერები</legend>
  <div class="input_fild">
  <div id="button_area" style="margin-top: 10px">
                <button id="add_button_voice">დამატება</button>
                <button id="delete_button_voice" >წაშლა</button>
                <input id="choose_voice" type="file" name="choose_voice" class="input" style="display: none;" accept="audio/*">
            </div>
    <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_voice" style="margin-right: 10px" >
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 60%">დასახელება</th>
                <th style="width: 20%">ტიპი</th>
                <th style="width: 20%">პოზიცია</th>
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-voice" name="check-all-voice"/>
                        <label for="check-all-voice"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
  </div>
</div>
</fieldset>
</div>
</div>
<input type="hidden" id="request_table_id" value="" />	';



    $data .= '<script>

  var answer_wait_time= document.getElementById(\'answer_wait_time\').value;
  var max_retries=document.getElementById(\'max_retries\').value;
  var retry_time=document.getElementById(\'retry_time\').value;
  var done_listen_time=document.getElementById(\'done_listen_time\').value;
  var channel_quantity=document.getElementById(\'channel_quantity\').value;
  var max_sum_listen_time=document.getElementById(\'max_sum_listen_time\').value;
  var code=document.getElementById(\'code\').value;
  var call_type_id=document.getElementById(\'call_type_id\').value;
  
  if(call_type_id==4){
      document.getElementById("voice_bt").style.display="none";
      document.getElementById("week_bt").style.display="none";
      document.getElementById("position_bt").style.display="none";
      document.getElementById("td_1").style.display="none";
      document.getElementById("td_2").style.display="none";
      document.getElementById("td_3").style.display="none";
      document.getElementById("td_4").style.display="none";
      document.getElementById("td_5").style.display="none";
      document.getElementById("td_6").style.display="none";
    
      document.getElementById("tr_2").style.display="none";
      document.getElementById("tr_3").style.display="none";
      document.getElementById("tr_4").style.display="none";
      document.getElementById("tr_5").style.display="none";
      document.getElementById("answer_wait_time").style.display="none";
      document.getElementById("max_retries").style.display="none";
      document.getElementById("retry_time").style.display="none";
      document.getElementById("done_listen_time").style.display="none";
      document.getElementById("channel_quantity").style.display="none";
      document.getElementById("max_sum_listen_time").style.display="none";
  }else if(call_type_id==1) {
      document.getElementById("file_bt").style.display="none";
  }
  function change_input_tab(x){
      if(x==1||x==0){
          document.getElementById("file_bt").style.display="none";
      }else {
          document.getElementById("file_bt").style.display="";
      }
      
      if(x==4 || x==0){
           document.getElementById("voice_bt").style.display="none";
           document.getElementById("position_bt").style.display="none";
           document.getElementById("get_button_base").style.display="none";
           document.getElementById("week_bt").style.display="none";
           document.getElementById("td_1").style.display="none";
      document.getElementById("td_2").style.display="none";
      document.getElementById("td_3").style.display="none";
      document.getElementById("td_4").style.display="none";
      document.getElementById("td_5").style.display="none";
      document.getElementById("td_6").style.display="none";
   
      document.getElementById("tr_2").style.display="none";
      document.getElementById("tr_3").style.display="none";
      document.getElementById("tr_4").style.display="none";
      document.getElementById("tr_5").style.display="none";
      document.getElementById("answer_wait_time").style.display="none";
      document.getElementById("max_retries").style.display="none";
      document.getElementById("retry_time").style.display="none";
      document.getElementById("done_listen_time").style.display="none";
      document.getElementById("channel_quantity").style.display="none";
      document.getElementById("max_sum_listen_time").style.display="none";
      }else {
           document.getElementById("voice_bt").style.display="block";
           document.getElementById("position_bt").style.display="block";
           document.getElementById("week_bt").style.display="block";
           document.getElementById("get_button_base").style.display="";
           document.getElementById("td_1").style.display="";
      document.getElementById("td_2").style.display="";
      document.getElementById("td_3").style.display="";
      document.getElementById("td_4").style.display="";
      document.getElementById("td_5").style.display="";
      document.getElementById("td_6").style.display="";
      
      document.getElementById("tr_2").style.display="";
      document.getElementById("tr_3").style.display="";
      document.getElementById("tr_4").style.display="";
      document.getElementById("tr_5").style.display="";
      document.getElementById("answer_wait_time").style.display="";
      document.getElementById("max_retries").style.display="";
      document.getElementById("retry_time").style.display="";
      document.getElementById("done_listen_time").style.display="";
      document.getElementById("channel_quantity").style.display="";
      document.getElementById("max_sum_listen_time").style.display="";
      }
      
  }
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();
</script>';


    return $data;
}

function GetPage_for_edit($res)
{

    $data = '';
    $data .= '
    <style>
    
    .tab {
        float: left;
        background-color: #f1f1f1;
        margin-top: 38px;
        position: absolute;
        border: 1px solid #000;
        border-left: 0;
    }
    
    .tab button {
      display: block;
      background-color: inherit;
      color: black;
      padding: 10px 15px;
      width: 100%;
      border: none;
      outline: none;
      text-align: left;
      cursor: pointer;
      font-size: 17px;
      height: auto;
    }
    
    
    .tab button:hover {
      background-color: #ddd;
    }
    
    
    .tab button.active {
        background-color: #ffff;
        border-bottom: 1px solid #000;
        border-top: 1px solid #000;
        margin-left: 1px;
    }
    
    .tabcontent {
    
    margin-left:51px;
     
      height: 450px;
     
    }
    .input_fild {
    
    }
    
    
    .tooltip {
      position: relative;
      display: inline-block;
      opacity: 1
      
    }
    
    .tooltip .tooltiptext {
      visibility: hidden;
      width: 150px;
      background-color: #555;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
    
      left: 50%;
      margin-left: 16px;
      opacity: 1;
      transition: opacity 0.3s;
    }
    
    
    
    .tooltip:hover .tooltiptext {
      visibility: visible;
      opacity: 1;
    }
    .tooltiptext{
    position: fixed;
    }
    
#add-edit-form fieldset {
    padding: 0px 3px 3px 12px;
}

    </style>
    
    ';

    $data .= '
	
	    
			<div class="tab">
  <button class="tablinks tooltip" style="border-top: 0" onclick="openCity(event, \'request\')" id="defaultOpen"><img src="media/images/icons/auto_dialer_home.png" height="20px" width="15px"><span class="tooltiptext">ძირითადი ინფორმაცია</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'week_time\')" id="week_bt"><img src="media/images/icons/auto_dialer_periods.png" height="20px" width="20px"><span class="tooltiptext">ზარის პერიოდები</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'voice\')" id="voice_bt"><img src="media/images/icons/auto_dialer_record.png" height="20px" width="20px"><span class="tooltiptext">ჩანაწერები</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'detail\')" id="detail_bt"><img src="media/images/icons/auto_dialer_file.png" height="20px" width="20px"><span class="tooltiptext">ბაზის ატვირთვა</span></button>
  <button class="tablinks tooltip" onclick="openCity(event, \'positions\')" id="position_bt"><img src="media/images/icons/auto_dialer_operator.png" height="20px" width="20px"><span class="tooltiptext">ოპერატორების ჯგუფი</span></button>
  <button class="tablinks tooltip" style="border-bottom: 0" onclick="openCity(event, \'all_file\')" id="file_bt"><img src="media/images/icons/auto_dialer_all_file.png" height="20px" width="20px"><span class="tooltiptext">ფაილები</span></button>
  
</div>

<div id="all_file" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 450px">

<legend>ბაზის ატვირთვა</legend>
   <div class="input_fild">
            <div id="button_area" style=" margin-top: 10px">
            <table>
            <tbody>
            <tr>
            
            <td><button id="delete_button_all_file">წაშლა</button></td>
            <td><button id="all_file_up" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" style="width: 100%" > 
					   <span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
					   <span class="ui-button-text">ფაილის ატვირთვა</span>
			    </button>
			</td>
			<td> <input id="choose_all_file" type="file" name="choose_all_file" class="input" style="display: none;"></td>
            
            </tr>
            
            </tbody>
            
            </table>
               
                
                 
               
                
            </div>
         <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_all_file" style="margin-right: 10px; width: 100%">
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 60%">ფაილის სახელი</th>
                <th style="width: 40%">ტიპი</th>
               
                
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-file" name="check-all-file"/>
                        <label for="check-all-file"></label>
                    </div>
                </th>
            </tr>
            </thead>
           
        </table>
        </div>
 
</div>
</fieldset>
</div>


</div>

<div style="margin-left: 50px;" id="request" class="tabcontent">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 450px">
<legend>ძირითადი ინფორმაცია</legend>
  <table class="dialog-form-table" style="width: 100%;">
                <tr>
                
                <td>ტიპი</td>
                    <td> დასახელება</td>
                    <td>კოდი</td>
                    
                      
                   
                
               </tr>
			
			    <tr>
			    <td>
						<select onchange="change_input_tab(this.value)" id="call_type_id" class="idle" style="width: 95%"  readonly>' . get_nick($res['call_type_id']) . '</select>
					</td>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="name" class="idle"  value="' . $res['name'] . '"  />
					</td>
					<td>
						<input class="idle" type="text" style="width: 95%;" id="code" class="idle"  value="' . $res['code'] . '" onchange="input_type_check(this.value,this.id)" />
					   </td>
					
					
					
				</tr>
				
				<tr>
				  <td>დაწყების თარიღი</td>
                    <td>დასრულების თარიღი</td>
                    <td>ხანგრძლივობა</td>
                   
                    
                
               </tr>
                <tr id="tr_1"> 
                      <td>
						<input type="text" class="idle" style="width: 95%;" id="start_datetime" class="idle"  value="' . $res['start_datetime'] . '"  />
					</td>
                    <td>
						<input type="text" class="idle" style="width: 95%;" id="end_datetime" class="idle"  value="' . $res['end_datetime'] . '"  />
					</td>
					<td>
						<input type="text" class="idle" style="width: 95%;" id="duration" class="idle"  value="' . $res['duration'] . '"  />
					</td>
					
                       
					
				</tr>
				
				<tr id="tr_2">
				    <td id="td_2"> ლოდინის დრო (წამი)</td>
				    <td id="td_3"> განმეორებითი ზარის შუალედი (წამი)</td>
                    <td id="td_4">მაქსიმალური ჯამური ხანგძლივობა (წამი)</td>
                   
				
				
                </tr>
                
                <tr id="tr_3">
                        <td>
						<input class="idle" type="text" style="width: 95%;" id="max_retries" class="idle"  value="' . $res['max_retries'] . '" onchange="input_type_check(this.value,this.id)"  />
					   </td>
                       <td>
						<input class="idle" type="text" style="width: 95%;" id="retry_time" class="idle"  value="' . $res['retry_time'] . '"  onchange="input_type_check(this.value,this.id)" />
					   </td>
					   <td>
						<input class="idle" type="text" style="width: 95%;" id="done_listen_time" class="idle"  value="' . $res['done_listen_time'] . '" onchange="input_type_check(this.value,this.id)" />
					   </td>
					   
					  
                
                </tr>
                
                <tr id="tr_4">
                   <td id="td_1"> განმეორებითი ზარის შუალედი (წამი)</td>
                    <td id="td_5">სრულად მოსმენის დრო (წამი)</td>
                    <td id="td_6">ოპერატორების/არხების რ-ბა</td>
                   
                 
                </tr>
                
                <tr id="tr_5">
                       <td>
						<input class="idle" type="text" style="width: 95%;" id="answer_wait_time" class="idle"  value="' . $res['answer_wait_time'] . '"  onchange="input_type_check(this.value,this.id)" />
					   </td>
                        <td>
						<input class="idle" type="text" style="width: 95%;" id="channel_quantity" class="idle"  value="' . $res['channel_quantity'] . '" onchange="input_type_check(this.value,this.id)" />
					   </td>
                       <td>
						<input class="idle" type="text" style="width: 95%;" id="max_sum_listen_time" class="idle"  value="' . $res['max_sum_listen_time'] . '"  onchange="input_type_check(this.value,this.id)" />
					   </td>
					   
					  
                
                </tr>
                 <tr>
                       <td colspan="3">კომენტარი</td>                             
                </tr>
                <tr>
                  <td colspan="3">
                  <textarea id="comment_main" style=" resize: vertical;width: 98.5%; height: 50px;"> ' . $res['comment_main'] . ' </textarea>
                  </td>
                </tr>
                <tr>
                       <td colspan="3">შენიშვნა</td>                             
                </tr>
                <tr>
                  <td colspan="3">
                  <textarea id="note" style=" resize: vertical;width: 98.5%; height: 50px;">' . $res['note'] . ' </textarea>
                  </td>
                </tr>
               


			</table>
		
		
		
	</fieldset>
</div>
</div>

<div id="positions" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 100%">
<legend>ოპერატორების ჯგუფი</legend>
  <div class="input_fild">
  
        <div id="button_area" style="margin-top: 10px">
            <button id="add_button_position">დამატება</button>
            <button id="delete_button_position">წაშლა</button>
        
        </div>
        <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_position" style="margin-right: 10px" >
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 100%"> რიგის ნომერი</th>
                
      
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"   />
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-position" name="check-all-position"/>
                        <label for="check-all-position"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
    </dov>
</div>
</fieldset>
</div>
</div>



<div id="week_time" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 100%">
<legend>ზარის პერიოდები</legend>
 <div class="input_fild">
  
  <div id="button_area" style="margin-top: 10px">
                <button id="add_button_week_time">დამატება</button>
                <button id="delete_button_week_time">წაშლა</button>
            
            </div>
        <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_week_time" style="margin-right: 10px" >
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 33%">კვირის დღე</th>
                <th style="width: 33%">ზარის პერიოდის დაწყების დრო</th>
                <th style="width: 33%">ზარის პერიოდის დასრულების დრო</th>
      
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-week-time" name="check-all-week-time"/>
                        <label for="check-all-week-time"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
       </div>
</div>
</fieldset>
</div>
</div>


<div id="detail" style="margin-left:51px" class="tabcontent" >
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 450px">

<legend>ბაზის ატვირთვა</legend>
   <div class="input_fild">
            <div id="button_area" style=" margin-top: 10px">';

    if($res['call_type_id']==4){


    }else{
        $data.='  <button id="get_button_base">დამატება</button>';
    }


    $data.='<button id="delete_button_details">წაშლა</button>
                
            </div>
        <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_details" style="margin-right: 10px; width: 100%">
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 20%">დამატების თარიღი</th>
                <th style="width: 40%">კომენტარი</th>
                <th style="width: 20%">პროგრესი</th>
                <th style="width: 20%">რაოდენობა</th>
               
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-details" name="check-all-details"/>
                        <label for="check-all-details"></label>
                    </div>
                </th>
            </tr>
            </thead>
                
        </table>
        </div>
 
</div>
</fieldset>
</div>

</div>

<div id="voice" class="tabcontent" style="margin-left: 51px">
<div id="dialog-form" style="height:100%; width: 99%;">
<fieldset style="height: 100%" >
<legend>ჩანაწერები</legend>
  <div class="input_fild">
  
  <div id="button_area" style="margin-top: 10px">
                <button id="add_button_voice">დამატება</button>
                <button id="delete_button_voice">წაშლა</button>
                <input id="choose_voice" type="file" name="choose_voice" class="input" style="display: none;" accept="audio/*">
            </div>
        <div style="height: 336px; overflow-y: auto;">
         <table class="display" id="example_voice" style="margin-right: 10px" >
            <thead>
            <tr id="datatable_header">

                <th></th>
                <th style="width: 60%">დასახელება</th>
                <th style="width: 20%">ტიპი</th>
                <th style="width: 20%">პოზიცია</th>
                <th class="check">#</th>
            </tr>
            </thead>
            <thead>
            <tr class="search_header">
                <th class="colum_hidden">
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
                <th>
                    <input type="text" name="search_category" value="ფილტრი" class="search_init"/>
                </th>
               
                <th>
                    <div class="callapp_checkbox">
                        <input type="checkbox" id="check-all-voice" name="check-all-voice"/>
                        <label for="check-all-voice"></label>
                    </div>
                </th>
            </tr>
            </thead>
        </table>
    </div>
</div>
</fieldset>
</div>
</div>
    
   <input type="hidden" id="request_table_id" value=' . $res['id'] . ' />	
    
    ';

    $data .= '<script>
  var answer_wait_time= document.getElementById(\'answer_wait_time\').value;
  var max_retries=document.getElementById(\'max_retries\').value;
  var retry_time=document.getElementById(\'retry_time\').value;
  var done_listen_time=document.getElementById(\'done_listen_time\').value;
  var channel_quantity=document.getElementById(\'channel_quantity\').value;
  var max_sum_listen_time=document.getElementById(\'max_sum_listen_time\').value;
  var code=document.getElementById(\'code\').value;
  var call_type_id=document.getElementById(\'call_type_id\').value;
  
  if(call_type_id==4){
      document.getElementById("voice_bt").style.display="none";
      document.getElementById("week_bt").style.display="none";
      document.getElementById("position_bt").style.display="none";
      document.getElementById("td_1").style.display="none";
      document.getElementById("td_2").style.display="none";
      document.getElementById("td_3").style.display="none";
      document.getElementById("td_4").style.display="none";
      document.getElementById("td_5").style.display="none";
      document.getElementById("td_6").style.display="none";
      
      document.getElementById("tr_2").style.display="none";
      document.getElementById("tr_3").style.display="none";
      document.getElementById("tr_4").style.display="none";
      document.getElementById("tr_5").style.display="none";
      document.getElementById("answer_wait_time").style.display="none";
      document.getElementById("max_retries").style.display="none";
      document.getElementById("retry_time").style.display="none";
      document.getElementById("done_listen_time").style.display="none";
      document.getElementById("channel_quantity").style.display="none";
      document.getElementById("max_sum_listen_time").style.display="none";
  }else if(call_type_id==1) {
      document.getElementById("file_bt").style.display="none";
  }
  function change_input_tab(x){
      if(x==1||x==0){
          document.getElementById("file_bt").style.display="none";
      }else {
          document.getElementById("file_bt").style.display="";
      }
      
      if(x==4 || x==0){
           document.getElementById("voice_bt").style.display="none";
           document.getElementById("position_bt").style.display="none";
           document.getElementById("get_button_base").style.display="none";
           document.getElementById("week_bt").style.display="none";
           document.getElementById("td_1").style.display="none";
      document.getElementById("td_2").style.display="none";
      document.getElementById("td_3").style.display="none";
      document.getElementById("td_4").style.display="none";
      document.getElementById("td_5").style.display="none";
      document.getElementById("td_6").style.display="none";
    
      document.getElementById("tr_2").style.display="none";
      document.getElementById("tr_3").style.display="none";
      document.getElementById("tr_4").style.display="none";
      document.getElementById("tr_5").style.display="none";
      document.getElementById("answer_wait_time").style.display="none";
      document.getElementById("max_retries").style.display="none";
      document.getElementById("retry_time").style.display="none";
      document.getElementById("done_listen_time").style.display="none";
      document.getElementById("channel_quantity").style.display="none";
      document.getElementById("max_sum_listen_time").style.display="none";
      }else {
           document.getElementById("voice_bt").style.display="block";
           document.getElementById("position_bt").style.display="block";
           document.getElementById("week_bt").style.display="block";
           document.getElementById("get_button_base").style.display="";
           document.getElementById("td_1").style.display="";
      document.getElementById("td_2").style.display="";
      document.getElementById("td_3").style.display="";
      document.getElementById("td_4").style.display="";
      document.getElementById("td_5").style.display="";
      document.getElementById("td_6").style.display="";
  
      document.getElementById("tr_2").style.display="";
      document.getElementById("tr_3").style.display="";
      document.getElementById("tr_4").style.display="";
      document.getElementById("tr_5").style.display="";
      document.getElementById("answer_wait_time").style.display="";
      document.getElementById("max_retries").style.display="";
      document.getElementById("retry_time").style.display="";
      document.getElementById("done_listen_time").style.display="";
      document.getElementById("channel_quantity").style.display="";
      document.getElementById("max_sum_listen_time").style.display="";
      }
      
  }
  
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();
</script>';


    return $data;
}

?>