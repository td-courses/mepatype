<?php
// require_once('../../includes/classes/core.php');
include('../../includes/classes/class.Mysqli.php');
$mysqli = new dbClass();
$action	= $_REQUEST['act'];
$error	= '';
$data	= array();

switch ($action) {
	case 'get_add_page':
		$page		= GetPage();
		$data		= array('page'	=> $page);

		break;
	case 'get_edit_page':
		$comment_id	 = $_REQUEST['id'];
		$page		 = GetPage(Getcomment($comment_id));
		$data		 = array('page'	=> $page);

		break;
	
	case 'get_list_fb_blacklist' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    
	    $query = "  SELECT    fb_chat_black_list.id,
            				  fb_chat_black_list.datetime,
            				  user_info.name,
            				  fb_chat_black_list.name,
            				  fb_chat_black_list.reason
                    FROM      fb_chat_black_list
                    LEFT JOIN user_info ON user_info.user_id = fb_chat_black_list.user_id
                    WHERE     fb_chat_black_list.actived = 1";
	    
	    $mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);
	    break;
	case 'get_list_site_blacklist' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    
	    $query = "  SELECT    site_chat_black_list.id,
            				  site_chat_black_list.datetime,
            				  user_info.name,
            				  site_chat_black_list.name,
            				  site_chat_black_list.reason
                    FROM      site_chat_black_list
                    LEFT JOIN user_info ON user_info.user_id = site_chat_black_list.user_id
                    WHERE     site_chat_black_list.actived = 1";
	    
		$mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);
	    
	    break;
	case 'get_list_mail_blacklist' :
	    $count	= $_REQUEST['count'];
	    $hidden	= $_REQUEST['hidden'];
	    
	    $query = "  SELECT    mail_chat_black_list.id,
            				  mail_chat_black_list.datetime,
            				  user_info.name,
            				  mail_chat_black_list.sender_id,
            				  mail_chat_black_list.reason
                    FROM      mail_chat_black_list
                    LEFT JOIN user_info ON user_info.user_id = mail_chat_black_list.user_id
                    WHERE     mail_chat_black_list.actived = 1";
	    
	    $mysqli->setQuery($query);
		$data = $mysqli->getList($count,$hidden,1);
	    
	    break;
	
	case 'disable':
		$comment_id	= $_REQUEST['id'];
		$source     = $_REQUEST['source'];
		Disablecomment($comment_id,$source);

		break;
	default:
		$error = 'Action is Null';
}

$data['error'] = $error;

echo json_encode($data);


/* ******************************
 *	Category Functions
* ******************************
*/
function Disablecomment($id,$source){
    global $mysqli;
    if ($source==1){
		$query = "DELETE FROM fb_chat_black_list WHERE id = $id";
		$mysqli -> setQuery($query);
	    $mysqli -> execQuery(); 
    }elseif ($source==2){
		$query = "DELETE FROM site_chat_black_list WHERE id = $id";
		$mysqli -> setQuery($query);
	    $mysqli -> execQuery(); 
    }elseif ($source==3){
		$query = "DELETE FROM mail_chat_black_list WHERE id = $id";
		$mysqli -> setQuery($query);
	    $mysqli -> execQuery(); 
    }
	
}


?>
